﻿namespace PmsLogger
{
    partial class Logger
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Logger));
            this.pb_ie = new System.Windows.Forms.PictureBox();
            this.pb_sougou = new System.Windows.Forms.PictureBox();
            this.pb_360 = new System.Windows.Forms.PictureBox();
            this.lb_error = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pb_ie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_sougou)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_360)).BeginInit();
            this.SuspendLayout();
            // 
            // pb_ie
            // 
            this.pb_ie.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_ie.BackgroundImage")));
            this.pb_ie.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pb_ie.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_ie.Location = new System.Drawing.Point(38, 82);
            this.pb_ie.Name = "pb_ie";
            this.pb_ie.Size = new System.Drawing.Size(139, 137);
            this.pb_ie.TabIndex = 1;
            this.pb_ie.TabStop = false;
            this.pb_ie.Tag = "";
            this.pb_ie.Click += new System.EventHandler(this.pb_ie_Click);
            // 
            // pb_sougou
            // 
            this.pb_sougou.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pb_sougou.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_sougou.BackgroundImage")));
            this.pb_sougou.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_sougou.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pb_sougou.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_sougou.Location = new System.Drawing.Point(237, 82);
            this.pb_sougou.Name = "pb_sougou";
            this.pb_sougou.Size = new System.Drawing.Size(139, 137);
            this.pb_sougou.TabIndex = 2;
            this.pb_sougou.TabStop = false;
            this.pb_sougou.Click += new System.EventHandler(this.pb_sougou_Click);
            // 
            // pb_360
            // 
            this.pb_360.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_360.BackgroundImage")));
            this.pb_360.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_360.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pb_360.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pb_360.Location = new System.Drawing.Point(438, 82);
            this.pb_360.Name = "pb_360";
            this.pb_360.Size = new System.Drawing.Size(139, 137);
            this.pb_360.TabIndex = 3;
            this.pb_360.TabStop = false;
            this.pb_360.Click += new System.EventHandler(this.pb_360_Click);
            // 
            // lb_error
            // 
            this.lb_error.AutoSize = true;
            this.lb_error.ForeColor = System.Drawing.Color.Red;
            this.lb_error.Location = new System.Drawing.Point(227, 143);
            this.lb_error.Name = "lb_error";
            this.lb_error.Size = new System.Drawing.Size(161, 12);
            this.lb_error.TabIndex = 4;
            this.lb_error.Text = "初始化失败，请联系开发者！";
            this.lb_error.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.label1.Location = new System.Drawing.Point(258, 271);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "sdt version:2.2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label2.Location = new System.Drawing.Point(38, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(174, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "点击以下任意浏览器进入系统";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(279, 222);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "未安装";
            this.label3.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(489, 222);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 8;
            this.label4.Text = "未安装";
            this.label4.Visible = false;
            // 
            // Logger
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(625, 284);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lb_error);
            this.Controls.Add(this.pb_360);
            this.Controls.Add(this.pb_sougou);
            this.Controls.Add(this.pb_ie);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Logger";
            this.Text = "SPM配件管理系统";
            this.Load += new System.EventHandler(this.Logger_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pb_ie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_sougou)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_360)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pb_ie;
        private System.Windows.Forms.PictureBox pb_sougou;
        private System.Windows.Forms.PictureBox pb_360;
        private System.Windows.Forms.Label lb_error;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}

