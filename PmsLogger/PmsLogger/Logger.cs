﻿using System;
using System.Diagnostics;
using System.Management;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;

namespace PmsLogger {
    public partial class Logger : Form {
        private static readonly string urlAddress = "http://tspm.sih.cq.cn:81/";  //测试环境
        //private static readonly string urlAddress = "http://spms.saic-hongyan.com/";  //生产环境
        private string allMac = string.Empty;
        private string browser360Url = string.Empty;
        private string browserSougouUrl = string.Empty;
        private static readonly string browserUrl1 = @"Software\Clients\StartMenuInternet\";
        private static readonly string browserUrl2 = @"\Shell\open\command\";
        private static readonly string se360 = "360se6";
        private static readonly string se360Exe = "360se6.exe";
        private static readonly string sogouExplorer = "SogouExplorer";
        private static readonly string sogouExplorerExe = "SogouExplorer.exe";
        private static readonly string strKeyName = string.Empty;
        public Logger() {
            InitializeComponent();
        }

        //初始化
        private void Logger_Load(object sender, EventArgs e) {
            try {
                string mac = "";
                ManagementClass mc;
                //mac地址
                mc = new ManagementClass("Win32_NetworkAdapterConfiguration");
                ManagementObjectCollection moc = mc.GetInstances();
                foreach(ManagementObject mo in moc) {
                    if(mo["MacAddress"] != null && !mac.Contains(mo["MacAddress"].ToString())) {
                        mac += mo["MacAddress"].ToString();
                    }
                }
                var encodeStr = "mac=" + mac;
                byte[] key = Encoding.Unicode.GetBytes(encodeStr); //定义字节数组，用来存储密钥
                allMac = Convert.ToBase64String(key);


                //获取浏览器的地址
                RegistryKey regUserKey = Registry.CurrentUser;
                RegistryKey regMachineKey = Registry.LocalMachine;
                RegistryKey regSub360Key = regUserKey.OpenSubKey(browserUrl1 + se360 + browserUrl2, false);
                if(regSub360Key == null) {
                    regSub360Key = regUserKey.OpenSubKey(browserUrl1 + se360Exe + browserUrl2, false);
                }
                if(regSub360Key == null) {
                    regSub360Key = regMachineKey.OpenSubKey(browserUrl1 + se360 + browserUrl2, false);
                }
                if(regSub360Key == null) {
                    regSub360Key = regMachineKey.OpenSubKey(browserUrl1 + se360Exe + browserUrl2, false);
                }
                RegistryKey regSubSogouKey = regUserKey.OpenSubKey(browserUrl1 + sogouExplorer + browserUrl2, false);
                if(regSubSogouKey == null) {
                    regSubSogouKey = regUserKey.OpenSubKey(browserUrl1 + sogouExplorerExe + browserUrl2, false);
                }
                if(regSubSogouKey == null) {
                    regSubSogouKey = regMachineKey.OpenSubKey(browserUrl1 + sogouExplorer + browserUrl2, false);
                }
                if(regSubSogouKey == null) {
                    regSubSogouKey = regMachineKey.OpenSubKey(browserUrl1 + sogouExplorerExe + browserUrl2, false);
                }
                if(regSub360Key != null) {
                    object objResult = regSub360Key.GetValue(strKeyName);
                    RegistryValueKind regValueKind = regSub360Key.GetValueKind(strKeyName);
                    if(regValueKind == RegistryValueKind.String) {
                        browser360Url = objResult.ToString();
                    }
                }
                if(string.IsNullOrEmpty(browser360Url)) {
                    label4.Visible = true;
                }
                if(regSubSogouKey != null) {
                    object objResult = regSubSogouKey.GetValue(strKeyName);
                    RegistryValueKind regValueKind = regSubSogouKey.GetValueKind(strKeyName);
                    if(regValueKind == RegistryValueKind.String) {
                        browserSougouUrl = objResult.ToString();
                    }
                }
                if(string.IsNullOrEmpty(browserSougouUrl)) {
                    label3.Visible = true;
                }
            } catch(Exception ex) {
                lb_error.Visible = true;
                pb_ie.Visible = false;
                pb_sougou.Visible = false;
                pb_360.Visible = false;
            }
        }

        private void pb_ie_Click(object sender, EventArgs e) {
            ProcessStartInfo wordProcess = new ProcessStartInfo {
                FileName = "IEXPLORE.EXE",
                Arguments = urlAddress + "?" + allMac
            };
            Process.Start(wordProcess);
            //this.Close();
        }

        private void pb_sougou_Click(object sender, EventArgs e) {
            if(!string.IsNullOrEmpty(browserSougouUrl)) {
                Process.Start(browserSougouUrl, urlAddress + "?" + allMac);
                //this.Close();
            } else {
                MessageBox.Show("请安装搜狗浏览器！", "提醒");
            }
        }

        private void pb_360_Click(object sender, EventArgs e) {
            if(!string.IsNullOrEmpty(browser360Url)) {
                Process.Start(browser360Url, urlAddress + "?" + allMac);
                //this.Close();
            } else {
                MessageBox.Show("请安装360浏览器！", "提醒");
            }
        }
    }
}
