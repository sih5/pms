﻿using System.Windows.Forms;

namespace Sunlight.Ce.Controls {
    public class CustomMenuItem : MenuItem {
        public object Tag {
            get;
            set;
        }
    }
}
