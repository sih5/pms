using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Sunlight.Ce.Controls {
    /// <summary>
    /// 此类解决窗体非全屏的问题
    /// </summary>
    public class CustomScreenForm : Form {
        private bool centered = true;

        [DllImport("coredll.dll")]
        private static extern UInt32 SetWindowLong(IntPtr hWnd, int nIndex, UInt32 dwNewLong);

        //[DllImport("aygshell.dll")]
        //private static extern int SHDoneButton(IntPtr hwndRequester, UInt32 dwState);
        private readonly int GWL_STYLE = (-16);
        private readonly UInt32 WS_CAPTION = 0x00C00000;     /* WS_BORDER | WS_DLGFRAME  */
        private readonly UInt32 WS_BORDER = 0x00800000;
        private readonly UInt32 WS_POPUP = 0x80000000;
        //private readonly UInt32 SHDB_SHOW = 0x0001;
        //private readonly UInt32 SHDB_HIDE = 0x0002;

        public bool CenterFormOnScreen {
            get {
                return this.centered;
            }
            set {
                this.centered = value;

                if (this.centered) {
                    this.CenterWithinScreen();
                }
            }
        }

        protected override void OnLoad(EventArgs e) {
            //默认情况下，如果你在设置窗体的大小，在Visual Studio窗体设计器也不会考虑到的额外高度
            //标题，所以我们将在这里补充一点，高度
            this.Height += SystemInformation.MenuHeight;
            base.OnLoad(e);
            //当我们设置窗体的FormBorderStyle属性为None
            //边框和标题将无效，我们在WIN32的层面来做，这将导致.NET Compact Framework的包装，以不同步
            uint style = this.WS_BORDER | this.WS_CAPTION | this.WS_POPUP;
            SetWindowLong(this.Handle, this.GWL_STYLE, style);
            // Add/Remove an [OK] button from the dialog's
            // caption bar as required
            //SHDoneButton(this.Handle, this.ControlBox ? this.SHDB_SHOW : this.SHDB_HIDE);
            // Center the form if requested
            if (this.centered) {
                this.CenterWithinScreen();
            }
        }

        protected override void OnResize(EventArgs e) {
            base.OnResize(e);
            if (this.centered) {
                this.CenterWithinScreen();
            }
        }

        //Key 事件
        protected override void OnKeyDown(KeyEventArgs e) {
            base.OnKeyDown(e);
            if (this.ControlBox) {
                if (e.KeyCode == Keys.Return || e.KeyCode == Keys.Escape) {
                    this.DialogResult = DialogResult.OK;
                }
            }
        }

        //移动位置处理
        private void CenterWithinScreen() {
            int x = (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2;
            int y = (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2;
            this.Location = new Point(x, y);
        }
    }
}
