using System.Windows.Forms;

namespace Sunlight.Ce.Controls {
    public class DataGridCustomComboBoxColumn : DataGridCustomTextBoxColumn {
        protected override Control CreateHostedControl() {
            var box = new ComboBox();
            box.DropDownStyle = ComboBoxStyle.DropDown;
            return box;
        }
    }
}
