using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Sunlight.Ce.Controls {
    public class DataGridCustomNumericColumn : DataGridCustomColumnBase {
        //private string txtNumeric;
        /// <summary>
        /// 整数或者小数验证
        /// </summary>
        public const string NUMERIC_REGEX = "^\\d+(\\.\\d+)?$";

        public virtual TextBox TextBox {
            get { return this.HostedControl as TextBox; }
        }

        protected override string GetBoundPropertyName() {
            return "Text";
        }

        protected override Control CreateHostedControl() {
            var box = new TextBox();
            box.BorderStyle = BorderStyle.None;
            box.Multiline = false;
            box.WordWrap = false;
            box.AcceptsReturn = false;
            box.AcceptsTab = false;
            box.TextAlign = this.Alignment;
            box.GotFocus -= this.box_GotFocus;
            box.GotFocus += this.box_GotFocus;

            box.LostFocus -= this.box_LostFocus;
            box.LostFocus += this.box_LostFocus;

            box.KeyUp -= this.box_KeyUp;
            box.KeyUp += this.box_KeyUp;
            return box;
        }

        private void box_KeyUp(object sender, KeyEventArgs e) {
            var txtBox = sender as TextBox;
            if (txtBox == null)
                return;
            if (e.KeyCode != Keys.Enter)
                return;
            if (string.IsNullOrEmpty(txtBox.Text))
                return;
            var validTxt = txtBox.Text.Replace("\r\n", "");
            if (string.IsNullOrEmpty(validTxt))
                return;
            validTxt = validTxt.Replace("\n", "");
            if (string.IsNullOrEmpty(validTxt))
                return;
            validTxt = validTxt.Replace("\r", "");
            if (validTxt.Contains("。")) {
                validTxt = validTxt.Replace("。", ".");
            }
            if (validTxt.Contains("．")) {
                validTxt = validTxt.Replace("．", ".");
            }
            if (validTxt.Contains(" ")) {
                validTxt = validTxt.Replace(" ", "");
            }

            txtBox.Text = !this.IsNumerric(validTxt) ? "0" : Convert.ToDecimal(validTxt).ToString(CultureInfo.InvariantCulture);
        }

        private void box_LostFocus(object sender, EventArgs e) {
            var txtBox = sender as TextBox;
            if (txtBox == null)
                return;
            if (string.IsNullOrEmpty(txtBox.Text))
                return;
            if (!this.IsNumerric(txtBox.Text)) {
                txtBox.Text = "0";
                return;
            }
            txtBox.Text = Convert.ToDecimal(txtBox.Text).ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// 验证是否是整数或者小数
        /// </summary>
        /// <param name="content">验证文本</param>
        /// <returns>返回验证结果</returns>
        public bool IsNumerric(string content) {
            return new Regex(NUMERIC_REGEX).IsMatch(content);
        }

        private void box_GotFocus(object sender, EventArgs e) {
            var txtBox = sender as TextBox;
            if (txtBox == null)
                return;
            this.updateHostedControl();
        }
    }
}
