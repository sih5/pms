﻿//--------------------------------------------------------------------- 
//THIS CODE AND INFORMATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY 
//KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE 
//IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A 
//PARTICULAR PURPOSE. 
//---------------------------------------------------------------------

using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;

namespace Sunlight.Ce.Controls {
    public partial class DataGridCustomSelectLinesTextBoxColumn : DataGridCustomColumnBase {

        //选择行的背景颜色
        private Color _selectRowBackgroundColor = SystemColors.Window;

        //选择的行的集合
        private List<int> _selectRowList = new List<int>();

        //文本框
        public virtual TextBox TextBox {
            get { return this.HostedControl as TextBox; }
        }

        //返回特性的名称
        protected override string GetBoundPropertyName() {
            return "Text";
        }

        //创建控件
        protected override Control CreateHostedControl() {
            TextBox box = new TextBox();

            box.BorderStyle = BorderStyle.None;
            box.Multiline = true;
            box.TextAlign = this.Alignment;

            return box;
        }


        /// <summary>
        /// 选择多行的背景颜色
        /// </summary>
        public Color SelectRowBackGroundColor {
            set {
                _selectRowBackgroundColor = value;
            }
            get {
                return _selectRowBackgroundColor;
            }
        }

        /// <summary>
        /// 选择多行的集合
        /// </summary>
        public List<int> SelectRowList {
            set {
                _selectRowList = value;
            }
            get {
                return _selectRowList;
            }
        }

        /// <summary>
        /// 绘制背景
        /// </summary>
        /// <param name="g"></param>
        /// <param name="bounds"></param>
        /// <param name="rowNum"></param>
        /// <param name="backBrush"></param>
        protected override void DrawBackground(Graphics g, Rectangle bounds, int rowNum, Brush backBrush) {

            Brush background;

            //判断交替色是否设置并且是交替行
            if (AlternatingBackColor != null && rowNum % 2 == 0) {

                //设置成交替色
                background = new SolidBrush(AlternatingBackColor);
            }
            else {
                //设置成原有背景色彩
                background = backBrush;
            }

            //判断多行选择背景色不为null且指定行存在于列表中
            if ((null != _selectRowList) && _selectRowList.Contains(rowNum)) {
                //设置多行选择背景色
                background = new SolidBrush(_selectRowBackgroundColor);
            }

            //绘制形状
            g.FillRectangle(background, bounds);
        }
    }
}
