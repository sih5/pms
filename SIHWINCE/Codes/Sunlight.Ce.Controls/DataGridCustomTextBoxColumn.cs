using System.Windows.Forms;

namespace Sunlight.Ce.Controls {
    public class DataGridCustomTextBoxColumn : DataGridCustomColumnBase {
        public virtual TextBox TextBox {
            get { return this.HostedControl as TextBox; }
        }

        protected override string GetBoundPropertyName() {
            return "Text";
        }

        protected override Control CreateHostedControl() {
            var box = new TextBox();
            box.BorderStyle = BorderStyle.None;
            box.Multiline = true;
            box.TextAlign = this.Alignment;
            return box;
        }
    }
}
