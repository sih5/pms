using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using System.Text;

namespace Sunlight.Ce.Controls {
    public partial class MessageBoxForm : CustomScreenForm {
        private SizeF myAutoScaleFactor;
        private MessageBeepType beepType = MessageBeepType.Default;

        #region Native Platform Invoke

        [DllImport("coredll.dll")]
        private static extern int MessageBeep(MessageBeepType uType);

        [DllImport("coredll.dll")]
        private static extern int DrawText(IntPtr hDc, string lpString, int nCount, ref RECT lpRect, uint uFormat);

        [DllImport("coredll.dll")]
        private static extern IntPtr SelectObject(IntPtr hDc, IntPtr hobj);

        [DllImport("coredll.dll")]
        private static extern int DeleteObject(IntPtr hgdiobj);

        public enum MessageBeepType : uint {
            None = 0xFFFFFFFE,
            Simple = 0xFFFFFFFF,            // Simple beep
            Default = 0x00000000,           // MB_OK
            SystemHand = 0x00000010,        // MB_ICONHAND
            SystemQuestion = 0x00000020,    // MB_ICONQUESTION
            SystemExclamation = 0x00000030, // MB_ICONEXCLAMATION
            SystemAsterisk = 0x00000040     // MB_ICONASTERISK
        }

        private struct RECT {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;

            public RECT(Rectangle rc) {
                this.Left = rc.Left;
                this.Right = rc.Right;
                this.Top = rc.Top;
                this.Bottom = rc.Bottom;
            }
        }

        private static readonly uint DT_LEFT = 0x00000000;
        private static readonly uint DT_TOP = 0x00000000;
        private static readonly uint DT_WORDBREAK = 0x00000010;
        private static readonly uint DT_CALCRECT = 0x00000400;
        private static readonly uint DT_NOPREFIX = 0x00000800;

        #endregion

        public MessageBoxForm() {
            this.InitializeComponent();
            this.myAutoScaleFactor = new SizeF(
              this.AutoScaleDimensions.Width / 96F,
              this.AutoScaleDimensions.Height / 96F);
        }

        public static DialogResult Show(Form owner, string message) {
            return Show(owner, message, null, null);
        }

        public static DialogResult Show(Form owner, string message, string caption) {
            return Show(owner, message, caption, null);
        }

        public static DialogResult Show(Form owner, string message, Dictionary<string, DialogResult> buttons) {
            return Show(owner, message, null, buttons);
        }

        public static DialogResult Show(Form owner, string message, string caption, Dictionary<string, DialogResult> buttons) {
            System.Media.SystemSounds.Exclamation.Play();
            Thread.Sleep(150);
            System.Media.SystemSounds.Exclamation.Play();
            var form = new MessageBoxForm();
            form.Owner = owner;
            form.Text = caption;
            //新加
            var newFont = new Font("Arial", 10, FontStyle.Regular);
            //
            form.lblMessage.Text = message;
            //if (!form.lblMessage.Text.Contains("\r\n")) {
            //    var stringBuilder = new StringBuilder();
            //    try {
            //        if (message.Length <= 13)
            //            stringBuilder.Append(message);
            //        else {
            //            var rowLine = (int)(Math.Ceiling(message.Length * 1.0 / 13));

            //            for (var row = 1; row <= rowLine; row++) {
            //                if (message.Substring((row - 1) * 13).Length >= 13)
            //                    stringBuilder.AppendLine(message.Substring((row - 1) * 13, 13));
            //                else {
            //                    stringBuilder.AppendLine(message.Substring((row - 1) * 13));
            //                }
            //            }
            //        }
            //    }
            //    catch (Exception) {
            //        //MessageBox.Show("弹框基类算法出错，请联系管理员");
            //        stringBuilder = new StringBuilder();
            //        stringBuilder.Append(message);
            //    }
            //    message = stringBuilder.ToString();
            form.lblMessage.Text = message;
            //}

            form.ControlBox = (buttons == null || buttons.Count == 0);
            SizeFormToContent(form, !form.ControlBox);
            if(!form.ControlBox) {
                form.SuspendLayout();

                int buttonWidth = (form.pnlButtons.Width - (int)(form.myAutoScaleFactor.Width * 2) - (buttons.Count - 1) * form.pnlButtons.Left) / buttons.Count;
                foreach(KeyValuePair<string, DialogResult> kp in buttons) {
                    var button = new Button();
                    //新加
                    button.Font = newFont;
                    //
                    button.Text = kp.Key;
                    button.Height = 24;
                    button.DialogResult = kp.Value;
                    if(buttons != null && buttons.Count == 1) {
                        button.Dock = DockStyle.Right;
                        button.Width = 70;
                    }
                    else {
                        button.Dock = DockStyle.Left;
                        button.Width = buttonWidth;
                    }

                    form.pnlButtons.Controls.Add(button);
                    form.pnlButtons.Controls.SetChildIndex(button, 0);
                    var seperator = new Panel();
                    seperator.Width = form.pnlButtons.Left;
                    seperator.Dock = DockStyle.Left;
                    form.pnlButtons.Controls.Add(seperator);
                    form.pnlButtons.Controls.SetChildIndex(seperator, 0);
                }

                form.ResumeLayout();
            }
            return form.ShowDialog();
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            if(this.beepType != MessageBeepType.None) {
                MessageBeep(this.beepType);
            }
        }

        private static Size CalcTextSize(MessageBoxForm form) {
            var bounds = new RECT(form.lblMessage.ClientRectangle);
            int titleWidth;
            using(Graphics g = form.CreateGraphics()) {
                IntPtr hdc = IntPtr.Zero;
                IntPtr hFont = IntPtr.Zero;
                try {
                    hdc = g.GetHdc();
                    hFont = form.lblMessage.Font.ToHfont();
                    IntPtr oldFont = SelectObject(hdc, hFont);
                    DrawText(hdc, form.lblMessage.Text, form.lblMessage.Text.Length, ref bounds, DT_LEFT | DT_TOP | DT_CALCRECT | DT_NOPREFIX | DT_WORDBREAK);
                    SelectObject(hdc, oldFont);
                    titleWidth = (int)(g.MeasureString(form.Text, form.Font).Width * 2.2) + SystemInformation.MenuHeight;
                }
                finally {
                    if(hFont != IntPtr.Zero) {
                        DeleteObject(hFont);
                    }

                    if(hdc != IntPtr.Zero) {
                        g.ReleaseHdc(hdc);
                    }
                }
            }

            if((bounds.Right - bounds.Left) < titleWidth)
                return new Size(titleWidth, bounds.Bottom - bounds.Top);
            return new Size(bounds.Right - bounds.Left + (int)(form.myAutoScaleFactor.Width * 4), bounds.Bottom - bounds.Top);
        }

        private static void SizeFormToContent(MessageBoxForm form, bool hasButtons) {
            form.pnlButtons.Visible = hasButtons;
            form.Width = Screen.PrimaryScreen.WorkingArea.Width / 10 * 9;
            Size textSize = CalcTextSize(form);
            int widthInternal;
            const int BOTTOM = 10;
            int heightInternal;
            var rows = 1;
            if(textSize.Width > (158 + form.pnlButtons.Left * 2)) {
                widthInternal = 158;
                rows = (int)(Math.Ceiling(textSize.Width * 1.0 / widthInternal));
                heightInternal = rows * textSize.Height;//文本行数*行高
            }
            else {
                widthInternal = textSize.Width + 2;
                heightInternal = textSize.Height;
            }
            int width = 2 * form.lblMessage.Left + widthInternal;
            if(width < 120)
                width = 120;
            int height = 2 * form.lblMessage.Top + heightInternal;
            if(hasButtons)
                height = form.lblMessage.Top * 2 + form.pnlButtons.Height + heightInternal + BOTTOM;

            form.pnlButtons.Location = new Point(form.pnlButtons.Left, 2 * form.lblMessage.Top + heightInternal);
            form.lblMessage.Height = 2 * form.lblMessage.Top + rows * 16;
            form.Size = new Size(width, height);
        }
    }
    /// <summary>
    /// 自动关闭窗体,可以进行设置关闭的时间
    /// </summary>
    public class AutoClosingMessageBox {
        System.Threading.Timer _timeoutTimer;
        string _caption;
        AutoClosingMessageBox(string text, string caption, int timeout) {
            _caption = caption;
            _timeoutTimer = new System.Threading.Timer(OnTimerElapsed,
                null, timeout, System.Threading.Timeout.Infinite);
            MessageBox.Show(text, caption);
        }
        public static void Show(string text, string caption, int timeout) {
            new AutoClosingMessageBox(text, caption, timeout);
        }
        void OnTimerElapsed(object state) {
            IntPtr mbWnd = FindWindow(null, _caption);
            if(mbWnd != IntPtr.Zero)
                SendMessage(mbWnd, WM_CLOSE, IntPtr.Zero, IntPtr.Zero);
            _timeoutTimer.Dispose();
        }
        const int WM_CLOSE = 0x0010;
        [System.Runtime.InteropServices.DllImport("coredll.dll", SetLastError = true)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        [System.Runtime.InteropServices.DllImport("coredll.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, IntPtr wParam, IntPtr lParam);
    }


    public class ShowMsg {

        [DllImport("coredll.dll", SetLastError = true)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("coredll.dll")]
        static extern bool EndDialog(IntPtr hDlg, out IntPtr nResult);

        //三个参数：1、文本提示-text，2、提示框标题-caption，3、按钮类型-MessageBoxButtons ，4、自动消失时间设置-timeout
        public static void ShowMessageBoxTimeout(string text, string caption,
            MessageBoxButtons buttons, int timeout) {
            ThreadPool.QueueUserWorkItem(new WaitCallback(CloseMessageBox),
                new CloseState(caption, timeout));
            MessageBox.Show(text, caption, buttons, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button2);
        }


        private static void CloseMessageBox(object state) {
            CloseState closeState = state as CloseState;

            Thread.Sleep(closeState.Timeout);
            IntPtr dlg = FindWindow(null, closeState.Caption);

            if(dlg != IntPtr.Zero) {
                IntPtr result;
                EndDialog(dlg, out result);
            }
        }
    }
    public class CloseState {
        private int _Timeout;

        /**/
        /// <summary>
        /// In millisecond
        /// </summary>
        public int Timeout {
            get {
                return _Timeout;
            }
        }

        private string _Caption;

        /**/
        /// <summary>
        /// Caption of dialog
        /// </summary>
        public string Caption {
            get {
                return _Caption;
            }
        }

        public CloseState(string caption, int timeout) {
            _Timeout = timeout;
            _Caption = caption;
        }
    }

    /*------------------------------调用代码----------------------------
  * AutoClosedMsgBox.Show("嘘...\r\n窗口5秒后关闭...", "提示", 5000);
  * AutoClosedMsgBox.Show("嘘...\r\n窗口5秒后关闭...", "提示", 5000, 0);
  * AutoClosedMsgBox.Show("嘘...\r\n窗口5秒后关闭...", "提示", 5000, MsgBoxStyle.OK);
  */
    public class AutoClosedMsgBox {
        [DllImport("coredll.dll", EntryPoint = "FindWindow", CharSet = CharSet.Auto)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("coredll.dll")]
        static extern bool EndDialog(IntPtr hDlg, int nResult);

        [DllImport("coredll.dll")]
        static extern int MessageBoxTimeout(IntPtr hwnd, string txt, string caption,
            int wtype, int wlange, int dwtimeout);

        const int WM_CLOSE = 0x10;
        /// <summary>
        /// 弹出自动关闭的MessageBox窗口，只有“确定”按钮
        /// </summary>
        /// <param name="text">弹出窗口的显示内容</param>
        /// <param name="caption">弹出窗口的标题</param>
        /// <param name="milliseconds">窗口持续显示时间(毫秒)</param>
        /// <returns>固定返回DialogResult.OK</returns>
        public static DialogResult Show(string text, string caption, int milliseconds) {
            System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
            timer.Interval = milliseconds;
            timer.Tick += (a, b) => {
                IntPtr ptr = FindWindow(null, caption);
                if(ptr != IntPtr.Zero) EndDialog(ptr, 0);
                timer.Enabled = false;
            };
            timer.Enabled = true;
            MessageBox.Show(text, caption);
            return DialogResult.OK;
        }

        /// <summary>
        /// 弹出自动关闭的MessageBox窗口，有多种显示方式
        /// </summary>
        /// <param name="txt">弹出窗口的显示内容</param>
        /// <param name="caption">弹出窗口的标题</param>
        /// <param name="style">窗口样式(枚举)</param>
        /// <param name="dwtimeout">窗口持续显示时间(毫秒)</param>
        /// <returns>0-无显示 1-确定 2-取消 3-终止 4-重试 5-忽略 6-是 7-否 10-重试 11-继续 32000-系统关闭</returns>
        public static int Show(string text, string caption, int milliseconds, MsgBoxStyle style) {
            return MessageBoxTimeout(IntPtr.Zero, text, caption, (int)style, 0, milliseconds);
        }

        public static int Show(string text, string caption, int milliseconds, int style) {
            return MessageBoxTimeout(IntPtr.Zero, text, caption, style, 0, milliseconds);
        }
    }
    public enum MsgBoxStyle {
        OK = 0, OKCancel = 1, AbortRetryIgnore = 2, YesNoCancel = 3, YesNo = 4,
        RetryCancel = 5, CancelRetryContinue = 6,

        //红叉 + ...
        RedCritical_OK = 16, RedCritical_OKCancel = 17, RedCritical_AbortRetryIgnore = 18,
        RedCritical_YesNoCancel = 19, RedCritical_YesNo = 20,
        RedCritical_RetryCancel = 21, RedCritical_CancelRetryContinue = 22,

        //蓝问号 + ...
        BlueQuestion_OK = 32, BlueQuestion_OKCancel = 33, BlueQuestion_AbortRetryIgnore = 34,
        BlueQuestion_YesNoCancel = 35, BlueQuestion_YesNo = 36,
        BlueQuestion_RetryCancel = 37, BlueQuestion_CancelRetryContinue = 38,

        //黄叹号 + ...
        YellowAlert_OK = 48, YellowAlert_OKCancel = 49, YellowAlert_AbortRetryIgnore = 50,
        YellowAlert_YesNoCancel = 51, YellowAlert_YesNo = 52,
        YellowAlert_RetryCancel = 53, YellowAlert_CancelRetryContinue = 54,

        //蓝叹号 + ...
        BlueInfo_OK = 64, BlueInfo_OKCancel = 65, BlueInfo_AbortRetryIgnore = 66,
        BlueInfo_YesNoCancel = 67, BlueInfo_YesNo = 68,
        BlueInfo_RetryCancel = 69, BlueInfo_CancelRetryContinue = 70,
    }
}