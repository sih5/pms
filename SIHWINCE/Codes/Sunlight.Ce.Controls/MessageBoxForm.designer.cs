namespace Sunlight.Ce.Controls
{
  partial class MessageBoxForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
      private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (this.components != null))
      {
        this.components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageBoxForm));
        this.lblMessage = new System.Windows.Forms.Label();
        this.pnlButtons = new System.Windows.Forms.Panel();
        this.SuspendLayout();
        // 
        // lblMessage
        // 
        resources.ApplyResources(this.lblMessage, "lblMessage");
        this.lblMessage.Name = "lblMessage";
        // 
        // pnlButtons
        // 
        resources.ApplyResources(this.pnlButtons, "pnlButtons");
        this.pnlButtons.Name = "pnlButtons";
        // 
        // MessageBoxForm
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
        this.CenterFormOnScreen = true;
        resources.ApplyResources(this, "$this");
        this.Controls.Add(this.pnlButtons);
        this.Controls.Add(this.lblMessage);
        this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
        this.MinimizeBox = false;
        this.Name = "MessageBoxForm";
        this.TopMost = true;
        this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lblMessage;
    private System.Windows.Forms.Panel pnlButtons;
  }
}