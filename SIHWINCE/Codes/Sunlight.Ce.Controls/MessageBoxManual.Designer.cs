﻿namespace Sunlight.Ce.Controls
{
    partial class MessageBoxManual
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMessage = new System.Windows.Forms.Label();
            this.btnhide = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblMessage
            // 
            this.lblMessage.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lblMessage.Location = new System.Drawing.Point(9, 9);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(173, 40);
            this.lblMessage.Text = "This is my example message!";
            // 
            // btnhide
            // 
            this.btnhide.Location = new System.Drawing.Point(179, 0);
            this.btnhide.Name = "btnhide";
            this.btnhide.Size = new System.Drawing.Size(12, 10);
            this.btnhide.TabIndex = 5;
            this.btnhide.Visible = false;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnClose.Location = new System.Drawing.Point(9, 51);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(173, 26);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "确认";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // MessageBoxManual
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(191, 86);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnhide);
            this.Controls.Add(this.lblMessage);
            this.KeyPreview = true;
            this.Location = new System.Drawing.Point(24, 104);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MessageBoxManual";
            this.Text = "MessageBoxManual";
            this.TopMost = true;
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Button btnhide;
        private System.Windows.Forms.Button btnClose;
    }
}