﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace Sunlight.Ce.Controls
{
    public partial class MessageBoxManual : Form
    {
        public MessageBoxManual(string message)
        {
            InitializeComponent();
            this.lblMessage.Text = message;
            this.btnhide.Focus();
        }

        public static void Show(Form owner, string message, string caption){
            System.Media.SystemSounds.Exclamation.Play();
            Thread.Sleep(150);
            System.Media.SystemSounds.Exclamation.Play();
            var form = new MessageBoxManual(message);
            form.Owner = owner;
            form.Text = caption;
            form.ShowDialog();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}