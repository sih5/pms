﻿namespace Sunlight.Ce.Entities.DTO {
    public class BoxUpTaskDTO {
        /// <summary>
        /// 装箱任务单id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 装箱任务单编号
        /// </summary>
        public string Code {
            get;
            set;
        }
        /// <summary>
        /// 装箱状态
        /// </summary>
        public int Status {
            get;
            set;
        }

        /// <summary>
        /// 仓库Id
        /// </summary>
        public int WarehouseId {
            get;
            set;
        }
        /// <summary>
        /// 清单Id
        /// </summary>
        public int DetailId {
            get;
            set;
        }

        /// <summary>
        /// 出库计划单ID
        /// </summary>
        public int PartsOutboundPlanId {
            get;
            set;
        }

        /// <summary>
        /// 源单据编号
        /// </summary>
        public string SourceCode {
            get;
            set;
        }
        /// <summary>
        /// 备件id
        /// </summary>
        public int? SparePartId {
            get;
            set;
        }

        /// <summary>
        ///     备件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        ///     备件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        ///     箱号
        /// </summary>
        public string ContainerNumber {
            get;
            set;
        }
        /// <summary>
        /// 计划量
        /// </summary>
        public int PlanQty {
            get;
            set;
        }

        /// <summary>
        /// 已装箱数量
        /// </summary>
        public int? BoxUpQty {
            get;
            set;
        }

        /// <summary>
        /// 本次装箱量
        /// </summary>
        public int? ThisBoxUpQty {
            get;
            set;
        }

        /// <summary>
        /// 是否紧急
        /// </summary>
        public string isUrgent {
            get;
            set;
        }
    }
}
