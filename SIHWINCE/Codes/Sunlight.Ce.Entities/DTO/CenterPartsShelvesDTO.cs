﻿namespace Sunlight.Ce.Entities.DTO {
    public class CenterPartsShelvesDTO {
        public string BatchNumber {
            get;
            set;
        }
        public int BranchId {
            get;
            set;
        }
        public int? LockedQty {
            get;
            set;
        }
        public string MeasureUnit {
            get;
            set;
        }
        public int? OutingQty {
            get;
            set;
        }
        public int PartsStockId {
            get;
            set;
        }
        public int Quantity {
            get;
            set;
        }
        public string SparePartCode {
            get;
            set;
        }
        public int SparePartId {
            get;
            set;
        }
        public string SparePartName {
            get;
            set;
        }
        public int UsableQuantity {
            get;
            set;
        }
        public int WarehouseAreaCategory {
            get;
            set;
        }
        public string WarehouseAreaCode {
            get;
            set;
        }
        public int WarehouseAreaId {
            get;
            set;
        }
        public string WarehouseCode {
            get;
            set;
        }
        public int WarehouseId {
            get;
            set;
        }
        public string WarehouseName {
            get;
            set;
        }
        public int StorageCompanyId {
            get;
            set;
        }
        public int StorageCompanyType {
            get;
            set;
        }
        /// <summary>
        /// 推荐货位Id
        /// </summary>
        public int RecommendWarehouseAreaId {
            get;
            set;
        }
        /// <summary>
        /// 推荐货位编号
        /// </summary>
        public string RecommendWarehouseAreaCode {
            get;
            set;
        }
        /// <summary>
        /// 目标货位Id
        /// </summary>
        public int DestWarehouseAreaId {
            get;
            set;
        }
        /// <summary>
        /// 目标货位编号
        /// </summary>
        public string DestWarehouseAreaCode {
            get;
            set;
        }

        /// <summary>
        /// 本次上架量
        /// </summary>
        public int ThisShelvesAmount {
            get;
            set;
        }
    }
}
