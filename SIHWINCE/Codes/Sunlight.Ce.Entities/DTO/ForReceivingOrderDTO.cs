﻿namespace Sunlight.Ce.Entities.DTO {
    public class ForReceivingOrderDTO {
        /// <summary>
        /// 单号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        ///     状态
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 备件id
        /// </summary>
        public int PartsId { get; set; }

        /// <summary>
        ///     备件编号
        /// </summary>
        public string PartsCode { get; set; }

        /// <summary>
        /// 供应商图号
        /// </summary>
        public string SupplierPartCode { get; set; }

        /// <summary>
        ///     备件名称
        /// </summary>
        public string PartsName { get; set; }

        /// <summary>
        ///     待收数量
        /// </summary>
        public int PlannedAmount { get; set; }

        /// <summary>
        ///     实收数量
        /// </summary>
        public int? ReceiveAmount { get; set; }

        /// <summary>
        ///     入库类型
        /// </summary>
        public int InboundType { get; set; }

        /// <summary>
        ///     已收货数量
        /// </summary>
        public int? ReceivedAmount { get; set; }

        /// <summary>
        /// 是否包装
        /// </summary>
        public bool? IsPacking { get; set; }

        public decimal? Price {
            get;
            set;
        }
    }
}
