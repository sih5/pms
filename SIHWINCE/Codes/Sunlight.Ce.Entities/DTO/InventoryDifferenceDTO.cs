﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Sunlight.Ce.Entities.DTO {
    public class InventoryDifferenceDTO {
        public InventoryDifferenceDTO() {
            SIHCodes = new List<string>();
        }

        public string WarehouseAreaCode { get; set; }

        public string SparePartCode { get; set; }

        public List<string> SIHCodes { get; set; }

        public int Quantity { get; set; }
    }
}
