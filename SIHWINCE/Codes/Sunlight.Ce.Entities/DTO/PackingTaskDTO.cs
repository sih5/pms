﻿using System.Collections.Generic;
namespace Sunlight.Ce.Entities.DTO {
    public class PackingTaskDTO {
        public PackingTaskDTO() {
            this.BoxCodes = new List<string>();
            this.TraceCodes = new List<string>();
            this.SIHCodes = new List<string>();
        }
        /// <summary>
        /// 包装任务单id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// 包装任务单单号
        /// </summary>
        public string Code {
            get;
            set;
        }

        /// <summary>
        /// 备件id
        /// </summary>
        public int? SparePartId {
            get;
            set;
        }

        /// <summary>
        ///     备件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        ///     备件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        /// 计划量id
        /// </summary>
        public int PlanQty {
            get;
            set;
        }

        /// <summary>
        /// 已包装数量
        /// </summary>
        public int? PackingQty {
            get;
            set;
        }

        /// <summary>
        /// 本次包装量
        /// </summary>
        public int? ThisPackingQty {
            get;
            set;
        }

        /// <summary>
        ///     包装类型
        /// </summary>
        public int? PackingType {
            get;
            set;
        }

        /// <summary>
        ///     包装系数
        /// </summary>
        public int? PackingCoefficient {
            get;
            set;
        }

        /// <summary>
        ///     计量单位
        /// </summary>
        public string MeasureUnit {
            get;
            set;
        }

        /// <summary>
        /// 包材
        /// </summary>
        public string PackingMaterial {
            get;
            set;
        }

        /// <summary>
        /// 追溯属性
        /// </summary>
        public int? TraceProperty {
            get;
            set;
        }
        /// <summary>
        /// 追溯码
        /// </summary>
        public List<string> TraceCodes {
            get;
            set;
        }
        /// <summary>
        /// SIH标签码
        /// </summary>
        public List<string> SIHCodes {
            get;
            set;
        }
        /// <summary>
        /// 箱码
        /// </summary>
        public List<string> BoxCodes {
            get;
            set;
        }
    }
}
