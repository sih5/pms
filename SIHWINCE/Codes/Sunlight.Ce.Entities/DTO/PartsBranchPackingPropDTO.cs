﻿namespace Sunlight.Ce.Entities.DTO {
    public class PartsBranchPackingPropDTO {
        /// <summary>
        ///     包装类型
        /// </summary>
        public int? PackingType {
            get;
            set;
        }

        /// <summary>
        ///     包装系数
        /// </summary>
        public int? PackingCoefficient {
            get;
            set;
        }

        /// <summary>
        ///     计量单位
        /// </summary>
        public string MeasureUnit {
            get;
            set;
        }
    }
}
