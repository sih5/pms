﻿namespace Sunlight.Ce.Entities.DTO {
    public class PartsInboundPlanDetailDTO {
        /// <summary>
        ///     id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        ///     配件入库计划编号
        /// </summary>
        public string PartsInboundPlanCode {
            get;
            set;
        }

        /// <summary>
        ///     配件入库计划id
        /// </summary>
        public int PartsInboundPlanId {
            get;
            set;
        }

        /// <summary>
        ///     配件id
        /// </summary>
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        ///     配件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        ///     配件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        ///     供应商图号
        /// </summary>
        public string DefaultSupplier {
            get;
            set;
        }

        /// <summary>
        ///     计划量
        /// </summary>
        public int PlannedAmount {
            get;
            set;
        }

        /// <summary>
        ///     检验量
        /// </summary>
        public int? InspectedQuantity {
            get;
            set;
        }

        /// <summary>
        ///     待收数量
        /// </summary>
        public int? ReceivedQuantity {
            get;
            set;
        }

        /// <summary>
        ///     冗余字段
        /// </summary>
        public string Remark {
            get;
            set;
        }
    }
}
