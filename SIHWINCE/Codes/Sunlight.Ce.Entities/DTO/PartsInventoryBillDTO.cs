﻿using System.Collections.Generic;
namespace Sunlight.Ce.Entities.DTO {
    public class PartsInventoryBillDTO {
        public PartsInventoryBillDTO() {
            SIHCodes = new List<string>();
        }
        /// <summary>
        /// 盘点单Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// 盘点单编号
        /// </summary>
        public string Code {
            get;
            set;
        }

        /// <summary>
        /// 发起人
        /// </summary>
        public string InitiatorName {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }

        /// <summary>
        /// 盘点单Id
        /// </summary>
        public int DetailId {
            get;
            set;
        }
        /// <summary>
        /// 备件id
        /// </summary>
        public int? SparePartId {
            get;
            set;
        }

        /// <summary>
        ///     备件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        ///     备件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }
        /// <summary>
        /// 库位Id
        /// </summary>
        public int? WarehouseAreaId {
            get;
            set;
        }
        /// <summary>
        ///  库位编号
        /// </summary>
        public string WarehouseAreaCode {
            get;
            set;
        }
        /// <summary>
        /// 当前库存
        /// </summary>
        public int CurrentStorage {
            get;
            set;
        }
        /// <summary>
        /// 盘点后库存
        /// </summary>
        public int? StorageAfterInventory {
            get;
            set;
        }

        /// <summary>
        /// 已盘点后库存
        /// </summary>
        public int? AlreadyStorageAfterInventory {
            get;
            set;
        }

        /// <summary>
        /// 库存差异
        /// </summary>
        public int? StorageDifference {
            get;
            set;
        }

        /// <summary>
        /// 追溯属性
        /// </summary>
        public int? TraceProperty {
            get;
            set;
        }

        /// <summary>
        /// 标签码
        /// </summary>
        public List<string> SIHCodes {
            get;
            set;
        }
    }
}
