﻿namespace Sunlight.Ce.Entities.DTO {
    public class PartsShelvesDTO {
        /// <summary>
        /// 上架任务单id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 上架任务单编号
        /// </summary>
        public string Code {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        /// 计划量
        /// </summary>
        public int PlannedAmount {
            get;
            set;
        }

        /// <summary>
        /// 已上架量
        /// </summary>
        public int ShelvesAmount {
            get;
            set;
        }

        /// <summary>
        /// 本次上架量
        /// </summary>
        public int ThisShelvesAmount {
            get;
            set;
        }

        /// <summary>
        /// 原单据编号
        /// </summary>
        public string SourceBillCode {
            get;
            set;
        }

        /// <summary>
        /// 推荐货位Id
        /// </summary>
        public int? WarehouseAreaId {
            get;
            set;
        }
        /// <summary>
        /// 推荐货位编号
        /// </summary>
        public string WarehouseAreaCode {
            get;
            set;
        }

        /// <summary>
        /// 目标货位Id
        /// </summary>
        public int? TargetWarehouseAreaId {
            get;
            set;
        }
        /// <summary>
        /// 目标货位编号
        /// </summary>
        public string TargetWarehouseAreaCode {
            get;
            set;
        }

        /// <summary>
        /// SIH标签码
        /// </summary>
        public string SIHCodes {
            get;
            set;
        }

        /// <summary>
        /// 追溯属性
        /// </summary>
        public int? TraceProperty {
            get;
            set;
        }
    }
}
