﻿using System.Collections.Generic;
namespace Sunlight.Ce.Entities.DTO {
    public class PartsShiftOrderDTO {

        public PartsShiftOrderDTO() {
            this.PartsShiftSIHDetails = new List<PartsShiftSIHDetail>();            
        }


        /// <summary>
        /// 配件移库单id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 配件移库单编号
        /// </summary>
        public string Code {
            get;
            set;
        }

        /// <summary>
        /// 移库状态
        /// </summary>
        public int ShiftStatus {
            get;
            set;
        }


        /// <summary>
        /// 配件Id
        /// </summary>
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        /// 源库位Id
        /// </summary>
        public int OriginalWarehouseAreaId {
            get;
            set;
        }

        /// <summary>
        /// 源库位编号
        /// </summary>
        public string OriginalWarehouseAreaCode {
            get;
            set;
        }

        /// <summary>
        /// 目标库位Id
        /// </summary>
        public int DestWarehouseAreaId {
            get;
            set;
        }

        /// <summary>
        /// 目标库位编号
        /// </summary>
        public string DestWarehouseAreaCode {
            get;
            set;
        }        

        /// <summary>
        /// 数量
        /// </summary>
        public int Quantity {
            get;
            set;
        }

        /// <summary>
        /// 上架数量
        /// </summary>
        public int? UpShelfQty {
            get;
            set;
        }

        /// <summary>
        /// 下架数量
        /// </summary>
        public int? DownShelfQty {
            get;
            set;
        }

        /// <summary>
        /// 本次上架数量
        /// </summary>
        public int? ThisUpShelfQty {
            get;
            set;
        }

        /// <summary>
        /// 本次下架数量
        /// </summary>
        public int? ThisDownShelfQty {
            get;
            set;
        }        

        /// <summary>
        /// 追溯属性
        /// </summary>
        public int? TraceProperty {
            get;
            set;
        }

        public List<PartsShiftSIHDetail> PartsShiftSIHDetails {
            get;
            set;
        }

    }

    public class PartsShiftSIHDetail {
        /// <summary>
        /// SIH标签码
        /// </summary>
        public string SIHCode {
            get;
            set;
        }

        /// <summary>
        /// 数量
        /// </summary>
        public int? Quantity {
            get;
            set;
        }

    }
}
