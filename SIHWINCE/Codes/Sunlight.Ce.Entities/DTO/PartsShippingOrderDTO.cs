﻿namespace Sunlight.Ce.Entities.DTO {
    public class PartsShippingOrderDTO {
        /// <summary>
        /// 单号
        /// </summary>
        public string Code {
            get;
            set;
        }

        /// <summary>
        /// id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        ///     状态
        /// </summary>
        public int Status {
            get;
            set;
        }

        /// <summary>
        /// 清单Id
        /// </summary>
        public int DetailId {
            get;
            set;
        }

        /// <summary>
        /// 备件id
        /// </summary>
        public int PartsId {
            get;
            set;
        }

        /// <summary>
        ///     备件编号
        /// </summary>
        public string PartsCode {
            get;
            set;
        }

        /// <summary>
        ///     备件名称
        /// </summary>
        public string PartsName {
            get;
            set;
        }

        /// <summary>
        /// 货箱号
        /// </summary>
        public string ContainerNumber {
            get;
            set;
        }

        /// <summary>
        /// 发运量
        /// </summary>
        public int ShippingAmount {
            get;
            set;
        }

        /// <summary>
        /// 确认量
        /// </summary>
        public int ConfirmedAmount {
            get;
            set;
        }

        /// <summary> 
        /// 出库计划单ID 
        /// </summary> 
        public int PartsOutboundPlanId {
            get;
            set;
        }
    }
}
