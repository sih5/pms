﻿namespace Sunlight.Ce.Entities.DTO {
    public class PickingIsUrgentDTO {
        /// <summary>
        ///     拣货任务Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        ///     拣货任务单号
        /// </summary>
        public string Code {
            get;
            set;
        }

        /// <summary>
        /// 是否紧急
        /// </summary>
        public string isUrgent {
            get;
            set;
        }
    }
}
