﻿using System.Collections.Generic;
namespace Sunlight.Ce.Entities.DTO {
    public class PickingTaskDTO {
        /// <summary>
        /// 拣货任务单id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 拣货任务单编号
        /// </summary>
        public string Code {
            get;
            set;
        }

        /// <summary>
        /// 对方单位Id
        /// </summary>
        public int CounterpartCompanyId {
            get;
            set;
        }

        /// <summary>
        /// 对方单位名称
        /// </summary>
        public string CounterpartCompanyName {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        /// 货位Id
        /// </summary>
        public int? WarehouseAreaId {
            get;
            set;
        }
        /// <summary>
        /// 货位编号
        /// </summary>
        public string WarehouseAreaCode {
            get;
            set;
        }

        /// <summary>
        /// 计划量
        /// </summary>
        public int PlanQty {
            get;
            set;
        }

        /// <summary>
        /// 已拣货量
        /// </summary>
        public int? PickingQty {
            get;
            set;
        }

        /// <summary>
        /// 本次拣货量
        /// </summary>
        public int? ThisPickingQty {
            get;
            set;
        }

        /// <summary>
        /// 出库计划单ID
        /// </summary>
        public int PartsOutboundPlanId {
            get;
            set;
        }

        /// <summary>
        /// 批次号
        /// </summary>
        public string BatchNumber {
            get;
            set;
        }

        /// <summary>
        /// 追溯属性
        /// </summary>
        public int? TraceProperty {
            get;
            set;
        }

        /// <summary>
        /// 标签码
        /// </summary>
        public List<string> SIHCodes {
            get;
            set;
        }
    }
}
