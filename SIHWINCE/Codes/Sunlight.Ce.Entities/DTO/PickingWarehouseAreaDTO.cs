﻿namespace Sunlight.Ce.Entities.DTO {
    public class PickingWarehouseAreaDTO {
        /// <summary>
        /// 货位Id
        /// </summary>
        public int? WarehouseAreaId {
            get;
            set;
        }
        /// <summary>
        /// 货位编号
        /// </summary>
        public string WarehouseAreaCode {
            get;
            set;
        }
        /// <summary>
        /// 任务个数
        /// </summary>
        public int TaskNumber {
            get;
            set;
        }
    }
}
