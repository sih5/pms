﻿namespace Sunlight.Ce.Entities.DTO {
    public class SparePartDTO {
        /// <summary>
        ///     备件Id
        /// </summary>
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        ///     备件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        ///     备件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        ///     显示类型
        /// </summary>
        public string TypeName {
            get;
            set;
        }

        /// <summary>
        ///     裸件长（cm）
        /// </summary>
        public double? Length {
            get;
            set;
        }

        /// <summary>
        ///     裸件宽(cm)
        /// </summary>
        public double? Width {
            get;
            set;
        }

        /// <summary>
        ///     裸件高(cm)
        /// </summary>
        public double? Height {
            get;
            set;
        }

        /// <summary>
        ///     裸件重量(kg)
        /// </summary>
        public double? Weight {
            get;
            set;
        }

        /// <summary>
        ///     毛件长（cm）
        /// </summary>
        public double? GrossLength {
            get;
            set;
        }

        /// <summary>
        ///     毛件宽(cm)
        /// </summary>
        public double? GrossWidth {
            get;
            set;
        }

        /// <summary>
        ///     毛件高(cm)
        /// </summary>
        public double? GrossHeight {
            get;
            set;
        }

        /// <summary>
        ///     毛件重量（Kg）
        /// </summary>
        public double? MinPackingWeight {
            get;
            set;
        }

        /// <summary>
        ///     包装工艺
        /// </summary>
        public int PackageType {
            get;
            set;
        }

        /// <summary>
        ///     最小包装数量
        /// </summary>
        public int MinPackingAmount {
            get;
            set;
        }
    }

    public class SparePartExtend1 {
        /// <summary>
        ///     备件Id
        /// </summary>
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        ///     备件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        ///     备件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }
        /// <summary>
        ///     英文名
        /// </summary>
        public string Englishname {
            get;
            set;
        }
        /// <summary>
        ///     最小包装数量
        /// </summary>
        public int MinPackingAmount {
            get;
            set;
        }
    }
}
