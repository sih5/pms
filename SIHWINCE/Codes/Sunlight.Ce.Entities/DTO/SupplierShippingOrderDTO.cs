﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Sunlight.Ce.Entities.DTO {
    public class SupplierShippingOrderDTO {
        public string Code { get; set; }
        public DateTime Date { get; set; }
    }
}
