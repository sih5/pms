﻿using System.Collections.Generic;

namespace Sunlight.Ce.Entities.Menus {
    public class AppMenusModel {
        private AuthorizeUser authorizeUser = new AuthorizeUser();
        private List<SystemModel> systemModels = new List<SystemModel>();

        public AuthorizeUser AuthorizeUser {
            get {
                return this.authorizeUser;
            }
            set {
                this.authorizeUser = value;
            }
        }

        public List<SystemModel> SystemModels {
            get {
                return this.systemModels;
            }
            set {
                this.systemModels = value;
            }
        }
    }
}
