﻿using System.Xml.Serialization;

namespace Sunlight.Ce.Entities.Menus {
    /// <summary>
    ///     认证用户的信息存储
    /// </summary>
    public class AuthorizeUser {
        /// <summary>
        ///     用户Id
        /// </summary>
        [XmlAttribute(AttributeName = "UserId")]
        public int UserId {
            get;
            set;
        }

        /// <summary>
        ///     用户名
        /// </summary>
        [XmlAttribute(AttributeName = "UserName")]
        public string UserName {
            get;
            set;
        }

        /// <summary>
        ///     用户编号
        /// </summary>
        [XmlAttribute(AttributeName = "UserCode")]
        public string UserCode {
            get;
            set;
        }

        /// <summary>
        ///     企业Id
        /// </summary>
        [XmlAttribute(AttributeName = "EnterpriseId")]
        public int EnterpriseId {
            get;
            set;
        }

        /// <summary>
        ///     企业编号
        /// </summary>
        [XmlAttribute(AttributeName = "EnterpriseCode")]
        public string EnterpriseCode {
            get;
            set;
        }

        /// <summary>
        ///     企业名称
        /// </summary>
        [XmlAttribute(AttributeName = "EnterpriseName")]
        public string EnterpriseName {
            get;
            set;
        }

        /// <summary>
        ///     企业类型
        /// </summary>
        [XmlAttribute(AttributeName = "EnterpriseType")]
        public int EnterpriseType {
            get;
            set;
        }
    }
}
