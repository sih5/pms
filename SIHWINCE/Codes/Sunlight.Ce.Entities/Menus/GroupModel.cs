﻿using System.Collections.Generic;

namespace Sunlight.Ce.Entities.Menus {
    public class GroupModel {
        private List<PageModel> pageModels = new List<PageModel>();

        public int Id {
            get;
            set;
        }

        public string PageId {
            get;
            set;
        }

        public string Name {
            get;
            set;
        }

        public List<PageModel> PageModels {
            get {
                return this.pageModels;
            }
            set {
                this.pageModels = value;
            }
        }
    }
}
