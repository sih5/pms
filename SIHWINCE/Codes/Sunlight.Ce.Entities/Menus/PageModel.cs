﻿namespace Sunlight.Ce.Entities.Menus {
    public class PageModel {
        public int Id {
            get;
            set;
        }

        public string PageId {
            get;
            set;
        }

        public string Name {
            get;
            set;
        }
    }
}
