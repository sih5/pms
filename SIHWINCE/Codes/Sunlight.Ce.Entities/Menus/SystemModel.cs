﻿using System.Collections.Generic;

namespace Sunlight.Ce.Entities.Menus {
    public class SystemModel {
        private List<GroupModel> groupModels = new List<GroupModel>();

        public int Id {
            get;
            set;
        }

        public string PageId {
            get;
            set;
        }

        public string Name {
            get;
            set;
        }

        public List<GroupModel> GroupModels {
            get {
                return this.groupModels;
            }
            set {
                this.groupModels = value;
            }
        }
    }
}
