﻿namespace Sunlight.Ce.Entities {
    public class WarehouseOperator {
        public int Id {
            get;
            set;
        }

        public int WarehouseId {
            get;
            set;
        }

        public string WarehouseCode {
            get;
            set;
        }

        public string WarehouseName {
            get;
            set;
        }

        public string Abbreviation {
            get;
            set;
        }
    }
}
