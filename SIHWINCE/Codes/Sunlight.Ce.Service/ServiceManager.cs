﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using Newtonsoft.Json;
using Sunlight.Ce.Entities;
using Sunlight.Ce.Entities.DTO;
using Sunlight.Ce.Entities.Menus;
using Sunlight.Ce.Utils;
using Sunlight.Ce.Utils.Extends;

namespace Sunlight.Ce.Service {
    public static class ServiceManager {
        //登录成功后，该变量赋值
        private static AuthorizeUser userInfo;
        private static AppMenusModel appMenusModel;
        private static WarehouseOperator currentWarehouse;
        //private static PdaPrinter printer;

        /// <summary>
        ///     当前登录用户的信息缓存
        /// </summary>
        /// <returns>当前用户信息</returns>
        public static AuthorizeUser GetCurrentUser() {
            if (userInfo == null)
                throw new Exception("未获取到登录信息，请联系管理员");
            return userInfo;
        }

        /// <summary>
        ///     获取当前系统的权限菜单
        /// </summary>
        /// <returns>当前用户的权限菜单</returns>
        public static AppMenusModel GetCurrentAppMenus() {
            if (appMenusModel == null)
                throw new Exception("未获取到任何菜单，请联系管理员");
            return appMenusModel;
        }

        /// <summary>
        ///     获取当前系统要操作的仓库
        /// </summary>
        /// <returns>仓库</returns>
        public static WarehouseOperator GetCurrentWarehouse() {
            if (currentWarehouse == null)
                throw new Exception("未获取到任何仓库，请联系管理员");
            return currentWarehouse;
        }

        /// <summary>
        /// 判断是否已经选择仓库
        /// </summary>
        /// <returns>是否已经选择仓库</returns>
        public static bool IsExistSelectedWarehouse() {
            if (currentWarehouse == null)
                return false;
            return true;
        }

        /// <summary>
        ///     初始化全局仓库
        /// </summary>
        /// <param name="warehouse">当前选择的仓库</param>
        public static void InitializeWarehouse(WarehouseOperator warehouse) {
            currentWarehouse = warehouse;
        }

        /// <summary>
        ///     退出时执行
        /// </summary>
        public static void SingOut() {
            userInfo = null;
            FormsAuth.Cookie = string.Empty;
        }

        /// <summary>
        ///     同步获取服务器信息
        /// </summary>
        /// <typeparam name="T">反序列类型</typeparam>
        /// <param name="urlRoute">API地址</param>
        /// <param name="para">参数</param>
        /// <returns>服务端返回的内容反序列后结果</returns>
        private static MessageInfo<List<KeyValueItem>> ConvertMessageInfo<T>(string urlRoute, Object para) {
            var msgstringInfo = new MessageInfo<List<KeyValueItem>>();
            try {
                string hostUrl = HelperUtil.CombineUrl("ServiceAddress", urlRoute);
                string jsonPara = JsonConvert.SerializeObject(para);
                string result = HelperUtil.Instance.Post(hostUrl, jsonPara);
                msgstringInfo = JsonConvert.DeserializeObject<MessageInfo<List<KeyValueItem>>>(result);
                return msgstringInfo;
            }
            catch (Exception ex) {
                msgstringInfo.IsSuccess = false;
                msgstringInfo.Message = ex.Message;
                msgstringInfo.Data = new List<KeyValueItem>();
                return msgstringInfo;
            }
        }

        /// <summary>
        ///     登录方法
        /// </summary>
        /// <param name="enterpriseCodePara">企业编号</param>
        /// <param name="userCodePara">用户编号</param>
        /// <param name="userPwdPara">用户密码</param>
        public static MessageInfo<AppMenusModel> Login(string enterpriseCodePara, string userCodePara, string userPwdPara) {
            string hostUrl = HelperUtil.CombineUrl("ServiceAddress", Constants.Login);
            string jsonPara = JsonConvert.SerializeObject(new {
                EnterpriseCode = enterpriseCodePara,
                LoginId = userCodePara,
                Password = userPwdPara,
            });
            string result = HelperUtil.Instance.Login(hostUrl, jsonPara);
            var msgInfo = JsonConvert.DeserializeObject<MessageInfo<AppMenusModel>>(result);

            if (msgInfo.IsSuccess && msgInfo.Data != null) {
                userInfo = msgInfo.Data.AuthorizeUser;
                appMenusModel = msgInfo.Data;
            }

            return msgInfo;
        }

        /// <summary>
        ///     获取字典项
        /// </summary>
        /// <param name="names">字典Names集合 如["Area_Kind","BaseData_Status"]</param>
        public static MessageInfo<List<KeyValueItem>> LoadKeyValueItemsBy(string[] names) {
            //return ConvertMessageInfo<List<KeyValueItem>>(Constants.LoadKeyValueItemsBy, new[] { "Area_Kind", "BaseData_Status" });
            return ConvertMessageInfo<List<KeyValueItem>>(Constants.LoadKeyValueItemsBy, names);
        }

        /// <summary>
        ///     获取当前人员具有权限的仓库
        /// </summary>
        /// <returns>当前人员对应的仓库列表</returns>
        public static MessageInfo<List<WarehouseOperator>> GetWarehouseOperators() {
            var msgWarehouseOperators = new MessageInfo<List<WarehouseOperator>>();
            try {
                string hostUrl = HelperUtil.CombineUrl("ServiceAddress", Constants.GetWarehouseOperators);

                string result = HelperUtil.Instance.Get(hostUrl, "");
                msgWarehouseOperators = JsonConvert.DeserializeObject<MessageInfo<List<WarehouseOperator>>>(result);
                return msgWarehouseOperators;
            }
            catch (Exception ex) {
                msgWarehouseOperators.IsSuccess = false;
                msgWarehouseOperators.Message = ex.Message;
                msgWarehouseOperators.Data = null;
                return msgWarehouseOperators;
            }
        }

        /// <summary>
        ///     删除指定路径的指定文件
        /// </summary>
        /// <returns></returns>
        public static bool DeletePic(string filename) {
            try {
                string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
                string combinePath = string.Format(path + "\\Config\\{0}", filename);
                return HelperUtil.RemoveFile(combinePath);
            }
            catch (Exception) {
                return false;
            }
        }

        /// <summary>
        /// 根据配件编号或者货位编号查询库存
        /// </summary>
        /// <param name="partCodePara">配件编号</param>
        /// <param name="locationCode">库位编号</param>
        /// <param name="isTrue">数量是否大于0</param>
        /// <returns></returns>
        public static MessageInfo<List<PartsStock>> GetBasicLocation(string partCodePara, string locationCode, bool isTrue) {
            var receivings = new MessageInfo<List<PartsStock>>();
            try {
                string hostUrl = HelperUtil.CombineUrl("ServiceAddress", Constants.GetPartsStockBy);
                string result = HelperUtil.Instance.Get(hostUrl, string.Format("sparePartCode={0}&locationCode={2}&isTrue={3}&warehouseId={1}", partCodePara, currentWarehouse.WarehouseId, locationCode, isTrue));
                receivings = JsonConvert.DeserializeObject<MessageInfo<List<PartsStock>>>(result);
                return receivings;
            }
            catch (Exception ex) {
                receivings.IsSuccess = false;
                receivings.Message = ex.Message;
                receivings.Data = null;
                return receivings;
            }
        }

        #region 收货业务
        /// <summary>
        /// 查询未收货的收货单
        /// </summary>
        /// <param name="code">入库计划单号</param>
        /// <returns></returns>
        public static MessageInfo<List<ForReceivingOrderDTO>> GetReceiptDetail(string code) {
            var receivings = new MessageInfo<List<ForReceivingOrderDTO>>();
            try {
                string hostUrl = HelperUtil.CombineUrl("ServiceAddress", Constants.GetReceiptDetail);
                string result = HelperUtil.Instance.Get(hostUrl, string.Format("code={0}&warehouseId={1}", code, currentWarehouse.WarehouseId));
                receivings = JsonConvert.DeserializeObject<MessageInfo<List<ForReceivingOrderDTO>>>(result);
                return receivings;
            }
            catch (Exception ex) {
                receivings.IsSuccess = false;
                receivings.Message = ex.Message;
                receivings.Data = null;
                return receivings;
            }
        }

        /// <summary>
        /// 收货确认
        /// </summary>
        /// <param name="receivingOrders"></param>
        /// <returns></returns>
        public static MessageInfo<bool> ConfirmationReceipts(List<ForReceivingOrderDTO> receivingOrders) {
            var receivings = new MessageInfo<bool>();
            try {
                string hostUrl = HelperUtil.CombineUrl("ServiceAddress", Constants.ConfirmReceive);
                string result = HelperUtil.Instance.Post(hostUrl, JsonConvert.SerializeObject(receivingOrders));
                receivings = JsonConvert.DeserializeObject<MessageInfo<bool>>(result);
                return receivings;
            }
            catch (Exception ex) {
                receivings.IsSuccess = false;
                receivings.Message = ex.Message;
                receivings.Data = false;
                return receivings;
            }
        }

        /// <summary>
        /// 根据编号查询待收货确认发运单
        /// </summary>
        /// <param name="code">发运单单号</param>
        /// <returns></returns>
        public static MessageInfo<List<PartsShippingOrderDTO>> GetShippingDetial(string code) {
            var receivings = new MessageInfo<List<PartsShippingOrderDTO>>();
            try {
                string hostUrl = HelperUtil.CombineUrl("ServiceAddress", Constants.GetShippingDetial);
                string result = HelperUtil.Instance.Get(hostUrl, string.Format("code={0}&warehouseId={1}", code, currentWarehouse.WarehouseId));
                receivings = JsonConvert.DeserializeObject<MessageInfo<List<PartsShippingOrderDTO>>>(result);
                return receivings;
            }
            catch (Exception ex) {
                receivings.IsSuccess = false;
                receivings.Message = ex.Message;
                receivings.Data = null;
                return receivings;
            }
        }

        /// <summary>
        /// 中心库收货确认
        /// </summary>
        /// <param name="partsShippingOrders"></param>
        /// <returns></returns>
        public static MessageInfo<bool> CenterConfirmReceive(List<PartsShippingOrderDTO> partsShippingOrders) {
            var receivings = new MessageInfo<bool>();
            try {
                string hostUrl = HelperUtil.CombineUrl("ServiceAddress", Constants.CenterConfirmReceive);
                string result = HelperUtil.Instance.Post(hostUrl, JsonConvert.SerializeObject(partsShippingOrders));
                receivings = JsonConvert.DeserializeObject<MessageInfo<bool>>(result);
                return receivings;
            }
            catch (Exception ex) {
                receivings.IsSuccess = false;
                receivings.Message = ex.Message;
                receivings.Data = false;
                return receivings;
            }
        }
        #endregion

        #region 包装业务
        /// <summary>
        /// 查询待包装任务单
        /// </summary>
        /// <param name="code">入库检验单编号</param>
        /// <param name="sparePartCode">配件编号</param>
        /// <returns></returns>
        public static MessageInfo<List<PackingTaskDTO>> GetPackingTask(string code, string sparePartCode) {
            var packingTasks = new MessageInfo<List<PackingTaskDTO>>();
            try {
                string hostUrl = HelperUtil.CombineUrl("ServiceAddress", Constants.GetPackingTask);
                string result = HelperUtil.Instance.Get(hostUrl, string.Format("code={0}&sparePartCode={2}&warehouseId={1}", code, currentWarehouse.WarehouseId, sparePartCode));
                packingTasks = JsonConvert.DeserializeObject<MessageInfo<List<PackingTaskDTO>>>(result);
                return packingTasks;
            }
            catch (Exception ex) {
                packingTasks.IsSuccess = false;
                packingTasks.Message = ex.Message;
                packingTasks.Data = null;
                return packingTasks;
            }
        }

        /// <summary>
        /// 包装登记
        /// </summary>
        /// <param name="packingTaskExtend"></param>
        /// <returns></returns>
        public static MessageInfo<bool> Packing(PackingTaskDTO packingTaskExtend) {
            var packingTasks = new MessageInfo<bool>();
            try {
                string hostUrl = HelperUtil.CombineUrl("ServiceAddress", Constants.Packing);
                string result = HelperUtil.Instance.Post(hostUrl, JsonConvert.SerializeObject(packingTaskExtend));
                packingTasks = JsonConvert.DeserializeObject<MessageInfo<bool>>(result);
                return packingTasks;
            }
            catch (Exception ex) {
                packingTasks.IsSuccess = false;
                packingTasks.Message = ex.Message;
                packingTasks.Data = false;
                return packingTasks;
            }
        }
        #endregion

        #region 上架业务
        /// <summary>
        /// 查询待上架任务单
        /// </summary>
        /// <param name="code">入库检验单编号</param>
        /// <param name="sparePartCode">配件编号</param>
        /// <returns></returns>
        public static MessageInfo<List<PartsShelvesDTO>> GetShelvesTask(string code, string sparePartCode) {
            var shelves = new MessageInfo<List<PartsShelvesDTO>>();
            try {
                string hostUrl = HelperUtil.CombineUrl("ServiceAddress", Constants.GetShelvesTask);
                string result = HelperUtil.Instance.Get(hostUrl, string.Format("code={0}&sparePartCode={2}&warehouseId={1}", code, currentWarehouse.WarehouseId, sparePartCode));
                shelves = JsonConvert.DeserializeObject<MessageInfo<List<PartsShelvesDTO>>>(result);
                return shelves;
            }
            catch (Exception ex) {
                shelves.IsSuccess = false;
                shelves.Message = ex.Message;
                shelves.Data = null;
                return shelves;
            }
        }

        /// <summary>
        /// 上架
        /// </summary>
        /// <param name="partsShelvesExtend"></param>
        /// <returns></returns>
        public static MessageInfo<bool> Shelvesing(PartsShelvesDTO partsShelvesExtend) {
            var shelves = new MessageInfo<bool>();
            try {
                string hostUrl = HelperUtil.CombineUrl("ServiceAddress", Constants.Shelvesing);
                string result = HelperUtil.Instance.Post(hostUrl, JsonConvert.SerializeObject(partsShelvesExtend));
                shelves = JsonConvert.DeserializeObject<MessageInfo<bool>>(result);
                return shelves;
            }
            catch (Exception ex) {
                shelves.IsSuccess = false;
                shelves.Message = ex.Message;
                shelves.Data = false;
                return shelves;
            }
        }
        #endregion

        #region 拣货业务
        /// <summary>
        /// 查询拣货任务库区列表
        /// </summary>
        /// <returns></returns>
        public static MessageInfo<List<PickingWarehouseAreaDTO>> GetPickingAreas() {
            var picking = new MessageInfo<List<PickingWarehouseAreaDTO>>();
            try {
                string hostUrl = HelperUtil.CombineUrl("ServiceAddress", Constants.GetPickingAreas);
                string result = HelperUtil.Instance.Get(hostUrl, string.Format("warehouseId={0}", currentWarehouse.WarehouseId));
                picking = JsonConvert.DeserializeObject<MessageInfo<List<PickingWarehouseAreaDTO>>>(result);
                return picking;
            }
            catch (Exception ex) {
                picking.IsSuccess = false;
                picking.Message = ex.Message;
                picking.Data = null;
                return picking;
            }
        }

        /// <summary>
        /// 根据货位查询待拣货任务
        /// </summary>
        /// <returns></returns>
        public static MessageInfo<List<PickingIsUrgentDTO>> GetPickings(int warehouseAreaId) {
            var picking = new MessageInfo<List<PickingIsUrgentDTO>>();
            try {
                string hostUrl = HelperUtil.CombineUrl("ServiceAddress", Constants.GetPickings);
                string result = HelperUtil.Instance.Get(hostUrl, string.Format("warehouseAreaId={1}&warehouseId={0}", currentWarehouse.WarehouseId, warehouseAreaId));
                picking = JsonConvert.DeserializeObject<MessageInfo<List<PickingIsUrgentDTO>>>(result);
                return picking;
            }
            catch (Exception ex) {
                picking.IsSuccess = false;
                picking.Message = ex.Message;
                picking.Data = null;
                return picking;
            }
        }

        /// <summary>
        /// 根据货位查询待拣货任务
        /// </summary>
        /// <returns></returns>
        public static MessageInfo<List<PickingTaskDTO>> GetPickingDetails(string code, int warehouseAreaId) {
            var picking = new MessageInfo<List<PickingTaskDTO>>();
            try {
                string hostUrl = HelperUtil.CombineUrl("ServiceAddress", Constants.GetPickingDetails);
                string result = HelperUtil.Instance.Get(hostUrl, string.Format("code={2}&warehouseAreaId={1}&warehouseId={0}", currentWarehouse.WarehouseId, warehouseAreaId, code));
                picking = JsonConvert.DeserializeObject<MessageInfo<List<PickingTaskDTO>>>(result);
                return picking;
            }
            catch (Exception ex) {
                picking.IsSuccess = false;
                picking.Message = ex.Message;
                picking.Data = null;
                return picking;
            }
        }

        /// <summary>
        /// 拣货
        /// </summary>
        /// <param name="pickingTaskExtends"></param>
        /// <returns></returns>
        public static MessageInfo<bool> Picking(List<PickingTaskDTO> pickingTaskExtends) {
            var picking = new MessageInfo<bool>();
            try {
                string hostUrl = HelperUtil.CombineUrl("ServiceAddress", Constants.Picking);
                string result = HelperUtil.Instance.Post(hostUrl, JsonConvert.SerializeObject(pickingTaskExtends));
                picking = JsonConvert.DeserializeObject<MessageInfo<bool>>(result);
                return picking;
            }
            catch (Exception ex) {
                picking.IsSuccess = false;
                picking.Message = ex.Message;
                picking.Data = false;
                return picking;
            }
        }

        /// <summary>
        /// 完成拣货
        /// </summary>
        /// <param name="pickingTaskExtends"></param>
        /// <returns></returns>
        public static MessageInfo<bool> PickingOver(List<PickingTaskDTO> pickingTaskExtends) {
            var picking = new MessageInfo<bool>();
            try {
                string hostUrl = HelperUtil.CombineUrl("ServiceAddress", Constants.PickingOver);
                string result = HelperUtil.Instance.Post(hostUrl, JsonConvert.SerializeObject(pickingTaskExtends));
                picking = JsonConvert.DeserializeObject<MessageInfo<bool>>(result);
                return picking;
            }
            catch (Exception ex) {
                picking.IsSuccess = false;
                picking.Message = ex.Message;
                picking.Data = false;
                return picking;
            }
        }
        #endregion

        #region 装箱业务
        /// <summary>
        /// 查询拣货任务库区列表
        /// </summary>
        /// <returns></returns>
        public static MessageInfo<List<BoxUpTaskDTO>> GetBoxUpCodes(string code) {
            var boxUp = new MessageInfo<List<BoxUpTaskDTO>>();
            try {
                string hostUrl = HelperUtil.CombineUrl("ServiceAddress", Constants.GetBoxUpCodes);
                string result = HelperUtil.Instance.Get(hostUrl, string.Format("code={1}&warehouseId={0}", currentWarehouse.WarehouseId, code));
                boxUp = JsonConvert.DeserializeObject<MessageInfo<List<BoxUpTaskDTO>>>(result);
                return boxUp;
            }
            catch (Exception ex) {
                boxUp.IsSuccess = false;
                boxUp.Message = ex.Message;
                boxUp.Data = null;
                return boxUp;
            }
        }
        /// <summary>
        /// 装箱
        /// </summary>
        /// <param name="boxUpTasks"></param>
        /// <returns></returns>
        public static MessageInfo<List<BoxUpTaskDTO>> BoxUping(List<BoxUpTaskDTO> boxUpTasks) {
            var picking = new MessageInfo<List<BoxUpTaskDTO>>();
            try {
                string hostUrl = HelperUtil.CombineUrl("ServiceAddress", Constants.BoxUping);
                string result = HelperUtil.Instance.Post(hostUrl, JsonConvert.SerializeObject(boxUpTasks));
                picking = JsonConvert.DeserializeObject<MessageInfo<List<BoxUpTaskDTO>>>(result);
                return picking;
            }
            catch (Exception ex) {
                picking.IsSuccess = false;
                picking.Message = ex.Message;
                picking.Data = null;
                return picking;
            }
        }

        /// <summary>
        /// 完成装箱
        /// </summary>
        /// <param name="boxUpTasks"></param>
        /// <returns></returns>
        public static MessageInfo<bool> BoxUpingOver(List<BoxUpTaskDTO> boxUpTasks) {
            var picking = new MessageInfo<bool>();
            try {
                string hostUrl = HelperUtil.CombineUrl("ServiceAddress", Constants.BoxUpingOver);
                string result = HelperUtil.Instance.Post(hostUrl, JsonConvert.SerializeObject(boxUpTasks));
                picking = JsonConvert.DeserializeObject<MessageInfo<bool>>(result);
                return picking;
            }
            catch (Exception ex) {
                picking.IsSuccess = false;
                picking.Message = ex.Message;
                picking.Data = false;
                return picking;
            }
        }
        #endregion

        #region 盘点业务
        /// <summary>
        /// 根据单号查询未完成的盘点单
        /// </summary>
        /// <param name="code">扫描盘点号</param>
        /// <returns></returns>
        public static MessageInfo<List<PartsInventoryBillDTO>> GetInventoryBills(string code) {
            var inventory = new MessageInfo<List<PartsInventoryBillDTO>>();
            try {
                string hostUrl = HelperUtil.CombineUrl("ServiceAddress", Constants.GetInventoryBills);
                string result = HelperUtil.Instance.Get(hostUrl, string.Format("code={1}&warehouseId={0}", currentWarehouse.WarehouseId, code));
                inventory = JsonConvert.DeserializeObject<MessageInfo<List<PartsInventoryBillDTO>>>(result);
                return inventory;
            }
            catch (Exception ex) {
                inventory.IsSuccess = false;
                inventory.Message = ex.Message;
                inventory.Data = null;
                return inventory;
            }
        }

        /// <summary>
        /// 盘点结果录入
        /// </summary>
        /// <param name="partsInventoryBills">盘点结果单据</param>
        /// <returns></returns>
        public static MessageInfo<bool> ResultsInput(List<PartsInventoryBillDTO> partsInventoryBills) {
            var picking = new MessageInfo<bool>();
            try {
                string hostUrl = HelperUtil.CombineUrl("ServiceAddress", Constants.ResultsInput);
                string result = HelperUtil.Instance.Post(hostUrl, JsonConvert.SerializeObject(partsInventoryBills));
                picking = JsonConvert.DeserializeObject<MessageInfo<bool>>(result);
                return picking;
            }
            catch (Exception ex) {
                picking.IsSuccess = false;
                picking.Message = ex.Message;
                picking.Data = false;
                return picking;
            }
        }
        #endregion

        public static MessageInfo<List<CenterPartsShelvesDTO>> GetCenterShelvesTask(string sparePartCode) {
            var inventory = new MessageInfo<List<CenterPartsShelvesDTO>>();
            try {
                string hostUrl = HelperUtil.CombineUrl("ServiceAddress", Constants.GetCenterShelvesTask);
                string result = HelperUtil.Instance.Get(hostUrl, string.Format("sparePartCode={1}&warehouseId={0}", currentWarehouse.WarehouseId, sparePartCode));
                inventory = JsonConvert.DeserializeObject<MessageInfo<List<CenterPartsShelvesDTO>>>(result);
                return inventory;
            }
            catch (Exception ex) {
                inventory.IsSuccess = false;
                inventory.Message = ex.Message;
                inventory.Data = null;
                return inventory;
            }
        }

        public static MessageInfo<bool> CenterShelvesing(List<CenterPartsShelvesDTO> centerPartsShelves) {
            var picking = new MessageInfo<bool>();
            try {
                string hostUrl = HelperUtil.CombineUrl("ServiceAddress", Constants.CenterShelvesing);
                string result = HelperUtil.Instance.Post(hostUrl, JsonConvert.SerializeObject(centerPartsShelves));
                picking = JsonConvert.DeserializeObject<MessageInfo<bool>>(result);
                return picking;
            }
            catch (Exception ex) {
                picking.IsSuccess = false;
                picking.Message = ex.Message;
                picking.Data = false;
                return picking;
            }
        }

        /// <summary>
        /// 供应商发运确认
        /// </summary>
        /// <param name="code"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public static MessageInfo<bool> ConfirmSupplierShipping(SupplierShippingOrderDTO data) {
            var supplierShipping = new MessageInfo<bool>();
            try {
                string hostUrl = HelperUtil.CombineUrl("ServiceAddress", Constants.ConfirmSupplierShipping);
                string result = HelperUtil.Instance.Post(hostUrl, JsonConvert.SerializeObject(data));
                supplierShipping = JsonConvert.DeserializeObject<MessageInfo<bool>>(result);
                return supplierShipping;
            }
            catch (Exception ex) {
                supplierShipping.IsSuccess = false;
                supplierShipping.Message = ex.Message;
                supplierShipping.Data = false;
                return supplierShipping;
            }
        }

        #region 移库业务
        /// <summary>
        /// 查询上架移库单
        /// </summary>
        /// <returns></returns>
        public static MessageInfo<List<PartsShiftOrderDTO>> GetUpPartsShift(string code) {
            var shiftOrder = new MessageInfo<List<PartsShiftOrderDTO>>();
            try {
                string hostUrl = HelperUtil.CombineUrl("ServiceAddress", Constants.GetUpPartsShift);
                string result = HelperUtil.Instance.Get(hostUrl, string.Format("code={1}&warehouseId={0}", currentWarehouse.WarehouseId, code));
                shiftOrder = JsonConvert.DeserializeObject<MessageInfo<List<PartsShiftOrderDTO>>>(result);
                return shiftOrder;
            }
            catch (Exception ex) {
                shiftOrder.IsSuccess = false;
                shiftOrder.Message = ex.Message;
                shiftOrder.Data = null;
                return shiftOrder;
            }
        }

        /// <summary>
        /// 查询下架移库单
        /// </summary>
        /// <returns></returns>
        public static MessageInfo<List<PartsShiftOrderDTO>> GetDownPartsShift(string code) {
            var shiftOrder = new MessageInfo<List<PartsShiftOrderDTO>>();
            try {
                string hostUrl = HelperUtil.CombineUrl("ServiceAddress", Constants.GetDownPartsShift);
                string result = HelperUtil.Instance.Get(hostUrl, string.Format("code={1}&warehouseId={0}", currentWarehouse.WarehouseId, code));
                shiftOrder = JsonConvert.DeserializeObject<MessageInfo<List<PartsShiftOrderDTO>>>(result);
                return shiftOrder;
            }
            catch (Exception ex) {
                shiftOrder.IsSuccess = false;
                shiftOrder.Message = ex.Message;
                shiftOrder.Data = null;
                return shiftOrder;
            }
        }

        /// <summary>
        /// 移库上架
        /// </summary>
        /// <param name="pickingTaskExtends"></param>
        /// <returns></returns>
        public static MessageInfo<bool> PartsShiftUping(List<PartsShiftOrderDTO> partsShift) {
            var shiftOrder = new MessageInfo<bool>();
            try {
                string hostUrl = HelperUtil.CombineUrl("ServiceAddress", Constants.PartsShiftUping);
                string result = HelperUtil.Instance.Post(hostUrl, JsonConvert.SerializeObject(partsShift));
                shiftOrder = JsonConvert.DeserializeObject<MessageInfo<bool>>(result);
                return shiftOrder;
            }
            catch (Exception ex) {
                shiftOrder.IsSuccess = false;
                shiftOrder.Message = ex.Message;
                shiftOrder.Data = false;
                return shiftOrder;
            }
        }

        /// <summary>
        /// 移库下架
        /// <returns></returns>
        public static MessageInfo<bool> PartsShiftDowning(List<PartsShiftOrderDTO> partsShift) {
            var shiftOrder = new MessageInfo<bool>();
            try {
                string hostUrl = HelperUtil.CombineUrl("ServiceAddress", Constants.PartsShiftDowning);
                string result = HelperUtil.Instance.Post(hostUrl, JsonConvert.SerializeObject(partsShift));
                shiftOrder = JsonConvert.DeserializeObject<MessageInfo<bool>>(result);
                return shiftOrder;
            }
            catch (Exception ex) {
                shiftOrder.IsSuccess = false;
                shiftOrder.Message = ex.Message;
                shiftOrder.Data = false;
                return shiftOrder;
            }
        }

        #endregion
    }
}
