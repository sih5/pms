﻿using System;
using System.Runtime.InteropServices;

namespace Sunlight.Ce.Update {
    [StructLayout(LayoutKind.Sequential)]
    public struct SystemTime {
        public short m_year;

        public short m_month;

        public short m_dayOfWeek;

        public short m_day;

        public short m_hour;

        public short m_minute;

        public short m_second;

        public short m_milliseconds;

        public SystemTime(DateTime value) {
            this.m_year = (Int16)value.Year;

            this.m_month = (Int16)value.Month;

            this.m_dayOfWeek = (Int16)value.DayOfWeek;

            this.m_day = (Int16)value.Day;

            this.m_hour = (Int16)value.Hour;

            this.m_minute = (Int16)value.Minute;

            this.m_second = (Int16)value.Second;

            this.m_milliseconds = (Int16)value.Millisecond;
        }
    }
}
