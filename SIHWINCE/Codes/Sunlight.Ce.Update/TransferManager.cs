﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace Sunlight.Ce.Update {
    public class TransferManager {
        public delegate void TransferProgress(int progress);

        private TransferProgress transferProgressDelegate;
        private volatile bool abortTransfer = false;

        public TransferManager() {
        }

        public bool DownloadFile(String url, out Stream s, String path, TransferProgress del) {
            this.transferProgressDelegate = del;
            var buffer = new byte[4096];
            var fileStream = new FileStream(path, FileMode.Create, FileAccess.ReadWrite);

            WebRequest wr = WebRequest.Create(url);
            wr.Proxy = GlobalProxySelection.Select;
            try {
                using (WebResponse response = wr.GetResponse()) {
                    using (Stream responseStream = response.GetResponseStream()) {
                        int count = 0;
                        int dataRead = 0;
                        do {
                            count = responseStream.Read(buffer, 0, buffer.Length);
                            fileStream.Write(buffer, 0, count);

                            float progress = ((float)dataRead / (float)response.ContentLength) * 100.0f;
                            this.OnProgress((int)progress);

                            dataRead += count;
                        } while (count != 0 && !this.abortTransfer);
                    }
                }
            }
            catch (Exception ex) {
                MessageBox.Show("无网络连接");
                fileStream.Close();
                s = null;
                return false;
            }
            s = fileStream;
            return true;
        }

        protected void OnProgress(int progress) {
            if (this.transferProgressDelegate != null)
                this.transferProgressDelegate(progress);
        }
    }
}
