﻿using System;
using System.Diagnostics;
using System.ComponentModel;
using System.Windows.Forms;

namespace Sunlight.Ce.Update {
    public partial class UpdateFrm : Form {
        private volatile bool updateStarted = false;
        private Updater updater;

        public UpdateFrm() {
            //updater = new Updater("http://54.222.142.97:8090/update.xml");
            updater = new Updater("http://HKS-WMS-1763942487.cn-north-1.elb.amazonaws.com.cn:83/update.xml");
            InitializeComponent();
        }

        private void btnUpdate_Click(object sender, EventArgs e) {
            try {
                Cursor.Current = Cursors.WaitCursor;
                var button = (Button)sender;
                button.Visible = false;
                button.Dock = DockStyle.None;
                updater.performUpdate(updater.updateFilePath);
                this.updateStarted = true;
                Cursor.Current = Cursors.Default;
                updater.cleanup(updater.updateFilePath);
                Process.Start(updater.appPath + "\\Sunlight.Ce.WinUI.exe", "");
                Process myproc = Process.GetCurrentProcess();
                myproc.Kill();
            } catch(Exception exception) {
                MessageBox.Show(exception.Message);
            }          
        }

        private void btnClose_Click(object sender, EventArgs e) {
            updater.cleanup(updater.updateFilePath);
            Process myproc = Process.GetCurrentProcess();
            myproc.Kill();
        }

        private void UpdateFrm_Closing(object sender, CancelEventArgs e) {
            updater.cleanup(updater.updateFilePath);
        }
    }
}