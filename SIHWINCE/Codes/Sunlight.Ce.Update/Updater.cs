﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Xml;
using Ionic.Zip;

namespace Sunlight.Ce.Update {
    public class Updater {
        [DllImport("coredll")]
        protected static extern bool CeRunAppAtTime(string pwszAppName, ref SystemTime lpTime);

        public delegate void UpdateDone();

        public event UpdateDone UpdateDoneEvent;

        private readonly String URL;
        public readonly String appPath;
        private string zipFileURL;
        private UpdateFrm update;
        public readonly Assembly callingAssembly;
        public readonly String updateFilePath;

        public Updater(String url) {
            this.URL = url;
            this.callingAssembly = Assembly.GetCallingAssembly();
            String fullAppName = this.callingAssembly.GetName().CodeBase;
            this.appPath = Path.GetDirectoryName(fullAppName);
            this.updateFilePath = Path.Combine(this.appPath, "update.xml");
        }

        public void CheckForNewVersion() {
            Stream s;
            var tm = new TransferManager();
            if (tm.DownloadFile(this.URL, out s, this.updateFilePath, null)) {
                s.Close();
            }
        }

        public void cleanup(string updateFilePathPara) {
            this.removeFile(updateFilePathPara);
        }

        public void performUpdate(string updateFile) {
            var xDoc = new XmlDocument();
            xDoc.Load(updateFile);
            XmlNodeList links = xDoc.GetElementsByTagName("link");
            var url = (string)links[0].InnerText;
            String fullAppName = this.callingAssembly.GetName().CodeBase;
            String appPath = Path.GetDirectoryName(fullAppName);
            String updateDir = appPath;
            String updateFilename = this.getFilename(url);
            string updateZip = updateDir + "\\" + updateFilename;
            Directory.CreateDirectory(updateDir);
            var tm = new TransferManager();

            Stream s;
            if (!tm.DownloadFile(url, out s, updateZip, null)) {
                cleanup(updateFilePath);
                File.Delete(updateZip);
                return; 
            }
            if (s != null)
                s.Close();
            using (ZipFile zip1 = ZipFile.Read(updateZip)) {
                foreach (ZipEntry e in zip1)
                    e.Extract(updateDir, ExtractExistingFileAction.OverwriteSilently);
            };
            File.Delete(updateZip);
            //foreach (string filepath in Directory.GetFiles(updateDir)) {
            //    string originalFile = appPath + "\\" + this.getFilenameFromPath(filepath);
            //    if (File.Exists(originalFile)) {
            //        File.Move(filepath, originalFile);
            //    }
            //    this.OnUpdateDone();
            //}
        }

        private string getFilename(String url) {
            char[] delim = {
                '/'
            };
            string[] a = url.Split(delim);
            return a[a.Length - 1];
        }

        private string getFilenameFromPath(String path) {
            char[] delim = {
                '\\'
            };
            string[] a = path.Split(delim);
            return a[a.Length - 1];
        }

        protected void OnUpdateDone() {
            if (this.UpdateDoneEvent != null)
                this.UpdateDoneEvent();
        }

        #region Remove File
        protected bool removeFile(string FileName) {
            bool Ret = false;
            try {
                if (File.Exists(FileName) == false)
                    return true;
                File.Delete(FileName);
                Ret = true;
            }
            catch (Exception) {
                throw;
            }
            return Ret;
        }
        #endregion
    }
}
