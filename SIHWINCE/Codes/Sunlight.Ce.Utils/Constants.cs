﻿namespace Sunlight.Ce.Utils {
    /// <summary>
    /// 常量Route定义
    /// </summary>
    public static class Constants {
        /// <summary>
        /// 登录
        /// </summary>
        public static string Login = "api/Account/Login";

        /// <summary>
        /// 查询配件
        /// </summary>
        public static string LoadSpareParts = "api/SparePart";

        /// <summary>
        /// 获取具体打印内容
        /// </summary>
        public static string LoadCaseNo = "logistics/partsbaleCase/getcaseno";

        /// <summary>
        /// 根据对象名称获取新流水编号
        /// </summary>
        public static string Genernate = "api/CodeGenerate/GetByEntityName";

        /// <summary>
        /// 根据编号获取箱号生成流水号转编号
        /// </summary>
        public static string GetByCode = "api/PartsBoxBill/GetByCode";

        /// <summary>
        /// 根据对象名称以及企业编号获取流水编号
        /// </summary>
        public static string GenernateByCorpCode = "api/CodeGenernate/GetByCorpCode";

        /// <summary>
        /// 获取字典项
        /// </summary>
        public static string LoadKeyValueItemsBy = "api/KeyValueItem/LoadByNames ";

        /// <summary>
        /// 获取当前人员具有权限的仓库列表
        /// </summary>
        public static string GetWarehouseOperators = "api/WarehouseOperator/GetWarehouseOperators";

        /// <summary>
        /// 查询未收货的收货单
        /// </summary>
        public static string GetPartsStockBy = "api/PartsStock/GetPartsStockBy";
        /// <summary>
        /// 查询未收货的收货单
        /// </summary>
        public static string GetReceiptDetail = "api/PartsInboundPlan/GetReceiptDetail";
        /// <summary>
        /// 收货确认
        /// </summary>
        public static string ConfirmReceive = "api/ReceiptConfirmation/ConfirmReceive";
        /// <summary>
        /// 查询待包装任务单
        /// </summary>
        public static string GetPackingTask = "api/PackingTask/GetPackingTask";
        /// <summary>
        /// 包装登记
        /// </summary>
        public static string Packing = "api/PackingTask/Packing";
        /// <summary>
        /// 查询待上架任务单
        /// </summary>
        public static string GetShelvesTask = "api/PartsShelves/GetShelvesTask";
        /// <summary>
        /// 上架
        /// </summary>
        public static string Shelvesing = "api/PartsShelves/Shelvesing";
        /// <summary>
        /// 查询拣货任务库区列表
        /// </summary>
        public static string GetPickingAreas = "api/PickingTask/GetPickingAreas";
        /// <summary>
        /// 根据货位查询待拣货任务
        /// </summary>
        public static string GetPickings = "api/PickingTask/GetPickings";
        /// <summary>
        /// 查询拣货任务单详情
        /// </summary>
        public static string GetPickingDetails = "api/PickingTask/GetPickingDetails";
        /// <summary>
        /// 拣货
        /// </summary>
        public static string Picking = "api/PickingTask/Picking";
        /// <summary>
        /// 完成拣货
        /// </summary>
        public static string PickingOver = "api/PickingTask/PickingOver";
        /// <summary>
        /// 根据单号查询待装箱任务单
        /// </summary>
        public static string GetBoxUpCodes = "api/BoxUpTask/GetBoxUpCodes";
        /// <summary>
        /// 装箱
        /// </summary>
        public static string BoxUping = "api/BoxUpTask/BoxUping";
        /// <summary>
        /// 装箱
        /// </summary>
        public static string BoxUpingOver = "api/BoxUpTask/BoxUpingOver";
        /// <summary>
        /// 根据单号查询未完成的盘点单
        /// </summary>
        public static string GetInventoryBills = "api/PartsInventoryBill/GetInventoryBills";
        /// <summary>
        /// 盘点结果录入
        /// </summary>
        public static string ResultsInput = "api/PartsInventoryBill/ResultsInput";
        /// <summary>
        /// 根据编号查询待收货确认发运单
        /// </summary>
        public static string GetShippingDetial = "api/ReceiptConfirmation/GetShippingDetial";
        /// <summary>
        /// 中心库收货确认
        /// </summary>
        public static string CenterConfirmReceive = "api/ReceiptConfirmation/CenterConfirmReceive";

        public static string GetCenterShelvesTask = "api/PartsShelves/GetCenterShelvesTask";

        public static string CenterShelvesing = "api/PartsShelves/CenterShelvesing";

        /// <summary>
        /// 供应商发运单确认
        /// </summary>
        public static string ConfirmSupplierShipping = "api/SupplierShippingOrder/Confirm";

        /// <summary>
        /// 查询上架移库单
        /// </summary>
        public static string GetUpPartsShift = "api/PartsShiftOrder/GetUpPartsShift";
        /// <summary>
        /// 查询下架移库单
        /// </summary>
        public static string GetDownPartsShift = "api/PartsShiftOrder/GetDownPartsShift";
        /// <summary>
        /// 移库上架
        /// </summary>
        public static string PartsShiftUping = "api/PartsShiftOrder/PartsShiftUping";
        /// <summary>
        /// 移库下架
        /// </summary>
        public static string PartsShiftDowning = "api/PartsShiftOrder/PartsShiftDowning";
    }
}
