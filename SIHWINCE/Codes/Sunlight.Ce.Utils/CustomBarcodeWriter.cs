﻿using System.Drawing;
using ZXing;
using ZXing.Rendering;

namespace Sunlight.Ce.Utils {
    public class CustomBarcodeWriter : BarcodeWriter {
        public CustomBarcodeWriter() {
            var render = new BitmapRenderer();
            render.Background = Color.Transparent;
            //render.Foreground = Color.Red;
            this.Renderer = render;
        }

        public CustomBarcodeWriter(Color backgroundColor, Color foreground) {
            var render = new BitmapRenderer();
            render.Background = backgroundColor;
            render.Foreground = foreground;
            Renderer = render;
        }
    }
}
