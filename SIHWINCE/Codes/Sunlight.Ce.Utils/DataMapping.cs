﻿using Newtonsoft.Json;

namespace Sunlight.Ce.Utils {
    public class DataMapping {
        /// <summary>
        /// Json反射为实体
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="jsonStr">Json字符串</param>
        /// <returns>返回对象</returns>
        public static T Deserialize<T>(string jsonStr) {
            var t = JsonConvert.DeserializeObject<T>(jsonStr);
            return t;
        }

        /// <summary>
        /// 对象转换为String字符串
        /// </summary>
        /// <typeparam name="T">对象类型</typeparam>
        /// <param name="instance">对象实例</param>
        /// <returns>返回Json字符串</returns>
        public static string Serialize<T>(T instance) {
            return JsonConvert.SerializeObject(instance);
        }
    }
}
