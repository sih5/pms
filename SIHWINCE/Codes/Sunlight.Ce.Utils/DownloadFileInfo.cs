using System.IO;

namespace Sunlight.Ce.Utils {
    public class DownloadFileInfo {
        readonly string downloadUrl = "";
        readonly string fileName = "";
        string lastver = "";
        readonly int size;

        /// <summary>
        /// 要从哪里下载文件
        /// </summary>
        public string DownloadUrl { get { return this.downloadUrl; } }
        /// <summary>
        /// 下载完成后要放到哪里去
        /// </summary>
        public string FileFullName { get { return this.fileName; } }
        public string FileName { get { return Path.GetFileName(this.FileFullName); } }
        public string LastVer { get { return this.lastver; } set { this.lastver = value; } }
        public int Size { get { return this.size; } }

        public DownloadFileInfo(string url, string name, string ver, int size) {
            this.downloadUrl = url;
            this.fileName = name;
            this.lastver = ver;
            this.size = size;
        }
    }
}