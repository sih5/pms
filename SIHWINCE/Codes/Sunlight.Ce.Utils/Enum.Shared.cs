﻿namespace Sunlight.Ce.Utils {
    /// <summary>
    /// 基本数据状态
    /// </summary>
    public enum BaseDataStatus {
        作废 = 0,
        有效 = 1
    }

    //装箱任务单状态
    public enum DcsBoxUpTaskStatus {
        新建 = 1,
        部分装箱 = 2,
        装箱完成 = 3,
        停用 = 4,
        终止 = 5,
        作废 = 99
    }

    public enum DcsPartsInboundType {
        配件采购 = 1,
        销售退货 = 2,
        配件调拨 = 3,
        内部领入 = 4,
        配件零售退货 = 5,
        积压件调剂 = 6,
        配件外采 = 7,
        质量件索赔 = 8,
        代发退货 = 9
    }

    //追溯属性
    public enum DCSTraceProperty {
        精确追溯 = 1,
        批次追溯 = 2
    }

    //配件盘点单状态
    public enum DcsPartsInventoryBillStatus {
        新建 = 1,
        已结果录入 = 2,
        已库存覆盖 = 3,
        已审批 = 4,
        作废 = 99
    }
}