﻿namespace Sunlight.Ce.Utils.Extends {
    public class CacheUserInfo {
        public string EnterpriseCode {
            get;
            set;
        }

        public string UserId {
            get;
            set;
        }
    }
    public class CacheWarehouse {
        public string EnterpriseCode {
            get;
            set;
        }

        public string UserId {
            get;
            set;
        }

        public int WarehouseId {
            get;
            set;
        }
    }
}
