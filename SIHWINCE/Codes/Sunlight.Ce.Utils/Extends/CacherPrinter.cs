﻿namespace Sunlight.Ce.Utils.Extends {
    public class CacherPrinter {
        /// <summary>
        /// 普通打印机的IP地址
        /// </summary>
        public string Ip {
            get;
            set;
        }

        /// <summary>
        /// 普通打印机的端口
        /// </summary>
        public string Port {
            get;
            set;
        }

        /// <summary>
        /// 标签打印机的IP地址
        /// </summary>
        public string LabelIp {
            get;
            set;
        }

        /// <summary>
        /// 标签打印机的端口
        /// </summary>
        public string LabelPort {
            get;
            set;
        }

        /// <summary>
        /// 普通打印机调用的打印机名称
        /// </summary>
        public string PrinterName {
            get;
            set;
        }
    }
}
