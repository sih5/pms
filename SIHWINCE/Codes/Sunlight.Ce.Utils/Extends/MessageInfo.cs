﻿using Newtonsoft.Json;

namespace Sunlight.Ce.Utils.Extends {
    /// <summary>
    ///     返回信息封装
    /// </summary>
    public class MessageInfo<T> {
        [JsonProperty("message")]
        public string Message {
            get;
            set;
        }

        [JsonProperty("data")]
        public T Data {
            get;
            set;
        }

        [JsonProperty("success")]
        public bool IsSuccess {
            get;
            set;
        }
    }

    /// <summary>
    ///     返回信息封装
    /// </summary>
    public class MessageInfo {
        [JsonProperty("message")]
        public string Message {
            get;
            set;
        }

        [JsonProperty("success")]
        public bool IsSuccess {
            get;
            set;
        }
    }
}
