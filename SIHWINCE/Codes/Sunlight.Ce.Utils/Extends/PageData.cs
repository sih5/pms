﻿using Newtonsoft.Json;

namespace Sunlight.Ce.Utils.Extends {
    /// <summary>
    ///     分页处理类
    /// </summary>
    public class PageData<T> {
        private int pageSize = 15;
        private string message = "加载成功！";

        /// <summary>
        ///     总数
        /// </summary>
        [JsonProperty("total")]
        public int TotalCount {
            set;
            get;
        }

        /// <summary>
        /// 调用是否成功
        /// </summary>
        [JsonProperty("success")]
        public bool IsSuccess {
            get;
            set;
        }

        /// <summary>
        ///     页面显示的条数
        /// </summary>
        [JsonProperty("pageSize")]
        public int PageSize {
            get {
                return this.pageSize;
            }
            set {
                this.pageSize = value;
            }
        }

        /// <summary>
        ///     页数
        /// </summary>
        [JsonProperty("pageTotal")]
        public int PageCount {
            set;
            get;
        }

        /// <summary>
        ///     当前页
        /// </summary>
        [JsonProperty("currentIndex")]
        public int CurrentPageIndex {
            set;
            get;
        }

        /// <summary>
        ///     当前页数据
        /// </summary>
        [JsonProperty("data")]
        public T Data {
            set;
            get;
        }

        /// <summary>
        ///     响应消息
        /// </summary>
        [JsonProperty("message")]
        public string Message {
            get {
                return this.message;
            }
            set {
                this.message = value;
            }
        }

        /// <summary>
        ///     用户自定义
        /// </summary>
        [JsonIgnore]
        public object UserObject {
            get;
            set;
        }
    }
}
