﻿namespace Sunlight.Ce.Utils.Extends {
    /// <summary>
    /// 服务器打印机项
    /// </summary>
    public class ServerPrinter {
        /// <summary>
        /// 打印机标识，此处仅指数据唯一键
        /// </summary>
        public int Key {
            get;
            set;
        }

        /// <summary>
        /// 打印机名称
        /// </summary>
        public string PrinterName {
            get;
            set;
        }

        /// <summary>
        /// 此处为方便调试使用
        /// </summary>
        /// <returns>打印机名称</returns>
        public override string ToString() {
            return this.PrinterName;
        }
    }
}
