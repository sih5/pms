﻿using System.Security.Cryptography;
using System.Text;

namespace Sunlight.Ce.Utils {
    public static class FormsAuth {
        /// <summary>
        /// 登录成功后，记录Cookie
        /// </summary>
        public static string Cookie = "";

        /// <summary>
        /// Security库的用户密码加密
        /// </summary>
        /// <param name="password">登陆密码</param>
        /// <returns>返回加密结果</returns>
        public static string HashPassword(string password) {
            var data = Encoding.UTF8.GetBytes(password);
            using (var sha1 = new SHA1Managed())
                data = sha1.ComputeHash(data);
            var sbResult = new StringBuilder(data.Length * 2);
            foreach (var b in data)
                sbResult.AppendFormat("{0:X2}", b);
            return sbResult.ToString();
        }
    }
}
