﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Sunlight.Ce.Utils.Extends;
using Sunlight.Ce.Utils.SysConfig;

namespace Sunlight.Ce.Utils {
    /// <summary>
    /// 帮助类
    /// </summary>
    public class HelperUtil {
        private static HelperUtil instance;
        private const string DEFAULT_CONTENT_TYPE = "application/json;charset=UTF-8";

        /// <summary>
        /// 本地时间转换成UTC时间
        /// C#和Unix时间戳的转换
        /// </summary>
        public static long DateTimeToUtc(DateTime dt) {
            return (long)(dt.ToUniversalTime() - (new DateTime(1970, 1, 1, 0, 0, 0, 0))).TotalMilliseconds;
        }

        public static DateTime UtcToDateTime(long number) {
            return new DateTime(1970, 1, 1, 0, 0, 0, 0).AddMilliseconds(number).ToUniversalTime();
        }

        /// <summary>
        /// 初始化实例
        /// </summary>
        public static HelperUtil Instance {
            get {
                return instance ?? (instance = new HelperUtil());
            }
        }

        /// <summary>
        /// 下载文件
        /// </summary>
        /// <param name="fileUrl">远程文件地址</param>    
        /// <param name="filePath">本地存储路径</param>
        public static void DownLoadFile(string fileUrl, string filePath) {
            try {
                var req = (HttpWebRequest)WebRequest.Create(fileUrl);
                var resp = (HttpWebResponse)req.GetResponse();
                using (var respStream = resp.GetResponseStream()) {
                    using (var wrtr = new FileStream(filePath, FileMode.Create)) {
                        var inData = new byte[4096];
                        int bytesRead = respStream.Read(inData, 0, inData.Length);
                        while (bytesRead > 0) {
                            wrtr.Write(inData, 0, bytesRead);
                            bytesRead = respStream.Read(inData, 0, inData.Length);
                        }
                    }
                }
            }
            catch (Exception ep) {
                MessageBox.Show(ep.Message);
            }
        }

        /// <summary>
        /// 获取UI项目下的Config目录中文件完整路径--文件路径属性，必须更改为始终复制方可使用，否则会报路径找不到(UpdateService.xml)
        /// </summary>
        /// <returns>文件的完整路径</returns>
        public string GetPath(string fileName) {
            var path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string combinePath = string.Format(path + "\\Config\\{0}", fileName);
            return combinePath;
        }

        //缓存用户信息，方便下次登录直接加载
        public void CachCurrentUserInfo(string enterpriseCode, string userId) {
            try {
                var path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                var combinePath = string.Format(path + "\\Config\\{0}", "CachUserInfo.xml");
                var xmlSer = new XmlSerializer(typeof(CacheUserInfo));
                //如果第二保存的信息比第一的短，比如原来的登陆人是10个字符，现在变成9个，则生成的xml会多出最后〉
                //目前先将原始文件删除
                if (File.Exists(combinePath))
                    File.Delete(combinePath);
                using (var file = new FileStream(combinePath, FileMode.OpenOrCreate, FileAccess.ReadWrite)) {
                    xmlSer.Serialize(file, new CacheUserInfo { EnterpriseCode = enterpriseCode, UserId = userId });
                }
            }
            catch (Exception) {
            }
        }

        /// <summary>
        /// 缓存用户登录信息
        /// </summary>
        /// <returns>企业id和用户名</returns>
        public CacheUserInfo GetCacheUserInfo() {
            var user = new CacheUserInfo();
            try {
                var path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                var combinePath = string.Format(path + "\\Config\\{0}", "CachUserInfo.xml");
                var xmlSer = new XmlSerializer(typeof(CacheUserInfo));
                using (var file = new FileStream(combinePath, FileMode.Open, FileAccess.Read)) {
                    user = xmlSer.Deserialize(file) as CacheUserInfo;
                }
            }
            catch (Exception ex) {

            }
            return user;
        }

        //缓存仓库信息
        public void CachCurrentWarehouse(string enterpriseCode, string userId, int warehouseId) {
            try {
                var path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                var combinePath = string.Format(path + "\\Config\\{0}", "CacheWarehouse.xml");
                var xmlSer = new XmlSerializer(typeof(CacheWarehouse));
                //如果第二保存的信息比第一的短，比如原来的登陆人是10个字符，现在变成9个，则生成的xml会多出最后〉
                //目前先将原始文件删除
                if (File.Exists(combinePath))
                    File.Delete(combinePath);
                using (var file = new FileStream(combinePath, FileMode.OpenOrCreate, FileAccess.ReadWrite)) {
                    xmlSer.Serialize(file, new CacheWarehouse { EnterpriseCode = enterpriseCode, UserId = userId, WarehouseId = warehouseId });
                }
            }
            catch (Exception) {
            }
        }

        /// <summary>
        /// 获取缓存用户登录仓库
        /// </summary>
        /// <returns>选择的仓库</returns>
        public CacheWarehouse GetCacheWarehouse() {
            var warehouse = new CacheWarehouse();
            try {
                var path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                var combinePath = string.Format(path + "\\Config\\{0}", "CacheWarehouse.xml");
                var xmlSer = new XmlSerializer(typeof(CacheWarehouse));
                using (var file = new FileStream(combinePath, FileMode.Open, FileAccess.Read)) {
                    warehouse = xmlSer.Deserialize(file) as CacheWarehouse;
                }
            }
            catch (Exception ex) {

            }
            return warehouse;
        }

        /// <summary>
        /// 缓存打印的信息
        /// </summary>
        /// <param name="ip">打印机IP地址</param>
        /// <param name="port">端口</param>
        /// <param name="labelIp">标签打印机IP</param>
        /// <param name="labelPort">标签打印机端口</param>
        public void CachCurrentPrinter(string ip, string port, string labelIp, string labelPort) {
            try {
                var path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                var combinePath = string.Format(path + "\\Config\\{0}", "CachePrinter.xml");
                var xmlSer = new XmlSerializer(typeof(CacherPrinter));
                //如果第二保存的信息比第一的短，比如原来的登陆人是10个字符，现在变成9个，则生成的xml会多出最后〉
                //目前先将原始文件删除
                if (File.Exists(combinePath))
                    File.Delete(combinePath);
                using (var file = new FileStream(combinePath, FileMode.OpenOrCreate, FileAccess.ReadWrite)) {
                    xmlSer.Serialize(file, new CacherPrinter { Ip = ip, Port = port, LabelIp = labelIp, LabelPort = labelPort });
                }
            }
            catch (Exception) {
            }
        }

        /// <summary>
        /// 缓存打印服务器的打印列表
        /// </summary>
        public void CachServerPrinterList(List<ServerPrinter> keyValuePairs) {
            try {
                var path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                var combinePath = string.Format(path + "\\Config\\{0}", "CachePrinterServerList.xml");
                if (File.Exists(combinePath))
                    File.Delete(combinePath);
                var xmlSer = new XmlSerializer(typeof(List<ServerPrinter>));
                using (var file = new FileStream(combinePath, FileMode.OpenOrCreate, FileAccess.ReadWrite)) {
                    xmlSer.Serialize(file, keyValuePairs);
                }
            }
            catch (Exception) {
            }
        }

        /// <summary>
        /// 缓存打印服务器的打印列表
        /// </summary>
        public List<ServerPrinter> GetServerPrinterList() {
            try {
                var path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                var combinePath = string.Format(path + "\\Config\\{0}", "CachePrinterServerList.xml");
                if (!File.Exists(combinePath))
                    return new List<ServerPrinter>();
                var xmlSer = new XmlSerializer(typeof(List<ServerPrinter>));
                using (var file = new FileStream(combinePath, FileMode.Open, FileAccess.Read)) {
                    return xmlSer.Deserialize(file) as List<ServerPrinter>;
                }
            }
            catch (Exception ex) {
                return new List<ServerPrinter>();
            }
        }

        /// <summary>
        /// 格式化json字符串
        /// </summary>
        /// <param name="source">格式化的源JSON数据</param>
        /// <returns>格式化后的JSON数据</returns>
        public string ConvertJsonString(string source) {
            //
            var serializer = new JsonSerializer();
            TextReader tr = new StringReader(source);
            var jtr = new JsonTextReader(tr);
            object obj = serializer.Deserialize(jtr);
            if (obj != null) {
                var textWriter = new StringWriter();
                var jsonWriter = new JsonTextWriter(textWriter) {
                    Formatting = Formatting.Indented,
                    Indentation = 4,
                    IndentChar = ' '
                };
                serializer.Serialize(jsonWriter, obj);
                return textWriter.ToString();
            }
            return source;
        }

        /// <summary>
        /// 获取打印的信息
        /// </summary>
        /// <returns></returns>
        public CacherPrinter GetCachePrinter() {
            var user = new CacherPrinter();
            try {
                var path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                var combinePath = string.Format(path + "\\Config\\{0}", "CachePrinter.xml");
                var xmlSer = new XmlSerializer(typeof(CacherPrinter));
                using (var file = new FileStream(combinePath, FileMode.OpenOrCreate, FileAccess.ReadWrite)) {
                    user = xmlSer.Deserialize(file) as CacherPrinter;
                }
            }
            catch (Exception) {
            }
            return user;
        }

        public static bool SettingPrinter() {
            try {
                var userInfo = Instance.GetCachePrinter();
                if (string.IsNullOrEmpty(userInfo.Ip) || string.IsNullOrEmpty(userInfo.Port)) {
                    return false;
                }
                return true;
            }
            catch (Exception) {
                return false;
            }
        }

        /// <summary>
        /// XML反序列化实体
        /// </summary>
        /// <typeparam name="T">需要反序列化的实体类型</typeparam>
        /// <param name="fileName">Config下的文件</param>
        /// <returns>返回T类型的实例</returns>
        public T Deserialize<T>(string fileName) {
            var combinePath = GetPath(fileName);
            var xmlSer = new XmlSerializer(typeof(T));
            object tInstance;
            using (var file = new FileStream(combinePath, FileMode.Open, FileAccess.Read)) {
                tInstance = xmlSer.Deserialize(file);
            }
            return (T)tInstance;
        }

        /// <summary>
        /// 传入Key，得到Configuration对象
        /// </summary>
        /// <returns>Configuration</returns>
        public Configuration GetConfigurationByKey(string key) {
            var sysConfig = Instance.Deserialize<SysConfig.SysConfig>("SysConfig.xml");
            if (sysConfig == null || sysConfig.Configurations == null || !sysConfig.Configurations.Any() || sysConfig.Configurations.All(r => r.AppSetting != key))
                throw new Exception("请检查对应的AppSetting Key是否存在");
            return sysConfig.Configurations.First(r => r.AppSetting == key);
        }

        /// <remarks>
        /// body是要传递的参数,格式"roleId=1&uid=2"
        /// post的cotentType填写:
        /// </remarks>
        /// <param name="url"></param>
        /// <param name="parameters">要传递的参数,格式"roleId=1&uid=2"</param>
        /// <returns>服务端响应的Json string</returns>
        public string Post(string url, string parameters) {
            GC.Collect();
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = DEFAULT_CONTENT_TYPE;
            httpWebRequest.Method = "POST";
            httpWebRequest.Timeout = 1000000;
            httpWebRequest.Headers.Add("Cookie", FormsAuth.Cookie);
            byte[] btBodys = Encoding.UTF8.GetBytes(parameters);
            httpWebRequest.ContentLength = btBodys.Length;
            using (Stream reqStream = httpWebRequest.GetRequestStream()) {
                reqStream.Write(btBodys, 0, btBodys.Length);
            }
            string responseData;
            using (var response = (HttpWebResponse)httpWebRequest.GetResponse()) {
                using (var reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8)) {
                    responseData = reader.ReadToEnd();
                }
            }
            httpWebRequest.Abort();
            return responseData;
        }

        /// <remarks>
        /// body是要传递的参数,格式JSON 
        /// post的cotentType填写:
        /// </remarks>
        /// <param name="url"></param>
        /// <param name="parameters">要传递的参数,格式JSON</param>
        /// <returns>服务端响应的Json string</returns>
        public string Put(string url, string parameters) {
            GC.Collect();
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = DEFAULT_CONTENT_TYPE;
            httpWebRequest.Method = "PUT";
            httpWebRequest.Timeout = 1000000;
            httpWebRequest.Headers.Add("Cookie", FormsAuth.Cookie);
            byte[] btBodys = Encoding.UTF8.GetBytes(parameters);
            httpWebRequest.ContentLength = btBodys.Length;
            using (Stream reqStream = httpWebRequest.GetRequestStream()) {
                reqStream.Write(btBodys, 0, btBodys.Length);
            }
            string responseData;
            using (var response = (HttpWebResponse)httpWebRequest.GetResponse()) {
                using (var reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8)) {
                    responseData = reader.ReadToEnd();
                }
            }
            httpWebRequest.Abort();
            return responseData;
        }

        /// <summary>
        /// Get Http 如：this.GetHttp("http://192.168.17.93:83/logistics/partsinboundplan/scan", "orderCode=\"we\"");
        /// </summary>
        /// <param name="url">Url地址</param>
        /// <param name="parameter">参数,一般为主键Id</param>
        /// <returns>返回响应的Json 字符串</returns>
        public string Delete(string url, string parameter) {
            GC.Collect();
            if (!string.IsNullOrEmpty(parameter)) {
                url += ("/" + parameter);
            }
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            // httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "DELETE";
            httpWebRequest.Timeout = 20000;
            httpWebRequest.KeepAlive = false;
            httpWebRequest.Headers.Add("Cookie", FormsAuth.Cookie);
            string responseContent;
            using (var httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse()) {
                using (var streamReader = new StreamReader(httpWebResponse.GetResponseStream())) {
                    responseContent = streamReader.ReadToEnd();
                }
            }
            return responseContent;
        }

        /// <summary>
        /// Get Http 如：this.GetHttp("http://192.168.17.93:83/logistics/partsinboundplan/scan", "orderCode=\"we\"");
        /// </summary>
        /// <param name="url">Url地址</param>
        /// <param name="parameters">参数，格式如：name=1&sex=1</param>
        /// <returns>返回响应的Json 字符串</returns>
        public string Get(string url, string parameters) {
            GC.Collect();
            if (!string.IsNullOrEmpty(parameters)) {
                url += ("?" + parameters);
            }
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            // httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "GET";
            httpWebRequest.Timeout = 20000;
            httpWebRequest.KeepAlive = false;
            httpWebRequest.Headers.Add("Cookie", FormsAuth.Cookie);
            string responseContent;
            using (var httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse()) {
                using (var streamReader = new StreamReader(httpWebResponse.GetResponseStream())) {
                    responseContent = streamReader.ReadToEnd();
                }
            }
            return responseContent;
        }

        /// <remarks>
        /// 登录使用，此处需要拿到response.GetResponseHeader("Set-Cookie")，作为服务端访问的钥匙
        /// </remarks>
        /// <param name="url"></param>
        /// <param name="parameters">要传递的参数"</param>
        /// <returns>服务端响应的Json string</returns>
        public string Login(string url, string parameters) {
            GC.Collect();
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json;charset=UTF-8";
            httpWebRequest.Method = "POST";
            httpWebRequest.Timeout = 1000000;
            byte[] btBodys = Encoding.UTF8.GetBytes(parameters);
            httpWebRequest.ContentLength = btBodys.Length;
            using (Stream reqStream = httpWebRequest.GetRequestStream()) {
                reqStream.Write(btBodys, 0, btBodys.Length);
            }
            string responseData;
            using (var response = (HttpWebResponse)httpWebRequest.GetResponse()) {
                FormsAuth.Cookie = response.GetResponseHeader("Set-Cookie");
                int index = FormsAuth.Cookie.IndexOf(".ASPXAUTH=");
                if (index != -1)
                    FormsAuth.Cookie = FormsAuth.Cookie.Substring(index, FormsAuth.Cookie.Length - index);
                using (var reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8)) {
                    responseData = reader.ReadToEnd();
                }
            }
            httpWebRequest.Abort();
            return responseData;
        }

        /// <summary>
        /// 组合URL 如：http://192.168.17.93:83/logistics/partsinboundplan/scan
        /// </summary>
        /// <param name="urlKey">如：http://192.168.17.93:83/</param>
        /// <param name="urlRoute">如：logistics/partsinboundplan/scan</param>
        /// <returns>返回完整的URL地址</returns>
        public static string CombineUrl(string urlKey, string urlRoute) {
            return Instance.GetConfigurationByKey("ServiceAddress").AppSettingValue + urlRoute;
        }

        /// <summary>
        /// 返回JSon数据
        /// </summary>
        /// <param name="jsonData">要处理的JSON数据</param>
        /// <param name="url">要提交的URL</param>
        /// <returns>返回的JSON处理字符串</returns>
        public string GetResponseData(string jsonData, string url) {
            byte[] bytes = Encoding.UTF8.GetBytes(jsonData);
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentLength = bytes.Length;
            request.ContentType = "text/xml";
            using (Stream reqstream = request.GetRequestStream()) {
                reqstream.Write(bytes, 0, bytes.Length);
            }

            //声明一个HttpWebRequest请求
            request.Timeout = 90000;
            //设置连接超时时间
            request.Headers.Set("Pragma", "no-cache");
            var response = (HttpWebResponse)request.GetResponse();
            string strResult;
            using (Stream streamReceive = response.GetResponseStream()) {
                Encoding encoding = Encoding.UTF8;
                using (var streamReader = new StreamReader(streamReceive, encoding)) {
                    strResult = streamReader.ReadToEnd();
                }
            }
            return strResult;
        }

        /// <summary>
        /// C# byte数组合并(（二进制数组合并）
        /// </summary>
        /// <param name="srcArray1">待合并数组1</param>
        /// <param name="srcArray2">待合并数组2</param>
        /// <returns>合并后的数组</returns>
        public static byte[] CombineBinaryArray(byte[] srcArray1, byte[] srcArray2) {
            //根据要合并的两个数组元素总数新建一个数组
            var newArray = new byte[srcArray1.Length + srcArray2.Length];

            //把第一个数组复制到新建数组
            Array.Copy(srcArray1, 0, newArray, 0, srcArray1.Length);

            //把第二个数组复制到新建数组
            Array.Copy(srcArray2, 0, newArray, srcArray1.Length, srcArray2.Length);

            return newArray;
        }

        public static bool RemoveFile(string fileName) {
            try {
                if (File.Exists(fileName) == false)
                    return true;
                File.Delete(fileName);
            }
            catch (Exception) {
                return false;
            }
            return true;
        }
    }
}
