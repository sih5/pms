﻿using System;
using System.Collections.Generic;

namespace Sunlight.Ce.Utils {
    //由于WINCE对Enum.GetEnums的不支持，QAS采用下拉列表手动维护，统一在此类中维护
    public sealed class KeyValueManager {
        public List<KeyValuePair> this[Type enumType] {
            get {
                switch (enumType.Name) {
                    case "BaseDataStatus":
                        return this.BaseDataStatus();
                }
                return new List<KeyValuePair>();
            }
        }

        //基础数据状态
        private List<KeyValuePair> BaseDataStatus() {
            var result = new List<KeyValuePair>();
            result.Add(new KeyValuePair { Key = 0, Value = "作废" });
            result.Add(new KeyValuePair { Key = 1, Value = "有效" });
            return result;
        }
    }
}
