﻿namespace Sunlight.Ce.Utils {
    public class KeyValuePair {
        public int Key {
            get;
            set;
        }
        public string Value {
            get;
            set;
        }
        public object UserObject {
            get;
            set;
        }
    }
}
