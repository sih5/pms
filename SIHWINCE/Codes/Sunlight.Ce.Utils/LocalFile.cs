using System.Xml.Serialization;

namespace Sunlight.Ce.Utils {
    public class LocalFile {
        private string path = "";
        private string lastver = "";
        private int size = 0;

        [XmlAttribute("path")]
        public string Path { get { return this.path; } set { this.path = value; } }
        [XmlAttribute("lastver")]
        public string LastVer { get { return this.lastver; } set { this.lastver = value; } }
        [XmlAttribute("size")]
        public int Size { get { return this.size; } set { this.size = value; } }

        public LocalFile(string path, string ver, int size) {
            this.path = path;
            this.lastver = ver;
            this.size = size;
        }

        public LocalFile() {
        }

    }
}