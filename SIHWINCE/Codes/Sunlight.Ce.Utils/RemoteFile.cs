using System;
using System.Xml;

namespace Sunlight.Ce.Utils {
    public class RemoteFile {
        private string path = "";
        private string url = "";
        private string lastver = "";
        private int size = 0;
        private bool needRestart = false;

        public string Path { get { return this.path; } }
        public string Url { get { return this.url; } }
        public string LastVer { get { return this.lastver; } }
        public int Size { get { return this.size; } }
        public bool NeedRestart { get { return this.needRestart; } }

        public RemoteFile(XmlNode node) {
            this.path = node.Attributes["path"].Value;
            this.url = node.Attributes["url"].Value;
            this.lastver = node.Attributes["lastver"].Value;
            this.size = Convert.ToInt32(node.Attributes["size"].Value);
            this.needRestart = Convert.ToBoolean(node.Attributes["needRestart"].Value);
        }
    }
}