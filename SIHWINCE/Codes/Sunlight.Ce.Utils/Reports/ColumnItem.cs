﻿namespace Sunlight.Ce.Utils.Reports {
    public class ColumnItem {
        /// <summary>
        ///     名称
        /// </summary>
        public string Name {
            get;
            set;
        }

        /// <summary>
        ///     显示文本
        /// </summary>
        public string Text {
            get;
            set;
        }

        /// <summary>
        ///     格式化
        /// </summary>
        public DisplayStyle Format {
            get;
            set;
        }

        /// <summary>
        ///     显示列顺序
        /// </summary>
        public int DisplayIndex {
            get;
            set;
        }
        /// <summary>
        /// 是否换行
        /// </summary>
        public bool IsWrap {
            get;
            set;
        }
    }
}

public enum DisplayStyle {
    /// <summary>
    ///     小数
    /// </summary>
    NUMERIC,

    /// <summary>
    ///     金额
    /// </summary>
    CURRENCY,
}
