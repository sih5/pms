﻿namespace Sunlight.Ce.Utils.Reports {
    public class HeadColumnItem : ColumnItem {
        /// <summary>
        ///     标题
        /// </summary>
        public string Title {
            get;
            set;
        }

        /// <summary>
        ///     列宽
        /// </summary>
        public int Width {
            get;
            set;
        }
    }
}
