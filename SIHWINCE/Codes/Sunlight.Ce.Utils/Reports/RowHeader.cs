﻿using System.Collections.Generic;

namespace Sunlight.Ce.Utils.Reports {
    public class RowHeader {
        private readonly List<HeadColumnItem> columns = new List<HeadColumnItem>();

        /// <summary>
        ///     标题高
        /// </summary>
        public int HeadHeight {
            get;
            set;
        }

        /// <summary>
        ///     显示列
        /// </summary>
        public List<HeadColumnItem> Columns {
            get {
                return this.columns;
            }
        }
    }
}
