﻿using System.Collections.Generic;

namespace Sunlight.Ce.Utils.Reports {
    public class RowItem {
        private readonly List<ColumnItem> columns = new List<ColumnItem>();

        /// <summary>
        ///     行高
        /// </summary>
        public int Height {
            get;
            set;
        }

        /// <summary>
        ///     序号
        /// </summary>
        public int Serial {
            get;
            set;
        }

        /// <summary>
        ///     当前页序号
        /// </summary>
        public int CurrentPageSerial {
            get;
            set;
        }

        /// <summary>
        ///     列
        /// </summary>
        public List<ColumnItem> Columns {
            get {
                return this.columns;
            }
        }
    }
}
