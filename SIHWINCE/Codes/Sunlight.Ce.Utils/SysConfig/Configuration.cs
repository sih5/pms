﻿using System.Xml.Serialization;

namespace Sunlight.Ce.Utils.SysConfig {
    public class Configuration {
        [XmlElement("appSetting")]
        public string AppSetting {
            get;
            set;
        }
        [XmlElement("appSettingValue")]
        public string AppSettingValue {
            get;
            set;
        }
        [XmlElement("appSettingExtend")]
        public string AppSettingExtend {
            get;
            set;
        }
    }
}
