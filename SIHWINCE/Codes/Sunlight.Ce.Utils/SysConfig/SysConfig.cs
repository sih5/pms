﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Sunlight.Ce.Utils.SysConfig {
    [XmlRoot("sysConfig")]
    public class SysConfig {
        [XmlArray("configurations")]
        [XmlArrayItem("configuration")]
        public List<Configuration> Configurations {
            get;
            set;
        }
    }
}
