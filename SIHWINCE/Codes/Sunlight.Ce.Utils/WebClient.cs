﻿using System;

namespace Sunlight.Ce.Utils {
    /// <summary>
    /// 自定义WebClient  .NET Compact Framework 不存在WebClient
    /// </summary>
    public class WebClient {
        public delegate void BeginEventHandler(object sender, EventArgs e);
        public delegate void ProcessingEventHandler(object sender, EventArgs e);
        public delegate void CompleteEventHandler(object sender, EventArgs e);
        //用event 关键字声明事件对象
        public event BeginEventHandler BeginEvent;
        public event BeginEventHandler ProcessingEvent;
        public event BeginEventHandler CompleteEvent;

        //开始更新时触发事件
        protected virtual void OnBeginEvent(EventArgs e) {
            if (BeginEvent != null)
                BeginEvent(this, e);
        }

        //处理进度条使用事件
        protected virtual void OnProcessingEvent(EventArgs e) {
            if (ProcessingEvent != null)
                ProcessingEvent(this, e);
        }

        //结束时处理事件
        protected virtual void OnCompleteEvent(EventArgs e) {
            if (CompleteEvent != null)
                CompleteEvent(this, e);
        }

        //触发事件引擎
        public void RaiseEvent() {
            var beginEvent = new EventArgs();
            OnBeginEvent(beginEvent);
            var processingEvent = new EventArgs();
            OnBeginEvent(processingEvent);
            var completeEvent = new EventArgs();
            OnBeginEvent(completeEvent);
        }
    }
}
