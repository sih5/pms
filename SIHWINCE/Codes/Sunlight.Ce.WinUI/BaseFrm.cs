﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace Sunlight.Ce.WinUI {
    [DesignerCategory("BaseFrm")]
    [DesignTimeVisible(false)]
    public class BaseFrm : Form {
        private bool isInitialize;
        private readonly CommonRegister register = new CommonRegister();
        public event EventHandler EditSubmitted; //该事件用户保存后，返回主界面之后的处理

        /// <summary>
        /// 唤醒EditSubmitted事件
        /// </summary>
        protected void RasieEditSubmitted() {
            if (EditSubmitted != null)
                EditSubmitted.Invoke(this, null);
        }

        /// <summary>
        /// 注册控件，标记为数字类型的控件
        /// </summary>
        public CommonRegister Register {
            get {
                return this.register;
            }
        }

        /// <summary>
        /// 是否启用Loading
        /// </summary>
        protected void IsLoading(bool isLoading) {
            switch (isLoading) {
                case true:
                    Cursor.Current = Cursors.WaitCursor;
                    break;
                case false:
                    Cursor.Current = Cursors.Default;
                    break;
                default:
                    Cursor.Current = Cursors.Default;
                    break;
            }
        }

        /// <summary>
        /// 是否跳过业务操作，此处的目的是用于标记，是否跳过业务，用于关闭窗体时，避免该窗体内的LostFocus事件的继续执行
        /// </summary>
        protected bool IsSkipBusinessOperation {
            get;
            set;
        }

        /// <summary>
        /// 重置弹出框的OK按钮文字为 “确定”
        /// </summary>
        public Dictionary<string, DialogResult> DefaultButtonResults = new Dictionary<string, DialogResult> {
            {
                "确定", DialogResult.OK
            }
        };

        /// <summary>
        /// 重置弹出框的OK Cancel为“确定”和“取消”
        /// </summary>
        public Dictionary<string, DialogResult> ButtonResults = new Dictionary<string, DialogResult> {
            {
                "确定", DialogResult.OK
            }, {
                "取消", DialogResult.Cancel
            }
        };

        /// <summary>
        /// 该方法的内容只在第一次loaded之后执行
        /// </summary>
        private void Initialize() {
            //隐藏最大或者最小化窗口的功能
            this.MaximizeBox = false;
            this.MinimizeBox = false;

            this.KeyPreview = true;
            this.KeyUp -= this.BaseFrm_KeyUp;
            this.KeyUp += this.BaseFrm_KeyUp;
        }

        private void BaseFrm_KeyUp(object sender, KeyEventArgs e) {
            try {
                if (e.KeyCode == Keys.Escape)
                    this.Close();
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message, "提示");
            }
        }

        /// <summary>
        /// 公共的关闭窗体处理
        /// </summary>
        protected virtual void SetWindowClose() {
            try {
                this.Close();
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message, "提示");
            }
        }

        private void BaseFrm_Load(object sender, EventArgs e) {
            //初始化时禁止跳过业务，具体用法参考 备件拣货
            this.IsSkipBusinessOperation = false;

            if (!this.isInitialize) {
                isInitialize = true;
                Initialize();
            }
        }

        public BaseFrm() {
            this.Load -= this.BaseFrm_Load;
            this.Load += this.BaseFrm_Load;
        }

        public int integerToInt(int? integer) {
            return integer.HasValue ? integer.Value : 0;
        }

        /// <summary>
        /// 设置选中行的颜色
        /// </summary>
        /// <param name="parmsDataGrid"></param>
        public void SelectRowColor(DataGrid parmsDataGrid) {
            try {
                if (parmsDataGrid.VisibleRowCount == 0) {
                    return;
                }
                else {
                    parmsDataGrid.Select(parmsDataGrid.CurrentRowIndex);
                }
            }
            catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }
    }
}
