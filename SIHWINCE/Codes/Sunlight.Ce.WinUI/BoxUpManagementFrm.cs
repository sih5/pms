﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Sunlight.Ce.Controls;
using Sunlight.Ce.Entities.DTO;
using Sunlight.Ce.Service;
using Sunlight.Ce.Utils.Extends;

namespace Sunlight.Ce.WinUI {
    public partial class BoxUpManagementFrm : BaseFrm {
        private List<BoxUpTaskDTO> boxUpTasks;
        private bool isInitialize = false;
        public BoxUpManagementFrm() {
            InitializeComponent();
            this.Initialize();
        }

        private void Initialize() {
            if (!this.isInitialize) {
                this.isInitialize = true;

                this.dgvPicking.TableStyles.Clear();
                DataTable dt = this.SetDataSource();
                DataView dv = dt.DefaultView;
                dv.Sort = "单号 Asc";
                dv.Sort = "紧急 Desc";
                dt = dv.ToTable();
                this.SetDataGridStyle(this.dgvPicking, dt);
                this.dgvPicking.DataSource = dt;

                this.txtScanCode.Text = string.Empty;
            }
        }

        private DataTable SetDataSource() {
            var dd = new DataTable();
            dd.TableName = "dgvPicking";
            dd.Columns.Add("单号");
            dd.Columns.Add("紧急");
            if (boxUpTasks != null) {
                foreach (var t in boxUpTasks.GroupBy(o => o.Code).OrderBy(o => o.Key)) {
                    DataRow dr = dd.NewRow();
                    dr["单号"] = t.Key;
                    var boxUpTaskDto = boxUpTasks.FirstOrDefault(o => o.Code == t.Key);
                    if (boxUpTaskDto != null)
                        dr["紧急"] = boxUpTaskDto.isUrgent;
                    else
                        dr["紧急"] = "";
                    dd.Rows.Add(dr);
                }
            }
            return dd;
        }

        public void SelectDataGridRow(DataGrid dataGrid) {
            if (dataGrid.VisibleRowCount == 0)
                return;
            dataGrid.Select(dataGrid.CurrentRowIndex);
            if (dataGrid.DataSource != null && dataGrid.CurrentRowIndex != -1) {
                string code = dataGrid[dataGrid.CurrentRowIndex, 0].ToString();
                var newBoxUpTasks = boxUpTasks.Where(o => o.Code == code).ToList();
                if (newBoxUpTasks.Count > 0) {
                    var buf = new BoxUpingFrm(newBoxUpTasks);
                    buf.ShowDialog();
                    this.isInitialize = false;
                    boxUpTasks = new List<BoxUpTaskDTO>();
                    this.Initialize();
                }
                else
                    MessageBoxForm.Show(this, "根据单号，为找到任务清单，请重新扫描单号！", "提示", this.DefaultButtonResults);
            }
        }

        private void SetDataGridStyle(DataGrid dataGrid, DataTable dataTable) {
            Color alternatingColor = SystemColors.ControlDark; //交替行颜色

            dataGrid.BackgroundColor = Color.White;
            dataGrid.RowHeadersVisible = false;

            var dataGridTableStyle = new DataGridTableStyle();
            dataGridTableStyle.MappingName = dataTable.TableName;

            dataGridTableStyle.GridColumnStyles.Add(new DataGridCustomTextBoxColumn {
                Owner = dataGrid,
                HeaderText = "单号",
                MappingName = dataTable.Columns[0].ColumnName,
                NullText = "",
                ReadOnly = true,
                Alignment = HorizontalAlignment.Center,
                AlternatingBackColor = alternatingColor,
                Width = 180
            });
            dataGridTableStyle.GridColumnStyles.Add(new DataGridCustomTextBoxColumn {
                Owner = dataGrid,
                HeaderText = "紧急",
                MappingName = dataTable.Columns[1].ColumnName,
                NullText = "",
                ReadOnly = true,
                Alignment = HorizontalAlignment.Center,
                AlternatingBackColor = alternatingColor,
                Width = 45
            });

            dataGrid.TableStyles.Add(dataGridTableStyle);
        }
        private void dgvPicking_DoubleClick(object sender, EventArgs e) {
            this.SelectDataGridRow(this.dgvPicking);
        }

        private void dgvPicking_GotFocus(object sender, EventArgs e) {
            try {
                this.SelectRowColor(this.dgvPicking);
            }
            catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }

        private void dgvPicking_LostFocus(object sender, EventArgs e) {
            try {
                if (this.dgvPicking.CurrentRowIndex >= 0) {
                    this.dgvPicking.UnSelect(this.dgvPicking.CurrentRowIndex);
                }
            }
            catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }

        private void dgvPicking_KeyDown(object sender, KeyEventArgs e) {
            try {
                switch (e.KeyCode) {
                    case Keys.Enter:
                        SelectDataGridRow(this.dgvPicking);
                        break;
                    case Keys.Up:
                        this.SelectRowColor(this.dgvPicking);
                        break;
                    case Keys.Down:
                        this.SelectRowColor(this.dgvPicking);
                        break;
                }
            }
            catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }

        private int ScanTicketClear = 0;
        private void txtScanCode_KeyDown(object sender, KeyEventArgs e) {
            if (ScanTicketClear == 0) {
                this.txtScanCode.Text = string.Empty;
                ScanTicketClear++;
            }
            if (e.KeyCode == Keys.Enter) {
                ScanTicketClear = 0;
                this.IsLoading(true);
                MessageInfo<List<BoxUpTaskDTO>> messageInfo = ServiceManager.GetBoxUpCodes(txtScanCode.Text.Trim());
                this.IsLoading(false);

                if (messageInfo.IsSuccess)
                    if (messageInfo.Data != null && messageInfo.Data.Any()) {
                        boxUpTasks = messageInfo.Data;
                        if (boxUpTasks == null)
                            boxUpTasks = new List<BoxUpTaskDTO>();
                        //界面参数
                        this.dgvPicking.TableStyles.Clear();
                        DataTable dt = this.SetDataSource();
                        DataView dv = dt.DefaultView;
                        dv.Sort = "单号 Asc";
                        dv.Sort = "紧急 Desc";
                        dt = dv.ToTable();
                        this.SetDataGridStyle(this.dgvPicking, dt);
                        this.dgvPicking.DataSource = dt;

                        if (boxUpTasks.GroupBy(o => o.Code).Count() == 1) {
                            var buf = new BoxUpingFrm(boxUpTasks);
                            buf.ShowDialog();
                            this.isInitialize = false;
                            boxUpTasks = new List<BoxUpTaskDTO>();
                            this.Initialize();
                            return;
                        }
                    }
                    else {
                        boxUpTasks = new List<BoxUpTaskDTO>();
                        this.dgvPicking.TableStyles.Clear();
                        DataTable dt = this.SetDataSource();
                        this.SetDataGridStyle(this.dgvPicking, dt);
                        this.dgvPicking.DataSource = dt;
                        MessageBoxForm.Show(this, "无查询结果", "提示", this.DefaultButtonResults);
                    }
                else
                    MessageBoxForm.Show(this, messageInfo.Message, "提示", this.DefaultButtonResults);
            }
        }
    }
}