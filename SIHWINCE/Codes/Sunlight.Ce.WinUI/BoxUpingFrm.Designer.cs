﻿namespace Sunlight.Ce.WinUI {
    partial class BoxUpingFrm {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent() {
            this.txtCode = new System.Windows.Forms.TextBox();
            this.lblCode = new System.Windows.Forms.Label();
            this.txtScanSparePartCode = new System.Windows.Forms.TextBox();
            this.lblSparePartCode = new System.Windows.Forms.Label();
            this.txtScanNumber = new System.Windows.Forms.TextBox();
            this.lblScanNumber = new System.Windows.Forms.Label();
            this.txtSparePartsName = new System.Windows.Forms.TextBox();
            this.lblSparePartsName = new System.Windows.Forms.Label();
            this.txtThisShelvesAmount = new System.Windows.Forms.TextBox();
            this.lblThisShelvesAmount = new System.Windows.Forms.Label();
            this.labSprit = new System.Windows.Forms.Label();
            this.btnOver = new System.Windows.Forms.Button();
            this.btnUpload = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtCode
            // 
            this.txtCode.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtCode.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtCode.Location = new System.Drawing.Point(71, 14);
            this.txtCode.Name = "txtCode";
            this.txtCode.ReadOnly = true;
            this.txtCode.Size = new System.Drawing.Size(167, 26);
            this.txtCode.TabIndex = 50;
            // 
            // lblCode
            // 
            this.lblCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblCode.Location = new System.Drawing.Point(1, 18);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(54, 27);
            this.lblCode.Text = "单号";
            // 
            // txtScanSparePartCode
            // 
            this.txtScanSparePartCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtScanSparePartCode.Location = new System.Drawing.Point(69, 100);
            this.txtScanSparePartCode.Name = "txtScanSparePartCode";
            this.txtScanSparePartCode.Size = new System.Drawing.Size(169, 26);
            this.txtScanSparePartCode.TabIndex = 61;
            this.txtScanSparePartCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtScanSparePartCode_KeyDown);
            // 
            // lblSparePartCode
            // 
            this.lblSparePartCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblSparePartCode.Location = new System.Drawing.Point(0, 104);
            this.lblSparePartCode.Name = "lblSparePartCode";
            this.lblSparePartCode.Size = new System.Drawing.Size(92, 20);
            this.lblSparePartCode.Text = "扫描配件";
            // 
            // txtScanNumber
            // 
            this.txtScanNumber.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtScanNumber.Location = new System.Drawing.Point(69, 56);
            this.txtScanNumber.Name = "txtScanNumber";
            this.txtScanNumber.Size = new System.Drawing.Size(169, 26);
            this.txtScanNumber.TabIndex = 60;
            this.txtScanNumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtScanNumber_KeyDown);
            // 
            // lblScanNumber
            // 
            this.lblScanNumber.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblScanNumber.Location = new System.Drawing.Point(0, 60);
            this.lblScanNumber.Name = "lblScanNumber";
            this.lblScanNumber.Size = new System.Drawing.Size(92, 20);
            this.lblScanNumber.Text = "扫描箱号";
            // 
            // txtSparePartsName
            // 
            this.txtSparePartsName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSparePartsName.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtSparePartsName.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtSparePartsName.Location = new System.Drawing.Point(71, 145);
            this.txtSparePartsName.Name = "txtSparePartsName";
            this.txtSparePartsName.ReadOnly = true;
            this.txtSparePartsName.Size = new System.Drawing.Size(167, 26);
            this.txtSparePartsName.TabIndex = 65;
            // 
            // lblSparePartsName
            // 
            this.lblSparePartsName.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblSparePartsName.Location = new System.Drawing.Point(-1, 147);
            this.lblSparePartsName.Name = "lblSparePartsName";
            this.lblSparePartsName.Size = new System.Drawing.Size(95, 27);
            this.lblSparePartsName.Text = "零件名称";
            // 
            // txtThisShelvesAmount
            // 
            this.txtThisShelvesAmount.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtThisShelvesAmount.Location = new System.Drawing.Point(71, 188);
            this.txtThisShelvesAmount.Name = "txtThisShelvesAmount";
            this.txtThisShelvesAmount.Size = new System.Drawing.Size(84, 26);
            this.txtThisShelvesAmount.TabIndex = 69;
            this.txtThisShelvesAmount.TextChanged += new System.EventHandler(this.txtThisShelvesAmount_TextChanged);
            // 
            // lblThisShelvesAmount
            // 
            this.lblThisShelvesAmount.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblThisShelvesAmount.Location = new System.Drawing.Point(2, 190);
            this.lblThisShelvesAmount.Name = "lblThisShelvesAmount";
            this.lblThisShelvesAmount.Size = new System.Drawing.Size(60, 25);
            this.lblThisShelvesAmount.Text = "数量";
            // 
            // labSprit
            // 
            this.labSprit.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.labSprit.Location = new System.Drawing.Point(176, 190);
            this.labSprit.Name = "labSprit";
            this.labSprit.Size = new System.Drawing.Size(60, 26);
            this.labSprit.Text = "/";
            // 
            // btnOver
            // 
            this.btnOver.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnOver.Location = new System.Drawing.Point(20, 229);
            this.btnOver.Name = "btnOver";
            this.btnOver.Size = new System.Drawing.Size(84, 35);
            this.btnOver.TabIndex = 73;
            this.btnOver.Text = "完成装箱";
            this.btnOver.Click += new System.EventHandler(this.btnOver_Click);
            // 
            // btnUpload
            // 
            this.btnUpload.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnUpload.Location = new System.Drawing.Point(135, 229);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(84, 35);
            this.btnUpload.TabIndex = 72;
            this.btnUpload.Text = "完成一箱";
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // BoxUpingFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 295);
            this.Controls.Add(this.btnOver);
            this.Controls.Add(this.btnUpload);
            this.Controls.Add(this.txtThisShelvesAmount);
            this.Controls.Add(this.lblThisShelvesAmount);
            this.Controls.Add(this.labSprit);
            this.Controls.Add(this.txtSparePartsName);
            this.Controls.Add(this.lblSparePartsName);
            this.Controls.Add(this.txtScanSparePartCode);
            this.Controls.Add(this.lblSparePartCode);
            this.Controls.Add(this.txtScanNumber);
            this.Controls.Add(this.lblScanNumber);
            this.Controls.Add(this.txtCode);
            this.Controls.Add(this.lblCode);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BoxUpingFrm";
            this.Text = "装箱";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtCode;
        private System.Windows.Forms.Label lblCode;
        private System.Windows.Forms.TextBox txtScanSparePartCode;
        private System.Windows.Forms.Label lblSparePartCode;
        private System.Windows.Forms.TextBox txtScanNumber;
        private System.Windows.Forms.Label lblScanNumber;
        private System.Windows.Forms.TextBox txtSparePartsName;
        private System.Windows.Forms.Label lblSparePartsName;
        private System.Windows.Forms.TextBox txtThisShelvesAmount;
        private System.Windows.Forms.Label lblThisShelvesAmount;
        private System.Windows.Forms.Label labSprit;
        private System.Windows.Forms.Button btnOver;
        private System.Windows.Forms.Button btnUpload;
    }
}