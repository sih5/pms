﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Sunlight.Ce.Controls;
using Sunlight.Ce.Entities.DTO;
using Sunlight.Ce.Service;
using Sunlight.Ce.Utils;

namespace Sunlight.Ce.WinUI {
    public partial class BoxUpingFrm : BaseFrm {
        private List<BoxUpTaskDTO> boxUpTasks;
        private bool isInitialize;
        private int index = 0;
        private string oldNumber;
        public BoxUpingFrm(List<BoxUpTaskDTO> boxUpTasksPara) {
            InitializeComponent();

            boxUpTasks = boxUpTasksPara;
            this.Initialize();
        }

        private void Initialize() {
            if (!this.isInitialize) {
                this.isInitialize = true;

                this.txtThisShelvesAmount.ReadOnly = true;
                if (boxUpTasks[index].Status == (int)DcsBoxUpTaskStatus.新建)
                    this.btnOver.Enabled = false;

                this.txtScanSparePartCode.ReadOnly = true;
                this.txtCode.Text = boxUpTasks[index].Code;
                this.txtScanNumber.Text = string.Empty;
                this.txtScanSparePartCode.Text = string.Empty;
                this.txtSparePartsName.Text = string.Empty;
                this.txtThisShelvesAmount.Text = string.Empty;
                this.labSprit.Text = string.Empty;
                this.txtScanNumber.Focus();
            }
        }

        private void ScanInit() {
            this.txtSparePartsName.Text = boxUpTasks[index].SparePartName;
            this.labSprit.Text = (boxUpTasks[index].BoxUpQty ?? 0) + "/" + boxUpTasks[index].PlanQty; ;
        }

        private int ScanTicketClear = 0;
        private void txtScanNumber_KeyDown(object sender, KeyEventArgs e) {
            if (ScanTicketClear == 0) {
                this.txtScanNumber.Text = string.Empty;
                ScanTicketClear++;
            }
            if (e.KeyCode == Keys.Enter) {
                ScanTicketClear = 0;

                var indexs = boxUpTasks.FindIndex(o => o.ContainerNumber == oldNumber && o.ThisBoxUpQty > 0);
                if (indexs >= 0) {
                    MessageBoxForm.Show(this, "请先完成" + oldNumber + "箱号的任务！", "提示", this.DefaultButtonResults);
                    this.txtScanNumber.Text = oldNumber;
                    return;
                }
                if (string.IsNullOrEmpty(oldNumber) || string.IsNullOrEmpty(this.txtScanSparePartCode.Text)) {
                    oldNumber = this.txtScanNumber.Text;
                }
                this.txtScanSparePartCode.ReadOnly = false;
                this.txtScanSparePartCode.Focus();
            }
        }

        private void txtScanSparePartCode_KeyDown(object sender, KeyEventArgs e) {
            if (ScanTicketClear == 0) {
                this.txtScanSparePartCode.Text = string.Empty;
                ScanTicketClear++;
            }
            if (e.KeyCode == Keys.Enter) {
                ScanTicketClear = 0;

                string[] codeAndNum = this.txtScanSparePartCode.Text.Split('|');
                if (codeAndNum.Length < 2) {
                    MessageBoxForm.Show(this, "扫描配件条码错误！", "提示", this.DefaultButtonResults);
                    this.txtScanSparePartCode.Text = null;
                    return;
                }
                var newIndex = boxUpTasks.FindIndex(o => o.SparePartCode == codeAndNum[0] && o.PlanQty - (o.BoxUpQty ?? 0) >= Convert.ToInt32(codeAndNum[1]) && o.PlanQty != ((o.BoxUpQty ?? 0) + (o.ThisBoxUpQty ?? 0)));
                if (newIndex != index) {
                    if (newIndex >= 0) {
                        this.txtThisShelvesAmount.Text = string.Empty;
                        index = newIndex;
                    }
                    else {
                        MessageBoxForm.Show(this, "扫描配件条码错误！", "提示", this.DefaultButtonResults);
                        this.txtScanSparePartCode.Text = null;
                        return;
                    }
                }
                ScanInit();
                boxUpTasks[index].ContainerNumber = this.txtScanNumber.Text;
                var thisShelvesAmount = string.IsNullOrEmpty(this.txtThisShelvesAmount.Text) ? 0 : Convert.ToInt32(this.txtThisShelvesAmount.Text);
                if ((thisShelvesAmount + Convert.ToInt32(codeAndNum[1])) + integerToInt(boxUpTasks[index].BoxUpQty) > boxUpTasks[index].PlanQty) {//不能大于任务计划量  
                    MessageBoxForm.Show(this, "数量不能大于待装量,请扫描正确的条码！", "提示", this.DefaultButtonResults);
                    return;
                }
                this.txtThisShelvesAmount.Text = (thisShelvesAmount + Convert.ToInt32(codeAndNum[1])).ToString();
                this.txtThisShelvesAmount.ReadOnly = false;
            }
        }

        private void txtThisShelvesAmount_TextChanged(object sender, EventArgs e) {
            try {
                var rgx = new Regex(@"^\d+$");
                var tempTxtshippingAmount = sender as TextBox;
                if (tempTxtshippingAmount != null && !string.IsNullOrEmpty(tempTxtshippingAmount.Text)) {
                    var thisNumber = tempTxtshippingAmount.Text;
                    if (!rgx.IsMatch(thisNumber)) {
                        MessageBoxForm.Show(this, "你输入的不是数字！", "提示", this.DefaultButtonResults);
                        this.txtThisShelvesAmount.Text = null;
                        return;
                    }
                    boxUpTasks[index].ThisBoxUpQty = Convert.ToInt32(thisNumber.Trim());
                }
            }
            catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }

        private void btnUpload_Click(object sender, EventArgs e) {
            try {
                if (boxUpTasks != null && boxUpTasks.Count > 0) {
                    var number = boxUpTasks.FindIndex(o => (o.ThisBoxUpQty ?? 0) > 0);
                    if (number == -1) {
                        MessageBoxForm.Show(this, "本次提交内容，全部没有填写装箱量！", "提示", this.DefaultButtonResults);
                        return;
                    }
                    this.IsLoading(true);
                    var result = ServiceManager.BoxUping(boxUpTasks.Where(o => !string.IsNullOrEmpty(o.ContainerNumber) && (o.ThisBoxUpQty ?? 0) > 0).ToList());
                    this.IsLoading(false);
                    if (result.IsSuccess) {
                        MessageBoxForm.Show(this, result.Message, "提示", this.DefaultButtonResults);

                        boxUpTasks = result.Data;
                        if (boxUpTasks.Count == 0) {
                            RasieEditSubmitted();
                            this.Close();
                            return;
                        }
                        this.index = 0;
                        this.btnOver.Enabled = true;
                        this.isInitialize = false;
                        Initialize();
                    }
                    else {
                        MessageBoxForm.Show(this, result.Message, "提示", this.DefaultButtonResults);
                    }
                }
            }
            catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }

        private void btnOver_Click(object sender, EventArgs e) {
            try {
                if (boxUpTasks != null && boxUpTasks.Count > 0) {
                    this.IsLoading(true);
                    var result = ServiceManager.BoxUpingOver(boxUpTasks);
                    this.IsLoading(false);
                    if (result.IsSuccess) {
                        MessageBoxForm.Show(this, result.Message, "提示", this.DefaultButtonResults);
                        RasieEditSubmitted();
                        this.Close();
                    }
                    else {
                        MessageBoxForm.Show(this, result.Message, "提示", this.DefaultButtonResults);
                    }
                }
            }
            catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }
    }
}