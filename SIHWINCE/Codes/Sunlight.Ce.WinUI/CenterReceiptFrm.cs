﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Sunlight.Ce.Controls;
using Sunlight.Ce.Entities.DTO;
using Sunlight.Ce.Service;

namespace Sunlight.Ce.WinUI {
    public partial class CenterReceiptFrm : BaseFrm {
        private List<PartsShippingOrderDTO> partsShippingOrders;
        private List<PartsShippingOrderDTO> partsShippingOrderGroup;

        //grid数据，用来  扫描箱号打钩
        private DataTable dt;

        public CenterReceiptFrm(List<PartsShippingOrderDTO> partsShippingOrderPara) {
            InitializeComponent();
            this.partsShippingOrders = partsShippingOrderPara;
            Init();
        }

        public void Init() {
            this.txtOrderCode.Text = partsShippingOrders[0].Code;
            this.txtOrderCode.Enabled = false;

            var dtCode = this.QetDataTableByCode();
            SetDataGridStyle(this.dgvParts, dtCode);
            this.dgvParts.DataSource = dtCode;

            DataView dv = dtCode.DefaultView;
            dv.Sort = "操作 Asc";
            dtCode = dv.ToTable();

        }

        //datagrid数据源，盘点异常任务
        private DataTable QetDataTableByCode() {
            dt = new DataTable();
            dt.TableName = "单号集合";
            dt.Columns.Add("箱号");
            dt.Columns.Add("操作");

            var rsCaseNumber = partsShippingOrders.GroupBy(o => o.ContainerNumber).ToList();
            groupCaseNumberAndSpartsCode();

            foreach (var st in rsCaseNumber) {
                DataRow dr = dt.NewRow();
                dr["箱号"] = st.Key;

                int index = partsShippingOrderGroup.FindIndex(o => o.ContainerNumber == st.Key);
                if (st.Key == dr["箱号"].ToString()) {
                    //待处理量
                    int pending = partsShippingOrderGroup[index].ShippingAmount - partsShippingOrderGroup[index].ConfirmedAmount;
                    if (pending == 0) {
                        dr["操作"] = "√";
                    }
                    else {
                        dr["操作"] = "";
                    }
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        // 设置datagrid样式
        private void SetDataGridStyle(DataGrid dataGrid, DataTable dataTable) {
            var alternatingColor = SystemColors.ControlDark;//交替行颜色

            dataGrid.BackgroundColor = Color.White;
            dataGrid.RowHeadersVisible = false;

            var dataGridTableStyle = new DataGridTableStyle();
            dataGridTableStyle.MappingName = dataTable.TableName;

            dataGridTableStyle.GridColumnStyles.Add(new DataGridCustomTextBoxColumn() {
                Owner = dataGrid,
                HeaderText = "箱号",
                MappingName = dataTable.Columns[0].ColumnName,
                NullText = "",
                ReadOnly = true,
                Alignment = HorizontalAlignment.Center,
                AlternatingBackColor = alternatingColor,
                Width = 185
            });

            dataGridTableStyle.GridColumnStyles.Add(new DataGridCustomTextBoxColumn() {
                Owner = dataGrid,
                HeaderText = "操作",
                MappingName = dataTable.Columns[1].ColumnName,
                NullText = "",
                ReadOnly = true,
                Alignment = HorizontalAlignment.Center,
                AlternatingBackColor = alternatingColor,
                Width = 50
            });

            dataGrid.TableStyles.Add(dataGridTableStyle);
        }

        private int ScanTicketClear = 0;
        private void txtPartsCode_KeyDown(object sender, KeyEventArgs e) {
            if (ScanTicketClear == 0) {
                this.txtPartsCode.Text = string.Empty;
                ScanTicketClear++;
            }
            if (e.KeyCode == Keys.Enter) {
                ScanTicketClear = 0;
                if (!string.IsNullOrEmpty(this.txtPartsCode.Text)) {
                    foreach (DataRow dr in dt.Rows) {
                        if (this.txtPartsCode.Text == dr["箱号"].ToString()) {
                            dr["操作"] = "√";
                        }
                    }
                    this.dgvParts.DataSource = dt;

                    int index = partsShippingOrders.FindIndex(o => o.ContainerNumber == this.txtPartsCode.Text && o.ShippingAmount > o.ConfirmedAmount);
                    if (index == -1) {
                        MessageBoxForm.Show(this, "扫描箱号错误！", "提示",  this.DefaultButtonResults);
                        return;
                    }
                    while (index >= 0) {
                        partsShippingOrders[index].ConfirmedAmount = partsShippingOrders[index].ShippingAmount;

                        index = partsShippingOrders.FindIndex(o => o.ContainerNumber == this.txtPartsCode.Text && o.ShippingAmount > o.ConfirmedAmount);
                    }
                }

                this.newFrm(this.txtPartsCode.Text);
            }
        }

        public void newFrm(string newCaseNo) {
            groupCaseNumberAndSpartsCode();
            foreach (DataRow dr in dt.Rows) {
                var prpdCaseNo = partsShippingOrderGroup.FirstOrDefault(o => o.ContainerNumber == newCaseNo);
                if (newCaseNo == dr["箱号"].ToString()) {
                    if (prpdCaseNo != null && prpdCaseNo.ShippingAmount == prpdCaseNo.ConfirmedAmount) {
                        dr["操作"] = "√";
                    }
                    else {
                        dr["操作"] = "";
                    }
                }
            }
        }

        private void groupCaseNumberAndSpartsCode() {
            partsShippingOrderGroup = (from p in partsShippingOrders
                                       group p by new { p.ContainerNumber, p.PartsCode, p.PartsId, p.PartsName } into g
                                       select new PartsShippingOrderDTO {
                                           ContainerNumber = g.Key.ContainerNumber,
                                           PartsCode = g.Key.PartsCode,
                                           PartsName = g.Key.PartsName,
                                           ShippingAmount = g.Sum(p => p.ShippingAmount),
                                           ConfirmedAmount = g.Sum(p => p.ConfirmedAmount)
                                       }).ToList();
        }

        private void btnUpload_Click(object sender, EventArgs e) {
            foreach (DataRow dr in dt.Rows) {
                if (!string.IsNullOrEmpty(dr["操作"].ToString())) {
                    try {
                        this.IsLoading(true);
                        var result = ServiceManager.CenterConfirmReceive(partsShippingOrders);
                        this.IsLoading(false);
                        if (result.IsSuccess) {
                            MessageBoxForm.Show(this, result.Message, "提示", this.DefaultButtonResults);
                            RasieEditSubmitted();
                            this.Close();
                        }
                        else {
                            MessageBoxForm.Show(this, result.Message, "提示", this.DefaultButtonResults);
                        }
                    }
                    catch (Exception ex) {
                        Cursor.Current = Cursors.Default;
                        MessageBoxForm.Show(this, ex.Message, "提示",  this.DefaultButtonResults);
                    }
                    return;
                }
            }
            MessageBoxForm.Show(this, "未收货！", "提示",  this.DefaultButtonResults);
        }

        private void dgvParts_Click(object sender, EventArgs e) {
            if (dgvParts.VisibleRowCount == 0) {
                return;
            }
            dgvParts.Select(dgvParts.CurrentRowIndex);
            if (dgvParts.DataSource != null && dgvParts.CurrentRowIndex != -1) {
                foreach (DataRow dr in dt.Rows) {
                    if (dgvParts[dgvParts.CurrentRowIndex, 0].ToString() == dr["箱号"].ToString() && dr["操作"].ToString() == "√") {
                        dr["操作"] = "";

                        int index = partsShippingOrders.FindIndex(o => o.ContainerNumber == dgvParts[dgvParts.CurrentRowIndex, 0].ToString() && o.ConfirmedAmount > 0);
                        while (index >= 0) {
                            partsShippingOrders[index].ConfirmedAmount = 0;

                            index = partsShippingOrders.FindIndex(o => o.ContainerNumber == dgvParts[dgvParts.CurrentRowIndex, 0].ToString() && o.ConfirmedAmount > 0);
                        }
                        break;
                    }
                    else if (dgvParts[dgvParts.CurrentRowIndex, 0].ToString() == dr["箱号"].ToString()) {
                        dr["操作"] = "√";

                        int index = partsShippingOrders.FindIndex(o => o.ContainerNumber == dgvParts[dgvParts.CurrentRowIndex, 0].ToString() && o.ShippingAmount > o.ConfirmedAmount);
                        while (index >= 0) {
                            partsShippingOrders[index].ConfirmedAmount = partsShippingOrders[index].ShippingAmount;

                            index = partsShippingOrders.FindIndex(o => o.ContainerNumber == dgvParts[dgvParts.CurrentRowIndex, 0].ToString() && o.ShippingAmount > o.ConfirmedAmount);
                        }
                        break;
                    }
                }
            }
        }
    }
}