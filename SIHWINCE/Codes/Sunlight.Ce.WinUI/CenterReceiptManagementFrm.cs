﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Sunlight.Ce.Controls;
using Sunlight.Ce.Entities.DTO;
using Sunlight.Ce.Service;
using Sunlight.Ce.Utils.Extends;

namespace Sunlight.Ce.WinUI {
    public partial class CenterReceiptManagementFrm : BaseFrm {
        private List<PartsShippingOrderDTO> partsShippingOrders;
        private bool isInitialize;

        public CenterReceiptManagementFrm() {
            InitializeComponent();

            Initialize();
        }

        public void SelectDataGridRow(DataGrid dataGrid) {
            if (dataGrid.VisibleRowCount == 0)
                return;
            dataGrid.Select(dataGrid.CurrentRowIndex);
            if (dataGrid.DataSource != null && dataGrid.CurrentRowIndex != -1) {
                string code = dataGrid[dataGrid.CurrentRowIndex, 0].ToString();
                var crf = new CenterReceiptFrm(partsShippingOrders.Where(o => o.Code.ToLower() == code.ToLower()).ToList());
                crf.ShowDialog();
                this.BeginRefresh();
            }
        }

        private void SetDataGridStyle(DataGrid dataGrid, DataTable dataTable) {
            Color alternatingColor = SystemColors.ControlDark; //交替行颜色

            dataGrid.BackgroundColor = Color.White;
            dataGrid.RowHeadersVisible = false;

            var dataGridTableStyle = new DataGridTableStyle();
            dataGridTableStyle.MappingName = dataTable.TableName;

            dataGridTableStyle.GridColumnStyles.Add(new DataGridCustomTextBoxColumn {
                Owner = dataGrid,
                HeaderText = "单号",
                MappingName = dataTable.Columns[0].ColumnName,
                NullText = "",
                ReadOnly = true,
                Alignment = HorizontalAlignment.Center,
                AlternatingBackColor = alternatingColor,
                Width = 230
            });

            dataGrid.TableStyles.Add(dataGridTableStyle);
        }

        private void dgvReceipt_DoubleClick(object sender, EventArgs e) {
            this.SelectDataGridRow(this.dgvReceipt);
        }

        private void dgvReceipt_GotFocus(object sender, EventArgs e) {
            try {
                this.SelectRowColor(this.dgvReceipt);
            }
            catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }

        private void dgvReceipt_LostFocus(object sender, EventArgs e) {
            try {
                if (this.dgvReceipt.CurrentRowIndex >= 0)
                    this.dgvReceipt.UnSelect(this.dgvReceipt.CurrentRowIndex);
            }
            catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }

        private void dgvReceipt_KeyDown(object sender, KeyEventArgs e) {
            try {
                switch (e.KeyCode) {
                    case Keys.Enter:
                        SelectDataGridRow(this.dgvReceipt);
                        break;
                    case Keys.Up:
                        this.SelectRowColor(this.dgvReceipt);
                        break;
                    case Keys.Down:
                        this.SelectRowColor(this.dgvReceipt);
                        break;
                }
            }
            catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }

        private void BeginRefresh() {
            //先清空缓存
            this.txtScanCode.Text = string.Empty;

            DataTable dt = this.SetDataSource(null);
            this.dgvReceipt.TableStyles.Clear();
            this.SetDataGridStyle(this.dgvReceipt, dt);
            this.dgvReceipt.DataSource = dt;
        }

        private DataTable SetDataSource(List<PartsShippingOrderDTO> data) {
            var dd = new DataTable();
            dd.TableName = "dgvReceipt";
            dd.Columns.Add("单号");

            if (data == null)
                return dd;
            foreach (var t in data.GroupBy(o => o.Code).OrderBy(o => o.Key)) {
                DataRow dr = dd.NewRow();
                dr["单号"] = t.Key;
                dd.Rows.Add(dr);
            }
            return dd;
        }

        //初始化方法
        private void Initialize() {
            if (!this.isInitialize) {
                this.isInitialize = true;

                var dd = new DataTable();
                dd.TableName = "dgvReceipt";
                dd.Columns.Add("单号");
                this.SetDataGridStyle(this.dgvReceipt, dd);
                this.dgvReceipt.DataSource = dd;
            }
        }

        private int ScanTicketClear = 0;
        private void txtScanCode_KeyDown(object sender, KeyEventArgs e) {
            if (ScanTicketClear == 0) {
                this.txtScanCode.Text = string.Empty;
                ScanTicketClear++;
            }
            if (e.KeyCode == Keys.Enter) {
                ScanTicketClear = 0;
                this.IsLoading(true);
                MessageInfo<List<PartsShippingOrderDTO>> messageInfo = ServiceManager.GetShippingDetial(txtScanCode.Text.Trim());
                this.IsLoading(false);

                if (messageInfo.IsSuccess)
                    if (messageInfo.Data != null && messageInfo.Data.Any()) {
                        partsShippingOrders = messageInfo.Data;
                        if (partsShippingOrders == null)
                            partsShippingOrders = new List<PartsShippingOrderDTO>();
                        //界面参数
                        this.dgvReceipt.TableStyles.Clear();
                        DataTable dt = this.SetDataSource(partsShippingOrders);
                        this.SetDataGridStyle(this.dgvReceipt, dt);
                        this.dgvReceipt.DataSource = dt;

                        if (partsShippingOrders.GroupBy(o => o.Code).ToList().Count == 1) {
                            var crf = new CenterReceiptFrm(partsShippingOrders);
                            crf.ShowDialog();
                            this.BeginRefresh();
                            return;
                        }
                    }
                    else {
                        this.dgvReceipt.TableStyles.Clear();
                        DataTable dt = this.SetDataSource(new List<PartsShippingOrderDTO>());
                        this.SetDataGridStyle(this.dgvReceipt, dt);
                        this.dgvReceipt.DataSource = dt;
                        MessageBoxForm.Show(this, "无查询结果", "提示", this.DefaultButtonResults);
                    }
                else
                    MessageBoxForm.Show(this, messageInfo.Message, "提示", this.DefaultButtonResults);
            }
        }
    }
}