﻿namespace Sunlight.Ce.WinUI {
    partial class CenterShelvesManagementFrm {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent() {
            this.txtSparePartCode = new System.Windows.Forms.TextBox();
            this.lblSparePartCode = new System.Windows.Forms.Label();
            this.txtRecommendLocation = new System.Windows.Forms.TextBox();
            this.txtSparePartsName = new System.Windows.Forms.TextBox();
            this.lblRecommendLocation = new System.Windows.Forms.Label();
            this.lblSparePartsName = new System.Windows.Forms.Label();
            this.txtScanLocation = new System.Windows.Forms.TextBox();
            this.lblScanLocation = new System.Windows.Forms.Label();
            this.txtThisShelvesAmount = new System.Windows.Forms.TextBox();
            this.lblThisShelvesAmount = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labSprit = new System.Windows.Forms.Label();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtSparePartCode
            // 
            this.txtSparePartCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtSparePartCode.Location = new System.Drawing.Point(69, 5);
            this.txtSparePartCode.Name = "txtSparePartCode";
            this.txtSparePartCode.Size = new System.Drawing.Size(169, 26);
            this.txtSparePartCode.TabIndex = 43;
            this.txtSparePartCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSparePartCode_KeyDown);
            // 
            // lblSparePartCode
            // 
            this.lblSparePartCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblSparePartCode.Location = new System.Drawing.Point(0, 9);
            this.lblSparePartCode.Name = "lblSparePartCode";
            this.lblSparePartCode.Size = new System.Drawing.Size(92, 20);
            this.lblSparePartCode.Text = "扫描配件";
            // 
            // txtRecommendLocation
            // 
            this.txtRecommendLocation.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRecommendLocation.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtRecommendLocation.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtRecommendLocation.Location = new System.Drawing.Point(69, 77);
            this.txtRecommendLocation.Name = "txtRecommendLocation";
            this.txtRecommendLocation.ReadOnly = true;
            this.txtRecommendLocation.Size = new System.Drawing.Size(167, 26);
            this.txtRecommendLocation.TabIndex = 49;
            // 
            // txtSparePartsName
            // 
            this.txtSparePartsName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSparePartsName.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtSparePartsName.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtSparePartsName.Location = new System.Drawing.Point(69, 40);
            this.txtSparePartsName.Name = "txtSparePartsName";
            this.txtSparePartsName.ReadOnly = true;
            this.txtSparePartsName.Size = new System.Drawing.Size(167, 26);
            this.txtSparePartsName.TabIndex = 48;
            // 
            // lblRecommendLocation
            // 
            this.lblRecommendLocation.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblRecommendLocation.Location = new System.Drawing.Point(-3, 79);
            this.lblRecommendLocation.Name = "lblRecommendLocation";
            this.lblRecommendLocation.Size = new System.Drawing.Size(95, 27);
            this.lblRecommendLocation.Text = "推荐货位";
            // 
            // lblSparePartsName
            // 
            this.lblSparePartsName.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblSparePartsName.Location = new System.Drawing.Point(0, 44);
            this.lblSparePartsName.Name = "lblSparePartsName";
            this.lblSparePartsName.Size = new System.Drawing.Size(95, 27);
            this.lblSparePartsName.Text = "零件名称";
            // 
            // txtScanLocation
            // 
            this.txtScanLocation.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtScanLocation.Location = new System.Drawing.Point(69, 117);
            this.txtScanLocation.Name = "txtScanLocation";
            this.txtScanLocation.Size = new System.Drawing.Size(169, 26);
            this.txtScanLocation.TabIndex = 53;
            this.txtScanLocation.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtScanLocation_KeyDown);
            // 
            // lblScanLocation
            // 
            this.lblScanLocation.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblScanLocation.Location = new System.Drawing.Point(-3, 119);
            this.lblScanLocation.Name = "lblScanLocation";
            this.lblScanLocation.Size = new System.Drawing.Size(92, 20);
            this.lblScanLocation.Text = "扫描货位";
            // 
            // txtThisShelvesAmount
            // 
            this.txtThisShelvesAmount.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtThisShelvesAmount.Location = new System.Drawing.Point(69, 195);
            this.txtThisShelvesAmount.Name = "txtThisShelvesAmount";
            this.txtThisShelvesAmount.Size = new System.Drawing.Size(84, 26);
            this.txtThisShelvesAmount.TabIndex = 56;
            this.txtThisShelvesAmount.TextChanged += new System.EventHandler(this.txtThisShelvesAmount_TextChanged);
            // 
            // lblThisShelvesAmount
            // 
            this.lblThisShelvesAmount.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblThisShelvesAmount.Location = new System.Drawing.Point(0, 199);
            this.lblThisShelvesAmount.Name = "lblThisShelvesAmount";
            this.lblThisShelvesAmount.Size = new System.Drawing.Size(66, 24);
            this.lblThisShelvesAmount.Text = "数量";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.label1.Location = new System.Drawing.Point(-3, 158);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 24);
            this.label1.Text = "待上架量";
            // 
            // labSprit
            // 
            this.labSprit.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.labSprit.Location = new System.Drawing.Point(83, 156);
            this.labSprit.Name = "labSprit";
            this.labSprit.Size = new System.Drawing.Size(60, 28);
            this.labSprit.Text = "0";
            this.labSprit.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnSubmit
            // 
            this.btnSubmit.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnSubmit.Location = new System.Drawing.Point(131, 234);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(102, 42);
            this.btnSubmit.TabIndex = 61;
            this.btnSubmit.Text = "上传";
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // CenterShelvesManagementFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 295);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.labSprit);
            this.Controls.Add(this.txtThisShelvesAmount);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblThisShelvesAmount);
            this.Controls.Add(this.txtScanLocation);
            this.Controls.Add(this.lblScanLocation);
            this.Controls.Add(this.txtRecommendLocation);
            this.Controls.Add(this.txtSparePartsName);
            this.Controls.Add(this.lblRecommendLocation);
            this.Controls.Add(this.lblSparePartsName);
            this.Controls.Add(this.txtSparePartCode);
            this.Controls.Add(this.lblSparePartCode);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CenterShelvesManagementFrm";
            this.Text = "上架";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtSparePartCode;
        private System.Windows.Forms.Label lblSparePartCode;
        private System.Windows.Forms.TextBox txtRecommendLocation;
        private System.Windows.Forms.TextBox txtSparePartsName;
        private System.Windows.Forms.Label lblRecommendLocation;
        private System.Windows.Forms.Label lblSparePartsName;
        private System.Windows.Forms.TextBox txtScanLocation;
        private System.Windows.Forms.Label lblScanLocation;
        private System.Windows.Forms.TextBox txtThisShelvesAmount;
        private System.Windows.Forms.Label lblThisShelvesAmount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labSprit;
        private System.Windows.Forms.Button btnSubmit;
    }
}