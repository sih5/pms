﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Sunlight.Ce.Controls;
using Sunlight.Ce.Entities.DTO;
using Sunlight.Ce.Service;
using Sunlight.Ce.Utils.Extends;

namespace Sunlight.Ce.WinUI {
    public partial class CenterShelvesManagementFrm : BaseFrm {
        private List<CenterPartsShelvesDTO> centerPartsShelves;
        private int index = 0;
        public CenterShelvesManagementFrm() {
            InitializeComponent();

            this.txtSparePartCode.Focus();
        }
        private int ScanTicketClear = 0;
        private void txtSparePartCode_KeyDown(object sender, KeyEventArgs e) {
            if (ScanTicketClear == 0) {
                this.txtSparePartCode.Text = string.Empty;
                ScanTicketClear++;
            }
            if (e.KeyCode == Keys.Enter) {
                ScanTicketClear = 0;
                string[] codeAndNum = this.txtSparePartCode.Text.Split('|');
                if (codeAndNum.Length < 2) {
                    MessageBoxForm.Show(this, "扫描配件条码错误！", "提示", this.DefaultButtonResults);
                    this.txtSparePartCode.Text = null;
                    return;
                }

                this.IsLoading(true);
                MessageInfo<List<CenterPartsShelvesDTO>> messageInfo = ServiceManager.GetCenterShelvesTask(codeAndNum[0]);
                this.IsLoading(false);

                if (messageInfo.IsSuccess)
                    if (messageInfo.Data != null && messageInfo.Data.Any()) {
                        centerPartsShelves = messageInfo.Data;
                        if (centerPartsShelves == null)
                            centerPartsShelves = new List<CenterPartsShelvesDTO>();
                        else {
                            this.txtSparePartsName.Text = centerPartsShelves[index].SparePartName;
                            this.txtRecommendLocation.Text = centerPartsShelves[index].RecommendWarehouseAreaCode;
                            this.labSprit.Text = centerPartsShelves[index].Quantity.ToString();
                            this.txtScanLocation.Focus();
                        }
                    }
                    else {
                        MessageBoxForm.Show(this, "无查询结果", "提示", this.DefaultButtonResults);
                    }
                else
                    MessageBoxForm.Show(this, messageInfo.Message, "提示", this.DefaultButtonResults);
            }
        }

        private void txtScanLocation_KeyDown(object sender, KeyEventArgs e) {
            if (ScanTicketClear == 0) {
                this.txtScanLocation.Text = string.Empty;
                ScanTicketClear++;
            }
            if (e.KeyCode == Keys.Enter) {
                ScanTicketClear = 0;

                centerPartsShelves[index].DestWarehouseAreaCode = this.txtScanLocation.Text;
                this.txtThisShelvesAmount.Focus();
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e) {
            try {
                if (centerPartsShelves != null && centerPartsShelves.Count > 0) {
                    var number = centerPartsShelves.FindIndex(o => o.ThisShelvesAmount > 0);
                    if (number == -1) {
                        MessageBoxForm.Show(this, "本次提交内容，全部没有填写本次上架量！", "提示", this.DefaultButtonResults);
                        return;
                    }
                    this.IsLoading(true);
                    var result = ServiceManager.CenterShelvesing(centerPartsShelves);
                    this.IsLoading(false);
                    if (result.IsSuccess) {
                        MessageBoxForm.Show(this, result.Message, "提示", this.DefaultButtonResults);
                        this.centerPartsShelves = null;
                        this.index = 0;
                        this.txtSparePartCode.Text = string.Empty;
                        this.txtSparePartsName.Text = string.Empty;
                        this.txtSparePartCode.Text = string.Empty;
                        this.txtRecommendLocation.Text = string.Empty;
                        this.txtScanLocation.Text = string.Empty;
                        this.txtThisShelvesAmount.Text = string.Empty;
                        this.labSprit.Text = string.Empty;
                    }
                    else {
                        MessageBoxForm.Show(this, result.Message, "提示", this.DefaultButtonResults);
                    }
                }
            }
            catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }

        private void txtThisShelvesAmount_TextChanged(object sender, EventArgs e) {
            try {
                var rgx = new Regex(@"^\d+$");
                var tempTxtshippingAmount = sender as TextBox;
                if (tempTxtshippingAmount != null && !string.IsNullOrEmpty(tempTxtshippingAmount.Text)) {
                    var thisNumber = tempTxtshippingAmount.Text;
                    if (!rgx.IsMatch(thisNumber)) {
                        MessageBoxForm.Show(this, "你输入的不是数字！", "提示", this.DefaultButtonResults);
                        this.txtThisShelvesAmount.Text = null;
                        return;
                    }
                    if (Convert.ToInt32(thisNumber.Trim()) > centerPartsShelves[index].Quantity) {//不能大于任务计划量  
                        MessageBoxForm.Show(this, "本次上架量不能大于待上架量！", "提示", this.DefaultButtonResults);
                        this.txtThisShelvesAmount.Text = centerPartsShelves[index].Quantity.ToString();
                        return;
                    }
                    centerPartsShelves[index].ThisShelvesAmount = Convert.ToInt32(thisNumber.Trim());
                }
            }
            catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }
    }
}