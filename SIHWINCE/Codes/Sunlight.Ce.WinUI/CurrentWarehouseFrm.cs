﻿using System;
using System.Collections.Generic;
using Sunlight.Ce.Controls;
using Sunlight.Ce.Entities;
using Sunlight.Ce.Service;
using Sunlight.Ce.Utils;

namespace Sunlight.Ce.WinUI {
    public partial class CurrentWarehouseFrm : BaseFrm {
        private readonly SystemMenuFrm systemMenuFrm = new SystemMenuFrm();
        private readonly string enterpriseCode;
        private readonly string userName;

        private void btnRefresh_Click(object sender, EventArgs e) {
            this.LoadComboBoxDataSource();
        }

        private void CurrentWarehouseFrm_Load(object sender, EventArgs e) {
            this.LoadComboBoxDataSource();
        }

        private void LoadComboBoxDataSource() {
            this.IsLoading(true);
            var messageInfo = ServiceManager.GetWarehouseOperators();

            if(messageInfo.Data != null && messageInfo.Data.Count > 0) {
                SetDataSourceBinding(messageInfo.Data);

                //如果已经选择仓库，默认选择设置该仓库为选择状态
                var currentWarehouse = HelperUtil.Instance.GetCacheWarehouse();
                this.cmbWarehouse.SelectedValue = currentWarehouse.WarehouseId;
            }
            else if(messageInfo.Data == null || messageInfo.Data.Count <= 0) {
                SetDataSourceBinding(new List<WarehouseOperator>());
                ServiceManager.InitializeWarehouse(null);
            }

            this.IsLoading(false);
        }

        private void SetDataSourceBinding(List<WarehouseOperator> warehouseOperators) {
            this.cmbWarehouse.DataSource = warehouseOperators;
            this.cmbWarehouse.DisplayMember = "WarehouseName";
            this.cmbWarehouse.ValueMember = "WarehouseId";
        }

        private void cmbWarehouse_SelectedIndexChanged(object sender, EventArgs e) {
            var selected = this.cmbWarehouse.SelectedItem as WarehouseOperator;

            if(selected != null)
                ServiceManager.InitializeWarehouse(selected);
        }

        public CurrentWarehouseFrm(string enterpriseCode, string userName) {
            InitializeComponent();
            try {
                this.enterpriseCode = enterpriseCode;
                this.userName = userName;
                this.Load -= this.CurrentWarehouseFrm_Load;
                this.Load += this.CurrentWarehouseFrm_Load;
                this.cmbWarehouse.SelectedIndexChanged -= this.cmbWarehouse_SelectedIndexChanged;
                this.cmbWarehouse.SelectedIndexChanged += this.cmbWarehouse_SelectedIndexChanged;
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch(Exception) {
            }
        }

        private void btnStartUse_Click(object sender, EventArgs e) {
            if(this.cmbWarehouse.SelectedItem == null) {
                MessageBoxForm.Show(this, "使用该系统，必须选择仓库", "提示", this.DefaultButtonResults);
                return;
            }

            var selectedItem = this.cmbWarehouse.SelectedItem as WarehouseOperator;
            if(selectedItem == null)
                return;
            //如果能进行开始使用，就进行缓存
            HelperUtil.Instance.CachCurrentWarehouse(enterpriseCode, userName, selectedItem.WarehouseId);
            //获取打印机配置，无论成功或者错误，不影响其他操作
            //ServiceManager.GetPdaPrinterByWarehouseId(selectedItem.WarehouseId);
            systemMenuFrm.Owner = this;
            systemMenuFrm.ShowDialog();
        }

    }
}