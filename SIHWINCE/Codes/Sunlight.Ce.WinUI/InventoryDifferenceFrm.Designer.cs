﻿namespace Sunlight.Ce.WinUI {
    partial class InventoryDifferenceFrm {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent() {
            this.txtScanSparePartCode = new System.Windows.Forms.TextBox();
            this.lblSparePartCode = new System.Windows.Forms.Label();
            this.txtScanLocation = new System.Windows.Forms.TextBox();
            this.lblScanLocation = new System.Windows.Forms.Label();
            this.txtSparePartsCode = new System.Windows.Forms.TextBox();
            this.lblSparePartsCode = new System.Windows.Forms.Label();
            this.txtStorageAfterInventory = new System.Windows.Forms.TextBox();
            this.lblThisShelvesAmount = new System.Windows.Forms.Label();
            this.btnNext = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtScanSparePartCode
            // 
            this.txtScanSparePartCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtScanSparePartCode.Location = new System.Drawing.Point(64, 67);
            this.txtScanSparePartCode.Name = "txtScanSparePartCode";
            this.txtScanSparePartCode.Size = new System.Drawing.Size(169, 26);
            this.txtScanSparePartCode.TabIndex = 95;
            this.txtScanSparePartCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtScanSparePartCode_KeyDown);
            // 
            // lblSparePartCode
            // 
            this.lblSparePartCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblSparePartCode.Location = new System.Drawing.Point(-5, 71);
            this.lblSparePartCode.Name = "lblSparePartCode";
            this.lblSparePartCode.Size = new System.Drawing.Size(92, 20);
            this.lblSparePartCode.Text = "扫描配件";
            // 
            // txtScanLocation
            // 
            this.txtScanLocation.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtScanLocation.Location = new System.Drawing.Point(64, 17);
            this.txtScanLocation.Name = "txtScanLocation";
            this.txtScanLocation.Size = new System.Drawing.Size(169, 26);
            this.txtScanLocation.TabIndex = 94;
            this.txtScanLocation.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtScanLocation_KeyDown);
            // 
            // lblScanLocation
            // 
            this.lblScanLocation.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblScanLocation.Location = new System.Drawing.Point(-5, 21);
            this.lblScanLocation.Name = "lblScanLocation";
            this.lblScanLocation.Size = new System.Drawing.Size(92, 20);
            this.lblScanLocation.Text = "扫描货位";
            // 
            // txtSparePartsCode
            // 
            this.txtSparePartsCode.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSparePartsCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtSparePartsCode.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtSparePartsCode.Location = new System.Drawing.Point(66, 115);
            this.txtSparePartsCode.Name = "txtSparePartsCode";
            this.txtSparePartsCode.ReadOnly = true;
            this.txtSparePartsCode.Size = new System.Drawing.Size(167, 26);
            this.txtSparePartsCode.TabIndex = 101;
            // 
            // lblSparePartsCode
            // 
            this.lblSparePartsCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblSparePartsCode.Location = new System.Drawing.Point(-6, 117);
            this.lblSparePartsCode.Name = "lblSparePartsCode";
            this.lblSparePartsCode.Size = new System.Drawing.Size(95, 27);
            this.lblSparePartsCode.Text = "零件编号";
            // 
            // txtStorageAfterInventory
            // 
            this.txtStorageAfterInventory.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtStorageAfterInventory.Location = new System.Drawing.Point(67, 157);
            this.txtStorageAfterInventory.Name = "txtStorageAfterInventory";
            this.txtStorageAfterInventory.Size = new System.Drawing.Size(84, 26);
            this.txtStorageAfterInventory.TabIndex = 104;
            this.txtStorageAfterInventory.TextChanged += new System.EventHandler(this.txtStorageAfterInventory_TextChanged);
            // 
            // lblThisShelvesAmount
            // 
            this.lblThisShelvesAmount.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblThisShelvesAmount.Location = new System.Drawing.Point(1, 159);
            this.lblThisShelvesAmount.Name = "lblThisShelvesAmount";
            this.lblThisShelvesAmount.Size = new System.Drawing.Size(60, 25);
            this.lblThisShelvesAmount.Text = "数量";
            // 
            // btnNext
            // 
            this.btnNext.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnNext.Location = new System.Drawing.Point(150, 211);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(74, 35);
            this.btnNext.TabIndex = 106;
            this.btnNext.Text = "确认";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // InventoryDifferenceFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 295);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.txtStorageAfterInventory);
            this.Controls.Add(this.lblThisShelvesAmount);
            this.Controls.Add(this.txtSparePartsCode);
            this.Controls.Add(this.lblSparePartsCode);
            this.Controls.Add(this.txtScanSparePartCode);
            this.Controls.Add(this.lblSparePartCode);
            this.Controls.Add(this.txtScanLocation);
            this.Controls.Add(this.lblScanLocation);
            this.Name = "InventoryDifferenceFrm";
            this.Text = "新增盘点单清单";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtScanSparePartCode;
        private System.Windows.Forms.Label lblSparePartCode;
        private System.Windows.Forms.TextBox txtScanLocation;
        private System.Windows.Forms.Label lblScanLocation;
        private System.Windows.Forms.TextBox txtSparePartsCode;
        private System.Windows.Forms.Label lblSparePartsCode;
        private System.Windows.Forms.TextBox txtStorageAfterInventory;
        private System.Windows.Forms.Label lblThisShelvesAmount;
        private System.Windows.Forms.Button btnNext;
    }
}