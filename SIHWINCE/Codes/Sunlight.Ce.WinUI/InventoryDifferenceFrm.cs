﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Sunlight.Ce.Entities.DTO;
using Sunlight.Ce.Controls;
using System.Text.RegularExpressions;

namespace Sunlight.Ce.WinUI {
    public partial class InventoryDifferenceFrm : BaseFrm {
        private InventoryingFrm inventoryingFrm;
        private List<InventoryDifferenceDTO> differenceDetails = new List<InventoryDifferenceDTO>();
        public InventoryDifferenceFrm(InventoryingFrm frm) {
            InitializeComponent();

            inventoryingFrm = frm;
        }

        private void btnNext_Click(object sender, EventArgs e) {
            foreach (var item in differenceDetails) {
                inventoryingFrm.partsInventoryBills.Add(new PartsInventoryBillDTO() {
                    Id = inventoryingFrm.partsInventoryBills.First(o => !string.IsNullOrEmpty(o.Code)).Id,
                    SparePartCode = item.SparePartCode,
                    WarehouseAreaCode = item.WarehouseAreaCode,
                    SIHCodes = item.SIHCodes,
                    StorageAfterInventory = item.Quantity
                });
            }
            this.Close();
        }

        private void txtScanSparePartCode_KeyDown(object sender, KeyEventArgs e) {
            if (ScanTicketClear == 0) {
                this.txtScanSparePartCode.Text = string.Empty;
                ScanTicketClear++;
            }
            if (e.KeyCode == Keys.Enter) {
                ScanTicketClear = 0;
                if (string.IsNullOrEmpty(this.txtScanLocation.Text)) {
                    MessageBoxForm.Show(this, "请先扫描货位！", "提示", this.DefaultButtonResults);
                    this.txtScanSparePartCode.Text = null;
                } else {
                    string[] codeAndNum = this.txtScanSparePartCode.Text.Split('|');
                    if (codeAndNum.Length < 2) {
                        MessageBoxForm.Show(this, "扫描配件条码错误！", "提示", this.DefaultButtonResults);
                        this.txtScanSparePartCode.Text = null;
                        return;
                    }

                    var detailSihCodes = inventoryingFrm.partsInventoryBills.FirstOrDefault(o => o.WarehouseAreaCode == this.txtScanLocation.Text
                        && o.SparePartCode == codeAndNum[0]);
                    if (detailSihCodes != null) {
                        MessageBoxForm.Show(this, string.Format("货位{0}备件编号{1}，已存在当前盘点任务中，无法新增清单",
                            this.txtScanLocation.Text, codeAndNum[0]), "提示", this.DefaultButtonResults);
                        this.txtScanSparePartCode.Text = null;
                        return;
                    }

                    var differenceDetail = differenceDetails.FirstOrDefault(o => o.WarehouseAreaCode == this.txtScanLocation.Text && o.SparePartCode == codeAndNum[0]);
                    if (differenceDetail != null) {
                        differenceDetail.Quantity += Convert.ToInt32(codeAndNum[1]);
                        differenceDetail.SIHCodes.Add(this.txtScanSparePartCode.Text);
                    } else {
                        differenceDetail = new InventoryDifferenceDTO() {
                            WarehouseAreaCode = this.txtScanLocation.Text,
                            SparePartCode = codeAndNum[0],
                            Quantity = Convert.ToInt32(codeAndNum[1]),
                            SIHCodes = new List<string>() { this.txtScanSparePartCode.Text }
                        };
                        differenceDetails.Add(differenceDetail);
                    }
                    this.txtSparePartsCode.Text = codeAndNum[0];
                    this.txtStorageAfterInventory.Text = differenceDetail.Quantity.ToString();
                }
            }
        }

        private int ScanTicketClear = 0;
        private void txtScanLocation_KeyDown(object sender, KeyEventArgs e) {
            if (ScanTicketClear == 0) {
                this.txtScanLocation.Text = string.Empty;
                ScanTicketClear++;
            }
            if (e.KeyCode == Keys.Enter) {
                ScanTicketClear = 0;

                this.txtScanSparePartCode.Focus();
            }
        }

        private void txtStorageAfterInventory_TextChanged(object sender, EventArgs e) {
            try {
                var rgx = new Regex(@"^\d+$");
                var tempTxtshippingAmount = sender as TextBox;
                if (tempTxtshippingAmount != null && !string.IsNullOrEmpty(tempTxtshippingAmount.Text)) {
                    var thisNumber = tempTxtshippingAmount.Text;
                    if (!rgx.IsMatch(thisNumber)) {
                        MessageBoxForm.Show(this, "你输入的不是数字！", "提示", this.DefaultButtonResults);
                        this.txtStorageAfterInventory.Text = null;
                        return;
                    }

                    var differenceDetail = differenceDetails.FirstOrDefault(o => o.WarehouseAreaCode == this.txtScanLocation.Text && o.SparePartCode == this.txtSparePartsCode.Text);
                    differenceDetail.Quantity = Convert.ToInt32(thisNumber.Trim());
                }
            } catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }
    }
}