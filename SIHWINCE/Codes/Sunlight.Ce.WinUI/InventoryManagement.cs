﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Sunlight.Ce.Controls;
using Sunlight.Ce.Entities.DTO;
using Sunlight.Ce.Service;
using Sunlight.Ce.Utils.Extends;

namespace Sunlight.Ce.WinUI {
    public partial class InventoryManagement : BaseFrm {
        private List<PartsInventoryBillDTO> partsInventoryBills;
        private bool isInitialize;
        public InventoryManagement() {
            InitializeComponent();

            Initialize();
        }

        public void SelectDataGridRow(DataGrid dataGrid) {
            if (dataGrid.VisibleRowCount == 0)
                return;
            dataGrid.Select(dataGrid.CurrentRowIndex);
            if (dataGrid.DataSource != null && dataGrid.CurrentRowIndex != -1) {
                string code = dataGrid[dataGrid.CurrentRowIndex, 0].ToString();
                var iof = new InventoryingFrm(partsInventoryBills.Where(o => o.Code.ToLower() == code.ToLower()).ToList());
                iof.ShowDialog();
                this.BeginRefresh();
            }
        }

        private void SetDataGridStyle(DataGrid dataGrid, DataTable dataTable) {
            Color alternatingColor = SystemColors.ControlDark; //交替行颜色

            dataGrid.BackgroundColor = Color.White;
            dataGrid.RowHeadersVisible = false;

            var dataGridTableStyle = new DataGridTableStyle();
            dataGridTableStyle.MappingName = dataTable.TableName;

            dataGridTableStyle.GridColumnStyles.Add(new DataGridCustomTextBoxColumn {
                Owner = dataGrid,
                HeaderText = "单号",
                MappingName = dataTable.Columns[0].ColumnName,
                NullText = "",
                ReadOnly = true,
                Alignment = HorizontalAlignment.Center,
                AlternatingBackColor = alternatingColor,
                Width = 180
            });

            dataGridTableStyle.GridColumnStyles.Add(new DataGridCustomTextBoxColumn {
                Owner = dataGrid,
                HeaderText = "发起人",
                MappingName = dataTable.Columns[1].ColumnName,
                NullText = "",
                ReadOnly = true,
                Alignment = HorizontalAlignment.Center,
                AlternatingBackColor = alternatingColor,
                Width = 60
            });

            dataGrid.TableStyles.Add(dataGridTableStyle);
        }

        private void dgvInventory_CurrentCellChanged(object sender, EventArgs e) {
            var dataGrid = sender as DataGrid;
            if (dataGrid == null)
                return;
            int index = ((DataGrid)sender).CurrentCell.RowNumber;
            if (((DataTable)(dataGrid.DataSource)).Rows.Count > 0)
                ((DataGrid)sender).Select(index);
        }

        private void dgvInventory_DoubleClick(object sender, EventArgs e) {
            this.SelectDataGridRow(this.dgvInventory);
        }

        private void dgvInventory_GotFocus(object sender, EventArgs e) {
            var dataGrid = sender as DataGrid;
            if (dataGrid == null)
                return;
            int index = ((DataGrid)sender).CurrentCell.RowNumber;
            if (((DataTable)(dataGrid.DataSource)).Rows.Count > 0)
                ((DataGrid)sender).Select(index);
        }

        private void BeginRefresh() {
            //先清空缓存
            this.txtCode.Text = string.Empty;

            DataTable dt = this.SetDataSource(null);
            this.dgvInventory.TableStyles.Clear();
            this.SetDataGridStyle(this.dgvInventory, dt);
            this.dgvInventory.DataSource = dt;
        }

        private DataTable SetDataSource(List<PartsInventoryBillDTO> data) {
            var dd = new DataTable();
            dd.TableName = "dgvInventory";
            dd.Columns.Add("单号");
            dd.Columns.Add("发起人");

            if (data == null)
                return dd;
            foreach (var t in data.GroupBy(o => o.Code).OrderBy(o => o.Key)) {
                var result = data.FirstOrDefault(o => o.Code == t.Key);
                DataRow dr = dd.NewRow();
                dr["单号"] = t.Key;
                dr["发起人"] = result == null ? "" : result.InitiatorName;
                dd.Rows.Add(dr);
            }
            return dd;
        }

        //初始化方法
        private void Initialize() {
            if (!this.isInitialize) {
                this.isInitialize = true;

                var dd = new DataTable();
                dd.TableName = "dgvInventory";
                dd.Columns.Add("单号");
                dd.Columns.Add("发起人");
                this.SetDataGridStyle(this.dgvInventory, dd);
                this.dgvInventory.DataSource = dd;
            }
        }

        private int ScanTicketClear = 0;
        private void txtCode_KeyDown(object sender, KeyEventArgs e) {
            if (ScanTicketClear == 0) {
                this.txtCode.Text = string.Empty;
                ScanTicketClear++;
            }
            if (e.KeyCode == Keys.Enter) {
                ScanTicketClear = 0;
                this.IsLoading(true);
                MessageInfo<List<PartsInventoryBillDTO>> messageInfo = ServiceManager.GetInventoryBills(txtCode.Text.Trim());
                this.IsLoading(false);

                if (messageInfo.IsSuccess)
                    if (messageInfo.Data != null && messageInfo.Data.Any()) {
                        partsInventoryBills = messageInfo.Data.OrderByDescending(o => o.Code).ThenBy(o => o.WarehouseAreaCode).ThenBy(o => o.SparePartCode).ToList();
                        if (partsInventoryBills == null)
                            partsInventoryBills = new List<PartsInventoryBillDTO>();
                        //界面参数
                        this.dgvInventory.TableStyles.Clear();
                        DataTable dt = this.SetDataSource(partsInventoryBills);
                        this.SetDataGridStyle(this.dgvInventory, dt);
                        this.dgvInventory.DataSource = dt;

                        if (partsInventoryBills.GroupBy(o => o.Code).ToList().Count == 1) {
                            var iof = new InventoryingFrm(partsInventoryBills);
                            iof.ShowDialog();
                            this.BeginRefresh();
                            return;
                        }
                    } else {
                        this.dgvInventory.TableStyles.Clear();
                        DataTable dt = this.SetDataSource(new List<PartsInventoryBillDTO>());
                        this.SetDataGridStyle(this.dgvInventory, dt);
                        this.dgvInventory.DataSource = dt;
                        MessageBoxForm.Show(this, "无查询结果", "提示", this.DefaultButtonResults);
                    } else
                    MessageBoxForm.Show(this, messageInfo.Message, "提示", this.DefaultButtonResults);
            }
        }
    }
}