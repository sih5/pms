﻿namespace Sunlight.Ce.WinUI {
    partial class InventoryingFrm {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent() {
            this.btnNext = new System.Windows.Forms.Button();
            this.txtStorageAfterInventory = new System.Windows.Forms.TextBox();
            this.lblThisShelvesAmount = new System.Windows.Forms.Label();
            this.labSprit = new System.Windows.Forms.Label();
            this.txtScanSparePartCode = new System.Windows.Forms.TextBox();
            this.lblSparePartCode = new System.Windows.Forms.Label();
            this.txtScanLocation = new System.Windows.Forms.TextBox();
            this.lblScanLocation = new System.Windows.Forms.Label();
            this.txtRecommendLocation = new System.Windows.Forms.TextBox();
            this.txtSparePartsName = new System.Windows.Forms.TextBox();
            this.txtSparePartsCode = new System.Windows.Forms.TextBox();
            this.lblSparePartsName = new System.Windows.Forms.Label();
            this.lblRecommendLocation = new System.Windows.Forms.Label();
            this.lblSparePartsCode = new System.Windows.Forms.Label();
            this.btnUpload = new System.Windows.Forms.Button();
            this.txtTraceProperty = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAddDetail = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnNext
            // 
            this.btnNext.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnNext.Location = new System.Drawing.Point(155, 247);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(74, 35);
            this.btnNext.TabIndex = 78;
            this.btnNext.Text = "确认";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // txtStorageAfterInventory
            // 
            this.txtStorageAfterInventory.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtStorageAfterInventory.Location = new System.Drawing.Point(69, 212);
            this.txtStorageAfterInventory.Name = "txtStorageAfterInventory";
            this.txtStorageAfterInventory.Size = new System.Drawing.Size(84, 26);
            this.txtStorageAfterInventory.TabIndex = 77;
            this.txtStorageAfterInventory.TextChanged += new System.EventHandler(this.txtStorageAfterInventory_TextChanged);
            // 
            // lblThisShelvesAmount
            // 
            this.lblThisShelvesAmount.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblThisShelvesAmount.Location = new System.Drawing.Point(3, 214);
            this.lblThisShelvesAmount.Name = "lblThisShelvesAmount";
            this.lblThisShelvesAmount.Size = new System.Drawing.Size(60, 25);
            this.lblThisShelvesAmount.Text = "数量";
            // 
            // labSprit
            // 
            this.labSprit.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.labSprit.Location = new System.Drawing.Point(164, 213);
            this.labSprit.Name = "labSprit";
            this.labSprit.Size = new System.Drawing.Size(60, 26);
            this.labSprit.Text = "/";
            // 
            // txtScanSparePartCode
            // 
            this.txtScanSparePartCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtScanSparePartCode.Location = new System.Drawing.Point(69, 180);
            this.txtScanSparePartCode.Name = "txtScanSparePartCode";
            this.txtScanSparePartCode.Size = new System.Drawing.Size(169, 26);
            this.txtScanSparePartCode.TabIndex = 76;
            this.txtScanSparePartCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtScanSparePartCode_KeyDown);
            // 
            // lblSparePartCode
            // 
            this.lblSparePartCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblSparePartCode.Location = new System.Drawing.Point(0, 184);
            this.lblSparePartCode.Name = "lblSparePartCode";
            this.lblSparePartCode.Size = new System.Drawing.Size(92, 20);
            this.lblSparePartCode.Text = "扫描配件";
            // 
            // txtScanLocation
            // 
            this.txtScanLocation.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtScanLocation.Location = new System.Drawing.Point(69, 113);
            this.txtScanLocation.Name = "txtScanLocation";
            this.txtScanLocation.Size = new System.Drawing.Size(169, 26);
            this.txtScanLocation.TabIndex = 75;
            this.txtScanLocation.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtScanLocation_KeyDown);
            // 
            // lblScanLocation
            // 
            this.lblScanLocation.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblScanLocation.Location = new System.Drawing.Point(0, 117);
            this.lblScanLocation.Name = "lblScanLocation";
            this.lblScanLocation.Size = new System.Drawing.Size(92, 20);
            this.lblScanLocation.Text = "扫描货位";
            // 
            // txtRecommendLocation
            // 
            this.txtRecommendLocation.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRecommendLocation.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtRecommendLocation.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtRecommendLocation.Location = new System.Drawing.Point(71, 77);
            this.txtRecommendLocation.Name = "txtRecommendLocation";
            this.txtRecommendLocation.ReadOnly = true;
            this.txtRecommendLocation.Size = new System.Drawing.Size(167, 26);
            this.txtRecommendLocation.TabIndex = 74;
            // 
            // txtSparePartsName
            // 
            this.txtSparePartsName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSparePartsName.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtSparePartsName.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtSparePartsName.Location = new System.Drawing.Point(71, 6);
            this.txtSparePartsName.Name = "txtSparePartsName";
            this.txtSparePartsName.ReadOnly = true;
            this.txtSparePartsName.Size = new System.Drawing.Size(167, 26);
            this.txtSparePartsName.TabIndex = 72;
            // 
            // txtSparePartsCode
            // 
            this.txtSparePartsCode.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSparePartsCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtSparePartsCode.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtSparePartsCode.Location = new System.Drawing.Point(71, 41);
            this.txtSparePartsCode.Name = "txtSparePartsCode";
            this.txtSparePartsCode.ReadOnly = true;
            this.txtSparePartsCode.Size = new System.Drawing.Size(167, 26);
            this.txtSparePartsCode.TabIndex = 73;
            // 
            // lblSparePartsName
            // 
            this.lblSparePartsName.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblSparePartsName.Location = new System.Drawing.Point(-1, 8);
            this.lblSparePartsName.Name = "lblSparePartsName";
            this.lblSparePartsName.Size = new System.Drawing.Size(95, 27);
            this.lblSparePartsName.Text = "零件名称";
            // 
            // lblRecommendLocation
            // 
            this.lblRecommendLocation.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblRecommendLocation.Location = new System.Drawing.Point(-1, 79);
            this.lblRecommendLocation.Name = "lblRecommendLocation";
            this.lblRecommendLocation.Size = new System.Drawing.Size(95, 27);
            this.lblRecommendLocation.Text = "货位号";
            // 
            // lblSparePartsCode
            // 
            this.lblSparePartsCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblSparePartsCode.Location = new System.Drawing.Point(-1, 43);
            this.lblSparePartsCode.Name = "lblSparePartsCode";
            this.lblSparePartsCode.Size = new System.Drawing.Size(95, 27);
            this.lblSparePartsCode.Text = "零件编号";
            // 
            // btnUpload
            // 
            this.btnUpload.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnUpload.Location = new System.Drawing.Point(69, 247);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(73, 35);
            this.btnUpload.TabIndex = 78;
            this.btnUpload.Text = "上传";
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // txtTraceProperty
            // 
            this.txtTraceProperty.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTraceProperty.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtTraceProperty.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtTraceProperty.Location = new System.Drawing.Point(69, 146);
            this.txtTraceProperty.Name = "txtTraceProperty";
            this.txtTraceProperty.ReadOnly = true;
            this.txtTraceProperty.Size = new System.Drawing.Size(167, 26);
            this.txtTraceProperty.TabIndex = 88;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.label1.Location = new System.Drawing.Point(-3, 148);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 27);
            this.label1.Text = "追溯属性";
            // 
            // btnAddDetail
            // 
            this.btnAddDetail.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnAddDetail.Location = new System.Drawing.Point(7, 247);
            this.btnAddDetail.Name = "btnAddDetail";
            this.btnAddDetail.Size = new System.Drawing.Size(48, 35);
            this.btnAddDetail.TabIndex = 97;
            this.btnAddDetail.Text = "+";
            this.btnAddDetail.Click += new System.EventHandler(this.btnAddDetail_Click);
            // 
            // InventoryingFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 295);
            this.Controls.Add(this.btnAddDetail);
            this.Controls.Add(this.txtTraceProperty);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnUpload);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.txtStorageAfterInventory);
            this.Controls.Add(this.lblThisShelvesAmount);
            this.Controls.Add(this.labSprit);
            this.Controls.Add(this.txtScanSparePartCode);
            this.Controls.Add(this.lblSparePartCode);
            this.Controls.Add(this.txtScanLocation);
            this.Controls.Add(this.lblScanLocation);
            this.Controls.Add(this.txtRecommendLocation);
            this.Controls.Add(this.txtSparePartsName);
            this.Controls.Add(this.txtSparePartsCode);
            this.Controls.Add(this.lblSparePartsName);
            this.Controls.Add(this.lblRecommendLocation);
            this.Controls.Add(this.lblSparePartsCode);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InventoryingFrm";
            this.Text = "盘点";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.TextBox txtStorageAfterInventory;
        private System.Windows.Forms.Label lblThisShelvesAmount;
        private System.Windows.Forms.Label labSprit;
        private System.Windows.Forms.TextBox txtScanSparePartCode;
        private System.Windows.Forms.Label lblSparePartCode;
        private System.Windows.Forms.TextBox txtScanLocation;
        private System.Windows.Forms.Label lblScanLocation;
        private System.Windows.Forms.TextBox txtRecommendLocation;
        private System.Windows.Forms.TextBox txtSparePartsName;
        private System.Windows.Forms.TextBox txtSparePartsCode;
        private System.Windows.Forms.Label lblSparePartsName;
        private System.Windows.Forms.Label lblRecommendLocation;
        private System.Windows.Forms.Label lblSparePartsCode;
        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.TextBox txtTraceProperty;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAddDetail;
    }
}