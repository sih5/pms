﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using Sunlight.Ce.Controls;
using Sunlight.Ce.Entities.DTO;
using System.Text.RegularExpressions;
using Sunlight.Ce.Service;
using Sunlight.Ce.Utils;

namespace Sunlight.Ce.WinUI {
    public partial class InventoryingFrm : BaseFrm {
        private bool isInitialize;
        public List<PartsInventoryBillDTO> partsInventoryBills;
        private int index = 0;
        public InventoryingFrm(List<PartsInventoryBillDTO> partsInventoryBillPara) {
            InitializeComponent();

            partsInventoryBills = partsInventoryBillPara;
            this.Initialize();
        }

        private void Initialize() {
            if (!this.isInitialize) {
                this.isInitialize = true;

                InitEntity();
            }
        }

        public void InitEntity() {
            //this.txtStorageAfterInventory.ReadOnly = true;
            //this.txtStorageAfterInventory.Enabled = true;
            this.txtStorageAfterInventory.Text = partsInventoryBills[index].StorageAfterInventory.ToString();
            this.txtSparePartsCode.Text = partsInventoryBills[index].SparePartCode;
            this.txtSparePartsName.Text = partsInventoryBills[index].SparePartName;
            this.txtRecommendLocation.Text = partsInventoryBills[index].WarehouseAreaCode;
            this.txtTraceProperty.Text = TracePropertyValue(partsInventoryBills[index].TraceProperty);
            this.labSprit.Text = partsInventoryBills[index].AlreadyStorageAfterInventory + "/" + partsInventoryBills[index].CurrentStorage;
            this.txtScanLocation.Focus();
            if (index + 1 != partsInventoryBills.Count)
                this.btnNext.Enabled = true;
            else
                this.btnNext.Enabled = false;
        }

        private void btnNext_Click(object sender, EventArgs e) {
            this.index++;
            InitEntity();
            this.txtScanLocation.Text = string.Empty;
            this.txtScanSparePartCode.Text = string.Empty;
            if (index + 1 == partsInventoryBills.Count)
                this.btnNext.Enabled = false;
        }

        private void txtStorageAfterInventory_TextChanged(object sender, EventArgs e) {
            try {
                var rgx = new Regex(@"^\d+$");
                var tempTxtshippingAmount = sender as TextBox;
                if (tempTxtshippingAmount != null && !string.IsNullOrEmpty(tempTxtshippingAmount.Text)) {
                    var thisNumber = tempTxtshippingAmount.Text;
                    if (!rgx.IsMatch(thisNumber)) {
                        MessageBoxForm.Show(this, "你输入的不是数字！", "提示", this.DefaultButtonResults);
                        this.txtStorageAfterInventory.Text = null;
                        return;
                    }
                    partsInventoryBills[index].StorageAfterInventory = Convert.ToInt32(thisNumber.Trim());
                    partsInventoryBills[index].StorageDifference = (partsInventoryBills[index].StorageAfterInventory ?? 0) - partsInventoryBills[index].CurrentStorage;
                }
            } catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }

        private void txtScanSparePartCode_KeyDown(object sender, KeyEventArgs e) {
            if (ScanTicketClear == 0) {
                this.txtScanSparePartCode.Text = string.Empty;
                ScanTicketClear++;
            }
            if (e.KeyCode == Keys.Enter) {
                ScanTicketClear = 0;
                if (string.IsNullOrEmpty(this.txtScanLocation.Text)) {
                    MessageBoxForm.Show(this, "请先扫描货位！", "提示", this.DefaultButtonResults);
                    this.txtScanSparePartCode.Text = null;
                } else {
                    string[] codeAndNum = this.txtScanSparePartCode.Text.Split('|');
                    if (codeAndNum.Length < 2) {
                        MessageBoxForm.Show(this, "扫描配件条码错误！", "提示", this.DefaultButtonResults);
                        this.txtScanSparePartCode.Text = null;
                        return;
                    }
                    var newIndex = partsInventoryBills.FindIndex(o => o.SparePartCode == codeAndNum[0] && o.WarehouseAreaCode == this.txtScanLocation.Text);
                    if (newIndex >= 0) {
                        if (newIndex != index) {
                            index = newIndex;
                            InitEntity();
                            this.txtScanSparePartCode.Focus();
                            if (index + 1 == partsInventoryBills.Count)
                                this.btnNext.Enabled = false;
                        }
                        if (partsInventoryBills[index].TraceProperty.HasValue && partsInventoryBills[index].TraceProperty == (int)DCSTraceProperty.精确追溯
                            && partsInventoryBills[index].SIHCodes.Contains(this.txtScanSparePartCode.Text)) {
                            MessageBoxForm.Show(this, "当前标签码已被扫描！", "提示", this.DefaultButtonResults);
                            this.txtScanSparePartCode.Text = null;
                            return;
                        }
                        partsInventoryBills[index].SIHCodes.Add(this.txtScanSparePartCode.Text);
                        var storageAfterInventory = string.IsNullOrEmpty(this.txtStorageAfterInventory.Text) ? 0 : Convert.ToInt32(this.txtStorageAfterInventory.Text);
                        this.txtStorageAfterInventory.Text = (storageAfterInventory + Convert.ToInt32(codeAndNum[1])).ToString();
                    } else {
                        MessageBoxForm.Show(this, "当前货位与配件编号，不存在当前盘点任务清单！", "提示", this.DefaultButtonResults);
                        this.txtScanSparePartCode.Text = null;
                    }
                }
            }
        }

        private int ScanTicketClear = 0;
        private void txtScanLocation_KeyDown(object sender, KeyEventArgs e) {
            if (ScanTicketClear == 0) {
                this.txtScanLocation.Text = string.Empty;
                ScanTicketClear++;
            }
            if (e.KeyCode == Keys.Enter) {
                ScanTicketClear = 0;

                this.txtScanSparePartCode.Focus();
            }
        }

        private void btnUpload_Click(object sender, EventArgs e) {
            try {
                if (partsInventoryBills != null && partsInventoryBills.Count > 0) {
                    this.IsLoading(true);
                    var data = partsInventoryBills[index].Status == (int)DcsPartsInventoryBillStatus.新建 ? partsInventoryBills :
                    partsInventoryBills.Where(o => o.StorageAfterInventory.HasValue).ToList();
                    if (data.Count == 0) {
                        this.IsLoading(false);
                        MessageBoxForm.Show(this, "未录入盘点结果，不允许上传.", "提示", this.DefaultButtonResults);
                        return;
                    }
                    var result = ServiceManager.ResultsInput(data);
                    this.IsLoading(false);
                    if (result.IsSuccess) {
                        MessageBoxForm.Show(this, result.Message, "提示", this.DefaultButtonResults);
                        RasieEditSubmitted();
                        this.Close();
                    } else {
                        MessageBoxForm.Show(this, result.Message, "提示", this.DefaultButtonResults);
                        partsInventoryBills.RemoveAll(o => { return string.IsNullOrEmpty(o.Code); });
                    }
                }
            } catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }

        private string TracePropertyValue(int? type) {
            switch (type) {
                case (int)DCSTraceProperty.精确追溯:
                    return "精确追溯";
                case (int)DCSTraceProperty.批次追溯:
                    return "批次追溯";
                default:
                    return "";
            }
        }

        private void btnAddDetail_Click(object sender, EventArgs e) {
            var differenceFrm = new InventoryDifferenceFrm(this);
            differenceFrm.ShowDialog();
        }
    }
}