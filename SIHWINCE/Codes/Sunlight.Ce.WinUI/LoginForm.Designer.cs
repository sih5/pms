﻿namespace Sunlight.Ce.WinUI {
    partial class LoginForm {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent() {
            this.btnExit = new System.Windows.Forms.Button();
            this.btnLogin = new System.Windows.Forms.Button();
            this.txtPwd = new System.Windows.Forms.TextBox();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.txtEnterpriseCode = new System.Windows.Forms.TextBox();
            this.lblPwd = new System.Windows.Forms.Label();
            this.lblEnterpriseCode = new System.Windows.Forms.Label();
            this.lblUserCode = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblTopTitle = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.cboShouPassWord = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.btnExit.Location = new System.Drawing.Point(126, 214);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(58, 36);
            this.btnExit.TabIndex = 22;
            this.btnExit.Text = "关闭";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnLogin
            // 
            this.btnLogin.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.btnLogin.Location = new System.Drawing.Point(41, 214);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(58, 36);
            this.btnLogin.TabIndex = 21;
            this.btnLogin.Text = "登录";
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // txtPwd
            // 
            this.txtPwd.Location = new System.Drawing.Point(98, 154);
            this.txtPwd.Name = "txtPwd";
            this.txtPwd.PasswordChar = '*';
            this.txtPwd.Size = new System.Drawing.Size(100, 23);
            this.txtPwd.TabIndex = 20;
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(98, 117);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(100, 23);
            this.txtUserName.TabIndex = 19;
            // 
            // txtEnterpriseCode
            // 
            this.txtEnterpriseCode.Location = new System.Drawing.Point(98, 78);
            this.txtEnterpriseCode.Name = "txtEnterpriseCode";
            this.txtEnterpriseCode.Size = new System.Drawing.Size(100, 23);
            this.txtEnterpriseCode.TabIndex = 18;
            // 
            // lblPwd
            // 
            this.lblPwd.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular);
            this.lblPwd.Location = new System.Drawing.Point(41, 155);
            this.lblPwd.Name = "lblPwd";
            this.lblPwd.Size = new System.Drawing.Size(51, 28);
            this.lblPwd.Text = "密码";
            this.lblPwd.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblEnterpriseCode
            // 
            this.lblEnterpriseCode.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular);
            this.lblEnterpriseCode.Location = new System.Drawing.Point(3, 78);
            this.lblEnterpriseCode.Name = "lblEnterpriseCode";
            this.lblEnterpriseCode.Size = new System.Drawing.Size(96, 20);
            this.lblEnterpriseCode.Text = "企业编号";
            this.lblEnterpriseCode.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblUserCode
            // 
            this.lblUserCode.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular);
            this.lblUserCode.Location = new System.Drawing.Point(12, 117);
            this.lblUserCode.Name = "lblUserCode";
            this.lblUserCode.Size = new System.Drawing.Size(87, 20);
            this.lblUserCode.Text = "用户名";
            this.lblUserCode.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(12, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(211, 32);
            this.label1.Text = "条码管理系统";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblTopTitle
            // 
            this.lblTopTitle.Font = new System.Drawing.Font("Verdana", 16F, System.Drawing.FontStyle.Bold);
            this.lblTopTitle.Location = new System.Drawing.Point(68, 13);
            this.lblTopTitle.Name = "lblTopTitle";
            this.lblTopTitle.Size = new System.Drawing.Size(106, 30);
            this.lblTopTitle.Text = "红岩配件";
            // 
            // lblVersion
            // 
            this.lblVersion.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.lblVersion.Location = new System.Drawing.Point(151, 268);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(75, 20);
            this.lblVersion.Text = " 1.0.1.4";
            // 
            // cboShouPassWord
            // 
            this.cboShouPassWord.Location = new System.Drawing.Point(127, 178);
            this.cboShouPassWord.Name = "cboShouPassWord";
            this.cboShouPassWord.Size = new System.Drawing.Size(91, 18);
            this.cboShouPassWord.TabIndex = 28;
            this.cboShouPassWord.Text = "显示密码";
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.ControlBox = false;
            this.Controls.Add(this.cboShouPassWord);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.txtPwd);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.txtEnterpriseCode);
            this.Controls.Add(this.lblPwd);
            this.Controls.Add(this.lblEnterpriseCode);
            this.Controls.Add(this.lblUserCode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblTopTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoginForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.TextBox txtPwd;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.TextBox txtEnterpriseCode;
        private System.Windows.Forms.Label lblPwd;
        private System.Windows.Forms.Label lblEnterpriseCode;
        private System.Windows.Forms.Label lblUserCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblTopTitle;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.CheckBox cboShouPassWord;
    }
}