﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Reflection;
using System.Windows.Forms;
using System.Xml;
using Sunlight.Ce.Controls;
using Sunlight.Ce.Service;
using Sunlight.Ce.Update;
using Sunlight.Ce.Utils;

namespace Sunlight.Ce.WinUI {
    public partial class LoginForm : BaseFrm {
        private CurrentWarehouseFrm warehosueFrm;
        private readonly HelperUtil helpinstance = new HelperUtil();

        private void LoginForm_Load(object sender, EventArgs e) {
            var userInfo = HelperUtil.Instance.GetCacheUserInfo();
            //this.txtEnterpriseCode.Text = "BAIC";
            if(string.IsNullOrEmpty(userInfo.UserId) || string.IsNullOrEmpty(userInfo.EnterpriseCode)) {
                return;
            }
            this.txtUserName.Text = userInfo.UserId;

            //this.txtEnterpriseCode.Text = userInfo.WarehouseCode;
            this.txtPwd.Focus();
        }

        private void InitializeEvent() {
            this.btnLogin.Click -= btnLogin_Click;
            this.btnExit.Click -= this.btnExit_Click;
            this.btnLogin.Click += btnLogin_Click;
            this.btnExit.Click += this.btnExit_Click;
            this.cboShouPassWord.CheckStateChanged -= this.cboShouPassWord_CheckStateChanged;
            this.cboShouPassWord.CheckStateChanged += this.cboShouPassWord_CheckStateChanged;
        }

        private void btnExit_Click(object sender, EventArgs e) {
            Application.Exit();
        }

        private void btnLogin_Click(object sender, EventArgs e) {
            this.CommonLoginOperation();
        }

        public LoginForm() {
            InitializeComponent();
            this.InitializeEvent();
            this.Load -= this.LoginForm_Load;
            this.Load += this.LoginForm_Load;

            this.txtEnterpriseCode.KeyUp -= this.txtCommon_KeyUp;
            this.txtUserName.KeyUp -= this.txtCommon_KeyUp;
            this.txtPwd.KeyUp -= this.txtCommon_KeyUp;

            this.txtEnterpriseCode.KeyUp += this.txtCommon_KeyUp;
            this.txtUserName.KeyUp += this.txtCommon_KeyUp;
            this.txtPwd.KeyUp += this.txtCommon_KeyUp;
        }

        private void CommonLoginOperation() {
            var enterpriseCode = txtEnterpriseCode.Text;
            var userName = txtUserName.Text;
            var userPwd = txtPwd.Text;

            if(string.IsNullOrEmpty(enterpriseCode) || string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(userPwd)) {
                var errorMessage = string.Empty;
                errorMessage = string.IsNullOrEmpty(enterpriseCode) ? "登录企业不能为空！" : errorMessage;
                if(string.IsNullOrEmpty(errorMessage))
                    errorMessage = string.IsNullOrEmpty(userName) ? "登录用户名不能为空！" : errorMessage;
                if(string.IsNullOrEmpty(errorMessage))
                    errorMessage = string.IsNullOrEmpty(userPwd) ? "登录密码不能为空！" : errorMessage;

                MessageBoxForm.Show(this, errorMessage, "提示", this.DefaultButtonResults);
                return;
            }

            try {
                this.IsLoading(true);
                var loginMessage = ServiceManager.Login(enterpriseCode.Trim(), userName.Trim(), userPwd.Trim());
                this.IsLoading(false);

                if(!loginMessage.IsSuccess) {
                    MessageBoxForm.Show(this, loginMessage.Message, "提示", this.DefaultButtonResults);
                    return;
                }
                var isProudction = Boolean.Parse(helpinstance.GetConfigurationByKey("IsProduction").AppSettingValue);
                if(isProudction) {
                    UpdateThread();
                }

                HelperUtil.Instance.CachCurrentUserInfo(enterpriseCode, userName);
                warehosueFrm = new CurrentWarehouseFrm(enterpriseCode, userName);
                warehosueFrm.Owner = this;
                warehosueFrm.ShowDialog();
            }
            catch(Exception ex) {
                this.IsLoading(false);
                MessageBoxForm.Show(this, ex.Message, "提示", DefaultButtonResults);
            }
        }

        private void txtCommon_KeyUp(object sender, KeyEventArgs e) {
            if(e.KeyCode != Keys.Enter)
                return;
            this.CommonLoginOperation();
        }

        /// <summary>
        /// 更新程序
        /// </summary>
        public void UpdateThread() {
            //当前环境类型
            try {
                //此处实现版本自检索，实现版本升级使用
                var updater = new Updater(helpinstance.GetConfigurationByKey("UpdateAddress").AppSettingValue);
                updater.CheckForNewVersion();
                var callingAssembly = Assembly.GetCallingAssembly();
                string fullAppName = callingAssembly.GetName().CodeBase;
                var appPath = Path.GetDirectoryName(fullAppName);
                var updateFilePath = Path.Combine(appPath, "update.xml");
                if(File.Exists(updateFilePath)) {
                    Version currentVersion = callingAssembly.GetName().Version;
                    var xDoc = new XmlDocument();
                    xDoc.Load(updateFilePath);
                    XmlNodeList versions = xDoc.GetElementsByTagName("version");

                    var newVersion = new Version(int.Parse(versions[0].Attributes["maj"].Value), int.Parse(versions[0].Attributes["min"].Value), int.Parse(versions[0].Attributes["bld"].Value), int.Parse(versions[0].Attributes["fix"].Value));

                    if(currentVersion.CompareTo(newVersion) < 0) {
                        Process.Start(appPath + "\\Sunlight.Ce.Update.exe", "");
                        Process myproc = Process.GetCurrentProcess();
                        myproc.Kill();
                    }
                    updater.cleanup(updater.updateFilePath);
                }
            }
            catch(WebException webex) {
                MessageBoxForm.Show(this, "[网络错误]" + webex.Message, "提示", new Dictionary<string, DialogResult> { { "确认", DialogResult.OK } });
                //AutoClosingMessageBox.Show("[网络错误]" + webex.Message, "提示", 4000);
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch {
            }
            finally {
                Cursor.Current = Cursors.Default;
            }
        }

        private void cboShouPassWord_CheckStateChanged(object sender, EventArgs e) {
            if(cboShouPassWord.Checked) {
                txtPwd.PasswordChar = new char();
            }
            else {
                txtPwd.PasswordChar = '*';
            }
        }
    }
}