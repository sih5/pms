﻿namespace Sunlight.Ce.WinUI {
    partial class PackingManagementFrm {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent() {
            this.txtinCode = new System.Windows.Forms.TextBox();
            this.lblCode = new System.Windows.Forms.Label();
            this.txtThisPackingQty = new System.Windows.Forms.TextBox();
            this.lblSparePartCode = new System.Windows.Forms.Label();
            this.txtSparePartsName = new System.Windows.Forms.TextBox();
            this.lblSparePartsName = new System.Windows.Forms.Label();
            this.labSprit = new System.Windows.Forms.Label();
            this.txtSIHCode = new System.Windows.Forms.TextBox();
            this.lblone = new System.Windows.Forms.Label();
            this.lblthree = new System.Windows.Forms.Label();
            this.lbltwo = new System.Windows.Forms.Label();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.lblFirstNumber = new System.Windows.Forms.Label();
            this.txtBoxCode = new System.Windows.Forms.TextBox();
            this.lblBoxCode = new System.Windows.Forms.Label();
            this.txtTraceCode = new System.Windows.Forms.TextBox();
            this.lblSIHCode = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTraceProperty = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtinCode
            // 
            this.txtinCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtinCode.Location = new System.Drawing.Point(69, 5);
            this.txtinCode.Name = "txtinCode";
            this.txtinCode.Size = new System.Drawing.Size(168, 26);
            this.txtinCode.TabIndex = 4;
            this.txtinCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtinCode_KeyDown);
            // 
            // lblCode
            // 
            this.lblCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblCode.Location = new System.Drawing.Point(0, 9);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(92, 20);
            this.lblCode.Text = "扫描单号";
            // 
            // txtThisPackingQty
            // 
            this.txtThisPackingQty.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtThisPackingQty.Location = new System.Drawing.Point(87, 189);
            this.txtThisPackingQty.Name = "txtThisPackingQty";
            this.txtThisPackingQty.Size = new System.Drawing.Size(84, 26);
            this.txtThisPackingQty.TabIndex = 7;
            this.txtThisPackingQty.TextChanged += new System.EventHandler(this.txtThisPackingQty_TextChanged);
            // 
            // lblSparePartCode
            // 
            this.lblSparePartCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblSparePartCode.Location = new System.Drawing.Point(-1, 71);
            this.lblSparePartCode.Name = "lblSparePartCode";
            this.lblSparePartCode.Size = new System.Drawing.Size(92, 20);
            this.lblSparePartCode.Text = "扫描件码";
            // 
            // txtSparePartsName
            // 
            this.txtSparePartsName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSparePartsName.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtSparePartsName.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtSparePartsName.Location = new System.Drawing.Point(69, 129);
            this.txtSparePartsName.Name = "txtSparePartsName";
            this.txtSparePartsName.ReadOnly = true;
            this.txtSparePartsName.Size = new System.Drawing.Size(167, 26);
            this.txtSparePartsName.TabIndex = 35;
            // 
            // lblSparePartsName
            // 
            this.lblSparePartsName.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblSparePartsName.Location = new System.Drawing.Point(-3, 131);
            this.lblSparePartsName.Name = "lblSparePartsName";
            this.lblSparePartsName.Size = new System.Drawing.Size(95, 27);
            this.lblSparePartsName.Text = "零件名称";
            // 
            // labSprit
            // 
            this.labSprit.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.labSprit.Location = new System.Drawing.Point(177, 190);
            this.labSprit.Name = "labSprit";
            this.labSprit.Size = new System.Drawing.Size(60, 28);
            this.labSprit.Text = "/";
            // 
            // txtSIHCode
            // 
            this.txtSIHCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtSIHCode.Location = new System.Drawing.Point(68, 67);
            this.txtSIHCode.Name = "txtSIHCode";
            this.txtSIHCode.Size = new System.Drawing.Size(169, 26);
            this.txtSIHCode.TabIndex = 4;
            this.txtSIHCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSparePartCode_KeyDown);
            // 
            // lblone
            // 
            this.lblone.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblone.Location = new System.Drawing.Point(0, 215);
            this.lblone.Name = "lblone";
            this.lblone.Size = new System.Drawing.Size(131, 28);
            this.lblone.Text = "基本包装 ";
            this.lblone.Visible = false;
            // 
            // lblthree
            // 
            this.lblthree.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblthree.Location = new System.Drawing.Point(0, 253);
            this.lblthree.Name = "lblthree";
            this.lblthree.Size = new System.Drawing.Size(122, 28);
            this.lblthree.Text = "三级包装 ";
            this.lblthree.Visible = false;
            // 
            // lbltwo
            // 
            this.lbltwo.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lbltwo.Location = new System.Drawing.Point(120, 215);
            this.lbltwo.Name = "lbltwo";
            this.lbltwo.Size = new System.Drawing.Size(118, 28);
            this.lbltwo.Text = "二级包装 ";
            this.lbltwo.Visible = false;
            // 
            // btnSubmit
            // 
            this.btnSubmit.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnSubmit.Location = new System.Drawing.Point(126, 224);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(102, 42);
            this.btnSubmit.TabIndex = 42;
            this.btnSubmit.Text = "上传";
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel1.Location = new System.Drawing.Point(2, 200);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(233, 1);
            this.panel1.Visible = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel2.Location = new System.Drawing.Point(119, 204);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1, 80);
            this.panel2.Visible = false;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.label5.Location = new System.Drawing.Point(-3, 191);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(102, 27);
            this.label5.Text = "本次包装量";
            // 
            // lblFirstNumber
            // 
            this.lblFirstNumber.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblFirstNumber.Location = new System.Drawing.Point(120, 253);
            this.lblFirstNumber.Name = "lblFirstNumber";
            this.lblFirstNumber.Size = new System.Drawing.Size(118, 28);
            this.lblFirstNumber.Text = "包材数量 0 份";
            this.lblFirstNumber.Visible = false;
            // 
            // txtBoxCode
            // 
            this.txtBoxCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtBoxCode.Location = new System.Drawing.Point(68, 36);
            this.txtBoxCode.Name = "txtBoxCode";
            this.txtBoxCode.Size = new System.Drawing.Size(169, 26);
            this.txtBoxCode.TabIndex = 53;
            this.txtBoxCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBoxCode_KeyDown);
            // 
            // lblBoxCode
            // 
            this.lblBoxCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblBoxCode.Location = new System.Drawing.Point(-1, 40);
            this.lblBoxCode.Name = "lblBoxCode";
            this.lblBoxCode.Size = new System.Drawing.Size(92, 20);
            this.lblBoxCode.Text = "扫描箱码";
            // 
            // txtTraceCode
            // 
            this.txtTraceCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtTraceCode.Location = new System.Drawing.Point(69, 98);
            this.txtTraceCode.Name = "txtTraceCode";
            this.txtTraceCode.Size = new System.Drawing.Size(169, 26);
            this.txtTraceCode.TabIndex = 56;
            this.txtTraceCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSIHCode_KeyDown);
            // 
            // lblSIHCode
            // 
            this.lblSIHCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblSIHCode.Location = new System.Drawing.Point(0, 102);
            this.lblSIHCode.Name = "lblSIHCode";
            this.lblSIHCode.Size = new System.Drawing.Size(92, 20);
            this.lblSIHCode.Text = "扫追溯码";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.label1.Location = new System.Drawing.Point(-1, 161);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 27);
            this.label1.Text = "追溯属性";
            // 
            // txtTraceProperty
            // 
            this.txtTraceProperty.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTraceProperty.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtTraceProperty.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtTraceProperty.Location = new System.Drawing.Point(71, 159);
            this.txtTraceProperty.Name = "txtTraceProperty";
            this.txtTraceProperty.ReadOnly = true;
            this.txtTraceProperty.Size = new System.Drawing.Size(167, 26);
            this.txtTraceProperty.TabIndex = 35;
            // 
            // PackingManagementFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 295);
            this.Controls.Add(this.txtTraceCode);
            this.Controls.Add(this.lblSIHCode);
            this.Controls.Add(this.txtBoxCode);
            this.Controls.Add(this.lblBoxCode);
            this.Controls.Add(this.lblFirstNumber);
            this.Controls.Add(this.txtThisPackingQty);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.txtSIHCode);
            this.Controls.Add(this.lbltwo);
            this.Controls.Add(this.lblthree);
            this.Controls.Add(this.lblone);
            this.Controls.Add(this.labSprit);
            this.Controls.Add(this.txtTraceProperty);
            this.Controls.Add(this.txtSparePartsName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblSparePartsName);
            this.Controls.Add(this.lblSparePartCode);
            this.Controls.Add(this.txtinCode);
            this.Controls.Add(this.lblCode);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PackingManagementFrm";
            this.Text = "包装";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtinCode;
        private System.Windows.Forms.Label lblCode;
        private System.Windows.Forms.TextBox txtThisPackingQty;
        private System.Windows.Forms.Label lblSparePartCode;
        private System.Windows.Forms.TextBox txtSparePartsName;
        private System.Windows.Forms.Label lblSparePartsName;
        private System.Windows.Forms.Label labSprit;
        private System.Windows.Forms.TextBox txtSIHCode;
        private System.Windows.Forms.Label lblone;
        private System.Windows.Forms.Label lblthree;
        private System.Windows.Forms.Label lbltwo;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblFirstNumber;
        private System.Windows.Forms.TextBox txtBoxCode;
        private System.Windows.Forms.Label lblBoxCode;
        private System.Windows.Forms.TextBox txtTraceCode;
        private System.Windows.Forms.Label lblSIHCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTraceProperty;
    }
}