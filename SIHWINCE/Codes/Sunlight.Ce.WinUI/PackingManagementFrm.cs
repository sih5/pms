﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using Sunlight.Ce.Controls;
using Sunlight.Ce.Entities.DTO;
using Sunlight.Ce.Service;
using System.Text.RegularExpressions;
using Sunlight.Ce.Utils;


namespace Sunlight.Ce.WinUI {
    public partial class PackingManagementFrm : BaseFrm {
        private List<PackingTaskDTO> packingTasks;

        public PackingManagementFrm() {
            InitializeComponent();

            this.txtSIHCode.ReadOnly = true;
            this.txtSIHCode.Enabled = false;
            this.txtBoxCode.ReadOnly = true;
            this.txtBoxCode.Enabled = false;
            this.txtTraceCode.ReadOnly = true;
            this.txtTraceCode.Enabled = false;
            this.txtThisPackingQty.ReadOnly = true;
            this.txtThisPackingQty.Enabled = false;
            this.btnSubmit.Enabled = false;
            this.txtinCode.Focus();
        }

        private int ScanTicketClear = 0;
        private void txtinCode_KeyDown(object sender, KeyEventArgs e) {
            if (ScanTicketClear == 0) {
                this.txtinCode.Text = string.Empty;
                ScanTicketClear++;
            }
            if (e.KeyCode == Keys.Enter) {
                ScanTicketClear = 0;

                if (string.IsNullOrEmpty(this.txtSIHCode.Text)) {
                    this.txtBoxCode.ReadOnly = false;
                    this.txtBoxCode.Enabled = true;
                    this.txtBoxCode.Focus();
                } else {
                    this.txtinCode.Text = string.Empty;
                }
            }
        }

        private string sparePartCode;
        private void txtSparePartCode_KeyDown(object sender, KeyEventArgs e) {
            if (ScanTicketClear == 0) {
                this.txtSIHCode.Text = string.Empty;
                ScanTicketClear++;
            }
            if (e.KeyCode == Keys.Enter) {
                try {
                    ScanTicketClear = 0;
                    if (string.IsNullOrEmpty(this.txtinCode.Text)) {
                        MessageBoxForm.Show(this, "请先扫描单号！", "提示", this.DefaultButtonResults);
                        this.txtSIHCode.Text = string.Empty;
                        return;
                    } else {
                        string[] codeAndNum = this.txtSIHCode.Text.Split('|');
                        if (codeAndNum.Length < 2) {
                            MessageBoxForm.Show(this, "扫描配件条码错误！", "提示", this.DefaultButtonResults);
                            this.txtSIHCode.Text = null;
                            return;
                        }
                        if (!string.IsNullOrEmpty(this.sparePartCode)) {
                            if (!sparePartCode.Equals(codeAndNum[0])) {
                                MessageBoxForm.Show(this, "扫描配件条码与任务单不一致！", "提示", this.DefaultButtonResults);
                                this.txtSIHCode.Text = null;
                                return;
                            }
                            if (codeAndNum[codeAndNum.Length - 1] != "J" && codeAndNum[codeAndNum.Length - 1] != "X") {
                                MessageBoxForm.Show(this, "扫描配件条码错误，必须为件标或箱标！", "提示", this.DefaultButtonResults);
                                this.txtSIHCode.Text = null;
                                return;
                            }
                            if (packingTasks[0].TraceProperty.HasValue && packingTasks[0].TraceProperty.Value == (int)DCSTraceProperty.精确追溯) {
                                if (packingTasks[0].SIHCodes.Contains(this.txtSIHCode.Text)) {
                                    MessageBoxForm.Show(this, "当前标签码已被扫描！", "提示", this.DefaultButtonResults);
                                    this.txtSIHCode.Text = null;
                                    return;
                                }
                                this.txtThisPackingQty.Text = string.IsNullOrEmpty(this.txtThisPackingQty.Text) ? "1" : (Convert.ToInt32(this.txtThisPackingQty.Text) + 1).ToString();
                                packingTasks[0].BoxCodes.Add(this.txtBoxCode.Text);
                                packingTasks[0].SIHCodes.Add(this.txtSIHCode.Text);

                                this.txtTraceCode.ReadOnly = false;
                                this.txtTraceCode.Enabled = true;
                                this.txtTraceCode.Focus();
                            } else {
                                this.txtThisPackingQty.Text = string.IsNullOrEmpty(this.txtThisPackingQty.Text) ? codeAndNum[1] : (Convert.ToInt32(this.txtThisPackingQty.Text) + Convert.ToInt32(codeAndNum[1])).ToString();
                                if (!packingTasks[0].SIHCodes.Contains(this.txtSIHCode.Text)) {
                                    packingTasks[0].BoxCodes.Add(this.txtBoxCode.Text);
                                    packingTasks[0].SIHCodes.Add(this.txtSIHCode.Text);
                                }

                                this.txtThisPackingQty.ReadOnly = false;
                                this.txtThisPackingQty.Enabled = true;
                                this.txtThisPackingQty.Focus();
                            }
                        }
                    }
                } catch (Exception ex) {
                    MessageBoxForm.Show(this, ex.Message, "错误", this.DefaultButtonResults);
                    return;
                }
            }
        }

        private void txtThisPackingQty_TextChanged(object sender, EventArgs e) {
            try {
                var rgx = new Regex(@"^\d+$");
                var tempTxtshippingAmount = sender as TextBox;
                if (tempTxtshippingAmount != null && !string.IsNullOrEmpty(tempTxtshippingAmount.Text) && !string.IsNullOrEmpty(tempTxtshippingAmount.Text)) {
                    var thisNumber = tempTxtshippingAmount.Text;
                    if (!rgx.IsMatch(thisNumber)) {
                        MessageBoxForm.Show(this, "你输入的不是数字！", "提示", this.DefaultButtonResults);
                        this.txtThisPackingQty.Text = null;
                        return;
                    }
                    if (Convert.ToInt32(thisNumber.Trim()) + integerToInt(packingTasks[0].PackingQty) > packingTasks[0].PlanQty) {//不能大于任务计划量  
                        MessageBoxForm.Show(this, "数量不能大于计划量", "提示", this.DefaultButtonResults);
                        this.txtThisPackingQty.Text = (packingTasks[0].PlanQty - integerToInt(packingTasks[0].PackingQty)).ToString();
                        return;
                    }
                    packingTasks[0].ThisPackingQty = Convert.ToInt32(thisNumber.Trim());

                    this.lblFirstNumber.Text = "包材数量 " + Math.Ceiling(Convert.ToDouble(packingTasks[0].ThisPackingQty) / Convert.ToDouble(packingTasks[0].PackingCoefficient)).ToString() + " 份";
                } else {
                    if (packingTasks != null)
                        packingTasks[0].ThisPackingQty = 0;
                    this.lblFirstNumber.Text = "包材数量 0 份";
                }
            } catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e) {
            try {
                if (packingTasks[0] != null) {
                    if (integerToInt(packingTasks[0].ThisPackingQty) == 0) {
                        MessageBoxForm.Show(this, "请填写本次包装量！", "提示", this.DefaultButtonResults);
                        return;
                    }
                    if (integerToInt(packingTasks[0].PackingCoefficient) == 0 || !packingTasks[0].PackingType.HasValue) {
                        MessageBoxForm.Show(this, "没有配件包装属性，请先维护包装属性！", "提示", this.DefaultButtonResults);
                        return;
                    }
                    if (packingTasks[0].TraceProperty == (int)DCSTraceProperty.精确追溯 && packingTasks[0].TraceCodes.Any() && packingTasks[0].SIHCodes.Count != packingTasks[0].TraceCodes.Count) {
                        MessageBoxForm.Show(this, "当前配件为精确追溯，标签码与追溯码数量不一致,请重新扫描！", "提示", this.DefaultButtonResults);
                        packingTasks[0].BoxCodes.Clear();
                        packingTasks[0].SIHCodes.Clear();
                        packingTasks[0].TraceCodes.Clear();
                        packingTasks[0].ThisPackingQty = 0;
                        return;
                    }
                    this.IsLoading(true);
                    var result = ServiceManager.Packing(packingTasks[0]);
                    this.IsLoading(false);
                    if (result.IsSuccess) {
                        MessageBoxForm.Show(this, result.Message, "提示", this.DefaultButtonResults);
                        this.packingTasks = null;
                        this.txtSIHCode.ReadOnly = true;
                        this.txtSIHCode.Enabled = false;
                        this.txtThisPackingQty.ReadOnly = true;
                        this.txtThisPackingQty.Enabled = false;
                        this.txtBoxCode.ReadOnly = true;
                        this.txtBoxCode.Enabled = false;
                        this.txtTraceCode.ReadOnly = true;
                        this.txtTraceCode.Enabled = false;
                        this.btnSubmit.Enabled = false;
                        this.txtinCode.Focus();

                        this.txtinCode.Text = string.Empty;
                        this.txtSIHCode.Text = string.Empty;
                        this.txtSparePartsName.Text = string.Empty;
                        this.txtThisPackingQty.Text = string.Empty;
                        this.txtBoxCode.Text = string.Empty;
                        this.txtTraceCode.Text = string.Empty;
                        this.lblone.Text = "基本包装";
                        this.lbltwo.Text = "二级包装";
                        this.lblthree.Text = "三级包装";
                        this.labSprit.Text = "/";
                        this.lblFirstNumber.Text = "包材数量 0 份";
                        sparePartCode = string.Empty;
                    } else {
                        MessageBoxForm.Show(this, result.Message, "提示", this.DefaultButtonResults);
                    }
                }
            } catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }

        private int ScanBoxCodeClear;
        private void txtBoxCode_KeyDown(object sender, KeyEventArgs e) {
            if (ScanBoxCodeClear == 0) {
                this.txtBoxCode.Text = string.Empty;
                ScanBoxCodeClear++;
            }
            if (e.KeyCode == Keys.Enter) {
                try {
                    ScanBoxCodeClear = 0;
                    if (string.IsNullOrEmpty(this.txtinCode.Text)) {
                        MessageBoxForm.Show(this, "请先扫描单号！", "提示", this.DefaultButtonResults);
                        this.txtBoxCode.Text = string.Empty;
                        return;
                    } else {
                        string[] codeAndNum = this.txtBoxCode.Text.Split('|');
                        if (codeAndNum.Length < 2) {
                            MessageBoxForm.Show(this, "扫描配件箱码错误！", "提示", this.DefaultButtonResults);
                            this.txtBoxCode.Text = null;
                            return;
                        }
                        if (codeAndNum[codeAndNum.Length - 1] != "X") {
                            MessageBoxForm.Show(this, "扫描配件条码错误，必须为箱标！", "提示", this.DefaultButtonResults);
                            this.txtBoxCode.Text = null;
                            return;
                        }
                        if (!string.IsNullOrEmpty(this.sparePartCode)) {
                            if (!sparePartCode.Equals(codeAndNum[0])) {
                                MessageBoxForm.Show(this, "扫描配件箱码与任务单不一致！", "提示", this.DefaultButtonResults);
                                this.txtBoxCode.Text = null;
                                return;
                            }
                            this.txtSIHCode.Focus();
                            return;
                        }

                        this.IsLoading(true);
                        var result = ServiceManager.GetPackingTask(this.txtinCode.Text, codeAndNum[0]);
                        this.IsLoading(false);
                        if (result.IsSuccess == true && result.Data != null) {
                            packingTasks = result.Data;
                            if (packingTasks.Count == 0) {
                                MessageBoxForm.Show(this, "根据单号与配件编号，未找到有效的包装任务单，请检查单号条码是否有效！", "提示", this.DefaultButtonResults);
                                this.txtBoxCode.Text = string.Empty;
                                return;
                            }

                            if (!packingTasks[0].TraceProperty.HasValue || packingTasks[0].TraceProperty.Value == (int)DCSTraceProperty.批次追溯) {
                                this.txtTraceCode.Text = string.Join(",", packingTasks[0].TraceCodes.ToArray());
                            }

                            this.txtSIHCode.ReadOnly = false;
                            this.txtSIHCode.Enabled = true;
                            this.txtSIHCode.Focus();
                            this.btnSubmit.Enabled = true;

                            this.txtSparePartsName.Text = packingTasks[0].SparePartName;
                            this.txtTraceProperty.Text = this.TracePropertyValue(packingTasks[0].TraceProperty);
                            this.labSprit.Text = integerToInt(packingTasks[0].PackingQty) + "/" + packingTasks[0].PlanQty;
                            this.sparePartCode = codeAndNum[0];

                            if (packingTasks[0].PackingType == 1)
                                this.lblone.Text = "基本包装 " + integerToInt(packingTasks[0].PackingCoefficient) + "/" +
                                                   packingTasks[0].MeasureUnit;
                            if (packingTasks[0].PackingType == 2)
                                this.lbltwo.Text = "二级包装 " + integerToInt(packingTasks[0].PackingCoefficient) + "/" +
                                                   packingTasks[0].MeasureUnit;
                            if (packingTasks[0].PackingType == 3)
                                this.lblthree.Text = "三级包装 " + integerToInt(packingTasks[0].PackingCoefficient) + "/" + packingTasks[0].MeasureUnit;
                        } else {
                            MessageBoxForm.Show(this, "根据单号与配件编号，未找到包装任务单，请检查单号条码是否有效！", "提示", this.DefaultButtonResults);
                            this.txtBoxCode.Text = string.Empty;
                            return;
                        }
                    }
                } catch (Exception ex) {
                    MessageBoxForm.Show(this, ex.Message, "错误", this.DefaultButtonResults);
                    return;
                }
            }
        }

        private int ScanSIHCodeClear;
        private void txtSIHCode_KeyDown(object sender, KeyEventArgs e) {
            if (ScanSIHCodeClear == 0) {
                this.txtTraceCode.Text = string.Empty;
                ScanSIHCodeClear++;
            }
            if (e.KeyCode == Keys.Enter) {
                try {
                    ScanSIHCodeClear = 0;
                    if (string.IsNullOrEmpty(this.txtinCode.Text)) {
                        MessageBoxForm.Show(this, "请先扫描单号！", "提示", this.DefaultButtonResults);
                        this.txtSIHCode.Text = string.Empty;
                        return;
                    } else {
                        if (!string.IsNullOrEmpty(this.sparePartCode) && packingTasks[0].TraceProperty.Value == (int)DCSTraceProperty.精确追溯) {
                            if (this.txtTraceCode.Text.Length != 17) {
                                MessageBoxForm.Show(this, "追溯码必须为17位，请重新输入！", "提示", this.DefaultButtonResults);
                                this.txtTraceCode.Text = null;
                                return;
                            }
                            packingTasks[0].TraceCodes.Add(this.txtTraceCode.Text);

                            this.txtSIHCode.Focus();
                        }
                    }
                } catch (Exception ex) {
                    MessageBoxForm.Show(this, ex.Message, "错误", this.DefaultButtonResults);
                    return;
                }
            }
        }

        private string TracePropertyValue(int? type) {
            switch (type) {
                case (int)DCSTraceProperty.精确追溯:
                    return "精确追溯";
                case (int)DCSTraceProperty.批次追溯:
                    return "批次追溯";
                default:
                    return "";
            }
        }
    }
}