﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Sunlight.Ce.Controls;
using Sunlight.Ce.Entities.DTO;
using Sunlight.Ce.Service;
using Sunlight.Ce.Utils.Extends;


namespace Sunlight.Ce.WinUI {
    public partial class PartsShiftFrm : BaseFrm {

        public PartsShiftFrm() {
            InitializeComponent();
            this.Initialize();
        }

        private bool isInitialize = false;
        private List<PartsShiftOrderDTO> shiftOrders;

        private void Initialize() {
            if (!this.isInitialize) {
                this.isInitialize = true;

                this.dgvPartsShiftOrder.TableStyles.Clear();
                DataTable dt = this.SetDataSource();
                DataView dv = dt.DefaultView;
                dt = dv.ToTable();
                this.SetDataGridStyle(this.dgvPartsShiftOrder, dt);
                this.dgvPartsShiftOrder.DataSource = dt;

                this.txtCode.Text = string.Empty;
            }
        }

        private void SetDataGridStyle(DataGrid dataGrid, DataTable dataTable) {
            Color alternatingColor = SystemColors.ControlDark; //交替行颜色

            dataGrid.BackgroundColor = Color.White;
            dataGrid.RowHeadersVisible = false;

            var dataGridTableStyle = new DataGridTableStyle();
            dataGridTableStyle.MappingName = dataTable.TableName;

            dataGridTableStyle.GridColumnStyles.Add(new DataGridCustomTextBoxColumn {
                Owner = dataGrid,
                HeaderText = "移库单号",
                MappingName = dataTable.Columns[0].ColumnName,
                NullText = "",
                ReadOnly = true,
                Alignment = HorizontalAlignment.Center,
                AlternatingBackColor = alternatingColor,
                Width = 195
            });
            dataGridTableStyle.GridColumnStyles.Add(new DataGridCustomTextBoxColumn {
                Owner = dataGrid,
                HeaderText = "移库状态",
                MappingName = dataTable.Columns[1].ColumnName,
                NullText = "",
                ReadOnly = true,
                Alignment = HorizontalAlignment.Center,
                AlternatingBackColor = alternatingColor,
                Width = 70
            });

            dataGrid.TableStyles.Add(dataGridTableStyle);
        }

        private DataTable SetDataSource() {
            var dd = new DataTable();
            dd.TableName = "dgvPartsShiftOrder";
            dd.Columns.Add("移库单号");
            dd.Columns.Add("移库状态");
            if (shiftOrders != null) {
                foreach (var t in shiftOrders.GroupBy(o => o.Code).OrderBy(o => o.Key)) {
                    DataRow dr = dd.NewRow();
                    dr["移库单号"] = t.Key;
                    var shiftOrderDto = shiftOrders.FirstOrDefault(o => o.Code == t.Key);
                    dr["移库状态"] = JudgementShiftStatus(shiftOrderDto.ShiftStatus);
                    dd.Rows.Add(dr);
                }
            }

            return dd;
        }

        private string JudgementShiftStatus(int shiftStatus) {
            if (shiftStatus == 1)
                return "待移库";
            else if (shiftStatus == 2)
                return "部分移库";
            else
                return "移库完成";
        }

        //上架
        private void btnUp_Click(object sender, EventArgs e) {

            this.Text = "上架";
            this.IsLoading(true);
            MessageInfo<List<PartsShiftOrderDTO>> messageInfo = ServiceManager.GetUpPartsShift(this.txtCode.Text.Trim());
            this.IsLoading(false);

            if (messageInfo.IsSuccess)
                if (messageInfo.Data != null && messageInfo.Data.Any()) {
                    shiftOrders = messageInfo.Data;
                    if (shiftOrders == null)
                        shiftOrders = new List<PartsShiftOrderDTO>();
                    //界面参数
                    this.dgvPartsShiftOrder.TableStyles.Clear();
                    DataTable dt = this.SetDataSource();
                    DataView dv = dt.DefaultView;
                    dt = dv.ToTable();
                    this.SetDataGridStyle(this.dgvPartsShiftOrder, dt);
                    this.dgvPartsShiftOrder.DataSource = dt;

                    if (shiftOrders.GroupBy(o => o.Code).Count() == 1) {
                        var buf = new PartsShiftUpingFrm(shiftOrders);
                        buf.ShowDialog();
                        this.isInitialize = false;
                        shiftOrders = new List<PartsShiftOrderDTO>();
                        this.Initialize();
                        return;
                    }
                }
                else {
                    shiftOrders = new List<PartsShiftOrderDTO>();
                    this.dgvPartsShiftOrder.TableStyles.Clear();
                    DataTable dt = this.SetDataSource();
                    this.SetDataGridStyle(this.dgvPartsShiftOrder, dt);
                    this.dgvPartsShiftOrder.DataSource = dt;
                    MessageBoxForm.Show(this, "无查询结果", "提示", this.DefaultButtonResults);
                }
            else
                MessageBoxForm.Show(this, messageInfo.Message, "提示", this.DefaultButtonResults);

        }

        private void btnDown_Click(object sender, EventArgs e) {
            this.Text = "下架";
            this.IsLoading(true);
            MessageInfo<List<PartsShiftOrderDTO>> messageInfo = ServiceManager.GetDownPartsShift(this.txtCode.Text.Trim());
            this.IsLoading(false);

            if (messageInfo.IsSuccess)
                if (messageInfo.Data != null && messageInfo.Data.Any()) {
                    shiftOrders = messageInfo.Data;
                    if (shiftOrders == null)
                        shiftOrders = new List<PartsShiftOrderDTO>();
                    //界面参数
                    this.dgvPartsShiftOrder.TableStyles.Clear();
                    DataTable dt = this.SetDataSource();
                    DataView dv = dt.DefaultView;
                    dt = dv.ToTable();
                    this.SetDataGridStyle(this.dgvPartsShiftOrder, dt);
                    this.dgvPartsShiftOrder.DataSource = dt;

                    if (shiftOrders.GroupBy(o => o.Code).Count() == 1) {
                        var buf = new PartsShiftDowningFrm(shiftOrders);
                        buf.ShowDialog();
                        this.isInitialize = false;
                        shiftOrders = new List<PartsShiftOrderDTO>();
                        this.Initialize();
                        return;
                    }
                }
                else {
                    shiftOrders = new List<PartsShiftOrderDTO>();
                    this.dgvPartsShiftOrder.TableStyles.Clear();
                    DataTable dt = this.SetDataSource();
                    this.SetDataGridStyle(this.dgvPartsShiftOrder, dt);
                    this.dgvPartsShiftOrder.DataSource = dt;
                    MessageBoxForm.Show(this, "无查询结果", "提示", this.DefaultButtonResults);
                }
            else
                MessageBoxForm.Show(this, messageInfo.Message, "提示", this.DefaultButtonResults);
        }

        private void dgvPartsShiftOrder_DoubleClick(object sender, EventArgs e) {
            this.SelectDataGridRow(this.dgvPartsShiftOrder);
        }

        public void SelectDataGridRow(DataGrid dataGrid) {
            if (dataGrid.VisibleRowCount == 0)
                return;
            dataGrid.Select(dataGrid.CurrentRowIndex);
            if (dataGrid.DataSource != null && dataGrid.CurrentRowIndex != -1) {
                string code = dataGrid[dataGrid.CurrentRowIndex, 0].ToString();
                var newPartsShiftOrders = shiftOrders.Where(o => o.Code == code).ToList();
                if (newPartsShiftOrders.Count > 0) {
                    if (this.Text.Equals("上架")) {
                        var buf = new PartsShiftUpingFrm(newPartsShiftOrders);
                        buf.ShowDialog();
                        this.isInitialize = false;
                        shiftOrders = new List<PartsShiftOrderDTO>();
                        this.Initialize();
                    }
                    else {
                        var buf1 = new PartsShiftDowningFrm(newPartsShiftOrders);
                        buf1.ShowDialog();
                        this.isInitialize = false;
                        shiftOrders = new List<PartsShiftOrderDTO>();
                        this.Initialize();
                    }
                }
                else
                    MessageBoxForm.Show(this, "根据单号，为找到移库清单，请重新扫描单号！", "提示", this.DefaultButtonResults);
            }
        }
    }
}