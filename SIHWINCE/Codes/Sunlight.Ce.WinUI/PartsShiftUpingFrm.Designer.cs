﻿namespace Sunlight.Ce.WinUI
{
    partial class PartsShiftUpingFrm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.txtScanPartCode = new System.Windows.Forms.TextBox();
            this.lblScanPartCode = new System.Windows.Forms.Label();
            this.txtPartName = new System.Windows.Forms.TextBox();
            this.lblPartName = new System.Windows.Forms.Label();
            this.txtTraceProperty = new System.Windows.Forms.TextBox();
            this.lblTraceProperty = new System.Windows.Forms.Label();
            this.txtQuantity = new System.Windows.Forms.TextBox();
            this.lblQuantity = new System.Windows.Forms.Label();
            this.labSprit = new System.Windows.Forms.Label();
            this.btnUpload = new System.Windows.Forms.Button();
            this.lblPartCode = new System.Windows.Forms.Label();
            this.txtPartCode = new System.Windows.Forms.TextBox();
            this.lblAreaCode = new System.Windows.Forms.Label();
            this.txtAreaCode = new System.Windows.Forms.TextBox();
            this.btnNext = new System.Windows.Forms.Button();
            this.lblPageNumber = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtScanPartCode
            // 
            this.txtScanPartCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtScanPartCode.Location = new System.Drawing.Point(80, 141);
            this.txtScanPartCode.Name = "txtScanPartCode";
            this.txtScanPartCode.Size = new System.Drawing.Size(158, 26);
            this.txtScanPartCode.TabIndex = 55;
            this.txtScanPartCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtScanPartCode_KeyDown);
            // 
            // lblScanPartCode
            // 
            this.lblScanPartCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblScanPartCode.Location = new System.Drawing.Point(1, 143);
            this.lblScanPartCode.Name = "lblScanPartCode";
            this.lblScanPartCode.Size = new System.Drawing.Size(78, 27);
            this.lblScanPartCode.Text = "扫描配件";
            // 
            // txtPartName
            // 
            this.txtPartName.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtPartName.Location = new System.Drawing.Point(80, 45);
            this.txtPartName.Name = "txtPartName";
            this.txtPartName.ReadOnly = true;
            this.txtPartName.Size = new System.Drawing.Size(158, 26);
            this.txtPartName.TabIndex = 58;
            // 
            // lblPartName
            // 
            this.lblPartName.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblPartName.Location = new System.Drawing.Point(1, 47);
            this.lblPartName.Name = "lblPartName";
            this.lblPartName.Size = new System.Drawing.Size(118, 27);
            this.lblPartName.Text = "配件名称";
            // 
            // txtTraceProperty
            // 
            this.txtTraceProperty.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtTraceProperty.Location = new System.Drawing.Point(80, 77);
            this.txtTraceProperty.Name = "txtTraceProperty";
            this.txtTraceProperty.ReadOnly = true;
            this.txtTraceProperty.Size = new System.Drawing.Size(158, 26);
            this.txtTraceProperty.TabIndex = 61;
            // 
            // lblTraceProperty
            // 
            this.lblTraceProperty.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblTraceProperty.Location = new System.Drawing.Point(1, 79);
            this.lblTraceProperty.Name = "lblTraceProperty";
            this.lblTraceProperty.Size = new System.Drawing.Size(78, 27);
            this.lblTraceProperty.Text = "追溯属性";
            // 
            // txtQuantity
            // 
            this.txtQuantity.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtQuantity.Location = new System.Drawing.Point(80, 173);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Size = new System.Drawing.Size(70, 26);
            this.txtQuantity.TabIndex = 72;
            this.txtQuantity.TextChanged += new System.EventHandler(this.txtQuantity_TextChanged);
            // 
            // lblQuantity
            // 
            this.lblQuantity.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblQuantity.Location = new System.Drawing.Point(1, 175);
            this.lblQuantity.Name = "lblQuantity";
            this.lblQuantity.Size = new System.Drawing.Size(60, 25);
            this.lblQuantity.Text = "数量";
            // 
            // labSprit
            // 
            this.labSprit.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.labSprit.ForeColor = System.Drawing.SystemColors.WindowText;
            this.labSprit.Location = new System.Drawing.Point(176, 173);
            this.labSprit.Name = "labSprit";
            this.labSprit.Size = new System.Drawing.Size(60, 26);
            this.labSprit.Text = "/";
            // 
            // btnUpload
            // 
            this.btnUpload.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnUpload.Location = new System.Drawing.Point(3, 228);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(81, 35);
            this.btnUpload.TabIndex = 75;
            this.btnUpload.Text = "上传";
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // lblPartCode
            // 
            this.lblPartCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblPartCode.Location = new System.Drawing.Point(1, 15);
            this.lblPartCode.Name = "lblPartCode";
            this.lblPartCode.Size = new System.Drawing.Size(118, 27);
            this.lblPartCode.Text = "配件编号";
            // 
            // txtPartCode
            // 
            this.txtPartCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtPartCode.Location = new System.Drawing.Point(80, 13);
            this.txtPartCode.Name = "txtPartCode";
            this.txtPartCode.ReadOnly = true;
            this.txtPartCode.Size = new System.Drawing.Size(158, 26);
            this.txtPartCode.TabIndex = 84;
            // 
            // lblAreaCode
            // 
            this.lblAreaCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblAreaCode.Location = new System.Drawing.Point(1, 111);
            this.lblAreaCode.Name = "lblAreaCode";
            this.lblAreaCode.Size = new System.Drawing.Size(78, 27);
            this.lblAreaCode.Text = "库位";
            // 
            // txtAreaCode
            // 
            this.txtAreaCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtAreaCode.Location = new System.Drawing.Point(80, 109);
            this.txtAreaCode.Name = "txtAreaCode";
            this.txtAreaCode.ReadOnly = true;
            this.txtAreaCode.Size = new System.Drawing.Size(158, 26);
            this.txtAreaCode.TabIndex = 87;
            // 
            // btnNext
            // 
            this.btnNext.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnNext.Location = new System.Drawing.Point(90, 228);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(81, 35);
            this.btnNext.TabIndex = 88;
            this.btnNext.Text = "确认";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // lblPageNumber
            // 
            this.lblPageNumber.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lblPageNumber.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblPageNumber.Location = new System.Drawing.Point(176, 240);
            this.lblPageNumber.Name = "lblPageNumber";
            this.lblPageNumber.Size = new System.Drawing.Size(58, 23);
            this.lblPageNumber.Text = "/0";
            this.lblPageNumber.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // PartsShiftUpingFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 295);
            this.Controls.Add(this.lblPageNumber);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.txtAreaCode);
            this.Controls.Add(this.lblAreaCode);
            this.Controls.Add(this.txtPartCode);
            this.Controls.Add(this.lblPartCode);
            this.Controls.Add(this.btnUpload);
            this.Controls.Add(this.txtQuantity);
            this.Controls.Add(this.lblQuantity);
            this.Controls.Add(this.labSprit);
            this.Controls.Add(this.txtTraceProperty);
            this.Controls.Add(this.lblTraceProperty);
            this.Controls.Add(this.txtPartName);
            this.Controls.Add(this.lblPartName);
            this.Controls.Add(this.txtScanPartCode);
            this.Controls.Add(this.lblScanPartCode);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PartsShiftUpingFrm";
            this.Text = "上架";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtScanPartCode;
        private System.Windows.Forms.Label lblScanPartCode;
        private System.Windows.Forms.TextBox txtPartName;
        private System.Windows.Forms.Label lblPartName;
        private System.Windows.Forms.TextBox txtTraceProperty;
        private System.Windows.Forms.Label lblTraceProperty;
        private System.Windows.Forms.TextBox txtQuantity;
        private System.Windows.Forms.Label lblQuantity;
        private System.Windows.Forms.Label labSprit;
        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.Label lblPartCode;
        private System.Windows.Forms.TextBox txtPartCode;
        private System.Windows.Forms.Label lblAreaCode;
        private System.Windows.Forms.TextBox txtAreaCode;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Label lblPageNumber;
    }
}