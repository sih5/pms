﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Sunlight.Ce.Controls;
using Sunlight.Ce.Entities.DTO;
using Sunlight.Ce.Service;
using Sunlight.Ce.Utils;

namespace Sunlight.Ce.WinUI {
    public partial class PartsShiftUpingFrm : BaseFrm {
        private List<PartsShiftOrderDTO> shiftOrders;
        private bool isInitialize;
        private int index = 0;

        public PartsShiftUpingFrm(List<PartsShiftOrderDTO> shiftOrdersPara) {
            InitializeComponent();

            shiftOrders = shiftOrdersPara;
            this.Initialize();
        }

        private void Initialize() {
            if (!this.isInitialize) {
                this.isInitialize = true;

                this.btnNext.Enabled = true;
                this.txtPartCode.Text = shiftOrders[index].SparePartCode;
                this.txtPartName.Text = shiftOrders[index].SparePartName;
                this.txtTraceProperty.Text = this.TracePropertyValue(shiftOrders[index].TraceProperty);
                this.txtAreaCode.Text = shiftOrders[index].DestWarehouseAreaCode;
                this.labSprit.Text = (shiftOrders[index].UpShelfQty ?? 0).ToString() + "/" + shiftOrders[index].DownShelfQty.ToString();
                this.lblPageNumber.Text = index + 1 + "/" + shiftOrders.Count;
                
                if (index + 1 == shiftOrders.Count)
                    this.btnNext.Enabled = false;

                this.txtScanPartCode.Focus();

            }
        }

        private string TracePropertyValue(int? type) {
            switch (type) {
                case (int)DCSTraceProperty.精确追溯:
                    return "精确追溯";
                case (int)DCSTraceProperty.批次追溯:
                    return "批次追溯";
                default:
                    return "";
            }
        }

        private int ScanTicketClear = 0;
        private void txtScanPartCode_KeyDown(object sender, KeyEventArgs e) {
            if (ScanTicketClear == 0) {
                this.txtScanPartCode.Text = string.Empty;
                ScanTicketClear++;
            }
            if (e.KeyCode == Keys.Enter) {
                try {
                    ScanTicketClear = 0;

                    string[] codeAndNum = this.txtScanPartCode.Text.Split('|');
                    if (codeAndNum.Length < 2) {
                        MessageBoxForm.Show(this, "扫描配件条码错误！", "提示", this.DefaultButtonResults);
                        this.txtScanPartCode.Text = null;
                        return;
                    }

                    if (codeAndNum[0].Equals(shiftOrders[index].SparePartCode)) {
                        if (shiftOrders[index].TraceProperty.HasValue && shiftOrders[index].TraceProperty.Value == (int)DCSTraceProperty.精确追溯)
                            this.txtQuantity.ReadOnly = true;
                        else
                            this.txtQuantity.ReadOnly = false;                        
                    }
                    else {
                        MessageBoxForm.Show(this, "扫描配件条码错误！", "提示", this.DefaultButtonResults);
                        this.txtScanPartCode.Text = null;
                    }                    

                    var SIHCodeTmp = shiftOrders[index].PartsShiftSIHDetails.FirstOrDefault(o => o.SIHCode == this.txtScanPartCode.Text);
                    if (SIHCodeTmp == null) {
                        var partsShiftSIHDetail = new PartsShiftSIHDetail();
                        partsShiftSIHDetail.SIHCode = this.txtScanPartCode.Text;
                        partsShiftSIHDetail.Quantity = Convert.ToInt32(codeAndNum[1]);
                        shiftOrders[index].PartsShiftSIHDetails.Add(partsShiftSIHDetail);

                        var Quantitytmp = string.IsNullOrEmpty(this.txtQuantity.Text) ? 0 : Convert.ToInt32(this.txtQuantity.Text);
                        this.txtQuantity.Text = (Quantitytmp + Convert.ToInt32(codeAndNum[1])).ToString();
                    }
                    else {
                        if (SIHCodeTmp.SIHCode.Contains(this.txtScanPartCode.Text) && shiftOrders[index].TraceProperty.HasValue &&
                            shiftOrders[index].TraceProperty.Value == (int)DCSTraceProperty.精确追溯) {
                            MessageBoxForm.Show(this, "当前标签码已被扫描！", "提示", this.DefaultButtonResults);
                            this.txtScanPartCode.Text = null;
                            return;
                        }
                        var Quantitytmp = string.IsNullOrEmpty(this.txtQuantity.Text) ? 0 : Convert.ToInt32(this.txtQuantity.Text);
                        this.txtQuantity.Text = (Quantitytmp + Convert.ToInt32(codeAndNum[1])).ToString();

                        SIHCodeTmp.Quantity = Convert.ToInt32(this.txtQuantity.Text) - shiftOrders[index].PartsShiftSIHDetails.Where(o => o.SIHCode != this.txtScanPartCode.Text).Sum(r => r.Quantity);
                    }                                     
                }
                catch (Exception ex) {
                    MessageBoxForm.Show(this, ex.Message, "错误", this.DefaultButtonResults);
                    return;
                }
            }
        }

        private void txtQuantity_TextChanged(object sender, EventArgs e) {
            try {
                var rgx = new Regex(@"^\d+$");
                var tempQuantity = sender as TextBox;
                if (tempQuantity != null && !string.IsNullOrEmpty(tempQuantity.Text)) {
                    var thisNumber = tempQuantity.Text;
                    if (!rgx.IsMatch(thisNumber)) {
                        MessageBoxForm.Show(this, "你输入的不是数字！", "提示", this.DefaultButtonResults);
                        this.txtQuantity.Text = null;
                        return;
                    }

                    if ((shiftOrders[index].UpShelfQty ?? 0) + Convert.ToInt32(thisNumber.Trim()) > shiftOrders[index].DownShelfQty) {
                        MessageBoxForm.Show(this, "上架数量不能大于下架数量！", "提示", this.DefaultButtonResults);
                        this.txtQuantity.Text = (shiftOrders[index].DownShelfQty - (shiftOrders[index].UpShelfQty ?? 0)).ToString();
                        return;
                    }
                    shiftOrders[index].ThisUpShelfQty = Convert.ToInt32(thisNumber.Trim());

                    var SIHCodeTmp = shiftOrders[index].PartsShiftSIHDetails.FirstOrDefault(o => o.SIHCode == this.txtScanPartCode.Text);
                    SIHCodeTmp.Quantity = Convert.ToInt32(this.txtQuantity.Text) - shiftOrders[index].PartsShiftSIHDetails.Where(o => o.SIHCode != this.txtScanPartCode.Text).Sum(r => r.Quantity);

                }
            }
            catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }

        private void btnUpload_Click(object sender, EventArgs e) {
            try {
                if (shiftOrders != null && shiftOrders.Count > 0) {

                    var number = shiftOrders.FindIndex(o => o.ThisUpShelfQty > 0);
                    if (number == -1) {
                        MessageBoxForm.Show(this, "本次提交内容，全部没有填写上架数量！", "提示", this.DefaultButtonResults);
                        return;
                    }
                    this.IsLoading(true);
                    var result = ServiceManager.PartsShiftUping(shiftOrders.Where(o => o.ThisUpShelfQty > 0).ToList());

                    this.IsLoading(false);
                    if (result.IsSuccess) {
                        MessageBoxForm.Show(this, result.Message, "提示", this.DefaultButtonResults);

                        if (result.Data) {
                            RasieEditSubmitted();
                            this.Close();
                            return;
                        }
                    }
                    else
                        MessageBoxForm.Show(this, result.Message, "提示", this.DefaultButtonResults);
                }
            }
            catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }

        private void btnNext_Click(object sender, EventArgs e) {
            this.index++;
            this.txtPartCode.Text = shiftOrders[index].SparePartCode;
            this.txtPartName.Text = shiftOrders[index].SparePartName;
            this.txtTraceProperty.Text = this.TracePropertyValue(shiftOrders[index].TraceProperty);
            this.txtAreaCode.Text = shiftOrders[index].DestWarehouseAreaCode;
            this.labSprit.Text = (shiftOrders[index].UpShelfQty ?? 0).ToString() + "/" + shiftOrders[index].DownShelfQty.ToString();
            this.lblPageNumber.Text = index + 1 + "/" + shiftOrders.Count;

            this.txtScanPartCode.Text = string.Empty;            
            this.txtQuantity.Text = string.Empty;
            this.txtScanPartCode.Focus();

            if (index + 1 == shiftOrders.Count)
                this.btnNext.Enabled = false;
        }
    }
}