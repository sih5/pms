﻿namespace Sunlight.Ce.WinUI {
    partial class PickingFrm {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent() {
            this.txtRecommendLocation = new System.Windows.Forms.TextBox();
            this.txtSparePartsCode = new System.Windows.Forms.TextBox();
            this.lblRecommendLocation = new System.Windows.Forms.Label();
            this.lblSparePartsCode = new System.Windows.Forms.Label();
            this.lblSparePartsName = new System.Windows.Forms.Label();
            this.txtSparePartsName = new System.Windows.Forms.TextBox();
            this.txtScanLocation = new System.Windows.Forms.TextBox();
            this.lblScanLocation = new System.Windows.Forms.Label();
            this.txtScanSparePartCode = new System.Windows.Forms.TextBox();
            this.lblSparePartCode = new System.Windows.Forms.Label();
            this.txtThisShelvesAmount = new System.Windows.Forms.TextBox();
            this.lblThisShelvesAmount = new System.Windows.Forms.Label();
            this.labSprit = new System.Windows.Forms.Label();
            this.btnUpload = new System.Windows.Forms.Button();
            this.btnOver = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.lblPageNumber = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTraceProperty = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtRecommendLocation
            // 
            this.txtRecommendLocation.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRecommendLocation.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtRecommendLocation.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtRecommendLocation.Location = new System.Drawing.Point(71, 142);
            this.txtRecommendLocation.Name = "txtRecommendLocation";
            this.txtRecommendLocation.ReadOnly = true;
            this.txtRecommendLocation.Size = new System.Drawing.Size(167, 26);
            this.txtRecommendLocation.TabIndex = 49;
            // 
            // txtSparePartsCode
            // 
            this.txtSparePartsCode.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSparePartsCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtSparePartsCode.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtSparePartsCode.Location = new System.Drawing.Point(71, 78);
            this.txtSparePartsCode.Name = "txtSparePartsCode";
            this.txtSparePartsCode.ReadOnly = true;
            this.txtSparePartsCode.Size = new System.Drawing.Size(167, 26);
            this.txtSparePartsCode.TabIndex = 48;
            // 
            // lblRecommendLocation
            // 
            this.lblRecommendLocation.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblRecommendLocation.Location = new System.Drawing.Point(-1, 144);
            this.lblRecommendLocation.Name = "lblRecommendLocation";
            this.lblRecommendLocation.Size = new System.Drawing.Size(95, 27);
            this.lblRecommendLocation.Text = "推荐货位";
            // 
            // lblSparePartsCode
            // 
            this.lblSparePartsCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblSparePartsCode.Location = new System.Drawing.Point(-1, 80);
            this.lblSparePartsCode.Name = "lblSparePartsCode";
            this.lblSparePartsCode.Size = new System.Drawing.Size(95, 27);
            this.lblSparePartsCode.Text = "零件编号";
            // 
            // lblSparePartsName
            // 
            this.lblSparePartsName.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblSparePartsName.Location = new System.Drawing.Point(-1, 47);
            this.lblSparePartsName.Name = "lblSparePartsName";
            this.lblSparePartsName.Size = new System.Drawing.Size(95, 27);
            this.lblSparePartsName.Text = "零件名称";
            // 
            // txtSparePartsName
            // 
            this.txtSparePartsName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSparePartsName.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtSparePartsName.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtSparePartsName.Location = new System.Drawing.Point(71, 45);
            this.txtSparePartsName.Name = "txtSparePartsName";
            this.txtSparePartsName.ReadOnly = true;
            this.txtSparePartsName.Size = new System.Drawing.Size(167, 26);
            this.txtSparePartsName.TabIndex = 48;
            // 
            // txtScanLocation
            // 
            this.txtScanLocation.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtScanLocation.Location = new System.Drawing.Point(69, 174);
            this.txtScanLocation.Name = "txtScanLocation";
            this.txtScanLocation.Size = new System.Drawing.Size(169, 26);
            this.txtScanLocation.TabIndex = 54;
            this.txtScanLocation.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtScanLocation_KeyDown);
            // 
            // lblScanLocation
            // 
            this.lblScanLocation.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblScanLocation.Location = new System.Drawing.Point(0, 178);
            this.lblScanLocation.Name = "lblScanLocation";
            this.lblScanLocation.Size = new System.Drawing.Size(92, 20);
            this.lblScanLocation.Text = "扫描货位";
            // 
            // txtScanSparePartCode
            // 
            this.txtScanSparePartCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtScanSparePartCode.Location = new System.Drawing.Point(69, 207);
            this.txtScanSparePartCode.Name = "txtScanSparePartCode";
            this.txtScanSparePartCode.Size = new System.Drawing.Size(169, 26);
            this.txtScanSparePartCode.TabIndex = 57;
            this.txtScanSparePartCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtScanSparePartCode_KeyDown);
            // 
            // lblSparePartCode
            // 
            this.lblSparePartCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblSparePartCode.Location = new System.Drawing.Point(0, 211);
            this.lblSparePartCode.Name = "lblSparePartCode";
            this.lblSparePartCode.Size = new System.Drawing.Size(92, 20);
            this.lblSparePartCode.Text = "扫描配件";
            // 
            // txtThisShelvesAmount
            // 
            this.txtThisShelvesAmount.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtThisShelvesAmount.Location = new System.Drawing.Point(69, 237);
            this.txtThisShelvesAmount.Name = "txtThisShelvesAmount";
            this.txtThisShelvesAmount.Size = new System.Drawing.Size(84, 26);
            this.txtThisShelvesAmount.TabIndex = 61;
            this.txtThisShelvesAmount.TextChanged += new System.EventHandler(this.txtThisShelvesAmount_TextChanged);
            // 
            // lblThisShelvesAmount
            // 
            this.lblThisShelvesAmount.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblThisShelvesAmount.Location = new System.Drawing.Point(3, 239);
            this.lblThisShelvesAmount.Name = "lblThisShelvesAmount";
            this.lblThisShelvesAmount.Size = new System.Drawing.Size(60, 25);
            this.lblThisShelvesAmount.Text = "数量";
            // 
            // labSprit
            // 
            this.labSprit.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.labSprit.Location = new System.Drawing.Point(177, 239);
            this.labSprit.Name = "labSprit";
            this.labSprit.Size = new System.Drawing.Size(60, 26);
            this.labSprit.Text = "/";
            // 
            // btnUpload
            // 
            this.btnUpload.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnUpload.Location = new System.Drawing.Point(5, 3);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(71, 35);
            this.btnUpload.TabIndex = 64;
            this.btnUpload.Text = "上传";
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // btnOver
            // 
            this.btnOver.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnOver.Location = new System.Drawing.Point(4, 3);
            this.btnOver.Name = "btnOver";
            this.btnOver.Size = new System.Drawing.Size(84, 35);
            this.btnOver.TabIndex = 64;
            this.btnOver.Text = "拣货完成";
            this.btnOver.Visible = false;
            this.btnOver.Click += new System.EventHandler(this.btnOver_Click);
            // 
            // btnNext
            // 
            this.btnNext.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnNext.Location = new System.Drawing.Point(94, 4);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(71, 35);
            this.btnNext.TabIndex = 64;
            this.btnNext.Text = "确认";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // lblPageNumber
            // 
            this.lblPageNumber.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lblPageNumber.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblPageNumber.Location = new System.Drawing.Point(177, 9);
            this.lblPageNumber.Name = "lblPageNumber";
            this.lblPageNumber.Size = new System.Drawing.Size(58, 23);
            this.lblPageNumber.Text = "/0";
            this.lblPageNumber.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.label1.Location = new System.Drawing.Point(-4, 112);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 27);
            this.label1.Text = "追溯属性";
            // 
            // txtTraceProperty
            // 
            this.txtTraceProperty.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTraceProperty.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtTraceProperty.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtTraceProperty.Location = new System.Drawing.Point(68, 110);
            this.txtTraceProperty.Name = "txtTraceProperty";
            this.txtTraceProperty.ReadOnly = true;
            this.txtTraceProperty.Size = new System.Drawing.Size(167, 26);
            this.txtTraceProperty.TabIndex = 48;
            // 
            // PickingFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 295);
            this.Controls.Add(this.btnUpload);
            this.Controls.Add(this.lblPageNumber);
            this.Controls.Add(this.btnOver);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.txtThisShelvesAmount);
            this.Controls.Add(this.lblThisShelvesAmount);
            this.Controls.Add(this.labSprit);
            this.Controls.Add(this.txtScanSparePartCode);
            this.Controls.Add(this.lblSparePartCode);
            this.Controls.Add(this.txtScanLocation);
            this.Controls.Add(this.lblScanLocation);
            this.Controls.Add(this.txtRecommendLocation);
            this.Controls.Add(this.txtSparePartsName);
            this.Controls.Add(this.txtTraceProperty);
            this.Controls.Add(this.txtSparePartsCode);
            this.Controls.Add(this.lblSparePartsName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblRecommendLocation);
            this.Controls.Add(this.lblSparePartsCode);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PickingFrm";
            this.Text = "拣货";
            this.Load += new System.EventHandler(this.PickingFrm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtRecommendLocation;
        private System.Windows.Forms.TextBox txtSparePartsCode;
        private System.Windows.Forms.Label lblRecommendLocation;
        private System.Windows.Forms.Label lblSparePartsCode;
        private System.Windows.Forms.Label lblSparePartsName;
        private System.Windows.Forms.TextBox txtSparePartsName;
        private System.Windows.Forms.TextBox txtScanLocation;
        private System.Windows.Forms.Label lblScanLocation;
        private System.Windows.Forms.TextBox txtScanSparePartCode;
        private System.Windows.Forms.Label lblSparePartCode;
        private System.Windows.Forms.TextBox txtThisShelvesAmount;
        private System.Windows.Forms.Label lblThisShelvesAmount;
        private System.Windows.Forms.Label labSprit;
        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.Button btnOver;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Label lblPageNumber;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTraceProperty;
    }
}