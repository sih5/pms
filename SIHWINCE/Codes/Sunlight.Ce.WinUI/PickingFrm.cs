﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Sunlight.Ce.Controls;
using Sunlight.Ce.Entities.DTO;
using Sunlight.Ce.Service;
using Sunlight.Ce.Utils;

namespace Sunlight.Ce.WinUI {
    public partial class PickingFrm : BaseFrm {
        private bool isInitialize;
        private List<PickingTaskDTO> pickingTasks;
        private int index = 0;
        private string code;
        private int warehouseAreaId;
        public PickingFrm(string codePara, int warehouseAreaIdPara) {
            InitializeComponent();

            code = codePara;
            warehouseAreaId = warehouseAreaIdPara;
        }

        private void Initialize() {
            if (!this.isInitialize) {
                this.isInitialize = true;

                var result = ServiceManager.GetPickingDetails(code, warehouseAreaId);
                if (result.IsSuccess && result.Data.Count > 0) {
                    pickingTasks = result.Data;

                    this.Text = "拣货 - " + pickingTasks[index].CounterpartCompanyName;
                    this.txtThisShelvesAmount.ReadOnly = true;
                    this.txtThisShelvesAmount.Enabled = false;
                    this.btnNext.Enabled = true;
                    this.txtSparePartsCode.Text = pickingTasks[index].SparePartCode;
                    this.txtSparePartsName.Text = pickingTasks[index].SparePartName;
                    this.txtTraceProperty.Text = this.TracePropertyValue(pickingTasks[index].TraceProperty);
                    this.txtRecommendLocation.Text = pickingTasks[index].WarehouseAreaCode;
                    this.labSprit.Text = pickingTasks[index].PickingQty + "/" + pickingTasks[index].PlanQty;
                    this.lblPageNumber.Text = index + 1 + "/" + pickingTasks.Count;
                    this.txtScanLocation.Focus();
                    if (index + 1 == pickingTasks.Count)
                        this.btnNext.Enabled = false;
                } else {
                    MessageBoxForm.Show(this, "根据单号未查到有效的数据！", "提示", this.DefaultButtonResults);
                    //RasieEditSubmitted();
                    this.Close();
                }
            }
        }

        private int ScanSparePartCodeClear = 0;
        private void txtScanSparePartCode_KeyDown(object sender, KeyEventArgs e) {
            if (ScanSparePartCodeClear == 0) {
                this.txtScanSparePartCode.Text = string.Empty;
                ScanSparePartCodeClear++;
            }
            if (e.KeyCode == Keys.Enter) {
                ScanSparePartCodeClear = 0;
                string[] codeAndNum = this.txtScanSparePartCode.Text.Split('|');
                if (codeAndNum.Length < 2) {
                    MessageBoxForm.Show(this, "扫描配件条码错误！", "提示", this.DefaultButtonResults);
                    this.txtScanSparePartCode.Text = null;
                    return;
                }

                if (codeAndNum[0].Equals(pickingTasks[index].SparePartCode)) {
                    if (!pickingTasks[index].TraceProperty.HasValue || pickingTasks[index].TraceProperty.Value != (int)DCSTraceProperty.精确追溯) {
                        this.txtThisShelvesAmount.ReadOnly = false;
                        this.txtThisShelvesAmount.Enabled = true;
                        this.txtThisShelvesAmount.Focus();
                    }
                } else {
                    MessageBoxForm.Show(this, "扫描配件条码错误！", "提示", this.DefaultButtonResults);
                    this.txtScanSparePartCode.Text = null;
                }
                if (pickingTasks[index].TraceProperty.HasValue && pickingTasks[index].TraceProperty.Value == (int)DCSTraceProperty.精确追溯) {
                    if (pickingTasks[index].SIHCodes.Contains(this.txtScanSparePartCode.Text)) {
                        MessageBoxForm.Show(this, "当前标签码已被扫描！", "提示", this.DefaultButtonResults);
                        this.txtScanSparePartCode.Text = null;
                        return;
                    }
                }
                if (!pickingTasks[index].SIHCodes.Contains(this.txtScanSparePartCode.Text))
                    pickingTasks[index].SIHCodes.Add(this.txtScanSparePartCode.Text);
                this.txtThisShelvesAmount.Text = string.IsNullOrEmpty(this.txtThisShelvesAmount.Text) ? codeAndNum[1] : (Convert.ToInt32(this.txtThisShelvesAmount.Text) + Convert.ToInt32(codeAndNum[1])).ToString();
            }
        }

        private void btnUpload_Click(object sender, EventArgs e) {
            try {
                if (pickingTasks != null && pickingTasks.Count > 0) {
                    var number = pickingTasks.FindIndex(o => (o.ThisPickingQty ?? 0) > 0);
                    if (number == -1) {
                        MessageBoxForm.Show(this, "本次提交内容，全部没有填写拣货量！", "提示", this.DefaultButtonResults);
                        return;
                    }
                    this.IsLoading(true);
                    var result = ServiceManager.Picking(pickingTasks);
                    this.IsLoading(false);
                    if (result.IsSuccess) {
                        MessageBoxForm.Show(this, result.Message, "提示", this.DefaultButtonResults);
                        RasieEditSubmitted();
                        this.Close();
                    } else {
                        MessageBoxForm.Show(this, result.Message, "提示", this.DefaultButtonResults);
                    }
                }
            } catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }

        private void btnOver_Click(object sender, EventArgs e) {
            try {
                if (pickingTasks != null && pickingTasks.Count > 0) {
                    this.IsLoading(true);
                    var result = ServiceManager.PickingOver(pickingTasks);
                    this.IsLoading(false);
                    if (result.IsSuccess) {
                        MessageBoxForm.Show(this, result.Message, "提示", this.DefaultButtonResults);
                        RasieEditSubmitted();
                        this.Close();
                    } else {
                        MessageBoxForm.Show(this, result.Message, "提示", this.DefaultButtonResults);
                    }
                }
            } catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }

        private void btnNext_Click(object sender, EventArgs e) {
            this.index++;
            this.lblPageNumber.Text = index + 1 + "/" + pickingTasks.Count;

            this.txtSparePartsCode.Text = pickingTasks[index].SparePartCode;
            this.txtSparePartsName.Text = pickingTasks[index].SparePartName;
            this.txtRecommendLocation.Text = pickingTasks[index].WarehouseAreaCode;
            this.labSprit.Text = pickingTasks[index].PickingQty + "/" + pickingTasks[index].PlanQty;
            this.txtScanLocation.Text = string.Empty;
            this.txtScanSparePartCode.Text = string.Empty;
            this.txtThisShelvesAmount.Text = string.Empty;
            this.txtScanLocation.Focus();
            this.txtThisShelvesAmount.ReadOnly = true;
            this.txtThisShelvesAmount.Enabled = false;
            if (index + 1 == pickingTasks.Count)
                this.btnNext.Enabled = false;
        }

        private void PickingFrm_Load(object sender, EventArgs e) {
            this.Initialize();
        }

        private void txtScanLocation_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                if (pickingTasks[index].WarehouseAreaCode.Equals(this.txtScanLocation.Text.Trim())) {
                    this.txtScanSparePartCode.Focus();
                } else {
                    MessageBoxForm.Show(this, "扫描货位错误！", "提示", this.DefaultButtonResults);
                    this.txtScanLocation.Text = null;
                }
            }
        }

        private void txtThisShelvesAmount_TextChanged(object sender, EventArgs e) {
            try {
                var rgx = new Regex(@"^\d+$");
                var tempTxtshippingAmount = sender as TextBox;
                if (tempTxtshippingAmount != null && !string.IsNullOrEmpty(tempTxtshippingAmount.Text)) {
                    var thisNumber = tempTxtshippingAmount.Text;
                    if (!rgx.IsMatch(thisNumber)) {
                        MessageBoxForm.Show(this, "你输入的不是数字！", "提示", this.DefaultButtonResults);
                        this.txtThisShelvesAmount.Text = null;
                        return;
                    }
                    if (Convert.ToInt32(thisNumber.Trim()) + integerToInt(pickingTasks[index].PickingQty) > pickingTasks[index].PlanQty) {//不能大于任务计划量  
                        MessageBoxForm.Show(this, "数量不能大于计划量", "提示", this.DefaultButtonResults);
                        this.txtThisShelvesAmount.Text = (pickingTasks[index].PlanQty - integerToInt(pickingTasks[index].PickingQty)).ToString();
                        return;
                    }
                    pickingTasks[index].ThisPickingQty = Convert.ToInt32(thisNumber.Trim());
                }
            } catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }

        private string TracePropertyValue(int? type) {
            switch (type) {
                case (int)DCSTraceProperty.精确追溯:
                    return "精确追溯";
                case (int)DCSTraceProperty.批次追溯:
                    return "批次追溯";
                default:
                    return "";
            }
        }
    }
}