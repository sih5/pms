﻿namespace Sunlight.Ce.WinUI {
    partial class PickingManagementFrm {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent() {
            this.lblCode = new System.Windows.Forms.Label();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.cmbWarehouseArea = new System.Windows.Forms.ComboBox();
            this.lblPrompt = new System.Windows.Forms.Label();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblCode
            // 
            this.lblCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblCode.Location = new System.Drawing.Point(16, 17);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(192, 46);
            this.lblCode.Text = "先选择作业库区后，点击确定";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(171, 66);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(64, 24);
            this.btnRefresh.TabIndex = 5;
            this.btnRefresh.Text = "刷新";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // cmbWarehouseArea
            // 
            this.cmbWarehouseArea.Location = new System.Drawing.Point(16, 67);
            this.cmbWarehouseArea.Name = "cmbWarehouseArea";
            this.cmbWarehouseArea.Size = new System.Drawing.Size(137, 23);
            this.cmbWarehouseArea.TabIndex = 4;
            this.cmbWarehouseArea.TextChanged += new System.EventHandler(this.cmbWarehouseArea_TextChanged);
            // 
            // lblPrompt
            // 
            this.lblPrompt.Location = new System.Drawing.Point(96, 167);
            this.lblPrompt.Name = "lblPrompt";
            this.lblPrompt.Size = new System.Drawing.Size(131, 20);
            // 
            // btnSubmit
            // 
            this.btnSubmit.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnSubmit.Location = new System.Drawing.Point(45, 204);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(138, 42);
            this.btnSubmit.TabIndex = 56;
            this.btnSubmit.Text = "确    定";
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // PickingManagemenetFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 295);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.lblPrompt);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.cmbWarehouseArea);
            this.Controls.Add(this.lblCode);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PickingManagemenetFrm";
            this.Text = "拣货-选择库存";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblCode;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.ComboBox cmbWarehouseArea;
        private System.Windows.Forms.Label lblPrompt;
        private System.Windows.Forms.Button btnSubmit;

    }
}