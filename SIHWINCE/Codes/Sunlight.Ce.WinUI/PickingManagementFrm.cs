﻿using System;
using System.Collections.Generic;
using Sunlight.Ce.Entities.DTO;
using Sunlight.Ce.Service;

namespace Sunlight.Ce.WinUI {
    public partial class PickingManagementFrm : BaseFrm {
        private List<PickingWarehouseAreaDTO> pickingWarehouseAreas;

        public PickingManagementFrm() {
            InitializeComponent();

            dataSourceBinding();
        }

        private void btnSubmit_Click(object sender, EventArgs e) {
            var pickingWarehouseArea = pickingWarehouseAreas.Find(o => o.WarehouseAreaCode == this.cmbWarehouseArea.Text);
            if (pickingWarehouseArea.WarehouseAreaId != null) {
                var pst = new PickingSelectTaskFrm(pickingWarehouseArea.WarehouseAreaId.Value);
                pst.ShowDialog();
                dataSourceBinding();
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e) {
            dataSourceBinding();
        }

        /// <summary>
        /// 查询未完成拣货单中的货位
        /// </summary>
        public void dataSourceBinding() {
            this.IsLoading(true);
            var messageInfo = ServiceManager.GetPickingAreas();

            if (messageInfo.Data != null && messageInfo.Data.Count > 0) {
                SetDataSourceBinding(messageInfo.Data);
            }
            else if (messageInfo.Data == null || messageInfo.Data.Count <= 0) {
                SetDataSourceBinding(new List<PickingWarehouseAreaDTO>());
            }
            this.IsLoading(false);
        }

        /// <summary>
        /// 绑定下拉框数值
        /// </summary>
        /// <param name="pickingWarehouseArea"></param>
        private void SetDataSourceBinding(List<PickingWarehouseAreaDTO> pickingWarehouseArea) {
            pickingWarehouseAreas = pickingWarehouseArea;
            this.cmbWarehouseArea.DataSource = pickingWarehouseArea;
            this.cmbWarehouseArea.DisplayMember = "WarehouseAreaCode";
            this.cmbWarehouseArea.ValueMember = "WarehouseAreaId";

            if (pickingWarehouseArea != null && pickingWarehouseArea.Count > 0)
                this.lblPrompt.Text = pickingWarehouseArea[0].WarehouseAreaCode + "区共用" + pickingWarehouseArea[0].TaskNumber + "个任务";
        }

        private void cmbWarehouseArea_TextChanged(object sender, EventArgs e) {
            if (!string.IsNullOrEmpty(this.cmbWarehouseArea.Text)) {
                var pickingWarehouseArea = pickingWarehouseAreas.Find(o => o.WarehouseAreaCode == this.cmbWarehouseArea.Text);
                this.lblPrompt.Text = pickingWarehouseArea.WarehouseAreaCode + "区共用" + pickingWarehouseArea.TaskNumber + "个任务";
            }
        }
    }
}