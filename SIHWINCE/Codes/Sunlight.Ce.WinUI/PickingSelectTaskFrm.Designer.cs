﻿namespace Sunlight.Ce.WinUI {
    partial class PickingSelectTaskFrm {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent() {
            this.lblCode = new System.Windows.Forms.Label();
            this.dgvPicking = new System.Windows.Forms.DataGrid();
            this.SuspendLayout();
            // 
            // lblCode
            // 
            this.lblCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblCode.Location = new System.Drawing.Point(12, 16);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(213, 26);
            this.lblCode.Text = "选择一张任务单开始拣货";
            // 
            // dgvPicking
            // 
            this.dgvPicking.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dgvPicking.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.dgvPicking.Location = new System.Drawing.Point(0, 45);
            this.dgvPicking.Name = "dgvPicking";
            this.dgvPicking.Size = new System.Drawing.Size(238, 224);
            this.dgvPicking.TabIndex = 7;
            this.dgvPicking.LostFocus += new System.EventHandler(this.dgvPicking_LostFocus);
            this.dgvPicking.DoubleClick += new System.EventHandler(this.dgvPicking_DoubleClick);
            this.dgvPicking.GotFocus += new System.EventHandler(this.dgvPicking_GotFocus);
            this.dgvPicking.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvPicking_KeyDown);
            // 
            // PickingSelectTaskFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 295);
            this.Controls.Add(this.dgvPicking);
            this.Controls.Add(this.lblCode);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PickingSelectTaskFrm";
            this.Text = "拣货-选择任务";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblCode;
        private System.Windows.Forms.DataGrid dgvPicking;
    }
}