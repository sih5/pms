﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Sunlight.Ce.Controls;
using Sunlight.Ce.Entities.DTO;
using Sunlight.Ce.Service;

namespace Sunlight.Ce.WinUI {
    public partial class PickingSelectTaskFrm : BaseFrm {
        private bool isInitialize;
        private int? warehouseAreaId;
        public PickingSelectTaskFrm(int warehouseAreaIdPara) {
            InitializeComponent();

            this.warehouseAreaId = warehouseAreaIdPara;
            Initialize(warehouseAreaIdPara);
        }

        private void Initialize(int warehouseAreaIdPara) {
            if (!this.isInitialize) {
                this.isInitialize = true;

                var dd = this.SetDataSource(warehouseAreaIdPara);
                DataView dv = dd.DefaultView;
                dv.Sort = "单号 Asc";
                dv.Sort = "类型 Desc";
                dd = dv.ToTable();
                this.dgvPicking.TableStyles.Clear();
                this.SetDataGridStyle(this.dgvPicking, dd);
                this.dgvPicking.DataSource = dd;
            }
        }

        private DataTable SetDataSource(int warehouseAreaIdPara) {
            var dd = new DataTable();
            dd.TableName = "dgvPicking";
            dd.Columns.Add("单号");
            dd.Columns.Add("类型");

            this.IsLoading(true);
            var messageInfo = ServiceManager.GetPickings(warehouseAreaIdPara);
            this.IsLoading(false);
            if (messageInfo.Data != null) {
                foreach (var t in messageInfo.Data) {
                    DataRow dr = dd.NewRow();
                    dr["单号"] = t.Code;
                    dr["类型"] = this.JudgementType(t.isUrgent);
                    dd.Rows.Add(dr);
                }
            }
            return dd;
        }

        private string JudgementType(string orderTypeName) {
            if (orderTypeName.Equals("urgent"))
                return "紧急";
            else if (orderTypeName.Equals("exit"))
                return "出口";
            else if (orderTypeName.Equals("accident"))
                return "事故";
            else
                return "正常";
        }

        public void SelectDataGridRow(DataGrid dataGrid) {
            if (dataGrid.VisibleRowCount == 0)
                return;
            dataGrid.Select(dataGrid.CurrentRowIndex);
            if (dataGrid.DataSource != null && dataGrid.CurrentRowIndex != -1) {
                string code = dataGrid[dataGrid.CurrentRowIndex, 0].ToString();
                if (this.warehouseAreaId.HasValue) {
                    var pf = new PickingFrm(code, this.warehouseAreaId.Value);
                    pf.ShowDialog();
                    this.isInitialize = false;
                    //刷新gird
                    Initialize(warehouseAreaId.Value);
                }
                else
                    MessageBoxForm.Show(this, "货区id丢失！", "提示", this.DefaultButtonResults);
            }
        }

        private void SetDataGridStyle(DataGrid dataGrid, DataTable dataTable) {
            Color alternatingColor = SystemColors.ControlDark; //交替行颜色

            dataGrid.BackgroundColor = Color.White;
            dataGrid.RowHeadersVisible = false;

            var dataGridTableStyle = new DataGridTableStyle();
            dataGridTableStyle.MappingName = dataTable.TableName;

            dataGridTableStyle.GridColumnStyles.Add(new DataGridCustomTextBoxColumn {
                Owner = dataGrid,
                HeaderText = "单号",
                MappingName = dataTable.Columns[0].ColumnName,
                NullText = "",
                ReadOnly = true,
                Alignment = HorizontalAlignment.Center,
                AlternatingBackColor = alternatingColor,
                Width = 180
            });
            dataGridTableStyle.GridColumnStyles.Add(new DataGridCustomTextBoxColumn {
                Owner = dataGrid,
                HeaderText = "类型",
                MappingName = dataTable.Columns[1].ColumnName,
                NullText = "",
                ReadOnly = true,
                Alignment = HorizontalAlignment.Center,
                AlternatingBackColor = alternatingColor,
                Width = 50
            });

            dataGrid.TableStyles.Add(dataGridTableStyle);
        }
        private void dgvPicking_DoubleClick(object sender, EventArgs e) {
            this.SelectDataGridRow(this.dgvPicking);
        }

        private void dgvPicking_GotFocus(object sender, EventArgs e) {
            try {
                this.SelectRowColor(this.dgvPicking);
            }
            catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }

        private void dgvPicking_LostFocus(object sender, EventArgs e) {
            try {
                if (this.dgvPicking.CurrentRowIndex >= 0)
                    this.dgvPicking.UnSelect(this.dgvPicking.CurrentRowIndex);
            }
            catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }

        private void dgvPicking_KeyDown(object sender, KeyEventArgs e) {
            try {
                switch (e.KeyCode) {
                    case Keys.Enter:
                        SelectDataGridRow(this.dgvPicking);
                        break;
                    case Keys.Up:
                        this.SelectRowColor(this.dgvPicking);
                        break;
                    case Keys.Down:
                        this.SelectRowColor(this.dgvPicking);
                        break;
                }
            }
            catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }
    }
}