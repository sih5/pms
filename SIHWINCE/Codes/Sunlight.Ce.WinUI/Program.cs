﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Reflection;
using System.Windows.Forms;
using System.Xml;
using Sunlight.Ce.Update;
using Sunlight.Ce.Utils;
using Sunlight.Ce.Controls;

namespace Sunlight.Ce.WinUI {
    static class Program {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [MTAThread]
        static void Main() {
            var helpinstance = new HelperUtil();
            //当前环境类型
            var isProudction = Boolean.Parse(helpinstance.GetConfigurationByKey("IsProduction").AppSettingValue);
            if (isProudction) {
                try {
                    //此处实现版本自检索，实现版本升级使用
                    var updater = new Updater(helpinstance.GetConfigurationByKey("UpdateAddress").AppSettingValue);
                    updater.CheckForNewVersion();
                    var callingAssembly = Assembly.GetCallingAssembly();
                    string fullAppName = callingAssembly.GetName().CodeBase;
                    var appPath = Path.GetDirectoryName(fullAppName);
                    var updateFilePath = Path.Combine(appPath, "update.xml");
                    if (File.Exists(updateFilePath)) {
                        Version currentVersion = callingAssembly.GetName().Version;
                        var xDoc = new XmlDocument();
                        xDoc.Load(updateFilePath);
                        XmlNodeList versions = xDoc.GetElementsByTagName("version");

                        var newVersion = new Version(int.Parse(versions[0].Attributes["maj"].Value), int.Parse(versions[0].Attributes["min"].Value), int.Parse(versions[0].Attributes["bld"].Value), int.Parse(versions[0].Attributes["fix"].Value));

                        if (currentVersion.CompareTo(newVersion) < 0) {
                            Process.Start(appPath + "\\Sunlight.Ce.Update.exe", "");
                            Process myproc = Process.GetCurrentProcess();
                            myproc.Kill();
                        }
                        updater.cleanup(updater.updateFilePath);
                    }
                }
                catch (WebException webex) {
                    AutoClosingMessageBox.Show("[网络错误]" + webex.Message, "提示", 4000);
                }
                catch {

                }
                finally {
                    Cursor.Current = Cursors.Default;
                }
            }
            Application.Run(new LoginForm());
        }
    }
}