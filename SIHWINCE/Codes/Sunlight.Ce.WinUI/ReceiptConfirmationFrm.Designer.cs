﻿namespace Sunlight.Ce.WinUI
{
    partial class ReceiptConfirmationFrm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNumber = new System.Windows.Forms.TextBox();
            this.labSprit = new System.Windows.Forms.Label();
            this.lblAcceptanceNumber = new System.Windows.Forms.Label();
            this.txtSparePartsName = new System.Windows.Forms.TextBox();
            this.lblSparePartsName = new System.Windows.Forms.Label();
            this.txtSparePartsCode = new System.Windows.Forms.TextBox();
            this.lblSparePartsCode = new System.Windows.Forms.Label();
            this.lblMeasureUnit = new System.Windows.Forms.Label();
            this.btnEnd = new System.Windows.Forms.Button();
            this.lblIsPacking = new System.Windows.Forms.Label();
            this.cbIsPacking = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // txtNumber
            // 
            this.txtNumber.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtNumber.Location = new System.Drawing.Point(90, 115);
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.Size = new System.Drawing.Size(54, 26);
            this.txtNumber.TabIndex = 29;
            // 
            // labSprit
            // 
            this.labSprit.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.labSprit.Location = new System.Drawing.Point(150, 117);
            this.labSprit.Name = "labSprit";
            this.labSprit.Size = new System.Drawing.Size(54, 28);
            this.labSprit.Text = "/";
            // 
            // lblAcceptanceNumber
            // 
            this.lblAcceptanceNumber.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular);
            this.lblAcceptanceNumber.Location = new System.Drawing.Point(2, 115);
            this.lblAcceptanceNumber.Name = "lblAcceptanceNumber";
            this.lblAcceptanceNumber.Size = new System.Drawing.Size(96, 30);
            this.lblAcceptanceNumber.Text = "收货数量";
            // 
            // txtSparePartsName
            // 
            this.txtSparePartsName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSparePartsName.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtSparePartsName.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtSparePartsName.Location = new System.Drawing.Point(90, 65);
            this.txtSparePartsName.Name = "txtSparePartsName";
            this.txtSparePartsName.ReadOnly = true;
            this.txtSparePartsName.Size = new System.Drawing.Size(148, 26);
            this.txtSparePartsName.TabIndex = 33;
            // 
            // lblSparePartsName
            // 
            this.lblSparePartsName.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular);
            this.lblSparePartsName.Location = new System.Drawing.Point(-1, 65);
            this.lblSparePartsName.Name = "lblSparePartsName";
            this.lblSparePartsName.Size = new System.Drawing.Size(84, 27);
            this.lblSparePartsName.Text = "零件名称";
            // 
            // txtSparePartsCode
            // 
            this.txtSparePartsCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtSparePartsCode.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtSparePartsCode.Location = new System.Drawing.Point(90, 20);
            this.txtSparePartsCode.Name = "txtSparePartsCode";
            this.txtSparePartsCode.Size = new System.Drawing.Size(148, 26);
            this.txtSparePartsCode.TabIndex = 32;
            // 
            // lblSparePartsCode
            // 
            this.lblSparePartsCode.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular);
            this.lblSparePartsCode.Location = new System.Drawing.Point(2, 20);
            this.lblSparePartsCode.Name = "lblSparePartsCode";
            this.lblSparePartsCode.Size = new System.Drawing.Size(74, 26);
            this.lblSparePartsCode.Text = "零件号";
            // 
            // lblMeasureUnit
            // 
            this.lblMeasureUnit.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.lblMeasureUnit.Location = new System.Drawing.Point(204, 115);
            this.lblMeasureUnit.Name = "lblMeasureUnit";
            this.lblMeasureUnit.Size = new System.Drawing.Size(34, 29);
            // 
            // btnEnd
            // 
            this.btnEnd.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.btnEnd.Location = new System.Drawing.Point(168, 200);
            this.btnEnd.Name = "btnEnd";
            this.btnEnd.Size = new System.Drawing.Size(58, 36);
            this.btnEnd.TabIndex = 30;
            this.btnEnd.Text = "确定";
            this.btnEnd.Click += new System.EventHandler(this.btnEnd_Click);
            // 
            // lblIsPacking
            // 
            this.lblIsPacking.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular);
            this.lblIsPacking.Location = new System.Drawing.Point(3, 166);
            this.lblIsPacking.Name = "lblIsPacking";
            this.lblIsPacking.Size = new System.Drawing.Size(96, 30);
            this.lblIsPacking.Text = "是否包装";
            // 
            // cbIsPacking
            // 
            this.cbIsPacking.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.cbIsPacking.Items.Add("是");
            this.cbIsPacking.Items.Add("否");
            this.cbIsPacking.Location = new System.Drawing.Point(90, 166);
            this.cbIsPacking.Name = "cbIsPacking";
            this.cbIsPacking.Size = new System.Drawing.Size(54, 26);
            this.cbIsPacking.TabIndex = 38;
            // 
            // ReceiptConfirmationFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 295);
            this.Controls.Add(this.cbIsPacking);
            this.Controls.Add(this.lblIsPacking);
            this.Controls.Add(this.lblMeasureUnit);
            this.Controls.Add(this.txtNumber);
            this.Controls.Add(this.labSprit);
            this.Controls.Add(this.lblAcceptanceNumber);
            this.Controls.Add(this.txtSparePartsName);
            this.Controls.Add(this.lblSparePartsName);
            this.Controls.Add(this.txtSparePartsCode);
            this.Controls.Add(this.lblSparePartsCode);
            this.Controls.Add(this.btnEnd);
            this.Name = "ReceiptConfirmationFrm";
            this.Text = "入库-输入数量";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtNumber;
        private System.Windows.Forms.Label labSprit;
        private System.Windows.Forms.Label lblAcceptanceNumber;
        private System.Windows.Forms.TextBox txtSparePartsName;
        private System.Windows.Forms.Label lblSparePartsName;
        private System.Windows.Forms.TextBox txtSparePartsCode;
        private System.Windows.Forms.Label lblSparePartsCode;
        private System.Windows.Forms.Label lblMeasureUnit;
        private System.Windows.Forms.Button btnEnd;
        private System.Windows.Forms.Label lblIsPacking;
        private System.Windows.Forms.ComboBox cbIsPacking;
    }
}