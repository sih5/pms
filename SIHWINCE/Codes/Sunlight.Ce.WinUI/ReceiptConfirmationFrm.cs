﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Sunlight.Ce.Controls;
using Sunlight.Ce.Service;
using Sunlight.Ce.Utils;

namespace Sunlight.Ce.WinUI {
    public partial class ReceiptConfirmationFrm : BaseFrm {
        private ReceiptFrm rpf;

        private string spartsCode;

        //当前界面数据中的索引
        private int index = -1;

        public ReceiptConfirmationFrm(ReceiptFrm rcnfPara, string spartsCodePara, string code) {
            InitializeComponent();

            rpf = rcnfPara;
            spartsCode = spartsCodePara;

            if (rpf.receivingOrders != null)
                index = rpf.receivingOrders.FindIndex(o => (o.PartsCode == spartsCode) && o.Code == code);
            //init
            if (index != -1) {
                if (rpf.receivingOrders[index].InboundType == (int)DcsPartsInboundType.配件采购)
                    this.txtSparePartsCode.Text = string.IsNullOrEmpty(rpf.receivingOrders[index].SupplierPartCode) ? rpf.receivingOrders[index].PartsCode : rpf.receivingOrders[index].SupplierPartCode;
                else
                    this.txtSparePartsCode.Text = rpf.receivingOrders[index].PartsCode;
                this.txtSparePartsName.Text = rpf.receivingOrders[index].PartsName;
                this.labSprit.Text = "/" + (rpf.receivingOrders[index].PlannedAmount - integerToInt(rpf.receivingOrders[index].ReceivedAmount));
                //this.lblMeasureUnit.Text = rpf.receivingOrders[index].MeasureUnit;
                this.txtNumber.Text = rpf.receivingOrders[index].ReceiveAmount.HasValue ? rpf.receivingOrders[index].ReceiveAmount.HasValue.ToString() : "";
                this.txtNumber.Focus();
                this.txtSparePartsCode.ReadOnly = true;

                var userinfo = ServiceManager.GetCurrentUser();
                if (userinfo.EnterpriseType != 1) {
                    this.cbIsPacking.Hide();
                    this.lblIsPacking.Hide();
                    this.cbIsPacking.SelectedIndex = 1;
                } else {
                    this.cbIsPacking.Show();
                    this.lblIsPacking.Show();
                    this.cbIsPacking.SelectedIndex = 0;
                }
            }
        }

        private void btnEnd_Click(object sender, EventArgs e) {
            int thisNumber = string.IsNullOrEmpty(this.txtNumber.Text) ? 0 : Convert.ToInt32(this.txtNumber.Text);
            if (thisNumber < 0) {
                MessageBoxForm.Show(this, "请填写正整数！", "提示", new Dictionary<string, DialogResult> { { "确认", DialogResult.OK } });
                return;
            }
            //待收货量
            int pending = rpf.receivingOrders[index].PlannedAmount - integerToInt(rpf.receivingOrders[index].ReceivedAmount);
            if (pending < thisNumber) {
                MessageBoxForm.Show(this, "本次收获量加上本次拒收量不能大于计划量！", "提示", new Dictionary<string, DialogResult> { { "确认", DialogResult.OK } });
                return;
            }
            this.rpf.number = thisNumber;
            this.rpf.isPacking = this.cbIsPacking.Text == "是" ? true : false;
            this.Close();
        }
    }
}