﻿namespace Sunlight.Ce.WinUI {
    partial class ReceiptFrm {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent() {
            this.txtPartsCode = new System.Windows.Forms.TextBox();
            this.btnUpload = new System.Windows.Forms.Button();
            this.dgvParts = new System.Windows.Forms.DataGrid();
            this.lblCaseNo = new System.Windows.Forms.Label();
            this.datagridOrderNO = new System.Windows.Forms.DataGrid();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtPartsCode
            // 
            this.txtPartsCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtPartsCode.Location = new System.Drawing.Point(65, 38);
            this.txtPartsCode.Name = "txtPartsCode";
            this.txtPartsCode.Size = new System.Drawing.Size(93, 26);
            this.txtPartsCode.TabIndex = 23;
            this.txtPartsCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPartsCode_KeyDown);
            // 
            // btnUpload
            // 
            this.btnUpload.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnUpload.Location = new System.Drawing.Point(164, 36);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(71, 29);
            this.btnUpload.TabIndex = 27;
            this.btnUpload.Text = "上传";
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // dgvParts
            // 
            this.dgvParts.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dgvParts.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.dgvParts.Location = new System.Drawing.Point(0, 74);
            this.dgvParts.Name = "dgvParts";
            this.dgvParts.Size = new System.Drawing.Size(238, 203);
            this.dgvParts.TabIndex = 26;
            this.dgvParts.LostFocus += new System.EventHandler(this.dgvParts_LostFocus);
            this.dgvParts.DoubleClick += new System.EventHandler(this.dgvParts_DoubleClick);
            this.dgvParts.GotFocus += new System.EventHandler(this.dgvParts_GotFocus);
            this.dgvParts.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvParts_KeyDown);
            // 
            // lblCaseNo
            // 
            this.lblCaseNo.Location = new System.Drawing.Point(1, 44);
            this.lblCaseNo.Name = "lblCaseNo";
            this.lblCaseNo.Size = new System.Drawing.Size(86, 20);
            this.lblCaseNo.Text = "扫描配件";
            // 
            // datagridOrderNO
            // 
            this.datagridOrderNO.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.datagridOrderNO.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular);
            this.datagridOrderNO.Location = new System.Drawing.Point(0, 74);
            this.datagridOrderNO.Name = "datagridOrderNO";
            this.datagridOrderNO.Size = new System.Drawing.Size(238, 203);
            this.datagridOrderNO.TabIndex = 25;
            // 
            // txtCode
            // 
            this.txtCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtCode.Location = new System.Drawing.Point(3, 7);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(232, 26);
            this.txtCode.TabIndex = 24;
            // 
            // ReceiptFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 295);
            this.Controls.Add(this.txtPartsCode);
            this.Controls.Add(this.btnUpload);
            this.Controls.Add(this.dgvParts);
            this.Controls.Add(this.lblCaseNo);
            this.Controls.Add(this.datagridOrderNO);
            this.Controls.Add(this.txtCode);
            this.Name = "ReceiptFrm";
            this.Text = "入库-扫描配件";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtPartsCode;
        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.DataGrid dgvParts;
        private System.Windows.Forms.Label lblCaseNo;
        private System.Windows.Forms.DataGrid datagridOrderNO;
        private System.Windows.Forms.TextBox txtCode;

    }
}