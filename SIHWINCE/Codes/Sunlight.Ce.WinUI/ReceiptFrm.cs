﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Sunlight.Ce.Controls;
using Sunlight.Ce.Entities.DTO;
using Sunlight.Ce.Service;
using Sunlight.Ce.Utils;

namespace Sunlight.Ce.WinUI {
    public partial class ReceiptFrm : BaseFrm {
        public List<ForReceivingOrderDTO> receivingOrders;

        public int? number;

        public bool isPacking = true;

        private DataTable SetDataSource(string partsCode) {
            var dd = new DataTable();
            dd.TableName = "dgvParts";
            dd.Columns.Add("备件号");
            dd.Columns.Add("计");
            dd.Columns.Add("本");

            List<ForReceivingOrderDTO> displayList = receivingOrders;
            if (!string.IsNullOrEmpty(partsCode)) {
                int index = receivingOrders.FindIndex(o => o.PartsCode == partsCode);
                ForReceivingOrderDTO receivingOrder = receivingOrders[0];
                receivingOrders[0] = receivingOrders[index];
                receivingOrders[index] = receivingOrder;
            }
            foreach (var t in displayList) {
                DataRow dr = dd.NewRow();
                if (t.InboundType == (int)DcsPartsInboundType.配件采购)
                    dr["备件号"] = string.IsNullOrEmpty(t.SupplierPartCode) ? t.PartsCode : t.SupplierPartCode;
                else
                    dr["备件号"] = t.PartsCode;
                dr["计"] = t.PlannedAmount - integerToInt(t.ReceivedAmount);
                dr["本"] = t.ReceiveAmount.HasValue ? t.ReceiveAmount.Value.ToString() : "";
                dd.Rows.Add(dr);
            }
            return dd;
        }

        private void SetDataGridStyle(DataGrid dataGrid, DataTable dataTable) {
            Color alternatingColor = SystemColors.ControlDark; //交替行颜色

            dataGrid.BackgroundColor = Color.White;
            dataGrid.RowHeadersVisible = false;

            var dataGridTableStyle = new DataGridTableStyle();
            dataGridTableStyle.MappingName = dataTable.TableName;

            dataGridTableStyle.GridColumnStyles.Add(new DataGridCustomTextBoxColumn {
                Owner = dataGrid,
                HeaderText = "备件号",
                MappingName = dataTable.Columns[0].ColumnName,
                NullText = "",
                ReadOnly = true,
                Alignment = HorizontalAlignment.Center,
                AlternatingBackColor = alternatingColor,
                Width = 150
            });

            dataGridTableStyle.GridColumnStyles.Add(new DataGridCustomTextBoxColumn {
                Owner = dataGrid,
                HeaderText = "计",
                MappingName = dataTable.Columns[1].ColumnName,
                NullText = "",
                ReadOnly = true,
                Alignment = HorizontalAlignment.Center,
                AlternatingBackColor = alternatingColor,
                Width = 40
            });

            dataGridTableStyle.GridColumnStyles.Add(new DataGridCustomTextBoxColumn {
                Owner = dataGrid,
                HeaderText = "本",
                MappingName = dataTable.Columns[2].ColumnName,
                NullText = "",
                ReadOnly = true,
                Alignment = HorizontalAlignment.Center,
                AlternatingBackColor = alternatingColor,
                Width = 40
            });

            dataGrid.TableStyles.Add(dataGridTableStyle);
        }

        private void dgvParts_GotFocus(object sender, EventArgs e) {
            try {
                this.SelectRowColor(this.dgvParts);
            }
            catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }

        private void dgvParts_LostFocus(object sender, EventArgs e) {
            try {
                if (this.dgvParts.CurrentRowIndex >= 0)
                    this.dgvParts.UnSelect(this.dgvParts.CurrentRowIndex);
            }
            catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }

        private void dgvParts_KeyDown(object sender, KeyEventArgs e) {
            try {
                switch (e.KeyCode) {
                    case Keys.Enter:
                        SelectDataGridRow(this.dgvParts);
                        break;
                    case Keys.Up:
                        this.SelectRowColor(this.dgvParts);
                        break;
                    case Keys.Down:
                        this.SelectRowColor(this.dgvParts);
                        break;
                }
            }
            catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }

        public void SelectDataGridRow(DataGrid dataGrid) {
            if (dataGrid.VisibleRowCount == 0)
                return;
            dataGrid.Select(dataGrid.CurrentRowIndex);
            if (dataGrid.DataSource != null && dataGrid.CurrentRowIndex != -1) {
                string code = dataGrid[dataGrid.CurrentRowIndex, 0].ToString();
                int index = receivingOrders.FindIndex(o => o.PartsCode == code || o.SupplierPartCode == code);
                this.partsCodeFill(index);
            }
        }

        private void dgvParts_DoubleClick(object sender, EventArgs e) {
            this.SelectDataGridRow(this.dgvParts);
        }

        private int ScanTicketClear = 0;
        private void txtPartsCode_KeyDown(object sender, KeyEventArgs e) {
            if (ScanTicketClear == 0) {
                this.txtPartsCode.Text = string.Empty;
                ScanTicketClear++;
            }
            if (e.KeyCode == Keys.Enter) {
                ScanTicketClear = 0;

                if (this.txtPartsCode.Text.Length > 0) {
                    string[] codeAndNum = this.txtPartsCode.Text.Split('|');
                    if (codeAndNum.Length < 2 && this.txtPartsCode.Text.IndexOf("|") >= 0) {
                        MessageBoxForm.Show(this, "扫描配件条码错误！", "提示", this.DefaultButtonResults);
                        this.txtPartsCode.Text = null;
                        return;
                    }

                    int index = receivingOrders.FindIndex(o => o.PartsCode == codeAndNum[0]);
                    if (index >= 0) {
                        this.partsCodeFill(index);
                    }
                    else {
                        MessageBoxForm.Show(this, "扫描配件条码错误！", "提示", this.DefaultButtonResults);
                        this.txtPartsCode.Text = null;
                        return;

                    }
                }
                else {
                    MessageBoxForm.Show(this, "扫描配件条码错误！", "提示", this.DefaultButtonResults);
                    this.txtPartsCode.Text = null;
                    return;
                }
            }
        }

        private void partsCodeFill(int index) {
            ReceiptConfirmationFrm ipnf = new ReceiptConfirmationFrm(this, receivingOrders[index].PartsCode, receivingOrders[index].Code);
            ipnf.ShowDialog();

            if (index == -1) {
                MessageBoxForm.Show(this, "配件编号或收货单Id错误！", "提示", this.DefaultButtonResults);
                return;
            }
            receivingOrders[index].ReceiveAmount = number;
            receivingOrders[index].IsPacking = isPacking;

            var dtCode = this.SetDataSource(receivingOrders[index].PartsCode);
            this.dgvParts.DataSource = dtCode;
        }

        private void btnUpload_Click(object sender, EventArgs e) {
            try {
                var theReceiptList = receivingOrders.Where(o => (o.ReceiveAmount ?? 0) > 0).ToList();
                if (theReceiptList.Count == 0) {
                    MessageBoxForm.Show(this, "未收货,请填写收货量！", "提示", this.DefaultButtonResults);
                    return;
                }
                this.IsLoading(true);
                var messageInfo = ServiceManager.ConfirmationReceipts(theReceiptList);
                this.IsLoading(false);

                if (messageInfo.IsSuccess) {
                    MessageBoxForm.Show(this, "入库成功！", "提示", this.DefaultButtonResults);
                    RasieEditSubmitted();
                    this.SetWindowClose();
                }
                else {
                    MessageBoxForm.Show(this, messageInfo.Message, "提示", this.DefaultButtonResults);
                }
            }
            catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }

        public ReceiptFrm(List<ForReceivingOrderDTO> receipts) {
            InitializeComponent();

            this.receivingOrders = receipts;
            this.txtCode.Text = receivingOrders[0].Code;
            this.txtCode.ReadOnly = true;

            var dd = this.SetDataSource(null);
            this.SetDataGridStyle(this.dgvParts, dd);
            this.dgvParts.DataSource = dd;
        }
    }
}