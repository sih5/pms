﻿namespace Sunlight.Ce.WinUI {
    partial class ReceiptManagementFrm {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent() {
            this.txtinPlanCode = new System.Windows.Forms.TextBox();
            this.dgvReceipt = new System.Windows.Forms.DataGrid();
            this.lblCode = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtinPlanCode
            // 
            this.txtinPlanCode.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular);
            this.txtinPlanCode.Location = new System.Drawing.Point(76, 8);
            this.txtinPlanCode.Name = "txtinPlanCode";
            this.txtinPlanCode.Size = new System.Drawing.Size(161, 29);
            this.txtinPlanCode.TabIndex = 2;
            this.txtinPlanCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtinPlanCode_KeyDown);
            // 
            // dgvReceipt
            // 
            this.dgvReceipt.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dgvReceipt.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.dgvReceipt.Location = new System.Drawing.Point(0, 43);
            this.dgvReceipt.Name = "dgvReceipt";
            this.dgvReceipt.Size = new System.Drawing.Size(238, 224);
            this.dgvReceipt.TabIndex = 6;
            this.dgvReceipt.DoubleClick += new System.EventHandler(this.dgvReceipt_DoubleClick);
            this.dgvReceipt.CurrentCellChanged += new System.EventHandler(this.dgvReceipt_CurrentCellChanged);
            this.dgvReceipt.GotFocus += new System.EventHandler(this.dgvReceipt_GotFocus);
            // 
            // lblCode
            // 
            this.lblCode.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular);
            this.lblCode.Location = new System.Drawing.Point(0, 12);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(92, 20);
            this.lblCode.Text = "扫描单号";
            // 
            // ReceiptManagementFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 295);
            this.Controls.Add(this.txtinPlanCode);
            this.Controls.Add(this.lblCode);
            this.Controls.Add(this.dgvReceipt);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ReceiptManagementFrm";
            this.Text = "入库-扫描";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtinPlanCode;
        private System.Windows.Forms.DataGrid dgvReceipt;
        private System.Windows.Forms.Label lblCode;
    }
}