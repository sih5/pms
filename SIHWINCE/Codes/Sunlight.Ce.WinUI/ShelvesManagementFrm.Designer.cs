﻿namespace Sunlight.Ce.WinUI {
    partial class ShelvesManagementFrm {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent() {
            this.txtThisShelvesAmount = new System.Windows.Forms.TextBox();
            this.txtSparePartCode = new System.Windows.Forms.TextBox();
            this.labSprit = new System.Windows.Forms.Label();
            this.txtSparePartsName = new System.Windows.Forms.TextBox();
            this.lblSparePartsName = new System.Windows.Forms.Label();
            this.lblSparePartCode = new System.Windows.Forms.Label();
            this.txtinCode = new System.Windows.Forms.TextBox();
            this.lblCode = new System.Windows.Forms.Label();
            this.lblRecommendLocation = new System.Windows.Forms.Label();
            this.txtRecommendLocation = new System.Windows.Forms.TextBox();
            this.lblThisShelvesAmount = new System.Windows.Forms.Label();
            this.lblScanLocation = new System.Windows.Forms.Label();
            this.txtScanLocation = new System.Windows.Forms.TextBox();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.lblTraceProperty = new System.Windows.Forms.Label();
            this.txtTraceProperty = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtThisShelvesAmount
            // 
            this.txtThisShelvesAmount.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtThisShelvesAmount.Location = new System.Drawing.Point(69, 164);
            this.txtThisShelvesAmount.Name = "txtThisShelvesAmount";
            this.txtThisShelvesAmount.Size = new System.Drawing.Size(84, 26);
            this.txtThisShelvesAmount.TabIndex = 43;
            this.txtThisShelvesAmount.TextChanged += new System.EventHandler(this.txtThisShelvesAmount_TextChanged);
            // 
            // txtSparePartCode
            // 
            this.txtSparePartCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtSparePartCode.Location = new System.Drawing.Point(69, 36);
            this.txtSparePartCode.Name = "txtSparePartCode";
            this.txtSparePartCode.Size = new System.Drawing.Size(168, 26);
            this.txtSparePartCode.TabIndex = 41;
            this.txtSparePartCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSparePartCode_KeyDown);
            // 
            // labSprit
            // 
            this.labSprit.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.labSprit.Location = new System.Drawing.Point(177, 164);
            this.labSprit.Name = "labSprit";
            this.labSprit.Size = new System.Drawing.Size(60, 28);
            this.labSprit.Text = "/";
            // 
            // txtSparePartsName
            // 
            this.txtSparePartsName.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtSparePartsName.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtSparePartsName.Location = new System.Drawing.Point(69, 68);
            this.txtSparePartsName.Name = "txtSparePartsName";
            this.txtSparePartsName.ReadOnly = true;
            this.txtSparePartsName.Size = new System.Drawing.Size(167, 26);
            this.txtSparePartsName.TabIndex = 45;
            // 
            // lblSparePartsName
            // 
            this.lblSparePartsName.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblSparePartsName.Location = new System.Drawing.Point(0, 70);
            this.lblSparePartsName.Name = "lblSparePartsName";
            this.lblSparePartsName.Size = new System.Drawing.Size(95, 27);
            this.lblSparePartsName.Text = "零件名称";
            // 
            // lblSparePartCode
            // 
            this.lblSparePartCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblSparePartCode.Location = new System.Drawing.Point(0, 38);
            this.lblSparePartCode.Name = "lblSparePartCode";
            this.lblSparePartCode.Size = new System.Drawing.Size(92, 20);
            this.lblSparePartCode.Text = "扫描配件";
            // 
            // txtinCode
            // 
            this.txtinCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtinCode.Location = new System.Drawing.Point(69, 2);
            this.txtinCode.Name = "txtinCode";
            this.txtinCode.Size = new System.Drawing.Size(168, 26);
            this.txtinCode.TabIndex = 42;
            this.txtinCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtinCode_KeyDown);
            // 
            // lblCode
            // 
            this.lblCode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblCode.Location = new System.Drawing.Point(0, 6);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(92, 20);
            this.lblCode.Text = "扫描单号";
            // 
            // lblRecommendLocation
            // 
            this.lblRecommendLocation.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblRecommendLocation.Location = new System.Drawing.Point(0, 134);
            this.lblRecommendLocation.Name = "lblRecommendLocation";
            this.lblRecommendLocation.Size = new System.Drawing.Size(95, 27);
            this.lblRecommendLocation.Text = "推荐货位";
            // 
            // txtRecommendLocation
            // 
            this.txtRecommendLocation.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.txtRecommendLocation.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.txtRecommendLocation.Location = new System.Drawing.Point(69, 132);
            this.txtRecommendLocation.Name = "txtRecommendLocation";
            this.txtRecommendLocation.ReadOnly = true;
            this.txtRecommendLocation.Size = new System.Drawing.Size(167, 26);
            this.txtRecommendLocation.TabIndex = 45;
            // 
            // lblThisShelvesAmount
            // 
            this.lblThisShelvesAmount.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblThisShelvesAmount.Location = new System.Drawing.Point(0, 166);
            this.lblThisShelvesAmount.Name = "lblThisShelvesAmount";
            this.lblThisShelvesAmount.Size = new System.Drawing.Size(66, 24);
            this.lblThisShelvesAmount.Text = "数量";
            // 
            // lblScanLocation
            // 
            this.lblScanLocation.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblScanLocation.Location = new System.Drawing.Point(0, 198);
            this.lblScanLocation.Name = "lblScanLocation";
            this.lblScanLocation.Size = new System.Drawing.Size(92, 20);
            this.lblScanLocation.Text = "扫描货位";
            // 
            // txtScanLocation
            // 
            this.txtScanLocation.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtScanLocation.Location = new System.Drawing.Point(69, 196);
            this.txtScanLocation.Name = "txtScanLocation";
            this.txtScanLocation.Size = new System.Drawing.Size(169, 26);
            this.txtScanLocation.TabIndex = 41;
            this.txtScanLocation.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtScanLocation_KeyDown);
            // 
            // btnSubmit
            // 
            this.btnSubmit.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.btnSubmit.Location = new System.Drawing.Point(131, 234);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(102, 42);
            this.btnSubmit.TabIndex = 55;
            this.btnSubmit.Text = "上传";
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // lblTraceProperty
            // 
            this.lblTraceProperty.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.lblTraceProperty.Location = new System.Drawing.Point(0, 102);
            this.lblTraceProperty.Name = "lblTraceProperty";
            this.lblTraceProperty.Size = new System.Drawing.Size(78, 27);
            this.lblTraceProperty.Text = "追溯属性";
            // 
            // txtTraceProperty
            // 
            this.txtTraceProperty.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.txtTraceProperty.Location = new System.Drawing.Point(69, 100);
            this.txtTraceProperty.Name = "txtTraceProperty";
            this.txtTraceProperty.ReadOnly = true;
            this.txtTraceProperty.Size = new System.Drawing.Size(167, 26);
            this.txtTraceProperty.TabIndex = 65;
            // 
            // ShelvesManagementFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 295);
            this.Controls.Add(this.txtTraceProperty);
            this.Controls.Add(this.lblTraceProperty);
            this.Controls.Add(this.txtThisShelvesAmount);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.lblThisShelvesAmount);
            this.Controls.Add(this.txtScanLocation);
            this.Controls.Add(this.txtSparePartCode);
            this.Controls.Add(this.labSprit);
            this.Controls.Add(this.txtRecommendLocation);
            this.Controls.Add(this.txtSparePartsName);
            this.Controls.Add(this.lblRecommendLocation);
            this.Controls.Add(this.lblScanLocation);
            this.Controls.Add(this.lblSparePartsName);
            this.Controls.Add(this.lblSparePartCode);
            this.Controls.Add(this.txtinCode);
            this.Controls.Add(this.lblCode);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ShelvesManagementFrm";
            this.Text = "上架";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtThisShelvesAmount;
        private System.Windows.Forms.TextBox txtSparePartCode;
        private System.Windows.Forms.Label labSprit;
        private System.Windows.Forms.TextBox txtSparePartsName;
        private System.Windows.Forms.Label lblSparePartsName;
        private System.Windows.Forms.Label lblSparePartCode;
        private System.Windows.Forms.TextBox txtinCode;
        private System.Windows.Forms.Label lblCode;
        private System.Windows.Forms.Label lblRecommendLocation;
        private System.Windows.Forms.TextBox txtRecommendLocation;
        private System.Windows.Forms.Label lblThisShelvesAmount;
        private System.Windows.Forms.Label lblScanLocation;
        private System.Windows.Forms.TextBox txtScanLocation;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Label lblTraceProperty;
        private System.Windows.Forms.TextBox txtTraceProperty;
    }
}