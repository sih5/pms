﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Sunlight.Ce.Controls;
using Sunlight.Ce.Entities.DTO;
using Sunlight.Ce.Service;
using System.Text.RegularExpressions;
using Sunlight.Ce.Utils;

namespace Sunlight.Ce.WinUI {
    public partial class ShelvesManagementFrm : BaseFrm {
        private List<PartsShelvesDTO> partsShelvesExtends;

        public ShelvesManagementFrm() {
            InitializeComponent();

            this.BeginRefresh();
        }

        private void BeginRefresh() {
            //先清空缓存
            this.txtinCode.ReadOnly = false;
            this.txtinCode.Enabled = true;
            this.txtinCode.Text = string.Empty;
            this.txtSparePartCode.Text = string.Empty;
            this.txtSparePartsName.Text = string.Empty;
            this.txtRecommendLocation.Text = string.Empty;
            this.txtThisShelvesAmount.Text = string.Empty;
            this.txtScanLocation.Text = string.Empty;
            this.labSprit.Text = "/";

            this.txtinCode.Focus();
            this.txtSparePartCode.ReadOnly = true;
            this.txtSparePartCode.Enabled = false;
            this.txtThisShelvesAmount.ReadOnly = true;
            this.txtThisShelvesAmount.Enabled = false;
            this.txtScanLocation.ReadOnly = true;
            this.txtScanLocation.Enabled = false;
            this.btnSubmit.Enabled = false;
            this.sparePartCode = string.Empty;
        }

        private string sparePartCode;
        private void txtSparePartCode_KeyDown(object sender, KeyEventArgs e) {
            if (ScanTicketClear == 0) {
                this.txtSparePartCode.Text = string.Empty;
                ScanTicketClear++;
            }
            if (e.KeyCode == Keys.Enter) {
                try {
                    ScanTicketClear = 0;
                    if (string.IsNullOrEmpty(this.txtinCode.Text)) {
                        MessageBoxForm.Show(this, "请先扫描货位！", "提示", this.DefaultButtonResults);
                        this.txtSparePartCode.Text = string.Empty;
                        return;
                    } else {
                        string[] codeAndNum = this.txtSparePartCode.Text.Split('|');
                        if (codeAndNum.Length < 2) {
                            MessageBoxForm.Show(this, "扫描配件条码错误！", "提示", this.DefaultButtonResults);
                            this.txtSparePartCode.Text = null;
                            return;
                        }
                        if (!string.IsNullOrEmpty(this.sparePartCode)) {
                            if (!sparePartCode.Equals(codeAndNum[0])) {
                                MessageBoxForm.Show(this, "扫描配件条码与任务单不一致！", "提示", this.DefaultButtonResults);
                                this.txtSparePartCode.Text = null;
                                return;
                            }
                            if (partsShelvesExtends[0].SIHCodes.Contains(this.txtSparePartCode.Text) && partsShelvesExtends[0].TraceProperty.HasValue 
                                && partsShelvesExtends[0].TraceProperty.Value == (int)DCSTraceProperty.精确追溯) {
                                MessageBoxForm.Show(this, "当前标签码已被扫描！", "提示", this.DefaultButtonResults);
                                this.txtSparePartCode.Text = null;
                                return;
                            }
                            partsShelvesExtends[0].SIHCodes = partsShelvesExtends[0].SIHCodes + ";" + this.txtSparePartCode.Text;
                            this.txtThisShelvesAmount.Text = (Convert.ToInt32(this.txtThisShelvesAmount.Text) + Convert.ToInt32(codeAndNum[1])).ToString();
                            return;
                        }

                        this.IsLoading(true);
                        var result = ServiceManager.GetShelvesTask(this.txtinCode.Text, codeAndNum[0]);
                        this.IsLoading(false);
                        if (result.IsSuccess == true && result.Data != null) {
                            partsShelvesExtends = result.Data;
                            if (partsShelvesExtends.Count == 0) {
                                MessageBoxForm.Show(this, "根据单号与配件编号，未找到有效的上架任务单，请检查单号条码是否有效！", "提示", this.DefaultButtonResults);
                                this.txtSparePartCode.Text = string.Empty;
                                return;
                            }

                            this.txtTraceProperty.Text = this.TracePropertyValue(partsShelvesExtends[0].TraceProperty);
                            this.txtThisShelvesAmount.Focus();
                            this.txtinCode.ReadOnly = true;
                            this.txtinCode.Enabled = false;
                            this.txtThisShelvesAmount.ReadOnly = false;
                            this.txtThisShelvesAmount.Enabled = true;
                            this.txtScanLocation.ReadOnly = false;
                            this.txtScanLocation.Enabled = true;
                            this.txtSparePartsName.Text = partsShelvesExtends[0].SparePartName;
                            this.txtRecommendLocation.Text = partsShelvesExtends[0].WarehouseAreaCode;
                            this.labSprit.Text = integerToInt(partsShelvesExtends[0].ShelvesAmount) + "/" + partsShelvesExtends[0].PlannedAmount;
                            this.btnSubmit.Enabled = true;
                            sparePartCode = codeAndNum[0];
                            this.txtThisShelvesAmount.Text = Convert.ToInt32(codeAndNum[1]).ToString();

                            if (partsShelvesExtends[0].TraceProperty.HasValue && partsShelvesExtends[0].TraceProperty.Value == (int)DCSTraceProperty.精确追溯) {
                                this.txtThisShelvesAmount.Enabled = false;
                            } else {
                                this.txtThisShelvesAmount.Enabled = true;
                            }
                            partsShelvesExtends[0].SIHCodes = this.txtSparePartCode.Text;
                        }

                    }
                } catch (Exception ex) {
                    MessageBoxForm.Show(this, ex.Message, "错误", this.DefaultButtonResults);
                    this.txtSparePartCode.Text = string.Empty;
                    return;
                }
            }
        }

        private string TracePropertyValue(int? type) {
            switch (type) {
                case (int)DCSTraceProperty.精确追溯:
                    return "精确追溯";
                case (int)DCSTraceProperty.批次追溯:
                    return "批次追溯";
                default:
                    return "";
            }
        }

        private int ScanTicketClear = 0;
        private void txtinCode_KeyDown(object sender, KeyEventArgs e) {
            if (ScanTicketClear == 0) {
                this.txtinCode.Text = string.Empty;
                ScanTicketClear++;
            }
            if (e.KeyCode == Keys.Enter) {
                ScanTicketClear = 0;

                this.txtSparePartCode.ReadOnly = false;
                this.txtSparePartCode.Enabled = true;
                this.txtSparePartCode.Focus();
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e) {
            try {
                if (partsShelvesExtends != null && partsShelvesExtends[0] != null) {
                    if (integerToInt(partsShelvesExtends[0].ThisShelvesAmount) == 0) {
                        MessageBoxForm.Show(this, "请填写本次上架量！", "提示", this.DefaultButtonResults);
                        return;
                    }
                    if (string.IsNullOrEmpty(partsShelvesExtends[0].TargetWarehouseAreaCode)) {
                        MessageBoxForm.Show(this, "请扫描货位，在上传上架任务！", "提示", this.DefaultButtonResults);
                        return;
                    }
                    this.IsLoading(true);
                    var result = ServiceManager.Shelvesing(partsShelvesExtends[0]);
                    this.IsLoading(false);
                    if (result.IsSuccess) {
                        MessageBoxForm.Show(this, result.Message, "提示", this.DefaultButtonResults);

                        this.partsShelvesExtends = null;
                        this.BeginRefresh();
                    } else {
                        MessageBoxForm.Show(this, result.Message, "提示", this.DefaultButtonResults);
                    }
                }
            } catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }

        private void txtThisShelvesAmount_TextChanged(object sender, EventArgs e) {
            try {
                var rgx = new Regex(@"^\d+$");
                var tempTxtshippingAmount = sender as TextBox;
                if (tempTxtshippingAmount != null && !string.IsNullOrEmpty(tempTxtshippingAmount.Text) && !string.IsNullOrEmpty(tempTxtshippingAmount.Text)) {
                    var thisNumber = tempTxtshippingAmount.Text;
                    if (!rgx.IsMatch(thisNumber)) {
                        MessageBoxForm.Show(this, "你输入的不是数字！", "提示", this.DefaultButtonResults);
                        this.txtThisShelvesAmount.Text = null;
                        return;
                    }
                    if (Convert.ToInt32(thisNumber.Trim()) + integerToInt(partsShelvesExtends[0].ShelvesAmount) > partsShelvesExtends[0].PlannedAmount) {//不能大于任务计划量  
                        MessageBoxForm.Show(this, "数量不能大于计划量", "提示", this.DefaultButtonResults);
                        this.txtThisShelvesAmount.Text = (partsShelvesExtends[0].PlannedAmount - integerToInt(partsShelvesExtends[0].ShelvesAmount)).ToString();
                        return;
                    }
                    partsShelvesExtends[0].ThisShelvesAmount = Convert.ToInt32(thisNumber.Trim());
                } else {
                    if (partsShelvesExtends != null)
                        partsShelvesExtends[0].ThisShelvesAmount = 0;
                }
            } catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }

        private void txtScanLocation_KeyDown(object sender, KeyEventArgs e) {
            if (ScanTicketClear == 0) {
                this.txtScanLocation.Text = string.Empty;
                ScanTicketClear++;
            }
            if (e.KeyCode == Keys.Enter) {
                ScanTicketClear = 0;
                if (!string.IsNullOrEmpty(this.txtScanLocation.Text)) {
                    partsShelvesExtends[0].TargetWarehouseAreaCode = this.txtScanLocation.Text;
                    this.btnSubmit.Enabled = true;
                }
            }
        }
    }
}