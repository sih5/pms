﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Sunlight.Ce.Controls;
using Sunlight.Ce.Entities;

namespace Sunlight.Ce.WinUI {
    public partial class StockFrm : BaseFrm {
        List<PartsStock> stock = null;

        private void InitControl() {
            var dt = this.QetDataTableByCaseNo(this.stock);
            SetDataGridStyleCaseNo(this.dgvStock, dt);
            this.dgvStock.DataSource = dt;
        }

        private DataTable QetDataTableByCaseNo(List<PartsStock> stock) {
            var dt = new DataTable();
            dt.TableName = "查询结果";
            dt.Columns.Add("货位编号");
            dt.Columns.Add("数量");
            dt.Columns.Add("配件编号");
            dt.Columns.Add("配件名称");

            if (stock.Count > 0) {
                foreach (var st in stock) {
                    DataRow dr = dt.NewRow();
                    dr["货位编号"] = st.WarehouseAreaCode;
                    dr["数量"] = st.Quantity;
                    dr["配件编号"] = st.SparePartCode;
                    dr["配件名称"] = st.SparePartName;
                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }

        private void SetDataGridStyleCaseNo(DataGrid dg, DataTable dt) {
            var alternatingColor = SystemColors.ControlDark;//交替行颜色

            dg.BackgroundColor = Color.White;
            dg.RowHeadersVisible = false;

            var dataGridTableStyle = new DataGridTableStyle();
            dataGridTableStyle.MappingName = dt.TableName;

            dataGridTableStyle.GridColumnStyles.Add(new DataGridCustomTextBoxColumn() {
                Owner = dg,
                HeaderText = "货位编号",
                MappingName = dt.Columns[0].ColumnName,
                NullText = "",
                ReadOnly = true,
                Alignment = HorizontalAlignment.Left,
                AlternatingBackColor = alternatingColor,
                Width = 70
            });

            dataGridTableStyle.GridColumnStyles.Add(new DataGridCustomTextBoxColumn() {
                Owner = dg,
                HeaderText = "数量",
                MappingName = dt.Columns[1].ColumnName,
                NullText = "",
                ReadOnly = true,
                Alignment = HorizontalAlignment.Center,
                AlternatingBackColor = alternatingColor,
                Width = 30
            });
            dataGridTableStyle.GridColumnStyles.Add(new DataGridCustomTextBoxColumn() {
                Owner = dg,
                HeaderText = "配件编号",
                MappingName = dt.Columns[2].ColumnName,
                NullText = "",
                ReadOnly = true,
                Alignment = HorizontalAlignment.Left,
                AlternatingBackColor = alternatingColor,
                Width = 100
            });

            dataGridTableStyle.GridColumnStyles.Add(new DataGridCustomTextBoxColumn() {
                Owner = dg,
                HeaderText = "配件名称",
                MappingName = dt.Columns[3].ColumnName,
                NullText = "",
                ReadOnly = true,
                Alignment = HorizontalAlignment.Left,
                AlternatingBackColor = alternatingColor,
                Width = 130
            });

            dg.TableStyles.Add(dataGridTableStyle);
        }

        public StockFrm(List<PartsStock> st) {
            InitializeComponent();

            stock = st;
            InitControl();
        }
    }
}