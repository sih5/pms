﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Sunlight.Ce.Entities;
using Sunlight.Ce.Entities.Menus;
using Sunlight.Ce.Service;
using Sunlight.Ce.Controls;

namespace Sunlight.Ce.WinUI {
    public partial class StockTaskFrm : BaseFrm {
        private AuthorizeUser user = ServiceManager.GetCurrentUser();
        private WarehouseOperator wo = ServiceManager.GetCurrentWarehouse();

        private void txtPartCode_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                try {
                    if (string.IsNullOrEmpty(this.txtPartCode.Text))
                        return;
                    Cursor.Current = Cursors.WaitCursor;
                    var result = ServiceManager.GetBasicLocation(this.txtPartCode.Text, null, this.cboxIsTrue.Checked);
                    Cursor.Current = Cursors.Default;
                    if (result.IsSuccess) {
                        if (result.Data != null && result.Data.Count > 0) {
                            StockFrm stock = new StockFrm(result.Data);
                            stock.ShowDialog();
                            this.txtPartCode.Text = string.Empty;
                        }
                        else {
                            MessageBoxForm.Show(this, "没有库存记录！", "提示", new Dictionary<string, DialogResult> { { "确认", DialogResult.OK } });
                        }
                    }else{
                        MessageBoxForm.Show(this,result.Message,"提示",new Dictionary<string,DialogResult>{{"确认",DialogResult.OK}});
                        this.txtPartCode.Text = null;
                    }
                }
                catch (Exception ex) {
                    MessageBox.Show(ex.Message);
                    this.txtPartCode.Text = null;
                }
            }
        }

        private void txtLocationCode_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                try {
                    if (string.IsNullOrEmpty(this.txtLocationCode.Text))
                        return;
                    Cursor.Current = Cursors.WaitCursor;
                    var result = ServiceManager.GetBasicLocation(null, this.txtLocationCode.Text, this.cboxIsTrue.Checked);
                    Cursor.Current = Cursors.Default;
                    if (result.IsSuccess) {
                        if (result.Data != null && result.Data.Count > 0) {
                            StockFrm stock = new StockFrm(result.Data);
                            stock.ShowDialog();
                            this.txtLocationCode.Text = string.Empty;
                        }
                    }
                    else {
                        MessageBoxForm.Show(this, result.Message, "提示", new Dictionary<string, DialogResult> { { "确认", DialogResult.OK } });
                        this.txtLocationCode.Text = null;
                    }
                }
                catch (Exception ex) {
                    MessageBox.Show(ex.Message);
                    this.txtLocationCode.Text = null;
                }
            }
        }

        public StockTaskFrm() {
            InitializeComponent();

            this.txtPartCode.Focus();
            this.cboxIsTrue.Checked = true;
        }
    }
}