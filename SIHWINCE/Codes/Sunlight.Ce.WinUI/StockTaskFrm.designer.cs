﻿namespace Sunlight.Ce.WinUI {
    partial class StockTaskFrm {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent() {
            this.txtPartCode = new System.Windows.Forms.TextBox();
            this.txtLocationCode = new System.Windows.Forms.TextBox();
            this.lblPartCode = new System.Windows.Forms.Label();
            this.lblLocationCode = new System.Windows.Forms.Label();
            this.cboxIsTrue = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // txtPartCode
            // 
            this.txtPartCode.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.txtPartCode.Location = new System.Drawing.Point(18, 84);
            this.txtPartCode.Name = "txtPartCode";
            this.txtPartCode.Size = new System.Drawing.Size(208, 29);
            this.txtPartCode.TabIndex = 0;
            this.txtPartCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPartCode_KeyDown);
            // 
            // txtLocationCode
            // 
            this.txtLocationCode.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.txtLocationCode.Location = new System.Drawing.Point(18, 171);
            this.txtLocationCode.Name = "txtLocationCode";
            this.txtLocationCode.Size = new System.Drawing.Size(208, 29);
            this.txtLocationCode.TabIndex = 0;
            this.txtLocationCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtLocationCode_KeyDown);
            // 
            // lblPartCode
            // 
            this.lblPartCode.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular);
            this.lblPartCode.Location = new System.Drawing.Point(18, 51);
            this.lblPartCode.Name = "lblPartCode";
            this.lblPartCode.Size = new System.Drawing.Size(179, 20);
            this.lblPartCode.Text = "配件编号/条码";
            // 
            // lblLocationCode
            // 
            this.lblLocationCode.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular);
            this.lblLocationCode.Location = new System.Drawing.Point(15, 136);
            this.lblLocationCode.Name = "lblLocationCode";
            this.lblLocationCode.Size = new System.Drawing.Size(179, 20);
            this.lblLocationCode.Text = "货位编号";
            // 
            // cboxIsTrue
            // 
            this.cboxIsTrue.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular);
            this.cboxIsTrue.Location = new System.Drawing.Point(18, 220);
            this.cboxIsTrue.Name = "cboxIsTrue";
            this.cboxIsTrue.Size = new System.Drawing.Size(191, 20);
            this.cboxIsTrue.TabIndex = 2;
            this.cboxIsTrue.Text = "库存数量大于零";
            // 
            // StockTaskFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 295);
            this.Controls.Add(this.cboxIsTrue);
            this.Controls.Add(this.lblLocationCode);
            this.Controls.Add(this.lblPartCode);
            this.Controls.Add(this.txtLocationCode);
            this.Controls.Add(this.txtPartCode);
            this.KeyPreview = true;
            this.Name = "StockTaskFrm";
            this.Text = "库存查询";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtPartCode;
        private System.Windows.Forms.TextBox txtLocationCode;
        private System.Windows.Forms.Label lblPartCode;
        private System.Windows.Forms.Label lblLocationCode;
        private System.Windows.Forms.CheckBox cboxIsTrue;
    }
}