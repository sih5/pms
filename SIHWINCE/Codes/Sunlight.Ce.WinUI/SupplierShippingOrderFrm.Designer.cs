﻿namespace Sunlight.Ce.WinUI {
    partial class SupplierShippingOrderFrm {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent() {
            this.txtCode = new System.Windows.Forms.TextBox();
            this.lblCode = new System.Windows.Forms.Label();
            this.lblLogisticArrivalDate = new System.Windows.Forms.Label();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.dtLogisticArrivalDate = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // txtCode
            // 
            this.txtCode.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular);
            this.txtCode.Location = new System.Drawing.Point(10, 81);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(207, 29);
            this.txtCode.TabIndex = 0;
            // 
            // lblCode
            // 
            this.lblCode.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular);
            this.lblCode.Location = new System.Drawing.Point(10, 47);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(184, 20);
            this.lblCode.Text = "扫描发运单号：";
            // 
            // lblLogisticArrivalDate
            // 
            this.lblLogisticArrivalDate.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular);
            this.lblLogisticArrivalDate.Location = new System.Drawing.Point(10, 113);
            this.lblLogisticArrivalDate.Name = "lblLogisticArrivalDate";
            this.lblLogisticArrivalDate.Size = new System.Drawing.Size(184, 20);
            this.lblLogisticArrivalDate.Text = "承运商送达时间：";
            // 
            // btnConfirm
            // 
            this.btnConfirm.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular);
            this.btnConfirm.Location = new System.Drawing.Point(129, 198);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(88, 46);
            this.btnConfirm.TabIndex = 2;
            this.btnConfirm.Text = "确认";
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // dtLogisticArrivalDate
            // 
            this.dtLogisticArrivalDate.CalendarFont = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular);
            this.dtLogisticArrivalDate.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Regular);
            this.dtLogisticArrivalDate.Location = new System.Drawing.Point(10, 145);
            this.dtLogisticArrivalDate.Name = "dtLogisticArrivalDate";
            this.dtLogisticArrivalDate.Size = new System.Drawing.Size(207, 30);
            this.dtLogisticArrivalDate.TabIndex = 3;
            this.dtLogisticArrivalDate.Value = new System.DateTime(2020, 3, 24, 17, 28, 36, 0);
            // 
            // SupplierShippingOrderFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 295);
            this.Controls.Add(this.dtLogisticArrivalDate);
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.lblLogisticArrivalDate);
            this.Controls.Add(this.lblCode);
            this.Controls.Add(this.txtCode);
            this.Name = "SupplierShippingOrderFrm";
            this.Text = "供应商收货确认";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtCode;
        private System.Windows.Forms.Label lblCode;
        private System.Windows.Forms.Label lblLogisticArrivalDate;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.DateTimePicker dtLogisticArrivalDate;
    }
}