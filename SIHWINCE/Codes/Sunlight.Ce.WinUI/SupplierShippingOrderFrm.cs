﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Sunlight.Ce.Service;
using Sunlight.Ce.Controls;
using Sunlight.Ce.Entities.DTO;

namespace Sunlight.Ce.WinUI {
    public partial class SupplierShippingOrderFrm : BaseFrm {
        public SupplierShippingOrderFrm() {
            InitializeComponent();

            this.dtLogisticArrivalDate.Value = DateTime.Now;
        }

        private void btnConfirm_Click(object sender, EventArgs e) {
            try {
                if (string.IsNullOrEmpty(this.txtCode.Text)) {
                    MessageBoxForm.Show(this, "请扫描发运单编号.", "提示", this.DefaultButtonResults);
                    return;
                }

                Cursor.Current = Cursors.WaitCursor;
                var result = ServiceManager.ConfirmSupplierShipping(new SupplierShippingOrderDTO { Code = this.txtCode.Text, Date = this.dtLogisticArrivalDate.Value });
                Cursor.Current = Cursors.Default;
                if (result.IsSuccess) {
                    MessageBoxForm.Show(this, "供应商到货确认成功！", "提示", this.DefaultButtonResults);
                    RasieEditSubmitted();
                    this.SetWindowClose();
                } else {
                    MessageBoxForm.Show(this, result.Message, "提示", this.DefaultButtonResults);
                    this.txtCode.Text = null;
                    this.dtLogisticArrivalDate.Value = DateTime.Now;
                }
            } catch (Exception ex) {
                MessageBox.Show(ex.Message);
                this.txtCode.Text = null;
                this.dtLogisticArrivalDate.Value = DateTime.Now;
            }
        }
    }
}