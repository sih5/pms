﻿using System;
using System.Linq;
using System.Drawing;
using System.Windows.Forms;
using Sunlight.Ce.Controls;
using Sunlight.Ce.Service;

namespace Sunlight.Ce.WinUI {
    public partial class SystemMenuFrm : BaseFrm {
        private bool initialize;
        private SystemSubMenuFrm systemSubMenuFrm;
        private void SystemMenuFrm_Load(object sender, EventArgs e) {
            if(initialize)
                return;
            initialize = true;
            if(ServiceManager.GetCurrentAppMenus().SystemModels == null || !ServiceManager.GetCurrentAppMenus().SystemModels.Any()) {
                MessageBoxForm.Show(this, "当前用户，没有任何权限菜单，请联系管理员", "提示", this.DefaultButtonResults);
                return;
            }
            int number = 1;
            var mainMenu = new CustomMenuItem();
            mainMenu.Text = "菜单";
            var systemMenu = new CustomMenuItem();
            systemMenu.Text = "退出系统";
            systemMenu.Click -= this.systemMenu_Click;
            systemMenu.Click += this.systemMenu_Click;
            //this.mainMenuList.MenuItems.Add(mainMenu);
            //this.mainMenuList.MenuItems.Add(systemMenu);

            foreach(var group in ServiceManager.GetCurrentAppMenus().SystemModels[0].GroupModels) {
                var btnMenu = new Button();

                btnMenu.Location = new Point(37, 40 * number + 10 * (number - 1));
                btnMenu.Name = group.PageId;
                btnMenu.Size = new Size(156, 41);
                btnMenu.Tag = group.PageId;
                btnMenu.TabIndex = number - 1;
                btnMenu.Text = group.Name;
                btnMenu.Click -= this.btnMenu_Click;
                btnMenu.Click += this.btnMenu_Click;
                number++;

                this.Controls.Add(btnMenu);

                //处理mainMenu的菜单
                var subMainMenu = new CustomMenuItem();
                subMainMenu.Text = group.Name;
                subMainMenu = Initialize(group.PageId, subMainMenu);
                mainMenu.MenuItems.Add(subMainMenu);
            }
        }

        private CustomMenuItem Initialize(string pageId, CustomMenuItem parent) {
            if(ServiceManager.GetCurrentAppMenus().SystemModels == null || !ServiceManager.GetCurrentAppMenus().SystemModels.Any()) {
                return parent;
            }
            var group = ServiceManager.GetCurrentAppMenus().SystemModels[0].GroupModels.FirstOrDefault(r => r.PageId == pageId);
            if(group == null) {
                return parent;
            }
            foreach(var page in group.PageModels.ToList()) {
                var subMainMenu = new CustomMenuItem();
                subMainMenu.Text = page.Name;
                subMainMenu.Tag = page.PageId;
                subMainMenu.Click -= this.subMainMenu_Click;
                subMainMenu.Click += this.subMainMenu_Click;
                parent.MenuItems.Add(subMainMenu);
            }

            return parent;
        }

        private void subMainMenu_Click(object sender, EventArgs e) {
            var menuItem = sender as CustomMenuItem;
            if(menuItem == null || menuItem.Tag == null)
                return;
            //根据PageId跳转到自己的页面 TODO:
        }

        private void systemMenu_Click(object sender, EventArgs e) {
            Application.Exit();
        }

        private void btnMenu_Click(object sender, EventArgs e) {
            var button = sender as Button;
            if(button == null)
                return;
            systemSubMenuFrm = new SystemSubMenuFrm(button.Name, button.Text);
            systemSubMenuFrm.Owner = this;
            systemSubMenuFrm.ShowDialog();
        }

        public SystemMenuFrm() {
            InitializeComponent();
        }
    }
}