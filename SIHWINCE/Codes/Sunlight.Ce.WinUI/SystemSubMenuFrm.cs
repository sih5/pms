﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Sunlight.Ce.Controls;
using Sunlight.Ce.Service;
using System.Collections.Generic;

namespace Sunlight.Ce.WinUI {
    public partial class SystemSubMenuFrm : BaseFrm {
        /// <summary>
        /// 重置弹出框的OK按钮文字为 “确定”
        /// </summary>
        private readonly Dictionary<string, DialogResult> defaultButtonResults = new Dictionary<string, DialogResult> { { "确定", DialogResult.OK } };

        private void Initialize(string pageId, string title) {
            this.Controls.Clear();
            this.Text = title;
            if (ServiceManager.GetCurrentAppMenus().SystemModels == null || !ServiceManager.GetCurrentAppMenus().SystemModels.Any()) {
                MessageBoxForm.Show(this, "当前用户，没有任何权限菜单，请联系管理员", "提示", this.DefaultButtonResults);
                return;
            }
            var group = ServiceManager.GetCurrentAppMenus().SystemModels[0].GroupModels.FirstOrDefault(r => r.PageId == pageId);
            if (group == null) {
                MessageBoxForm.Show(this, string.Format("{0}下没有任何权限子菜单，请联系管理员", title), "提示", this.DefaultButtonResults);
                return;
            }
            int number = 1;
            foreach (var page in group.PageModels.ToList()) {
                var btnMenu = new Button();

                btnMenu.Location = new Point(37, 40 * number + 10 * (number - 1));
                btnMenu.Name = page.PageId;
                btnMenu.Size = new Size(156, 41);
                btnMenu.Tag = page.PageId;
                btnMenu.TabIndex = number - 1;
                btnMenu.Text = page.Name;
                btnMenu.Click -= this.btnMenu_Click;
                btnMenu.Click += this.btnMenu_Click;
                number++;

                this.Controls.Add(btnMenu);
            }
        }

        //窗体跳转
        private void btnMenu_Click(object sender, EventArgs e) {
            var btnMenu = sender as Button;
            if (btnMenu == null)
                return;
            Form frm;
            switch (btnMenu.Name) {
                case "SIH101":
                    frm = new ReceiptManagementFrm();
                    frm.Owner = this;
                    frm.ShowDialog();
                    break;
                case "SIH102":
                    frm = new PackingManagementFrm();
                    frm.Owner = this;
                    frm.ShowDialog();
                    break;
                case "SIH103":
                    frm = new ShelvesManagementFrm();
                    frm.Owner = this;
                    frm.ShowDialog();
                    break;
                case "SIH104":
                    frm = new CenterReceiptManagementFrm();
                    frm.Owner = this;
                    frm.ShowDialog();
                    break;
                case "SIH105":
                    frm = new CenterShelvesManagementFrm();
                    frm.Owner = this;
                    frm.ShowDialog();
                    break;
                case "SIH106":
                    frm = new SupplierShippingOrderFrm();
                    frm.Owner = this;
                    frm.ShowDialog();
                    break;
                case "SIH401":
                    frm = new StockTaskFrm();
                    frm.Owner = this;
                    frm.ShowDialog();
                    break;
                case "SIH301":
                    frm = new InventoryManagement();
                    frm.Owner = this;
                    frm.ShowDialog();
                    break;
                case "SIH302":
                    frm = new PartsShiftFrm();
                    frm.Owner = this;
                    frm.ShowDialog();
                    break;
                case "SIH201":
                    frm = new PickingManagementFrm();
                    frm.Owner = this;
                    frm.ShowDialog();
                    break;
                case "SIH202":
                    frm = new BoxUpManagementFrm();
                    frm.Owner = this;
                    frm.ShowDialog();
                    break;
                case "SIH203":

                    break;
                default:
                    MessageBoxForm.Show(this, string.Format("没有找到对应的窗体", btnMenu.Name), "提示", this.defaultButtonResults);
                    break;
            }
        }

        public SystemSubMenuFrm(string pageId, string title) {
            InitializeComponent();
            try {
                Initialize(pageId, title);
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch (Exception) {
            }

        }

        public SystemSubMenuFrm() {
            InitializeComponent();
        }
    }
}