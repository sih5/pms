﻿using System.Drawing;
using ZXing;
using ZXing.Rendering;

namespace GDI绘制报表 {
    public class CustomBarcodeWriter : BarcodeWriter {
        public CustomBarcodeWriter() {
            var render = new BitmapRenderer();
            render.Background = Color.Transparent;
            //render.Foreground = Color.Red;
            Renderer = render;
        }
        public CustomBarcodeWriter(Color backgroundColor,Color foreground) {
            var render = new BitmapRenderer();
            render.Background = backgroundColor;
            render.Foreground = foreground;
            Renderer = render;
        }
    }
}
