﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using GDI绘制报表.Reports;
using Sunlight.Ce.Entities.Extends;
using ZXing;
using ZXing.Common;
using ZXing.QrCode;

namespace GDI绘制报表 {
    /// <summary>
    ///     绘制收货确认 标签页打印
    /// </summary>
    public partial class MainFrm : Form {
        public MainFrm() {
            InitializeComponent();
            WindowState = FormWindowState.Maximized;
        }

        private void button1_Click(object sender, EventArgs e) {
            var ghp = CreateGraphics();
            ghp.Clear(BackColor);
            var myPen = new Pen(Color.Black, 1);

            var sparePartName = "后封水盖板垫 (发动机水套后工艺孔封水盖板.491Q发动机使用)，后排肘靠右";

            //TODO中心线 307
            //绘制边框线
            ghp.DrawLine(myPen, 17, 102, 17, 302);
            ghp.DrawLine(myPen, 17, 302, 605, 302);
            ghp.DrawLine(myPen, 17, 102, 605, 102);
            ghp.DrawLine(myPen, 605, 102, 605, 302);

            ////绘制横线
            //ghp.DrawLine(myPen, 10, 155, 605, 155);
            //ghp.DrawLine(myPen, 10, 225, 307, 225);

            ////中心分割线
            //ghp.DrawLine(myPen, 307, 102, 307, 302);
            //ghp.DrawLine(myPen, 407, 102, 407, 302);
            Brush br = new SolidBrush(Color.Black);
            ghp.DrawString("EB24329392", new Font("宋体", 20, FontStyle.Regular), br, 20, 135);
            ghp.DrawString("QTY：", new Font("宋体", 20, FontStyle.Regular), br, 300, 135);
            ghp.DrawString("10000", new Font("宋体", 20, FontStyle.Regular), br, 360, 135);

            //ghp.DrawString("名称：", new Font("宋体", 20, FontStyle.Regular), br, 20, 145);
            if(!string.IsNullOrEmpty(sparePartName) && sparePartName.Length <= 13) {
                ghp.DrawString(sparePartName, new Font("宋体", 20, FontStyle.Regular), br, 20, 175);
            } else if(!string.IsNullOrEmpty(sparePartName) && sparePartName.Length > 13 && sparePartName.Length <= 26) {
                ghp.DrawString(sparePartName.Substring(0, 13), new Font("宋体", 20, FontStyle.Regular), br, 20, 175);
                ghp.DrawString(sparePartName.Substring(13), new Font("宋体", 20, FontStyle.Regular), br, 20, 205);
                ghp.DrawString("FUSE", new Font("宋体", 20, FontStyle.Regular), br, 20, 245);
            } else if(!string.IsNullOrEmpty(sparePartName) && sparePartName.Length > 26) {
                ghp.DrawString(sparePartName.Substring(0, 13), new Font("宋体", 20, FontStyle.Regular), br, 20, 175);
                ghp.DrawString(sparePartName.Substring(13, 13), new Font("宋体", 20, FontStyle.Regular), br, 20, 205);
                ghp.DrawString(sparePartName.Substring(26), new Font("宋体", 20, FontStyle.Regular), br, 20, 235);
                ghp.DrawString("FUSE", new Font("宋体", 20, FontStyle.Regular), br, 20, 275);
            }

            ghp.DrawImage(Generate(), new Point {
                X = 430,
                Y = 145
            });
        }

        private static Bitmap Generate() {
            EncodingOptions options = new QrCodeEncodingOptions {
                DisableECI = false,
                CharacterSet = "UTF-8",
                Width = 150,
                Height = 150,
                Margin = 0
            };
            BarcodeWriter writer = new CustomBarcodeWriter();
            writer.Format = BarcodeFormat.QR_CODE;
            writer.Options = options;

            var bitmap = writer.Write("EB24329392");

            return bitmap;
        }

        private static Bitmap GenerateBarCode() {
            EncodingOptions options = new QrCodeEncodingOptions {
                DisableECI = false,
                CharacterSet = "UTF-8",
                Width = 500,
                Height = 80,
                Margin = 0,
                PureBarcode = true
            };
            BarcodeWriter writer = new CustomBarcodeWriter(Color.Transparent, Color.Black);
            writer.Format = BarcodeFormat.CODE_128;
            writer.Options = options;

            var bitmap = writer.Write("3622553569688863");

            return bitmap;
        }

        private void button2_Click(object sender, EventArgs e) {
            WindowState = FormWindowState.Maximized;
            var result = new List<PartsInboundPlanDetailExtend>();
            for(int i = 0; i < 35; i++) {
                var detail = new PartsInboundPlanDetailExtend();
                detail.Id = i + 1;
                detail.DefaultSupplier = "BAIC";
                detail.SparePartCode = string.Format("EB243293{0}", i);
                detail.SparePartName = "带扣总成" + i;
                detail.PlannedAmount = i * 10;
                detail.InspectedQuantity = i + 1;
                detail.Remark = "RAW协议是大多数打印设备的默认协议。";
                result.Add(detail);
            }

            //收货确认 出库计划打印
            var ghp = CreateGraphics();
            ghp.Clear(BackColor);
            var defaultPen = new Pen(Color.Black, 1);

            //绘制边框线
            ghp.DrawLine(defaultPen, 10, 85, 10, 1100);
            ghp.DrawLine(defaultPen, 10, 1100, 1100, 1100);
            ghp.DrawLine(defaultPen, 10, 85, 1100, 85);
            ghp.DrawLine(defaultPen, 1100, 85, 1100, 1100);

            //绘制横线
            ghp.DrawLine(defaultPen, 10, 155, 1100, 155);
            ghp.DrawLine(defaultPen, 10, 255, 1100, 255);
            ghp.DrawLine(defaultPen, 10, 290, 1100, 290);

            //分割线 只是为布局使用，正式环境，需要禁用掉
            ghp.DrawLine(defaultPen, 60, 255, 60, 1100);
            ghp.DrawLine(defaultPen, 150, 255, 150, 1100);
            ghp.DrawLine(defaultPen, 300, 255, 300, 1100);
            ghp.DrawLine(defaultPen, 400, 255, 400, 1100);
            ghp.DrawLine(defaultPen, 500, 255, 500, 1100);
            ghp.DrawLine(defaultPen, 600, 255, 600, 1100);
            ghp.DrawLine(defaultPen, 700, 255, 700, 1100);
            ghp.DrawLine(defaultPen, 800, 255, 800, 1100);
            ghp.DrawLine(defaultPen, 900, 255, 900, 1100);

            Brush titleBrush = new SolidBrush(Color.Black);
            ghp.DrawString("北京汽车国际发展有限公司备件入库计划单", new Font("宋体", 20, FontStyle.Regular), titleBrush, 305, 105);

            //创建页脚
            var defaultDownPen = new Pen(Color.Green, 1);
            ghp.DrawLine(defaultDownPen, 10, 900, 1100, 900);
            ghp.DrawString("制单单位：", new Font("宋体", 13, FontStyle.Regular), titleBrush, 20, 905);
            ghp.DrawString("打印时间：", new Font("宋体", 13, FontStyle.Regular), titleBrush, 20, 935);

            ghp.DrawString("制单人：", new Font("宋体", 13, FontStyle.Regular), titleBrush, 400, 905);
            ghp.DrawString("制单时间：", new Font("宋体", 13, FontStyle.Regular), titleBrush, 550, 905);

            ghp.DrawString("打印人：", new Font("宋体", 13, FontStyle.Regular), titleBrush, 800, 905);
            ghp.DrawString(string.Format("第{0}页 共{1}页", 1, Math.Ceiling(result.Count * 1.0 / 10)), new Font("宋体", 13, FontStyle.Regular), titleBrush, 800, 935);
            ghp.DrawString("注：", new Font("宋体", 13, FontStyle.Regular), titleBrush, 400, 935);

            ghp.DrawString("供货单位：", new Font("宋体", 13, FontStyle.Regular), titleBrush, 20, 165);
            ghp.DrawString("收货单位：", new Font("宋体", 13, FontStyle.Regular), titleBrush, 20, 195);
            ghp.DrawString("采购订单类型：", new Font("宋体", 13, FontStyle.Regular), titleBrush, 20, 225);

            ghp.DrawString("入库计划单号：", new Font("宋体", 13, FontStyle.Regular), titleBrush, 500, 165);
            ghp.DrawString("入库类型：", new Font("宋体", 13, FontStyle.Regular), titleBrush, 500, 195);
            ghp.DrawString("源单据编号：", new Font("宋体", 13, FontStyle.Regular), titleBrush, 500, 225);

            ghp.DrawString("序号", new Font("宋体", 13, FontStyle.Regular), titleBrush, 15, 265);
            ghp.DrawString("二维码", new Font("宋体", 13, FontStyle.Regular), titleBrush, 65, 265);
            ghp.DrawString("备件编号", new Font("宋体", 13, FontStyle.Regular), titleBrush, 155, 265);
            ghp.DrawString("备件名称", new Font("宋体", 13, FontStyle.Regular), titleBrush, 305, 265);
            ghp.DrawString("参考图号", new Font("宋体", 13, FontStyle.Regular), titleBrush, 405, 265);
            ghp.DrawString("供应商图号", new Font("宋体", 13, FontStyle.Regular), titleBrush, 505, 265);
            ghp.DrawString("计划数量", new Font("宋体", 13, FontStyle.Regular), titleBrush, 605, 265);
            ghp.DrawString("检验数量", new Font("宋体", 13, FontStyle.Regular), titleBrush, 705, 265);
            ghp.DrawString("单位", new Font("宋体", 13, FontStyle.Regular), titleBrush, 805, 265);
            ghp.DrawString("备注", new Font("宋体", 13, FontStyle.Regular), titleBrush, 905, 265);

            var row = 1;
            foreach(var partsInboundPlanDetailExtend in result) {
                //第一页 小于等于15时，第一页
                if(row < 11) {
                    ghp.DrawString(string.Format("{0}", partsInboundPlanDetailExtend.Id), new Font("宋体", 13, FontStyle.Regular), titleBrush, 15, 290 + 60 * row - 37);
                    ghp.DrawImage(GenerateSmall(partsInboundPlanDetailExtend.SparePartCode), new Point {
                        X = 65,
                        Y = 235 + 60 * row
                    });
                    ghp.DrawString(string.Format("{0}", partsInboundPlanDetailExtend.SparePartCode), new Font("宋体", 13, FontStyle.Regular), titleBrush, 155, 290 + 60 * row - 37);
                    var spareNameRows = GetStringRows(ghp, new Font("宋体", 13, FontStyle.Regular), partsInboundPlanDetailExtend.SparePartName, 150);
                    if(spareNameRows != null && spareNameRows.Count == 1)
                        ghp.DrawString(string.Format("{0}", partsInboundPlanDetailExtend.SparePartName), new Font("宋体", 13, FontStyle.Regular), titleBrush, 305, 290 + 60 * row - 37);
                    else if(spareNameRows != null && spareNameRows.Count > 1) {
                        for(int i = spareNameRows.Count - 1; i >= 0; i--) {
                            ghp.DrawString(string.Format("{0}", spareNameRows[i]), new Font("宋体", 13, FontStyle.Regular), titleBrush, 305, 290 + 60 * row - (18 + 20 * (spareNameRows.Count - 1 - i)));
                        }
                    }

                    ghp.DrawString(string.Format("{0}", partsInboundPlanDetailExtend.SparePartCode), new Font("宋体", 13, FontStyle.Regular), titleBrush, 405, 290 + 60 * row - 37);
                    ghp.DrawString(string.Format("{0}", partsInboundPlanDetailExtend.DefaultSupplier), new Font("宋体", 13, FontStyle.Regular), titleBrush, 505, 290 + 60 * row - 37);
                    ghp.DrawString(string.Format("{0}", partsInboundPlanDetailExtend.PlannedAmount), new Font("宋体", 13, FontStyle.Regular), titleBrush, 605, 290 + 60 * row - 37);
                    ghp.DrawString(string.Format("{0}", partsInboundPlanDetailExtend.InspectedQuantity), new Font("宋体", 13, FontStyle.Regular), titleBrush, 705, 290 + 60 * row - 37);
                    ghp.DrawString(string.Format("{0}", "个"), new Font("宋体", 13, FontStyle.Regular), titleBrush, 805, 290 + 60 * row - 37);

                    var rows = GetStringRows(ghp, new Font("宋体", 13, FontStyle.Regular), partsInboundPlanDetailExtend.Remark, 200);
                    if(rows != null && rows.Count == 1)
                        ghp.DrawString(string.Format("{0}", partsInboundPlanDetailExtend.Remark), new Font("宋体", 13, FontStyle.Regular), titleBrush, 905, 290 + 60 * row - 37);
                    else if(rows != null && rows.Count > 1) {
                        for(int i = rows.Count - 1; i >= 0; i--) {
                            ghp.DrawString(string.Format("{0}", rows[i]), new Font("宋体", 13, FontStyle.Regular), titleBrush, 905, 290 + 60 * row - (18 + 20 * (rows.Count - 1 - i)));
                        }
                    }
                    ghp.DrawLine(defaultPen, 10, 290 + 60 * row, 1100, 290 + 60 * row);

                    row++;
                }
            }
        }

        private static Bitmap GenerateSmall(string sparePartCode) {
            EncodingOptions options = new QrCodeEncodingOptions {
                DisableECI = false,
                CharacterSet = "UTF-8",
                Width = 55,
                Height = 55,
                Margin = 0
            };
            BarcodeWriter writer = new CustomBarcodeWriter();
            writer.Format = BarcodeFormat.QR_CODE;
            writer.Options = options;

            var bitmap = writer.Write(sparePartCode);

            return bitmap;
        }

        private void button3_Click(object sender, EventArgs e) {
            CreateGraphics().Clear(this.BackColor);
        }

        private void button4_Click(object sender, EventArgs e) {
            //出库装箱 标签打印
            var ghp = CreateGraphics();
            ghp.Clear(BackColor);
            var myPen = new Pen(Color.Black, 1);

            //TODO中心线 307
            //绘制边框线
            ghp.DrawLine(myPen, 10, 85, 10, 295);
            ghp.DrawLine(myPen, 10, 295, 605, 295);
            ghp.DrawLine(myPen, 10, 85, 605, 85);
            ghp.DrawLine(myPen, 605, 85, 605, 295);

            ////绘制横线
            //ghp.DrawLine(myPen, 10, 175, 605, 175);
            //ghp.DrawLine(myPen, 10, 210, 605, 210);
            //ghp.DrawLine(myPen, 10, 240, 605, 240);
            //ghp.DrawLine(myPen, 10, 270, 605, 270);

            Brush br = new SolidBrush(Color.Black);

            var length = 490 / "3622553569688863".Length;
            var number = 0;
            foreach(var str in "3622553569688863") {
                ghp.DrawString(str.ToString(), new Font("宋体", 15, FontStyle.Bold), br, 68 + number * length, 175);
                number++;
            }
            br = new SolidBrush(Color.Black);
            ghp.DrawString(string.Format("CASE NO：{0}", "20170703B 16532342CL-Z2002"), new Font("宋体", 15, FontStyle.Regular), br, 60, 200);
            ghp.DrawString(string.Format("SHIPPING MARK ：{0}", "BAIC"), new Font("宋体", 15, FontStyle.Regular), br, 60, 220);
            ghp.DrawString(string.Format("CONTRACT NO：{0}", "BQGJ16456ZA-1"), new Font("宋体", 15, FontStyle.Regular), br, 60, 240);
            ghp.DrawString(string.Format("CONSIGNEE：{0}", "BAIC SA INVESTMENT (PTY) LTD"), new Font("宋体", 15, FontStyle.Regular), br, 60, 260);

            ghp.DrawImage(GenerateBarCode(), new Point {
                X = 60,
                Y = 95
            });
        }

        /// <summary>
        ///     将文本分行
        /// </summary>
        /// <param name="graphic">绘图图面</param>
        /// <param name="font">字体</param>GenerateSmall
        /// <param name="text">文本</param>
        /// <param name="width">行宽</param>
        /// <returns></returns>
        private List<string> GetStringRows(Graphics graphic, Font font, string text, int width) {
            int rowBeginIndex = 0;
            int textLength = text.Length;
            List<string> textRows = new List<string>();

            for(int index = 0; index < textLength; index++) {
                var rowEndIndex = index;

                if(index == textLength - 1) {
                    textRows.Add(text.Substring(rowBeginIndex));
                } else if(rowEndIndex + 1 < text.Length && text.Substring(rowEndIndex, 2) == "\\r\\n") {
                    textRows.Add(text.Substring(rowBeginIndex, rowEndIndex - rowBeginIndex));
                    rowEndIndex = index += 2;
                    rowBeginIndex = rowEndIndex;
                } else if(graphic.MeasureString(text.Substring(rowBeginIndex, rowEndIndex - rowBeginIndex + 1), font).Width > width) {
                    textRows.Add(text.Substring(rowBeginIndex, rowEndIndex - rowBeginIndex));
                    rowBeginIndex = rowEndIndex;
                }
            }

            return textRows;
        }

        private void button5_Click(object sender, EventArgs e) {
        }

        //第二页显示内容
        private void btnSecond_Click(object sender, EventArgs e) {
            var dataGrid = new DataGridItem();
            dataGrid.RowHeight = 60;
            dataGrid.Location = new Point {
                X = 10,
                Y = 200
            };
            dataGrid.PaddingLeft = 5;
            var rowHead = new RowHeader();
            rowHead.HeadHeight = 25;

            rowHead.Columns.Add(new HeadColumnItem {
                Width = 100,
                Name = "序号",
                Title = "序号",
                DisplayIndex = 0
            });
            rowHead.Columns.Add(new HeadColumnItem {
                Width = 100,
                Name = "二维码",
                Title = "二维码",
                DisplayIndex = 1
            });
            rowHead.Columns.Add(new HeadColumnItem {
                Width = 100,
                Name = "备件编号",
                Title = "备件编号",
                DisplayIndex = 2
            });
            rowHead.Columns.Add(new HeadColumnItem {
                Width = 100,
                Name = "备件名称",
                Title = "备件名称",
                DisplayIndex = 3
            });
            rowHead.Columns.Add(new HeadColumnItem {
                Width = 100,
                Name = "参考图号",
                Title = "参考图号",
                DisplayIndex = 4
            });
            rowHead.Columns.Add(new HeadColumnItem {
                Width = 100,
                Name = "供应商图号",
                Title = "供应商图号",
                DisplayIndex = 5
            });
            rowHead.Columns.Add(new HeadColumnItem {
                Width = 100,
                Name = "计划数量",
                Title = "计划数量",
                DisplayIndex = 6
            });
            rowHead.Columns.Add(new HeadColumnItem {
                Width = 100,
                Name = "检验数量",
                Title = "检验数量",
                DisplayIndex = 7
            });
            rowHead.Columns.Add(new HeadColumnItem {
                Width = 100,
                Name = "单位",
                Title = "单位",
                DisplayIndex = 8
            });
            rowHead.Columns.Add(new HeadColumnItem {
                Width = 100,
                Name = "备注",
                Title = "备注",
                DisplayIndex = 9
            });

            dataGrid.RowHeader = rowHead;
            var result = new List<PartsInboundPlanDetailExtend>();
            //TODO：此处处理当前页的数据，分页在外部核算
            for(int i = 0; i < 10; i++) {
                var detail = new PartsInboundPlanDetailExtend();
                detail.Id = i + 1;
                detail.DefaultSupplier = "BAIC";
                detail.SparePartCode = string.Format("EB243293{0}", i);
                detail.SparePartName = "带扣总成" + i;
                detail.PlannedAmount = i * 10;
                detail.InspectedQuantity = i + 1;
                detail.Remark = string.Format("RAW协议是大多数打印设备的默认{0}", i + 1);
                result.Add(detail);
            }
            var rowItems = new List<RowItem>();
            var serial = 1;
            foreach(var partsInboundPlanDetailExtend in result) {
                var rowItem = new RowItem();
                var column = new ColumnItem();
                rowItem.Serial = serial;
                rowItem.Height = dataGrid.RowHeight;

                column.Name = "序号";
                column.DisplayIndex = 0;
                column.Text = partsInboundPlanDetailExtend.Id.ToString();
                rowItem.Columns.Add(column);

                column = new ColumnItem();
                column.Name = "二维码";
                column.DisplayIndex = 1;
                column.Text = partsInboundPlanDetailExtend.SparePartCode;
                rowItem.Columns.Add(column);

                column = new ColumnItem();
                column.Name = "备件编号";
                column.DisplayIndex = 2;
                column.Text = partsInboundPlanDetailExtend.SparePartCode;
                rowItem.Columns.Add(column);

                column = new ColumnItem();
                column.Name = "备件名称";
                column.DisplayIndex = 3;
                column.Text = partsInboundPlanDetailExtend.SparePartName;
                rowItem.Columns.Add(column);

                column = new ColumnItem();
                column.Name = "参考图号";
                column.DisplayIndex = 4;
                column.Text = partsInboundPlanDetailExtend.SparePartCode;
                rowItem.Columns.Add(column);

                column = new ColumnItem();
                column.Name = "供应商图号";
                column.DisplayIndex = 5;
                column.Text = partsInboundPlanDetailExtend.DefaultSupplier;
                rowItem.Columns.Add(column);

                column = new ColumnItem();
                column.Name = "计划数量";
                column.DisplayIndex = 6;
                column.Text = partsInboundPlanDetailExtend.PlannedAmount.ToString();
                rowItem.Columns.Add(column);

                column = new ColumnItem();
                column.Name = "检验数量";
                column.DisplayIndex = 7;
                column.Text = partsInboundPlanDetailExtend.InspectedQuantity.ToString();
                rowItem.Columns.Add(column);

                column = new ColumnItem();
                column.Name = "单位";
                column.DisplayIndex = 8;
                column.Text = "个";
                rowItem.Columns.Add(column);

                column = new ColumnItem();
                column.Name = "备注";
                column.DisplayIndex = 9;
                column.Text = partsInboundPlanDetailExtend.Remark;
                rowItem.Columns.Add(column);
                rowItems.Add(rowItem);

                serial++;
            }
            dataGrid.RowItems.AddRange(rowItems);
            dataGrid.Size = new Size(rowHead.Columns.Sum(r => r.Width), dataGrid.RowItems.Sum(r => r.Height));

            //开始绘制
            var ghp = CreateGraphics();
            ghp.Clear(BackColor);
            var defaultPen = dataGrid.DefaultPen;
            var titleBrush = dataGrid.DefaultBrush;
            //绘制边框线
            ghp.DrawLine(defaultPen, dataGrid.Location.X, dataGrid.Location.Y, dataGrid.Location.X, dataGrid.Size.Height + dataGrid.Location.Y);
            ghp.DrawLine(defaultPen, dataGrid.Location.X, dataGrid.Size.Height + dataGrid.Location.Y, dataGrid.Location.X + dataGrid.Size.Width, dataGrid.Size.Height + dataGrid.Location.Y);
            ghp.DrawLine(defaultPen, dataGrid.Location.X, dataGrid.Location.Y, dataGrid.Location.X + dataGrid.Size.Width, dataGrid.Location.Y);
            ghp.DrawLine(defaultPen, dataGrid.Location.X + dataGrid.Size.Width, dataGrid.Location.Y, dataGrid.Location.X + dataGrid.Size.Width, dataGrid.Size.Height + dataGrid.Location.Y);
            //绘制页脚
            ghp.DrawString("制单单位：", new Font("宋体", 13, FontStyle.Regular), titleBrush, 20, 905);
            ghp.DrawString("打印时间：", new Font("宋体", 13, FontStyle.Regular), titleBrush, 20, 935);

            ghp.DrawString("制单人：", new Font("宋体", 13, FontStyle.Regular), titleBrush, 400, 905);
            ghp.DrawString("制单时间：", new Font("宋体", 13, FontStyle.Regular), titleBrush, 550, 905);

            ghp.DrawString("打印人：", new Font("宋体", 13, FontStyle.Regular), titleBrush, 800, 905);
            ghp.DrawString(string.Format("第{0}页 共{1}页", 1, Math.Ceiling(result.Count * 1.0 / 10)), new Font("宋体", 13, FontStyle.Regular), titleBrush, 800, 935);
            ghp.DrawString("注：", new Font("宋体", 13, FontStyle.Regular), titleBrush, 400, 935);

            foreach(var rowHeaderColumn in dataGrid.RowHeader.Columns) {
                var width = dataGrid.RowHeader.Columns.Where(r => r.DisplayIndex <= rowHeaderColumn.DisplayIndex).Sum(r => r.Width);

                ghp.DrawLine(defaultPen, dataGrid.Location.X + width, dataGrid.Location.Y, dataGrid.Location.X + width, dataGrid.Location.Y + dataGrid.Size.Height);
            }
            foreach(var row in dataGrid.RowItems) {
                var height = dataGrid.RowItems.Where(r => r.Serial <= row.Serial).Sum(r => r.Height);
                var width = dataGrid.RowHeader.Columns.Sum(r => r.Width);

                foreach(var columnItem in row.Columns) {
                    var pointX = dataGrid.RowHeader.Columns.Where(r => r.DisplayIndex < columnItem.DisplayIndex).Sum(r => r.Width) + dataGrid.PaddingLeft + dataGrid.Location.X;
                    var sumRowHeight = dataGrid.RowItems.Where(r => r.Serial < row.Serial).Sum(r => r.Height) + dataGrid.Location.Y;

                    if(dataGrid.Location.Y < dataGrid.RowHeight)
                        sumRowHeight = sumRowHeight - dataGrid.RowHeight;

                    if(columnItem.DisplayIndex == 1) {
                        ghp.DrawImage(GenerateQrCode(columnItem.Text), new Point {
                            X = pointX,
                            Y = sumRowHeight + 5
                        });
                    } else if(columnItem.DisplayIndex == 3) {
                        ghp.DrawString(string.Format("{0}", columnItem.Text), dataGrid.RowFont, titleBrush, pointX, sumRowHeight + (int)Math.Floor(dataGrid.RowHeight * 1.0 / 3));
                    } else if(columnItem.DisplayIndex == 9) {
                        var spareNameRows = GetStringRows(ghp, dataGrid.RowFont, columnItem.Text, dataGrid.RowHeader.Columns[columnItem.DisplayIndex].Width);

                        if(spareNameRows != null && spareNameRows.Count == 1)
                            ghp.DrawString(string.Format("{0}", columnItem.Text), dataGrid.RowFont, titleBrush, pointX, sumRowHeight + (int)Math.Floor(dataGrid.RowHeight * 1.0 / 3));
                        else if(spareNameRows != null && spareNameRows.Count > 1) {
                            for(int i = spareNameRows.Count - 1; i >= 0; i--) {
                                var remarkHeight = sumRowHeight - (18 + 20 * (spareNameRows.Count - 1 - i)) + dataGrid.RowHeight;
                                ghp.DrawString(string.Format("{0}", spareNameRows[i]), dataGrid.RowFont, titleBrush, pointX, remarkHeight);
                            }
                        }
                    } else {
                        ghp.DrawString(string.Format("{0}", columnItem.Text), dataGrid.RowFont, titleBrush, pointX, sumRowHeight + (int)Math.Floor(dataGrid.RowHeight * 1.0 / 3));//290 + 60 * pointX - 37);
                    }
                }
                ghp.DrawLine(defaultPen, dataGrid.Location.X, dataGrid.Location.Y + height, dataGrid.Location.X + width, dataGrid.Location.Y + height);
            }
        }

        private static Bitmap GenerateQrCode(string code) {
            EncodingOptions options = new QrCodeEncodingOptions {
                DisableECI = false,
                CharacterSet = "UTF-8",
                Width = 55,
                Height = 55,
                Margin = 0
            };
            BarcodeWriter writer = new CustomBarcodeWriter();
            writer.Format = BarcodeFormat.QR_CODE;
            writer.Options = options;

            var bitmap = writer.Write(code);

            return bitmap;
        }

        private void button6_Click(object sender, EventArgs e) {
            List<PartsPickingDetail2Extend> partsPickingDetails = new List<PartsPickingDetail2Extend>();

            for(int i = 0; i < 26; i++) {
                var detail = new PartsPickingDetail2Extend();
                detail.Id = i + 1;
                detail.WarehouseAreaCode = "SH101" + i;
                detail.SparePartCode = "EB252252252" + i;
                detail.SparePartId = i + 1;
                detail.DefaultSupplier = "供应商图号" + i;
                detail.PlannedAmount = 100;
                detail.SparePartName = "备件名称" + i;
                detail.MeasureUnit = "计量单位";
                detail.Remark = "RAW协议是大多数打印设备的默认";
                partsPickingDetails.Add(detail);
            }
            var dataGrid = InitDataGridItem(partsPickingDetails);
            dataGrid.Location = new Point(10, 20);
            PrintOtherPage(dataGrid,1);
            return;
            //开始绘制
            var ghp = CreateGraphics();
            ghp.Clear(BackColor);
            var defaultPen = dataGrid.DefaultPen;
            var titleBrush = dataGrid.DefaultBrush;
            //绘制边框线
            ghp.DrawLine(defaultPen, dataGrid.Location.X, dataGrid.Location.Y, dataGrid.Location.X, dataGrid.Size.Height + dataGrid.Location.Y);
            ghp.DrawLine(defaultPen, dataGrid.Location.X, dataGrid.Size.Height + dataGrid.Location.Y, dataGrid.Location.X + dataGrid.Size.Width, dataGrid.Size.Height + dataGrid.Location.Y);
            ghp.DrawLine(defaultPen, dataGrid.Location.X, dataGrid.Location.Y, dataGrid.Location.X + dataGrid.Size.Width, dataGrid.Location.Y);
            ghp.DrawLine(defaultPen, dataGrid.Location.X + dataGrid.Size.Width, dataGrid.Location.Y, dataGrid.Location.X + dataGrid.Size.Width, dataGrid.Size.Height + dataGrid.Location.Y);
            //绘制页脚
            ghp.DrawString(string.Format("第{0}页 共{1}页", 1, Math.Ceiling(partsPickingDetails.Count() * 1.0 / 10)), new Font("宋体", 13, FontStyle.Regular), titleBrush, (int)(dataGrid.Size.Width * 1.0 / 2 - 100), dataGrid.Location.Y + dataGrid.RowItems.Sum(r => r.Height) + 5);

            foreach(var rowHeaderColumn in dataGrid.RowHeader.Columns) {
                var width = dataGrid.RowHeader.Columns.Where(r => r.DisplayIndex <= rowHeaderColumn.DisplayIndex).Sum(r => r.Width);

                ghp.DrawLine(defaultPen, dataGrid.Location.X + width, dataGrid.Location.Y, dataGrid.Location.X + width, dataGrid.Location.Y + dataGrid.Size.Height);
            }
            foreach(var row in dataGrid.RowItems) {
                var height = dataGrid.RowItems.Where(r => r.Serial < row.Serial).Sum(r => r.Height);
                var width = dataGrid.RowHeader.Columns.Sum(r => r.Width);

                foreach(var columnItem in row.Columns) {
                    var pointX = dataGrid.RowHeader.Columns.Where(r => r.DisplayIndex < columnItem.DisplayIndex).Sum(r => r.Width) + dataGrid.PaddingLeft + dataGrid.Location.X;
                    var sumRowHeight = dataGrid.RowItems.Where(r => r.Serial < row.Serial).Sum(r => r.Height) + dataGrid.Location.Y;

                    if(dataGrid.Location.Y < dataGrid.RowHeight)
                        sumRowHeight = sumRowHeight - dataGrid.RowHeight;

                    if(columnItem.DisplayIndex == 1) {
                        EncodingOptions options = new QrCodeEncodingOptions {
                            DisableECI = false,
                            CharacterSet = "UTF-8",
                            Width = 30,
                            Height = 45,
                            Margin = 0,
                            PureBarcode = false
                        };
                        BarcodeWriter writer = new CustomBarcodeWriter(Color.Transparent, Color.Black);
                        writer.Format = BarcodeFormat.CODE_128;
                        writer.Options = options;

                        var bitmap = writer.Write(columnItem.Text);


                        ghp.DrawImage(bitmap, new Point {
                            X = pointX,
                            Y = sumRowHeight + 5
                        });
                    } else if(columnItem.DisplayIndex == 3) {
                        ghp.DrawString(string.Format("{0}", columnItem.Text), dataGrid.RowFont, titleBrush, pointX, sumRowHeight + (int)Math.Floor(dataGrid.RowHeight * 1.0 / 3));
                    } else if(columnItem.DisplayIndex == 7) {
                        var spareNameRows = GetStringRows(ghp, dataGrid.RowFont, columnItem.Text, dataGrid.RowHeader.Columns[columnItem.DisplayIndex].Width);

                        if(spareNameRows != null && spareNameRows.Count == 1)
                            ghp.DrawString(string.Format("{0}", columnItem.Text), dataGrid.RowFont, titleBrush, pointX, sumRowHeight + (int)Math.Floor(dataGrid.RowHeight * 1.0 / 3));
                        else if(spareNameRows != null && spareNameRows.Count > 1) {
                            for(int i = spareNameRows.Count - 1; i >= 0; i--) {
                                var remarkHeight = sumRowHeight - (18 + 20 * (spareNameRows.Count - 1 - i)) + dataGrid.RowHeight;
                                ghp.DrawString(string.Format("{0}", spareNameRows[i]), dataGrid.RowFont, titleBrush, pointX, remarkHeight);
                            }
                        }
                    } else {
                        ghp.DrawString(string.Format("{0}", columnItem.Text), dataGrid.RowFont, titleBrush, pointX, sumRowHeight + (int)Math.Floor(dataGrid.RowHeight * 1.0 / 3));//290 + 60 * pointX - 37);
                    }
                }
                ghp.DrawLine(defaultPen, dataGrid.Location.X, dataGrid.Location.Y + height, dataGrid.Location.X + width, dataGrid.Location.Y + height);
            }
        }

        private void button7_Click(object sender, EventArgs e) {
            var partsPicking = new PartsPicking1Extend();

            List<PartsPickingDetail2Extend> partsPickingDetails = new List<PartsPickingDetail2Extend>();

            for(int i = 0; i < 10; i++) {
                var detail = new PartsPickingDetail2Extend();
                detail.Id = i + 1;
                detail.WarehouseAreaCode = "SH101SH101" + i;
                detail.SparePartCode = "BZBAIC20170508005" + i;
                detail.SparePartId = i + 1;
                detail.DefaultSupplier = "供应商图号" + i;
                detail.PlannedAmount = 100;
                detail.SparePartName = "RAW协议是大多数打印设备的默认" + i;
                detail.MeasureUnit = "箱";
                detail.Remark = "RAW协议是大多数打印设备的默认";
                partsPickingDetails.Add(detail);
            }
            var dataGrid = InitDataGridItem(partsPickingDetails);
            partsPicking.PartsPickingDetails = new List<PartsPickingDetail2Extend>();
            partsPicking.PartsPickingDetails.AddRange(partsPickingDetails);
            PrintFirstPage(partsPicking, dataGrid, 10);
            return;
            //开始绘制
            var ghp = CreateGraphics();
            ghp.Clear(BackColor);
            var defaultPen = dataGrid.DefaultPen;
            var titleBrush = dataGrid.DefaultBrush;

            /*********************************A4第一部分************************************/
            var marginTop = 10;
            var marginBottom = 10;
            var tableHeight = 150;
            var pointY = marginTop + marginBottom + tableHeight;
            //绘制边框线
            ghp.DrawLine(defaultPen, dataGrid.Location.X, marginTop, dataGrid.Location.X, tableHeight + marginTop);
            ghp.DrawLine(defaultPen, dataGrid.Location.X, tableHeight + marginTop, dataGrid.Location.X + dataGrid.Size.Width, tableHeight + marginTop);
            ghp.DrawLine(defaultPen, dataGrid.Location.X, marginTop, dataGrid.Location.X + dataGrid.Size.Width, marginTop);
            ghp.DrawLine(defaultPen, dataGrid.Location.X + dataGrid.Size.Width, marginTop, dataGrid.Location.X + dataGrid.Size.Width, tableHeight + marginTop);

            //绘制横线
            //ghp.DrawLine(defaultPen, dataGrid.Location.X, marginTop + (int)(tableHeight * 1.0 / 4), dataGrid.Location.X + dataGrid.RowHeader.Columns.Sum(r=>r.Width), marginTop + (int)(tableHeight * 1.0 / 4));
            ghp.DrawLine(defaultPen, dataGrid.Location.X, marginTop + (int)(tableHeight * 1.0 / 4) * 2, dataGrid.Location.X + dataGrid.RowHeader.Columns.Sum(r => r.Width), marginTop + (int)(tableHeight * 1.0 / 4) * 2);
            //ghp.DrawLine(defaultPen, dataGrid.Location.X, marginTop + (int)(tableHeight * 1.0 / 4) * 3, dataGrid.Location.X + dataGrid.RowHeader.Columns.Sum(r => r.Width), marginTop + (int)(tableHeight * 1.0 / 4) * 3);

            //定义第一部分，竖线的坐标变量，便于后面的维护工作
            var firstLine = 90;
            var secondLine = 160;
            var thirdLine = 280;
            var fourLine = 380;
            var fiveLine = 450;
            var sixLine = 570;
            var sevenLine = 690;
            var eightLine = 790;
            var nineLine = 880;

            ghp.DrawLine(defaultPen, firstLine, marginTop + (int)(tableHeight * 1.0 / 4) * 3, thirdLine, marginTop + (int)(tableHeight * 1.0 / 4) * 3);
            ghp.DrawLine(defaultPen, fourLine, marginTop + (int)(tableHeight * 1.0 / 4) * 3, dataGrid.Location.X + dataGrid.RowHeader.Columns.Sum(r => r.Width), marginTop + (int)(tableHeight * 1.0 / 4) * 3);

            //绘制竖线
            ghp.DrawLine(defaultPen, firstLine, marginTop + (int)(tableHeight * 1.0 / 4) * 2, firstLine, tableHeight + marginTop);
            ghp.DrawLine(defaultPen, secondLine, marginTop + (int)(tableHeight * 1.0 / 4) * 2, secondLine, tableHeight + marginTop);
            ghp.DrawLine(defaultPen, thirdLine, marginTop + (int)(tableHeight * 1.0 / 4) * 2, thirdLine, tableHeight + marginTop);
            ghp.DrawLine(defaultPen, fourLine, marginTop + (int)(tableHeight * 1.0 / 4) * 2, fourLine, tableHeight + marginTop);
            ghp.DrawLine(defaultPen, fiveLine, marginTop + (int)(tableHeight * 1.0 / 4) * 2, fiveLine, tableHeight + marginTop);
            ghp.DrawLine(defaultPen, sixLine, marginTop + (int)(tableHeight * 1.0 / 4) * 2, sixLine, tableHeight + marginTop);
            ghp.DrawLine(defaultPen, sevenLine, marginTop + (int)(tableHeight * 1.0 / 4) * 2, sevenLine, tableHeight + marginTop);
            ghp.DrawLine(defaultPen, eightLine, marginTop + (int)(tableHeight * 1.0 / 4) * 2, eightLine, tableHeight + marginTop);
            ghp.DrawLine(defaultPen, nineLine, marginTop + (int)(tableHeight * 1.0 / 4) * 2, nineLine, tableHeight + marginTop);

            ghp.DrawString("北汽国际备件拣货单", new Font("宋体", 20, FontStyle.Regular), titleBrush, (int)(dataGrid.Size.Width * 1.0 / 2 - 100), marginTop + (int)(tableHeight * 1.0 / 6));
            ghp.DrawString("拣货单号", dataGrid.RowFont, dataGrid.DefaultBrush, dataGrid.Location.X + 5, marginTop + (int)(tableHeight * 1.0 / 2 + tableHeight * 1.0 / 4 - 10));
            ghp.DrawString("一维码", dataGrid.RowFont, dataGrid.DefaultBrush, firstLine + 5, marginTop + (int)(tableHeight * 1.0 / 4 * 2 + 10));
            ghp.DrawString("单号", dataGrid.RowFont, dataGrid.DefaultBrush, 95, marginTop + (int)(tableHeight * 1.0 / 4 * 3 + 10));

            ghp.DrawString("包装批次号", dataGrid.RowFont, dataGrid.DefaultBrush, 285, marginTop + (int)(tableHeight * 1.0 / 2 + tableHeight * 1.0 / 4 - 10));
            ghp.DrawString("一维码", dataGrid.RowFont, dataGrid.DefaultBrush, 385, marginTop + (int)(tableHeight * 1.0 / 4 * 2 + 10));
            ghp.DrawString("单号", dataGrid.RowFont, dataGrid.DefaultBrush, 385, marginTop + (int)(tableHeight * 1.0 / 4 * 3 + 10));

            ghp.DrawString("对方单位编号", dataGrid.RowFont, dataGrid.DefaultBrush, 575, marginTop + (int)(tableHeight * 1.0 / 4 * 2 + 10));
            ghp.DrawString("对方单位名称", dataGrid.RowFont, dataGrid.DefaultBrush, 575, marginTop + (int)(tableHeight * 1.0 / 4 * 3 + 10));

            ghp.DrawString("仓库编号", dataGrid.RowFont, dataGrid.DefaultBrush, 795, marginTop + (int)(tableHeight * 1.0 / 4 * 2 + 10));
            ghp.DrawString("仓库名称", dataGrid.RowFont, dataGrid.DefaultBrush, 795, marginTop + (int)(tableHeight * 1.0 / 4 * 3 + 10));

            EncodingOptions optionsForPicking = new QrCodeEncodingOptions {
                DisableECI = false,
                CharacterSet = "UTF-8",
                Width = 120,
                Height = 30,
                Margin = 0,
                PureBarcode = true
            };
            BarcodeWriter writerForPicking = new CustomBarcodeWriter(Color.Transparent, Color.Black);
            writerForPicking.Format = BarcodeFormat.CODE_128;
            writerForPicking.Options = optionsForPicking;

            var bitmapForPicking = writerForPicking.Write("BZBAIC20170508005");


            ghp.DrawImage(bitmapForPicking, new Point {
                X = 165,
                Y = marginTop + (int)(tableHeight * 1.0 / 4) * 2 + 5
            });
            ghp.DrawString("BZBAIC20170508005", dataGrid.RowFont, dataGrid.DefaultBrush, 165, marginTop + (int)(tableHeight * 1.0 / 4 * 3 + 10));

            ghp.DrawImage(bitmapForPicking, new Point {
                X = fiveLine + 5,
                Y = marginTop + (int)(tableHeight * 1.0 / 4) * 2 + 5
            });
            ghp.DrawString("BZBAIC20170508005", dataGrid.RowFont, dataGrid.DefaultBrush, fiveLine + 5, marginTop + (int)(tableHeight * 1.0 / 4 * 3 + 10));

            ghp.DrawString("EB343663636", dataGrid.RowFont, dataGrid.DefaultBrush, sevenLine + 5, marginTop + (int)(tableHeight * 1.0 / 4 * 2 + 10));
            ghp.DrawString("BZBAIC20170508005", dataGrid.RowFont, dataGrid.DefaultBrush, sevenLine + 5, marginTop + (int)(tableHeight * 1.0 / 4 * 3 + 10));

            ghp.DrawString("EB343663636", dataGrid.RowFont, dataGrid.DefaultBrush, nineLine + 5, marginTop + (int)(tableHeight * 1.0 / 4 * 2 + 10));
            ghp.DrawString("BZBAIC20170508005", dataGrid.RowFont, dataGrid.DefaultBrush, nineLine + 5, marginTop + (int)(tableHeight * 1.0 / 4 * 3 + 10));
            /*********************************A4第一部分************************************/




            //绘制边框线
            ghp.DrawLine(defaultPen, dataGrid.Location.X, dataGrid.Location.Y, dataGrid.Location.X, dataGrid.Size.Height + dataGrid.Location.Y);
            ghp.DrawLine(defaultPen, dataGrid.Location.X, dataGrid.Size.Height + dataGrid.Location.Y, dataGrid.Location.X + dataGrid.Size.Width, dataGrid.Size.Height + dataGrid.Location.Y);
            ghp.DrawLine(defaultPen, dataGrid.Location.X, dataGrid.Location.Y, dataGrid.Location.X + dataGrid.Size.Width, dataGrid.Location.Y);
            ghp.DrawLine(defaultPen, dataGrid.Location.X + dataGrid.Size.Width, dataGrid.Location.Y, dataGrid.Location.X + dataGrid.Size.Width, dataGrid.Size.Height + dataGrid.Location.Y);
            //绘制页脚
            ghp.DrawString(string.Format("第{0}页 共{1}页", 1, Math.Ceiling(partsPickingDetails.Count * 1.0 / 10)), new Font("宋体", 13, FontStyle.Regular), titleBrush, (int)(dataGrid.Size.Width * 1.0 / 2 - 100), dataGrid.Location.Y + dataGrid.RowItems.Sum(r => r.Height) + 5);

            foreach(var rowHeaderColumn in dataGrid.RowHeader.Columns) {
                var width = dataGrid.RowHeader.Columns.Where(r => r.DisplayIndex <= rowHeaderColumn.DisplayIndex).Sum(r => r.Width);

                ghp.DrawLine(defaultPen, dataGrid.Location.X + width, dataGrid.Location.Y, dataGrid.Location.X + width, dataGrid.Location.Y + dataGrid.Size.Height);
            }
            foreach(var row in dataGrid.RowItems) {
                var height = dataGrid.RowItems.Where(r => r.Serial <= row.Serial).Sum(r => r.Height);
                var width = dataGrid.RowHeader.Columns.Sum(r => r.Width);

                foreach(var columnItem in row.Columns) {
                    var pointX = dataGrid.RowHeader.Columns.Where(r => r.DisplayIndex < columnItem.DisplayIndex).Sum(r => r.Width) + dataGrid.PaddingLeft + dataGrid.Location.X;
                    var sumRowHeight = dataGrid.RowItems.Where(r => r.Serial < row.Serial).Sum(r => r.Height) + dataGrid.Location.Y;

                    if(dataGrid.Location.Y < dataGrid.RowHeight)
                        sumRowHeight = sumRowHeight - dataGrid.RowHeight;

                    if(columnItem.DisplayIndex == 2) {
                        EncodingOptions options = new QrCodeEncodingOptions {
                            DisableECI = false,
                            CharacterSet = "UTF-8",
                            Width = 150,
                            Height = 50,
                            Margin = 0,
                            PureBarcode = false
                        };
                        BarcodeWriter writer = new CustomBarcodeWriter(Color.Transparent, Color.Black);
                        writer.Format = BarcodeFormat.CODE_128;
                        writer.Options = options;

                        var bitmap = writer.Write(columnItem.Text);


                        ghp.DrawImage(bitmap, new Point {
                            X = pointX,
                            Y = sumRowHeight + 5
                        });
                    } else if(columnItem.IsWrap) {
                        var spareNameRows = GetStringRows(ghp, dataGrid.RowFont, columnItem.Text, dataGrid.RowHeader.Columns[columnItem.DisplayIndex].Width);

                        if(spareNameRows != null && spareNameRows.Count == 1)
                            ghp.DrawString(string.Format("{0}", columnItem.Text), dataGrid.RowFont, titleBrush, pointX, sumRowHeight + (int)Math.Floor(dataGrid.RowHeight * 1.0 / 3));
                        else if(spareNameRows != null && spareNameRows.Count > 1) {
                            for(int i = spareNameRows.Count - 1; i >= 0; i--) {
                                var remarkHeight = sumRowHeight - (18 + 20 * (spareNameRows.Count - 1 - i)) + dataGrid.RowHeight;
                                ghp.DrawString(string.Format("{0}", spareNameRows[i]), dataGrid.RowFont, titleBrush, pointX, remarkHeight);
                            }
                        }
                    } else {
                        ghp.DrawString(string.Format("{0}", columnItem.Text), dataGrid.RowFont, titleBrush, pointX, sumRowHeight + (int)Math.Floor(dataGrid.RowHeight * 1.0 / 3));//290 + 60 * pointX - 37);
                    }
                }
                ghp.DrawLine(defaultPen, dataGrid.Location.X, dataGrid.Location.Y + height, dataGrid.Location.X + width, dataGrid.Location.Y + height);
            }

        }

        //打印第一页
        private Graphics PrintFirstPage(PartsPicking1Extend partsPicking, DataGridItem dataGrid, int pageSize) {
            var ghp = this.CreateGraphics();
            ghp.Clear(BackColor);
            var defaultPen = dataGrid.DefaultPen;
            var titleBrush = dataGrid.DefaultBrush;

            /*********************************A4第一部分************************************/
            const int MARGIN_TOP = 10;
            //const int MARGIN_BOTTOM = 10;
            const int TABLE_HEIGHT = 150;
            //var pointY = MARGIN_TOP + MARGIN_BOTTOM + TABLE_HEIGHT;
            //绘制边框线
            ghp.DrawLine(defaultPen, dataGrid.Location.X, MARGIN_TOP, dataGrid.Location.X, TABLE_HEIGHT + MARGIN_TOP);
            ghp.DrawLine(defaultPen, dataGrid.Location.X, TABLE_HEIGHT + MARGIN_TOP, dataGrid.Location.X + dataGrid.Size.Width, TABLE_HEIGHT + MARGIN_TOP);
            ghp.DrawLine(defaultPen, dataGrid.Location.X, MARGIN_TOP, dataGrid.Location.X + dataGrid.Size.Width, MARGIN_TOP);
            ghp.DrawLine(defaultPen, dataGrid.Location.X + dataGrid.Size.Width, MARGIN_TOP, dataGrid.Location.X + dataGrid.Size.Width, TABLE_HEIGHT + MARGIN_TOP);

            //绘制横线
            //ghp.DrawLine(defaultPen, dataGrid.Location.X, marginTop + (int)(tableHeight * 1.0 / 4), dataGrid.Location.X + dataGrid.RowHeader.Columns.Sum(r=>r.Width), marginTop + (int)(tableHeight * 1.0 / 4));
            ghp.DrawLine(defaultPen, dataGrid.Location.X, MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 4) * 2, dataGrid.Location.X + dataGrid.RowHeader.Columns.Sum(r => r.Width), MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 4) * 2);
            //ghp.DrawLine(defaultPen, dataGrid.Location.X, marginTop + (int)(tableHeight * 1.0 / 4) * 3, dataGrid.Location.X + dataGrid.RowHeader.Columns.Sum(r => r.Width), marginTop + (int)(tableHeight * 1.0 / 4) * 3);

            //定义第一部分，竖线的坐标变量，便于后面的维护工作
            const int FIRST_LINE = 90;
            const int SECOND_LINE = 160;
            const int THIRD_LINE = 280;
            const int FOUR_LINE = 380;
            const int FIVE_LINE = 450;
            const int SIX_LINE = 570;
            const int SEVEN_LINE = 690;
            const int EIGHT_LINE = 790;
            const int NINE_LINE = 880;

            ghp.DrawLine(defaultPen, FIRST_LINE, MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 4) * 3, THIRD_LINE, MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 4) * 3);
            ghp.DrawLine(defaultPen, FOUR_LINE, MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 4) * 3, dataGrid.Location.X + dataGrid.RowHeader.Columns.Sum(r => r.Width), MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 4) * 3);

            //绘制竖线
            ghp.DrawLine(defaultPen, FIRST_LINE, MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 4) * 2, FIRST_LINE, TABLE_HEIGHT + MARGIN_TOP);
            ghp.DrawLine(defaultPen, SECOND_LINE, MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 4) * 2, SECOND_LINE, TABLE_HEIGHT + MARGIN_TOP);
            ghp.DrawLine(defaultPen, THIRD_LINE, MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 4) * 2, THIRD_LINE, TABLE_HEIGHT + MARGIN_TOP);
            ghp.DrawLine(defaultPen, FOUR_LINE, MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 4) * 2, FOUR_LINE, TABLE_HEIGHT + MARGIN_TOP);
            ghp.DrawLine(defaultPen, FIVE_LINE, MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 4) * 2, FIVE_LINE, TABLE_HEIGHT + MARGIN_TOP);
            ghp.DrawLine(defaultPen, SIX_LINE, MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 4) * 2, SIX_LINE, TABLE_HEIGHT + MARGIN_TOP);
            ghp.DrawLine(defaultPen, SEVEN_LINE, MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 4) * 2, SEVEN_LINE, TABLE_HEIGHT + MARGIN_TOP);
            ghp.DrawLine(defaultPen, EIGHT_LINE, MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 4) * 2, EIGHT_LINE, TABLE_HEIGHT + MARGIN_TOP);
            ghp.DrawLine(defaultPen, NINE_LINE, MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 4) * 2, NINE_LINE, TABLE_HEIGHT + MARGIN_TOP);

            ghp.DrawString("北汽国际备件拣货单", new Font("宋体", 20, FontStyle.Regular), titleBrush, (int)(dataGrid.Size.Width * 1.0 / 2 - 100), MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 6));
            ghp.DrawString("拣货单号", dataGrid.RowFont, dataGrid.DefaultBrush, dataGrid.Location.X + 5, MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 2 + TABLE_HEIGHT * 1.0 / 4 - 10));
            ghp.DrawString("一维码", dataGrid.RowFont, dataGrid.DefaultBrush, FIRST_LINE + 5, MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 4 * 2 + 10));
            ghp.DrawString("单号", dataGrid.RowFont, dataGrid.DefaultBrush, 95, MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 4 * 3 + 10));

            ghp.DrawString("包装批次号", dataGrid.RowFont, dataGrid.DefaultBrush, 285, MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 2 + TABLE_HEIGHT * 1.0 / 4 - 10));
            ghp.DrawString("一维码", dataGrid.RowFont, dataGrid.DefaultBrush, 385, MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 4 * 2 + 10));
            ghp.DrawString("单号", dataGrid.RowFont, dataGrid.DefaultBrush, 385, MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 4 * 3 + 10));

            ghp.DrawString("对方单位编号", dataGrid.RowFont, dataGrid.DefaultBrush, 575, MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 4 * 2 + 10));
            ghp.DrawString("对方单位名称", dataGrid.RowFont, dataGrid.DefaultBrush, 575, MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 4 * 3 + 10));

            ghp.DrawString("仓库编号", dataGrid.RowFont, dataGrid.DefaultBrush, 795, MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 4 * 2 + 10));
            ghp.DrawString("仓库名称", dataGrid.RowFont, dataGrid.DefaultBrush, 795, MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 4 * 3 + 10));

            EncodingOptions optionsForPicking = new QrCodeEncodingOptions {
                DisableECI = false,
                CharacterSet = "UTF-8",
                Width = 120,
                Height = 30,
                Margin = 0,
                PureBarcode = true
            };
            BarcodeWriter writerForPicking = new CustomBarcodeWriter(Color.Transparent, Color.Black);
            writerForPicking.Format = BarcodeFormat.CODE_128;
            writerForPicking.Options = optionsForPicking;

            if(!string.IsNullOrEmpty(partsPicking.Code)) {
                var bitmapForPicking = writerForPicking.Write(partsPicking.Code);
                ghp.DrawImage(bitmapForPicking, 165, MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 4) * 2 + 5);
            }
            if(!string.IsNullOrEmpty(partsPicking.PackBatchNumber)) {
                var bitmapForBatchCode = writerForPicking.Write(partsPicking.PackBatchNumber);
                ghp.DrawImage(bitmapForBatchCode, FIVE_LINE + 5, MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 4) * 2 + 5);
            }

            ghp.DrawString(partsPicking.Code, dataGrid.RowFont, dataGrid.DefaultBrush, 165, MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 4 * 3 + 10));
            ghp.DrawString(partsPicking.PackBatchNumber, dataGrid.RowFont, dataGrid.DefaultBrush, FIVE_LINE + 5, MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 4 * 3 + 10));

            ghp.DrawString(partsPicking.CounterpartCompanyCode, dataGrid.RowFont, dataGrid.DefaultBrush, SEVEN_LINE + 5, MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 4 * 2 + 10));
            ghp.DrawString(partsPicking.CounterpartCompanyName, dataGrid.RowFont, dataGrid.DefaultBrush, SEVEN_LINE + 5, MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 4 * 3 + 10));

            ghp.DrawString(partsPicking.WarehouseCode, dataGrid.RowFont, dataGrid.DefaultBrush, NINE_LINE + 5, MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 4 * 2 + 10));
            ghp.DrawString(partsPicking.WarehouseName, dataGrid.RowFont, dataGrid.DefaultBrush, NINE_LINE + 5, MARGIN_TOP + (int)(TABLE_HEIGHT * 1.0 / 4 * 3 + 10));
            /*********************************A4第一部分************************************/

            //绘制边框线
            ghp.DrawLine(defaultPen, dataGrid.Location.X, dataGrid.Location.Y, dataGrid.Location.X, dataGrid.Size.Height + dataGrid.Location.Y);
            ghp.DrawLine(defaultPen, dataGrid.Location.X, dataGrid.Size.Height + dataGrid.Location.Y, dataGrid.Location.X + dataGrid.Size.Width, dataGrid.Size.Height + dataGrid.Location.Y);
            ghp.DrawLine(defaultPen, dataGrid.Location.X, dataGrid.Location.Y, dataGrid.Location.X + dataGrid.Size.Width, dataGrid.Location.Y);
            ghp.DrawLine(defaultPen, dataGrid.Location.X + dataGrid.Size.Width, dataGrid.Location.Y, dataGrid.Location.X + dataGrid.Size.Width, dataGrid.Size.Height + dataGrid.Location.Y);
            //绘制页脚
            ghp.DrawString(string.Format("第{0}页 共{1}页", 1, Math.Ceiling(dataGrid.RowItems.Count * 1.0 / 10)), new Font("宋体", 13, FontStyle.Regular), titleBrush, (int)(dataGrid.Size.Width * 1.0 / 2 - 100), dataGrid.Location.Y + dataGrid.RowItems.Sum(r => r.Height) + 5);

            foreach(var rowHeaderColumn in dataGrid.RowHeader.Columns) {
                // ReSharper disable once AccessToForEachVariableInClosure
                var width = dataGrid.RowHeader.Columns.Where(r => r.DisplayIndex <= rowHeaderColumn.DisplayIndex).Sum(r => r.Width);

                ghp.DrawLine(defaultPen, dataGrid.Location.X + width, dataGrid.Location.Y, dataGrid.Location.X + width, dataGrid.Location.Y + dataGrid.Size.Height);
            }
            var currentRows = new List<RowItem>();

            currentRows.AddRange(dataGrid.RowItems.Count <= pageSize ? dataGrid.RowItems : dataGrid.RowItems.Take(pageSize));

            foreach(var row in currentRows) {
                // ReSharper disable once AccessToForEachVariableInClosure
                var height = dataGrid.RowItems.Where(r => r.Serial <= row.Serial).Sum(r => r.Height);
                var width = dataGrid.RowHeader.Columns.Sum(r => r.Width);

                foreach(var columnItem in row.Columns) {
                    // ReSharper disable once AccessToForEachVariableInClosure
                    var pointX = dataGrid.RowHeader.Columns.Where(r => r.DisplayIndex < columnItem.DisplayIndex).Sum(r => r.Width) + dataGrid.PaddingLeft + dataGrid.Location.X;
                    // ReSharper disable once AccessToForEachVariableInClosure
                    var sumRowHeight = dataGrid.RowItems.Where(r => r.Serial < row.Serial).Sum(r => r.Height) + dataGrid.Location.Y;

                    if(dataGrid.Location.Y < dataGrid.RowHeight)
                        sumRowHeight = sumRowHeight - dataGrid.RowHeight;

                    if(columnItem.DisplayIndex == 2) {
                        EncodingOptions options = new QrCodeEncodingOptions {
                            DisableECI = false,
                            CharacterSet = "UTF-8",
                            Width = 150,
                            Height = 50,
                            Margin = 0,
                            PureBarcode = false
                        };
                        BarcodeWriter writer = new CustomBarcodeWriter(Color.Transparent, Color.Black);
                        writer.Format = BarcodeFormat.CODE_128;
                        writer.Options = options;

                        var bitmap = writer.Write(columnItem.Text);


                        ghp.DrawImage(bitmap, pointX, sumRowHeight + 5);
                    } else if(columnItem.IsWrap) {
                        var spareNameRows = GetStringRows(ghp, dataGrid.RowFont, columnItem.Text, dataGrid.RowHeader.Columns[columnItem.DisplayIndex].Width);

                        if(spareNameRows != null && spareNameRows.Count == 1)
                            ghp.DrawString(string.Format("{0}", columnItem.Text), dataGrid.RowFont, titleBrush, pointX, sumRowHeight + (int)Math.Floor(dataGrid.RowHeight * 1.0 / 3));
                        else if(spareNameRows != null && spareNameRows.Count > 1) {
                            for(int i = spareNameRows.Count - 1; i >= 0; i--) {
                                var remarkHeight = sumRowHeight - (18 + 20 * (spareNameRows.Count - 1 - i)) + dataGrid.RowHeight;
                                ghp.DrawString(string.Format("{0}", spareNameRows[i]), dataGrid.RowFont, titleBrush, pointX, remarkHeight);
                            }
                        }
                    } else {
                        ghp.DrawString(string.Format("{0}", columnItem.Text), dataGrid.RowFont, titleBrush, pointX, sumRowHeight + (int)Math.Floor(dataGrid.RowHeight * 1.0 / 3));//290 + 60 * pointX - 37);
                    }
                }
                ghp.DrawLine(defaultPen, dataGrid.Location.X, dataGrid.Location.Y + height, dataGrid.Location.X + width, dataGrid.Location.Y + height);
            }

            return ghp;
        }

        //打印除第一页之外的数据 从1开始(1为第二页)
        private Graphics PrintOtherPage(DataGridItem dataGrid, int pageIndex) {
            var ghp = this.CreateGraphics();
            ghp.Clear(BackColor);
            var defaultPen = dataGrid.DefaultPen;
            var titleBrush = dataGrid.DefaultBrush;

            //绘制边框线
            ghp.DrawLine(defaultPen, dataGrid.Location.X, dataGrid.Location.Y, dataGrid.Location.X, dataGrid.Size.Height + dataGrid.Location.Y);
            ghp.DrawLine(defaultPen, dataGrid.Location.X, dataGrid.Size.Height + dataGrid.Location.Y, dataGrid.Location.X + dataGrid.Size.Width, dataGrid.Size.Height + dataGrid.Location.Y);
            ghp.DrawLine(defaultPen, dataGrid.Location.X, dataGrid.Location.Y, dataGrid.Location.X + dataGrid.Size.Width, dataGrid.Location.Y);
            ghp.DrawLine(defaultPen, dataGrid.Location.X + dataGrid.Size.Width, dataGrid.Location.Y, dataGrid.Location.X + dataGrid.Size.Width, dataGrid.Size.Height + dataGrid.Location.Y);
            //绘制页脚
            ghp.DrawString(string.Format("第{0}页 共{1}页", pageIndex + 1, dataGrid.TotalPage), new Font("宋体", 13, FontStyle.Regular), titleBrush, (int)(dataGrid.Size.Width * 1.0 / 2 - 100), dataGrid.Location.Y + dataGrid.RowItems.Sum(r => r.Height) + 5);

            foreach(var rowHeaderColumn in dataGrid.RowHeader.Columns) {
                // ReSharper disable once AccessToForEachVariableInClosure
                var width = dataGrid.RowHeader.Columns.Where(r => r.DisplayIndex <= rowHeaderColumn.DisplayIndex).Sum(r => r.Width);

                ghp.DrawLine(defaultPen, dataGrid.Location.X + width, dataGrid.Location.Y, dataGrid.Location.X + width, dataGrid.Location.Y + dataGrid.Size.Height);
            }
            var currentRows = dataGrid.RowItems.Skip(dataGrid.FirstPageSize + (pageIndex - 1) * dataGrid.PageSize).Take(dataGrid.PageSize).ToList();
            foreach(var row in currentRows) {
                row.CurrentPageSerial = row.Serial - dataGrid.FirstPageSize - (pageIndex - 1) * dataGrid.PageSize;
            }
            foreach(var row in currentRows) {
                // ReSharper disable once AccessToForEachVariableInClosure
                var height = currentRows.Where(r => r.CurrentPageSerial <= row.CurrentPageSerial).Sum(r => r.Height);
                var width = dataGrid.RowHeader.Columns.Sum(r => r.Width);

                foreach(var columnItem in row.Columns) {
                    // ReSharper disable once AccessToForEachVariableInClosure
                    var pointX = dataGrid.RowHeader.Columns.Where(r => r.DisplayIndex < columnItem.DisplayIndex).Sum(r => r.Width) + dataGrid.PaddingLeft + dataGrid.Location.X;
                    // ReSharper disable once AccessToForEachVariableInClosure
                    var sumRowHeight = currentRows.Where(r => r.CurrentPageSerial <= row.CurrentPageSerial).Sum(r => r.Height) + dataGrid.Location.Y;

                    if(dataGrid.Location.Y < dataGrid.RowHeight)
                        sumRowHeight = sumRowHeight - dataGrid.RowHeight;

                    if(columnItem.DisplayIndex == 2) {
                        EncodingOptions options = new QrCodeEncodingOptions {
                            DisableECI = false,
                            CharacterSet = "UTF-8",
                            Width = 150,
                            Height = 50,
                            Margin = 0,
                            PureBarcode = false
                        };
                        BarcodeWriter writer = new CustomBarcodeWriter(Color.Transparent, Color.Black);
                        writer.Format = BarcodeFormat.CODE_128;
                        writer.Options = options;

                        var bitmap = writer.Write(columnItem.Text);


                        ghp.DrawImage(bitmap, pointX, sumRowHeight + 5);
                    } else if(columnItem.IsWrap) {
                        var spareNameRows = GetStringRows(ghp, dataGrid.RowFont, columnItem.Text, dataGrid.RowHeader.Columns[columnItem.DisplayIndex].Width);

                        if(spareNameRows != null && spareNameRows.Count == 1)
                            ghp.DrawString(string.Format("{0}", columnItem.Text), dataGrid.RowFont, titleBrush, pointX, sumRowHeight + (int)Math.Floor(dataGrid.RowHeight * 1.0 / 3));
                        else if(spareNameRows != null && spareNameRows.Count > 1) {
                            for(int i = spareNameRows.Count - 1; i >= 0; i--) {
                                var remarkHeight = sumRowHeight - (18 + 20 * (spareNameRows.Count - 1 - i)) + dataGrid.RowHeight;
                                ghp.DrawString(string.Format("{0}", spareNameRows[i]), dataGrid.RowFont, titleBrush, pointX, remarkHeight);
                            }
                        }
                    } else {
                        ghp.DrawString(string.Format("{0}", columnItem.Text), dataGrid.RowFont, titleBrush, pointX, sumRowHeight + (int)Math.Floor(dataGrid.RowHeight * 1.0 / 3));//290 + 60 * pointX - 37);
                    }
                }
                ghp.DrawLine(defaultPen, dataGrid.Location.X, dataGrid.Location.Y + height, dataGrid.Location.X + width, dataGrid.Location.Y + height);
            }

            return ghp;
        }

        /// <summary>
        /// 将数据进行处理
        /// </summary>
        /// <param name="partsPickingDetails">需要打印的清单数据</param>
        /// <returns>处理后的DataGridItem</returns>
        private DataGridItem InitDataGridItem(IEnumerable<PartsPickingDetail2Extend> partsPickingDetails) {
            var dataGrid = new DataGridItem();
            dataGrid.RowHeight = 60;
            dataGrid.Location = new Point {
                X = 10,
                Y = 200
            };

            dataGrid.FirstPageSize = 10;
            dataGrid.PageSize = 15;
            dataGrid.PaddingLeft = 5;
            var rowHead = new RowHeader();
            rowHead.HeadHeight = 25;

            rowHead.Columns.Add(new HeadColumnItem {
                Width = 50,
                Name = "序号",
                Title = "序号",
                DisplayIndex = 0
            });
            rowHead.Columns.Add(new HeadColumnItem {
                Width = 120,
                Name = "货位号",
                Title = "货位号",
                DisplayIndex = 1
            });
            rowHead.Columns.Add(new HeadColumnItem {
                Width = 190,
                Name = "备件编号",
                Title = "备件编号",
                DisplayIndex = 2
            });
            rowHead.Columns.Add(new HeadColumnItem {
                Width = 150,
                Name = "备件名称",
                Title = "备件名称",
                DisplayIndex = 3
            });
            rowHead.Columns.Add(new HeadColumnItem {
                Width = 100,
                Name = "供应商图号",
                Title = "供应商图号",
                DisplayIndex = 4
            });
            rowHead.Columns.Add(new HeadColumnItem {
                Width = 60,
                Name = "计划量",
                Title = "计划量",
                DisplayIndex = 5
            });
            rowHead.Columns.Add(new HeadColumnItem {
                Width = 60,
                Name = "单位",
                Title = "单位",
                DisplayIndex = 6
            });
            rowHead.Columns.Add(new HeadColumnItem {
                Width = 300,
                Name = "备注",
                Title = "备注",
                DisplayIndex = 7
            });

            dataGrid.RowHeader = rowHead;

            var rowItems = new List<RowItem>();
            var serial = 1;
            foreach(var partsInboundPlanDetailExtend in partsPickingDetails) {
                var rowItem = new RowItem();
                var column = new ColumnItem();
                rowItem.Serial = serial;
                rowItem.Height = dataGrid.RowHeight;

                column.Name = "序号";
                column.DisplayIndex = 0;
                column.Text = serial.ToString(CultureInfo.InvariantCulture);
                rowItem.Columns.Add(column);

                column = new ColumnItem();
                column.Name = "货位号";
                column.DisplayIndex = 1;
                column.Text = partsInboundPlanDetailExtend.WarehouseAreaCode;
                rowItem.Columns.Add(column);

                column = new ColumnItem();
                column.Name = "备件编号";
                column.DisplayIndex = 2;
                column.Text = partsInboundPlanDetailExtend.SparePartCode;
                rowItem.Columns.Add(column);

                column = new ColumnItem();
                column.Name = "备件名称";
                column.DisplayIndex = 3;
                column.IsWrap = true;
                column.Text = partsInboundPlanDetailExtend.SparePartName;
                rowItem.Columns.Add(column);

                column = new ColumnItem();
                column.Name = "供应商图号";
                column.DisplayIndex = 4;
                column.Text = partsInboundPlanDetailExtend.DefaultSupplier;
                rowItem.Columns.Add(column);

                column = new ColumnItem();
                column.Name = "计划量";
                column.DisplayIndex = 5;
                column.Text = partsInboundPlanDetailExtend.PlannedAmount.ToString(CultureInfo.InvariantCulture);
                rowItem.Columns.Add(column);

                column = new ColumnItem();
                column.Name = "单位";
                column.DisplayIndex = 6;
                column.Text = partsInboundPlanDetailExtend.MeasureUnit;
                rowItem.Columns.Add(column);

                column = new ColumnItem();
                column.Name = "备注";
                column.DisplayIndex = 7;
                column.IsWrap = true;
                column.Text = partsInboundPlanDetailExtend.Remark;
                rowItem.Columns.Add(column);

                rowItems.Add(rowItem);

                serial++;
            }
            dataGrid.RowItems.AddRange(rowItems);
            dataGrid.Size = new Size(rowHead.Columns.Sum(r => r.Width), dataGrid.RowItems.Sum(r => r.Height));
            dataGrid.TotalCount = rowItems.Count;
            if(dataGrid.TotalCount > dataGrid.FirstPageSize)
                dataGrid.TotalPage = (int)Math.Ceiling((dataGrid.TotalCount - dataGrid.FirstPageSize) * 1.0 / dataGrid.PageSize) + 1;
            else
                dataGrid.TotalPage = 1;
            return dataGrid;
        }
    }
}
