﻿namespace Sunlight.Ce.Utils {
   public static class RegexConstants {
       /// <summary>
       /// 整数或者小数验证
       /// </summary>
       public const string NUMERIC_REGEX=@"^[-]?[0-9]+(\.[0-9]+)?";
       /// <summary>
       /// 整数验证
       /// </summary>
       public const string NUMBER_REGEX = @"^\d+$";
    }
}
