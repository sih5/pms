﻿using System.Text.RegularExpressions;

namespace Sunlight.Ce.Utils {
    public static class RegexUtils {
        /// <summary>
        /// 验证是否是整数或者小数
        /// </summary>
        /// <param name="content">验证文本</param>
        /// <returns>返回验证结果</returns>
        public static bool IsNumerric(string content) {
            return new Regex(RegexConstants.NUMERIC_REGEX).IsMatch(content);
        }

        /// <summary>
        /// 验证是否是整数
        /// </summary>
        /// <param name="content">验证文本</param>
        /// <returns>返回验证结果</returns>
        public static bool IsNumber(string content) {
            return new Regex(RegexConstants.NUMBER_REGEX).IsMatch(content);
        }

        /// <summary>
        /// 判断是否是中文
        /// </summary>
        /// <param name="content">判断字符</param>
        /// <returns>判断结果</returns>
        public static bool IsChinese(char content) {
            return Regex.IsMatch(content.ToString(), @"[\u4e00-\u9fbb]+{1}");
        }
    }
}
