﻿using System.Collections.Generic;
using System.Drawing;

namespace GDI绘制报表.Reports {
    public class DataGridItem {
        private Font headFont = new Font("宋体", 13, FontStyle.Bold);
        private Font contentFont = new Font("宋体", 13, FontStyle.Regular);
        private SolidBrush defaultBrush = new SolidBrush(Color.Black);
        private Pen defaultPen = new Pen(Color.Black);
        private List<RowItem> rowItems = new List<RowItem>();
        /// <summary>
        /// 行标题
        /// </summary>
        public RowHeader RowHeader {
            get;
            set;
        }
        /// <summary>
        /// 行
        /// </summary>
        public List<RowItem> RowItems {
            get {
                return rowItems;
            }
        }
        /// <summary>
        /// 行高
        /// </summary>
        public int RowHeight {
            get;
            set;
        }
        /// <summary>
        /// 页面大小设置
        /// </summary>
        public Size Size {
            get;
            set;
        }
        /// <summary>
        /// DataGrid左上角的坐标位置
        /// </summary>
        public Point Location {
            get;
            set;
        }
        /// <summary>
        /// 文字显示左Padding
        /// </summary>
        public int PaddingLeft {
            get;
            set;
        }
        /// <summary>
        /// Head 字体
        /// </summary>
        public Font HeadFont {
            get {
                return headFont;
            }
            set {
                headFont = value;
            }
        }
        /// <summary>
        /// Row 字体
        /// </summary>
        public Font RowFont {
            get {
                return contentFont;
            }
            set {
                contentFont = value;
            }
        }
        /// <summary>
        /// 字体画刷
        /// </summary>
        public SolidBrush DefaultBrush {
            get {
                return defaultBrush;
            }
            set {
                defaultBrush = value;
            }
        }
        /// <summary>
        /// 默认画笔
        /// </summary>
        public Pen DefaultPen {
            get {
                return defaultPen;
            }
            set {
                defaultPen = value;
            }
        }
        /// <summary>
        /// 数据总数
        /// </summary>
        public int TotalCount {
            get;
            set;
        }
        /// <summary>
        /// 数据总页数
        /// </summary>
        public int TotalPage {
            get;
            set;
        }
        /// <summary>
        /// 除首页外page size
        /// </summary>
        public int PageSize {
            get;
            set;
        }
        /// <summary>
        /// 首页size
        /// </summary>
        public int FirstPageSize {
            get;
            set;
        }
    }
}
//new Font("宋体", 13, FontStyle.Regular)