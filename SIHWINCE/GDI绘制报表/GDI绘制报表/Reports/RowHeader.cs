﻿using System.Collections.Generic;

namespace GDI绘制报表.Reports {
    public class RowHeader {
        private List<HeadColumnItem> columns = new List<HeadColumnItem>();
        /// <summary>
        /// 标题高
        /// </summary>
        public int HeadHeight {
            get;
            set;
        }
        /// <summary>
        /// 显示列
        /// </summary>
        public List<HeadColumnItem> Columns {
            get {
                return columns;
            }
        }
    }
}
