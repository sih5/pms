﻿namespace Sunlight.Ce.Entities.Extends {
    public class ForReceivingOrderExtend {
        /// <summary>
        ///     订单编号
        /// </summary>
        public string Code
        {
            get;
            set;
        }

        /// <summary>
        ///     id
        /// </summary>
        public int Id
        {
            get;
            set;
        }

        /// <summary>
        ///     供应商编号
        /// </summary>
        public string PartsSupplierCode
        {
            get;
            set;
        }

        /// <summary>
        ///     供应商Id
        /// </summary>
        public int PartsSupplierId
        {
            get;
            set;
        }

        /// <summary>
        ///     供应商名称
        /// </summary>
        public string PartsSupplierName
        {
            get;
            set;
        }

        /// <summary>
        ///     状态
        /// </summary>
        public int Status
        {
            get;
            set;
        }

        /// <summary>
        ///     待收数量
        /// </summary>
        public int? Amount
        {
            get;
            set;
        }

        /// <summary>
        ///     品种
        /// </summary>
        public int Variety
        {
            get;
            set;
        }

        /// <summary>
        ///     下游单据编号
        /// </summary>
        public string DownstreamCode
        {
            get;
            set;
        }

        /// <summary>
        ///     备件名称
        /// </summary>
        public string PartsName
        {
            get;
            set;
        }

        /// <summary>
        ///     供应商图号
        /// </summary>
        public string SupplierPartCode
        {
            get;
            set;
        }

        /// <summary>
        ///     待收数量
        /// </summary>
        public int? CollectedAmount
        {
            get;
            set;
        }

        /// <summary>
        ///     实收数量
        /// </summary>
        public int ReceiveAmount
        {
            get;
            set;
        }

        /// <summary>
        ///     单据类型
        /// </summary>
        public int OrderType
        {
            get;
            set;
        }
    }
}
