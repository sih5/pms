﻿namespace Sunlight.Ce.Entities.Extends {
    public class PartsInboundCheckBillExtend {
        /// <summary>
        ///     采购订单类型
        /// </summary>
        public string PartsPurchaseOrderType
        {
            get;
            set;
        }

        /// <summary>
        ///     入库计划Id
        /// </summary>
        public int PartsInboundPlanId
        {
            get;
            set;
        }

        /// <summary>
        ///     配件名称
        /// </summary>
        public string SparePartName
        {
            get;
            set;
        }

        /// <summary>
        ///     配件Id
        /// </summary>
        public int SparePartId
        {
            get;
            set;
        }

        /// <summary>
        ///     配件编号
        /// </summary>
        public string SparePartCode
        {
            get;
            set;
        }

        /// <summary>
        ///     实收数量
        /// </summary>
        public int ReceivedQuantity
        {
            get;
            set;
        }
    }
}
