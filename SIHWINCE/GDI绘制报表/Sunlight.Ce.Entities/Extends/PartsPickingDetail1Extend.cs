﻿namespace Sunlight.Ce.Entities.Extends {
    public class PartsPickingDetail1Extend {
        /// <summary>
        ///     拣货单清单Id
        /// </summary>
        public int PartsPickingDetailId
        {
            get;
            set;
        }

        /// <summary>
        ///     拣货单Id
        /// </summary>
        public int PartsPickingId
        {
            get;
            set;
        }

        /// <summary>
        ///     仓库id
        /// </summary>
        public int WarehouseId
        {
            get;
            set;
        }

        /// <summary>
        ///     库区库位Id
        /// </summary>
        public int? WarehouseAreaId
        {
            get;
            set;
        }

        /// <summary>
        ///     库区库位编号
        /// </summary>
        public string WarehouseAreaCode
        {
            get;
            set;
        }

        /// <summary>
        ///     备件Id
        /// </summary>
        public int SparePartId
        {
            get;
            set;
        }

        /// <summary>
        ///     备件编号
        /// </summary>
        public string SparePartCode
        {
            get;
            set;
        }

        /// <summary>
        ///     备件名称
        /// </summary>
        public string SparePartName
        {
            get;
            set;
        }

        /// <summary>
        ///     待拣货量
        /// </summary>
        public int UnPackingQuantity
        {
            get;
            set;
        }

        /// <summary>
        ///     库存数量
        /// </summary>
        public int Quantity
        {
            get;
            set;
        }

        /// <summary>
        ///     客户端冗余 是否已经拣货
        /// </summary>
        public bool IsPartsPacked
        {
            get;
            set;
        }

        /// <summary>
        ///     客户端冗余 是否已确认
        /// </summary>
        public bool IsConfirmed
        {
            get;
            set;
        }

        /// <summary>
        ///     客户端冗余 是否正在拣货中
        /// </summary>
        public bool IsPartsPacking
        {
            get;
            set;
        }
    }
}
