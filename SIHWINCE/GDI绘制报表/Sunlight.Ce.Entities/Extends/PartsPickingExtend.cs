﻿namespace Sunlight.Ce.Entities.Extends {
    public class PartsPickingExtend {
        /// <summary>
        ///     拣货单id
        /// </summary>
        public int Id
        {
            get;
            set;
        }

        /// <summary>
        ///     拣货单号
        /// </summary>
        public string Code
        {
            get;
            set;
        }

        /// <summary>
        ///     包装批次号
        /// </summary>
        public string PackBatchNumber
        {
            get;
            set;
        }

        /// <summary>
        ///     优先级
        /// </summary>
        public int Priority
        {
            get;
            set;
        }

        /// <summary>
        ///     品种数
        /// </summary>
        public int Variety
        {
            get;
            set;
        }
    }
}
