﻿namespace Sunlight.Ce.Entities.Extends {
    public class PartsShiftOrderExtend2 {
        /// <summary>
        ///     配件Id
        /// </summary>
        public int SparePartId
        {
            get;
            set;
        }

        /// <summary>
        ///     配件编号
        /// </summary>
        public string SparePartCode
        {
            get;
            set;
        }

        /// <summary>
        ///     配件名称
        /// </summary>
        public string SparePartName
        {
            get;
            set;
        }

        /// <summary>
        ///     数量量
        /// </summary>
        public string Quantity
        {
            get;
            set;
        }
    }
}
