﻿using System.Collections.Generic;

namespace Sunlight.Ce.Entities.Extends {
    public class PartsStockExtend {
        /// <summary>
        ///     备件Id
        /// </summary>
        public int PartId
        {
            get;
            set;
        }

        /// <summary>
        ///     备件数量
        /// </summary>
        public int Quantity
        {
            get;
            set;
        }

        /// <summary>
        ///     库区库位用途
        /// </summary>
        public int? WarehouseAreaCategoryId
        {
            get;
            set;
        }

        /// <summary>
        ///     库区库位Id
        /// </summary>
        public int WarehouseAreaId
        {
            get;
            set;
        }

        /// <summary>
        ///     仓库Id
        /// </summary>
        public int WarehouseId
        {
            get;
            set;
        }

        /// <summary>
        ///     备件Id
        /// </summary>
        public int SparePartId
        {
            get;
            set;
        }

        /// <summary>
        ///     备件编号
        /// </summary>
        public string SparePartCode
        {
            get;
            set;
        }

        /// <summary>
        ///     备件名称
        /// </summary>
        public string SparePartName
        {
            get;
            set;
        }

        /// <summary>
        ///     库区库位编号
        /// </summary>
        public string WarehouseAreaCode
        {
            get;
            set;
        }

        /// <summary>
        ///     库区库位用途
        /// </summary>
        public int WarehouseAreaCategory
        {
            get;
            set;
        }

        /// <summary>
        ///     当前登录仓库编号
        /// </summary>
        public string WarehouseCode
        {
            get;
            set;
        }
    }

    public class PartsStockGroupExtend {
        /// <summary>
        ///     仓库Id
        /// </summary>
        public int WarehouseId
        {
            get;
            set;
        }

        /// <summary>
        ///     备件Id
        /// </summary>
        public int SparePartId
        {
            get;
            set;
        }

        /// <summary>
        ///     备件编号
        /// </summary>
        public string SparePartCode
        {
            get;
            set;
        }

        /// <summary>
        ///     备件名称
        /// </summary>
        public string SparePartName
        {
            get;
            set;
        }

        public List<WarehouseAreasExtend> WarehosueAreas
        {
            get;
            set;
        }
    }

    public class WarehouseAreasExtend {
        /// <summary>
        ///     备件数量
        /// </summary>
        public int Quantity
        {
            get;
            set;
        }

        /// <summary>
        ///     库区库位用途
        /// </summary>
        public int? WarehouseAreaCategoryId
        {
            get;
            set;
        }

        /// <summary>
        ///     库区库位Id
        /// </summary>
        public int WarehouseAreaId
        {
            get;
            set;
        }

        /// <summary>
        ///     备件Id
        /// </summary>
        public int SparePartId
        {
            get;
            set;
        }

        /// <summary>
        ///     库区库位编号
        /// </summary>
        public string WarehouseAreaCode
        {
            get;
            set;
        }

        /// <summary>
        ///     库区库位用途
        /// </summary>
        public int WarehouseAreaCategory
        {
            get;
            set;
        }
    }
}
