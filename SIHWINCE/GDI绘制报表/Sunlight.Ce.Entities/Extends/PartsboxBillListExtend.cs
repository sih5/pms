﻿namespace Sunlight.Ce.Entities.Extends {
    public class PartsboxBillListExtend {
        /// <summary>
        ///     装箱数量
        /// </summary>
        public int Quantity
        {
            get;
            set;
        }

        /// <summary>
        ///     备件编号
        /// </summary>
        public string SparePartCode
        {
            get;
            set;
        }

        /// <summary>
        ///     备件Id
        /// </summary>
        public int SparePartId
        {
            get;
            set;
        }

        /// <summary>
        ///     备件名称
        /// </summary>
        public string SparePartName
        {
            get;
            set;
        }

        /// <summary>
        ///     客户端虚拟 标记已经处理完成的备件
        /// </summary>
        public bool IsComplete
        {
            get;
            set;
        }
    }
}
