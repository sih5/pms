﻿using System.Collections.Generic;

namespace Sunlight.Ce.Entities.Extends {
    public class WarehouseAreaWithPartsStockExtend {
        /// <summary>
        ///     源库位
        /// </summary>
        public List<PartsShelvesExtend> OriginalWarehouseAreaWithPartsStocks
        {
            get;
            set;
        }

        /// <summary>
        ///     目标库位
        /// </summary>
        public List<PartsShelvesExtend> TargetWarehouseAreaWithPartsStocks
        {
            get;
            set;
        }
    }
}
