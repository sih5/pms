﻿namespace Sunlight.Ce.Entities {
    public class KeyValueItem {
        /// <summary>
        ///     字典项Id
        /// </summary>
        public int Id
        {
            get;
            set;
        }

        /// <summary>
        ///     字典项名称
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        ///     字典项的key
        /// </summary>
        public int Key
        {
            get;
            set;
        }

        /// <summary>
        ///     字典项的Value
        /// </summary>
        public string Value
        {
            get;
            set;
        }
    }
}
