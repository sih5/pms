﻿namespace Sunlight.Ce.Entities.Parameters {
    public class PartsShiftOrder1Parameter {
        /// <summary>
        ///     配件Id
        /// </summary>
        public int SparePartId
        {
            get;
            set;
        }

        /// <summary>
        ///     配件名称
        /// </summary>
        public string SparePartName
        {
            get;
            set;
        }

        /// <summary>
        ///     配件编号
        /// </summary>
        public string SparePartCode
        {
            get;
            set;
        }

        /// <summary>
        ///     仓库Id
        /// </summary>
        public int WarehouseId
        {
            get;
            set;
        }

        /// <summary>
        ///     仓库编号
        /// </summary>
        public string WarehouseCode
        {
            get;
            set;
        }

        /// <summary>
        ///     仓库名称
        /// </summary>
        public string WarehouseName
        {
            get;
            set;
        }

        /// <summary>
        ///     库位id
        /// </summary>
        public int WarehouseAreaId
        {
            get;
            set;
        }

        /// <summary>
        ///     库位编号
        /// </summary>
        public string WarehouseAreaCode
        {
            get;
            set;
        }

        /// <summary>
        ///     目标库位编号
        /// </summary>
        public string ToWarehouseAreaCode
        {
            get;
            set;
        }

        /// <summary>
        ///     数量
        /// </summary>
        public int Quantity
        {
            get;
            set;
        }

        /// <summary>
        ///     库区用途
        /// </summary>
        public int WarehouseAreaCategoryId
        {
            get;
            set;
        }

        /// <summary>
        ///     库区用途
        /// </summary>
        public int WarehouseAreaCategory
        {
            get;
            set;
        }
    }
}
