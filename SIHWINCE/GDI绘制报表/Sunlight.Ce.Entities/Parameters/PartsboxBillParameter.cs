﻿/**
 * PartsBoxBillController 中所有Post请求，参数处理类
 */

namespace Sunlight.Ce.Entities.Parameters {
    public class PartsboxBillParameter {
        /// <summary>
        ///     箱单编号
        /// </summary>
        public string Code
        {
            get;
            set;
        }

        /// <summary>
        ///     备件Id
        /// </summary>
        public int SparePartId
        {
            get;
            set;
        }

        /// <summary>
        ///     装箱量
        /// </summary>
        public int Quantity
        {
            get;
            set;
        }

        /// <summary>
        ///     拣货单Id
        /// </summary>
        public int PartsPickingId
        {
            get;
            set;
        }
    }

    public class PartsboxBill1Parameter {
        /// <summary>
        ///     箱高
        /// </summary>
        public double? BoxHigh
        {
            get;
            set;
        }

        /// <summary>
        ///     箱长
        /// </summary>
        public double? BoxLong
        {
            get;
            set;
        }

        /// <summary>
        ///     箱型
        /// </summary>
        public int? BoxType
        {
            get;
            set;
        }

        /// <summary>
        ///     箱宽
        /// </summary>
        public double? BoxWide
        {
            get;
            set;
        }

        /// <summary>
        ///     箱号
        /// </summary>
        public string Code
        {
            get;
            set;
        }

        /// <summary>
        ///     拣货单编号
        /// </summary>
        public string PartsPickingCode
        {
            get;
            set;
        }

        /// <summary>
        ///     拣货单Id
        /// </summary>
        public int? PartsPickingId
        {
            get;
            set;
        }

        /// <summary>
        ///     材质
        /// </summary>
        public int? Texture
        {
            get;
            set;
        }

        /// <summary>
        ///     箱宽
        /// </summary>
        public double? Weight
        {
            get;
            set;
        }
    }

    public class PartsboxBill2Parameter {
        /// <summary>
        ///     箱单Id
        /// </summary>
        public int PartsboxBillId
        {
            get;
            set;
        }

        /// <summary>
        ///     备件Id
        /// </summary>
        public int SparePartId
        {
            get;
            set;
        }

        /// <summary>
        ///     移除量
        /// </summary>
        public int Quantity
        {
            get;
            set;
        }

        /// <summary>
        ///     拣货单Id
        /// </summary>
        public int PartsPickingId
        {
            get;
            set;
        }
    }
}
