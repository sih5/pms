﻿using System;

namespace Sunlight.Ce.Entities {
    public class PartsInventoryBill {
        public int? AbandonerId
        {
            get;
            set;
        }

        public string AbandonerName
        {
            get;
            set;
        }

        public DateTime? AbandonTime
        {
            get;
            set;
        }

        public int? AccountingStatus
        {
            get;
            set;
        }

        public decimal? AmountDifference
        {
            get;
            set;
        }

        public int? ApproverId
        {
            get;
            set;
        }

        public string ApproverName
        {
            get;
            set;
        }

        public DateTime? ApproveTime
        {
            get;
            set;
        }

        public int BranchId
        {
            get;
            set;
        }

        public string Code
        {
            get;
            set;
        }

        public DateTime? CreateTime
        {
            get;
            set;
        }

        public int? CreatorId
        {
            get;
            set;
        }

        public string CreatorName
        {
            get;
            set;
        }

        public int Id
        {
            get;
            set;
        }

        public string InitiatorName
        {
            get;
            set;
        }

        public string InventoryReason
        {
            get;
            set;
        }

        public int? InventoryRecordOperatorId
        {
            get;
            set;
        }

        public string InventoryRecordOperatorName
        {
            get;
            set;
        }

        public DateTime? InventoryRecordTime
        {
            get;
            set;
        }

        public int? ModifierId
        {
            get;
            set;
        }

        public string ModifierName
        {
            get;
            set;
        }

        public DateTime? ModifyTime
        {
            get;
            set;
        }

        public int? PartsInventoryBillType
        {
            get;
            set;
        }

        public string RejectComment
        {
            get;
            set;
        }

        public string Remark
        {
            get;
            set;
        }

        public int? ResultInputOperatorId
        {
            get;
            set;
        }

        public string ResultInputOperatorName
        {
            get;
            set;
        }

        public DateTime? ResultInputTime
        {
            get;
            set;
        }

        public int Status
        {
            get;
            set;
        }

        public string StorageCompanyCode
        {
            get;
            set;
        }

        public int StorageCompanyId
        {
            get;
            set;
        }

        public string StorageCompanyName
        {
            get;
            set;
        }

        public int StorageCompanyType
        {
            get;
            set;
        }

        public int WarehouseAreaCategory
        {
            get;
            set;
        }

        public string WarehouseCode
        {
            get;
            set;
        }

        public int WarehouseId
        {
            get;
            set;
        }

        public string WarehouseName
        {
            get;
            set;
        }

        /// <summary>
        ///     冗余字段 备件品种数(用户汇总清单中备件的种类)
        /// </summary>
        public int SpareTypeCount
        {
            get;
            set;
        }
    }
}
