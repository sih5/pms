﻿namespace Sunlight.Ce.Entities {
    public class PartsPicking {
        public string Code
        {
            get;
            set;
        }

        public string CounterpartCompanyCode
        {
            get;
            set;
        }

        public int CounterpartCompanyId
        {
            get;
            set;
        }

        public string CounterpartCompanyName
        {
            get;
            set;
        }

        public string CreatorName
        {
            get;
            set;
        }

        public int Id
        {
            get;
            set;
        }

        public string Mark
        {
            get;
            set;
        }

        public string ModifierName
        {
            get;
            set;
        }

        public string PackBatchNumber
        {
            get;
            set;
        }

        public string PartsPackingPlanCode
        {
            get;
            set;
        }

        public int PartsPackingPlanId
        {
            get;
            set;
        }

        public int PickingStatus
        {
            get;
            set;
        }

        public int Priority
        {
            get;
            set;
        }

        public string Remark
        {
            get;
            set;
        }

        public string StorageCompanyCode
        {
            get;
            set;
        }

        public int StorageCompanyId
        {
            get;
            set;
        }

        public string StorageCompanyName
        {
            get;
            set;
        }

        public int StorageCompanyType
        {
            get;
            set;
        }

        public int UrgencyLevel
        {
            get;
            set;
        }

        public string WarehouseCode
        {
            get;
            set;
        }

        public int WarehouseId
        {
            get;
            set;
        }

        public string WarehouseName
        {
            get;
            set;
        }

        public string WarehouseReservoirAreaCode
        {
            get;
            set;
        }

        public int? WarehouseReservoirAreaId
        {
            get;
            set;
        }

        public int Variety
        {
            get;
            set;
        }

        public int? WarehouseAreaId
        {
            get;
            set;
        }

        public string WarehouseAreaCode
        {
            get;
            set;
        }

        public int SparePartId
        {
            get;
            set;
        }

        public string SparePartCode
        {
            get;
            set;
        }

        public string SparePartName
        {
            get;
            set;
        }

        public int? ToPickQuantity
        {
            get;
            set;
        }

        public int? Quantity
        {
            get;
            set;
        }

        public int PartsPickingDetailId
        {
            get;
            set;
        }

        /// <summary>
        ///     客户端冗余 是否已经拣货
        /// </summary>
        public bool IsPartsPacked
        {
            get;
            set;
        }

        /// <summary>
        ///     客户端冗余 是否已确认
        /// </summary>
        public bool IsConfirmed
        {
            get;
            set;
        }

        /// <summary>
        ///     扩展字段 汇总清单数据
        /// </summary>
        public int PlannedAmount
        {
            get;
            set;
        }

        /// <summary>
        ///     扩展字段 汇总清单数据
        /// </summary>
        public int? PackingAmount
        {
            get;
            set;
        }

        /// <summary>
        ///     拣货单Id
        /// </summary>
        public int PartsPickingId
        {
            get;
            set;
        }

        /// <summary>
        ///     客户端冗余 是否正在拣货中
        /// </summary>
        public bool IsPartsPacking
        {
            get;
            set;
        }
    }
}
