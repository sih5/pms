﻿using System;

namespace Sunlight.Ce.Entities {
    public class PartsStock {
        public int BranchId
        {
            get;
            set;
        }

        public DateTime? CreateTime
        {
            get;
            set;
        }

        public int? CreatorId
        {
            get;
            set;
        }

        public string CreatorName
        {
            get;
            set;
        }

        public int Id
        {
            get;
            set;
        }

        public int? ModifierId
        {
            get;
            set;
        }

        public string ModifierName
        {
            get;
            set;
        }

        public DateTime? ModifyTime
        {
            get;
            set;
        }

        public int PartId
        {
            get;
            set;
        }

        public int Quantity
        {
            get;
            set;
        }

        public string Remark
        {
            get;
            set;
        }

        public int StorageCompanyId
        {
            get;
            set;
        }

        public int StorageCompanyType
        {
            get;
            set;
        }

        public int? WarehouseAreaCategoryId
        {
            get;
            set;
        }

        public int WarehouseAreaId
        {
            get;
            set;
        }

        public int WarehouseId
        {
            get;
            set;
        }
    }
}
