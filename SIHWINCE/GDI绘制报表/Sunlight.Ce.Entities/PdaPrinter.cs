﻿namespace Sunlight.Ce.Entities {
    public class PdaPrinter {
        public string GeneralPrinterPort
        {
            get;
            set;
        }

        public string GeneralPrinterIp
        {
            get;
            set;
        }

        public int Id
        {
            get;
            set;
        }

        public string LabelPrinterIp
        {
            get;
            set;
        }

        public string LabelPrinterPort
        {
            get;
            set;
        }

        public int WarehouseId
        {
            get;
            set;
        }
    }
}
