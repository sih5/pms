﻿using System;

namespace Sunlight.Ce.Entities {
    /// <summary>
    ///     配件信息
    /// </summary>
    public class SparePart {
        public int? AbandonerId
        {
            get;
            set;
        }

        public string AbandonerName
        {
            get;
            set;
        }

        public DateTime? AbandonTime
        {
            get;
            set;
        }

        public string CadCode
        {
            get;
            set;
        }

        public string CadName
        {
            get;
            set;
        }

        public string Color
        {
            get;
            set;
        }

        public string ContractElement
        {
            get;
            set;
        }

        public DateTime? CreateTime
        {
            get;
            set;
        }

        public int? CreatorId
        {
            get;
            set;
        }

        public string CreatorName
        {
            get;
            set;
        }

        public string DefaultSupplier
        {
            get;
            set;
        }

        public string ElectricParam
        {
            get;
            set;
        }

        public int? ElectrophoresisMark
        {
            get;
            set;
        }

        public int? EmarkMark
        {
            get;
            set;
        }

        public string EnglishName
        {
            get;
            set;
        }

        public string Feature
        {
            get;
            set;
        }

        public string Groups
        {
            get;
            set;
        }

        public string HarmonizationCode
        {
            get;
            set;
        }

        public string HscodeBigClassCode
        {
            get;
            set;
        }

        public string HscodeBigClassName
        {
            get;
            set;
        }

        public string HscodeSmallClassCode
        {
            get;
            set;
        }

        public string HscodeSmallClassName
        {
            get;
            set;
        }

        /// <summary>
        ///     配件Id
        /// </summary>
        public int Id
        {
            get;
            set;
        }

        /// <summary>
        ///     配件名称
        /// </summary>
        public bool? IsOverseasDedicated
        {
            get;
            set;
        }

        public string LastSubstitute
        {
            get;
            set;
        }

        public string Material
        {
            get;
            set;
        }

        public string MeasureUnit
        {
            get;
            set;
        }

        public int? MinCount
        {
            get;
            set;
        }

        public string MinPackingSize
        {
            get;
            set;
        }

        public double? MinPackingVolume
        {
            get;
            set;
        }

        public int? ModifierId
        {
            get;
            set;
        }

        public string ModifierName
        {
            get;
            set;
        }

        public DateTime? ModifyTime
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        /// <summary>
        ///     裸件长
        /// </summary>
        public double? Length
        {
            get;
            set;
        }

        /// <summary>
        ///     裸件宽
        /// </summary>
        public double? Width
        {
            get;
            set;
        }

        /// <summary>
        ///     裸件高
        /// </summary>
        public double? Height
        {
            get;
            set;
        }

        /// <summary>
        ///     裸件重量
        /// </summary>
        public double? Weight
        {
            get;
            set;
        }

        /// <summary>
        ///     毛件长
        /// </summary>
        public double? GrossLength
        {
            get;
            set;
        }

        /// <summary>
        ///     毛件宽
        /// </summary>
        public double? GrossWidth
        {
            get;
            set;
        }

        /// <summary>
        ///     毛件高
        /// </summary>
        public double? GrossHeight
        {
            get;
            set;
        }

        /// <summary>
        ///     毛件重量
        /// </summary>
        public double? MinPackingWeight
        {
            get;
            set;
        }

        /// <summary>
        ///     包装工艺
        /// </summary>
        public string PackageType
        {
            get;
            set;
        }

        /// <summary>
        ///     最小包装数
        /// </summary>
        public int? MinPackingAmount
        {
            get;
            set;
        }

        /// <summary>
        ///     配件库存.数量
        /// </summary>
        public int PartsStockQuantity
        {
            get;
            set;
        }

        /// <summary>
        ///     配件编号
        /// </summary>
        public string Code
        {
            get;
            set;
        }

        public string NextSubstitute
        {
            get;
            set;
        }

        public int? PackingAmount
        {
            get;
            set;
        }

        public string PackingSpecification
        {
            get;
            set;
        }

        public string PartsInPackingCode
        {
            get;
            set;
        }

        public string PartsOutPackingCode
        {
            get;
            set;
        }

        public int? PartType
        {
            get;
            set;
        }

        public string PinyinCode
        {
            get;
            set;
        }

        public int? PriceType
        {
            get;
            set;
        }

        public string ReferenceCode
        {
            get;
            set;
        }

        public string ReferenceName
        {
            get;
            set;
        }

        public string Remark
        {
            get;
            set;
        }

        public int? ShelfLife
        {
            get;
            set;
        }

        public string Sizes
        {
            get;
            set;
        }

        public string SpareBigType
        {
            get;
            set;
        }

        public string SpareMiniType
        {
            get;
            set;
        }

        public string Specification
        {
            get;
            set;
        }

        public int Status
        {
            get;
            set;
        }

        public string StopBac
        {
            get;
            set;
        }

        public int? StorageType
        {
            get;
            set;
        }

        public string SubBrand
        {
            get;
            set;
        }

        public string VehicleApplication
        {
            get;
            set;
        }

        public double? Volume
        {
            get;
            set;
        }

        public int? Quality
        {
            get;
            set;
        }
    }
}
