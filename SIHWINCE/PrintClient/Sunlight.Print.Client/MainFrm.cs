﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Newtonsoft.Json;
using Sunlight.Print.Model;

namespace Sunlight.Print.Client {
    public partial class MainFrm : Form {
        public MainFrm() {
            this.InitializeComponent();
        }

        private Socket socketSend;

        private void btnSend_Click(object sender, EventArgs e) {
            var messageInfo = new MessageInfo();
            try {
                messageInfo.PrintOrderType = 1;
                var data = new List<PartsInboundPlanExtend>();
                //构造数据
                var partsInboundPlan = new PartsInboundPlanExtend {
                    Id = 1,
                    PartsInboundPlanCode = "PIPBAIC20170626000001",
                    PartsInboundPlanId = 1905,
                    SparePartId = 2508,
                    SparePartCode = "128404181",
                    DefaultSupplier = "",
                    PlannedAmount = 10,
                    InspectedQuantity = 0,
                    ReceivedQuantity = 10,
                    Remark = ""
                };
                data.Add(partsInboundPlan);
                messageInfo.PrintContent = JsonConvert.SerializeObject(data);

                var content = JsonConvert.SerializeObject(messageInfo);
                //按照指定的IP，端口，进行数据发送
                byte[] buffer = Encoding.UTF8.GetBytes(content);

                ShowMsgLocal(string.Format("{0}--{1}:\r\n{2}", "LocalHost", DateTime.Now.ToString(CultureInfo.InvariantCulture), content));
                //清除文本框中的内容
                //txtMsg.Text = string.Empty;
            }
            catch (Exception ex) {
                ShowMsgLocal(ex.Message);
            }

        }

        private void ShowMsgLocal(string strMsg) {
            txtMsg.Text += (strMsg + "\r\n");
        }

        private void ShowMsgSend(string strMsg) {
            this.txtMsg.Text += (strMsg + "\r\n");
        }

        #region Receive
        private void Receive() {
            try {
                while (true) {
                    byte[] buffer = new byte[1024 * 1024 * 5];
                    int r = this.socketSend.Receive(buffer);
                    if (r == 0) {
                        break;
                    }
                    string strMsg = Encoding.UTF8.GetString(buffer, 0, r);
                    this.ShowMsgSend(string.Format("{0}--{1}:\r\n{2}", this.socketSend.RemoteEndPoint.ToString(), DateTime.Now.ToString(CultureInfo.InvariantCulture), strMsg));
                }
            }
            catch (Exception) {
            }

        }
        #endregion

        #region btnConnect_Click
        private void btnConnect_Click(object sender, EventArgs e) {
            {
                try {
                    socketSend = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    var point = new IPEndPoint(IPAddress.Parse("192.168.0.192"), 50001);
                    socketSend.Connect(point);
                    ShowMsgSend(string.Format("连接{0}服务器成功！", "192.168.0.192"));

                    var th = new Thread(Receive);
                    th.IsBackground = true;
                    th.Start();

                    //禁用Connect button
                    btnConnect.Enabled = false;
                }
                catch (Exception) {
                    MessageBox.Show("服务器暂时没有开启，请稍后再试！");
                }
            }
        #endregion
        }
    }
}