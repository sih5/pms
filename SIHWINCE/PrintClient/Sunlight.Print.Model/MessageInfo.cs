﻿namespace Sunlight.Print.Model {
    public class MessageInfo {
        public int PrintOrderType { get; set; }

        public string PrintContent { get; set; }
    }
}