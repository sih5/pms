﻿namespace Sunlight.Print.Model {
    /// <summary>
    ///     认证用户的信息存储
    /// </summary>
    public class AuthorizeUser {
        /// <summary>
        ///     用户Id
        /// </summary>
        public int UserId {
            get;
            set;
        }

        /// <summary>
        ///     用户名
        /// </summary>
        public string UserName {
            get;
            set;
        }

        /// <summary>
        ///     用户编号
        /// </summary>
        public string UserCode {
            get;
            set;
        }

        /// <summary>
        ///     企业Id
        /// </summary>
        public int EnterpriseId {
            get;
            set;
        }

        /// <summary>
        ///     企业编号
        /// </summary>
        public string EnterpriseCode {
            get;
            set;
        }

        /// <summary>
        ///     企业名称
        /// </summary>
        public string EnterpriseName {
            get;
            set;
        }

        /// <summary>
        ///     企业类型
        /// </summary>
        public int EnterpriseType {
            get;
            set;
        }
    }
}
