﻿namespace Sunlight.Print.Model {
    /// <summary>
    /// 打印的数据容器
    /// </summary>
    public class MessageRequestPrintInfo {
        /// <summary>
        /// 消息内容类型
        /// </summary>
        public int MonitorType {
            get;
            set;
        }

        /// <summary>
        /// 会话Id
        /// </summary>
        public string UniqueId {
            get;
            set;
        }

        /// <summary>
        /// 打印数据
        /// </summary>
        public string PrintContent {
            get;
            set;
        }

        /// <summary>
        /// 打印机名称
        /// </summary>
        public string PrintName {
            get;
            set;
        }
    }
}