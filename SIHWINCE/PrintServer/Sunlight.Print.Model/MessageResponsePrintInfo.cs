﻿namespace Sunlight.Print.Model
{
    /// <summary>
    /// 服务器接收打印数据，并且处理成功后，返回处理结果
    /// </summary>
    public class MessageResponsePrintInfo {
        /// <summary>
        /// 是否处理成功
        /// </summary>
        public bool IsSuccess {
            get;
            set;
        }

        /// <summary>
        /// 响应类型
        /// </summary>
        public int ResponseType {
            get;
            set;
        }

        /// <summary>
        /// 处理结果
        /// </summary>
        public string Message {
            get;
            set;
        }
    }
}