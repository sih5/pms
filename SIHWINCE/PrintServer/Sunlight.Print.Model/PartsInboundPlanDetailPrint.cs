﻿namespace Sunlight.Print.Model {
    public class PartsInboundPlanDetailPrint {
        /// <summary>
        ///     序号
        /// </summary>
        public int Serial {
            get;
            set;
        }

        /// <summary>
        ///     备件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        ///     备件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        /// 备件参考图号
        /// </summary>
        public string ReferenceCode {
            get;
            set;
        }

        /// <summary>
        ///     数量
        /// </summary>
        public int? InspectedQuantity {
            get;
            set;
        }

        /// <summary>
        ///     备注
        /// </summary>
        public string Remark {
            get;
            set;
        }

        /// <summary>
        /// 计划数量
        /// </summary>
        public int PlannedAmount {
            get;
            set;
        }

        /// <summary>
        /// 供应商图号
        /// </summary>
        public string DefaultSupplier {
            get;
            set;
        }

        /// <summary>
        /// 单位
        /// </summary>
        public string MeasureUnit {
            get;
            set;
        }
    }
}
