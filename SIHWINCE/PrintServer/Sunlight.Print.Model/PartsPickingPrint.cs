﻿using System.Collections.Generic;

namespace Sunlight.Print.Model {
    public class PartsPickingPrint {
        /// <summary>
        /// 营销分公司编号
        /// </summary>
        public string BranchCode {
            get;
            set;
        }
        /// <summary>
        /// 营销分公司Id
        /// </summary>
        public int BranchId {
            get;
            set;
        }
        /// <summary>
        /// 营销分公司名称
        /// </summary>
        public string BranchName {
            get;
            set;
        }
        /// <summary>
        /// 拣货单编号

        /// </summary>
        public string Code {
            get;
            set;
        }
        /// <summary>
        /// 对方单位编号
        /// </summary>
        public string CounterpartCompanyCode {
            get;
            set;
        }
        /// <summary>
        /// 对方单位Id
        /// </summary>
        public int CounterpartCompanyId {
            get;
            set;
        }
        /// <summary>
        /// 对方单位名称
        /// </summary>
        public string CounterpartCompanyName {
            get;
            set;
        }
        /// <summary>
        /// 主键Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// 唛头
        /// </summary>
        public string Mark {
            get;
            set;
        }
        /// <summary>
        /// 包装批次号
        /// </summary>
        public string PackBatchNumber {
            get;
            set;
        }
        /// <summary>
        /// 包装计划编号
        /// </summary>
        public string PartsPackingPlanCode {
            get;
            set;
        }
        /// <summary>
        /// 包装计划Id
        /// </summary>
        public int PartsPackingPlanId {
            get;
            set;
        }
        /// <summary>
        /// 状态
        /// </summary>
        public int PickingStatus {
            get;
            set;
        }
        /// <summary>
        /// 优先级
        /// </summary>
        public int Priority {
            get;
            set;
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 仓储企业编号
        /// </summary>
        public string StorageCompanyCode {
            get;
            set;
        }
        /// <summary>
        /// 仓储企业Id
        /// </summary>
        public int StorageCompanyId {
            get;
            set;
        }
        /// <summary>
        /// 仓储企业名称
        /// </summary>
        public string StorageCompanyName {
            get;
            set;
        }
        /// <summary>
        /// 仓储企业类型
        /// </summary>
        public int StorageCompanyType {
            get;
            set;
        }
        /// <summary>
        /// 紧急程度
        /// </summary>
        public int UrgencyLevel {
            get;
            set;
        }
        /// <summary>
        /// 配件仓库编号
        /// </summary>
        public string WarehouseCode {
            get;
            set;
        }
        /// <summary>
        /// 配件仓库Id
        /// </summary>
        public int WarehouseId {
            get;
            set;
        }
        /// <summary>
        /// 配件仓库名称
        /// </summary>
        public string WarehouseName {
            get;
            set;
        }
        /// <summary>
        /// 库区编号
        /// </summary>
        public string WarehouseReservoirAreaCode {
            get;
            set;
        }
        /// <summary>
        /// 库区Id
        /// </summary>
        public int? WarehouseReservoirAreaId {
            get;
            set;
        }
        /// <summary>
        /// 库区用途
        /// </summary>
        public string WarehouseAreaCategory {
            get;
            set;
        }
        /// <summary>
        /// 国家 (营销分公司市场部.市场部名称)
        /// </summary>
        public string CountryName {
            get;
            set;
        }
        /// <summary>
        /// 配件拣货单清单
        /// </summary>
        public List<PartsPickingDetailPrint> PartsPickingDetails {
            get;
            set;
        }

    }
}
