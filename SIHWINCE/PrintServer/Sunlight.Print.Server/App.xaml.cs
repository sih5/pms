﻿using System;
using System.IO;
using System.Reflection;
using System.Windows;

namespace Sunlight.Print.Server {
    /// <summary>
    ///     App.xaml 的交互逻辑
    /// </summary>
    public partial class App {
        private void App_OnStartup(object sender, StartupEventArgs e) {
            var name = Assembly.GetExecutingAssembly().Location;
            var path = Path.GetDirectoryName(name);
            AppDomain.CurrentDomain.AppendPrivatePath(path); 
        }
    }
}