﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Sunlight.Print.Server {
    public class CommonRegister {
        public static void RegisterNumber(TextBox[] textBoxs) {
            if(textBoxs == null || !textBoxs.Any())
                return;
            foreach(var textBox in textBoxs) {
                textBox.PreviewKeyDown += textBox_PreviewKeyDown;
                textBox.PreviewTextInput += textBox_PreviewTextInput;
                DataObject.AddPastingHandler(textBox, OnClipboardPaste);
            }
        }

        private static void OnClipboardPaste(object sender, DataObjectPastingEventArgs e) {
            if(e.DataObject.GetDataPresent(typeof(string))) {
                var text = (string)e.DataObject.GetData(typeof(string));

                if(!IsNumberic(text))

                    e.CancelCommand();
            } else {
                e.CancelCommand();
            }
        }

        private static void textBox_PreviewKeyDown(object sender, KeyEventArgs e) {
            if(e.Key == Key.Space)
                e.Handled = true;
        }


        private static void textBox_PreviewTextInput(object sender, TextCompositionEventArgs e) {
            e.Handled = !IsNumberic(e.Text);
        }

        private static bool IsNumberic(string _string) {
            if(string.IsNullOrEmpty(_string))
                return false;

            foreach(var c in _string)
                if(!char.IsDigit(c))
                    //if(c<'0' c="">'9')//最好的方法,在下面测试数据中再加一个0，然后这种方法效率会搞10毫秒左右
                    return false;
            return true;
        }
    }
}