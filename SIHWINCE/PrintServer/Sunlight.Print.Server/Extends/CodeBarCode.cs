﻿using System.Windows.Media;

namespace Sunlight.Print.Server.Extends {
    public class CodeBarCode {
        public ImageSource BarCode {
            get;
            set;
        }
    }
}
