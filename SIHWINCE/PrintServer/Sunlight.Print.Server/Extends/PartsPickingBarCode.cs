﻿using System.Windows.Media;

namespace Sunlight.Print.Server.Extends {
    public class PartsPickingBarCode {
        /// <summary>
        /// 拣货单号
        /// </summary>
        public ImageSource Code {
            get;
            set;
        }

        /// <summary>
        /// 批次号
        /// </summary>
        public ImageSource PartsBatchNumber {
            get;
            set;
        }
    }
}
