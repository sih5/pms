﻿using System.Windows.Documents;

namespace Sunlight.Print.Server {
    public interface IDocumentRenderer {
        void Render(FlowDocument doc, object data);
    }
}