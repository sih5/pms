﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Threading;
using Sunlight.Print.Model;

namespace Sunlight.Print.Server {
    public partial class MainPrintWindow {
        public MainPrintWindow() {
            InitializeComponent();
        }

        private void DoPrint(PrintDialog pdlg, DocumentPaginator paginator) {
            pdlg.PrintDocument(paginator, "Order Document");
        }

        private void EnableButton() {
            btnPrintDirect.IsEnabled = true;
        }

        private void btnPrintPreview_Click(object sender, RoutedEventArgs e) {
            var partsInboundPlanPrint = new PartsInboundPlanPrint();
            //TODO:插入入库计划数据数据
            var previewWnd = new PrintPreviewWindow("PartsInboundPlanDocument.xaml", partsInboundPlanPrint, new PartsInboundPlanDocumentRender());
            previewWnd.Owner = this;
            previewWnd.ShowInTaskbar = false;
            previewWnd.ShowDialog();
        }

        private void btnPrintDlg_Click(object sender, RoutedEventArgs e) {
            var pdlg = new PrintDialog();
            if(pdlg.ShowDialog() == true) {
                var doc = PrintPreviewWindow.LoadDocumentAndRender("PartsInboundPlanDocument.xaml", new PartsPickingPrint(), new PartsInboundPlanDocumentRender());
                Dispatcher.BeginInvoke(new DoPrintMethod(DoPrint), DispatcherPriority.ApplicationIdle, pdlg,
                    ((IDocumentPaginatorSource)doc).DocumentPaginator);
            }
        }

        private void btnPrintDirect_Click(object sender, RoutedEventArgs e) {
            btnPrintDirect.IsEnabled = false;
            var pdlg = new PrintDialog();
            var doc = PrintPreviewWindow.LoadDocumentAndRender("PartsInboundPlanDocument.xaml", new PartsInboundPlanPrint(),
                new PartsInboundPlanDocumentRender());
            Dispatcher.BeginInvoke(new DoPrintMethod(DoPrint), DispatcherPriority.ApplicationIdle, pdlg,
                ((IDocumentPaginatorSource)doc).DocumentPaginator);
        }

        public void TestTimerCallback(object state) {
            Dispatcher.BeginInvoke(new EnableButtonMethod(EnableButton));
        }

        private delegate void DoPrintMethod(PrintDialog pdlg, DocumentPaginator paginator);

        private delegate void EnableButtonMethod();

        private void btnPrintPartsPickingPreview_Click(object sender, RoutedEventArgs e) {
            var partsInboundPlanPrint = new PartsPickingPrint();
            //TODO:插入备件拣货单数据
            var previewWnd = new PrintPreviewWindow("PartsPartsPickingDocument.xaml", partsInboundPlanPrint, new PartsPartsPickingDocumentRender());
            previewWnd.Owner = this;
            previewWnd.ShowInTaskbar = false;
            previewWnd.ShowDialog();
        }
    }
}