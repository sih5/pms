﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Threading;
using Newtonsoft.Json;
using Sunlight.Print.ExtendUtils;
using Sunlight.Print.Model;

namespace Sunlight.Print.Server {
    public partial class MainWindow {
        public MainWindow() {
            InitializeComponent();

            ResizeMode = ResizeMode.CanMinimize;

            ShellRegister.DisableMaxmizeBox(this, true);
            CommonRegister.RegisterNumber(new[] { TxtPort });
            Loaded += MainWindow_Loaded;
            Closed += MainWindows_Closed;
        }

        /// <summary>
        ///     消息处理方法
        /// </summary>
        /// <param name="msg">需要记录的消息</param>
        private void ShowMessage(string msg) {
            //Action action = () => MsgContent.AppendText(msg + "\r\n");
            //if(Thread.CurrentThread != MsgContent.Dispatcher.Thread)
            //    MsgContent.Dispatcher.Invoke(DispatcherPriority.Normal, action);
            //else
            //    action();
        }

        /// <summary>
        /// 监听客户端连接
        /// </summary>
        /// <param name="socket">服务器套接字</param>
        private void ListenClientConnect(object socket) {
            var socketServer = socket as Socket;
            if(socketServer == null)
                return;
            while(true) {
                Socket clientSocket = socketServer.Accept();
                Thread receiveThread = new Thread(ReceiveMessage);
                receiveThread.Start(clientSocket);
            }
        }

        /// <summary>
        /// 收取客户端消息
        /// </summary>
        /// <param name="clientSocket">客户端套接字</param>
        private void ReceiveMessage(object clientSocket) {
            Socket myClientSocket = (Socket)clientSocket;
            //声明字节数组，一次接收数据的长度为 1024 字节  
            byte[] recvBytes = new byte[1024];
            int receivedLength = 0;
            while(true) {
                try {
                    var stringBuilder = new StringBuilder();
                    var bytes = myClientSocket.Receive(recvBytes, 20, 0);
                    //将读取的字节数转换为字符串  
                    string dataLength = Encoding.Default.GetString(recvBytes, 0, bytes);
                    if(string.IsNullOrEmpty(dataLength))
                        return;
                    var fileLength = Convert.ToInt32(dataLength);

                    //1.2接收文件内容
                    while(receivedLength < fileLength) {
                        bytes = myClientSocket.Receive(recvBytes, recvBytes.Length, 0);
                        receivedLength += bytes;
                        stringBuilder.Append(Encoding.Default.GetString(recvBytes, 0, bytes));
                    }

                    var requestMsg = stringBuilder.ToString();
                    //打印数据，进行记录
                    LoggerUtils.Info(requestMsg);
                    var printData = JsonConvert.DeserializeObject<MessageRequestPrintInfo>(requestMsg);
                    if(printData == null)
                        return;
                    switch(printData.MonitorType) {
                        case (int)MonitorType.关闭连接:
                            myClientSocket.Shutdown(SocketShutdown.Both);
                            myClientSocket.Close();
                            break;
                        case (int)MonitorType.获取打印机列表:
                            var printSource = new List<string>();
                            foreach(string sPrint in PrinterSettings.InstalledPrinters)
                                printSource.Add(sPrint);
                            var responseMsg = new MessageResponsePrintInfo {
                                Message = JsonConvert.SerializeObject(printSource),
                                IsSuccess = true,
                                ResponseType = (int)ResponseType.打印机列表
                            };
                            ServerSocketSend(myClientSocket, JsonConvert.SerializeObject(responseMsg));
                            break;
                        case (int)MonitorType.打印配件拣货单:
                        case (int)MonitorType.打印箱号标签:
                        case (int)MonitorType.打印配件入库计划:
                            Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new DoPrintDataFunc(PrintDataFunc), printData, Enum.GetName(typeof(MonitorType), printData.MonitorType));
                            break;
                    }
                } catch(Exception exception) {
                    LoggerUtils.Error(exception.Message);
                    LoggerUtils.Trace(exception.StackTrace);
                    myClientSocket.Shutdown(SocketShutdown.Both);
                    myClientSocket.Close();
                    break;
                }
            }
        }

        /// <summary>
        ///     异步发送消息
        /// </summary>
        /// <param name="client">客户端套接字</param>
        /// <param name="message">发送消息</param>
        private void ServerSocketSend(Socket client, string message) {
            var bdata = Encoding.Default.GetBytes(message);
            //文件长度, 固定为20字节，前面会自动补零
            byte[] fileLengthArray = Encoding.Default.GetBytes(bdata.Length.ToString("D20"));
            //合并byte数组
            byte[] bdata1 = Utils.CombineBinaryArray(fileLengthArray, bdata);
            //发文件长度+文件内容
            client.Send(bdata1, bdata1.Length, 0);
        }

        //开始打印
        private void DoPrint(PrintDialog pdlg, DocumentPaginator paginator, string printDocumentTitle, string targetPrintName) {
            var print = new PrintDocument {
                PrinterSettings = {
                    Copies = 1 //此处默认打印1份
                }
            };

            var sDefault = print.PrinterSettings.PrinterName;

            var allPrintNames = new List<string>();
            foreach(string installedPrinter in PrinterSettings.InstalledPrinters) {
                allPrintNames.Add(installedPrinter);
            }

            if(allPrintNames.Contains(targetPrintName) && !string.IsNullOrEmpty(targetPrintName))
                PrinterUtils.SetDefaultPrinter(targetPrintName); //设置默认打印机

            pdlg.PrintDocument(paginator, printDocumentTitle);
            PrinterUtils.SetDefaultPrinter(sDefault);//还原之前的默认打印机
        }

        //窗体加载后的默认处理
        private void MainWindow_Loaded(object sender, RoutedEventArgs e) {
            this.ComboBoxIpList.ItemsSource = Utils.GetAddressIp();
            try {
                //启动时 删除3天以前的文件记录
                var filePath = AppDomain.CurrentDomain.BaseDirectory;
                filePath += "Logs";
                if(!Directory.Exists(filePath))
                    return;
                var fileNames = Directory.GetDirectories(filePath);
                var nowTime = DateTime.Now;
                foreach(var item in fileNames) {
                    var lastWriteTime = new FileInfo(item).LastWriteTime;
                    var start = Convert.ToDateTime(lastWriteTime.ToShortDateString());
                    var end = Convert.ToDateTime(nowTime.ToShortDateString());
                    var sp = end.Subtract(start);
                    var days = sp.Days;
                    if(days > 3) {
                        var di = new DirectoryInfo(item);
                        di.Delete(true);
                    }
                }
            } catch(Exception ex) {
                LoggerUtils.Error(ex.Message);
            }
        }

        //开始监听
        private void Start_OnClick(object sender, RoutedEventArgs e) {
            if(ComboBoxIpList.SelectedItem == null) {
                MessageBox.Show(this, "请选择要监听的IP地址", "提示信息", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if(string.IsNullOrEmpty(TxtPort.Text)) {
                MessageBox.Show(this, "端口不能为空,请输入有效的端口号", "提示信息", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            try {
                var serverIp = new IPEndPoint(IPAddress.Parse(ComboBoxIpList.SelectedItem.ToString()), Convert.ToInt32(TxtPort.Text));
                var tcpServer = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                tcpServer.Bind(serverIp);
                tcpServer.Listen(100);

                Thread thread = new Thread(ListenClientConnect);
                thread.Start(tcpServer);
                MessageBox.Show(this, "监听成功", "提示信息", MessageBoxButton.OK, MessageBoxImage.Information);
                this.Start.IsEnabled = false;
            } catch(SocketException exception) {
                LoggerUtils.Error(exception.Message);
                LoggerUtils.Trace(exception.StackTrace);
            } catch(Exception exception) {
                LoggerUtils.Error(exception.Message);
                LoggerUtils.Trace(exception.StackTrace);
            }
        }

        private delegate void DoPrintMethod(PrintDialog pdlg, DocumentPaginator paginator, string printDocumentTitle, string targetPrintName);

        private delegate void DoPrintDataFunc(MessageRequestPrintInfo data, string printDocumentTitle);

        private void PrintDataFunc(MessageRequestPrintInfo data, string printDocumentTitle) {
            try {
                var pdlg = new PrintDialog();
                FlowDocument doc = null;
                switch(data.MonitorType) {
                    case (int)MonitorType.打印配件入库计划:
                        doc = PrintPreviewWindow.LoadDocumentAndRender("PartsInboundPlanDocument.xaml", JsonConvert.DeserializeObject<PartsInboundPlanPrint>(data.PrintContent), new PartsInboundPlanDocumentRender());
                        break;
                    case (int)MonitorType.打印配件拣货单:
                        doc = PrintPreviewWindow.LoadDocumentAndRender("PartsPartsPickingDocument.xaml", JsonConvert.DeserializeObject<PartsPickingPrint>(data.PrintContent), new PartsPartsPickingDocumentRender());
                        break;
                    case (int)MonitorType.打印箱号标签:
                        doc = PrintPreviewWindow.LoadDocumentAndRender("PartsboxBillListDocument.xaml", JsonConvert.DeserializeObject<PartsboxBillListPrint>(data.PrintContent), new PartsboxBillListDocumentRender());
                        break;
                }
                if(doc != null) {
                    Dispatcher.BeginInvoke(new DoPrintMethod(DoPrint), DispatcherPriority.ApplicationIdle, pdlg, PrintPreviewWindow.GetPaginator(doc), printDocumentTitle, data.PrintName);
                }
            } catch(Exception exception) {
                LoggerUtils.Fatal(exception.Message);
                LoggerUtils.Trace(exception.StackTrace);
            }
        }

        private void MainWindows_Closed(object sender, EventArgs e) {
            Environment.Exit(0);
        }
    }
}