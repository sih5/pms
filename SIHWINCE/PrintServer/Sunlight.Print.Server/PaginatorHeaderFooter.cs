﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace Sunlight.Print.Server {
    public class PaginatorHeaderFooter : DocumentPaginator {
        readonly DocumentPaginator m_paginator;

        public PaginatorHeaderFooter(DocumentPaginator paginator) {
            m_paginator = paginator;
        }

        public override DocumentPage GetPage(int pageNumber) {
            DocumentPage page = m_paginator.GetPage(pageNumber);
            ContainerVisual newpage = new ContainerVisual();

            //页眉:公司名称
            DrawingVisual header = new DrawingVisual();
            using(DrawingContext ctx = header.RenderOpen()) {
                FormattedText text = new FormattedText("上海ABCD信息技术有限公司",
                    System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight,
                    new Typeface("Courier New"), 14, Brushes.Black);
                ctx.DrawText(text, new Point(page.ContentBox.Left, page.ContentBox.Top));
                ctx.DrawLine(new Pen(Brushes.Black, 0.5), new Point(page.ContentBox.Left, page.ContentBox.Top + 16), new Point(page.ContentBox.Right, page.ContentBox.Top + 16));
            }

            //页脚:第几页
            DrawingVisual footer = new DrawingVisual();
            using(DrawingContext ctx = footer.RenderOpen()) {
                FormattedText text = new FormattedText("第" + (pageNumber + 1) + "页　　总任务　" + PageCount + "-" + (pageNumber + 1),
                    System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight,
                    new Typeface("Courier New"), 14, Brushes.Black);
                ctx.DrawText(text, new Point(page.ContentBox.Right / 2 - 44, page.ContentBox.Bottom));
            }

            //将原页面微略压缩(使用矩阵变换)
            ContainerVisual mainPage = new ContainerVisual();
            mainPage.Children.Add(page.Visual);
            mainPage.Transform = new MatrixTransform(1, 0, 0, 0.95, 0, 0.025 * page.ContentBox.Height);

            //在现页面中加入原页面，页眉和页脚
            newpage.Children.Add(mainPage);
            //newpage.Children.Add(header);
            newpage.Children.Add(footer);

            return new DocumentPage(newpage, page.Size, page.BleedBox, page.ContentBox);
        }

        public override bool IsPageCountValid {
            get {
                return m_paginator.IsPageCountValid;
            }
        }

        public override int PageCount {
            get {
                return m_paginator.PageCount;
            }
        }

        public override Size PageSize {
            get {
                return m_paginator.PageSize;
            }

            set {
                m_paginator.PageSize = value;
            }
        }

        public override IDocumentPaginatorSource Source {
            get {
                return m_paginator.Source;
            }
        }
    }
}
