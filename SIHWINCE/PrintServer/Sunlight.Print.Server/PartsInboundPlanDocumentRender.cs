﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Sunlight.Print.Model;
using Sunlight.Print.Server.Extends;
using ZXing;
using ZXing.Common;
using ZXing.QrCode;

namespace Sunlight.Print.Server {
    public class PartsInboundPlanDocumentRender : IDocumentRenderer {
        public void Render(FlowDocument doc, object data) {
            var group = doc.FindName("RowsDetails") as TableRowGroup;
            var table = doc.FindName("PartsInboundPlanTable") as Table;
            var tableGroup = doc.FindName("PartsInboundPlanGroupTable") as Table;
            if(group == null || table == null || tableGroup == null)
                return;

            var styleCell = doc.Resources["BorderedCell"] as Style;
            var partsInboundPlan = data as PartsInboundPlanPrint;
            if(partsInboundPlan == null)
                return;
            if(partsInboundPlan.CounterpartCompanyName.Length > 5) {
                partsInboundPlan.CounterpartCompanyName = partsInboundPlan.CounterpartCompanyName.Substring(0, 5);
            }
            if(partsInboundPlan.PurchaseOrderType.Contains("采购订单")) {
                partsInboundPlan.PurchaseOrderType = partsInboundPlan.PurchaseOrderType.Substring(0, 2);
            }
            if(partsInboundPlan.PurchaseOrderType.Contains("调拨")) {
                partsInboundPlan.PurchaseOrderType = partsInboundPlan.PurchaseOrderType.Substring(0, 2);
            }
            partsInboundPlan.InboundType = partsInboundPlan.InboundType.Substring(partsInboundPlan.InboundType.Length - 2, 2);

            int codeWidth;
            var barCode = new CodeBarCode();
            barCode.BarCode = CreateQrCode(partsInboundPlan.Code, out codeWidth, true);
            tableGroup.RowGroups[0].Rows[2].Cells[2].DataContext = barCode;
            var sparePartsWidth = new List<int>();
            var defaultSuppliersWidth = new List<int>();
            foreach(var item in partsInboundPlan.PartsInboundPlanDetails) {
                var row = new TableRow();
                var cell = new TableCell(new Paragraph(new Run(item.Serial.ToString())));
                cell.Padding = new Thickness(10, 0, 0, 0);
                cell.Style = styleCell;
                row.Cells.Add(cell);

                cell = new TableCell(new Paragraph(new Run(item.SparePartCode)));
                cell.Style = styleCell;
                row.Cells.Add(cell);

                cell = new TableCell(new Paragraph(new Run(item.SparePartName)));
                cell.Style = styleCell;
                row.Cells.Add(cell);

                table.Columns[1].Width = new GridLength(300);
                table.Columns[0].Width = new GridLength(50);
                if(!string.IsNullOrEmpty(item.DefaultSupplier)) {
                    cell = new TableCell(new Paragraph(new Run(item.DefaultSupplier)));
                    cell.Style = styleCell;
                    row.Cells.Add(cell);
                } else {
                    cell = new TableCell(new Paragraph(new Run(string.Empty)));
                    cell.Style = styleCell;
                    row.Cells.Add(cell);
                }


                cell = new TableCell(new Paragraph(new Run(item.PlannedAmount.ToString())));
                cell.Style = styleCell;
                row.Cells.Add(cell);

                cell = new TableCell(new Paragraph(new Run(item.MeasureUnit)));
                cell.Style = styleCell;
                //cell.Padding = new Thickness(0, 20, 0, 0);
                row.Cells.Add(cell);

                cell = new TableCell(new Paragraph(new Run(item.Remark)));
                cell.Style = styleCell;
                //cell.Padding = new Thickness(0, 20, 0, 0);
                row.Cells.Add(cell);

                var row1 = new TableRow();
                cell = new TableCell(new Paragraph(new Run(string.Empty)));
                cell.Style = styleCell;
                row1.Cells.Add(cell);

                int sparePartWidth;
                var bitmap = CreateQrCode(item.SparePartCode, out sparePartWidth, true);
                sparePartsWidth.Add(sparePartWidth);
                var map = new BlockUIContainer(new Image {
                    Source = bitmap,
                    Margin = new Thickness(0),
                    Stretch = Stretch.None
                });
                map.Padding = new Thickness(0, 0, 0, 0);
                map.Margin = new Thickness(0);
                var fig = new Figure(map);
                fig.Margin = new Thickness(0);
                fig.Padding = new Thickness(0);
                cell = new TableCell(new Paragraph(fig));
                cell.Style = styleCell;
                cell.FontSize = 12;
                cell.Padding = new Thickness(0);
                row1.Cells.Add(cell);

                cell = new TableCell(new Paragraph(new Run(string.Empty)));
                cell.Style = styleCell;
                row1.Cells.Add(cell);


                if(!string.IsNullOrEmpty(item.DefaultSupplier)) {
                    int defaultSupplierWidth;
                    var bitmapForSupplier = CreateQrCode(item.DefaultSupplier, out defaultSupplierWidth, true);
                    defaultSuppliersWidth.Add(defaultSupplierWidth);
                    var mapForSupplier = new BlockUIContainer(new Image {
                        Source = bitmapForSupplier,
                        Margin = new Thickness(0),
                        Stretch = Stretch.None
                    });
                    mapForSupplier.Padding = new Thickness(0, 0, 0, 0);
                    mapForSupplier.Margin = new Thickness(0);
                    var figForSupplier = new Figure(mapForSupplier);
                    figForSupplier.Margin = new Thickness(0);
                    figForSupplier.Padding = new Thickness(0);
                    cell = new TableCell(new Paragraph(figForSupplier));
                    cell.Style = styleCell;
                    cell.FontSize = 14;
                    cell.Padding = new Thickness(0);
                    row1.Cells.Add(cell);
                } else {
                    cell = new TableCell(new Paragraph(new Run(string.Empty)));
                    cell.Style = styleCell;
                    row1.Cells.Add(cell);
                }
                group.Rows.Add(row);
                group.Rows.Add(row1);
            }

            table.Columns[1].Width = !sparePartsWidth.Any() ? new GridLength(100) : new GridLength(sparePartsWidth.Max() <= 100 ? 100 : sparePartsWidth.Max());
            table.Columns[4].Width = !defaultSuppliersWidth.Any() ? new GridLength(100) : new GridLength(defaultSuppliersWidth.Max() <= 100 ? 100 : defaultSuppliersWidth.Max());

        }

        private ImageSource CreateQrCode(string content, out int width) {
            EncodingOptions options = new QrCodeEncodingOptions {
                DisableECI = false,
                CharacterSet = "UTF-8",
                //Height = 60,
                //Width = 150,
                Margin = 0,
                Height = 40,
                PureBarcode = false
            };
            var write = new BarcodeWriter {
                Format = BarcodeFormat.CODE_128,
                Options = options
            };
            var bitmap = write.Write(content);
            width = bitmap.Width;
            var ip = bitmap.GetHbitmap();
            var bitmapSource = Imaging.CreateBitmapSourceFromHBitmap(ip, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
            DeleteObject(ip);
            return bitmapSource;
        }
        private ImageSource CreateQrCode(string content, out int width, bool isPureBarcode) {
            EncodingOptions options = new QrCodeEncodingOptions {
                DisableECI = false,
                CharacterSet = "UTF-8",
                //Height = 60,
                //Width = 150,
                Margin = 0,
                Height = 20,
                PureBarcode = isPureBarcode
            };
            var write = new BarcodeWriter {
                Format = BarcodeFormat.CODE_128,
                Options = options
            };
            var bitmap = write.Write(content);
            width = bitmap.Width;
            var ip = bitmap.GetHbitmap();
            var bitmapSource = Imaging.CreateBitmapSourceFromHBitmap(ip, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
            DeleteObject(ip);
            return bitmapSource;
        }

        [DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);
    }
}