﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using Sunlight.Print.Model;
using Sunlight.Print.Server.Extends;
using ZXing;

namespace Sunlight.Print.Server {
    public class PartsPartsPickingDocumentRender : IDocumentRenderer {
        public void Render(FlowDocument doc, object data) {
            var group = doc.FindName("RowsDetails") as TableRowGroup;
            var table = doc.FindName("PartsPickingDetailTable") as Table;
            var partsPickingTable = doc.FindName("PartsPickingTable") as Table;
            var styleCell = doc.Resources["BorderedCell"] as Style;
            var urgencyLevel = doc.FindName("UrgencyLevel") as Run;
            var partsPickingPrint = data as PartsPickingPrint;
            if(partsPickingPrint == null || styleCell == null || group == null || table == null || partsPickingTable == null || urgencyLevel == null)
                return;
            var partsPickingBarCode = new PartsPickingBarCode();
            if(!string.IsNullOrEmpty(partsPickingPrint.Code))
                partsPickingBarCode.Code = ServerUtils.CreateQrCode(partsPickingPrint.Code, BarcodeFormat.CODE_128, true);
            if(!string.IsNullOrEmpty(partsPickingPrint.PackBatchNumber))
                partsPickingBarCode.PartsBatchNumber = ServerUtils.CreateQrCode(partsPickingPrint.PackBatchNumber, BarcodeFormat.CODE_128, true);
            //partsPickingTable.DataContext = partsPickingBarCode;
            switch(partsPickingPrint.UrgencyLevel) {
                case 1:
                    urgencyLevel.Text = "紧急";
                    break;
                case 2:
                    urgencyLevel.Text = "常规";
                    break;
                default:
                    urgencyLevel.Text = "";
                    break;
            }
            partsPickingTable.RowGroups[0].Rows[1].Cells[0].DataContext = partsPickingBarCode;
            //partsPickingTable.RowGroups[0].Rows[1].Cells[1].DataContext = partsPickingBarCode;

            //记录所有的备件编号一维码宽度
            var sparePartsWidth = new List<int>();
            foreach(var item in partsPickingPrint.PartsPickingDetails) {
                var row = new TableRow();

                int sparePartWidth;
                var imageSource = ServerUtils.CreateQrCode(item.SparePartCode, out sparePartWidth, BarcodeFormat.CODE_128);
                sparePartsWidth.Add(sparePartWidth);

                var cell = new TableCell(new Paragraph(new Run(item.Serial.ToString()))) {
                    Style = styleCell
                };
                row.Cells.Add(cell);

                //cell = new TableCell(new Paragraph(new Run(item.WarehouseAreaCode))) {
                //    Style = styleCell
                //};
                //row.Cells.Add(cell);

                //cell = new TableCell(new Paragraph(ServerUtils.Figure(imageSource))) {
                //    Style = styleCell,
                //    FontSize = 12,
                //    Padding = new Thickness(3, 3, 3, 0)
                //};
                cell = new TableCell(new Paragraph(new Run(item.SparePartCode))) {
                    Style = styleCell
                };
                row.Cells.Add(cell);

                cell = new TableCell(new Paragraph(new Run(item.SparePartName))) {
                    Style = styleCell
                };
                row.Cells.Add(cell);

                //cell = new TableCell(new Paragraph(new Run(item.DefaultSupplier))) {
                //    Style = styleCell
                //};
                //row.Cells.Add(cell);

                cell = new TableCell(new Paragraph(new Run(item.PackingAmount.ToString()))) {
                    Style = styleCell
                };
                row.Cells.Add(cell);

                cell = new TableCell(new Paragraph(new Run(item.MeasureUnit))) {
                    Style = styleCell
                };
                row.Cells.Add(cell);

                cell = new TableCell(new Paragraph(new Run(item.Remark))) {
                    Style = styleCell
                };
                row.Cells.Add(cell);

                group.Rows.Add(row);
            }

            table.Columns[1].Width = !sparePartsWidth.Any() ? new GridLength(100 + 6) : new GridLength(sparePartsWidth.Max() <= 100 ? 106 : sparePartsWidth.Max() + 6);
        }
    }
}