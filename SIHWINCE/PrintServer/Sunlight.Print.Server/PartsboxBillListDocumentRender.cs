﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Sunlight.Print.Model;
using Sunlight.Print.Server.Extends;
using ZXing;
using ZXing.Common;
using ZXing.QrCode;

namespace Sunlight.Print.Server {
    public class PartsboxBillListDocumentRender : IDocumentRenderer {
        public void Render(FlowDocument doc, object data) {
            var tableGroup = doc.FindName("LogoGroupTable") as Table;
            if(tableGroup == null)
                return;

            var partsboxBillListPrint = data as PartsboxBillListPrint;
            if(partsboxBillListPrint == null)
                return;

            if(!string.IsNullOrEmpty(partsboxBillListPrint.Code)) {
                var barCode = new CodeBarCode();
                int codeWidth;
                barCode.BarCode = CreateQrCode(partsboxBillListPrint.Code, out codeWidth, false);
                tableGroup.RowGroups[0].Rows[0].Cells[1].DataContext = barCode;
            }
        }

        private ImageSource CreateQrCode(string content, out int width, bool isPureBarcode) {
            EncodingOptions options = new QrCodeEncodingOptions {
                DisableECI = false,
                CharacterSet = "UTF-8",
                Margin = 0,
                Height = 60,
                PureBarcode = isPureBarcode
            };
            var write = new BarcodeWriter {
                Format = BarcodeFormat.CODE_128,
                Options = options
            };
            var bitmap = write.Write(content);
            width = bitmap.Width;
            var ip = bitmap.GetHbitmap();
            var bitmapSource = Imaging.CreateBitmapSourceFromHBitmap(ip, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
            DeleteObject(ip);
            return bitmapSource;
        }

        [DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);
    }
}