﻿using System;

namespace Sunlight.Print.Server {
    public class PrintEventArgs<T> : EventArgs {
        public T Data {
            get;
            set;
        }
    }
}