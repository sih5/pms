﻿using System;
using System.IO;
using System.IO.Packaging;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Threading;
using System.Windows.Xps.Packaging;

namespace Sunlight.Print.Server {
    public partial class PrintPreviewWindow {
        private readonly FlowDocument mDoc;

        public PrintPreviewWindow(string strTmplName, object data, IDocumentRenderer renderer = null) {
            InitializeComponent();
            mDoc = LoadDocumentAndRender(strTmplName, data, renderer);
            Dispatcher.BeginInvoke(new LoadXpsMethod(LoadXps), DispatcherPriority.ApplicationIdle);
        }

        public static FlowDocument LoadDocumentAndRender(string strTmplName, object data,
            IDocumentRenderer renderer = null) {
            var doc = (FlowDocument)Application.LoadComponent(new Uri(strTmplName, UriKind.RelativeOrAbsolute));
            doc.PagePadding = new Thickness(50);
            doc.DataContext = data;
            if(renderer != null)
                renderer.Render(doc, data);
            return doc;
        }
        public static DocumentPaginator GetPaginator(FlowDocument doc) {
            bool? bPrintHeaderAndFooter = doc.Resources["PrintHeaderAndFooter"] as bool?;
            if(bPrintHeaderAndFooter == true) {
                return new PaginatorHeaderFooter(((IDocumentPaginatorSource)doc).DocumentPaginator);
            } else {
                return ((IDocumentPaginatorSource)doc).DocumentPaginator;
            }
        }

        public void LoadXps() {
            //构造一个基于内存的xps document
            var ms = new MemoryStream();
            var package = Package.Open(ms, FileMode.Create, FileAccess.ReadWrite);
            var documentUri = new Uri("pack://InMemoryDocument.xps");
            PackageStore.RemovePackage(documentUri);
            PackageStore.AddPackage(documentUri, package);
            var xpsDocument = new XpsDocument(package, CompressionOption.Fast, documentUri.AbsoluteUri);

            //将flow document写入基于内存的xps document中去
            var writer = XpsDocument.CreateXpsDocumentWriter(xpsDocument);
            //writer.Write(((IDocumentPaginatorSource)mDoc).DocumentPaginator);
            writer.Write(GetPaginator(mDoc));
            //获取这个基于内存的xps document的fixed document
            docViewer.Document = xpsDocument.GetFixedDocumentSequence();
           
            //关闭基于内存的xps document
            xpsDocument.Close();
        }

        private delegate void LoadXpsMethod();
    }
}