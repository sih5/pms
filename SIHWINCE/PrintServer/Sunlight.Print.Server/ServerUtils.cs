﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ZXing;
using ZXing.Common;
using ZXing.QrCode;

namespace Sunlight.Print.Server {
    public class ServerUtils {
        public static ImageSource CreateQrCode(string content, out int width, BarcodeFormat barcodeFormat, bool isPureBarcode = false, string characterSet = "UTF-8", int defaultHeight = 46, int margin = 0) {
            EncodingOptions options = new QrCodeEncodingOptions {
                DisableECI = false,
                CharacterSet = characterSet,
                Margin = margin,
                Height = defaultHeight,
                PureBarcode = isPureBarcode
            };
            var write = new BarcodeWriter {
                Format = barcodeFormat,
                Options = options
            };
            var bitmap = write.Write(content);
            width = bitmap.Width;
            var ip = bitmap.GetHbitmap();
            var bitmapSource = Imaging.CreateBitmapSourceFromHBitmap(ip, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());

            DeleteObject(ip);
            return bitmapSource;
        }

        public static ImageSource CreateQrCode(string content, BarcodeFormat barcodeFormat, bool isPureBarcode = false, string characterSet = "UTF-8", int defaultHeight = 23, int margin = 0) {
            EncodingOptions options = new QrCodeEncodingOptions {
                DisableECI = false,
                CharacterSet = characterSet,
                Margin = margin,
                Height = defaultHeight,
                PureBarcode = isPureBarcode
            };
            var write = new BarcodeWriter {
                Format = barcodeFormat,
                Options = options
            };
            var bitmap = write.Write(content);
            var ip = bitmap.GetHbitmap();
            var bitmapSource = Imaging.CreateBitmapSourceFromHBitmap(ip, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());

            DeleteObject(ip);
            return bitmapSource;
        }

        public static Figure Figure(ImageSource imageSource) {
            var image = new Image {
                Source = imageSource,
                Margin = new Thickness(0),
                Stretch = Stretch.None
            };

            var map = new BlockUIContainer(image) {
                Padding = new Thickness(0, 0, 0, 0),
                Margin = new Thickness(0)
            };
            var fig = new Figure(map) {
                Margin = new Thickness(0),
                Padding = new Thickness(0)
            };

            return fig;
        }

        [DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);
    }
}
