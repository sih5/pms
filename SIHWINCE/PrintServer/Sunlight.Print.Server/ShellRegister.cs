﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;

namespace Sunlight.Print.Server {
    public class ShellRegister {
        [DllImport("user32.dll", EntryPoint = "GetWindowLong")]
        public static extern int GetWindowLong(IntPtr hwnd, int nIndex);

        [DllImport("user32.dll", EntryPoint = "SetWindowLong")]
        public static extern int SetWindowLong(IntPtr hMenu, int nIndex, int dwNewLong);

        [DllImport("user32.dll")]
        private static extern int SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int x, int y, int cx, int cy,
            int uFlags);

        /// <summary>
        ///     是否禁止最大化， 允许拖动
        /// </summary>
        /// <param name="sender">注册的窗体</param>
        /// <param name="isDisable">True：禁止  False：允许</param>
        public static void DisableMaxmizeBox(Window sender, bool isDisable) {
            var GWL_STYLE = -16;
            var WS_MAXIMIZEBOX = 0x00010000;
            var SWP_NOSIZE = 0x0001;
            var SWP_NOMOVE = 0x0002;
            var SWP_FRAMECHANGED = 0x0020;

            var handle = new WindowInteropHelper(sender).Handle;

            var nStyle = GetWindowLong(handle, GWL_STYLE);
            if (isDisable)
                nStyle &= ~WS_MAXIMIZEBOX;
            else
                nStyle |= WS_MAXIMIZEBOX;

            SetWindowLong(handle, GWL_STYLE, nStyle);
            SetWindowPos(handle, IntPtr.Zero, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE | SWP_FRAMECHANGED);
        }
    }
}