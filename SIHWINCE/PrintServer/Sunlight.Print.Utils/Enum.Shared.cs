﻿namespace Sunlight.Print.ExtendUtils {
    public enum MonitorType {
        打印配件入库计划 = 1,
        打印配件拣货单 = 2,
        获取打印机列表 = 3,
        打印箱号标签 = 4,
        关闭连接 = 99
    }

    /// <summary>
    /// 定义服务器响应客户端的数据类型，如果是消息，直接处理就可以，如果是响应的数据，数据类型，还需要客户端响应的反序列话对应的数据
    /// </summary>
    public enum ResponseType {
        消息 = 1,
        打印机列表 = 2
    }
}