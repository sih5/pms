﻿using System;
using System.Configuration;
using System.IO;
using System.Net;

namespace Sunlight.Print.ExtendUtils {
    /// <summary>
    /// 帮助类
    /// </summary>
    public class HttpUtils {
        /// <summary>
        /// Get Http 如：this.GetHttp("http://192.168.17.93:83/logistics/partsinboundplan/scan", "orderCode=\"we\"");
        /// </summary>
        /// <param name="url">Url地址</param>
        /// <param name="parameters">参数，格式如：name=1&sex=1</param>
        /// <returns>返回响应的Json 字符串</returns>
        public static string Get(string url, string parameters) {
            GC.Collect();
            if(!string.IsNullOrEmpty(parameters)) {
                url += "?" + parameters;
            }
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            // httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "GET";
            httpWebRequest.Timeout = 20000;
            httpWebRequest.KeepAlive = false;
            //httpWebRequest.Headers.Add("Cookie", FormsAuth.Cookie); 服务端设置匿名访问，避开Cookie校验
            string responseContent;
            using(var httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse()) {
                using(var streamReader = new StreamReader(httpWebResponse.GetResponseStream())) {
                    responseContent = streamReader.ReadToEnd();
                }
            }
            return responseContent;
        }

        /// <summary>
        /// 组合URL 如：http://192.168.17.93:83/logistics/partsinboundplan/scan
        /// </summary>
        /// <param name="urlRoute">如：logistics/partsinboundplan/scan</param>
        /// <returns>返回完整的URL地址</returns>
        public static string CombineUrl(string urlRoute) {
            string urlPath = ConfigurationManager.AppSettings["ServiceAddress"] + urlRoute;

            return urlPath;
        }
    }
}
