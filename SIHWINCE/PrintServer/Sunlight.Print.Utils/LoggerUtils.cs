﻿using System;
using NLog;

namespace Sunlight.Print.ExtendUtils {
    public static class LoggerUtils {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// 调试处理
        /// </summary>
        /// <param name="msg">写入调试消息</param>
        public static void Debug(String msg) {
            Logger.Debug(msg);
        }
        /// <summary>
        /// 故障信息处理
        /// </summary>
        /// <param name="msg">重大故障消息</param>
        public static void Fatal(String msg) {
            Logger.Fatal(msg);
        }
        /// <summary>
        /// 消息处理
        /// </summary>
        /// <param name="msg">写入日志的消息</param>
        public static void Info(String msg) {
            Logger.Info(msg);
        }
        /// <summary>
        /// 错误信息处理
        /// </summary>
        /// <param name="msg">写入日志的错误信息</param>
        public static void Error(String msg) {
            Logger.Error(msg);
        }
        /// <summary>
        /// 跟踪信息处理
        /// </summary>
        /// <param name="msg">写入日志的跟踪信息</param>
        public static void Trace(String msg) {
            Logger.Trace(msg);
        }
        /// <summary>
        /// 警告信息处理
        /// </summary>
        /// <param name="msg">写入日志的消息</param>
        public static void Warn(String msg) {
            Logger.Warn(msg);
        }
    }
}
