﻿using System;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Sunlight.Print.ExtendUtils {
    public partial class SocketUtils {
        /// <summary>
        /// 负责监听端口
        /// </summary>
        private static Socket socketListener;

        public static Hashtable CurrentConnections=new Hashtable();

        /// <summary>
        /// 负责客户端和服务端通信端口
        /// </summary>
        private static Socket socketConnection;

        /// <summary>
        /// 负责监听
        /// </summary>
        private static Thread threadListenPort;

        public static void StartService(IPAddress address, int port) {
            socketListener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            var endPoint = new IPEndPoint(address, port);
            socketListener.Bind(endPoint);
            socketListener.Listen(100);

            threadListenPort = new Thread(ListenPort);
            threadListenPort.Start();
        }

        public static void CloseCurrentSocket() {
            socketListener.Close();
            socketConnection.Close();
            threadListenPort.Abort();
        }

        private static void ListenPort() {
            while(true) {
                try {
                    //负责和客户端Socket通信，未收到消息会处于堵塞状态
                    socketConnection = socketListener.Accept();
                    var receiveMsg = string.Empty;
                    var receiveContent = new byte[4096];
                    var receiveContentLength = socketConnection.Receive(receiveContent, receiveContent.Length, 0);

                    //收到的内容
                    receiveMsg += Encoding.Default.GetString(receiveContent);

                    var responseMsg = "客户端请查收，服务器已经处理完毕";
                    byte[] sendBytes = Encoding.Default.GetBytes(responseMsg);
                    //发送的消息
                    socketConnection.Send(sendBytes, sendBytes.Length, 0);
                } catch(Exception) {
                    break;
                }
            }
        }
    }
}
