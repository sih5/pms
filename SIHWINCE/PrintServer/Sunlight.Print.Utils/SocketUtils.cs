﻿using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Newtonsoft.Json;
using Sunlight.Print.Model;

namespace Sunlight.Print.ExtendUtils {
    public partial class SocketUtils {
        /// <summary>
        /// 服务器监听器
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public static TcpListener ServerListener;

        /// <summary>
        ///     保存所有客户端会话，用于后面的清空工作
        /// </summary>
        public static Hashtable UserTable = new Hashtable();

        /// <summary>
        ///     关闭监听并释放资源
        /// </summary>
        public static void Close() {
            if(ServerListener != null)
                ServerListener.Stop();
            //关闭客户端连接并清理资源
            //每次连接都会有一个连接通道资源
            if(UserTable.Count != 0) {
                foreach(Socket session in UserTable.Values)
                    session.Shutdown(SocketShutdown.Both);
                UserTable.Clear();
            }
        }

        /// <summary>
        /// 启动监听
        /// </summary>
        /// <param name="address">监听的服务器IP地址</param>
        /// <param name="port">监听的服务器有效端口</param>
        public static void StartServer(IPAddress address, int port) {
            ServerListener = new TcpListener(address, port);
            ServerListener.Start();
            //while(true) {
            //创建用户的Socket
            var newClient = ServerListener.AcceptSocket();
            var packetBuff = new byte[1024 * 1024];
            var length = newClient.Receive(packetBuff, packetBuff.Length, 0);
            //接收客户端的需要打印的数据
            var messageContent = Encoding.Default.GetString(packetBuff, 0, length);
            var msgInfo = JsonConvert.DeserializeObject<MessageRequestPrintInfo>(messageContent);
            //处理打印

            //如果没有错误，将打印的结果返回
            var successMsg = new MessageResponsePrintInfo();
            successMsg.Message = "接收数据成功，客户端-服务端连接结束，剩下的交给服务端处理";
            successMsg.IsSuccess = true;

            newClient.Send(Encoding.Default.GetBytes(JsonConvert.SerializeObject(successMsg)), Encoding.Default.GetBytes(JsonConvert.SerializeObject(successMsg)).Length, 0);
            //将会话进行缓存
            if(!UserTable.ContainsKey(msgInfo.UniqueId))
                UserTable.Add(msgInfo.UniqueId, newClient);
            //结束当前会话
            var newClientThread = new Thread(Transmit.TransmitThread);
            newClientThread.Start(msgInfo.UniqueId);
            MessageBox.Show("a");
            // }
            // ReSharper disable once FunctionNeverReturns
        }
    }
}