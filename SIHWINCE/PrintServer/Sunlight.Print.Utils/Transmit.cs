﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Newtonsoft.Json;
using Sunlight.Print.Model;

namespace Sunlight.Print.ExtendUtils {
    public class Transmit {
        /// <summary>
        /// 序列化在线列表
        /// </summary>
        /// <returns>在线连接数</returns>
        public static byte[] AlignmentOnlineList() {
            StringCollection onlineList = new StringCollection();
            foreach(object online in SocketUtils.UserTable.Keys) {
                onlineList.Add(online as string);
            }
            IFormatter format = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            format.Serialize(stream, onlineList);
            byte[] returnList = stream.ToArray();
            stream.Close();
            return returnList;
        }

        public static MessageRequestPrintInfo DecodingClientMsg(byte[] clientMessage) {
            var msg = Encoding.Default.GetString(clientMessage, 0, clientMessage.Length);
            return JsonConvert.DeserializeObject<MessageRequestPrintInfo>(msg);
        }

        public static void TransmitThread(object userNumber) {
            Socket clientSocket = SocketUtils.UserTable[userNumber] as Socket;
            if(clientSocket == null)
                return;//TODO:此处待验证是否存在，否则会影响PDA正常的打印
            while(true) {
                try {
                    byte[] receivecmd = new byte[32];
                    //将消息载入receivecmd
                    clientSocket.Receive(receivecmd, receivecmd.Length, 0);
                    var msgInfo = DecodingClientMsg(receivecmd);
                    switch(msgInfo.MonitorType) {
                        case (int)MonitorType.关闭连接:
                            SocketUtils.UserTable.Remove(userNumber);
                            Thread.CurrentThread.Abort();
                            break;
                        case (int)MonitorType.打印配件入库计划:

                            break;
                        case (int)MonitorType.打印配件拣货单:

                            break;
                    }
                } catch(SocketException) {
                    //将发生异常的用户从用户列表中移除
                    SocketUtils.UserTable.Remove(userNumber);
                    string exceptionMessage = string.Format("[系统消息]用户{0}的客户端在{1}意外终止", userNumber, DateTime.Now);
                    //TODO:此处处理日志
                    //Thread.CurrentThread.Abort();
                    MessageBox.Show("");
                }
            }
        }
    }
}
