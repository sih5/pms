﻿using System;
using System.Collections.Generic;
using System.Net;

namespace Sunlight.Print.ExtendUtils {
    public class Utils {
        /// <summary>
        ///     获取本地IP地址信息
        /// </summary>
        public static List<IPAddress> GetAddressIp() {
            var addressIps = new List<IPAddress>();
            foreach(var ipAddress in Dns.GetHostEntry(Dns.GetHostName()).AddressList)
                if(ipAddress.AddressFamily.ToString() == "InterNetwork")
                    addressIps.Add(ipAddress);
            return addressIps;
        }

        /// <summary>
        /// C# byte数组合并(（二进制数组合并）
        /// </summary>
        /// <param name="srcArray1">待合并数组1</param>
        /// <param name="srcArray2">待合并数组2</param>
        /// <returns>合并后的数组</returns>
        public static byte[] CombineBinaryArray(byte[] srcArray1, byte[] srcArray2) {
            //根据要合并的两个数组元素总数新建一个数组
            var newArray = new byte[srcArray1.Length + srcArray2.Length];

            //把第一个数组复制到新建数组
            Array.Copy(srcArray1, 0, newArray, 0, srcArray1.Length);

            //把第二个数组复制到新建数组
            Array.Copy(srcArray2, 0, newArray, srcArray1.Length, srcArray2.Length);

            return newArray;
        }
    }
}