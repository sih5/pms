﻿using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Api.BLL {
    /// <summary>
    /// 1.此项目中的服务命名采用设计中的中文名称命名便于后续查找服务
    /// 2.参数需要添加注释说明
    /// </summary>
    public partial class DcsServiceImp {

        // ReSharper disable once InconsistentNaming
        private readonly DcsEntities ObjectContext = new DcsEntities();

        /// <summary>
        /// 根据编码规则生成新编号，该函数在单独的事务中完成。
        /// </summary>
        public string Generate(string name) {
            return CodeGenerator.Generate(name);
        }

        /// <summary>
        /// 根据编码规则生成新编号，该函数在单独的事务中完成。
        /// </summary>
        public string Generate(string name, string corpCode) {
            return CodeGenerator.Generate(name, corpCode);
        }
    }
}
