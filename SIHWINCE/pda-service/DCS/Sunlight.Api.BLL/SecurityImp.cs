﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.IO;
using System.Xml.Serialization;
using Sunlight.Api.Models;
using Sunlight.Api.Security;
using Sunlight.Api.Utils;
using Sunlight.Silverlight.Security.Web;

namespace Sunlight.Api.BLL {
    public partial class SecurityImp {
        //TODO 暂时这么做，后面考虑使用DepencyInject实现
        private const int DEFAULT_EXPAIR_HOUR = 2;
        private readonly SecurityEntities dbContext = new SecurityEntities();
        private readonly SecurityDomainService securityDomainService = new SecurityDomainService();

        private AuthorizeUser CreateAuthenticationUser(Personnel personnel) {
            var userInfo = new AuthorizeUser();
            userInfo.UserId = personnel.Id;
            userInfo.UserCode = personnel.LoginId;
            userInfo.UserName = personnel.Name;
            userInfo.EnterpriseId = personnel.EnterpriseId;
            userInfo.EnterpriseCode = personnel.Enterprise.Code;
            userInfo.EnterpriseName = personnel.Enterprise.Name;
            userInfo.EnterpriseType = personnel.Enterprise.EnterpriseCategoryId;
            return userInfo;
        }

        /// <summary>
        /// 登陆验证
        /// </summary>
        /// <param name="authenticationUser">登陆的相关参数</param>
        public MessageInfo<AppMenusModel> Login(AuthenticationUserModel authenticationUser) {
            int effectiveHours;
            var messageInfo = new MessageInfo<AppMenusModel>();
            var presonnel = this.securityDomainService.LoginForPDA(authenticationUser.LoginId, authenticationUser.Password, authenticationUser.EnterpriseCode);

            if (presonnel == null)
                throw new ValidationException("请输入正确的登陆信息");

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LoginEffectiveHours"])) {
                var isValid = int.TryParse(ConfigurationManager.AppSettings["LoginEffectiveHours"], out effectiveHours);
                if (!isValid || effectiveHours <= 0)
                    effectiveHours = DEFAULT_EXPAIR_HOUR;
            } else
                effectiveHours = DEFAULT_EXPAIR_HOUR;

            var userInfo = CreateAuthenticationUser(presonnel);

            FormsAuth.SignIn(presonnel.LoginId, userInfo, 60 * effectiveHours);

            var menuXml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>" +
            Environment.NewLine +
            this.securityDomainService.GetMenuXmlForPDA(userInfo.UserId);

            var menuWithUserInfoXml = menuXml.Replace("@UserId", userInfo.UserId.ToString())
                                             .Replace("@UserCode", userInfo.UserCode)
                                             .Replace("@UserName", userInfo.UserName)
                                             .Replace("@EnterpriseId", userInfo.EnterpriseId.ToString())
                                             .Replace("@EnterpriseCode", userInfo.EnterpriseCode)
                                             .Replace("@EnterpriseName", userInfo.EnterpriseName)
                                             .Replace("@EnterpriseType", userInfo.EnterpriseType.ToString());

            using (StringReader sr = new StringReader(menuWithUserInfoXml)) {
                XmlSerializer xmldes = new XmlSerializer(typeof(AppMenusModel));
                messageInfo.Data = xmldes.Deserialize(sr) as AppMenusModel;
            }

            messageInfo.Success = true;
            messageInfo.Message = "登陆成功";
            return messageInfo;
        }
    }
}
