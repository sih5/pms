﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Transactions;
using Sunlight.Api.Models;
using Sunlight.Api.Security;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Api.BLL {
    public partial class DcsServiceImp {
        public List<BoxUpTaskModel> 根据单号查询待装箱任务单(string code, int warehouseId) {
            var userInfo = FormsAuth.GetUserData();
            var result = (from bt in this.ObjectContext.BoxUpTasks.Where(o => o.Code.IndexOf(code) >= 0 && o.WarehouseId == warehouseId && (o.Status == (int)DcsBoxUpTaskStatus.新建 || o.Status == (int)DcsBoxUpTaskStatus.部分装箱 || o.Status == (int)DcsBoxUpTaskStatus.停用))
                          join btd in this.ObjectContext.BoxUpTaskDetails on bt.Id equals btd.BoxUpTaskId
                          join pt in this.ObjectContext.PickingTasks on btd.PickingTaskId equals pt.Id
                          where btd.PlanQty > (btd.BoxUpQty ?? 0)
                          select new BoxUpTaskModel {
                              Id = bt.Id,
                              Code = bt.Code,
                              Status = bt.Status.Value,
                              WarehouseId = bt.WarehouseId.Value,
                              DetailId = btd.Id,
                              PartsOutboundPlanId = btd.PartsOutboundPlanId.Value,
                              SourceCode = btd.SourceCode,
                              SparePartId = btd.SparePartId,
                              SparePartCode = btd.SparePartCode,
                              SparePartName = btd.SparePartName,
                              PlanQty = btd.PlanQty.Value,
                              BoxUpQty = btd.BoxUpQty,
                              isUrgent = !string.IsNullOrEmpty(pt.OrderTypeName) ? (pt.OrderTypeName.IndexOf("急") > 0 ? "是" : "否") : "否"
                          }).ToList();
            return result;
        }

        public bool 装箱(List<BoxUpTaskModel> boxUpTasks, bool isBoxUpFinish = false) {
            var user = FormsAuth.GetUserData();
            var boxUpTaskId = boxUpTasks.First().Id;
            var boxUpTaskOrig = ObjectContext.BoxUpTasks.FirstOrDefault(r => r.Id == boxUpTaskId);
            var boxUpTaskDetailOrigs = ObjectContext.BoxUpTaskDetails.Where(r => r.BoxUpTaskId == boxUpTaskId).ToList();

            foreach(var boxUpTaskModel in boxUpTasks) {
                var boxUpTaskDetailOrig = boxUpTaskDetailOrigs.Single(r => r.Id == boxUpTaskModel.DetailId);
                var nowBoxUpQuantity = (boxUpTaskModel.ThisBoxUpQty ?? 0);
                var planQuantity = boxUpTaskDetailOrig.PlanQty;
                var boxUpQuantity = boxUpTaskDetailOrig.BoxUpQty ?? 0;

                if(nowBoxUpQuantity + boxUpQuantity > planQuantity) {
                    throw new ValidationException("本次装箱量不能大于可装箱量");
                }

                if(nowBoxUpQuantity > 0) {
                    //如果数据库中清单箱号为空，或者箱号相同，累加已装箱数量
                    if(string.IsNullOrEmpty(boxUpTaskDetailOrig.ContainerNumber) || boxUpTaskDetailOrig.ContainerNumber.Equals(boxUpTaskModel.ContainerNumber)) {
                        //更新装箱单清单
                        boxUpTaskDetailOrig.BoxUpQty = boxUpQuantity + nowBoxUpQuantity;
                        boxUpTaskDetailOrig.ContainerNumber = boxUpTaskModel.ContainerNumber;
                    }
                        //如果数据库箱号与本次装箱箱号不一致,拆分原清单
                    else {
                        //更新装箱单清单
                        boxUpTaskDetailOrig.PlanQty = boxUpQuantity;

                        //拆分,计划量 = 原清单计划量 - 原清单已装箱量，已装箱量 = 本次装箱量
                        var newBoxUpTaskDetail = new BoxUpTaskDetail();
                        newBoxUpTaskDetail.BoxUpTaskId = boxUpTaskOrig.Id;
                        newBoxUpTaskDetail.BoxUpTaskCode = boxUpTaskOrig.Code;
                        newBoxUpTaskDetail.PartsOutboundPlanId = boxUpTaskDetailOrig.PartsOutboundPlanId;
                        newBoxUpTaskDetail.PartsOutboundPlanCode = boxUpTaskDetailOrig.PartsOutboundPlanCode;
                        newBoxUpTaskDetail.PickingTaskId = boxUpTaskDetailOrig.PickingTaskId;
                        newBoxUpTaskDetail.PickingTaskCode = boxUpTaskDetailOrig.PickingTaskCode;
                        newBoxUpTaskDetail.SourceCode = boxUpTaskDetailOrig.SourceCode;
                        newBoxUpTaskDetail.SparePartId = boxUpTaskDetailOrig.SparePartId;
                        newBoxUpTaskDetail.SparePartCode = boxUpTaskDetailOrig.SparePartCode;
                        newBoxUpTaskDetail.SparePartName = boxUpTaskDetailOrig.SparePartName;
                        newBoxUpTaskDetail.SihCode = boxUpTaskDetailOrig.SihCode;
                        newBoxUpTaskDetail.PlanQty = planQuantity - boxUpQuantity;
                        newBoxUpTaskDetail.PickingTaskDetailId = boxUpTaskDetailOrig.PickingTaskDetailId;
                        newBoxUpTaskDetail.ContainerNumber = boxUpTaskModel.ContainerNumber;
                        newBoxUpTaskDetail.BoxUpQty = nowBoxUpQuantity;
                        ObjectContext.BoxUpTaskDetails.AddObject(newBoxUpTaskDetail);
                    }

                    //新增出库实绩
                    PartsOutboundPerformance partsOutboundPerformance = new PartsOutboundPerformance();
                    partsOutboundPerformance.Code = boxUpTaskModel.Code;
                    partsOutboundPerformance.OutType = (int)DcsPartsOutboundPer_OutType.装箱;
                    partsOutboundPerformance.Qty = nowBoxUpQuantity;
                    partsOutboundPerformance.OperaterId = user.UserId;
                    partsOutboundPerformance.OperaterName = user.UserName;
                    partsOutboundPerformance.OperaterEndTime = DateTime.Now;
                    partsOutboundPerformance.Equipment = (int)DcsPartsOutboundPer_EquipmentType.手持;
                    partsOutboundPerformance.ContainerNumber = boxUpTaskModel.ContainerNumber;
                    partsOutboundPerformance.SparePartId = boxUpTaskModel.SparePartId;
                    partsOutboundPerformance.SpareCode = boxUpTaskModel.SparePartCode;
                    partsOutboundPerformance.SpareName = boxUpTaskModel.SparePartName;
                    ObjectContext.PartsOutboundPerformances.AddObject(partsOutboundPerformance);
                }
            }

            ObjectContext.SaveChanges();
            //重新查询清单数据
            boxUpTaskDetailOrigs = ObjectContext.BoxUpTaskDetails.Where(r => r.BoxUpTaskId == boxUpTaskId).ToList();

            //如果是点击装箱完成按钮，未装箱的部分拆分成另一张装箱单
            var now = DateTime.Now;
            if(isBoxUpFinish) {
                //如果全部未装箱，则直接改为停用状态
                var boxupSum = boxUpTaskDetailOrigs.Sum(r => r.BoxUpQty);
                if((boxupSum ?? 0) == 0) {
                    //更新主单状态
                    boxUpTaskOrig.Status = (int)DcsBoxUpTaskStatus.停用;
                    boxUpTaskOrig.ModifierId = user.UserId;
                    boxUpTaskOrig.ModifierName = user.UserName;
                    boxUpTaskOrig.ModifyTime = now;
                } else {
                    //更新主单状态
                    boxUpTaskOrig.Status = (int)DcsBoxUpTaskStatus.装箱完成;
                    boxUpTaskOrig.ModifierId = user.UserId;
                    boxUpTaskOrig.ModifierName = user.UserName;
                    boxUpTaskOrig.ModifyTime = now;
                    boxUpTaskOrig.BoxUpFinishTime = now;

                    var unFinish = boxUpTaskDetailOrigs.Where(r => r.BoxUpQty != r.PlanQty).ToArray();
                    if(unFinish != null && unFinish.Count() > 0) {
                        //创建拆分之后产生的主单,状态=停用
                        var boxUpTask = new BoxUpTask();
                        boxUpTask.Code = CodeGenerator.Generate("BoxUpTask", user.EnterpriseCode);
                        boxUpTask.PartsSalesCategoryId = boxUpTaskOrig.PartsSalesCategoryId;
                        boxUpTask.WarehouseId = boxUpTaskOrig.WarehouseId;
                        boxUpTask.WarehouseCode = boxUpTaskOrig.WarehouseCode;
                        boxUpTask.WarehouseName = boxUpTaskOrig.WarehouseName;
                        boxUpTask.CounterpartCompanyId = boxUpTaskOrig.CounterpartCompanyId;
                        boxUpTask.CounterpartCompanyName = boxUpTaskOrig.CounterpartCompanyName;
                        boxUpTask.CounterpartCompanyCode = boxUpTaskOrig.CounterpartCompanyCode;
                        boxUpTask.Status = (int)DcsBoxUpTaskStatus.停用;
                        boxUpTask.IsExistShippingOrder = false;
                        boxUpTask.CreatorId = user.UserId;
                        boxUpTask.CreatorName = user.UserName;
                        boxUpTask.CreateTime = now;

                        foreach(var item in unFinish) {
                            var plan = item.PlanQty;
                            var boxup = item.BoxUpQty ?? 0;
                            var newPlan = plan - boxup;

                            //创建拆分之后的清单
                            var newBoxUpDetail = new BoxUpTaskDetail();
                            newBoxUpDetail.BoxUpTaskId = boxUpTask.Id;
                            newBoxUpDetail.BoxUpTaskCode = boxUpTask.Code;
                            newBoxUpDetail.PartsOutboundPlanId = item.PartsOutboundPlanId;
                            newBoxUpDetail.PartsOutboundPlanCode = item.PartsOutboundPlanCode;
                            newBoxUpDetail.PickingTaskId = item.PickingTaskId;
                            newBoxUpDetail.PickingTaskCode = item.PickingTaskCode;
                            newBoxUpDetail.SourceCode = item.SourceCode;
                            newBoxUpDetail.SparePartId = item.SparePartId;
                            newBoxUpDetail.SparePartCode = item.SparePartCode;
                            newBoxUpDetail.SparePartName = item.SparePartName;
                            newBoxUpDetail.SihCode = item.SihCode;
                            newBoxUpDetail.PlanQty = newPlan;
                            newBoxUpDetail.PickingTaskDetailId = item.PickingTaskDetailId;
                            boxUpTask.BoxUpTaskDetails.Add(newBoxUpDetail);

                            if(boxup == 0) {
                                //删除原清单
                                ObjectContext.DeleteObject(item);
                            } else {
                                //更新原清单
                                item.PlanQty = boxup;
                            }
                        }
                        ObjectContext.BoxUpTasks.AddObject(boxUpTask);
                    }
                }
            } else {
                bool flag = true;
                //更新主单状态
                foreach(var boxUpTaskDetailOrig1 in boxUpTaskDetailOrigs) {
                    var boxupqty = boxUpTaskDetailOrig1.BoxUpQty ?? 0;
                    var planqty = boxUpTaskDetailOrig1.PlanQty;
                    if(planqty != boxupqty) {
                        flag = false;
                    }
                }
                if(flag) {
                    boxUpTaskOrig.Status = (int)DcsBoxUpTaskStatus.装箱完成;
                    boxUpTaskOrig.BoxUpFinishTime = DateTime.Now;
                } else
                    boxUpTaskOrig.Status = (int)DcsBoxUpTaskStatus.部分装箱;
                boxUpTaskOrig.ModifierId = user.UserId;
                boxUpTaskOrig.ModifierName = user.UserName;
                boxUpTaskOrig.ModifyTime = DateTime.Now;
            }

            ObjectContext.SaveChanges();
            //更新出库计划单状态
            var partsOutboundPlanIds = boxUpTasks.Select(r => r.PartsOutboundPlanId).Distinct().ToArray();
            var partsOutboundPlans = ObjectContext.PartsOutboundPlans.Where(r => partsOutboundPlanIds.Contains(r.Id)).ToArray();
            foreach(var partsOutboundPlan in partsOutboundPlans) {
                更新出库计划单状态(partsOutboundPlan);
            }
            return true;
        }
    }
}
