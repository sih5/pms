﻿using System.Collections.Generic;
using Sunlight.Api.Models;
using System.Linq;

namespace Sunlight.Api.BLL {
    public partial class DcsServiceImp {
        public List<KeyValueItemModel> GetKeyValueItems(string[] names) {
            if(names == null || !names.Any())
                return null;

            return this.ObjectContext.KeyValueItems.Where(r => names.Any(k => k == r.Name)).Select(r => new KeyValueItemModel {
                Id = r.Id,
                Name = r.Name,
                Value = r.Value,
                Key = r.Key
            }).ToList();
        }
    }
}
