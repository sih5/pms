﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Sunlight.Api.Models;
using Sunlight.Api.Security;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Api.BLL {
    public partial class DcsServiceImp {
        public List<PackingTaskModel> 查询待包装任务(string code, string sparePartCode, int warehouseId) {
            var currentUser = FormsAuth.GetUserData();

            var result = (from pt in this.ObjectContext.PackingTasks.Where(o => o.PartsInboundCheckBillCode.IndexOf(code) >= 0 && o.WarehouseId == warehouseId && (o.Status == (int)DcsPackingTaskStatusd.新增 || o.Status == (int)DcsPackingTaskStatusd.部分包装) && o.SparePartCode == sparePartCode)
                          join sp in this.ObjectContext.SpareParts on pt.SparePartId equals sp.Id
                          select new PackingTaskModel {
                              Id = pt.Id,
                              Code = pt.Code,
                              SparePartId = pt.SparePartId,
                              SparePartCode = pt.SparePartCode,
                              SparePartName = pt.SparePartName,
                              PlanQty = pt.PlanQty,
                              PackingQty = pt.PackingQty,
                              TraceProperty = sp.TraceProperty,
                              PartsInboundPlanId = pt.PartsInboundPlanId
                          }).ToList();
            foreach(var item in result.OrderBy(o => o.Code)) {
                var partsBranchPackingProp = this.ObjectContext.PartsBranchPackingProps.OrderByDescending(o => o.PackingType).FirstOrDefault(o => o.SparePartId == item.SparePartId);
                item.PackingType = partsBranchPackingProp.PackingType;
                item.PackingCoefficient = partsBranchPackingProp.PackingCoefficient;
                item.MeasureUnit = partsBranchPackingProp.MeasureUnit;
                item.PackingMaterial = partsBranchPackingProp.PackingMaterial;

                if(item.TraceProperty == (int)DCSTraceProperty.批次追溯) {
                    var traceCodes = this.ObjectContext.AccurateTraces.Where(o => o.SourceBillId == item.PartsInboundPlanId
                                   && o.PartId == item.SparePartId && o.Type == (int)DCSAccurateTraceType.出库).Select(o => o.TraceCode).ToList();
                    item.TraceCodes = traceCodes;
                }
            }
            return result;
        }

        public bool 包装登记(PackingTaskModel packingTaskModel) {
            var userInfo = FormsAuth.GetUserData();
            var packingTask = ObjectContext.PackingTasks.FirstOrDefault(o => o.Id == packingTaskModel.Id);
            packingTask.ScanNumber = (packingTaskModel.ThisPackingQty ?? 0);
            packingTask.PackingNumber = (packingTaskModel.PackingCoefficient ?? 0);
            packingTask.TracePropertyInt = packingTaskModel.TraceProperty;
            var packingMaterial = packingTaskModel.PackingMaterial;

            int containerNum = (int)(Math.Ceiling(Double.Parse(packingTask.ScanNumber.ToString()) / Double.Parse(packingTask.PackingNumber.ToString())));
            //生成包装任务单清单
            var packingTaskDetail = new PackingTaskDetail {
                PackingTaskId = packingTask.Id,
                RecomPackingMaterial = packingMaterial,
                RecomQty = containerNum,
                PhyPackingMaterial = packingMaterial,
                PhyQty = containerNum
            };
            ObjectContext.PackingTaskDetails.AddObject(packingTaskDetail);

            int scanner = packingTask.ScanNumber;
            //D、更新配件检验区锁定库存      
            var warehouseAreaCategoryIds = ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.检验区).Select(v => v.Id).ToArray();
            if(warehouseAreaCategoryIds.Length == 0)
                return false;

            var partsStocks = GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => r.PartId == packingTask.SparePartId && warehouseAreaCategoryIds.Contains(r.WarehouseAreaCategoryId.Value) && r.WarehouseId == packingTask.WarehouseId && r.LockedQty >= scanner)).ToList();

            //var partsStocks = (from a in ObjectContext.PartsStocks
            //                   join b in ObjectContext.WarehouseAreaCategories on a.WarehouseAreaCategoryId equals b.Id
            //                   where a.PartId == packingTask.SparePartId && b.Category == 3 && a.WarehouseId == packingTask.WarehouseId && a.LockedQty >= scanner
            //                   select a).ToArray();
            if(partsStocks.Count == 0)
                throw new ValidationException("未找到配件检验区库存！");

            var partsStock = partsStocks.FirstOrDefault(o => o.PartId == packingTask.SparePartId);
            if(partsStock.LockedQty == null || packingTask.ScanNumber > partsStock.LockedQty) {
                throw new ValidationException("检验区库存异常！");
            }
            partsStock.LockedQty = partsStock.LockedQty - packingTask.ScanNumber;
            partsStock.ModifierId = userInfo.UserId;
            partsStock.ModifierName = userInfo.UserName;
            partsStock.ModifyTime = DateTime.Now;

            //F、生成上架任务单
            var partsShelvesTask = new PartsShelvesTask {
                PackingTaskId = packingTask.Id,
                PackingTaskCode = packingTask.Code,
                PartsInboundPlanId = packingTask.PartsInboundPlanId,
                PartsInboundPlanCode = packingTask.PartsInboundPlanCode,
                PartsInboundCheckBillId = packingTask.PartsInboundCheckBillId,
                PartsInboundCheckBillCode = packingTask.PartsInboundCheckBillCode,
                WarehouseId = packingTask.WarehouseId,
                WarehouseCode = packingTask.WarehouseCode,
                WarehouseName = packingTask.WarehouseName,
                SparePartId = packingTask.SparePartId,
                PlannedAmount = packingTask.ScanNumber,
                SourceBillCode = packingTask.SourceCode,
                CreatorId = userInfo.UserId,
                CreatorName = userInfo.UserName,
                CreateTime = DateTime.Now,
                Status = (int)DcsShelvesTaskStatus.新增,
                BatchNumber = packingTask.BatchNumber

            };
            //获取编号    
            partsShelvesTask.Code = CodeGenerator.Generate("PartsShelvesTask", userInfo.EnterpriseCode);
            ObjectContext.PartsShelvesTasks.AddObject(partsShelvesTask);
            //生成入库实绩
            var partsInboundPerformance = new PartsInboundPerformance {
                InType = (int)DcsPartsInboundPer_InType.包装,
                BarCode = packingTask.BatchNumber,
                Equipment = (int)DcsPartsInboundPer_EquipmentType.手持,
                WarehouseAreaId = packingTask.WarehouseId,
                ContainerNum = containerNum,
                Qty = packingTask.ScanNumber,
                OperaterId = userInfo.UserId,
                OperaterName = userInfo.UserName,
                OperaterEndTime = DateTime.Now,
                Code = packingTask.Code,
                SparePartId = packingTask.SparePartId,
                SpareCode = packingTask.SparePartCode,
                SpareName = packingTask.SparePartName
            };
            ObjectContext.PartsInboundPerformances.AddObject(partsInboundPerformance);
            //更新包装任务单
            var oldPacking = ObjectContext.PackingTasks.Where(r => r.Id == packingTask.Id).FirstOrDefault();
            if(null == oldPacking.PackingQty) {
                oldPacking.PackingQty = 0;
            }
            oldPacking.PackingQty = oldPacking.PackingQty + packingTask.ScanNumber;
            if(oldPacking.PackingQty > oldPacking.PlanQty) {
                throw new ValidationException("已装箱量大于计划量，不允许装箱");
            }
            if(oldPacking.PackingQty == oldPacking.PlanQty) {
                oldPacking.Status = (int)DcsPackingTaskStatusd.包装完成;
            } else {
                oldPacking.Status = (int)DcsPackingTaskStatusd.部分包装;
            }
            oldPacking.ModifierId = userInfo.UserId;
            oldPacking.ModifierName = userInfo.UserName;
            oldPacking.ModifyTime = DateTime.Now;
            //追溯码相关
            var sihCodes = packingTaskModel.SIHCodes.ToArray();
            var traceCodes = packingTaskModel.TraceCodes.ToArray();
            var boxCodes = packingTaskModel.BoxCodes.ToArray();
            var traces = ObjectContext.AccurateTraces.Where(t => t.Status == (int)DCSAccurateTraceStatus.有效 && t.Type == (int)DCSAccurateTraceType.入库 && t.CompanyId == userInfo.EnterpriseId).ToArray();
            //若存在已终止的入库计划单，则状态改为不考核 
            var abandAtrace = ObjectContext.AccurateTraces.Where(t => t.SourceBillId != oldPacking.PartsInboundPlanId && t.Status == (int)DCSAccurateTraceStatus.有效 && t.Type == (int)DCSAccurateTraceType.入库 && t.CompanyId == userInfo.EnterpriseId && t.SIHLabelCode == null && ObjectContext.PartsInboundPlans.Any(e => e.Id == t.SourceBillId && e.Status == (int)DcsPartsInboundPlanStatus.终止 && ObjectContext.PartsInboundPlanDetails.Any(y => y.PartsInboundPlanId == e.Id && y.SparePartId == t.PartId))).ToArray();
            //作废的追溯
            var abandTrace = ObjectContext.AccurateTraces.Where(t => t.Status == (int)DCSAccurateTraceStatus.有效 && t.SIHLabelCode != "" && t.CompanyId == userInfo.EnterpriseId && t.Type == (int)DCSAccurateTraceType.入库 && t.SourceBillId != oldPacking.PartsInboundPlanId && (t.OutQty != null && t.InQty == t.OutQty)).ToArray();
            if(boxCodes.Count() > 0 && sihCodes.Count() > 0 && traceCodes.Count() > 0 && packingTask.TracePropertyInt.HasValue && traces.Count() > 0) {

                if(packingTask.TracePropertyInt == (int)DCSTraceProperty.精确追溯) {
                    for(int i = 0; i < scanner; i++) {
                        var abandAccutraces = abandAtrace.Where(t => t.TraceCode == traceCodes[i]).ToArray();
                        foreach(var aband in abandAccutraces) {
                            aband.ModifierId = userInfo.UserId;
                            aband.ModifierName = userInfo.UserName;
                            aband.ModifyTime = DateTime.Now;
                            aband.Status = (int)DCSAccurateTraceStatus.不考核;
                        }
                        var sih = traces.Where(t => t.SIHLabelCode == sihCodes[i] && t.PackingTaskId == packingTask.Id).FirstOrDefault();
                        if(sih != null) {
                            throw new ValidationException("SIH标签码:" + sihCodes[i] + "已包装完成");
                        }
                        var trace = traces.Where(t => t.TraceCode == traceCodes[i] && t.SIHLabelCode == null && t.SourceBillId == oldPacking.PartsInboundPlanId).FirstOrDefault();
                        if(trace == null) {
                            if(oldPacking.CounterpartCompanyCode.Equals("1000003829") || oldPacking.CounterpartCompanyCode.Equals("1000002367")) {
                                //	当配件供应商为SIH、车桥公司时
                                //新增入库类型的【精确追溯】信息，不需要校验精确码信息，自动绑定“备件编号”+“SIH防伪标签码”和“精确码”
                                var newAccurateTrace = new AccurateTrace {
                                    Type = (int)DCSAccurateTraceType.入库,
                                    CompanyType = (int)DCSAccurateTraceCompanyType.配件中心,
                                    CompanyId = userInfo.EnterpriseId,
                                    SourceBillId = oldPacking.PartsInboundPlanId,
                                    PartId = oldPacking.SparePartId,
                                    TraceCode = traceCodes[i],
                                    SIHLabelCode = sihCodes[i],
                                    Status = (int)DCSAccurateTraceStatus.有效,
                                    TraceProperty = packingTask.TracePropertyInt,
                                    BoxCode = boxCodes[i],
                                    //  InQty = 1,
                                    CreateTime = DateTime.Now,
                                    CreatorId = userInfo.UserId,
                                    CreatorName = userInfo.UserName,
                                    PackingTaskId = oldPacking.Id

                                };
                                this.ObjectContext.AccurateTraces.AddObject(newAccurateTrace); continue;
                            } else
                                throw new ValidationException("未找到有效的追溯码:" + traceCodes[i]);

                        }
                        trace.SIHLabelCode = sihCodes[i];
                        trace.BoxCode = boxCodes[i];
                        //  trace.InQty = 1;
                        trace.ModifierId = userInfo.UserId;
                        trace.ModifierName = userInfo.UserName;
                        trace.ModifyTime = DateTime.Now;
                        trace.PackingTaskId = oldPacking.Id;
                        var cancel = abandTrace.Where(t => t.TraceCode == traceCodes[i]).FirstOrDefault();
                        if(cancel != null) {
                            cancel.ModifierId = userInfo.UserId;
                            cancel.ModifierName = userInfo.UserName;
                            cancel.ModifyTime = DateTime.Now;
                            cancel.Status = (int)DCSAccurateTraceStatus.作废;
                        }
                    }
                } else {
                    var abandAccutraces = abandAtrace.Where(t => t.TraceCode == traceCodes[0]).ToArray();
                    foreach(var aband in abandAccutraces) {
                        aband.ModifierId = userInfo.UserId;
                        aband.ModifierName = userInfo.UserName;
                        aband.ModifyTime = DateTime.Now;
                        aband.Status = (int)DCSAccurateTraceStatus.不考核;
                    }
                    var trace = traces.Where(t => t.TraceCode == traceCodes[0] && t.SourceBillId == oldPacking.PartsInboundPlanId).FirstOrDefault();
                    if(trace != null) {
                        if(trace.SIHLabelCode == null) {
                            trace.SIHLabelCode = sihCodes[0];
                            trace.BoxCode = boxCodes[0];
                        }

                        //  trace.InQty = (trace.InQty == null ? 0 : trace.InQty.Value) + packingTask.ScanNumber;
                        trace.ModifierId = userInfo.UserId;
                        trace.ModifierName = userInfo.UserName;
                        trace.ModifyTime = DateTime.Now;
                        trace.PackingTaskId = oldPacking.Id;
                        var cancel = abandTrace.Where(t => t.TraceCode == traceCodes[0]).FirstOrDefault();
                        if(cancel != null) {
                            cancel.ModifierId = userInfo.UserId;
                            cancel.ModifierName = userInfo.UserName;
                            cancel.ModifyTime = DateTime.Now;
                            cancel.Status = (int)DCSAccurateTraceStatus.作废;
                        }
                    } else if(oldPacking.CounterpartCompanyCode.Equals("1000003829") || oldPacking.CounterpartCompanyCode.Equals("1000002367")) {
                        //	当配件供应商为SIH、车桥公司时
                        //新增入库类型的【精确追溯】信息，不需要校验精确码信息，自动绑定“备件编号”+“SIH防伪标签码”和“精确码”
                        var newAccurateTrace = new AccurateTrace {
                            Type = (int)DCSAccurateTraceType.入库,
                            CompanyType = (int)DCSAccurateTraceCompanyType.配件中心,
                            CompanyId = userInfo.EnterpriseId,
                            SourceBillId = oldPacking.PartsInboundPlanId,
                            PartId = oldPacking.SparePartId,
                            TraceCode = traceCodes[0],
                            SIHLabelCode = sihCodes[0],
                            Status = (int)DCSAccurateTraceStatus.有效,
                            TraceProperty = packingTask.TracePropertyInt,
                            BoxCode = boxCodes[0],
                            //  InQty = packingTask.ScanNumber,
                            CreateTime = DateTime.Now,
                            CreatorId = userInfo.UserId,
                            CreatorName = userInfo.UserName,
                            PackingTaskId = oldPacking.Id

                        };
                        this.ObjectContext.AccurateTraces.AddObject(newAccurateTrace);
                    }

                }
            } else if(boxCodes.Count() > 0 && sihCodes.Count() > 0 && (!packingTask.TracePropertyInt.HasValue || traces.Count() == 0)) {
                //有SIH标签码，但没有追溯码
                for(int i = 0; i < sihCodes.Count(); i++) {
                  //  if(traces.Any(t => t.SIHLabelCode == sihCodes[i] && t.SourceBillId == packingTask.PartsInboundPlanId)) {
                 //      throw new ValidationException("SIH标签码:" + sihCodes[i] + "已包装完成");
                 //   }
                    var oldTrace = traces.Where(t => t.BoxCode == boxCodes[i] && t.SIHLabelCode == sihCodes[i] && t.SourceBillId == packingTask.PartsInboundPlanId &&  t.PackingTaskId ==packingTask.Id).FirstOrDefault();
                    if(oldTrace != null) {
                        // oldTrace.InQty = oldTrace.InQty + packingTask.ScanNumber;
                        oldTrace.ModifierId = userInfo.UserId;
                        oldTrace.ModifierName = userInfo.UserName;
                        oldTrace.ModifyTime = DateTime.Now;
                    } else {
                        var newAccurateTrace = new AccurateTrace {
                            Type = (int)DCSAccurateTraceType.入库,
                            CompanyType = (int)DCSAccurateTraceCompanyType.配件中心,
                            CompanyId = userInfo.EnterpriseId,
                            SourceBillId = oldPacking.PartsInboundPlanId,
                            PartId = oldPacking.SparePartId,
                            SIHLabelCode = sihCodes[i],
                            Status = (int)DCSAccurateTraceStatus.有效,
                            TraceProperty = packingTask.TracePropertyInt,
                            BoxCode = boxCodes[i],
                            //  InQty = packingTask.ScanNumber,
                            CreateTime = DateTime.Now,
                            CreatorId = userInfo.UserId,
                            CreatorName = userInfo.UserName,
                            PackingTaskId = packingTask.Id
                        };
                        this.ObjectContext.AccurateTraces.AddObject(newAccurateTrace);
                    }
                }
            }
            ObjectContext.SaveChanges();
            //若包装单状态为已包装，且配件供应商为SIH、车桥公司时，更新原 入库追溯信息为不考核
            if(oldPacking.Status == (int)DcsPackingTaskStatusd.包装完成 && (oldPacking.CounterpartCompanyCode.Equals("1000003829") || oldPacking.CounterpartCompanyCode.Equals("1000002367"))) {
                var tracest = ObjectContext.AccurateTraces.Where(t => t.CompanyId == userInfo.EnterpriseId && t.SourceBillId == oldPacking.PartsInboundPlanId && t.SIHLabelCode == null).ToArray();
                if(tracest.Length > 0) {
                    foreach(var item in tracest) {
                        item.Status = (int)DCSAccurateTraceStatus.不考核;
                        item.ModifierId = userInfo.UserId;
                        item.ModifierName = userInfo.UserName;
                        item.ModifyTime = DateTime.Now;
                    }
                }
            }
            // E、更新入库计划单状态 //调用公共方法
            var partsInboundPlan = ObjectContext.PartsInboundPlans.FirstOrDefault(r => r.Id == packingTask.PartsInboundPlanId);
            if(null == partsInboundPlan) {
                throw new ValidationException("未找到有效的入库计划单");
            }
            ObjectContext.SaveChanges();
            更新入库计划状态(partsInboundPlan);
            return true;
        }
    }
}
