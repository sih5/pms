﻿using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Api.BLL
{
    public partial class DcsServiceImp
    {
        public string PartsBoxCodeGenerate(string code)
        {
            if (string.IsNullOrEmpty(code))
                throw new ValidationException("箱号生成流水号编号不能为空");
            using (var objectContext = new DcsEntities())
            {
                //var codeTemplate = objectContext.PartsBoxCodeTemplates.SingleOrDefault(d => d.Code == code);
                //if(codeTemplate == null) {
                //    codeTemplate = new PartsBoxCodeTemplate();
                //    codeTemplate.Code = code;
                //    codeTemplate.Serial = 1;
                //    objectContext.PartsBoxCodeTemplates.AddObject(codeTemplate);
                //} else {
                //    codeTemplate.Serial += 1;
                //}
                //objectContext.SaveChanges();
                //return string.Format("{0}{1}", code, codeTemplate.Serial.ToString(CultureInfo.InvariantCulture).PadLeft(3, '0'));
                return "";
            }
        }
    }
}
