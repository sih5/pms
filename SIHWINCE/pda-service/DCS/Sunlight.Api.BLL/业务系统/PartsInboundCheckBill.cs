﻿using Microsoft.Data.Extensions;
using Sunlight.Api.Models;
using Sunlight.Api.Security;
using Sunlight.Silverlight.Dcs.Web;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Objects;
using System.Linq;

namespace Sunlight.Api.BLL
{
    public partial class DcsServiceImp
    {
        private void InsertPartsInboundCheckBillValidate(PartsInboundCheckBill partsInboundCheckBill)
        {
            if (string.IsNullOrWhiteSpace(partsInboundCheckBill.Code) || partsInboundCheckBill.Code == Sunlight.Silverlight.Security.Web.GlobalVar.ASSIGNED_BY_SERVER)
                partsInboundCheckBill.Code = CodeGenerator.Generate("PartsInboundCheckBill", partsInboundCheckBill.StorageCompanyCode);
            var userInfo = FormsAuth.GetUserData();
            partsInboundCheckBill.CreatorId = userInfo.UserId;
            partsInboundCheckBill.CreatorName = userInfo.UserName;
            partsInboundCheckBill.CreateTime = DateTime.Now;
        }
        private PartsLogisticBatch PartsLogisticBatch
        {
            get;
            set;
        }

        public bool 收货确认(List<ForReceivingOrderModel> receivingOrders)
        {
            var userInfo = FormsAuth.GetUserData();
            var partsInboundPlanId = receivingOrders[0].Id;
            var partsInboundPlan = ObjectContext.PartsInboundPlans.Include("PartsSalesCategory").SingleOrDefault(e => e.Id == partsInboundPlanId && (e.Status == (int)DcsPartsInboundPlanStatus.新建 || e.Status == (int)DcsPartsInboundPlanStatus.部分检验));
            if (partsInboundPlan == null)
                throw new ValidationException("只能为处于“新建”或 “部分检验” 状态下的入库计划单生成配件入库单");


            if (receivingOrders.GroupBy(v => v.PartsCode).Any(v => v.Count() > 1))
                throw new ValidationException("收货单清单，配件不允许重复！");

            var partsInboundPlanDetailSparePartIds = receivingOrders.Select(r => r.PartsId).ToList();
            var partsInboundPlanDetails = partsInboundPlan.PartsInboundPlanDetails.Where(r => r.PartsInboundPlanId == partsInboundPlan.Id && partsInboundPlanDetailSparePartIds.Contains(r.SparePartId));

            var warehouseAreaCategoryIds = ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.检验区).Select(v => v.Id).ToArray();
            if (warehouseAreaCategoryIds.Length == 0)
                return false;
            //新增入库检验单
            var partsInboundCheckBill = new PartsInboundCheckBill();
            partsInboundCheckBill.Code = CodeGenerator.Generate("PartsInboundCheckBill", userInfo.EnterpriseCode);
            partsInboundCheckBill.PartsInboundPlanId = partsInboundPlan.Id;
            partsInboundCheckBill.WarehouseId = partsInboundPlan.WarehouseId;
            partsInboundCheckBill.WarehouseCode = partsInboundPlan.WarehouseCode;
            partsInboundCheckBill.WarehouseName = partsInboundPlan.WarehouseName;
            partsInboundCheckBill.StorageCompanyId = partsInboundPlan.StorageCompanyId;
            partsInboundCheckBill.StorageCompanyCode = partsInboundPlan.StorageCompanyCode;
            partsInboundCheckBill.StorageCompanyName = partsInboundPlan.StorageCompanyName;
            partsInboundCheckBill.StorageCompanyType = partsInboundPlan.StorageCompanyType;
            partsInboundCheckBill.PartsSalesCategoryId = partsInboundPlan.PartsSalesCategoryId;
            partsInboundCheckBill.BranchId = partsInboundPlan.BranchId;//partsInboundPlan.BranchId;
            partsInboundCheckBill.BranchCode = partsInboundPlan.BranchCode;//partsInboundPlan.BranchCode;
            partsInboundCheckBill.BranchName = partsInboundPlan.BranchName;//partsInboundPlan.BranchName;
            partsInboundCheckBill.CounterpartCompanyId = partsInboundPlan.CounterpartCompanyId;
            partsInboundCheckBill.CounterpartCompanyCode = partsInboundPlan.CounterpartCompanyCode;
            partsInboundCheckBill.CounterpartCompanyName = partsInboundPlan.CounterpartCompanyName;
            partsInboundCheckBill.InboundType = partsInboundPlan.InboundType;
            partsInboundCheckBill.CustomerAccountId = partsInboundPlan.CustomerAccountId;
            partsInboundCheckBill.OriginalRequirementBillId = partsInboundPlan.OriginalRequirementBillId;
            partsInboundCheckBill.OriginalRequirementBillType = partsInboundPlan.OriginalRequirementBillType;
            partsInboundCheckBill.OriginalRequirementBillCode = partsInboundPlan.OriginalRequirementBillCode;
            partsInboundCheckBill.Status = (int)DcsPartsInboundCheckBillStatus.新建;
            partsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
            partsInboundCheckBill.CreatorId = userInfo.UserId;
            partsInboundCheckBill.CreateTime = DateTime.Now;
            partsInboundCheckBill.CreatorName = userInfo.UserName;
            partsInboundCheckBill.GPMSPurOrderCode = partsInboundPlan.GPMSPurOrderCode;
            partsInboundCheckBill.PartsSalesCategoryId = partsInboundPlan.PartsSalesCategoryId;
            partsInboundCheckBill.PartsSalesCategory = partsInboundPlan.PartsSalesCategory;
            partsInboundCheckBill.SAPPurchasePlanCode = partsInboundPlan.SAPPurchasePlanCode;//SAP采购计划单号
            partsInboundCheckBill.ERPSourceOrderCode = partsInboundPlan.ERPSourceOrderCode;

            this.PartsLogisticBatch = partsInboundPlan.PartsLogisticBatchBillDetails.Select(v => v.PartsLogisticBatch).FirstOrDefault();

            var warehouseAreas = ObjectContext.WarehouseAreas.Where(r => warehouseAreaCategoryIds.Contains(r.AreaCategoryId.Value) && r.WarehouseId == partsInboundCheckBill.WarehouseId && r.Status == (int)DcsBaseDataStatus.有效 && r.AreaKind == (int)DcsAreaKind.库位).ToArray();
            foreach (var entityDetail in partsInboundPlanDetails)
            {
                var receivingOrder = receivingOrders.FirstOrDefault(o => o.Id == entityDetail.PartsInboundPlanId && o.PartsId == entityDetail.SparePartId);
                partsInboundCheckBill.PartsInboundCheckBillDetails.Add(new PartsInboundCheckBillDetail
                {
                    PartsInboundCheckBill = partsInboundCheckBill,
                    SparePartId = entityDetail.SparePartId,
                    SparePartCode = entityDetail.SparePartCode,
                    SparePartName = entityDetail.SparePartName,
                    InspectedQuantity = (receivingOrder.ReceiveAmount ?? 0),
                    SettlementPrice = entityDetail.Price,
                    WarehouseAreaId = warehouseAreas[0].Id,
                    WarehouseAreaCode = warehouseAreas[0].Code,
                    POCode = entityDetail.POCode,
                    IsPacking = receivingOrder.IsPacking,
                    CostPrice = entityDetail.Price
                });
            }

            InsertPartsInboundCheckBillValidate(partsInboundCheckBill);
            ObjectContext.PartsInboundCheckBills.AddObject(partsInboundCheckBill);

            //锁定库存和企业成本
            var partsStocks = GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => partsInboundPlanDetailSparePartIds.Contains(r.PartId) && warehouseAreaCategoryIds.Contains(r.WarehouseAreaCategoryId.Value) && r.WarehouseId == partsInboundCheckBill.WarehouseId)).ToList();
            var enterprisePartsCosts = GetEnterprisePartsCostsWithLock(ObjectContext.EnterprisePartsCosts.Where(r => r.OwnerCompanyId == partsInboundPlan.StorageCompanyId && partsInboundPlanDetailSparePartIds.Contains(r.SparePartId))).ToList();

            var company = ObjectContext.Companies.Single(r => r.Id == partsInboundCheckBill.StorageCompanyId);

            if (partsInboundPlan.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件采购订单)
            {
                var partsPurchaseOrder = ObjectContext.PartsPurchaseOrders.FirstOrDefault(r => r.Id == partsInboundPlan.OriginalRequirementBillId);
                if (partsPurchaseOrder != null && !string.IsNullOrEmpty(partsPurchaseOrder.PurOrderCode))
                    partsInboundCheckBill.PurOrderCode = partsPurchaseOrder.PurOrderCode;
            }

            if (partsInboundPlan.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件调拨单)
            {
                var partsTransferOrder = ObjectContext.PartsTransferOrders.FirstOrDefault(r => r.Id == partsInboundPlan.OriginalRequirementBillId);
                if (partsTransferOrder != null && !string.IsNullOrEmpty(partsTransferOrder.PurOrderCode))
                    partsInboundCheckBill.PurOrderCode = partsTransferOrder.PurOrderCode;
            }

            if (company.Type == (int)DcsCompanyType.分公司 && partsInboundPlan.InboundType == (int)DcsPartsInboundType.销售退货)
            {
                //回写索赔单状态
                var partsClaimOrderNew = ObjectContext.PartsClaimOrderNews.Where(r => r.Id == partsInboundPlan.SourceId).FirstOrDefault();
                if (partsClaimOrderNew != null && partsClaimOrderNew.Status == (int)DcsPartsClaimBillStatusNew.区域已发运)
                {
                    partsClaimOrderNew.Status = (int)DcsPartsClaimBillStatusNew.入库完成;
                    partsClaimOrderNew.ModifierId = userInfo.UserId;
                    partsClaimOrderNew.ModifierName = userInfo.UserName;
                    partsClaimOrderNew.ModifyTime = DateTime.Now;
                }
            }

            foreach (var partsInboundCheckBillDetail in partsInboundCheckBill.PartsInboundCheckBillDetails)
            {
                //更新配件入库计划清单
                var partsInboundPlanDetail = partsInboundPlanDetails.SingleOrDefault(v => v.SparePartId == partsInboundCheckBillDetail.SparePartId && v.PlannedAmount != (v.InspectedQuantity ?? 0) && v.Price == partsInboundCheckBillDetail.CostPrice);
                if (partsInboundPlanDetail == null)
                    throw new ValidationException(string.Format("对应的入库计划单上，不存在编号为“{0}”的配件", partsInboundCheckBillDetail.SparePartCode));
                if (!partsInboundPlanDetail.InspectedQuantity.HasValue)
                {
                    partsInboundPlanDetail.InspectedQuantity = 0;
                }

                if (partsInboundPlanDetail.InspectedQuantity + partsInboundCheckBillDetail.InspectedQuantity > partsInboundPlanDetail.PlannedAmount)
                {
                    throw new ValidationException("检验量已处理过");
                }
                partsInboundPlanDetail.InspectedQuantity += partsInboundCheckBillDetail.InspectedQuantity;

                //重新计算企业配件成本价
                var enterprisePartsCost = enterprisePartsCosts == null ? null : enterprisePartsCosts.FirstOrDefault(r => r.SparePartId == partsInboundCheckBillDetail.SparePartId);
                if (enterprisePartsCost != null)
                {
                    enterprisePartsCost.Quantity += partsInboundCheckBillDetail.InspectedQuantity;
                    if (partsInboundPlan.InboundType == (int)DcsPartsInboundType.配件采购)
                        enterprisePartsCost.CostAmount += partsInboundCheckBillDetail.InspectedQuantity * partsInboundCheckBillDetail.SettlementPrice;
                    else
                        enterprisePartsCost.CostAmount += partsInboundCheckBillDetail.InspectedQuantity * enterprisePartsCost.CostPrice;
                    enterprisePartsCost.CostPrice = Math.Round(enterprisePartsCost.CostAmount / enterprisePartsCost.Quantity, 2);
                }
                else
                {
                    if (partsInboundPlan.InboundType != (int)DcsPartsInboundType.配件采购)
                        throw new ValidationException(string.Format("编号为{0}的配件，没有企业配件成本不允许入库！", partsInboundCheckBillDetail.SparePartCode));
                    var epc = new EnterprisePartsCost
                    {
                        OwnerCompanyId = partsInboundCheckBill.StorageCompanyId,
                        PartsSalesCategoryId = partsInboundCheckBill.PartsSalesCategoryId,
                        SparePartId = partsInboundCheckBillDetail.SparePartId,
                        Quantity = partsInboundCheckBillDetail.InspectedQuantity,
                        CostAmount = partsInboundCheckBillDetail.InspectedQuantity * partsInboundCheckBillDetail.SettlementPrice,
                        CostPrice = Math.Round(partsInboundCheckBillDetail.SettlementPrice, 2),
                        CreatorId = userInfo.UserId,
                        CreatorName = userInfo.UserName,
                        CreateTime = DateTime.Now,
                    };
                    if (partsInboundPlan.InboundType == (int)DcsPartsInboundType.配件采购)
                        ObjectContext.EnterprisePartsCosts.AddObject(epc);
                }
                //更新库存
                var partsStock = partsStocks.FirstOrDefault(r => r.PartId == partsInboundCheckBillDetail.SparePartId && r.WarehouseAreaId == partsInboundCheckBillDetail.WarehouseAreaId);
                var warehouseArea = warehouseAreas.FirstOrDefault(o => o.Id == partsInboundCheckBillDetail.WarehouseAreaId);
                if (partsStock == null)
                {
                    //新增配件库存
                    if (warehouseArea == null)
                    {
                        throw new ValidationException("新增配件库存时没有找到对应的库位");
                    }
                    var ps = new PartsStock
                    {
                        WarehouseId = partsInboundCheckBill.WarehouseId,
                        StorageCompanyId = partsInboundCheckBill.StorageCompanyId,
                        StorageCompanyType = partsInboundCheckBill.StorageCompanyType,
                        BranchId = partsInboundCheckBill.BranchId,
                        WarehouseAreaId = warehouseArea.Id,
                        PartId = partsInboundCheckBillDetail.SparePartId,
                        Quantity = partsInboundCheckBillDetail.InspectedQuantity,
                        Remark = partsInboundCheckBillDetail.Remark,
                        WarehouseAreaCategoryId = warehouseArea.AreaCategoryId
                    };
                    if ((partsInboundCheckBillDetail.IsPacking ?? false) == true)
                        ps.LockedQty = partsInboundCheckBillDetail.InspectedQuantity;
                    ObjectContext.PartsStocks.AddObject(ps);
                    partsStocks.Add(ps);
                }
                else
                {
                    //更新配件库存
                    partsStock.Quantity = partsStock.Quantity + partsInboundCheckBillDetail.InspectedQuantity;
                    if ((partsInboundCheckBillDetail.IsPacking ?? false) == true)
                        partsStock.LockedQty = (partsStock.LockedQty.HasValue ? partsStock.LockedQty.Value : 0) + partsInboundCheckBillDetail.InspectedQuantity;
                    partsStock.ModifyTime = DateTime.Now;
                    partsStock.ModifierId = userInfo.UserId;
                    partsStock.ModifierName = userInfo.UserName;
                }

                if (company.Type == (int)DcsCompanyType.分公司)
                {
                    if (partsInboundCheckBill.InboundType == (int)DcsPartsInboundType.配件调拨)
                    {
                        var tmpPartsShippingOrderRefOutBill = (from b in ObjectContext.PartsOutboundBills
                                                               join c in ObjectContext.PartsOutboundBillDetails on b.Id equals c.PartsOutboundBillId
                                                               where b.PartsShippingOrderId == partsInboundPlan.SourceId
                                                               select new
                                                               {
                                                                   SparePartId = c.SparePartId,
                                                                   CostPrice = c.CostPrice
                                                               }).ToArray();
                        if (tmpPartsShippingOrderRefOutBill == null)
                            throw new ValidationException("配件入库计划源单据对应的配件出库单不存在");
                        partsInboundCheckBillDetail.CostPrice = tmpPartsShippingOrderRefOutBill.FirstOrDefault(r => r.SparePartId == partsInboundCheckBillDetail.SparePartId).CostPrice;
                    }
                    else if (partsInboundCheckBill.InboundType == (int)DcsPartsInboundType.配件采购)
                    {
                        partsInboundCheckBillDetail.CostPrice = partsInboundPlanDetail.Price;
                    }
                    else
                    {
                        partsInboundCheckBillDetail.CostPrice = enterprisePartsCost.CostPrice;
                    }
                    if (partsInboundCheckBillDetail.CostPrice == 0)
                    {
                        throw new ValidationException(string.Format("编号为“{0}”的备件的成本价等于0",
                            partsInboundCheckBillDetail.SparePartCode));
                    }
                }

                //批量更新或者新增配件库存
                var checkPartIds = partsStocks.Select(r => r.PartId);
                var checkWarehouseAreaIds = partsStocks.Select(r => r.WarehouseAreaId);
                var checkPartsStocks = ObjectContext.PartsStocks.Where(r => checkPartIds.Contains(r.PartId) && checkWarehouseAreaIds.Contains(r.WarehouseAreaId)).SetMergeOption(MergeOption.NoTracking).ToArray();
                foreach (var item in partsStocks)
                {
                    //数量要大于0
                    if (item.Quantity < 0)
                    {
                        throw new ValidationException("数量必须大于0");
                    }
                    //新增修改的唯一性校验
                    switch (item.EntityState)
                    {
                        case EntityState.Modified:
                            var checkExists = checkPartsStocks.FirstOrDefault(r => r.Id == item.Id);
                            if (checkExists == null)
                            {
                                throw new ValidationException("需要修改的对象不存在");
                            }
                            var checkOnlyOneForUpdate = checkPartsStocks.SingleOrDefault(r => r.Id != item.Id && r.PartId == item.PartId && r.WarehouseAreaId == item.WarehouseAreaId);
                            if (checkOnlyOneForUpdate != null)
                            {
                                throw new ValidationException("配件已经存在于指定的库位");
                            }
                            item.ModifierId = userInfo.UserId;
                            item.ModifierName = userInfo.UserName;
                            item.ModifyTime = DateTime.Now;
                            break;
                        case EntityState.Added:
                            var checkOnlyOneForInsert = checkPartsStocks.SingleOrDefault(r => r.PartId == item.PartId && r.WarehouseAreaId == item.WarehouseAreaId);
                            if (checkOnlyOneForInsert != null)
                            {
                                throw new ValidationException("此库位下已存在该配件对应的库存记录");
                            }
                            item.CreatorId = userInfo.UserId;
                            item.CreatorName = userInfo.UserName;
                            item.CreateTime = DateTime.Now;
                            break;
                    }
                }
            }

            if (partsInboundCheckBill.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售退货单)
            {
                var partsSaleReturnBill = ObjectContext.PartsSalesReturnBills.Where(r => r.Id == partsInboundCheckBill.OriginalRequirementBillId).FirstOrDefault();
                if (partsSaleReturnBill != null)
                {
                    if (partsSaleReturnBill.IsBarter == true)
                        partsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.已结算;
                    else
                        partsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                }
            }

            var warehouse = ObjectContext.Warehouses.FirstOrDefault(o => o.Id == partsInboundCheckBill.WarehouseId && o.Status == (int)DcsBaseDataStatus.有效);
            if (company.Type == (int)DcsCompanyType.分公司)
            {
                var batchNumber = CodeGenerator.Generate("PartsBatchRecord", partsInboundPlan.WarehouseCode);
                foreach (var partsInboundCheckBillDetail in partsInboundCheckBill.PartsInboundCheckBillDetails)
                {
                    partsInboundCheckBillDetail.BatchNumber = batchNumber;
                    if ((partsInboundCheckBillDetail.IsPacking ?? false) == true && warehouse.Name != "马钢库")
                    {
                        var partsPacking = new PackingTask
                        {
                            Code = CodeGenerator.Generate("PartsPacking", userInfo.EnterpriseCode),
                            PartsInboundPlanId = partsInboundPlan.Id,
                            PartsInboundPlanCode = partsInboundPlan.Code,
                            PartsInboundCheckBillId = partsInboundCheckBill.Id,
                            PartsInboundCheckBillCode = partsInboundCheckBill.Code,
                            SparePartId = partsInboundCheckBillDetail.SparePartId,
                            SparePartCode = partsInboundCheckBillDetail.SparePartCode,
                            SparePartName = partsInboundCheckBillDetail.SparePartName,
                            PlanQty = partsInboundCheckBillDetail.InspectedQuantity,
                            SourceCode = partsInboundPlan.OriginalRequirementBillCode,
                            Status = (int)DcsPackingTaskStatusd.新增,
                            CounterpartCompanyCode = partsInboundCheckBill.CounterpartCompanyCode,
                            CounterpartCompanyName = partsInboundCheckBill.CounterpartCompanyName,
                            ExpectedPlaceDate = partsInboundPlan.PlanDeliveryTime,
                            CreatorId = userInfo.UserId,
                            CreatorName = userInfo.UserName,
                            CreateTime = DateTime.Now,
                            WarehouseId = partsInboundCheckBill.WarehouseId,
                            WarehouseCode = partsInboundCheckBill.WarehouseCode,
                            WarehouseName = partsInboundCheckBill.WarehouseName,
                        };
                        if (partsInboundCheckBill.InboundType == (int)DcsPartsInboundType.配件采购)
                        {
                            partsPacking.BatchNumber = batchNumber;
                        }
                        ObjectContext.PackingTasks.AddObject(partsPacking);
                    }
                    else
                    {
                        var partsShelvesTask = new PartsShelvesTask
                        {
                            Code = CodeGenerator.Generate("PartsShelvesTask", userInfo.EnterpriseCode),
                            PartsInboundPlanId = partsInboundPlan.Id,
                            PartsInboundPlanCode = partsInboundPlan.Code,
                            PartsInboundCheckBillId = partsInboundCheckBill.Id,
                            PartsInboundCheckBillCode = partsInboundCheckBill.Code,
                            WarehouseId = partsInboundCheckBill.WarehouseId,
                            WarehouseCode = partsInboundCheckBill.WarehouseCode,
                            WarehouseName = partsInboundCheckBill.WarehouseName,
                            SparePartId = partsInboundCheckBillDetail.SparePartId,
                            //SparePartCode = partsInboundCheckBillDetail.SparePartCode,
                            //SparePartName = partsInboundCheckBillDetail.SparePartName,
                            PlannedAmount = partsInboundCheckBillDetail.InspectedQuantity,
                            SourceBillCode = partsInboundPlan.OriginalRequirementBillCode,
                            Status = (int)DcsShelvesTaskStatus.新增,
                            CreatorId = userInfo.UserId,
                            CreatorName = userInfo.UserName,
                            CreateTime = DateTime.Now,
                        };
                        if (partsInboundCheckBill.InboundType == (int)DcsPartsInboundType.配件采购)
                        {
                            partsShelvesTask.BatchNumber = batchNumber;
                        }
                        ObjectContext.PartsShelvesTasks.AddObject(partsShelvesTask);
                    }
                    var partsInboundPlanDetail = partsInboundPlanDetails.FirstOrDefault(o => o.PartsInboundPlanId == partsInboundCheckBill.PartsInboundPlanId && partsInboundCheckBillDetail.SparePartId == o.SparePartId);
                    partsInboundPlanDetail.BatchNumber = batchNumber;
                }
            }

            //更新供应商发运单
            var supplierShippingOrder = ObjectContext.SupplierShippingOrders.Where(t => t.Code == partsInboundPlan.SourceCode && t.Id == partsInboundPlan.SourceId).FirstOrDefault();
            if (supplierShippingOrder != null && supplierShippingOrder.LogisticArrivalDate == null)
            {
                supplierShippingOrder.LogisticArrivalDate = DateTime.Now;
                supplierShippingOrder.ArriveMethod = (int)DCSSupplierShippingOrderArriveMethod.默认到货;
                supplierShippingOrder.ComfirmStatus = (int)DCSSupplierShippingOrderComfirmStatus.入库确认;
            }
            if (supplierShippingOrder != null && supplierShippingOrder.InboundTime == null)
            {
                supplierShippingOrder.InboundTime = DateTime.Now;
            }
            //收货管理，当入库类型=销售退货、调拨时，
            //如果选择是否包装=否，默认将原本出库的追溯信息保留，原本的入库追溯信息变更为在库，可再次出库（采用原来的SIH标签码）
            if (partsInboundCheckBill.InboundType == (int)DcsPartsInboundType.销售退货 || partsInboundCheckBill.InboundType == (int)DcsPartsInboundType.配件调拨)
            {
                var accurateTraces = ObjectContext.AccurateTraces.Where(t => t.SourceBillId == partsInboundCheckBill.PartsInboundPlanId && t.Type == (int)DCSAccurateTraceType.入库).ToArray();
                if (accurateTraces.Count() > 0)
                {
                    var traceCodes = accurateTraces.Select(r => r.TraceCode).ToArray();
                    var outAccurateTraces = ObjectContext.AccurateTraces.Where(t => t.Type == (int)DCSAccurateTraceType.出库 && traceCodes.Contains(t.TraceCode) && t.Status == (int)DCSAccurateTraceStatus.有效).ToArray();
                    foreach (var partsInboundCheckBillDetail in partsInboundCheckBill.PartsInboundCheckBillDetails)
                    {
                        var inboundQty = partsInboundCheckBillDetail.InspectedQuantity;
                        var upQty = 0;
                        if (!partsInboundCheckBillDetail.IsPacking.Value)
                        {
                            var inAccurateTraces = accurateTraces.Where(t => t.PartId == partsInboundCheckBillDetail.SparePartId).ToArray();
                            foreach (var item in inAccurateTraces)
                            {
                                if (inboundQty == upQty)
                                    continue;
                                var outTrace = outAccurateTraces.FirstOrDefault(t => t.TraceCode == item.TraceCode && t.PartId == item.PartId);
                                if (outTrace != null)
                                {
                                    item.SIHLabelCode = outTrace.SIHLabelCode;
                                    item.BoxCode = outTrace.BoxCode;
                                    item.InQty = partsInboundCheckBillDetail.InspectedQuantity;
                                    item.ModifierId = userInfo.UserId;
                                    item.ModifierName = userInfo.UserName;
                                    item.ModifyTime = DateTime.Now;
                                }
                                upQty++;
                            }
                        }

                    }

                }
            }

            this.ObjectContext.SaveChanges();
            更新入库计划状态(partsInboundPlan);
            return true;
        }
    }
}