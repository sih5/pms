﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Sunlight.Api.Models;
using Sunlight.Api.Security;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Api.BLL {
    public partial class DcsServiceImp {

        /// <summary>
        /// 查询未收货的收货单
        /// </summary>
        /// <param name="code">入库计划单号</param>
        /// <param name="warehouseId">当前登陆仓库id</param>
        /// <returns></returns>
        public List<ForReceivingOrderModel> GetReceiptDetail(string code, int warehouseId) {
            var currentUser = FormsAuth.GetUserData();
            var result = (from inPlan in this.ObjectContext.PartsInboundPlans.Where(o => o.Code.IndexOf(code) >= 0 && o.WarehouseId == warehouseId && (o.Status == (int)DcsPartsInboundPlanStatus.新建 || o.Status == (int)DcsPartsInboundPlanStatus.部分检验))
                          join inPlanDetail in this.ObjectContext.PartsInboundPlanDetails on inPlan.Id equals inPlanDetail.PartsInboundPlanId
                          join psr in this.ObjectContext.PartsSupplierRelations on new {
                              SupplierId = inPlan.CounterpartCompanyId,
                              SparePartId = inPlanDetail.SparePartId,
                              Status = (int)DcsBaseDataStatus.有效
                          } equals new {
                              SupplierId = psr.SupplierId,
                              SparePartId = psr.PartId,
                              Status = psr.Status
                          } into temp
                          from t in temp.DefaultIfEmpty()
                          where inPlanDetail.PlannedAmount > (inPlanDetail.InspectedQuantity ?? 0)
                          select new ForReceivingOrderModel {
                              Id = inPlan.Id,
                              Code = inPlan.Code,
                              Status = inPlan.Status,
                              InboundType = inPlan.InboundType,
                              PartsId = inPlanDetail.SparePartId,
                              PartsCode = inPlanDetail.SparePartCode,
                              SupplierPartCode = t.SupplierPartCode,
                              PartsName = inPlanDetail.SparePartName,
                              PlannedAmount = inPlanDetail.PlannedAmount,
                              ReceivedAmount = inPlanDetail.InspectedQuantity,
                              Price = inPlanDetail.Price
                          }).ToList();
            return result;
        }

        public void 更新入库计划状态(PartsInboundPlan partsInboundPlan) {
            var partsInboundPlanDetails = ObjectContext.PartsInboundPlanDetails.Where(r => r.PartsInboundPlanId == partsInboundPlan.Id).ToArray();
            var plannedAmount = partsInboundPlanDetails.Sum(o => o.PlannedAmount);
            var inspectedQuantity = partsInboundPlanDetails.Sum(o => o.InspectedQuantity);

            // packingPart 部分包装的包装任务单   packingComplete包装完成的包装任务单
            var packingTasks = ObjectContext.PackingTasks.Where(r => r.PartsInboundPlanId == partsInboundPlan.Id).ToArray();
            var planQty = packingTasks.Sum(o => o.PlanQty);
            var packingQty = packingTasks.Sum(o => o.PackingQty);

            var partsShelvesTasks = ObjectContext.PartsShelvesTasks.Where(r => r.PartsInboundPlanId == partsInboundPlan.Id).ToArray();
            var plannedAmountShelves = partsShelvesTasks.Sum(o => o.PlannedAmount);
            var shelvesAmount = partsShelvesTasks.Sum(o => o.ShelvesAmount);

            if(plannedAmount == inspectedQuantity && plannedAmount == (shelvesAmount ?? 0))
                partsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.上架完成;
            else if(plannedAmount == inspectedQuantity && plannedAmount > (shelvesAmount ?? 0) && (shelvesAmount ?? 0) > 0 && planQty == (packingQty ?? 0))
                partsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.部分上架;
            else if(plannedAmount == inspectedQuantity && plannedAmount == (packingQty ?? 0))
                partsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.包装完成;
            else if(plannedAmount == inspectedQuantity && plannedAmount > (packingQty ?? 0) && (packingQty ?? 0) > 0)
                partsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.部分包装;
            else if(plannedAmount > (inspectedQuantity ?? 0) && (inspectedQuantity ?? 0) > 0)
                partsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.部分检验;
            else if(plannedAmount == (inspectedQuantity ?? 0))
                partsInboundPlan.Status = (int)DcsPartsInboundPlanStatus.检验完成;

            UpdatePartsInboundPlanValidate(partsInboundPlan);
            this.ObjectContext.SaveChanges();
        }

        internal void UpdatePartsInboundPlanValidate(PartsInboundPlan partsInboundPlan) {
            var userInfo = FormsAuth.GetUserData();
            partsInboundPlan.ModifierId = userInfo.UserId;
            partsInboundPlan.ModifierName = userInfo.UserName;
            partsInboundPlan.ModifyTime = DateTime.Now;
        }

        internal void InsertPartsInboundPlanValidate(PartsInboundPlan partsInboundPlan) {
            if(string.IsNullOrWhiteSpace(partsInboundPlan.Code) || partsInboundPlan.Code == "<尚未生成>")
                partsInboundPlan.Code = CodeGenerator.Generate("PartsInboundPlan", partsInboundPlan.StorageCompanyCode);
            var userInfo = FormsAuth.GetUserData();
            partsInboundPlan.CreatorId = userInfo.UserId;
            partsInboundPlan.CreatorName = userInfo.UserName;
            partsInboundPlan.CreateTime = DateTime.Now;
        }
    }
}