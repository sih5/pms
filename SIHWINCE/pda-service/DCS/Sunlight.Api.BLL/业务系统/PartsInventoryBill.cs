﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Sunlight.Api.Models;
using Sunlight.Api.Security;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Api.BLL {
    public partial class DcsServiceImp {
        /// <summary>
        /// 根据单号查询未完成的盘点单
        /// </summary>
        /// <param name="code"></param>
        /// <param name="warehouseId"></param>
        /// <returns></returns>
        public List<PartsInventoryBillModel> 根据单号查询未完成的盘点单(string code, int warehouseId) {
            var userInfo = FormsAuth.GetUserData();
            var result = (from pib in ObjectContext.PartsInventoryBills.Where(o => o.Code.IndexOf(code) >= 0 && o.WarehouseId == warehouseId && (o.Status == (int)DcsPartsInventoryBillStatus.新建 || o.Status == (int)DcsPartsInventoryBillStatus.已结果录入))
                          join pid in ObjectContext.PartsInventoryDetails on pib.Id equals pid.PartsInventoryBillId
                          join sp in ObjectContext.SpareParts on pid.SparePartId equals sp.Id
                          select new PartsInventoryBillModel {
                              Id = pib.Id,
                              Code = pib.Code,
                              InitiatorName = pib.InitiatorName,
                              Status = pib.Status,
                              DetailId = pid.Id,
                              SparePartId = pid.SparePartId,
                              SparePartCode = pid.SparePartCode,
                              SparePartName = pid.SparePartName,
                              WarehouseAreaId = pid.WarehouseAreaId,
                              WarehouseAreaCode = pid.WarehouseAreaCode,
                              CurrentStorage = 0,
                              AlreadyStorageAfterInventory = pid.CostPrice == null ? null : (int?)pid.StorageAfterInventory,
                              StorageAfterInventory = null,
                              StorageDifference = pid.StorageDifference,
                              TraceProperty = sp.TraceProperty
                          }).ToList();
            return result;
        }

        /// <summary>
        /// 盘点结果录入
        /// </summary>
        /// <param name="partsInventoryBillModels"></param>
        /// <returns></returns>
        public bool 盘点结果录入(List<PartsInventoryBillModel> partsInventoryBillModels) {
            var userInfo = FormsAuth.GetUserData();
            using(var dcsEntities = new DcsEntities()) {
                var partsInventoryBillId = partsInventoryBillModels != null && partsInventoryBillModels.Count > 0 ? partsInventoryBillModels[0].Id : 0;
                var partsInventoryBill = dcsEntities.PartsInventoryBills.FirstOrDefault(r => (r.Status == (int)DcsPartsInventoryBillStatus.新建 || r.Status == (int)DcsPartsInventoryBillStatus.已结果录入) && r.Id == partsInventoryBillId);
                if(partsInventoryBill == null)
                    throw new ValidationException("只能录入处于“新建”或“已结果录入”状态的配件盘点单");
                partsInventoryBill.Status = (int)DcsPartsInventoryBillStatus.已结果录入;
                partsInventoryBill.ResultInputOperatorId = userInfo.UserId;
                partsInventoryBill.ResultInputOperatorName = userInfo.UserName;
                partsInventoryBill.ResultInputTime = System.DateTime.Now;
                //获取当前仓库的销售组织.配件销售类型Id
                var salesunitquery = from a in ObjectContext.SalesUnits.Where(r => r.Status == (int)DcsMasterDataStatus.有效)
                                     join b in ObjectContext.SalesUnitAffiWarehouses on a.Id equals b.SalesUnitId
                                     join c in ObjectContext.Warehouses.Where(r => r.Id == partsInventoryBill.WarehouseId) on b.WarehouseId equals c.Id
                                     select a;
                var salesUnit = salesunitquery.FirstOrDefault();
                if(salesUnit == null) {
                    throw new ValidationException("未获取到销售组织");
                }
                if(salesUnit.PartsSalesCategoryId <= 0) {
                    throw new ValidationException("销售组织中记录的品牌不正确");
                }

                foreach(var item in partsInventoryBillModels.Where(o => string.IsNullOrEmpty(o.Code)).ToList()) {
                    if(ObjectContext.PartsInventoryDetails.Any(o => (o.PartsInventoryBill.Status != (int)DcsPartsInventoryBillStatus.已库存覆盖 && o.PartsInventoryBill.Status != (int)DcsPartsInventoryBillStatus.作废)
                        && o.SparePartCode == item.SparePartCode && o.WarehouseAreaCode == item.WarehouseAreaCode)) {
                        throw new ValidationException(string.Format("配件{0}货位{1},存在未完成的盘点计划，无法新增盘点清单", item.SparePartCode, item.WarehouseAreaCode));
                    }
                }

                //获取差异录入，配件、货位得详细信息
                var warehouseAreaCodes = partsInventoryBillModels.Where(o => string.IsNullOrEmpty(o.Code)).Select(o => o.WarehouseAreaCode).ToList();
                var warehouseAreas = ObjectContext.WarehouseAreas.Include("WarehouseAreaCategory").Where(o => warehouseAreaCodes.Contains(o.Code) && o.WarehouseId == partsInventoryBill.WarehouseId
                && o.Status == (int)DcsBaseDataStatus.有效).ToList();

                var invalidWarehouseAreaCodes = warehouseAreaCodes.Where(o => !warehouseAreas.Any(r => r.Code == o)).ToList();
                if(invalidWarehouseAreaCodes.Count > 0) {
                    throw new ValidationException(string.Format("货位{0},不是当前盘点仓库得货位，无法新增盘点清单。", string.Join(",", invalidWarehouseAreaCodes)));
                }

                var errorWarehouseAreaCodes = warehouseAreas.Where(o => o.WarehouseAreaCategory.Category != partsInventoryBill.WarehouseAreaCategory).Select(o => o.Code).ToList();
                if(errorWarehouseAreaCodes.Count > 0)
                    throw new ValidationException(string.Format("货位{0},库区用途与盘点单不一致，无法新增盘点清单。", string.Join(",", errorWarehouseAreaCodes)));

                var sparePartCodes = partsInventoryBillModels.Where(o => string.IsNullOrEmpty(o.Code)).Select(o => o.SparePartCode).ToList();
                var parts = ObjectContext.SpareParts.Where(o => sparePartCodes.Contains(o.Code)).ToList();
                var newPartsIds = parts.Select(o => (int?)o.Id).Distinct().ToList();
                var warehouseAreaIds = warehouseAreas.Select(o => o.Id).Distinct().ToList();

                var partsIds = partsInventoryBillModels.Where(o => o.SparePartId.HasValue).Select(o => o.SparePartId).ToList();
                if(parts.Count > 0)
                    partsIds.AddRange(newPartsIds);
                var enterprisePartsCosts = ObjectContext.EnterprisePartsCosts.Where(r => r.PartsSalesCategoryId == salesUnit.PartsSalesCategoryId && partsIds.Contains(r.SparePartId) && r.OwnerCompanyId == userInfo.EnterpriseId);
                //获取新增盘点单的库存信息
                var partsStocks = ObjectContext.PartsStocks.Where(o => o.WarehouseId == partsInventoryBill.WarehouseId && newPartsIds.Contains(o.PartId) && warehouseAreaIds.Contains(o.WarehouseAreaId));

                //获取追溯码信息表，取当前入库数量保存
                var sihCodes = partsInventoryBillModels.SelectMany(o => o.SIHCodes).ToList();
                var accurateTraces = ObjectContext.AccurateTraces.Where(t => t.Type == (int)DCSAccurateTraceType.入库 && t.Status == (int)DCSAccurateTraceStatus.有效 && t.CompanyId == partsInventoryBill.StorageCompanyId
                 && (sihCodes.Contains(t.BoxCode) || sihCodes.Contains(t.SIHLabelCode)) && ObjectContext.PartsInboundPlans.Any(y => y.Id == t.SourceBillId && y.WarehouseId == partsInventoryBill.WarehouseId)).ToArray();

                var traceTempTypes = new List<TraceTempType>();
                //获取清单中配件的计划价
                decimal amountDifference = 0;
                foreach(var partsInventoryBillModel in partsInventoryBillModels) {
                    var pibdetail = new PartsInventoryDetail();
                    if(string.IsNullOrEmpty(partsInventoryBillModel.Code)) {
                        var part = parts.FirstOrDefault(o => o.Code == partsInventoryBillModel.SparePartCode);
                        if(part == null)
                            throw new ValidationException(string.Format("不存在配件{0}信息，无法新增盘点清单", partsInventoryBillModel.SparePartCode));
                        var warehouseArea = warehouseAreas.FirstOrDefault(o => o.Code == partsInventoryBillModel.WarehouseAreaCode);
                        var partsStock = partsStocks.FirstOrDefault(o => o.WarehouseAreaId == warehouseArea.Id && o.PartId == part.Id);
                        var enterprisePartsCost = enterprisePartsCosts.FirstOrDefault(r => r.SparePartId == part.Id);

                        decimal price = enterprisePartsCost != null ? enterprisePartsCost.CostPrice : 0;
                        var currentStorage = partsStock == null ? 0 : partsStock.Quantity;
                        var newPibdetail = new PartsInventoryDetail {
                            PartsInventoryBillId = partsInventoryBillId,
                            SparePartId = part.Id,
                            SparePartCode = part.Code,
                            SparePartName = part.Name,
                            WarehouseAreaId = warehouseArea.Id,
                            WarehouseAreaCode = warehouseArea.Code,
                            CurrentStorage = currentStorage,
                            StorageAfterInventory = partsInventoryBillModel.StorageAfterInventory.Value,
                            StorageDifference = partsInventoryBillModel.StorageAfterInventory.Value - currentStorage,
                            CostPrice = price,
                            Remark = "PDA新增盘点单",
                            Ifcover = true,
                            AmountDifference = price * (partsInventoryBillModel.StorageAfterInventory.Value - currentStorage)
                        };
                        dcsEntities.PartsInventoryDetails.AddObject(newPibdetail);
                        dcsEntities.SaveChanges();

                        pibdetail = newPibdetail;
                    } else {
                        pibdetail = partsInventoryBill.PartsInventoryDetails.FirstOrDefault(o => o.Id == partsInventoryBillModel.DetailId);
                        var enterprisePartsCost = enterprisePartsCosts.FirstOrDefault(r => r.SparePartId == pibdetail.SparePartId);
                        decimal price = enterprisePartsCost != null ? enterprisePartsCost.CostPrice : 0;

                        pibdetail.StorageAfterInventory = (partsInventoryBillModel.StorageAfterInventory ?? 0);
                        pibdetail.StorageDifference = pibdetail.StorageAfterInventory - pibdetail.CurrentStorage;

                        pibdetail.CostPrice = price;
                        pibdetail.AmountDifference = price * pibdetail.StorageDifference;
                        amountDifference += price * pibdetail.StorageDifference;
                    }

                    //删除当前盘点清单下，某一个配件的条码清单，
                    var sourceBillDetailIds = ObjectContext.TraceTempTypes.Where(o => o.PartId == pibdetail.SparePartId && o.WarehouseAreaId == pibdetail.WarehouseAreaId).Select(o => o.SourceBillDetailId).ToList();
                    if(sourceBillDetailIds.Count > 0) {
                        dcsEntities.ExecuteStoreCommand(string.Format("DELETE from TraceTempType where SourceBillId={0} and SourceBillDetailId in ({1}) ", partsInventoryBillId, string.Join(",", sourceBillDetailIds)));
                        dcsEntities.SaveChanges();
                    }
                    if(partsInventoryBillModel.SIHCodes.Sum(o => Convert.ToInt32(o.Split('|')[1])) > (partsInventoryBillModel.StorageAfterInventory ?? 0))
                        partsInventoryBillModel.SIHCodes.Clear();
                    //重新录入条码清单
                    foreach(var code in partsInventoryBillModel.SIHCodes) {
                        var codeAndNum = code.Split('|');
                        var accurateTrace = accurateTraces.FirstOrDefault(o => (o.BoxCode == code || o.SIHLabelCode == code) && o.WarehouseAreaId == partsInventoryBillModel.WarehouseAreaId);
                        var traceTempType = traceTempTypes.FirstOrDefault(o => o.SIHLabelCode == code);
                        if(traceTempType != null) {
                            traceTempType.Quantity += Convert.ToInt32(codeAndNum[1]);
                        } else {
                            var partsInventoryDetail = dcsEntities.PartsInventoryDetails.FirstOrDefault(o => o.PartsInventoryBillId == partsInventoryBill.Id
                            && o.WarehouseAreaId == pibdetail.WarehouseAreaId && o.SparePartId == pibdetail.SparePartId);
                            var newTraceTempType = new TraceTempType() {
                                Type = (int)DCSTraceTempType.盘点,
                                SourceBillId = partsInventoryBill.Id,
                                SourceBillDetailId = partsInventoryDetail.Id,
                                PartId = pibdetail.SparePartId,
                                SIHLabelCode = code,
                                Status = (int)DcsBaseDataStatus.有效,
                                TraceProperty = partsInventoryBillModel.TraceProperty,
                                CreateTime = DateTime.Now,
                                CreatorId = userInfo.UserId,
                                CreatorName = userInfo.UserName,
                                WarehouseAreaId = partsInventoryBillModel.WarehouseAreaId,
                                WarehouseAreaCode = partsInventoryBillModel.WarehouseAreaCode,
                                Quantity = Convert.ToInt32(codeAndNum[1]),
                                InboundQty = accurateTrace == null ? 0 : accurateTrace.InQty
                            };
                            dcsEntities.TraceTempTypes.AddObject(newTraceTempType);
                            traceTempTypes.Add(newTraceTempType);
                        }
                    }
                }
                partsInventoryBill.AmountDifference = amountDifference;
                partsInventoryBill.ModifierId = userInfo.UserId;
                partsInventoryBill.ModifierName = userInfo.UserName;
                partsInventoryBill.ModifyTime = DateTime.Now;

                dcsEntities.SaveChanges();
                return true;
            }
        }
    }
}
