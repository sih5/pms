﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Transactions;
using Sunlight.Api.Models;
using Sunlight.Api.Security;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Api.BLL {
    public partial class DcsServiceImp {
        public void 更新出库计划单状态(PartsOutboundPlan partsOutboundPlan) {
            // 计算检验状态
            // billPart 部分出库
            var partsOutboundPlanDetails = ObjectContext.PartsOutboundPlanDetails.Where(r => r.PartsOutboundPlanId == partsOutboundPlan.Id).ToArray();
            var billPart = partsOutboundPlanDetails.Where(r => r.PlannedAmount > (r.OutboundFulfillment ?? 0) && (r.OutboundFulfillment ?? 0) > 0).ToArray();
            var billComplete = partsOutboundPlanDetails.Where(r => r.PlannedAmount == (r.OutboundFulfillment ?? 0) && (r.OutboundFulfillment ?? 0) > 0).ToArray();
            // pickingPart 部分拣货的拣货任务单   pickingComplete拣货完成的拣货任务单
            var pickingTaskDetails = ObjectContext.PickingTaskDetails.Where(r => r.PartsOutboundPlanId == partsOutboundPlan.Id && ObjectContext.PickingTasks.Any(o => o.Id == r.PickingTaskId && o.Status != (int)DcsPickingTaskStatus.作废)).ToArray();
            var pickingPart = pickingTaskDetails.Where(r => (r.PickingQty ?? 0) != r.PlanQty && (r.PickingQty ?? 0) > 0).ToArray();
            var pickingComplete = pickingTaskDetails.Where(r => (r.PickingQty ?? 0) == r.PlanQty).ToArray();

            var boxUpTaskDetails = ObjectContext.BoxUpTaskDetails.Where(r => r.PartsOutboundPlanId == partsOutboundPlan.Id && ObjectContext.BoxUpTasks.Any(o => o.Id == r.BoxUpTaskId && (o.Status != (int)DcsBoxUpTaskStatus.终止 || o.Status != (int)DcsBoxUpTaskStatus.作废))).ToArray();
            var boxUpPart = boxUpTaskDetails.Where(r => (r.BoxUpQty ?? 0) != r.PlanQty && (r.BoxUpQty ?? 0) > 0).ToArray();
            var boxUpComplete = boxUpTaskDetails.Where(r => (r.BoxUpQty ?? 0) == r.PlanQty).ToArray();


            if((partsOutboundPlan.Status == (int)DcsPartsOutboundPlanStatus.分配完成 || partsOutboundPlan.Status == (int)DcsPartsOutboundPlanStatus.部分拣货) && pickingTaskDetails.Length == pickingComplete.Length && pickingTaskDetails.Length > 0)
                partsOutboundPlan.Status = (int)DcsPartsOutboundPlanStatus.拣货完成;
            else if(partsOutboundPlan.Status == (int)DcsPartsOutboundPlanStatus.分配完成 && pickingPart.Length > 0 || (pickingComplete.Length > 0 && pickingComplete.Length != pickingTaskDetails.Length))
                partsOutboundPlan.Status = (int)DcsPartsOutboundPlanStatus.部分拣货;
            if(partsOutboundPlan.Status == (int)DcsPartsOutboundPlanStatus.拣货完成 && boxUpPart.Length > 0 || (boxUpComplete.Length > 0 && boxUpComplete.Length != boxUpTaskDetails.Length))
                partsOutboundPlan.Status = (int)DcsPartsOutboundPlanStatus.部分装箱;
            else if((partsOutboundPlan.Status == (int)DcsPartsOutboundPlanStatus.拣货完成 || partsOutboundPlan.Status == (int)DcsPartsOutboundPlanStatus.部分装箱) && boxUpTaskDetails.Length == boxUpComplete.Length && boxUpComplete.Length > 0)
                partsOutboundPlan.Status = (int)DcsPartsOutboundPlanStatus.装箱完成;
            if(partsOutboundPlan.Status == (int)DcsPartsOutboundPlanStatus.装箱完成 && billPart.Length > 0 || (partsOutboundPlanDetails.Length != billComplete.Length && billComplete.Length > 0))
                partsOutboundPlan.Status = (int)DcsPartsOutboundPlanStatus.部分出库;
            else if((partsOutboundPlan.Status == (int)DcsPartsOutboundPlanStatus.装箱完成 || partsOutboundPlan.Status == (int)DcsPartsOutboundPlanStatus.部分出库) && partsOutboundPlanDetails.Length == billComplete.Length && billComplete.Length > 0)
                partsOutboundPlan.Status = (int)DcsPartsOutboundPlanStatus.出库完成;

            UpdatePartsOutboundPlanValidate(partsOutboundPlan);
            this.ObjectContext.SaveChanges();
        }

        private void UpdatePartsOutboundPlanValidate(PartsOutboundPlan partsOutboundPlan) {
            var userInfo = FormsAuth.GetUserData();
            partsOutboundPlan.ModifierId = userInfo.UserId;
            partsOutboundPlan.ModifierName = userInfo.UserName;
            partsOutboundPlan.ModifyTime = DateTime.Now;
        }
    }
}
