﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Transactions;
using Sunlight.Api.Models;
using Sunlight.Api.Models.Extends;
using Sunlight.Api.Security;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Api.BLL {
    public partial class DcsServiceImp {

        public List<PartsShelvesModel> 查询待上架任务(string code, string sparePartCode, int warehouseId) {
            var currentUser = FormsAuth.GetUserData();
            var result = (from pst in this.ObjectContext.PartsShelvesTasks.Where(o => o.PartsInboundCheckBillCode.ToUpper().IndexOf(code.ToUpper()) >= 0 && o.WarehouseId == warehouseId && (o.Status == (int)DcsPartsShelvesTaskStatus.新增 || o.Status == (int)DcsPartsShelvesTaskStatus.部分上架))
                          join sp in this.ObjectContext.SpareParts on pst.SparePartId equals sp.Id into temp
                          from t in temp.DefaultIfEmpty()
                          where t.Code == sparePartCode
                          select new PartsShelvesModel {
                              Id = pst.Id,
                              Code = pst.Code,
                              SparePartId = (pst.SparePartId ?? 0),
                              SparePartCode = t.Code,
                              SparePartName = t.Name,
                              PlannedAmount = (pst.PlannedAmount ?? 0),
                              ShelvesAmount = (pst.ShelvesAmount ?? 0),
                              SourceBillCode = pst.SourceBillCode,
                              TraceProperty = t.TraceProperty
                          }).ToList();
            foreach(var item in result.OrderBy(o => o.Code)) {
                var partsStocks = ObjectContext.PartsStocks.FirstOrDefault(v => v.SparePart.Code == sparePartCode && v.WarehouseAreaCategory.Category == (int)DcsAreaType.保管区
                && v.WarehouseId == warehouseId && ObjectContext.WarehouseAreas.Any(area => area.Id == v.WarehouseAreaId && area.Status == (int)DcsBaseDataStatus.有效
                    && ObjectContext.WarehouseAreaManagers.Any(manager => manager.WarehouseAreaId == area.TopLevelWarehouseAreaId && manager.ManagerId == currentUser.UserId)));
                if(partsStocks != null) {
                    var warehouseAreas = ObjectContext.WarehouseAreas.FirstOrDefault(v => v.Id == partsStocks.WarehouseAreaId && v.Status == (int)DcsBaseDataStatus.有效);
                    item.WarehouseAreaId = warehouseAreas.Id;
                    item.WarehouseAreaCode = warehouseAreas.Code;
                }
            }
            return result;
        }

        public bool 上架(PartsShelvesModel partsShelvesModel) {
            using(var transaction = new TransactionScope()) {
                var userInfo = FormsAuth.GetUserData();
                var partsShelves = this.ObjectContext.PartsShelvesTasks.FirstOrDefault(o => o.Id == partsShelvesModel.Id);
                if(partsShelves.Status == (int)DcsPartsShelvesTaskStatus.上架完成) {
                    throw new ValidationException(string.Format("已上架完成"));
                }
                var nowShelvesAmount = partsShelvesModel.ThisShelvesAmount;
                var plannedAmount = partsShelves.PlannedAmount ?? 0;
                var shelvesAmount = partsShelves.ShelvesAmount ?? 0;

                var warehouseAreaCategoryIds = ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.保管区).Select(v => v.Id).ToArray();
                if(warehouseAreaCategoryIds.Length == 0)
                    throw new ValidationException(string.Format("货位{0}不是有效的保管区库位！", partsShelvesModel.TargetWarehouseAreaCode));
                var warehouseArea = ObjectContext.WarehouseAreas.FirstOrDefault(o => warehouseAreaCategoryIds.Contains(o.AreaCategoryId ?? 0) && o.Code == partsShelvesModel.TargetWarehouseAreaCode && o.WarehouseId == partsShelves.WarehouseId.Value && o.Status == (int)DcsBaseDataStatus.有效 && o.AreaKind == (int)DcsAreaKind.库位);
                if(warehouseArea == null) {
                    throw new ValidationException(string.Format("编号为{0}的配件,库位[{1}]无效！", partsShelvesModel.SparePartCode, partsShelvesModel.TargetWarehouseAreaCode));
                }


                var warehouseAreaManager = ObjectContext.WarehouseAreaManagers.FirstOrDefault(o => o.WarehouseAreaId == warehouseArea.TopLevelWarehouseAreaId && o.ManagerId == userInfo.UserId);
                if(warehouseAreaManager == null)
                    throw new ValidationException(string.Format("没有上架[{0}]库区的权限，请确认", warehouseArea.Code));

                partsShelvesModel.TargetWarehouseAreaId = warehouseArea.Id;

                if(nowShelvesAmount + shelvesAmount > plannedAmount) {
                    throw new ValidationException(string.Format("编号为{0}的配件,本次上架量不能大于可上架量", partsShelvesModel.SparePartCode));
                }
                if(!partsShelvesModel.TargetWarehouseAreaId.HasValue) {
                    throw new ValidationException(string.Format("编号为{0}的配件,库位不可为空", partsShelvesModel.SparePartCode));
                }

                var nowTime = DateTime.Now;
                partsShelves.ModifierId = userInfo.UserId;
                partsShelves.ModifierName = userInfo.UserName;
                partsShelves.ModifyTime = nowTime;
                partsShelves.ShelvesAmount = shelvesAmount + nowShelvesAmount;
                if(partsShelves.PlannedAmount == partsShelves.ShelvesAmount) {
                    partsShelves.Status = (int)DcsPartsShelvesTaskStatus.上架完成;
                    partsShelves.ShelvesFinishTime = DateTime.Now;
                } else {
                    partsShelves.Status = (int)DcsPartsShelvesTaskStatus.部分上架;
                }

                var partsInboundCheckBillDetails = ObjectContext.PartsInboundCheckBillDetails.Where(r => r.PartsInboundCheckBillId == partsShelves.PartsInboundCheckBillId).ToList();
                var partsInboundPlan = ObjectContext.PartsInboundPlans.FirstOrDefault(r => r.Id == partsShelves.PartsInboundPlanId);

                var warehouseAreaIds = partsInboundCheckBillDetails.Select(r => r.WarehouseAreaId).Distinct();

                //锁定待入区原始货位，库存记录
                var partsStocks = GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => r.PartId == partsShelves.SparePartId.Value && warehouseAreaIds.Contains(r.WarehouseAreaId) && r.WarehouseId == partsShelves.WarehouseId.Value && r.Quantity >= partsShelvesModel.ThisShelvesAmount)).ToList();
                //锁定保留区目标货位，库存记录
                var targetPartsStocks = GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => r.PartId == partsShelves.SparePartId && r.WarehouseAreaId == partsShelvesModel.TargetWarehouseAreaId && r.WarehouseId == partsShelves.WarehouseId)).ToList();


                //添加上架实绩
                var performance = new PartsInboundPerformance();
                performance.Code = partsShelves.Code;
                performance.InType = (int)DcsPartsInboundPer_InType.上架;
                performance.BarCode = partsShelves.BatchNumber;
                performance.WarehouseAreaId = partsShelvesModel.TargetWarehouseAreaId;
                performance.Qty = nowShelvesAmount;
                performance.OperaterId = userInfo.UserId;
                performance.OperaterName = userInfo.UserName;
                performance.OperaterEndTime = nowTime;
                performance.Equipment = (int)DcsPartsInboundPer_EquipmentType.手持;
                performance.SparePartId = partsShelvesModel.SparePartId;
                performance.SpareCode = partsShelvesModel.SparePartCode;
                performance.SpareName = partsShelvesModel.SparePartName;
                ObjectContext.PartsInboundPerformances.AddObject(performance);

                //添加配件库存批次明细
                var type = ObjectContext.Companies.FirstOrDefault(r => r.Id == userInfo.EnterpriseId).Type;
                if(partsShelves.BatchNumber != null) {
                    var partsStockBatchDetail = new PartsStockBatchDetail();
                    partsStockBatchDetail.PartsStockId = partsShelvesModel.TargetWarehouseAreaId.Value;
                    partsStockBatchDetail.WarehouseId = partsShelves.WarehouseId.Value;
                    partsStockBatchDetail.StorageCompanyId = userInfo.EnterpriseId;
                    partsStockBatchDetail.StorageCompanyType = type;
                    partsStockBatchDetail.PartId = partsShelvesModel.SparePartId;
                    partsStockBatchDetail.BatchNumber = partsShelves.BatchNumber;
                    partsStockBatchDetail.Quantity = nowShelvesAmount;
                    partsStockBatchDetail.InboundTime = nowTime;
                    ObjectContext.PartsStockBatchDetails.AddObject(partsStockBatchDetail);
                }

                //修改库存数量
                var partsInboundCheckBillDetail = partsInboundCheckBillDetails.Where(r => r.SparePartId == partsShelvesModel.SparePartId).FirstOrDefault();
                if(partsInboundCheckBillDetail == null) throw new ValidationException(string.Format("编号为{0}的配件,未找到对应的入库单", partsShelvesModel.SparePartCode));
                var partsStock = partsStocks.FirstOrDefault(r => r.PartId == partsShelvesModel.SparePartId && r.WarehouseAreaId == partsInboundCheckBillDetail.WarehouseAreaId);
                if(partsStock != null) {
                    if(partsStock.Quantity - nowShelvesAmount < 0) {
                        throw new ValidationException(string.Format("编号为{0}的配件,未找到有效库存", partsShelvesModel.SparePartCode));
                    }
                    partsStock.Quantity -= nowShelvesAmount;
                    partsStock.ModifierId = userInfo.UserId;
                    partsStock.ModifierName = userInfo.UserName;
                    partsStock.ModifyTime = nowTime;
                } else
                    throw new ValidationException(string.Format("编号为{0}的配件,未找到有效库存", partsShelvesModel.SparePartCode));
                var stock = new PartsStock();
                //增加上架之后的库位库存数量
                var targetPartsStock = targetPartsStocks.FirstOrDefault(r => r.PartId == partsShelvesModel.SparePartId && r.WarehouseAreaId == partsShelvesModel.TargetWarehouseAreaId);
                if(targetPartsStock != null) {
                    targetPartsStock.Quantity += nowShelvesAmount;
                    targetPartsStock.ModifierId = userInfo.UserId;
                    targetPartsStock.ModifierName = userInfo.UserName;
                    targetPartsStock.ModifyTime = nowTime;
                    stock = targetPartsStock;
                } else {
                    if(warehouseArea == null) {
                        throw new ValidationException("新增配件库存时没有找到对应的库位");
                    }
                    var ps = new PartsStock {
                        WarehouseId = partsShelves.WarehouseId ?? 0,
                        StorageCompanyId = userInfo.EnterpriseId,
                        StorageCompanyType = type,
                        BranchId = partsInboundPlan.BranchId,
                        WarehouseAreaId = partsShelvesModel.TargetWarehouseAreaId.Value,
                        PartId = partsShelvesModel.SparePartId,
                        Quantity = nowShelvesAmount,
                        WarehouseAreaCategoryId = warehouseArea.AreaCategoryId
                    };
                    this.ObjectContext.PartsStocks.AddObject(ps);
                    ObjectContext.SaveChanges();
                    stock = ps;
                }
                if(!string.IsNullOrEmpty(partsShelvesModel.SIHCodes)) {
                    var accurateTraces = ObjectContext.AccurateTraces.Where(t => t.PartId == partsShelvesModel.SparePartId && t.CompanyId == userInfo.EnterpriseId && t.Type == (int)DCSAccurateTraceType.入库 && t.Status == (int)DCSAccurateTraceStatus.有效 && t.SIHLabelCode != null).ToArray();
                    var sihCodes = partsShelvesModel.SIHCodes.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Distinct().ToArray();
                    if(partsShelvesModel.TraceProperty.HasValue && partsShelvesModel.TraceProperty == (int)DCSTraceProperty.精确追溯) {
                        if(sihCodes.Any(t => t.Contains("X"))) {
                            var codes = accurateTraces.Where(t => t.PackingTaskId == partsShelves.PackingTaskId && sihCodes.Contains(t.BoxCode) && t.PartsStockId == null && t.PartId == partsShelvesModel.SparePartId).ToArray();
                            if(codes.Count() < partsShelvesModel.ThisShelvesAmount) {
                                throw new ValidationException("箱标" + partsShelvesModel.SIHCodes + "对应的未上架数量小于于本次上架数量");
                            }
                            var upqty = codes.Count();
                            if(codes.Count() >= partsShelvesModel.ThisShelvesAmount) {
                                upqty = partsShelvesModel.ThisShelvesAmount;
                            }
                            int i = 0;
                            foreach(var item in codes) {
                                if(i >= upqty) {
                                    continue;
                                }
                                item.PartsStockId = stock.Id;
                                item.WarehouseAreaId = stock.WarehouseAreaId;
                                item.WarehouseAreaCode = partsShelvesModel.WarehouseAreaCode;
                                item.InQty = 1;
                                item.ModifierId = userInfo.UserId;
                                item.ModifierName = userInfo.UserName;
                                item.ModifyTime = DateTime.Now;
                                i++;
                            }
                        } else {
                            foreach(var item in sihCodes) {
                                var codes = accurateTraces.Where(t => t.PackingTaskId == partsShelves.PackingTaskId && t.SIHLabelCode == item && t.PartsStockId == null && t.PartId == partsShelvesModel.SparePartId).FirstOrDefault();
                                if(codes == null) {
                                    throw new ValidationException("标签码" + partsShelvesModel.SIHCodes + "已上架，请重新选择");
                                } else {
                                    codes.PartsStockId = stock.Id;
                                    codes.WarehouseAreaId = stock.WarehouseAreaId;
                                    codes.WarehouseAreaCode = partsShelvesModel.WarehouseAreaCode;
                                    codes.InQty = 1;
                                    codes.ModifierId = userInfo.UserId;
                                    codes.ModifierName = userInfo.UserName;
                                    codes.ModifyTime = DateTime.Now;
                                }
                            }
                        }
                        if(string.IsNullOrEmpty(partsShelves.SIHCodes)) {
                            partsShelvesModel.SIHCodes = partsShelves.SIHCodes;
                        } else {
                            partsShelvesModel.SIHCodes = partsShelvesModel.SIHCodes + ";" + partsShelves.SIHCodes;
                        }
                    } else {
                        var codes = accurateTraces.Where(t => t.PackingTaskId == partsShelves.PackingTaskId && t.SIHLabelCode == sihCodes[0] && t.PartId == partsShelvesModel.SparePartId).OrderByDescending(y => y.CreateTime).ToArray();
                        if(sihCodes[0].Contains("X")) {
                            codes = accurateTraces.Where(t => t.PackingTaskId == partsShelves.PackingTaskId && t.BoxCode == sihCodes[0] && t.PartId == partsShelvesModel.SparePartId).OrderByDescending(y => y.CreateTime).ToArray();
                        }
                        if(codes.Count()==0) {
                            throw new ValidationException("标签码" + partsShelvesModel.SIHCodes + "不是当前上架单的标签");
                        }
                        if(codes.Any(t => t.PartsStockId == stock.Id)) {
                            var sihCode = codes.First(t => t.PartsStockId == stock.Id);
                            sihCode.InQty = sihCode.InQty + partsShelvesModel.ThisShelvesAmount;
                            sihCode.ModifierId = userInfo.UserId;
                            sihCode.ModifierName = userInfo.UserName;
                            sihCode.ModifyTime = DateTime.Now;
                        } else {
                            if(codes.Any(t => t.PartsStockId == null)) {
                                var sihCode = codes.First(t => t.PartsStockId == null);
                                sihCode.InQty = partsShelvesModel.ThisShelvesAmount;
                                sihCode.PartsStockId = stock.Id;
                                sihCode.WarehouseAreaId = stock.WarehouseAreaId;
                                sihCode.WarehouseAreaCode = partsShelvesModel.WarehouseAreaCode;
                                sihCode.ModifierId = userInfo.UserId;
                                sihCode.ModifierName = userInfo.UserName;
                                sihCode.ModifyTime = DateTime.Now;
                            } else if(codes.All(t => t.PartsStockId != null)) {
                                var sihCode = codes.First();
                                var newSihCode = new AccurateTrace {
                                    Type = sihCode.Type,
                                    CompanyType = sihCode.CompanyType,
                                    CompanyId = sihCode.CompanyId,
                                    SourceBillId = sihCode.SourceBillId,
                                    PartId = sihCode.PartId,
                                    TraceCode = sihCode.TraceCode,
                                    SIHLabelCode = sihCode.SIHLabelCode,
                                    Status = sihCode.Status,
                                    CreatorId = userInfo.UserId,
                                    CreateTime = DateTime.Now,
                                    CreatorName = userInfo.UserName,
                                    TraceProperty = sihCode.TraceProperty,
                                    BoxCode = sihCode.BoxCode,
                                    InQty = partsShelvesModel.ThisShelvesAmount,
                                    PackingTaskId = sihCode.PackingTaskId,
                                    PartsStockId = stock.Id,
                                    WarehouseAreaId = stock.WarehouseAreaId,
                                    WarehouseAreaCode = partsShelvesModel.WarehouseAreaCode
                                };
                                this.ObjectContext.AccurateTraces.AddObject(newSihCode);
                            }
                        }
                        if(string.IsNullOrEmpty(partsShelves.SIHCodes)) {
                            partsShelves.SIHCodes = partsShelvesModel.SIHCodes;
                        }

                    }
                }
                ObjectContext.SaveChanges();
                更新入库计划状态(partsInboundPlan);
                transaction.Complete();
            }
            return true;
        }

        public List<PartsStockShelvesExtend> 根据配件编号查询检验区库存(string sparePartCode, int warehouseId) {
            var userInfo = FormsAuth.GetUserData();
            //查询出指定库区库位
            var warehouseAreaManagerAreaIds = ObjectContext.WarehouseAreaManagers.Where(r => r.ManagerId == userInfo.UserId).Select(v => v.WarehouseAreaId).ToArray();
            if(warehouseAreaManagerAreaIds.Length == 0)
                return null;
            var saleUnit = ObjectContext.SalesUnits.FirstOrDefault(r => ObjectContext.SalesUnitAffiWarehouses.Any(v => v.SalesUnitId == r.Id && v.WarehouseId == warehouseId && ObjectContext.WarehouseAreas.Any(k => warehouseAreaManagerAreaIds.Contains(k.Id) && k.WarehouseId == v.WarehouseId)));
            if(saleUnit == null)
                return null;

            var resultPartsStock = from partsStock in ObjectContext.PartsStocks.Where(r => r.Quantity > 0)
                                   from warehouseArea in ObjectContext.WarehouseAreas.Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.AreaKind == (int)DcsAreaKind.库位)
                                   from warehouse in ObjectContext.Warehouses.Where(r => r.Status == (int)DcsBaseDataStatus.有效)
                                   from sparePart in ObjectContext.SpareParts.Where(r => r.Status == (int)DcsMasterDataStatus.有效)
                                   where warehouseArea.Id == partsStock.WarehouseAreaId
                                   && warehouseAreaManagerAreaIds.Contains(warehouseArea.TopLevelWarehouseAreaId.Value)
                                   && ObjectContext.WarehouseAreaCategories.Any(r => r.Category == (int)DcsAreaType.检验区 && r.Id == warehouseArea.AreaCategoryId)
                                   && warehouse.Id == partsStock.WarehouseId
                                   && sparePart.Id == partsStock.PartId
                                   && warehouse.Id == warehouseId
                                   && sparePart.Code == sparePartCode
                                   select new {
                                       PartsStockId = partsStock.Id,
                                       partsStock.WarehouseId,
                                       WarehouseCode = warehouse.Code,
                                       WarehouseName = warehouse.Name,
                                       partsStock.StorageCompanyId,
                                       partsStock.StorageCompanyType,
                                       partsStock.BranchId,
                                       SparePartId = sparePart.Id,
                                       SparePartCode = sparePart.Code,
                                       SparePartName = sparePart.Name,
                                       sparePart.MeasureUnit,
                                       partsStock.WarehouseAreaId,
                                       WarehouseAreaCode = warehouseArea.Code,
                                       partsStock.Quantity
                                   };

            var result = (from rt in resultPartsStock
                          join partsStockBatchDetail in ObjectContext.PartsStockBatchDetails on rt.PartsStockId equals partsStockBatchDetail.PartsStockId into details
                          from v in details.DefaultIfEmpty()
                          where ((int?)v.PartsStockId).HasValue || ObjectContext.PartsBranches.Any(r => r.PartId == rt.SparePartId && r.BranchId == rt.BranchId && r.Status == (int)DcsBaseDataStatus.有效
                             && r.PartsSalesCategoryId == saleUnit.PartsSalesCategoryId && (r.PartsWarhouseManageGranularity == null || r.PartsWarhouseManageGranularity == (int)DcsPartsBranchPartsWarhouseManageGranularity.无批次管理))
                          select new PartsStockShelvesExtend {
                              PartsStockId = rt.PartsStockId,
                              WarehouseId = rt.WarehouseId,
                              WarehouseCode = rt.WarehouseCode,
                              WarehouseName = rt.WarehouseName,
                              StorageCompanyId = rt.StorageCompanyId,
                              StorageCompanyType = rt.StorageCompanyType,
                              BranchId = rt.BranchId,
                              SparePartId = rt.SparePartId,
                              SparePartCode = rt.SparePartCode,
                              SparePartName = rt.SparePartName,
                              MeasureUnit = rt.MeasureUnit,
                              WarehouseAreaId = rt.WarehouseAreaId,
                              WarehouseAreaCode = rt.WarehouseAreaCode,
                              Quantity = rt.Quantity,
                              UsableQuantity = (int?)v.Quantity ?? rt.Quantity,//此处不能改为v.Quantity 因join 可能导致v.Quantity为Null 所以强制转换有效
                              BatchNumber = v.BatchNumber ?? "",
                              WarehouseAreaCategory = (int)DcsAreaType.检验区
                          }).ToList();

            foreach(var item in result.OrderBy(r => r.PartsStockId)) {
                var partsStocks = ObjectContext.PartsStocks.FirstOrDefault(v => v.SparePart.Code == sparePartCode && v.WarehouseAreaCategory.Category == (int)DcsAreaType.保管区
               && v.WarehouseId == warehouseId && ObjectContext.WarehouseAreas.Any(area => area.Id == v.WarehouseAreaId && area.Status == (int)DcsBaseDataStatus.有效
                   && ObjectContext.WarehouseAreaManagers.Any(manager => manager.WarehouseAreaId == area.TopLevelWarehouseAreaId && manager.ManagerId == userInfo.UserId)));
                if(partsStocks != null) {
                    var warehouseAreas = ObjectContext.WarehouseAreas.FirstOrDefault(v => v.Id == partsStocks.WarehouseAreaId && v.Status == (int)DcsBaseDataStatus.有效);
                    item.RecommendWarehouseAreaId = warehouseAreas.Id;
                    item.RecommendWarehouseAreaCode = warehouseAreas.Code;
                }
            }
            return result;
        }

        public bool 中心库上架(List<PartsStockShelvesExtend> partsStockShelves) {
            var userInfo = FormsAuth.GetUserData();
            var partsShiftOrder = new PartsShiftOrder();
            partsShiftOrder.Code = CodeGenerator.Generate("PartsShiftOrder", userInfo.EnterpriseCode);
            partsShiftOrder.BranchId = partsStockShelves[0].BranchId;
            partsShiftOrder.WarehouseId = partsStockShelves[0].WarehouseId;
            partsShiftOrder.WarehouseCode = partsStockShelves[0].WarehouseCode;
            partsShiftOrder.WarehouseName = partsStockShelves[0].WarehouseName;
            partsShiftOrder.StorageCompanyId = userInfo.EnterpriseId;
            partsShiftOrder.StorageCompanyCode = userInfo.EnterpriseCode;
            partsShiftOrder.StorageCompanyName = userInfo.EnterpriseName;
            partsShiftOrder.StorageCompanyType = partsStockShelves[0].StorageCompanyType;
            partsShiftOrder.Type = (int)DcsPartsShiftOrderType.上架;
            foreach(var stock in partsStockShelves) {
                var warehouseArea = ObjectContext.WarehouseAreas.FirstOrDefault(o => o.WarehouseAreaCategory.Category == (int)DcsAreaType.保管区 && o.Warehouse.Id == partsShiftOrder.WarehouseId
                    && o.Status == (int)DcsBaseDataStatus.有效 && o.AreaKind == (int)DcsAreaKind.库位 && o.Code == stock.DestWarehouseAreaCode);
                if(warehouseArea == null)
                    throw new ValidationException(string.Format("目标货位“{0}”无效，不是有效的保管区库位！", stock.DestWarehouseAreaCode));
                partsShiftOrder.PartsShiftOrderDetails.Add(new PartsShiftOrderDetail {
                    SparePartCode = stock.SparePartCode,
                    SparePartName = stock.SparePartName,
                    BatchNumber = stock.BatchNumber,
                    OriginalWarehouseAreaId = stock.WarehouseAreaId,
                    OriginalWarehouseAreaCode = stock.WarehouseAreaCode,
                    OriginalWarehouseAreaCategory = (int)DcsAreaType.检验区,
                    DestWarehouseAreaCategory = (int)DcsAreaType.保管区,
                    DestWarehouseAreaId = warehouseArea.Id,
                    DestWarehouseAreaCode = warehouseArea.Code,
                    //ShelvesAmount = stock.UsableQuantity,
                    Quantity = stock.UsableQuantity,
                    SparePartId = stock.SparePartId,
                    //MeasureUnit = stock.MeasureUnit
                });
            }

            if(!partsShiftOrder.PartsShiftOrderDetails.Any()) {
                throw new ValidationException("配件移库清单不能为空！");
            }

            var partsShiftOrderDetails = partsShiftOrder.PartsShiftOrderDetails.ToArray();
            var originalWarehouseAreaIds = partsShiftOrderDetails.Select(r => r.OriginalWarehouseAreaId).ToArray();
            var warehouseAreaIds = partsShiftOrderDetails.Select(r => r.DestWarehouseAreaId).Union(originalWarehouseAreaIds);
            var warehouses = ObjectContext.WarehouseAreas.Where(r => warehouseAreaIds.Any(v => v == r.Id)).ToArray();
            if(warehouses.Any(r => r.Status == (int)DcsBaseDataStatus.作废)) {
                throw new ValidationException("清单中存在状态为作废的库位!");
            }
            var sparePartIds = partsShiftOrderDetails.Select(r => r.SparePartId);
            var company = ObjectContext.Companies.FirstOrDefault(r => r.Id == partsShiftOrder.StorageCompanyId);
            if(company == null)
                throw new ValidationException("该配件移库单对应的仓储企业不存在!");
            var partsStocks = GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => r.WarehouseId == partsShiftOrder.WarehouseId && sparePartIds.Contains(r.PartId) && warehouseAreaIds.Contains(r.WarehouseAreaId))).ToList();
            //汇总源库位配件
            var originalPartsShiftOrderDetails = partsShiftOrderDetails.GroupBy(v => new {
                v.SparePartId,
                v.OriginalWarehouseAreaId
            }).Select(v => new {
                v.Key.SparePartId,
                v.Key.OriginalWarehouseAreaId,
                v.First().SparePartCode,
                v.First().OriginalWarehouseAreaCode,
                Quantity = v.Sum(detail => detail.Quantity),
                detailsWithBatchNumber = v.Where(detail => !string.IsNullOrEmpty(detail.BatchNumber)).ToArray(),
            }).ToArray();

            #region 校验是否有未完成的盘点单
            var originalWarehouseArea = originalPartsShiftOrderDetails.Select(r => r.OriginalWarehouseAreaId).ToArray();
            var checkInventoryTemp = (from a in ObjectContext.PartsInventoryBills.Where(r => r.WarehouseId == partsShiftOrder.WarehouseId && r.Status == (int)DcsPartsInventoryBillStatus.已审批)
                                      from b in a.PartsInventoryDetails
                                      where originalWarehouseArea.Any(v => v == b.WarehouseAreaId)
                                      select b).ToArray();
            var check = checkInventoryTemp.Where(r => originalPartsShiftOrderDetails.Any(v => v.OriginalWarehouseAreaId == r.WarehouseAreaId && v.SparePartId == r.SparePartId)).ToArray();

            if(check.Length > 0) {
                var errorsparePartString = string.Join(",", check.Select(r => r.SparePartCode).ToArray());
                throw new ValidationException("配件编号" + errorsparePartString + "存在未完成的配件盘点单，请完成盘点单后再移库");
            }
            #endregion


            //汇总目标库位配件
            var destPartsShiftOrderDetails = partsShiftOrderDetails.GroupBy(v => new {
                v.SparePartId,
                v.DestWarehouseAreaId
            }).Select(v => new {
                v.Key.SparePartId,
                v.Key.DestWarehouseAreaId,
                Quantity = v.Sum(detail => detail.Quantity),
                detailsWithBatchNumber = v.Where(detail => !string.IsNullOrEmpty(detail.BatchNumber)).ToArray(),
            }).ToArray();
            //获取配件库存批次明细
            var partsShiftOrderDetailsWithBatchNumber = partsShiftOrderDetails.Where(r => !string.IsNullOrEmpty(r.BatchNumber)).ToArray();
            var batchNumbers = partsShiftOrderDetailsWithBatchNumber.Select(r => r.BatchNumber.ToLower());
            var partsStockBatchDetails = ObjectContext.PartsStockBatchDetails.Where(r => batchNumbers.Contains(r.BatchNumber.ToLower())).ToArray();

            //校验并调整源库位库存
            foreach(var detail in originalPartsShiftOrderDetails) {
                var partsStock = partsStocks.FirstOrDefault(v => v.WarehouseAreaId == detail.OriginalWarehouseAreaId && v.PartId == detail.SparePartId);
                if(partsStock == null)
                    throw new ValidationException(string.Format("编号为“{0}”的配件，在编号为“{1}”的库位上不存在库存记录", detail.SparePartCode, detail.OriginalWarehouseAreaCode));
                if(partsStock.Quantity < detail.Quantity)
                    throw new ValidationException(string.Format("编号为“{0}”的配件，在编号为“{1}”的库位上的库存", detail.SparePartCode, detail.OriginalWarehouseAreaCode));
                partsStock.Quantity -= detail.Quantity;
                partsStock.ModifierId = userInfo.UserId;
                partsStock.ModifierName = userInfo.UserName;
                partsStock.ModifyTime = DateTime.Now;

                foreach(var detailWithBatchNumber in detail.detailsWithBatchNumber) {
                    var partsStockBatchDetail = partsStockBatchDetails.FirstOrDefault(v => String.Compare(v.BatchNumber, detailWithBatchNumber.BatchNumber, StringComparison.OrdinalIgnoreCase) == 0 && v.Quantity == detailWithBatchNumber.Quantity);
                    if(partsStockBatchDetail == null)
                        throw new ValidationException(string.Format("批次号为“{0}”配件库存批次不存在", detailWithBatchNumber.BatchNumber));
                    if(partsStock.Id != partsStockBatchDetail.PartsStockId)
                        throw new ValidationException(string.Format("批次号为“{0}”配件库存批次不在指定库位上", detailWithBatchNumber.BatchNumber));
                }
            }

            //调整目标库位库存
            foreach(var detail in destPartsShiftOrderDetails) {
                var partsStock = partsStocks.FirstOrDefault(v => v.WarehouseAreaId == detail.DestWarehouseAreaId && v.PartId == detail.SparePartId);
                if(partsStock == null) {
                    var tempWarehouseArea = warehouses.Single(r => r.Id == detail.DestWarehouseAreaId);
                    partsStock = new PartsStock {
                        WarehouseId = partsShiftOrder.WarehouseId,
                        WarehouseAreaCategoryId = tempWarehouseArea.AreaCategoryId,
                        StorageCompanyId = partsShiftOrder.StorageCompanyId,
                        StorageCompanyType = partsShiftOrder.StorageCompanyType,
                        BranchId = partsShiftOrder.BranchId,
                        WarehouseAreaId = detail.DestWarehouseAreaId,
                        PartId = detail.SparePartId,
                        Quantity = detail.Quantity
                    };
                    partsStock.CreatorId = userInfo.UserId;
                    partsStock.CreatorName = userInfo.UserName;
                    partsStock.CreateTime = DateTime.Now;
                    ObjectContext.PartsStocks.AddObject(partsStock);
                } else {
                    partsStock.Quantity += detail.Quantity;
                    partsStock.ModifierId = userInfo.UserId;
                    partsStock.ModifierName = userInfo.UserName;
                    partsStock.ModifyTime = DateTime.Now;
                }
                foreach(var detailWithBatchNumber in detail.detailsWithBatchNumber) {
                    var partsStockBatchDetail = partsStockBatchDetails.Single(v => String.Compare(v.BatchNumber, detailWithBatchNumber.BatchNumber, StringComparison.OrdinalIgnoreCase) == 0 && v.Quantity == detailWithBatchNumber.Quantity);
                    partsStockBatchDetail.PartsStockId = partsStock.Id;
                    partsStock.PartsStockBatchDetails.Add(partsStockBatchDetail);
                }
            }
            partsShiftOrder.StorageCompanyCode = company.Code;
            partsShiftOrder.StorageCompanyName = company.Name;
            partsShiftOrder.Status = (int)DcsPartsShiftOrderStatus.生效;
            partsShiftOrder.CreatorId = userInfo.UserId;
            partsShiftOrder.CreatorName = userInfo.UserName;
            partsShiftOrder.CreateTime = DateTime.Now;
            ObjectContext.PartsShiftOrders.AddObject(partsShiftOrder);
            int i = ObjectContext.SaveChanges();
            return i > 0;
            //}
        }
    }
}
