﻿using Sunlight.Api.Models;
using Sunlight.Api.Security;
using Sunlight.Silverlight.Dcs.Web;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;

namespace Sunlight.Api.BLL
{
    public partial class DcsServiceImp
    {
        //点击上架按钮，查询可以上架的移库单号
        public List<PartsShiftOrderDTO> GetUpPartsShift(string code)
        {
            var userInfo = FormsAuth.GetUserData();
            var shift = ObjectContext.PartsShiftOrders.Where(t => (t.ShiftStatus == (int)DCSPartsShiftOrderShiftStatus.部分移库 || t.ShiftStatus == (int)DCSPartsShiftOrderShiftStatus.待移库) && t.StorageCompanyId == userInfo.EnterpriseId && ObjectContext.PartsShiftOrderDetails.Any(p => t.Id == p.PartsShiftOrderId && p.DownShelfQty > 0 && (p.DownShelfQty > p.UpShelfQty || p.UpShelfQty == null)));
            if (!string.IsNullOrEmpty(code))
            {
                shift = shift.Where(t => t.Code.ToUpper() == code.ToUpper());
            }
            var result = (from a in shift
                          join b in ObjectContext.PartsShiftOrderDetails on a.Id equals b.PartsShiftOrderId
                          join c in ObjectContext.SpareParts on b.SparePartId equals c.Id
                          select new PartsShiftOrderDTO
                          {
                              Id = a.Id,
                              Code = a.Code,
                              ShiftStatus = a.ShiftStatus.Value,
                              SparePartId = b.SparePartId,
                              SparePartCode = b.SparePartCode,
                              SparePartName = b.SparePartName,
                              OriginalWarehouseAreaCode = b.OriginalWarehouseAreaCode,
                              DestWarehouseAreaCode = b.DestWarehouseAreaCode,
                              OriginalWarehouseAreaId = b.OriginalWarehouseAreaId,
                              DestWarehouseAreaId = b.DestWarehouseAreaId,
                              TraceProperty = c.TraceProperty,
                              DownShelfQty = b.DownShelfQty.Value,
                              UpShelfQty = b.UpShelfQty.Value,
                              Quantity = b.Quantity
                          }).ToList();
            return result.OrderBy(t => t.DestWarehouseAreaCode).ThenBy(r => r.SparePartCode).ToList();
        }
        //上架界面中，点击上传按钮后，更新上架数据
        public bool PartsShiftUping(List<PartsShiftOrderDTO> inPutPartsShiftOrders)
        {
            var userInfo = FormsAuth.GetUserData();
            var company = ObjectContext.Companies.Where(t => t.Id == userInfo.EnterpriseId).First();
            var spIds = inPutPartsShiftOrders.Select(t => t.SparePartId).Distinct().ToArray();
            var codes = ObjectContext.AccurateTraces.Where(t => spIds.Contains(t.PartId.Value) && t.CompanyId == userInfo.EnterpriseId && t.Status == (int)DCSAccurateTraceStatus.有效).ToArray();
            var oldPartsShiftSIHDetailsAll = ObjectContext.PartsShiftSIHDetails.Where(t => t.Type == (int)DCSPartsShiftSIHDetailType.上架).ToArray();
            var oldAccutrace = ObjectContext.AccurateTraces.Where(t => t.CompanyId == userInfo.EnterpriseId && t.Status == (int)DCSAccurateTraceStatus.有效 && t.Type == (int)DCSAccurateTraceType.入库).ToArray();
            var partsLockedStocks = ObjectContext.PartsLockedStocks.Where(t => spIds.Contains(t.PartId)).ToArray();
            foreach (var partsShiftOrders in inPutPartsShiftOrders)
            {
                if (partsShiftOrders.PartsShiftSIHDetails == null || partsShiftOrders.PartsShiftSIHDetails.Count() == 0)
                {
                    continue;
                }

                var dbpartsShiftOrder = ObjectContext.PartsShiftOrders.Include("PartsShiftOrderDetails").Where(r => (r.ShiftStatus == (int)DCSPartsShiftOrderShiftStatus.部分移库 || r.ShiftStatus == (int)DCSPartsShiftOrderShiftStatus.待移库) && r.Status == (int)DcsPartsShiftOrderStatus.生效 && r.Id == partsShiftOrders.Id).SingleOrDefault();
                if (dbpartsShiftOrder == null)
                    throw new ValidationException("未找到有效的移库单上架");
                var oldPartsShiftSIHDetails = oldPartsShiftSIHDetailsAll.Where(t => t.PartsShiftOrderId == dbpartsShiftOrder.Id).ToArray();
                var oldPartsShiftOrderDetail = dbpartsShiftOrder.PartsShiftOrderDetails.Where(t => t.SparePartId == partsShiftOrders.SparePartId && t.OriginalWarehouseAreaId == partsShiftOrders.OriginalWarehouseAreaId && t.DestWarehouseAreaId == partsShiftOrders.DestWarehouseAreaId).ToArray();
                oldPartsShiftOrderDetail.First().CourrentUpShelfQty = partsShiftOrders.PartsShiftSIHDetails.Sum(t => t.Quantity);
                var originalWarehouseAreaIds = partsShiftOrders.OriginalWarehouseAreaId;
                var warehouseAreaIds = partsShiftOrders.DestWarehouseAreaId;
                var warehouses = ObjectContext.WarehouseAreas.Where(r => r.Id == originalWarehouseAreaIds || r.Id == warehouseAreaIds).ToArray();
                var partsStocks = GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => r.WarehouseId == dbpartsShiftOrder.WarehouseId && partsShiftOrders.SparePartId == r.PartId && partsShiftOrders.DestWarehouseAreaId == r.WarehouseAreaId)).ToList();
                var partsStocksOr = GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => r.WarehouseId == dbpartsShiftOrder.WarehouseId && partsShiftOrders.SparePartId == r.PartId && partsShiftOrders.OriginalWarehouseAreaId == r.WarehouseAreaId)).ToList();

                //汇总目标库位配件
                var destPartsShiftOrderDetails = oldPartsShiftOrderDetail.GroupBy(v => new
                {
                    v.SparePartId,
                    v.DestWarehouseAreaId
                }).Select(v => new
                {
                    v.Key.SparePartId,
                    v.Key.DestWarehouseAreaId,
                    Quantity = v.Sum(detail => detail.CourrentUpShelfQty),
                    detailsWithBatchNumber = v.Where(detail => !string.IsNullOrEmpty(detail.BatchNumber)).ToArray(),
                }).ToArray();

                //汇总源库位配件
                var originalPartsShiftOrderDetails = oldPartsShiftOrderDetail.GroupBy(v => new
                {
                    v.SparePartId,
                    v.OriginalWarehouseAreaId
                }).Select(v => new
                {
                    v.Key.SparePartId,
                    v.Key.OriginalWarehouseAreaId,
                    v.First().SparePartCode,
                    v.First().OriginalWarehouseAreaCode,
                    Quantity = v.Sum(detail => detail.CourrentUpShelfQty),
                    detailsWithBatchNumber = v.Where(detail => !string.IsNullOrEmpty(detail.BatchNumber)).ToArray(),
                }).ToArray();
                //获取配件库存批次明细
                var partsShiftOrderDetailsWithBatchNumber = oldPartsShiftOrderDetail.Where(r => !string.IsNullOrEmpty(r.BatchNumber)).ToArray();
                var batchNumbers = partsShiftOrderDetailsWithBatchNumber.Select(r => r.BatchNumber.ToLower());
                var partsStockBatchDetails = ObjectContext.PartsStockBatchDetails.Where(r => batchNumbers.Contains(r.BatchNumber.ToLower())).ToArray();
                var partsShiftSIHDetailDown = ObjectContext.PartsShiftSIHDetails.Where(t => t.PartsShiftOrderId == dbpartsShiftOrder.Id).ToArray();
                var item = oldPartsShiftOrderDetail.First();
                foreach (var upDetail in partsShiftOrders.PartsShiftSIHDetails)
                {
                    List<AccurateTrace> traceCodes = new List<AccurateTrace>();
                    var downs = partsShiftSIHDetailDown.Where(t => t.PartsShiftOrderDetailId == item.Id).ToArray();
                    var insetCode = codes.Where(t => upDetail.SIHCode == t.SIHLabelCode && t.PartId == item.SparePartId && downs.Any(y => y.SIHCode == t.SIHLabelCode) && (t.WarehouseAreaId == item.OriginalWarehouseAreaId || t.WarehouseAreaId == null)).ToArray();
                    if (upDetail.SIHCode.EndsWith("X"))
                    {
                        insetCode = codes.Where(t => upDetail.SIHCode == t.BoxCode && t.PartId == item.SparePartId && downs.Any(y => y.SIHCode == t.SIHLabelCode) && (t.WarehouseAreaId == item.OriginalWarehouseAreaId || t.WarehouseAreaId == null)).ToArray();
                    }
                    if (insetCode == null || insetCode.Count() == 0)
                    {
                        //判断是否为下架标签
                        if (!downs.Any(t => t.SIHCode == upDetail.SIHCode))
                        {
                            throw new ValidationException(upDetail.SIHCode + "不是已下架标签，请重新扫描");
                        }
                        var oldT = oldAccutrace.Where(t => t.PartId == item.SparePartId && (t.SIHLabelCode == upDetail.SIHCode || t.BoxCode == upDetail.SIHCode) && t.WarehouseAreaId == item.DestWarehouseAreaId).FirstOrDefault();
                        if (oldT == null)
                        {
                            var newAcc = new AccurateTrace
                            {
                                Type = (int)DCSAccurateTraceType.入库,
                                CompanyId = userInfo.EnterpriseId,
                                CompanyType = company.Type == (int)DcsCompanyType.分公司 ? (int)DCSAccurateTraceCompanyType.配件中心 : (int)DCSAccurateTraceCompanyType.中心库,
                                PartId = item.SparePartId,
                                SIHLabelCode = upDetail.SIHCode.EndsWith("J") ? upDetail.SIHCode : "",
                                Status = (int)DCSAccurateTraceStatus.有效,
                                CreateTime = DateTime.Now,
                                CreatorId = userInfo.UserId,
                                CreatorName = userInfo.UserName,
                                TraceProperty = partsShiftOrders.TraceProperty,
                                BoxCode = upDetail.SIHCode.EndsWith("X") ? upDetail.SIHCode : "",
                                InQty = 1,
                                WarehouseAreaId = item.DestWarehouseAreaId,
                                WarehouseAreaCode = item.DeWarehouseAreaCode
                            };
                            this.ObjectContext.AccurateTraces.AddObject(newAcc);
                        }
                        else
                        {
                            if (!partsShiftOrders.TraceProperty.HasValue || partsShiftOrders.TraceProperty == (int)DCSTraceProperty.批次追溯)
                            {
                                oldT.InQty = oldT.InQty + upDetail.Quantity;
                                oldT.ModifierId = userInfo.UserId;
                                oldT.ModifierName = userInfo.UserName;
                                oldT.ModifyTime = DateTime.Now;
                            }
                        }
                        var qty = 0;
                        if (partsShiftOrders.TraceProperty.HasValue && partsShiftOrders.TraceProperty == (int)DCSTraceProperty.精确追溯)
                        {
                            qty = 1;
                        }
                        else
                            qty = upDetail.Quantity.Value;
                        var old = oldPartsShiftSIHDetails.Where(t => t.PartsShiftOrderDetailId == item.Id && t.SIHCode == upDetail.SIHCode).FirstOrDefault();
                        if (old == null)
                        {
                            var partsShiftSIHDetail = new Sunlight.Silverlight.Dcs.Web.PartsShiftSIHDetail
                            {
                                PartsShiftOrderId = partsShiftOrders.Id,
                                PartsShiftOrderDetailId = item.Id,
                                Type = (int)DCSPartsShiftSIHDetailType.上架,
                                SIHCode = upDetail.SIHCode,
                                Quantity = qty,
                            };
                            this.ObjectContext.PartsShiftSIHDetails.AddObject(partsShiftSIHDetail);
                        }
                        else
                        {
                            old.Quantity = old.Quantity + qty;
                        }
                    }
                    foreach (var insertCode in insetCode)
                    {
                        traceCodes.Add(insertCode);
                    }
                    if (item.UpShelfQty == null)
                    {
                        item.UpShelfQty = 0;
                    }
                    item.UpShelfQty = item.UpShelfQty + upDetail.Quantity;
                    if (traceCodes == null || traceCodes.Count() == 0)
                    {
                        continue;
                    }
                    if (traceCodes.Count() == 0 || (traceCodes.Count() < upDetail.Quantity && partsShiftOrders.TraceProperty == (int)DCSTraceProperty.精确追溯))
                    {
                        throw new ValidationException("未找到有效的SIH标签码" + item.CourrentSIHCode);
                    }
                    if (item.DownShelfQty < item.UpShelfQty)
                    {
                        throw new ValidationException(item.SparePartCode + "上架数量大于下架数量");
                    }
                    if (string.IsNullOrEmpty(item.SIHCode))
                    {
                        item.SIHCode = upDetail.SIHCode;
                    }
                    else if (!item.SIHCode.Contains(upDetail.SIHCode))
                        item.SIHCode = item.SIHCode + ";" + upDetail.SIHCode;

                    var upQty = upDetail.Quantity;
                    int i = 0;
                    foreach (var sihCode in traceCodes)
                    {
                        //判断是否为下架标签
                        if (!downs.Any(t => t.SIHCode == sihCode.SIHLabelCode))
                        {
                            throw new ValidationException(sihCode + "不是已下架标签，请重新扫描");
                        }
                        var downSih = downs.Where(t => t.SIHCode == sihCode.SIHLabelCode).First();
                        var upQtyPc = downSih.Quantity;
                        if (downSih.Quantity > upDetail.Quantity)
                        {
                            upQtyPc = upDetail.Quantity;
                        }
                        var oldT = oldAccutrace.Where(t => t.PartId == item.SparePartId && t.SIHLabelCode == sihCode.SIHLabelCode && t.BoxCode == sihCode.BoxCode && t.WarehouseAreaId == item.DestWarehouseAreaId).FirstOrDefault();
                        if (partsShiftOrders.TraceProperty.HasValue && partsShiftOrders.TraceProperty == (int)DCSTraceProperty.精确追溯)
                        {
                            if (i >= upQty)
                            {
                                continue;
                            }
                            if (oldT != null)
                            {
                                oldT.Status = (int)DCSAccurateTraceStatus.作废;
                                oldT.ModifierId = userInfo.UserId;
                                oldT.ModifierName = userInfo.UserName;
                                oldT.ModifyTime = DateTime.Now;
                            }
                            var newAcc = new AccurateTrace
                            {
                                Type = sihCode.Type,
                                CompanyId = sihCode.CompanyId,
                                CompanyType = sihCode.CompanyType,
                                SourceBillId = sihCode.SourceBillId,
                                PartId = sihCode.PartId,
                                TraceCode = sihCode.TraceCode,
                                SIHLabelCode = sihCode.SIHLabelCode,
                                Status = (int)DCSAccurateTraceStatus.有效,
                                CreateTime = DateTime.Now,
                                CreatorId = userInfo.UserId,
                                CreatorName = userInfo.UserName,
                                TraceProperty = sihCode.TraceProperty,
                                BoxCode = sihCode.BoxCode,
                                InQty = 1,
                                PackingTaskId = sihCode.PackingTaskId,
                                WarehouseAreaId = item.DestWarehouseAreaId,
                                WarehouseAreaCode = item.DeWarehouseAreaCode
                            };
                            this.ObjectContext.AccurateTraces.AddObject(newAcc);
                            var partsShiftSIHDetail = new Sunlight.Silverlight.Dcs.Web.PartsShiftSIHDetail
                            {
                                PartsShiftOrderId = partsShiftOrders.Id,
                                PartsShiftOrderDetailId = item.Id,
                                Type = (int)DCSPartsShiftSIHDetailType.上架,
                                SIHCode = sihCode.SIHLabelCode,
                                Quantity = 1,
                            };
                            this.ObjectContext.PartsShiftSIHDetails.AddObject(partsShiftSIHDetail);
                            i++;
                        }
                        else
                        {
                            if (oldT != null)
                            {
                                oldT.InQty = oldT.InQty + upQtyPc;
                                oldT.ModifierId = userInfo.UserId;
                                oldT.ModifierName = userInfo.UserName;
                                oldT.ModifyTime = DateTime.Now;
                            }
                            else
                            {
                                var newAcc = new AccurateTrace
                                {
                                    Type = sihCode.Type,
                                    CompanyId = sihCode.CompanyId,
                                    CompanyType = sihCode.CompanyType,
                                    SourceBillId = sihCode.SourceBillId,
                                    PartId = sihCode.PartId,
                                    TraceCode = sihCode.TraceCode,
                                    SIHLabelCode = sihCode.SIHLabelCode,
                                    Status = (int)DCSAccurateTraceStatus.有效,
                                    CreateTime = DateTime.Now,
                                    CreatorId = userInfo.UserId,
                                    CreatorName = userInfo.UserName,
                                    TraceProperty = sihCode.TraceProperty,
                                    BoxCode = sihCode.BoxCode,
                                    InQty = upQtyPc,
                                    PackingTaskId = sihCode.PackingTaskId,
                                    WarehouseAreaId = item.DestWarehouseAreaId,
                                    WarehouseAreaCode = item.DeWarehouseAreaCode
                                };
                                this.ObjectContext.AccurateTraces.AddObject(newAcc);
                            }
                            var old = oldPartsShiftSIHDetails.Where(t => t.PartsShiftOrderDetailId == item.Id && t.SIHCode == sihCode.SIHLabelCode).FirstOrDefault();
                            if (old == null)
                            {
                                var partsShiftSIHDetail = new Sunlight.Silverlight.Dcs.Web.PartsShiftSIHDetail
                                {
                                    PartsShiftOrderId = partsShiftOrders.Id,
                                    PartsShiftOrderDetailId = item.Id,
                                    Type = (int)DCSPartsShiftSIHDetailType.上架,
                                    SIHCode = sihCode.SIHLabelCode,
                                    Quantity = upQtyPc,
                                };
                                this.ObjectContext.PartsShiftSIHDetails.AddObject(partsShiftSIHDetail);
                            }
                            else
                            {
                                old.Quantity = old.Quantity + upDetail.Quantity;
                            }
                        }
                    }

                }
                dbpartsShiftOrder.ModifierId = userInfo.UserId;
                dbpartsShiftOrder.ModifierName = userInfo.UserName;
                dbpartsShiftOrder.ModifyTime = DateTime.Now;
                //   校验并调整源库位库存
                foreach (var detail in originalPartsShiftOrderDetails)
                {
                    var partsStock = partsStocksOr.SingleOrDefault(v => v.WarehouseAreaId == detail.OriginalWarehouseAreaId && v.PartId == detail.SparePartId);
                    if (partsStock == null)
                        throw new ValidationException(string.Format("编号为“{0}”的配件，在编号为“{1}”的库位上不存在库存记录", detail.SparePartCode, detail.OriginalWarehouseAreaCode));
                    partsStock.LockedQty = partsStock.LockedQty - detail.Quantity;
                    partsStock.Quantity = partsStock.Quantity - detail.Quantity.Value;
                    if (partsStock.LockedQty < 0 || partsStock.Quantity < 0)
                        throw new ValidationException(string.Format("{0}库存异常", detail.SparePartCode));
                    partsStock.ModifyTime = DateTime.Now;
                    partsStock.ModifierId = userInfo.UserId;
                    partsStock.ModifierName = userInfo.UserName;
                    var partsLockedStock = partsLockedStocks.Where(t => t.PartId == detail.SparePartId && t.WarehouseId == partsStock.WarehouseId).FirstOrDefault();
                    if (partsLockedStock != null)
                    {
                        if (partsLockedStock.LockedQuantity < detail.Quantity.Value)
                        {
                            throw new ValidationException(detail.SparePartCode + "审单锁定量异常");
                        }
                        partsLockedStock.LockedQuantity = partsLockedStock.LockedQuantity - detail.Quantity.Value;
                        partsLockedStock.ModifierId = userInfo.UserId;
                        partsLockedStock.ModifierName = userInfo.UserName;
                        partsLockedStock.ModifyTime = DateTime.Now;
                    }
                }
                //调整目标库位库存
                foreach (var detail in destPartsShiftOrderDetails)
                {
                    var partsStock = partsStocks.SingleOrDefault(v => v.WarehouseAreaId == detail.DestWarehouseAreaId && v.PartId == detail.SparePartId);
                    if (partsStock == null)
                    {
                        var tempWarehouseArea = warehouses.Single(r => r.Id == detail.DestWarehouseAreaId);
                        partsStock = new PartsStock
                        {
                            WarehouseId = dbpartsShiftOrder.WarehouseId,
                            WarehouseAreaCategoryId = tempWarehouseArea.AreaCategoryId,
                            StorageCompanyId = dbpartsShiftOrder.StorageCompanyId,
                            StorageCompanyType = dbpartsShiftOrder.StorageCompanyType,
                            BranchId = dbpartsShiftOrder.BranchId,
                            WarehouseAreaId = detail.DestWarehouseAreaId,
                            PartId = detail.SparePartId,
                            Quantity = detail.Quantity.Value
                        };
                        ObjectContext.PartsStocks.AddObject(partsStock);
                        partsStocks.Add(partsStock);
                    }
                    else
                    {
                        partsStock.Quantity += detail.Quantity.Value;
                    }
                    foreach (var detailWithBatchNumber in detail.detailsWithBatchNumber)
                    {
                        var partsStockBatchDetail = partsStockBatchDetails.Single(v => String.Compare(v.BatchNumber, detailWithBatchNumber.BatchNumber, StringComparison.OrdinalIgnoreCase) == 0 && v.Quantity == detailWithBatchNumber.Quantity);
                        partsStockBatchDetail.PartsStockId = partsStock.Id;
                        partsStock.PartsStockBatchDetails.Add(partsStockBatchDetail);
                    }
                }
                if (dbpartsShiftOrder.PartsShiftOrderDetails.All(t => t.UpShelfQty == t.DownShelfQty && t.UpShelfQty == t.Quantity))
                {
                    dbpartsShiftOrder.ShiftStatus = (int)DCSPartsShiftOrderShiftStatus.移库完成;
                    //若移库清单中存在源库位库存数量=0，则删除配件的仓库库存信息
                    var partsstocks = (from a in ObjectContext.PartsStocks.Include("WarehouseAreaCategory").Where(t => t.StorageCompanyId == userInfo.EnterpriseId && t.WarehouseId == dbpartsShiftOrder.WarehouseId && (t.Quantity == 0 || t.Quantity == null))
                                       join b in ObjectContext.PartsShiftOrderDetails.Where(t => t.PartsShiftOrderId == partsShiftOrders.Id) on new
                                       {
                                           a.PartId,
                                           a.WarehouseAreaId
                                       } equals new
                                       {
                                           PartId = b.SparePartId,
                                           WarehouseAreaId = b.OriginalWarehouseAreaId
                                       }
                                       select a).ToArray();
                    var spareIds = partsstocks.Select(t => t.PartId).ToArray();
                    var allStock = ObjectContext.PartsStocks.Include("WarehouseAreaCategory").Where(r => spareIds.Contains(r.PartId)).ToList();
                    foreach (var partsStock in partsStocks)
                    {
                        if (partsStock.Quantity == 0)
                        {
                            var stock = allStock.Where(r => r.PartId == partsStock.PartId && r.WarehouseAreaCategory.Category == partsStock.WarehouseAreaCategory.Category).ToArray();
                            if (stock.Count() > 1)
                            {
                                var partsStockHistory = new PartsStockHistory
                                {
                                    PartsStockId = partsStock.Id,
                                    WarehouseId = partsStock.WarehouseId,
                                    StorageCompanyId = partsStock.StorageCompanyId,
                                    StorageCompanyType = partsStock.StorageCompanyType,
                                    BranchId = partsStock.BranchId,
                                    WarehouseAreaId = partsStock.WarehouseAreaId,
                                    WarehouseAreaCategoryId = partsStock.WarehouseAreaCategoryId,
                                    PartId = partsStock.PartId,
                                    Quantity = partsStock.Quantity,
                                    Remark = partsStock.Remark,
                                    CreatorId = partsStock.CreatorId,
                                    CreatorName = partsStock.CreatorName,
                                    CreateTime = partsStock.CreateTime,
                                    ModifierId = partsStock.ModifierId,
                                    ModifierName = partsStock.ModifierName,
                                    ModifyTime = partsStock.ModifyTime
                                };
                                this.ObjectContext.PartsStockHistories.AddObject(partsStockHistory);
                                this.ObjectContext.PartsStocks.DeleteObject(partsStock);
                                allStock.Remove(partsStock);
                                //  DeleteFromDatabase(partsStock);
                            }

                        }
                    }
                }
                else
                    dbpartsShiftOrder.ShiftStatus = (int)DCSPartsShiftOrderShiftStatus.部分移库;
            }
            ObjectContext.SaveChanges();
            return true;
        }
        //点击下架按钮，查询可以下架的移库单号
        public List<PartsShiftOrderDTO> GetDownPartsShift(string code)
        {
            var userInfo = FormsAuth.GetUserData();
            var shift = ObjectContext.PartsShiftOrders.Where(t => t.Status == (int)DcsPartsShiftOrderStatus.生效 && (t.ShiftStatus == (int)DCSPartsShiftOrderShiftStatus.部分移库 || t.ShiftStatus == (int)DCSPartsShiftOrderShiftStatus.待移库) && t.StorageCompanyId == userInfo.EnterpriseId && ObjectContext.PartsShiftOrderDetails.Any(p => t.Id == p.PartsShiftOrderId && (p.Quantity > p.DownShelfQty || p.DownShelfQty == null)));
            if (!string.IsNullOrEmpty(code))
            {
                shift = shift.Where(t => t.Code.ToUpper() == code.ToUpper());
            }
            var result = (from a in shift
                          join b in ObjectContext.PartsShiftOrderDetails on a.Id equals b.PartsShiftOrderId
                          join c in ObjectContext.SpareParts on b.SparePartId equals c.Id
                          select new PartsShiftOrderDTO
                          {
                              Id = a.Id,
                              Code = a.Code,
                              ShiftStatus = a.ShiftStatus.Value,
                              SparePartId = b.SparePartId,
                              SparePartCode = b.SparePartCode,
                              SparePartName = b.SparePartName,
                              OriginalWarehouseAreaCode = b.OriginalWarehouseAreaCode,
                              DestWarehouseAreaCode = b.DestWarehouseAreaCode,
                              DestWarehouseAreaId = b.DestWarehouseAreaId,
                              OriginalWarehouseAreaId = b.OriginalWarehouseAreaId,
                              TraceProperty = c.TraceProperty,
                              DownShelfQty = b.DownShelfQty.Value,
                              UpShelfQty = b.UpShelfQty.Value,
                              Quantity = b.Quantity,
                          }).ToList();
            return result.OrderBy(t => t.OriginalWarehouseAreaCode).ThenBy(r => r.SparePartCode).ToList();
        }
        //下架界面中，点击上传按钮后，更新下架数据
        public bool PartsShiftDowning(List<PartsShiftOrderDTO> inPutPartsShiftOrders)
        {
            var userInfo = FormsAuth.GetUserData();
            var spids = inPutPartsShiftOrders.Select(t => t.SparePartId).ToArray();
            var partslockedstocks = ObjectContext.PartsLockedStocks.Where(y => spids.Contains(y.PartId)).ToList();
            foreach (var partsShiftOrder in inPutPartsShiftOrders)
            {
                if (partsShiftOrder.PartsShiftSIHDetails == null || partsShiftOrder.PartsShiftSIHDetails.Count() == 0)
                {
                    continue;
                }
                var dbpartsShiftOrder = ObjectContext.PartsShiftOrders.Include("PartsShiftOrderDetails").Where(r => (r.ShiftStatus == (int)DCSPartsShiftOrderShiftStatus.部分移库 || r.ShiftStatus == (int)DCSPartsShiftOrderShiftStatus.待移库) && r.Status == (int)DcsPartsShiftOrderStatus.生效 && r.Id == partsShiftOrder.Id).SingleOrDefault();
                if (dbpartsShiftOrder == null)
                    throw new ValidationException("未找到有效的可下架的单据");
                var oldPartsShiftOrderDetail = dbpartsShiftOrder.PartsShiftOrderDetails.Where(t => t.SparePartId == partsShiftOrder.SparePartId && t.OriginalWarehouseAreaId == partsShiftOrder.OriginalWarehouseAreaId && t.DestWarehouseAreaId == partsShiftOrder.DestWarehouseAreaId).ToArray();
                var codes = ObjectContext.AccurateTraces.Where(t => partsShiftOrder.SparePartId == t.PartId.Value && t.CompanyId == userInfo.EnterpriseId && t.Status == (int)DCSAccurateTraceStatus.有效 && t.InQty > 0 && (t.InQty != t.OutQty || t.OutQty == null)).ToArray();
                var originalWarehouseAreaIds = partsShiftOrder.OriginalWarehouseAreaId;
                var warehouseAreaIds = partsShiftOrder.DestWarehouseAreaId;
                var partsStocks = GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(r => r.WarehouseId == dbpartsShiftOrder.WarehouseId && partsShiftOrder.SparePartId == r.PartId && partsShiftOrder.OriginalWarehouseAreaId == r.WarehouseAreaId)).ToList();
                var oldPartsShiftSIHDetails = ObjectContext.PartsShiftSIHDetails.Where(t => t.PartsShiftOrderId == dbpartsShiftOrder.Id && t.Type == (int)DCSPartsShiftSIHDetailType.下架).ToArray();
                oldPartsShiftOrderDetail.First().CourrentDownShelfQty = partsShiftOrder.PartsShiftSIHDetails.Sum(t => t.Quantity);
                var item = oldPartsShiftOrderDetail.First();
                if (item.DownShelfQty == null)
                {
                    item.DownShelfQty = 0;
                }
                if ((item.Quantity - item.DownShelfQty) < partsShiftOrder.PartsShiftSIHDetails.Sum(t => t.Quantity))
                {
                    throw new ValidationException("下架数量大于未下架数量");
                }
                foreach (var downdetail in partsShiftOrder.PartsShiftSIHDetails)
                {
                    List<AccurateTrace> traceCodes = new List<AccurateTrace>();
                    var insetCode = codes.Where(t => downdetail.SIHCode == t.SIHLabelCode && t.PartId == item.SparePartId && (t.TraceProperty == (int)DCSTraceProperty.精确追溯 || ((t.TraceProperty == (int)DCSTraceProperty.批次追溯 || t.TraceProperty == null) && t.InQty >= downdetail.Quantity)) && (t.WarehouseAreaId == item.OriginalWarehouseAreaId || t.WarehouseAreaId == null)).ToArray();
                    if (downdetail.SIHCode.EndsWith("X"))
                    {
                        insetCode = codes.Where(t => downdetail.SIHCode == t.BoxCode && t.PartId == item.SparePartId && (t.TraceProperty == (int)DCSTraceProperty.精确追溯 || ((t.TraceProperty == (int)DCSTraceProperty.批次追溯 || t.TraceProperty == null) && t.InQty >= downdetail.Quantity)) && (t.WarehouseAreaId == item.OriginalWarehouseAreaId || t.WarehouseAreaId == null)).ToArray();
                    }
                    var qty = 0;
                    if (item.TraceProperty.HasValue && item.TraceProperty == (int)DCSTraceProperty.精确追溯)
                    {
                        qty = 1;
                    }
                    else
                        qty = downdetail.Quantity.Value;
                    if (insetCode == null || insetCode.Count() == 0)
                    {
                        var old = oldPartsShiftSIHDetails.Where(t => t.PartsShiftOrderDetailId == item.Id && t.SIHCode == downdetail.SIHCode).FirstOrDefault();
                        if (old == null)
                        {
                            var partsShiftSIHDetail = new Sunlight.Silverlight.Dcs.Web.PartsShiftSIHDetail
                            {
                                PartsShiftOrderId = dbpartsShiftOrder.Id,
                                PartsShiftOrderDetailId = item.Id,
                                Type = (int)DCSPartsShiftSIHDetailType.下架,
                                SIHCode = downdetail.SIHCode,
                                Quantity = qty,
                            };
                            this.ObjectContext.PartsShiftSIHDetails.AddObject(partsShiftSIHDetail);
                        }
                        else
                        {
                            old.Quantity = old.Quantity + qty;
                        }
                    }
                    else
                    {

                        foreach (var insertCode in insetCode)
                        {
                            if (insertCode.OutQty == null)
                            {
                                insertCode.OutQty = 0;
                            }
                            traceCodes.Add(insertCode);
                        }
                    }
                    if (item.DownShelfQty == null)
                    {
                        item.DownShelfQty = 0;
                    }
                    item.DownShelfQty = item.DownShelfQty + downdetail.Quantity;
                    if (traceCodes.Count() == 0)
                    {
                        continue;
                    }
                    if (traceCodes == null || traceCodes.Count() == 0 || (partsShiftOrder.TraceProperty.HasValue && traceCodes.Count() != downdetail.Quantity && partsShiftOrder.TraceProperty == (int)DCSTraceProperty.精确追溯))
                    {
                        throw new ValidationException(item.SparePartCode + "未找到有效的SIH标签码" + downdetail.SIHCode);
                    }
                    if (downdetail.SIHCode.EndsWith("X"))
                    {
                        if ((partsShiftOrder.TraceProperty == (int)DCSTraceProperty.批次追溯 || partsShiftOrder.TraceProperty == null) && traceCodes.Sum(t => t.InQty - t.OutQty) != downdetail.Quantity)
                        {
                            throw new ValidationException("SIH标签码" + downdetail.SIHCode + "未出库数量不等于本次下架数量");
                        }
                    }
                    else
                    {
                        if ((partsShiftOrder.TraceProperty == (int)DCSTraceProperty.批次追溯 || partsShiftOrder.TraceProperty == null) && (traceCodes.First().InQty - traceCodes.First().OutQty) < downdetail.Quantity)
                        {
                            throw new ValidationException("SIH标签码" + downdetail.SIHCode + "未出库数量小于本次下架数量");
                        }
                    }


                    if (item.Quantity < item.DownShelfQty)
                    {
                        throw new ValidationException(item.SparePartCode + "下架数量不允许大于移库数量");
                    }
                    if (!string.IsNullOrEmpty(item.DownSIHCode) && item.DownSIHCode.Contains(downdetail.SIHCode) && partsShiftOrder.TraceProperty.HasValue && partsShiftOrder.TraceProperty == (int)DCSTraceProperty.精确追溯)
                    {
                        throw new ValidationException(downdetail.SIHCode + "已下架，请重新扫描");
                    }
                    if (string.IsNullOrEmpty(item.DownSIHCode))
                    {
                        item.DownSIHCode = downdetail.SIHCode;
                    }
                    else if (!item.DownSIHCode.Contains(downdetail.SIHCode))
                        item.DownSIHCode = item.DownSIHCode + ";" + downdetail.SIHCode;
                    var upQty = downdetail.Quantity;
                    int i = 0;
                    var pcQty = downdetail.Quantity;
                    foreach (var sihCode in traceCodes)
                    {
                        if (partsShiftOrder.TraceProperty.HasValue && partsShiftOrder.TraceProperty == (int)DCSTraceProperty.精确追溯)
                        {
                            if (i >= upQty)
                            {
                                continue;
                            }
                            sihCode.OutQty = 1;
                            sihCode.ModifierId = userInfo.UserId;
                            sihCode.ModifierName = userInfo.UserName;
                            sihCode.ModifyTime = DateTime.Now;
                            var partsShiftSIHDetail = new Sunlight.Silverlight.Dcs.Web.PartsShiftSIHDetail
                            {
                                PartsShiftOrderId = dbpartsShiftOrder.Id,
                                PartsShiftOrderDetailId = item.Id,
                                Type = (int)DCSPartsShiftSIHDetailType.下架,
                                SIHCode = sihCode.SIHLabelCode,
                                Quantity = 1,
                            };
                            this.ObjectContext.PartsShiftSIHDetails.AddObject(partsShiftSIHDetail);
                            i++;
                        }
                        else
                        {
                            if (pcQty == 0)
                            {
                                continue;
                            }
                            var curQty = 0;
                            if (pcQty > (sihCode.InQty - sihCode.OutQty))
                            {
                                curQty = sihCode.InQty.Value - sihCode.OutQty.Value;
                            }
                            else
                            {
                                curQty = pcQty.Value;
                            }
                            sihCode.OutQty = sihCode.OutQty + pcQty;
                            sihCode.ModifierId = userInfo.UserId;
                            sihCode.ModifierName = userInfo.UserName;
                            sihCode.ModifyTime = DateTime.Now;
                            var old = oldPartsShiftSIHDetails.Where(t => t.PartsShiftOrderDetailId == item.Id && t.SIHCode == sihCode.SIHLabelCode).FirstOrDefault();
                            if (old == null)
                            {
                                var partsShiftSIHDetail = new Sunlight.Silverlight.Dcs.Web.PartsShiftSIHDetail
                                {
                                    PartsShiftOrderId = dbpartsShiftOrder.Id,
                                    PartsShiftOrderDetailId = item.Id,
                                    Type = (int)DCSPartsShiftSIHDetailType.下架,
                                    SIHCode = sihCode.SIHLabelCode,
                                    Quantity = curQty
                                };
                                this.ObjectContext.PartsShiftSIHDetails.AddObject(partsShiftSIHDetail);
                            }
                            else
                            {
                                old.Quantity = old.Quantity + curQty;
                            }
                            pcQty = pcQty - curQty;
                        }
                    }

                }
                dbpartsShiftOrder.ModifierId = userInfo.UserId;
                dbpartsShiftOrder.ModifierName = userInfo.UserName;
                dbpartsShiftOrder.ModifyTime = DateTime.Now;
                //汇总源库位配件
                var originalPartsShiftOrderDetails = oldPartsShiftOrderDetail.GroupBy(v => new
                {
                    v.SparePartId,
                    v.OriginalWarehouseAreaId
                }).Select(v => new
                {
                    v.Key.SparePartId,
                    v.Key.OriginalWarehouseAreaId,
                    v.First().SparePartCode,
                    v.First().OriginalWarehouseAreaCode,
                    Quantity = v.Sum(detail => detail.CourrentDownShelfQty),
                    detailsWithBatchNumber = v.Where(detail => !string.IsNullOrEmpty(detail.BatchNumber)).ToArray(),
                }).ToArray();
                #region 校验是否有未完成的盘点单
                var originalWarehouseArea = originalPartsShiftOrderDetails.Select(r => r.OriginalWarehouseAreaId).ToArray();
                var checkInventoryTemp = (from a in ObjectContext.PartsInventoryBills.Where(r => r.WarehouseId == dbpartsShiftOrder.WarehouseId && r.Status == (int)DcsPartsInventoryBillStatus.已审批)
                                          from b in a.PartsInventoryDetails
                                          where originalWarehouseArea.Any(v => v == b.WarehouseAreaId)
                                          select b).ToArray();
                var check = checkInventoryTemp.Where(r => originalPartsShiftOrderDetails.Any(v => v.OriginalWarehouseAreaId == r.WarehouseAreaId && v.SparePartId == r.SparePartId)).ToArray();

                if (check.Length > 0)
                {
                    var errorsparePartString = string.Join(",", check.Select(r => r.SparePartCode).ToArray());
                    throw new ValidationException("存在未完成的配件盘点单，请完成盘点单后再移库");
                }
                #endregion
                //获取配件库存批次明细
                var partsShiftOrderDetailsWithBatchNumber = oldPartsShiftOrderDetail.Where(r => !string.IsNullOrEmpty(r.BatchNumber)).ToArray();
                var batchNumbers = partsShiftOrderDetailsWithBatchNumber.Select(r => r.BatchNumber.ToLower());
                var partsStockBatchDetails = ObjectContext.PartsStockBatchDetails.Where(r => batchNumbers.Contains(r.BatchNumber.ToLower())).ToArray();

                //新增配件库位变更履历（这个新增不用校验）移库才记履历
                if (dbpartsShiftOrder.Type == (int)DcsPartsShiftOrderType.正常移库 || dbpartsShiftOrder.Type == (int)DcsPartsShiftOrderType.问题区移库)
                {
                    var warehouseAreaHistories = new List<WarehouseAreaHistory>();
                    foreach (var detail in oldPartsShiftOrderDetail)
                    {
                        var t = ObjectContext.WarehouseAreaCategories.FirstOrDefault(r => r.Category == detail.DestWarehouseAreaCategory);
                        if (t == null)
                        {
                            throw new ValidationException("找不到对应的库存用途");
                        }
                        var warehouseAreaHistory = new WarehouseAreaHistory();
                        warehouseAreaHistory.WarehouseId = dbpartsShiftOrder.WarehouseId;
                        warehouseAreaHistory.StorageCompanyId = dbpartsShiftOrder.StorageCompanyId;
                        warehouseAreaHistory.StorageCompanyType = dbpartsShiftOrder.StorageCompanyType;
                        warehouseAreaHistory.BranchId = dbpartsShiftOrder.BranchId;
                        warehouseAreaHistory.WarehouseAreaId = detail.OriginalWarehouseAreaId;
                        warehouseAreaHistory.WarehouseAreaCategoryId = detail.OriginalWarehouseAreaCategory;
                        warehouseAreaHistory.DestWarehouseAreaId = detail.DestWarehouseAreaId;
                        warehouseAreaHistory.DestWarehouseAreaCategoryId = t.Id;
                        warehouseAreaHistory.PartId = detail.SparePartId;
                        warehouseAreaHistory.Quantity = detail.CourrentDownShelfQty.Value;
                        warehouseAreaHistories.Add(warehouseAreaHistory);
                    }
                    foreach (var his in warehouseAreaHistories)
                    {
                        his.CreatorId = userInfo.UserId;
                        his.CreatorName = userInfo.UserName;
                        his.CreateTime = DateTime.Now;
                        ObjectContext.WarehouseAreaHistories.AddObject(his);
                    }
                }

                //   校验并调整源库位库存
                foreach (var detail in originalPartsShiftOrderDetails)
                {
                    var partsStock = partsStocks.SingleOrDefault(v => v.WarehouseAreaId == detail.OriginalWarehouseAreaId && v.PartId == detail.SparePartId);
                    if (partsStock == null)
                        throw new ValidationException(string.Format("编号为“{0}”的配件，在编号为“{1}”的库位上不存在库存记录", detail.SparePartCode, detail.OriginalWarehouseAreaCode));
                    if (partsStock.LockedQty == null)
                        partsStock.LockedQty = 0;
                    var partslockedstock = partslockedstocks.Where(t => t.PartId == detail.SparePartId && t.WarehouseId == dbpartsShiftOrder.WarehouseId).FirstOrDefault();
                    if (partslockedstock == null)
                    {
                        if ((partsStock.Quantity) < detail.Quantity)
                            throw new ValidationException(string.Format("编号为“{0}”的配件，在编号为“{1}”的库位上的库存，不足以满足本次移库要求", detail.SparePartCode, detail.OriginalWarehouseAreaCode));
                        //新增审单锁定量
                        var newPartsLockedStock = new PartsLockedStock
                        {
                            WarehouseId = dbpartsShiftOrder.WarehouseId,
                            StorageCompanyId = userInfo.EnterpriseId,
                            BranchId = userInfo.EnterpriseId,
                            PartId = detail.SparePartId,
                            LockedQuantity = detail.Quantity.Value,
                            CreateTime = DateTime.Now,
                            CreatorId = userInfo.UserId,
                            CreatorName = userInfo.UserCode
                        };
                        ObjectContext.PartsLockedStocks.AddObject(newPartsLockedStock);
                        partslockedstocks.Add(newPartsLockedStock);
                    }
                    else
                    {
                        if ((partsStock.Quantity - partslockedstock.LockedQuantity) < detail.Quantity)
                            throw new ValidationException(string.Format("编号为“{0}”的配件，在编号为“{1}”的库位上的库存，不足以满足本次移库要求", detail.SparePartCode, detail.OriginalWarehouseAreaCode));
                        partslockedstock.LockedQuantity = partslockedstock.LockedQuantity + detail.Quantity.Value;
                        partslockedstock.ModifierId = userInfo.UserId;
                        partslockedstock.ModifierName = userInfo.UserName;
                        partslockedstock.ModifyTime = DateTime.Now;
                    }

                    //  partsStock.Quantity -= detail.Quantity.Value;
                    partsStock.LockedQty = partsStock.LockedQty + detail.Quantity.Value;
                    partsStock.ModifyTime = DateTime.Now;
                    partsStock.ModifierId = userInfo.UserId;
                    partsStock.ModifierName = userInfo.UserName;

                    foreach (var detailWithBatchNumber in detail.detailsWithBatchNumber)
                    {
                        var partsStockBatchDetail = partsStockBatchDetails.SingleOrDefault(v => String.Compare(v.BatchNumber, detailWithBatchNumber.BatchNumber, StringComparison.OrdinalIgnoreCase) == 0 && v.Quantity == detailWithBatchNumber.Quantity);
                        if (partsStockBatchDetail == null)
                            throw new ValidationException(string.Format("批次号为“{0}”配件库存批次不存在", detailWithBatchNumber.BatchNumber));
                        if (partsStock.Id != partsStockBatchDetail.PartsStockId)
                            throw new ValidationException(string.Format("批次号为“{0}”配件库存批次不在指定库位上", detailWithBatchNumber.BatchNumber));
                    }
                }
            }
            ObjectContext.SaveChanges();
            return true;
        }

        /// <summary>
        /// 随移
        /// </summary>
        /// <param name="warehouseId">库位id</param>
        /// <param name="details"></param>
        /// <returns></returns>
        public bool MoveShiftLibrary(List<PartsShiftCustomizationDetailDTO> details)
        {
            var userInfo = FormsAuth.GetUserData();
            var partsStockIds = details.Select(x => x.PartsStockId).ToList();
            //把查出的所有备件清单加行级锁
            var partsStocks = GetPartsStocksWithLock(ObjectContext.PartsStocks.Include("Warehouse").Where(r => partsStockIds.Contains(r.Id))).ToList();
            var partsStockParts = partsStocks.Select(x => x.PartId).ToList();
            var warehouseId = partsStocks.FirstOrDefault()?.WarehouseId;

            //查询出保管区所有目标库位
            var destWarehouseAreaCodes = details.Select(x => x.DestWarehouseAreaCode).ToList();
            var warehouseAreas = ObjectContext.WarehouseAreas.Where(x => x.WarehouseId == warehouseId && destWarehouseAreaCodes.Contains(x.Code) && x.Status != (int)DcsBaseDataStatus.作废
                                                    && ObjectContext.WarehouseAreaCategories.Any(t => t.Category == (int)DcsAreaType.保管区 && x.AreaCategoryId == t.Id)).ToList();

            //目标库位 库存
            var destpartsStocks = GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(x => x.WarehouseId == warehouseId
                                                                                                 && destWarehouseAreaCodes.Contains(x.WarehouseArea.Code)
                                                                                                         && partsStockParts.Contains(x.PartId))).ToList();
            //所有的追溯备件信息
            //  var accurateTraces = ObjectContext.AccurateTraces.Where(x => partsStockParts.Contains(x.PartId.Value)).ToList();
            PartsShiftOrder partshiftOrder = null;
            foreach (var rowInfo in details)
            {
                var partsStock = partsStocks.FirstOrDefault(x => x.Id == rowInfo.PartsStockId);
                if (partsStock == null)
                    throw new ValidationException("不存在库存信息");
                if ((partsStock.Quantity - (partsStock.LockedQty ?? 0)) < rowInfo.Quantity)
                    throw new ValidationException(string.Format("{0}备件数量不可大于可移数量", rowInfo.SparePartCode));

                var destWarehouseArea = warehouseAreas.FirstOrDefault(x => x.Code == rowInfo.DestWarehouseAreaCode);
                if (destWarehouseArea == null)
                    throw new ValidationException(string.Format("{0}目标库位不属于保管区库位或不属于相同仓库", rowInfo.DestWarehouseAreaCode));

                if (partshiftOrder == null)
                {
                    partshiftOrder = new PartsShiftOrder
                    {
                        Code = CodeGenerator.Generate("PartsShiftOrder", userInfo.EnterpriseCode),
                        WarehouseCode = partsStock.Warehouse.Code,
                        WarehouseId = partsStock.Warehouse.Id,
                        WarehouseName = partsStock.Warehouse.Name,
                        StorageCompanyCode = userInfo.EnterpriseCode,
                        StorageCompanyId = userInfo.EnterpriseId,
                        StorageCompanyName = userInfo.EnterpriseName,
                        StorageCompanyType = userInfo.EnterpriseType,
                        Status = (int)DcsPartsShiftOrderStatus.生效,
                        Type = (int)DcsPartsShiftOrderType.正常移库,
                        ShiftStatus = (int)DCSPartsShiftOrderShiftStatus.移库完成,
                        BranchId = partsStock.BranchId,
                        CreatorId = userInfo.UserId,
                        CreateTime = DateTime.Now,
                        CreatorName = userInfo.UserName,
                        SubmitterId = userInfo.UserId,
                        SubmitterName = userInfo.UserName,
                        SubmitTime = DateTime.Now,
                        InitialApproverId = userInfo.UserId,
                        InitialApproverName = userInfo.UserName,
                        InitialApproveTime = DateTime.Now,
                        ApproverId = userInfo.UserId,
                        ApproverName = userInfo.UserName,
                        ApproveTime = DateTime.Now
                    };
                }

                //移库单清单新增
                var partsshiftdetail = new PartsShiftOrderDetail
                {
                    SparePartCode = partsStock.SparePart.Code,
                    SparePartId = partsStock.SparePart.Id,
                    SparePartName = partsStock.SparePart.Name,
                    OriginalWarehouseAreaId = partsStock.WarehouseArea.Id,
                    OriginalWarehouseAreaCode = partsStock.WarehouseArea.Code,
                    OriginalWarehouseAreaCategory = (int)DcsAreaType.保管区,
                    DestWarehouseAreaId = destWarehouseArea.Id,
                    DestWarehouseAreaCode = destWarehouseArea.Code,
                    DestWarehouseAreaCategory = (int)DcsAreaType.保管区,
                    Quantity = rowInfo.Quantity,
                    UpShelfQty = rowInfo.Quantity,
                    DownShelfQty = rowInfo.Quantity
                };
                partshiftOrder.PartsShiftOrderDetails.Add(partsshiftdetail);

                //目标库位是否存在相同备件信息
                var destpartsStock = destpartsStocks.FirstOrDefault(x => x.WarehouseArea.Code == rowInfo.DestWarehouseAreaCode && x.PartId == partsStock.PartId);
                if (destpartsStock != null)
                {
                    destpartsStock.Quantity += rowInfo.Quantity;
                }
                else
                {
                    var newPartsStock = new PartsStock
                    {
                        WarehouseId = partsStock.WarehouseId,
                        WarehouseAreaCategoryId = partsStock.WarehouseAreaCategoryId,
                        StorageCompanyId = userInfo.EnterpriseId,
                        StorageCompanyType = userInfo.EnterpriseType,
                        BranchId = partsStock.BranchId,
                        WarehouseAreaId = destWarehouseArea.Id,
                        PartId = partsStock.PartId,
                        Quantity = rowInfo.Quantity,
                        CreateTime = DateTime.Now,
                        CreatorId = userInfo.UserId,
                        CreatorName = userInfo.UserName
                    };
                    ObjectContext.PartsStocks.AddObject(newPartsStock);
                }

                //源库存扣减
                partsStock.Quantity -= rowInfo.Quantity;
            }
            ObjectContext.PartsShiftOrders.AddObject(partshiftOrder);
            ObjectContext.SaveChanges();

            var sihDetails = new List<PartsShiftSIHDetail>();
            foreach (var rowInfo in details)
            {
                var detials = partshiftOrder.PartsShiftOrderDetails.FirstOrDefault(o => o.OriginalWarehouseAreaCode == rowInfo.OriginalWarehouseAreaCode && o.SparePartCode == rowInfo.SparePartCode);
                foreach (var item in rowInfo.SIHCodes)
                {
                    var sihDetail = sihDetails.FirstOrDefault(o => o.SIHCode == item && o.Type == (int)DCSPartsShiftSIHDetailType.上架);
                    var codeAndNum = item.Split('|');
                    if (sihDetail != null && sihDetail.Type == (int)DCSPartsShiftSIHDetailType.上架)
                        sihDetail.Quantity += Convert.ToInt32(codeAndNum[1]);
                    else
                    {
                        //上架
                        var partsShiftSIHDetail = new PartsShiftSIHDetail
                        {
                            PartsShiftOrderId = detials.PartsShiftOrderId,
                            PartsShiftOrderDetailId = detials.Id,
                            Type = (int)DCSPartsShiftSIHDetailType.上架,
                            SIHCode = item,
                            Quantity = Convert.ToInt32(codeAndNum[1])
                        };

                        sihDetails.Add(partsShiftSIHDetail);
                    }

                }
                foreach (var item in rowInfo.SIHCodes)
                {
                    var sihDetail = sihDetails.FirstOrDefault(o => o.SIHCode == item && o.Type == (int)DCSPartsShiftSIHDetailType.下架);
                    var codeAndNum = item.Split('|');

                    if (sihDetail != null && sihDetail.Type == (int)DCSPartsShiftSIHDetailType.下架)
                        sihDetail.Quantity += Convert.ToInt32(codeAndNum[1]);
                    else
                    {
                        //下架
                        var downPartsShiftSIHDetail = new PartsShiftSIHDetail
                        {
                            PartsShiftOrderId = detials.PartsShiftOrderId,
                            PartsShiftOrderDetailId = detials.Id,
                            Type = (int)DCSPartsShiftSIHDetailType.下架,
                            SIHCode = item,
                            Quantity = Convert.ToInt32(codeAndNum[1])
                        };
                        sihDetails.Add(downPartsShiftSIHDetail);
                    }
                }
            }
            sihDetails.ForEach(o =>
            {
                ObjectContext.PartsShiftSIHDetails.AddObject(o);
            });
            ObjectContext.SaveChanges();
            return true;
        }
    }
}



