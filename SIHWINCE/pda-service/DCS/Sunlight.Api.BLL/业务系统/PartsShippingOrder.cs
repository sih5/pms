﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Transactions;
using Sunlight.Api.Models;
using Sunlight.Api.Security;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Api.BLL {
    public partial class DcsServiceImp {

        public List<PartsShippingOrderModel> 根据编号查询待收货确认发运单(string code, int warehouseId) {
            var result = (from ps in ObjectContext.PartsShippingOrders.Where(o => o.Code.IndexOf(code) >= 0 && o.ReceivingWarehouseId == warehouseId)
                          join psd in ObjectContext.PartsShippingOrderDetails on ps.Id equals psd.PartsShippingOrderId
                          select new PartsShippingOrderModel {
                              Id = ps.Id,
                              Code = ps.Code,
                              Type = ps.Type,
                              DetailId = psd.Id,
                              PartsId = psd.SparePartId,
                              PartsCode = psd.SparePartCode,
                              PartsName = psd.SparePartName,
                              ContainerNumber = psd.ContainerNumber,
                              ShippingAmount = psd.ShippingAmount,
                              ConfirmedAmount = psd.ConfirmedAmount ?? 0,
                              Status = ps.Status,
                              PartsOutboundPlanId = psd.PartsOutboundPlanId ?? 0
                          }).ToList();
            foreach(var item in result) {
                if(item.Status != (int)DcsPartsShippingOrderStatus.已发货) {
                    throw new ValidationException("只能收货，已发货的配件发运单！");
                }
                if(item.Type != (int)DcsPartsShippingOrderType.销售) {
                    throw new ValidationException("只能收货，类型为销售的配件发运单！");
                }
            }
            return result;
        }

        public bool 收货确认配件发运单(List<PartsShippingOrderModel> partsShippingOrderModels) {
            var userInfo = FormsAuth.GetUserData();
            using(var dcsEntities = new DcsEntities()) {
                var now = DateTime.Now;
                var partsShippingOrderId = partsShippingOrderModels[0].Id;
                //校验状态是否为新建 已发货
                var partsShippingOrder = dcsEntities.PartsShippingOrders.FirstOrDefault(r => r.Id == partsShippingOrderId && r.Status == (int)DcsPartsShippingOrderStatus.已发货);
                if(partsShippingOrder == null)
                    throw new ValidationException("只能完成处于“新建”状态的配件发运单");
                partsShippingOrder.Status = (int)DcsPartsShippingOrderStatus.收货确认;
                partsShippingOrder.ConsigneeId = userInfo.UserId;
                partsShippingOrder.ConsigneeName = userInfo.UserName;
                partsShippingOrder.ConfirmedReceptionTime = now;
                partsShippingOrder.ModifierId = userInfo.UserId;
                partsShippingOrder.ModifierName = userInfo.UserName;
                partsShippingOrder.ModifyTime = now;
                if(partsShippingOrder.IsTransportLosses.HasValue && partsShippingOrder.IsTransportLosses.Value) {
                    partsShippingOrder.TransportLossesDisposeStatus = (int)DcsPartsShippingOrderTransportLossesDisposeStatus.未处理;
                }

                var partsOutboundBills = ObjectContext.PartsOutboundBills.Where(r => r.PartsShippingOrderId == partsShippingOrder.Id).ToList();
                if(partsOutboundBills.Count == 0) {
                    throw new ValidationException("该配件发运单对应的关联中的配件出库单不存在");
                }
                var partsOutboundBillIds = partsOutboundBills.Select(r => r.Id);
                var partsSalesCategoryId = partsOutboundBills[0].PartsSalesCategory.Id;
                var partsSalesCategoryName = partsOutboundBills[0].PartsSalesCategory.Name;
                if(partsShippingOrder.ReceivingWarehouseId.HasValue && partsShippingOrderModels.Sum(r => r.ConfirmedAmount) > 0) {
                    //判断仓库
                    var warehouse = dcsEntities.Warehouses.FirstOrDefault(r => r.Id == partsShippingOrder.ReceivingWarehouseId && r.Status == (int)DcsBaseDataStatus.有效);
                    if(warehouse == null)
                        throw new ValidationException("配件发运单对应的收货仓库不存在");
                    //查询企业
                    var company = dcsEntities.Companies.FirstOrDefault(v => v.Id == warehouse.StorageCompanyId && v.Status == (int)DcsMasterDataStatus.有效);
                    if(company == null)
                        throw new ValidationException("收货仓库所属仓储企业不存在");
                    //查询营销分公司
                    var branch = dcsEntities.Branches.SingleOrDefault(r => r.Id == partsShippingOrder.BranchId && r.Status == (int)DcsMasterDataStatus.有效);
                    if(branch == null)
                        throw new ValidationException("配件发运单对应的营销分公司不存在");
                    if(!partsShippingOrder.OriginalRequirementBillId.HasValue)
                        throw new ValidationException("配件发运单原始需求单据不能为空");
                    if(!partsShippingOrder.OriginalRequirementBillType.HasValue)
                        throw new ValidationException("配件发运单原始需求单据类型不能为空");
                    var partsOutboundPlanIds = partsShippingOrder.PartsShippingOrderDetails.Select(o => o.PartsOutboundPlanId).Distinct();
                    var partsOutboundPlans = ObjectContext.PartsOutboundPlans.Where(r => partsOutboundPlanIds.Contains(r.Id));
                    //新增收货单位入库追溯
                    var outTraces = ObjectContext.AccurateTraces.Where(t => partsOutboundPlanIds.Contains(t.SourceBillId) && t.Type == (int)DCSAccurateTraceType.出库 && t.Status == (int)DCSAccurateTraceStatus.有效).ToArray();
                    var oldInTrace = ObjectContext.AccurateTraces.Where(t => t.CompanyId == company.Id && t.Type == (int)DCSAccurateTraceType.入库 && t.Status == (int)DCSAccurateTraceStatus.有效 && t.InQty == t.OutQty && t.OutQty > 0).ToArray();
                    foreach(var partsOutboundPlan in partsOutboundPlans) {
                        var partsInboundPlan = new PartsInboundPlan {
                            WarehouseId = partsShippingOrder.ReceivingWarehouseId.Value,
                            WarehouseCode = partsShippingOrder.ReceivingWarehouseCode,
                            WarehouseName = partsShippingOrder.ReceivingWarehouseName,
                            StorageCompanyId = company.Id,
                            StorageCompanyCode = company.Code,
                            StorageCompanyName = company.Name,
                            StorageCompanyType = company.Type,
                            BranchId = partsShippingOrder.BranchId,
                            BranchCode = branch.Code,
                            BranchName = branch.Name,
                            CounterpartCompanyId = partsShippingOrder.SettlementCompanyId,
                            CounterpartCompanyCode = partsShippingOrder.SettlementCompanyCode,
                            CounterpartCompanyName = partsShippingOrder.SettlementCompanyName,
                            SourceId = partsShippingOrder.Id,
                            SourceCode = partsShippingOrder.Code,
                            Status = (int)DcsPartsInboundPlanStatus.新建,
                            OriginalRequirementBillId = partsShippingOrder.OriginalRequirementBillId.Value,
                            OriginalRequirementBillCode = partsShippingOrder.OriginalRequirementBillCode,
                            OriginalRequirementBillType = partsShippingOrder.OriginalRequirementBillType.Value,
                            Remark = partsShippingOrder.Remark,
                            SAPPurchasePlanCode = partsShippingOrder.SAPPurchasePlanCode,
                            GPMSPurOrderCode = partsShippingOrder.GPMSPurOrderCode

                        };
                        SalesUnit saleUnit;
                        Dictionary<int, string> POCodeRef = new Dictionary<int, string>();
                        switch(partsShippingOrder.Type) {
                            case (int)DcsPartsShippingOrderType.销售:
                                partsInboundPlan.InboundType = (int)DcsPartsInboundType.配件采购;
                                partsInboundPlan.PartsSalesCategoryId = partsSalesCategoryId;
                                break;
                            case (int)DcsPartsShippingOrderType.退货:
                                partsInboundPlan.InboundType = (int)DcsPartsInboundType.销售退货;
                                partsInboundPlan.PartsSalesCategoryId = partsSalesCategoryId;
                                saleUnit = dcsEntities.SalesUnits.FirstOrDefault(r => r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesCategoryId == partsInboundPlan.PartsSalesCategoryId && r.OwnerCompanyId == partsInboundPlan.StorageCompanyId);
                                if(saleUnit == null) {
                                    throw new ValidationException("该配件发运单所属的销售组织对应的配件销售类型不存在");
                                }
                                var customerAccount = dcsEntities.CustomerAccounts.FirstOrDefault(r => r.AccountGroupId == saleUnit.AccountGroupId && r.CustomerCompanyId == partsShippingOrder.ShippingCompanyId);
                                if(customerAccount == null) {
                                    throw new ValidationException("未找到对应客户账户");
                                }
                                partsInboundPlan.CustomerAccountId = customerAccount.Id;
                                break;
                            case (int)DcsPartsShippingOrderType.调拨:
                                partsInboundPlan.InboundType = (int)DcsPartsInboundType.配件调拨;
                                saleUnit = dcsEntities.SalesUnits.FirstOrDefault(r => dcsEntities.SalesUnitAffiWarehouses.Any(v => v.SalesUnitId == r.Id && v.WarehouseId == partsShippingOrder.ReceivingWarehouseId) && r.Status == (int)DcsBaseDataStatus.有效);
                                if(saleUnit == null)
                                    throw new ValidationException("该配件发运单所属的销售组织对应的配件销售类型不存在");
                                partsInboundPlan.PartsSalesCategoryId = saleUnit.PartsSalesCategoryId;
                                if(partsShippingOrder.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件调拨单) {
                                    var _POCodeRef = (from a in dcsEntities.PartsShippingOrderDetails
                                                      join b in dcsEntities.PartsShippingOrders on a.PartsShippingOrderId equals b.Id
                                                      join c in dcsEntities.PartsTransferOrders on b.OriginalRequirementBillId equals c.Id
                                                      join d in dcsEntities.PartsTransferOrderDetails on new {
                                                          a.SparePartId,
                                                          c.Id
                                                      } equals new {
                                                          d.SparePartId,
                                                          Id = d.PartsTransferOrderId
                                                      }
                                                      where b.Id == partsShippingOrder.Id
                                                      select new {
                                                          d.SparePartId,
                                                          d.POCode
                                                      }).ToList();
                                    _POCodeRef.ForEach(i => {
                                        if(!POCodeRef.ContainsKey(i.SparePartId))
                                            POCodeRef.Add(i.SparePartId, i.POCode);
                                    });
                                }
                                break;
                            case (int)DcsPartsShippingOrderType.质量件索赔:
                                partsInboundPlan.InboundType = (int)DcsPartsInboundType.质量件索赔;
                                partsInboundPlan.PartsSalesCategoryId = partsSalesCategoryId;

                                saleUnit = dcsEntities.SalesUnits.FirstOrDefault(r => r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesCategoryId == partsInboundPlan.PartsSalesCategoryId && r.OwnerCompanyId == partsInboundPlan.StorageCompanyId);
                                if(saleUnit == null) {
                                    throw new ValidationException("该配件发运单所属的销售组织对应的配件销售类型不存在");
                                }
                                customerAccount = dcsEntities.CustomerAccounts.FirstOrDefault(r => r.AccountGroupId == saleUnit.AccountGroupId && r.CustomerCompanyId == partsShippingOrder.ShippingCompanyId);
                                if(customerAccount == null) {
                                    throw new ValidationException("未找到对应客户账户");
                                }
                                partsInboundPlan.CustomerAccountId = customerAccount.Id;

                                break;
                            default:
                                throw new ValidationException("配件入库计划入库类型不能为“调剂”");
                        }
                        var partsIds = partsShippingOrder.PartsShippingOrderDetails.Select(r => r.SparePartId).ToArray();

                        IQueryable<PartsPlannedPrice> partsPlannedPrices = null;

                        if(company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                            //var branchid = dcsEntities.AgencyAffiBranches.Where(r => r.AgencyId == company.Id);
                            partsPlannedPrices = dcsEntities.PartsPlannedPrices.Where(r => r.PartsSalesCategoryId == partsInboundPlan.PartsSalesCategoryId && r.OwnerCompanyId == partsShippingOrder.BranchId && partsIds.Contains(r.SparePartId));
                        } else {
                            partsPlannedPrices = dcsEntities.PartsPlannedPrices.Where(r => r.PartsSalesCategoryId == partsInboundPlan.PartsSalesCategoryId && r.OwnerCompanyId == partsShippingOrder.BranchId && partsIds.Contains(r.SparePartId));
                        }
                        foreach(var partsShippingOrderModel in partsShippingOrderModels.Where(o => o.PartsOutboundPlanId == partsOutboundPlan.Id)) {
                            var partsShippingOrderDetails = partsShippingOrder.PartsShippingOrderDetails.FirstOrDefault(o => o.Id == partsShippingOrderModel.DetailId);
                            partsShippingOrderDetails.ConfirmedAmount = partsShippingOrderModel.ConfirmedAmount;
                            if(!partsShippingOrderDetails.ConfirmedAmount.HasValue || partsShippingOrderDetails.ConfirmedAmount.Value == 0)
                                partsShippingOrderDetails.DifferenceClassification = (int)DcsDifferenceClassification.数量差异;
                            if(partsShippingOrderDetails.ConfirmedAmount > 0) {
                                var partsInboundPlanDetail = new PartsInboundPlanDetail {
                                    PartsInboundPlanId = partsInboundPlan.Id,
                                    SparePartId = partsShippingOrderDetails.SparePartId,
                                    SparePartCode = partsShippingOrderDetails.SparePartCode,
                                    SparePartName = partsShippingOrderDetails.SparePartName,
                                    PlannedAmount = partsShippingOrderDetails.ConfirmedAmount.Value,
                                    Price = partsShippingOrderDetails.SettlementPrice,
                                    Remark = partsShippingOrderDetails.Remark,
                                    POCode = POCodeRef.Keys.Contains(partsShippingOrderDetails.SparePartId) ? POCodeRef[partsShippingOrderDetails.SparePartId] : null
                                };
                                if(partsShippingOrder.Type == (int)DcsPartsShippingOrderType.调拨) {
                                    var partsPlannedPrice = partsPlannedPrices.FirstOrDefault(r => r.SparePartId == partsShippingOrderDetails.SparePartId);
                                    if(partsPlannedPrice == null)
                                        throw new ValidationException(string.Format("编号为“{0}”的配件不存在对应的计划价", partsShippingOrderDetails.SparePartCode));
                                    partsInboundPlanDetail.Price = partsPlannedPrice.PlannedPrice;
                                }
                                partsInboundPlan.PartsInboundPlanDetails.Add(partsInboundPlanDetail);
                            }
                        }
                        InsertPartsInboundPlanValidate(partsInboundPlan);
                        dcsEntities.PartsInboundPlans.AddObject(partsInboundPlan);
                        dcsEntities.SaveChanges();
                      
                        //中心库的零售订单不需要新增 收货单位的入库【精确码追溯】数据
                        //若收货企业已存在此精确码的【精确码追溯】有效入库信息，则作废
                        //当发运为配件公司的采购退货时，需将配件公司原入库【精确码追溯】的状态改为“不考核”
                        if(outTraces.Any()) {
							 var  outTrace = outTraces.Where(t=>t.SourceBillId==partsOutboundPlan.Id).ToArray();
							 if(outTrace.Any()){								 
                            foreach(var outDetail in outTrace) {
                                var UnKh = oldInTrace.Where(t => t.TraceCode.Equals(outDetail.TraceCode) && t.InQty == (t.OutQty ?? 0) && t.InQty > 0).FirstOrDefault();
                                if(UnKh != null) {
                                    UnKh.Status = (int)DCSAccurateTraceStatus.作废;
                                    UnKh.ModifierId = userInfo.UserId;
                                    UnKh.ModifierName = userInfo.UserName;
                                    UnKh.ModifyTime = DateTime.Now;
                                }
                                var inBound = new AccurateTrace {
                                    Type = (int)DCSAccurateTraceType.入库,
                                    CompanyType = (int)DCSAccurateTraceCompanyType.中心库,
                                    CompanyId = company.Id,
                                    SourceBillId = partsInboundPlan.Id,
                                    PartId = outDetail.PartId,
                                    TraceCode = outDetail.TraceCode,
                                    //SIHLabelCode=sIHLabelCode,
                                    Status = (int)DCSAccurateTraceStatus.有效,
                                    TraceProperty = outDetail.TraceProperty,
                                    CreateTime=DateTime.Now,
                                    CreatorId=userInfo.UserId,
                                    CreatorName=userInfo.UserName
                                };
                                if(company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站) {
                                    inBound.SIHLabelCode = outDetail.SIHLabelCode;
                                    inBound.BoxCode = outDetail.BoxCode;
                                    if(outDetail.TraceProperty == (int)DCSTraceProperty.精确追溯) {
                                        inBound.InQty = 1;
                                    } else {
                                        inBound.InQty = outDetail.OutQty;
                                    }
                                }
                                dcsEntities.AccurateTraces.AddObject(inBound);
                            }
							 }
                        }
                        //查询配件物流批次
                        var partsLogisticBatches = dcsEntities.PartsLogisticBatches.Where(v => dcsEntities.PartsLogisticBatchBillDetails.Any(r => partsOutboundBillIds.Contains(r.BillId) && r.PartsLogisticBatchId == v.Id && r.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单)).ToArray();
                        foreach(var partsLogisticBatch in partsLogisticBatches) {
                            var partsLogisticBatchBillDetail = new PartsLogisticBatchBillDetail {
                                BillType = (int)DcsPartsLogisticBatchBillDetailBillType.配件入库计划
                            };
                            partsLogisticBatch.PartsLogisticBatchBillDetails.Add(partsLogisticBatchBillDetail);
                            partsInboundPlan.PartsLogisticBatchBillDetails.Add(partsLogisticBatchBillDetail);
                            partsLogisticBatch.CreatorId = userInfo.UserId;
                            partsLogisticBatch.CreatorName = userInfo.UserName;
                            partsLogisticBatch.CreateTime = now;
                        }
                    }
                  
                }
                if(!partsShippingOrder.ReceivingWarehouseId.HasValue && partsShippingOrder.PartsShippingOrderDetails.Sum(r => r.ConfirmedAmount) > 0) {
                    var dealerPartsStocks = dcsEntities.DealerPartsStocks.Where(r => r.SubDealerId == -1 && r.DealerId == partsShippingOrder.ReceivingCompanyId && dcsEntities.SalesUnits.Any(v => v.PartsSalesCategoryId == r.SalesCategoryId && dcsEntities.PartsSalesOrders.Any(k => k.SalesUnitId == v.Id && dcsEntities.PartsOutboundPlans.Any(m => m.SourceId == k.Id && dcsEntities.PartsOutboundBills.Any(n => n.PartsOutboundPlanId == m.Id && n.PartsShippingOrderId == partsShippingOrder.Id))))).ToArray();
                    var saleunitdealerPartsStock = dealerPartsStocks.FirstOrDefault();

                    foreach(var partsShippingOrderDetail in partsShippingOrder.PartsShippingOrderDetails) {
                        if(!partsShippingOrderDetail.ConfirmedAmount.HasValue)
                            throw new ValidationException(string.Format("编号为“{0}”的配件未指定确认量", partsShippingOrderDetail.SparePartCode));
                        var dealerPartsStock = dealerPartsStocks.FirstOrDefault(r => r.SparePartId == partsShippingOrderDetail.SparePartId);
                        var dbpartsOutboundBill = (from partsout in dcsEntities.PartsOutboundBills
                                                   where partsout.PartsShippingOrderId == partsShippingOrder.Id
                                                   select partsout).FirstOrDefault();
                        var dbPartDeleaveInformation = dcsEntities.PartDeleaveInformations.SingleOrDefault(c => c.OldPartId == partsShippingOrderDetail.SparePartId && c.PartsSalesCategoryId == dbpartsOutboundBill.PartsSalesCategoryId && c.Status == (int)DcsBaseDataStatus.有效);
                        if(dealerPartsStock != null) {
                            if(dbPartDeleaveInformation != null) {
                                dealerPartsStock.Quantity += partsShippingOrderDetail.ConfirmedAmount.Value * dbPartDeleaveInformation.DeleaveAmount;
                            } else {
                                dealerPartsStock.Quantity += partsShippingOrderDetail.ConfirmedAmount.Value;
                            }
                            dealerPartsStock.ModifierId = userInfo.UserId;
                            dealerPartsStock.ModifierName = userInfo.UserName;
                            dealerPartsStock.ModifyTime = now;
                        } else {
                            var newdealerPartsStock = new DealerPartsStock {
                                DealerId = partsShippingOrder.ReceivingCompanyId,
                                DealerCode = partsShippingOrder.ReceivingCompanyCode,
                                DealerName = partsShippingOrder.ReceivingCompanyName,
                                BranchId = partsShippingOrder.BranchId,
                                SalesCategoryId = partsSalesCategoryId,
                                SalesCategoryName = partsSalesCategoryName,
                                SparePartId = partsShippingOrderDetail.SparePartId,
                                SparePartCode = partsShippingOrderDetail.SparePartCode,
                                Quantity = dbPartDeleaveInformation == null ? partsShippingOrderDetail.ConfirmedAmount.Value : dbPartDeleaveInformation.DeleaveAmount * partsShippingOrderDetail.ConfirmedAmount.Value,
                                SubDealerId = -1
                            };

                            newdealerPartsStock.ModifierId = userInfo.UserId;
                            newdealerPartsStock.ModifierName = userInfo.UserName;
                            newdealerPartsStock.ModifyTime = now;
                            dcsEntities.DealerPartsStocks.AddObject(newdealerPartsStock);
                        }
                    }
                }
                if(partsShippingOrder.Type == (int)DcsPartsShippingOrderType.质量件索赔) {

                    //更新配件索赔单（新）（配件索赔单（新）.配件索赔单编号=配件发运单.原始需求单据编号） 状态=生效，修改人=当前登陆人，修改时间=当前登陆时间，
                    var partsClaimOrderNew = dcsEntities.PartsClaimOrderNews.FirstOrDefault(r => r.Code == partsShippingOrder.OriginalRequirementBillCode);
                    if(partsClaimOrderNew == null) {
                        throw new ValidationException("未找到对应配件索赔单（新）");
                    }
                    partsClaimOrderNew.Status = (int)DcsPartsClaimBillStatusNew.入库完成;
                    partsClaimOrderNew.ModifierId = userInfo.UserId;
                    partsClaimOrderNew.ModifierName = userInfo.UserName;
                    partsClaimOrderNew.ModifyTime = now;
                }

                //查找运费处理单  更新其状态和出库单ID和Code
                //查询销售订单获取随车行处理单Id  根据随车行处理单ID查询运费处理单
                var partsSalesOrder = dcsEntities.PartsSalesOrders.Where(r => r.Code == partsShippingOrder.OriginalRequirementBillCode).FirstOrDefault();
                if(partsSalesOrder != null && partsSalesOrder.VehiclePartsHandleOrderId.HasValue) {
                    var ecommerceFreightDisposal = dcsEntities.EcommerceFreightDisposals.Where(r => r.VehiclePartsHandleOrderId == partsSalesOrder.VehiclePartsHandleOrderId.Value && r.ERPSourceOrderCode == partsSalesOrder.ERPSourceOrderCode).FirstOrDefault();
                    if(ecommerceFreightDisposal != null) {
                        if(ecommerceFreightDisposal.Status == default(int))
                            ecommerceFreightDisposal.Status = (int)DcsFreightDisposalStatus.待结算;
                        if(ecommerceFreightDisposal.SalesSettlementStatus == null)
                            ecommerceFreightDisposal.SalesSettlementStatus = (int)DcsFreightDisposalStatus.待结算;

                        if(ecommerceFreightDisposal.PartsOutboundBillId == null)
                            ecommerceFreightDisposal.PartsOutboundBillId = partsOutboundBills.Min(r => r.Id);

                        //查询出库单
                        var PartsOutboundBill = dcsEntities.PartsOutboundBills.FirstOrDefault(r => r.Id == ecommerceFreightDisposal.PartsOutboundBillId);
                        if(PartsOutboundBill != null && string.IsNullOrEmpty(ecommerceFreightDisposal.PartsOutboundBillCode))
                            ecommerceFreightDisposal.PartsOutboundBillCode = PartsOutboundBill.Code;
                    }

                }
                dcsEntities.SaveChanges();
                return true;
            }
        }
    }
}
