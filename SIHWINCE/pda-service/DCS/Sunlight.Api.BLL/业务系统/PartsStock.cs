﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Objects;
using System.Linq;
using Sunlight.Api.Models;
using Sunlight.Api.Models.Extends;
using Sunlight.Api.Security;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Api.BLL {
    public partial class DcsServiceImp {
        /// <summary>
        /// 获取指定的配件库存，并加数据库行级锁（待当前会话结束后释放）
        /// </summary>
        /// <param name="filter">PartsLockedStock的查询语句，或查询结果集</param>
        /// <returns></returns>
        internal IEnumerable<PartsStock> GetPartsStocksWithLock(IEnumerable<PartsStock> filter) {
            if(filter == null)
                return Enumerable.Empty<PartsStock>();

            //考虑到filter是本地过滤数据的可能性，使用Id关联代替对象关联
            var query = filter as IQueryable<PartsStock>;
            var partsStockIds = query == null ? filter.Select(v => v.Id) : query.Select(v => v.Id);

            var objectQuery = ObjectContext.PartsStocks.Where(v => partsStockIds.Contains(v.Id)) as ObjectQuery<PartsStock>;
            if(objectQuery == null)
                return Enumerable.Empty<PartsStock>();

            var sql = string.Format("select * from ({0}) for update", objectQuery.ToTraceString());
#if SqlServer
            var paramList = objectQuery.Parameters;
#else
            var paramList = objectQuery.Parameters.Select(v => new Devart.Data.Oracle.OracleParameter(v.Name, v.Value) as object).ToArray();
#endif
            return ObjectContext.ExecuteStoreQuery<PartsStock>(sql, "PartsStocks", MergeOption.AppendOnly, paramList);
        }

        /// <summary>
        /// 获取指定的企业配件成本，并加数据库行级锁（待当前会话结束后释放）
        /// </summary>
        /// <param name="filter">PartsLockedStock的查询语句，或查询结果集</param>
        /// <returns></returns>
        internal IEnumerable<EnterprisePartsCost> GetEnterprisePartsCostsWithLock(IEnumerable<EnterprisePartsCost> filter) {
            if(filter == null)
                return Enumerable.Empty<EnterprisePartsCost>();

            //考虑到filter是本地过滤数据的可能性，使用Id关联代替对象关联
            var query = filter as IQueryable<EnterprisePartsCost>;
            var enterprisePartsCostIds = query == null ? filter.Select(v => v.Id) : query.Select(v => v.Id);

            var objectQuery = ObjectContext.EnterprisePartsCosts.Where(v => enterprisePartsCostIds.Contains(v.Id)) as ObjectQuery<EnterprisePartsCost>;
            if(objectQuery == null)
                return Enumerable.Empty<EnterprisePartsCost>();

            var sql = string.Format("select * from ({0}) for update", objectQuery.ToTraceString());
#if SqlServer
            var paramList = objectQuery.Parameters;
#else
            var paramList = objectQuery.Parameters.Select(v => new Devart.Data.Oracle.OracleParameter(v.Name, v.Value) as object).ToArray();
#endif
            return ObjectContext.ExecuteStoreQuery<EnterprisePartsCost>(sql, "EnterprisePartsCosts", MergeOption.AppendOnly, paramList);
        }


        /// <summary>
        /// 根据备件编号查询配件库位和数量
        /// </summary>
        /// <param name="sparePartCode">备件编号参数</param>
        /// <param name="warehouseId">当前登录仓库</param>
        /// <returns>备件库存列表</returns>
        public List<PartsStockExtend> 根据备件编号查询配件库位和数量(string sparePartCode, string locationCode, bool isTrue, int warehouseId) {
            var userInfo = FormsAuth.GetUserData();

            if(string.IsNullOrEmpty(sparePartCode) && string.IsNullOrEmpty(locationCode))
                throw new ValidationException("参数配件编号或货位编号不能为空");
            if(!string.IsNullOrEmpty(sparePartCode)) {
                var result = (from partsStock in ObjectContext.PartsStocks.Where(r => r.StorageCompanyId == userInfo.EnterpriseId && r.WarehouseId == warehouseId && (isTrue ? r.Quantity > 0 : true))
                              join sparePart in ObjectContext.SpareParts on partsStock.PartId equals sparePart.Id
                              join warehouseArea in ObjectContext.WarehouseAreas.Where(r => r.AreaKind == (int)DcsAreaKind.库位 && r.Status == (int)DcsBaseDataStatus.有效)
                              on new {
                                  warehouseId = partsStock.WarehouseId,
                                  areaId = partsStock.WarehouseAreaId
                              } equals new {
                                  warehouseId = warehouseArea.WarehouseId,
                                  areaId = warehouseArea.Id
                              }
                              join warehouseAreaCategory in ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.保管区)
                              on (warehouseArea.AreaCategoryId ?? 0) equals warehouseAreaCategory.Id
                              where sparePart.Code.ToUpper() == sparePartCode.ToUpper()
                              select new PartsStockExtend {
                                  Id = partsStock.Id,
                                  Quantity = partsStock.Quantity,
                                  WarehouseAreaCategoryId = partsStock.WarehouseAreaCategoryId,
                                  WarehouseAreaCategory = warehouseAreaCategory.Category,
                                  WarehouseAreaId = partsStock.WarehouseAreaId,
                                  WarehouseAreaCode = warehouseArea.Code,
                                  WarehouseId = partsStock.WarehouseId,
                                  SparePartId = partsStock.PartId,
                                  SparePartName = sparePart.Name,
                                  SparePartCode = sparePart.Code
                              }).ToList();
                return result;
            } else {
                var result = (from partsStock in ObjectContext.PartsStocks.Where(r => r.StorageCompanyId == userInfo.EnterpriseId && r.WarehouseId == warehouseId && (isTrue ? r.Quantity > 0 : true))
                              join sparePart in ObjectContext.SpareParts on partsStock.PartId equals sparePart.Id
                              join warehouse in ObjectContext.Warehouses on partsStock.WarehouseId equals warehouse.Id
                              join warehouseArea in ObjectContext.WarehouseAreas.Where(r => r.AreaKind == (int)DcsAreaKind.库位)
                              on new {
                                  warehouseId = warehouse.Id,
                                  areaId = partsStock.WarehouseAreaId
                              } equals new {
                                  warehouseId = warehouseArea.WarehouseId,
                                  areaId = warehouseArea.Id
                              }
                              join warehouseAreaCategory in ObjectContext.WarehouseAreaCategories.Where(r => r.Category == (int)DcsAreaType.保管区) on (warehouseArea.AreaCategoryId ?? 0) equals warehouseAreaCategory.Id
                              where warehouseArea.Code == locationCode
                              select new PartsStockExtend {
                                  Id = partsStock.Id,
                                  Quantity = partsStock.Quantity,
                                  WarehouseAreaCategoryId = partsStock.WarehouseAreaCategoryId,
                                  WarehouseAreaCategory = warehouseAreaCategory.Category,
                                  WarehouseAreaId = partsStock.WarehouseAreaId,
                                  WarehouseAreaCode = warehouseArea.Code,
                                  WarehouseId = partsStock.WarehouseId,
                                  SparePartId = partsStock.PartId,
                                  SparePartName = sparePart.Name,
                                  SparePartCode = sparePart.Code
                              }).ToList();
                return result;
            }


        }


        public PartsStockShiftModel 随移查询库存记录(int warehouseId, string locationCode, string sparePartCode) {
            var warehouseAreas = ObjectContext.WarehouseAreas.Include("WarehouseAreaCategory").FirstOrDefault(o => o.Code == locationCode && o.WarehouseId == warehouseId);
            if(warehouseAreas == null)
                throw new ValidationException(string.Format("库位无效或不存在所选仓库中。"));
            if(warehouseAreas.WarehouseAreaCategory.Category != (int)DcsAreaType.保管区)
                throw new ValidationException(string.Format("扫描库位必须为保管区库位。"));
            var result = from ps in ObjectContext.PartsStocks
                         join sp in ObjectContext.SpareParts on ps.PartId equals sp.Id
                         where ps.WarehouseAreaId == warehouseAreas.Id && sp.Code == sparePartCode &&
                         ps.WarehouseAreaCategory.Category == (int)DcsAreaType.保管区 && sp.Status == (int)DcsMasterDataStatus.有效
                         select new PartsStockShiftModel() {
                             PartsStockId = ps.Id,
                             SparePartCode = sp.Code,
                             SparePartName = sp.Name,
                             TraceProperty = sp.TraceProperty,
                             OriginalWarehouseAreaCode = ps.WarehouseArea.Code,
                             StockQuantity = ps.Quantity - (ps.LockedQty ?? 0)
                         };
            if(!result.Any())
                throw new ValidationException(string.Format("当前货位备件，不存在库存记录，无法移库。"));
            return result.First();

        }
    }
}
