﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Sunlight.Api.Models;
using Sunlight.Api.Models.Extends;
using Sunlight.Api.Security;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Api.BLL {
    public partial class DcsServiceImp {

        public List<PickingWarehouseAreaModel> 查询拣货任务库区列表(int warehouseId) {
            var userInfo = FormsAuth.GetUserData();
            var result = (from pt in this.ObjectContext.PickingTasks.Where(o => o.WarehouseId == warehouseId && (o.Status == (int)DcsPickingTaskStatus.新建 || o.Status == (int)DcsPickingTaskStatus.部分拣货) && o.BranchId == userInfo.EnterpriseId)
                          join ptd in this.ObjectContext.PickingTaskDetails on pt.Id equals ptd.PickingTaskId
                          join wa in this.ObjectContext.WarehouseAreas on ptd.WarehouseAreaId equals wa.Id
                          join waParent in this.ObjectContext.WarehouseAreas on wa.ParentId equals waParent.Id
                          group waParent by new {
                              waParent.Id,
                              waParent.Code
                          } into g
                          select new PickingWarehouseAreaModel {
                              WarehouseAreaId = g.Key.Id,
                              WarehouseAreaCode = g.Key.Code
                          }).ToList();
            foreach(var item in result) {
                var WarehouseAreaIds = this.ObjectContext.WarehouseAreas.Where(p => p.ParentId == item.WarehouseAreaId).Select(o => o.Id);
                item.TaskNumber = this.ObjectContext.PickingTasks.Where(o => this.ObjectContext.PickingTaskDetails.Any(r => WarehouseAreaIds.Contains(r.WarehouseAreaId ?? 0) && r.PickingTaskId == o.Id) && o.WarehouseId == warehouseId
                    && (o.Status == (int)DcsPickingTaskStatus.新建 || o.Status == (int)DcsPickingTaskStatus.部分拣货) && o.BranchId == userInfo.EnterpriseId).ToList().Count;
            }
            return result;
        }

        public List<PickingTaskExtend> 根据库区查询待拣货任务(int warehouseAreaId, int warehouseId) {
            var userInfo = FormsAuth.GetUserData();
            var result = from pt in this.ObjectContext.PickingTasks.Where(o => o.WarehouseId == warehouseId && (o.Status == (int)DcsPickingTaskStatus.新建 || o.Status == (int)DcsPickingTaskStatus.部分拣货) && o.BranchId == userInfo.EnterpriseId)
                         join ptd in this.ObjectContext.PickingTaskDetails.Where(o => (o.PlanQty ?? 0) != (o.PickingQty ?? 0)) on pt.Id equals ptd.PickingTaskId
                         join wa in this.ObjectContext.WarehouseAreas on ptd.WarehouseAreaId equals wa.Id
                         where wa.ParentId == warehouseAreaId
                         group pt by new {
                             pt.Id,
                             pt.Code,
                             pt.OrderTypeName
                         } into g
                         select new PickingTaskExtend {
                             Id = g.Key.Id,
                             Code = g.Key.Code,
                             isUrgent = (g.Key.OrderTypeName.IndexOf("急") >= 0 || g.Key.OrderTypeName.IndexOf("三包垫件") >= 0) ? "urgent" : (g.Key.OrderTypeName.Equals("6K1") || g.Key.OrderTypeName.Equals("6K2") || g.Key.OrderTypeName.Equals("出口IV") || g.Key.OrderTypeName.Equals("出口其他") || g.Key.OrderTypeName.Equals("CKD") || g.Key.OrderTypeName.Equals("中国IV")) ? "exit" : (g.Key.OrderTypeName.Equals("事故订单") ? "accident" : "normal")
                         };
            return result.OrderBy(o => o.Code).ToList();
        }

        public List<PickingTaskModel> 查询拣货任务单详情(string code, int warehouseAreaId, int warehouseId) {
            var result = from pt in this.ObjectContext.PickingTasks.Where(o => o.WarehouseId == warehouseId && (o.Status == (int)DcsPickingTaskStatus.新建 || o.Status == (int)DcsPickingTaskStatus.部分拣货))
                         join ptd in this.ObjectContext.PickingTaskDetails on pt.Id equals ptd.PickingTaskId
                         join c in this.ObjectContext.Companies on pt.CounterpartCompanyId equals c.Id
                         join wa in this.ObjectContext.WarehouseAreas on ptd.WarehouseAreaId equals wa.Id
                         join sp in this.ObjectContext.SpareParts on ptd.SparePartId equals sp.Id
                         where wa.ParentId == warehouseAreaId && pt.Code == code && ptd.PlanQty.Value > (ptd.PickingQty ?? 0)
                         group ptd by new {
                             pt.Id,
                             pt.Code,
                             pt.CounterpartCompanyId,
                             c.ShortName,
                             ptd.SparePartId,
                             ptd.SparePartCode,
                             ptd.SparePartName,
                             ptd.WarehouseAreaId,
                             ptd.WarehouseAreaCode,
                             sp.TraceProperty
                         } into g
                         select new PickingTaskModel {
                             Id = g.Key.Id,
                             Code = g.Key.Code,
                             CounterpartCompanyId = (g.Key.CounterpartCompanyId ?? 0),
                             CounterpartCompanyName = g.Key.ShortName,
                             SparePartId = g.Key.SparePartId,
                             SparePartCode = g.Key.SparePartCode,
                             SparePartName = g.Key.SparePartName,
                             PlanQty = g.Sum(o => o.PlanQty ?? 0),
                             PickingQty = g.Sum(o => o.PickingQty ?? 0),
                             WarehouseAreaId = g.Key.WarehouseAreaId,
                             WarehouseAreaCode = g.Key.WarehouseAreaCode,
                             TraceProperty = g.Key.TraceProperty
                         };
            return result.OrderBy(o => o.WarehouseAreaCode).ToList();
        }

        public bool 拣货(List<PickingTaskModel> pickingTaskModels, bool isOver = false) {
            var userInfo = FormsAuth.GetUserData();
            var pickingTaskId = pickingTaskModels[0].Id;
            //更新主单
            var pickingTask = ObjectContext.PickingTasks.FirstOrDefault(r => r.Id == pickingTaskId && (r.Status == (int)DcsPickingTaskStatus.部分拣货 || r.Status == (int)DcsPickingTaskStatus.新建));
            if(pickingTask == null)
                throw new ValidationException("拣货单已完成拣货，请重新查询拣货单状态");
            //先查询是否已有物流批次单，若有则新增明细，没有则新增
            var partsLogisticBatch = ObjectContext.PartsLogisticBatches.FirstOrDefault(r => r.CompanyId == userInfo.EnterpriseId && r.SourceId == pickingTask.Id && r.Status == (int)DcsPartsLogisticBatchSourceType.配件出库计划);
            if(partsLogisticBatch == null) {
                partsLogisticBatch = new PartsLogisticBatch() {
                    CompanyId = userInfo.EnterpriseId,
                    Status = (int)DcsPartsLogisticBatchStatus.新增,
                    ShippingStatus = (int)DcsPartsLogisticBatchShippingStatus.待运,
                    SourceId = pickingTask.Id,
                    SourceType = (int)DcsPartsLogisticBatchSourceType.配件出库计划,
                    CreateTime = DateTime.Now,
                    CreatorId = userInfo.UserId,
                    CreatorName = userInfo.UserName
                };
                ObjectContext.PartsLogisticBatches.AddObject(partsLogisticBatch);
            };
            //锁定保管区库存
            var sparePartIds = pickingTask.PickingTaskDetails.Select(o => o.SparePartId).ToList();
            var warehouseAreaIds = pickingTask.PickingTaskDetails.Select(o => o.WarehouseAreaId).ToList();
            var partsStocks = GetPartsStocksWithLock(ObjectContext.PartsStocks.Where(o => o.WarehouseId == pickingTask.WarehouseId && sparePartIds.Contains(o.PartId) && warehouseAreaIds.Contains(o.WarehouseAreaId))).ToList();
            //锁定待发区库存
            var partsStockDfs = GetPartsStocksWithLock((from ps in ObjectContext.PartsStocks.Where(o => o.WarehouseId == pickingTask.WarehouseId && sparePartIds.Contains(o.PartId))
                                                        join wa in ObjectContext.WarehouseAreas.Where(o => o.Code == "X1-01-01-01" && o.Status == (int)DcsBaseDataStatus.有效 && o.AreaKind == (int)DcsAreaKind.库位) on ps.WarehouseAreaId equals wa.Id
                                                        select ps)).ToList();

            foreach(var item in pickingTaskModels) {
                var remnantQty = item.ThisPickingQty;
                var pickingTaskDetails = pickingTask.PickingTaskDetails.Where(o => o.SparePartId == item.SparePartId && o.WarehouseAreaId == item.WarehouseAreaId && o.PlanQty.Value > (o.PickingQty ?? 0)).OrderByDescending(o => o.PickingQty);
                foreach(var pickingTaskDetail in pickingTaskDetails) {
                    if(pickingTaskDetail.PlanQty < remnantQty + (pickingTaskDetail.PickingQty ?? 0)) {
                        pickingTaskDetail.CourrentPicking = pickingTaskDetail.PlanQty - (pickingTaskDetail.PickingQty ?? 0);
                        remnantQty = remnantQty - pickingTaskDetail.CourrentPicking;
                    } else {
                        pickingTaskDetail.CourrentPicking = remnantQty;
                        remnantQty = 0;
                    }
                    pickingTaskDetail.BatchNumber = item.BatchNumber;
                    if(null == pickingTaskDetail.CourrentPicking) {
                        pickingTaskDetail.CourrentPicking = 0;
                    } else {
                        //新增配件出库实绩
                        PartsOutboundPerformance formance = new PartsOutboundPerformance() {
                            OutType = (int)DcsPartsOutboundPer_OutType.拣货,
                            WarehouseAreaId = pickingTaskDetail.WarehouseAreaId,
                            Qty = pickingTaskDetail.CourrentPicking,
                            OperaterId = userInfo.UserId,
                            OperaterName = userInfo.UserName,
                            Equipment = (int)DcsPartsOutboundPer_EquipmentType.手持,
                            OperaterEndTime = DateTime.Now,
                            Code = pickingTask.Code,
                            SparePartId = item.SparePartId,
                            SpareCode = item.SparePartCode,
                            SpareName = item.SparePartName
                        };
                        ObjectContext.PartsOutboundPerformances.AddObject(formance);
                    }
                    //根据货位ID+配件ID查询库存信息
                    //更新库存
                    var partsStock = partsStocks.FirstOrDefault(o => o.PartId == pickingTaskDetail.SparePartId && o.WarehouseAreaId == pickingTaskDetail.WarehouseAreaId);
                    if(null == partsStock) {
                        throw new ValidationException(pickingTaskDetail.SparePartCode + "未找到有效库存");
                    }
                    if(null == partsStock.LockedQty) {
                        partsStock.LockedQty = 0;
                    }

                    if(!isOver && pickingTaskDetail.CourrentPicking > 0) {
                        if(pickingTaskDetail.CourrentPicking > partsStock.Quantity || pickingTaskDetail.CourrentPicking > partsStock.LockedQty) {
                            throw new ValidationException(pickingTaskDetail.SparePartCode + "库存数量不足");
                        }
                        partsStock.Quantity = partsStock.Quantity - Convert.ToInt32(pickingTaskDetail.CourrentPicking);
                        partsStock.LockedQty = partsStock.LockedQty - Convert.ToInt32(pickingTaskDetail.CourrentPicking);

                        //更新配件锁定库存
                        var partsLockedStock = ObjectContext.PartsLockedStocks.FirstOrDefault(r => r.PartId == pickingTaskDetail.SparePartId && r.WarehouseId == pickingTask.WarehouseId.Value);
                        partsLockedStock.LockedQuantity = partsLockedStock.LockedQuantity - pickingTaskDetail.CourrentPicking.Value;
                        partsLockedStock.ModifierId = userInfo.UserId;
                        partsLockedStock.ModifierName = userInfo.UserName;
                        partsLockedStock.ModifyTime = DateTime.Now;
                    } else if(isOver) {
                        //完成拣货业务
                        var thisPickingQty = (pickingTaskDetail.PlanQty ?? 0) - (pickingTaskDetail.PickingQty ?? 0);
                        if(thisPickingQty > partsStock.Quantity || thisPickingQty > partsStock.LockedQty) {
                            throw new ValidationException(pickingTaskDetail.SparePartCode + "库存数量不足");
                        }
                        partsStock.Quantity = partsStock.Quantity - (pickingTaskDetail.CourrentPicking ?? 0);
                        partsStock.LockedQty = partsStock.LockedQty - thisPickingQty;
                        //更新配件锁定库存
                        var partsLockedStock = ObjectContext.PartsLockedStocks.FirstOrDefault(r => r.PartId == pickingTaskDetail.SparePartId && r.WarehouseId == pickingTask.WarehouseId.Value);
                        partsLockedStock.LockedQuantity = partsLockedStock.LockedQuantity - thisPickingQty;
                        partsLockedStock.ModifierId = userInfo.UserId;
                        partsLockedStock.ModifierName = userInfo.UserName;
                        partsLockedStock.ModifyTime = DateTime.Now;
                    }

                    if(pickingTaskDetail.CourrentPicking > 0) {
                        var partsStockDf = partsStockDfs.FirstOrDefault(o => o.PartId == pickingTaskDetail.SparePartId);
                        if(null != partsStockDf) {
                            if(null == partsStockDf.LockedQty) {
                                partsStockDf.LockedQty = 0;
                            }
                            partsStockDf.Quantity = partsStockDf.Quantity + Convert.ToInt32(pickingTaskDetail.CourrentPicking);
                            partsStockDf.LockedQty = partsStockDf.LockedQty + Convert.ToInt32(pickingTaskDetail.CourrentPicking);
                        } else {
                            //新增待发区库存
                            //库区信息
                            var warehouseArea = ObjectContext.WarehouseAreas.FirstOrDefault(r => r.Code == "X1-01-01-01" && r.Status == (int)DcsBaseDataStatus.有效 && r.AreaKind == (int)DcsAreaKind.库位 && r.WarehouseId == pickingTask.WarehouseId);
                            if(null == warehouseArea) {
                                throw new ValidationException("未找到" + pickingTask.WarehouseCode + "仓库的待发区库位信息");
                            }
                            //库区用途
                            var warehouseAreaCategory = ObjectContext.WarehouseAreaCategories.FirstOrDefault(r => r.Category == (int)DcsAreaType.待发区);
                            var newPartsStock = new PartsStock() {
                                WarehouseId = Convert.ToInt32(pickingTask.WarehouseId),
                                StorageCompanyId = partsStock.StorageCompanyId,
                                StorageCompanyType = partsStock.StorageCompanyType,
                                BranchId = partsStock.BranchId,
                                WarehouseAreaId = warehouseArea.Id,
                                WarehouseAreaCategoryId = warehouseAreaCategory.Id,
                                PartId = pickingTaskDetail.SparePartId,
                                Quantity = Convert.ToInt32(pickingTaskDetail.CourrentPicking),
                                LockedQty = pickingTaskDetail.CourrentPicking,
                                CreateTime = DateTime.Now,
                                CreatorId = userInfo.UserId,
                                CreatorName = userInfo.UserName

                            };
                            ObjectContext.PartsStocks.AddObject(newPartsStock);
                            partsStockDfs.Add(newPartsStock);
                        }
                        if(null != pickingTaskDetail.BatchNumber && !"".Equals(pickingTaskDetail.BatchNumber)) {
                            var warehouse = ObjectContext.Warehouses.FirstOrDefault(r => r.Id == pickingTask.WarehouseId);
                            PartsLogisticBatchItemDetail detail = new PartsLogisticBatchItemDetail() {
                                CounterpartCompanyId = Convert.ToInt32(pickingTask.CounterpartCompanyId),
                                ReceivingCompanyId = Convert.ToInt32(pickingTask.CounterpartCompanyId),
                                ShippingCompanyId = warehouse.StorageCompanyId,
                                SparePartId = pickingTaskDetail.SparePartId,
                                SparePartCode = pickingTaskDetail.SparePartCode,
                                SparePartName = pickingTaskDetail.SparePartName,
                                BatchNumber = pickingTaskDetail.BatchNumber,
                                OutboundAmount = Convert.ToInt32(pickingTaskDetail.CourrentPicking),
                                InboundAmount = 0,
                                OutReturnAmount = 0

                            };
                            detail.PartsLogisticBatchId = partsLogisticBatch.Id;
                            ObjectContext.PartsLogisticBatchItemDetails.AddObject(detail);
                        }

                        //更新已拣货量
                        if(null == pickingTaskDetail.PickingQty) {
                            pickingTaskDetail.PickingQty = 0;
                        }
                        pickingTaskDetail.PickingQty += pickingTaskDetail.CourrentPicking;
                    }
                    if((pickingTaskDetail.PickingQty ?? 0) != pickingTaskDetail.PlanQty && isOver) {
                        pickingTaskDetail.ShortPickingReason = (int)DcsShortPickingReason.入库组欠货;
                    }
                }

                //增加出库追溯清单
                if(item.SIHCodes.Count() > 0) {  //根据SIH 追溯码增加 出库追溯信息
                                                 //入库
                    var tracOld = ObjectContext.AccurateTraces.Where(t => t.Status == (int)DCSAccurateTraceStatus.有效 && t.CompanyId == userInfo.EnterpriseId && t.Type == (int)DCSAccurateTraceType.入库 && t.SIHLabelCode != null && (t.OutQty == null || t.InQty != t.OutQty) && t.InQty.Value > 0).ToArray();
                    //出库
                    var tracOldOut = ObjectContext.AccurateTraces.Where(t => t.Status == (int)DCSAccurateTraceStatus.有效 && t.CompanyId == userInfo.EnterpriseId && t.Type == (int)DCSAccurateTraceType.出库 && t.SIHLabelCode != null).ToArray();
                    var traceCode = item.SIHCodes;
                    if(traceCode.Count() > 0) {
                        if(!item.TraceProperty.HasValue) {
                            item.TraceProperty = 0;
                        }
                        var picking = 0;
                        foreach(var code in traceCode) {
                            var olds = tracOld.Where(t => t.SIHLabelCode == code && t.PartId == item.SparePartId).ToArray();
                            if(code.EndsWith("X"))
                                olds = tracOld.Where(t => t.BoxCode == code && t.PartId == item.SparePartId).ToArray();
                            if(olds != null && olds.Count() > 0) {
                                foreach(var old in olds) {
                                    //判断入库的数量是否已全部出库
                                    //if ((old.InQty ?? 0) == (old.OutQty ?? 0) && (old.InQty ?? 0) > 0)
                                    //{
                                    //    throw new ValidationException("标签码:" + code + "已全部出库");
                                    //}
                                    if(picking == item.ThisPickingQty.Value) {
                                        continue;
                                    }
                                    var pickQty = 0;
                                    var inQty = 0;
                                    if(item.TraceProperty.Value == (int)DCSTraceProperty.精确追溯) {
                                        pickQty = 1;
                                        inQty = 1;
                                        picking = picking + 1;
                                    } else {
                                        if((old.InQty ?? 0) - (old.OutQty ?? 0) >= item.ThisPickingQty.Value) {
                                            pickQty = item.ThisPickingQty.Value;
                                            inQty = old.InQty.Value;
                                            picking = picking + item.ThisPickingQty.Value;
                                        } else {
                                            pickQty = (old.InQty ?? 0) - (old.OutQty ?? 0);
                                            inQty = old.InQty.Value;
                                            picking = picking + ((old.InQty ?? 0) - (old.OutQty ?? 0));
                                        }
                                    }
                                    if(old.TraceProperty.HasValue && old.TraceProperty.Value == (int)DCSTraceProperty.精确追溯 && !string.IsNullOrWhiteSpace(old.TraceCode)) {
                                        var outTraces = tracOldOut.Where(t => t.SourceBillId == item.PartsOutboundPlanId && t.TraceCode == old.TraceCode).FirstOrDefault();
                                        if(outTraces != null) {
                                            outTraces.OutQty = outTraces.OutQty + pickQty;
                                            outTraces.ModifierId = userInfo.UserId;
                                            outTraces.ModifierName = userInfo.UserName;
                                            outTraces.ModifyTime = DateTime.Now;
                                        } else {
                                            var newAccurateTrace = new AccurateTrace {
                                                Type = (int)DCSAccurateTraceType.出库,
                                                CompanyId = userInfo.EnterpriseId,
                                                CompanyType = userInfo.EnterpriseType == (int)DcsCompanyType.分公司 ? (int)DCSAccurateTraceCompanyType.配件中心 : (int)DCSAccurateTraceCompanyType.中心库,
                                                SourceBillId = pickingTask.PickingTaskDetails.First().PartsOutboundPlanId,
                                                PartId = item.SparePartId,
                                                TraceCode = old.TraceCode,
                                                SIHLabelCode = old.SIHLabelCode,
                                                Status = (int)DCSAccurateTraceStatus.有效,
                                                TraceProperty = item.TraceProperty,
                                                BoxCode = old.BoxCode,
                                                InQty = inQty,
                                                OutQty = pickQty,
                                                CreateTime = DateTime.Now,
                                                CreatorId = userInfo.UserId,
                                                CreatorName = userInfo.UserName
                                            };
                                            this.ObjectContext.AccurateTraces.AddObject(newAccurateTrace);
                                        }
                                    } else if(!old.TraceProperty.HasValue || string.IsNullOrWhiteSpace(old.TraceCode) || old.TraceProperty.Value == (int)DCSTraceProperty.批次追溯) {
                                        var outTraces = tracOldOut.Where(t => t.SourceBillId == item.PartsOutboundPlanId && t.SIHLabelCode == old.SIHLabelCode).FirstOrDefault();
                                        if(outTraces != null) {
                                            outTraces.OutQty = outTraces.OutQty + pickQty;
                                            outTraces.ModifierId = userInfo.UserId;
                                            outTraces.ModifierName = userInfo.UserName;
                                            outTraces.ModifyTime = DateTime.Now;
                                        } else {
                                            var newAccurateTrace = new AccurateTrace {
                                                Type = (int)DCSAccurateTraceType.出库,
                                                CompanyId = userInfo.EnterpriseId,
                                                CompanyType = userInfo.EnterpriseType == (int)DcsCompanyType.分公司 ? (int)DCSAccurateTraceCompanyType.配件中心 : (int)DCSAccurateTraceCompanyType.中心库,
                                                SourceBillId = pickingTask.PickingTaskDetails.First().PartsOutboundPlanId,
                                                PartId = item.SparePartId,
                                                TraceCode = old.TraceCode,
                                                SIHLabelCode = old.SIHLabelCode,
                                                Status = (int)DCSAccurateTraceStatus.有效,
                                                TraceProperty = item.TraceProperty,
                                                BoxCode = old.BoxCode,
                                                InQty = inQty,
                                                OutQty = pickQty,
                                                CreateTime = DateTime.Now,
                                                CreatorId = userInfo.UserId,
                                                CreatorName = userInfo.UserName
                                            };
                                            this.ObjectContext.AccurateTraces.AddObject(newAccurateTrace);
                                        }

                                    }
                                    //作废之前的出库追溯信息
                                    if(item.TraceProperty.Value == (int)DCSTraceProperty.精确追溯) {
                                        var outTraceLs = tracOldOut.Where(t => t.SourceBillId != item.PartsOutboundPlanId && t.TraceCode == old.TraceCode).ToArray();
                                        foreach(var outTrace in outTraceLs) {
                                            outTrace.ModifierId = userInfo.UserId;
                                            outTrace.ModifierName = userInfo.UserName;
                                            outTrace.ModifyTime = DateTime.Now;
                                            outTrace.Status = (int)DCSAccurateTraceStatus.作废;
                                        }
                                    }
                                    //增加入库追溯信息中的出库数量
                                    old.OutQty = (old.OutQty ?? 0) + pickQty;
                                    old.ModifierId = userInfo.UserId;
                                    old.ModifierName = userInfo.UserName;
                                    old.ModifyTime = DateTime.Now;
                                }
                            } else {
                                //  throw new ValidationException("标签码:" + code + "已全部出库,或不是当前配件的标签码");
                            }
                        }
                    }
                }

            }

            ObjectContext.SaveChanges();
            //pickingTaskModelOver.Count > 0则为拣货完成
            isOver = !ObjectContext.PickingTaskDetails.Any(o => o.PlanQty > (o.PickingQty ?? 0) && o.PickingTaskId == pickingTaskId);

            //判断是否存在，当前检验量+ 已检验量 大于0的数据，如果不存在则当前检验单一个配件都没有拣货
            var pickingQtyIsNull = pickingTaskModels.Any(o => (o.ThisPickingQty + o.PickingQty) > 0);
            pickingTask.Status = isOver ? (pickingQtyIsNull ? (int)DcsPickingTaskStatus.拣货完成 : (int)DcsPickingTaskStatus.已生成装箱任务) : (int)DcsPickingTaskStatus.部分拣货;
            pickingTask.IsExistShippingOrder = pickingQtyIsNull ? false : true;
            pickingTask.ModifierId = userInfo.UserId;
            pickingTask.ModifierName = userInfo.UserName;
            pickingTask.ModifyTime = DateTime.Now;
            //完成拣货业务
            if(isOver)
                pickingTask.PickingFinishTime = DateTime.Now;

            //更新出库计划单
            var outPlanIds = pickingTask.PickingTaskDetails.Select(r => r.PartsOutboundPlanId).Distinct().ToArray();
            foreach(var item in outPlanIds) {
                var outplan = ObjectContext.PartsOutboundPlans.FirstOrDefault(r => r.Id == item.Value);
                更新出库计划单状态(outplan);
            }
            return true;
        }
    }
}
