﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using Sunlight.Api.Models;
using System.Linq;
using Microsoft.VisualBasic;
using Sunlight.Api.Security;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Api.BLL {
    public partial class DcsServiceImp {
        private void UpdateSparePartValidate(SparePart sparePart) {
            var userInfo = FormsAuth.GetUserData();
            sparePart.Code = Strings.StrConv(sparePart.Code, VbStrConv.Narrow).Replace("【", "[");
            sparePart.ModifierId = userInfo.UserId;
            sparePart.ModifierName = userInfo.UserName;
            sparePart.ModifyTime = DateTime.Now;
        }
    }
}
