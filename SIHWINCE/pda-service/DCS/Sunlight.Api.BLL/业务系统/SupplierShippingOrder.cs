﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Sunlight.Api.Models;
using Sunlight.Api.Security;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Api.BLL {
    public partial class DcsServiceImp {
        public bool 供应商到货确认(SupplierShippingOrderConfirm data) {
            var partsInboundPlan = ObjectContext.PartsInboundPlans.FirstOrDefault(o => o.Code.ToUpper() == data.Code.ToUpper());
			if(partsInboundPlan == null)
                throw new ValidationException("根据入库计划单号，未找到供应商发运单");
            var supplierShippingOrder = ObjectContext.SupplierShippingOrders.FirstOrDefault(o => o.Code == partsInboundPlan.SourceCode);
            if(supplierShippingOrder == null)
                throw new ValidationException("根据入库计划单号，未找到供应商发运单");
            if(supplierShippingOrder.ComfirmStatus != (int)DCSSupplierShippingOrderComfirmStatus.新建)
                throw new ValidationException("发运单已确认完，请重新刷新数据");
            var userInfo = FormsAuth.GetUserData();
            supplierShippingOrder.LogisticArrivalDate = data.Date;
            supplierShippingOrder.ArriveMethod = (int)DCSSupplierShippingOrderArriveMethod.PDA收货确认;
            supplierShippingOrder.SupplierComfirmId = userInfo.UserId;
            supplierShippingOrder.SupplierComfirm = userInfo.UserName;
            supplierShippingOrder.SupplierComfirmDate = DateTime.Now;
            supplierShippingOrder.ComfirmStatus = (int)DCSSupplierShippingOrderComfirmStatus.确认完成;
            supplierShippingOrder.ModifierId = userInfo.UserId;
            supplierShippingOrder.ModifierName = userInfo.UserName;
            supplierShippingOrder.ModifyTime = DateTime.Now;
            ObjectContext.SaveChanges();
            return true;
        }
    }
}
