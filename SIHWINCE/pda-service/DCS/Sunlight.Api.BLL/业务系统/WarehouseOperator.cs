﻿using System.Collections.Generic;
using Sunlight.Api.Models;
using System.Linq;
using Sunlight.Api.Security;

namespace Sunlight.Api.BLL {
    public partial class DcsServiceImp {
        public List<WarehouseOperatorModel> GetWarehouseOperators() {
            var currentUser = FormsAuth.GetUserData();
            var result = this.ObjectContext.WarehouseOperators.Include("Warehouse").Where(r => r.OperatorId == currentUser.UserId && r.Warehouse.Status == (int)DcsBaseDataStatus.有效).Select(r => new WarehouseOperatorModel {
                Id = r.Id,
                WarehouseId = r.WarehouseId,
                WarehouseCode = r.Warehouse.Code,
                WarehouseName = r.Warehouse.Name,
            }).ToList();
            return result;
        }
    }
}
