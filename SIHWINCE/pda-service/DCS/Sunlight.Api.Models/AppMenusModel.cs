﻿using System.Collections.Generic;
using System.Xml.Serialization;
using Sunlight.Api.Security;

namespace Sunlight.Api.Models {
    [XmlRoot("AppMenus")]
    public class AppMenusModel {
        private AuthorizeUser authorizeUser = new AuthorizeUser();
        private List<SystemModel> systemModels = new List<SystemModel>();

        [XmlElement(ElementName = "AuthorizeUser")]
        public AuthorizeUser AuthorizeUser {
            get {
                return this.authorizeUser;
            }
            set {
                this.authorizeUser = value;
            }
        }

        [XmlElement(ElementName = "System")]
        public List<SystemModel> SystemModels {
            get {
                return this.systemModels;
            }
            set {
                this.systemModels = value;
            }
        }
    }
}
