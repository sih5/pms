﻿namespace Sunlight.Api.Models {
    public class AuthenticationUserModel {
        /// <summary>
        ///     企业编号
        /// </summary>
        public string EnterpriseCode {
            get;
            set;
        }

        /// <summary>
        ///     登陆的账号
        /// </summary>
        public string LoginId {
            get;
            set;
        }

        /// <summary>
        ///     密码，已经加密过的密码，不提供明文显示密码
        /// </summary>
        public string Password {
            get;
            set;
        }
    }
}
