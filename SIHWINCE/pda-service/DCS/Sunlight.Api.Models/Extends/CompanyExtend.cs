﻿namespace Sunlight.Api.Models.Extends {
    public class CompanyExtend {
        /// <summary>
        /// 企业Id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 简称
        /// </summary>
        public string ShortName {
            get;
            set;
        }
    }
}
