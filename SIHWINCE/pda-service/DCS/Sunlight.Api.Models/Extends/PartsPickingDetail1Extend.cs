﻿namespace Sunlight.Api.Models.Extends {
    public class PartsPickingDetail1Extend {
        /// <summary>
        ///     拣货单Id
        /// </summary>
        public int PartsPickingId {
            get;
            set;
        }

        /// <summary>
        ///     仓库id
        /// </summary>
        public int WarehouseId {
            get;
            set;
        }

        /// <summary>
        ///     库区库位Id
        /// </summary>
        public int? WarehouseAreaId {
            get;
            set;
        }

        /// <summary>
        ///     库区库位编号
        /// </summary>
        public string WarehouseAreaCode {
            get;
            set;
        }

        /// <summary>
        ///     备件Id
        /// </summary>
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        ///     备件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        ///     备件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        ///     待拣货量
        /// </summary>
        public int UnPackingQuantity {
            get;
            set;
        }

        /// <summary>
        ///     计划量
        /// </summary>
        public int PlanedQuantity {
            get;
            set;
        }

        /// <summary>
        ///     库存数量
        /// </summary>
        public int Quantity {
            get;
            set;
        }

        /// <summary>
        ///     清单条数
        /// </summary>
        public int ListCount {
            get;
            set;
        }

        /// <summary>
        ///     已拣量
        /// </summary>
        public int? PackingAmount {
            get;
            set;
        }
    }
}
