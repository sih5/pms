namespace Sunlight.Api.Models.Extends {
    public class PartsPickingDetailExtend {
        /// <summary>
        ///     备件ID
        /// </summary>
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        ///     备件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        ///     备件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        ///     待装箱量
        /// </summary>
        public int UnQuantity {
            get;
            set;
        }

        /// <summary>
        ///     待装品种数
        /// </summary>
        public int Category {
            get;
            set;
        }
    }
}
