﻿using System;
using System.Collections.Generic;

namespace Sunlight.Api.Models.Extends {
    public class PartsPickingExtend {
        public string BranchCode {
            get;
            set;
        }

        public int BranchId {
            get;
            set;
        }

        public string BranchName {
            get;
            set;
        }

        public string Code {
            get;
            set;
        }

        public string CounterpartCompanyCode {
            get;
            set;
        }

        public int CounterpartCompanyId {
            get;
            set;
        }

        public string CounterpartCompanyName {
            get;
            set;
        }

        public DateTime? CreateTime {
            get;
            set;
        }

        public int? CreatorId {
            get;
            set;
        }

        public string CreatorName {
            get;
            set;
        }

        public int Id {
            get;
            set;
        }

        public string Mark {
            get;
            set;
        }

        public int? ModifierId {
            get;
            set;
        }

        public string ModifierName {
            get;
            set;
        }

        public DateTime? ModifyTime {
            get;
            set;
        }

        public string PackBatchNumber {
            get;
            set;
        }

        public string PartsPackingPlanCode {
            get;
            set;
        }

        public int PartsPackingPlanId {
            get;
            set;
        }

        public int PickingStatus {
            get;
            set;
        }

        public int Priority {
            get;
            set;
        }

        public string Remark {
            get;
            set;
        }

        public string StorageCompanyCode {
            get;
            set;
        }

        public int StorageCompanyId {
            get;
            set;
        }

        public string StorageCompanyName {
            get;
            set;
        }

        public int StorageCompanyType {
            get;
            set;
        }

        public int UrgencyLevel {
            get;
            set;
        }

        public string WarehouseCode {
            get;
            set;
        }

        public int WarehouseId {
            get;
            set;
        }

        public string WarehouseName {
            get;
            set;
        }

        public string WarehouseReservoirAreaCode {
            get;
            set;
        }

        public int? WarehouseReservoirAreaId {
            get;
            set;
        }

        public List<PartsPickingDetail2Extend> PartsPickingDetails {
            get;
            set;
        }
    }
}
