﻿namespace Sunlight.Api.Models.Extends {
    public class PartsboxBillExtend {
        /// <summary>
        ///     木箱号
        /// </summary>
        public string Code {
            get;
            set;
        }

        /// <summary>
        ///     唛头
        /// </summary>
        public string Mark {
            get;
            set;
        }

        /// <summary>
        ///     分销商Id
        /// </summary>
        public int DealerId {
            get;
            set;
        }

        /// <summary>
        ///     分销商编号
        /// </summary>
        public string DealerCode {
            get;
            set;
        }

        /// <summary>
        ///     分销商名称
        /// </summary>
        public string DealerName {
            get;
            set;
        }
        /// <summary>
        /// 箱号
        /// </summary>
        public string BoxCode {
            get;
            set;
        }
    }
}
