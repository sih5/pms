﻿namespace Sunlight.Api.Models.Extends {
    public class PartsboxBillList1Extend {
        /// <summary>
        /// 装箱数量
        /// </summary>
        public int Quantity {
            get;
            set;
        }
        /// <summary>
        /// 备件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }
        /// <summary>
        /// 备件Id
        /// </summary>
        public int SparePartId {
            get;
            set;
        }
        /// <summary>
        /// 备件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }
        /// <summary>
        /// 编号
        /// </summary>
        public string Code {
            get;
            set;
        }
        /// <summary>
        /// 备件英文名称
        /// </summary>
        public string EnglishName {
            get;
            set;
        }
        /// <summary>
        /// 箱单Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// 备件拣货单
        /// </summary>
        public int? PartsPickingId {
            get;
            set;
        }
        /// <summary>
        /// 备件拣货单编号
        /// </summary>
        public string PartsPickingCode {
            get;
            set;
        }
        /// <summary>
        /// 配件品种
        /// </summary>
        public int PartsCategory {
            get;
            set;
        }
    }
}
