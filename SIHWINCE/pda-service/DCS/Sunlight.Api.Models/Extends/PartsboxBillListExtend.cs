﻿namespace Sunlight.Api.Models.Extends {
    public class PartsboxBillListExtend {
        /// <summary>
        /// 装箱数量
        /// </summary>
        public int Quantity {
            get;
            set;
        }
        /// <summary>
        /// 备件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }
        /// <summary>
        /// 备件Id
        /// </summary>
        public int SparePartId {
            get;
            set;
        }
        /// <summary>
        /// 备件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }
    }
}
