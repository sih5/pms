﻿namespace Sunlight.Api.Models.Extends {
    /// <summary>
    /// 此类用于备件标签打印和收货确认，查询打印标签
    /// </summary>
    public class SparePartLabelExtend {
        /// <summary>
        /// 入库计划Id
        /// </summary>
        public int PartsInboundPlanId {
            get;
            set;
        }
        /// <summary>
        /// 备件Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// 英文名称
        /// </summary>
        public string EnglishName {
            get;
            set;
        }
        /// <summary>
        /// 备件编号
        /// </summary>
        public string Code {
            get;
            set;
        }
        /// <summary>
        /// 备件名称
        /// </summary>
        public string Name {
            get;
            set;
        }
        /// <summary>
        /// QTY数量
        /// </summary>
        public int QtyAmount {
            get;
            set;
        }
        /// <summary>
        /// 补发量
        /// </summary>
        public int? ReissueAmount {
            get;
            set;
        }
        /// <summary>
        /// 打印标签数量
        /// </summary>
        public int PrintCount {
            get;
            set;
        }
    }
}
