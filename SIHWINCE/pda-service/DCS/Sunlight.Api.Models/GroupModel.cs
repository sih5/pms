﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Sunlight.Api.Models {
    public class GroupModel {
        private List<PageModel> pageModels = new List<PageModel>();

        [XmlAttribute(AttributeName = "Id")]
        public int Id {
            get;
            set;
        }

        [XmlAttribute(AttributeName = "PageId")]
        public string PageId {
            get;
            set;
        }

        [XmlAttribute(AttributeName = "Name")]
        public string Name {
            get;
            set;
        }

        [XmlElement(ElementName = "Page")]
        public List<PageModel> PageModels {
            get {
                return pageModels;
            }
            set {
                this.pageModels = value;
            }
        }
    }
}
