﻿namespace Sunlight.Api.Models {
    public class PdaPrinterModel {
        public string GeneralPrinterPort {
            get;
            set;
        }

        public string GeneralPrinterIp {
            get;
            set;
        }

        public int Id {
            get;
            set;
        }

        public string LabelPrinterIp {
            get;
            set;
        }

        public string LabelPrinterPort {
            get;
            set;
        }

        public int WarehouseId {
            get;
            set;
        }

        public int? PrinterLanguage {
            get;
            set;
        }

        public string PrinterName {
            get;
            set;
        }
    }
}
