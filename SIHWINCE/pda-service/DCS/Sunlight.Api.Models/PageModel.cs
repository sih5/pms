﻿using System.Xml.Serialization;

namespace Sunlight.Api.Models {
    public class PageModel {
        [XmlAttribute(AttributeName = "Id")]
        public int Id {
            get;
            set;
        }

        [XmlAttribute(AttributeName = "PageId")]
        public string PageId {
            get;
            set;
        }

        [XmlAttribute(AttributeName = "Name")]
        public string Name {
            get;
            set;
        }
    }
}
