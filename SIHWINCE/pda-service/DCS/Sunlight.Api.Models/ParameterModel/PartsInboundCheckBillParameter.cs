﻿namespace Sunlight.Api.Models.ParameterModel {
    public class PartsInboundCheckBillParameter {
        public int PartsInboundPlanId {
            get;
            set;
        }

        public int SparePartId {
            get;
            set;
        }

        public string SparePartCode {
            get;
            set;
        }

        public string SparePartName {
            get;
            set;
        }

        public int ReceivedQuantity {
            get;
            set;
        }

        public int WarehouseId {
            get;
            set;
        }

        /// <summary>
        /// 该备件是否已经确认过
        /// </summary>
        public bool IsConfirmOperation {
            get;
            set;
        }
    }
}
