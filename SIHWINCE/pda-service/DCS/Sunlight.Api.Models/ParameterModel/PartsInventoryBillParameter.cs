﻿namespace Sunlight.Api.Models.ParameterModel {
    public class PartsInventoryBillParameter {
        /// <summary>
        /// 盘点单Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// 盘点次数
        /// </summary>
        public int PartsInventoryCount {
            get;
            set;
        }
        /// <summary>
        /// 目标货位Id
        /// </summary>
        public int WarehouseAreaId {
            get;
            set;
        }
        /// <summary>
        /// 实际数量
        /// </summary>
        public int Quantity {
            get;
            set;
        }
        /// <summary>
        /// 备件Id
        /// </summary>
        public int SparePartId {
            get;
            set;
        }
        /// <summary>
        /// 盘点单状态
        /// </summary>
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 当前登录仓库
        /// </summary>
        public int WarehouseId {
            get;
            set;
        }
        /// <summary>
        /// 盘点单号
        /// </summary>
        public string Code {
            get;
            set;
        }
        /// <summary>
        /// 待盘
        /// </summary>
        public int ForCheckCount {
            get;
            set;
        }
    }

    public class PartsInventoryBill1Parameter {
        /// <summary>
        /// 货位Id
        /// </summary>
        public int WarehouseAreaId {
            get;
            set;
        }
        /// <summary>
        /// 货位编号
        /// </summary>
        public string WarehouseAreaCode {
            get;
            set;
        }
        /// <summary>
        /// 实际数量
        /// </summary>
        public int ActualQuantity {
            get;
            set;
        }
        /// <summary>
        /// 货位数量
        /// </summary>
        public int AreaQuantity {
            get;
            set;
        }
        /// <summary>
        /// 备件Id
        /// </summary>
        public int SparePartId {
            get;
            set;
        }
        /// <summary>
        /// 备件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }
        /// <summary>
        /// 备件编号
        /// </summary>
        public string SparePartName {
            get;
            set;
        }
        /// <summary>
        /// 当前登录仓库
        /// </summary>
        public int WarehouseId {
            get;
            set;
        }
        /// <summary>
        /// 当前登录仓库编号
        /// </summary>
        public string WarehouseCode {
            get;
            set;
        }
        /// <summary>
        /// 当前登录仓库名称
        /// </summary>
        public string WarehouseName {
            get;
            set;
        }
        /// <summary>
        /// 盘点单号
        /// </summary>
        public string Code {
            get;
            set;
        }

    }
}
