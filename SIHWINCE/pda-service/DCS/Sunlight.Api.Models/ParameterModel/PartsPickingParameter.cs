﻿namespace Sunlight.Api.Models.ParameterModel {
    public class PartsPickingParameter {
        //拣货单id
        public int PartsPickingId {
            get;
            set;
        }
        //仓库id
        public int WarehouseId {
            get;
            set;
        }
        //库位id
        public int WarehouseAreaId {
            get;
            set;
        }
        //当前文本框输入的库位编号，可能是更改过也可能没有更改过
        public string WarehouseAreaCodeForCurrent {
            get;
            set;
        }
        //配件id
        public int SparePartId {
            get;
            set;
        }
        //拣货单清单id
        public int PartsPickingDetailId {
            get;
            set;
        }
        //实拣量
        public int PickingNum {
            get;
            set;
        }
        /// <summary>
        /// 备件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }
    }
}
