﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Api.Models.ParameterModel {
    public class PartsShelvesParameter {
        /// <summary>
        /// 源库区库位Id
        /// </summary>
        public int OriginalWarehouseAreaId {
            get;
            set;
        }

        /// <summary>
        /// 仓库Id
        /// </summary>
        public int WarehouseId {
            get;
            set;
        }

        /// <summary>
        /// 源库区库位编号
        /// </summary>
        public string OriginalWarehouseAreaCode {
            get;
            set;
        }

        /// <summary>
        /// 目标库区库位编号
        /// </summary>
        public string TargetWarehouseAreaCode {
            get;
            set;
        }

        /// <summary>
        /// 库区用途Id
        /// </summary>
        public int? AreaCategoryId {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }

        /// <summary>
        /// 库区库位类型
        /// </summary>
        public int AreaKind {
            get;
            set;
        }

        /// <summary>
        /// 库区用途
        /// </summary>
        public int AreaCategory {
            get;
            set;
        }

        /// <summary>
        /// 仓库编号
        /// </summary>
        public string WarehouseCode {
            get;
            set;
        }

        /// <summary>
        /// 仓库名称
        /// </summary>
        public string WarehouseName {
            get;
            set;
        }

        /// <summary>
        /// 仓库类型
        /// </summary>
        public int WarehouseType {
            get;
            set;
        }

        public int BranchId {
            get;
            set;
        }

        /// <summary>
        /// 仓储企业Id
        /// </summary>
        public int StorageCompanyId {
            get;
            set;
        }

        public int StorageCompanyCode {
            get;
            set;
        }
        public int StorageCompanyName {
            get;
            set;
        }
        /// <summary>
        /// 配件Id
        /// </summary>
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        /// 计划量数量
        /// </summary>
        public int? PlanQuantity {
            get;
            set;
        }

        /// <summary>
        /// 现库存
        /// </summary>
        public int? PartsStockQuantity {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }

        public string Code {
            get;
            set;
        }

        public int ShelveQuantity {
            get;
            set;
        }

        public int StorageCompanyType {
            get;
            set;
        }

        public bool IsNeedAddPartsStock {
            get;
            set;
        }
    }
}
