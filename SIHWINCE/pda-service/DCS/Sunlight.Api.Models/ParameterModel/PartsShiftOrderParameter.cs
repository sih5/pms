﻿namespace Sunlight.Api.Models.ParameterModel {
    public class PartsShiftOrderParameter {
        /// <summary>
        /// 配件Id
        /// </summary>
        public int PartId {
            get;
            set;
        }
        /// <summary>
        /// 配件编号
        /// </summary>
        public string PartCode {
            get;
            set;
        }
        /// <summary>
        /// 配件名称
        /// </summary>
        public string PartName {
            get;
            set;
        }
        /// <summary>
        /// 周转容器编号 目标库区库位编号
        /// </summary>
        public string ContainerCode {
            get;
            set;
        }
        /// <summary>
        /// 移库量
        /// </summary>
        public int ShiftQuantity {
            get;
            set;
        }
        /// <summary>
        /// 仓库Id
        /// </summary>
        public int WarehouseId {
            get;
            set;
        }
    }

    public class PartsShiftOrder2Parameter {
        /// <summary>
        /// 配件Id
        /// </summary>
        public int PartId {
            get;
            set;
        }
        /// <summary>
        /// 配件名称
        /// </summary>
        public string PartCode {
            get;
            set;
        }
        /// <summary>
        /// 周转容器编号
        /// </summary>
        public string ContainerCode {
            get;
            set;
        }
        /// <summary>
        /// 移库量
        /// </summary>
        public int ShiftQuantity {
            get;
            set;
        }
        /// <summary>
        /// 仓库Id
        /// </summary>
        public int WarehouseId {
            get;
            set;
        }
    }
}
