﻿namespace Sunlight.Api.Models.ParameterModel {
    public class UpdateDetailsParameter {
        public int OrderType {
            get;
            set;
        }

        public string SparePartCode {
            get;
            set;
        }

        public string SparePartName {
            get;
            set;
        }

        public int ReceiveAmount {
            get;
            set;
        }

        public string OrderCode {
            get;
            set;
        }

        public string DownstreamCode {
            get;
            set;
        }

        public bool IsConfirmOperation {
            get;
            set;
        }
    }
}
