﻿using System.Runtime.Serialization;

namespace Sunlight.Api.Models {
    public class PartsInboundCheckBillModel {
        /// <summary>
        ///     配件名称
        /// </summary>
        [DataMember]
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        ///     配件Id
        /// </summary>
        [DataMember]
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        ///     配件编号
        /// </summary>
        [DataMember]
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        ///     实收数量
        /// </summary>
        [DataMember]
        public int ReceivedQuantity {
            get;
            set;
        }
    }
}
