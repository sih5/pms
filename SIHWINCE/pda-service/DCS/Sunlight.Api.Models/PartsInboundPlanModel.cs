﻿namespace Sunlight.Api.Models {
    /// <summary>
    ///     配件入库计划
    /// </summary>
    public class PartsInboundPlanModel {
        /// <summary>
        ///     Id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        ///     配件入库计划编号
        /// </summary>
        public string Code {
            get;
            set;
        }

        /// <summary>
        ///     配件仓库Id
        /// </summary>
        public int WarehouseId {
            get;
            set;
        }

        /// <summary>
        ///     配件仓库编号
        /// </summary>
        public string WarehouseCode {
            get;
            set;
        }

        /// <summary>
        ///     配件仓库名称
        /// </summary>
        public string WarehouseName {
            get;
            set;
        }

        /// <summary>
        ///     仓储企业Id
        /// </summary>
        public int StorageCompanyId {
            get;
            set;
        }

        /// <summary>
        ///     仓储企业编号
        /// </summary>
        public string StorageCompanyCode {
            get;
            set;
        }

        /// <summary>
        ///     仓储企业名称
        /// </summary>
        public string StorageCompanyName {
            get;
            set;
        }

        /// <summary>
        ///     仓储企业类型
        /// </summary>
        public int StorageCompanyType {
            get;
            set;
        }

        /// <summary>
        ///     营销分公司Id
        /// </summary>
        public int BranchId {
            get;
            set;
        }

        /// <summary>
        ///     营销分公司编号
        /// </summary>
        public string BranchCode {
            get;
            set;
        }

        /// <summary>
        ///     营销分公司名称
        /// </summary>
        public string BranchName {
            get;
            set;
        }

        /// <summary>
        ///     配件销售类型Id
        /// </summary>
        public int PartsSalesCategoryId {
            get;
            set;
        }

        /// <summary>
        ///     对方单位Id
        /// </summary>
        public int CounterpartCompanyId {
            get;
            set;
        }

        /// <summary>
        ///     对方单位编号
        /// </summary>
        public string CounterpartCompanyCode {
            get;
            set;
        }

        /// <summary>
        ///     对方单位名称
        /// </summary>
        public string CounterpartCompanyName {
            get;
            set;
        }

        /// <summary>
        ///     源单据Id
        /// </summary>
        public int SourceId {
            get;
            set;
        }

        /// <summary>
        ///     源单据编号
        /// </summary>
        public string SourceCode {
            get;
            set;
        }

        /// <summary>
        ///     入库类型
        /// </summary>
        public int InboundType {
            get;
            set;
        }

        /// <summary>
        ///     客户账户Id
        /// </summary>
        public int? CustomerAccountId {
            get;
            set;
        }

        /// <summary>
        ///     客户账户Id
        /// </summary>
        public int OriginalRequirementBillId {
            get;
            set;
        }

        /// <summary>
        ///     原始需求单据类型
        /// </summary>
        public int OriginalRequirementBillType {
            get;
            set;
        }

        /// <summary>
        ///     原始需求单据编号
        /// </summary>
        public string OriginalRequirementBillCode {
            get;
            set;
        }

        /// <summary>
        ///     状态
        /// </summary>
        public int Status {
            get;
            set;
        }
    }
}
