﻿namespace Sunlight.Api.Models {
    public class PartsInventoryDetailModel {
        public decimal? AmountDifference {
            get;
            set;
        }

        public decimal? CostPrice {
            get;
            set;
        }

        public string CurrentBatchNumber {
            get;
            set;
        }

        public int CurrentStorage {
            get;
            set;
        }

        public int Id {
            get;
            set;
        }

        public bool? Ifcover {
            get;
            set;
        }

        public string NewBatchNumber {
            get;
            set;
        }

        public int PartsInventoryBillId {
            get;
            set;
        }

        public int? PartsInventoryCount {
            get;
            set;
        }

        public string Remark {
            get;
            set;
        }

        public string SparePartCode {
            get;
            set;
        }

        public int SparePartId {
            get;
            set;
        }

        public string SparePartName {
            get;
            set;
        }

        public int StorageAfterInventory {
            get;
            set;
        }

        public int StorageDifference {
            get;
            set;
        }

        public string WarehouseAreaCode {
            get;
            set;
        }

        public int WarehouseAreaId {
            get;
            set;
        }
    }
}
