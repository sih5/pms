﻿namespace Sunlight.Api.Models {
    public class PartsPickingAndPartsboxBillModel {
        /// <summary>
        ///     拣货单ID
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        ///     批次号
        /// </summary>
        public string PackBatchNumber {
            get;
            set;
        }

        /// <summary>
        ///     拣货单编号
        /// </summary>
        public string Code {
            get;
            set;
        }

        /// <summary>
        ///     待装箱量
        /// </summary>
        public int UnPackingAmount {
            get;
            set;
        }

        /// <summary>
        ///     待拣量
        /// </summary>
        public int UnPickingAmount {
            get;
            set;
        }

        /// <summary>
        ///     待装箱品种数
        /// </summary>
        public int PartsCategory {
            get;
            set;
        }
    }
}
