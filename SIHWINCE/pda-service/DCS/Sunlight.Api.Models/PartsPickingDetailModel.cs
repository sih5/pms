﻿namespace Sunlight.Api.Models {
    public class PartsPickingDetailModel {
        public string EnglishName {
            get;
            set;
        }

        public int? FallbackQty {
            get;
            set;
        }

        public int Id {
            get;
            set;
        }

        public int? PackingAmount {
            get;
            set;
        }

        public int PartsPickingId {
            get;
            set;
        }

        public int PlannedAmount {
            get;
            set;
        }

        public int? Quantity {
            get;
            set;
        }

        public string Remark {
            get;
            set;
        }

        public string SparePartCode {
            get;
            set;
        }

        public int SparePartId {
            get;
            set;
        }

        public string SparePartName {
            get;
            set;
        }

        public string WarehouseAreaCode {
            get;
            set;
        }

        public int? WarehouseAreaId {
            get;
            set;
        }
    }
}
