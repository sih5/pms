﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Api.Models {
    public class PartsShelvesModel {
        /// <summary>
        /// 上架任务单id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 上架任务单编号
        /// </summary>
        public string Code {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        /// 计划量
        /// </summary>
        public int PlannedAmount {
            get;
            set;
        }

        /// <summary>
        /// 已上架量
        /// </summary>
        public int ShelvesAmount {
            get;
            set;
        }

        /// <summary>
        /// 本次上架量
        /// </summary>
        public int ThisShelvesAmount {
            get;
            set;
        }

        /// <summary>
        /// 原单据编号
        /// </summary>
        public string SourceBillCode {
            get;
            set;
        }

        /// <summary>
        /// 推荐货位Id
        /// </summary>
        public int? WarehouseAreaId {
            get;
            set;
        }
        /// <summary>
        /// 推荐货位编号
        /// </summary>
        public string WarehouseAreaCode {
            get;
            set;
        }

        /// <summary>
        /// 目标货位Id
        /// </summary>
        public int? TargetWarehouseAreaId {
            get;
            set;
        }
        /// <summary>
        /// 目标货位编号
        /// </summary>
        public string TargetWarehouseAreaCode {
            get;
            set;
        }       
        //上架标签码
        public string SIHCodes {
            get;
            set;
        }
        //追溯属性
        public int? TraceProperty {
            get;
            set;
        }
    }
}
