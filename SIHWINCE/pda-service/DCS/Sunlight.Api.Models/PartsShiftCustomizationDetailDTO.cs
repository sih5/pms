﻿using System.Collections.Generic;

namespace Sunlight.Api.Models {
    public class PartsShiftCustomizationDetailDTO {
        /// <summary>
        /// 目标库位
        /// </summary>
        public string DestWarehouseAreaCode { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// SIH标签码
        /// </summary>
        public List<string> SIHCodes {
            get;
            set;
        }
        /// <summary>
        /// 库存
        /// </summary>
        public int PartsStockId { get; set; }

        public string SparePartCode { get; set; }

        public string OriginalWarehouseAreaCode { get; set; }
    }
}
