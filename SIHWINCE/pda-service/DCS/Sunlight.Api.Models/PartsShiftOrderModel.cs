﻿using System;

namespace Sunlight.Api.Models {
    public class PartsShiftOrderModel {
        public int BranchId {
            get;
            set;
        }

        public string Code {
            get;
            set;
        }

        public DateTime? CreateTime {
            get;
            set;
        }

        public int? CreatorId {
            get;
            set;
        }

        public string CreatorName {
            get;
            set;
        }

        public int Id {
            get;
            set;
        }

        public int? IsShelves {
            get;
            set;
        }

        public int? PartsOutboundPlanId {
            get;
            set;
        }

        public string Remark {
            get;
            set;
        }

        public DateTime? RowVersion {
            get;
            set;
        }

        public int Status {
            get;
            set;
        }

        public string StorageCompanyCode {
            get;
            set;
        }

        public int StorageCompanyId {
            get;
            set;
        }

        public string StorageCompanyName {
            get;
            set;
        }

        public int StorageCompanyType {
            get;
            set;
        }

        public int Type {
            get;
            set;
        }

        public string WarehouseCode {
            get;
            set;
        }

        public int WarehouseId {
            get;
            set;
        }

        public string WarehouseName {
            get;
            set;
        }
        /// <summary>
        /// 配件Id
        /// </summary>
        public int SparePartId {
            get;
            set;
        }
        /// <summary>
        /// 配件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }
        /// <summary>
        /// 配件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }
        /// <summary>
        ///库位id
        /// </summary>
        public int WarehouseAreaId {
            get;
            set;
        }
        /// <summary>
        ///数量
        /// </summary>
        public int Quantity {
            get;
            set;
        }
    }
}
