﻿using System;

namespace Sunlight.Api.Models {
    public class PartsStockModel {
        public int Id {
            get;
            set;
        }

        public int Quantity {
            get;
            set;
        }

        public int WarehouseAreaId {
            get;
            set;
        }

        public string WarehouseAreaCode {
            get;
            set;
        }
        public string PartsCode {
            get;
            set;
        }

        public string PartsName {
            get;
            set;
        }
    }
}
