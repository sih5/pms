﻿namespace Sunlight.Api.Models {
    public class PartsStockShiftModel {
        public string SparePartCode { get; set; }
        public string SparePartName { get; set; }
        public string OriginalWarehouseAreaCode { get; set; }
        public int? StockQuantity { get; set; }
        public int? TraceProperty { get; set; }
        public int PartsStockId { get; set; }
    }
}
