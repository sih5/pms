﻿using System;

namespace Sunlight.Api.Models {
    public class PartsboxBillModel {
        public int? AbandonerId {
            get;
            set;
        }

        public string AbandonerName {
            get;
            set;
        }

        public DateTime? AbandonerTIme {
            get;
            set;
        }

        public string BoxCode {
            get;
            set;
        }

        public double? BoxHigh {
            get;
            set;
        }

        public double? BoxLong {
            get;
            set;
        }

        public int? BoxType {
            get;
            set;
        }

        public double? BoxWide {
            get;
            set;
        }

        public string BranchCode {
            get;
            set;
        }

        public int BranchId {
            get;
            set;
        }

        public string BranchName {
            get;
            set;
        }

        public double? Capacity {
            get;
            set;
        }

        public string Code {
            get;
            set;
        }

        public string ContractCode {
            get;
            set;
        }

        public DateTime? CreateTime {
            get;
            set;
        }

        public int? CreatorId {
            get;
            set;
        }

        public string CreatorName {
            get;
            set;
        }

        public string DealerCode {
            get;
            set;
        }

        public int DealerId {
            get;
            set;
        }

        public string DealerName {
            get;
            set;
        }

        public int? FinishId {
            get;
            set;
        }

        public string FinishName {
            get;
            set;
        }

        public DateTime? FinishTime {
            get;
            set;
        }

        public int Id {
            get;
            set;
        }

        public string Mark {
            get;
            set;
        }

        public int? ModifierId {
            get;
            set;
        }

        public string ModifierName {
            get;
            set;
        }

        public DateTime? ModifyTime {
            get;
            set;
        }

        public string PackBatchNumber {
            get;
            set;
        }

        public string PartsPickingCode {
            get;
            set;
        }

        public int? PartsPickingId {
            get;
            set;
        }

        public string Remark {
            get;
            set;
        }

        public int SalesCategoryId {
            get;
            set;
        }

        public string SalesCategoryName {
            get;
            set;
        }

        public int Status {
            get;
            set;
        }

        public string StorageCompanyCode {
            get;
            set;
        }

        public int StorageCompanyId {
            get;
            set;
        }

        public string StorageCompanyName {
            get;
            set;
        }

        public int StorageCompanyType {
            get;
            set;
        }

        public int? Texture {
            get;
            set;
        }

        public string WarehouseCode {
            get;
            set;
        }

        public int? WarehouseId {
            get;
            set;
        }

        public string WarehouseName {
            get;
            set;
        }

        public double? Weight {
            get;
            set;
        }
    }
}
