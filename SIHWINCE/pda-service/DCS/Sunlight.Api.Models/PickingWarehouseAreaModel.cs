﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Api.Models {
    public class PickingWarehouseAreaModel {

        /// <summary>
        /// 货位Id
        /// </summary>
        public int? WarehouseAreaId {
            get;
            set;
        }
        /// <summary>
        /// 货位编号
        /// </summary>
        public string WarehouseAreaCode {
            get;
            set;
        }

        /// <summary>
        /// 任务个数
        /// </summary>
        public int TaskNumber {
            get;
            set;
        }
    }
}
