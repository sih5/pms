﻿using System;
using System.Collections.Generic;

namespace Sunlight.Api.Models.Prints {
    public class PartsInboundPlanPrint {
        /// <summary>
        ///     入库计划编号
        /// </summary>
        public string Code {
            get;
            set;
        }

        /// <summary>
        ///     供货单位
        /// </summary>
        public string CounterpartCompanyName {
            get;
            set;
        }

        /// <summary>
        ///     制单时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }

        /// <summary>
        ///     制单人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }

        /// <summary>
        ///     入库类型
        /// </summary>
        public string InboundType {
            get;
            set;
        }

        /// <summary>
        /// 配件采购订单.配件采购订单类型或配件调拨单.调拨类型
        /// </summary>
        public string PurchaseOrderType {
            get;
            set;
        }

        /// <summary>
        ///     原始单据编号
        /// </summary>
        public string OriginalRequirementBillCode {
            get;
            set;
        }

        /// <summary>
        ///     收货单位,制单单位
        /// </summary>
        public string StorageCompanyName {
            get;
            set;
        }

        /// <summary>
        ///     入库计划清单
        /// </summary>
        public List<PartsInboundPlanDetailPrint> PartsInboundPlanDetails {
            get;
            set;
        }

        /// <summary>
        ///     打印人
        /// </summary>
        public string PrintUserName {
            get;
            set;
        }

        /// <summary>
        ///     打印时间
        /// </summary>
        public DateTime PrintTime {
            get;
            set;
        }

        /// <summary>
        /// 仓库名称
        /// </summary>
        public string WarehouseName {
            get;
            set;
        }

        /// <summary>
        /// 原单据编号
        /// </summary>
        public string SourceCode {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public string Status {
            get;
            set;
        }
    }
}
