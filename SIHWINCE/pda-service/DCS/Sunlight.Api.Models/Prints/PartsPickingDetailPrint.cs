﻿namespace Sunlight.Api.Models.Prints {
    public class PartsPickingDetailPrint {
        public string EnglishName {
            get;
            set;
        }

        public int? FallbackQty {
            get;
            set;
        }

        public int Id {
            get;
            set;
        }

        public int? PackingAmount {
            get;
            set;
        }

        public int PartsPickingId {
            get;
            set;
        }

        public int PlannedAmount {
            get;
            set;
        }

        public int? Quantity {
            get;
            set;
        }

        public string Remark {
            get;
            set;
        }

        public string SparePartCode {
            get;
            set;
        }

        public int SparePartId {
            get;
            set;
        }

        public string SparePartName {
            get;
            set;
        }

        public string WarehouseAreaCode {
            get;
            set;
        }

        public int? WarehouseAreaId {
            get;
            set;
        }

        /// <summary>
        /// 计量单位
        /// </summary>
        public string MeasureUnit {
            get;
            set;
        }

        /// <summary>
        /// 供应商图号
        /// </summary>
        public string DefaultSupplier {
            get;
            set;
        }

        /// <summary>
        /// 参考图号
        /// </summary>
        public string ReferenceCode {
            get;
            set;
        }
        /// <summary>
        ///     序号
        /// </summary>
        public int Serial {
            get;
            set;
        }
    }
}
