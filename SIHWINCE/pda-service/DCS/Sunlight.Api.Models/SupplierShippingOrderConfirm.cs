﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Api.Models {
    public class SupplierShippingOrderConfirm {
        public string Code {
            get;
            set;
        }

        public DateTime Date {
            get;
            set;
        }
    }
}
