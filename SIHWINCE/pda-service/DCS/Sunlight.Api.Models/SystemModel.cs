﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Sunlight.Api.Models {
    public class SystemModel {
        private List<GroupModel> groupModels = new List<GroupModel>();

        [XmlAttribute(AttributeName = "Id")]
        public int Id {
            get;
            set;
        }

        [XmlAttribute(AttributeName = "PageId")]
        public string PageId {
            get;
            set;
        }

        [XmlAttribute(AttributeName = "Name")]
        public string Name {
            get;
            set;
        }

        [XmlElement(ElementName = "Group")]
        public List<GroupModel> GroupModels {
            get {
                return this.groupModels;
            }
            set {
                this.groupModels = value;
            }
        }
    }
}
