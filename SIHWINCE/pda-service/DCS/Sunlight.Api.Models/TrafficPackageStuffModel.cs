﻿using System;

namespace Sunlight.Api.Models {
    public class TrafficPackageStuffModel {
        public int? BoxType {
            get;
            set;
        }

        public string BoxTypeValue {
            get;
            set;
        }

        public DateTime? CreateTime {
            get;
            set;
        }

        public int? CreatorId {
            get;
            set;
        }

        public string CreatorName {
            get;
            set;
        }

        public double? Height {
            get;
            set;
        }

        public int Id {
            get;
            set;
        }

        public double? Length {
            get;
            set;
        }

        public int? ModifierId {
            get;
            set;
        }

        public string ModifierName {
            get;
            set;
        }

        public DateTime? ModifyTime {
            get;
            set;
        }

        public DateTime? RowVersion {
            get;
            set;
        }

        public int? Status {
            get;
            set;
        }

        public int? Stuff {
            get;
            set;
        }

        public double? Volume {
            get;
            set;
        }

        public double? Width {
            get;
            set;
        }
    }
}
