﻿namespace Sunlight.Api.Models {
    public class WarehouseAreaWithPartsStockModel {
        public int AreaCategory {
            get;
            set;
        }

        public int? AreaCategoryId {
            get;
            set;
        }

        public int AreaKind {
            get;
            set;
        }

        public int BranchId {
            get;
            set;
        }

        public string EnglishName {
            get;
            set;
        }

        public int? Quantity {
            get;
            set;
        }

        public string Remark {
            get;
            set;
        }

        public string SparePartCode {
            get;
            set;
        }

        public int SparePartId {
            get;
            set;
        }

        public string SparePartName {
            get;
            set;
        }

        public int Status {
            get;
            set;
        }

        public int StorageCompanyId {
            get;
            set;
        }

        public int StorageCompanyType {
            get;
            set;
        }

        public int? TopLevelWarehouseAreaId {
            get;
            set;
        }

        public string WarehouseAreaCode {
            get;
            set;
        }

        public int WarehouseAreaId {
            get;
            set;
        }

        public int? WarehouseAreaParentId {
            get;
            set;
        }

        public string WarehouseCode {
            get;
            set;
        }

        public int WarehouseId {
            get;
            set;
        }

        public string WarehouseName {
            get;
            set;
        }

        public int WarehouseType {
            get;
            set;
        }
    }
}
