﻿namespace Sunlight.Api.Models {
    public class WarehouseOperatorModel {
        public int Id {
            get;
            set;
        }

        public int WarehouseId {
            get;
            set;
        }

        public string WarehouseCode {
            get;
            set;
        }

        public string WarehouseName {
            get;
            set;
        }
    }
}
