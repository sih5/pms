﻿using System.Web.Http;
using Sunlight.Api.Security;
using Sunlight.Api.Service.Extension;
using Sunlight.Api.Utils;

namespace Sunlight.Api.Service.Areas.Common.Controllers {
    /// <summary>
    ///     编号自动生成处理
    /// </summary>
    [RoutePrefix("api/CodeGenerate")]
    public class CodeGenerateController : CommonApiController {
        /// <summary>
        ///       自动生成编号
        /// </summary>
        /// <param name="entityName">编码规则的Name</param>
        /// <returns>流水号</returns>
        [HttpGet]
        [Route("GetByEntityName")]
        public MessageInfo<string> GetByEntityName(string entityName) {
            var msg = new MessageInfo<string>();
            msg.Data = this.DcsServiceImp.Generate(entityName);
            msg.Success = true;
            return msg;
        }

        /// <summary>
        ///     自动生成编号 
        /// </summary>
        /// <param name="entityName">编码规则的 Name</param>
        /// <returns>流水号</returns>
        [HttpGet]
        [Route("GetByCorpCode")]
        public MessageInfo<string> GetByCorpCode(string entityName) {
            var userInfo = FormsAuth.GetUserData();

            var msg = new MessageInfo<string>();
            msg.Data = this.DcsServiceImp.Generate(entityName, userInfo.EnterpriseCode);
            msg.Success = true;
            return msg;
        }
    }
}
