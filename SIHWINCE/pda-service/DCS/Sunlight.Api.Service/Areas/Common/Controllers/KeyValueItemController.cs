﻿using System.Collections.Generic;
using System.Web.Http;
using Sunlight.Api.Models;
using Sunlight.Api.Service.Extension;
using Sunlight.Api.Utils;

namespace Sunlight.Api.Service.Areas.Common.Controllers {
    /// <summary>
    /// 字典项处理
    /// </summary>
    [RoutePrefix("api/KeyValueItem")]
    public class KeyValueItemController : CommonApiController {
        /// <summary>
        /// 查询多个名称的字典项
        /// </summary>
        /// <returns>返回查询到的字典项结果集</returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("LoadByNames")]
        public MessageInfo<List<KeyValueItemModel>> LoadByNames(string[] names) {
            var messageInfo = new MessageInfo<List<KeyValueItemModel>>();

            if(names == null || names.Length <= 0) {
                messageInfo.Message = "参数不能为空";
                return messageInfo;
            }
            messageInfo.Data = DcsServiceImp.GetKeyValueItems(names);
            messageInfo.Success = true;
            messageInfo.Message = "加载成功";
            return messageInfo;
        }
    }
}
