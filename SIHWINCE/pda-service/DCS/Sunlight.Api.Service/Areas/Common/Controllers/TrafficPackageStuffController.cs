﻿using System.Collections.Generic;
using System.Web.Http;
using Sunlight.Api.Models;
using Sunlight.Api.Security;
using Sunlight.Api.Service.Extension;
using Sunlight.Api.Utils;

namespace Sunlight.Api.Service.Areas.Common.Controllers
{
    /// <summary>
    /// 字典项处理
    /// </summary>
    [RoutePrefix("api/TrafficPackageStuff")]
    public class TrafficPackageStuffController : CommonApiController
    {
        /// <summary>
        /// 查询多个名称的字典项
        /// </summary>
        /// <returns>返回查询到的字典项结果集</returns>
        [HttpGet]
        [Route("GetTrafficPackageStuff")]
        public MessageInfo<List<TrafficPackageStuffModel>> 查询运输包材()
        {
            var messageInfo = new MessageInfo<List<TrafficPackageStuffModel>>();
            //messageInfo.Data = this.DcsServiceImp.查询运输包材();
            messageInfo.Success = true;
            return messageInfo;
        }
    }
}
