﻿using System.Collections.Generic;
using System.Web.Http;
using Sunlight.Api.Models;
using Sunlight.Api.Service.Extension;
using Sunlight.Api.Utils;

namespace Sunlight.Api.Service.Areas.Common.Controllers {
    /// <summary>
    /// 仓库人员
    /// </summary>
    [RoutePrefix("api/WarehouseOperator")]
    public class WarehouseOperatorController : CommonApiController {
        /// <summary>
        /// 查询当前登录人的仓库列表
        /// </summary>
        /// <returns>仓库人员关系列表</returns>
        [HttpGet]
        [Route("GetWarehouseOperators")]
        public MessageInfo<List<WarehouseOperatorModel>> GetWarehouseOperators() {
            var messageInfor = new MessageInfo<List<WarehouseOperatorModel>>();

            messageInfor.Data = DcsServiceImp.GetWarehouseOperators();
            messageInfor.Success = true;
            return messageInfor;
        }
    }
}