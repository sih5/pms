﻿using Sunlight.Api.Service.Extension;
using System.Collections.Generic;
using System.Web.Http;
using Sunlight.Api.Models;
using Sunlight.Api.Utils;
using Sunlight.Api.Models.Extends;

namespace Sunlight.Api.Service.Areas.Storage.Controllers {
    /// <summary>
    /// 装箱业务
    /// </summary>
    [RoutePrefix("api/BoxUpTask")]
    public class BoxUpTaskController : CommonApiController {
        /// <summary>
        /// 根据单号查询待装箱任务单
        /// </summary>
        /// <param name="code">扫描单号</param>
        /// <param name="warehouseId">当前登陆仓库id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetBoxUpCodes")]
        public MessageInfo<List<BoxUpTaskModel>> GetBoxUpCodes(string code, int warehouseId) {
            var message = new MessageInfo<List<BoxUpTaskModel>>();

            message.Data = this.DcsServiceImp.根据单号查询待装箱任务单(code, warehouseId);
            message.Success = true;
            message.Message = "查询成功";

            return message;
        }

        /// <summary>
        /// 装箱
        /// </summary>
        /// <param name="boxUpTaskModels">手持装箱详情</param>
        /// <returns>未装箱完成的清单</returns>
        [HttpPost]
        [Route("BoxUping")]
        public MessageInfo<List<BoxUpTaskModel>> 装箱(List<BoxUpTaskModel> boxUpTaskModels) {
            this.DcsServiceImp.装箱(boxUpTaskModels);
            var messageInfo = new MessageInfo<List<BoxUpTaskModel>> {
                Success = true,
                Data = this.DcsServiceImp.根据单号查询待装箱任务单(boxUpTaskModels[0].Code, boxUpTaskModels[0].WarehouseId),
                Message = "装箱成功！"
            };
            return messageInfo;
        }

        /// <summary>
        /// 完成装箱
        /// </summary>
        /// <param name="boxUpTaskModels">装箱详细</param>
        /// <returns></returns>
        [HttpPost]
        [Route("BoxUpingOver")]
        public MessageInfo<bool> 完成装箱(List<BoxUpTaskModel> boxUpTaskModels) {
            var messageInfo = new MessageInfo<bool> {
                Success = true,
                Data = this.DcsServiceImp.装箱(boxUpTaskModels, true),
                Message = "装箱成功！"
            };
            return messageInfo;
        }
    }
}