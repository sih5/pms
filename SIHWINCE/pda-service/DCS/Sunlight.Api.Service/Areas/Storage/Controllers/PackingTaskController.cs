﻿using Sunlight.Api.Service.Extension;
using System.Collections.Generic;
using System.Web.Http;
using Sunlight.Api.Models;
using Sunlight.Api.Utils;

namespace Sunlight.Api.Service.Areas.Storage.Controllers {
    /// <summary>
    /// 包装业务
    /// </summary>
    [RoutePrefix("api/PackingTask")]
    public class PackingTaskController : CommonApiController {
        /// <summary>
        /// 查询待包装任务
        /// </summary>
        /// <param name="code">入库检验单编号</param>
        /// <param name="sparePartCode">配件编号</param>
        /// <param name="warehouseId">当前登陆所属仓库id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetPackingTask")]
        public MessageInfo<List<PackingTaskModel>> GetPackingTask(string code, string sparePartCode, int warehouseId) {
            var message = new MessageInfo<List<PackingTaskModel>>();

            message.Data = this.DcsServiceImp.查询待包装任务(code, sparePartCode, warehouseId);
            message.Success = true;
            message.Message = "查询成功";

            return message;
        }

        /// <summary>
        /// 包装登记
        /// </summary>
        /// <param name="packingTaskModel">包装任务单</param>
        /// <returns></returns>
        [HttpPost]
        [Route("Packing")]
        public MessageInfo<bool> 包装登记(PackingTaskModel packingTaskModel) {
            var messageInfo = new MessageInfo<bool> {
                Success = true,
                Data = this.DcsServiceImp.包装登记(packingTaskModel),
                Message = "包装成功！"
            };
            return messageInfo;
        }
    }
}