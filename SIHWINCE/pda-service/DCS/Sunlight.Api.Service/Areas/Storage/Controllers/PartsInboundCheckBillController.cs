﻿using System.Collections.Generic;
using System.Web.Http;
using Sunlight.Api.Models;
using Sunlight.Api.Service.Extension;
using Sunlight.Api.Utils;

namespace Sunlight.Api.Service.Areas.Storage.Controllers {
    /// <summary>
    /// 收货业务
    /// </summary>
    [RoutePrefix("api/ReceiptConfirmation")]
    public class PartsInboundCheckBillController : CommonApiController {
        /// <summary>
        /// 总部收货确认
        /// </summary>
        /// <param name="receivingOrders">收货详情</param>
        /// <returns></returns>
        [HttpPost]
        [Route("ConfirmReceive")]
        public MessageInfo<bool> 收货确认(List<ForReceivingOrderModel> receivingOrders) {
            var messageInfo = new MessageInfo<bool> {
                Success = true,
                Data = this.DcsServiceImp.收货确认(receivingOrders),
                Message = "收货成功！"
            };
            return messageInfo;
        }

        /// <summary>
        /// 根据编号查询待收货确认发运单
        /// </summary>
        /// <param name="code">发运单编号</param>
        /// <param name="warehouseId">当前登陆人所选仓库id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetShippingDetial")]
        public MessageInfo<List<PartsShippingOrderModel>> 根据编号查询待收货确认发运单(string code, int warehouseId) {
            var message = new MessageInfo<List<PartsShippingOrderModel>>();

            message.Data = DcsServiceImp.根据编号查询待收货确认发运单(code, warehouseId);
            message.Success = true;
            message.Message = "查询成功";

            return message;
        }

        /// <summary>
        /// 中心库收货确认
        /// </summary>
        /// <param name="receivingOrders">发运单详情</param>
        /// <returns></returns>
        [HttpPost]
        [Route("CenterConfirmReceive")]
        public MessageInfo<bool> 收货确认配件发运单(List<PartsShippingOrderModel> partsShippingOrderModels) {
            var messageInfo = new MessageInfo<bool> {
                Success = true,
                Data = this.DcsServiceImp.收货确认配件发运单(partsShippingOrderModels),
                Message = "收货成功！"
            };
            return messageInfo;
        }
    }
}
