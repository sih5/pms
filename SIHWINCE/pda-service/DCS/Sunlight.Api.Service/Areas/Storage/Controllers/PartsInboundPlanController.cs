﻿using Sunlight.Api.Service.Extension;
using System.Collections.Generic;
using System.Web.Http;
using Sunlight.Api.Models;
using Sunlight.Api.Utils;

namespace Sunlight.Api.Service.Areas.Storage.Controllers {
    /// <summary>
    /// 入库业务
    /// </summary>
    [RoutePrefix("api/PartsInboundPlan")]
    public class PartsInboundPlanController : CommonApiController {
        /// <summary>
        /// 查询可收货的入库计划（收货单）
        /// </summary>
        /// <param name="code">入库计划单编号</param>
        /// <param name="warehouseId">当前登陆人所选仓库id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetReceiptDetail")]
        public MessageInfo<List<ForReceivingOrderModel>> GetReceiptDetail(string code, int warehouseId) {
            var message = new MessageInfo<List<ForReceivingOrderModel>>();

            message.Data = DcsServiceImp.GetReceiptDetail(code, warehouseId);
            message.Success = true;
            message.Message = "查询成功";

            return message;
        }
    }
}
