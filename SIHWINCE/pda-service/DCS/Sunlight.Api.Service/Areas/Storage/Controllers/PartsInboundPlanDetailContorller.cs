﻿using Sunlight.Api.Service.Extension;
using System.Collections.Generic;
using System.Web.Http;
using Sunlight.Api.Models;
using Sunlight.Api.Utils;

namespace Sunlight.Api.Service.Areas.Storage.Controllers
{
    /// <summary>
    /// 查询配件入库计划清单
    /// </summary>
    [RoutePrefix("api/PartsInboundPlanDetail")]
    public class PartsInboundPlanDetailController : CommonApiController
    {
        /// <summary>
        /// 保存计划入库清单检验量
        /// </summary>
        /// <param name="partsInboundPlanDetailModel"></param>
        /// <returns>执行的结果</returns>
        [HttpPost]
        [Route("UpdatePartsInboundPlanDetails")]
        public MessageInfo UpdatePartsInboundPlanDetails(PartsInboundPlanDetailModel partsInboundPlanDetailModel)
        {
            var messageInfo = new MessageInfo();

            //DcsServiceImp.UpdatePartsInboundPlanDetails(partsInboundPlanDetailModel);

            messageInfo.Success = true;
            messageInfo.Message = "更新成功";
            return messageInfo;
        }


        /// <summary>
        /// 查询入库计划单
        /// </summary>
        /// <param name="code">入库计划编号</param>
        /// <returns>根据编号返回指定配件id，配件编号，供应商图号和待收数量</returns>
        [HttpGet]
        [Route("GetPartsInboundPlanDetailsByCode")]
        public MessageInfo<List<PartsInboundPlanDetailModel>> 查询入库计划(string code, int warehouseId)
        {
            var messageInfo = new MessageInfo<List<PartsInboundPlanDetailModel>>();
            //messageInfo.Data = DcsServiceImp.查询入库计划(code,warehouseId);
            messageInfo.Success = true;
            messageInfo.Message = "查询成功";
            return messageInfo;
        }
        /// <summary>
        ///  查询配件待收数量
        /// </summary>
        /// <param name="plansInboundPlanId">计划入库id</param>
        /// <param name="sparePartCode">配件编号</param>
        /// <returns>指定入库计划id和配件编号的配件待收数量</returns>
        [HttpGet]
        [Route("GetReceviedQuelities")]
        public MessageInfo<List<PartsInboundPlanDetailModel>> 查询配件待收数量(int plansInboundPlanId, string sparePartCode)
        {
            var messageInfo = new MessageInfo<List<PartsInboundPlanDetailModel>>();

            //messageInfo.Data = DcsServiceImp.查询配件待收数量(plansInboundPlanId, sparePartCode);
            messageInfo.Success = true;
            messageInfo.Message = "查询成功";

            return messageInfo;
        }


    }
}