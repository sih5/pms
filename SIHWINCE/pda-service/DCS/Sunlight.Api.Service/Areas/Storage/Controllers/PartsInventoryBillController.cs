﻿using Sunlight.Api.Service.Extension;
using System.Collections.Generic;
using System.Web.Http;
using Sunlight.Api.Models;
using Sunlight.Api.Utils;
using Sunlight.Api.Models.Extends;

namespace Sunlight.Api.Service.Areas.Storage.Controllers {
    /// <summary>
    /// 盘点业务
    /// </summary>
    [RoutePrefix("api/PartsInventoryBill")]
    public class PartsInventoryBillController : CommonApiController {
        /// <summary>
        /// 根据单号查询未完成的盘点单
        /// </summary>
        /// <param name="code">扫描盘点号</param>
        /// <param name="warehouseId">当前登陆仓库id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetInventoryBills")]
        public MessageInfo<List<PartsInventoryBillModel>> GetInventoryBills(string code, int warehouseId) {
            var message = new MessageInfo<List<PartsInventoryBillModel>>();

            message.Data = this.DcsServiceImp.根据单号查询未完成的盘点单(code, warehouseId);
            message.Success = true;
            message.Message = "查询成功";

            return message;
        }

        /// <summary>
        /// 盘点结果录入
        /// </summary>
        /// <param name="partsInventoryBills">盘点结果单据</param>
        /// <returns></returns>
        [HttpPost]
        [Route("ResultsInput")]
        public MessageInfo<bool> ResultsInput(List<PartsInventoryBillModel> partsInventoryBills) {
            var messageInfo = new MessageInfo<bool> {
                Success = true,
                Data = this.DcsServiceImp.盘点结果录入(partsInventoryBills),
                Message = "结果录入成功！"
            };
            return messageInfo;
        }
    }
}