﻿using System.Collections.Generic;
using System.Web.Http;
using Sunlight.Api.Models;
using Sunlight.Api.Models.Extends;
using Sunlight.Api.Service.Extension;
using Sunlight.Api.Utils;

namespace Sunlight.Api.Service.Areas.Storage.Controllers {
    /// <summary>
    /// 上架业务
    /// </summary>
    [RoutePrefix("api/PartsShelves")]
    public class PartsShelvesController : CommonApiController {
        /// <summary>
        /// 查询待上架任务
        /// </summary>
        /// <param name="code">入库检验单编号</param>
        /// <param name="sparePartCode">配件编号</param>
        /// <param name="warehouseId">当前登陆所属仓库id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetShelvesTask")]
        public MessageInfo<List<PartsShelvesModel>> GetShelvesTask(string code, string sparePartCode, int warehouseId) {
            var message = new MessageInfo<List<PartsShelvesModel>>();

            message.Data = this.DcsServiceImp.查询待上架任务(code, sparePartCode, warehouseId);
            message.Success = true;
            message.Message = "查询成功";

            return message;
        }


        /// <summary>
        /// 上架
        /// </summary>
        /// <param name="partsShelvesModel">手持上架单</param>
        /// <returns></returns>
        [HttpPost]
        [Route("Shelvesing")]
        public MessageInfo<bool> 上架(PartsShelvesModel partsShelvesModel) {
            var messageInfo = new MessageInfo<bool> {
                Success = true,
                Data = this.DcsServiceImp.上架(partsShelvesModel),
                Message = "上架成功！"
            };
            return messageInfo;
        }

        /// <summary>
        /// 根据配件编号查询检验区库存
        /// </summary>
        /// <param name="sparePartCode"></param>
        /// <param name="warehouseId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetCenterShelvesTask")]
        public MessageInfo<List<PartsStockShelvesExtend>> GetShelvesTask(string sparePartCode, int warehouseId) {
            var message = new MessageInfo<List<PartsStockShelvesExtend>>();

            message.Data = this.DcsServiceImp.根据配件编号查询检验区库存(sparePartCode, warehouseId);
            message.Success = true;
            message.Message = "查询成功";

            return message;
        }


        /// <summary>
        /// 中心库上架
        /// </summary>
        /// <param name="partsShelvesModel">手持上架单</param>
        /// <returns></returns>
        [HttpPost]
        [Route("CenterShelvesing")]
        public MessageInfo<bool> 中心库上架(List<PartsStockShelvesExtend> partsStockShelves) {
            var messageInfo = new MessageInfo<bool> {
                Success = true,
                Data = this.DcsServiceImp.中心库上架(partsStockShelves),
                Message = "上架成功！"
            };
            return messageInfo;
        }
    }
}