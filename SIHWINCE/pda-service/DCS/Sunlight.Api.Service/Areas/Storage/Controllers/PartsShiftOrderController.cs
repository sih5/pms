﻿using System.Collections.Generic;
using System.Web.Http;
using Sunlight.Api.Models;
using Sunlight.Api.Service.Extension;
using Sunlight.Api.Utils;

namespace Sunlight.Api.Service.Areas.Storage.Controllers {
    /// <summary>
    /// 移库业务
    /// </summary>
    [RoutePrefix("api/PartsShiftOrder")]
    public class PartsShiftOrderController : CommonApiController {

        /// <summary>
        /// 点击上架按钮，查询可以上架的移库单号
        /// </summary>
        /// <param name="code">移库单号</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetUpPartsShift")]
        public MessageInfo<List<PartsShiftOrderDTO>> GetUpPartsShift(string code) {
            var message = new MessageInfo<List<PartsShiftOrderDTO>>();

            message.Data = this.DcsServiceImp.GetUpPartsShift(code);
            message.Success = true;
            message.Message = "查询成功";
            return message;
        }
        /// <summary>
        /// 上架界面中，点击上传按钮后，更新上架数据
        /// </summary>
        /// <param name="partsShiftOrders">移库单</param>
        /// <returns></returns>
        [HttpPost]
        [Route("PartsShiftUping")]
        public MessageInfo<bool> PartsShiftUping(List<PartsShiftOrderDTO> partsShiftOrders) {
            var messageInfo = new MessageInfo<bool> {
                Success = true,
                Data = this.DcsServiceImp.PartsShiftUping(partsShiftOrders),
                Message = "上架成功！"
            };
            return messageInfo;
        }
        /// <summary>
        /// 点击上架按钮，查询可以下架的移库单号
        /// </summary>
        /// <param name="code">移库单号</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetDownPartsShift")]
        public MessageInfo<List<PartsShiftOrderDTO>> GetDownPartsShift(string code) {
            var message = new MessageInfo<List<PartsShiftOrderDTO>>();

            message.Data = this.DcsServiceImp.GetDownPartsShift(code);
            message.Success = true;
            message.Message = "查询成功";
            return message;
        }
        /// <summary>
        /// 下架界面中，点击上传按钮后，更新下架数据
        /// </summary>
        /// <param name="partsShiftOrders">移库单</param>
        /// <returns></returns>
        [HttpPost]
        [Route("PartsShiftDowning")]
        public MessageInfo<bool> PartsShiftDowning(List<PartsShiftOrderDTO> partsShiftOrders) {
            var messageInfo = new MessageInfo<bool> {
                Success = true,
                Data = this.DcsServiceImp.PartsShiftDowning(partsShiftOrders),
                Message = "下架成功！"
            };
            return messageInfo;
        }

        /// <summary>
        /// 随移
        /// </summary>
        /// <param name="partsShiftOrders"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("MoveShiftLibrary")]
        public MessageInfo<bool> MoveShiftLibrary(List<PartsShiftCustomizationDetailDTO> partsShiftOrders) {
            var messageInfo = new MessageInfo<bool> {
                Success = true,
                Data = this.DcsServiceImp.MoveShiftLibrary(partsShiftOrders),
                Message = "移库成功！"
            };
            return messageInfo;
        }
    }
}