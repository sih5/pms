﻿using System.Collections.Generic;
using System.Web.Http;
using Sunlight.Api.Models;
using Sunlight.Api.Models.Extends;
using Sunlight.Api.Service.Extension;
using Sunlight.Api.Utils;

namespace Sunlight.Api.Service.Areas.Storage.Controllers {
    /// <summary>
    /// 备件库存
    /// </summary>
    [RoutePrefix("api/PartsStock")]
    public class PartsStockController : CommonApiController {
        /// <summary>
        /// 根据备件编号查询配件库位和数量
        /// </summary>
        /// <param name="sparePartCode">备件编号参数</param>
        /// <param name="warehouseId">当前登录仓库</param>
        /// <param name="locationCode">库位编号</param>
        /// <param name="isTrue">数量是否大于0</param>
        /// <returns>备件库存列表</returns>
        [HttpGet]
        [Route("GetPartsStockBy")]
        public MessageInfo<List<PartsStockExtend>> 根据备件编号查询配件库位和数量(string sparePartCode, string locationCode, bool isTrue, int warehouseId) {
            var messageInfo = new MessageInfo<List<PartsStockExtend>> {
                Data = DcsServiceImp.根据备件编号查询配件库位和数量(sparePartCode, locationCode, isTrue, warehouseId),
                Success = true
            };
            return messageInfo;
        }

        /// <summary>
        /// 随移查询库存记录
        /// </summary>
        /// <param name="warehouseId"></param>
        /// <param name="locationCode"></param>
        /// <param name="sparePartCode"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetShiftStock")]
        public MessageInfo<PartsStockShiftModel> 随移查询库存记录(int warehouseId, string locationCode, string sparePartCode) {
            var messageInfo = new MessageInfo<PartsStockShiftModel> {
                Data = DcsServiceImp.随移查询库存记录(warehouseId, locationCode, sparePartCode),
                Success = true
            };
            return messageInfo;
        }
    }
}