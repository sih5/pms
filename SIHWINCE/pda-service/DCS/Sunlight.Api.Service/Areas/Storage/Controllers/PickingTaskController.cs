﻿using Sunlight.Api.Service.Extension;
using System.Collections.Generic;
using System.Web.Http;
using Sunlight.Api.Models;
using Sunlight.Api.Utils;
using Sunlight.Api.Models.Extends;

namespace Sunlight.Api.Service.Areas.Storage.Controllers {
    /// <summary>
    /// 拣货业务
    /// </summary>
    [RoutePrefix("api/PickingTask")]
    public class PickingTaskController : CommonApiController {

        /// <summary>
        /// 查询拣货任务库区列表
        /// </summary>
        /// <param name="warehouseId">当前登陆所属仓库id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetPickingAreas")]
        public MessageInfo<List<PickingWarehouseAreaModel>> GetPickingAreas(int warehouseId) {
            var message = new MessageInfo<List<PickingWarehouseAreaModel>>();

            message.Data = this.DcsServiceImp.查询拣货任务库区列表(warehouseId);
            message.Success = true;
            message.Message = "查询成功";

            return message;
        }

        /// <summary>
        /// 根据库区查询待拣货任务
        /// </summary>
        /// <param name="warehouseAreaId">库区id</param>
        /// <param name="warehouseId">当前登陆所属仓库id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetPickings")]
        public MessageInfo<List<PickingTaskExtend>> GetPickings(int warehouseAreaId, int warehouseId) {
            var message = new MessageInfo<List<PickingTaskExtend>>();

            message.Data = this.DcsServiceImp.根据库区查询待拣货任务(warehouseAreaId, warehouseId);
            message.Success = true;
            message.Message = "查询成功";

            return message;
        }

        /// <summary>
        /// 查询拣货任务单详情
        /// </summary>
        /// <param name="code">所选拣货任务单编号</param>
        /// <param name="warehouseAreaId">所选货位id</param>
        /// <param name="warehouseId">当前登陆所属仓库id</param>
        /// <returns>拣货任务单详情</returns>
        [HttpGet]
        [Route("GetPickingDetails")]
        public MessageInfo<List<PickingTaskModel>> GetPickingDetails(string code, int warehouseAreaId, int warehouseId) {
            var message = new MessageInfo<List<PickingTaskModel>>();

            message.Data = this.DcsServiceImp.查询拣货任务单详情(code, warehouseAreaId, warehouseId);
            message.Success = true;
            message.Message = "查询成功";

            return message;
        }

        /// <summary>
        /// 拣货
        /// </summary>
        /// <param name="pickingTaskModels">手持拣货详情</param>
        /// <returns></returns>
        [HttpPost]
        [Route("Picking")]
        public MessageInfo<bool> 拣货(List<PickingTaskModel> pickingTaskModels) {
            var messageInfo = new MessageInfo<bool> {
                Success = true,
                Data = this.DcsServiceImp.拣货(pickingTaskModels),
                Message = "拣货成功！"
            };
            return messageInfo;
        }

        /// <summary>
        /// 完成拣货
        /// </summary>
        /// <param name="pickingTaskModels">手持拣货详情</param>
        /// <returns></returns>
        [HttpPost]
        [Route("PickingOver")]
        public MessageInfo<bool> 完成拣货(List<PickingTaskModel> pickingTaskModels) {
            var messageInfo = new MessageInfo<bool> {
                Success = true,
                Data = this.DcsServiceImp.拣货(pickingTaskModels, true),
                Message = "拣货成功！"
            };
            return messageInfo;
        }
    }
}