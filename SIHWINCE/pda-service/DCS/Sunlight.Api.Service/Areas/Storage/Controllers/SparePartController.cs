﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Http;
using Sunlight.Api.Models;
using Sunlight.Api.Service.Extension;
using Sunlight.Api.Utils;

namespace Sunlight.Api.Service.Areas.Storage.Controllers
{
    /// <summary>
    ///     备件信息相关服务定义
    /// </summary>
    [RoutePrefix("api/SparePart")]
    public class SparePartController : CommonApiController
    {
        /// <summary>
        ///     查询备件信息
        /// </summary>
        /// <returns>返回根据条件查询到的备件信息</returns>
        public MessageInfo<List<SparePartModel>> Get()
        {
            var msgSparePart = new MessageInfo<List<SparePartModel>>();
            msgSparePart.Success = true;
            msgSparePart.Message = "";
            msgSparePart.Data = DcsServiceImp.GetSpareParts();
            return msgSparePart;
        }

        /// <summary>
        /// 根据编号查询备件
        /// </summary>
        /// <param name="sparePartCode">配件编号</param>
        /// <param name="warehouseId">仓库Id</param>
        /// <returns>匹配到当前编号的备件</returns>
        [HttpGet]
        [Route("GetSparePartsByCode")]
        public MessageInfo<List<SparePartModel>> GetSparePartsByCode(string sparePartCode, int warehouseId)
        {
            var msgSparePart = new MessageInfo<List<SparePartModel>>();
            msgSparePart.Success = true;
            msgSparePart.Message = "";
            //msgSparePart.Data = DcsServiceImp.GetSparePartsByCode(sparePartCode, warehouseId);
            return msgSparePart;
        }

        /// <summary>
        /// 根据编号查询备件生成包装移库单
        /// </summary>更新当前的配件信息
        /// <param name="sparePart">更新的配件信息</param>
        /// <returns>是否更新成功</returns>
        [HttpPost]
        [Route("UpdateSparePartBy")]
        public MessageInfo UpdateSparePartBy(SparePartModel sparePart)
        {
            var msgSparePart = new MessageInfo();
            //this.DcsServiceImp.UpdateSparePartBy(sparePart);

            msgSparePart.Success = true;
            msgSparePart.Message = "";

            return msgSparePart;
        }

        /// <summary>
        /// 查询配件待收数量
        /// </summary>
        /// <param name="partsInboundPlanId">配件入库计划id</param>
        /// <param name="sparePartCode">配件编号</param>
        /// <returns>待收数量</returns>
        [HttpGet]
        [Route("GetQuantity")]
        public MessageInfo<List<PartsInboundPlanDetailModel>> GetQuantity(int partsInboundPlanId, string sparePartCode)
        {
            var msgSparePart = new MessageInfo<List<PartsInboundPlanDetailModel>>();
            msgSparePart.Success = true;
            msgSparePart.Message = "";
            //msgSparePart.Data = DcsServiceImp.GetQuantity(partsInboundPlanId, sparePartCode);
            return msgSparePart;
        }
    }
}
