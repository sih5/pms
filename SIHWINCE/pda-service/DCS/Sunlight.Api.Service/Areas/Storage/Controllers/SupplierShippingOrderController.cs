﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Sunlight.Api.Models;
using Sunlight.Api.Models.Extends;
using Sunlight.Api.Service.Extension;
using Sunlight.Api.Utils;

namespace Sunlight.Api.Service.Areas.Storage.Controllers {
    /// <summary>
    /// 供应商发运业务
    /// </summary>
    [RoutePrefix("api/SupplierShippingOrder")]
    public class SupplierShippingOrderController : CommonApiController {
        /// <summary>
        /// 供应商到货确认
        /// </summary>
        /// <param name="data">发运单编号，承运商送达时间</param>
        /// <returns></returns>
        [HttpPost]
        [Route("Confirm")]
        public MessageInfo<bool> Confirm(SupplierShippingOrderConfirm data) {
            var message = new MessageInfo<bool>();
            message.Data = this.DcsServiceImp.供应商到货确认(data);
            message.Success = true;
            message.Message = "供应商到货确认成功";
            return message;
        }
    }
}
