﻿using System.Web.Http;
using Sunlight.Api.Service.Extension;
using Sunlight.Api.Utils;

namespace Sunlight.Api.Service.Areas.Storage.Controllers {
    [RoutePrefix("api/WarehouseArea")]
    public class WarehouseAreaController : CommonApiController {
        /// <summary>
        /// 根据配件id获取库位属性
        /// </summary>
        /// <param name="sparePartId">配件id</param>
        /// <param name="warehouseId">仓库id</param>
        /// <returns>指定id的库位属性</returns>
        [HttpGet]
        [Route("GetWarehouseAreas")]
        public MessageInfo<string> GetWarehouseAreas(int sparePartId, int warehouseId) {
            var messageInfo = new MessageInfo<string>();
            //messageInfo.Data = DcsServiceImp.GetWarehouseAreas(sparePartId, warehouseId);
            messageInfo.Success = true;
            messageInfo.Message = "查询成功";
            return messageInfo;
        }
    }
}
