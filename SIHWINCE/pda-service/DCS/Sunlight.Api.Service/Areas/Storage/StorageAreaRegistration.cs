﻿using System.Web.Mvc;

namespace Sunlight.Api.Service.Areas.Storage {
    public class StorageAreaRegistration : AreaRegistration {
        public override string AreaName {
            get {
                return "Storage";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) {
            context.MapRoute("Storage_default", "Storage/{controller}/{action}/{id}", new {
                action = "Index",
                id = UrlParameter.Optional
            }, new[] {
                "Sunlight.Api.Service.Areas.Storage.Controllers"
            });
        }
    }
}
