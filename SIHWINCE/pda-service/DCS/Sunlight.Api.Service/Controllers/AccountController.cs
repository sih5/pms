﻿using System;
using System.Web.Http;
using Sunlight.Api.BLL;
using Sunlight.Api.Models;
using Sunlight.Api.Security;
using Sunlight.Api.Service.Extension;
using Sunlight.Api.Service.Filters;
using Sunlight.Api.Utils;

namespace Sunlight.Api.Service.Controllers {
    /// <summary>
    ///     权限认证相关
    /// </summary>
    [AuthorizeFilter]
    [RoutePrefix("api/Account")]
    public class AccountController : CommonApiController {

        /// <summary>
        ///     登陆验证
        /// </summary>
        /// <returns>返回成功与否以及认证的Cookie</returns>
        [Route("Login")]
        [HttpPost]
        [AllowAnonymous]
        public MessageInfo<AppMenusModel> Login(AuthenticationUserModel model) {
            var imp = new SecurityImp();

            if(string.IsNullOrEmpty(model.EnterpriseCode))
                throw new Exception("企业编号不能为空");
            if(string.IsNullOrEmpty(model.LoginId))
                throw new Exception("用戶名不能为空");
            if(string.IsNullOrEmpty(model.Password))
                throw new Exception("登陆密码不能为空");

            return imp.Login(model);
        }

        /// <summary>
        ///     测试登陆后，有Cookie和没有Cookie是否能正常校验权限认证
        /// </summary>
        [Route("Valid")]
        [HttpGet]
        public IHttpActionResult Valid() {
            return Ok("请求成功");
        }

        /// <summary>
        ///     WINCE退出时调用或者注销时调用
        /// </summary>
        /// <returns></returns>
        [Route("Logout")]
        public IHttpActionResult Logout() {
            FormsAuth.SingOut();
            return Ok();
        }
    }
}
