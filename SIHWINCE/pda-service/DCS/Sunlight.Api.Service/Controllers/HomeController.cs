﻿using System.Web.Mvc;

namespace Sunlight.Api.Service.Controllers
{
    /// <summary>
    ///     主页
    /// </summary>
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "上汽依维柯红岩";

            return View();
        }
    }
}
