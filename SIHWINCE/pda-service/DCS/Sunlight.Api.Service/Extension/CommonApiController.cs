﻿using System.Web.Http;
using Sunlight.Api.BLL;
using Sunlight.Api.Service.Filters;

namespace Sunlight.Api.Service.Extension {
    /// <summary>
    ///     控制器公共基类抽取
    /// </summary>
    [AuthorizeFilter]
    [ExceptionFilter]
    public class CommonApiController : ApiController {
        private DcsServiceImp dcsServiceImp;

        /// <summary>
        ///     当前实体框架上下文
        /// </summary>
        protected DcsServiceImp DcsServiceImp {
            get {
                return dcsServiceImp ?? (dcsServiceImp = new DcsServiceImp());
            }
            set {
                dcsServiceImp = value;
            }
        }

        protected override void Dispose(bool disposing) {
            if(this.DcsServiceImp != null)
                this.DcsServiceImp = null;

            base.Dispose(disposing);
        }
    }
}
