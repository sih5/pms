﻿using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;
using Newtonsoft.Json;
using NLog;

namespace Sunlight.Api.Service.Extension {
    /// <summary>
    /// 异常公共类
    /// </summary>
    public class ExceptionFilter : ExceptionFilterAttribute {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public override void OnException (HttpActionExecutedContext actionExecutedContext) {
            // WINCE接收到的请求，除了200的，都按照的是WebException进行处理的，这里统一拦截后，根据Success标记是否成功完成请求，通过Message告知客户端，处理结果
            var message = new HttpResponseMessage();
            var optimisticConcurrencyException = actionExecutedContext.Exception as OptimisticConcurrencyException;
            string msgInfo;
            if(optimisticConcurrencyException != null) {
                msgInfo = "当前数据已被其他设备更新，请查询后重试";
            } else
                msgInfo = actionExecutedContext.Exception.InnerException == null ? actionExecutedContext.Exception.Message : actionExecutedContext.Exception.InnerException.Message;

            message.Content = new StringContent(JsonConvert.SerializeObject(new {
                Success = false,
                Message = msgInfo
            }));
            message.StatusCode = HttpStatusCode.OK;
            actionExecutedContext.Response = message;
            var isNeedWriteLog = !(actionExecutedContext.Exception is ValidationException);
            if(isNeedWriteLog) {
                Logger.Error(msgInfo, actionExecutedContext.Exception);
                Logger.Trace(actionExecutedContext.Exception.StackTrace);
            }
        }
    }
}