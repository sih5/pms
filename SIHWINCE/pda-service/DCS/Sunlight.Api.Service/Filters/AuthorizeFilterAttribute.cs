﻿using System;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Sunlight.Api.Security;

namespace Sunlight.Api.Service.Filters {
    /// <summary>
    ///     是否有访问权限检查
    /// </summary>
    public class AuthorizeFilterAttribute : AuthorizationFilterAttribute {
        public override void OnAuthorization(HttpActionContext actionContext) {
            var cookies = HttpContext.Current.Request.Cookies;

            //匿名用户，不进行认证校验
            if(actionContext.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any())
                return;

            if(cookies.Count <= 0)
                throw new Exception("无效的请求,缺少安全认证信息");

            var authenticationUser = FormsAuth.GetUserData<AuthorizeUser>();
            if(authenticationUser == null)
                throw new Exception("无效的请求,缺少正确的Cookie信息");

            if(HttpContext.Current.Request.Cookies["expires"] == null)
                throw new Exception("无效的请求,缺少正确的过期设置");

            var expires = HttpContext.Current.Request.Cookies["expires"].Value;
            if(string.IsNullOrEmpty(expires))
                throw new Exception("无效的请求,缺少正确的过期设置");

            DateTime expireDate;
            var isValidExpire = DateTime.TryParse(expires, out expireDate);
            if(!isValidExpire)
                throw new Exception("无效的请求,过期信息错误");

            if(expireDate < DateTime.Now)
                throw new Exception("登录会话过期，请重新登陆");
        }
    }
}
