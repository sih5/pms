﻿# 项目注意事项

### 开发事项
	
- API访问，暂时采用匿名用户访问，待权限完善后，需要在调用的API控制器上或者具体的API上标记 `[Authorize]`
- API代码书写，放置区域 `Areas`下的
- 控制器继承自 `CommonApiController`，目的是处理公共的异常，以及其他的重用内容
- 依据目前公司.NET项目的开发特点，返回的JSON采用Pascal命名
- 定义的Model以Model结尾
- JSON.NET对Entity的对象序列化，不支持，所以序列化实体的时候，需要构造Model，然后序列化Model
- 不建议将序列化后的string直接返回，异常不要进行处理
- 所有的API必须有xml注释（用来生成API文档）
- 使用DbMonitor监听，查询的SQL是否存在性能上的问题

注意：查看md文件，推荐使用Visual Studio Code进行查看