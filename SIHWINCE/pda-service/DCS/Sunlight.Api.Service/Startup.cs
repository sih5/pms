﻿using Microsoft.Owin;
using Owin;
using Sunlight.Api.Service;

[assembly: OwinStartup(typeof(Startup))]

namespace Sunlight.Api.Service {
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
