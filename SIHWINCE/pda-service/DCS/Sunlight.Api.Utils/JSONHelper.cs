﻿using System;
using System.Web.Script.Serialization;

namespace Sunlight.Api.Utils {
    public static class JSONHelper {
        /// <summary>
        /// 对象序列化扩展
        /// </summary>
        /// <param name="obj">序列化对象</param>
        /// <returns>序列化内容</returns>
        public static string ToJSON(this object obj) {
            var serializer = new JavaScriptSerializer();
            try {
                return serializer.Serialize(obj);
            } catch(Exception) {
                return "";
            }
        }
    }
}
