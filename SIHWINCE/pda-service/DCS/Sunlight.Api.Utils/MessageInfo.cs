﻿namespace Sunlight.Api.Utils {
    /// <summary>
    ///     返回信息封装
    /// </summary>
    public class MessageInfo<T> {
        /// <summary>
        ///     返回的信息描述
        /// </summary>
        public string Message {
            get;
            set;
        }

        /// <summary>
        ///     返回的数据
        /// </summary>
        public T Data {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public bool Success {
            get;
            set;
        }
    }

    /// <summary>
    ///     返回信息封装
    /// </summary>
    public class MessageInfo {
        /// <summary>
        ///     返回的信息描述
        /// </summary>
        public string Message {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public bool Success {
            get;
            set;
        }
    }
}
