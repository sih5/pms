# 开发环境搭建

- Windows 7 SP1的旗舰版(专业版和Windows 10验证无法支持Virtual PC 2007),是为了设置模拟器的网卡，让模拟器能链接到模拟器之外的网络
- IDE安装Visual Studio 2008， [软件路径](\\192.168.0.202\软件仓库\0.ISO\MS.Visual.Studio\Visual Studio 2008)           
- 安装Window Mobild 设备中心64位, [下载链接](https://www.microsoft.com/zh-cn/download/confirmation.aspx?id=3182)
- 安装Virtual PC 2007，[下载链接](https://www.microsoft.com/en-us/download/details.aspx?id=4580) 
- 安装Resharper，Ankhsvn
- IDE导入 `DevelopingEnvironment.vssettings`配置
- Resharper导入 `DMS.sln.DotSettings`配置

### 模拟器联网
- [参考资料](http://www.cnblogs.com/c51port/archive/2011/03/12/1981995.html)

*推荐查看md工具 Visual Studio Code*