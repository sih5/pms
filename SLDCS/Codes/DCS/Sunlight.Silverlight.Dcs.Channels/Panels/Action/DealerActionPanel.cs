﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;

namespace Sunlight.Silverlight.Dcs.Channels.Panels.Action {
    public class DealerActionPanel : DcsActionPanelBase {
        public DealerActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Dealer",
                Title = ChannelsUIStrings.ActionPanel_Title_Dealer,
                ActionItems = new[] {
                    new ActionItem {
                    //    Title = ChannelsUIStrings.Action_Title_AddTogether,
                    //    UniqueId = "AddTogether",
                    //    ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Channels/AddTogether.png"),
                    //    CanExecute = false
                    //}, new ActionItem {
                        Title = ChannelsUIStrings.Action_Title_EditTogether,
                        UniqueId = "EditTogether",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Channels/EditTogether.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = ChannelsUIStrings.Action_Title_Detail,
                        UniqueId = "Detail",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Detail.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = ChannelsUIStrings.Action_Title_DetailTogether,
                        UniqueId = "DetailTogether",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Channels/DetailTogether.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = ChannelsUIStrings.Action_Title_BatchBindingTel,
                        UniqueId = "BatchBindingTel",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Menu/Common/BatchBindingTel.png"),
                        CanExecute = false
                    }, new ActionItem{
                       Title = ChannelsUIStrings.Action_Title_ImportEdit,
                       UniqueId = "ImportEdit",
                       ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                       CanExecute = false
                    }, new ActionItem {
                        Title = ChannelsUIStrings.Action_Title_BatchImport,
                        UniqueId = "BatchImport",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    }, new ActionItem{
                       Title = ChannelsUIStrings.Action_Title_UpdateDistance,
                       UniqueId = "UpdateDistance",
                       ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Edit.png"),
                       CanExecute = false
                       
                    }
                }
            };
        }
    }
}
