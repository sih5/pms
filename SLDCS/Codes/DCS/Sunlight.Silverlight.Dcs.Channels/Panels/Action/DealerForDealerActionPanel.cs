﻿
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;

namespace Sunlight.Silverlight.Dcs.Channels.Panels.Action {
    public class DealerForDealerActionPanel : DcsActionPanelBase {
        public DealerForDealerActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = ChannelsUIStrings.ActionPanel_Title_Dealer,
                ActionItems = new[] {
                    new ActionItem {
                        Title = ChannelsUIStrings.Action_Title_EditTogether,
                        UniqueId = "EditTogether",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Channels/EditTogether.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = ChannelsUIStrings.Action_Title_DetailTogether,
                        UniqueId = "DetailTogether",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Channels/DetailTogether.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
