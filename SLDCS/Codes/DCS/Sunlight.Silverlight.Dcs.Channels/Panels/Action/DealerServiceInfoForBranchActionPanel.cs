﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;

namespace Sunlight.Silverlight.Dcs.Channels.Panels.Action {
    public class DealerServiceInfoForBranchActionPanel : DcsActionPanelBase {
        public DealerServiceInfoForBranchActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "DealerServiceInfoForBranch",
                Title = ChannelsUIStrings.Action_Title_DealerServiceInfoForBranch,
                ActionItems = new[] {
                     new ActionItem {
                        Title = ChannelsUIStrings.Action_Title_ImportEdit,
                        UniqueId = "BranchImport",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        Title = ChannelsUIStrings.Action_Title_BatchImport,
                        UniqueId = "ImportMainInfo",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    },
                      new ActionItem {
                        Title = ChannelsUIStrings.Action_Title_ImportOverlapInfo,
                        UniqueId = "ImportOverlapInfo",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = ChannelsUIStrings.Action_Title_ImportForEdito,
                        UniqueId = "ImportForEdito",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    },  new ActionItem{
                       Title = ChannelsUIStrings.Action_Title_UpdateDistance,
                       UniqueId = "UpdateDistance",
                       ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Edit.png"),
                       CanExecute = false
                    }
                }
            };
        }
    }
}
