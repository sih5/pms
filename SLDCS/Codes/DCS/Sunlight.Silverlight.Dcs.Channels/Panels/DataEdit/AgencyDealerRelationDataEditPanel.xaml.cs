﻿
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Channels.Panels.DataEdit {
    public partial class AgencyDealerRelationDataEditPanel {
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();
        public AgencyDealerRelationDataEditPanel() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.Initializer.Register(this.Initialize);
            this.Loaded += AgencyDealerRelationDataEditPanel_Loaded;//便于默认值
        }

        private void AgencyDealerRelationDataEditPanel_Loaded(object sender, RoutedEventArgs e)
        {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(er => er.Status == (int)DcsMasterDataStatus.有效 && er.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if (loadOp.HasError)
                    return;
                this.KvPartsSalesCategorys.Clear();
                foreach (var PartsSalesCategory in loadOp.Entities)
                    this.KvPartsSalesCategorys.Add(new KeyValuePair
                    {
                        Key = PartsSalesCategory.Id,
                        Value = PartsSalesCategory.Name
                    });
                this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            }, null);
        }

        public string BrachName {
            get {
                return BaseApp.Current.CurrentUserData.EnterpriseName;
            }
        }

        private void Initialize() {
            //var dcsDomainContext = new DcsDomainContext();
            //dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    foreach(var PartsSalesCategory in loadOp.Entities)
            //        this.KvPartsSalesCategorys.Add(new KeyValuePair {
            //            Key = PartsSalesCategory.Id,
            //            Value = PartsSalesCategory.Name
            //        });
            //    this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            //}, null);
        }

        private void CreateUI() {
            var queryWindow = DI.GetQueryWindow("AgencyAffiBranch");
            queryWindow.Loaded += queryWindow_Loaded;
            queryWindow.SelectionDecided += queryWindow_SelectionDecided;
            this.ptbAgencyName.PopupContent = queryWindow;
            var dealerQueryWindow = DI.GetQueryWindow("Dealer");
            dealerQueryWindow.SelectionDecided += dealerQueryWindow_SelectionDecided;
            this.ptbDealerName.PopupContent = dealerQueryWindow;
        }
        private void queryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;
            var filterItem = new FilterItem();
            filterItem.MemberName = "BranchId";
            filterItem.MemberType = typeof(int);
            filterItem.Operator = FilterOperator.IsEqualTo;
            filterItem.Value = BaseApp.Current.CurrentUserData.EnterpriseId;
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", filterItem);
        }
        void dealerQueryWindow_SelectionDecided(object sender, System.EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var dealer = queryWindow.SelectedEntities.Cast<Dealer>().SingleOrDefault();
            if(dealer == null)
                return;

            var agencyDealerRelation = this.DataContext as AgencyDealerRelation;
            if(agencyDealerRelation == null)
                return;
            agencyDealerRelation.DealerId = dealer.Id;
            agencyDealerRelation.DealerCode = dealer.Code;
            agencyDealerRelation.DealerName = dealer.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }
        void queryWindow_SelectionDecided(object sender, System.EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var agencyAffiBranch = queryWindow.SelectedEntities.Cast<AgencyAffiBranch>().SingleOrDefault();
            if(agencyAffiBranch == null)
                return;
            var agency = agencyAffiBranch.Agency;
            if(agency == null)
                return;
            var agencyDealerRelation = this.DataContext as AgencyDealerRelation;
            if(agencyDealerRelation == null)
                return;
            agencyDealerRelation.AgencyId = agencyAffiBranch.AgencyId;
            agencyDealerRelation.AgencyCode = agency.Code;
            agencyDealerRelation.AgencyName = agency.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }
        public ObservableCollection<KeyValuePair> KvPartsSalesCategorys {
            get {
                return this.kvPartsSalesCategorys;
            }
        }
    }
}
