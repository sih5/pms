﻿namespace Sunlight.Silverlight.Dcs.Channels.Panels.DataEdit {
    public partial class ChannelCapabilityDataEditPanel {
        private readonly string[] kvNames = {
            "Channel_Grade"
        };

        public ChannelCapabilityDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }
        public object KvChannelGrade {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
    }
}
