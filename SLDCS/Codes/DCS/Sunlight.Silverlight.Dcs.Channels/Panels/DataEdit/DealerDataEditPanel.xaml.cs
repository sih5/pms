﻿using System.Text.RegularExpressions;
using System.Windows.Input;
using Sunlight.Silverlight.Dcs.Web;
using System.Windows;
using System.ServiceModel.DomainServices.Client;
﻿using System;
﻿using System.Linq;
using Telerik.Windows.Controls;
using System.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Channels.Panels.DataEdit {
    public partial class DealerDataEditPanel {
        private string code;
        private readonly DcsDomainContext domainContext = new DcsDomainContext();
        public DealerDataEditPanel() {
            InitializeComponent();
             this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += this.DataEditPanel_DataContextChanged;
        }

        private void DataEditPanel_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var dealer = this.DataContext as Dealer;
            if (dealer == null)
                return;
            if (dealer.Code != null) {
                this.code = dealer.Code;
            } else {
                this.code = string.Empty;
            }
        }

        private void UIElement_OnKeyDown(object sender, KeyEventArgs e) {
            var rg = new Regex("^[\u4e00-\u9fa5\b]$"); //\b是退格键     
            if(!rg.IsMatch(e.Key.ToString())) {
                e.Handled = true;

            }
        }

        private void CreateUI() {
            this.radButtonSearch.Click += this.RadButtonSearch_Click;

            var customerInfo = DI.GetQueryWindow("CustomerInfo");
            this.popupTextBoxCode.PopupContent = customerInfo;
            customerInfo.SelectionDecided += popupTextBoxCode_SelectionDecided;
        }
        private void popupTextBoxCode_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if (queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var virtualIdNameForTmp = queryWindow.SelectedEntities.Cast<VirtualIdNameForTmp>().FirstOrDefault();
            if (virtualIdNameForTmp == null)
                return;
            var dealer = this.DataContext as Dealer;
            if (dealer == null)
                return;
            dealer.Code = virtualIdNameForTmp.Code;
            dealer.Name = virtualIdNameForTmp.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if (parent != null)
                parent.Close();
        }

         private void RadButtonSearch_Click(object sender, RoutedEventArgs e) {
             domainContext.Load(domainContext.getCustomerNameByCodeForTmpQuery(code),LoadBehavior.RefreshCurrent, loadOp => {
                 if (loadOp.HasError)
                     return;
                 var dealer = this.DataContext as  Dealer;
                 if (dealer != null && dealer.Id != 0) { 
                     foreach (var item in loadOp.Entities){
                         dealer.Name = item.Name;
                     }
                 }
             }, null);
        }
    }
}
