﻿
namespace Sunlight.Silverlight.Dcs.Channels.Panels.DataEdit {
    public partial class DealerKeyPositionDataEditPanel {

        public DealerKeyPositionDataEditPanel() {
            this.InitializeComponent();
        }

        public string BrachName {
            get {
                return BaseApp.Current.CurrentUserData.EnterpriseName;
            }
        }
    }
}
