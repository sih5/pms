﻿using System.ComponentModel;
using System.Windows.Input;

namespace Sunlight.Silverlight.Dcs.Channels.Panels.DataEdit {
    public partial class DealerServiceExtDataEditPanel : INotifyPropertyChanged {
        private KeyValueManager keyValueManager;
        private readonly string[] kvNames = {
            "DealerServiceExt_VehicleTravelRoute","DealerServiceExt_VehicleUseSpeciality","DealerServiceExt_VehicleDockingStations","Repair_Qualification_Grade","YesOrNo","RetainedCustomer_Location"
        };
        private new KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        public DealerServiceExtDataEditPanel() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
        }
        protected virtual void OnPropertyChanged(string propertyName) {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        private void CreateUI() {
            KeyValueManager.LoadData();
        }
        public object VehicleTravelRoutes {
            get {
                return this.keyValueManager[this.kvNames[0]];

            }
        }

        public object GeographicPositions {
            get {
                return this.keyValueManager[this.kvNames[5]];

            }
        }

        private void UIElement_OnKeyDown(object sender, KeyEventArgs e) {
            if(e.PlatformKeyCode == 189 || e.PlatformKeyCode == 229 || e.PlatformKeyCode == 109)
                e.Handled = true;
        }
        public object VehicleUseSpecialitys {
            get {
                return this.keyValueManager[this.kvNames[1]];
            }
        }
        public object VehicleDockingStationss {
            get {
                return this.keyValueManager[this.kvNames[2]];
            }
        }

        public object RepairQualifications {
            get {
                return this.keyValueManager[this.kvNames[3]];
            }
        }

        public object DangerousRepairQualifications {
            get {
                return this.keyValueManager[this.kvNames[4]];
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
