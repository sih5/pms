﻿namespace Sunlight.Silverlight.Dcs.Channels.Panels.DataEdit {
    public partial class DealerServiceInfoForDealerDataEditPanel {
        private readonly string[] kvNames = {
            "ServicePermission","Invoice_Type","DlrSerInfo_TrunkNetworkType","ExternalState"
        };

        public DealerServiceInfoForDealerDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }

        public object KvExternalStates {
            get {
                return this.KeyValueManager[this.kvNames[3]];
            }
        }
    }
}
