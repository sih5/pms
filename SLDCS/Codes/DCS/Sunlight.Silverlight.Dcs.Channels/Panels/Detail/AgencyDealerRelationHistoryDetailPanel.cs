﻿using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Channels.Views.DataGrid;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Dcs.Channels.Resources;

namespace Sunlight.Silverlight.Dcs.Channels.Panels.Detail {
    public class AgencyDealerRelationHistoryDetailPanel : AgencyDealerRelationHistoryDataGridView, IDetailPanel {

        public Uri Icon {
            get {
                return null;
            }
        }

        public string Title {
            get {
                return ChannelsUIStrings.DetailPanel_Title_AgencyDealerRelationHistory;
            }
        }
    }
}
