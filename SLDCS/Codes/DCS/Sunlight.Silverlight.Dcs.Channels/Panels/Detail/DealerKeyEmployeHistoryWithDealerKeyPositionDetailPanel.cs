﻿using System;
using System.Windows;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Channels.Views.DataGrid;

namespace Sunlight.Silverlight.Dcs.Channels.Panels.Detail {
    public class DealerKeyEmployeHistoryWithDealerKeyPositionDetailPanel : DealerKeyEmployeHistoryWithDealerKeyPositionDataGridView, IDetailPanel {
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DomainDataSource.PageSize = 10;
            this.GridView.ShowGroupPanel = false;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
        }

        public string Title {
            get {
                return ChannelsUIStrings.DetailPanel_Title_DealerKeyEmployeHistory;
            }
        }

        public Uri Icon {
            get {
                return null;
            }
        }
    }
}
