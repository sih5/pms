﻿namespace Sunlight.Silverlight.Dcs.Channels.Panels.Detail {
    public partial class DealerServiceExtDetailPanel {
        private readonly string[] kvNames = { "DealerServiceExt_VehicleTravelRoute", "DealerServiceExt_VehicleUseSpeciality", "DealerServiceExt_VehicleDockingStations", "Repair_Qualification_Grade", "YesOrNo", "RetainedCustomer_Location" };
        public DealerServiceExtDetailPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
