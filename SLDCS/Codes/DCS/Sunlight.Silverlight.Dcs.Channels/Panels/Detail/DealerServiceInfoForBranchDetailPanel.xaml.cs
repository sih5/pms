﻿
namespace Sunlight.Silverlight.Dcs.Channels.Panels.Detail {
    public partial class DealerServiceInfoForBranchDetailPanel {
        private readonly string[] kvNames = new[] {
            "ServicePermission","Invoice_Type","DlrSerInfo_TrunkNetworkType","ExternalState"
        };
        public DealerServiceInfoForBranchDetailPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }

    }
}
