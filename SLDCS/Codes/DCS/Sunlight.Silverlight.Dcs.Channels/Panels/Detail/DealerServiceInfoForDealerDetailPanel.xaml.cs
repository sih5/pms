﻿
namespace Sunlight.Silverlight.Dcs.Channels.Panels.Detail {
    public partial class DealerServiceInfoForDealerDetailPanel {

        private readonly string[] kvNames = new[] {
            "ServicePermission","Invoice_Type","DlrSerInfo_TrunkNetworkType","ExternalState"
        };

        public DealerServiceInfoForDealerDetailPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }

    }
}
