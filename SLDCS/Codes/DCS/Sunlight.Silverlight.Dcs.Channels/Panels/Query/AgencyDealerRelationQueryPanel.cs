﻿
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.Channels.Panels.Query {
    public class AgencyDealerRelationQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        public AgencyDealerRelationQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(AgencyDealerRelation),
                    Title = ChannelsUIStrings.QueryPanel_Title_AgencyDealerRelation,
                    QueryItems = new QueryItem[]{
                        new CustomQueryItem{
                          ColumnName = "Company.Code",
                          Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_Agency_Code,
                          DataType = typeof(string)
                        },new CustomQueryItem{
                          ColumnName = "Company.Name",
                          Title =  ChannelsUIStrings.QueryPanel_QueryItem_Title_Agency_Name,
                          DataType = typeof(string)
                        },new CustomQueryItem {
                          ColumnName = "Company1.Code",
                          Title = ChannelsUIStrings.QueryPanel_QueryItem_DealerServiceInfo_Title_Dealer_Code,
                          DataType = typeof(string)
                        },new CustomQueryItem{
                          ColumnName = "Company1.Name" ,
                          Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_Dealer_Name,
                          DataType = typeof(string)
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            DefaultValue = (int)DcsBaseDataStatus.有效,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        },new DateTimeRangeQueryItem{ 
                            ColumnName = "CreateTime",
                        },new CustomQueryItem {
                            Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_Agency_Area,
                            ColumnName = "SalesRegionName",
                            DataType = typeof(string)
                        },new CustomQueryItem {
                            Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_Agency_MarketingDepartment,
                            ColumnName = "MarketingDepartmentName",
                                   DataType = typeof(string)
                        }
                    }
                }
            };
        }
    }
}
