﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Channels.Panels.Query {
    public class ChannelCapabilityQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "MasterData_Status"
        };

        public ChannelCapabilityQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(ChannelCapability),
                    Title = ChannelsUIStrings.QueryPanel_Title_ChannelCapability,
                    QueryItems = new QueryItem[] {
                         new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsMasterDataStatus.有效,
                            IsExact = true
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            IsExact = true
                        }
                    }
                }
            };
        }
    }
}
