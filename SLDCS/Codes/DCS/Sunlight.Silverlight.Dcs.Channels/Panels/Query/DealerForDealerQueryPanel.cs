﻿
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Channels.Panels.Query {
    public class DealerForDealerQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "MasterData_Status"
        };

        public DealerForDealerQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(Dealer),
                    Title = ChannelsUIStrings.QueryPanel_Title_DealerInfo,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_DealerInfo_Code
                        },
                        new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsMasterDataStatus.有效,
                            IsExact = false
                        },
                        new QueryItem {
                            ColumnName = "Name",
                            Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_DealerInfo_Name,
                        },
                        new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            IsExact = false
                        }
                    }
                }
            };
        }
    }
}