﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Channels.Panels.Query {
    public class DealerForSelectQueryPanel : DcsQueryPanelBase {
        public DealerForSelectQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(Dealer),
                    Title = ChannelsUIStrings.QueryPanel_Title_Dealer,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code"
                        }, new QueryItem {
                            ColumnName = "Name"
                        }
                    }
                }
            };
        }
    }
}
