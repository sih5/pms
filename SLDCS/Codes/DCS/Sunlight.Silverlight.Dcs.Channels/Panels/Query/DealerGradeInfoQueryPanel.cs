﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Channels.Panels.Query {
    public class DealerGradeInfoQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };
        private ObservableCollection<KeyValuePair> kvBrands = new ObservableCollection<KeyValuePair>();

        private DcsDomainContext domainContext = new DcsDomainContext();
        public DealerGradeInfoQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(e => e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                }
                foreach(var item in loadOp.Entities) {
                    this.kvBrands.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.Name
                    });
                }
            }, null);
            this.QueryItemGroups = new[] { 
                new QueryItemGroup{
                    UniqueId = "Common",
                    Title = ChannelsUIStrings.QueryPanel_Title_DealerGradeInfo,
                    EntityType = typeof(GradeCoefficient),
                    QueryItems = new[] {
                        new KeyValuesQueryItem{
                            ColumnName = "PartsSalesCategoryId",
                            KeyValueItems = this.kvBrands,
                            Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name
                        }, new QueryItem{
                            ColumnName = "GradeName",
                            Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_DealerGradeInfo_GradeName
                        }, new KeyValuesQueryItem{
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = DcsBaseDataStatus.有效
                        }, new DateTimeRangeQueryItem{
                            ColumnName = "CreateTime",
                            //DefaultValue = new[]{
                            //    new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                            //}
                        }
                    }
                }
            };
        }

    }
}
