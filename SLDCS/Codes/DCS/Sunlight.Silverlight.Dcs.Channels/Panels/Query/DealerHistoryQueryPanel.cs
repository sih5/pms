﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Channels.Panels.Query {
    public class DealerHistoryQueryPanel : DcsQueryPanelBase {
        public DealerHistoryQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(DealerHistory),
                    Title = ChannelsUIStrings.QueryPanel_Title_DealerHistory,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code"
                        },
                        new QueryItem {
                            ColumnName = "Name"
                        },
                        new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            IsExact = false
                        }
                    }
                }
            };
        }
    }
}
