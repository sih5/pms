﻿
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Channels.Panels.Query {
    public class DealerKeyEmployeeForSelectQueryPanel : DcsQueryPanelBase {
        public DealerKeyEmployeeForSelectQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategorys in loadOp.Entities)
                    this.kvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = partsSalesCategorys.Id,
                        Value = partsSalesCategorys.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(DealerKeyEmployee),
                    Title = ChannelsUIStrings.QueryPanel_Title_DealerKeyEmployee,
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {  
                            Title =ChannelsUIStrings.QueryPanel_QueryItem_Title_DealerKeyEmployee_Dealer,
                            ColumnName = "Dealer.Name",
                            DataType =typeof(string)
                        }, new KeyValuesQueryItem {
                            Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name,
                            ColumnName = "PartsSalesCategoryId",
                            KeyValueItems=this.kvPartsSalesCategorys,
                            IsEnabled = false
                        },new CustomQueryItem{
                            Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerKeyPositionName,
                            ColumnName="DealerKeyPosition.PositionName",
                            DataType = typeof(string)
                        },new DateTimeRangeQueryItem {
                            Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_CreateTime,
                            ColumnName="CreateTime"
                        }
                    }
                }
            };
        }
    }
}
