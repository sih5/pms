﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Channels.Panels.Query {
    public class DealerKeyEmployeeQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvKeyPositions = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvIsObJob = new ObservableCollection<KeyValuePair>();
    
        public DealerKeyEmployeeQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var branch in loadOp.Entities)
                    this.kvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name
                    });
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategorys in loadOp.Entities)
                    this.kvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = partsSalesCategorys.Id,
                        Value = partsSalesCategorys.Name
                    });
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetDealerKeyPositionsQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOpKey => {
                if(loadOpKey.HasError)
                    return;
                foreach(var dealerKeyPosition in loadOpKey.Entities)
                    this.kvKeyPositions.Add(new KeyValuePair {
                        Key = dealerKeyPosition.Id,
                        Value = dealerKeyPosition.PositionName
                    });
            }, null);
            this.kvIsObJob.Add(new KeyValuePair {
                Key = 0,
                Value = ChannelsUIStrings.QueryPanel_QueryItem_Title_No
            });
            this.kvIsObJob.Add(new KeyValuePair {
                Key = 1,
                Value = ChannelsUIStrings.QueryPanel_QueryItem_Title_Yes
            });

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = ChannelsUIStrings.QueryPanel_Title_DealerKeyEmployee,
                    EntityType = typeof(DealerKeyEmployee),
                    QueryItems = new[] {
                        new KeyValuesQueryItem {
                            Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_Branch_Name,
                            ColumnName = "DealerKeyPosition.BranchId",
                            DefaultValue = BaseApp.Current.CurrentUserData.EnterpriseId,
                            KeyValueItems = this.kvBranches,
                            IsEnabled=false
                        },new KeyValuesQueryItem {
                            Title = ChannelsUIStrings.DataEditPanel_Text_PartsSalesCategory_Name,
                            ColumnName = "PartsSalesCategoryId",
                            KeyValueItems = this.kvPartsSalesCategorys,
                        }, new CustomQueryItem {
                            Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_DealerInfo_Name,
                            ColumnName = "Dealer.Name",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                            Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_DealerInfo_Code,
                            ColumnName = "Dealer.Code",
                            DataType = typeof(string)
                        }, new KeyValuesQueryItem {
                            Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_DealerKeyPosition_PositionName,
                            ColumnName = "KeyPositionId",
                            KeyValueItems = kvKeyPositions
                        },new QueryItem {
                             ColumnName = "Name",
                             Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_DealerKeyEmployee_Name
                        },new CustomQueryItem {
                              ColumnName = "MarketingDepartment",
                              DataType = typeof(string),
                              Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_Agency_MarketingDepartment
                        },new DateTimeRangeQueryItem {
                             ColumnName = "CreateTime"
                        }  ,new QueryItem{
                              ColumnName="IsKeyPositions",
                              Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_DealerKeyPosition_IsKeyPositions
                         },
                         new ComboQueryItem{
                              ColumnName="IsOnJob",
                              Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_DealerKeyPosition_IsOnJob,
                              DisplayMemberPath="Value",
                              SelectedValuePath="Key",
                             ItemsSource=this.kvIsObJob
                            
                        }

                    }
                }
            };
        }
    }
}
