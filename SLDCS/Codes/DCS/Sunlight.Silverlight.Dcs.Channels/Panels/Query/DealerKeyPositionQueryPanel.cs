﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Channels.Panels.Query {
    public class DealerKeyPositionQueryPanel : DcsQueryPanelBase {

        public DealerKeyPositionQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = ChannelsUIStrings.QueryPanel_Title_DealerKeyPosition,
                    EntityType = typeof(DealerKeyPosition),
                    QueryItems = new[] {
                        new CustomQueryItem {
                            Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_Branch_Name,
                            ColumnName = "Branch.Name",
                            DefaultValue = BaseApp.Current.CurrentUserData.EnterpriseName,
                            IsEnabled = false,
                            DataType = typeof(string)
                        }, new QueryItem {
                            Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_DealerKeyPosition_PositionName,
                            ColumnName = "PositionName"
                        }
                        ,new QueryItem{
                              ColumnName="IsKeyPositions",
                              Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_DealerKeyPosition_IsKeyPositions,
                            
                        }
                    }
                }
            };
        }
    }
}
