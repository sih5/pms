﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Core.ViewModel;
using System.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Channels.Panels.Query {
    public class DealerServiceInfoForBranchQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvUsedPartsWarehouses = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
            "MasterData_Status","DealerServiceInfo_ServiceStationType"
        };

        public DealerServiceInfoForBranchQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
            this.Initializer.Register(this.viewModel.Initialize);
        }
        private readonly DealerServiceInfoForBranchQueryPanelViewModel viewModel = new DealerServiceInfoForBranchQueryPanelViewModel();
        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            //dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    foreach(var partsSalesCategorys in loadOp.Entities)
            //        this.kvPartsSalesCategorys.Add(new KeyValuePair {
            //            Key = partsSalesCategorys.Id,
            //            Value = partsSalesCategorys.Name
            //        });
            //}, null);
            dcsDomainContext.Load(dcsDomainContext.GetUsedPartsWarehousesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var usedPartsWarehouses in loadOp.Entities)
                    this.kvUsedPartsWarehouses.Add(new KeyValuePair {
                        Key = usedPartsWarehouses.Id,
                        Value = usedPartsWarehouses.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = ChannelsUIStrings.QueryPanel_Title_DealerServiceInfoForBranch,
                    EntityType = typeof(DealerServiceInfo),
                    QueryItems = new [] {
                        new CustomQueryItem {
                            ColumnName = "Dealer.Code",
                            DataType = typeof(string),
                            Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_DealerServiceInfo_DealerCode
                        },new CustomQueryItem {
                            ColumnName = "Dealer.Name",
                            DataType = typeof(string),
                            Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_DealerServiceInfo_DealerName
                        },new QueryItem {
                            ColumnName = "BusinessCode"
                        },new QueryItem {
                            ColumnName = "BusinessName"
                        },new ComboQueryItem {  ColumnName = "PartsSalesCategoryId",
                         //   KeyValueItems = this.KvPartsSalesCategorys,
                            Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name,
                            SelectedValuePath="Id",
                            DisplayMemberPath="Name",
                            ItemsSource=this.viewModel.KvPartsSalesCategorys,
                            SelectedItemBinding = new Binding("SelectedPartsSalesCategory") {
                                    Source = this.viewModel,
                                    Mode = BindingMode.TwoWay
                                },
                        },new CustomQueryItem {
                            Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_Agency_MarketingDepartment,
                            ColumnName = "MarketingDepartment.Name",
                            DataType = typeof(string)
                        },new KeyValuesQueryItem {
                            Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_UsedPartsWarehouse_Name,
                            ColumnName = "UsedPartsWarehouseId",
                            KeyValueItems = this.kvUsedPartsWarehouses,
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        },
                        new KeyValuesQueryItem {
                            Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_DelarServiceInfo_ServiceStationType,
                            ColumnName = "ServiceStationType",
                            KeyValueItems =this.KeyValueManager[this.kvNames[1]]
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }

                    }
                }
            };
        }

        public class DealerServiceInfoForBranchQueryPanelViewModel : ViewModelBase
        {

            public ObservableCollection<PartsSalesCategory> kvPartsSalesCategorys;
            private PartsSalesCategory partsSalesCategory;
            private PagedCollectionView partsPurchaseOrderTypes;

            public readonly ObservableCollection<DealerGradeInfo> allMarketingDepartments = new ObservableCollection<DealerGradeInfo>();
            public ObservableCollection<PartsSalesCategory> KvPartsSalesCategorys
            {
                get
                {
                    if (this.kvPartsSalesCategorys == null)
                        this.kvPartsSalesCategorys = new ObservableCollection<PartsSalesCategory>();
                    return kvPartsSalesCategorys;
                }
            }


            public PartsSalesCategory SelectedPartsSalesCategory
            {
                get
                {
                    return this.partsSalesCategory;
                }
                set
                {
                    if (this.partsSalesCategory == value)
                        return;
                    this.partsSalesCategory = value;
                    this.NotifyOfPropertyChange("PartsSalesCategory");
                    this.MarketingDepartments.Refresh();
                }
            }
            public PagedCollectionView MarketingDepartments
            {
                get
                {
                    if (this.partsPurchaseOrderTypes == null)
                    {
                        this.partsPurchaseOrderTypes = new PagedCollectionView(this.allMarketingDepartments);
                        this.partsPurchaseOrderTypes.Filter = o => ((DealerGradeInfo)o).PartsSalesCategoryId == (this.SelectedPartsSalesCategory != null ? this.SelectedPartsSalesCategory.Id : int.MinValue);
                    }
                    return this.partsPurchaseOrderTypes;
                }
            }

            public override void Validate()
            {
                //
            }

            public void Initialize()
            {
                var domainContext = new DcsDomainContext();
                //domainContext.Load(domainContext.GetDealerGradeInfoesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                //    if (loadOp.HasError)
                //        return;
                //    foreach (var partsPurchaseOrderTypes in loadOp.Entities)
                //        this.allMarketingDepartments.Add(partsPurchaseOrderTypes);
                //}, null);
                domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(e => e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                    if (loadOp.HasError)
                        return;
                    this.kvPartsSalesCategorys.Clear();
                    foreach (var partsSalesCategories in loadOp.Entities)
                        this.KvPartsSalesCategorys.Add(partsSalesCategories);
                }, null);
            }
        }
    }
}
