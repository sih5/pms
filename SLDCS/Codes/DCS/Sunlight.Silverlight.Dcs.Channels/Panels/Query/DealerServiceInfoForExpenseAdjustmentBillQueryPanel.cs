﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Channels.Panels.Query {
    public class DealerServiceInfoForExpenseAdjustmentBillQueryPanel : DcsQueryPanelBase {
        public DealerServiceInfoForExpenseAdjustmentBillQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(DealerServiceInfo),
                    Title = ChannelsUIStrings.QueryPanel_Title_Dealer,
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                            ColumnName = "Dealer.Code",
                            Title=ChannelsUIStrings.QueryPanel_QueryItem_DealerServiceInfo_Title_Dealer_Code,
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                            ColumnName = "Dealer.Name",
                              Title=ChannelsUIStrings.QueryPanel_QueryItem_Title_Dealer_Name,
                            DataType = typeof(string)
                        }
                    }
                }
            };
        }
    }
}
