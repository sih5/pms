﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.Channels.Panels.Query {
    public class DealerServiceInfoForSelectQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames ={
            "MasterData_Status"
            };
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();

        public DealerServiceInfoForSelectQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var branch in loadOp.Entities)
                    this.kvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name
                    });
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategorys in loadOp.Entities)
                    this.kvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = partsSalesCategorys.Id,
                        Value = partsSalesCategorys.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(DealerServiceInfo),
                    Title = ChannelsUIStrings.QueryPanel_Title_DealerServiceInfo,
                    QueryItems = new QueryItem[] {
                        new KeyValuesQueryItem {  
                            Title =ChannelsUIStrings.QueryPanel_QueryItem_Title_Branch_Name,
                            ColumnName = "BranchId",
                            KeyValueItems=this.kvBranches
                        }, new KeyValuesQueryItem {
                            Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name,
                            ColumnName = "PartsSalesCategoryId",
                            KeyValueItems=this.kvPartsSalesCategorys
                        },new CustomQueryItem {
                            Title=ChannelsUIStrings.QueryPanel_QueryItem_DealerServiceInfo_Title_Dealer_Code,
                            ColumnName="Dealer.Code",
                            DataType = typeof(string)
                        },new CustomQueryItem {
                            Title=ChannelsUIStrings.QueryPanel_QueryItem_DealerServiceInfo_Title_Dealer_Name,
                            ColumnName="Dealer.Name",
                            DataType = typeof(string)
                        },new KeyValuesQueryItem{
                            ColumnName="Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            IsExact = false
                        }
                    }
                }
            };
        }
    }
}
