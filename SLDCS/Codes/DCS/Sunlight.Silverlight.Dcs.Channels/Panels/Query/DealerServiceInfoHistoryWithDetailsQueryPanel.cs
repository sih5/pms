﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Channels.Panels.Query {
    public class DealerServiceInfoHistoryWithDetailsQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvUsedPartsWarehouses = new ObservableCollection<KeyValuePair>();

        public DealerServiceInfoHistoryWithDetailsQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategorys in loadOp.Entities)
                    this.kvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = partsSalesCategorys.Id,
                        Value = partsSalesCategorys.Name
                    });
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetUsedPartsWarehousesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var usedPartsWarehouses in loadOp.Entities)
                    this.kvUsedPartsWarehouses.Add(new KeyValuePair {
                        Key = usedPartsWarehouses.Id,
                        Value = usedPartsWarehouses.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = ChannelsUIStrings.QueryPanel_Title_DealerServiceInfoHistory,
                    EntityType = typeof(DealerServiceInfoHistory),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                            ColumnName = "Dealer.Code",
                            DataType = typeof(string),
                            Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_DealerServiceInfo_DealerCode
                        }, new CustomQueryItem {
                            ColumnName = "Dealer.Name",
                            DataType = typeof(string),
                            Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_DealerServiceInfo_DealerName
                        },new KeyValuesQueryItem {
                            Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name,
                            ColumnName = "PartsSalesCategoryId",
                            KeyValueItems = this.kvPartsSalesCategorys,
                        }, new CustomQueryItem {
                            Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_MarketingDepartment_Name,
                            ColumnName = "MarketingDepartment.Name",
                            DataType = typeof(string)
                        },new KeyValuesQueryItem {
                            Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_UsedPartsWarehouse_Name,
                            ColumnName = "UsedPartsWarehouseId",
                            KeyValueItems = this.kvUsedPartsWarehouses,
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        }
                    }
                }
            };

        }
    }
}
