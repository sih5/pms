﻿
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.Channels.Panels.Query {
    public class PersonSubDealerQueryPanel : DcsQueryPanelBase {
        public PersonSubDealerQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[]{
                 new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(PersonSubDealer),
                    Title = ChannelsUIStrings.QueryPanel_Title_PersonSubDealer,
                    QueryItems = new[] {
                        new CustomQueryItem{
                          ColumnName = "SubDealer.Code",
                          Title=ChannelsUIStrings.QueryPanel_QueryItem_Title_SubDealer_Code,
                          DataType=typeof(string)
                        },new CustomQueryItem {
                          ColumnName = "SubDealer.Name",
                          Title=ChannelsUIStrings.QueryPanel_QueryItem_Title_SubDealer_Name,
                          DataType=typeof(string)
                        },new CustomQueryItem{ 
                          ColumnName = "Personnel.Name",
                          Title=ChannelsUIStrings.QueryPanel_QueryItem_Title_Personnel_Name,
                          DataType=typeof(string)
                        }
                    }
                }
            };
        }
    }
}
