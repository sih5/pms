﻿
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.Channels.Panels.Query {
    public class SubDealerQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames ={
            "MasterData_Status"
        };

        public SubDealerQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            this.QueryItemGroups = new[]{
                 new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(SubDealer),
                    Title = ChannelsUIStrings.QueryPanel_Title_SubDealer,
                    QueryItems = new[] {
                        new QueryItem {
                          ColumnName = "Name"  
                        },new QueryItem{
                          ColumnName = "Code"
                        },new DateTimeRangeQueryItem{ 
                            ColumnName = "CreateTime",
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }
                    }
                }
            };
        }
    }
}
