﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Channels.Panels.Query {
    public class SubdealerForQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames ={
            "BaseData_Status"
        };

        public SubdealerForQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            this.QueryItemGroups = new[]{
                 new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(SubDealer),
                    Title = ChannelsUIStrings.QueryPanel_Title_SubDealer,
                    QueryItems = new[] {
                        new QueryItem {
                          ColumnName = "Name"  
                        },new QueryItem{
                          ColumnName = "Code"
                        },new CustomQueryItem{
                          ColumnName = "Dealer.Code",
                          Title = ChannelsUIStrings.QueryPanel_QueryItem_DealerServiceInfo_Title_Dealer_Code,
                          DataType = typeof(string)
                        },new CustomQueryItem{
                          ColumnName = "Dealer.Name",
                          Title = ChannelsUIStrings.QueryPanel_QueryItem_DealerServiceInfo_Title_Dealer_Name,
                          DataType = typeof(string)
                        }, new DateTimeRangeQueryItem{ 
                            ColumnName = "CreateTime",
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }
                    }
                }
            };
        }
    }
}
