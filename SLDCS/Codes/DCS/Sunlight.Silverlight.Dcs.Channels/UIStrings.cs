﻿using System.Linq;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs.Channels {
    public class UIStrings : ModelBase {
        public void Refresh(params string[] propertyNames) {
            this.NotifyOfPropertiesChange(propertyNames.Length == 0 ? this.GetType().GetProperties().Select(p => p.Name).ToArray() : propertyNames);
        }

        public string DataEditPanel_Text_ChannelCapability_Name {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_ChannelCapability_Name;
            }
        }

        public string DataEditPanel_Text_Region_City {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_Region_City;
            }
        }

        public string DataEditPanel_Text_Region_District {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_Region_District;
            }
        }

        public string DataEditPanel_Text_Region_Province {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_Region_Province;
            }
        }

        public string DataEditPanel_Text_Region_Region {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_Region_Region;
            }
        }

        public string DataEditPanel_Text_DealerServiceExt_DangerousRepairQualificationBefore {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_DealerServiceExt_DangerousRepairQualificationBefore;
            }
        }

        public string DataEditPanel_Text_DealerServiceExt_DangerousRepairQualificationAfter {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_DealerServiceExt_DangerousRepairQualificationAfter;
            }
        }

        public string DataEditPanel_Text_Dealer_BillAmountUnit {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_Dealer_BillAmountUnit;
            }
        }

        public string DataEditPanel_Text_Dealer_RegisterCapitalUnit {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_Dealer_RegisterCapitalUnit;
            }
        }

        public string DataEditPanel_Text_Dealer_TaxRateUnit {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_Dealer_TaxRateUnit;
            }
        }

        public string QueryPanel_Title_TiledRegion {
            get {
                return Resources.ChannelsUIStrings.QueryPanel_Title_TiledRegion;
            }
        }

        public string DataEditView_GroupTitle_DealerServiceInfo {
            get {
                return Resources.ChannelsUIStrings.DataEditView_GroupTitle_DealerServiceInfo;
            }
        }

        public string DataEditView_GroupTitle_GeneralInformation {
            get {
                return Resources.ChannelsUIStrings.DataEditView_GroupTitle_GeneralInformation;
            }
        }

        public string DataEditPanel_Text_ChannelCapability_Range {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_ChannelCapability_Range;
            }
        }

        public string DataEditPanel_Text_ChannelCapability_IsPart {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_ChannelCapability_IsPart;
            }
        }

        public string DataEditPanel_Text_ChannelCapability_IsSale {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_ChannelCapability_IsSale;
            }
        }

        public string DataEditPanel_Text_ChannelCapability_IsService {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_ChannelCapability_IsService;
            }
        }

        public string DataEditPanel_Text_ChannelCapability_IsSurvey {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_ChannelCapability_IsSurvey;
            }
        }

        public string DataEditPanel_Text_AgencyDealerRelation_AgencyName {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_AgencyDealerRelation_AgencyName;
            }
        }

        public string DataEditPanel_Text_AgencyDealerRelation_DealerName {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_AgencyDealerRelation_DealerName;
            }
        }

        public string QueryWindow_Title_AgencyAffiBranch {
            get {
                return Resources.ChannelsUIStrings.QueryWindow_Title_AgencyAffiBranch;
            }
        }

        public string QueryWindow_Title_Dealer {
            get {
                return Resources.ChannelsUIStrings.QueryWindow_Title_Dealer;
            }
        }

        public string DataEditPanel_Text_PartsSalesCategory_Name {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_PartsSalesCategory_Name;
            }
        }

        public string DataGridView_Error_dealerBusinessPermit_ServiceProductLine {
            get {
                return Resources.ChannelsUIStrings.DataGridView_Error_dealerBusinessPermit_ServiceProductLine;
            }
        }

        public string DataEditPanel_Text_MarketingDepartment_Name {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_MarketingDepartment_Name;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsSalesCategory_Names {
            get {
                return Resources.ChannelsUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Names;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsSalesCategory_Name {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategory_Name;
            }
        }

        public string QueryPanel_QueryItem_Title_Dealer_DealerName {
            get {
                return Resources.ChannelsUIStrings.QueryPanel_QueryItem_Title_Dealer_DealerName;
            }
        }

        public string DataGridView_ColumnItem_Title_LaborHourUnitPriceGrade_Name {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_LaborHourUnitPriceGrade_Name;
            }
        }

        public string DataEditPanel_Text_DealerServiceInfo_MarketingDepartmentName {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_DealerServiceInfo_MarketingDepartmentName;
            }
        }

        public string DataEditPanel_Text_GradeCoefficient_Grade {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_GradeCoefficient_Grade;
            }
        }

        public string QueryPanel_QueryItem_Title_MarketingDepartment_Name {
            get {
                return Resources.ChannelsUIStrings.QueryPanel_QueryItem_Title_MarketingDepartment_Name;
            }
        }

        public string DataGridView_ColumnItem_Title_Dealer_PartsSalesCategory_Name {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_PartsSalesCategory_Name;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerBusinessPermit_PartsSalesCategory_Name {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerBusinessPermit_PartsSalesCategory_Name;
            }
        }

        public string QueryPanel_Title_SubDealer {
            get {
                return Resources.ChannelsUIStrings.QueryPanel_Title_SubDealer;
            }
        }

        public string QueryWindow_Title_SubDealer {
            get {
                return Resources.ChannelsUIStrings.QueryWindow_Title_SubDealer;
            }
        }

        public string QueryPanel_QueryItem_Title_Personnel_Name {
            get {
                return Resources.ChannelsUIStrings.QueryPanel_QueryItem_Title_Personnel_Name;
            }
        }

        public string QueryPanel_QueryItem_Title_SubDealer_Code {
            get {
                return Resources.ChannelsUIStrings.QueryPanel_QueryItem_Title_SubDealer_Code;
            }
        }

        public string QueryPanel_QueryItem_Title_SubDealer_Name {
            get {
                return Resources.ChannelsUIStrings.QueryPanel_QueryItem_Title_SubDealer_Name;
            }
        }

        public string DataManagementView_Confirm_Delete {
            get {
                return Resources.ChannelsUIStrings.DataManagementView_Confirm_Delete;
            }
        }

        public string DataManagementView_Notification_DeleteSuccess {
            get {
                return Resources.ChannelsUIStrings.DataManagementView_Notification_DeleteSuccess;
            }
        }

        public string DataEditView_Text_PersonSubDealer_SubDealer {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Text_PersonSubDealer_SubDealer;
            }
        }

        public string QueryPanel_Title_Personnel {
            get {
                return Resources.ChannelsUIStrings.QueryPanel_Title_Personnel;
            }
        }

        public string DataEditView_Title_PersonSubDealerDataEditView {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Title_PersonSubDealerDataEditView;
            }
        }

        public string DataGridView_ColumnItem_Title_Personnel_SequeueNumber {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_Personnel_SequeueNumber;
            }
        }

        public string DataManagementView_Title_PersonSubDealer {
            get {
                return Resources.ChannelsUIStrings.DataManagementView_Title_PersonSubDealer;
            }
        }

        public string QueryPanel_Title_PersonSubDealer {
            get {
                return Resources.ChannelsUIStrings.QueryPanel_Title_PersonSubDealer;
            }
        }

        public string DataEditView_Error_PersonSubDealer_PersonnelName {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Error_PersonSubDealer_PersonnelName;
            }
        }

        public string QueryPanel_QueryItem_DealerServiceInfo_Title_Dealer_Code {
            get {
                return Resources.ChannelsUIStrings.QueryPanel_QueryItem_DealerServiceInfo_Title_Dealer_Code;
            }
        }

        public string QueryPanel_QueryItem_DealerServiceInfo_Title_Dealer_Name {
            get {
                return Resources.ChannelsUIStrings.QueryPanel_QueryItem_DealerServiceInfo_Title_Dealer_Name;
            }
        }

        public string QueryWindow_Title_DealerServiceInfo {
            get {
                return Resources.ChannelsUIStrings.QueryWindow_Title_DealerServiceInfo;
            }
        }

        public string DataManagementView_Title_Subdealer {
            get {
                return Resources.ChannelsUIStrings.DataManagementView_Title_Subdealer;
            }
        }

        public string DataEditView_Validation_SubDealer_CodeIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_SubDealer_CodeIsNull;
            }
        }

        public string DataEditView_Validation_SubDealer_NameIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_SubDealer_NameIsNull;
            }
        }

        public string DataEditView_Validation_PersonSubDealer_SubDealerIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_PersonSubDealer_SubDealerIsNull;
            }
        }

        public string DataEditView_Text_UsedPartsWarehouse {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Text_UsedPartsWarehouse;
            }
        }

        public string QueryWindow_Title_MarketingDepartment {
            get {
                return Resources.ChannelsUIStrings.QueryWindow_Title_MarketingDepartment;
            }
        }

        public string QueryWindow_Title_UsedPartsWarehouse {
            get {
                return Resources.ChannelsUIStrings.QueryWindow_Title_UsedPartsWarehouse;
            }
        }

        public string DataEditView_Text_DealerServiceExt_BrandScope {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Text_DealerServiceExt_BrandScope;
            }
        }

        public string DataEditPanel_GroupTitle_BasicInfo {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_GroupTitle_BasicInfo;
            }
        }

        public string DataEditView_GroupTitle_ScaleInfo {
            get {
                return Resources.ChannelsUIStrings.DataEditView_GroupTitle_ScaleInfo;
            }
        }

        public string DataEditView_Text_DealerServiceExt_CompetitiveBrandScope {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Text_DealerServiceExt_CompetitiveBrandScope;
            }
        }

        public string DataEditView_Text_DealerServiceExt_EmployeeNumber {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Text_DealerServiceExt_EmployeeNumber;
            }
        }

        public string DataEditView_Text_DealerServiceInfo_ServiceStationType {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Text_DealerServiceInfo_ServiceStationType;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfo_ServiceStationType {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_ServiceStationType;
            }
        }

        public string DataEditView_Validation_DealerServiceInfo_AccreditTimeIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_DealerServiceInfo_AccreditTimeIsNull;
            }
        }

        public string DataEditView_Validation_DealerServiceInfo_IsOnDutyIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_DealerServiceInfo_IsOnDutyIsNull;
            }
        }

        public string DataEditView_Validation_DealerServiceInfo_OutFeeGradeIdIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_DealerServiceInfo_OutFeeGradeIdIsNull;
            }
        }

        public string DataEditView_Validation_DealerServiceInfo_PartsManagingFeeGradeIdIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_DealerServiceInfo_PartsManagingFeeGradeIdIsNull;
            }
        }

        public string DataEditView_Validation_DealerServiceInfo_MarketingDepartmentIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_DealerServiceInfo_MarketingDepartmentIsNull;
            }
        }

        public string DataEditView_Validation_DealerServiceInfo_GradeCoefficientIdIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_DealerServiceInfo_GradeCoefficientIdIsNull;
            }
        }

        public string DataEditView_Validation_DealerServiceInfo_RepairAuthorityGradeIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_DealerServiceInfo_RepairAuthorityGradeIsNull;
            }
        }

        public string DataEditView_Text_DealerServiceExt_RepairQualification {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Text_DealerServiceExt_RepairQualification;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfo_RepairAuthorityGrade {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_RepairAuthorityGrade;
            }
        }

        public string DealerServiceInfo_DealerName {
            get {
                return Resources.ChannelsUIStrings.DealerServiceInfo_DealerName;
            }
        }

        public string DataManagementView_Title_DealerServiceInfoHistory {
            get {
                return Resources.ChannelsUIStrings.DataManagementView_Title_DealerServiceInfoHistory;
            }
        }

        public string QueryPanel_Title_DealerServiceInfoHistory {
            get {
                return Resources.ChannelsUIStrings.QueryPanel_Title_DealerServiceInfoHistory;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfoHistory_Area {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_Area;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfoHistory_BusinessDivision {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_BusinessDivision;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfoHistory_ChannelCapabilityId {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_ChannelCapabilityId;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfoHistory_Fax {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_Fax;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfoHistory_Fix {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_Fix;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfoHistory_HotLine {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_HotLine;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfoHistory_InvoiceTypeInvoiceRatio {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_InvoiceTypeInvoiceRatio;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfoHistory_IsOnDuty {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_IsOnDuty;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfoHistory_LaborCostCostInvoiceType {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_LaborCostCostInvoiceType;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfoHistory_LaborCostInvoiceRatio {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_LaborCostInvoiceRatio;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfoHistory_MaterialCostInvoiceType {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_MaterialCostInvoiceType;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfoHistory_OutServiceradii {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_OutServiceradii;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfoHistory_RegionType {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_RegionType;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfoHistory_ServicePermission {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_ServicePermission;
            }
        }

        public string DataEditView_Validation_PartsSalesOrder_PartsSalesCategoryIdIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_PartsSalesOrder_PartsSalesCategoryIdIsNull;
            }
        }

        public string QueryPanel_QueryItem_Title_DealerBusinessPermit_CreatorName {
            get {
                return Resources.ChannelsUIStrings.QueryPanel_QueryItem_Title_DealerBusinessPermit_CreatorName;
            }
        }

        public string DataManagementView_Title_SubdealerForBranch {
            get {
                return Resources.ChannelsUIStrings.DataManagementView_Title_SubdealerForBranch;
            }
        }

        public string DataEditView_GroupTitle_EnterpriseInvoicingInfo {
            get {
                return Resources.ChannelsUIStrings.DataEditView_GroupTitle_EnterpriseInvoicingInfo;
            }
        }

        public string DataEditView_Text_Dealer_Code {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Text_Dealer_Code;
            }
        }

        public string DataEditView_Text_Dealer_Name {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Text_Dealer_Name;
            }
        }

        public string DataEditView_Validation_DealerServiceInfo_BusinessCode {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_DealerServiceInfo_BusinessCode;
            }
        }

        public string DataEditView_Validation_DealerServiceInfo_BusinessName {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_DealerServiceInfo_BusinessName;
            }
        }

        public string DataEditView_Validation_Dealer_CodeIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_Dealer_CodeIsNull;
            }
        }

        public string DataEditView_Validation_Dealer_NameIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_Dealer_NameIsNull;
            }
        }

        public string DataEditView_GroupTitle_MobileNumberList {
            get {
                return Resources.ChannelsUIStrings.DataEditView_GroupTitle_MobileNumberList;
            }
        }

        public string DataEditView_TabItem_Title_MobileNumberList {
            get {
                return Resources.ChannelsUIStrings.DataEditView_TabItem_Title_MobileNumberList;
            }
        }

        public string DataEditView_Title_BatchBindingTel {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Title_BatchBindingTel;
            }
        }

        public string DataEditView_Validation_AuthenticationType_AuthenticationTypeNameIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_AuthenticationType_AuthenticationTypeNameIsNull;
            }
        }

        public string DataEditView_Validation_TrainingType_TrainingTypeNameIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_TrainingType_TrainingTypeNameIsNull;
            }
        }

        public string DataEditView_Validation_DealerPerTrainAut_DealerIdIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_DealerPerTrainAut_DealerIdIsNull;
            }
        }

        public string DataEditView_Validation_DealerPerTrainAut_MarketingDepartmentIdIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_DealerPerTrainAut_MarketingDepartmentIdIsNull;
            }
        }

        public string DataEditView_Validation_DealerPerTrainAut_NameIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_DealerPerTrainAut_NameIsNull;
            }
        }

        public string DataEditView_Validation_DealerPerTrainAut_PositionIdIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_DealerPerTrainAut_PositionIdIsNull;
            }
        }

        public string DataEditView_Text_DealerPerTrainAut_PositionId {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Text_DealerPerTrainAut_PositionId;
            }
        }

        public string DataEditView_GroupTitle_Grade {
            get {
                return Resources.ChannelsUIStrings.DataEditView_GroupTitle_Grade;
            }
        }

        public string DataEditView_Group_Grade {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Group_Grade;
            }
        }

        public string DataEditView_Validation_BrandGradeMessage_DetailNotNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_BrandGradeMessage_DetailNotNull;
            }
        }

        public string DataEditView_Validation_DealerGradeInfo_DetailNotNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_DealerGradeInfo_DetailNotNull;
            }
        }

        public string DataEditView_Validation_GradeCoefficient_BrandNotNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_GradeCoefficient_BrandNotNull;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfoHistory_BrandGradeMessageName {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_BrandGradeMessageName;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfo_BrandGradeMessageName {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_BrandGradeMessageName;
            }
        }

        public string DataEditView_Validation_TrainingType_TrainingCodeIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_TrainingType_TrainingCodeIsNull;
            }
        }

        public string DataEditView_Validation_TrainingType_TrainingNameIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_TrainingType_TrainingNameIsNull;
            }
        }

        public string DataEditView_Validation_TrainingType_TrainingTypeCodeIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_TrainingType_TrainingTypeCodeIsNull;
            }
        }

        public string DataEditView_Validation_AuthenticationType_AuthenticationTypeCodeIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_AuthenticationType_AuthenticationTypeCodeIsNull;
            }
        }

        public string DataEditView_Validation_AuthenticationType_ValDateIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_AuthenticationType_ValDateIsNull;
            }
        }

        public string Action_Title_Abandon {
            get {
                return Resources.ChannelsUIStrings.Action_Title_Abandon;
            }
        }

        public string Action_Title_Add {
            get {
                return Resources.ChannelsUIStrings.Action_Title_Add;
            }
        }

        public string Action_Title_BatchImport {
            get {
                return Resources.ChannelsUIStrings.Action_Title_BatchImport;
            }
        }

        public string Action_Title_Edit {
            get {
                return Resources.ChannelsUIStrings.Action_Title_Edit;
            }
        }

        public string Action_Title_Export {
            get {
                return Resources.ChannelsUIStrings.Action_Title_Export;
            }
        }

        public string Action_Title_ImportEdit {
            get {
                return Resources.ChannelsUIStrings.Action_Title_ImportEdit;
            }
        }

        public string Action_Title_ImportForEdito {
            get {
                return Resources.ChannelsUIStrings.Action_Title_ImportForEdito;
            }
        }

        public string Action_Title_ImportOverlapInfo {
            get {
                return Resources.ChannelsUIStrings.Action_Title_ImportOverlapInfo;
            }
        }

        public string Action_Title_KeyPositionRoot {
            get {
                return Resources.ChannelsUIStrings.Action_Title_KeyPositionRoot;
            }
        }

        public string Action_Title_UpdateDistance {
            get {
                return Resources.ChannelsUIStrings.Action_Title_UpdateDistance;
            }
        }

        public string DataEditPanel_Text_DealerServiceExt_EmployeeNumber {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_DealerServiceExt_EmployeeNumber;
            }
        }

        public string DataEditPanel_Text_DealerServiceInfo_DefaultWarehouse {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_DealerServiceInfo_DefaultWarehouse;
            }
        }

        public string DataEditPanel_Text_DealerServiceInfo_GradeCoefficient {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_DealerServiceInfo_GradeCoefficient;
            }
        }

        public string DataEditPanel_Text_DealerServiceInfo_NetMode {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_DealerServiceInfo_NetMode;
            }
        }

        public string DataEditPanel_Text_Dealer_Code {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_Dealer_Code;
            }
        }

        public string DataEditPanel_Text_Dealer_Manager {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_Dealer_Manager;
            }
        }

        public string DataEditPanel_Text_Dealer_Name {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_Dealer_Name;
            }
        }

        public string DataEditPanel_Text_Dealer_RefreshName {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_Dealer_RefreshName;
            }
        }

        public string DataEditPanel_Text_Dealer_ShortName {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_Dealer_ShortName;
            }
        }

        public string DataEditPanel_Text_NotRequired {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_NotRequired;
            }
        }

        public string DataEditPanel_Text_Required {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_Required;
            }
        }

        public string DetailPanel_Title_AgencyDealerRelationHistory {
            get {
                return Resources.ChannelsUIStrings.DetailPanel_Title_AgencyDealerRelationHistory;
            }
        }

        public string Action_Title_DealerServiceInfoForBranch {
            get {
                return Resources.ChannelsUIStrings.Action_Title_DealerServiceInfoForBranch;
            }
        }

        public string DataEditView_Common_ExportTemplate {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Common_ExportTemplate;
            }
        }

        public string DataEditView_Common_Import {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Common_Import;
            }
        }

        public string DataEditView_Common_ImportFinishAndSomeFailed {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Common_ImportFinishAndSomeFailed;
            }
        }

        public string DataEditView_Common_ImportSuccess {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Common_ImportSuccess;
            }
        }

        public string DataEditView_Title_AgencyDealerRelation {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Title_AgencyDealerRelation;
            }
        }

        public string DataEditView_Title_AgencyDealerRelation_Import {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Title_AgencyDealerRelation_Import;
            }
        }

        public string DataEditView_Title_AgencyDealerRelation_ImportDetails {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Title_AgencyDealerRelation_ImportDetails;
            }
        }

        public string DataEditView_Title_BranchName {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Title_BranchName;
            }
        }

        public string DataEditView_Title_DealerExtendInformation {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Title_DealerExtendInformation;
            }
        }

        public string DataEditView_Title_DealerGradeName {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Title_DealerGradeName;
            }
        }

        public string DataEditView_Title_DealerServiceInfo_CenterStack {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Title_DealerServiceInfo_CenterStack;
            }
        }

        public string DataEditView_Title_DealerServiceInfo_DealerCode {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Title_DealerServiceInfo_DealerCode;
            }
        }

        public string DataEditView_Title_DealerServiceInfo_DealerName {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Title_DealerServiceInfo_DealerName;
            }
        }

        public string DataEditView_Title_DealerServiceInfo_Distance {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Title_DealerServiceInfo_Distance;
            }
        }

        public string DataEditView_Title_DealerServiceInfo_ExternalState {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Title_DealerServiceInfo_ExternalState;
            }
        }

        public string DataEditView_Title_DealerServiceInfo_HourPhone24 {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Title_DealerServiceInfo_HourPhone24;
            }
        }

        public string DataEditView_Title_DealerServiceInfo_MarketingDepartment {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Title_DealerServiceInfo_MarketingDepartment;
            }
        }

        public string DataEditView_Title_DealerServiceInfo_MinServiceCode {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Title_DealerServiceInfo_MinServiceCode;
            }
        }

        public string DataEditView_Title_DealerServiceInfo_MinServiceName {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Title_DealerServiceInfo_MinServiceName;
            }
        }

        public string DataEditView_Title_DealerServiceInfo_SubDealerType {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Title_DealerServiceInfo_SubDealerType;
            }
        }

        public string DataEditView_Title_DealerServiceInfo_TrunkNetworkType {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Title_DealerServiceInfo_TrunkNetworkType;
            }
        }

        public string DataEditView_Title_DealerType {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Title_DealerType;
            }
        }

        public string DataEditView_Validation_Company_CityNameHasChanged {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_Company_CityNameHasChanged;
            }
        }

        public string DataEditView_Validation_Company_EffectivePhone {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_Company_EffectivePhone;
            }
        }

        public string DataEditView_Validation_Company_MaxInputSixPoint {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_Company_MaxInputSixPoint;
            }
        }

        public string DataEditView_Validation_DealerServiceInfo_Validation1 {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_DealerServiceInfo_Validation1;
            }
        }

        public string DataEditView_Validation_DealerServiceInfo_Validation2 {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_DealerServiceInfo_Validation2;
            }
        }

        public string DataEditView_Validation_DealerServiceInfo_Validation3 {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_DealerServiceInfo_Validation3;
            }
        }

        public string DataEditView_Validation_DealerServiceInfo_Validation4 {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_DealerServiceInfo_Validation4;
            }
        }

        public string DataEditView_Validation_Dealer_BaseInformation {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_Dealer_BaseInformation;
            }
        }

        public string DataEditView_Validation_Dealer_BusinessAddressIsNotNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_Dealer_BusinessAddressIsNotNull;
            }
        }

        public string DataEditView_Validation_Dealer_CodeIsNotNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_Dealer_CodeIsNotNull;
            }
        }

        public string DataEditView_Validation_Dealer_CodeLengthMoreThanEleven {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_Dealer_CodeLengthMoreThanEleven;
            }
        }

        public string DataEditView_Validation_Dealer_EffiectiveContactPostCode {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_Dealer_EffiectiveContactPostCode;
            }
        }

        public string DataEditView_Validation_Dealer_LegalRepresentativeLessThanTwo {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_Dealer_LegalRepresentativeLessThanTwo;
            }
        }

        public string DataEditView_Validation_Dealer_ManagerLessThanTwo {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_Dealer_ManagerLessThanTwo;
            }
        }

        public string DataEditView_Validation_Dealer_NameIsNotNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_Dealer_NameIsNotNull;
            }
        }

        public string DataEditView_Validation_Dealer_PhoneIsNotNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_Dealer_PhoneIsNotNull;
            }
        }

        public string DataEditView_Validation_Dealer_ShortNameIsNotNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_Dealer_ShortNameIsNotNull;
            }
        }

        public string DataEditView_Validation_EducationIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_EducationIsNull;
            }
        }

        public string DataEditView_Validation_EntryTimeIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_EntryTimeIsNull;
            }
        }

        public string DataEditView_Validation_IdCardNumberIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_IdCardNumberIsNull;
            }
        }

        public string DataEditView_Validation_IsOnJobIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_IsOnJobIsNull;
            }
        }

        public string DataEditView_Validation_LeaveTimeIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_LeaveTimeIsNull;
            }
        }

        public string DataEditView_Validation_LinkmanLessThanTwo {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_LinkmanLessThanTwo;
            }
        }

        public string DataEditView_Validation_MobileNumberIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_MobileNumberIsNull;
            }
        }

        public string DataEditView_Validation_ProfessionalRankIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_ProfessionalRankIsNull;
            }
        }

        public string DataEditView_Validation_SexIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_SexIsNull;
            }
        }

        public string DataEditView_Validation_SkillLevelIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_SkillLevelIsNull;
            }
        }

        public string DataEditView_Validation_WorkTypeIsNull {
            get {
                return Resources.ChannelsUIStrings.DataEditView_Validation_WorkTypeIsNull;
            }
        }

        public string DataGridView_ColumnItem_Title_Agency_Code {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_Agency_Code;
            }
        }

        public string DataGridView_ColumnItem_Title_Agency_MarketingDepartment {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_Agency_MarketingDepartment;
            }
        }

        public string DataGridView_ColumnItem_Title_Agency_Name {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_Agency_Name;
            }
        }

        public string DataGridView_ColumnItem_Title_BranchCode {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_BranchCode;
            }
        }

        public string DataGridView_ColumnItem_Title_BranchName {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_BranchName;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerBusinessPermit_ProductLineType {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerBusinessPermit_ProductLineType;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerGradeInfo_GradeName {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerGradeInfo_GradeName;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerKeyEmployee_MobileNumber {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerKeyEmployee_MobileNumber;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfo_A24HourPhone {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_A24HourPhone;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfo_ACenterStack {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_ACenterStack;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfo_ATrunkNetworkType {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_ATrunkNetworkType;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfo_CenterStack {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_CenterStack;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfo_DefaultWarehouse {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_DefaultWarehouse;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfo_ExternalState {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_ExternalState;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfo_Grade {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_Grade;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfo_GradeCoefficientGrade {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_GradeCoefficientGrade;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfo_HotLine {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_HotLine;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfo_HourPhone24 {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_HourPhone24;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfo_InvoiceTypeInvoiceRatio {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_InvoiceTypeInvoiceRatio;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfo_LaborCostCostInvoiceType {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_LaborCostCostInvoiceType;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfo_LaborCostInvoiceRatio {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_LaborCostInvoiceRatio;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfo_MaterialCostInvoiceType {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_MaterialCostInvoiceType;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfo_OutServiceradii {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_OutServiceradii;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfo_PartReserveAmount {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_PartReserveAmount;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerServiceInfo_TrunkNetworkType {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_TrunkNetworkType;
            }
        }

        public string DataGridView_ColumnItem_Title_Dealer_BusinessAddressLatitude {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_BusinessAddressLatitude;
            }
        }

        public string DataGridView_ColumnItem_Title_Dealer_BusinessAddressLongitude {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_BusinessAddressLongitude;
            }
        }

        public string DataGridView_ColumnItem_Title_Dealer_BusinessContactMethod {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_BusinessContactMethod;
            }
        }

        public string DataGridView_ColumnItem_Title_Dealer_BusinessLinkName {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_BusinessLinkName;
            }
        }

        public string DataGridView_ColumnItem_Title_Dealer_Code {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_Code;
            }
        }

        public string DataGridView_ColumnItem_Title_Dealer_CompDocumentType {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_CompDocumentType;
            }
        }

        public string DataGridView_ColumnItem_Title_Dealer_ContactMobile {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_ContactMobile;
            }
        }

        public string DataGridView_ColumnItem_Title_Dealer_Distance {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_Distance;
            }
        }

        public string DataGridView_ColumnItem_Title_Dealer_EmployeeNumber {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_EmployeeNumber;
            }
        }

        public string DataGridView_ColumnItem_Title_Dealer_FixedAsset {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_FixedAsset;
            }
        }

        public string DataGridView_ColumnItem_Title_Dealer_LegalRepresentative {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_LegalRepresentative;
            }
        }

        public string DataGridView_ColumnItem_Title_Dealer_MinServiceCode {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_MinServiceCode;
            }
        }

        public string DataGridView_ColumnItem_Title_Dealer_MinServiceName {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_MinServiceName;
            }
        }

        public string DataGridView_ColumnItem_Title_Dealer_Name {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_Name;
            }
        }

        public string DataGridView_ColumnItem_Title_Dealer_RegisteredAddress {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_RegisteredAddress;
            }
        }

        public string DataGridView_ColumnItem_Title_Dealer_VehicleDockingStation {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_VehicleDockingStation;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsSalesCategoryName {
            get {
                return Resources.ChannelsUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategoryName;
            }
        }

        public string DataManagementView_Notification_DealerServiceInfo_NoCompanyInfo {
            get {
                return Resources.ChannelsUIStrings.DataManagementView_Notification_DealerServiceInfo_NoCompanyInfo;
            }
        }

        public string DataManagementView_Notification_DealerServiceInfo_NoUsableCompanyInfo {
            get {
                return Resources.ChannelsUIStrings.DataManagementView_Notification_DealerServiceInfo_NoUsableCompanyInfo;
            }
        }

        public string DataManagementView_Notification_DealerServiceInfo_PleaseStopThisCategory {
            get {
                return Resources.ChannelsUIStrings.DataManagementView_Notification_DealerServiceInfo_PleaseStopThisCategory;
            }
        }

        public string DataManagementView_Notification_Dealer_CategoryCantAbandon {
            get {
                return Resources.ChannelsUIStrings.DataManagementView_Notification_Dealer_CategoryCantAbandon;
            }
        }

        public string DataManagementView_Notification_Dealer_CategoryCantStopUse {
            get {
                return Resources.ChannelsUIStrings.DataManagementView_Notification_Dealer_CategoryCantStopUse;
            }
        }

        public string DataManagementView_Notification_SaveSuccess {
            get {
                return Resources.ChannelsUIStrings.DataManagementView_Notification_SaveSuccess;
            }
        }

        public string DataManagementView_Title_DealerAgency {
            get {
                return Resources.ChannelsUIStrings.DataManagementView_Title_DealerAgency;
            }
        }

        public string QueryPanel_QueryItem_Title_Agency_Area {
            get {
                return Resources.ChannelsUIStrings.QueryPanel_QueryItem_Title_Agency_Area;
            }
        }

        public string QueryPanel_QueryItem_Title_Agency_Code {
            get {
                return Resources.ChannelsUIStrings.QueryPanel_QueryItem_Title_Agency_Code;
            }
        }

        public string QueryPanel_QueryItem_Title_Agency_MarketingDepartment {
            get {
                return Resources.ChannelsUIStrings.QueryPanel_QueryItem_Title_Agency_MarketingDepartment;
            }
        }

        public string QueryPanel_QueryItem_Title_Agency_Name {
            get {
                return Resources.ChannelsUIStrings.QueryPanel_QueryItem_Title_Agency_Name;
            }
        }

        public string QueryPanel_QueryItem_Title_CreateTime {
            get {
                return Resources.ChannelsUIStrings.QueryPanel_QueryItem_Title_CreateTime;
            }
        }

        public string QueryPanel_QueryItem_Title_DealerGradeInfo_GradeName {
            get {
                return Resources.ChannelsUIStrings.QueryPanel_QueryItem_Title_DealerGradeInfo_GradeName;
            }
        }

        public string QueryPanel_QueryItem_Title_DealerInfo_Code {
            get {
                return Resources.ChannelsUIStrings.QueryPanel_QueryItem_Title_DealerInfo_Code;
            }
        }

        public string QueryPanel_QueryItem_Title_DealerInfo_Name {
            get {
                return Resources.ChannelsUIStrings.QueryPanel_QueryItem_Title_DealerInfo_Name;
            }
        }

        public string QueryPanel_QueryItem_Title_DealerKeyEmployee_Dealer {
            get {
                return Resources.ChannelsUIStrings.QueryPanel_QueryItem_Title_DealerKeyEmployee_Dealer;
            }
        }

        public string QueryPanel_QueryItem_Title_DealerKeyEmployee_Name {
            get {
                return Resources.ChannelsUIStrings.QueryPanel_QueryItem_Title_DealerKeyEmployee_Name;
            }
        }

        public string QueryPanel_QueryItem_Title_No {
            get {
                return Resources.ChannelsUIStrings.QueryPanel_QueryItem_Title_No;
            }
        }

        public string QueryPanel_QueryItem_Title_Yes {
            get {
                return Resources.ChannelsUIStrings.QueryPanel_QueryItem_Title_Yes;
            }
        }

        public string QueryPanel_Title_DealerInfo {
            get {
                return Resources.ChannelsUIStrings.QueryPanel_Title_DealerInfo;
            }
        }

        public string QueryPanel_Title_Dealer_Information {
            get {
                return Resources.ChannelsUIStrings.QueryPanel_Title_Dealer_Information;
            }
        }

        public string DataEditView_ImportTemplateColumn_Remark {
            get {
                return Resources.ChannelsUIStrings.DataEditView_ImportTemplateColumn_Remark;
            }
        }

        public string DataGridView_Title_AgencyDealerRelation_FileName {
            get {
                return Resources.ChannelsUIStrings.DataGridView_Title_AgencyDealerRelation_FileName;
            }
        }

        public string DataGridView_Title_DealerServiceInfo_BatchImport {
            get {
                return Resources.ChannelsUIStrings.DataGridView_Title_DealerServiceInfo_BatchImport;
            }
        }

        public string DataGridView_Title_DealerServiceInfo_BatchImportDetails {
            get {
                return Resources.ChannelsUIStrings.DataGridView_Title_DealerServiceInfo_BatchImportDetails;
            }
        }

        public string DataGridView_Title_DealerServiceInfo_FileName {
            get {
                return Resources.ChannelsUIStrings.DataGridView_Title_DealerServiceInfo_FileName;
            }
        }

        public string DataEditPanel_Text_SelectCompany {
            get {
                return Resources.ChannelsUIStrings.DataEditPanel_Text_SelectCompany;
            }
        }

        public string DealerServerInfo_Validate1 {
            get {
                return Resources.ChannelsUIStrings.DealerServerInfo_Validate1;
            }
        }

        public string DealerServerInfo_Validate2 {
            get {
                return Resources.ChannelsUIStrings.DealerServerInfo_Validate2;
            }
        }

        public string DealerServerInfo_Validate3 {
            get {
                return Resources.ChannelsUIStrings.DealerServerInfo_Validate3;
            }
        }

        public string DealerServerInfo_Validate4 {
            get {
                return Resources.ChannelsUIStrings.DealerServerInfo_Validate4;
            }
        }

        public string DealerServerInfo_Validate5 {
            get {
                return Resources.ChannelsUIStrings.DealerServerInfo_Validate5;
            }
        }

        public string DealerServerInfo_Validate6 {
            get {
                return Resources.ChannelsUIStrings.DealerServerInfo_Validate6;
            }
        }

        public string DealerServerInfo_Validate7 {
            get {
                return Resources.ChannelsUIStrings.DealerServerInfo_Validate7;
            }
        }

        public string DealerServerInfo_Validate8 {
            get {
                return Resources.ChannelsUIStrings.DealerServerInfo_Validate8;
            }
        }

        public string DealerServerInfo_Validate9 {
            get {
                return Resources.ChannelsUIStrings.DealerServerInfo_Validate9;
            }
        }

        public string DetailPanel_Title_DealerKeyEmploye {
            get {
                return Resources.ChannelsUIStrings.DetailPanel_Title_DealerKeyEmploye;
            }
        }

        public string DetailPanel_Title_DealerKeyService {
            get {
                return Resources.ChannelsUIStrings.DetailPanel_Title_DealerKeyService;
            }
        }

        public string DetailPanel_Title_No {
            get {
                return Resources.ChannelsUIStrings.DetailPanel_Title_No;
            }
        }

    }
}
