﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Channels.Views
{
    [PageMeta("Common", "Dealer", "DealerAgency", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_EXPORT_IMPORT
    })]
    public class AgencyDealerRelationManagement : DcsDataManagementViewBase
    {

        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditViewImport;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private const string DATA_EDIT_VIEW_IMPORT = "_DataEditViewImport_";

        public AgencyDealerRelationManagement()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = ChannelsUIStrings.DataManagementView_Title_DealerAgency;
        }

        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("AgencyDealerRelation"));
            }
        }

        private DataEditViewBase DataEditView
        {
            get
            {
                if (this.dataEditView == null)
                {
                    this.dataEditView = DI.GetDataEditView("AgencyDealerRelation");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataEditViewImport
        {
            get
            {
                if (this.dataEditViewImport == null)
                {
                    this.dataEditViewImport = DI.GetDataEditView("AgencyDealerRelationForImport");
                    this.dataEditViewImport.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewImport;
            }
        }
        private void ResetEditView()
        {
            this.dataEditView = null;
            this.dataEditViewImport = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e)
        {
            this.ResetEditView();
            if (this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();

            UIHelper.ShowNotification(ChannelsUIStrings.DataManagementView_Notification_SaveSuccess);
            System.Threading.Thread.Sleep(500);
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e)
        {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_IMPORT, () => this.DataEditViewImport);
        }

        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "AgencyDealerRelation"
                };
            }
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId)
            {
                case CommonActionKeys.ADD:
                    var agencyDealerRelation = this.DataEditView.CreateObjectToEdit<AgencyDealerRelation>();
                    agencyDealerRelation.Status = (int)DcsBaseDataStatus.有效;
                    agencyDealerRelation.BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(ChannelsUIStrings.DataManagementView_Confirm_Abandon, () =>
                    {
                        var entity = this.DataGridView.SelectedEntities.Cast<AgencyDealerRelation>().SingleOrDefault();
                        if (entity == null)
                            return;
                        try
                        {
                            if (entity.Can作废代理库与经销商隶属关系)
                                entity.作废代理库与经销商隶属关系();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if (domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp =>
                            {
                                if (submitOp.HasError)
                                {
                                    if (!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(ChannelsUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        }
                        catch (Exception ex)
                        {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORT:
                    ShellViewModel.Current.IsBusy = true;
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if (filterItem == null)
                        return;
                    var agencyCode = filterItem.Filters.Single(e => e.MemberName == "Company.Code").Value as string;
                    var agencyName = filterItem.Filters.Single(e => e.MemberName == "Company.Name").Value as string;
                    var dealerCode = filterItem.Filters.Single(e => e.MemberName == "Company1.Code").Value as string;
                    var dealerName = filterItem.Filters.Single(e => e.MemberName == "Company1.Name").Value as string;
                    var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                    var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    if (createTime != null)
                    {
                        createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                        createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    }
                    var salesRegionName = filterItem.Filters.Single(e => e.MemberName == "SalesRegionName").Value as string;
                    var marketingDepartmentName = filterItem.Filters.Single(e => e.MemberName == "MarketingDepartmentName").Value as string;
                    //string salesRegionName, string marketingDepartmentName
                    this.dcsDomainContext.ExportAgencyDealerRelationQuery(agencyCode, agencyName, dealerCode, dealerName, status, createTimeBegin, createTimeEnd, salesRegionName, marketingDepartmentName, loadOp =>
                    {
                        ShellViewModel.Current.IsBusy = false;
                        if (loadOp.HasError)
                            return;
                        if (loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value))
                        {
                            UIHelper.ShowNotification(loadOp.Value);
                        }
                        if (loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                    }, null);

                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORT);
                    break;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem)
        {
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem
            {
                MemberName = "BranchId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            if (filterItem != null)
            {
                var originalFilterItem = filterItem as CompositeFilterItem;
                if (originalFilterItem != null)
                    foreach (var filter in originalFilterItem.Filters)
                        compositeFilterItem.Filters.Add(filter);
                else
                    compositeFilterItem.Filters.Add(filterItem);
            }
            ClientVar.ConvertTime(compositeFilterItem);
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch (uniqueId)
            {
                case CommonActionKeys.ADD:
                case CommonActionKeys.IMPORT:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if (this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<AgencyDealerRelation>().ToArray();
                    if (entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsBaseDataStatus.有效;
                default:
                    return false;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
            }
        }

        private void ExportAgencyDealerRelation(string agencyCode, string agencyName, string dealerCode, string dealerName, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd)
        {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportAgencyDealerRelationAsync(agencyCode, agencyName, dealerCode, dealerName, status, createTimeBegin, createTimeEnd);
            this.excelServiceClient.ExportAgencyDealerRelationCompleted -= excelServiceClient_ExportAgencyDealerRelationCompleted;
            this.excelServiceClient.ExportAgencyDealerRelationCompleted += excelServiceClient_ExportAgencyDealerRelationCompleted;
        }

        private void excelServiceClient_ExportAgencyDealerRelationCompleted(object sender, ExportAgencyDealerRelationCompletedEventArgs e)
        {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
    }
}