﻿
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataEdit {
    public partial class AgencyDealerRelationDataEditView {
        public AgencyDealerRelationDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            this.LayoutRoot.Children.Add(DI.GetDataEditPanel("AgencyDealerRelation"));
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void OnEditSubmitting() {
            var agencyDealerRelation = this.DataContext as AgencyDealerRelation;
            if(agencyDealerRelation == null)
                return;
            if(agencyDealerRelation.PartsSalesOrderTypeId <= 0) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_PartsSalesOrder_PartsSalesCategoryIdIsNull);
                return;
            }
            ((IEditableObject)agencyDealerRelation).EndEdit();
            base.OnEditSubmitting();
        }

        protected override string BusinessName {
            get {
                return ChannelsUIStrings.DataEditView_Title_AgencyDealerRelation;
            }
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetAgencyDealerRelationsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);

            }, null);
        }
    }
}
