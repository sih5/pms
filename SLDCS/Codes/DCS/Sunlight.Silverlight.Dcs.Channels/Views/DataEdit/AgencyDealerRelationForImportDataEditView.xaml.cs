﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataEdit {
    public partial class AgencyDealerRelationForImportDataEditView {
        private DataGridViewBase agencyDealerRelationForImportDataGridView;
        private const string EXPORT_DATA_FILE_NAME = "导出中心库与经销商隶属关系模板.xlsx";
        private ICommand exportFileCommand;

        public AgencyDealerRelationForImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);

        }

        protected override string Title {
            get {
                return ChannelsUIStrings.DataEditView_Title_AgencyDealerRelation_Import;
            }
        }
        protected DataGridViewBase ImportDataGridView {
            get {
                return this.agencyDealerRelationForImportDataGridView ?? (this.agencyDealerRelationForImportDataGridView = DI.GetDataGridView("AgencyDealerRelationForImport"));
            }
        }
        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = ChannelsUIStrings.DataEditView_Title_AgencyDealerRelation_ImportDetails,
                Content = this.ImportDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }
        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }
        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.ImportAgencyDealerRelationAsync(fileName);
            this.ExcelServiceClient.ImportAgencyDealerRelationCompleted -= ExcelServiceClient_ImportAgencyDealerRelationCompleted;
            this.ExcelServiceClient.ImportAgencyDealerRelationCompleted += ExcelServiceClient_ImportAgencyDealerRelationCompleted;
        }
        private void ExcelServiceClient_ImportAgencyDealerRelationCompleted(object sender, ImportAgencyDealerRelationCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!this.HasImportingError) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Common_ImportSuccess, 5);
            } else {
                if(!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if(!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Common_ImportFinishAndSomeFailed, 10);
            }
        }
        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }
        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columns = new List<ImportTemplateColumn> {
                                new ImportTemplateColumn {
                                    Name =ChannelsUIStrings.DataGridView_ColumnItem_Title_BranchName,
                                    IsRequired = true,
                                },
                                new ImportTemplateColumn {
                                    Name = ChannelsUIStrings.DataGridView_ColumnItem_Title_Agency_Code,
                                    IsRequired = true,
                                },
                                new ImportTemplateColumn {
                                    Name = ChannelsUIStrings.DataGridView_ColumnItem_Title_Agency_Name,
                                    IsRequired = true,
                                },
                                new ImportTemplateColumn {
                                    Name = ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_PartsSalesCategory_Name,
                                    IsRequired = true,
                                },
                                new ImportTemplateColumn {
                                    Name = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_DealerCode,
                                    IsRequired = true,
                                },
                                new ImportTemplateColumn {
                                    Name = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_DealerName,
                                    IsRequired = true,
                                },
                                new ImportTemplateColumn {
                                    Name = ChannelsUIStrings.DataEditView_ImportTemplateColumn_Remark,
                                }
                            };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columns, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
    }
}