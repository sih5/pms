﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class BatchBindingTelDataEditView {
        private DataGridViewBase batchBindingTelForImportDataGridView;
        private const string EXPORT_DATA_FILE_NAME = "导出视频手机清单模板.xlsx";
        private ICommand exportFileCommand;
        public BatchBindingTelDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return ChannelsUIStrings.DataEditView_Title_BatchBindingTel;
            }
        }

        protected DataGridViewBase ImportDataGridView {
            get {
                if(this.batchBindingTelForImportDataGridView == null) {
                    this.batchBindingTelForImportDataGridView = DI.GetDataGridView("BatchBindingTelForImport");
                    this.batchBindingTelForImportDataGridView.DataContext = this.DataContext;
                }
                return this.batchBindingTelForImportDataGridView;
            }
        }

        private ObservableCollection<DealerMobileNumberList> dealerMobileNumberLists;

        public ObservableCollection<DealerMobileNumberList> DealerMobileNumberLists {
            get {
                if(this.dealerMobileNumberLists == null) {
                    this.dealerMobileNumberLists = new ObservableCollection<DealerMobileNumberList>();
                } return dealerMobileNumberLists;
            }
        }

        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = ChannelsUIStrings.DataEditView_TabItem_Title_MobileNumberList,
                Content = this.ImportDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.root.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.ImportDealerMobileNumberListAsync(fileName);
            this.ExcelServiceClient.ImportDealerMobileNumberListCompleted -= ExcelServiceClientOnImportDealerMobileNumberListCompleted;
            this.ExcelServiceClient.ImportDealerMobileNumberListCompleted += ExcelServiceClientOnImportDealerMobileNumberListCompleted;
        }

        private void ExcelServiceClientOnImportDealerMobileNumberListCompleted(object sender, ImportDealerMobileNumberListCompletedEventArgs e) {
            var dataEditView = this.DataContext as BatchBindingTelDataEditView;
            if(dataEditView == null)
                return;
            if(e.rightData != null) {
                foreach(var data in e.rightData) {
                    dataEditView.DealerMobileNumberLists.Add(new DealerMobileNumberList {
                        DealerCode = data.DealerCode,
                        DealerName = data.DealerName,
                        VideoMobileNumber = data.VideoMobileNumber
                    });
                }
            }
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!this.HasImportingError) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Notification_CompletedImport, 5);
            } else {
                if(!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if(!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                       new ImportTemplateColumn {
                                                Name =ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_DealerCode,
                                                    IsRequired = true
                                            },new ImportTemplateColumn {
                                                Name = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_DealerName,
                                                    IsRequired = true
                                            },new ImportTemplateColumn {
                                                Name = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerMobileNumberList_VideoMobileNumber
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
    }
}
