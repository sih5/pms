﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataEdit {
    public partial class ChannelCapabilityDataEditView {
        public ChannelCapabilityDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("ChannelCapability"));
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_ChannelCapability;
            }
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext==null) {
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            } else {
                this.LoadEntityToEdit((int)id);
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetChannelCapabilitiesQuery().Where(e => e.Id==id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();

                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        protected override void OnEditSubmitting() {
            var channelCapability = this.DataContext as ChannelCapability;
            if(channelCapability == null)
                return;
            channelCapability.ValidationErrors.Clear();
            if(string.IsNullOrWhiteSpace(channelCapability.Name))
                channelCapability.ValidationErrors.Add(new ValidationResult(ChannelsUIStrings.DataEditView_Validation_ChannelCapability_NameIsNull, new[] {
                    "Name"
                }));
            if(channelCapability.ChannelGrade==default(int))
                channelCapability.ValidationErrors.Add(new ValidationResult(ChannelsUIStrings.DataEditView_Validation_ChannelCapability_ChannelGradeIsNull, new[] {
                    "ChannelGrade"
                }));
            if(string.IsNullOrWhiteSpace(channelCapability.Description))
                channelCapability.ValidationErrors.Add(new ValidationResult(ChannelsUIStrings.DataEditView_Validation_ChannelCapability_DescriptionIsNull, new[] {
                    "Description"
                }));
            if(channelCapability.HasValidationErrors)
                return;
            ((IEditableObject)channelCapability).EndEdit();

            try {
                if(this.EditState == DataEditState.Edit)
                    if(channelCapability.Can修改网络业务能力)
                        channelCapability.修改网络业务能力();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

    }
}
