﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Common.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataEdit {
    public partial class DealerDataEditView {
        private ObservableCollection<DealerServiceInfo> dealerServiceInfoes;
        private List<DealerServiceInfo> dealerServiceInfoesOriginal;
        private List<TiledRegion> tiledRegions;
        private bool i = true;

        private List<TiledRegion> TiledRegions {
            get {
                return this.tiledRegions ?? (this.tiledRegions = new List<TiledRegion>());
            }
        }

        public DealerDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += DealerDataEditView_DataContextChanged;
        }

        void DealerDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var dealer = this.DataContext as Dealer;
            if(dealer == null)
                return;
            dealer.PropertyChanged -= dealer_PropertyChanged;
            dealer.PropertyChanged += dealer_PropertyChanged;
        }

        void dealer_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var dealer = this.DataContext as Dealer;
            if(dealer == null)
                return;
            switch(e.PropertyName) {
                case "Company":
                    if(dealer.Company == null)
                        return;
                    dealer.Company.PropertyChanged -= Company_PropertyChanged;
                    dealer.Company.PropertyChanged += Company_PropertyChanged;
                    break;
            }
        }


        private void Company_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var dealer = this.DataContext as Dealer;
            if(dealer == null)
                return;
            //var str = (dealer.Company.BusinessAddressLongitude).ToString();
            //int index = str.LastIndexOf(".");
            //string value = str.Substring(index, str.Length);
            switch(e.PropertyName) {
                case "RegisterCapital":
                    if(dealer.Company.RegisterCapital < 0)
                        dealer.Company.RegisterCapital = dealer.Company.RegisterCapital * -1;
                    break;
                case "BusinessAddressLongitude":
                    string str = (dealer.Company.BusinessAddressLongitude).ToString();
                    string[] sArray = str.Split('.');
                    if(sArray.Length > 1) {
                        if(sArray[1] != null && sArray[1].Length > 6) {
                            dealer.Company.ValidationErrors.Add(new ValidationResult(ChannelsUIStrings.DataEditView_Validation_Company_MaxInputSixPoint, new[] {
                    "BusinessAddressLongitude"
                                   
                }));
                        }
                    }
                    break;
                case "BusinessAddressLatitude":
                    string strs = (dealer.Company.BusinessAddressLatitude).ToString();
                    string[] sArrays = strs.Split('.');
                    if(sArrays.Length > 1) {
                        if(sArrays[1] != null && sArrays[1].Length > 6) {
                            dealer.Company.ValidationErrors.Add(new ValidationResult(ChannelsUIStrings.DataEditView_Validation_Company_MaxInputSixPoint, new[] {
                    "BusinessAddressLatitude"
                                   
                }));
                        }
                    }
                    break;
                case "ContactMobile":
                    if(dealer.Company.ContactMobile != null && dealer.Company.ContactMobile.Length != 11) {
                        dealer.Company.ValidationErrors.Add(new ValidationResult(ChannelsUIStrings.DataEditView_Validation_Company_EffectivePhone, new[] {
                    "ContactMobile"
                   
                    }));
                    }
                    break;
            }
        }
        public ObservableCollection<DealerServiceInfo> DealerServiceInfoes {
            get {
                return this.dealerServiceInfoes ?? (this.dealerServiceInfoes = new ObservableCollection<DealerServiceInfo>());
            }
        }

        public List<DealerServiceInfo> DealerServiceInfoesOriginal {
            get {
                return this.dealerServiceInfoesOriginal ?? (this.dealerServiceInfoesOriginal = new List<DealerServiceInfo>());
            }
        }

        private EnterpriseInformationForDealerDataEditPanel dataEditPanel;
        private EnterpriseInformationForDealerDataEditPanel DataEditPanel {
            get {
                return this.dataEditPanel ?? (this.dataEditPanel = (EnterpriseInformationForDealerDataEditPanel)DI.GetDataEditPanel("EnterpriseInformationForDealer"));
            }
        }

        protected virtual void CreateUI() {
            var dealerPanel = DI.GetDataEditPanel("Dealer");
            this.Root.Children.Add(dealerPanel);
            this.DataEditPanel.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(this.DataEditPanel);
            this.Loaded += DealerDataEditView_Loaded;
        }

        private void DealerDataEditView_Loaded(object sender, RoutedEventArgs e) {
            if(this.EditState == DataEditState.New) {
                this.DealerServiceInfoes.Clear();
                this.DealerServiceInfoesOriginal.Clear();
            }
        }

        private void SetTiledRegionId() {
            var dealer = this.DataContext as Dealer;
            if(dealer == null)
                return;
            var company = dealer.Company;
            if(company == null)
                return;
            if(company.ProvinceName == null)
                return;
            if(this.DataEditPanel.KvProvinceNames.Any()) {
                company.ProvinceId = this.DataEditPanel.KvProvinceNames.First(e => e.Value == company.ProvinceName).Key;
                if(company.CityName == null)
                    return;
                company.CityId = this.DataEditPanel.KvCityNames.First(e => e.Value == company.CityName).Key;
                if(company.CountyName == null)
                    return;
                company.CountyId = this.DataEditPanel.KvCountyNames.First(e => e.Value == company.CountyName).Key;
            } else {
                this.DomainContext.Load(DomainContext.GetTiledRegionsQuery().Where(ex => ex.CountyName != null).OrderBy(ex => ex.ProvinceName), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError)
                        return;
                    foreach(var repairObject in loadOp.Entities)
                        this.TiledRegions.Add(new TiledRegion {
                            Id = repairObject.Id,
                            ProvinceName = repairObject.ProvinceName,
                            CityName = repairObject.CityName,
                            CountyName = repairObject.CountyName,
                        });
                    this.DataEditPanel.KvProvinceNames.Clear();
                    foreach(var item in TiledRegions) {
                        var values = this.DataEditPanel.KvProvinceNames.Select(e => e.Value);
                        if(values.Contains(item.ProvinceName))
                            continue;
                        this.DataEditPanel.KvProvinceNames.Add(new KeyValuePair {
                            Key = item.Id,
                            Value = item.ProvinceName
                        });
                    }
                    company.ProvinceId = this.DataEditPanel.KvProvinceNames.First(e => e.Value == company.ProvinceName).Key;
                    if(company.CityName == null)
                        return;
                    //company.CityId = this.DataEditPanel.KvCityNames.First(e => e.Value == company.CityName).Key;
                    var tempCityId = this.DataEditPanel.KvCityNames.FirstOrDefault(e => e.Value == company.CityName);
                    if(tempCityId == null) {
                        UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_Company_CityNameHasChanged);
                        return;
                    }
                    company.CityId = tempCityId.Key;
                    if(company.CountyName == null)
                        return;
                    company.CountyId = this.DataEditPanel.KvCountyNames.First(e => e.Value == company.CountyName).Key;

                }, null);
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetDealersWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();

                if(entity != null) {
                    this.SetObjectToEdit(entity);
                    entity.Company.PropertyChanged -= Company_PropertyChanged;
                    entity.Company.PropertyChanged += Company_PropertyChanged;
                    //给省 市 县 赋值
                    SetTiledRegionId();
                }
            }, null);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {
            var dealer = this.DataContext as Dealer;
            if(dealer == null)
                return;
            dealer.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(dealer.Code)) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_Dealer_CodeIsNotNull);
                return;
            }
            dealer.Code = dealer.Code.Trim();
            if(dealer.Code.Length > 11) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_Dealer_CodeLengthMoreThanEleven);
                return;
            }
            if(string.IsNullOrEmpty(dealer.Name)) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_Dealer_NameIsNotNull);
                return;
            }
            if(string.IsNullOrEmpty(dealer.ShortName)) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_Dealer_ShortNameIsNotNull);
                return;
            }
            //if(string.IsNullOrEmpty(dealer.Company.CustomerCode)) {
            //    UIHelper.ShowNotification("请填写MDM客户编码");
            //    return;
            //}
            if(string.IsNullOrEmpty(dealer.Company.CityName) || string.IsNullOrEmpty(dealer.Company.ProvinceName) || string.IsNullOrEmpty(dealer.Company.CountyName)) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_Company_CityNameIsNull);
                return;
            }
            if(string.IsNullOrEmpty(dealer.Company.ContactPhone)) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_Dealer_PhoneIsNotNull);
                return;
            }
            //if(string.IsNullOrEmpty(dealer.Company.ContactAddress)) {
            if(string.IsNullOrEmpty(dealer.Company.BusinessAddress)) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_Dealer_BusinessAddressIsNotNull);
                return;
            }
            //if(!(dealer.Company.BusinessAddressLongitude).HasValue) {
            //    UIHelper.ShowNotification("请填写营业地址(经度)");
            //    return;
            //}
            //if(!(dealer.Company.BusinessAddressLatitude).HasValue) {
            //    UIHelper.ShowNotification("请填写营业地址(纬度)");
            //    return;
            //}
            //if(string.IsNullOrEmpty(dealer.Company.LegalRepresentative)) {
            //    UIHelper.ShowNotification("请填写法人代表");
            //    return;
            //}
            //if(string.IsNullOrEmpty(dealer.Company.ContactPerson)) {
            //    UIHelper.ShowNotification("请填写联系人");
            //    return;
            //}
            //if(string.IsNullOrEmpty(dealer.Company.ContactMobile)) {
            //    UIHelper.ShowNotification("请填写联系人手机");
            //    return;
            //}
            if(!string.IsNullOrEmpty(dealer.Manager) && dealer.Manager.Length < 2) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_Dealer_ManagerLessThanTwo);
                return;
            }
            if(!string.IsNullOrEmpty(dealer.Company.LegalRepresentative) && dealer.Company.LegalRepresentative.Length < 2) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_Dealer_LegalRepresentativeLessThanTwo);
                return;
            }

            if(!string.IsNullOrEmpty(dealer.Company.ContactPostCode) && !Regex.IsMatch(dealer.Company.ContactPostCode, @"^\d{6}$", RegexOptions.IgnoreCase)) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_Dealer_EffiectiveContactPostCode);
                return;
            }
            foreach(var entity in dealer.DealerServiceInfoes)
                entity.ValidationErrors.Clear();
            dealer.Company.ValidationErrors.Clear();
            if(this.EditState == DataEditState.New) {
                dealer.Company.Type = (int)DcsCompanyType.服务站;
            }
            dealer.Company.Name = dealer.Name;
            dealer.Company.Code = dealer.Code;
            dealer.Company.RegionId = dealer.Company.CountyId;
            if(!string.IsNullOrEmpty(dealer.Company.CustomerCode)) {
                dealer.Company.CustomerCode = dealer.Company.CustomerCode.Trim();
            }
            if(!string.IsNullOrEmpty(dealer.Company.SupplierCode)) {
                dealer.Company.SupplierCode = dealer.Company.SupplierCode.Trim();
            }

            if(dealer.ValidationErrors.Any() || dealer.DealerServiceInfoes.Any(r => r.HasValidationErrors) || dealer.Company.ValidationErrors.Any())
                return;
            ((IEditableObject)dealer).EndEdit();
            try {
                if(this.EditState == DataEditState.New) {
                    if(i) {
                        i = false;
                        this.DomainContext.生成经销商基本信息集中(dealer.Company, dealer, null, null, null, invokeOp => {
                            if(invokeOp.HasError) {
                                if(!invokeOp.IsErrorHandled)
                                    invokeOp.MarkErrorAsHandled();
                                var error = invokeOp.ValidationErrors.FirstOrDefault();
                                if(error != null) {
                                    UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage, 5);
                                } else {
                                    DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                                }
                                DomainContext.RejectChanges();
                                i = true;
                                return;
                            }
                            this.NotifyEditSubmitted();
                            this.OnCustomEditSubmitted();
                        }, null);
                    }
                } else {
                    this.DomainContext.修改经销商基本信息集中(dealer.Company, dealer, null, null, null, invokeOp => {
                        if(invokeOp.HasError) {
                            if(!invokeOp.IsErrorHandled)
                                invokeOp.MarkErrorAsHandled();
                            var error = invokeOp.ValidationErrors.FirstOrDefault();
                            if(error != null) {
                                UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage, 5);
                            } else {
                                DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                            }
                            DomainContext.RejectChanges();
                            return;
                        }
                        this.NotifyEditSubmitted();
                        this.OnCustomEditSubmitted();
                    }, null);

                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
        }

        public void OnCustomEditSubmitted() {
            i = true;
            this.DealerServiceInfoes.Clear();
            //this.DataEditPanel.KvProvinceNames.Clear();
            this.DealerServiceInfoesOriginal.Clear();
            var dealer = this.DataContext as Dealer;
            if(dealer != null && (dealer.Company != null && this.DomainContext.Companies.Contains(dealer.Company))) {
                this.DomainContext.Companies.Detach(dealer.Company);
            }
            if(this.DomainContext.Dealers.Contains(dealer))
                this.DomainContext.Dealers.Detach(dealer);
        }

        public new event EventHandler EditSubmitted;
        private void NotifyEditSubmitted() {
            var handler = this.EditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        protected override void Reset() {
            i = true;
            this.DealerServiceInfoes.Clear();
            //this.DataEditPanel.KvProvinceNames.Clear();
            this.DealerServiceInfoesOriginal.Clear();
            var dealer = this.DataContext as Dealer;
            if(dealer != null && (dealer.Company != null && this.DomainContext.Companies.Contains(dealer.Company))) {
                this.DomainContext.Companies.Detach(dealer.Company);
            }
            if(this.DomainContext.Dealers.Contains(dealer))
                this.DomainContext.Dealers.Detach(dealer);
        }

        protected override string BusinessName {
            get {
                return ChannelsUIStrings.DataEditView_Validation_Dealer_BaseInformation;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

    }
}
