﻿using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Dcs.Channels.Panels.Detail;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataEdit {
    public partial class DealerDetailDataEditView {
        private KeyValueManager keyValueManager;
        private readonly string[] kvNames = new[] {
            "MasterData_Status"
        };

        public DealerDetailDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvNames);
        }

        protected KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private DealerDetailPanel dealerDetailPanel;

        private DealerDetailPanel DealerDetailPanel {
            get {
                return this.dealerDetailPanel ?? (this.dealerDetailPanel = new DealerDetailPanel());
            }
        }

        protected virtual void CreateUI() {
            this.Root.Children.Add(DealerDetailPanel);
            var enterpriseInformationDataDetailPanel = DI.GetDetailPanel("EnterpriseInformation");
            enterpriseInformationDataDetailPanel.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(enterpriseInformationDataDetailPanel);
            this.KeyValueManager.LoadData();
        }

        protected override string Title {
            get {
                return ChannelsUIStrings.DataEditView_Title_DealerDetail;
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetDealersWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                    //由于提交后再次查看，作废的数据仍在缓存中，此处手动清除
                    foreach(var item in entity.DealerServiceInfoes.Where(e => e.Status == (int)DcsBaseDataStatus.作废)) {
                        this.DomainContext.DealerServiceInfos.Detach(item);
                    }
                }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
