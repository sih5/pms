﻿using System.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataEdit {
    public class DealerDetailForCAMDataEditView : DealerDetailDataEditView {
        protected override void CreateUI() {
            var dealerPanel = DI.GetDetailPanel("Dealer");
            this.Root.Children.Add(dealerPanel);
            var enterpriseInformationDataDetailPanel = DI.GetDetailPanel("EnterpriseInformation");
            enterpriseInformationDataDetailPanel.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(enterpriseInformationDataDetailPanel);
            this.KeyValueManager.LoadData();
        }
    }
}
