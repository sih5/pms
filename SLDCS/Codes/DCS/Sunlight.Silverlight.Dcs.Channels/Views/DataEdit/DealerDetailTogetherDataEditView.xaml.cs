﻿using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Common.Panels.Detail;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using DealerDetailPanel = Sunlight.Silverlight.Dcs.Channels.Panels.Detail.DealerDetailPanel;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataEdit {
    public partial class DealerDetailTogetherDataEditView {
        private readonly string[] kvNames = new[] {
            "MasterData_Status"
        };

        private KeyValueManager keyValueManager;
        public DealerDetailTogetherDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvNames);
        }

        protected KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private DealerDetailPanel dealerDetailPanel;
        private DealerDetailPanel DealerDetailPanel {
            get {
                return this.dealerDetailPanel ?? (this.dealerDetailPanel = new DealerDetailPanel());
            }
        }

        private EnterpriseInvoicingInfoForDealerDetailPanel enterpriseInvoicingInfoForDealer;

        protected EnterpriseInvoicingInfoForDealerDetailPanel EnterpriseInvoicingInfoForDealer {
            get {
                return this.enterpriseInvoicingInfoForDealer ?? (this.enterpriseInvoicingInfoForDealer = new EnterpriseInvoicingInfoForDealerDetailPanel());
            }
        }

        private DataGridViewBase dealerMobileNumberListForDetailDataGridView;
        public DataGridViewBase DealerMobileNumberListForDetailDataGridView {
            get {
                if(this.dealerMobileNumberListForDetailDataGridView == null) {
                    this.dealerMobileNumberListForDetailDataGridView = DI.GetDataGridView("DealerMobileNumberListForDetail");
                    this.dealerMobileNumberListForDetailDataGridView.DomainContext = this.DomainContext;
                }
                return this.dealerMobileNumberListForDetailDataGridView;
            }
        }

        protected virtual void CreateUI() {
            this.Root.Children.Add(DealerDetailPanel);
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.Register(ChannelsUIStrings.DataEditView_GroupTitle_MobileNumberList, null, () => this.DealerMobileNumberListForDetailDataGridView);
            //this.gridTabItemMobileNumberList.Children.Add(detailDataEditView);
            this.gridEnterpriseInformation.Children.Add(DI.GetDetailPanel("EnterpriseInformation"));
            this.radTabItemDealerServiceExt.Content = DI.GetDetailPanel("DealerServiceExt");
            this.radTabItemEnterpriseInvoicingInfo.Content = this.EnterpriseInvoicingInfoForDealer;
            this.KeyValueManager.LoadData();
        }

        protected override string Title {
            get {
                return ChannelsUIStrings.DataEditView_Title_DealerDetailTogether;
            }
        }

        protected virtual void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetDealersWithCompanyInvoiceInfoAndDealerServiceExtQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.DomainContext.Load(this.DomainContext.GetDealerMobileNumberListsQuery().Where(c => c.DealerId == entity.Id), LoadBehavior.RefreshCurrent, loadOp1 => {
                    if(loadOp1.HasError) {
                        if(!loadOp1.IsErrorHandled)
                            loadOp1.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    var details = loadOp1.Entities;
                    if(details == null)
                        return;
                    var i = 1;
                    foreach(var item in details) {
                        item.SerialNumber = i++;
                    }
                }, null);
                var companyInvoiceInfo = entity.CompanyInvoiceInfo.FirstOrDefault(e => e.InvoiceCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId);
                if (companyInvoiceInfo != null) companyInvoiceInfo.CompDocumentType = entity.Company.CompDocumentType;
                this.EnterpriseInvoicingInfoForDealer.SetValue(DataContextProperty, companyInvoiceInfo);
                this.SetObjectToEdit(entity);
                //由于提交后再次查看，作废的数据仍在缓存中，此处手动清除
                foreach(var item in entity.DealerServiceInfoes.Where(e => e.Status == (int)DcsBaseDataStatus.作废)) {
                    this.DomainContext.DealerServiceInfos.Detach(item);
                }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
