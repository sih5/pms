﻿
namespace Sunlight.Silverlight.Dcs.Channels.Views.DataEdit {
    public class DealerDetailTogetherForCAMDataEditView : DealerDetailTogetherDataEditView {
        protected override void CreateUI() {
            var dealerPanel = DI.GetDetailPanel("Dealer");
            this.Root.Children.Add(dealerPanel);
            this.gridEnterpriseInformation.Children.Add(DI.GetDetailPanel("EnterpriseInformation"));
            this.radTabItemDealerServiceExt.Content = DI.GetDetailPanel("DealerServiceExt");
            this.KeyValueManager.LoadData();
        }
    }
}
