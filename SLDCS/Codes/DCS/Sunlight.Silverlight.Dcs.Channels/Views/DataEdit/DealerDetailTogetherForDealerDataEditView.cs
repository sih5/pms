﻿
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.Channels.Views.DataEdit {
    public class DealerDetailTogetherForDealerDataEditView : DealerDetailTogetherDataEditView {
        protected override void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetDealersWithCompanyInvoiceInfoAndDealerServiceExtQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.DomainContext.Load(this.DomainContext.GetDealerMobileNumberListsQuery().Where(c => c.DealerId == entity.Id), LoadBehavior.RefreshCurrent, loadOp1 => {
                    if(loadOp1.HasError) {
                        if(!loadOp1.IsErrorHandled)
                            loadOp1.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    var details = loadOp1.Entities;
                    if(details == null)
                        return;
                    var i = 1;
                    foreach(var item in details) {
                        item.SerialNumber = i++;
                    }
                }, null);
                var companyInvoiceInfo = entity.CompanyInvoiceInfo.FirstOrDefault(e => e.CompanyId == BaseApp.Current.CurrentUserData.EnterpriseId);
                this.EnterpriseInvoicingInfoForDealer.SetValue(DataContextProperty, companyInvoiceInfo);
                this.SetObjectToEdit(entity);
                //由于提交后再次查看，作废的数据仍在缓存中，此处手动清除
                foreach(var item in entity.DealerServiceInfoes.Where(e => e.Status == (int)DcsBaseDataStatus.作废)) {
                    this.DomainContext.DealerServiceInfos.Detach(item);
                }
            }, null);
        }
    }
}
