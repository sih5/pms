﻿using System.Windows;
using System.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataEdit {
    public class DealerForCAMDataEditView : DealerDataEditView {
        protected override void CreateUI() {
            var dealerPanel = DI.GetDataEditPanel("Dealer");
            this.Root.Children.Add(dealerPanel);
            var enterpriseInformationDataEditPanel = DI.GetDataEditPanel("EnterpriseInformation");
            enterpriseInformationDataEditPanel.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(enterpriseInformationDataEditPanel);
            this.Line.Visibility = Visibility.Collapsed;
        }
    }
}
