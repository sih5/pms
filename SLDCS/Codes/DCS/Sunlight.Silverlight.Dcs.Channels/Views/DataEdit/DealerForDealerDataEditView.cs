﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Channels.Panels.Detail;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Common.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataEdit {
    public class DealerForDealerDataEditView : DealerTogetherDataEditView {
        private DataGridViewBase dealerMobileNumberListForDetailDataGridView;
        public DataGridViewBase DealerMobileNumberListForDetailDataGridView {
            get {
                if(this.dealerMobileNumberListForDetailDataGridView == null) {
                    this.dealerMobileNumberListForDetailDataGridView = DI.GetDataGridView("DealerMobileNumberListForDetail");
                    this.dealerMobileNumberListForDetailDataGridView.DomainContext = this.DomainContext;
                }
                return this.dealerMobileNumberListForDetailDataGridView;
            }
        }
        private EnterpriseInformationForDealerEditDataEditPanel dataEditPanel;

        private EnterpriseInformationForDealerEditDataEditPanel DataEditPanel {
            get {
                return this.dataEditPanel ?? (this.dataEditPanel = (EnterpriseInformationForDealerEditDataEditPanel)DI.GetDataEditPanel("EnterpriseInformationForDealerEdit"));
            }
        }
        private CompanyInvoiceInfo companyInvoiceInfos;

        private CompanyInvoiceInfo CompanyInvoiceInfos {
            get {
                return this.companyInvoiceInfos ?? (this.companyInvoiceInfos = new CompanyInvoiceInfo());
            }
            set {
                this.companyInvoiceInfos = value;
            }

        }

        private EnterpriseInvoicingInfoForDealerEditDataEditPanel enterpriseInvoicingInfoForDealer;

        private EnterpriseInvoicingInfoForDealerEditDataEditPanel EnterpriseInvoicingInfoForDealerEdit {
            get {
                return this.enterpriseInvoicingInfoForDealer ?? (this.enterpriseInvoicingInfoForDealer = new EnterpriseInvoicingInfoForDealerEditDataEditPanel());
            }
        }
        protected override void CreateUI() {
            this.Root.Children.Add(new DealerDetailPanel());
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(ChannelsUIStrings.DataEditView_GroupTitle_MobileNumberList, null, () => this.DealerMobileNumberListForDetailDataGridView);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            this.gridEnterpriseInformation.Children.Add(this.DataEditPanel);
            this.gridTabItemDealerServiceExt.Children.Add(DI.GetDetailPanel("DealerServiceExt"));
            this.gridTabItemEnterpriseInvoicingInfo.Children.Add(this.EnterpriseInvoicingInfoForDealerEdit);
            //this.gridTabItemMobileNumberList.Children.Add(detailDataEditView);
        }
        protected override void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetDealersWithCompanyInvoiceInfoAndDealerServiceExtQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.DomainContext.Load(this.DomainContext.GetDealerMobileNumberListsQuery().Where(c => c.DealerId == entity.Id), LoadBehavior.RefreshCurrent, loadOp1 => {
                    if(loadOp1.HasError) {
                        if(!loadOp1.IsErrorHandled)
                            loadOp1.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    var details = loadOp1.Entities;
                    if(details == null)
                        return;
                    var i = 1;
                    foreach(var item in details) {
                        item.SerialNumber = i++;
                    }
                }, null);
                if(entity.CompanyInvoiceInfo != null) {
                    CompanyInvoiceInfos = entity.CompanyInvoiceInfo.FirstOrDefault(r => r.CompanyId == BaseApp.Current.CurrentUserData.EnterpriseId);
                    this.EnterpriseInvoicingInfoForDealerEdit.SetValue(DataContextProperty, CompanyInvoiceInfos);
                }
                this.SetObjectToEdit(entity);
                //由于提交后再次查看，作废的数据仍在缓存中，此处手动清除
                foreach(var item in entity.DealerServiceInfoes.Where(e => e.Status == (int)DcsBaseDataStatus.作废)) {
                    this.DomainContext.DealerServiceInfos.Detach(item);
                }
            }, null);
        }
        protected override void OnEditSubmitting() {
            var dealer = this.DataContext as Dealer;
            if(dealer == null)
                return;

            dealer.Company.Code = dealer.Code;
            dealer.Company.Name = dealer.Name;
            //dealer.Company.Type = (int)DcsCompanyType.服务站;
            if(!dealer.CompanyInvoiceInfo.Any()) {
                CompanyInvoiceInfos.InvoiceCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                CompanyInvoiceInfos.CompanyCode = dealer.Code;
                CompanyInvoiceInfos.CompanyName = dealer.Name;
                CompanyInvoiceInfos.Status = (int)DcsBaseDataStatus.有效;
                dealer.CompanyInvoiceInfo.Add(CompanyInvoiceInfos);
            } else {
                var companyInvoiceInfo = dealer.CompanyInvoiceInfo.First();
                if(!string.IsNullOrEmpty(companyInvoiceInfo.Linkman) && companyInvoiceInfo.Linkman.Length < 2) {
                    UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_LinkmanLessThanTwo);
                    return;
                }
            }
            dealer.ValidationErrors.Clear();
            foreach(var entity in dealer.DealerServiceInfoes)
                entity.ValidationErrors.Clear();
            dealer.Company.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(dealer.Company.CityName) || string.IsNullOrEmpty(dealer.Company.ProvinceName) || string.IsNullOrEmpty(dealer.Company.CountyName)) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_Company_CityNameIsNull);
                return;
            }
            if(dealer.ValidationErrors.Any() || dealer.DealerServiceInfoes.Any(r => r.HasValidationErrors) || dealer.Company.ValidationErrors.Any())
                return;
            ((IEditableObject)dealer).EndEdit();
            try {
                if(this.EditState == DataEditState.Edit) {
                    this.DomainContext.修改经销商基本信息集中(dealer.Company, dealer, dealer.DealerServiceExt, dealer.CompanyInvoiceInfo.First(), dealer.DealerMobileNumberLists.ToArray(), invokeOp => {
                        if(invokeOp.HasError) {
                            if(!invokeOp.IsErrorHandled)
                                invokeOp.MarkErrorAsHandled();
                            var error = invokeOp.ValidationErrors.FirstOrDefault();
                            if(error != null) {
                                UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage, 5);
                            } else {
                                DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                            }
                            DomainContext.RejectChanges();
                            return;
                        }
                        this.NotifyEditSubmitted();
                        this.OnCustomEditSubmitted();
                    }, null);

                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
        }

        public new event EventHandler EditSubmitted;
        private void NotifyEditSubmitted() {
            var handler = this.EditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }
    }
}
