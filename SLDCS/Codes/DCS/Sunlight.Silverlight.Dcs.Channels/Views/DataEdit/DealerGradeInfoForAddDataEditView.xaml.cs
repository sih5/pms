﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataEdit {
    public partial class DealerGradeInfoForAddDataEditView : INotifyPropertyChanged {
        public DealerGradeInfoForAddDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private DealerGradeInfo dealerGradeInfo;
        private ObservableCollection<DealerGradeInfo> dealerGradeInfos;
        private ObservableCollection<KeyValuePair> kvBrands;
        private DataGridViewBase gradeForEditDataGridView;

        public ObservableCollection<DealerGradeInfo> DealerGradeInfos
        {
            get {
                if(this.dealerGradeInfos == null)
                    this.dealerGradeInfos = new ObservableCollection<DealerGradeInfo>();
                return this.dealerGradeInfos;
            }
        }

        public DealerGradeInfo DealerGradeInfo
        {
            get {
                return dealerGradeInfo;
            }
            set {
                dealerGradeInfo = value;
                this.OnPropertyChanged("DealerGradeInfo");
            }
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_GradeName;
            }
        }

        public ObservableCollection<KeyValuePair> KvBrands {
            get {
                if(this.kvBrands == null)
                    this.kvBrands = new ObservableCollection<KeyValuePair>();
                return this.kvBrands;
            }
        }

        public DataGridViewBase DealerGradeForEditDataGridView
        {
            get {
                if(this.gradeForEditDataGridView == null) {
                    this.gradeForEditDataGridView = DI.GetDataGridView("DealerGradeForEdit");
                    this.gradeForEditDataGridView.DomainContext = this.DomainContext;
                    this.gradeForEditDataGridView.DataContext = this;
                }
                return this.gradeForEditDataGridView;
            }
        }


         
        private void OnPropertyChanged(string propertyName) {
            if(PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void CreateUI() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(e => e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                foreach(var item in loadOp.Entities) {
                    this.KvBrands.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.Name,
                        UserObject = item
                    });
                }
            }, null);
            this.LayoutRoot.Children.Add(this.CreateHorizontalLine(0, 1, 0, 0));
            var detail = new DcsDetailDataEditView();
            detail.Register(ChannelsUIStrings.DataEditView_Group_Grade, null, () => this.DealerGradeForEditDataGridView);
            Grid.SetRow(detail, 2);
            this.LayoutRoot.Children.Add(detail);
            
           
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetDealerGradeInfoesQuery().Where(entity => entity.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                this.DealerGradeInfo = loadOp.Entities.Cast<DealerGradeInfo>().FirstOrDefault();
                this.DealerGradeInfos.Clear();
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.DealerGradeInfo = entity;
                    entity.SerialNumber = 1;
                    this.SetObjectToEdit(entity);
                    this.DealerGradeInfos.Add(entity);
                }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public void InitializeAddData() {
            var userData = BaseApp.Current.CurrentUserData;
            this.DealerGradeInfo = new DealerGradeInfo
            {

                Status = (int)DcsBaseDataStatus.有效
            };
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {
         
            if(!this.DealerGradeForEditDataGridView.CommitEdit())
                return;
            if(!this.DealerGradeInfos.Any()) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_DealerGradeInfo_DetailNotNull);
                return;
            }
            if(this.DealerGradeInfos.GroupBy(e => e.GradeName).Any(obj => obj.Count() > 1)) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_DealerGradeInfo_GradeNotRepeat);
                return;
            }
            if (DealerGradeInfo.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_GradeCoefficient_BrandNotNull);
                return;
            }
            var currrentPartsSalesCategory = this.KvBrands.SingleOrDefault(e => e.Key == this.DealerGradeInfo.PartsSalesCategoryId).UserObject as PartsSalesCategory;
            foreach (var entity in this.DealerGradeInfos)
            {
                entity.PartsSalesCategoryId = currrentPartsSalesCategory.Id;
                entity.PartsSalesCategoryName = currrentPartsSalesCategory.Name;

                ((IEditableObject)entity).EndEdit();
                if (!this.DomainContext.DealerGradeInfos.Contains(entity))
                    this.DomainContext.DealerGradeInfos.Add(entity);
            }
            base.OnEditSubmitting();
        }

        protected override void OnEditSubmitted() {
            this.DealerGradeInfos.Clear();
            base.OnEditSubmitted();
        }

        protected override void OnEditCancelled() {
            this.DomainContext.RejectChanges();
            this.DealerGradeInfos.Clear();
            base.OnEditCancelled();
        }
 
    }
}
