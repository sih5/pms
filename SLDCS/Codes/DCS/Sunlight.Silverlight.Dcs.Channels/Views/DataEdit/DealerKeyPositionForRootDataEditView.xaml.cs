﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Core;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataEdit {
    public partial class DealerKeyPositionForRootDataEditView {
        public DealerKeyPositionForRootDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("DealerKeyPositionForRoot"));
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetDealerKeyPositionsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();

                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_DealerKeyPosition;
            }
        }

        protected override void OnEditSubmitting() {
            var dealerKeyPosition = this.DataContext as DealerKeyPosition;
            if(dealerKeyPosition == null)
                return;
            dealerKeyPosition.ValidationErrors.Clear();
            if(string.IsNullOrWhiteSpace(dealerKeyPosition.PositionName))
                dealerKeyPosition.ValidationErrors.Add(new ValidationResult(ChannelsUIStrings.DataEditView_Validation_DealerKeyPosition_PositionNameIsNull, new[] {
                    "PositionName"
                }));
            if(string.IsNullOrWhiteSpace(dealerKeyPosition.Responsibility))
                dealerKeyPosition.ValidationErrors.Add(new ValidationResult(ChannelsUIStrings.DataEditView_Validation_DealerKeyPosition_ResponsibilityIsNull, new[] {
                    "Responsibility"
                }));
            if(dealerKeyPosition.HasValidationErrors)
                return;          
            ((IEditableObject)dealerKeyPosition).EndEdit();
                try {
                    if (dealerKeyPosition.Can是否关键岗位修改)
                        dealerKeyPosition.是否关键岗位修改();

            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();        
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
