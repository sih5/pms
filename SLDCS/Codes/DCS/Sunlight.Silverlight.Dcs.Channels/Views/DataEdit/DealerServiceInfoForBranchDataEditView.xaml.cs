﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Channels.Views.DataGrid;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
namespace Sunlight.Silverlight.Dcs.Channels.Views.DataEdit {
    public partial class DealerServiceInfoForBranchDataEditView : INotifyPropertyChanged {
        private ObservableCollection<DealerBusinessPermit> dealerBusinessPermits;
        private KeyValueManager keyValueManager;
        public int MarketingDepartmentId = 0;
        public int DealerId;

        private readonly ObservableCollection<KeyValuePair> kvOutFeeGrade = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsManagingFeeGrade = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvServiceFeeGrade = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvGradeCoefficient = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategory = new ObservableCollection<KeyValuePair>();


        public ObservableCollection<DealerBusinessPermit> DealerBusinessPermits {
            get {
                return this.dealerBusinessPermits ?? (this.dealerBusinessPermits = new ObservableCollection<DealerBusinessPermit>());
            }
        }

        private ICommand searchCommand;
        public ICommand SearchCommand {
            get {
                return this.searchCommand ?? (this.searchCommand = new DelegateCommand(this.Search));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
        private readonly string[] kvNames = {
            "ServicePermission","DealerServiceInfo_ServiceStationType","Invoice_Type","SaleServiceLayout","DlrSerInfo_TrunkNetworkType","IsOrNot","ExternalState"
        };
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private DataGridViewBase dealerServiceInfoBindServiceProductLineForEditDataGridView;
        private DataGridViewBase DealerServiceInfoBindServiceProductLineForEditDataGridView {
            get {
                if(this.dealerServiceInfoBindServiceProductLineForEditDataGridView == null) {
                    this.dealerServiceInfoBindServiceProductLineForEditDataGridView = DI.GetDataGridView("DealerServiceInfoBindServiceProductLineForEdit");
                    this.dealerServiceInfoBindServiceProductLineForEditDataGridView.DomainContext = this.DomainContext;
                    this.dealerServiceInfoBindServiceProductLineForEditDataGridView.DataContext = this;
                }
                return this.dealerServiceInfoBindServiceProductLineForEditDataGridView;
            }
        }

        public DealerServiceInfoForBranchDataEditView() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.keyValueManager.LoadData();
            this.Initializer.Register(this.CreateUI);

        }

        private void CreateUI() {
            //选择服务站
            var dealerQueryWindow = DI.GetQueryWindow("DealerForServiceInfo");
            this.ptbDealerName.PopupContent = dealerQueryWindow;
            dealerQueryWindow.SelectionDecided += dealerQueryWindow_SelectionDecided;

            //选择市场部
            var marketDepearmentQueryWindow = DI.GetQueryWindow("MarketingDepartment");
            this.ptbMarketingDepartment.PopupContent = marketDepearmentQueryWindow;
            marketDepearmentQueryWindow.SelectionDecided += marketDepearmentQueryWindow_SelectionDecided;
            marketDepearmentQueryWindow.Loaded += marketDepearmentQueryWindow_Loaded;
            //选择旧件仓库
            //var usedPartsWarehouseQueryWindow = DI.GetQueryWindow("UsedPartsWarehouse");
            ////this.ptbUsedPartsWarehouse.PopupContent = usedPartsWarehouseQueryWindow;
            //usedPartsWarehouseQueryWindow.SelectionDecided += usedPartsWarehouseQueryWindow_SelectionDecided;
            //usedPartsWarehouseQueryWindow.Loaded += usedPartsWarehouseQueryWindow_Loaded;
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(ChannelsUIStrings.DataEditView_Title_DealerBusinessPermitEdit, null, () => this.DealerServiceInfoBindServiceProductLineForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 1);
            detailDataEditView.Margin = new Thickness(50, 0, 0, 0);
            this.cbxPartsSalesCategory.SelectionChanged += cbxPartsSalesCategory_SelectionChanged;
            this.DomainContext.Load(this.DomainContext.GetChannelCapabilitiesQuery().Where(entity => entity.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                if(loadOp.HasError)
                    return;
                this.cbxChannelCapability.ItemsSource = loadOp.Entities;
            }, null);
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoryByBreachIdQuery(BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if(loadOp.HasError)
                    return;
                this.cbxPartsSalesCategory.ItemsSource = loadOp.Entities;
            }, null);
        }

        private void marketDepearmentQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var dealerServiceInfo = this.DataContext as DealerServiceInfo;
            if(dealerServiceInfo == null)
                return;
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null)
                return;
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "PartsSalesCategoryId", dealerServiceInfo.PartsSalesCategoryId
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "PartsSalesCategoryId", false
            });
        }
       
        protected override void OnEditSubmitted() {
            this.partsSalesCategoryId = 0;
            this.DealerBusinessPermits.Clear();
            base.OnEditSubmitted();
        }

        protected override void OnEditCancelled() {
            this.partsSalesCategoryId = 0;
            this.DealerBusinessPermits.Clear();
            base.OnEditCancelled();
        }

        private void SetComboxItemsSource() {
            var dealerServiceInfo = this.DataContext as DealerServiceInfo;
            if(dealerServiceInfo == null)
                return;

            //订货默认仓库
            this.DomainContext.Load(this.DomainContext.查询销售组织仓库Query(dealerServiceInfo.DealerId, BaseApp.Current.CurrentUserData.EnterpriseId, null, dealerServiceInfo.PartsSalesCategoryId, null).Where(r => r.Warehouse.Status == (int)DcsBaseDataStatus.有效 && r.SalesUnit.Status == (int)DcsMasterDataStatus.有效), loadOp => {
                if(loadOp.HasError)
                    return;
                this.cbxWarehouse.ItemsSource = loadOp.Entities.Select(r => r.Warehouse).ToArray();
            }, null);
        }

        private int partsSalesCategoryId = 0;

        private void cbxPartsSalesCategory_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            var dealerServiceInfo = this.DataContext as DealerServiceInfo;
            if(dealerServiceInfo == null)
                return;
            var dataGridView = this.DealerServiceInfoBindServiceProductLineForEditDataGridView as DealerServiceInfoBindServiceProductLineForEditDataGridView;
            if(dataGridView == null)
                return;
            if(DealerBusinessPermits.Any() && this.partsSalesCategoryId != 0) {
                DcsUtils.Confirm(ChannelsUIStrings.DataEditView_Validation_DealerServiceInfo_DealerBusinessPermit, () => {
                    this.DealerBusinessPermits.Clear();
                    this.partsSalesCategoryId = dealerServiceInfo.PartsSalesCategoryId;
                    SetComboxItemsSource();
                    this.DomainContext.Load(this.DomainContext.GetServiceProductLineViewWithDetailQuery().Where(r => r.PartsSalesCategoryId == dealerServiceInfo.PartsSalesCategoryId && r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            loadOp.MarkErrorAsHandled();
                            return;
                        }
                        dataGridView.ServiceProductLineViews.Clear();
                        dataGridView.DicServiceProductLineViews.Clear();
                        foreach(var entity in loadOp.Entities) {
                            if(dataGridView.ServiceProductLineViews.All(ex => ex.ProductLineId != entity.ProductLineId)) {
                                dataGridView.ServiceProductLineViews.Add(entity);
                                dataGridView.DicServiceProductLineViews.Add(new KeyValuePair {
                                    Key = entity.ProductLineId,
                                    Value = entity.ProductLineCode,
                                    UserObject = entity
                                });
                            }
                            if(dealerServiceInfo.EntityState == EntityState.New) {
                                this.DealerBusinessPermits.Add(new DealerBusinessPermit {
                                    ServiceProductLineId = entity.ProductLineId,
                                    ServiceProductLineCode = entity.ProductLineCode,
                                    ServiceProductLineName = entity.ProductLineName,
                                    ProductLineType = (int)entity.ProductLineType,
                                    BranchId = entity.BranchId
                                });
                            }
                        }
                    }, null);
                }, () => {
                    dealerServiceInfo.PartsSalesCategoryId = this.partsSalesCategoryId;
                });
            } else {
                this.partsSalesCategoryId = dealerServiceInfo.PartsSalesCategoryId;
                SetComboxItemsSource();
                this.DomainContext.Load(this.DomainContext.GetServiceProductLineViewWithDetailQuery().Where(r => r.PartsSalesCategoryId == dealerServiceInfo.PartsSalesCategoryId && r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        loadOp.MarkErrorAsHandled();
                        return;
                    }
                    dataGridView.ServiceProductLineViews.Clear();
                    dataGridView.DicServiceProductLineViews.Clear();
                    foreach(var entity in loadOp.Entities) {
                        if(dataGridView.ServiceProductLineViews.All(ex => ex.ProductLineId != entity.ProductLineId)) {
                            dataGridView.ServiceProductLineViews.Add(entity);
                            dataGridView.DicServiceProductLineViews.Add(new KeyValuePair {
                                Key = entity.ProductLineId,
                                Value = entity.ProductLineCode,
                                UserObject = entity
                            });
                            if(dealerServiceInfo.EntityState == EntityState.New) {
                                this.DealerBusinessPermits.Add(new DealerBusinessPermit {
                                    ServiceProductLineId = entity.ProductLineId,
                                    ServiceProductLineCode = entity.ProductLineCode,
                                    ServiceProductLineName = entity.ProductLineName,
                                    ProductLineType = (int)entity.ProductLineType,
                                    BranchId = entity.BranchId
                                });
                            }
                        }
                    }
                }, null);
            }
        }

        protected override void Reset() {
            var dealerBusinessPermits = this.DomainContext.DealerBusinessPermits.ToArray();
            if(dealerBusinessPermits.Any())
                foreach(var rate in dealerBusinessPermits)
                    this.DomainContext.DealerBusinessPermits.Detach(rate);
        }

        private void usedPartsWarehouseQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var dealerServiceInfo = this.DataContext as DealerServiceInfo;
            var queryWindow = sender as DcsQueryWindowBase;
            if(dealerServiceInfo != null && dealerServiceInfo.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_PartsRetailOrder_WarehouseIsNull);
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
                return;
            }
            if(queryWindow == null)
                return;
            //获取 根据所选仓库Id 获取 仓库与销售组织关系。 在获取 配件销售类型Id  传入弹出框。获取唯一的价格属性
            if(dealerServiceInfo != null)
                queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                    "Common", "PartsSalesCategoryId", dealerServiceInfo.PartsSalesCategoryId
                });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "PartsSalesCategoryId", false
            });
            
        }

        public object RepairAuthorityGrades {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public object ServiceStationTypes {
            get {
                return this.KeyValueManager[this.kvNames[1]];
            }
        }

        public object MaterialCostInvoiceTypes {
            get {
                return this.KeyValueManager[this.kvNames[2]];
            }
        }

        public object LaborCostCostInvoiceTypes {
            get {
                return this.KeyValueManager[this.kvNames[2]];
            }
        }

        public object SaleandServicesiteLayouts {
            get {
                return this.KeyValueManager[this.kvNames[3]];
            }
        }
        public object KvTrunkNetworkTypes {
            get {
                return this.KeyValueManager[this.kvNames[4]];
            }
        }

        public object KvOrNot {
            get {
                return this.KeyValueManager[this.kvNames[5]];
            }
        }
        public object KvGrades {
            get {
                return this.KeyValueManager[this.kvNames[6]];
            }
        }
        public object KvExternalStates {
            get {
                return this.KeyValueManager[this.kvNames[7]];
            }
        }

        public ObservableCollection<KeyValuePair> KvServiceFeeGrades {
            get {
                return this.kvServiceFeeGrade;
            }
        }

        public ObservableCollection<KeyValuePair> KvPartsManagingFeeGrades {
            get {
                return this.kvPartsManagingFeeGrade;
            }
        }

        public ObservableCollection<KeyValuePair> KvOutFeeGrades {
            get {
                return this.kvOutFeeGrade;
            }
        }

        public ObservableCollection<KeyValuePair> KvGradeCoefficients {
            get {
                return this.kvGradeCoefficient;
            }
        }

        public ObservableCollection<KeyValuePair> KvPartsSalesCategories {
            get {
                return this.kvPartsSalesCategory;
            }
        }

        public ObservableCollection<KeyValuePair> KvcbxChannelCapabilities {
            get {
                return this.kvPartsSalesCategory;
            }
        }

        private void dealerQueryWindow_SelectionDecided(object sender, System.EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var dealer = queryWindow.SelectedEntities.Cast<Dealer>().SingleOrDefault();
            if(dealer == null)
                return;
            var dealerServiceInfo = this.DataContext as DealerServiceInfo;
            if(dealerServiceInfo == null)
                return;
            dealerServiceInfo.Dealer = null;
            this.DomainContext.Load(this.DomainContext.GetDealerWithCompanyQuery(dealer.Id), LoadBehavior.MergeIntoCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var item = loadOp.Entities.FirstOrDefault();
                if(item == null) {
                    UIHelper.ShowNotification(ChannelsUIStrings.DealerServerInfo_Validate1);
                    return;
                } else {
                    dealerServiceInfo.Dealer = item;
                    this.DealerId = item.Id;
                }
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }, null);

        }

        private void usedPartsWarehouseQueryWindow_SelectionDecided(object sender, System.EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var dealer = queryWindow.SelectedEntities.Cast<UsedPartsWarehouse>().SingleOrDefault();
            if(dealer == null)
                return;
            var dealerServiceInfo = this.DataContext as DealerServiceInfo;
            if(dealerServiceInfo == null)
                return;
            if(dealerServiceInfo.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_PartsRetailOrder_WarehouseIsNull);
                return;
            }
            this.DomainContext.Load(this.DomainContext.GetUsedPartsWarehousesQuery().Where(ex => ex.Id == dealer.Id), loadOp => {
                if(loadOp.HasError)
                    return;
                var item = loadOp.Entities.SingleOrDefault();
                if(item != null) {
                    dealerServiceInfo.UsedPartsWarehouse = item;
                }
            }, null);
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void marketDepearmentQueryWindow_SelectionDecided(object sender, System.EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var marketingDepartment = queryWindow.SelectedEntities.Cast<MarketingDepartment>().SingleOrDefault();
            if(marketingDepartment == null)
                return;
            var dealerServiceInfo = this.DataContext as DealerServiceInfo;
            if(dealerServiceInfo == null)
                return;
            if(dealerServiceInfo.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_PartsRetailOrder_WarehouseIsNull);
                return;
            }
            this.DomainContext.Load(this.DomainContext.GetMarketingDepartmentsQuery().Where(ex => ex.Id == marketingDepartment.Id), loadOp => {
                if(loadOp.HasError)
                    return;
                var item = loadOp.Entities.SingleOrDefault();
                if(item != null) {
                    dealerServiceInfo.MarketingDepartmentName = item.Name;
                    dealerServiceInfo.MarketingDepartmentId = item.Id;
                    this.MarketingDepartmentId = item.Id;
                }
            }, null);
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void LoadEntityToEdit(int id) {
            
            this.DomainContext.Load(this.DomainContext.GetDealerServiceInfoForDealerWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.partsSalesCategoryId = entity.PartsSalesCategoryId;
                this.DealerId = entity.DealerId;
                if(entity.MarketingDepartment != null) {
                    entity.MarketingDepartmentName = entity.MarketingDepartment.Name;
                    this.MarketingDepartmentId = entity.MarketingDepartmentId;
                }

                this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(i => i.Id == entity.DealerId), LoadBehavior.RefreshCurrent, _loadOp => {
                    if(_loadOp.HasError) {
                        if(!_loadOp.IsErrorHandled)
                            _loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(_loadOp);
                        return;
                    }
                    var companyEntity = _loadOp.Entities.SingleOrDefault();
                    if(companyEntity == null)
                        return;
                    entity.Company = companyEntity;

                }, null);
                this.SetObjectToEdit(entity);
                if(entity.Dealer.DealerBusinessPermits != null)
                    this.DealerBusinessPermits.Clear();

                if(entity.Dealer.DealerBusinessPermits != null)
                    foreach(var item in entity.Dealer.DealerBusinessPermits) {
                        this.DealerBusinessPermits.Add(item);
                    }
                this.SetComboxItemsSource();
            }, null);
        }

        protected override void OnEditSubmitting() {
            if(!DealerServiceInfoBindServiceProductLineForEditDataGridView.CommitEdit())
                return;
            var dealerServiceInfo = this.DataContext as DealerServiceInfo;
            if(dealerServiceInfo == null)
                return;
            dealerServiceInfo.ValidationErrors.Clear();
            if(dealerServiceInfo.DealerId == default(int)) {
                UIHelper.ShowNotification(ChannelsUIStrings.DealerServerInfo_Validate2);
                return;
            }
            if(string.IsNullOrEmpty(dealerServiceInfo.Dealer.ShortName)) {
                UIHelper.ShowNotification(ChannelsUIStrings.DealerServerInfo_Validate3);
                return;
            }
            if(string.IsNullOrEmpty(dealerServiceInfo.BusinessCode)) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_DealerServiceInfo_BusinessCode);
                return;
            }
            if(string.IsNullOrEmpty(dealerServiceInfo.BusinessName)) {
                UIHelper.ShowNotification(ChannelsUIStrings.DealerServerInfo_Validate4);
                return;
            }
            if(string.IsNullOrEmpty(dealerServiceInfo.BusinessDivision)) {
                UIHelper.ShowNotification(ChannelsUIStrings.DealerServerInfo_Validate5);
                return;
            }
            if(!string.IsNullOrEmpty(dealerServiceInfo.HourPhone24)) {
                if(dealerServiceInfo.HourPhone24.Length != 11) {
                    UIHelper.ShowNotification(ChannelsUIStrings.DealerServerInfo_Validate6);
                    return;
                }
            }
            if(dealerServiceInfo.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_GradeCoefficient_BrandNotNull);
                return;
            }
            dealerServiceInfo.MarketingDepartmentId = this.MarketingDepartmentId;
            if(dealerServiceInfo.MarketingDepartmentId == default(int)) {
                UIHelper.ShowNotification(ChannelsUIStrings.DealerServerInfo_Validate7);
                return;
            }
            if(dealerServiceInfo.ChannelCapabilityId == default(int)) {
                UIHelper.ShowNotification(ChannelsUIStrings.DealerServerInfo_Validate8);
                return;
            }
            //if(dealerServiceInfo.ServiceStationType == default(int)) {
            //    UIHelper.ShowNotification("请选择服务站类型");
            //    return;
            //}
            foreach(var item in dealerServiceInfo.Dealer.DealerBusinessPermits) {
                if(this.DealerBusinessPermits.All(r => r.Id != item.Id)) {
                    DomainContext.DealerBusinessPermits.Remove(item);
                }
            }

            foreach(var dealerBusinessPermit in DealerBusinessPermits) {
                dealerBusinessPermit.DealerId = dealerServiceInfo.DealerId;
                dealerServiceInfo.Dealer.DealerBusinessPermits.Add(dealerBusinessPermit);

            }

            if(dealerServiceInfo.Dealer.DealerBusinessPermits.GroupBy(entity => new {
                entity.ServiceProductLineId
            }).Any(array => array.Count() > 1)) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_DealerBusinessPermits_DoubleServiceProductLineId);
                return;
            }
            if (this.EditState == DataEditState.Edit && dealerServiceInfo.ExternalState == (int)DcsExternalState.有效) {
                this.DomainContext.Load(this.DomainContext.GetDealerKeyPositionsQuery().Where(r => r.IsKeyPositions == true && r.BranchId == dealerServiceInfo.BranchId && r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp1 => {
                    if(loadOp1.HasError) {
                        if(!loadOp1.IsErrorHandled)
                            loadOp1.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp1);
                        return;
                    }
                    var result = loadOp1.Entities.ToArray();
                    if(result == null)
                        return;
                    var keyPositionIds = result.Select(o => o.Id);
                    this.DomainContext.Load(this.DomainContext.GetDealerKeyEmployeesForKeyPositionIdsQuery(keyPositionIds.ToArray()).Where(r => r.DealerId == dealerServiceInfo.DealerId && r.PartsSalesCategoryId == dealerServiceInfo.PartsSalesCategoryId && r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp2 => {
                        if(loadOp2.HasError) {
                            if(!loadOp2.IsErrorHandled)
                                loadOp2.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp2);
                            return;
                        }
                        var dealerKeyEmployees = loadOp2.Entities.ToArray();
                        if(dealerKeyEmployees == null) {
                            UIHelper.ShowNotification(ChannelsUIStrings.DealerServerInfo_Validate9);
                            return;
                        }
                        var dealerKeyPositions = result.Count(v => dealerKeyEmployees.Any(o => o.KeyPositionId == v.Id));
                        if(dealerKeyPositions != result.Count()) {
                            UIHelper.ShowNotification(ChannelsUIStrings.DealerServerInfo_Validate9);
                            return;
                        } else {
                            this.DomainContext.Load(this.DomainContext.GetChannelCapabilitiesQuery().Where(t => t.Id == dealerServiceInfo.ChannelCapabilityId), LoadBehavior.RefreshCurrent, loadOp => {
                                if(loadOp.HasError) {
                                    if(!loadOp.IsErrorHandled) {
                                        loadOp.MarkErrorAsHandled();
                                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                                    }
                                }
                                var entity = loadOp.Entities.First();
                                //if(entity.Name == "服务站" || entity.Name == "4S店") {
                                //    if(dealerServiceInfo.UsedPartsWarehouse == null) {
                                //        UIHelper.ShowNotification("请选择旧件仓库");
                                //        return;
                                //    }
                                //    if(dealerServiceInfo.AccreditTime == null) {
                                //        UIHelper.ShowNotification("授权时间不能为空");
                                //        return;
                                //    }
                                //    if(string.IsNullOrEmpty(dealerServiceInfo.HotLine)) {
                                //        UIHelper.ShowNotification("24小时热线不能为空");
                                //        return;
                                //    }
                                //}
                                if(entity.Name == "4S店") {
                                    if(dealerServiceInfo.SaleandServicesiteLayout == default(int)) {
                                        UIHelper.ShowNotification("请选择销售与服务场地布局关系");
                                        return;
                                    }
                                }
                                ((IEditableObject)dealerServiceInfo).EndEdit();
                                var error = entity.ValidationErrors;
                                if(this.EditState == DataEditState.Edit) {
                                    try {
                                        if(dealerServiceInfo.Can修改经销商分公司管理信息) {
                                            dealerServiceInfo.修改经销商分公司管理信息();
                                        }
                                    } catch(ValidationException ex) {
                                        UIHelper.ShowAlertMessage(ex.Message);
                                        return;
                                    }
                                } else {
                                    try {
                                        if(dealerServiceInfo.Can生成经销商分公司管理信息) {
                                            dealerServiceInfo.生成经销商分公司管理信息();
                                        }
                                    } catch(ValidationException ex) {
                                        UIHelper.ShowAlertMessage(ex.Message);
                                        return;
                                    }
                                }
                                base.OnEditSubmitting();
                            }, null);
                        }
                    }, null);
                }, null);
            } else {
                this.DomainContext.Load(this.DomainContext.GetChannelCapabilitiesQuery().Where(t => t.Id == dealerServiceInfo.ChannelCapabilityId), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled) {
                            loadOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                        }
                    }
                    var entity = loadOp.Entities.First();
                    //if(entity.Name == "服务站" || entity.Name == "4S店") {
                    //    if(dealerServiceInfo.UsedPartsWarehouse == null) {
                    //        UIHelper.ShowNotification("请选择旧件仓库");
                    //        return;
                    //    }
                    //    if(dealerServiceInfo.AccreditTime == null) {
                    //        UIHelper.ShowNotification("授权时间不能为空");
                    //        return;
                    //    }
                    //    if(string.IsNullOrEmpty(dealerServiceInfo.HotLine)) {
                    //        UIHelper.ShowNotification("24小时热线不能为空");
                    //        return;
                    //    }
                    //}
                    if(entity.Name == "4S店") {
                        if(dealerServiceInfo.SaleandServicesiteLayout == default(int)) {
                            UIHelper.ShowNotification("请选择销售与服务场地布局关系");
                            return;
                        }
                    }
                    ((IEditableObject)dealerServiceInfo).EndEdit();
                    var error = entity.ValidationErrors;
                    if(this.EditState == DataEditState.Edit) {
                        try {
                            if(dealerServiceInfo.Can修改经销商分公司管理信息) {
                                dealerServiceInfo.修改经销商分公司管理信息();
                            }
                        } catch(ValidationException ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                            return;
                        }
                    } else {
                        try {
                            if(dealerServiceInfo.Can生成经销商分公司管理信息) {
                                dealerServiceInfo.生成经销商分公司管理信息();
                            }
                        } catch(ValidationException ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                            return;
                        }
                    }
                    base.OnEditSubmitting();
                }, null);
            }
        }
        public static void Confirm(string text, Action action) {
            if(action == null)
                throw new ArgumentNullException("action");

            var parameters = new DialogParameters {
                Header = BaseApp.Current.ApplicationName,
                Content = text,
            };
            parameters.Closed += (sender, args) => {
                if(args.DialogResult.HasValue && args.DialogResult.Value)
                    action();
            };
            RadWindow.Alert(parameters);
        }
        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_DealerServiceInfo;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        //private readonly List<TextBlock> changeTextBlocksColor = new List<TextBlock>();
        //private void AddChangeColorTextBlock() {
        //    changeTextBlocksColor.Add(this.tbAccreditTime);
        //    changeTextBlocksColor.Add(this.tbHotLine);
        //    //changeTextBlocksColor.Add(this.tbUsedPartsWarehouse);
        //    changeTextBlocksColor.Add(this.tbServicePermission);
        //}

        //private void cbxChannelCapability_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
        //    var dealerServiceInfo = this.DataContext as DealerServiceInfo;
        //    if(dealerServiceInfo == null || dealerServiceInfo.ChannelCapabilityId == default(int))
        //        return;
        //    if(changeTextBlocksColor == null || !changeTextBlocksColor.Any()) {
        //        AddChangeColorTextBlock();
        //    }
        //    this.DomainContext.Load(this.DomainContext.GetChannelCapabilitiesQuery().Where(t => t.Id == dealerServiceInfo.ChannelCapabilityId), LoadBehavior.RefreshCurrent, loadOp => {
        //        if(loadOp.HasError) {
        //            if(!loadOp.IsErrorHandled) {
        //                loadOp.MarkErrorAsHandled();
        //                DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
        //            }
        //        }
        //        var entity = loadOp.Entities.First();
        //        tbSaleandServicesiteLayout.Foreground = entity.Name == "4S店" ? new SolidColorBrush(Colors.Red) : new SolidColorBrush(Colors.Black);
        //        foreach(var textBlock in changeTextBlocksColor) {
        //            textBlock.Foreground = entity.Name == "专卖店" ? new SolidColorBrush(Colors.Black) : new SolidColorBrush(Colors.Red);
        //        }
        //    }, null);
        //}
        /// <summary>
        /// 查询按钮
        /// </summary>
        private void Search() {
            var dealerServiceInfo = this.DataContext as DealerServiceInfo;
            if(dealerServiceInfo == null)
                return;
            dealerServiceInfo.ValidationErrors.Clear();
            if(dealerServiceInfo.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_PartsRetailOrder_WarehouseIsNull);
                return;
            }
        }
    }
}
