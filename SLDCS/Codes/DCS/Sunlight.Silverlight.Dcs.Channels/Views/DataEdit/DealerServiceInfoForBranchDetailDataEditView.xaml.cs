﻿using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataEdit {
    public partial class DealerServiceInfoForBranchDetailDataEditView {
        public DealerServiceInfoForBranchDetailDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
        private ObservableCollection<DealerBusinessPermit> dealerBusinessPermits;
        public ObservableCollection<DealerBusinessPermit> DealerBusinessPermits {
            get {
                return this.dealerBusinessPermits ?? (this.dealerBusinessPermits = new ObservableCollection<DealerBusinessPermit>());
            }
        }
        private DataGridViewBase dealerServiceInfoBindServiceProductLineForEditDataGridView;
        private DataGridViewBase DealerServiceInfoBindServiceProductLineForEditDataGridView {
            get {
                if(this.dealerServiceInfoBindServiceProductLineForEditDataGridView == null) {
                    this.dealerServiceInfoBindServiceProductLineForEditDataGridView = DI.GetDataGridView("DealerServiceInfoBindServiceProductLineForEdit");
                    this.dealerServiceInfoBindServiceProductLineForEditDataGridView.DomainContext = this.DomainContext;
                    this.dealerServiceInfoBindServiceProductLineForEditDataGridView.DataContext = this;
                }
                return this.dealerServiceInfoBindServiceProductLineForEditDataGridView;
            }
        }
        protected override void OnEditCancelled() {
            this.DealerBusinessPermits.Clear();
            base.OnEditCancelled();
        }
        protected override void Reset() {

            //foreach(var DealerBusinessPermit in dealerServiceInfo.Dealer.DealerBusinessPermits) {
            //    dealerServiceInfo.Dealer.DealerBusinessPermits.Remove(DealerBusinessPermit);
            //}
            var dealerBusinessPermits = this.DomainContext.DealerBusinessPermits.ToArray();
            if(dealerBusinessPermits.Any())
                foreach(var rate in dealerBusinessPermits)
                    this.DomainContext.DealerBusinessPermits.Detach(rate);
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetDealerServiceInfoForDealerWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
            //this.DomainContext.Load(this.DomainContext.GetDealerServiceInfoWithDetailsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(i => i.Id == entity.DealerId), LoadBehavior.RefreshCurrent, _loadOp => {
                    if (_loadOp.HasError) {
                        if (!_loadOp.IsErrorHandled)
                            _loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(_loadOp);
                        return;
                    }
                    var companyEntity = _loadOp.Entities.SingleOrDefault();
                    if (companyEntity == null)
                        return;
                    entity.Company = companyEntity;

                }, null);
                if(entity != null) {
                    foreach(var item in entity.Dealer.DealerBusinessPermits) {
                        this.DealerBusinessPermits.Add(item);
                    }
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        protected override string Title {
            get {
                return ChannelsUIStrings.DetailPanel_Title_Common;
            }

        }
        private void CreateUI() {
            this.LayoutRoot.Children.Add(DI.GetDetailPanel("DealerServiceInfoForBranch"));
            //var detailDataEditView = new DcsDetailDataEditView();
            //detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            //detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            //detailDataEditView.Register(ChannelsUIStrings.DataEditView_Title_DealerBusinessPermitEdit, null, () => this.DealerServiceInfoBindServiceProductLineForEditDataGridView);
            //detailDataEditView.SetValue(Grid.ColumnProperty, 1);
            //this.LayoutRoot.Children.Add(detailDataEditView);
        }

    }
}
