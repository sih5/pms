﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Channels.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Channels.Views.DataGrid;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataEdit {
    public partial class DealerServiceInfoForDealerDataEditView {
        private DataGridViewBase dealerKeyEmployeeEditDataGridView;
        private ObservableCollection<DealerKeyEmployee> dealerKeyEmployees;
        public int branchId;
        public int partsSalesCategoryId;
        public DealerServiceInfoForDealerDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        public ObservableCollection<DealerKeyEmployee> DealerKeyEmployees {
            get {
                return this.dealerKeyEmployees ?? (this.dealerKeyEmployees = new ObservableCollection<DealerKeyEmployee>());
            }
        }


        private DataGridViewBase DealerKeyEmployeeEditDataGridView {
            get {
                if(this.dealerKeyEmployeeEditDataGridView == null) {
                    this.dealerKeyEmployeeEditDataGridView = DI.GetDataGridView("DealerKeyEmployeeForEdit");
                    this.dealerKeyEmployeeEditDataGridView.DomainContext = this.DomainContext;
                    this.dealerKeyEmployeeEditDataGridView.DataContext = this;
                }
                return this.dealerKeyEmployeeEditDataGridView;
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("DealerServiceInfoForDealer"));
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(ChannelsUIStrings.DetailPanel_Title_DealerKeyEmploye, null, () => this.DealerKeyEmployeeEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 1);
            this.Root.Children.Add(detailDataEditView);
        }

        private void LoadEntityToEdit(int id) {
            //this.DomainContext.Load(this.DomainContext.GetDealerServiceInfoForDealerWithDetailsDealerKeyEmployeesQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
            this.DomainContext.Load(this.DomainContext.GetDealerServiceInfoForDealerWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                this.DealerKeyEmployees.Clear();
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    //branchId = entity.BranchId;
                    partsSalesCategoryId = entity.PartsSalesCategoryId;
                    branchId = entity.BranchId;
                    this.SetObjectToEdit(entity);
                    foreach(var item in entity.Dealer.DealerKeyEmployees)
                        DealerKeyEmployees.Add(item);
                    //DealerKeyPosition
                    this.DomainContext.Load(this.DomainContext.GetDealerKeyPositionsQuery().Where(r => r.IsKeyPositions == true && r.BranchId == branchId && r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp1 => {
                        if(loadOp1.HasError) {
                            if(!loadOp1.IsErrorHandled)
                                loadOp1.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp1);
                            return;
                        }
                        var result = loadOp1.Entities.ToArray();
                        if(result == null)
                            return;
                        foreach(var item in result) {
                            if(DealerKeyEmployees.Any(r => r.KeyPositionId == item.Id))
                                continue;
                            var dealerKeyEmployee = new DealerKeyEmployee();
                            dealerKeyEmployee.DealerKeyPosition = item;
                            dealerKeyEmployee.KeyPositionId = item.Id;
                            dealerKeyEmployee.DealerKeyPosition.Responsibility = item.Responsibility;
                            dealerKeyEmployee.IsKeyPositions = item.IsKeyPositions;
                            DealerKeyEmployees.Add(dealerKeyEmployee);

                        }
                    }, null);
                    var dataGridView = this.DealerKeyEmployeeEditDataGridView as DealerKeyEmployeeForEditDataGridView;
                    if(dataGridView == null)
                        return;
                    dataGridView.KvDealerKeyPositions.Clear();
                    DealerServiceInfoForDealerDataEditPanel dataEditPanel = this.FindChildrenByType<DcsDataEditPanelBase>().FirstOrDefault() as DealerServiceInfoForDealerDataEditPanel;
                    if(entity.Status !=(int)DcsMasterDataStatus.有效 ) {
                        dataEditPanel.comboBoxExternalState.IsEnabled = false;
                    } else {
                        dataEditPanel.comboBoxExternalState.IsEnabled = true;
                    }
                    
                    //var dcsDomainContext = new DcsDomainContext();
                    ////dcsDomainContext.Load(dcsDomainContext.GetDealerKeyPositionsQuery().Where(e => e.BranchId == branchId), LoadBehavior.RefreshCurrent, loadOpA => {
                    //    if(loadOpA.HasError)
                    //        return;
                    //    var entities = loadOpA.Entities.Where(r => r.IsKeyPositions == true);
                    //    foreach(var item in entities) {
                    //        if(!DealerKeyEmployees.Any(r => r.KeyPositionId == item.Id))
                    //       DealerKeyEmployees.Add(new DealerKeyEmployee {
                    //                IsKeyPositions = item.IsKeyPositions,
                    //                DealerKeyPosition = item,
                    //                KeyPositionId = item.Id,                                    
                    //            });
                    //    }
                    //}, null);
                    dataGridView.bingDataEditData();
                }
            }, null);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_DealerServiceInfo;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.dealerKeyEmployeeEditDataGridView.CommitEdit())
                return;

            var dealerServiceInfo = this.DataContext as DealerServiceInfo;
            if(dealerServiceInfo == null)
                return;
            dealerServiceInfo.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(dealerServiceInfo.Fix))
                dealerServiceInfo.ValidationErrors.Add(new ValidationResult(ChannelsUIStrings.DataEditView_Validation_DealerServiceInfo_FixIsNull, new[] {
                    "Fix"
                }));
            if(string.IsNullOrWhiteSpace(dealerServiceInfo.HotLine))
                dealerServiceInfo.ValidationErrors.Add(new ValidationResult(ChannelsUIStrings.DataEditView_Validation_DealerServiceInfo_HotLineIsNull, new[] {
                    "HotLine"
                }));
            if(string.IsNullOrWhiteSpace(dealerServiceInfo.Fax))
                dealerServiceInfo.ValidationErrors.Add(new ValidationResult(ChannelsUIStrings.DataEditView_Validation_DealerServiceInfo_FaxIsNull, new[] {
                    "Fax"
                }));
            ((IEditableObject)dealerServiceInfo).EndEdit();
            foreach(var item in dealerServiceInfo.Dealer.DealerKeyEmployees) {
                if(this.DealerKeyEmployees.All(r => r.Id != item.Id)) {
                    dealerServiceInfo.Dealer.DealerKeyEmployees.Remove(item);
                }
            }
            foreach(var item2 in DealerKeyEmployees) {
                item2.Status = (int)DcsBaseDataStatus.有效;
                item2.DealerId = dealerServiceInfo.DealerId;
                item2.PartsSalesCategoryId = partsSalesCategoryId;
                dealerServiceInfo.Dealer.DealerKeyEmployees.Add(item2);
            }

            foreach(var employee in dealerServiceInfo.Dealer.DealerKeyEmployees) {
                employee.ValidationErrors.Clear();
                if(employee.KeyPositionId == default(int))
                    employee.ValidationErrors.Add(new ValidationResult(ChannelsUIStrings.DataGridView_Validation_DealerKeyEmployee_KeyPositionIdIsNull, new[] {
                        "KeyPositionId"
                    }));
                if(employee.Status == default(int))
                    employee.ValidationErrors.Add(new ValidationResult(ChannelsUIStrings.DataGridView_Validation_DealerKeyEmployee_StatusIsNull, new[] {
                        "Status"
                    }));
                if(string.IsNullOrWhiteSpace(employee.Name))
                    employee.ValidationErrors.Add(new ValidationResult(ChannelsUIStrings.DataGridView_Validation_DealerKeyEmployee_NameIsNull, new[] {
                        "Name"
                    }));
                if(string.IsNullOrWhiteSpace(employee.MobileNumber))
                    employee.ValidationErrors.Add(new ValidationResult(ChannelsUIStrings.DataEditView_Validation_MobileNumberIsNull, new[] {
                        "MobileNumber"
                    }));
                if(string.IsNullOrWhiteSpace(employee.IdCardNumber) && employee.IsKeyPositions == true)
                    employee.ValidationErrors.Add(new ValidationResult(ChannelsUIStrings.DataEditView_Validation_IdCardNumberIsNull, new[] {
                       "IdCardNumber"
                  
                    }));

                if(!employee.Sex.HasValue && employee.IsKeyPositions == true) {
                    UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_SexIsNull);
                    return;
                }
                if(!employee.Education.HasValue && employee.IsKeyPositions == true) {
                    UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_EducationIsNull);
                    return;
                }
                if(!employee.ProfessionalRank.HasValue && employee.IsKeyPositions == true) {
                    UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_ProfessionalRankIsNull);
                    return;
                }
                if(!employee.EntryTime.HasValue && employee.IsKeyPositions == true) {
                    UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_EntryTimeIsNull);
                    return;
                }

                if(!employee.IsOnJob.HasValue) {
                    UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_IsOnJobIsNull);
                    return;
                }
                if(employee.IsOnJob == (int)DcsIsOrNot.否 && !employee.LeaveTime.HasValue) {
                    UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_LeaveTimeIsNull);
                    return;
                }

                if(employee.DealerKeyPosition.PositionName == ChannelsUIStrings.DetailPanel_Title_DealerKeyService && !employee.WorkType.HasValue) {
                    UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_WorkTypeIsNull);
                    return;
                }
                if(employee.DealerKeyPosition.PositionName == ChannelsUIStrings.DetailPanel_Title_DealerKeyService && !employee.SkillLevel.HasValue) {
                    UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_SkillLevelIsNull);
                    return;
                }

                if(!employee.HasValidationErrors)
                    ((IEditableObject)employee).EndEdit();

            }
            var entities = dealerServiceInfo.Dealer.DealerKeyEmployees.Where(r => r.IsKeyPositions == true && r.IsOnJob == (int)DcsIsOrNot.是).GroupBy(r => r.KeyPositionId).Where(v => v.Count() > 1);
            if(entities != null && entities.Any()) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_DealerServiceInfo_Validation1);
                return;
            }
            var entitiesy1 = DealerKeyEmployees.Where(r => r.IsKeyPositions == true).GroupBy(r => r.KeyPositionId).Count();
            var entitiesy2 = DealerKeyEmployees.Where(r => r.IsKeyPositions == true && r.IsOnJob == (int)DcsIsOrNot.是).GroupBy(r => r.KeyPositionId).Count();

            if(entitiesy1 != entitiesy2) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_DealerServiceInfo_Validation2);
                return;
            }
            //dealerServiceInfo.Dealer.DealerKeyEmployees.DealerKeyPosition
            //int[] ids = dealerServiceInfo.Dealer.DealerKeyEmployees.Where(r => r.IsOnJob == (int)DcsIsOrNot.是).Select(r => r.KeyPositionId).ToArray();
            //this.DomainContext.Load(DomainContext.GetDealerKeyPositionIdsQuery(ids), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError) {
            //        loadOp.MarkErrorAsHandled();
            //        return;
            //    }
            //    var result = loadOp.Entities.ToArray();
            //    if(result == null || !result.Any()) {
            //        UIHelper.ShowNotification("不存在在职的站长和服务协调人，请维护！");
            //        return;
            //    }
            //    if(!result.Any(r => r.PositionName == "站长")) {
            //        UIHelper.ShowNotification("不存在在职的站长，请维护！");
            //        return;
            //    }
            //    if(!result.Any(r => r.PositionName == "服务协调人")) {
            //        UIHelper.ShowNotification("不存在在职的服务协调人，请维护！");
            //        return;
            //    }

            if(this.EditState == DataEditState.Edit && dealerServiceInfo.ExternalState == (int)DcsExternalState.有效) {
                this.DomainContext.Load(this.DomainContext.GetDealerKeyPositionsQuery().Where(r => r.IsKeyPositions == true && r.BranchId == dealerServiceInfo.BranchId && r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp1 => {
                    if(loadOp1.HasError) {
                        if(!loadOp1.IsErrorHandled)
                            loadOp1.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp1);
                        return;
                    }
                    var result = loadOp1.Entities.ToArray();
                    if(result == null)
                        return;
                    if(dealerServiceInfo.Dealer.DealerKeyEmployees == null) {
                        UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_DealerServiceInfo_Validation3);
                        return;
                    }
                    var dealerKeyPositions = result.Count(v => dealerServiceInfo.Dealer.DealerKeyEmployees.Any(o => o.KeyPositionId == v.Id));
                    if(dealerKeyPositions != result.Count()) {
                        UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_DealerServiceInfo_Validation4);
                        return;
                    } else {
                        if(dealerServiceInfo.Dealer.HasValidationErrors || dealerServiceInfo.HasValidationErrors || dealerServiceInfo.Dealer.DealerKeyEmployees.Any(employee => employee.HasValidationErrors))
                            return;
                        try {
                            if(EditState == DataEditState.Edit)
                                if(dealerServiceInfo.Can修改经销商分公司管理信息)
                                    dealerServiceInfo.修改经销商分公司管理信息();
                        } catch(ValidationException ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                            return;
                        }
                        base.OnEditSubmitting();
                    }
                }, null);
            } else {
                ((IEditableObject)dealerServiceInfo).EndEdit();

                if(dealerServiceInfo.Dealer.HasValidationErrors || dealerServiceInfo.HasValidationErrors || dealerServiceInfo.Dealer.DealerKeyEmployees.Any(employee => employee.HasValidationErrors))
                    return;
                try {
                    if(EditState == DataEditState.Edit)
                        if(dealerServiceInfo.Can修改经销商分公司管理信息)
                            dealerServiceInfo.修改经销商分公司管理信息();
                } catch(ValidationException ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                    return;
                }

                base.OnEditSubmitting();
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void Reset() {
            DealerKeyEmployees.Clear();
            var dealerKeyEmployees = this.DomainContext.DealerKeyEmployees.ToArray();
            if(dealerKeyEmployees.Any())
                foreach(var rate in dealerKeyEmployees)
                    this.DomainContext.DealerKeyEmployees.Detach(rate);
            branchId = 0;
        }
    }
}
