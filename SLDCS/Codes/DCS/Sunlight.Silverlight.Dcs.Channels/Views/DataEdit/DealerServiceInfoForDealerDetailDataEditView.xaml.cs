﻿using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataEdit {
    public partial class DealerServiceInfoForDealerDetailDataEditView {
        private ObservableCollection<DealerKeyEmployee> dealerKeyEmployees;
        public DealerServiceInfoForDealerDetailDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        public ObservableCollection<DealerKeyEmployee> DealerKeyEmployees {
            get {
                return this.dealerKeyEmployees ?? (this.dealerKeyEmployees = new ObservableCollection<DealerKeyEmployee>());
            }
        }

        private DataGridViewBase dealerKeyEmployeeEditDataGridView;
        private DataGridViewBase DealerKeyEmployeeEditDataGridView {
            get {
                if(this.dealerKeyEmployeeEditDataGridView == null) {
                    this.dealerKeyEmployeeEditDataGridView = DI.GetDataGridView("DealerKeyEmployeeDetail");
                    this.dealerKeyEmployeeEditDataGridView.DomainContext = this.DomainContext;
                    this.dealerKeyEmployeeEditDataGridView.DataContext = this;
                }
                return this.dealerKeyEmployeeEditDataGridView;
            }
        }

        private DataGridViewBase dealerServiceInfoBindServiceProductLineForEditDataGridView;
        private DataGridViewBase DealerServiceInfoBindServiceProductLineForEditDataGridView {
            get {
                if(this.dealerServiceInfoBindServiceProductLineForEditDataGridView == null) {
                    //  this.dealerServiceInfoBindServiceProductLineForEditDataGridView = DI.GetDataGridView("DealerServiceInfoBindServiceProductLineForDetail");
                    this.dealerServiceInfoBindServiceProductLineForEditDataGridView = DI.GetDataGridView("DealerServiceInfoBindServiceProductLineForEdit");
                    this.dealerServiceInfoBindServiceProductLineForEditDataGridView.DomainContext = this.DomainContext;
                    this.dealerServiceInfoBindServiceProductLineForEditDataGridView.DataContext = this;
                }
                return this.dealerServiceInfoBindServiceProductLineForEditDataGridView;
            }
        }

        private ObservableCollection<DealerBusinessPermit> dealerBusinessPermits;
        public ObservableCollection<DealerBusinessPermit> DealerBusinessPermits {
            get {
                return this.dealerBusinessPermits ?? (this.dealerBusinessPermits = new ObservableCollection<DealerBusinessPermit>());
            }
        }

        protected override string Title {
            get {
                return ChannelsUIStrings.DetailPanel_Title_Common;
            }
        }

        protected override void OnEditCancelled() {
            this.DealerBusinessPermits.Clear();
            base.OnEditCancelled();
        }
        protected override void Reset() {

            //foreach(var DealerBusinessPermit in dealerServiceInfo.Dealer.DealerBusinessPermits) {
            //    dealerServiceInfo.Dealer.DealerBusinessPermits.Remove(DealerBusinessPermit);
            //}
            var dealerBusinessPermits = this.DomainContext.DealerBusinessPermits.ToArray();
            if(dealerBusinessPermits.Any())
                foreach(var rate in dealerBusinessPermits)
                    this.DomainContext.DealerBusinessPermits.Detach(rate);
            var dealerKeyEmployees = this.DomainContext.DealerKeyEmployees.ToArray();
            if(dealerKeyEmployees.Any())
                foreach(var rate2 in dealerKeyEmployees)
                    this.DomainContext.DealerKeyEmployees.Detach(rate2);
        }
        private void LoadEntityToEdit(int id) {
            //this.DomainContext.Load(this.DomainContext.GetDealerServiceInfoForDealerWithDetailsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {            
            this.DomainContext.Load(this.DomainContext.GetDealerServiceInfoForDealerWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                this.DealerKeyEmployees.Clear();
                this.DealerBusinessPermits.Clear();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                    if(entity.Dealer.DealerKeyEmployees != null)
                        foreach(var item in entity.Dealer.DealerKeyEmployees)
                            DealerKeyEmployees.Add(item);
                    if(entity.Dealer.DealerBusinessPermits != null)
                        foreach(var dealerBusinessPermits in entity.Dealer.DealerBusinessPermits)
                            DealerBusinessPermits.Add(dealerBusinessPermits);
                }
            }, null);
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void CreateUI() {
            this.LayoutRoot.Children.Add(DI.GetDetailPanel("DealerServiceInfoForDealer"));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(ChannelsUIStrings.DataEditView_Title_DealerBusinessPermitEdit, null, () => this.DealerServiceInfoBindServiceProductLineForEditDataGridView);
            detailDataEditView.Register(ChannelsUIStrings.DataManagementView_Title_DealerKeyEmployee, null, () => this.DealerKeyEmployeeEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 1);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            this.LayoutRoot.Children.Add(detailDataEditView);

        }
    }
}