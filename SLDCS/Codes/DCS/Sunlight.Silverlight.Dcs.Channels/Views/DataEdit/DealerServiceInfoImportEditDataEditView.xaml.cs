﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataEdit {
    public partial class DealerServiceInfoImportEditDataEditView {

        private ICommand exportFileCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出批量修改经销商服务经营权限模板.xlsx";
        private DataGridViewBase dealerBusinessPermitImport;

        private DataGridViewBase DealerBusinessPermitImport {
            get {
                return this.dealerBusinessPermitImport ?? (this.dealerBusinessPermitImport = DI.GetDataGridView("DealerBusinessPermitImport"));
            }
        }
        public DealerServiceInfoImportEditDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return ChannelsUIStrings.DataGridView_Title_DealerServiceInfo_BatchImport;
            }
        }

        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = ChannelsUIStrings.DataGridView_Title_DealerServiceInfo_BatchImportDetails,
                Content = this.DealerBusinessPermitImport
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.ImportEditDealerServiceInfoAsync(fileName);
            this.ExcelServiceClient.ImportEditDealerServiceInfoCompleted -= ExcelServiceClient_ImportEditDealerServiceInfoCompleted;
            this.ExcelServiceClient.ImportEditDealerServiceInfoCompleted += ExcelServiceClient_ImportEditDealerServiceInfoCompleted;
        }

        void ExcelServiceClient_ImportEditDealerServiceInfoCompleted(object sender, ImportEditDealerServiceInfoCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!this.HasImportingError) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Notification_CompletedImport, 5);
            } else {
                if(!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if(!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = ChannelsUIStrings.QueryPanel_QueryItem_DealerServiceInfo_Title_Dealer_Code,
                                                IsRequired = true
                                            },
                                             new ImportTemplateColumn {
                                                Name = ChannelsUIStrings.QueryPanel_QueryItem_DealerServiceInfo_Title_Dealer_Name
                                            },
                                            new ImportTemplateColumn {
                                                Name = ChannelsUIStrings.QueryPanel_QueryItem_Title_DealerPerTrainAut_PartsSalesCategoryId,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = ChannelsUIStrings.QueryPanel_QueryItem_Title_ServiceProductLine_Code,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = ChannelsUIStrings.QueryPanel_QueryItem_Title_ServiceProductLine_Name
                                            },new ImportTemplateColumn {
                                                Name = ChannelsUIStrings.QueryPanel_QueryItem_Title_ServiceProductLine_Type,
                                                IsRequired = true
                                            },new ImportTemplateColumn {
                                                Name = ChannelsUIStrings.DataGridView_ColumnItem_Title_LaborHourUnitPriceGrade_Name,
                                                IsRequired = true
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }
    }
}
