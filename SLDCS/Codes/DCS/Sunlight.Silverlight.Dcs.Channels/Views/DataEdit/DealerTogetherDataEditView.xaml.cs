﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Text.RegularExpressions;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Common.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataEdit {
    public partial class DealerTogetherDataEditView {
        private ObservableCollection<DealerServiceInfo> dealerServiceInfoes;
        private List<DealerServiceInfo> dealerServiceInfoesOriginal;
        private List<TiledRegion> tiledRegions;
        private CompanyInvoiceInfo companyInvoiceInfo;
        private DataGridViewBase dealerMobileNumberListForEditDataGridView;
        private bool i = true;

        private CompanyInvoiceInfo CompanyInvoiceInfo {
            get {
                return this.companyInvoiceInfo ?? (this.companyInvoiceInfo = new CompanyInvoiceInfo());
            }
            set {
                this.companyInvoiceInfo = value;
            }

        }

        private List<TiledRegion> TiledRegions {
            get {
                return this.tiledRegions ?? (this.tiledRegions = new List<TiledRegion>());
            }
        }

        public DealerTogetherDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += DealerTogetherDataEditView_DataContextChanged;
        }

        protected virtual void DealerTogetherDataEditView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            this.radTabControl.SelectedIndex = 0;
            var dealer = this.DataContext as Dealer;
            if(dealer == null)
                return;
            this.EnterpriseInvoicingInfoForDealer.SetValue(DataContextProperty, CompanyInvoiceInfo);
            dealer.PropertyChanged -= dealer_PropertyChanged;
            dealer.PropertyChanged += dealer_PropertyChanged;
        }

        void dealer_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var dealer = this.DataContext as Dealer;
            if(dealer == null)
                return;
            switch(e.PropertyName) {
                case "Company":
                    if(dealer.Company == null)
                        return;
                    dealer.Company.PropertyChanged -= Company_PropertyChanged;
                    dealer.Company.PropertyChanged += Company_PropertyChanged;
                    break;
                case "DealerServiceExt":
                    if(dealer.DealerServiceExt == null)
                        return;
                    dealer.DealerServiceExt.PropertyChanged -= DealerServiceExt_PropertyChanged;
                    dealer.DealerServiceExt.PropertyChanged += DealerServiceExt_PropertyChanged;
                    break;
            }
        }

        void DealerServiceExt_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var dealer = this.DataContext as Dealer;
            if(dealer == null)
                return;
            switch(e.PropertyName) {
                case "EmployeeNumber":
                    if(dealer.DealerServiceExt.EmployeeNumber < 0)
                        dealer.DealerServiceExt.EmployeeNumber = dealer.DealerServiceExt.EmployeeNumber * -1;
                    break;

            }
        }

        private void Company_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var dealer = this.DataContext as Dealer;
            if(dealer == null)
                return;
            switch(e.PropertyName) {
                case "RegisterCapital":
                    if(dealer.Company.RegisterCapital < 0)
                        dealer.Company.RegisterCapital = dealer.Company.RegisterCapital * -1;
                    break;
                case "DealerServiceExt":
                    break;
                case "BusinessAddressLongitude":
                    string str = (dealer.Company.BusinessAddressLongitude).ToString();
                    string[] sArray = str.Split('.');
                    if(sArray.Length > 1) {
                        if(sArray[1] != null && sArray[1].Length > 6) {
                            dealer.Company.ValidationErrors.Add(new ValidationResult(ChannelsUIStrings.DataEditView_Validation_Company_MaxInputSixPoint, new[] {
                    "BusinessAddressLongitude"
                                   
                }));
                        }
                    }
                    break;
                case "BusinessAddressLatitude":
                    string strs = (dealer.Company.BusinessAddressLongitude).ToString();
                    string[] sArrays = strs.Split('.');
                    if(sArrays.Length > 1) {
                        if(sArrays[1] != null && sArrays[1].Length > 6) {
                            dealer.Company.ValidationErrors.Add(new ValidationResult(ChannelsUIStrings.DataEditView_Validation_Company_MaxInputSixPoint, new[] {
                    "BusinessAddressLatitude"
                                   
                }));
                        }
                    }
                    break;
                case "ContactMobile":
                    if(dealer.Company.ContactMobile != null && dealer.Company.ContactMobile.Length != 11) {
                        dealer.Company.ValidationErrors.Add(new ValidationResult(ChannelsUIStrings.DataEditView_Validation_Company_EffectivePhone, new[] {
                    "ContactMobile"
                   
                    }));
                    }
                    break;
            }
        }

        public ObservableCollection<DealerServiceInfo> DealerServiceInfoes {
            get {
                return this.dealerServiceInfoes ?? (this.dealerServiceInfoes = new ObservableCollection<DealerServiceInfo>());
            }
        }

        public List<DealerServiceInfo> DealerServiceInfoesOriginal {
            get {
                return this.dealerServiceInfoesOriginal ?? (this.dealerServiceInfoesOriginal = new List<DealerServiceInfo>());
            }
        }

        private EnterpriseInformationForDealerDataEditPanel dataEditPanel;

        private EnterpriseInformationForDealerDataEditPanel DataEditPanel {
            get {
                return this.dataEditPanel ?? (this.dataEditPanel = (EnterpriseInformationForDealerDataEditPanel)DI.GetDataEditPanel("EnterpriseInformationForDealer"));
            }
        }

        private EnterpriseInvoicingInfoForDealerDataEditPanel enterpriseInvoicingInfoForDealer;

        private EnterpriseInvoicingInfoForDealerDataEditPanel EnterpriseInvoicingInfoForDealer {
            get {
                return this.enterpriseInvoicingInfoForDealer ?? (this.enterpriseInvoicingInfoForDealer = new EnterpriseInvoicingInfoForDealerDataEditPanel());
            }
        }

        private DataGridViewBase DealerMobileNumberListForEditDataGridView {
            get {
                if(this.dealerMobileNumberListForEditDataGridView == null) {
                    this.dealerMobileNumberListForEditDataGridView = DI.GetDataGridView("DealerMobileNumberListForEdit");
                    this.dealerMobileNumberListForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.dealerMobileNumberListForEditDataGridView;
            }
        }

        protected virtual void CreateUI() {
            var dealerPanel = DI.GetDataEditPanel("Dealer");
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(ChannelsUIStrings.DataEditView_GroupTitle_MobileNumberList, null, () => this.DealerMobileNumberListForEditDataGridView);
            this.Root.Children.Add(dealerPanel);
            this.gridEnterpriseInformation.Children.Add(this.DataEditPanel);
            this.gridTabItemDealerServiceExt.Children.Add(DI.GetDataEditPanel("DealerServiceExt"));
            this.gridTabItemEnterpriseInvoicingInfo.Children.Add(this.EnterpriseInvoicingInfoForDealer);
            //this.gridTabItemMobileNumberList.Children.Add(detailDataEditView);
        }

        private void SetTiledRegionId() {
            var dealer = this.DataContext as Dealer;
            if(dealer == null)
                return;
            var company = dealer.Company;
            if(company == null)
                return;
            this.DomainContext.Load(DomainContext.GetTiledRegionsQuery().Where(ex => ex.CountyName != null).OrderBy(ex => ex.ProvinceName), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var repairObject in loadOp.Entities)
                    this.TiledRegions.Add(new TiledRegion {
                        Id = repairObject.Id,
                        ProvinceName = repairObject.ProvinceName,
                        CityName = repairObject.CityName,
                        CountyName = repairObject.CountyName,
                    });
                this.DataEditPanel.KvProvinceNames.Clear();
                foreach(var item in TiledRegions) {
                    var values = this.DataEditPanel.KvProvinceNames.Select(e => e.Value);
                    if(values.Contains(item.ProvinceName))
                        continue;
                    this.DataEditPanel.KvProvinceNames.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.ProvinceName
                    });
                }
                company.ProvinceId = this.DataEditPanel.KvProvinceNames.First(e => e.Value == company.ProvinceName).Key;
                if(company.CityName == null)
                    return;
                var tempCityId = this.DataEditPanel.KvCityNames.FirstOrDefault(e => e.Value == company.CityName);
                if(tempCityId == null) {
                    UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_Company_CityNameHasChanged);
                    return;
                }
                company.CityId = tempCityId.Key;
                if(company.CountyName == null)
                    return;
                company.CountyId = this.DataEditPanel.KvCountyNames.First(e => e.Value == company.CountyName).Key;
            }, null);
        }

        protected virtual void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetDealersWithCompanyInvoiceInfoAndDealerServiceExtQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    if(entity.DealerServiceExt == null) {
                        entity.DealerServiceExt = new DealerServiceExt {
                            Status = (int)DcsMasterDataStatus.有效,
                            HasBranch = (int)DcsYesOrNo.无
                        };
                    }
                    if(entity.CompanyInvoiceInfo != null) {
                        CompanyInvoiceInfo = entity.CompanyInvoiceInfo.FirstOrDefault(r => r.InvoiceCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && r.Status == (int)DcsBaseDataStatus.有效);
                        CompanyInvoiceInfo.CompDocumentType = entity.Company.CompDocumentType;
                    }
                    this.DomainContext.Load(this.DomainContext.GetDealerMobileNumberListsQuery().Where(c => c.DealerId == entity.Id && c.VideoMobileNumber != "无"), LoadBehavior.RefreshCurrent, loadOp1 => {
                        if(loadOp1.HasError) {
                            if(!loadOp1.IsErrorHandled)
                                loadOp1.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                            return;
                        }
                        var details = loadOp1.Entities;
                        if(details != null) {
                            var i = 1;
                            foreach(var item in details) {
                                item.SerialNumber = i++;
                            }
                        }
                    }, null);
                    this.SetObjectToEdit(entity);
                    entity.Company.PropertyChanged -= Company_PropertyChanged;
                    entity.Company.PropertyChanged += Company_PropertyChanged;
                    if(entity.DealerServiceExt != null) {

                        entity.DealerServiceExt.PropertyChanged -= DealerServiceExt_PropertyChanged;
                        entity.DealerServiceExt.PropertyChanged += DealerServiceExt_PropertyChanged;
                    }
                    SetTiledRegionId();
                }
            }, null);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {
            if(!this.DealerMobileNumberListForEditDataGridView.CommitEdit())
                return;
            var dealer = this.DataContext as Dealer;
            if(dealer == null)
                return;
            if(string.IsNullOrEmpty(dealer.Code)) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_Dealer_CodeIsNotNull);
                return;
            }
            dealer.Code = dealer.Code.Trim();
            if(dealer.Code.Length > 11) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_Dealer_CodeLengthMoreThanEleven);
                return;
            }
            if(string.IsNullOrEmpty(dealer.Name)) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_Dealer_NameIsNotNull);
                return;
            }
            if(string.IsNullOrEmpty(dealer.ShortName)) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_Dealer_ShortNameIsNotNull);
                return;
            }
            if(!string.IsNullOrEmpty(dealer.Manager) && dealer.Manager.Length < 2) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_Dealer_ManagerLessThanTwo);
                return;
            }
            if(!string.IsNullOrEmpty(dealer.Company.LegalRepresentative) && dealer.Company.LegalRepresentative.Length < 2) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_Dealer_LegalRepresentativeLessThanTwo);
                return;
            }
            if(!string.IsNullOrEmpty(dealer.Company.ContactPostCode) && !Regex.IsMatch(dealer.Company.ContactPostCode, @"^\d{6}$", RegexOptions.IgnoreCase)) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_Dealer_EffiectiveContactPostCode);
                return;
            }

            dealer.Company.Code = dealer.Code;
            dealer.Company.Name = dealer.Name;
            if(this.EditState == DataEditState.New) {
                dealer.Company.Type = (int)DcsCompanyType.服务站;
            }
            if(!string.IsNullOrEmpty(dealer.Company.CustomerCode)) {
                dealer.Company.CustomerCode = dealer.Company.CustomerCode.Trim();
            }
            if(!string.IsNullOrEmpty(dealer.Company.SupplierCode)) {
                dealer.Company.SupplierCode = dealer.Company.SupplierCode.Trim();
            }
            if(dealer.CompanyInvoiceInfo.All(r => r.InvoiceCompanyId != BaseApp.Current.CurrentUserData.EnterpriseId)) {
                CompanyInvoiceInfo.InvoiceCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                CompanyInvoiceInfo.CompanyCode = dealer.Code;
                CompanyInvoiceInfo.CompanyName = dealer.Name;
                companyInvoiceInfo.Status = (int)DcsBaseDataStatus.有效;
                dealer.CompanyInvoiceInfo.Add(CompanyInvoiceInfo);
            } else {
                var companyInvoiceInfo = dealer.CompanyInvoiceInfo.First(r => r.InvoiceCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId);
                if(!string.IsNullOrEmpty(companyInvoiceInfo.Linkman) && companyInvoiceInfo.Linkman.Length < 2) {
                    UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_LinkmanLessThanTwo);
                    return;
                }
            }
            if((dealer.DealerMobileNumberLists.All(r => r.VideoMobileNumber != ChannelsUIStrings.DetailPanel_Title_No) && !dealer.DealerMobileNumberLists.Any()) && this.EditState == DataEditState.New) {
                var dealerMobileNumberList = new DealerMobileNumberList();
                dealerMobileNumberList.VideoMobileNumber = ChannelsUIStrings.DetailPanel_Title_No;
                dealer.DealerMobileNumberLists.Add(dealerMobileNumberList);
            }
            dealer.ValidationErrors.Clear();
            foreach(var entity in dealer.DealerServiceInfoes)
                entity.ValidationErrors.Clear();
            dealer.Company.ValidationErrors.Clear();
            //校验企业基本信息
            //if(string.IsNullOrEmpty(dealer.Company.CustomerCode)) {
            //    UIHelper.ShowNotification("请填写MDM客户编码");
            //    return;
            //}
            if(string.IsNullOrEmpty(dealer.Company.CityName) || string.IsNullOrEmpty(dealer.Company.ProvinceName) || string.IsNullOrEmpty(dealer.Company.CountyName)) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_Company_CityNameIsNull);
                return;
            }
            if(string.IsNullOrEmpty(dealer.Company.ContactPhone)) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_Dealer_PhoneIsNotNull);
                return;
            }
            //if(string.IsNullOrEmpty(dealer.Company.ContactAddress)) {
            if(string.IsNullOrEmpty(dealer.Company.BusinessAddress)) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_Dealer_BusinessAddressIsNotNull);
                return;
            }

            //if(!(dealer.Company.BusinessAddressLongitude).HasValue) {
            //    UIHelper.ShowNotification("请填写营业地址(经度)");
            //    return;
            //}
            //if(!(dealer.Company.BusinessAddressLatitude).HasValue) {
            //    UIHelper.ShowNotification("请填写营业地址(纬度)");
            //    return;
            //}

            //if(string.IsNullOrEmpty(dealer.Company.LegalRepresentative)) {
            //    UIHelper.ShowNotification("请填写法人代表");
            //    return;
            //}
            //if(string.IsNullOrEmpty(dealer.Company.ContactPerson)) {
            //    UIHelper.ShowNotification("请填写联系人");
            //    return;
            //}

            ////校验扩展信息
            //if(dealer.DealerServiceExt.DangerousRepairQualification == null) {
            //    UIHelper.ShowNotification("请选择扩展信息的危险品运输车辆维修许可证");
            //    return;
            //}
            //if(dealer.DealerServiceExt.GeographicPosition == default(int) || dealer.DealerServiceExt.GeographicPosition == null) {
            //    UIHelper.ShowNotification("请选择扩展信息的地理位置");
            //    return;
            //}
            //if(dealer.DealerServiceExt.VehicleTravelRoute == default(int) || dealer.DealerServiceExt.VehicleTravelRoute == null) {
            //    UIHelper.ShowNotification("请选择扩展信息的车辆行驶路线");
            //    return;
            //}
            //if(dealer.DealerServiceExt.VehicleDockingStation == default(int) || dealer.DealerServiceExt.VehicleDockingStation == null) {
            //    UIHelper.ShowNotification("请选择扩展信息的车辆停靠点");
            //    return;
            //}
            //if(dealer.DealerServiceExt.RepairQualification == default(int) || dealer.DealerServiceExt.RepairQualification == null) {
            //    UIHelper.ShowNotification("请选择扩展信息的维修资质");
            //    return;
            //}
            //if(dealer.DealerServiceExt.HasBranch == null) {
            //    UIHelper.ShowNotification("请选择扩展信息的有无二级服务站");
            //    return;
            //}


            //校验企业开票信息
            //if(dealer.CompanyInvoiceInfo != null) {
            //    var companyInvoiceInfoForCheck = dealer.CompanyInvoiceInfo.First(r => r.InvoiceCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId);
            //    if(string.IsNullOrEmpty(companyInvoiceInfoForCheck.TaxRegisteredNumber)) {
            //        UIHelper.ShowNotification("请选择企业开票信息的税务登记号");
            //        return;
            //    }
            //    if(string.IsNullOrEmpty(companyInvoiceInfoForCheck.InvoiceTitle)) {
            //        UIHelper.ShowNotification("请填写企业开票信息的开票名称");
            //        return;
            //    }
            //    if(companyInvoiceInfoForCheck.InvoiceType == default(int)) {
            //        UIHelper.ShowNotification("请选择企业开票信息的开票类型");
            //        return;
            //    }
            //    if(string.IsNullOrEmpty(companyInvoiceInfoForCheck.BankName)) {
            //        UIHelper.ShowNotification("请填写企业开票信息的银行名称");
            //        return;
            //    }
            //    if(string.IsNullOrEmpty(companyInvoiceInfoForCheck.BankAccount)) {
            //        UIHelper.ShowNotification("请填写企业开票信息的银行账户");
            //        return;
            //    }
            //    if(string.IsNullOrEmpty(companyInvoiceInfoForCheck.ContactNumber)) {
            //        UIHelper.ShowNotification("请填写企业开票信息的财务联系人电话");
            //        return;
            //    }
            //    if(string.IsNullOrEmpty(companyInvoiceInfoForCheck.ContactNumber)) {
            //        UIHelper.ShowNotification("请填写企业开票信息的税务登记地址");
            //        return;
            //    }
            //}

            if(dealer.ValidationErrors.Any() || dealer.DealerServiceInfoes.Any(r => r.HasValidationErrors) || dealer.Company.ValidationErrors.Any())
                return;
            ((IEditableObject)dealer).EndEdit();
            try {
                if(this.EditState == DataEditState.New) {
                    if(i) {
                        i = false;
                        this.DomainContext.生成经销商基本信息集中(dealer.Company, dealer, dealer.DealerServiceExt, dealer.CompanyInvoiceInfo.First(r => r.InvoiceCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), dealer.DealerMobileNumberLists.ToArray(), invokeOp => {
                            if(invokeOp.HasError) {
                                if(!invokeOp.IsErrorHandled)
                                    invokeOp.MarkErrorAsHandled();
                                var error = invokeOp.ValidationErrors.FirstOrDefault();
                                if(error != null) {
                                    UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage, 5);
                                } else {
                                    DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                                }
                                DomainContext.RejectChanges();
                                i = true;
                                return;
                            }
                            this.NotifyEditSubmitted();
                            this.OnCustomEditSubmitted();
                        }, null);
                    }
                } else {
                    this.DomainContext.修改经销商基本信息集中(dealer.Company, dealer, dealer.DealerServiceExt, dealer.CompanyInvoiceInfo.First(r => r.InvoiceCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), dealer.DealerMobileNumberLists.ToArray(), invokeOp => {
                        if(invokeOp.HasError) {
                            if(!invokeOp.IsErrorHandled)
                                invokeOp.MarkErrorAsHandled();
                            var error = invokeOp.ValidationErrors.FirstOrDefault();
                            if(error != null) {
                                UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage, 5);
                            } else {
                                DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                            }
                            DomainContext.RejectChanges();
                            return;
                        }
                        this.NotifyEditSubmitted();
                        this.OnCustomEditSubmitted();
                    }, null);

                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
        }

        protected override void Reset() {
            this.DealerServiceInfoes.Clear();
            //this.DealerMobileNumberLists.Clear();
            this.DealerServiceInfoesOriginal.Clear();
            var dealer = this.DataContext as Dealer;
            if(dealer != null && (dealer.Company != null && this.DomainContext.Companies.Contains(dealer.Company))) {
                this.DomainContext.Companies.Detach(dealer.Company);
            }
            i = true;
            //if(dealer != null && (dealer.DealerMobileNumberLists != null && this.DomainContext.DealerMobileNumberLists.Contains(dealer.DealerMobileNumberLists))) {
            //    this.DomainContext.DealerMobileNumberLists.Detach(dealer.DealerMobileNumberLists);
            //}
            if(dealer != null && (dealer.DealerMobileNumberLists != null)) {
                foreach(var rate in dealer.DealerMobileNumberLists.Where(rate => this.DomainContext.DealerMobileNumberLists.Contains(rate)))
                    this.DomainContext.DealerMobileNumberLists.Detach(rate);
            }
            CompanyInvoiceInfo = null;
            if(this.DomainContext.Dealers.Contains(dealer))
                this.DomainContext.Dealers.Detach(dealer);


        }

        protected override string Title {
            get {
                return this.EditState == DataEditState.Edit ? ChannelsUIStrings.DataEditView_Title_DealerTogetherEdit : ChannelsUIStrings.DataEditView_Title_DealerTogetherAdd;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public void OnCustomEditSubmitted() {
            this.DealerServiceInfoes.Clear();
            i = true;
            this.DealerServiceInfoesOriginal.Clear();
            var dealer = this.DataContext as Dealer;
            if(dealer != null && (dealer.Company != null && this.DomainContext.Companies.Contains(dealer.Company))) {
                this.DomainContext.Companies.Detach(dealer.Company);
            }
            if(dealer != null && (dealer.DealerMobileNumberLists != null)) {
                foreach(var rate in dealer.DealerMobileNumberLists.Where(rate => this.DomainContext.DealerMobileNumberLists.Contains(rate)))
                    this.DomainContext.DealerMobileNumberLists.Detach(rate);
            }
            CompanyInvoiceInfo = null;
            if(this.DomainContext.Dealers.Contains(dealer))
                this.DomainContext.Dealers.Detach(dealer);
        }

        public new event EventHandler EditSubmitted;
        private void NotifyEditSubmitted() {
            var handler = this.EditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }
    }
}
