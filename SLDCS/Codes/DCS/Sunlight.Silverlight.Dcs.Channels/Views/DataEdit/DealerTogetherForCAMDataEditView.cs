﻿
namespace Sunlight.Silverlight.Dcs.Channels.Views.DataEdit {
    public class DealerTogetherForCAMDataEditView : DealerTogetherDataEditView {
        protected override void CreateUI() {
            var dealerPanel = DI.GetDataEditPanel("Dealer");
            this.Root.Children.Add(dealerPanel);
            this.gridEnterpriseInformation.Children.Add(DI.GetDataEditPanel("EnterpriseInformation"));
            this.radTabItemDealerServiceExt.Content = DI.GetDataEditPanel("DealerServiceExt");
        }
    }
}
