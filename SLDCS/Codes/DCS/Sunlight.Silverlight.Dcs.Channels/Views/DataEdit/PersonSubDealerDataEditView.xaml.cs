﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
namespace Sunlight.Silverlight.Dcs.Channels.Views.DataEdit {
    public partial class PersonSubDealerDataEditView : INotifyPropertyChanged {
        private DataGridViewBase personSubDealerForEditDataGridView;
        private ObservableCollection<PersonSubDealer> personSubDealers;
        public event PropertyChangedEventHandler PropertyChanged;
        private int subDealerId;
        private string subDealerName;
        public string SubDealerName {
            get {
                return this.subDealerName;
            }
            set {
                this.subDealerName = value;
                this.OnPropertyChanged("SubDealerName");
            }
        }

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public PersonSubDealerDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreatUI);
        }

        public DataGridViewBase PersonSubDealerForEditDataGridView {
            get {
                if(this.personSubDealerForEditDataGridView == null) {
                    this.personSubDealerForEditDataGridView = DI.GetDataGridView("PersonSubDealerForEdit");
                    this.personSubDealerForEditDataGridView.DataContext = this;
                }
                return this.personSubDealerForEditDataGridView;
            }
        }

        public ObservableCollection<PersonSubDealer> PersonSubDealers {
            get {
                if(this.personSubDealers == null) {
                    this.personSubDealers = new ObservableCollection<PersonSubDealer>();
                }
                return personSubDealers;
            }
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PersonSubDealer;
            }
        }

        private void CreatUI() {
            var subQueryWindow = DI.GetQueryWindow("SubDealer");
            subQueryWindow.SelectionDecided += subQueryWindow_SelectionDecided;
            this.ptbSubDealerName.PopupContent = subQueryWindow;

            this.LayoutRoot.Children.Add(this.CreateVerticalLine(2));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(ChannelsUIStrings.DataEditView_Title_PersonSubDealerDataEditView, null, () => {
                var view = this.PersonSubDealerForEditDataGridView;
                view.DomainContext = this.DomainContext;
                return view;
            });
            detailDataEditView.SetValue(Grid.ColumnProperty, 3);
            this.LayoutRoot.Children.Add(detailDataEditView);
        }

        private void subQueryWindow_SelectionDecided(object sender, System.EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var subDealer = queryWindow.SelectedEntities.Cast<SubDealer>().FirstOrDefault();
            if(subDealer == null)
                return;
            subDealerId = subDealer.Id;
            SubDealerName = subDealer.Name;

            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        protected override void Reset() {
            subDealerId = -1;
            this.PersonSubDealers.Clear();
            SubDealerName = null;
        }

        protected override void OnEditSubmitting() {
            if(!this.personSubDealerForEditDataGridView.CommitEdit())
                return;
            foreach(var entity in this.PersonSubDealers)
                entity.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(SubDealerName)) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Validation_PersonSubDealer_SubDealerIsNull);
                return;
            }
            foreach(var entity in this.PersonSubDealers) {
                entity.CompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                entity.SubDealerId = subDealerId;
                ((IEditableObject)entity).EndEdit();
                try {
                    if(this.EditState == DataEditState.Edit)
                        if(entity.Can修改登陆人员与二级站关系)
                            entity.修改登陆人员与二级站关系();
                } catch(ValidationException ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                    return;
                }
            }
            foreach(var item in this.DomainContext.PersonSubDealers.Where(e => e.EntityState == EntityState.New).ToList()) {
                if(!PersonSubDealers.Contains(item))
                    this.DomainContext.PersonSubDealers.Remove(item);
            }
            if(this.PersonSubDealers.Any(r => r.ValidationErrors.Any()))
                return;

            base.OnEditSubmitting();
        }

        protected override void OnEditSubmitted() {
            SubDealerName = null;
            PersonSubDealers.Clear();
        }

        private void LoadEntityToEdit(int id) {
            PersonSubDealers.Clear();
            this.DomainContext.Load(this.DomainContext.GetPersonSubDealerWithDetailQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                entity.Personnel.SequeueNumber = 1;
                SubDealerName = entity.SubDealer.Name;
                subDealerId = entity.SubDealerId;
                PersonSubDealers.Add(entity);
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }
    }
}
