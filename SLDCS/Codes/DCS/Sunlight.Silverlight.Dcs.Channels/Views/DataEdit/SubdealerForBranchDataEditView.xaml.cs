﻿
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataEdit {
    public partial class SubdealerForBranchDataEditView {
        public SubdealerForBranchDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData();
        }

        private void CreateUI() {
            this.isConent = true;
            //选择服务站
            var dealerQueryWindow = DI.GetQueryWindow("DealerServiceInfo");
            this.ptbDealerName.PopupContent = dealerQueryWindow;
            dealerQueryWindow.SelectionDecided += dealerQueryWindow_SelectionDecided;
        }

        private readonly string[] kvNames = { "SubDealerType" };

        public object SubDealerTypes {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        private void dealerQueryWindow_SelectionDecided(object sender, System.EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var dealerServiceInfo = queryWindow.SelectedEntities.Cast<DealerServiceInfo>().FirstOrDefault();
            if(dealerServiceInfo == null)
                return;
            var subDealer = this.DataContext as SubDealer;
            if(subDealer == null)
                return;
            subDealer.DealerId = dealerServiceInfo.DealerId;
            this.DomainContext.Load(this.DomainContext.GetDealersQuery().Where(ex => ex.Id == subDealer.DealerId), loadOp => {
                if(loadOp.HasError)
                    return;
                var item = loadOp.Entities.SingleOrDefault();
                if(item != null) {
                    subDealer.DealerCode = item.Code;
                    subDealer.DealerName = item.Name;
                }
            }, null);
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private KeyValueManager keyValueManager;

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_SubDealer;
            }
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        protected override void OnEditSubmitting() {
            var subDealer = this.DataContext as SubDealer;
            if(subDealer == null)
                return;
            subDealer.ValidationErrors.Clear();
            if(subDealer.Code == null)
                subDealer.ValidationErrors.Add(new ValidationResult(ChannelsUIStrings.DataEditView_Validation_SubDealer_CodeIsNull, new[]{
                    "Code"
                }));
            if(subDealer.Name == null)
                subDealer.ValidationErrors.Add(new ValidationResult(ChannelsUIStrings.DataEditView_Validation_SubDealer_NameIsNull, new[]{
                    "Name"
                }));
            if(subDealer.DealerCode == null)
                subDealer.ValidationErrors.Add(new ValidationResult(ChannelsUIStrings.DataEditView_Validation_Dealer_CodeIsNull, new[]{
                    "DealerCode"
                }));
            if(subDealer.DealerName == null)
                subDealer.ValidationErrors.Add(new ValidationResult(ChannelsUIStrings.DataEditView_Validation_Dealer_NameIsNull, new[]{
                    "DealerName"
                }));
            ((IEditableObject)subDealer).EndEdit();
            if(subDealer.HasValidationErrors)
                return;
            try {
                if(this.EditState == DataEditState.Edit)
                    if(subDealer.Can修改二级站信息)
                        subDealer.修改二级站信息();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetSubDealersQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.DomainContext.Load(this.DomainContext.GetDealersQuery().Where(t => t.Id == entity.DealerId), LoadBehavior.RefreshCurrent, loadOp1 => {
                        if(loadOp1.HasError) {
                            if(!loadOp1.IsErrorHandled)
                                loadOp1.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                            return;
                        }
                        var dealer = loadOp1.Entities.SingleOrDefault();
                        if(dealer != null) {
                            entity.DealerName = dealer.Name;
                            entity.DealerCode = dealer.Code;
                        }
                        this.SetObjectToEdit(entity);
                    }, null);

                }
            }, null);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
