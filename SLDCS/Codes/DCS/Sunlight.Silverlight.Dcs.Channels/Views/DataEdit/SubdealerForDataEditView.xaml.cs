﻿
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Core.Model;
using System.Collections.ObjectModel;
namespace Sunlight.Silverlight.Dcs.Channels.Views.DataEdit {
    public partial class SubdealerForDataEditView {
        public SubdealerForDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData();
        }
        private void CreateUI() {
            this.isConent = true;
        }
        private readonly string[] kvNames = {
            "SubDealerType"
        };

        public object SubDealerTypes {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        private KeyValueManager keyValueManager;
        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_SubDealer;
            }
        }
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        protected override void OnEditSubmitting() {
            var subDealer = this.DataContext as SubDealer;
            if(subDealer == null)
                return;
            subDealer.ValidationErrors.Clear();
            if(subDealer.Code == null)
                subDealer.ValidationErrors.Add(new ValidationResult(ChannelsUIStrings.DataEditView_Validation_SubDealer_CodeIsNull, new[]{
                    "Code"
                }));
            if(subDealer.Name == null)
                subDealer.ValidationErrors.Add(new ValidationResult(ChannelsUIStrings.DataEditView_Validation_SubDealer_NameIsNull, new[]{
                    "Name"
                }));
            ((IEditableObject)subDealer).EndEdit();

            if(subDealer.HasValidationErrors)
                return;
            try {
                if(this.EditState == DataEditState.Edit)
                    if(subDealer.Can修改二级站信息)
                        subDealer.修改二级站信息();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetSubDealersQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
