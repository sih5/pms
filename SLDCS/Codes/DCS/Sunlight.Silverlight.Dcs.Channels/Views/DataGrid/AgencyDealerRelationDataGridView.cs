﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class AgencyDealerRelationDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
             "BaseData_Status"
        };

        public AgencyDealerRelationDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(AgencyDealerRelation);
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "AgencyDealerRelationHistory"
                    }
                };
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Company.Code",
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_Agency_Code
                    }, new ColumnItem {
                        Name = "Company.Name",
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_Agency_Name
                    }, new ColumnItem {
                        Name = "Company1.Code",
                        Title = ChannelsUIStrings.DataEditView_Title_DealerServiceInfo_DealerCode
                    }, new ColumnItem {
                        Name = "Company1.Name",
                        Title = ChannelsUIStrings.DataEditView_Title_DealerServiceInfo_DealerName
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime"
                    }, new ColumnItem {
                        Name = "Branch.Name",
                        IsSortDescending = false
                    },new ColumnItem{
                        Name="PartsSalesCategory.Name",
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }

        protected override string OnRequestQueryName() {
            return "GetAgencyDealerRelationsWithBranch";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filterItem = this.FilterItem as CompositeFilterItem;
            if(filterItem == null)
                return null;
            switch(parameterName) {
                case "salesRegionName":
                    var salesRegionNameFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "SalesRegionName");
                    return salesRegionNameFilterItem == null ? null : salesRegionNameFilterItem.Value;
                case "marketingDepartmentName":
                    var departmentNameFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "MarketingDepartmentName");
                    return departmentNameFilterItem == null ? null : departmentNameFilterItem.Value;
                default:
                    return null;
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                var param = new[] { "SalesRegionName", "MarketingDepartmentName" };
                foreach(var item in compositeFilterItem.Filters.Where(e => !param.Contains(e.MemberName))) {
                    newCompositeFilterItem.Filters.Add(item);
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }
}
