﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Telerik.Windows.Controls.GridView;
namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class AgencyDealerRelationForImportDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new ColumnItem {
                           Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_Agency_Code,
                        Name = "AgencyCode"
                      },new ColumnItem {
                         Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_Agency_Name,
                        Name = "AgencyName"
                      },new ColumnItem {
                         Title = Utils.GetEntityLocalizedName(typeof(AgencyDealerRelation), "DealerCode"),
                        Name = "DealerCode"
                      },new ColumnItem {
                         Title = Utils.GetEntityLocalizedName(typeof(AgencyDealerRelation), "DealerName"),
                        Name = "DealerName"
                      },new ColumnItem {
                         Title = Utils.GetEntityLocalizedName(typeof(AgencyDealerRelation), "Remark"),
                        Name = "Remark"
                      }, new ColumnItem {
                         Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_BranchName,
                        Name = "BranchNameStr"
                      },   new ColumnItem {
                         Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategoryName,
                        Name = "PartsSalesOrderTypeNameStr"
                      }
                   
                };
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(AgencyDealerRelationExtend);
            }
        }
    }
}
