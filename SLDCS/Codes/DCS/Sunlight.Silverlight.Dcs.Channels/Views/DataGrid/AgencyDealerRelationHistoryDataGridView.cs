﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Dcs.Channels.Resources;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class AgencyDealerRelationHistoryDataGridView : DcsDataGridViewBase {
        int AgencyDealerRelationId = default(int);
        private readonly string[] kvNames = {
             "BaseData_Status"
        };

        public AgencyDealerRelationHistoryDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData();
            this.DataContextChanged += AgencyDealerRelationHistoryDataGridView_DataContextChanged;
        }

        private void AgencyDealerRelationHistoryDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var agencyDealerRelation = e.NewValue as AgencyDealerRelation;
            if(agencyDealerRelation == null)
                return;
            AgencyDealerRelationId = agencyDealerRelation.Id;
            this.ExecuteQueryDelayed();
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                       Name = "AgencyCode",
                       Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_Agency_Code
                    }, new ColumnItem {
                       Name = "AgencyName",
                       Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_Agency_Name
                    }, new ColumnItem {
                       Name = "DealerCode",
                       Title=ChannelsUIStrings.DataEditView_Title_DealerServiceInfo_DealerCode
                    }, new ColumnItem {
                        Name = "DealerName",
                       Title= ChannelsUIStrings.DataEditView_Title_DealerServiceInfo_DealerName
                    }, new KeyValuesColumnItem {
                       Name = "Status",
                       KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                       //Title="状态"
                    }, new ColumnItem {
                       Name = "CreatorName",
                       //Title="创建人"
                    }, new ColumnItem {
                       Name = "CreateTime",
                       //Title="创建时间"
                    },new ColumnItem {
                       Name = "Remark",
                       //Title="备注"
                    }, new ColumnItem {
                       Name = "BranchName",
                       Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_BranchName
                    },new ColumnItem{
                       Name="PartsSalesCategoryName",
                       Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategoryName
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetHistoriesByAgencyDealerRelationId";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            switch(parameterName) {
                case "agencyDealerRelationId":
                    return AgencyDealerRelationId;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
        }

        protected override Type EntityType {
            get {
                return typeof(AgencyDealerRelationHistoryEx);
            }
        }
    }
}
