﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Controls;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class BatchBindingTelForImportDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                new ColumnItem {
                        Name = "DealerCode",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_DealerCode
                    },new ColumnItem {
                        Name = "DealerName",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_DealerName
                    },new ColumnItem {
                        Name = "VideoMobileNumber",
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerMobileNumberList_VideoMobileNumber
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerMobileNumberList);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.Columns.Insert(0, new GridViewSerialNumberColumn {
                Header = ChannelsUIStrings.DataGridView_ColumnItem_Title_Personnel_SequeueNumber
            });
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("DealerMobileNumberLists");
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }
    }
}
