﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class ChannelCapabilityDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
             "MasterData_Status"
        };

        public ChannelCapabilityDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(ChannelCapability);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Name"
                    }, new ColumnItem {
                        Name = "ChannelGrade"
                    }, new ColumnItem {
                        Name = "Description"
                    }, new ColumnItem {
                        Name = "IsSale",
                        Title = ChannelsUIStrings.DataGridView_Text_ChannelCapability_IsSale
                    }, new ColumnItem {
                        Name = "IsPart",
                        Title = ChannelsUIStrings.DataGridView_Text_ChannelCapability_IsPart
                    }, new ColumnItem {
                        Name = "IsService",
                        Title = ChannelsUIStrings.DataGridView_Text_ChannelCapability_IsService
                    }, new ColumnItem {
                        Name = "IsSurvey",
                        Title = ChannelsUIStrings.DataGridView_Text_ChannelCapability_IsSurvey
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime"
                    }
                };
            }
        }


        protected override string OnRequestQueryName() {
            return "GetChannelCapabilities";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
        }
    }
}
