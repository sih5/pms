﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;
using FilterOperator = Sunlight.Silverlight.Core.Model.FilterOperator;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class DealerBusinessPermitDataGridView : DcsDataGridViewBase {

        protected override Type EntityType {
            get {
                return typeof(DealerBusinessPermit);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Dealer.Code"
                    }, new ColumnItem {
                        Name = "Dealer.Name"
                    }, new ColumnItem {
                        Name = "ServiceProductLine.Code"
                    }, new ColumnItem {
                        Name = "ServiceProductLine.Name"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "Branch.Name"
                    }, new ColumnItem {
                        Name = "ServiceProductLine.PartsSalesCategory.Name",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerBusinessPermit_PartsSalesCategory_Name
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetDealerBusinessPermitWithDetail";
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            if(FilterItem == null)
                return base.OnRequestFilterDescriptor(queryName);
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem == null) {
                compositeFilterItem = new CompositeFilterItem();
                compositeFilterItem.Filters.Add(this.FilterItem);
            }
            compositeFilterItem.Filters.Add(new FilterItem {
                IsExact = true,
                MemberName = "BranchId",
                Operator = FilterOperator.IsEqualTo,
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            return compositeFilterItem.ToFilterDescriptor();
        }
    }
}
