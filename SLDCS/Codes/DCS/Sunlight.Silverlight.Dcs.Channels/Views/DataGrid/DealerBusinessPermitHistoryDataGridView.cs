﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class DealerBusinessPermitHistoryDataGridView : DcsDataGridViewBase {

        public DealerBusinessPermitHistoryDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        private readonly string[] kvNames = {
            "ServiceProductLineView_ProductLineType"
        };

        protected override Type EntityType {
            get {
                return typeof(DealerBusinessPermitHistory);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Dealer.Code",
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_DealerCode
                    }, new ColumnItem {
                        Name = "Dealer.Name",
                        Title =  ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_DealerName
                    },  new ColumnItem {
                        Name = "ServiceProductLineCode",
                        Title =  ChannelsUIStrings.DataGridView_ColumnItem_Title_ServiceProductLine_Code
                    }, new ColumnItem {
                        Name = "ServiceProductLineName",
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_ServiceProductLine_Name
                    }, new KeyValuesColumnItem {
                        Name = "ProductLineType",
                        Title =ChannelsUIStrings.DataGridView_ColumnItem_Title_ServiceProductLine_Type,
                        IsReadOnly = true,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
        protected override string OnRequestQueryName() {
            return "GetDealerBusinessPermitHistoryQuery";
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.ShowGroupPanel = false;
        }

    }
}
