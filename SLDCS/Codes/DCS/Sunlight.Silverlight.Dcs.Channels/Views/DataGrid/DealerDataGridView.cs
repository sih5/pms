﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.Channels.Resources;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class DealerDataGridView : DcsDataGridViewBase {

        private readonly string[] kvNames = {
             "MasterData_Status","Customer_IdDocumentType","PartsSupplier_TaxpayerKind","Invoice_Type","Repair_Qualification_Grade","YesOrNo","DealerServiceExt_VehicleTravelRoute","Company_CityLevel","CompanyDocumentType","Corporate_Nature"
        };

        public DealerDataGridView() {
            this.KeyValueManager.Register(this.kvNames);

        }

        protected override Type EntityType {
            get {
                return typeof(Dealer);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, 
                    new ColumnItem {   
                        Name = "Code",
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_Code,
                        IsSortDescending = false
                    },new ColumnItem {
                        Name = "Company.Name",
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_Name,
                    }, new ColumnItem {
                        Name = "ShortName"
                    },new ColumnItem{
                        Name="Manager"
                    }
                    //,new ColumnItem{
                    //    Name="Company.CustomerCode",
                    //    Title="MDM客户编码"
                    //},
                    //new ColumnItem{
                    //    Name="Company.SupplierCode",
                    //    Title="MDM供应商编码"
                    //},
                    //new ColumnItem {
                    //    Name = "Company.FoundDate"
                    //}
                    ,new ColumnItem {
                        Name = "Company.ProvinceName"
                    }, new ColumnItem {
                        Name = "Company.CityName"
                    }, new ColumnItem {
                        Name = "Company.CountyName"
                    }, new KeyValuesColumnItem{
                        Name="Company.CityLevel",
                        KeyValueItems=this.KeyValueManager[this.kvNames[7]]
                    },new ColumnItem{
                        Name="Company.ContactPerson"
                    },new ColumnItem{
                        Name="Company.ContactPhone"
                    },new ColumnItem{
                        Name="Company.BusinessLinkName",
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_BusinessLinkName
                    },new ColumnItem{
                        Name="Company.BusinessContactMethod",
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_BusinessContactMethod
                    },new ColumnItem{
                        Name="Company.ContactMail"
                    },new ColumnItem{
                        Name="Company.ContactMobile",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_ContactMobile
                    },new ColumnItem{
                        Name="Company.Fax"
                    },new ColumnItem{
                        Name="Company.ContactPostCode"
                    },new ColumnItem{
                        Name="Company.BusinessAddress"
                    }, new ColumnItem {
                        Name = "Company.BusinessAddressLongitude",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_BusinessAddressLongitude
                    }, new ColumnItem {
                        Name = "Company.BusinessAddressLatitude",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_BusinessAddressLatitude
                    }, new ColumnItem {
                        Name = "Company.Distance",
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_Distance
                    }, new ColumnItem {
                        Name = "Company.MinServiceCode",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_MinServiceCode
                    }, new ColumnItem {
                        Name = "Company.MinServiceName",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_MinServiceName
                    },new ColumnItem{
                        Name="Company.RegisterCode"
                    },new ColumnItem{
                        Name="Company.RegisterName"
                    },new ColumnItem{
                        Name="Company.RegisterDate"
                    },new ColumnItem{
                        Name="Company.RegisterCapital"
                    },new ColumnItem{
                        Name="Company.FixedAsset",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_FixedAsset
                    },
                    new ColumnItem {
                        Name = "Company.LegalRepresentative",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_LegalRepresentative
                    },  new ColumnItem {
                        Name = "Company.LegalRepresentTel"
                    },new KeyValuesColumnItem{
                        Name="Company.CorporateNature",
                        KeyValueItems=this.KeyValueManager[this.kvNames[9]]
                    }, new ColumnItem {
                        Name = "Company.BusinessScope"
                    },new ColumnItem{
                        Name="Company.RegisteredAddress",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_RegisteredAddress
                    },new KeyValuesColumnItem{
                        Name="Company.IdDocumentType",
                        KeyValueItems=this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem{
                        Name="Company.IdDocumentNumber"
                    },new KeyValuesColumnItem{
                        Name="DealerServiceExt.RepairQualification",
                        KeyValueItems=this.KeyValueManager[this.kvNames[4]]
                    },new ColumnItem{
                        Name="DealerServiceExt.OwnerCompany"
                    },new ColumnItem{
                        Name="DealerServiceExt.BuildTime"
                    },new ColumnItem{
                        Name="DealerServiceExt.TrafficRestrictionsdescribe"
                    },new ColumnItem{
                        Name="DealerServiceExt.MainBusinessAreas"
                    },new ColumnItem{
                        Name="DealerServiceExt.AndBusinessAreas"
                    },
                    //new ColumnItem{
                    //    Name="DealerServiceExt.BrandScope",
                    //    Title="红岩授权品牌"
                    //},new ColumnItem{
                    //    Name="DealerServiceExt.CompetitiveBrandScope",
                    //    Title="非红岩授权品牌"
                    //},
                    new KeyValuesColumnItem{
                        Name="DealerServiceExt.DangerousRepairQualification",
                        KeyValueItems=this.KeyValueManager[this.kvNames[5]]
                    },new KeyValuesColumnItem{
                        Name="DealerServiceExt.HasBranch",
                        KeyValueItems=this.KeyValueManager[this.kvNames[5]]
                    },new KeyValuesColumnItem{
                        Name="DealerServiceExt.VehicleTravelRoute",
                        KeyValueItems=this.KeyValueManager[this.kvNames[6]]
                    },new ColumnItem{
                        Name="DealerServiceExt.VehicleUseSpeciality"
                    },new ColumnItem{
                        Name="DealerServiceExt.VehicleDockingStation",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_VehicleDockingStation
                    },new ColumnItem{
                        Name="DealerServiceExt.EmployeeNumber",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_EmployeeNumber
                    },new ColumnItem{
                        Name="DealerServiceExt.RepairingArea",
                        
                    },new ColumnItem{
                        Name="DealerServiceExt.ReceptionRoomArea"
                    },new ColumnItem{
                        Name="DealerServiceExt.ParkingArea"
                    },new ColumnItem{
                        Name="DealerServiceExt.PartWarehouseArea"
                    },
                    new KeyValuesColumnItem{
                        Name="Company.CompDocumentType",
                        KeyValueItems=this.KeyValueManager[this.kvNames[8]],
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_CompDocumentType
                    },
                    //new ColumnItem{
                    //    Name="Company.CompanyInvoiceInfo.TaxRegisteredNumber"
                    //},
                    //new KeyValuesColumnItem{
                    //    Name="Company.CompanyInvoiceInfo.TaxpayerQualification",
                    //    KeyValueItems=this.KeyValueManager[this.kvNames[2]]
                    //},new ColumnItem{
                    //    Name="Company.CompanyInvoiceInfo.InvoiceAmountQuota"
                    //},new ColumnItem{
                    //    Name="Company.CompanyInvoiceInfo.InvoiceTitle"
                    //},new KeyValuesColumnItem{
                    //    Name="Company.CompanyInvoiceInfo.InvoiceType",
                    //    KeyValueItems=this.KeyValueManager[this.kvNames[3]]
                    //},new  ColumnItem{
                    //    Name="Company.CompanyInvoiceInfo.BankName"
                    //},new ColumnItem{
                    //    Name="Company.CompanyInvoiceInfo.InvoiceTax"
                    //},new ColumnItem{
                    //    Name="Company.CompanyInvoiceInfo.BankAccount"
                    //},new ColumnItem{
                    //    Name="Company.CompanyInvoiceInfo.Linkman"
                    //},new ColumnItem{
                    //    Name="Company.CompanyInvoiceInfo.ContactNumber"
                    //},new ColumnItem{
                    //    Name="Company.CompanyInvoiceInfo.Fax"
                    //},new ColumnItem{
                    //    Name="Company.CompanyInvoiceInfo.TaxRegisteredAddress"
                    //},
                    new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }
                };
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            CompanyInvoiceInfo c = new CompanyInvoiceInfo();




        }
        protected override string OnRequestQueryName() {
            return "GetDealersWithCompanyInvoiceInfoAndDealerServiceExt";
        }

    }
}
