﻿
using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.Channels.Resources;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class DealerForDealerDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
             "MasterData_Status"
        };

        public DealerForDealerDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(Dealer);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },
                    new ColumnItem {
                        Name = "Code",
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_Code,
                        IsSortDescending = false
                    }, new ColumnItem {
                        Name = "Company.Name",
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_Name,
                    }, new ColumnItem {
                        Name = "ShortName"
                    }, new ColumnItem {
                        Name = "Company.FoundDate"
                    },new ColumnItem {
                        Name = "Company.ProvinceName"
                    }, new ColumnItem {
                        Name = "Company.CityName"
                    }, new ColumnItem {
                        Name = "Company.CountyName"
                    },  new ColumnItem {
                        Name = "Company.LegalRepresentative"
                    }, new ColumnItem {
                        Name = "Company.RegisterCapital"
                    }, new ColumnItem {
                        Name = "Company.BusinessScope"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }
                };
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
        protected override string OnRequestQueryName() {
            return "GetDealersWithCompany";
        }
    }
}
