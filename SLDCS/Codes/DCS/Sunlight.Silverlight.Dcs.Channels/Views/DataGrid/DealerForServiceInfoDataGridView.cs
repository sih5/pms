﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;


namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class DealerForServiceInfoDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "Name"
                    }, new ColumnItem {
                        Name = "ShortName"
                    }, new ColumnItem {
                        Name = "Company.ProvinceName"
                    }, new ColumnItem {
                        Name = "Company.CityName"
                    }, new ColumnItem {
                        Name = "Company.CountyName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(Dealer);
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.ShowGroupPanel = false;
        }


        protected override string OnRequestQueryName() {
            return "GetDealersWithCompanyForService";
        }
    }
}
