﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using System.Linq;
using Sunlight.Silverlight.Dcs.Channels.Resources;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class DealerGradeForEditDataGridView : DcsDataGridViewBase {
        protected override Type EntityType {
            get {
                return typeof(DealerGradeInfo);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem{
                        Name = "GradeName",
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerGradeInfo_GradeName
                    }, new ColumnItem{
                        Name = "Remark"
                    }
                };
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("DealerGradeInfos");
        }

        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
            base.OnControlsCreated();
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var maxSerialNumber = 1;
            var userData = BaseApp.Current.CurrentUserData;
            if(this.GridView.HasItems)
                maxSerialNumber = this.GridView.Items.Cast<DealerGradeInfo>().Max(obj => obj.SerialNumber) + 1;
            e.NewObject = new DealerGradeInfo{
                SerialNumber = maxSerialNumber,
                PartsSalesCategoryId = 0,
                PartsSalesCategoryName = "提交时赋值",
                Status = (int)DcsBaseDataStatus.有效
            };
        }
    }
}
