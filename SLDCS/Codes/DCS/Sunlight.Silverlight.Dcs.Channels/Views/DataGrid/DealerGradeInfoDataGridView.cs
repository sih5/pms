﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.Channels.Resources;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class DealerGradeInfoDataGridView : DcsDataGridViewBase {

        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        public DealerGradeInfoDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(DealerGradeInfo);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new ColumnItem {
                        Name = "GradeName",
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerGradeInfo_GradeName
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems=this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime"
                    },new ColumnItem {
                        Name = "PartsSalesCategoryName",
                        IsSortDescending = false,
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategoryName
                    }
                };
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
        protected override string OnRequestQueryName() {
            return "GetDealerGradeInfo";

        }
    }
}
