﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class DealerHistoryDataGridView : DcsDataGridViewBase {
        protected readonly string[] KvNames = {
            "Repair_Qualification_Grade","YesOrNo","DealerServiceExt_VehicleTravelRoute","Invoice_Type","PartsSupplier_TaxpayerKind","MasterData_Status","Company_CityLevel","Customer_IdDocumentType"
        };

        public DealerHistoryDataGridView() {
            this.KeyValueManager.Register(this.KvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.KvNames[5]]
                    },
                    new ColumnItem{
                        Name="Code"
                    },new ColumnItem {
                        Name = "Name"
                    }, new ColumnItem {
                        Name = "AName"
                    }, new ColumnItem {
                        Name = "ShortName"
                    },new ColumnItem {
                        Name = "AShortName"
                    },new ColumnItem {
                        Name = "Manager"
                    },new ColumnItem {
                        Name = "AManager"
                    },new KeyValuesColumnItem{
                        Name="RepairQualification",
                        KeyValueItems=this.KeyValueManager[this.KvNames[0]]
                    },new KeyValuesColumnItem{
                        Name="ARepairQualification",
                        KeyValueItems=this.KeyValueManager[this.KvNames[0]]
                    },new KeyValuesColumnItem{
                        Name="HasBranch",
                        KeyValueItems=this.KeyValueManager[this.KvNames[1]]
                    },new KeyValuesColumnItem{
                        Name="AHasBranch",
                        KeyValueItems=this.KeyValueManager[this.KvNames[1]]
                    },new KeyValuesColumnItem{
                        Name="DangerousRepairQualification",
                        KeyValueItems=this.KeyValueManager[this.KvNames[1]]
                    },new KeyValuesColumnItem{
                        Name="ADangerousRepairQualification",
                        KeyValueItems=this.KeyValueManager[this.KvNames[1]]
                    }, new ColumnItem {
                        Name = "BrandScope"
                    },new ColumnItem {
                        Name = "ABrandScope"
                    },new ColumnItem {
                        Name = "CompetitiveBrandScope"
                    },  new ColumnItem {
                        Name = "ACompetitiveBrandScope"
                    },new ColumnItem{
                        Name="ParkingArea"
                    },new ColumnItem{
                        Name="AParkingArea"
                    },new ColumnItem{
                        Name="RepairingArea"
                    },new ColumnItem{
                        Name="ARepairingArea"
                    },new ColumnItem{
                        Name="EmployeeNumber"
                    },new ColumnItem{
                        Name="AEmployeeNumber"
                    },new ColumnItem{
                        Name="PartWarehouseArea"
                    },new ColumnItem{
                        Name="APartWarehouseArea"
                    },new ColumnItem{
                        Name="GeographicPosition"
                    },new ColumnItem{
                        Name="AGeographicPosition"
                    },new ColumnItem{
                        Name="OwnerCompany"
                    },new ColumnItem{
                        Name="AOwnerCompany"
                    },new ColumnItem{
                        Name="MainBusinessAreas"
                    },new ColumnItem{
                        Name="AMainBusinessAreas"
                    },new ColumnItem{
                        Name="AndBusinessAreas"
                    },new ColumnItem{
                        Name="AAndBusinessAreas"
                    },new ColumnItem{
                        Name="BuildTime"
                    },new ColumnItem{
                        Name="ABuildTime"
                    },new ColumnItem{
                        Name="TrafficRestrictionsdescribe"
                    },new ColumnItem{
                        Name="ATrafficRestrictionsdescribe"
                    },new ColumnItem{
                        Name="ManagerPhoneNumber"
                    },new ColumnItem{
                        Name="AManagerPhoneNumber"
                    },new ColumnItem{
                        Name="ManagerMobile"
                    },new ColumnItem{
                        Name="AManagerMobile"
                    },new ColumnItem {
                        Name = "ManagerMail"
                    },new ColumnItem {
                        Name = "AManagerMail"
                    },new ColumnItem {
                        Name = "ReceptionRoomArea"
                    },new ColumnItem {
                        Name = "AReceptionRoomArea"
                    },new ColumnItem {
                        Name = "IsVehicleSalesInvoice"
                    },new ColumnItem {
                        Name = "AIsVehicleSalesInvoice"
                    },new ColumnItem {
                        Name = "IsPartsSalesInvoice"
                    },new ColumnItem {
                        Name = "AIsPartsSalesInvoice"
                    },new ColumnItem {
                        Name = "TaxRegisteredNumber"
                    },new ColumnItem {
                        Name = "ATaxRegisteredNumber"
                    },new ColumnItem {
                        Name = "InvoiceTitle"
                    },new ColumnItem {
                        Name = "AInvoiceTitle"
                    },new KeyValuesColumnItem{
                        Name="InvoiceType",
                        KeyValueItems=this.KeyValueManager[this.KvNames[3]]
                    },new KeyValuesColumnItem{
                        Name="AInvoiceType",
                        KeyValueItems=this.KeyValueManager[this.KvNames[3]]
                    },new KeyValuesColumnItem{
                        Name="TaxpayerQualification",
                        KeyValueItems=this.KeyValueManager[this.KvNames[4]]
                    },new KeyValuesColumnItem{
                        Name="ATaxpayerQualification",
                        KeyValueItems=this.KeyValueManager[this.KvNames[4]]
                    },new ColumnItem{
                        Name="InvoiceAmountQuota"
                    },new ColumnItem{
                        Name="AInvoiceAmountQuota"
                    },new ColumnItem{
                        Name="TaxRegisteredAddress"
                    },new ColumnItem{
                        Name="ATaxRegisteredAddress"
                    }, new ColumnItem{
                        Name="TaxRegisteredPhone"
                    },  new ColumnItem{
                        Name="ATaxRegisteredPhone"
                    },new ColumnItem{
                        Name="Fax"
                    }, new ColumnItem{
                        Name="AFax"
                    }, new ColumnItem {
                        Name = "BankName"
                    }, new ColumnItem {
                        Name = "ABankName"
                    }, new ColumnItem {
                        Name = "BankAccount"
                    }, new ColumnItem {
                        Name = "ABankAccount"
                    }, new ColumnItem {
                        Name = "InvoiceTax"
                    }, new ColumnItem {
                        Name = "AInvoiceTax"
                    }, new ColumnItem {
                        Name = "Linkman"
                    }, new ColumnItem {
                        Name = "ALinkman"
                    },new ColumnItem {
                        Name = "ContactNumber"
                    }, new ColumnItem {
                        Name = "AContactNumber"
                    },new ColumnItem {
                        Name = "Fax"
                    }, new ColumnItem {
                        Name = "AFax"
                    }, new KeyValuesColumnItem {
                        Name = "AStatus",
                        KeyValueItems = this.KeyValueManager[this.KvNames[5]]
                    }, new ColumnItem {
                        Name = "CustomerCode"
                    }, new ColumnItem {
                        Name = "ACustomerCode"
                    }, new ColumnItem {
                        Name = "SupplierCode"
                    }, new ColumnItem {
                        Name = "ASupplierCode"
                    }, new ColumnItem {
                        Name = "FoundDate"
                    }, new ColumnItem {
                        Name = "AFoundDate"
                    }, new ColumnItem {
                        Name = "ProvinceName"
                    }, new ColumnItem {
                        Name = "AProvinceName"
                    }, new ColumnItem {
                        Name = "CityName"
                    }, new ColumnItem {
                        Name = "ACityName"
                    }, new ColumnItem {
                        Name = "CountyName"
                    }, new ColumnItem {
                        Name = "ACountyName"
                    }, new KeyValuesColumnItem {
                        Name = "CityLevel",
                        KeyValueItems=this.KeyValueManager[this.KvNames[6]]
                    }, new KeyValuesColumnItem {
                        Name = "ACityLevel",
                        KeyValueItems=this.KeyValueManager[this.KvNames[6]]
                    }, new ColumnItem {
                        Name = "ContactPerson"
                    }, new ColumnItem {
                        Name = "AContactPerson"
                    }, new ColumnItem {
                        Name = "ContactMobile"
                    }, new ColumnItem {
                        Name = "AContactMobile"
                    }, new ColumnItem {
                        Name = "ContactPhone"
                    }, new ColumnItem {
                        Name = "AContactPhone"
                    }, new ColumnItem {
                        Name = "ContactMail"
                    }, new ColumnItem {
                        Name = "AContactMail"
                    }, new ColumnItem {
                        Name = "CompanyFax"
                    }, new ColumnItem {
                        Name = "ACompanyFax"
                    }, new ColumnItem {
                        Name = "ContactPostCode"
                    }, new ColumnItem {
                        Name = "AContactPostCode"
                    }, new ColumnItem {
                        Name = "BusinessAddress"
                    }, new ColumnItem {
                        Name = "ABusinessAddress"
                    }, new ColumnItem {
                        Name = "RegisterCode"
                    }, new ColumnItem {
                        Name = "ARegisterCode"
                    }, new ColumnItem {
                        Name = "RegisterName"
                    }, new ColumnItem {
                        Name = "ARegisterName"
                    }, new ColumnItem {
                        Name = "RegisterCapital"
                    }, new ColumnItem {
                        Name = "ARegisterCapital"
                    }, new ColumnItem {
                        Name = "FixedAsset"
                    }, new ColumnItem {
                        Name = "AFixedAsset"
                    }, new ColumnItem {
                        Name = "RegisterDate"
                    }, new ColumnItem {
                        Name = "ARegisterDate"
                    }, new ColumnItem {
                        Name = "CorporateNature"
                    }, new ColumnItem {
                        Name = "ACorporateNature"
                    }, new ColumnItem {
                        Name = "LegalRepresentative"
                    }, new ColumnItem {
                        Name = "ALegalRepresentative"
                    }, new ColumnItem {
                        Name = "LegalRepresentTel"
                    }, new ColumnItem {
                        Name = "ALegalRepresentTel"
                    }, new KeyValuesColumnItem {
                        Name = "IdDocumentType",
                        KeyValueItems=this.KeyValueManager[this.KvNames[7]]
                    }, new KeyValuesColumnItem {
                        Name = "AIdDocumentType",
                        KeyValueItems=this.KeyValueManager[this.KvNames[7]]
                    }, new ColumnItem {
                        Name = "IdDocumentNumber"
                    }, new ColumnItem {
                        Name = "AIdDocumentNumber"
                    }, new ColumnItem {
                        Name = "RegisteredAddress"
                    }, new ColumnItem {
                        Name = "ARegisteredAddress"
                    }, new ColumnItem {
                        Name = "BusinessScope"
                    }, new ColumnItem {
                        Name = "ABusinessScope"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerHistory);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override string OnRequestQueryName() {
            return "GetDealerHistories";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.ShowGroupPanel = false;
        }
    }
}
