﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class DealerKeyEmployeHistoryWithDealerKeyPositionDataGridView : DcsDataGridViewBase {
        public DealerKeyEmployeHistoryWithDealerKeyPositionDataGridView() {
            this.DataContextChanged += this.DealerKeyEmployeHistoryWithDealerKeyPositionDataGridView_DataContextChanged;
        }

        private void DealerKeyEmployeHistoryWithDealerKeyPositionDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var dealerKeyEmployee = e.NewValue as DealerKeyEmployee;
            if(dealerKeyEmployee == null)
                return;
            CompositeFilterItem compositeFilterItem;
            if(this.FilterItem is CompositeFilterItem) {
                compositeFilterItem = this.FilterItem as CompositeFilterItem;
            } else {
                compositeFilterItem = new CompositeFilterItem();
                var filterItemDealer = new FilterItem {
                    MemberName = "DealerId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo
                };
                var filterItemPosition = new FilterItem {
                    MemberName = "KeyPositionId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo
                };
                compositeFilterItem.Filters.Add(filterItemDealer);
                compositeFilterItem.Filters.Add(filterItemPosition);
                this.FilterItem = compositeFilterItem;
            }
            compositeFilterItem.Filters.Single(filter => filter.MemberName == "DealerId").Value = dealerKeyEmployee.DealerId;
            compositeFilterItem.Filters.Single(filter => filter.MemberName == "KeyPositionId").Value = dealerKeyEmployee.KeyPositionId;
            this.ExecuteQueryDelayed();
        }

        protected override Type EntityType {
            get {
                return typeof(DealerKeyEmployeeHistory);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Status"
                    }, new ColumnItem {
                        Name = "DealerKeyPosition.Responsibility"
                    }, new ColumnItem {
                        Name = "Name"
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    },  new ColumnItem {
                        Name = "DealerKeyPosition.PositionName"
                    }, 
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetDealerKeyEmployeeHistoriesWithDealerKeyPosition";
        }
    }
}
