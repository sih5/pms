﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Channels.Resources;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class DealerKeyEmployeeDetailDataGridView : DcsDataGridViewBase {
        protected override Type EntityType {
            get {
                return typeof(DealerKeyEmployee);
            }
        }
        private readonly string[] kvNames = {
            "Sex_Type"
        };

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "DealerKeyPosition.PositionName",
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "DealerKeyPosition.Responsibility",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Name"
                    },new ColumnItem{
                        Name="MobileNumber",
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerKeyEmployee_MobileNumber
                    }, new ColumnItem {
                        Name = "PhoneNumber",
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_PhoneNumber
                    }, new ColumnItem {
                        Name = "Mail"
                    }
                };
            }
        }
        public DealerKeyEmployeeDetailDataGridView() {
            this.KeyValueManager.Register(kvNames);
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("DealerKeyEmployees");
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
        }

    }
}
