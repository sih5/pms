﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Channels.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class DealerKeyEmployeeForEditDataGridView : DcsDataGridViewBase {
        private ObservableCollection<KeyValuePair> kvDealerKeyPositions = new ObservableCollection<KeyValuePair>();

        public ObservableCollection<KeyValuePair> KvDealerKeyPositions {
            get {
                return this.kvDealerKeyPositions ?? (this.kvDealerKeyPositions = new ObservableCollection<KeyValuePair>());
            }
        }
        private readonly ObservableCollection<KeyValuePair> kvSex = new ObservableCollection<KeyValuePair>();
        public ObservableCollection<KeyValuePair> KvSex {
            get {
                return this.kvSex;
            }
        }
        private readonly string[] kvNames = {
            "Sex_Type","Education","SocialTitle","IsOrNot","WorkType","SkillLevel"
        };

        private DcsDomainContext dcsDomainContext {
            get {
                return this.DomainContext as DcsDomainContext ?? new DcsDomainContext();
            }
        }

        public DealerKeyEmployeeForEditDataGridView() {
            this.KeyValueManager.Register(kvNames);
            this.KeyValueManager.LoadData(() => {
                this.kvSex.Clear();
                foreach(var item in this.KeyValueManager[this.kvNames[0]].Where(e => e.Key == (int)DcsSexType.男 || e.Key == (int)DcsSexType.女)) {
                    this.kvSex.Add(new KeyValuePair {
                        Key = item.Key,
                        Value = item.Value
                    });
                }
            });
            this.Loaded += DealerKeyEmployeeForEditDataGridView_Loaded;
            //this.DataLoaded += DealerKeyEmployeeForEditDataGridView_DataLoaded;
            this.DataContextChanged += DealerKeyEmployeeForEditDataGridView_DataContextChanged;

        }
        public void bingDataEditData() {
            var dataEdit = this.DataContext as DealerServiceInfoForDealerDataEditView;
            // var dataEdit = this.DomainContext as DealerKeyEmployee;           
            if(dataEdit == null)
                return;
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetDealerKeyPositionsQuery().Where(entity => entity.BranchId == dataEdit.branchId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                kvDealerKeyPositions.Clear();
                foreach(var dealerKeyPosition in loadOp.Entities)
                    this.kvDealerKeyPositions.Add(new KeyValuePair {
                        Key = dealerKeyPosition.Id,
                        Value = dealerKeyPosition.PositionName,
                        UserObject = dealerKeyPosition
                    });
                if(!dataEdit.DealerKeyEmployees.Any())
                    return;
                var sortDesc = new SortDescriptor {
                    Member = "KeyPositionId",
                    SortDirection = ListSortDirection.Ascending
                };
                //通过排序 刷新下拉列表中不显示的值
                this.GridView.SortDescriptors.Add(sortDesc);
                this.GridView.SortDescriptors.Remove(sortDesc);
            }, null);
        }

        void DealerKeyEmployeeForEditDataGridView_DataLoaded(object sender, EventArgs e) {
            var sortDesc = new SortDescriptor {
                Member = "KeyPositionId",
                SortDirection = ListSortDirection.Ascending
            };
            //通过排序 刷新下拉列表中不显示的值
            this.GridView.SortDescriptors.Add(sortDesc);
            this.GridView.SortDescriptors.Remove(sortDesc);
        }

        void DealerKeyEmployeeForEditDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var dataEdit = this.DataContext as DealerServiceInfoForDealerDataEditView;
            // var dataEdit = this.DomainContext as DealerKeyEmployee;           
            if(dataEdit == null)
                return;

            dcsDomainContext.Load(dcsDomainContext.GetDealerKeyPositionsQuery().Where(entity => entity.BranchId == dataEdit.branchId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                kvDealerKeyPositions.Clear();
                foreach(var dealerKeyPosition in loadOp.Entities)
                    this.kvDealerKeyPositions.Add(new KeyValuePair {
                        Key = dealerKeyPosition.Id,
                        Value = dealerKeyPosition.PositionName,
                        UserObject = dealerKeyPosition
                    });
                if(!dataEdit.DealerKeyEmployees.Any())
                    return;
                var sortDesc = new SortDescriptor {
                    Member = "KeyPositionId",
                    SortDirection = ListSortDirection.Ascending
                };
                //通过排序 刷新下拉列表中不显示的值
                this.GridView.SortDescriptors.Add(sortDesc);
                this.GridView.SortDescriptors.Remove(sortDesc);
            }, null);
        }

        void DealerKeyEmployeeForEditDataGridView_Loaded(object sender, RoutedEventArgs e) {
            var dataEdit = this.DataContext as DealerServiceInfoForDealerDataEditView;
            // var dataEdit = this.DomainContext as DealerKeyEmployee;           
            if(dataEdit == null)
                return;
            dcsDomainContext.Load(dcsDomainContext.GetDealerKeyPositionsQuery().Where(entity => entity.BranchId == dataEdit.branchId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                kvDealerKeyPositions.Clear();
                foreach(var dealerKeyPosition in loadOp.Entities)
                    this.kvDealerKeyPositions.Add(new KeyValuePair {
                        Key = dealerKeyPosition.Id,
                        Value = dealerKeyPosition.PositionName,
                        UserObject = dealerKeyPosition
                    });
                if(!dataEdit.DealerKeyEmployees.Any())
                    return;
                var sortDesc = new SortDescriptor {
                    Member = "KeyPositionId",
                    SortDirection = ListSortDirection.Ascending
                };
                //通过排序 刷新下拉列表中不显示的值
                this.GridView.SortDescriptors.Add(sortDesc);
                this.GridView.SortDescriptors.Remove(sortDesc);
            }, null);
        }

        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs eventArgs) {

            var employee = eventArgs.Row.DataContext as DealerKeyEmployee;
            if(employee != null && employee.EntityState != EntityState.New && employee.EntityState != EntityState.Detached)
                return;
            if(eventArgs.Cell.Column.UniqueName == "Status")
                eventArgs.Cancel = true;
        }

        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {

            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "KeyPositionId":
                    var positionId = e.NewValue is int ? (int)e.NewValue : 0;
                    if(positionId == default(int)) {
                        e.IsValid = false;
                        e.ErrorMessage = ChannelsUIStrings.DataGridView_Validation_DealerKeyEmployee_KeyPositionIdIsNull;
                    }
                    var keyValuePair = this.kvDealerKeyPositions.SingleOrDefault(r => r.Key == positionId);
                    if(keyValuePair == null)
                        return;
                    var dealerKeyEmployee = e.Row.DataContext as DealerKeyEmployee;
                    if(dealerKeyEmployee == null)
                        return;
                    var dataEditView = this.DataContext as DealerServiceInfoForDealerDataEditView;
                    var dealerKeyEmployees = dataEditView.DealerKeyEmployees;
                    if(keyValuePair.Value == "服务协调人") {
                        if(dealerKeyEmployees.Any(r => r.KeyPositionId == keyValuePair.Key && r.IsOnJob == (int)DcsIsOrNot.是 && (r.Id != dealerKeyEmployee.Id || r.Id == default(int)))) {
                            e.IsValid = false;
                            e.ErrorMessage = string.Format("{0}不允许重复维护多个在职的", keyValuePair.Value);
                        }
                    }
                    if(keyValuePair.Value == "站长") {
                        if(dealerKeyEmployees.Any(r => r.KeyPositionId == keyValuePair.Key && r.IsOnJob==(int)DcsIsOrNot.是 && (r.Id != dealerKeyEmployee.Id || r.Id == default(int)))) {
                            e.IsValid = false;
                            e.ErrorMessage = string.Format("{0}不允许重复维护多个在职的", keyValuePair.Value);
                        }
                    }
                    break;
                case "Status":
                    if(e.NewValue == null) {
                        e.IsValid = false;
                        e.ErrorMessage = ChannelsUIStrings.DataGridView_Validation_DealerKeyEmployee_StatusIsNull;
                    }
                    break;
                case "Name":
                    var name = e.NewValue as string;
                    if(name == null || string.IsNullOrWhiteSpace(name)) {
                        e.IsValid = false;
                        e.ErrorMessage = ChannelsUIStrings.DataGridView_Validation_DealerKeyEmployee_NameIsNull;
                    }

                    break;
                case "MobileNumber":
                    var mobileNumber = e.NewValue as string;
                    if(mobileNumber == null || string.IsNullOrWhiteSpace(mobileNumber)) {
                        //e.IsValid = false;
                        //e.ErrorMessage = "手机号码不能为空!!!";
                        return;
                    } else if(mobileNumber != null && mobileNumber.Length != 11) {
                        e.IsValid = false;
                        e.ErrorMessage = "手机号码必须为11位有效号码!!!";
                    }
                    break;
                case "IdCardNumber":
                    var idCardNumber = e.NewValue as string;
                    if(idCardNumber == null || string.IsNullOrWhiteSpace(idCardNumber))
                        return;
                    if(idCardNumber.Length != 15 && idCardNumber.Length != 18) {
                        e.IsValid = false;
                        e.ErrorMessage = "请输入正确的身份证号码位数!!!";
                        return;
                    }
                    if(Regex.IsMatch(idCardNumber.Substring(0, 5), KeyRegularExpression.ID_DOCUMENT_NUMBER_BEFORE)) {
                        e.IsValid = false;
                        e.ErrorMessage = "身份证号码前6位不能完全一样!!!";
                        return;
                    }
                    switch(idCardNumber.Length) {
                        case 15:
                            try {
                                var bornOldString = idCardNumber.Substring(0, 14);
                                if(!Regex.IsMatch(bornOldString, "^\\d+$")) {
                                    e.IsValid = false;
                                    e.ErrorMessage = "身份证号码前14/17位只能为数字";
                                    return;
                                }
                                var bornlastString = idCardNumber.Substring(14, 1);
                                if(!Regex.IsMatch(bornlastString, "^\\d+$") && !(bornlastString.Equals("x") || bornlastString.Equals("X"))) {
                                    e.IsValid = false;
                                    e.ErrorMessage = "身份证号码最后一位只能为数字或者X";
                                    return;
                                }
                                var month = int.Parse(idCardNumber.Substring(8, 2));
                                if(month < 1 || month > 12) {
                                    e.IsValid = false;
                                    e.ErrorMessage = "身份证号码中月份只允许01-12";
                                    return;
                                }
                                var day = int.Parse(idCardNumber.Substring(10, 2));
                                if(day < 1 || day > 31) {
                                    e.IsValid = false;
                                    e.ErrorMessage = "身份证号码中日期只允许01-31";
                                    return;
                                }
                            } catch(Exception) {
                                e.IsValid = false;
                                e.ErrorMessage = "请输入正确的身份证号码";
                                return;
                            }

                            break;
                        case 18:
                            try {
                                var bornNewString = idCardNumber.Substring(0, 17);
                                if(!Regex.IsMatch(bornNewString, "^\\d+$")) {
                                    e.IsValid = false;
                                    e.ErrorMessage = "身份证前14/17位只能为数字";
                                    return;
                                }
                                var bornlastString = idCardNumber.Substring(17, 1);
                                if(!Regex.IsMatch(bornlastString, "^\\d+$") && !(bornlastString.Equals("x") || bornlastString.Equals("X"))) {
                                    e.IsValid = false;
                                    e.ErrorMessage = "身份证最后一位只能为数字或者X";
                                    return;
                                }
                                if(idCardNumber.Substring(6, 2) != "19" && idCardNumber.Substring(6, 2) != "20") {
                                    e.IsValid = false;
                                    e.ErrorMessage = "身份证号码中年份只允许是19-20开头";
                                    return;
                                }
                                var month = int.Parse(idCardNumber.Substring(10, 2));
                                if(month < 1 || month > 12) {
                                    e.IsValid = false;
                                    e.ErrorMessage = "身份证号码中月份只允许01-12";
                                    return;
                                }
                                var day = int.Parse(idCardNumber.Substring(12, 2));
                                if(day < 1 || day > 31) {
                                    e.IsValid = false;
                                    e.ErrorMessage = "身份证号码中日期只允许01-31";
                                    return;
                                }
                            } catch(Exception) {
                                e.IsValid = false;
                                e.ErrorMessage = "请输入正确的身份证号码";
                                return;
                            }
                            break;
                    }
                    break;
            }
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var dataEdit = this.DataContext as DealerServiceInfoForDealerDataEditView;
            if(dataEdit == null)
                return;
            //dataEdit.DealerKeyEmployees.Add(new DealerKeyEmployee {
            //    Status = (int)DcsBaseDataStatus.有效,
            //    DealerId = BaseApp.Current.CurrentUserData.EnterpriseId,
            //    PartsSalesCategoryId = dataEdit.partsSalesCategoryId
            //});
        }


        void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            
            var dDomainContext = new DcsDomainContext();
            if(e.Cell.DataColumn.UniqueName.Equals("KeyPositionId")) {
                var dealerKeyEmployee = e.Cell.ParentRow.DataContext as DealerKeyEmployee;
                if(dealerKeyEmployee != null) {
                    dDomainContext.Load(dDomainContext.GetDealerKeyEmployeesQuery().Where(r => r.Id == dealerKeyEmployee.Id), LoadBehavior.RefreshCurrent, loadOps => {
                        if(loadOps.HasError) {
                            loadOps.MarkErrorAsHandled();
                            return;
                        }
                        if(loadOps.Entities != null && loadOps.Entities.Any()) {
                            dealerKeyEmployee.BTPositionId = loadOps.Entities.First().KeyPositionId;
                            dealerKeyEmployee.TransferTime = DateTime.Now;
                          
                        }
                    }, null);
                    var dealerKeyPosition = this.KvDealerKeyPositions.FirstOrDefault(r => r.Key == dealerKeyEmployee.KeyPositionId);
                    if(dealerKeyPosition != null) {
                        dcsDomainContext.Load(dcsDomainContext.GetDealerKeyPositionsQuery().Where(r => r.Id == dealerKeyPosition.Key), LoadBehavior.RefreshCurrent, loadOp => {
                            if(loadOp.HasError) {
                                loadOp.MarkErrorAsHandled();
                                return;
                            }
                            if(loadOp.Entities != null && loadOp.Entities.Any()) {
                                dealerKeyEmployee.DealerKeyPosition = loadOp.Entities.First();
                                dealerKeyEmployee.IsKeyPositions = loadOp.Entities.First().IsKeyPositions;
                            }
                        }, null);
                    }
                }
            }
            if(e.Cell.Column.UniqueName.Equals("Name")) {
                var dealerKeyEmployee = e.Cell.ParentRow.DataContext as DealerKeyEmployee;
                if(dealerKeyEmployee == null)
                    return;
                var data = DataContext as DealerServiceInfo;
                 
                if(dealerKeyEmployee.Id != 0) {
                    dealerKeyEmployee.Name = e.OldData.ToString();
                    UIHelper.ShowNotification("修改时.姓名不可修改");
                    return;
                }
            }
        }

        //private void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
        //    ////foreach(var employee in e.Items.Cast<DealerKeyEmployee>())
        //    ////    if(employee.Can取消经销商关键岗位人员)
        //    ////        employee.取消经销商关键岗位人员();
        //    //var dealerServiceInfo = this.DataContext as DealerServiceInfo;
        //    //if(dealerServiceInfo == null)
        //    //    return;            

        //}

        protected override Type EntityType {
            get {
                return typeof(DealerKeyEmployee);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "KeyPositionId",
                        KeyValueItems = this.kvDealerKeyPositions
                    }, new ColumnItem {
                        Name = "DealerKeyPosition.Responsibility",
                        IsReadOnly = true
                    },new ColumnItem{
                        Name="IsKeyPositions",
                        Title="是否关键岗位",
                        IsReadOnly=true
                    },new ColumnItem {
                        Name = "Name",
                    },new ColumnItem{
                        Name="MobileNumber",
                        Title="手机号码"
                    }, new ColumnItem {
                        Name = "PhoneNumber",
                        Title="固定电话"
                    }, new ColumnItem {
                        Name = "Mail"
                    }, new KeyValuesColumnItem {
                        Name = "Sex",
                        Title="性别",
                        KeyValueItems = kvSex
                    }, new ColumnItem {
                        Name = "IdCardNumber",
                        Title="身份证号"
                    }, new ColumnItem {
                        Name = "School",
                        Title="学校"
                    }, new KeyValuesColumnItem {
                        Name = "Education",
                        Title="学历",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Name = "Professional",
                        Title="专业"                 
                    },new KeyValuesColumnItem {
                        Name = "ProfessionalRank",
                        Title="社会职称",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    }, new ColumnItem {
                        Name = "EntryTime",
                        Title="入职时间"
                    }, new KeyValuesColumnItem {
                        Name = "IsOnJob",
                        Title="是否在职",
                        KeyValueItems = this.KeyValueManager[this.kvNames[3]]
                    }, new ColumnItem {
                        Name = "LeaveTime",
                        Title="离职时间"
                    },new KeyValuesColumnItem {
                        Name = "WorkType",
                        Title="工种",
                        KeyValueItems = this.KeyValueManager[this.kvNames[4]]
                    },new KeyValuesColumnItem {
                        Name = "SkillLevel",
                        Title="技能等级",
                        KeyValueItems = this.KeyValueManager[this.kvNames[5]]
                    }, new KeyValuesColumnItem {
                        Name = "BTPositionId",
                        Title="内部调岗前岗位",
                         KeyValueItems = this.kvDealerKeyPositions,
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "TransferTime",
                        Title="调岗时间",
                        IsReadOnly=true
                    }
                };
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("DealerKeyEmployees");
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.CellValidating += this.GridView_CellValidating;
            this.GridView.BeginningEdit += this.GridView_BeginningEdit;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            //this.GridView.Deleted += this.GridView_Deleted;
            this.GridView.Deleting += GridView_Deleting;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            this.GridView.RowEditEnded += GridView_RowEditEnded;
            //var sortDesc = new SortDescriptor {
            //    Member = "KeyPositionId",
            //    SortDirection = ListSortDirection.Ascending
            //};
            ////通过排序 刷新下拉列表中不显示的值
            //this.GridView.SortDescriptors.Add(sortDesc);
            //this.GridView.SortDescriptors.Remove(sortDesc);
        }

        void GridView_RowEditEnded(object sender, GridViewRowEditEndedEventArgs e) {
            var dDomainContext = new DcsDomainContext();
            var dataEdit = this.DataContext as DealerServiceInfoForDealerDataEditView;
            var dealerServiceInfo = dataEdit.DataContext as DealerServiceInfo;
            if(dataEdit == null)
                return;
            if(dealerServiceInfo == null)
                return;
            var dealerKeyEmployee =e.Row.DataContext as DealerKeyEmployee;
            if(dealerKeyEmployee == null)
                return;
            dDomainContext.Load(dDomainContext.GetDealerKeyPositionsQuery().Where(r => r.IsKeyPositions == true && r.BranchId == dataEdit.branchId && r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp1 => {
                if(loadOp1.HasError) {
                    if(!loadOp1.IsErrorHandled)
                        loadOp1.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp1);
                    return;
                }
                var result = loadOp1.Entities.ToArray();
                if(result == null)
                    return;
                var dealerKeyPositions = result.Count(v => dataEdit.DealerKeyEmployees.Any(o => o.KeyPositionId == v.Id));
                if(dealerKeyPositions == result.Count()) {
                    var dealerKeyEmployees = dataEdit.DealerKeyEmployees.Where(v => v.IsKeyPositions == true && v.Status == (int)DcsBaseDataStatus.有效 && v.Name == null);
                    if(!dealerKeyEmployees.Any()) {
                        dealerServiceInfo.ExternalState = (int)DcsExternalState.有效;
                    }
                }
            }, null);
        }

        void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            ////var dealer = this.DataContext as Dealer;
            ////if(dealer == null)
            ////    return;
            ////foreach(var dealerKeyEmployee in dealer.DealerKeyEmployees.Where(entity => e.Items.Contains(entity))) {
            ////    if(dealerKeyEmployee.Id == 0) {
            ////        dealer.DealerKeyEmployees.Remove(dealerKeyEmployee);
            ////    } else {
            ////        UIHelper.ShowNotification("已经存在的关键岗位人员信息不允许删除!");
            ////        return;
            ////    }

            //}
            //var dataEditView = this.DataContext as DealerServiceInfoForDealerDataEditView;
            //if(dataEditView == null)
            //    return;
            //var domainContext = this.DomainContext as DcsDomainContext;
            //if(domainContext == null)
            //    return;
            //foreach(var item in e.Items.Cast<DealerKeyEmployee>()) {
            //    if(item.Id != 0) {
            //        UIHelper.ShowNotification("已经存在的关键岗位人员信息不允许删除!");
            //        return;
            //    }
            //    domainContext.DealerKeyEmployees.Remove(item);
            //}
            var items = e.Items;


            if(items == null)
                e.Cancel = true;
            e.Cancel = items.Cast<DealerKeyEmployee>().Any(invoice => invoice.Id != 0);
            if(e.Cancel)
                UIHelper.ShowNotification(string.Format("已经存在的关键岗位人员信息不允许删除!"));
            else if(items.Cast<DealerKeyEmployee>().Any(v => v.DealerKeyPosition.IsKeyPositions == true)) {
                UIHelper.ShowNotification(string.Format("关键岗位人员中“是否关键岗位”字段为“是”的人员信息不允许删除"));
                e.Cancel = true;
            }





        }

    }
}
