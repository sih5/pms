﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class DealerKeyEmployeeForSelectDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "DealerKeyPosition.PositionName",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerKeyPositionName,
                    },new ColumnItem {
                        Name = "DealerKeyPosition.Responsibility"
                    },new ColumnItem {
                        Name = "Name"
                    },new ColumnItem {
                        Name = "PhoneNumber"
                    },new ColumnItem {
                        Name = "Dealer.Code"
                    },new ColumnItem {
                        Name = "Dealer.Name"
                    },new ColumnItem {
                        Name = "Mail"
                    },new ColumnItem {
                        Name = "Remark"
                    },new ColumnItem {
                        Name = "CreateTime"
                    },new ColumnItem {
                        Name = "CreatorName"
                    },new ColumnItem {
                        Name = "PartsSalesCategory.Name",
                        Title =  ChannelsUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name
                    }
                };
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.ShowGroupPanel = false;
        }

        //protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
        //    var composite = this.FilterItem as CompositeFilterItem;
        //    var newComposite = new CompositeFilterItem();
        //    if(composite != null)
        //        foreach(var filterItem in composite.Filters.Where(filter => filter.MemberName != "Dealer.Name"))
        //            newComposite.Filters.Add(filterItem);
        //    return newComposite.ToFilterDescriptor();
        //}
        protected override string OnRequestQueryName() {
            return "GetDealerKeyEmployeesWithDetails";
        }
        protected override Type EntityType {
            get {
                return typeof(DealerKeyEmployee);
            }
        }
    }
}
