﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class DealerKeyEmployeeWithDetailsDataGridView : DcsDataGridViewBase {
        private readonly ObservableCollection<KeyValuePair> kvIsObJob = new ObservableCollection<KeyValuePair>();
        protected override Type EntityType {
            get {
                return typeof(DealerKeyEmployee);
            }
        }
        private readonly string[] kvNames = {
             "Sex_Type","Education","SocialTitle","WorkType","SkillLevel","IsOrNot"
        };
        public DealerKeyEmployeeWithDetailsDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.kvIsObJob.Add(new KeyValuePair {
                Key = 0,
                Value = "否"
            });
            this.kvIsObJob.Add(new KeyValuePair {
                Key = 1,
                Value = "是"
            });



        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    //new ColumnItem {
                    //    Name = "DealerKeyPosition.Branch.Name",
                    //    IsSortDescending = false
                    //},
                    new ColumnItem {
                        Name = "Dealer.Code",
                        Title = "服务站/经销商编号"
                    }, new ColumnItem {
                        Name = "Dealer.Name",
                        Title = "服务站/经销商名称"
                    }, new ColumnItem {
                        Name = "DealerKeyPosition.PositionName"
                    }, new ColumnItem {
                        Name = "DealerKeyPosition.Responsibility"
                    }, new ColumnItem {
                        Name = "Name"
                    },new ColumnItem {
                         Name  = "MobileNumber",
                        Title =ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_MobileNumber
                    }, new ColumnItem {
                        Name = "PhoneNumber",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_PhoneNumber
                    }, new ColumnItem {
                        Name = "MarketingDepartmentNameQuery",
                        Title="市场部"
                    }, new KeyValuesColumnItem {
                        Name = "Sex",
                        Title="性别",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "IdCardNumber",
                        Title="身份证号"
                    }, new ColumnItem {
                        Name = "School",
                        Title="学校"
                    }, new KeyValuesColumnItem {
                        Name = "Education",
                        Title="学历",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Name = "Professional",
                        Title="专业"
                    }, new KeyValuesColumnItem {
                        Name = "ProfessionalRank",
                        Title="社会职称",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    }, new ColumnItem {
                        Name = "EntryTime",
                        Title="入职时间"
                    }, new KeyValuesColumnItem {
                        Name = "IsOnJob",
                        Title="是否在职",
                        KeyValueItems = this.KeyValueManager[this.kvNames[5]]
                    }, new ColumnItem {
                        Name = "LeaveTime",
                        Title="离职时间"
                    }, new KeyValuesColumnItem {
                        Name = "WorkType",
                        Title="工种",
                        KeyValueItems = this.KeyValueManager[this.kvNames[3]]
                    }, new KeyValuesColumnItem {
                        Name = "SkillLevel",
                        Title="技能等级",
                        KeyValueItems = this.KeyValueManager[this.kvNames[4]]
                    }, new ColumnItem {
                        Name = "DealerKeyPosition.PositionName",
                        Title="内部调岗前岗位"
                    }, new ColumnItem {
                        Name = "TransferTime",
                        Title="调岗时间"
                    },new  ColumnItem{
                        Name="IsKeyPositions",
                        Title="是否关键岗位"
                         
                    },
                    new ColumnItem {
                        Name = "Mail",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_Mail
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem{
                        Name="PartsSalesCategory.Name",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_PartsSalesCategory_Name
                    }
                };
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "DealerKeyEmployeHistoryWithDealerKeyPosition"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetDealerKeyEmployeesWithDetailsByMarketDtpName";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "dtpName":
                        var filterCategory = filters.Filters.SingleOrDefault(item => item.MemberName == "MarketingDepartment");
                        return filterCategory == null ? null : filterCategory.Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "MarketingDepartment"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override void OnDataLoaded() {
            var dealerKeyEmployees = this.Entities.Cast<DealerKeyEmployee>().ToArray();
            if(dealerKeyEmployees == null) {
                return;
            }
            var dcsDomainContext = this.DomainContext as DcsDomainContext;
            dcsDomainContext.Load(dcsDomainContext.GetDealerServiceWithMarketingDeptNameByDealerKeyEmployeeIdQuery(dealerKeyEmployees.Select(r => r.Id).ToArray()), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    return;
                }
                var dealerServiceWithMarketingDeptNames = loadOp.Entities.ToList();
                if(dealerServiceWithMarketingDeptNames == null)
                    return;
                foreach(var dealerKeyEmployee in dealerKeyEmployees) {
                    var dealerServiceWithMarketingDeptName = dealerServiceWithMarketingDeptNames.FirstOrDefault(r => r.DealerId == dealerKeyEmployee.DealerId && r.PartsSalesCategoryId == dealerKeyEmployee.PartsSalesCategoryId);
                    if(dealerServiceWithMarketingDeptName != null && dealerServiceWithMarketingDeptName.MarketingDepartment != null)
                        dealerKeyEmployee.MarketingDepartmentNameQuery = dealerServiceWithMarketingDeptName.MarketingDepartment.Name;
                }
            }, null);
        }
    }
}

