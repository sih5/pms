﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class DealerKeyPositionDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "MasterData_Status"
        };
        protected override Type EntityType {
            get {
                return typeof(DealerKeyPosition);
            }
        }

        public DealerKeyPositionDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "PositionName"
                    }, new ColumnItem {
                        Name = "Responsibility"
                    }, 
                    new ColumnItem{
                        Name="IsKeyPositions" ,
                        Title="是否关键岗位"
                    },
                    new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    },  new ColumnItem {
                        Name = "Branch.Name",
                        IsSortDescending = false
                    }
                         

                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetDealerKeyPositionsWithBranch";
        }
    }
}
