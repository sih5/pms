﻿using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class DealerMobileNumberListForDetailDataGridView : DealerMobileNumberListForEditDataGridView {
        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            base.OnControlsCreated();
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, 
                    new ColumnItem{
                        Name = "VideoMobileNumber",
                       IsReadOnly = true
                    }
                };
            }
        }

    }
}
