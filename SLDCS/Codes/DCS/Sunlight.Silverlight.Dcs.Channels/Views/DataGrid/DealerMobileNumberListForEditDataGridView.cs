﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class DealerMobileNumberListForEditDataGridView : DcsDataGridViewBase {

        protected override Type EntityType {
            get {
                return typeof(DealerMobileNumberList);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, 
                    new ColumnItem{
                        Name = "VideoMobileNumber"
                    }
                };
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("DealerMobileNumberLists");
        }

        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
            this.GridView.Deleting -= GridView_Deleting;
            this.GridView.Deleting += GridView_Deleting;
            base.OnControlsCreated();
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var maxSerialNumber = 1;
            if(this.GridView.HasItems)
                maxSerialNumber = this.GridView.Items.Cast<DealerMobileNumberList>().Max(obj => obj.SerialNumber) + 1;
            e.NewObject = new DealerMobileNumberList {
                SerialNumber = maxSerialNumber
            };
        }

        private void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            var dealer = this.DataContext as Dealer;
            if(dealer == null)
                return;
            var serialNumber = 1;
            foreach(var dealerMobileNumberList in dealer.DealerMobileNumberLists.Where(entity => !e.Items.Contains(entity))) {
                dealerMobileNumberList.SerialNumber = serialNumber;
                serialNumber++;
            }
            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            foreach(var item in e.Items.Cast<DealerMobileNumberList>().Where(item => domainContext.DealerMobileNumberLists.Contains(item)))
                domainContext.DealerMobileNumberLists.Remove(item);
        }
    }
}
