﻿
using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class DealerServiceInfoBindServiceProductLineForDetailDataGridView : DcsDataGridViewBase {
        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("DealerBusinessPermits");
        }


        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new ColumnItem {
                        Name = "ServiceProductLine.Code",
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_ServiceProductLine_Code
                    },new ColumnItem {
                        Name = "ServiceProductLine.Name",
                        IsReadOnly = true
                    },new ColumnItem{
                        Name="ProductLineType",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerBusinessPermit_ProductLineType
                    }
                };
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerBusinessPermit);
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }


    }
}
