﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Channels.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class DealerServiceInfoBindServiceProductLineForEditDataGridView : DcsDataGridViewBase {
        public ObservableCollection<KeyValuePair> kvLaborHourUnitPriceGrades = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
            "ServiceProductLineView_ProductLineType","EngineRepairPower"
        };
        private ObservableCollection<ServiceProductLineView> serviceProductLineViews;
        private ObservableCollection<KeyValuePair> dicServiceProductLineViews;
        public DealerServiceInfoBindServiceProductLineForEditDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.Loaded += DealerServiceInfoBindServiceProductLineForEditDataGridView_Loaded;

        }

        void DealerServiceInfoBindServiceProductLineForEditDataGridView_Loaded(object sender, RoutedEventArgs e) {
            var dcsDomainContext = this.DomainContext as DcsDomainContext;
            if(dcsDomainContext == null)
                return;
            //dcsDomainContext.Load(dcsDomainContext.GetLaborHourUnitPriceGradesQuery().Where(ex => ex.Status == (int)DcsBaseDataStatus.有效 && ex.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    kvLaborHourUnitPriceGrades.Clear();
            //    foreach(var item in loadOp.Entities)
            //        this.kvLaborHourUnitPriceGrades.Add(new KeyValuePair {
            //            Key = item.Id,
            //            Value = item.Name,
            //            UserObject = item
            //        });
            //}, null);

        }
        public ObservableCollection<KeyValuePair> DicServiceProductLineViews {
            get {
                return this.dicServiceProductLineViews ?? (this.dicServiceProductLineViews = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<ServiceProductLineView> ServiceProductLineViews {
            get {
                return serviceProductLineViews ?? (this.serviceProductLineViews = new ObservableCollection<ServiceProductLineView>());
            }
            set {
                this.serviceProductLineViews = value;
            }
        }
        private void GridView_PreparingCellForEdit(object sender, GridViewPreparingCellForEditEventArgs e) {
            if(e.Column.UniqueName.Equals("ServiceProductLineCode")) {
                //var dealerServiceInfoForBranchDataEditView = this.DataContext as DealerServiceInfoForBranchDataEditView;
                //if(dealerServiceInfoForBranchDataEditView == null)
                //    return;
                //var serviceProductLineIds = dealerServiceInfoForBranchDataEditView.DealerBusinessPermits.Select(r => r.ServiceProductLineId);
                var detail = e.Row.DataContext as DealerBusinessPermit;
                if(detail == null)
                    return;
                var comboBox = e.EditingElement as RadComboBox;
                if(comboBox == null)
                    return;
                comboBox.ItemsSource = this.DicServiceProductLineViews;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new ColumnItem {
                        Name = "ServiceProductLineCode",
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_ServiceProductLine_Code
                    },new ColumnItem {
                        Name = "ServiceProductLineName",
                         Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_ServiceProductLine_Name,
                        IsReadOnly = true
                    },new KeyValuesColumnItem {
                        Name = "ProductLineType",
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerBusinessPermit_ProductLineType,
                        IsReadOnly = true,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new KeyValuesColumnItem{
                        Name="EngineRepairPower",
                        KeyValueItems=this.KeyValueManager[this.kvNames[1]]
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.IsReadOnly = false;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.Deleted += GridView_Deleted;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            //this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.CellValidating += GridView_CellValidating;
            this.GridView.Columns["ServiceProductLineCode"].CellEditTemplate =
                                                      (DataTemplate)XamlReader.
                                                      Load("<DataTemplate xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation' xmlns:telerik='http://schemas.telerik.com/2008/xaml/presentation'><telerik:RadComboBox IsEditable='False' DisplayMemberPath='Value' SelectedValuePath='Key' SelectedValue='{Binding ServiceProductLineId}' /></DataTemplate>");
            this.GridView.PreparingCellForEdit += GridView_PreparingCellForEdit;
            this.GridView.BeginningEdit += GridView_BeginningEdit;

        }

        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            switch(e.Cell.Column.UniqueName) {
                case "EngineRepairPower":
                    var dealerBusinessPermit = e.Row.DataContext as DealerBusinessPermit;
                    if(dealerBusinessPermit != null && dealerBusinessPermit.ProductLineType == (int)DcsServiceProductLineViewProductLineType.发动机产品线 && (dealerBusinessPermit.ServiceProductLineCode == "OM-FDJ-ISGFGL" || dealerBusinessPermit.ServiceProductLineCode == "OM-FDJ-ISGGL"))
                        e.Cancel = false;
                    else
                        e.Cancel = true;
                    break;
            }
        }

        void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {

            var dealerServiceInfo = this.DataContext as DealerServiceInfoForBranchDataEditView;

            if(dealerServiceInfo == null)
                return;

            var a1111 = this.GridView.SelectedItems.Cast<DealerBusinessPermit>().ToArray();
            foreach(var businessPermit in a1111) {
                dealerServiceInfo.DealerBusinessPermits.Remove(businessPermit);
            }
        }

        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
            if(e.Cell.Column.UniqueName.Equals("ServiceProductLineCode")) {
                var dealerBusinessPermit = e.Row.DataContext as DealerBusinessPermit;
                if(dealerBusinessPermit == null)
                    return;
                var comboBox = e.EditingElement as RadComboBox;
                if(comboBox == null)
                    return;
                var serviceProductLine = comboBox.SelectedItem as KeyValuePair;
                if(serviceProductLine == null)
                    return;
                var serviceProductLineView = serviceProductLine.UserObject as ServiceProductLineView;
                if(serviceProductLineView == null)
                    return;
                var selecteServiceProductLineEntity = ServiceProductLineViews.FirstOrDefault(r => r.ProductLineId == serviceProductLineView.ProductLineId && serviceProductLineView.ProductLineType == r.ProductLineType);
                if(selecteServiceProductLineEntity == null)
                    return;
                dealerBusinessPermit.ProductLineType = (int)selecteServiceProductLineEntity.ProductLineType;//TODO 设计类型错误，待修正
                //if(selecteServiceProductLineEntity.ProductLineType == (int)DcsServiceProductLineViewProductLineType.发动机产品线) {
                //    dealerBusinessPermit.EngineProductLine = selecteServiceProductLineEntity.EngineProductLine;
                //    dealerBusinessPermit.ServiceProductLineName = selecteServiceProductLineEntity.EngineProductLine.EngineName;
                //} 
                //else if(selecteServiceProductLineEntity.ProductLineType == (int)DcsServiceProductLineViewProductLineType.服务产品线) {
                //    dealerBusinessPermit.ServiceProductLine = selecteServiceProductLineEntity.ServiceProductLine;
                //    dealerBusinessPermit.ServiceProductLineName = selecteServiceProductLineEntity.ServiceProductLine.Name;
                //}
                dealerBusinessPermit.BranchId = selecteServiceProductLineEntity.BranchId;
                var dealerServiceInfoForBranchDataEditView = this.DataContext as DealerServiceInfoForBranchDataEditView;
                if(dealerServiceInfoForBranchDataEditView == null)
                    return;
                var quantityError = dealerBusinessPermit.ValidationErrors.FirstOrDefault(r => r.MemberNames.Contains("ServiceProductLineCode"));
                if(quantityError != null)
                    dealerBusinessPermit.ValidationErrors.Remove(quantityError);
                if(dealerServiceInfoForBranchDataEditView.DealerBusinessPermits.Count(ex => ex.ServiceProductLineId == selecteServiceProductLineEntity.ProductLineId && ex.ProductLineType == selecteServiceProductLineEntity.ProductLineType) > 1) {
                    e.IsValid = false;
                    //e.ErrorMessage = string.Format(ChannelsUIStrings.DataGridView_Error_dealerBusinessPermit_ServiceProductLine, selecteServiceProductLineEntity.ProductLineType == (int)DcsServiceProductLineViewProductLineType.服务产品线 ? selecteServiceProductLineEntity.ServiceProductLine.Name : selecteServiceProductLineEntity.EngineProductLine.EngineName);
                }
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerBusinessPermit);
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }


        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("DealerBusinessPermits");
        }
    }
}
