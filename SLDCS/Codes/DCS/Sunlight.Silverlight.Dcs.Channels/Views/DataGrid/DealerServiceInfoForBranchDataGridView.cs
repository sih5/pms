﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using FilterOperator = Sunlight.Silverlight.Core.Model.FilterOperator;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class DealerServiceInfoForBranchDataGridView : DcsDataGridViewBase {
        protected readonly string[] kvNames = {
            "ServicePermission", "MasterData_Status","DealerServiceInfo_ServiceStationType","DlrSerInfo_TrunkNetworkType","Invoice_Type","SaleServiceLayout","ExternalState"
        };

        public DealerServiceInfoForBranchDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },
                    new ColumnItem{
                        Name="ChannelCapability.Name",
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_ChannelCapabilityId
                    }, new ColumnItem {
                        Name = "Dealer.Code",
                        Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_DealerServiceInfo_DealerCode
                    }, new ColumnItem {
                        Name = "Dealer.Name",
                        Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_DealerServiceInfo_DealerName
                    },new ColumnItem {
                        Name = "BusinessCode"
                    },new ColumnItem {
                        Name = "BusinessName"
                    }, new ColumnItem {
                        Name = "Dealer.ShortName"
                    },
                   new ColumnItem{
                        Name="BusinessDivision"
                    }, new ColumnItem {
                        Name = "MarketingDepartment.Name",
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_Agency_MarketingDepartment,
                    },new KeyValuesColumnItem{
                        Name="ServiceStationType",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_ServiceStationType,
                        KeyValueItems=this.KeyValueManager[this.kvNames[2]]
                    },new ColumnItem{
                        Name="Area"
                    },new ColumnItem{
                        Name="RegionType"
                    },new ColumnItem{
                        Name="ServicePermission"
                    },new ColumnItem{
                        Name="AccreditTime"
                    },  new ColumnItem {
                        Name = "HourPhone24",
                        Title= ChannelsUIStrings.DataEditView_Title_DealerServiceInfo_HourPhone24
                    }, new ColumnItem {
                        Name = "HotLine",
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_HotLine
                    },new ColumnItem{
                        Name="Fix"
                    },
                    new ColumnItem{
                        Name="Fax"
                    },
                    new ColumnItem {
                        Name = "Warehouse.Name",
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_DefaultWarehouse
                    },new KeyValuesColumnItem{
                        Name="SaleandServicesiteLayout",
                        KeyValueItems=this.KeyValueManager[this.kvNames[5]]
                    },
                    new ColumnItem{
                        Name="PartReserveAmount",
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_PartReserveAmount
                    },
                    new ColumnItem {
                        Name = "UsedPartsWarehouse.Name",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_UsedPartsWarehouseName
                    }, new KeyValuesColumnItem{
                        Name="MaterialCostInvoiceType",
                        KeyValueItems=this.KeyValueManager[this.kvNames[4]],
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_MaterialCostInvoiceType
                    },
                    new ColumnItem{
                        Name="InvoiceTypeInvoiceRatio",
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_InvoiceTypeInvoiceRatio
                    },new KeyValuesColumnItem{
                        Name="LaborCostCostInvoiceType",
                        KeyValueItems=this.KeyValueManager[this.kvNames[4]],
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_LaborCostCostInvoiceType
                    },
                    new ColumnItem{
                        Name="LaborCostInvoiceRatio",
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_LaborCostInvoiceRatio
                    },
                     new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    },
                    new ColumnItem{
                        Name="PartsSalesCategory.Name",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_PartsSalesCategory_Name
                    }, new ColumnItem{
                        Name="Branch.Name"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerServiceInfo);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.DataPager.PageSize = 200;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override string OnRequestQueryName() {
            return "GetDealerServiceInfoWithDetailsInfo";
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            if(FilterItem == null)
                return base.OnRequestFilterDescriptor(queryName);
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem == null) {
                compositeFilterItem = new CompositeFilterItem();
                compositeFilterItem.Filters.Add(this.FilterItem);
            }
            compositeFilterItem.Filters.Add(new FilterItem {
                IsExact = true,
                MemberName = "BranchId",
                Operator = FilterOperator.IsEqualTo,
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            return compositeFilterItem.ToFilterDescriptor();
        }
    }
}
