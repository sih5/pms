﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class DealerServiceInfoForDealerDataGridView : DcsDataGridViewBase {
        protected readonly string[] kvNames = {
            "ServicePermission", "MasterData_Status","DealerServiceInfo_ServiceStationType","DlrSerInfo_TrunkNetworkType","Invoice_Type","SaleServiceLayout","ExternalState"
        };

        public DealerServiceInfoForDealerDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(DealerServiceInfo);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },
                    new ColumnItem{
                        Name="ChannelCapability.Name",
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_ChannelCapabilityId
                    },
                    new ColumnItem {
                        Name = "Dealer.Code",
                        Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_DealerServiceInfo_DealerCode
                    }, new ColumnItem {
                        Name = "Dealer.Name",
                        Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_DealerServiceInfo_DealerName
                    },new ColumnItem {
                        Name = "BusinessCode"
                    },new ColumnItem {
                        Name = "BusinessName"
                    }, new ColumnItem {
                        Name = "Dealer.ShortName"
                    },
                   new ColumnItem{
                        Name="BusinessDivision"
                    }, new ColumnItem {
                        Name = "MarketingDepartment.Name",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_MarketingDepartmentName
                    },new KeyValuesColumnItem{
                        Name="ServiceStationType",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_ServiceStationType,
                        KeyValueItems=this.KeyValueManager[this.kvNames[2]]
                    },new ColumnItem{
                        Name="Area"
                    },new ColumnItem{
                        Name="RegionType"
                    },new ColumnItem{
                        Name="ServicePermission"
                    },new ColumnItem{
                        Name="AccreditTime"
                    },
                    new ColumnItem{
                        Name="IsOnDuty"
                    },  new ColumnItem {
                        Name = "HourPhone24",
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_HourPhone24
                    }, new ColumnItem {
                        Name = "HotLine",
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_HotLine
                    },new ColumnItem{
                        Name="Fix"
                    },
                    new ColumnItem{
                        Name="Fax"
                    },
                    new ColumnItem {
                        Name = "Warehouse.Name",
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_DefaultWarehouse
                    },new KeyValuesColumnItem{
                        Name="SaleandServicesiteLayout",
                        KeyValueItems=this.KeyValueManager[this.kvNames[5]]
                    },
                    new ColumnItem{
                        Name="PartReserveAmount",
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_PartReserveAmount
                    },                 
                    new KeyValuesColumnItem {
                        Name = "RepairAuthorityGrade",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_RepairAuthorityGrade
                    },new ColumnItem {
                        Name = "PartsManagementCostGrade.Name",
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_PartsManagementCostGradeName
                    }, 
                    new ColumnItem{
                        Name="Grade",
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_Grade
                    } ,
                    new ColumnItem{
                        Name="GradeCoefficient.Grade",
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_GradeCoefficientGrade
                    },
                    new ColumnItem {
                        Name = "OutServiceradii",
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_OutServiceradii
                    },
                    new ColumnItem {
                        Name = "Company.Distance",
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_Distance
                    }, new ColumnItem {
                        Name = "Company.MinServiceCode",
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_MinServiceCode
                    }, new ColumnItem {
                        Name = "Company.MinServiceName",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_MinServiceName
                    },
                  
                    new KeyValuesColumnItem {
                        Name = "TrunkNetworkType",
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_TrunkNetworkType,
                        KeyValueItems = this.KeyValueManager[this.kvNames[3]],
                    }, new ColumnItem {
                        Name = "CenterStack",
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_CenterStack,
                    },
                   
                    new ColumnItem {
                        Name = "UsedPartsWarehouse.Name",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_UsedPartsWarehouseName
                    }, new KeyValuesColumnItem{
                        Name="MaterialCostInvoiceType",
                        KeyValueItems=this.KeyValueManager[this.kvNames[4]],
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_MaterialCostInvoiceType
                    },
                    new ColumnItem{
                        Name="InvoiceTypeInvoiceRatio",
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_InvoiceTypeInvoiceRatio
                    },new KeyValuesColumnItem{
                        Name="LaborCostCostInvoiceType",
                        KeyValueItems=this.KeyValueManager[this.kvNames[4]],
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_LaborCostCostInvoiceType
                    },
                    new ColumnItem{
                        Name="LaborCostInvoiceRatio",
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_LaborCostInvoiceRatio
                    },
                    new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    },
                    new KeyValuesColumnItem {
                        Name = "ExternalState",
                        KeyValueItems = this.KeyValueManager[this.kvNames[6]],
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_ExternalState
                    }, new ColumnItem{
                        Name="PartsSalesCategory.Name",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_PartsSalesCategory_Name
                    }, new ColumnItem{
                        Name="Branch.Name"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override string OnRequestQueryName() {
            return "GetDealerServiceInfoWithDetailsInfo";
        }
    }
}
