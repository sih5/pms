﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class DealerServiceInfoForDetailDataGridView : DcsDataGridViewBase {
        private ObservableCollection<KeyValuePair> branches;

        private ObservableCollection<KeyValuePair> Branches {
            get {
                return this.branches ?? (this.branches = new ObservableCollection<KeyValuePair>());
            }
        }

        private ObservableCollection<KeyValuePair> channelCapabilityNames;

        private ObservableCollection<KeyValuePair> ChannelCapabilityNames {
            get {
                return this.channelCapabilityNames ?? (this.channelCapabilityNames = new ObservableCollection<KeyValuePair>());
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new KeyValuesColumnItem {
                        Title = Utils.GetEntityLocalizedName(typeof(ChannelCapability),"Name"),
                        Name = "ChannelCapabilityId"
                    },new ColumnItem {
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_ChannelCapabilityIsSale,
                        Name = "ChannelCapability.IsSale",
                        IsReadOnly = true
                    },new ColumnItem {
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_ChannelCapabilityIsPart,
                        Name = "ChannelCapability.IsPart",
                        IsReadOnly = true
                    },new ColumnItem {
                        Title  = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_ChannelCapabilityIsService,
                        Name = "ChannelCapability.IsService",
                        IsReadOnly = true
                    }, new KeyValuesColumnItem {
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_Branch_Name,
                        Name = "BranchId"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerServiceInfo);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        private void GridView_Loaded(object sender, RoutedEventArgs e) {
            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;

            domainContext.Load(domainContext.GetBranchesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }

                foreach(var entity in loadOp.Entities) {
                    if(this.Branches.Any(r => r.Key == entity.Id)) {
                        continue;
                    }

                    this.Branches.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name
                    });
                }

                domainContext.Load(domainContext.GetChannelCapabilitiesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOption => {
                    if(loadOp.HasError) {
                        loadOp.MarkErrorAsHandled();
                        return;
                    }
                    foreach(var entity in loadOption.Entities) {
                        if(this.ChannelCapabilityNames.Any(r => r.Key == entity.Id)) {
                            continue;
                        }

                        this.ChannelCapabilityNames.Add(new KeyValuePair {
                            Key = entity.Id,
                            Value = entity.Name
                        });
                    }

                    var gridViewComboBoxColumn = GridView.Columns["BranchId"] as GridViewComboBoxColumn;
                    if(gridViewComboBoxColumn != null) {
                        gridViewComboBoxColumn.ItemsSource = this.Branches;
                        gridViewComboBoxColumn.SelectedValueMemberPath = "Key";
                        gridViewComboBoxColumn.DisplayMemberPath = "Value";
                    }
                    var gridViewComboBoxColumn1 = GridView.Columns["ChannelCapabilityId"] as GridViewComboBoxColumn;
                    if(gridViewComboBoxColumn1 != null) {
                        gridViewComboBoxColumn1.ItemsSource = this.ChannelCapabilityNames;
                        gridViewComboBoxColumn1.SelectedValueMemberPath = "Key";
                        gridViewComboBoxColumn1.DisplayMemberPath = "Value";
                    }
                }, null);
            }, null);
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("DealerServiceInfoes");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.Loaded += this.GridView_Loaded;
        }
    }
}
