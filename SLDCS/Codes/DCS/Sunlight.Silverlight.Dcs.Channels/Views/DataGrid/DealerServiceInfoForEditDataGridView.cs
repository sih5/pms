﻿using System.Linq;
using System.Windows.Data;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Channels.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class DealerServiceInfoForEditDataGridView : DealerServiceInfoForDetailDataGridView {
        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
            this.GridView.CellValidating += GridView_CellValidating;
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var dealerDataEditView = this.DataContext as DealerDataEditView;
            if(dealerDataEditView == null || (dealerDataEditView.DataContext as Dealer) == null)
                return;
            e.NewObject = new DealerServiceInfo {
                Status = (int)DcsMasterDataStatus.有效,
                Dealer = (Dealer)dealerDataEditView.DataContext
            };
        }

        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
            if(this.GridView.Items == null || this.GridView.Items.Count < 1)
                return;

            var items = this.GridView.Items.Cast<DealerServiceInfo>().ToArray();
            if(items == null)
                return;
            var dealerServiceInfo = e.Cell.DataContext as DealerServiceInfo;
            if(dealerServiceInfo == null)
                return;
            if(!e.Cell.Column.UniqueName.Equals("BranchId"))
                return;
            var branchIdError = dealerServiceInfo.ValidationErrors.FirstOrDefault(r => r.MemberNames.Contains("BranchId"));
            if(branchIdError != null)
                dealerServiceInfo.ValidationErrors.Remove(branchIdError);
            //和Original数据做比较
            if((e.NewValue is int) && items.Count(r => r.BranchId == (int)e.NewValue)>1) {
                e.IsValid = false;
                e.ErrorMessage = ChannelsUIStrings.DataGridView_Validation_Dealer_InSameDealerBranchIsUnique;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("DealerServiceInfoes");
        }
    }
}
