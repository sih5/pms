﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class DealerServiceInfoForExpenseAdjustmentBillDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Dealer.Code"
                    }, new ColumnItem {
                        Name = "Dealer.Name"
                    }, new ColumnItem {
                        Name = "Dealer.ShortName"
                    }, new ColumnItem {
                        Name = "Dealer.Company.ProvinceName"
                    }, new ColumnItem {
                        Name = "Dealer.Company.CityName"
                    }, new ColumnItem {
                        Name = "Dealer.Company.CountyName"
                    }, new ColumnItem {
                        Name = "Dealer.CreateTime"
                    }, new ColumnItem {
                        Name = "Dealer.CreatorName"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerServiceInfo);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.ShowGroupPanel = false;
        }

        protected override string OnRequestQueryName() {
            return "GetDealerServiceInfoByEnterPriseId";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            switch(parameterName) {
                case "enterpriseId":
                    return BaseApp.Current.CurrentUserData.EnterpriseId;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
    }
}
