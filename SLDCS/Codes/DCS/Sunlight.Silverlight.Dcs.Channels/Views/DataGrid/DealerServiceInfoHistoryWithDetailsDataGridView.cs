﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class DealerServiceInfoHistoryWithDetailsDataGridView : DcsDataGridViewBase {
        protected readonly string[] KvNames = {
            "ServicePermission","DealerServiceInfo_ServiceStationType","SaleServiceLayout","Invoice_Type","MasterData_Status","DlrSerInfo_TrunkNetworkType"
        };

        public DealerServiceInfoHistoryWithDetailsDataGridView() {
            this.KeyValueManager.Register(this.KvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.KvNames[4]]
                    }, new KeyValuesColumnItem {
                        Name = "AStatus",
                        KeyValueItems = this.KeyValueManager[this.KvNames[4]]
                    },
                    new ColumnItem {
                        Name = "Dealer.Code",
                        Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_DealerServiceInfo_DealerCode
                    }, new ColumnItem {
                        Name = "Dealer.Name",
                        Title = ChannelsUIStrings.QueryPanel_QueryItem_Title_DealerServiceInfo_DealerName
                    }, new ColumnItem {
                        Name = "Dealer.ShortName"
                    },new ColumnItem {
                        Name = "BusinessCode"
                    },new ColumnItem {
                        Name = "ABusinessCode"
                    },new ColumnItem {
                        Name = "BusinessName"
                    },new ColumnItem {
                        Name = "ABusinessName"
                    }, new ColumnItem{
                        Name="BusinessDivision",
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_BusinessDivision
                    },new ColumnItem {
                        Name = "ABusinessDivision"
                    }, new ColumnItem {
                        Name = "MarketingDepartment.Name",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_MarketingDepartmentName
                    },new ColumnItem {
                        Name = "AMarketingDepartment.Name",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_MarketingDepartmentName
                    },new ColumnItem {
                        Name = "ChannelCapability.Name",
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_ChannelCapabilityId
                    },  new ColumnItem {
                        Name = "AChannelCapability.Name",
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_AChannelCapabilityId
                    },new KeyValuesColumnItem{
                        Name="ServiceStationType",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_ServiceStationType,
                        KeyValueItems=this.KeyValueManager[this.KvNames[1]]
                    },new KeyValuesColumnItem{
                        Name="AServiceStationType",
                        KeyValueItems=this.KeyValueManager[this.KvNames[1]]
                    },new ColumnItem{
                        Name="Area",
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_Area
                    },new ColumnItem{
                        Name="AArea"
                    },new ColumnItem{
                        Name="RegionType",
                          Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_RegionType
                    },new ColumnItem{
                        Name="ARegionType"
                    },new ColumnItem {
                        Name = "ServicePermission",
                          Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_ServicePermission
                    },new ColumnItem {
                        Name = "AServicePermission"
                    },new ColumnItem{
                        Name="IsOnDuty",
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_IsOnDuty
                    },new ColumnItem{
                        Name="AIsOnDuty",
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_AIsOnDuty
                    },new ColumnItem{
                        Name="HotLine",
                         Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_HotLine
                    },new ColumnItem{
                        Name="AHotLine"
                    }, new ColumnItem{
                        Name="Fix",
                         Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_Fix
                    },  new ColumnItem{
                        Name="AFix"
                    },new ColumnItem{
                        Name="Fax",
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_Fax
                    }, new ColumnItem{
                        Name="AFax"
                    }, new ColumnItem {
                        Name = "Warehouse.Name",
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_DefaultWarehouse
                    }, new ColumnItem {
                        Name = "AWarehouse.Name",
                         Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_WarehouseName
                    },
                    new KeyValuesColumnItem {
                        Name = "RepairAuthorityGrade",
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]],
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_RepairAuthorityGrade
                    }, new KeyValuesColumnItem {
                        Name = "ARepairAuthorityGrade",
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]]
                    }, new ColumnItem {
                        Name = "PartsManagementCostGrade.Name",
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_PartsManagementCostGradeName
                    },new ColumnItem {
                        Name = "APartsManagementCostGrade.Name",
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_APartsManagingFeeGrade
                    }, new ColumnItem {
                        Name = "Grade",
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_BrandGradeMessageName
                    },new ColumnItem {
                        Name = "AGrade",
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_BrandGradeMessageName
                    },
                    new ColumnItem {
                        Name = "GradeCoefficient.Grade",
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_GradeCoefficientName
                    },new ColumnItem {
                        Name = "AGradeCoefficient.Grade",
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_GradeCoefficientName
                    }, new ColumnItem {
                        Name = "OutServiceradii",
                         Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_OutServiceradii
                    }, new ColumnItem {
                        Name = "AOutServiceradii"
                    },new ColumnItem {
                        Name = "UsedPartsWarehouse.Name",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_UsedPartsWarehouseName
                    },  new ColumnItem {
                        Name = "AUsedPartsWarehouse.Name",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_UsedPartsWarehouseName
                    }, new ColumnItem {
                        Name = "PartReserveAmount"
                    }, new ColumnItem {
                        Name = "APartReserveAmount"
                    },  new KeyValuesColumnItem {
                        Name = "MaterialCostInvoiceType",
                        KeyValueItems = this.KeyValueManager[this.KvNames[3]],
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_MaterialCostInvoiceType
                    }, new KeyValuesColumnItem {
                        Name = "AMaterialCostInvoiceType",
                        KeyValueItems = this.KeyValueManager[this.KvNames[3]]
                    }, new KeyValuesColumnItem {
                        Name = "LaborCostCostInvoiceType",
                        KeyValueItems = this.KeyValueManager[this.KvNames[3]],
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_LaborCostCostInvoiceType
                    }, new KeyValuesColumnItem {
                        Name = "ALaborCostCostInvoiceType",
                        KeyValueItems = this.KeyValueManager[this.KvNames[3]]
                    },new ColumnItem {
                        Name = "LaborCostInvoiceRatio",
                          Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_LaborCostInvoiceRatio
                    },new ColumnItem {
                        Name = "ALaborCostInvoiceRatio"
                    },new ColumnItem {
                        Name = "InvoiceTypeInvoiceRatio",
                          Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfoHistory_InvoiceTypeInvoiceRatio
                    },new ColumnItem {
                        Name = "AInvoiceTypeInvoiceRatio"
                    },new KeyValuesColumnItem {
                        Name = "SaleandServicesiteLayout",
                        KeyValueItems = this.KeyValueManager[this.KvNames[2]],
                    },new KeyValuesColumnItem {
                        Name = "ASaleandServicesiteLayout",
                          KeyValueItems = this.KeyValueManager[this.KvNames[2]]
                    },new ColumnItem {
                        Name = "HourPhone24",
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_HourPhone24
                    },new ColumnItem {
                        Name = "A24HourPhone",
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_A24HourPhone
                    },new KeyValuesColumnItem {
                        Name = "TrunkNetworkType",
                        KeyValueItems = this.KeyValueManager[this.KvNames[5]],
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_TrunkNetworkType
                    },new KeyValuesColumnItem {
                        Name = "ATrunkNetworkType",
                        KeyValueItems = this.KeyValueManager[this.KvNames[5]],
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_ATrunkNetworkType
                    },new ColumnItem {
                        Name = "CenterStack",
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_CenterStack
                    },new ColumnItem {
                        Name = "ACenterStack",
                        Title= ChannelsUIStrings.DataGridView_ColumnItem_Title_DealerServiceInfo_ACenterStack
                    },new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    },new ColumnItem{
                        Name="PartsSalesCategory.Name",
                        Title=ChannelsUIStrings.DataGridView_ColumnItem_Title_Dealer_PartsSalesCategory_Name
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerServiceInfoHistory);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override string OnRequestQueryName() {
            return "GetDealerServiceInfoHistoryQuery";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.ShowGroupPanel = false;
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            if(FilterItem == null)
                return base.OnRequestFilterDescriptor(queryName);
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem == null) {
                compositeFilterItem = new CompositeFilterItem();
                compositeFilterItem.Filters.Add(this.FilterItem);
            }
            compositeFilterItem.Filters.Add(new FilterItem {
                IsExact = true,
                MemberName = "BranchId",
                Operator = Sunlight.Silverlight.Core.Model.FilterOperator.IsEqualTo,
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            return compositeFilterItem.ToFilterDescriptor();
        }
    }
}
