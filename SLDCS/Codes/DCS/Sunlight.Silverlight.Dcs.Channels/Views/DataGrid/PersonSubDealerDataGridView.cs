﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class PersonSubDealerDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SubDealer.Code",
                    }, new ColumnItem {
                        Name = "SubDealer.Name"
                    }, new ColumnItem {
                        Name = "Personnel.Name"
                    }, new ColumnItem {
                        Name = "Personnel.CorporationName"
                    }, new ColumnItem {
                        Name = "Personnel.CellNumber"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PersonSubDealer);
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }

        protected override string OnRequestQueryName() {
            return "GetPersonSubDealerWithDetail";
        }
    }
}
