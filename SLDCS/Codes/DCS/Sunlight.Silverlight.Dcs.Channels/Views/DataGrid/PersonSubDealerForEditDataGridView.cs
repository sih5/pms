﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Channels.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class PersonSubDealerForEditDataGridView : DcsDataGridViewBase {
        private RadWindow radQueryWindow;

        private DcsMultiPopupsQueryWindowBase personnelQueryWindow;

        private DcsMultiPopupsQueryWindowBase PersonnelQueryWindow {
            get {
                if(this.personnelQueryWindow == null) {
                    this.personnelQueryWindow = DI.GetQueryWindow("PersonnelForMultiSelect") as DcsMultiPopupsQueryWindowBase;
                    this.personnelQueryWindow.SelectionDecided += personnelQueryWindow_SelectionDecided;
                    this.personnelQueryWindow.Loaded += personnelQueryWindow_Loaded;
                }
                return this.personnelQueryWindow;
            }
        }

        private void personnelQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null)
                return;
            var filterItem = new FilterItem();
            filterItem.MemberName = "CorporationId";
            filterItem.MemberType = typeof(int);
            filterItem.Operator = FilterOperator.IsEqualTo;
            filterItem.Value = BaseApp.Current.CurrentUserData.EnterpriseId;
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", filterItem);
            queryWindow.ExchangeData(null, "RefreshQueryResult", null);
        }

        private void personnelQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var queryWindowpersonnel = queryWindow.SelectedEntities.Cast<Personnel>();
            if(queryWindowpersonnel == null)
                return;

            var dataEditView = this.DataContext as PersonSubDealerDataEditView;
            if(dataEditView == null)
                return;
            if(dataEditView.PersonSubDealers.Any(ex => queryWindowpersonnel.Any(ep => ep.Id == ex.PersonId))) {
                UIHelper.ShowNotification(ChannelsUIStrings.DataEditView_Error_PersonSubDealer_PersonnelName);
                return;
            }
            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            var maxSerialNumber = dataEditView.PersonSubDealers.Count;
            foreach(var person in queryWindowpersonnel) {
                domainContext.Load(domainContext.GetPersonnelsQuery().Where(d => d.Id == person.Id), loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    var personnels = loadOp.Entities.SingleOrDefault();
                    maxSerialNumber++;
                    if(personnels != null) {
                        personnels.SequeueNumber = maxSerialNumber;
                        dataEditView.PersonSubDealers.Add(new PersonSubDealer {
                            PersonId = person.Id,
                            Personnel = personnels
                        });
                    }

                }, null);
            }
        }

        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = this.PersonnelQueryWindow,
                    Header = ChannelsUIStrings.QueryPanel_Title_Personnel,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen
                });
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PersonSubDealers");
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem{
                        Name = "Personnel.SequeueNumber",
                        Title = ChannelsUIStrings.DataGridView_ColumnItem_Title_Personnel_SequeueNumber,
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "Personnel.Name",
                        IsReadOnly=true
                    },new ColumnItem {
                        Name = "Personnel.CorporationName",
                    }, new ColumnItem {
                        Name = "Personnel.CellNumber",
                        IsReadOnly=true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PersonSubDealer);
            }
        }

        protected override void OnControlsCreated() {
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
            this.GridView.Deleted -= GridView_Deleted;
            this.GridView.Deleted += GridView_Deleted;
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            base.OnControlsCreated();
        }
        private void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var dataEditView = this.DataContext as PersonSubDealerDataEditView;
            if(dataEditView == null)
                return;
            var serialNumber = 1;
            foreach(var personSubDealers in dataEditView.PersonSubDealers) {
                personSubDealers.Personnel.SequeueNumber = serialNumber;
                serialNumber++;
            }

            foreach(var item in e.Items.Cast<PersonSubDealer>()) {
                if(item.Can删除登陆人员与二级站关系)
                    item.删除登陆人员与二级站关系();
            }
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            RadQueryWindow.ShowDialog();
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
    }
}
