﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class SubDealerDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames ={
            "MasterData_Status"
        };

        public SubDealerDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },
                     new ColumnItem {
                        Name = "Code"
                    },new ColumnItem {
                        Name = "Name"
                    },  new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "Manager"
                    }, new ColumnItem {
                        Name = "ManagerPhoneNumber",
                    }, new ColumnItem {
                        Name = "ManagerMobile",
                    }, new ColumnItem {
                        Name = "ManagerMail",
                    }, new ColumnItem {
                        Name = "Remark",
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SubDealer);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetSubDealers";
        }
    }
}
