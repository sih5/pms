﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class SubdealerForBranchDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames ={
            "BaseData_Status","SubDealerType"
        };

        public SubdealerForBranchDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },
                     new ColumnItem {
                        Name = "Code"
                    },new ColumnItem {
                        Name = "Name"
                    }, new ColumnItem {
                        Name = "Dealer.Code",
                        Title= ChannelsUIStrings.DataEditView_Title_DealerServiceInfo_DealerCode
                    },new ColumnItem {
                        Name = "Dealer.Name",
                        Title= ChannelsUIStrings.DataEditView_Title_DealerServiceInfo_DealerName
                    }, new KeyValuesColumnItem {
                        Name = "SubDealerType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title= ChannelsUIStrings.DataEditView_Title_DealerServiceInfo_SubDealerType
                    }, new ColumnItem {
                        Name = "DealerManager"
                    }, 
                    //new ColumnItem {
                    //    Name = "CreateTime"
                    //}, 
                    new ColumnItem {
                        Name = "Manager"
                    }, new ColumnItem {
                        Name = "ManagerPhoneNumber",
                    }, new ColumnItem {
                        Name = "ManagerMobile",
                    }, new ColumnItem {
                        Name = "ManagerMail",
                    },new ColumnItem {
                        Name = "Address",
                       Title = ChannelsUIStrings.DataGridColumn_Title_SubDealer_Address
                    }, new ColumnItem {
                        Name = "Remark",
                    },new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    } , new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    } ,  new ColumnItem {   
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime"
                    }     
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SubDealer);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询二级站信息分公司";
        }
    }
}

