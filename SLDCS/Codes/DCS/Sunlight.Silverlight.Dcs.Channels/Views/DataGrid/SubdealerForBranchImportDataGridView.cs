﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class SubdealerForBranchImportDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = { "SubDealerType" };

        public SubdealerForBranchImportDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new ColumnItem {
                        Name = "Code"
                    },new ColumnItem {
                        Name = "Name"
                    }, new ColumnItem {
                        Name = "DealerCode",
                        Title= ChannelsUIStrings.DataEditView_Title_DealerServiceInfo_DealerCode
                    },new ColumnItem {
                        Name = "DealerName",
                        Title = ChannelsUIStrings.DataEditView_Title_DealerServiceInfo_DealerName
                    }, new KeyValuesColumnItem {
                        Name = "SubDealerType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title= ChannelsUIStrings.DataEditView_Title_DealerServiceInfo_SubDealerType
                    }, new ColumnItem {
                        Name = "Manager"
                    }, new ColumnItem {
                        Name = "DealerManager"
                    }, new ColumnItem {
                        Name = "ManagerPhoneNumber",
                    },  new ColumnItem {
                        Name = "ManagerMail",
                    }, new ColumnItem {
                        Name = "ManagerMobile",
                    },new ColumnItem {
                        Name = "Address",
                       Title = ChannelsUIStrings.DataGridColumn_Title_SubDealer_Address
                    },new ColumnItem {
                        Name = "Remark",
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SubDealer);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("SubDealers");
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }


    }
}
