﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Channels.Views {
    [PageMeta("Channels", "Dealer", "DealerForCAM", ActionPanelKeys = new[] {
        CommonActionKeys.EDIT_ABANDON_PAUSE_RESUME_EXPORT_IMPORT, "Dealer"
    })]
    public class DealerForCAMManagement : DcsDataManagementViewBase {
        private DataEditViewBase dataEditView;
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditViewDealerTogetherForCAM;
        private DataEditViewBase dataEditViewDealerDetailForCAM;
        private DataEditViewBase dataEditViewDealerDetailTogetherForCAM;
        private const string DATA_EDIT_VIEW_DEALERTOGETHERFORCAM = "_DataEditViewDealerTogetherForCAM_";
        private const string DATA_EDIT_VIEW_DEALERDETAILFORCAM = "_DataEditViewDealerDetailForCAM_";
        private const string DATA_EDIT_VIEW_DEALERDETAILTOGETHERFORCAM = "_DataEditViewDealerDetailTogetherForCAM_";

        public DealerForCAMManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = ChannelsUIStrings.DataManagementView_Title_Dealer;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_DEALERTOGETHERFORCAM, () => this.DataEditViewDealerTogetherForCAM);
            this.RegisterView(DATA_EDIT_VIEW_DEALERDETAILTOGETHERFORCAM, () => this.DataEditViewDealerDetailTogetherForCAM);
            this.RegisterView(DATA_EDIT_VIEW_DEALERDETAILFORCAM, () => this.DataEditViewDealerDetailForCAM);
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("Dealer"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("DealerForCAM");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataEditViewDealerTogetherForCAM {
            get {
                if(this.dataEditViewDealerTogetherForCAM == null) {
                    this.dataEditViewDealerTogetherForCAM = DI.GetDataEditView("DealerTogetherForCAM");
                    this.dataEditViewDealerTogetherForCAM.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditViewDealerTogetherForCAM.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewDealerTogetherForCAM;
            }
        }

        private DataEditViewBase DataEditViewDealerDetailForCAM {
            get {
                if(this.dataEditViewDealerDetailForCAM == null) {
                    this.dataEditViewDealerDetailForCAM = DI.GetDataEditView("DealerDetailForCAM");
                    this.dataEditViewDealerDetailForCAM.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewDealerDetailForCAM;
            }
        }

        private DataEditViewBase DataEditViewDealerDetailTogetherForCAM {
            get {
                if(this.dataEditViewDealerDetailTogetherForCAM == null) {
                    this.dataEditViewDealerDetailTogetherForCAM = DI.GetDataEditView("DealerDetailTogetherForCAM");
                    this.dataEditViewDealerDetailTogetherForCAM.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewDealerDetailTogetherForCAM;
            }
        }

         private void ResetEditView(){
            this.dataEditView = null;
            this.dataEditViewDealerTogetherForCAM = null;
            this.dataEditViewDealerDetailForCAM = null;
            this.dataEditViewDealerDetailTogetherForCAM = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "Dealer"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterAll = filterItem as CompositeFilterItem;
            if(compositeFilterAll == null)
                return;
            ClientVar.ConvertTime(compositeFilterAll);
            this.DataGridView.FilterItem = compositeFilterAll;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var dealer = this.DataEditView.CreateObjectToEdit<Dealer>();
                    dealer.Status = (int)DcsMasterDataStatus.有效;
                    dealer.Company = new Company();
                    dealer.Company.Type = (int)DcsCompanyType.服务站;
                    dealer.Company.Status = (int)DcsMasterDataStatus.有效;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.DataContext = null;
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(ChannelsUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<Dealer>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废经销商基本信息)
                                entity.作废经销商基本信息();
                            this.ExecuteSerivcesMethod(ChannelsUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORT:
                    ((DcsDataGridViewBase)this.DataGridView).ExportData();
                    break;
                case CommonActionKeys.IMPORT:
                    //TODO: 导入暂时不实现
                    break;
                case CommonActionKeys.PAUSE:
                    DcsUtils.Confirm(ChannelsUIStrings.DataManagementView_Confirm_Pause, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<Dealer>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can停用经销商信息)
                                entity.停用经销商信息();
                            this.ExecuteSerivcesMethod(ChannelsUIStrings.DataManagementView_Notification_PauseSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.RESUME:
                    DcsUtils.Confirm(ChannelsUIStrings.DataManagementView_Confirm_Resume, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<Dealer>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can恢复经销商基本信息)
                                entity.恢复经销商基本信息();
                            this.ExecuteSerivcesMethod(ChannelsUIStrings.DataManagementView_Notification_ResumeSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "AddTogether":
                    var dealerTogether = this.DataEditViewDealerTogetherForCAM.CreateObjectToEdit<Dealer>();
                    dealerTogether.Status = (int)DcsMasterDataStatus.有效;
                    dealerTogether.DealerServiceExt = new DealerServiceExt {
                        Status = (int)DcsMasterDataStatus.有效,
                        HasBranch = (int)DcsYesOrNo.无
                    };
                    dealerTogether.Company = new Company {
                        Status = (int)DcsMasterDataStatus.有效
                    };
                    this.SwitchViewTo(DATA_EDIT_VIEW_DEALERTOGETHERFORCAM);
                    break;
                case "EditTogether":
                    this.DataEditViewDealerTogetherForCAM.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_DEALERTOGETHERFORCAM);
                    break;
                case "Detail":
                    this.DataEditViewDealerDetailForCAM.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_DEALERDETAILFORCAM);
                    break;
                case "DetailTogether":
                    this.DataEditViewDealerDetailTogetherForCAM.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_DEALERDETAILTOGETHERFORCAM);
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                case "AddTogether":
                    return true;
                case CommonActionKeys.EDIT:
                case "EditTogether":
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.PAUSE:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<Dealer>().Any(r => r.Status == (int)DcsMasterDataStatus.有效);
                case CommonActionKeys.RESUME:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<Dealer>().Any(r => r.Status == (int)DcsMasterDataStatus.停用);
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.IMPORT:
                    //TODO: 导入暂时不实现
                    return false;
                case "Detail":
                case "DetailTogether":
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return true;
                default:
                    return false;
            }
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }
    }
}
