﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Channels.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Channels.Views {
    [PageMeta("Common", "Dealer", "DealerForDealer", ActionPanelKeys = new[] {
        "DealerForDealer"
    })]
    public class DealerForDealerManagement : DcsDataManagementViewBase {
        private const string DATA_EDIT_VIEW_DEALERTOGETHER = "_DataEditViewDealerTogether_";
        private const string DATA_EDIT_VIEW_DEALERDETAILTOGETHER = "_DataEditViewDealerDetailTogether_";
        private DataEditViewBase dataEditViewDealerTogether;
        private DataGridViewBase dataGridView;
        public DealerForDealerManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = ChannelsUIStrings.DataManagementView_Title_Dealer;
        }

        private DataEditViewBase dataEditViewDealerDetailTogether;

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW_DEALERTOGETHER, () => this.DataEditViewDealerTogether);
            this.RegisterView(DATA_EDIT_VIEW_DEALERDETAILTOGETHER, () => this.DataEditViewDealerDetailTogether);
        }
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("Dealer"));
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "DealerForDealer"
                };
            }
        }

        private DataEditViewBase DataEditViewDealerDetailTogether {
            get {
                if(this.dataEditViewDealerDetailTogether == null) {
                    this.dataEditViewDealerDetailTogether = DI.GetDataEditView("DealerDetailTogetherForDealer");
                    this.dataEditViewDealerDetailTogether.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewDealerDetailTogether;
            }
        }

        private DataEditViewBase DataEditViewDealerTogether {
            get {
                if(this.dataEditViewDealerTogether == null) {
                    this.dataEditViewDealerTogether = DI.GetDataEditView("DealerForDealer");
                    ((DealerForDealerDataEditView)this.dataEditViewDealerTogether).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditViewDealerTogether.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewDealerTogether;
            }
        }
        private void ResetEditView() {
            this.dataEditViewDealerTogether = null;
            this.dataEditViewDealerDetailTogether = null;
        }
        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case "DetailTogether":
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return true;
                case "EditTogether":
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<Dealer>().Any(r => r.Status == (int)DcsMasterDataStatus.有效);
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case "EditTogether":
                    this.DataEditViewDealerTogether.DataContext = null;
                    this.DataEditViewDealerTogether.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_DEALERTOGETHER);
                    break;
                case "DetailTogether":
                    this.DataEditViewDealerDetailTogether.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_DEALERDETAILTOGETHER);
                    break;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                compositeFilterItem = filterItem as CompositeFilterItem;
                ClientVar.ConvertTime(compositeFilterItem);
            } else
                compositeFilterItem.Filters.Add(filterItem);
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "Company.Id",
                Operator = FilterOperator.IsEqualTo,
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
