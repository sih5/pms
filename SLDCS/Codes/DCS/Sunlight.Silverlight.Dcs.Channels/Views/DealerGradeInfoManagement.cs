﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Channels.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using System.Linq;
using Sunlight.Silverlight.ViewModel;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;

namespace Sunlight.Silverlight.Dcs.Channels.Views {
    [PageMeta("Common", "Dealer", "DealerGradeInfo", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_EXPORT
    })]
    public class DealerGradeInfoManagement : DcsDataManagementViewBase {

        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditViewforAdd;
        private const string DATA_EDIT_VIEW_ADD = "_DataEditViewforAdd_";
        private DcsDomainContext dcsDomainContext = new DcsDomainContext();
        public DealerGradeInfoManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = ChannelsUIStrings.DataManagementView_Title_DealerGradeInfo;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("DealerGradeInfo"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_ADD,()=>this.DataEditViewforAdd);
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewforAdd = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("DealerGradeInfo");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
         private DataEditViewBase DataEditViewforAdd {
            get {
                if(this.dataEditViewforAdd == null) {
                    this.dataEditViewforAdd = DI.GetDataEditView("DealerGradeInfoForAdd");
                    this.dataEditViewforAdd.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditViewforAdd.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewforAdd;
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "DealerGradeInfo"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    ((DealerGradeInfoForAddDataEditView)this.DataEditViewforAdd).InitializeAddData();
                    this.SwitchViewTo(DATA_EDIT_VIEW_ADD);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(ChannelsUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var dealerGradeInfo = this.DataGridView.SelectedEntities.Cast<DealerGradeInfo>().SingleOrDefault();
                        if(dealerGradeInfo == null)
                            return;
                        if(dealerGradeInfo.Can作废品牌分级信息)
                            dealerGradeInfo.作废品牌分级信息();
                        this.DataGridView.DomainContext.SubmitChanges(submitOp => {
                            if(submitOp.HasError) {
                                if(!submitOp.IsErrorHandled)
                                    submitOp.MarkErrorAsHandled();
                                DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                return;
                            }
                            UIHelper.ShowNotification(ChannelsUIStrings.DataManagementView_Notification_AbandonSuccess);
                        }, null);
                    });
                    break;
                case CommonActionKeys.EXPORT:
                  
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<DealerGradeInfo>().Select(r => r.Id).ToArray();
                        this.dcsDomainContext.ExportDealerGradeInfo(ids, null, null, null, null, null, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                                ShellViewModel.Current.IsBusy = false;
                            }
                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                ShellViewModel.Current.IsBusy = false;
                            }
                        }, null);
                    } else {
                        var compositeFilterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(compositeFilterItem != null) {
                            var grade = compositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "GradeName") == null ? null : compositeFilterItem.Filters.Single(r => r.MemberName == "GradeName").Value as string;
                            var brandId = compositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSalesCategoryId") == null ? null : compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                            var status = compositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "Status") == null ? null : compositeFilterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                            var createTime = compositeFilterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                            DateTime? startDateTime = null;
                            DateTime? endDateTime = null;
                            if(createTime != null) {
                                startDateTime = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                endDateTime = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                            }
                            this.dcsDomainContext.ExportDealerGradeInfo(new int[] { }, brandId, grade, status, startDateTime, endDateTime, loadOp => {
                                if(loadOp.HasError)
                                    return;
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                    ShellViewModel.Current.IsBusy = false;
                                }

                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                    ShellViewModel.Current.IsBusy = false;
                                }
                            }, null);
                        }
                    }
                    break;
                default:
                    break;
                   
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<DealerGradeInfo>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsBaseDataStatus.有效;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
    }
}
