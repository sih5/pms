﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Channels.Views {
    [PageMeta("Common", "Dealer", "DealerKeyEmployeeQuery", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class DealerKeyEmployeeQuery : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();

        public DealerKeyEmployeeQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = ChannelsUIStrings.DataManagementView_Title_DealerKeyEmployee;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("DealerKeyEmployeeWithDetails"));
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var composite = filterItem as CompositeFilterItem;
            if(composite == null)
                return;
            ClientVar.ConvertTime(composite);
            this.DataGridView.FilterItem = composite;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "DealerKeyEmployee"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<DealerKeyEmployee>().Select(r => r.Id).ToArray();
                        this.dcsDomainContext.ExportDealerKeyEmployee(ids, null, null, null, null, null,null,null, null, null, null, null, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                                ShellViewModel.Current.IsBusy = false;
                            }

                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                ShellViewModel.Current.IsBusy = false;
                            }
                        }, null);
                    } else {
                        var compositeFilterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(compositeFilterItem != null) {
                            var branchId = compositeFilterItem.Filters.Single(r => r.MemberName == "DealerKeyPosition.BranchId").Value as int?;
                            var partsSalesCategoryId = compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                            var dealerCode = compositeFilterItem.Filters.Single(r => r.MemberName == "Dealer.Code").Value as string;
                            var dealerName = compositeFilterItem.Filters.Single(r => r.MemberName == "Dealer.Name").Value as string;
                            var keyPositionId = compositeFilterItem.Filters.Single(r => r.MemberName == "KeyPositionId").Value as int?;
                            var isOnJob = compositeFilterItem.Filters.Single(r => r.MemberName == "IsOnJob").Value as int?;
                            var isKeyPositions = compositeFilterItem.Filters.Single(r => r.MemberName == "IsKeyPositions").Value as bool?;
                            var name = compositeFilterItem.Filters.Single(r => r.MemberName == "Name").Value as string;
                            var dptname = compositeFilterItem.Filters.Single(r => r.MemberName == "MarketingDepartment").Value as string;
                            var createTime = compositeFilterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                            DateTime? startDateTime = null;
                            DateTime? endDateTime = null;
                            if(createTime != null) {
                                startDateTime = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                endDateTime = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                            }
                            this.dcsDomainContext.ExportDealerKeyEmployee(new int[] {
                            }, branchId, partsSalesCategoryId, dealerCode, dealerName, keyPositionId,isOnJob,isKeyPositions, name, dptname, startDateTime, endDateTime, loadOp => {
                                if(loadOp.HasError)
                                    return;
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                    ShellViewModel.Current.IsBusy = false;
                                }

                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                    ShellViewModel.Current.IsBusy = false;
                                }
                            }, null);
                        }
                    }
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
    }
}

