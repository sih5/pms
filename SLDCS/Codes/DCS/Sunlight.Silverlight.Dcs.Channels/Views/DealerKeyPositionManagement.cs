﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Channels.Views {
    [PageMeta("Common", "Dealer", "DealerKeyPosition", ActionPanelKeys = new[] {
        "DealerKeyPosition"
    })]
    public class DealerKeyPositionManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditViewForRoot;
        protected const string DATA_EDITROOT_VIEW = "_DataEditRootView_";
        public DealerKeyPositionManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = ChannelsUIStrings.DataManagementView_Title_DealerKeyPosition;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("DealerKeyPosition"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("DealerKeyPosition");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private DataEditViewBase DataEditViewForRoot {
            get {
                if(this.dataEditViewForRoot == null) {
                    this.dataEditViewForRoot = DI.GetDataEditView("DealerKeyPositionForRoot");
                    this.dataEditViewForRoot.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditViewForRoot.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewForRoot;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDITROOT_VIEW, () => this.DataEditViewForRoot);
        }

         private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewForRoot = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "DealerKeyPosition"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var dealerKeyPosition = this.DataEditView.CreateObjectToEdit<DealerKeyPosition>();
                    dealerKeyPosition.Status = (int)DcsMasterDataStatus.有效;
                    dealerKeyPosition.BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    dealerKeyPosition.IsKeyPositions = false;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(ChannelsUIStrings.DataManagementView_Confirm_Abandon, () => this.DataGridView.UpdateSelectedEntities(entity => ((DealerKeyPosition)entity).Status = (int)DcsBaseDataStatus.作废, () => {
                        UIHelper.ShowNotification(ChannelsUIStrings.DataManagementView_Notification_AbandonSuccess);
                        this.CheckActionsCanExecute();
                    }));
                    break;
                case CommonActionKeys.EXPORT:
                    ((DcsDataGridViewBase)this.DataGridView).ExportData();
                    break;
                case "KeyPositionRoot":
                    this.DataEditViewForRoot.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDITROOT_VIEW);
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                     if(this.DataGridView.SelectedEntities == null)
                        return false;
                     var editentities = this.DataGridView.SelectedEntities.Cast<DealerKeyPosition>().ToArray();
                    if(editentities.Length != 1)
                        return false;
                    return editentities[0].Status == (int)DcsMasterDataStatus.有效 && editentities[0].IsKeyPositions == false;
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<DealerKeyPosition>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsMasterDataStatus.有效;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case "KeyPositionRoot":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities1 = this.DataGridView.SelectedEntities.Cast<DealerKeyPosition>().ToArray();
                    if(entities1.Length != 1)
                        return false;
                    return entities1[0].Status == (int)DcsMasterDataStatus.有效;

                default:
                    return false;
            }
        }
    }
}

