﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Channels.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using System.ServiceModel.DomainServices.Client;

namespace Sunlight.Silverlight.Dcs.Channels.Views {
    [PageMeta("Common", "Company", "Dealer", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_PAUSE_RESUME_EXPORT_IMPORT, "Dealer"
    })]
    public class DealerManagement : DcsDataManagementViewBase {
        private DataEditViewBase dataEditView;
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditViewDealerTogether;
        private DataEditViewBase dataEditViewDealerDetail;
        private DataEditViewBase dataEditViewDealerDetailTogether;
        private DataEditViewBase batchBindingTelDataEditView;
        private DataEditViewBase dealerInfoImport;
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private const string DATA_EDIT_VIEW_DEALERTOGETHER = "_DataEditViewDealerTogether_";
        private const string DATA_EDIT_VIEW_DEALERDETAIL = "_DataEditViewDealerDetail_";
        private const string DATA_EDIT_VIEW_DEALERDETAILTOGETHER = "_DataEditViewDealerDetailTogether_";
        private const string BATCHBINDING_DATA_EDIT_VIEW = "_DataEditViewBatchBindingTel_";
        private const string DATA_EDIT_VIEW_MAINIMPORT = "_DataEditViewMainImport_";
        private const string DATA_EDIT_IMPORT = "_DataEditImport_";

        public DealerManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = ChannelsUIStrings.DataManagementView_Title_Dealer;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_DEALERTOGETHER, () => this.DataEditViewDealerTogether);
            this.RegisterView(DATA_EDIT_VIEW_DEALERDETAILTOGETHER, () => this.DataEditViewDealerDetailTogether);
            this.RegisterView(DATA_EDIT_VIEW_DEALERDETAIL, () => this.DataEditViewDealerDetail);
            this.RegisterView(BATCHBINDING_DATA_EDIT_VIEW, () => this.BatchBindingTelDataEditView);
            this.RegisterView(DATA_EDIT_VIEW_MAINIMPORT, () => this.DealerInfoImport);
            this.RegisterView(DATA_EDIT_IMPORT, () => this.DataEditImport);
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("Dealer"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("Dealer");
                    ((DealerDataEditView)this.dataEditView).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DealerInfoImport {
            get {
                if(this.dealerInfoImport == null) {
                    this.dealerInfoImport = DI.GetDataEditView("DealerInfoImport");
                    this.dealerInfoImport.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dealerInfoImport;
            }
        }

        private DataEditViewBase dataEditImport;
        private DataEditViewBase DataEditImport {
            get {
                if(this.dataEditImport == null) {
                    this.dataEditImport = DI.GetDataEditView("DataEditImport");
                    this.dataEditImport.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataEditImport;
            }

        }

        //private void dataEditImport_EditCancelled(object sender, EventArgs e) {
        //    this.SwitchViewTo(DATA_GRID_VIEW);
        //}

        private DataEditViewBase DataEditViewDealerTogether {
            get {
                if(this.dataEditViewDealerTogether == null) {
                    this.dataEditViewDealerTogether = DI.GetDataEditView("DealerTogether");
                    ((DealerTogetherDataEditView)this.dataEditViewDealerTogether).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditViewDealerTogether.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewDealerTogether;
            }
        }

        private DataEditViewBase DataEditViewDealerDetail {
            get {
                if(this.dataEditViewDealerDetail == null) {
                    this.dataEditViewDealerDetail = DI.GetDataEditView("DealerDetail");
                    this.dataEditViewDealerDetail.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewDealerDetail;
            }
        }

        private DataEditViewBase DataEditViewDealerDetailTogether {
            get {
                if(this.dataEditViewDealerDetailTogether == null) {
                    this.dataEditViewDealerDetailTogether = DI.GetDataEditView("DealerDetailTogether");
                    this.dataEditViewDealerDetailTogether.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewDealerDetailTogether;
            }
        }

        private DataEditViewBase BatchBindingTelDataEditView {
            get {
                if(this.batchBindingTelDataEditView == null) {
                    this.batchBindingTelDataEditView = DI.GetDataEditView("BatchBindingTel");
                    this.batchBindingTelDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.batchBindingTelDataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewDealerTogether =null;
            this.dataEditViewDealerDetail =null;
            this.dataEditViewDealerDetailTogether =null;
            this.batchBindingTelDataEditView =null;
            this.dealerInfoImport = null;
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "Dealer"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            CompositeFilterItem compositeFilter;

            if(!(filterItem is CompositeFilterItem)) {
                compositeFilter = new CompositeFilterItem();
                compositeFilter.Filters.Add(filterItem);
            } else
                compositeFilter = filterItem as CompositeFilterItem;
            if(compositeFilter.Filters.Any(filter => filter.GetType() == typeof(CompositeFilterItem))) {
                var createTimeFilter = compositeFilter.Filters.Single(filter => filter.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                if(createTimeFilter == null)
                    return;
                var dateTime = createTimeFilter.Filters.ElementAt(1).Value as DateTime?;
                if(dateTime.HasValue)
                    createTimeFilter.Filters.ElementAt(1).Value = new DateTime(dateTime.Value.Year, dateTime.Value.Month, dateTime.Value.Day, 23, 59, 59);
            }
            this.DataGridView.FilterItem = compositeFilter;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var dealer = this.DataEditView.CreateObjectToEdit<Dealer>();
                    dealer.Status = (int)DcsMasterDataStatus.有效;
                    dealer.Company = new Company();
                    dealer.Company.Type = (int)DcsCompanyType.服务站;
                    dealer.Company.Status = (int)DcsMasterDataStatus.有效;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.DataContext = null;
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    var entity = this.DataGridView.SelectedEntities.Cast<Dealer>().SingleOrDefault();
                    bool DealerServiceInfoIsEffective = false;
                    dcsDomainContext.Load(dcsDomainContext.GetDealerServiceInfoesQuery().Where(e => e.DealerId == entity.Id), LoadBehavior.RefreshCurrent, loadOp1 => {
                        if(loadOp1.HasError)
                            return;
                        foreach(var DealerServiceInfo in loadOp1.Entities)
                            if(DealerServiceInfo.Status == (int)DcsMasterDataStatus.有效 ||
                                DealerServiceInfo.Status == (int)DcsMasterDataStatus.停用) {
                                DealerServiceInfoIsEffective = true;
                                break;
                            }
                    }, null);
                    DcsUtils.Confirm(ChannelsUIStrings.DataManagementView_Confirm_Abandon, () => {
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废经销商基本信息) {
                                if(!DealerServiceInfoIsEffective) {
                                    entity.作废经销商基本信息();
                                    this.ExecuteSerivcesMethod(ChannelsUIStrings.DataManagementView_Notification_AbandonSuccess);
                                } else {
                                    this.DataGridView.DomainContext.RejectChanges();
                                    UIHelper.ShowAlertMessage(ChannelsUIStrings.DataManagementView_Notification_Dealer_CategoryCantAbandon);
                                }
                            }
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORT:
                    //如果选中一条数据 导出参数为 DealerID 
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var id = this.DataGridView.SelectedEntities.Cast<Dealer>().Select(r => r.Id).First() as int?;
                        this.ExportDealer(id, null, null, null, null, null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var status = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Status") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var name = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Name") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Name").Value as string;
                        var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeStart = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeStart = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        this.ExportDealer(null, code, name, status, createTimeStart, createTimeEnd);
                    }
                    break;
                case CommonActionKeys.IMPORT:
                    //TODO: 导入暂时不实现
                    break;
                case CommonActionKeys.PAUSE:
                    var entity1 = this.DataGridView.SelectedEntities.Cast<Dealer>().SingleOrDefault();
                    bool DealerServiceInfoIsEffective1 = false;
                    dcsDomainContext.Load(dcsDomainContext.GetDealerServiceInfoesQuery().Where(e => e.DealerId == entity1.Id), LoadBehavior.RefreshCurrent, loadOp1 => {
                        if(loadOp1.HasError)
                            return;
                        foreach(var DealerServiceInfo in loadOp1.Entities)
                            if(DealerServiceInfo.Status == (int)DcsMasterDataStatus.有效) {
                                DealerServiceInfoIsEffective1 = true;
                                break;
                            }
                    }, null);
                    DcsUtils.Confirm(ChannelsUIStrings.DataManagementView_Confirm_Pause, () => {
                        if(entity1 == null)
                            return;
                        try {
                            if(entity1.Can停用经销商信息) {
                                if(!DealerServiceInfoIsEffective1) {
                                    entity1.停用经销商信息();
                                    this.ExecuteSerivcesMethod(ChannelsUIStrings.DataManagementView_Notification_PauseSuccess);
                                } else {
                                    this.DataGridView.DomainContext.RejectChanges();
                                    UIHelper.ShowAlertMessage(ChannelsUIStrings.DataManagementView_Notification_Dealer_CategoryCantStopUse);
                                }
                            }
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.RESUME:
                    DcsUtils.Confirm(ChannelsUIStrings.DataManagementView_Confirm_Resume, () => {
                        var entity2 = this.DataGridView.SelectedEntities.Cast<Dealer>().SingleOrDefault();
                        if(entity2 == null)
                            return;
                        try {
                            if(entity2.Can恢复经销商基本信息)
                                entity2.恢复经销商基本信息();
                            this.ExecuteSerivcesMethod(ChannelsUIStrings.DataManagementView_Notification_ResumeSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "AddTogether":
                    var dealerTogether = this.DataEditViewDealerTogether.CreateObjectToEdit<Dealer>();
                    dealerTogether.Status = (int)DcsMasterDataStatus.有效;
                    dealerTogether.DealerServiceExt = new DealerServiceExt {
                        Status = (int)DcsMasterDataStatus.有效,
                        HasBranch = (int)DcsYesOrNo.无
                    };
                    dealerTogether.Company = new Company {
                        Status = (int)DcsMasterDataStatus.有效
                    };
                    this.SwitchViewTo(DATA_EDIT_VIEW_DEALERTOGETHER);
                    break;
                case "EditTogether":
                    this.DataEditView.DataContext = null;
                    this.DataEditViewDealerTogether.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_DEALERTOGETHER);
                    break;
                case "Detail":
                    this.DataEditViewDealerDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_DEALERDETAIL);
                    break;
                case "DetailTogether":
                    this.DataEditViewDealerDetailTogether.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_DEALERDETAILTOGETHER);
                    break;
                case "BatchBindingTel":
                    this.SwitchViewTo(BATCHBINDING_DATA_EDIT_VIEW);
                    break;
                case "BatchImport":
                    this.SwitchViewTo(DATA_EDIT_VIEW_MAINIMPORT);
                    break;
                case "ImportEdit":
                    this.SwitchViewTo(DATA_EDIT_IMPORT);
                    break;
                case "UpdateDistance":
                    string EnterpriseCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                    ShellViewModel.Current.IsBusy = true;
                    this.UpdateDistanceRelation(EnterpriseCode);
                    break;
            }
        }

        public void ExportDealer(int? id, string code, string name, int? status, DateTime? createTimeStart, DateTime? createTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            excelServiceClient.ExportDealerAsync(id, code, name, status, createTimeStart, createTimeEnd);
            excelServiceClient.ExportDealerCompleted -= excelServiceClient_ExportDealerCompleted;
            excelServiceClient.ExportDealerCompleted += excelServiceClient_ExportDealerCompleted;
        }

        private void excelServiceClient_ExportDealerCompleted(object sender, ExportDealerCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
        private void UpdateDistanceRelation(string EnterpriseCode) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.更新站间距离Async(EnterpriseCode);
            this.excelServiceClient.更新站间距离Completed += excelServiceClient_更新站间距离Completed;
            this.excelServiceClient.更新站间距离Completed += excelServiceClient_更新站间距离Completed;
        }

        private void excelServiceClient_更新站间距离Completed(object sender, 更新站间距离CompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            // HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }


        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                case "AddTogether":
                case "BatchImport":
                case "ImportEdit":
                case "UpdateDistance":
                    return true;
                case CommonActionKeys.EDIT:
                case "EditTogether":
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.PAUSE:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<Dealer>().Any(r => r.Status == (int)DcsMasterDataStatus.有效);
                case CommonActionKeys.RESUME:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<Dealer>().Any(r => r.Status == (int)DcsMasterDataStatus.停用);
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.IMPORT:
                    //TODO: 导入暂时不实现
                    return false;
                case "Detail":
                case "DetailTogether":
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return true;
                case "BatchBindingTel":
                    return true;
                default:
                    return false;
            }
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }
    }
}
