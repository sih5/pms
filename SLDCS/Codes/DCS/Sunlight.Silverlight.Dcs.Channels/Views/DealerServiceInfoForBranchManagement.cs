﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Channels.Views {
    [PageMeta("Common", "Company", "DealerServiceInfoForBranch", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_PAUSE_RESUME_EXPORT_IMPORT_DETAIL,"DealerServiceInfoForBranch"
    })]
    public class DealerServiceInfoForBranchManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private const string DATA_EDIT_VIEW_DETAIL = "_DataEditViewDetail_";
        private const string DATA_EDIT_VIEW_IMPORT = "_DataEditViewImport_";
        private const string DATA_EDIT_VIEW_MAINIMPORT = "_DataEditViewMainImport_";
        private const string DATA_EDIT_VIEW_IMPORTEDIT = "_DataEditViewImportEdit_";
        private const string DATA_EDIT_VIEW_BRANCHIMPORTEDIT = "_DataEditViewBranchImportEdit_";
        private DataEditViewBase dataEditViewDetail;
        private DataEditViewBase dealerBusinessPermitImport;
        private DataEditViewBase dealerServiceInfoImport;
        private int companyType;
        private DcsDomainContext domainContext = new DcsDomainContext();
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public DealerServiceInfoForBranchManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = ChannelsUIStrings.DataManagementView_Title_DealerServiceInfoForBranch;
            domainContext.Load(domainContext.GetCompaniesQuery().Where(e => e.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.companyType = loadOp.Entities.First().Type;
            }, null);
        }
        private DataEditViewBase DataEditViewDetail {
            get {
                if(this.dataEditViewDetail == null) {
                    this.dataEditViewDetail = DI.GetDataEditView("DealerServiceInfoForBranchDetail");
                    this.dataEditViewDetail.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewDetail;
            }
        }
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("DealerServiceInfoForBranch"));
            }
        }

        private DataEditViewBase DealerBusinessPermitImport {
            get {
                if(this.dealerBusinessPermitImport == null) {
                    this.dealerBusinessPermitImport = DI.GetDataEditView("DealerBusinessPermitImport");
                    this.dealerBusinessPermitImport.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dealerBusinessPermitImport;
            }
        }

        private DataEditViewBase DealerServiceInfoImport {
            get {
                if(this.dealerServiceInfoImport == null) {
                    this.dealerServiceInfoImport = DI.GetDataEditView("DealerServiceInfoImport");
                    this.dealerServiceInfoImport.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dealerServiceInfoImport;
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("DealerServiceInfoForBranch");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase dealerServiceInfoImportEdit;
        private DataEditViewBase DealerServiceInfoImportEdit {
            get {
                if(this.dealerServiceInfoImportEdit == null) {
                    this.dealerServiceInfoImportEdit = DI.GetDataEditView("DealerServiceInfoImportEdit");
                    this.dealerServiceInfoImportEdit.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dealerServiceInfoImportEdit;
            }
        }

        private DataEditViewBase dealerServiceInfoBranchImport;
        private DataEditViewBase DealerServiceInfoBranchImport {
            get {
                if(this.dealerServiceInfoBranchImport == null) {
                    this.dealerServiceInfoBranchImport = DI.GetDataEditView("DealerServiceInfoBranchImport");
                    this.dealerServiceInfoBranchImport.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dealerServiceInfoBranchImport;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_DETAIL, () => this.DataEditViewDetail);
            this.RegisterView(DATA_EDIT_VIEW_IMPORT, () => this.DealerBusinessPermitImport);
            this.RegisterView(DATA_EDIT_VIEW_MAINIMPORT, () => this.DealerServiceInfoImport);
            this.RegisterView(DATA_EDIT_VIEW_IMPORTEDIT, () => this.DealerServiceInfoImportEdit);
            this.RegisterView(DATA_EDIT_VIEW_BRANCHIMPORTEDIT, () => this.DealerServiceInfoBranchImport);
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewDetail = null;
            this.dealerBusinessPermitImport = null;
            this.dealerServiceInfoImport = null;
        }
		

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var composite = filterItem as CompositeFilterItem;
            if(composite == null)
                return;
            ClientVar.ConvertTime(composite);
            this.DataGridView.FilterItem = composite;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.DETAIL:
                    this.DataEditViewDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.Cast<DealerServiceInfo>().First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_DETAIL);
                    break;
                case CommonActionKeys.ADD:
                    var dealerServiceInfo = this.DataEditView.CreateObjectToEdit<DealerServiceInfo>();
                    dealerServiceInfo.BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    dealerServiceInfo.Status = (int)DcsMasterDataStatus.有效;
                    dealerServiceInfo.IsOnDuty = false;
                    dealerServiceInfo.ChannelCapabilityId = 2;//网络业务能力默认为服务站
                    dealerServiceInfo.ExternalState = (int)DcsExternalState.停用;//字典项-（1.有效  2.停用）
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.Cast<DealerServiceInfo>().First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(ChannelsUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<DealerServiceInfo>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废经销商分公司管理信息)
                                entity.作废经销商分公司管理信息();
                            this.ExecuteSerivcesMethod(ChannelsUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.PAUSE:
                    DcsUtils.Confirm(ChannelsUIStrings.DataManagementView_Confirm_Pause, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<DealerServiceInfo>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can停用经销商分公司管理信息) {
                                //this.domainContext.Dealer
                                this.domainContext.DealerServiceInfoValid1(entity.Dealer.Code, entity.PartsSalesCategory.Name, invokeOp => {
                                    if(invokeOp.HasError) {
                                        if(!invokeOp.IsErrorHandled)
                                            invokeOp.MarkErrorAsHandled();
                                        var error = invokeOp.ValidationErrors.First();
                                        if(error != null)
                                            UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage);
                                        else
                                            DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                                        return;
                                    }
                                    if(invokeOp.Value == null) {
                                        entity.停用经销商分公司管理信息();
                                        this.ExecuteSerivcesMethod(ChannelsUIStrings.DataManagementView_Notification_PauseSuccess);

                                    } else {
                                        if(invokeOp.Value == "未找到当前品牌的MDM品牌映射") {
                                            return;
                                        }
                                        if(invokeOp.Value == "请在MDM系统停用当前品牌") {
                                            UIHelper.ShowNotification(ChannelsUIStrings.DataManagementView_Notification_DealerServiceInfo_PleaseStopThisCategory);
                                            entity.停用经销商分公司管理信息();
                                            this.ExecuteSerivcesMethod(ChannelsUIStrings.DataManagementView_Notification_PauseSuccess);
                                        }
                                    }
                                }, null);
                            }
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.RESUME:
                    DcsUtils.Confirm(ChannelsUIStrings.DataManagementView_Confirm_Resume, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<DealerServiceInfo>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            domainContext.Load(domainContext.GetDealersQuery().Where(e => e.Id == entity.DealerId), LoadBehavior.RefreshCurrent, loadOp => {
                                if(loadOp.HasError) {
                                    if(!loadOp.IsErrorHandled)
                                        loadOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                                    return;
                                }
                                var DealerTmp = loadOp.Entities.SingleOrDefault();
                                if(DealerTmp == null) {
                                    UIHelper.ShowAlertMessage(ChannelsUIStrings.DataManagementView_Notification_DealerServiceInfo_NoCompanyInfo);
                                    return;
                                }
                                if(DealerTmp.Status == (int)DcsMasterDataStatus.停用 || DealerTmp.Status == (int)DcsMasterDataStatus.作废) {
                                    UIHelper.ShowAlertMessage(ChannelsUIStrings.DataManagementView_Notification_DealerServiceInfo_NoUsableCompanyInfo);
                                    return;
                                }
                                if(entity.Can恢复经销商分公司管理信息)
                                    entity.恢复经销商分公司管理信息();
                                this.ExecuteSerivcesMethod(ChannelsUIStrings.DataManagementView_Notification_ResumeSuccess);
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                    var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                    var usedPartsWarehouseId = filterItem.Filters.Single(e => e.MemberName == "UsedPartsWarehouseId").Value as int?;

                    var dealerCode = filterItem.Filters.Single(e => e.MemberName == "Dealer.Code").Value as string;
                    var dealerName = filterItem.Filters.Single(e => e.MemberName == "Dealer.Name").Value as string;
                    var businessCode = filterItem.Filters.Single(r => r.MemberName == "BusinessCode").Value as string;
                    var businessName = filterItem.Filters.Single(r => r.MemberName == "BusinessName").Value as string;
                    var marketingDepartmentName = filterItem.Filters.Single(e => e.MemberName == "MarketingDepartment.Name").Value as string;
                    var compositeFilterItem = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    if(compositeFilterItem != null) {
                        createTimeBegin = compositeFilterItem.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                        createTimeEnd = compositeFilterItem.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    }
                    var entities = this.DataGridView.SelectedEntities;
                    int? id = null;
                    if(entities != null && entities.Any())
                        id = entities.Cast<DealerServiceInfo>().First().Id;
                    this.ExportDealerServiceInfo(id, BaseApp.Current.CurrentUserData.EnterpriseId, dealerCode, dealerName, businessCode, businessName, partsSalesCategoryId, marketingDepartmentName, usedPartsWarehouseId, createTimeBegin, createTimeEnd, status);
                    break;
                //屏蔽
                case CommonActionKeys.IMPORT:
                    break;
                case "ImportOverlapInfo":
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORT);
                    break;
                case "ImportMainInfo":
                    this.SwitchViewTo(DATA_EDIT_VIEW_MAINIMPORT);
                    break;
                case "ImportForEdito":
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORTEDIT);
                    break;
                case "BranchImport":
                    this.SwitchViewTo(DATA_EDIT_VIEW_BRANCHIMPORTEDIT);
                    break;
                case "UpdateDistance":
                    string EnterpriseCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                    ShellViewModel.Current.IsBusy = true;
                    this.UpdateDistanceRelation(EnterpriseCode);
                    break;
            }
        }

        private void UpdateDistanceRelation(string EnterpriseCode) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.更新站间距离分品牌Async(EnterpriseCode);
            this.excelServiceClient.更新站间距离分品牌Completed += excelServiceClient_更新站间距离分品牌Completed;
            this.excelServiceClient.更新站间距离分品牌Completed += excelServiceClient_更新站间距离分品牌Completed;
        }

        private void excelServiceClient_更新站间距离分品牌Completed(object sender, 更新站间距离分品牌CompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            //HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        private void ExportDealerServiceInfo(int? dealerServiceInfoId, int branchId, string dealerCode, string dealerName, string businessCode, string businessName, int? partsSalesCategoryId, string marketingDepartmentName, int? usedPartsWarehouseId, DateTime? createTimeBegin, DateTime? createTimeEnd, int? status) {
            ShellViewModel.Current.IsBusy = true;
            excelServiceClient.ExportDealerServiceInfoAsync(dealerServiceInfoId, branchId, dealerCode, dealerName, businessCode, businessName, partsSalesCategoryId, marketingDepartmentName, usedPartsWarehouseId, createTimeBegin, createTimeEnd, status, this.companyType);
            excelServiceClient.ExportDealerServiceInfoCompleted -= excelServiceClient_ExportDealerServiceInfoCompleted;
            excelServiceClient.ExportDealerServiceInfoCompleted += excelServiceClient_ExportDealerServiceInfoCompleted;
        }

        private void excelServiceClient_ExportDealerServiceInfoCompleted(object sender, ExportDealerServiceInfoCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                case "ImportOverlapInfo":
                case "ImportMainInfo":
                case "ImportForEdito":
                case "BranchImport":
                case "UpdateDistance":
                    return true;
                case CommonActionKeys.EDIT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var dealerServiceInfo = this.DataGridView.SelectedEntities.Cast<DealerServiceInfo>().ToArray();
                    if(dealerServiceInfo.Length != 1)
                        return false;
                    if(String.CompareOrdinal(uniqueId, CommonActionKeys.ADD) == 0)
                        return dealerServiceInfo[0].Status == default(int);
                    return dealerServiceInfo[0] != null && dealerServiceInfo[0].Status == (int)DcsMasterDataStatus.有效;
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.PAUSE:
                case CommonActionKeys.RESUME:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<DealerServiceInfo>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    if(String.CompareOrdinal(uniqueId, CommonActionKeys.RESUME) == 0)
                        return entities[0].Status == (int)DcsMasterDataStatus.停用;
                    return entities[0].Status == (int)DcsMasterDataStatus.有效;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.DETAIL:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<DealerServiceInfo>().ToArray().Length == 1;
                //屏蔽
                case CommonActionKeys.IMPORT:
                    return false;
                default:
                    return false;
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "DealerServiceInfoForBranch"
                };
            }
        }
    }
}
