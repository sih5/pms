﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Channels.Views {
    [PageMeta("Common", "Dealer", "DealerServiceInfoForDealer", ActionPanelKeys = new[] {
        CommonActionKeys.EDIT_DETAIL
    })]

    public class DealerServiceInfoForDealerManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private const string DATA_EDIT_VIEW_DETAIL = "_DataEditViewDetail_";
        private DataEditViewBase dataEditViewDetail;

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("DealerServiceInfoForDealer"));
            }
        }

        private DataEditViewBase DataEditViewDetail {
            get {
                if(this.dataEditViewDetail == null) {
                    this.dataEditViewDetail = DI.GetDataEditView("DealerServiceInfoForDealerDetail");
                    this.dataEditViewDetail.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewDetail;
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("DealerServiceInfoForDealer");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        public DealerServiceInfoForDealerManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = ChannelsUIStrings.DataManagementView_Title_DealerServiceInfo;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_DETAIL, () => this.DataEditViewDetail);
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewDetail = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            if(filterItem is CompositeFilterItem) {
                var compositeFilterItem = filterItem as CompositeFilterItem;
                compositeFilterItem.Filters.Add(new FilterItem {
                    //由于返回结果集是Dealer，所以 DealerId -> Id
                    MemberName = "DealerId",
                    Operator = FilterOperator.IsEqualTo,
                    MemberType = typeof(int),
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });
            }
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "DealerServiceInfoForDealer"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.DETAIL:
                    this.DataEditViewDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_DETAIL);
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            if(this.DataGridView.SelectedEntities == null)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EDIT:
                    var entities = this.DataGridView.SelectedEntities.Cast<DealerServiceInfo>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status != (int)DcsBaseDataStatus.作废;
                case CommonActionKeys.DETAIL:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<DealerServiceInfo>().ToArray().Length == 1;
                default:
                    return false;
            }
        }
    }
}
