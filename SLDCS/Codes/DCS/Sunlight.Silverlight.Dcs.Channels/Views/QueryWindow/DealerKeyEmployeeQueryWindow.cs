﻿

namespace Sunlight.Silverlight.Dcs.Channels.Views.QueryWindow {
    /// <summary>
    /// 服务站关键岗位人员查询
    /// </summary>
    public class DealerKeyEmployeeQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "DealerKeyEmployeeForSelect";
            }
        }

        public override string QueryPanelKey {
            get {
                return "DealerKeyEmployeeForSelect";
            }
        }
    }
}
