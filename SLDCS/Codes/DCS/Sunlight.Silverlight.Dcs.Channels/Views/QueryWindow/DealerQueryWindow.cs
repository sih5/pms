﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Channels.Views.QueryWindow {
    /// <summary>
    /// 经销商基本信息选择
    /// </summary>
    public class DealerQueryWindow : DcsQueryWindowBase {
        public DealerQueryWindow() {
            this.SetDefaultFilterItem(new FilterItem {
                MemberName = "Status",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = (int)DcsMasterDataStatus.有效
            });
        }

        public override string QueryPanelKey {
            get {
                return "DealerForSelect";
            }
        }

        public override string DataGridViewKey {
            get {
                return "DealerForSelect";
            }
        }
    }
}
