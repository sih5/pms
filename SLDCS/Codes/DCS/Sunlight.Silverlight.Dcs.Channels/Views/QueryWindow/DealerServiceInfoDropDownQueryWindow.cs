﻿using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Channels.Views.QueryWindow {
    public class DealerServiceInfoDropDownQueryWindow : DcsDropDownQueryWindowBase {
        /// <summary>
        /// 选择服务站信息(分公司)
        /// </summary>
        public DealerServiceInfoDropDownQueryWindow() {
            this.SetDefaultQueryItemEnable("Common", "Status", false);
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
        }
        public override string Title {
            get {
                return ChannelsUIStrings.QueryWindow_Title_DealerServiceInfo;
            }
        }

        public override string DataGridViewKey {
            get {
                return "DealerServiceInfoForSelect";
            }
        }

        public override string QueryPanelKey {
            get {
                return "DealerServiceInfoForSelect";
            }
        }
    }
}
