﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Channels.Views.QueryWindow {
    /// <summary>
    /// 经销商分公司管理信息选择(根据当前登陆企业过滤)
    /// </summary>
    public class DealerServiceInfoForExpenseAdjustmentBillQueryWindow : DcsQueryWindowBase {
        public DealerServiceInfoForExpenseAdjustmentBillQueryWindow() {
            //this.SetDefaultFilterItem(new FilterItem {
            //    MemberName = "Status",
            //    MemberType = typeof(int),
            //    Operator = FilterOperator.IsEqualTo,
            //    Value = (int)DcsMasterDataStatus.有效
            //});
        }

        public override string QueryPanelKey {
            get {
                return "DealerServiceInfoForExpenseAdjustmentBill";
            }
        }

        public override string DataGridViewKey {
            get {
                return "DealerServiceInfoForExpenseAdjustmentBill";
            }
        }
    }
}
