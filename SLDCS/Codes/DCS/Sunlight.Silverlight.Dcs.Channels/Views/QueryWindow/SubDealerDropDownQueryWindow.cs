﻿using Sunlight.Silverlight.Dcs.Channels.Resources;

namespace Sunlight.Silverlight.Dcs.Channels.Views.QueryWindow {
    /// <summary>
    /// 二级站选择
    /// </summary>
    public class SubDealerDropDownQueryWindow : DcsDropDownQueryWindowBase {
        //public SubDealerDropDownQueryWindow() {
        //    this.SetDefaultFilterItem(new FilterItem("DealerId", typeof(string), FilterOperator.IsEqualTo, BaseApp.Current.CurrentUserData.EnterpriseId));
        //}
        public override string Title {
            get {
                return ChannelsUIStrings.QueryWindow_Title_SubDealer;
            }
        }

        public override string DataGridViewKey {
            get {
                return "SubDealer";
            }
        }

        public override string QueryPanelKey {
            get {
                return "SubDealer";
            }
        }
    }
}
