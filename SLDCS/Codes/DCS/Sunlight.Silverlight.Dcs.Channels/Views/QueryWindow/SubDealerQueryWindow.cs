﻿
using Sunlight.Silverlight.Core.Model;
namespace Sunlight.Silverlight.Dcs.Channels.Views.QueryWindow {
    /// <summary>
    /// 二级站选择
    /// </summary>
    public class SubDealerQueryWindow : DcsQueryWindowBase {
        public SubDealerQueryWindow() {
            this.SetDefaultFilterItem(new FilterItem("DealerId", typeof(int), FilterOperator.IsEqualTo, BaseApp.Current.CurrentUserData.EnterpriseId));
        }
        public override string DataGridViewKey {
            get {
                return "SubDealer";
            }
        }

        public override string QueryPanelKey {
            get {
                return "SubDealer";
            }
        }
    }
}
