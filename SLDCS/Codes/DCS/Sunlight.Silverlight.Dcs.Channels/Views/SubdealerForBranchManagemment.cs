﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Channels.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Channels.Views {
    [PageMeta("Common", "Dealer", "SubdealerForBranch", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_EXPORT_IMPORT
    })]
    public class SubdealerForBranchManagemment : DcsDataManagementViewBase {

        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase importDataEditView;
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        private const string IMPORT_DATA_EDIT_VIEW = "_IMPORT_DATA_EDIT_VIEW_";

        public SubdealerForBranchManagemment() {
            this.Initializer.Register(this.Initialize);
            this.Title = ChannelsUIStrings.DataManagementView_Title_SubdealerForBranch;
        }

        private DataEditViewBase ImportDataEditView {
            get {
                if(this.importDataEditView == null) {
                    this.importDataEditView = DI.GetDataEditView("SubdealerForBranchImport");
                    this.importDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.importDataEditView;
            }
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("SubdealerForBranch"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("SubdealerForBranch");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.importDataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(IMPORT_DATA_EDIT_VIEW, () => this.ImportDataEditView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "SubdealerFor"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var subDealer = this.DataEditView.CreateObjectToEdit<SubDealer>();
                    subDealer.Status = (int)DcsBaseDataStatus.有效;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(ChannelsUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<SubDealer>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废二级站信息)
                                entity.作废二级站信息();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(ChannelsUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORT:
                    //如果选中数据,导出参数为二级站ID 
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<SubDealer>().Select(r => r.Id).ToArray();
                        this.dcsDomainContext.ExportSubDealer(ids, null, null, null, null, null, null, null, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                            }
                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                        }, null);
                    }
                        //否则 导出参数为
                    else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var name = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Name") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Name").Value as string;
                        var status = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Status") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var dealerName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Dealer.Name") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Dealer.Name").Value as string;
                        var dealerCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Dealer.Code") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Dealer.Code").Value as string;
                        var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeStart = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeStart = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        this.dcsDomainContext.ExportSubDealer(new int[] { }, code, name, dealerCode, dealerName, status, createTimeStart, createTimeEnd, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                            }
                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                        }, null);

                    }
                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(IMPORT_DATA_EDIT_VIEW);
                    break;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<SubDealer>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsBaseDataStatus.有效;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.IMPORT:
                    return true;
                default:
                    return false;
            }
        }
    }
}
