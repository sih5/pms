﻿using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Dcs.Common.Dashboard.ViewModels;
using Sunlight.Silverlight.Dcs.Dashboard;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.Charts.ViewModels {
    public abstract class ChartViewModelBase : ViewModelBase {
        private bool isBusy;
        private DashboardClient proxy;

        protected virtual DashboardClient Proxy {
            get {
                return this.proxy ?? (this.proxy = new DashboardClient());
            }
        }

        public bool IsBusy {
            get {
                return this.isBusy;
            }
            protected set {
                if(this.isBusy != value) {
                    this.isBusy = value;
                    this.OnPropertyChanged("IsBusy");
                    this.RefreshCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public DelegateCommand RefreshCommand {
            get;
            protected set;
        }

        protected abstract void Refresh();

        protected virtual bool CanRefresh() {
            return !this.IsBusy;
        }

        protected ChartViewModelBase() {
            this.RefreshCommand = new DelegateCommand(this.Refresh, this.CanRefresh);
        }
    }
}
