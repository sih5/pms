﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Sunlight.Silverlight.Dcs.Dashboard;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.Charts.ViewModels.Crm {
    public sealed class CustomerProportionByLevelViewModel : ChartViewModelBase {
        private ObservableCollection<KeyValuePair<string, int>> data;

        public ObservableCollection<KeyValuePair<string, int>> Data {
            get {
                return this.data;
            }
            private set {
                if(this.data != value) {
                    this.data = value;
                    this.OnPropertyChanged("Data");
                }
            }
        }

        private void ProxyOnGetCustomerProportionsByLevelCompleted(object sender, GetCustomerProportionsByLevelCompletedEventArgs e) {
            this.IsBusy = false;
            this.Data = new ObservableCollection<KeyValuePair<string, int>>(e.Result);
        }

        protected override void Refresh() {
            this.Proxy.GetCustomerProportionsByLevelAsync();
            this.IsBusy = true;
        }

        public CustomerProportionByLevelViewModel() {
            this.Proxy.GetCustomerProportionsByLevelCompleted += this.ProxyOnGetCustomerProportionsByLevelCompleted;
        }
    }
}
