﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Dcs.Dashboard;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.Charts.ViewModels.Crm {
    public sealed class TestDriveRecordByDayViewModel : ChartViewModelBase {
        private Dictionary<DateTime, int> data;

        public Dictionary<DateTime, int> Data {
            get {
                return this.data;
            }
            private set {
                if(this.data != value) {
                    this.data = value;
                    this.OnPropertyChanged("Data");
                }
            }
        }

        private void Proxy_GetTestDriveRecordsByDayCompleted(object sender, GetTestDriveRecordsByDayCompletedEventArgs e) {
            this.IsBusy = false;
            this.Data = e.Result;
        }

        protected override void Refresh() {
            this.Proxy.GetTestDriveRecordsByDayAsync();
            this.IsBusy = true;
        }

        public TestDriveRecordByDayViewModel() {
            this.Proxy.GetTestDriveRecordsByDayCompleted += this.Proxy_GetTestDriveRecordsByDayCompleted;
        }
    }
}
