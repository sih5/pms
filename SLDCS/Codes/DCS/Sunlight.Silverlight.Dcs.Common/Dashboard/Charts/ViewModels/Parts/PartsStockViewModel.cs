﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Sunlight.Silverlight.Dcs.Dashboard;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.Charts.ViewModels.Parts {
    public sealed class PartsStockViewModel : ChartViewModelBase {
        private ObservableCollection<KeyValuePair<string, decimal>> data;

        public ObservableCollection<KeyValuePair<string, decimal>> Data {
            get {
                return this.data;
            }
            private set {
                if(this.data != value) {
                    this.data = value;
                    this.OnPropertyChanged("Data");
                }
            }
        }

        private void Proxy_GetSparePartStocksCompleted(object sender, GetSparePartStocksCompletedEventArgs e) {
            this.IsBusy = false;
            this.Data = new ObservableCollection<KeyValuePair<string, decimal>>(e.Result);
        }

        protected override void Refresh() {
            this.Proxy.GetSparePartStocksAsync();
            this.IsBusy = true;
        }

        public PartsStockViewModel() {
            this.Proxy.GetSparePartStocksCompleted += this.Proxy_GetSparePartStocksCompleted;
        }
    }
}
