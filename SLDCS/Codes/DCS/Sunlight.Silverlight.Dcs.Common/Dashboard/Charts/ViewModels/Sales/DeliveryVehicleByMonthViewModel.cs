﻿using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Dcs.Dashboard;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.Charts.ViewModels.Sales {
    public sealed class DeliveryVehicleByMonthViewModel : ChartViewModelBase {
        private List<VehicleModelCount> data;

        public List<VehicleModelCount> Data {
            get {
                return this.data;
            }
            private set {
                if(this.data != value) {
                    this.data = value;
                    this.OnPropertyChanged("Data");
                }
            }
        }

        private void Proxy_GetDeliveryVehiclesByMonthCompleted(object sender, GetDeliveryVehiclesByMonthCompletedEventArgs e) {
            this.IsBusy = false;
            this.Data = e.Result.ToList();
        }

        protected override void Refresh() {
            this.Proxy.GetDeliveryVehiclesByMonthAsync();
            this.IsBusy = true;
        }

        public DeliveryVehicleByMonthViewModel() {
            this.Proxy.GetDeliveryVehiclesByMonthCompleted += this.Proxy_GetDeliveryVehiclesByMonthCompleted;
        }
    }
}
