﻿using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Dcs.Dashboard;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.Charts.ViewModels.Sales {
    public sealed class StockViewModel : ChartViewModelBase {
        private List<VehicleModelCount> data;

        public List<VehicleModelCount> Data {
            get {
                return this.data;
            }
            private set {
                if(this.data != value) {
                    this.data = value;
                    this.OnPropertyChanged("Data");
                }
            }
        }

        private void Proxy_GetVehicleStocksCompleted(object sender, GetVehicleStocksCompletedEventArgs e) {
            this.IsBusy = false;
            this.Data = e.Result.ToList();
        }

        protected override void Refresh() {
            this.Proxy.GetVehicleStocksAsync();
            this.IsBusy = true;
        }

        public StockViewModel() {
            this.Proxy.GetVehicleStocksCompleted += this.Proxy_GetVehicleStocksCompleted;
        }
    }
}