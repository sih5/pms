﻿using Sunlight.Silverlight.Dcs.Dashboard;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.Charts.ViewModels.Service {
    public sealed class RepairContractBookingPercentViewModel : ChartViewModelBase {
        private decimal data;

        public decimal Data {
            get {
                return this.data;
            }
            private set {
                if(this.data != value) {
                    this.data = value;
                    this.OnPropertyChanged("Data");
                }
            }
        }

        private void Proxy_GetRepairContractBookingPercentCompleted(object sender, GetRepairContractBookingPercentCompletedEventArgs e) {
            this.IsBusy = false;
            this.Data = e.Result * 100;
        }

        protected override void Refresh() {
            this.Proxy.GetRepairContractBookingPercentAsync();
            this.IsBusy = true;
        }

        public RepairContractBookingPercentViewModel() {
            this.Proxy.GetRepairContractBookingPercentCompleted += this.Proxy_GetRepairContractBookingPercentCompleted;
        }
    }
}
