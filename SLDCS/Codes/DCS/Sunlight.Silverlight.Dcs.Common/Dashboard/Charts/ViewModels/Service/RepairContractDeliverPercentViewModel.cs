﻿using Sunlight.Silverlight.Dcs.Dashboard;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.Charts.ViewModels.Service {
    public sealed class RepairContractDeliverPercentViewModel : ChartViewModelBase {
        private decimal deliverPercent;
        private decimal repairPercent;

        public decimal DeliverPercent {
            get {
                return this.deliverPercent;
            }
            private set {
                if(this.deliverPercent != value) {
                    this.deliverPercent = value;
                    this.OnPropertyChanged("DeliverPercent");
                }
            }
        }

        public decimal RepairPercent {
            get {
                return this.repairPercent;
            }
            private set {
                if(this.repairPercent != value) {
                    this.repairPercent = value;
                    this.OnPropertyChanged("RepairPercent");
                }
            }
        }

        private void Proxy_GetRepairContractDeliverPrecentCompleted(object sender, GetRepairContractDeliverPrecentCompletedEventArgs e) {
            this.IsBusy = false;
            if(e.Result.Length >= 1)
                this.DeliverPercent = e.Result[0] * 100;
            if(e.Result.Length >= 2)
                this.RepairPercent = e.Result[1] * 100;
        }

        protected override void Refresh() {
            this.Proxy.GetRepairContractDeliverPrecentAsync();
            this.IsBusy = true;
        }

        public RepairContractDeliverPercentViewModel() {
            this.Proxy.GetRepairContractDeliverPrecentCompleted += this.Proxy_GetRepairContractDeliverPrecentCompleted;
        }
    }
}
