﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Sunlight.Silverlight.Dcs.Dashboard;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.Charts.ViewModels.Service {
    public sealed class RepairProportionByAdvisorViewModel : ChartViewModelBase {
        private ObservableCollection<KeyValuePair<string, int>> data;

        public ObservableCollection<KeyValuePair<string, int>> Data {
            get {
                return this.data;
            }
            private set {
                if(this.data != value) {
                    this.data = value;
                    this.OnPropertyChanged("Data");
                }
            }
        }

        private void Proxy_GetRepairProportionsByAdvisorCompleted(object sender, GetRepairProportionsByAdvisorCompletedEventArgs e) {
            this.IsBusy = false;
            this.Data = new ObservableCollection<KeyValuePair<string, int>>(e.Result);
        }

        protected override void Refresh() {
            this.Proxy.GetRepairProportionsByAdvisorAsync();
            this.IsBusy = true;
        }

        public RepairProportionByAdvisorViewModel() {
            this.Proxy.GetRepairProportionsByAdvisorCompleted += this.Proxy_GetRepairProportionsByAdvisorCompleted;
        }
    }
}
