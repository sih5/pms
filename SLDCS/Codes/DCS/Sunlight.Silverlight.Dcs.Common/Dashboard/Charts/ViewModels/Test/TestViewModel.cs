﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Sunlight.Silverlight.Dcs.Dashboard;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.Charts.ViewModels.Test {
    public sealed class TestViewModel : ChartViewModelBase {
        private ObservableCollection<KeyValuePair<string, int>> data;

        public ObservableCollection<KeyValuePair<string, int>> Data {
            get {
                return this.data;
            }
            private set {
                if(this.data != value) {
                    this.data = value;
                    this.OnPropertyChanged("Data");
                }
            }
        }

        private void Proxy_GetPartsStocksByWarehouseCompleted(object sender, GetPartsStocksByWarehouseCompletedEventArgs e) {
            this.IsBusy = false;
            this.Data = new ObservableCollection<KeyValuePair<string, int>>(e.Result);
        }

        protected override void Refresh() {
            this.Proxy.GetPartsStocksByWarehouseAsync();
            this.IsBusy = true;
        }

        public TestViewModel() {
            this.Proxy.GetPartsStocksByWarehouseCompleted += this.Proxy_GetPartsStocksByWarehouseCompleted;
        }
    }
}
