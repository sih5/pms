﻿using System;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Dashboard.Charts.ViewModels;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.Charts.Views {
    public class ChartViewBase : Page {
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e) {
            var viewModel = this.DataContext as ChartViewModelBase;
            if(viewModel != null)
                viewModel.RefreshCommand.Execute();

            base.OnNavigatedTo(e);
        }

        public ChartViewBase() {
            var resourceDictionary = new ResourceDictionary {
                Source = new Uri("/Sunlight.Silverlight.Dcs.Common;component/Dashboard/Styles/ChartView.xaml", UriKind.Relative)
            };
            this.Resources.MergedDictionaries.Add(resourceDictionary);
        }
    }
}
