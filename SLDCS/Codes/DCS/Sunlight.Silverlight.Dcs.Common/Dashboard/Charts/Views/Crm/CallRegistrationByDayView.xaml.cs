﻿using System;
using System.Collections.Generic;
using System.Windows;
using Telerik.Windows.Controls.ChartView;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.Charts.Views.Crm {
    public partial class CallRegistrationByDayView {
        /// <summary>
        ///     为 Series 增加 X/Y 轴的数据绑定
        /// </summary>
        /// <remarks>
        ///     在 XAML 中，Series 的 CategoryBinding/ValueBinding 仅支持 PropertyNameDataPointBinding。
        ///     这种绑定是通过反射创建关联，仅适用 Source 为引用类型而不是值类型的数据集合。
        ///     例 Source 为 KeyValuePair (结构体)，则运行时会抛出异常 ArgumentException: Dynamic getter is not supported for value types.
        ///
        ///     所以在 ItemsSource 为 Dictionary&lt;TKey, TValue&gt; 时，需将其 CategoryBinding/ValueBinding 赋值为 GenericDataPointBinding 类型。
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LineSeries_OnLoaded(object sender, RoutedEventArgs e) {
            var lineSeries = sender as LineSeries;
            if(lineSeries != null) {
                if(lineSeries.CategoryBinding == null)
                    lineSeries.CategoryBinding = new GenericDataPointBinding<KeyValuePair<DateTime, int>, DateTime> {
                        ValueSelector = kv => kv.Key
                    };
                if(lineSeries.ValueBinding == null)
                    lineSeries.ValueBinding = new GenericDataPointBinding<KeyValuePair<DateTime, int>, int> {
                        ValueSelector = kv => kv.Value
                    };
            }
        }

        public CallRegistrationByDayView() {
            this.InitializeComponent();
        }
    }
}
