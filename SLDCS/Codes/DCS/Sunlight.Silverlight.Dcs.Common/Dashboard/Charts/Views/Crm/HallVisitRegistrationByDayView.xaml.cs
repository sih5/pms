﻿using System;
using System.Collections.Generic;
using System.Windows;
using Telerik.Windows.Controls.ChartView;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.Charts.Views.Crm {
    public partial class HallVisitRegistrationByDayView {
        private void LineSeries_OnLoaded(object sender, RoutedEventArgs e) {
            var lineSeries = sender as LineSeries;
            if(lineSeries != null) {
                if(lineSeries.CategoryBinding == null)
                    lineSeries.CategoryBinding = new GenericDataPointBinding<KeyValuePair<DateTime, int>, DateTime> {
                        ValueSelector = kv => kv.Key
                    };
                if(lineSeries.ValueBinding == null)
                    lineSeries.ValueBinding = new GenericDataPointBinding<KeyValuePair<DateTime, int>, int> {
                        ValueSelector = kv => kv.Value
                    };
            }
        }

        public HallVisitRegistrationByDayView() {
            this.InitializeComponent();
        }
    }
}
