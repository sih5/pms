﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.Charts.Views.DataGrid
{
    public class SalesSatisfactionCentralRateDataGridView : DcsDataGridViewBase
    {
        private readonly string[] kvNames = new[] {
            "Company_Type"
        };

        public SalesSatisfactionCentralRateDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);

        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                       Name = "Name",
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_SubmitCompanyName,
                    }, new KeyValuesColumnItem {
                        Name = "Type",
                        KeyValueItems = this.KeyValueManager[kvNames[0]],
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_CustomerTypeStr
                    },new ColumnItem {
                        Title="满足条目数（品种）",
                       Name = "Ycpzs"
                    },new ColumnItem {
                        Title="总条目数（品种）",
                       Name = "Totalpz" 
                    },new ColumnItem {
                        Title="条目数满足率(%)",
                       Name = "Tmmzl"
                    },new ColumnItem {
                        Title="满足条目建议售价金额",
                       Name = "Ycfee"
                    },new ColumnItem {
                        Title="总条目建议售价总金额",
                       Name = "Totalfee"
                    },new ColumnItem {
                        Title="金额满足率(%)",
                       Name = "Jemzl"
                    }
                };
            }
        }

        protected override Type EntityType
        {
            get
            {
                return typeof(SalesSatisfactionRate);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }
        protected override bool UsePaging
        {
            get
            {
                return false;
            }
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override Binding OnRequestDataSourceBinding()
        {
            return new Binding("SalesSatisfactionRatess");
        }
    }
}
