﻿using System.Collections.Generic;
using System.Windows;
using Telerik.Windows.Controls.ChartView;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.Charts.Views.Parts {
    public partial class PartsStockPieView {
        /// <summary>
        ///     为 Series 增加 X/Y 轴的数据绑定
        /// </summary>
        /// <remarks>
        ///     在 XAML 中，Series 的 LabelBinding/ValueBinding 仅支持 PropertyNameDataPointBinding。
        ///     这种绑定是通过反射创建关联，仅适用 Source 为引用类型而不是值类型的数据集合。
        ///     例 Source 为 KeyValuePair (结构体)，则运行时会抛出异常 ArgumentException: Dynamic getter is not supported for value types.
        ///
        ///     所以在 ItemsSource 为 Dictionary&lt;TKey, TValue&gt; 时，需将其 LabelBinding/ValueBinding 赋值为 GenericDataPointBinding 类型。
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PieSeries_Loaded(object sender, RoutedEventArgs e) {
            var pieSeries = sender as PieSeries;
            if(pieSeries != null) {
                if(pieSeries.LabelDefinitions.Count == 0)
                    pieSeries.LabelDefinitions.Add(new ChartSeriesLabelDefinition {
                        Margin = new Thickness(2, 0, 0, 0),
                        Binding = new GenericDataPointBinding<KeyValuePair<string, decimal>, string> {
                            ValueSelector = kv => kv.Key
                        }
                    });
                if(pieSeries.ValueBinding == null)
                    pieSeries.ValueBinding = new GenericDataPointBinding<KeyValuePair<string, decimal>, decimal> {
                        ValueSelector = kv => kv.Value
                    };
            }
        }

        public PartsStockPieView() {
            this.InitializeComponent();
        }
    }
}
