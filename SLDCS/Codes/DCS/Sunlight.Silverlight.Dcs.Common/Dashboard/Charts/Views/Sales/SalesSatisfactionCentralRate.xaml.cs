﻿
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using System.Collections.ObjectModel;
using System.Windows;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.Charts.Views.Sales
{
    public partial class SalesSatisfactionCentralRate : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private DataGridViewBase salesSatisfactionCentralRateDataGridView;
        private ObservableCollection<SalesSatisfactionRate> salesSatisfactionRateDetail;
        public SalesSatisfactionCentralRate()
        {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
        protected override string Title
        {
            get
            {
                return "销售订单一次满足率对中心库";
            }
        }
        protected virtual void CreateUI()
        {
            var detailEditView = new DcsDetailDataEditView();
            detailEditView.Register(null, null, () => this.SalesSatisfactionCentralRateDataGridView);
            detailEditView.UnregisterButton(detailEditView.InsertButton);
            detailEditView.UnregisterButton(detailEditView.DeleteButton);
            this.Root.Children.Add(detailEditView);
            this.HideCancelButton();
            this.DomainContext.Load(this.DomainContext.GetSalesSatisfactionCentralRateQuery(), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                {
                    if (!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                else
                {
                    var entity = loadOp.Entities.ToArray();
                    if (entity != null)
                    {
                        this.SalesSatisfactionRatess.Clear();
                        foreach (var item in loadOp.Entities)
                        {
                            this.SalesSatisfactionRatess.Add(item);
                        }
                    }
                }
            }, null);

        }
        private DataGridViewBase SalesSatisfactionCentralRateDataGridView
        {
            get
            {
                if (this.salesSatisfactionCentralRateDataGridView == null)
                {
                    this.salesSatisfactionCentralRateDataGridView = DI.GetDataGridView("SalesSatisfactionCentralRate");
                    this.salesSatisfactionCentralRateDataGridView.DomainContext = this.DomainContext;
                    this.salesSatisfactionCentralRateDataGridView.DataContext = this;
                }
                return this.salesSatisfactionCentralRateDataGridView;
            }
        }
        public ObservableCollection<SalesSatisfactionRate> SalesSatisfactionRatess
        {
            get
            {
                return this.salesSatisfactionRateDetail ?? (this.salesSatisfactionRateDetail = new ObservableCollection<SalesSatisfactionRate>());
            }
            set
            {
                this.salesSatisfactionRateDetail = value;
                this.OnPropertyChanged("SalesSatisfactionRatess");
            }
        }
        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
    }
}
