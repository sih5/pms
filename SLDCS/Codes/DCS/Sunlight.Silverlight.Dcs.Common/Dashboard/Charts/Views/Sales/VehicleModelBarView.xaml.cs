﻿using Sunlight.Silverlight.Dcs.Common.Dashboard.Charts.ViewModels.Sales;
using System.Windows.Navigation;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.Charts.Views.Sales {
    public partial class VehicleModelBarView {
        // 当用户导航到此页面时执行。
        protected override void OnNavigatedTo(NavigationEventArgs e) {
            if(this.NavigationContext.QueryString.ContainsKey("viewModel"))
                switch(this.NavigationContext.QueryString["viewModel"].ToLower()) {
                    case "stock":
                        this.DataContext = new StockViewModel();
                        break;
                    case "deliveryvehiclebyday":
                        this.DataContext = new DeliveryVehicleByDayViewModel();
                        break;
                    case "deliveryvehiclebymonth":
                        this.DataContext = new DeliveryVehicleByMonthViewModel();
                        break;
                }

            base.OnNavigatedTo(e);
        }

        public VehicleModelBarView() {
            this.InitializeComponent();
        }
    }
}