﻿using System;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Media;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard {
    public static class DashboardUtils {
        /// <summary>
        /// 将形如#FF3D3D3D的字符串转换为<see cref="Color"/>类型的颜色对象。
        /// </summary>
        /// <param name="hexString">格式必须为#ARGB的16进制字符串</param>
        /// <returns></returns>
        public static Color ColorFromHex(string hexString) {
            return Color.FromArgb(
                Convert.ToByte(hexString.Substring(1, 2), 16),
                Convert.ToByte(hexString.Substring(3, 2), 16),
                Convert.ToByte(hexString.Substring(5, 2), 16),
                Convert.ToByte(hexString.Substring(7, 2), 16)
                );
        }

        /// <summary>
        /// 将针对服务端的相对路径转换为绝对路径。
        /// </summary>
        /// <example>
        /// App路径为http://example.com/
        /// <code>var absoluteUri = EnsureServerUri(new Uri("Client/Images/refresh.png", UriKind.Relative));</code>
        /// absoluteUri 值为 http://example.com/Client/Images/refresh.png
        /// </example>        
        /// <param name="uri">针对服务端的相对路径</param>
        /// <returns>若相对路径为null，则绝对路径值也为null</returns>
        public static Uri EnsureServerUri(Uri uri) {
            if(uri != null)
                if(uri.IsAbsoluteUri)
                    return uri;
                else {
                    if(Application.Current.Host.Source == null)
                        throw new InvalidOperationException();

                    var baseUri = Application.Current.Host.Source.OriginalString;
                    var index = baseUri.IndexOf("ClientBin/", StringComparison.InvariantCultureIgnoreCase);
                    if(index > -1)
                        baseUri = baseUri.Remove(index);
                    else {
                        var parts = baseUri.Split(new[] {
                            '/'
                        }, StringSplitOptions.RemoveEmptyEntries);
                        if(parts.Length > 0) {
                            index = baseUri.IndexOf(parts[parts.Length - 1], StringComparison.InvariantCultureIgnoreCase);
                            baseUri = baseUri.Remove(index);
                        }
                    }
                    return new Uri(new Uri(baseUri), uri);
                }
            return null;
        }

        /// <summary>
        /// 将时间间隔转换为易于阅读的文本
        /// </summary>
        /// <example>
        /// <code>var text = new TimeSpan(0, 1, 30).ToReadableString()</code>
        /// text值为"1分30秒"。
        /// </example>
        /// <param name="timeSpan">时间间隔</param>
        /// <returns></returns>
        public static string ToReadableString(this TimeSpan timeSpan) {
            var uiCulture = Thread.CurrentThread.CurrentUICulture;
            var stringBuilder = new StringBuilder();
            var dayFormat = "{0:天;天}";
            var hourFormat = "{0:小时;小时}";
            var minuteFormat = timeSpan.Seconds > 0 ? "{0:分;分}" : "{0:分钟;分钟}";
            var secondFormat = "{0:秒;秒}";

            if(uiCulture.Name == "en-US") {
                dayFormat = "{0 :day;days} ";
                hourFormat = "{0 :hr;hrs} ";
                minuteFormat = "{0 :min;mins} ";
                secondFormat = "{0 :sec;secs}";
            }
            if(timeSpan.Days > 0)
                stringBuilder.AppendFormat(new PluralFormatProvider(), dayFormat, timeSpan.Days);
            if(timeSpan.Hours > 0)
                stringBuilder.AppendFormat(new PluralFormatProvider(), hourFormat, timeSpan.Hours);
            if(timeSpan.Minutes > 0)
                stringBuilder.AppendFormat(new PluralFormatProvider(), minuteFormat, timeSpan.Minutes);
            if(timeSpan.Seconds == 0 && stringBuilder.Length == 0 || timeSpan.Seconds > 0)
                stringBuilder.AppendFormat(new PluralFormatProvider(), secondFormat, timeSpan.Seconds);
            return stringBuilder.ToString().TrimEnd();
        }
    }

    /// <summary>
    /// 支持复数格式的格式化类，
    /// 
    /// StringFormat单复数格式应以英文分号隔开，形如<code>"{0:day;days}"</code>。
    /// </summary>    
    public class PluralFormatProvider : IFormatProvider, ICustomFormatter {
        public object GetFormat(Type formatType) {
            return this;
        }

        public string Format(string format, object arg, IFormatProvider formatProvider) {
            var forms = format.Split(';');
            var value = (int)arg;
            return value + forms[value == 1 ? 0 : 1];
        }
    }
}
