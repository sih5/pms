#首先得执行命令 powershell Set-ExecutionPolicy Unrestricted
$input = Read-Host "请输入端口号"
echo "开始生成代理类..."
$path="..\..\Sunlight.Silverlight.Dcs.Common\Dashboard"
cd $path
Start-Process -FilePath "C:\Program Files (x86)\Microsoft SDKs\Silverlight\v5.0\Tools\SlSvcUtil.exe" -ArgumentList "http://localhost:$input/Services/Dashboard.svc /edb /n:""*,Sunlight.Silverlight.Dcs.Dashboard"" /r:""C:\Program Files (x86)\Microsoft Silverlight\5.1.50907.0\System.Windows.dll"" /o:DashboardServiceClient.cs" -NoNewWindow -Wait
echo "删除配置文件"

echo "配置文件信息加入生成的代理文件中"
$file=get-childitem DashboardServiceClient.cs
$content=[IO.File]::ReadAllText($file,[Text.Encoding]::UTF8)
$regex = [regex]"[ ]*public DashboardServiceClient\(\)\r\n[ ]*{\r\n[ ]*}\r\n"
$content=$regex.Replace($content,"        public DashboardServiceClient()
            : base(new BasicHttpBinding(Application.Current.Host.Source.Scheme == Uri.UriSchemeHttps ? BasicHttpSecurityMode.Transport : BasicHttpSecurityMode.None) {
                }, new EndpointAddress(DcsUtils.MakeServerUri(""Services/Dashboard.svc""))) {
        }
        ");
        
$regex = [regex]"namespace Sunlight.Silverlight.Dcs.Dashboard\r\n{"
$content=$regex.Replace($content,"namespace Sunlight.Silverlight.Dcs.Dashboard {
    using System;
    using System.ServiceModel;
    using System.Windows;");
[IO.File]::WriteAllText($file,$content,[Text.Encoding]::UTF8)
echo "结束"
Start-Sleep -s 300
