﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.Utils {
    /// <summary>
    /// 若文本不为空则显示，否则不显示。
    /// 若 parameter 为 IgnoreZero，则 value 等于 0 时不显示。
    /// </summary>
    public sealed class WarningDisplayTextToVisibilityConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            var str = value as string;
            var ignoreZeroStr = parameter as string;
            if(string.Compare(ignoreZeroStr, "IgnoreZero", StringComparison.InvariantCultureIgnoreCase) == 0) {
                int intValue;
                if(int.TryParse(str, out intValue))
                    return intValue == 0 ? Visibility.Collapsed : Visibility.Visible;
            }

            return string.IsNullOrEmpty(str) ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotSupportedException();
        }
    }

    /// <summary>
    /// 若 Value 不为 null 则显示，否则不显示。
    /// </summary>
    public sealed class ObjectToVisibilityConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            return value != null ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}