﻿using System.Collections.Generic;
using Sunlight.Silverlight.Dcs.Dashboard;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.Utils {
    public class SecurityPageComparer : IEqualityComparer<SecurityPage> {
        public bool Equals(SecurityPage x, SecurityPage y) {
            return x.Id == y.Id;
        }

        public int GetHashCode(SecurityPage obj) {
            return obj.Id;
        }
    }
}
