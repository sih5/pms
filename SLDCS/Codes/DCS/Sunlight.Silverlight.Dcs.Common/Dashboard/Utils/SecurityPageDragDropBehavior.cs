﻿using System;
using System.Windows;
using Sunlight.Silverlight.Dcs.Dashboard;
using Telerik.Windows.DragDrop.Behaviors;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.Utils {
    public enum AllowDropTypeEnum {
        Chart = 1,
        Warning = 2,
        Other = 3
    }

    public class SecurityPageDragDropBehavior : ListBoxDragDropBehavior {
        public static readonly DependencyProperty AllowDropTypeProperty;

        public AllowDropTypeEnum AllowDropType {
            get {
                return (AllowDropTypeEnum)GetValue(AllowDropTypeProperty);
            }
            set {
                SetValue(AllowDropTypeProperty, value);
            }
        }

        public override bool CanDrop(DragDropState state) {
            if(!base.CanDrop(state))
                return false;

            foreach(var item in state.DraggedItems) {
                var page = item as SecurityPage;
                if(page == null)
                    return false;
                AllowDropTypeEnum allowDropType;
                if(Enum.TryParse(page.PageType, true, out allowDropType)) {
                    if(allowDropType != this.AllowDropType || allowDropType == AllowDropTypeEnum.Other)
                        return false;
                } else if(this.AllowDropType != AllowDropTypeEnum.Other)
                    return false;
            }
            return true;
        }

        static SecurityPageDragDropBehavior() {
            AllowDropTypeProperty = DependencyProperty.Register("AllowDropType", typeof(AllowDropTypeEnum), typeof(SecurityPageDragDropBehavior), null);
        }
    }
}
