﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Sunlight.Silverlight.Core.Command;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.ViewModels {
    /// <summary>
    /// 表达一组图表信息<see cref="ChartViewModel"/>的ViewModel
    /// </summary>
    public class ChartCollectionViewModel : ViewModelBase {
        private ObservableCollection<ChartViewModel> chartItems;
        private ChartViewModel selectedChart;

        /// <summary>
        /// 该集合中包括的所有图表信息
        /// </summary>
        public ObservableCollection<ChartViewModel> ChartItems {
            get {
                return this.chartItems;
            }
            private set {
                if(this.chartItems != value) {
                    this.chartItems = value;
                    this.OnPropertyChanged("ChartItems");
                }
            }
        }

        /// <summary>
        /// 当前图表信息
        /// </summary>
        public ChartViewModel SelectedChart {
            get {
                return this.selectedChart;
            }
            set {
                if(this.selectedChart != value) {
                    this.selectedChart = value;
                    this.OnPropertyChanged("SelectedChart");
                    this.PrevCommand.RaiseCanExecuteChanged();
                    this.NextCommand.RaiseCanExecuteChanged();
                }
            }
        }

        /// <summary>
        /// 选择上一个图表
        /// </summary>
        public DelegateCommand PrevCommand {
            get;
            private set;
        }

        /// <summary>
        /// 选择下一个图表
        /// </summary>
        public DelegateCommand NextCommand {
            get;
            private set;
        }

        private bool CanPrev() {
            return this.SelectedChart != null && this.ChartItems.IndexOf(this.SelectedChart) > 0;
        }

        private void Prev() {
            if(this.ChartItems.Count == 0)
                return;

            var index = Math.Max(this.ChartItems.IndexOf(this.SelectedChart) - 1, 0);
            this.SelectedChart = this.ChartItems[index];
        }

        private bool CanNext() {
            return this.SelectedChart != null && this.ChartItems.IndexOf(this.SelectedChart) < this.ChartItems.Count - 1;
        }

        private void Next() {
            if(this.ChartItems.Count == 0)
                return;

            var index = Math.Min(this.ChartItems.IndexOf(this.SelectedChart) + 1, this.ChartItems.Count - 1);
            this.SelectedChart = this.ChartItems[index];
        }

        public ChartCollectionViewModel(List<ChartViewModel> chartItems) {
            this.PrevCommand = new DelegateCommand(this.Prev, this.CanPrev);
            this.NextCommand = new DelegateCommand(this.Next, this.CanNext);
            this.ChartItems = new ObservableCollection<ChartViewModel>(chartItems);
            this.SelectedChart = this.ChartItems.FirstOrDefault();
        }
    }
}
