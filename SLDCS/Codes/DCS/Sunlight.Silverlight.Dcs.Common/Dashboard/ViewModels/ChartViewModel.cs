﻿
using Sunlight.Silverlight.Dcs.Dashboard;
namespace Sunlight.Silverlight.Dcs.Common.Dashboard.ViewModels {
    /// <summary>
    /// 指向某个图表节点的ViewModel
    /// </summary>
    public class ChartViewModel : PageViewModel {
        public ChartViewModel(SecurityPage page, int displayOrder)
            : base(page, displayOrder) {
        }
    }
}