﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using Sunlight.Silverlight.Dcs.Common.Dashboard.Views;
using Sunlight.Silverlight.Dcs.Controls;
using Sunlight.Silverlight.Dcs.Dashboard;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.ViewModels {
    /// <summary>
    /// 表达主控台信息的ViewModel
    /// </summary>
    public class DashboardViewModel : ViewModelBase {
        private enum SecurityPageType {
            系统 = 0,
            分组 = 1,
            页面 = 2
        }

        internal const int SALES_TASK_NOTIFICATION_ID = 98511;
        internal const int SERVICE_TASK_NOTIFICATION_ID = 98512;

        private WarningCollectionViewModel warnings, todoWarnings;
        private ChartCollectionViewModel charts;
        private ShortcutCollectionViewModel shortcuts;
        private SpecialTaskWarningCollectionViewModel specialTasks;
        private DashboardSetting setting;
        private DashboardClient proxy;
        private bool initializing;
        private bool isRefreshing;
        private bool isSuspended;

        /// <summary>
        /// DashboardService 的代理
        /// </summary>
        private DashboardClient Proxy {
            get {
                if(this.proxy == null) {
                    this.proxy = new DashboardClient();
                    this.proxy.GetDashboardSettingCompleted += Proxy_GetDashboardSettingCompleted;
                }
                return this.proxy;
            }
        }

        /// <summary>
        /// 设置信息
        /// </summary>
        public DashboardSetting Setting {
            get {
                return this.setting;
            }
            private set {
                if(this.setting != value) {
                    this.setting = value;
                    this.setting.PropertyChanged -= Setting_PropertyChanged;
                    this.setting.PropertyChanged += Setting_PropertyChanged;
                    this.OnPropertyChanged("Setting");
                }
            }
        }

        /// <summary>
        /// 特殊类型的警告信息，目前包括销售任务提醒和服务任务提醒。
        /// </summary>
        public SpecialTaskWarningCollectionViewModel SpecialTasks {
            get {
                return this.specialTasks;
            }
            private set {
                if(this.specialTasks != value) {
                    this.specialTasks = value;
                    this.OnPropertyChanged("SpecialTasks");
                }
            }
        }

        /// <summary>
        /// 可显示的警告信息集合
        /// </summary>
        public WarningCollectionViewModel Warnings {
            get {
                return this.warnings;
            }
            private set {
                if(this.warnings != value) {
                    this.warnings = value;
                    this.OnPropertyChanged("Warnings");
                }
            }
        }

        /// <summary>
        /// 可显示的待办事项警告信息集合
        /// </summary>
        public WarningCollectionViewModel TodoWarnings {
            get {
                return this.todoWarnings;
            }
            private set {
                if(this.todoWarnings != value) {
                    this.todoWarnings = value;
                    this.OnPropertyChanged("TodoWarnings");
                }
            }
        }

        /// <summary>
        /// 可显示的图表信息集合
        /// </summary>
        public ChartCollectionViewModel Charts {
            get {
                return this.charts;
            }
            private set {
                if(this.charts != value) {
                    this.charts = value;
                    this.OnPropertyChanged("Charts");
                }
            }
        }

        /// <summary>
        /// 可显示的快捷方式集合
        /// </summary>
        public ShortcutCollectionViewModel Shortcuts {
            get {
                return this.shortcuts;
            }
            private set {
                if(this.shortcuts != value) {
                    this.shortcuts = value;
                    this.OnPropertyChanged("Shortcuts");
                }
            }
        }

        /// <summary>
        /// 正在初始化
        /// </summary>
        public bool Initializing {
            get {
                return this.initializing;
            }
            private set {
                if(this.initializing != value) {
                    this.initializing = value;
                    this.OnPropertyChanged("Initializing");
                }
            }
        }

        /// <summary>
        /// 正在刷新数据
        /// </summary>
        public bool IsRefreshing {
            get {
                return this.isRefreshing;
            }
            private set {
                if(this.isRefreshing != value) {
                    this.isRefreshing = value;
                    this.OnPropertyChanged("IsRefreshing");
                    this.RefreshCommand.RaiseCanExecuteChanged();
                    this.SetCommand.RaiseCanExecuteChanged();
                }
            }
        }

        /// <summary>
        /// 操作进行中，正忙
        /// </summary>
        public bool IsBusy {
            get {
                return this.Initializing || this.IsRefreshing;
            }
        }

        public DelegateCommand RefreshCommand {
            get;
            private set;
        }

        public DelegateCommand SetCommand {
            get;
            private set;
        }

        /// <summary>
        /// 挂起状态，处于挂起状态时要求子控件一并挂起。
        /// </summary>
        public bool IsSuspended {
            get {
                return this.isSuspended;
            }
            set {
                if(this.isSuspended == value)
                    return;
                this.isSuspended = value;

                if(this.SpecialTasks != null) {
                    this.SpecialTasks.IsSuspended = this.isSuspended;
                }
                if(this.Warnings != null) {
                    this.Warnings.IsSuspended = this.isSuspended;
                }
                if(this.TodoWarnings != null) {
                    this.TodoWarnings.IsSuspended = this.isSuspended;
                }
            }
        }

        private bool CanRefresh() {
            return !this.IsBusy;
        }

        private void Refresh() {
            if(this.IsRefreshing)
                return;
            this.Proxy.GetDashboardSettingAsync();
            this.IsRefreshing = true;
        }

        private bool CanSetDashboard() {
            return !this.IsBusy;
        }

        private void SetDashboard() {
            var radWindow = new DmsPopupWindow {
                MinHeight = 300,
                MinWidth = 400,
                Header = "主控台设置",
                IsRestricted = true,
                WindowState = WindowState.Maximized,
                FontFamily = new FontFamily("Microsoft YaHei")
            };
            var viewModel = new SettingViewModel(this.Setting, radWindow);
            radWindow.Content = new SettingView {
                DataContext = viewModel
            };
            radWindow.Closed += (sender, args) => {
                if(!args.DialogResult.HasValue || !args.DialogResult.Value)
                    return;
                this.Setting.AuthorizedPages = viewModel.SelectedPages.ToArray();
            };

            radWindow.ShowDialog();
        }

        private void UpdateWarningsAndCharts() {
            //警告和图表
            var warningItems = new List<WarningViewModel>();
            var todoItems = new List<WarningViewModel>();
            var chartItems = new List<ChartViewModel>();
            var specialItems = new List<WarningViewModel>();
            if(this.Setting != null && this.Setting.AuthorizedPages != null) {
                foreach(var authorizedPage in this.Setting.AuthorizedPages.Where(page => page.Type == (int)SecurityPageType.页面).OrderBy(o => o.Id))
                    switch(authorizedPage.PageType) {
                        case "Warning":
                            if(authorizedPage.Id == SALES_TASK_NOTIFICATION_ID || authorizedPage.Id == SERVICE_TASK_NOTIFICATION_ID)
                                specialItems.Add(new WarningViewModel(authorizedPage, authorizedPage.Sequence));
                            else if(authorizedPage.PageUri != null)
                                todoItems.Add(new WarningViewModel(authorizedPage, authorizedPage.Sequence));
                            else
                                warningItems.Add(new WarningViewModel(authorizedPage, authorizedPage.Sequence));
                            break;
                        case "Chart":
                            chartItems.Add(new ChartViewModel(authorizedPage, authorizedPage.Sequence));
                            break;
                    }
            }
            this.Warnings = new WarningCollectionViewModel(warningItems);
            this.TodoWarnings = new WarningCollectionViewModel(todoItems);
            this.Charts = new ChartCollectionViewModel(chartItems);
            this.SpecialTasks = new SpecialTaskWarningCollectionViewModel(specialItems);
        }

        /// <summary>
        /// 配置更新后刷新警告数据、统计图表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Setting_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            switch(e.PropertyName) {
                case "AuthorizedPages":
                    this.UpdateWarningsAndCharts();
                    break;
            }
        }

        private void Proxy_GetDashboardSettingCompleted(object sender, GetDashboardSettingCompletedEventArgs e) {
            this.Initializing = false;
            this.IsRefreshing = false;
            if(e.Error == null) {
                this.Setting = e.Result;
                this.UpdateWarningsAndCharts();
            }
            //TODO: 初始化失败，应给出提示。
        }

        public DashboardViewModel() {
            this.RefreshCommand = new DelegateCommand(this.Refresh, this.CanRefresh);
            this.SetCommand = new DelegateCommand(this.SetDashboard, this.CanSetDashboard);
        }

        public void Initialize() {
            this.Initializing = true;
            this.Proxy.GetDashboardSettingAsync();
            this.IsRefreshing = true;
            this.Shortcuts = new ShortcutCollectionViewModel();
        }
    }
}
