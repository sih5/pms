﻿using System;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.ViewModels {
    /// <summary>
    /// 可排序的ViewModel，排序属性为<see cref="DisplayOrder"/>。
    /// </summary>
    public class OrderedViewModelBase : ViewModelBase, IComparable {
        private int displayOrder;

        /// <summary>
        /// 指定排序顺序
        /// </summary>
        public int DisplayOrder {
            get {
                return this.displayOrder;
            }
            protected set {
                if(this.displayOrder != value) {
                    this.displayOrder = value;
                    this.OnPropertyChanged("DisplayOrder");
                }
            }
        }

        public virtual int CompareTo(object obj) {
            var viewModel = obj as OrderedViewModelBase;
            if(viewModel == null)
                return 1;
            return this.DisplayOrder - viewModel.DisplayOrder;
        }
    }
}