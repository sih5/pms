﻿using System;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Dcs.Dashboard;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.ViewModels {
    /// <summary>
    ///     表达某个流程节点信息的ViewModel
    /// </summary>
    public class PageViewModel : OrderedViewModelBase {
        private string displayName;
        private string description;
        private Uri iconUri;
        private Uri pageUri;
        private DelegateCommand openPage;

        /// <summary>
        ///     该节点的Id，对应Page数据表的Id列。
        /// </summary>
        public int Id {
            get;
            protected set;
        }

        /// <summary>
        ///     该节点的名称，根据语言对应Page数据表的Name/Name_enUS/Name_jaJP列。
        /// </summary>
        public string DisplayName {
            get {
                return this.displayName;
            }
            protected set {
                if(this.displayName != value) {
                    this.displayName = value;
                    this.OnPropertyChanged("DisplayName");
                }
            }
        }

        /// <summary>
        ///     该节点的描述，根据语言对应Page数据表的Description/Description_enUS/Description_jaJP列。
        /// </summary>
        public string Description {
            get {
                return this.description;
            }
            protected set {
                if(this.description != value) {
                    this.description = value;
                    this.OnPropertyChanged("Description");
                }
            }
        }

        /// <summary>
        ///     该节点的图标路径，这是针对于服务端的相对路径。对应Page数据表的Icon列。
        /// </summary>
        /// <example>"Client/Images/Dms/Menu/WarehouseOrder.png"</example>
        public Uri IconUri {
            get {
                return this.iconUri;
            }
            protected set {
                if(this.iconUri != value) {
                    this.iconUri = value;
                    this.OnPropertyChanged("ButtonImage");
                }
            }
        }

        /// <summary>
        ///     该节点关联的页面路径，这是针对于服务端的相对路径。
        /// </summary>
        /// <example>"/Sales/SaleOrder"</example>
        public Uri PageUri {
            get {
                return this.pageUri;
            }
            protected set {
                if(this.pageUri != value) {
                    this.pageUri = value;
                    this.OnPropertyChanged("PageUri");
                }
            }
        }

        /// <summary>
        ///     在程序中加载当前页面。
        /// </summary>
        public DelegateCommand OpenPage {
            get {
                return this.openPage;
            }
            private set {
                if(this.openPage != value) {
                    this.openPage = value;
                    this.OnPropertyChanged("OpenPage");
                }
            }
        }

        protected virtual void OpenPageAction() {
            ShellViewModel.Current.PageUri = this.PageUri;
        }

        protected virtual bool CanOpenPage() {
            return this.PageUri != null;
        }

        public PageViewModel(int displayOrder) {
            this.DisplayOrder = displayOrder;
            this.OpenPage = new DelegateCommand(this.OpenPageAction, this.CanOpenPage);
        }

        public PageViewModel(SecurityPage page, int displayOrder)
            : this(displayOrder) {
            this.Id = page.Id;
            this.DisplayName = page.DisplayName;
            this.Description = page.Description;
            this.IconUri = DashboardUtils.EnsureServerUri(page.IconUri);
            this.PageUri = page.PageUri;
        }
    }
}
