﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using Sunlight.Silverlight.Dcs.Common.Dashboard.Utils;
using Sunlight.Silverlight.Dcs.Dashboard;
using Sunlight.Silverlight.Model;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.ViewModels {
    public class SettingViewModel : ViewModelBase {
        private DashboardClient proxy;
        private bool isBusy;
        private ObservableCollection<SecurityPage> availableWarnings, selectedWarnings;
        private ObservableCollection<SecurityPage> availableTodoWarnings, selectedTodoWarnings;
        private ObservableCollection<SecurityPage> availableCharts, selectedCharts;
        private ObservableCollection<PageMenuItem> shortcuts;
        private readonly RadWindow window;

        private DashboardClient Proxy {
            get {
                if(this.proxy == null) {
                    this.proxy = new DashboardClient();
                    this.proxy.GetAllAuthorizedPagesCompleted += this.Proxy_GetAllAuthorizedPagesCompleted;
                    this.proxy.SaveDashboardSettingCompleted += this.Proxy_SaveDashboardSettingCompleted;
                }
                return this.proxy;
            }
        }

        public bool IsBusy {
            get {
                return this.isBusy;
            }
            private set {
                if(this.isBusy == value)
                    return;
                this.isBusy = value;
                this.OnPropertyChanged("IsBusy");
            }
        }

        public ObservableCollection<SecurityPage> AvailableWarnings {
            get {
                return this.availableWarnings;
            }
            private set {
                if(this.availableWarnings == value)
                    return;
                this.availableWarnings = value;
                this.OnPropertyChanged("AvailableWarnings");
            }
        }

        public ObservableCollection<SecurityPage> SelectedWarnings {
            get {
                return this.selectedWarnings;
            }
            private set {
                if(this.selectedWarnings == value)
                    return;
                this.selectedWarnings = value;
                this.OnPropertyChanged("SelectedWarnings");
            }
        }

        public ObservableCollection<SecurityPage> AvailableTodoWarnings {
            get {
                return this.availableTodoWarnings;
            }
            private set {
                if(this.availableTodoWarnings == value)
                    return;
                this.availableTodoWarnings = value;
                this.OnPropertyChanged("AvailableTodoWarnings");
            }
        }

        public ObservableCollection<SecurityPage> SelectedTodoWarnings {
            get {
                return this.selectedTodoWarnings;
            }
            private set {
                if(this.selectedTodoWarnings == value)
                    return;
                this.selectedTodoWarnings = value;
                this.OnPropertyChanged("SelectedTodoWarnings");
            }
        }

        public ObservableCollection<SecurityPage> AvailableCharts {
            get {
                return this.availableCharts;
            }
            private set {
                if(this.availableCharts == value)
                    return;
                this.availableCharts = value;
                this.OnPropertyChanged("AvailableCharts");
            }
        }

        public ObservableCollection<SecurityPage> SelectedCharts {
            get {
                return this.selectedCharts;
            }
            private set {
                if(this.selectedCharts == value)
                    return;
                this.selectedCharts = value;
                this.OnPropertyChanged("SelectedCharts");
            }
        }

        public ObservableCollection<PageMenuItem> Shortcuts {
            get {
                return this.shortcuts;
            }
            private set {
                if(this.shortcuts == value)
                    return;
                this.shortcuts = value;
                this.OnPropertyChanged("Shortcuts");
            }
        }

        public IEnumerable<SecurityPage> SelectedPages {
            get {
                return this.SelectedWarnings.Union(this.SelectedTodoWarnings).Union(this.SelectedCharts);
            }
        }

        public DelegateCommand SaveCommand {
            get;
            private set;
        }

        private void Save() {
            ShellViewModel.Current.FavoritePageManager.ChangeMenuItemsOrder(this.Shortcuts.Select(item => item.Id).ToList());
            for(var i = 0; i < this.SelectedWarnings.Count; i++)
                this.SelectedWarnings[i].Sequence = i;
            for(var i = 0; i < this.SelectedTodoWarnings.Count; i++)
                this.SelectedTodoWarnings[i].Sequence = i;
            for(var i = 0; i < this.SelectedCharts.Count; i++)
                this.SelectedCharts[i].Sequence = i;
            this.Proxy.SaveDashboardSettingAsync(this.SelectedPages.ToArray());
            this.IsBusy = true;
        }

        private void Proxy_SaveDashboardSettingCompleted(object sender, AsyncCompletedEventArgs e) {
            this.IsBusy = false;
            if(this.window != null) {
                this.window.DialogResult = true;
                this.window.Close();
            }
        }

        private void Proxy_GetAllAuthorizedPagesCompleted(object sender, GetAllAuthorizedPagesCompletedEventArgs e) {
            this.IsBusy = false;
            if(e.Error != null) //TODO: 获取数据失败，应要求用户重新打开此界面。
                return;

            var securityPageComparer = new SecurityPageComparer();
            var authorizedWarnings = e.Result.Where(page => page.PageType == "Warning" && page.PageUri == null);
            this.AvailableWarnings = new ObservableCollection<SecurityPage>(authorizedWarnings.Except(this.SelectedWarnings, securityPageComparer));

            var authorizedTodoWarnings = e.Result.Where(page => page.PageType == "Warning" && page.PageUri != null);
            this.AvailableTodoWarnings = new ObservableCollection<SecurityPage>(authorizedTodoWarnings.Except(this.SelectedTodoWarnings, securityPageComparer));

            var authorizedCharts = e.Result.Where(page => page.PageType == "Chart");
            this.AvailableCharts = new ObservableCollection<SecurityPage>(authorizedCharts.Except(this.SelectedCharts, securityPageComparer));
        }

        public SettingViewModel(DashboardSetting setting, RadWindow window) {
            this.SaveCommand = new DelegateCommand(this.Save);

            var warnings = new List<SecurityPage>();
            var todoWarnings = new List<SecurityPage>();
            var charts = new List<SecurityPage>();
            if(setting != null) {
                warnings = setting.AuthorizedPages.Where(page => page.PageType == "Warning" && page.PageUri == null).ToList();
                warnings.Sort((x, y) => x.Sequence - y.Sequence);

                todoWarnings = setting.AuthorizedPages.Where(page => page.PageType == "Warning" && page.PageUri != null).ToList();
                todoWarnings.Sort((x, y) => x.Sequence - y.Sequence);

                charts = setting.AuthorizedPages.Where(page => page.PageType == "Chart").ToList();
                charts.Sort((x, y) => x.Sequence - y.Sequence);
            }
            this.SelectedWarnings = new ObservableCollection<SecurityPage>(warnings);
            this.AvailableWarnings = new ObservableCollection<SecurityPage>();

            this.SelectedTodoWarnings = new ObservableCollection<SecurityPage>(todoWarnings);
            this.AvailableTodoWarnings = new ObservableCollection<SecurityPage>();

            this.SelectedCharts = new ObservableCollection<SecurityPage>(charts);
            this.AvailableCharts = new ObservableCollection<SecurityPage>();

            this.Shortcuts = new ObservableCollection<PageMenuItem>(ShellViewModel.Current.FavoritePageManager.MenuItems.ToList());
            this.Proxy.GetAllAuthorizedPagesAsync();
            this.IsBusy = true;

            //TODO: 待改善。
            this.window = window;
        }
    }
}
