﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using Sunlight.Silverlight.Model;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.ViewModels {
    public class ShortcutCollectionViewModel : ViewModelBase {
        private ObservableCollection<ShortcutViewModel> shortcutItems;

        public ObservableCollection<ShortcutViewModel> ShortcutItems {
            get {
                return this.shortcutItems;
            }
            private set {
                if(this.shortcutItems != value) {
                    this.shortcutItems = value;
                    foreach(var shortcutViewModel in this.shortcutItems) {
                        shortcutViewModel.CloseClicked -= this.ShortcutViewModel_CloseClicked;
                        shortcutViewModel.CloseClicked += this.ShortcutViewModel_CloseClicked;
                    }
                    this.shortcutItems.CollectionChanged += this.ShortcutItems_CollectionChanged;
                    this.OnPropertyChanged("ShortcutItems");
                }
            }
        }

        private void ShortcutItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
            if(e.NewItems != null)
                foreach(ShortcutViewModel item in e.NewItems) {
                    item.CloseClicked -= this.ShortcutViewModel_CloseClicked;
                    item.CloseClicked += this.ShortcutViewModel_CloseClicked;
                }

            if(e.OldItems != null)
                foreach(ShortcutViewModel item in e.OldItems)
                    item.CloseClicked -= this.ShortcutViewModel_CloseClicked;
        }

        private void ShortcutViewModel_CloseClicked(object sender, EventArgs e) {
            var shortcut = sender as ShortcutViewModel;
            if(shortcut != null) {
                this.ShortcutItems.Remove(shortcut);

                var favoritePageManager = ShellViewModel.Current.FavoritePageManager;
                var menuItem = favoritePageManager.MenuItems.SingleOrDefault(item => item.Id == shortcut.Id);
                if(menuItem != null)
                    favoritePageManager.Remove(menuItem);
            }
        }

        private void FavoritePageManager_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
            switch(e.Action) {
                case NotifyCollectionChangedAction.Add:
                    foreach(MenuItem newItem in e.NewItems)
                        this.ShortcutItems.Add(new ShortcutViewModel(newItem, this.ShortcutItems.Count));
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach(MenuItem oldItem in e.OldItems) {
                        var shortcutItem = this.ShortcutItems.SingleOrDefault(shortcut => shortcut.Id == oldItem.Id);
                        if(shortcutItem != null)
                            this.ShortcutItems.Remove(shortcutItem);
                    }
                    break;
                case NotifyCollectionChangedAction.Reset:
                    this.ShortcutItems.Clear();
                    break;
                case NotifyCollectionChangedAction.Replace:
                    foreach(MenuItem newItem in e.NewItems)
                        this.ShortcutItems[e.NewStartingIndex] = new ShortcutViewModel(newItem, e.NewStartingIndex);
                    break;
            }
        }

        public ShortcutCollectionViewModel() {
            this.ShortcutItems = new ObservableCollection<ShortcutViewModel>();
            foreach(var menuItem in ShellViewModel.Current.FavoritePageManager.MenuItems)
                this.ShortcutItems.Add(new ShortcutViewModel(menuItem, this.ShortcutItems.Count));
            ShellViewModel.Current.FavoritePageManager.CollectionChanged += this.FavoritePageManager_CollectionChanged;
        }
    }
}
