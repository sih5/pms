﻿using System;
using Sunlight.Silverlight.Dcs.Dashboard;
using Sunlight.Silverlight.Model;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.ViewModels {
    /// <summary>
    /// 指向某个流程节点的快捷方式ViewModel
    /// </summary>
    public class ShortcutViewModel : PageViewModel {
        public event EventHandler CloseClicked;

        public DelegateCommand CloseCommand {
            get;
            private set;
        }

        private void Close(object parameter) {
            var handler = this.CloseClicked;
            if(handler != null)
                handler(this, new EventArgs());
        }

        public ShortcutViewModel(SecurityPage page, int displayOrder)
            : base(page, displayOrder) {
            this.CloseCommand = new DelegateCommand(this.Close);
        }

        public ShortcutViewModel(MenuItem menuItem, int displayOrder)
            : base(displayOrder) {
            this.Id = menuItem.Id;
            this.DisplayName = menuItem.DisplayName;
            this.Description = menuItem.Description;
            this.IconUri = menuItem.ImageUri;
            this.PageUri = ShellViewModel.Current.GetPageUri(menuItem.Id);

            this.CloseCommand = new DelegateCommand(this.Close);
        }
    }
}
