﻿using System.Collections.Generic;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.ViewModels {
    public class SpecialTaskWarningCollectionViewModel : WarningCollectionViewModel {
        private WarningViewModel salesTask;
        private WarningViewModel serviceTask;

        /// <summary>
        /// 销售任务警告信息
        /// </summary>
        public WarningViewModel SalesTask {
            get {
                return this.salesTask;
            }
            private set {
                if(this.salesTask != value) {
                    this.salesTask = value;
                    this.OnPropertyChanged("SalesTask");
                }
            }
        }

        /// <summary>
        /// 服务任务警告信息
        /// </summary>
        public WarningViewModel ServiceTask {
            get {
                return this.serviceTask;
            }
            private set {
                if(this.serviceTask != value) {
                    this.serviceTask = value;
                    this.OnPropertyChanged("ServiceTask");
                }
            }
        }

        public SpecialTaskWarningCollectionViewModel(List<WarningViewModel> warningItems)
            : base(warningItems) {
            this.SalesTask = this.WarningItems.SingleOrDefault(item => item.Id == DashboardViewModel.SALES_TASK_NOTIFICATION_ID);
            this.ServiceTask = this.WarningItems.SingleOrDefault(item => item.Id == DashboardViewModel.SERVICE_TASK_NOTIFICATION_ID);
        }
    }
}
