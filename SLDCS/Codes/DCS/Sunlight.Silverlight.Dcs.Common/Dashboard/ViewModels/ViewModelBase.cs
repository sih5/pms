﻿using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.ViewModels {
    /// <summary>
    /// MVVM模式中ViewModel基类，已实现<see cref="INotifyPropertyChanged"/>。
    /// </summary>
    /// <example>
    /// 需向外界通知变更的属性建议定义如下：
    /// <code>
    /// private string displayName;
    /// 
    /// public string DisplayName {
    ///     get {
    ///         return this.displayName;
    ///     }
    ///     protected set {
    ///         if(this.displayName != value) {
    ///             this.displayName = value;
    ///             this.OnPropertyChanged("DisplayName");
    ///         }
    ///     }
    /// }
    /// </code>
    /// </example>
    public class ViewModelBase : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
