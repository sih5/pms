﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Threading;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Dcs.Dashboard;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.ViewModels {
    /// <summary>
    /// 表达一组警告信息 <see cref="WarningViewModel"/> 的 ViewModel，仍对应着 Page 数据表的某条记录。
    /// </summary>
    public class WarningCollectionViewModel : ViewModelBase {
        private DashboardClient proxy;
        private bool isBusy, isSuspended;
        private DispatcherTimer elaspedTimer;
        private string elaspedTimeText;
        private DateTime lastUpdated;
        private ObservableCollection<WarningViewModel> warningItems;
        private DispatcherTimer timer;

        /// <summary>
        /// DashboardSerivce 的代理
        /// </summary>
        private DashboardClient Proxy {
            get {
                if(this.proxy == null) {
                    this.proxy = new DashboardClient();
                    this.proxy.GetWarningsCompleted += Proxy_GetWarningsCompleted;
                }
                return this.proxy;
            }
        }

        /// <summary>
        /// 更新最近一次刷新时间文本 <see cref="ElaspedTimeText"/> 的计时器，5秒钟刷新一次
        /// </summary>
        private DispatcherTimer ElaspedTimer {
            get {
                if(this.elaspedTimer == null) {
                    this.elaspedTimer = new DispatcherTimer {
                        Interval = new TimeSpan(0, 0, 5)
                    };
                    this.elaspedTimer.Tick += (sender, args) => this.UpdateElaspedTime();
                }
                return this.elaspedTimer;
            }
        }

        /// <summary>
        ///    定时获取警告信息值的计时器，5分钟刷新一次
        /// </summary>
        private DispatcherTimer Timer {
            get {
                if(this.timer == null) {
                    this.timer = new DispatcherTimer {
                        Interval = new TimeSpan(0, 5, 0)
                    };
                    this.timer.Tick += (sender, args) => {
                        if(!this.IsBusy)
                            this.Refresh.Execute();
                    };
                }
                return this.timer;
            }
        }

        /// <summary>
        /// 该集合中包括的所有警告信息
        /// </summary>
        public ObservableCollection<WarningViewModel> WarningItems {
            get {
                return this.warningItems;
            }
            private set {
                if(this.warningItems != value) {
                    this.warningItems = value;
                    this.OnPropertyChanged("WarningItems");
                }
            }
        }

        /// <summary>
        /// 正在执行刷新操作
        /// </summary>
        public bool IsBusy {
            get {
                return this.isBusy;
            }
            private set {
                if(this.isBusy != value) {
                    this.isBusy = value;
                    this.OnPropertyChanged("IsBusy");
                    this.Refresh.RaiseCanExecuteChanged();
                }
            }
        }

        /// <summary>
        /// 刷新操作
        ///
        /// 向服务端获取该集合中所有警告信息的返回值，并填充至 <see cref="WarningItems"/> 的每个警告的 <see cref="WarningViewModel.DisplayText"/> 属性。
        /// </summary>
        public DelegateCommand Refresh {
            get;
            private set;
        }

        /// <summary>
        /// 当前时间距离最近一次刷新时间的间隔
        /// </summary>
        public string ElaspedTimeText {
            get {
                return this.elaspedTimeText;
            }
            private set {
                if(this.elaspedTimeText != value) {
                    this.elaspedTimeText = value;
                    this.OnPropertyChanged("ElaspedTimeText");
                }
            }
        }

        /// <summary>
        ///     处于挂起状态，在挂起时不刷新 <see cref="WarningItems"/> 和 <see cref="ElaspedTimeText"/>。
        /// </summary>
        public bool IsSuspended {
            get {
                return this.isSuspended;
            }
            set {
                if(this.isSuspended == value)
                    return;
                this.isSuspended = value;
                if(this.isSuspended) {
                    this.ElaspedTimer.Stop();
                    this.Timer.Stop();
                } else {
                    if(!this.ElaspedTimer.IsEnabled)
                        this.ElaspedTimer.Start();
                    this.Timer.Start();
                }
            }
        }

        private void UpdateElaspedTime() {
            this.ElaspedTimeText = (DateTime.Now - this.lastUpdated).ToString(@"hh\:mm\:ss");
        }

        private void Proxy_GetWarningsCompleted(object sender, GetWarningsCompletedEventArgs e) {
            this.IsBusy = false;
            if(e.Error == null && e.Result != null) {
                this.lastUpdated = DateTime.Now;
                foreach(var keyValuePair in e.Result) {
                    var warningItem = this.WarningItems.SingleOrDefault(item => item.Id == keyValuePair.Key);
                    if(warningItem != null)
                        warningItem.DisplayText = keyValuePair.Value;
                }
                this.WarningItems = new ObservableCollection<WarningViewModel>(this.WarningItems.OrderByDescending(r => Convert.ToInt32(r.DisplayText)).ToList());
                if(!this.ElaspedTimer.IsEnabled)
                    this.ElaspedTimer.Start();
                this.UpdateElaspedTime();
            }
            //TODO:刷新操作失败，应给出提示。
        }

        private bool CanRefresh() {
            return !this.IsBusy;
        }

        private void RefreshAction() {
            if(this.IsBusy)
                return;
            this.Proxy.GetWarningsAsync(this.WarningItems.Select(item => item.Id).ToArray());
            this.IsBusy = true;
        }

        public WarningCollectionViewModel(List<WarningViewModel> warningItems) {
            this.WarningItems = new ObservableCollection<WarningViewModel>(warningItems);
            this.Refresh = new DelegateCommand(this.RefreshAction, this.CanRefresh);
            this.Refresh.Execute();
            this.Timer.Start();
        }
    }
}
