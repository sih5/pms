﻿using Sunlight.Silverlight.Dcs.Dashboard;
namespace Sunlight.Silverlight.Dcs.Common.Dashboard.ViewModels {
    /// <summary>
    /// 表达警告信息的ViewModel，可指向某个流程节点。
    /// </summary>
    public class WarningViewModel : PageViewModel {
        private string displayText;

        /// <summary>
        /// 用于界面显示的文本，<see cref="WarningCollectionViewModel"/> 在服务端获取该警告返回值后将对其赋值。
        /// </summary>
        public string DisplayText {
            get {
                return this.displayText;
            }
            set {
                if(this.displayText != value) {
                    this.displayText = value;
                    this.OnPropertyChanged("DisplayText");
                }
            }
        }

        public WarningViewModel(SecurityPage page, int displayOrder)
            : base(page, displayOrder) {
        }
    }
}
