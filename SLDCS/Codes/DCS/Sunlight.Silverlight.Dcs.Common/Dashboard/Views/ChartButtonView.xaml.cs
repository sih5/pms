﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.Views {
    public partial class ChartButtonView {
        private readonly Storyboard sbMarquee;

        private void DescriptionCanvas_OnSizeChanged(object sender, SizeChangedEventArgs e) {
            this.contentCanvas.Clip = new RectangleGeometry {
                Rect = new Rect(0, 0, this.contentCanvas.Width, this.contentCanvas.Height)
            };
        }

        private void FrameworkElement_OnMouseEnter(object sender, System.Windows.Input.MouseEventArgs e) {
            var range = this.contentCanvas.Width - this.contentText.ActualWidth;
            if(range >= 0)
                return;

            var beginTime = TimeSpan.FromSeconds(0.5);
            var pauseTime = TimeSpan.FromSeconds(1.3);
            var duration = TimeSpan.FromSeconds(-range / 50);
            this.sbMarquee.Duration = new Duration(beginTime + duration + pauseTime);
            var da = new DoubleAnimation {
                BeginTime = beginTime,
                Duration = new Duration(duration),
                From = 0,
                To = range,
            };
            Storyboard.SetTarget(da, this.contentText);
            Storyboard.SetTargetProperty(da, new PropertyPath("(Canvas.Left)"));
            this.sbMarquee.Children.Clear();
            this.sbMarquee.Children.Add(da);
            this.sbMarquee.Stop();
            this.sbMarquee.Begin();
        }

        private void FrameworkElement_OnMouseLeave(object sender, System.Windows.Input.MouseEventArgs e) {
            this.sbMarquee.Stop();
        }

        private void FrameworkElement_OnUnloaded(object sender, RoutedEventArgs e) {
            this.sbMarquee.Stop();
        }

        public ChartButtonView() {
            InitializeComponent();

            this.sbMarquee = new Storyboard {
                AutoReverse = true,
                RepeatBehavior = RepeatBehavior.Forever,
            };
        }
    }
}
