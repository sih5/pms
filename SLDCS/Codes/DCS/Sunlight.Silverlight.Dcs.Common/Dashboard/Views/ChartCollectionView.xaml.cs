﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.Views {
    public partial class ChartCollectionView {
        private void Frame_NavigationFailed(object sender, NavigationFailedEventArgs e) {
            e.Handled = true;
            var navigationService = sender as NavigationService;
            if(navigationService != null)
                navigationService.Navigate(new Uri("/Sunlight.Silverlight.Dms.Common;component/Dashboard/Charts/Views/Default.xaml", UriKind.Relative));
        }

        private void FrameworkElement_OnSizeChanged(object sender, SizeChangedEventArgs e) {
            var listBox = sender as ListBox;
            if(listBox == null || listBox.SelectedItem == null)
                return;

            listBox.UpdateLayout();
            listBox.ScrollIntoView(listBox.SelectedItem);
        }

        public ChartCollectionView() {
            this.InitializeComponent();
        }
    }
}
