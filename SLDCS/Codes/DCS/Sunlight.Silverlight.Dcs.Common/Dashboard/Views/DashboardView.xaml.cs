﻿using System;
using System.IO.IsolatedStorage;
using System.ServiceModel.DomainServices.Client;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media.Imaging;
using Sunlight.Silverlight.Dcs.Common.Dashboard.ViewModels;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.Views {
    public partial class DashboardView {
        private readonly IsolatedStorageSettings appSettings = IsolatedStorageSettings.ApplicationSettings;
        private readonly string prefix = BaseApp.Current.CurrentUserData.EnterpriseCode + "/" + BaseApp.Current.CurrentUserData.UserCode + "/";

        /// <summary>
        /// 统计信息相对于待办事项的高度比
        /// </summary>
        private string WarningTabHeightKey {
            get {
                return this.prefix + "Dashboard_WarningTabHeight";
            }
        }

        private bool initialized;

        private DashboardViewModel ViewModel {
            get {
                return (DashboardViewModel)this.DataContext;
            }
        }

        private void LoadSettingsFromIsolatedStorage() {
            try {
                if(this.appSettings.Contains(this.WarningTabHeightKey))
                    this.TopRow.Height = new GridLength((double)this.appSettings[this.WarningTabHeightKey], GridUnitType.Star);
            } catch(IsolatedStorageException) {
                // 用户在打开业务系统后，删除应用程序存储，避免因此造成的访问异常。
            }
        }
        /// <summary>
        /// 如果登陆企业为服务站，并且只属于一家分公司，存在图片维护，则显示分公司图片
        /// 如果登陆企业为代理库，并且只属于一家分公司，存在图片维护，则显示分公司图片
        /// 如果登陆企业为分公司，并且存在图片维护，则显示分公司图片
        /// </summary>
        private void LoadImg() {
            var uri=  Core.Utils.MakeServerUri("/");
            var uriAd= Core.Utils.MakeServerUri(string.Format("Client/Images/loginpic{0}.png",uri.Port));
            ImgLogin.Source = new BitmapImage(uriAd);
        }

        private void SaveSettingsFromIsolatedStorage() {
            try {
                this.appSettings[this.WarningTabHeightKey] = this.TopRow.Height.Value / this.BottomRow.Height.Value;
            } catch(IsolatedStorageException) {
                // 用户在打开业务系统后，删除应用程序存储，避免因此造成的访问异常。
            }
        }

        private void DashboardView_Loaded(object sender, RoutedEventArgs e) {
            if(this.initialized) {
                this.ViewModel.IsSuspended = false;
            }

            if(this.initialized)
                return;
            this.initialized = true;
            this.ViewModel.Initialize();
            this.LoadSettingsFromIsolatedStorage();
            //加载图片
            this.LoadImg();
        }

        private void DashboardView_Unloaded(object sender, RoutedEventArgs e) {
            this.ViewModel.IsSuspended = true;
        }

        private void GridSplitter_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e) {
            this.SaveSettingsFromIsolatedStorage();
        }

        public DashboardView() {
            this.InitializeComponent();
            this.Unloaded += DashboardView_Unloaded;
            this.Language = XmlLanguage.GetLanguage(Thread.CurrentThread.CurrentUICulture.Name);
        }
    }
}
