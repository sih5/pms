﻿using Sunlight.Silverlight.Dcs.Common.Dashboard.Utils;
using System.Windows.Controls;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.Views {
    public partial class ShortcutCollectionView {
        private VirtualizingStackPanel mainListBoxItemsHost;

        private VirtualizingStackPanel MainListBoxItemsHost {
            get {
                return this.mainListBoxItemsHost ?? (this.mainListBoxItemsHost = this.mainListBox.GetItemsHost() as VirtualizingStackPanel);
            }
        }

        public DelegateCommand PrevCommand {
            get;
            private set;
        }

        public DelegateCommand NextCommand {
            get;
            private set;
        }

        private void MainListBox_LayoutUpdated(object sender, System.EventArgs e) {
            this.PrevCommand.InvalidateCanExecute();
            this.NextCommand.InvalidateCanExecute();
        }

        private bool CanNext(object parameter) {
            return this.MainListBoxItemsHost != null && (this.MainListBoxItemsHost.HorizontalOffset + this.MainListBoxItemsHost.ViewportWidth < this.mainListBox.Items.Count);
        }

        private bool CanPrev(object parameter) {
            return this.MainListBoxItemsHost != null && this.MainListBoxItemsHost.HorizontalOffset > 0.0;
        }

        private void Next(object parameter) {
            if(this.MainListBoxItemsHost != null)
                this.MainListBoxItemsHost.LineRight();
        }

        private void Prev(object parameter) {
            if(this.MainListBoxItemsHost != null)
                this.MainListBoxItemsHost.LineLeft();
        }

        public ShortcutCollectionView() {
            InitializeComponent();

            this.PrevCommand = new DelegateCommand(this.Prev, this.CanPrev);
            this.NextCommand = new DelegateCommand(this.Next, this.CanNext);
            this.mainListBox.LayoutUpdated += MainListBox_LayoutUpdated;
        }
    }
}
