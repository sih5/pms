﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.Views {
    public partial class ShortcutView {
        private readonly Storyboard sbMarquee;

        private void FrameworkElement_OnMouseEnter(object sender, MouseEventArgs e) {
            this.closeButton.Visibility = Visibility.Visible;

            var range = this.descriptionCanvas.Width - this.descriptionText.ActualWidth;
            if(range >= 0)
                return;

            var beginTime = TimeSpan.FromSeconds(1);
            var pauseTime = TimeSpan.FromSeconds(1.5);
            var duration = TimeSpan.FromSeconds(-range / 50);
            this.sbMarquee.Duration = new Duration(beginTime + duration + pauseTime);
            var da = new DoubleAnimation {
                BeginTime = beginTime,
                Duration = new Duration(duration),
                From = 0,
                To = range,
            };
            Storyboard.SetTarget(da, this.descriptionText);
            Storyboard.SetTargetProperty(da, new PropertyPath("(Canvas.Left)"));
            this.sbMarquee.Children.Clear();
            this.sbMarquee.Children.Add(da);
            this.sbMarquee.Stop();
            this.sbMarquee.Begin();
        }

        private void FrameworkElement_OnMouseLeave(object sender, MouseEventArgs e) {
            this.closeButton.Visibility = Visibility.Collapsed;

            this.sbMarquee.Stop();
        }

        private void DescriptionCanvas_OnSizeChanged(object sender, SizeChangedEventArgs e) {
            this.descriptionCanvas.Clip = new RectangleGeometry {
                Rect = new Rect(0, 0, this.descriptionCanvas.Width, this.descriptionCanvas.Height)
            };
        }

        private void FrameworkElement_OnUnloaded(object sender, RoutedEventArgs e) {
            this.sbMarquee.Stop();
        }

        public ShortcutView() {
            this.InitializeComponent();

            this.sbMarquee = new Storyboard {
                AutoReverse = true,
                RepeatBehavior = RepeatBehavior.Forever,
            };
        }
    }
}
