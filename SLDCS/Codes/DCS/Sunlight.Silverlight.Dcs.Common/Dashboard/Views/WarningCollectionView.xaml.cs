﻿using System.Windows;

namespace Sunlight.Silverlight.Dcs.Common.Dashboard.Views {
    public partial class WarningCollectionView {
        public static readonly DependencyProperty TitleProperty = DependencyProperty.Register("Title", typeof(string), typeof(WarningCollectionView), new PropertyMetadata(default(string)));

        public string Title {
            get {
                return (string)this.GetValue(TitleProperty);
            }
            set {
                this.SetValue(TitleProperty, value);
            }
        }

        public WarningCollectionView() {
            this.InitializeComponent();
        }
    }
}
