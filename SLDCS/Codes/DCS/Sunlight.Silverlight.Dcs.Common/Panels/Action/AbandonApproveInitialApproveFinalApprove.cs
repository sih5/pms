﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class AbandonApproveInitialApproveFinalApprove : DcsActionPanelBase {
        public AbandonApproveInitialApproveFinalApprove() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = CommonUIStrings.ActionPanel_Title_General,
                ActionItems = new[] {
                    new ActionItem {
                        Title = CommonUIStrings.Action_Title_Abandon,
                        UniqueId = CommonActionKeys.ABANDON,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Abandon.png"),
                        CanExecute = false
                    },
                    new ActionItem { 
                        Title = CommonUIStrings.Action_Title_InitialApprove, 
                        UniqueId = CommonActionKeys.INITIALAPPROVE, 
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Service/InitialApprove.png"), 
                        CanExecute = false 
                    }, new ActionItem {
                        Title = CommonUIStrings.Action_Title_Approve,
                        UniqueId = CommonActionKeys.APPROVE,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Approve.png"),
                        CanExecute = false
                    },
                    new ActionItem { 
                        Title = CommonUIStrings.Action_Title_FinalApprove, 
                        UniqueId = CommonActionKeys.FINALAPPROVE, 
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Service/FinalApprove.png"), 
                        CanExecute = false 
                    }
                }
            };
        }
    }
}
