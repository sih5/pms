﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class AddEditApproveExport : DcsActionPanelBase {
        public AddEditApproveExport() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = CommonUIStrings.ActionPanel_Title_General,
                ActionItems = new[] {
                    new ActionItem {
                        UniqueId = CommonActionKeys.ADD,
                        Title = CommonUIStrings.Action_Title_Add,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Add.png"),
                        CanExecute = false
                    }, new ActionItem {
                        UniqueId = CommonActionKeys.EDIT,
                        Title = CommonUIStrings.Action_Title_Edit,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Edit.png"),
                        CanExecute = false
                    }, new ActionItem {
                        UniqueId = CommonActionKeys.APPROVE,
                        Title = CommonUIStrings.Action_Title_Approve,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Approve.png"),
                        CanExecute = false
                    }, new ActionItem {
                        UniqueId = CommonActionKeys.EXPORT,
                        Title = CommonUIStrings.Action_Title_Export,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Export.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
