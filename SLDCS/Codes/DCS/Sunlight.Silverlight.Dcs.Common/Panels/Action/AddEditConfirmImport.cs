﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class AddEditConfirmImport : DcsActionPanelBase {
        public AddEditConfirmImport() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = CommonUIStrings.ActionPanel_Title_General,
                ActionItems = new[] {
                    new ActionItem {
                        Title = CommonUIStrings.Action_Title_Add,
                        UniqueId = CommonActionKeys.ADD,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Add.png")
                    }, new ActionItem {
                        Title = CommonUIStrings.Action_Title_Edit,
                        UniqueId = CommonActionKeys.EDIT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Edit.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = CommonUIStrings.Action_Title_Confirm,
                        UniqueId = CommonActionKeys.CONFIRM,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Confirm.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = CommonUIStrings.Action_Title_Import,
                        UniqueId = CommonActionKeys.IMPORT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/IMPORT.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
