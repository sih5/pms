﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class AllSparePartReportActionPanel : DcsActionPanelBase {
        public AllSparePartReportActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = CommonUIStrings.Action_Title_Bussiness,
                ActionItems = new[] {
                    new ActionItem {
                        Title = CommonUIStrings.Action_Title_Export,
                        UniqueId = CommonActionKeys.EXPORT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Export.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = "导出配件",
                        UniqueId = CommonActionKeys.MERGEEXPORT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/MergeExport.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
