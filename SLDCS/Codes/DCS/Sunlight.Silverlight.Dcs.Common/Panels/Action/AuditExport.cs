﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class AuditExport : DcsActionPanelBase {
        public AuditExport() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = CommonUIStrings.ActionPanel_Title_General,
                ActionItems = new[]{
                    new ActionItem{
                        Title=CommonUIStrings.Action_Title_Audit,
                        UniqueId=CommonActionKeys.AUDIT,
                       ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Approve.png"),
                       CanExecute=false
                    }, new ActionItem {
                        UniqueId = CommonActionKeys.EXPORT,
                        Title = CommonUIStrings.Action_Title_Export,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Export.png"),
                        CanExecute = false
                    }
                }

            };
        }
    }
}
