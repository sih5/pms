﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class AuditTerminate : DcsActionPanelBase {
        public AuditTerminate() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = CommonUIStrings.ActionPanel_Title_General,
                ActionItems = new[] {
                     new ActionItem {
                        Title = CommonUIStrings.Action_Title_Audit,
                        UniqueId = CommonActionKeys.AUDIT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Approve.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = CommonUIStrings.Action_Title_Terminate,
                        UniqueId = CommonActionKeys.TERMINATE,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Terminate.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
