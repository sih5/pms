﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class BottomStockActionPanel : DcsActionPanelBase {
        public BottomStockActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = CommonUIStrings.Action_Title_Bottom_Edit,
                ActionItems = new[] {
                    new ActionItem {
                        Title =CommonUIStrings.Action_Title_Add,
                        UniqueId = "Add",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Add.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = CommonUIStrings.Action_Title_Edit,
                        UniqueId = "Edit",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Edit.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = CommonUIStrings.Action_Title_Abandon,
                        UniqueId = "Abandon",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Abandon.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title =CommonUIStrings.Action_Title_Import,
                        UniqueId ="Import",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title =CommonUIStrings.Action_Title_Export,
                        UniqueId ="Export",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Export.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title =CommonUIStrings.Action_Title_Import_Abandon,
                        UniqueId ="ImportAbandon",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
