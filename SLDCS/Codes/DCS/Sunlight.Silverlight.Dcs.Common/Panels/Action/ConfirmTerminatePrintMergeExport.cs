﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class ConfirmTerminatePrintMergeExport : DcsActionPanelBase {
        public ConfirmTerminatePrintMergeExport() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = CommonUIStrings.ActionPanel_Title_General,
                ActionItems = new[]{
                    new ActionItem{
                        Title=CommonUIStrings.Action_Title_Replace_Confirm,
                        UniqueId=CommonActionKeys.CONFIRM,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/PartsPurchasing/PendingApproval.png"),
                        CanExecute = false
                    },new ActionItem{
                        Title=CommonUIStrings.Action_Title_Terminate,
                        UniqueId=CommonActionKeys.TERMINATE,
                        ImageUri=Utils.MakeServerUri("Client/Dcs/Images/Operations/Terminate.png"),
                        CanExecute=false
                    },new ActionItem{
                        Title=CommonUIStrings.Action_Title_Print,
                        UniqueId=CommonActionKeys.PRINT,
                        ImageUri=Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute=false
                    },new ActionItem{
                        Title=CommonUIStrings.Action_Title_MergeExport,
                        UniqueId=CommonActionKeys.MERGEEXPORT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/MergeExport.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
