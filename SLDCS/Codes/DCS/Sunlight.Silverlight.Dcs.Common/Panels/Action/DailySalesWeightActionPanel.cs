﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class DailySalesWeightActionPanel : DcsActionPanelBase {
        public DailySalesWeightActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = CommonUIStrings.ActionPanel_Title_General,
                ActionItems = new[] {
                    new ActionItem {
                        Title = CommonUIStrings.Action_Title_Import,
                        UniqueId = CommonActionKeys.IMPORT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = CommonUIStrings.Action_Title_Export,
                        UniqueId = CommonActionKeys.EXPORT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Export.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
