﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class DetailMVSPictureMVSVideoMVSRoute : DcsActionPanelBase {
        public DetailMVSPictureMVSVideoMVSRoute() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = CommonUIStrings.ActionPanel_Title_General,
                ActionItems = new[] {
                    new ActionItem {
                        Title = CommonUIStrings.Action_Title_Detail,
                        UniqueId = CommonActionKeys.DETAIL,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Detail.png"),
                        CanExecute = false
                    },new ActionItem{
                        Title = CommonUIStrings.Action_Title_MVSPicture,
                        UniqueId = CommonActionKeys.MVSPicture,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Menu/Service/MVSPicture.png"),
                        CanExecute = false
                    },new ActionItem{
                        Title = CommonUIStrings.Action_Title_MVSVideo,
                        UniqueId = CommonActionKeys.MVSVideo,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Menu/Service/MVSVideo.png"),
                        CanExecute = false
                    },new ActionItem{
                        Title =CommonUIStrings.Action_Title_MVSRoute,
                        UniqueId = CommonActionKeys.MVSRoute,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Menu/Service/MVSRoute.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
