﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class DetailTerminateMergeExport : DcsActionPanelBase {
        public DetailTerminateMergeExport() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = CommonUIStrings.ActionPanel_Title_General,
                ActionItems = new[] {
                    new ActionItem {
                        Title = CommonUIStrings.Action_Title_Detail,
                        UniqueId = CommonActionKeys.DETAIL,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Detail.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = CommonUIStrings.Action_Title_Terminate,
                        UniqueId = CommonActionKeys.TERMINATE,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Terminate.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = CommonUIStrings.Action_Title_MergeExport,
                        UniqueId = CommonActionKeys.MERGEEXPORT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/MergeExport.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
