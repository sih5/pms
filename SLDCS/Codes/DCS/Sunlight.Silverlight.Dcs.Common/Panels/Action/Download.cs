﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class Download : DcsActionPanelBase {
        public Download() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = CommonUIStrings.ActionPanel_Title_General,
                ActionItems = new[] {
                    new ActionItem {
                        Title = CommonUIStrings.Action_Title_DownLoad,
                        UniqueId = CommonActionKeys.DOWNLOAD,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/DownLoad.png")
                    }
                }
            };
        }
    }
}
