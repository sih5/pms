﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class ExportAll : DcsActionPanelBase {
        public ExportAll() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = CommonUIStrings.ActionPanel_Title_General,
                ActionItems = new[] {
                    new ActionItem {
                        Title = CommonUIStrings.Action_Title_ExportAll,
                        UniqueId = CommonActionKeys.EXPORT_ALL,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/export_all.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
