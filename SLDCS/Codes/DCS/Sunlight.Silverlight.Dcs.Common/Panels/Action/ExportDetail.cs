﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class ExportDetail : DcsActionPanelBase {

        // 初始化函数，为 ActionItemGroup 属性赋值并引发界面的生成
        public ExportDetail() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = CommonUIStrings.ActionPanel_Title_General,
                ActionItems = new[] {
                    new ActionItem {
                       UniqueId = CommonActionKeys.EXPORTDETAIL,
                        Title = CommonUIStrings.Action_Title_ExportDetail,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/ExportDetail.png"),
                        CanExecute = false
                    }
                }
            };
        }

    }
}
