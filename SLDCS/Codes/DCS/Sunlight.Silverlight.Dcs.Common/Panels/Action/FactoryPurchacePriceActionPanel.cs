﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class FactoryPurchacePriceActionPanel : DcsActionPanelBase {
        public FactoryPurchacePriceActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "FactoryPurchacePrice",
                Title = CommonUIStrings.Action_Title_Bussiness,
                ActionItems = new[] {
                    new ActionItem {
                        Title = CommonUIStrings.Action_Title_Add_Application,
                        UniqueId = "Create",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Add.png"),
                        CanExecute = false
                    },new ActionItem {  
                        UniqueId = "ExactExport",  
                        Title = CommonUIStrings.Action_Title_Export_Accurate,  
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/MergeExport.png"),  
                        CanExecute = false  
                    } 
                }
            };
        }
    }
}
