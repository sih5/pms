﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class FactoryPurchacePriceForHandActionPanel : DcsActionPanelBase {
        public FactoryPurchacePriceForHandActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "FactoryPurchacePrice",
                Title = CommonUIStrings.Action_Title_Bussiness,
                ActionItems = new[] {
                    new ActionItem {
                        Title = CommonUIStrings.Action_Title_Add_Application,
                        UniqueId = "Create",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Add.png"),
                        CanExecute = false
                    },new ActionItem {  
                        UniqueId = "ExactExport",  
                        Title = CommonUIStrings.Action_Title_Export,  
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Export.png"),  
                        CanExecute = false  
                    } 
                }
            };
        }
    }
}
