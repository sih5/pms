﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class ForceReserveBillDealer : DcsActionPanelBase {
        public ForceReserveBillDealer() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "ForceReserveBillCenter",
                Title = CommonUIStrings.ActionPanel_Title_General,
                ActionItems = new[]{                 
                    new ActionItem {
                        Title = CommonUIStrings.Action_Title_Add,
                        UniqueId = CommonActionKeys.ADD,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Add.png")
                    }, new ActionItem {
                        Title = CommonUIStrings.Action_Title_Edit,
                        UniqueId = CommonActionKeys.EDIT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Edit.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = CommonUIStrings.Action_Title_Abandon,
                        UniqueId = CommonActionKeys.ABANDON,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Abandon.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = CommonUIStrings.Action_Title_Submit,
                        UniqueId = CommonActionKeys.SUBMIT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Submit.png"),
                        CanExecute = false
                    },new ActionItem { 
                        Title =  CommonUIStrings.Action_Title_Audit,
                        UniqueId = CommonActionKeys.AUDIT, 
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Service/InitialApprove.png"), 
                        CanExecute = false 
                    }, new ActionItem {
                        Title = CommonUIStrings.Action_Title_Confirm,
                        UniqueId = CommonActionKeys.CONFIRM,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Confirm.png"),
                        CanExecute = false
                    },new ActionItem { 
                        Title = CommonUIStrings.Action_Title_Approve,
                        UniqueId = CommonActionKeys.APPROVE, 
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Service/FinalApprove.png"), 
                        CanExecute = false 
                    },new ActionItem{
                        Title =CommonUIStrings.Action_Title_MergeExport,
                        UniqueId = CommonActionKeys.MERGEEXPORT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/MergeExport.png"),
                        CanExecute = false
                    }
                    //按钮图片
                    ,new ActionItem{
                        Title =CommonUIStrings.Action_Title_Export,
                        UniqueId = CommonActionKeys.EXPORT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Export.png"),
                        CanExecute = false
                    }
                    
                }
            };
        }
    }
}