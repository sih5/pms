﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class InitialApprove : DcsActionPanelBase {
        public InitialApprove() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Workflow",
                Title = CommonUIStrings.ActionPanel_Title_Workflow,
                ActionItems = new[] {
                    new ActionItem {
                        Title = CommonUIStrings.Action_Title_InitialApprove,
                        UniqueId = WorkflowActionKey.INITIALAPPROVE,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/InitialApprove.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
