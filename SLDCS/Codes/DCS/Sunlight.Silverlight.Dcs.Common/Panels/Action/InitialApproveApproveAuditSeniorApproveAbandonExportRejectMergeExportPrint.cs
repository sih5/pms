﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action
{
    public class InitialApproveApproveAuditSeniorApproveAbandonExportRejectMergeExportPrint: DcsActionPanelBase {
        public InitialApproveApproveAuditSeniorApproveAbandonExportRejectMergeExportPrint()
        {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = CommonUIStrings.ActionPanel_Title_General,
                ActionItems = new[] {
                     new ActionItem { 
                        Title = CommonUIStrings.Action_Title_InitialApprove, 
                        UniqueId = CommonActionKeys.INITIALAPPROVE, 
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Service/InitialApprove.png"), 
                        CanExecute = false 
                    }, new ActionItem { 
                        Title =  CommonUIStrings.Action_Title_Audit, 
                        UniqueId = CommonActionKeys.AUDIT, 
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Approve.png"),
                        CanExecute = false 
                    },
                    new ActionItem {
                        Title = CommonUIStrings.Action_Title_Approve,
                        UniqueId = CommonActionKeys.APPROVE,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Approve.png"),
                        CanExecute = false
                    }
                    , new ActionItem {
                        Title = CommonUIStrings.Action_Title_Abandon,
                        UniqueId = CommonActionKeys.ABANDON,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Abandon.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = CommonUIStrings.Action_Title_Export,
                        UniqueId = CommonActionKeys.EXPORT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Export.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = CommonUIStrings.Action_Title_Reject,
                        UniqueId = CommonActionKeys.REJECT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Service/Reject.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = CommonUIStrings.Action_Title_MergeExport,
                        UniqueId = CommonActionKeys.MERGEEXPORT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/MergeExport.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = CommonUIStrings.Action_Title_Print,
                        UniqueId = CommonActionKeys.PRINT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}