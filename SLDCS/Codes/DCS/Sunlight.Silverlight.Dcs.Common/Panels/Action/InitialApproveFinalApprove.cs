﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class InitialApproveFinalApprove : DcsActionPanelBase {
        public InitialApproveFinalApprove() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Workflow",
                Title = CommonUIStrings.ActionPanel_Title_Workflow,
                ActionItems = new[] {
                    new ActionItem {
                        Title = CommonUIStrings.Action_Title_Audit,
                        UniqueId = WorkflowActionKey.INITIALAPPROVE,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/InitialApprove.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = CommonUIStrings.Action_Title_Approve,
                        UniqueId = WorkflowActionKey.FINALAPPROVE,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/FinalApprove.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
