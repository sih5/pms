﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class MVSVideo : DcsActionPanelBase {
        public MVSVideo() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = CommonUIStrings.ActionPanel_Title_General,
                ActionItems = new[] {
                    new ActionItem {
                        Title = CommonUIStrings.Action_Title_MVSVideo,
                        UniqueId = CommonActionKeys.MVSVideo,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Menu/Service/MVSVideo.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
