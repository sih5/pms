﻿
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class Notification :DcsActionPanelBase {
        public Notification() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "NotificationTop",
                Title = CommonUIStrings.Action_Title_Top,//CommonUIStrings.ActionPanel_Title_ServiceProductLine,
                ActionItems = new[] {
                    new ActionItem {
                        Title = CommonUIStrings.Action_Title_Setup,
                        UniqueId = "Top",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Setup.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
