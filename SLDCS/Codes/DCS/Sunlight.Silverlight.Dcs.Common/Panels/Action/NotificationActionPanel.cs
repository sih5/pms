﻿
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class NotificationActionPanel : DcsActionPanelBase {
        public NotificationActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = CommonUIStrings.ActionPanel_Title_General,
                ActionItems = new[] {
                    new ActionItem {
                        Title =  CommonUIStrings.Action_Title_Top,
                        UniqueId = "Top",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Setup.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = CommonUIStrings.Action_Title_Reply,
                        UniqueId = CommonActionKeys.REPLY,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Reply.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = CommonUIStrings.Action_Title_Upload,
                        UniqueId = CommonActionKeys.UPLOAD,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Upload.png"),
                        CanExecute = false
                    }

                }
            };
        }
    }
}
