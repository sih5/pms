﻿
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class NotificationQueryActionPanel : DcsActionPanelBase {
        public NotificationQueryActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = CommonUIStrings.ActionPanel_Title_General,
                ActionItems = new[] {
                    new ActionItem {
                        Title = CommonUIStrings.Action_Title_Detail,
                        UniqueId = CommonActionKeys.DETAIL,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Detail.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = CommonUIStrings.Action_Title_Reply,
                        UniqueId = CommonActionKeys.REPLY,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Reply.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = CommonUIStrings.Action_Title_Upload,
                        UniqueId = CommonActionKeys.UPLOAD,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Upload.png"),
                        CanExecute = false
                    }

                }
            };
        }
    }
}