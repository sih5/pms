﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class PartsBranchActionPanel : DcsActionPanelBase {
        public PartsBranchActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "PartsBranch",
                Title = CommonUIStrings.Action_Title_PartsBranch,
                ActionItems = new[] {
                    new ActionItem {
                        Title = CommonUIStrings.Action_Title_Purchasing,
                        UniqueId = "Purchasing",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Purchasing.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = CommonUIStrings.Action_Title_Serviceing,
                        UniqueId = "Serviceing",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Marketing.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = CommonUIStrings.Action_Title_Marketing,
                        UniqueId = "Marketing",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Marketing.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = CommonUIStrings.Action_Title_Import,
                        UniqueId = CommonActionKeys.IMPORT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    },new ActionItem {
                         Title =CommonUIStrings.Action_Title_Import_Edit,
                        UniqueId = "ImportEdit",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    },new ActionItem {
                         Title =CommonUIStrings.Action_Title_Batch_Replace_Purchase,
                        UniqueId = "ReplaceIsOrderable",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    },new ActionItem {
                         Title =CommonUIStrings.Action_Title_Batch_Replace_Sale,
                        UniqueId = "ReplaceIsSalableAndDirectSupply",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    },new ActionItem {
                         Title =CommonUIStrings.Action_Title_Batch_Replace_Price,
                        UniqueId = "ReplaceIncreaseRateGroup",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    },new ActionItem {
                         Title =CommonUIStrings.Action_Title_Batch_Replace_Guarantee,
                        UniqueId = "ReplacePartsWarrantyCategory",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    },new ActionItem {
                         Title =CommonUIStrings.Action_Title_Batch_Replace_Old,
                        UniqueId = "ReplacePartsReturnPolicy",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    },new ActionItem {
                         Title =CommonUIStrings.Action_Title_Batch_Replace_High,
                        UniqueId = "ReplaceLossTypeAndStockLimit",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    },new ActionItem {
                         Title =CommonUIStrings.Action_Title_Batch_Replace_Remark,
                        UniqueId = "ReplaceRemark",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    },new ActionItem {
                         Title =CommonUIStrings.Action_Title_Batch_Stop,
                        UniqueId = "PauseEdit",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    },new ActionItem {
                         Title =CommonUIStrings.Action_Title_Batch_Recovery,
                        UniqueId = "ResumeEdit",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title =CommonUIStrings.Action_Title_Batch_Abandon,
                        UniqueId = "AbandonEdit",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title =CommonUIStrings.Action_Title_Batch_Replace_Approve,
                        UniqueId = "ReplaceApproveLimit",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title =CommonUIStrings.Action_Title_Batch_Import_Sparet,
                        UniqueId = "ReplaceRepairClassify",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    }
                    ,new ActionItem {
                        Title =CommonUIStrings.Action_Title_Batch_Import_SparetABC,
                        UniqueId = "ReplacePartABC",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    }
                    ,new ActionItem {
                        Title =CommonUIStrings.Action_Title_Batch_Import_StockMinimum,
                        UniqueId = "ReplaceStockMinimum",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
