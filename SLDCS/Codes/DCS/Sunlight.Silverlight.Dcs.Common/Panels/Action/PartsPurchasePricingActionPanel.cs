﻿
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class PartsPurchasePricingActionPanel : DcsActionPanelBase {
        public PartsPurchasePricingActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = CommonUIStrings.ActionPanel_Title_General,
                ActionItems = new[] {
                    new ActionItem {  
                        UniqueId = "Export",  
                        Title = CommonUIStrings.Action_Title_Export,  
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Export.png"),  
                        CanExecute = false  
                    },
                    new ActionItem {  
                        UniqueId = "ExactExport",  
                        Title = CommonUIStrings.Action_Title_Export_Accurate,  
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/MergeExport.png"),  
                        CanExecute = false  
                    }
                }
            };
        }
    }
}
