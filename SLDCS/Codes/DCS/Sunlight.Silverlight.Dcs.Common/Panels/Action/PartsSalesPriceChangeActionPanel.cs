﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class PartsSalesPriceChangeActionPanel : DcsActionPanelBase {
        public PartsSalesPriceChangeActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                Title = CommonUIStrings.ActionPanel_Title_Workflow,
                UniqueId = "Workflow",
                ActionItems = new[] {
                     new ActionItem {
                        Title = CommonUIStrings.Action_Title_InitialApprove,
                        UniqueId = "FirstApprove",
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/InitialApprove.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = CommonUIStrings.Action_Title_Audit,
                        UniqueId = "InitialApprove",
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/InitialApprove.png"),
                        CanExecute = false
                    }, new ActionItem {
                       Title = CommonUIStrings.Action_Title_Approve,
                        UniqueId = "FinalApprove",
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/FinalApprove.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
