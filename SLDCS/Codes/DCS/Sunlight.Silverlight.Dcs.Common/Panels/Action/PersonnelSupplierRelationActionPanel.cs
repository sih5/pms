﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class PersonnelSupplierRelationActionPanel : DcsActionPanelBase {
        public PersonnelSupplierRelationActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                Title = CommonUIStrings.ActionPanel_Title_General,
                UniqueId = "PersonnelSupplierRelation",
                ActionItems = new[] {
                     new ActionItem {
                        Title = CommonUIStrings.Action_Title_Add,
                        UniqueId = "Add",
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/Add.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title =CommonUIStrings.Action_Title_Edit,
                        UniqueId = "Edit",
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/Edit.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = CommonUIStrings.Action_Title_Abandon,
                        UniqueId = "Abandon",
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/Abandon.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = CommonUIStrings.Action_Title_Import,
                        UniqueId = "Import",
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = CommonUIStrings.Action_Title_MergeExport,
                        UniqueId = "MergeExport",
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/Export.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
