﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class ResponsibleUnitActionPanel : DcsActionPanelBase {
        public ResponsibleUnitActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "ResponsibleUnit",
                Title = CommonUIStrings.Action_Title_ResponsibleUnit,
                ActionItems = new[] {
                    new ActionItem {
                        Title = CommonUIStrings.Action_Title_Setup,
                        UniqueId = "Setup",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Setup.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
