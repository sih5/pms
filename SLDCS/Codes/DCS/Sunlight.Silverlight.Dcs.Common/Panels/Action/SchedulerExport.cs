﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class SchedulerExport : DcsActionPanelBase {
        // 初始化函数，为 ActionItemGroup 属性赋值并引发界面的生成
        public SchedulerExport() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = CommonUIStrings.ActionPanel_Title_General,
                ActionItems = new[] {
                    new ActionItem {
                        Title = CommonUIStrings.Action_Title_DownLoad,
                        UniqueId = CommonActionKeys.SCHEDULEREEXPORT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Common/SchedulerDownLoad.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
