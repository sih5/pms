﻿
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class SparePartActionPanel : DcsActionPanelBase {
        public SparePartActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "SparePart",
                Title = CommonUIStrings.ActionPanel_Title_General,
                ActionItems = new[] {
                    new ActionItem {  
                        UniqueId = "ExactExport",  
                        Title = CommonUIStrings.Action_Title_Export_Accurate,  
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/MergeExport.png"),  
                        CanExecute = false  
                    },new ActionItem {
                        Title = CommonUIStrings.Action_Title_Batch_Edit_Name,
                        UniqueId = "ReplaceName",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    },                   
                    new ActionItem {
                        Title =CommonUIStrings.Action_Title_Batch_Edit_Name_En,
                        UniqueId = "ReplaceEnglishName",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = CommonUIStrings.Action_Title_Batch_Replace_Sparepart_Type,
                        UniqueId = "ReplacePartType",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = CommonUIStrings.Action_Title_Batch_Replace_Features,
                        UniqueId = "ReplaceFeature",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = CommonUIStrings.Action_Title_Batch_Replace_ReferenceCode,
                        UniqueId = "ReplaceReferenceCode",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = CommonUIStrings.Action_Title_Batch_Replace_MInPackingAmount,
                        UniqueId = "ReplaceMInPackingAmount",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    },
                   
                    new ActionItem {
                        Title = CommonUIStrings.Action_Title_Batch_Replace_IMS,
                        UniqueId = "ReplaceIMSManufacturerNumber",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    },
                   
                    new ActionItem{
                      Title = CommonUIStrings.Action_Title_Batch_Replace_Declare,
                      UniqueId = "ReplaceStandardName",
                      ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                      CanExecute = false
                    },new ActionItem{
                      Title = CommonUIStrings.Action_Title_Batch_Import_Edit,
                      UniqueId = "ReplaceImportEdit",
                      ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                      CanExecute = false
                    },
                    new ActionItem{
                      Title = CommonUIStrings.Action_Title_Batch_Stop,
                      UniqueId = "ImportEdit",
                      ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                      CanExecute = false
                    },
                    new ActionItem{
                      Title = CommonUIStrings.Action_Title_Batch_Recovery,
                      UniqueId = "ResumeEdit",
                      ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                      CanExecute = false
                    },new ActionItem{
                      Title=CommonUIStrings.Action_Title_Batch_Replace_Overseas,
                      UniqueId="ReplaceOverseasPartsFigure",
                      ImageUri=Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                      CanExecute=false
                    },new ActionItem{
                      Title=CommonUIStrings.Action_Title_Batch_Replace_Classification,
                      UniqueId="ImportGoldenTaxClassify",
                      ImageUri=Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                      CanExecute=false
                    },new ActionItem{
                      Title=CommonUIStrings.Action_Title_Batch_Export_Classification,
                      UniqueId="ExportNoGoldenTaxClassify",
                      ImageUri=Utils.MakeServerUri("Client/Dcs/Images/Operations/Export.png"),
                      CanExecute=false
                    },
                  
                    new ActionItem{
                      Title=CommonUIStrings.Action_Title_Batch_Replace_Identification,
                      UniqueId="ImportExchangeIdentification",
                      ImageUri=Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                      CanExecute=false
                    },
                 
                    new ActionItem{
                      Title=CommonUIStrings.Action_Title_Batch_Replace_Code,
                      UniqueId="ImportImportCategoryCode",
                      ImageUri=Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                      CanExecute=false 
                    },new ActionItem {
                        Title = CommonUIStrings.Action_Title_Upload_Attachments,
                        UniqueId = CommonActionKeys.UPLOAD,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Upload.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = CommonUIStrings.Action_Title_Upload_IsSupplier,
                        UniqueId = "ImportIsSupplier",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = "批量修改追溯属性",
                        UniqueId = "ReplaceTraceProperty",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = "批量修改安全天数最大值",
                        UniqueId = "ReplaceSafeDays",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = "批量修改库房天数",
                        UniqueId = "ReplaceWarehousDays",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = "批量修改临时天数",
                        UniqueId = "ReplaceTemDays",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    }

                }
            };
        }
    }
}
