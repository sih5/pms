﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class TraceWarehouseAgeActionPanel  : DcsActionPanelBase {
        public TraceWarehouseAgeActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = "操作",
                ActionItems = new[] {
                     new ActionItem {
                        Title ="修改",
                        UniqueId = CommonActionKeys.EDIT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Edit.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title ="导出",
                        UniqueId = CommonActionKeys.EXPORT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Export.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title ="大标签打印",
                        UniqueId = "ChPrint",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title ="小标签打印",
                        UniqueId = "PcPrint",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
