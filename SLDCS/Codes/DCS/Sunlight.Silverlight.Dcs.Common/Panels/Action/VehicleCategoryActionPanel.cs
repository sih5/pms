﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class VehicleCategoryActionPanel : DcsActionPanelBase {
        public VehicleCategoryActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "VehicleCategory",
                Title = CommonUIStrings.ActionPanel_Title_VehicleCategory,
                ActionItems = new[] {
                    new ActionItem {
                        Title = CommonUIStrings.Action_Title_Setup,
                        UniqueId = "Setup",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Setup.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
