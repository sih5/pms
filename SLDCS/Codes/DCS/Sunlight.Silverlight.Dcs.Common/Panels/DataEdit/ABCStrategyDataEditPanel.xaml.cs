﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.DataEdit {
    public partial class ABCStrategyDataEditPanel {
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = {
            "ABCStrategy_ReferenceBaseType", "ABCStrategy_Category"
        };

        public ABCStrategyDataEditPanel() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var branch in loadOp.Entities)
                    this.kvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name,
                    });
            }, null);
        }

        public ObservableCollection<KeyValuePair> KvBranches {
            get {
                return this.kvBranches;
            }
        }

        public object StrategyCategories {
            get {
                return this.KeyValueManager[this.kvNames[1]];
            }
        }

        public object ReferenceBaseTypes {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
    }
}


