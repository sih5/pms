﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using System.Windows;
﻿using System;
﻿using System.Linq;
using Telerik.Windows.Controls;
using System.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Panels.DataEdit {
    public partial class AgencyDataEditPanel {
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();

        private string code;
        private readonly DcsDomainContext domainContext = new DcsDomainContext();
        public AgencyDataEditPanel() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += this.DataEditPanel_DataContextChanged;
        }
        public ObservableCollection<KeyValuePair> KvWarehouses {
            get {
                return this.kvWarehouses;
            }
        }
        private void DataEditPanel_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var agency = this.DataContext as  Agency;
            if (agency == null)
                return;
            if (agency.Code != null) {
                this.code = agency.Code;
            } else {
                this.code = string.Empty;
            }
        }

        private void CreateUI() {
            this.radButtonSearch.Click += this.RadButtonSearch_Click;

            var customerInfo = DI.GetQueryWindow("CustomerInfo");
            this.popupTextBoxCode.PopupContent = customerInfo;
            customerInfo.SelectionDecided += popupTextBoxCode_SelectionDecided;
            domainContext.Load(domainContext.GetWarehousesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效 && e.Type == (int)DcsWarehouseType.总库 && e.StorageCompanyType == (int)DcsCompanyType.分公司), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehouse in loadOp.Entities)
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
            }, null);
        }
        private void popupTextBoxCode_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if (queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var virtualIdNameForTmp = queryWindow.SelectedEntities.Cast<VirtualIdNameForTmp>().FirstOrDefault();
            if (virtualIdNameForTmp == null)
                return;
            var agency = this.DataContext as Agency;
            if (agency == null)
                return;
            agency.Code = virtualIdNameForTmp.Code;
            agency.Name = virtualIdNameForTmp.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if (parent != null)
                parent.Close();
        }

         private void RadButtonSearch_Click(object sender, RoutedEventArgs e) {
             domainContext.Load(domainContext.getCustomerNameByCodeForTmpQuery(code),LoadBehavior.RefreshCurrent, loadOp => {
                 if (loadOp.HasError)
                     return;
                 var agency = this.DataContext as  Agency;
                 if (agency != null && agency.Id != 0) { 
                     foreach (var item in loadOp.Entities){
                         agency.Name = item.Name;
                     }
                 }
             }, null);
        }
    }
}
