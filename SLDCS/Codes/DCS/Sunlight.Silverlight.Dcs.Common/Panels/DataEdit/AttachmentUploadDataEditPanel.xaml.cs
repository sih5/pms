﻿
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Panels.DataEdit {
    public partial class AttachmentUploadDataEditPanel : INotifyPropertyChanged {
        public ObservableCollection<FileEntityes> pathList = new ObservableCollection<FileEntityes>();
        public event PropertyChangedEventHandler PropertyChanged;
        private string attachmentPath;
        private string strFileName;
        private string attachmentName;

      
        public AttachmentUploadDataEditPanel() {
            this.InitializeComponent();
            this.dcsbutton.Click += dcsbutton_Click;
            this.RemoveItems.Click += deleteButton_Click;
            this.RadUpload.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
            this.RadUpload.FileUploadStarting += RadUpload_FileUploadStarting;
            this.RadUpload.UploadFinished += this.RadUpload_UploadFinished;
            this.RadUpload.FilesSelected += this.RadUpload_FilesSelected;
            this.RadUpload.FileUploaded += (sender, e) => this.pathList.Add(new FileEntityes(this.attachmentName, e.HandlerData.CustomData["Path"].ToString()));
            this.RadUpload.CancelUpload();
        }
        public ObservableCollection<FileEntityes> PathList {
            get {
                return pathList;
            }
            set {
                this.pathList = value;
                this.OnPropertyChanged("PathList");
            }
        }
        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
        /// <summary>
        /// 为true  隐藏 删除/上传按钮
        /// </summary>
        public bool isVisibility {
            set {
                if(value) {
                    this.dcsbutton.Visibility = Visibility.Collapsed;
                    this.RemoveItems.Visibility = Visibility.Collapsed;
                }
            }
        }

        public string AttachmentPath {
            get {
                attachmentPath = "";
                foreach(var pList in pathList) {
                    attachmentPath += pList.FileName + ":" + pList.FilePath + "?";
                }
                //去掉最后一个 ？
                return attachmentPath == "" ? attachmentPath : attachmentPath.Substring(0, attachmentPath.Length - 1);
            }
            set {
                if(string.IsNullOrEmpty(value))
                    return;
                var listPath = value.Split('?');
                PathList.Clear();
                foreach(var item in listPath) {
                    PathList.Add(new FileEntityes(item.Substring(0, item.IndexOf(':')), item.Substring(item.IndexOf(':') + 1, item.Length - item.IndexOf(':') - 1)));

                }
            }
        }

        void dcsbutton_Click(object sender, RoutedEventArgs e) {
            this.RadUpload.ShowFileDialog();
        }

        void deleteButton_Click(object sender, RoutedEventArgs e) {
            var fileEntityes = this.ContextMenu.SelectedItem as FileEntityes;
            if(fileEntityes == null)
                return;
            pathList.Remove(fileEntityes);
        }

        private void RadUpload_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload != null && upload.CurrentSession != null && upload.CurrentSession.CurrentFile != null) {
                upload.CancelUpload();
            }

            UIHelper.ShowNotification(CommonUIStrings.Action_Title_Upload_Success);
        }
        private void RadUpload_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any())
                try {
                    attachmentName = e.SelectedFiles[0].Name;
                    this.strFileName = string.Format("{0}_{1}{2}", Path.GetFileNameWithoutExtension(e.SelectedFiles[0].Name), Guid.NewGuid(), Path.GetExtension(e.SelectedFiles[0].Name));

                } catch(Exception ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                    e.Handled = true;
                }
        }
        private void RadUpload_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = this.strFileName;
            UIHelper.ShowNotification(CommonUIStrings.Action_Title_Uploading);//图片正在上传
        }
        private DateTime lastTime = DateTime.Now;
        private void Button_Click_1(object sender, RoutedEventArgs e) {
            //找到图片
            var button = sender as Button;
            if(button == null)
                return;
            var imagePath = button.Tag;
            if(imagePath == null)
                return;
            this.ContextMenu.SelectedItem = PathList.SingleOrDefault(ex => ex.FilePath == (string)imagePath);
            //if((DateTime.Now - lastTime).TotalMilliseconds < 300) {
                var sFileUrl = DcsUtils.GetDownloadFileUrl((string)imagePath).ToString();//调这个方法下载。固定路径
                HtmlPage.Window.Navigate(new Uri(sFileUrl), "_blank");
            //}
            lastTime = DateTime.Now;
        }
    }
}

