﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.ComponentModel;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.View.Custom;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.DataEdit {
    public partial class CheckBoxDataEditPanel : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        private void MainPage_Loaded(object sender, RoutedEventArgs e) {
            List<CompanyType> companyType = new List<CompanyType>();
            companyType.Add(new CompanyType {
                Type = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_BranchName,
                TypeValue=1
            });
            companyType.Add(new CompanyType {
                Type = CommonUIStrings.DataEditView_Notification_Type_Company,
                TypeValue = 2
            });
            companyType.Add(new CompanyType {
                Type = CommonUIStrings.DataEditView_Notification_Type_Center,
                TypeValue = 3
            });
            companyType.Add(new CompanyType {
                Type = CommonUIStrings.DataEditView_Notification_Type_LogisticCompany,
                TypeValue = 4
            });
            companyType.Add(new CompanyType {
                Type = CommonUIStrings.DataEditView_Notification_Type_ResponsibleUnit,
                TypeValue = 5
            });
            companyType.Add(new CompanyType {
                Type = CommonUIStrings.DataEditView_Notification_Type_PartsSupplier,
                TypeValue = 6
            });
            companyType.Add(new CompanyType {
                Type = CommonUIStrings.DataEditView_Notification_Type_Dealer,
                TypeValue = 7
            });
            //companyType.Add(new CompanyType {
            //    Type = "集团企业",
            //    TypeValue = 8
            //});
            Total.ItemsSource = companyType;
        }

        public CheckBoxDataEditPanel() {
            this.InitializeComponent();
            Loaded += MainPage_Loaded;
        }

    }
}
