﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Panels.DataEdit {
    public partial class CompanyAddressDataEditPanel : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;
        private string provinceName, cityName, countyName;

        private readonly string[] kvNames = new[] {
            "Address_usage"
        };

        public CompanyAddressDataEditPanel() {
            this.InitializeComponent();
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += this.DealerAddressDataEditPanel_DataContextChanged;
        }

        private void DealerAddressDataEditPanel_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var companyAddress = this.DataContext as CompanyAddress;
            if(companyAddress == null)
                return;
            if(companyAddress.TiledRegion != null) {
                this.ProvinceName = companyAddress.TiledRegion.ProvinceName;
                this.CityName = companyAddress.TiledRegion.CityName;
                this.CountyName = companyAddress.TiledRegion.CountyName;
            }else {
                this.ProvinceName = string.Empty;
                this.CityName = string.Empty;
                this.CountyName = string.Empty;
            }
        }

        private void CreateUI() {
            var queryWindowTiledRegion = DI.GetQueryWindow("TiledRegion");
            queryWindowTiledRegion.SelectionDecided += this.QueryWindowTiledRegion_SelectionDecided;
            this.popupTextBoxTiledRegion.PopupContent = queryWindowTiledRegion;
        }

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void QueryWindowTiledRegion_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var tiledRegion = queryWindow.SelectedEntities.Cast<TiledRegion>().FirstOrDefault();
            if(tiledRegion == null)
                return;
            var companyAddress = this.DataContext as CompanyAddress;
            if(companyAddress == null)
                return;
            companyAddress.RegionId = tiledRegion.Id;
            this.ProvinceName = tiledRegion.ProvinceName;
            this.CityName = tiledRegion.CityName;
            this.CountyName = tiledRegion.CountyName;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        public string ProvinceName {
            get {
                return this.provinceName;
            }
            set {
                this.provinceName = value;
                this.OnPropertyChanged("ProvinceName");
            }
        }

        public string CityName {
            get {
                return this.cityName;
            }
            set {
                this.cityName = value;
                this.OnPropertyChanged("CityName");
            }
        }

        public string CountyName {
            get {
                return this.countyName;
            }
            set {
                this.countyName = value;
                this.OnPropertyChanged("CountyName");
            }
        }

        public object Usages {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public string DealerName {
            get {
                return BaseApp.Current.CurrentUserData.EnterpriseName;
            }
        }

        public string DealerCode {
            get {
                return BaseApp.Current.CurrentUserData.EnterpriseCode;
            }
        }
    }
}
