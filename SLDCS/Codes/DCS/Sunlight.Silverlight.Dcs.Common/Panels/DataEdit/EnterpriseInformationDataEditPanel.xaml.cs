﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Text.RegularExpressions;
using System.Windows.Input;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Panels.DataEdit {
    public partial class EnterpriseInformationDataEditPanel {
        private readonly string[] kvNames = {
            "Customer_IdDocumentType", "Corporate_Nature","Company_CityLevel"
        };
        private ObservableCollection<KeyValuePair> kvProvinceNames;
        private ObservableCollection<KeyValuePair> kvCityNames;
        private ObservableCollection<KeyValuePair> kvCountyNames;
        private List<TiledRegion> TiledRegions = new List<TiledRegion>();

        public ObservableCollection<KeyValuePair> KvProvinceNames {
            get {
                return this.kvProvinceNames ?? (this.kvProvinceNames = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvCityNames {
            get {
                return this.kvCityNames ?? (this.kvCityNames = new ObservableCollection<KeyValuePair>());
            }
        }
        public ObservableCollection<KeyValuePair> KvCountyNames {
            get {
                return this.kvCountyNames ?? (this.kvCountyNames = new ObservableCollection<KeyValuePair>());
            }
        }

        public EnterpriseInformationDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetTiledRegionsQuery().Where(ex => ex.CountyName != null).OrderBy(ex => ex.ProvinceName), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var repairObject in loadOp.Entities)
                    this.TiledRegions.Add(new TiledRegion {
                        Id = repairObject.Id,
                        ProvinceName = repairObject.ProvinceName,
                        CityName = repairObject.CityName,
                        CountyName = repairObject.CountyName,
                    });

                foreach(var item in TiledRegions) {
                    var values = KvProvinceNames.Select(e => e.Value);
                    if(values.Contains(item.ProvinceName))
                        continue;
                    KvProvinceNames.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.ProvinceName
                    });
                }
            }, null);
        }

        private void CreateUI() {
            this.provinceNameRadCombox.SelectionChanged += provinceNameRadCombox_SelectionChanged;
            this.cityNameRadCombox.SelectionChanged += cityNameRadCombox_SelectionChanged;
        }

        void provinceNameRadCombox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            this.KvCityNames.Clear();
            foreach(var item in TiledRegions.Where(ex => ex.ProvinceName == provinceNameRadCombox.Text).OrderBy(ex => ex.CityName)) {
                var values = KvCityNames.Select(ex => ex.Value);
                if(values.Contains(item.CityName))
                    continue;
                KvCityNames.Add(new KeyValuePair {
                    Key = item.Id,
                    Value = item.CityName
                });
            }
        }

        void cityNameRadCombox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            this.KvCountyNames.Clear();
            foreach(var item in TiledRegions.Where(ex => ex.CityName == cityNameRadCombox.Text).OrderBy(ex => ex.CountyName)) {
                var values = KvCountyNames.Select(ex => ex.Value);
                if(values.Contains(item.CountyName))
                    continue;
                KvCountyNames.Add(new KeyValuePair {
                    Key = item.Id,
                    Value = item.CountyName
                });
            }
        }

        public object IdDocumentTypes {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public object CorporateNatures {
            get {
                return this.KeyValueManager[this.kvNames[1]];
            }
        }

        public object KvCityLevel {
            get {
                return this.KeyValueManager[this.kvNames[2]];
            }
        }

        private void UIElement_OnKeyDown(object sender, KeyEventArgs e) {
            var rg = new Regex("^[\u4e00-\u9fa5\b]$"); //\b是退格键     
            if(!rg.IsMatch(e.Key.ToString())) {
                e.Handled = true;

            }
        }
      
    }
}
