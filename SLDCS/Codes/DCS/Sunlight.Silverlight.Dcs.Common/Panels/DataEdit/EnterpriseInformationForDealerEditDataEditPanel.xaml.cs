﻿
using System;
using System.ComponentModel;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Panels.DataEdit {
    public partial class EnterpriseInformationForDealerEditDataEditPanel {
        public string UserName {
            get;
            set;
        }
        public IHubProxy HubProxy {
            get;
            set;
        }
        public HubConnection Connection {
            get;
            set;
        }
        private  void CloseQrCode() {
            ShellViewModel.Current.IsBusy = true;
            this.radBarcodeQr.Width = 30;
            this.radBarcodeQr.Height = 30;
            ShellViewModel.Current.IsBusy = false;
        }
        private readonly string[] kvNames = {
            "Customer_IdDocumentType", "Corporate_Nature","Company_CityLevel"
        };
        public EnterpriseInformationForDealerEditDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            radBarcodeQr.Width = 30;
            radBarcodeQr.Height = 30;
        }

        private void radBarcodeQr_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e) {
            if(Convert.ToInt32(radBarcodeQr.Height) == 30) {
                ShellViewModel.Current.IsBusy = true;
                ShellViewModel.Current.IsBusy = false;
                this.radBarcodeQr.Text = @"http://developer.baidu.com/map/jsdemo.htm#i8_1";
                radBarcodeQr.Width = 200;
                radBarcodeQr.Height = 200;
            } else {
                this.CloseQrCode();
            }

        }
    }
}
