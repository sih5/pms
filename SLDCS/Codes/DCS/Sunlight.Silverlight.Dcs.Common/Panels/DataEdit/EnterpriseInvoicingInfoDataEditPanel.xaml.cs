﻿
namespace Sunlight.Silverlight.Dcs.Common.Panels.DataEdit {
    public partial class EnterpriseInvoicingInfoDataEditPanel {
        private readonly string[] kvNames = {
            "PartsSupplier_TaxpayerKind","Invoice_Type"
        };
        public EnterpriseInvoicingInfoDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData();
        }
        public object TaxpayerQualifications {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public object KvInvoiceType {
            get {
                return this.KeyValueManager[this.kvNames[1]];
            }
        }
    }
}
