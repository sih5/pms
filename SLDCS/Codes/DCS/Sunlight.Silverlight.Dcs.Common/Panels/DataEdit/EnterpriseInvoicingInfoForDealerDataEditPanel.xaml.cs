﻿
using System.Text.RegularExpressions;
using System.Windows.Input;

namespace Sunlight.Silverlight.Dcs.Common.Panels.DataEdit {
    public partial class EnterpriseInvoicingInfoForDealerDataEditPanel {
        private readonly string[] kvNames = {
            "PartsSupplier_TaxpayerKind","Invoice_Type","CompanyDocumentType"
        };
        public EnterpriseInvoicingInfoForDealerDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.LoadData();
            this.KeyValueManager.Register(this.kvNames);
        }
        public object TaxpayerQualifications {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
        private void UIElement_OnKeyDown(object sender, KeyEventArgs e) {
            var rg = new Regex("^[\u4e00-\u9fa5\b]$"); //\b是退格键     
            if(!rg.IsMatch(e.Key.ToString())) {
                e.Handled = true;

            }
        }

        public object KvInvoiceType {
            get {
                return this.KeyValueManager[this.kvNames[1]];
            }
        }
        public object CompanyDocumentType {
            get {
                return this.KeyValueManager[this.kvNames[2]];
            }
        }
    }
}
