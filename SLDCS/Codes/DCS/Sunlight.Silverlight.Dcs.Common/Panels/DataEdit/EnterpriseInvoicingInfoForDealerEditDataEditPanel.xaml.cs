﻿
using System.Text.RegularExpressions;
using System.Windows.Input;

namespace Sunlight.Silverlight.Dcs.Common.Panels.DataEdit {
    public partial class EnterpriseInvoicingInfoForDealerEditDataEditPanel {

        private readonly string[] kvNames = {
            "PartsSupplier_TaxpayerKind","MasterData_Status","Invoice_Type"
        };
        public EnterpriseInvoicingInfoForDealerEditDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }
        private void UIElement_OnKeyDown(object sender, KeyEventArgs e) {
            var rg = new Regex("^[\u4e00-\u9fa5\b]$"); //\b是退格键     
            if(!rg.IsMatch(e.Key.ToString())) {
                e.Handled = true;

            }
        }
    }
}
