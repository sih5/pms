﻿using System.Windows.Input;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Panels.DataEdit {
    public partial class ImageInfoDataEditPanel : DcsDataEditPanelBase {
        public ImageInfoDataEditPanel() {
            InitializeComponent();
        }

        private void Image_KeyDown(object sender, KeyEventArgs e) {
            if(e.Key == Key.Escape || e.Key == Key.Enter)
                this.close.Command.Execute(WindowCommands.Close);
        }

        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) {
            if(e.ClickCount > 1)
                this.close.Command.Execute(WindowCommands.Close);
        }
    }
}
