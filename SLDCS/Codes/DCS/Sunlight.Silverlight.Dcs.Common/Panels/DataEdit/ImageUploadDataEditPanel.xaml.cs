﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.DataEdit {
    public partial class ImageUploadDataEditPanel : INotifyPropertyChanged {
        private ObservableCollection<FileEntityes> imagePathList = new ObservableCollection<FileEntityes>();
        public event PropertyChangedEventHandler PropertyChanged;
        private string imgPath;
        private string strFileName;
        private string imgName;
        public ObservableCollection<FileEntityes> ImagePathList {
            get {
                return imagePathList;
            }
            set {
                this.imagePathList = value;
                this.OnPropertyChanged("ImagePathList");
            }
        }
        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
        /// <summary>
        /// 为true  隐藏 删除/上传按钮
        /// </summary>
        public bool isVisibility {
            set {
                if(value) {
                    this.dcsbutton.Visibility = Visibility.Collapsed;
                    this.RemoveItems.Visibility = Visibility.Collapsed;
                }
            }
        }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title {
            set {
                this.PanelTitle.Text = value;
            }
        }

        public string ImgPath {
            get {
                imgPath = "";
                foreach(var imagePathLists in ImagePathList) {
                    imgPath += imagePathLists.FileName + ":" + imagePathLists.FilePath + ":" + imagePathLists.FtpServerKey + "?";
                }
                //去掉最后一个 ？
                return imgPath == "" ? imgPath : imgPath.Substring(0, imgPath.Length - 1);
            }
            set {
                if(string.IsNullOrEmpty(value)) {
                    ImagePathList.Clear();
                    return;
                }
                var listPath = value.Split('?');
                ImagePathList.Clear();
                foreach(var item in listPath) {
                    //ImagePathList.Add(new FileEntityes(item.Substring(0, item.IndexOf(':')), item.Substring(item.IndexOf(':') + 1, item.Length - item.IndexOf(':') - 1)));
                    var filePath = item.Split(':');

                    var fileEntitye = new FileEntityes();
                    fileEntitye.FileName = filePath[0];
                    fileEntitye.FilePath = filePath[1];
                    //历史数据没有存ftpserver 默认给老ftp服务器的ftpserver
                    fileEntitye.FtpServerKey = filePath.Length == 2 ? "FtpServer" : filePath[2];
                    ImagePathList.Add(fileEntitye);
                }
            }
        }

        public ImageUploadDataEditPanel() {
            IsReadOnly = false;
            InitializeComponent();
            this.dcsbutton.Click += dcsbutton_Click;
            this.RemoveItems.Click += deleteButton_Click;
            this.RadUpload.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
            this.RadUpload.FileUploadStarting -= RadUpload_FileUploadStarting;
            this.RadUpload.FileUploadStarting += RadUpload_FileUploadStarting;
            this.RadUpload.UploadFinished -= this.RadUpload_UploadFinished;
            this.RadUpload.UploadFinished += this.RadUpload_UploadFinished;
            this.RadUpload.FilesSelected -= this.RadUpload_FilesSelected;
            this.RadUpload.FilesSelected += this.RadUpload_FilesSelected;
            this.RadUpload.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_PRODUCT_DIR);
            this.RadUpload.FileUploaded -= RadUpload_FileUploaded;
            this.RadUpload.FileUploaded += RadUpload_FileUploaded;
            this.PanelTitle.Text = CommonUIStrings.DetailPanel_GroupTitle_Image;
            this.TitleVisibility = "Visible";
        }

        public string titleVisibility;
        public string TitleVisibility {
            get {
                return titleVisibility;
            }
            set {
                this.titleVisibility = value;
                this.OnPropertyChanged("TitleVisibility");
            }
        }
        private void RadUpload_FileUploaded(object sender, FileUploadedEventArgs e) {
            if(!string.IsNullOrEmpty(e.HandlerData.Message)) {
                UIHelper.ShowAlertMessage(CommonUIStrings.DataEditPanel_Validation_Notification_UploadError);
            } else {
                this.imagePathList.Add(new FileEntityes(this.imgName, e.HandlerData.CustomData["Path"].ToString(), e.HandlerData.CustomData["FtpServerKey"].ToString()));
                UIHelper.ShowNotification(CommonUIStrings.DataEditPanel_Validation_Notification_Upload_PiSuccess);
            }
        }

        private void RadUpload_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload != null && upload.CurrentSession != null && upload.CurrentSession.CurrentFile != null) {
                upload.CancelUpload();
            }
        }


        void dcsbutton_Click(object sender, RoutedEventArgs e) {
            this.RadUpload.ShowFileDialog();
        }

        void deleteButton_Click(object sender, RoutedEventArgs e) {
            var fileEntityes = this.ImageContextMenu.SelectedItem as FileEntityes;
            if(fileEntityes == null)
                return;
            imagePathList.Remove(fileEntityes);
        }


        private void RadUpload_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any())
                try {
                    imgName = e.SelectedFiles[0].Name.Replace("+", "");
                    imgName = imgName.Replace("_", "");
                    imgName = imgName.Replace("%", "");
                    imgName = imgName.Replace("{", "");
                    imgName = imgName.Replace("}", "");
                    strFileName = imgName;
                    //因为在服务端有format此处注释以前上传文件不受影响
                    // strFileName = string.Format("{0}_{1}{2}", Path.GetFileNameWithoutExtension(imgName), Guid.NewGuid(), Path.GetExtension(imgName));
                    if(this.CheckFileName(strFileName)) {
                        var upload = sender as RadUpload;
                        upload.CancelUpload();
                    }
                } catch(Exception ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                    e.Handled = true;
                }
        }

        private bool CheckFileName(string filePath) {
            try {
                var strFilePath = this.ImgPath;
                if(strFilePath == "") {
                    strFilePath = this.ImgPath + ":" + filePath;
                } else {
                    strFilePath = strFilePath + "?" + this.ImgPath + ":" + filePath;
                }
                if(string.IsNullOrEmpty(strFilePath)) {
                    return false;
                }
                var listPath = strFilePath.Split('?');
                foreach(var item in listPath) {
                    var filename = item.Substring(0, item.IndexOf(':'));
                    var filepath = item.Substring(item.IndexOf(':') + 1, item.Length - item.IndexOf(':') - 1);
                }
                return false;
            } catch(Exception) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditPanel_Validation_Notification_UploadError2);
                return true;
            }
        }
        private void RadUpload_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            UIHelper.ShowNotification(CommonUIStrings.Action_Title_Uploading);//图片正在上传
        }
        private DateTime lastTime = DateTime.Now;
        private void Button_Click_1(object sender, RoutedEventArgs e) {
            //找到图片
            var button = sender as Button;
            if(button == null)
                return;
            var imagePath = button.Tag;
            if(imagePath == null)
                return;
            var filePath = ImagePathList.SingleOrDefault(ex => ex.FilePath == (string)imagePath);
            this.ImageContextMenu.SelectedItem = filePath;
            if((DateTime.Now - lastTime).TotalMilliseconds < 300 && !this.IsReadOnly) {
                var sFileUrl = DcsUtils.GetDownloadFileUrl((string)imagePath, filePath.FtpServerKey).ToString();//调这个方法下载。固定路径
                HtmlPage.Window.Navigate(new Uri(sFileUrl), "_blank");
            }
            lastTime = DateTime.Now;
        }

        /// <summary>
        /// 设置控件是否可以下载图片,市场AB质量信息快报管理-服务站 节点有此特殊需求
        /// </summary>
        public bool IsReadOnly {
            get;
            set;
        }
    }
}

