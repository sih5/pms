﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.DataEdit {
    public partial class ImageUploadWithShowDataEditPanel : INotifyPropertyChanged {
        private ObservableCollection<FileEntityes> filePathList = new ObservableCollection<FileEntityes>();
        public event PropertyChangedEventHandler PropertyChanged;
        private string filePath;
        private string strFileName;
        private string fileName;
        public ObservableCollection<FileEntityes> FilePathList {
            get {
                this.coverFlow.ItemsSource = filePathList;
                return filePathList;
            }
            set {
                this.filePathList = value;
                this.OnPropertyChanged("FilePathList");
            }
        }
        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
        /// <summary>
        /// 为true  隐藏 删除/上传按钮
        /// </summary>
        public bool isVisibility {
            set {
                if(value) {
                    this.dcsbutton.Visibility = Visibility.Collapsed;
                    this.RemoveItems.Visibility = Visibility.Collapsed;
                }
            }
        }

        public string FilePath {
            get {
                filePath = "";
                foreach(var filePathLists in FilePathList) {
                    filePath += filePathLists.FileName + ":" + filePathLists.FilePath + "?";
                }
                //去掉最后一个 ？
                return filePath == "" ? filePath : filePath.Substring(0, filePath.Length - 1);
            }
            set {
                if(string.IsNullOrEmpty(value)) {
                    FilePathList.Clear();
                    return;
                }
                var listPath = value.Split('?');
                FilePathList.Clear();
                foreach(var item in listPath) {
                    // FilePathList.Add(new FileEntityes(item.Substring(0, item.IndexOf(':')), item.Substring(item.IndexOf(':') + 1, item.Length - item.IndexOf(':') - 1), DcsUtils.GetDownloadFileUrl(item.Substring(item.IndexOf(':') + 1, item.Length - item.IndexOf(':') - 1))));
                    var filePath = item.Split(':');
                    ///FilePathList.Add(new FileEntityes(item.Substring(0, item.IndexOf(':')), item.Substring(item.IndexOf(':') + 1, item.Length - item.IndexOf(':') - 1)));
                    FilePathList.Add(new FileEntityes(filePath[0], filePath[1], filePath[2]));
                }
            }
        }

        public ImageUploadWithShowDataEditPanel() {
            InitializeComponent();
            this.dcsbutton.Click += dcsbutton_Click;
            this.RemoveItems.Click += deleteButton_Click;
            this.RadUpload.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
            this.RadUpload.FileUploadStarting -= RadUpload_FileUploadStarting;
            this.RadUpload.FileUploadStarting += RadUpload_FileUploadStarting;
            this.RadUpload.UploadFinished -= this.RadUpload_UploadFinished;
            this.RadUpload.UploadFinished += this.RadUpload_UploadFinished;
            this.RadUpload.FilesSelected -= this.RadUpload_FilesSelected;
            this.RadUpload.FilesSelected += this.RadUpload_FilesSelected;
            this.RadUpload.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_PRODUCT_DIR);
            this.RadUpload.FileUploaded += RadUpload_FileUploaded;
        }

        void RadUpload_FileUploaded(object sender, FileUploadedEventArgs e) {
            if(!string.IsNullOrEmpty(e.HandlerData.Message)) {
                UIHelper.ShowAlertMessage(CommonUIStrings.DataEditPanel_Validation_Notification_UploadError);
            } else {
                this.FilePathList.Add(new FileEntityes(this.fileName, e.HandlerData.CustomData["Path"].ToString(), e.HandlerData.CustomData["FtpServerKey"].ToString(), DcsUtils.GetDownloadFileUrl(e.HandlerData.CustomData["Path"].ToString())));
                this.coverFlow.SelectedItem = FilePathList.LastOrDefault();
                UIHelper.ShowNotification(CommonUIStrings.DataEditPanel_Validation_Notification_Upload_Success);
            }
        }

        void dcsbutton_Click(object sender, RoutedEventArgs e) {
            this.RadUpload.ShowFileDialog();
        }

        void deleteButton_Click(object sender, RoutedEventArgs e) {
            var fileEntityes = this.coverFlow.SelectedItem as FileEntityes;
            if(fileEntityes == null)
                return;
            FilePathList.Remove(fileEntityes);
        }


        private void RadUpload_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload != null && upload.CurrentSession != null && upload.CurrentSession.CurrentFile != null) {
                upload.CancelUpload();
            }
            //var partsClaimMaterialDetail = this.DataContext as PartsClaimMaterialDetail;
            //if(partsClaimMaterialDetail != null) {
            //    partsClaimMaterialDetail.attachment1 = this.FilePath;
            //}
         
        }
        private void RadUpload_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any())
                try {
                    fileName = e.SelectedFiles[0].Name.Replace("+", "");
                    fileName = fileName.Replace("_", "");
                    fileName = fileName.Replace("%", "");
                    strFileName = string.Format("{0}_{1}{2}", Path.GetFileNameWithoutExtension(fileName), Guid.NewGuid(), Path.GetExtension(fileName));

                } catch(Exception ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                    e.Handled = true;
                }
        }
        private void RadUpload_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            UIHelper.ShowNotification(CommonUIStrings.Action_Title_Uploading);//文件正在上传
        }

        private void OnContentClicked(object sender, RoutedEventArgs e) {
            var fileEntityes = this.coverFlow.SelectedItem as FileEntityes;
            if(fileEntityes == null)
                return;
            var infoWindow = new ImageInfoDataEditPanel();
            infoWindow.DataContext = fileEntityes;
            var window = new RadWindow();
            window.Content = infoWindow;
            window.Header = fileEntityes.FileName;
            window.ResizeMode = ResizeMode.NoResize;
            window.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
            window.ShowDialog();
        }

        private void Button_MouseRightButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e) {
            var fileEntityes = this.coverFlow.SelectedItem as FileEntityes;
            if(fileEntityes == null)
                return;
            var sFileUrl = DcsUtils.GetDownloadFileUrl(fileEntityes.FilePath).ToString();//调这个方法下载。固定路径
            HtmlPage.Window.Navigate(new Uri(sFileUrl), "_blank");
        }
    }
}
