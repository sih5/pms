﻿
using Sunlight.Silverlight.Core.Model;
using System.Collections.ObjectModel;
namespace Sunlight.Silverlight.Dcs.Common.Panels.DataEdit {
    public partial class LogisticCompanyDataEditPanel {
        private readonly string[] kvNames ={
                                              "Storage_Center"
                                          };
        public ObservableCollection<KeyValuePair> KvStorageCenters {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public LogisticCompanyDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
