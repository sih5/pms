using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;

namespace Sunlight.Silverlight.Dcs.Common.Panels.DataEdit {
    public partial class MarketingDepartmentDataEditPanel {
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();
        private ObservableCollection<KeyValuePair> kvProvinceNames;
        private ObservableCollection<KeyValuePair> kvCityNames;
        private ObservableCollection<KeyValuePair> kvCountyNames;
        private List<TiledRegion> TiledRegions = new List<TiledRegion>();
        private readonly string[] kvNames = new[] {
            "MarketingDepartment_Type"
        };

        public MarketingDepartmentDataEditPanel() {
            this.InitializeComponent();
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
            this.Initializer.Register(this.CreateUI);
            this.Loaded += MarketingDepartmentDataEditPanel_Loaded;//便于默认值
        }

        private void MarketingDepartmentDataEditPanel_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if (loadOp.HasError)
                    return;
                this.kvPartsSalesCategorys.Clear();
                foreach (var partsSalesCategory in loadOp.Entities)
                    this.kvPartsSalesCategorys.Add(new KeyValuePair
                    {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
                this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            }, null);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            //dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    foreach(var partsSalesCategory in loadOp.Entities)
            //        this.kvPartsSalesCategorys.Add(new KeyValuePair {
            //            Key = partsSalesCategory.Id,
            //            Value = partsSalesCategory.Name
            //        });
            //    this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            //}, null);
            dcsDomainContext.Load(dcsDomainContext.GetTiledRegionsQuery().Where(ex => ex.CountyName != null).OrderBy(ex => ex.ProvinceName), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var repairObject in loadOp.Entities)
                    this.TiledRegions.Add(new TiledRegion {
                        Id = repairObject.Id,
                        ProvinceName = repairObject.ProvinceName,
                        CityName = repairObject.CityName,
                        CountyName = repairObject.CountyName,
                    });

                foreach(var item in TiledRegions) {
                    var values = KvProvinceNames.Select(e => e.Value);
                    if(values.Contains(item.ProvinceName))
                        continue;
                    KvProvinceNames.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.ProvinceName
                    });
                }
            }, null);
        }


        private void provinceNameRadCombox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var markeingDepartment = this.DataContext as MarketingDepartment;
            if(markeingDepartment == null)
                return;
            this.KvCityNames.Clear();
            foreach(var item in TiledRegions.Where(ex => ex.ProvinceName == provinceNameRadCombox.Text).OrderBy(ex => ex.CityName)) {
                var values = KvCityNames.Select(ex => ex.Value);
                if(values.Contains(item.CityName))
                    continue;
                KvCityNames.Add(new KeyValuePair {
                    Key = item.Id,
                    Value = item.CityName
                });
            }
        }

        void cityNameRadCombox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var markeingDepartment = this.DataContext as MarketingDepartment;
            if(markeingDepartment == null)
                return;
            this.KvCountyNames.Clear();
            foreach(var item in TiledRegions.Where(ex => ex.CityName == cityNameRadCombox.Text).OrderBy(ex => ex.CountyName)) {
                var values = KvCountyNames.Select(ex => ex.Value);
                if(values.Contains(item.CountyName))
                    continue;
                KvCountyNames.Add(new KeyValuePair {
                    Key = item.Id,
                    Value = item.CountyName
                });
            }
        }
        private void CreateUI() {
            this.provinceNameRadCombox.SelectionChanged += provinceNameRadCombox_SelectionChanged;
            this.cityNameRadCombox.SelectionChanged += cityNameRadCombox_SelectionChanged;
        }

        public object KvBusinessTypes {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public string BranchName {
            get {
                return BaseApp.Current.CurrentUserData.EnterpriseName;
            }
        }
        public ObservableCollection<KeyValuePair> KvPartsSalesCategorys {
            get {
                return this.kvPartsSalesCategorys;
            }
        }
        public ObservableCollection<KeyValuePair> KvProvinceNames {
            get {
                return this.kvProvinceNames ?? (this.kvProvinceNames = new ObservableCollection<KeyValuePair>());
            }
        }
        public ObservableCollection<KeyValuePair> KvCityNames {
            get {
                return this.kvCityNames ?? (this.kvCityNames = new ObservableCollection<KeyValuePair>());
            }
        }
        public ObservableCollection<KeyValuePair> KvCountyNames {
            get {
                return this.kvCountyNames ?? (this.kvCountyNames = new ObservableCollection<KeyValuePair>());
            }
        }
    }
}
