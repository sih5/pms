﻿
using System;
using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Common.Panels.DataEdit {
    public partial class NotificationDataEditPanel : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;

        private readonly string[] kvNames ={
            "Notification_Type","Company_Type","NotificationUrgentLevel"
        };

        public object KvTypes {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public object KvCompanyTypes {
            get {
                return this.KeyValueManager[this.kvNames[1]];
            }
        }

        public object KvUrgentLevel {
            get {
                return this.KeyValueManager[this.kvNames[2]];
            }
        }

        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public NotificationDataEditPanel() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData();
            this.checkBox.Click += checkBox_Click;
        }

        private void checkBox_Click(object sender, System.Windows.RoutedEventArgs e) {
            if(this.checkBox.IsChecked == false)
                this.topTime.SelectedValue = null;
            else
                this.topTime.SelectedValue = DateTime.Now.Date;

        }
    }
}
