﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;

namespace Sunlight.Silverlight.Dcs.Common.Panels.DataEdit {
    public partial class NotificationDataEditView {
         private KeyValueManager keyValueManager;

        public NotificationDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }

        private readonly string[] kvNames ={
            "Notification_Type"
        };

        //public object Categories {
        //    get {
        //        return this.KeyValueManager[this.kvNames[0]];
        //    }
        //}

       
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        public object KvTypes {
            get {
                return this.keyValueManager[this.kvNames[0]];
            }
        }

    

    }
}
