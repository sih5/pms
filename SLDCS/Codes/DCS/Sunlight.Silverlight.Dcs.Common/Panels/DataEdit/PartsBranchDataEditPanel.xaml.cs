﻿using System;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Panels.DataEdit {
    public partial class PartsBranchDataEditPanel {
        private readonly string[] kvNames = {
            "PartsBranch_PurchaseRoute", "ABCStrategy_Category", "PartsBranch_ProductLifeCycle"
        };

        public PartsBranchDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            var queryWindowSparePart = DI.GetQueryWindow("SparePart");
            queryWindowSparePart.SelectionDecided += this.PartsBranchSupplierName_SelectionDecided;
            this.ptbSparePart.PopupContent = queryWindowSparePart;

            var queryWindowPartsWarrantyCategory = DI.GetQueryWindow("PartsWarrantyCategory");
            queryWindowPartsWarrantyCategory.SelectionDecided += this.PartsBranchPartsWarrantyCategoryName_SelectionDecided;
            this.ptbPartsWarrantyCategory.PopupContent = queryWindowPartsWarrantyCategory;
        }

        private void PartsBranchPartsWarrantyCategoryName_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            //var partsWarrantyCategory = queryWindow.SelectedEntities.Cast<PartsWarrantyCategory>().FirstOrDefault();
            //if(partsWarrantyCategory == null)
            //    return;

            //var partsBranch = this.DataContext as PartsBranch;
            //if(partsBranch == null)
            //    return;
            //partsBranch.PartsWarrantyCategoryId = partsWarrantyCategory.Id;
            //partsBranch.PartsWarrantyCategoryCode = partsWarrantyCategory.Code;
            //partsBranch.PartsWarrantyCategoryName = partsWarrantyCategory.Name;

            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void PartsBranchSupplierName_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var sparePart = queryWindow.SelectedEntities.Cast<SparePart>().FirstOrDefault();
            if(sparePart == null)
                return;

            var partsBranch = this.DataContext as PartsBranch;
            if(partsBranch == null)
                return;
            partsBranch.PartId = sparePart.Id;
            partsBranch.PartCode = sparePart.Code;
            partsBranch.PartName = sparePart.Name;

            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        public object ABCStrategies {
            get {
                return this.KeyValueManager[this.kvNames[1]];
            }
        }

        public object PurchaseRoutes {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public object ProductLifeCycles {
            get {
                return this.KeyValueManager[this.kvNames[2]];
            }
        }
    }
}
