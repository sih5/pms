﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Windows;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using System.Collections.ObjectModel;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.View;
using System.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Service.Panels.DataEdit {
    public partial class PartsPurchasePricingChangeApproveDataEditPanel {

        private DcsDomainContext domainContext = new DcsDomainContext();
        private ObservableCollection<KeyValuePair> kvIsPasseds;
        public ObservableCollection<KeyValuePair> KvIsPasseds
        {
            get
            {
                return this.kvIsPasseds ?? (this.kvIsPasseds = new ObservableCollection<KeyValuePair>());
            }
        }

        public PartsPurchasePricingChangeApproveDataEditPanel()
        {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI()
        {
            this.kvIsPasseds.Add(new KeyValuePair
            {
                Key = 1,
                Value = CommonUIStrings.DataEditView_KeyValue_Approve
            });
            this.kvIsPasseds.Add(new KeyValuePair
            {
                Key = 0,
                Value = CommonUIStrings.Action_Title_Reject
            });

        }

    }
}
