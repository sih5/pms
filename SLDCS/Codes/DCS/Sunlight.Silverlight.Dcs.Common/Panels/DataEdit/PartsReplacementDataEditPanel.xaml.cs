﻿using System;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Panels.DataEdit {
    public partial class PartsReplacementDataEditPanel {

        private readonly string[] kvNames = new[] {
            "PartsReplacement_ReplacementType"
        };

        public PartsReplacementDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            var queryWindowOldSparePart = DI.GetQueryWindow("SparePart");
            queryWindowOldSparePart.SelectionDecided += this.QueryWindowOldSparePart_SelectionDecided;
            this.popupTextBoxOldPartCode.PopupContent = queryWindowOldSparePart;
            var queryWindowNewSparePart = DI.GetQueryWindow("SparePart");
            queryWindowNewSparePart.SelectionDecided += this.QueryWindowNewSparePart_SelectionDecided;
            this.popupTextBoxNewPartCode.PopupContent = queryWindowNewSparePart;
        }

        private void QueryWindowOldSparePart_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var sparePart = queryWindow.SelectedEntities.Cast<SparePart>().FirstOrDefault();
            if(sparePart == null)
                return;
            var partsReplacement = this.DataContext as PartsReplacement;
            if(partsReplacement == null)
                return;
            partsReplacement.OldPartId = sparePart.Id;
            partsReplacement.OldPartCode = sparePart.Code;
            partsReplacement.OldPartName = sparePart.Name;
            partsReplacement.OldMInAmount = sparePart.MInPackingAmount;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void QueryWindowNewSparePart_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var sparePart = queryWindow.SelectedEntities.Cast<SparePart>().FirstOrDefault();
            if(sparePart == null)
                return;
            var partsReplacement = this.DataContext as PartsReplacement;
            if(partsReplacement == null)
                return;
            partsReplacement.NewPartId = sparePart.Id;
            partsReplacement.NewPartCode = sparePart.Code;
            partsReplacement.NewPartName = sparePart.Name;
            partsReplacement.RepMInAmount = sparePart.MInPackingAmount;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        public object KvTypes {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
    }
}
