﻿
namespace Sunlight.Silverlight.Dcs.Common.Panels.DataEdit {
    public partial class ServiceProductLineForSetupDataEditPanel {
        public ServiceProductLineForSetupDataEditPanel() {
            InitializeComponent();
        }

        public string BranchName {
            get {
                return BaseApp.Current.CurrentUserData.EnterpriseName;
            }
        }
    }
}
