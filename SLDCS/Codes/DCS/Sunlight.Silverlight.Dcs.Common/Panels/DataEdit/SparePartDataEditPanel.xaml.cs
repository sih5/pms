﻿
using System;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using System.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Panels.DataEdit {
    public partial class SparePartDataEditPanel {
        private readonly string[] kvNames = {
            "SparePart_MeasureUnit", "SparePart_PartType", "SparePart_LossType","ABCGroupCategory","SparePart_KeyCode","TraceProperty"
        };

        public SparePartDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            var queryWindowGoldenTaxClassify = DI.GetQueryWindow("GoldenTaxClassify");
            queryWindowGoldenTaxClassify.SelectionDecided += this.QueryWindowGoldenTaxClassify_SelectionDecided;
            this.popupTextBoxGoldenTaxClassify.PopupContent = queryWindowGoldenTaxClassify;


            //var queryWindowProductStandard = DI.GetQueryWindow("ProductStandard");
            //queryWindowProductStandard.SelectionDecided += this.QueryWindowProductStandard_SelectionDecided;
            //this.popupTextBoxStandardCode.PopupContent = queryWindowProductStandard;
        }
        private void QueryWindowGoldenTaxClassify_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var goldenTaxClassifyMsg = queryWindow.SelectedEntities.Cast<GoldenTaxClassifyMsg>().FirstOrDefault();
            if(goldenTaxClassifyMsg == null)
                return;
            var sparepart = this.DataContext as SparePart;
            if(sparepart == null)
                return;
            sparepart.GoldenTaxClassifyCode = goldenTaxClassifyMsg.GoldenTaxClassifyCode;
            sparepart.GoldenTaxClassifyName = goldenTaxClassifyMsg.GoldenTaxClassifyName;
            sparepart.GoldenTaxClassifyId = goldenTaxClassifyMsg.Id;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void QueryWindowProductStandard_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            //var goldenTaxClassifyMsg = queryWindow.SelectedEntities.Cast<ProductStandard>().FirstOrDefault();
            //if(goldenTaxClassifyMsg == null)
            //    return;
            var sparepart = this.DataContext as SparePart;
            if(sparepart == null)
                return;
            //sparepart.StandardCode = goldenTaxClassifyMsg.StandardCode;
            //sparepart.StandardName = goldenTaxClassifyMsg.StandardName;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }
        public object PartTypes {
            get {
                return this.KeyValueManager[this.kvNames[1]];
            }
        }
         public object PartKeyCodes {
            get {
                return this.KeyValueManager[this.kvNames[4]];
            }
        }
        public object MeasureUnits {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public object LossTypes {
            get {
                return this.KeyValueManager[this.kvNames[2]];
            }
        }

        public object GroupABCCategorys {
            get {
                return this.KeyValueManager[this.kvNames[3]];
            }
        }
        public object TracePropertys {
            get {
                return this.KeyValueManager[this.kvNames[5]];
            }
        }
    }
}
