﻿
namespace Sunlight.Silverlight.Dcs.Common.Panels.DataEdit {
    public partial class SparePartPauseDataEditPanel {
        private readonly string[] kvNames = {
            "SparePart_PartType", "SparePart_LossType", "MasterData_Status"
        };

        public SparePartPauseDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
