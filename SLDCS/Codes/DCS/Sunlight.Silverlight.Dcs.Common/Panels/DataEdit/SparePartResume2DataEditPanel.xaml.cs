﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Sunlight.Silverlight.Dcs.Common.Panels.DataEdit {
    public partial class SparePartResume2DataEditPanel  {
          private readonly string[] kvNames = {
            "SparePart_PartType", "SparePart_LossType", "MasterData_Status"
        };

          public SparePartResume2DataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
