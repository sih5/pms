﻿
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels {
    public class DealerCoincidenceDetailStaQueryPanel : DcsQueryPanelBase {
        public DealerCoincidenceDetailStaQueryPanel() {
            Initializer.Register(Initialize);
        }

        private void Initialize() {
            QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = CommonUIStrings.QueryPanel_Title_PartsStockSame,
                        EntityType = typeof(PartsStock),
                        QueryItems = new QueryItem[] {
                      
                        }
                    }
                };
        }
    }
}
