﻿
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Detail {
    public partial class BottomStockDetailPanel {
        public BottomStockDetailPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }
        private readonly string[] kvNames = {
            "BaseData_Status","Company_Type"
        };

        public override string Title {
            get {
                return "保底库存明细";
            }
        }
    }
}
