﻿using System;
using System.Windows;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Common.Views.DataGrid;
using Sunlight.Silverlight.Dcs.Common.Resources;


namespace Sunlight.Silverlight.Dcs.Common.Panels.Detail {
    public class DealerDetailPanel : DealerForDetailDataGridView, IDetailPanel {
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DomainDataSource.PageSize = 10;
            this.GridView.ShowGroupPanel = false;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
        }

        public string Title {
            get {
                return CommonUIStrings.DataDetailPanel_Title_Dealer;
            }
        }

        public Uri Icon {
            get {
                return null;
            }
        }
    }
}
