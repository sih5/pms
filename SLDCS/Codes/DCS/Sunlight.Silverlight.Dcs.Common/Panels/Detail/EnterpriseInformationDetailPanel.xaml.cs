﻿
namespace Sunlight.Silverlight.Dcs.Common.Panels.Detail {
    public partial class EnterpriseInformationDetailPanel {
        private readonly string[] kvNames = {
            "Customer_IdDocumentType", "Corporate_Nature","Company_CityLevel"
        };
        public EnterpriseInformationDetailPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
