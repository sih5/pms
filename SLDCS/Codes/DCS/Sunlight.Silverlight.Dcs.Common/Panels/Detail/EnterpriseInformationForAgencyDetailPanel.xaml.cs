﻿
namespace Sunlight.Silverlight.Dcs.Common.Panels.Detail {
    public partial class EnterpriseInformationForAgencyDetailPanel {
        private readonly string[] kvNames = {
            "Customer_IdDocumentType", "Corporate_Nature","Company_CityLevel"
        };
        public EnterpriseInformationForAgencyDetailPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
