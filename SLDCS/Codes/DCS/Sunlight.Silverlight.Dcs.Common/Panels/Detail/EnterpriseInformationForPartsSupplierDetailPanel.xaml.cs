﻿
namespace Sunlight.Silverlight.Dcs.Common.Panels.Detail {
    public partial class EnterpriseInformationForPartsSupplierDetailPanel {
        private readonly string[] kvNames = {
            "Customer_IdDocumentType", "Corporate_Nature","Company_CityLevel"
        };
        public EnterpriseInformationForPartsSupplierDetailPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
