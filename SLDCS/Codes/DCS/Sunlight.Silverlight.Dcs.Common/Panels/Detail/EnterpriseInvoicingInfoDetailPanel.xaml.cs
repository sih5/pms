﻿
namespace Sunlight.Silverlight.Dcs.Common.Panels.Detail {
    public partial class EnterpriseInvoicingInfoDetailPanel {
        private readonly string[] kvNames = {
            "PartsSupplier_TaxpayerKind","MasterData_Status","Invoice_Type"
        };
        public EnterpriseInvoicingInfoDetailPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
