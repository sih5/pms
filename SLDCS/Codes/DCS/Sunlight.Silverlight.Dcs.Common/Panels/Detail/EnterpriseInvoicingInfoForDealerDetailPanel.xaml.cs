﻿

namespace Sunlight.Silverlight.Dcs.Common.Panels.Detail {
    public partial class EnterpriseInvoicingInfoForDealerDetailPanel {
        private readonly string[] kvNames = {
            "PartsSupplier_TaxpayerKind","MasterData_Status","Invoice_Type","CompanyDocumentType"
        };
        public EnterpriseInvoicingInfoForDealerDetailPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
