﻿using System.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;
using System.Collections.ObjectModel;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Detail {
    public partial class LogisticCompanyDetailPanel {
        private readonly string[] kvNames = {
            "MasterData_Status","Storage_Center"
        };

        public override string Title {
            get {
                return CommonUIStrings.DetailPanel_Title_Common;
            }
        }

        public LogisticCompanyDetailPanel() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            var enterpriseInformation = DI.GetDetailPanel("EnterpriseInformation");
            enterpriseInformation.SetValue(Grid.RowProperty, 2);
            enterpriseInformation.SetValue(Grid.ColumnProperty, 0);
            enterpriseInformation.SetValue(Grid.ColumnSpanProperty, 6);
            this.Root.Children.Add(enterpriseInformation);
        }

        public ObservableCollection<KeyValuePair> KvStorageCenters {
            get {
                return this.KeyValueManager[this.kvNames[1]];
            }
        }
    }
}
