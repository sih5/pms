﻿
using System.Windows;
using Sunlight.Silverlight.Dcs.Common.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;


namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Detail {
    public partial class NotificationReplyReplyFilePathDetailPanel {
        public NotificationReplyReplyFilePathDetailPanel() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += PartsExchangeHistoryDataGridView_DataContextChanged;
        }


        private void PartsExchangeHistoryDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var notificationReply = e.NewValue as NotificationReply;
            if(notificationReply != null)
                this.ReplyFileUploadDataEditPanels.FilePath = notificationReply.ReplyFilePath;
        }
        private void CreateUI() {
            this.LayoutRoot.Children.Add(ReplyFileUploadDataEditPanels);

        }
        public override string Title {
            get {
                return CommonUIStrings.DataEditPanel_Title_Enclosure;
            }
        }
        private FileUploadDataEditPanel replyFileUploadDataEditPanels;
        protected FileUploadDataEditPanel ReplyFileUploadDataEditPanels {
            get {
                if(replyFileUploadDataEditPanels != null)
                    return this.replyFileUploadDataEditPanels;
                var panel = this.replyFileUploadDataEditPanels = (FileUploadDataEditPanel)DI.GetDataEditPanel("FileUpload");
                panel.PPanelTitle.Text = CommonUIStrings.DataEditPanel_Title_Enclosure;
                panel.isHiddenButtons = true;
                //panel.PanelTitle.Foreground = new SolidColorBrush(Colors.Red);
                return panel;
            }
        }
    }
}
