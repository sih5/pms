﻿using System;
using System.Windows;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Common.Views.DataGrid;
using Sunlight.Silverlight.Dcs.Common.Resources;


namespace Sunlight.Silverlight.Dcs.Common.Panels.Detail {
    public class PartDeleaveHistoryDetailPanel : PartDeleaveHistoryDataGridView, IDetailPanel {
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DomainDataSource.PageSize = 10;
            this.GridView.ShowGroupPanel = false;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
        }

        public string Title {
            get {
                return CommonUIStrings.DataEditPanel_Title_SparePart;
            }
        }

        public Uri Icon {
            get {
                return null;
            }
        }
    }
}
