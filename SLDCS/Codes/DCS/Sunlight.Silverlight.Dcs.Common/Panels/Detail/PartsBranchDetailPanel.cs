﻿using System;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Common.Views.DataGrid;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Detail {
    public class PartsBranchDetailPanel : PartsBranchDetailDataGridView, IDetailPanel {
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        public string Title {
            get {
                return "配件营销信息变更履历";
            }
        }

        public Uri Icon {
            get {
                return null;
            }
        }
    }
}