﻿using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Detail {
    public partial class PartsBranchDetailPanel {
        private readonly string[] kvNames = {
            "ABCStrategy_Category", "BaseData_Status", "PartsBranch_PurchaseRoute","PartsBranch_ProductLifeCycle","WarrantySupply_Status", "PartsBranch_PartsWarhouseManageGranularity"
        };

        public PartsBranchDetailPanel() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }

        public override string Title {
            get {
                return CommonUIStrings.DetailPanel_Title_Common;
            }
        }
    }
}
