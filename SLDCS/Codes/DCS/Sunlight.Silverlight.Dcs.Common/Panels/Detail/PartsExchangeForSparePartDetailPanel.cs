﻿using System;
using System.Windows;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.DataGrid;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Detail {
    public class PartsExchangeForSparePartDetailPanel : PartsExchangeForSparePartDataGridView, IDetailPanel {
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DomainDataSource.PageSize = 10;
            this.GridView.ShowGroupPanel = false;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
        }

        public string Title {
            get {
                return CommonUIStrings.DetailPanel_Title_PartsExchangeForSparePart;
            }
        }

        public Uri Icon {
            get {
                return null;
            }
        }
    }
}
