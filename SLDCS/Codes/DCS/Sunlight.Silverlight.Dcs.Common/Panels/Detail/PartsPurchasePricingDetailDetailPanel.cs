﻿using System;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Common.Views.DataGrid;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Detail {
    public class PartsPurchasePricingDetailDetailPanel : PartsPurchasePricingDetailDataGridView, IDetailPanel {
        public string Title {
            get {
                return Utils.GetEntityLocalizedName(typeof(PartsPurchasePricingChange), "PartsPurchasePricingDetails");
            }
        }

        public Uri Icon {
            get {
                return null;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DomainDataSource.PageSize = 10;
            this.GridView.ShowGroupPanel = false;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
            ((GridViewDataColumn)this.GridView.Columns["ValidTo"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ValidFrom"]).DataFormatString = "d";
        }
    }
}
