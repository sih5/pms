﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Common.Views.DataGrid;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Detail {
    public class PartsPurchasePricingYXDetailDetailPanel : PartsPurchasePricingYXDetailDataGridView, IDetailPanel {
        public string Title {
            get {
                return Utils.GetEntityLocalizedName(typeof(PartsPurchasePricingChange), "PartsPurchasePricingDetails");
            }
        }

        public Uri Icon {
            get {
                return null;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DomainDataSource.PageSize = 10;
            this.GridView.ShowGroupPanel = false;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
            ((GridViewDataColumn)this.GridView.Columns["ValidTo"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ValidFrom"]).DataFormatString = "d";
        }
    }
}
