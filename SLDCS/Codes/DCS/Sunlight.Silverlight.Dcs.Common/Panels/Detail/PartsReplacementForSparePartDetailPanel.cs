﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.DataGrid;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Detail {
    public class PartsReplacementForSparePartDetailPanel : PartsReplacementForSparePartDataGridView, IDetailPanel {
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsReplacement);
            }
        }
        private readonly string[] kvNames = new[] {
            "BaseData_Status"
        };

         public PartsReplacementForSparePartDetailPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += this.PartsReplacementDataGridView_DataContextChanged;

        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DomainDataSource.PageSize = 10;
            this.GridView.ShowGroupPanel = false;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
        }

        public string Title {
            get {
                return CommonUIStrings.DetailPanel_Title_PartsReplacementForSparePart;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "NewPartCode"
                        //Title=CommonUIStrings.DetailPanel_Title_PartsInsteadInfo_SparePartCode
                    }, new ColumnItem {
                        Name = "NewPartName"
                        //Title=CommonUIStrings.DetailPanel_Title_PartsInsteadInfo_SparePartName
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }
                };
            }
        }

        public Uri Icon {
            get {
                return null;
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsReplacements";
        }

        private void PartsReplacementDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var sparePart = e.NewValue as SparePart;
            if(sparePart == null || sparePart.Id == default(int))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "PartId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                };
            this.FilterItem.Value = sparePart.Id;
            this.ExecuteQueryDelayed();
        }
    }
}
