﻿using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Detail {
    public partial class PartsSupplierDetailPanel {
        private readonly string[] kvNames = {
            "PartsSupplier_Type", "PartsSupplier_TaxpayerKind", "MasterData_Status"
        };

        public PartsSupplierDetailPanel() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }

        public override string Title {
            get {
                return CommonUIStrings.DetailPanel_Title_Common;
            }
        }
    }
}
