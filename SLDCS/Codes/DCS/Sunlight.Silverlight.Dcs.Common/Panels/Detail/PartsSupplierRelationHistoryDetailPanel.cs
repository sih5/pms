﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Common.Views.DataGrid;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;


namespace Sunlight.Silverlight.Dcs.Common.Panels.Detail {
    public class PartsSupplierRelationHistoryDetailPanel : PartsSupplierRelationHistoryDataGridView, IDetailPanel {

        public Uri Icon {
            get {
                return null;
            }
        }

        public string Title {
            get {
                return CommonUIStrings.DataDetailPanel_Title_PartsSupplierRelation;
            }
        }

        public PartsSupplierRelationHistoryDetailPanel() {
            this.DataContextChanged += this.PartsSupplierRelationHistoryDetailPanel_DataContextChanged;
        }

        void PartsSupplierRelationHistoryDetailPanel_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var partsSupplierRelation = e.NewValue as PartsSupplierRelation;
            if(partsSupplierRelation == null)
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "PartsSupplierRelationId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo
                };
            this.FilterItem.Value = partsSupplierRelation.Id;
            this.ExecuteQueryDelayed();
        }
    }
}
