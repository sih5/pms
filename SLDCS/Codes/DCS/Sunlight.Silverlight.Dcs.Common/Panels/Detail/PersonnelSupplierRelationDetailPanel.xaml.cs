﻿
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Detail {
    public partial class PersonnelSupplierRelationDetailPanel {
        public PersonnelSupplierRelationDetailPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }
        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        public override string Title {
            get {
                return CommonUIStrings.DataEditPanel_Title_PersonnelSupplierRelation;
            }
        }
    }
}
