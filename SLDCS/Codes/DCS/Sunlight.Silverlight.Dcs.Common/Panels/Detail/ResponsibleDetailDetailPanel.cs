﻿using System;
using System.Windows;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Common.Views.DataGrid;


namespace Sunlight.Silverlight.Dcs.Common.Panels.Detail {
    public class ResponsibleDetailDetailPanel : ResponsibleDetailDataGridView , IDetailPanel {
        public string Title {
            get {
                return "人员信息";
            }
        }

        public Uri Icon {
            get {
                return null;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DomainDataSource.PageSize = 10;
            this.GridView.ShowGroupPanel = false;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
        }
    }
}
