﻿using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Detail {
    public partial class SparePartDetailPanel {
        private readonly string[] kvNames = {
            "SparePart_PartType", "MasterData_Status", "SparePart_LossType"
        };

        public SparePartDetailPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }

        public override string Title {
            get {
                return CommonUIStrings.DetailPanel_Title_Common;
            }
        }
    }
}
