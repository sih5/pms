﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class ABCStrategyQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "ABCStrategy_Category", "BaseData_Status"
        };

        public ABCStrategyQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_ABCStrategy,
                    EntityType = typeof(ABCStrategy),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "BranchName"
                        }, new KeyValuesQueryItem {
                            ColumnName = "Category",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsABCStrategyCategory.A
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            DefaultValue = (int)DcsBaseDataStatus.有效
                        }
                    }
                }
            };
        }
    }
}
