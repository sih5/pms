﻿using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using System;
using Sunlight.Silverlight.Core.ViewModel;
using System.Windows.Data;
using Sunlight.Silverlight.Dcs.Common.Resources;


namespace Sunlight.Silverlight.Dcs.Common.Panels.Query
{
    public class AcrossBrandPartsTransferStaQueryPanel: DcsQueryPanelBase
    {
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvOutPartsSalesCategories = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvInPartsSalesCategories = new ObservableCollection<KeyValuePair>();

        private readonly AcrossBrandPartsTransferStaQueryPanelViewModel viewModel = new AcrossBrandPartsTransferStaQueryPanelViewModel();
        
        public AcrossBrandPartsTransferStaQueryPanel()
        {
            Initializer.Register(Initialize);
            this.Initializer.Register(this.viewModel.Initialize);
        }

        private void Initialize()
        {
            var dcsDomainContext = new DcsDomainContext();
            //如果是ReportAdmin登录
            if(BaseApp.Current.CurrentUserData.UserCode == "ReportAdmin") {
                QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = CommonUIStrings.QueryPanel_Title_AcrossBrandPartsTransfer,
                        EntityType = typeof(PartsShippingOrder),
                        QueryItems = new QueryItem[] {
                          
                            new ComboQueryItem{
                                Title = CommonUIStrings.QueryPanel_QueryItem_Title_BranchSupplierRelation_BranchName,
                                ColumnName = "BranchCode",
                                SelectedValuePath = "Key",
                                DisplayMemberPath = "Value",
                                ItemsSource = this.viewModel.KvKeyValueItem,
                                SelectedItemBinding = new Binding("SelectedKeyValueItem"){
                                    Source = this.viewModel,
                                    Mode = BindingMode.TwoWay
                                }
                               
                            },
                            new CustomQueryItem{
                                Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsTransferOrder_Code,
                                ColumnName="TransferCode",
                                DataType = typeof(string)
                            },new DateTimeRangeQueryItem{
                               Title = CommonUIStrings.QueryPanel_Title_ProductStandard_CreateTime,
                               ColumnName = "CreateTime",
                               DefaultValue = new[] {
                                    DateTime.Now.AddMonths(-1), DateTime.Now
                                }
                            },new CustomQueryItem{
                                Title=CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                                ColumnName="PartCode",
                                DataType = typeof(string)
                            },new CustomQueryItem{
                                Title=CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Name,
                                ColumnName="PartName",
                                DataType = typeof(string)
                            },new CustomQueryItem{
                                Title=CommonUIStrings.QueryPanel_QueryItem_Title_Company_Code,
                                ColumnName="ComCode",
                                DataType = typeof(string)
                            },new CustomQueryItem{
                                Title=CommonUIStrings.QueryPanel_QueryItem_Title_Company_Name,
                                ColumnName="ComName",
                                DataType = typeof(string)
                            },new CustomQueryItem{
                                Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrder_OriginalWarehouseName,
                                ColumnName="OriginalWarehouseName",
                                DataType = typeof(string)
                            },new CustomQueryItem{
                                Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrder_DestWarehouseName,
                                ColumnName="DestWarehouseName",
                                DataType = typeof(string)
                            },
                            new ComboQueryItem{
                                 ColumnName = "OutPartsSalesCateGoryId",
                                 Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrder_OutPartsSalesCate,
                                 SelectedValuePath = "Id",
                                 DisplayMemberPath = "Name",
                                 ItemsSource = this.viewModel.OutPartsSalesCategories
                            },
                            new ComboQueryItem{
                                ColumnName = "InPartsSalesCateGoryId",
                                Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrder_InPartsSalesCate,
                                SelectedValuePath = "Id",
                                DisplayMemberPath = "Name",
                                ItemsSource = this.viewModel.InPartsSalesCategories
                            }
                       }
                    }
                };
           // }, null);

            } else {
            //var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetKeyValueItemsQuery().Where(r => r.Name == "BranchList"), LoadBehavior.RefreshCurrent, loadOpKey =>
            {
                if (loadOpKey.HasError)
                {
                    if (!loadOpKey.IsErrorHandled)
                        loadOpKey.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOpKey);
                }
                this.kvBranches.Clear();
                foreach (var branch in loadOpKey.Entities)
                    this.kvBranches.Add(new KeyValuePair
                    {
                        Key = branch.Key,
                        Value = branch.Value
                    });
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(r => r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp2 =>
            {
                if (loadOp2.HasError)
                {
                    if (!loadOp2.IsErrorHandled)
                        loadOp2.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp2);
                    return;
                }
                this.kvOutPartsSalesCategories.Clear();
                foreach (var item in loadOp2.Entities)
                {
                    kvOutPartsSalesCategories.Add(new KeyValuePair
                    {
                        Key = item.Id,
                        Value = item.Name
                    });

                }
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(r => r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp2 =>
            {
                if (loadOp2.HasError)
                {
                    if (!loadOp2.IsErrorHandled)
                        loadOp2.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp2);
                    return;
                }
                this.kvInPartsSalesCategories.Clear();
                foreach (var item in loadOp2.Entities)
                {
                    kvInPartsSalesCategories.Add(new KeyValuePair
                    {
                        Key = item.Id,
                        Value = item.Name
                    });

                }
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetCompaniesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                    return;
                var company = loadOp.Entities.SingleOrDefault();
                if (company == null)
                    return;
                QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                         Title = CommonUIStrings.QueryPanel_Title_AcrossBrandPartsTransfer,
                        EntityType = typeof(PartsShippingOrder),
                        QueryItems = new QueryItem[] {
                            new KeyValuesQueryItem {
                                     Title = CommonUIStrings.QueryPanel_QueryItem_Title_BranchSupplierRelation_BranchName,
                                    ColumnName = "BranchCode",
                                    KeyValueItems = kvBranches,
                                    DefaultValue = int.Parse(BaseApp.Current.CurrentUserData.EnterpriseCode),
                                    IsEnabled =BaseApp.Current.CurrentUserData.UserCode=="ReportAdmin"
                            },new CustomQueryItem{
                                Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsTransferOrder_Code,
                                ColumnName="TransferCode",
                                DataType = typeof(string)
                            },new DateTimeRangeQueryItem{
                               Title = CommonUIStrings.QueryPanel_Title_ProductStandard_CreateTime,
                               ColumnName = "CreateTime",
                               DefaultValue = new[] {
                                    DateTime.Now.AddMonths(-1), DateTime.Now
                                }
                            },new CustomQueryItem{
                                 Title=CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                                ColumnName="PartCode",
                                DataType = typeof(string)
                            },new CustomQueryItem{
                                Title=CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Name,
                                ColumnName="PartName",
                                DataType = typeof(string)
                            },new CustomQueryItem{
                                Title=CommonUIStrings.QueryPanel_QueryItem_Title_Company_Code,
                                ColumnName="ComCode",
                                DataType = typeof(string)
                            },new CustomQueryItem{
                                Title=CommonUIStrings.QueryPanel_QueryItem_Title_Company_Name,
                                ColumnName="ComName",
                                DataType = typeof(string)
                            },new CustomQueryItem{
                                Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrder_OriginalWarehouseName,
                                ColumnName="OriginalWarehouseName",
                                DataType = typeof(string)
                            },new CustomQueryItem{
                                Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrder_DestWarehouseName,
                                ColumnName="DestWarehouseName",
                                DataType = typeof(string)
                            },new KeyValuesQueryItem {
                                ColumnName = "OutPartsSalesCateGoryId",
                                KeyValueItems = this.kvOutPartsSalesCategories,
                                Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrder_OutPartsSalesCate,
                            },new KeyValuesQueryItem {
                                ColumnName = "InPartsSalesCateGoryId",
                                KeyValueItems = this.kvInPartsSalesCategories,
                                Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrder_InPartsSalesCate,
                            }
                       }
                    }
                };
            }, null);
        }
        }
    }


    public class AcrossBrandPartsTransferStaQueryPanelViewModel : ViewModelBase {
        //分公司
        public readonly ObservableCollection<KeyValueItem> kvKeyValueItem = new ObservableCollection<KeyValueItem>();
        //出库品牌
        public readonly ObservableCollection<PartsSalesCategoryView> allOutPartsSalesCategories = new ObservableCollection<PartsSalesCategoryView>();
        //入库品牌
        public readonly ObservableCollection<PartsSalesCategoryView> allInPartsSalesCategories = new ObservableCollection<PartsSalesCategoryView>();

        private KeyValueItem keyValueItem;
        private PagedCollectionView outPartsSalesCategories;
        private PagedCollectionView inPartsSalesCategories;

        public ObservableCollection<KeyValueItem> KvKeyValueItem {
            get {
                return this.kvKeyValueItem;
            }
        }

        public KeyValueItem SelectedKeyValueItem {
            get {
                return this.keyValueItem;
            }
            set {
                if(this.keyValueItem == value)
                    return;
                this.keyValueItem = value;
                this.NotifyOfPropertyChange("SelectedKeyValueItem");
                this.OutPartsSalesCategories.Refresh();
                this.InPartsSalesCategories.Refresh();
            }
        }

        public PagedCollectionView OutPartsSalesCategories {
            get {
                if(this.outPartsSalesCategories == null){
                    this.outPartsSalesCategories = new PagedCollectionView(this.allOutPartsSalesCategories);
                    this.outPartsSalesCategories.Filter = o => ((PartsSalesCategoryView)o).BranchCode == (this.SelectedKeyValueItem != null ? this.SelectedKeyValueItem.Value : string.Empty);
                }
                return this.outPartsSalesCategories;
            }
        }
        public PagedCollectionView InPartsSalesCategories {
            get {
                if(this.inPartsSalesCategories == null) {
                    this.inPartsSalesCategories = new PagedCollectionView(this.allInPartsSalesCategories);
                    this.inPartsSalesCategories.Filter = o => ((PartsSalesCategoryView)o).BranchCode == (this.SelectedKeyValueItem != null ? this.SelectedKeyValueItem.Value : string.Empty);
                }
                return this.inPartsSalesCategories;
            }
        }

        public override void Validate() {
            //throw new NotImplementedException();
        }

        public void Initialize() {
            var domainContext = new DcsDomainContext();
            //分公司
            domainContext.Load(domainContext.GetKeyValueItemsQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.Name == "BranchList"), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.kvKeyValueItem.Clear();
                foreach(var branch in loadOp.Entities){
                    this.kvKeyValueItem.Add(branch);
                }
            }, null);
            domainContext.Load(domainContext.GetPartsSalesCategoryViewsQuery(), LoadBehavior.RefreshCurrent, loadOp2 => {
                if(loadOp2.HasError)
                    return;
                foreach(var outPartsSalesCategory in loadOp2.Entities){
                    this.allOutPartsSalesCategories.Add(outPartsSalesCategory);
                }
            }, null);
            domainContext.Load(domainContext.GetPartsSalesCategoryViewsQuery(), LoadBehavior.RefreshCurrent, loadOp3 => {
                if(loadOp3.HasError)
                    return;
                foreach(var inPartsSalesCategory in loadOp3.Entities){
                    this.allInPartsSalesCategories.Add(inPartsSalesCategory);
                }
            }, null);
           
        }
    }

}


