﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class AgencyAffiBranchQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "MasterData_Status"
        };

        public AgencyAffiBranchQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_AgencyAffiBranch,
                    EntityType = typeof(AgencyAffiBranch),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                            ColumnName = "Agency.Code",
                            DataType = typeof(string),
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_Code
                        },new CustomQueryItem {
                            ColumnName = "Agency.Name",
                            DataType = typeof(string),
                           Title = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_Name,
                        }, new KeyValuesQueryItem {
                            ColumnName = "Agency.Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsMasterDataStatus.有效,
                            IsEnabled = false,
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_Common_Status
                        },new CustomQueryItem{
                            ColumnName="Agency.Company.ProvinceName",
                            DataType=typeof(string),
                            Title=CommonUIStrings.DataGrid_QueryItem_Title_ProvinceName
                        },new CustomQueryItem{
                            ColumnName="Agency.Company.CityName",
                            DataType=typeof(string),
                            Title=CommonUIStrings.DetailPanel_Text_Region_City
                        },new CustomQueryItem{
                            ColumnName="Agency.Company.CountyName",
                            DataType=typeof(string),
                            Title="区/县"
                        }
                    }
                }
            };
        }
    }
}
