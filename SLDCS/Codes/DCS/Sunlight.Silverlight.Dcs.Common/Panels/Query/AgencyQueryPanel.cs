﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class AgencyQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "MasterData_Status"
        };

        public AgencyQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_Agency,
                    EntityType = typeof(Agency),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_Code,
                        },new QueryItem {
                            ColumnName = "Name",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_Name,
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsMasterDataStatus.有效
                        }
                    }
                }
            };
        }
    }
}
