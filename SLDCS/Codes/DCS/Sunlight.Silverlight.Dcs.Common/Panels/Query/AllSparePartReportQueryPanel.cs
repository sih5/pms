﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query
{
    public class AllSparePartReportQueryPanel : DcsQueryPanelBase
    {
        private readonly string[] kvNames = new[] {
            "ABCStrategy_Category"
        };
        private readonly ObservableCollection<KeyValuePair> kvIncrease = new ObservableCollection<KeyValuePair>();

        public AllSparePartReportQueryPanel()
        {
            this.KeyValueManager.Register(this.kvNames);
            Initializer.Register(Initialize);
        }

        private void Initialize()
        {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalePriceIncreaseRatesQuery().Where(e => e.status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                    return;
                foreach (var branch in loadOp.Entities)
                    this.kvIncrease.Add(new KeyValuePair
                    {
                        Key = branch.Id,
                        Value = branch.GroupName
                    });
            }, null);
            this.QueryItemGroups = new[] {
                      new QueryItemGroup {
                           UniqueId = "Common",
                           Title = CommonUIStrings.QueryPanel_Title_AllSparepart,
                           EntityType = typeof(AllSparePart),
                           QueryItems = new QueryItem[]{
                           new QueryItem {
                              Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_SparePartCode,
                              ColumnName = "Code"
                           },new QueryItem {
                              Title = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                              ColumnName = "ReferenceCode"
                           }, new KeyValuesQueryItem {
                              ColumnName = "PartABC",
                              KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                              Title=CommonUIStrings.QueryPanel_QueryItem_Title_Sparepart_PartABC,
                           }, new KeyValuesQueryItem {
                              ColumnName = "IncreaseRateGroupId",
                              KeyValueItems = this.kvIncrease,
                              Title=CommonUIStrings.QueryPanel_QueryItem_Title_Sparepart_IncreaseRateGroupId
                           }, new CustomQueryItem {
                              Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_IsSalable,
                              ColumnName = "IsSalable",
                              DataType = typeof(bool),
                              DefaultValue = null
                           }, new CustomQueryItem {
                              Title =  CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_IsOrderable,
                              ColumnName = "IsOrderable",
                              DataType = typeof(bool),
                              DefaultValue = null
                           }, new CustomQueryItem {
                              Title = CommonUIStrings.QueryPanel_QueryItem_Title_Sparepart_IsPrimary,
                              ColumnName = "IsPrimary",
                              DataType = typeof(bool),
                              DefaultValue = null
                           }, new CustomQueryItem {
                              Title = CommonUIStrings.QueryPanel_QueryItem_Title_Sparepart_IsEffective,
                              ColumnName = "IsEffective",
                              DataType = typeof(bool),
                              DefaultValue = null
                           }
                           }
                      }
               };
        }
    }
}
