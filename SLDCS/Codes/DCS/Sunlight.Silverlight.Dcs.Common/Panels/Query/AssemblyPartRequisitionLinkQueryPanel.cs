﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class AssemblyPartRequisitionLinkQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "BaseData_Status","SparePart_KeyCode"
        };

        public AssemblyPartRequisitionLinkQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_AssemblyPartRequisitionLink,
                    EntityType = typeof(AssemblyPartRequisitionLink),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "PartCode",
                            Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_SparePartCode
                        }, new KeyValuesQueryItem {
                            ColumnName = "KeyCode",
                            Title=CommonUIStrings.QueryPanel_Column_KeyCode,
                            KeyValueItems =  this.KeyValueManager[this.kvNames[1]]
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            IsExact = false,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsBaseDataStatus.有效
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        }
                    }
                }
            };
        }
    }
}
