﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class AuthenticationRepairQueryPanel : DcsQueryPanelBase{
        private readonly string[] kvNames = new[] {
            "RepairClaimBill_RepairClaimStatus"
        };

        public AuthenticationRepairQueryPanel(){
            this.KeyValueManager.Register(this.kvNames);
            Initializer.Register(Initialize);
        }

        private readonly ObservableCollection<KeyValuePair> kvServiceProductLines = new ObservableCollection<KeyValuePair>();
        private ObservableCollection<PartsSalesCategoryView> kvPartsSalesCategorys;

        public ObservableCollection<PartsSalesCategoryView> KvPartsSalesCategorys {
            get {
                if(this.kvPartsSalesCategorys == null)
                    this.kvPartsSalesCategorys = new ObservableCollection<PartsSalesCategoryView>();
                return kvPartsSalesCategorys;
            }
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();

            domainContext.Load(domainContext.GetPartsSalesCategoryViewsQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvPartsSalesCategorys.Clear();
                foreach(var partsSalesCategories in loadOp.Entities)
                    this.KvPartsSalesCategorys.Add(partsSalesCategories);
                QueryItemGroups = new[] {
                      new QueryItemGroup {
                           UniqueId = "Common",
                           Title =CommonUIStrings.QueryPanel_Title_DealerTrain,
                           EntityType = typeof(AuthenticationRepair),
                           QueryItems = new QueryItem[]{
                                new ComboQueryItem {
                                    Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name,
                                    ColumnName = "PartsSalesCategoryId",
                                    SelectedValuePath = "Id",
                                    DisplayMemberPath = "Name",
                                    ItemsSource = this.kvPartsSalesCategorys
                                 }, new KeyValuesQueryItem {
                                     Title = CommonUIStrings.DataGridView_ColumnItem_Title_ProductView_ServiceProductLine,
                                     ColumnName = "ServiceProductLineId",
                                     KeyValueItems = this.kvServiceProductLines
                                 },
                                   new CustomQueryItem {
                                     Title = CommonUIStrings.QueryPanel_QueryItem_Title_AuthenticationRepair_DealerCode,
                                     ColumnName = "DealerCode",
                                     DataType = typeof(string)
                                 },
                                   new CustomQueryItem {
                                     Title = CommonUIStrings.QueryPanel_QueryItem_Title_AuthenticationRepair_DealerName,
                                     ColumnName = "DealerName",
                                     DataType = typeof(string)
                                 },
                                   new CustomQueryItem {
                                     Title = CommonUIStrings.QueryPanel_QueryItem_Title_AuthenticationRepair_RepairWorkerName,
                                     ColumnName = "RepairWorkerName",
                                     DataType = typeof(string)
                                 },
                                   new CustomQueryItem {
                                     Title = CommonUIStrings.QueryPanel_QueryItem_Title_AuthenticationRepair_MalfunctionCode,
                                     ColumnName = "MalfunctionCode",
                                     DataType = typeof(string)
                                 }, new KeyValuesQueryItem {
                                     ColumnName = "RepairClaimStatus",
                                     KeyValueItems = this.KeyValueManager[kvNames[0]],
                                     Title = CommonUIStrings.QueryPanel_QueryItem_Title_AuthenticationRepair_RepairClaimStatus
                                 },
                                   new CustomQueryItem {
                                     Title = CommonUIStrings.DataGridView_ColumnItem_Title_EngineModel_EngineModel,
                                     ColumnName = "EngineModel",
                                     DataType = typeof(string)
                                 },
                                   new CustomQueryItem {
                                     Title = CommonUIStrings.QueryPanel_QueryItem_Title_AuthenticationRepair_TrainingName,
                                     ColumnName = "TrainingName",
                                     DataType = typeof(string)
                                 },
                                 new DateTimeRangeQueryItem {
                                     Title = CommonUIStrings.QueryPanel_QueryItem_Title_AuthenticationRepair_TrainingTime,
                                     ColumnName = "TrainingTime"
                                 },
                                   new CustomQueryItem {
                                     Title = CommonUIStrings.QueryPanel_QueryItem_Title_AuthenticationRepair_RepairItemCode,
                                     ColumnName = "RepairItemCode",
                                     DataType = typeof(string)
                                 },
                                   new CustomQueryItem {
                                     Title = CommonUIStrings.QueryPanel_QueryItem_Title_AuthenticationRepair_RepairItemName,
                                     ColumnName = "RepairItemName",
                                     DataType = typeof(string)
                                 },
                                 new DateTimeRangeQueryItem {
                                     Title = CommonUIStrings.QueryPanel_Title_ProductStandard_CreateTime,
                                     ColumnName = "RepairRequestTime"
                                 }
                           }

                      }
               };
            }, null);
        }
    }
}
