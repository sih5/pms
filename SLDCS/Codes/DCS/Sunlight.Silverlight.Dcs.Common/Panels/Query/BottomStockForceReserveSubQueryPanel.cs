﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class BottomStockForceReserveSubQueryPanel : DcsQueryPanelBase
    {
        private readonly string[] kvNames = new[] {
          "BaseData_Status"
        };
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();

        public BottomStockForceReserveSubQueryPanel()
        {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize()
        {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetBottomStockForceReserveTypesQuery().Where(r=> r.Status==(int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp1 =>
            {
                if (loadOp1.HasError)
                    return;
                foreach (var partsSalesCategory in loadOp1.Entities)
                    this.kvPartsSalesCategorys.Add(new KeyValuePair
                    {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.ReserveType
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title =CommonUIStrings.QueryPanel_Title_BottomStockForceReserveSub,
                    EntityType = typeof(BottomStockForceReserveSub),
                    QueryItems = new[] {
                        new KeyValuesQueryItem {
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_BottomStockForceReserveType_ReserveType,
                            ColumnName = "BottomStockForceReserveTypeId",
                            KeyValueItems = this.kvPartsSalesCategorys
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            IsExact = false,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_Status
                        }, new QueryItem {
                            ColumnName = "SubCode",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_BottomStockForceReserveSub_SubCode,
                        },new QueryItem {
                            ColumnName = "SubName",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_BottomStockForceReserveSub_SubName,
                        }
                    }
                }
            };
        }
    }
}