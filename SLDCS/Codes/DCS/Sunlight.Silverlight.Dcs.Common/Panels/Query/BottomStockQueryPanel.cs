﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query
{
    public class BottomStockQueryPanel : DcsQueryPanelBase
    {
        private readonly string[] kvNames = new[] {
           "Company_Type","BaseData_Status"
        };
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();

        public BottomStockQueryPanel()
        {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize()
        {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesWithBranchIdQuery().Where(r=>r.BranchId==BaseApp.Current.CurrentUserData.EnterpriseId && r.Status==(int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp1 =>
            {
                if (loadOp1.HasError)
                    return;
                foreach (var partsSalesCategory in loadOp1.Entities)
                    this.kvPartsSalesCategorys.Add(new KeyValuePair
                    {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title =CommonUIStrings.QueryPanel_Title_BottomStock,
                    EntityType = typeof(BottomStock),
                    QueryItems = new[] {
                        new KeyValuesQueryItem {
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name,
                            ColumnName = "PartsSalesCategoryId",
                            KeyValueItems = this.kvPartsSalesCategorys
                        }, new KeyValuesQueryItem {
                            ColumnName = "CompanyType",
                            IsExact = false,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_CompanyType
                        }, new QueryItem {
                            ColumnName = "CompanyCode",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_CompanyCode
                        }, new QueryItem {
                            ColumnName = "CompanyName",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_CompanyName
                        }, new CustomControlQueryItem {
                            ColumnName = "SparePartCode",
                            Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartCode,
                            CreateControl=type =>new Controls.MultipleTextQueryControl(type,"SparePartCode"),
                            IsExact =true
                        }, new QueryItem {
                            ColumnName = "SparePartName",
                            Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                           Title = CommonUIStrings.QueryPanel_Title_ProductStandard_CreateTime,
                            DefaultValue =new []{
                                new DateTime(DateTime.Now.Year,DateTime.Now.Month,1), DateTime.Now.Date.AddDays(1)
                            }
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "ModifyTime",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_ModifyTime
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            IsExact = false,
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_Status
                        }
                    }
                }
            };
        }
    }
}
