﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;


namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class BottomStockSettleDetailQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvType = new ObservableCollection<KeyValuePair>();
        public BottomStockSettleDetailQueryPanel() {
            this.kvType.Add(new KeyValuePair {
                Key = 1,
                Value = CommonUIStrings.DataEditView_Notification_Type_Agency
            });
            this.kvType.Add(new KeyValuePair {
                Key = 2,
                Value = CommonUIStrings.QueryPanel_Title_QueryItem_Dealer
            });
            this.kvType.Add(new KeyValuePair {
                Key = 3,
                Value = CommonUIStrings.DataEditView_Notification_Type_CompanyOrAgency
            });
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_BottomStockSettleDetailForReportQuery,
                    EntityType = typeof(BottomStockSettleTableReport),
                    QueryItems = new QueryItem[] {
                        new KeyValuesQueryItem {
                            Title = CommonUIStrings.QueryPanel_Title_CentralPartsSalesOrder_StatisticalUnit,
                            ColumnName = "Type",
                            KeyValueItems = this.kvType
                        }, new CustomQueryItem {
                           Title=CommonUIStrings.DataEditPanel_Text_MarketingDepartment_Names,
                            ColumnName = "DistributionCenterName",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title=CommonUIStrings.DataEditPanel_Text_Agency_Name,
                            ColumnName = "CenterName",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title=CommonUIStrings.Report_QueryPanel_Title_DealerNames,
                            ColumnName = "DealerName",
                            DataType = typeof(string)
                        }
                        , new CustomQueryItem {
                           Title=CommonUIStrings.Report_QueryPanel_Title_OverZero,
                            ColumnName = "OverZero",
                             DataType = typeof(bool)
                        }
                        }
                    }
                };
        }
    }
}