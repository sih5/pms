﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class BottomStockSettleForReserveQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvType = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();

        public BottomStockSettleForReserveQueryPanel() {
            this.kvType.Add(new KeyValuePair {
                Key = 1,
                Value = CommonUIStrings.DataEditPanel_Text_Company_Market
            });
            this.kvType.Add(new KeyValuePair {
                Key = 2,
                Value = CommonUIStrings.DataEditView_Notification_Type_Agency
            });
            this.kvType.Add(new KeyValuePair {
                Key = 3,
                Value = CommonUIStrings.QueryPanel_Title_QueryItem_Dealer
            });
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();

            dcsDomainContext.Load(dcsDomainContext.GetBottomStockForceReserveTypesQuery().Where(t=>t.Status==(int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp1 => {
                       if(loadOp1.HasError)
                           return;
                       foreach(var partsSalesCategory in loadOp1.Entities)
                           this.kvPartsSalesCategorys.Add(new KeyValuePair {
                               Key = partsSalesCategory.Id,
                               Value = partsSalesCategory.ReserveType
                           });                  
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.DataEditView_Notification_BottomTitle,
                    EntityType = typeof(BottomStockSettleTableReport),
                    QueryItems = new QueryItem[] {
                        new KeyValuesQueryItem {
                            Title = CommonUIStrings.QueryPanel_Title_CentralPartsSalesOrder_StatisticalUnit,
                            ColumnName = "Type",
                            KeyValueItems = this.kvType
                        }, new CustomQueryItem {
                           Title=CommonUIStrings.DataEditPanel_Text_MarketingDepartment_Names,
                            ColumnName = "DistributionCenterName",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title=CommonUIStrings.DataEditPanel_Text_Agency_Name,
                            ColumnName = "CenterName",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title=CommonUIStrings.Report_QueryPanel_Title_DealerNames,
                            ColumnName = "DealerName",
                            DataType = typeof(string)
                        }, new KeyValuesQueryItem {
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_BottomStockForceReserveType_ReserveType,
                            ColumnName = "ReserveTypeId",
                            KeyValueItems = this.kvPartsSalesCategorys
                        }, new CustomQueryItem {
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_ReserveTypeSubItem,
                            ColumnName = "ReserveTypeSubItem",
                            DataType = typeof(string)
                        }
                        }
                    }
                };
            }, null);
        }
    }
}