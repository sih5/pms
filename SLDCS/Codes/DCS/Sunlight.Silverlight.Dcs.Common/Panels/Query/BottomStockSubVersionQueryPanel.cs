﻿using System.Collections.ObjectModel;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class BottomStockSubVersionQueryPanel  : DcsQueryPanelBase {
        private readonly string[] kvNames = {
             "BaseData_Status"
        };
        private readonly ObservableCollection<KeyValuePair> kvCOmpanyTypes = new ObservableCollection<KeyValuePair>();

        public BottomStockSubVersionQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
            this.kvCOmpanyTypes.Add(new KeyValuePair {
                Key = (int)DcsCompanyType.代理库,
                Value = CommonUIStrings.DataEditView_Notification_Type_Agency
            });
            this.kvCOmpanyTypes.Add(new KeyValuePair {
                Key = (int)DcsCompanyType.服务站,
                Value = CommonUIStrings.QueryPanel_Title_QueryItem_Dealer
            });
            this.kvCOmpanyTypes.Add(new KeyValuePair {
                Key = (int)DcsCompanyType.服务站兼代理库,
                Value = CommonUIStrings.DataEditView_Notification_Type_CompanyOrAgency
            });
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.Report_Title_BottomStockSubVersion,
                    EntityType = typeof(BottomStockSubVersion),
                    QueryItems = new QueryItem[] {
                        new KeyValuesQueryItem {
                            ColumnName = "CompanyType",
                            KeyValueItems = this.kvCOmpanyTypes,
                            Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_CompanyType
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsBaseDataStatus.有效,
                            Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_Status
                        },new CustomQueryItem {
                              Title = CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_CompanyCode,
                              ColumnName = "CompanyCode",
                              DataType = typeof(string)
                        },new CustomQueryItem {
                              Title = CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_CompanyName,
                              ColumnName = "CompanyName",
                              DataType = typeof(string)
                        },new CustomQueryItem {
                              Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_SparePartCode,
                              ColumnName = "SparePartCode",
                              DataType = typeof(string)
                        },new CustomQueryItem {
                              Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_SparePartName,
                              ColumnName = "SparePartName",
                              DataType = typeof(string)
                        },new CustomQueryItem {
                              Title = CommonUIStrings.QueryPanel_QueryItem_Title_ForceReserveBill_SubVersionCode,
                              ColumnName = "SubVersionCode",
                              DataType = typeof(string)
                        },new CustomQueryItem {
                              Title = CommonUIStrings.QueryPanel_QueryItem_Title_ForceReserveBill_ColVersionCode,
                              ColumnName = "ColVersionCode",
                              DataType = typeof(string)
                        },new DateTimeRangeQueryItem {
                               Title=CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_ValidTo,
                              ColumnName = "StartTime",
                        },new CustomQueryItem {
                              Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_ReserveTypeSubItem,
                              ColumnName = "ReserveTypeSubItem",
                              DataType = typeof(string)
                        }
                    }
                }
            };
        }
    }
}
