﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class BranchQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
           "MasterData_Status"
        };

        public BranchQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_Branch,
                    EntityType = typeof(Branch),
                    QueryItems = new[] {
                        new QueryItem {
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_BranchForCurrent_Name,
                            ColumnName = "Name"
                        }, new QueryItem {
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_Branch_Code,
                            ColumnName = "Code"
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            IsExact = false,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsMasterDataStatus.有效
                        }
                    }
                }
            };
        }
    }
}
