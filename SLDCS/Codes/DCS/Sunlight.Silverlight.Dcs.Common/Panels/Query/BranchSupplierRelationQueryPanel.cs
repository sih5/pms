﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class BranchSupplierRelationQueryPanel : DcsQueryPanelBase {
        //private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategories = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = { 
            "BaseData_Status"
        };

        public BranchSupplierRelationQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            //var dcsDomainContext = new DcsDomainContext();
            //dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    foreach(var partsSalesCategor in loadOp.Entities)
            //        this.kvPartsSalesCategories.Add(new KeyValuePair {
            //            Key = partsSalesCategor.Id,
            //            Value = partsSalesCategor.Name
            //        });
            //}, null);

            this.QueryItemGroups = new[] { 
                new QueryItemGroup{
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_BranchSupplierRelation,
                    EntityType = typeof(BranchSupplierRelation),
                    QueryItems = new QueryItem[]{
                        new CustomQueryItem{
                            Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_PartsSupplierCode,
                            ColumnName = "PartsSupplier.Code",
                            DataType = typeof(string)
                        }, new CustomQueryItem{
                            Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_PartsSupplierName,
                            ColumnName = "PartsSupplier.Name",
                            DataType = typeof(string)
                        },new QueryItem {
                            ColumnName = "BusinessCode"
                        },new QueryItem {
                            ColumnName = "BusinessName"
                        }
                        //, new KeyValuesQueryItem {
                        //    ColumnName = "PartsSalesCategoryId",
                        //    KeyValueItems = this.kvPartsSalesCategories,
                        //    Title = CommonUIStrings.QueryPanel_QueryItem_Title_BranchSupplierRelation_PartsSalesCategoryName
                        //}
                        , new KeyValuesQueryItem{
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new DateTimeRangeQueryItem{
                            ColumnName = "CreateTime"
                        }
                    }
                }
            };
        }
    }
}
