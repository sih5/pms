﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System.Collections.ObjectModel;


namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class CenterApproveBillQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };
        private readonly ObservableCollection<KeyValuePair> kvWeeks = new ObservableCollection<KeyValuePair>();

        public CenterApproveBillQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);

            this.kvWeeks.Add(new KeyValuePair
            {
                Key = 1,
                Value = CommonUIStrings.QueryPanel_QueryItem_Title_CenterApproveBill_ApproveWeek_Monday
            });
            this.kvWeeks.Add(new KeyValuePair
            {
                Key = 2,
                Value = CommonUIStrings.QueryPanel_QueryItem_Title_CenterApproveBill_ApproveWeek_Tuesday
            });
            this.kvWeeks.Add(new KeyValuePair
            {
                Key = 3,
                Value = CommonUIStrings.QueryPanel_QueryItem_Title_CenterApproveBill_ApproveWeek_Wednesday
            });
            this.kvWeeks.Add(new KeyValuePair
            {
                Key = 4,
                Value = CommonUIStrings.QueryPanel_QueryItem_Title_CenterApproveBill_ApproveWeek_Thursday
            });
            this.kvWeeks.Add(new KeyValuePair
            {
                Key = 5,
                Value = CommonUIStrings.QueryPanel_QueryItem_Title_CenterApproveBill_ApproveWeek_Friday
            });
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_CenterApproveBill,
                    EntityType = typeof(CenterApproveBill),
                    QueryItems = new[] {
                        new QueryItem {
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_Code,
                            ColumnName = "CompanyCode"
                        },new QueryItem {
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_Name,
                            ColumnName = "CompanyName"
                        },
                        new KeyValuesQueryItem {
                            ColumnName = "ApproveWeek",
                            KeyValueItems = this.kvWeeks,
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_CenterApproveBill_ApproveWeek
                        },
                        new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsBaseDataStatus.有效
                        },
                        new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                           Title = CommonUIStrings.QueryPanel_Title_ProductStandard_CreateTime,
                        }
                    }
                }
            };
        }
    }
}
