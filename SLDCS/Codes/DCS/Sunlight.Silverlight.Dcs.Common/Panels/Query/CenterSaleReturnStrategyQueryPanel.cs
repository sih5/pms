﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class CenterSaleReturnStrategyQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "BaseData_Status","PartsSalesReturn_ReturnType"
        };

        public CenterSaleReturnStrategyQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_CenterSaleReturnStrategy,
                    EntityType = typeof(CenterSaleReturnStrategy),
                    QueryItems = new[] {
                        new KeyValuesQueryItem {
                            ColumnName = "ReturnType",
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_CenterSaleReturnStrategy_ReturnType,
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        },
                        new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsBaseDataStatus.有效
                        }
                    }
                }
            };
        }
    }
}
