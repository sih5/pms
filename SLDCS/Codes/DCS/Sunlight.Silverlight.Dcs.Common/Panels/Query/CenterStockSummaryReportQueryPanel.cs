﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class CenterStockSummaryReportQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "ABCSetting_Type"
        };
        public CenterStockSummaryReportQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(CenterStockSummary),
                    Title = CommonUIStrings.QueryPanel_Title_CenterStockSummary_Query,
                    QueryItems = new QueryItem[] {
                        new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title=CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_CreateTime,
                             DefaultValue =new []{
                                new DateTime(DateTime.Now.Year,DateTime.Now.Month,1), DateTime.Now.Date.AddDays(1)
                            }
                        },new CustomControlQueryItem {
                            ColumnName = "MarketingDepartmentName",
                            Title =CommonUIStrings.DataEditPanel_Text_Company_Market,
                            CreateControl=type =>new Controls.MultipleTextQueryControl(type,"MarketingDepartmentName")
                        }, new CustomControlQueryItem {
                            ColumnName = "CenterName",
                             Title =CommonUIStrings.DataEditPanel_Text_Agency_Name,
                            CreateControl=type =>new Controls.MultipleTextQueryControl(type,"CenterName")
                        },new CustomControlQueryItem {
                            ColumnName = "DealerName",
                            Title =CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_DealerName,
                            CreateControl=type =>new Controls.MultipleTextQueryControl(type,"DealerName")
                        }, new KeyValuesQueryItem {
                            ColumnName = "NewType",
                             Title =CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_NewType,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            AllowMultiSelect=true,
                        }                                                                  
                    }
                }
            };
        }
    }
}
