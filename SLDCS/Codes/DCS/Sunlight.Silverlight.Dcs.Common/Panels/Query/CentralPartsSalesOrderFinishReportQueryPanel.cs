﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class CentralPartsSalesOrderFinishReportQueryPanel  : DcsQueryPanelBase
    {
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesOrderTypes = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = new[] {
            "PartsSalesOrder_Status","PartsRetailOrder_Status"
        };
        public CentralPartsSalesOrderFinishReportQueryPanel()
        {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize()
        {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetWarehousesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.StorageCompanyType == (int)DcsCompanyType.代理库).OrderBy(t => t.Name), loadOp => {
                foreach (var entity in loadOp.Entities)
                {
                    this.kvWarehouses.Add(new KeyValuePair
                    {
                        Key = entity.Id,
                        Value = entity.Name
                    });
                }
                domainContext.Load(domainContext.GetPartsSalesOrderTypesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 ).OrderBy(t => t.Name), loadOp1 => {
                foreach (var entity in loadOp1.Entities)
                {
                    this.kvPartsSalesOrderTypes.Add(new KeyValuePair
                    {
                        Key = entity.Id,
                        Value = entity.Name
                    });
                }
                this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_PartsSalesOrderFinishReportFi,
                    EntityType = typeof(PartsSalesOrderFinishReport),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_MarketingDepartmentName,
                            ColumnName = "MarketingDepartmentName",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                             Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_SubmitCompanyName,
                            ColumnName = "SubmitCompanyName",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_PartsSalesOrderCode,
                            ColumnName = "Code",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartCode,
                            ColumnName = "SparePartCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                          Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                            ColumnName = "ReferenceCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_PartsOutboundBillCode,
                            ColumnName = "PartsOutboundBillCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_PartsPurchaseOrderCode,
                            ColumnName = "PartsPurchaseOrderCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_ContractCode,
                            ColumnName = "ContractCode",
                            DataType = typeof(string)
                        }, new KeyValuesQueryItem {
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_WarehouseId,
                            ColumnName = "WarehouseId",
                            KeyValueItems = this.kvWarehouses
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_SaleStatus,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_CreateTimeNew,
                        },new DateTimeRangeQueryItem {
                            ColumnName = "ApproveTime",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_ApproveTimeFir,
                        }
                        ,new DateTimeRangeQueryItem {
                            ColumnName = "ShippingDate",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_ShippingDate,
                        } ,new DateTimeRangeQueryItem {
                            ColumnName = "SubmitTime",
                            Title=CommonUIStrings.Report_QueryPanel_Title_SubmitTime,
                        }, new KeyValuesQueryItem {
                            ColumnName = "RetailStatus",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_RetailStatus,
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        }, new KeyValuesQueryItem {
                            ColumnName = "PartsSalesOrderTypeId",
                            Title="销售订单类型",
                            KeyValueItems = this.kvPartsSalesOrderTypes
                        }
                        }
                    }
                };
            }, null);
            }, null);
        }
    }
}