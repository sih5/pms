﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class CentralPartsSalesOrderForCenterReportQueryPanel : DcsQueryPanelBase {
        public CentralPartsSalesOrderForCenterReportQueryPanel() {
            this.Initializer.Register(this.Initialize);
           

        }
        private void Initialize() {

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_CentralPartsSalesOrderForDealer,
                    EntityType = typeof(PartsSalesOrderFinishReport),
                    QueryItems = new QueryItem[] {
                      new DateTimeRangeQueryItem {
                            ColumnName = "ApproveTime",
                            Title=CommonUIStrings.Report_Title_InOutSettleSummary_RecordTime,
                             DefaultValue=new[]{
                                DateTime.Now.AddDays(1 - DateTime.Now.Day).Date, DateTime.Now.Date
                            }
                        }, new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_CenterName,
                            ColumnName = "CenterName",
                            DataType = typeof(string)
                        }
                        }
                    }
                };
        }
    }
}