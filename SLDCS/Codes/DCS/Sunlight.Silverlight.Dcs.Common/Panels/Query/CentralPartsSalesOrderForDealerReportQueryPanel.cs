﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class CentralPartsSalesOrderForDealerReportQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvType = new ObservableCollection<KeyValuePair>();
        public CentralPartsSalesOrderForDealerReportQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.kvType.Add(new KeyValuePair {
                Key = 1,
                Value = CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_Center
            });
            this.kvType.Add(new KeyValuePair {
                Key = 2,
                Value = CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_Dealer
            });

        }
        private void Initialize() {

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.Report_Title_CentralPartsSalesOrderForDealerReportQueryPanel,
                    EntityType = typeof(PartsSalesOrderFinishReport),
                    QueryItems = new QueryItem[] {
                      new DateTimeRangeQueryItem {
                            ColumnName = "ApproveTime",
                            Title=CommonUIStrings.Report_Title_InOutSettleSummary_RecordTime,
                             DefaultValue=new[]{
                                DateTime.Now.AddDays(1 - DateTime.Now.Day).Date, DateTime.Now.Date
                            }
                        }, new KeyValuesQueryItem {
                           Title =CommonUIStrings.QueryPanel_Title_CentralPartsSalesOrder_StatisticalUnit,
                            ColumnName = "Type",
                            KeyValueItems = this.kvType
                        }, new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_CenterName,
                            ColumnName = "CenterName",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_DealerName,
                            ColumnName = "DealerName",
                            DataType = typeof(string)
                        }
                        }
                    }
                };
        }
    }
}