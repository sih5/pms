﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class ClaimApproverStrategyQueryPanel : DcsQueryPanelBase
    {
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();

        public ClaimApproverStrategyQueryPanel()
        {
            this.Initializer.Register(this.Initialize);
        }

        public void Initialize()
        {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetCompanyWithBranchIdsQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                    return;
                var company = loadOp.Entities.SingleOrDefault();
                if (company == null)
                    return;
                if (company.Type == (int)DcsCompanyType.代理库)
                {

                    dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp1 =>
                    {
                        if (loadOp1.HasError)
                            return;
                        foreach (var partsSalesCategory in loadOp1.Entities)
                            this.kvPartsSalesCategorys.Add(new KeyValuePair
                            {
                                Key = partsSalesCategory.Id,
                                Value = partsSalesCategory.Name
                            });
                    }, null);
                }
                else
                {
                    dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesWithBranchIdQuery(), LoadBehavior.RefreshCurrent, loadOp1 =>
                    {
                        if (loadOp1.HasError)
                            return;
                        foreach (var partsSalesCategory in loadOp1.Entities)
                            this.kvPartsSalesCategorys.Add(new KeyValuePair
                            {
                                Key = partsSalesCategory.Id,
                                Value = partsSalesCategory.Name
                            });
                    }, null);
                }
            }, null);

                this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_ClaimApproverStrategy,
                    EntityType = typeof(ClaimApproverStrategy),
                    QueryItems = new[] {
                        new QueryItem {
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_ClaimApproverStrategy_FeeFrom,
                            ColumnName = "FeeFrom"
                        },new QueryItem {
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_ClaimApproverStrategy_FeeTo,
                            ColumnName = "FeeTo"
                        },
                        new KeyValuesQueryItem {
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name,
                            ColumnName = "PartsSalesCategoryId",
                            KeyValueItems = this.kvPartsSalesCategorys
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title = CommonUIStrings.QueryPanel_Title_ProductStandard_CreateTime,
                            DefaultValue = new[] {
                                DateTime.Now.AddMonths(-1), DateTime.Now
                            }
                        },new DateTimeRangeQueryItem {
                            ColumnName = "MofidyTime",
                             Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_ModifyTime
                        }
                    }
                }
            };
        }
    }
}
