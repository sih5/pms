﻿
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class ClaimSupplierDetailsSplitQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvProductLine = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
            "RepairClaimBill_RepairClaimStatus","Repair_Tpye"
        };
        public ClaimSupplierDetailsSplitQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            Initializer.Register(Initialize);

        }
        private ObservableCollection<KeyValuePair> kvProductLineType = new ObservableCollection<KeyValuePair>();

        private void Initialize() {
            this.kvProductLineType.Clear();
            this.kvProductLineType.Add(new KeyValuePair {
                Key = 1,
                Value = "金刚"
            });
            this.kvProductLineType.Add(new KeyValuePair {
                Key = 2,
                Value = "瑞沃"
            });
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetKeyValueItemsQuery().Where(r => r.Name == "BranchList"), LoadBehavior.RefreshCurrent, loadOpKey => {
                if(loadOpKey.HasError) {
                    if(!loadOpKey.IsErrorHandled)
                        loadOpKey.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOpKey);
                }
                this.kvBranches.Clear();
                foreach(var branch in loadOpKey.Entities)
                    this.kvBranches.Add(new KeyValuePair {
                        Key = branch.Key,
                        Value = branch.Value
                    });
                dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效 && v.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp2 => {
                    if(loadOp2.HasError)
                        return;
                    this.kvPartsSalesCategoryName.Clear();
                    foreach(var partsSalesCategory in loadOp2.Entities) {
                        this.kvPartsSalesCategoryName.Add(new KeyValuePair {
                            Key = partsSalesCategory.Id,
                            Value = partsSalesCategory.Name
                        });
                    }
                    var kvPcn = this.kvPartsSalesCategoryName.Select(e => e.Key).ToArray();
                    //dcsDomainContext.Load(dcsDomainContext.GetServiceProductLinesQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp3 => {
                    //    if(loadOp3.HasError)
                    //        return;
                    //    this.kvProductLine.Clear();
                    //    foreach(var productLine in loadOp3.Entities) {
                    //        if(kvPcn.Contains(productLine.PartsSalesCategoryId))
                    //            this.kvProductLine.Add(new KeyValuePair {
                    //                Key = productLine.Id,
                    //                Value = productLine.Name
                    //            }
                    //            );
                    //    }
                    //}, null);
                }, null);
            }, null);

            dcsDomainContext.Load(dcsDomainContext.GetCompaniesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var company = loadOp.Entities.SingleOrDefault();
                QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = "供应商索赔结算(拆分结算)",
                        EntityType = typeof(PartsStock),
                        QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                                Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_BranchName,
                                ColumnName = "BranchCode",
                                DataType = typeof(string),
                                DefaultValue = company != null && company.Type == (int)DcsCompanyType.分公司 ?int.Parse( BaseApp.Current.CurrentUserData.EnterpriseCode ): -1,
                                IsEnabled = company != null && company.Type == (int)DcsCompanyType.集团企业
                            },
                             new KeyValuesQueryItem {
                                Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name,
                                ColumnName = "CategoryID",
                                KeyValueItems = kvPartsSalesCategoryName,
                                DefaultValue = kvPartsSalesCategoryName.FirstOrDefault()
                            },
                            new CustomQueryItem {
                                Title = CommonUIStrings.QueryPanel_Title_DealerProduct_Market,
                                ColumnName = "MarketName",
                                DataType = typeof(string)
                            },
                            new CustomQueryItem {
                                Title = "VIN",
                                ColumnName = "VIN",
                                DataType = typeof(string)
                            },
                            new CustomQueryItem {
                                Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierCode,
                                ColumnName = "SupplierCode",
                                DataType = typeof(string)
                            },
                              new CustomQueryItem {
                                Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierName,
                                ColumnName = "SupplierName",
                                DataType = typeof(string)
                            },
                            new KeyValuesQueryItem {
                                Title = "维修类型",
                                ColumnName = "RepairType",
                                KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                            },
                            new KeyValuesQueryItem {
                                Title = "产品线",
                                ColumnName = "ProductLineId",
                                KeyValueItems = kvProductLine
                            }, new KeyValuesQueryItem {
                                Title = "产品线分类",
                                ColumnName = "ProductLineType",
                                KeyValueItems = this.kvProductLineType
                            },
                             new KeyValuesQueryItem {
                                Title = CommonUIStrings.QueryPanel_QueryItem_Title_Common_Status,
                                ColumnName = "Status",
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                            },
                           new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                    DateTime.Now.AddMonths(-1), DateTime.Now
                                }
                        },
                            new DateTimeRangeQueryItem {
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_ApproveTimeNew,
                            ColumnName = "ApproveTime"
                        },
                         new DateTimeRangeQueryItem {
                            Title = "结算时间",
                            ColumnName = "CacuTime"
                        }
                        }

                    }
                };
            }, null);
        }
    }
}
