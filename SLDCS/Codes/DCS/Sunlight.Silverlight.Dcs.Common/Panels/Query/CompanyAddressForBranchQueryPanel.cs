﻿
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class CompanyAddressForBranchQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        public CompanyAddressForBranchQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_CompanyAddress,
                    EntityType = typeof(CompanyAddress),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                          ColumnName  = "Company.Code",
                          DataType = typeof(string),
                         Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_CompanyCode
                        },new CustomQueryItem {
                          ColumnName  = "Company.Name",
                          DataType = typeof(string),
                         Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_CompanyName
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsBaseDataStatus.有效
                        }
                    }
                }
            };
        }
    }
}
