﻿
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using System.Collections.ObjectModel;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query {
    public class CompanyForApproveBillQueryPanel : DcsQueryPanelBase {


        private readonly string[] kvNames = new[] {
           "Company_Type"
        };
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();
        public CompanyForApproveBillQueryPanel() {

            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_Company,
                    EntityType = typeof(Company),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_CompanyCode
                        },new QueryItem {
                            ColumnName = "Name",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_CompanyName
                        }, new KeyValuesQueryItem {
                            ColumnName = "Type",
                            IsExact = false,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_CompanyType
                        }
                    }
                }
            };
        }

    }
}
