﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class CompanyInvoiceInfoQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
           "MasterData_Status","Invoice_Type"
        };

        public CompanyInvoiceInfoQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_CompanyInvoiceInfo,
                    EntityType = typeof(CompanyInvoiceInfo),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "CompanyName"
                        }, new QueryItem {
                            ColumnName = "CompanyCode"
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            IsExact = false,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsMasterDataStatus.有效,
                            IsEnabled = false
                        }, new KeyValuesQueryItem {
                            ColumnName = "InvoiceType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        }
                    }
                }
            };
        }
    }
}
