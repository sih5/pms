﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class CompanyQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "MasterData_Status"
        };

        public CompanyQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_Company,
                    EntityType = typeof(Company),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Name"
                        }, new QueryItem {
                            ColumnName = "Code"
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            IsExact = false,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }
                    }
                }
            };
        }
    }
}
