﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System.Collections.ObjectModel;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class CompanyWithTypeQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "MasterData_Status","Company_Type"
        };

        private readonly ObservableCollection<KeyValuePair> kvCompanyTypes = new ObservableCollection<KeyValuePair>();
        public CompanyWithTypeQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            this.KeyValueManager.LoadData(() => {
                foreach(var keyValue in this.KeyValueManager[this.kvNames[1]].Where(entity => entity.Key == (int)DcsCompanyType.代理库 || entity.Key == (int)DcsCompanyType.服务站 || entity.Key == (int)DcsCompanyType.服务站兼代理库)) {
                    kvCompanyTypes.Add(keyValue);
                }
            });

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_Company,
                    EntityType = typeof(Company),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Name"
                        }, new QueryItem {
                            ColumnName = "Code"
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            IsExact = false,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new KeyValuesQueryItem {
                            ColumnName = "Type",
                            KeyValueItems = kvCompanyTypes,
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_CompanyType
                        }
                    }
                }
            };
        }
    }
}
