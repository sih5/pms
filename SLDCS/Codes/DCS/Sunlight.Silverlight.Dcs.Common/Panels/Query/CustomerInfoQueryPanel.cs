﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class CustomerInfoQueryPanel : DcsQueryPanelBase {
       
        public CustomerInfoQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_Company,
                    EntityType = typeof(Company),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_CompanyCode
                        }, new QueryItem {
                            ColumnName = "Name",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_CompanyName
                        }
                    }
                }
            };
        }
    }
}
