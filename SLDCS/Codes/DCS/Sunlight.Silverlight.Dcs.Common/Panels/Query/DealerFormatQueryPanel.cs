﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class DealerFormatQueryPanel  : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "MasterData_Status"
        };

        public DealerFormatQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title=CommonUIStrings.DataEditView_Title_DealerFormateQuery,
                    EntityType = typeof(DealerFormat),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "DealerCode",
                            Title = CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_DealerCode,
                        },new QueryItem {
                            ColumnName = "DealerName",
                            Title = CommonUIStrings.Report_QueryPanel_Title_DealerNames,
                        },new QueryItem {
                            ColumnName = "Format",
                            Title = CommonUIStrings.Report_QueryPanel_Title_Format,
                        },new QueryItem {
                            ColumnName = "Quarter",
                            Title = CommonUIStrings.Report_QueryPanel_Title_Quarter,
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsMasterDataStatus.有效
                        },new DateTimeRangeQueryItem {
                               Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_CreateTime,
                              ColumnName = "CreateTime",
                        }
                    }
                }
            };
        }
    }
}
