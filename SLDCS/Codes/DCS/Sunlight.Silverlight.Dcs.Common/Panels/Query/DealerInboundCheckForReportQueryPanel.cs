﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class DealerInboundCheckForReportQueryPanel : DcsQueryPanelBase {
     
        public DealerInboundCheckForReportQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }
          private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_PartsInboundCheckBillForReport,
                    EntityType = typeof(PartsInboundCheckBillForReport),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                            ColumnName = "Code",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_Code,
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_InboundType,
                            ColumnName = "InboundType",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_MarketingDepartmentName,
                            ColumnName = "MarketingDepartmentName",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                          Title = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_Name,
                            ColumnName = "StorageCompanyName",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                          Title = CommonUIStrings.QueryPanel_QueryItem_Title_AuthenticationRepair_DealerName,
                            ColumnName = "ReceivingCompanyName",
                            DataType = typeof(string)
                        },new DateTimeRangeQueryItem {
                            ColumnName = "ConfirmedReceptionTime",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_CreateTime,
                        }
                    }
                }
            };
        }
    }
}
