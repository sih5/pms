﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class DealerPartsOutboundBillReportQueryPanel : DcsQueryPanelBase {
        public DealerPartsOutboundBillReportQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }
          private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_DealerPartsOutboundBill,
                    EntityType = typeof(DealerPartsOutboundBill),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                            ColumnName = "MarketingDepartmentName",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_MarketingDepartmentName,
                             DataType = typeof(string)
                        },new CustomQueryItem {
                            ColumnName = "CenterName",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_Agency_Name,
                             DataType = typeof(string)
                        },new CustomQueryItem {
                            ColumnName = "Code",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_Code,
                             DataType = typeof(string)
                        }, new CustomQueryItem {
                            ColumnName = "OutInBandtype",
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_InboundType,
                             DataType = typeof(string)
                        }, new CustomQueryItem {
                          Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartCode,
                            ColumnName = "PartsCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                            Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                            ColumnName = "PartsName",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title = CommonUIStrings.QueryPanel_QueryItem_Title_AuthenticationRepair_DealerName,
                            ColumnName = "DealerName",
                            DataType = typeof(string)
                        },new DateTimeRangeQueryItem {
                            ColumnName = "OutBoundTime",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_PartsOutboundBillTime,
                        }
                    }
                }
            };
        }
    }
}
