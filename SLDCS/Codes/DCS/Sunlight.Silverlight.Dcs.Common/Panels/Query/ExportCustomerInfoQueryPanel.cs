﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class ExportCustomerInfoQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
           "BaseData_Status","ExportCustPriceType"
        };

        public ExportCustomerInfoQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_ExportCustomerInfo,
                    EntityType = typeof(ExportCustomerInfo),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "CustomerCode"
                        },new QueryItem {
                            ColumnName = "CustomerName"
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            IsExact = false,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsBaseDataStatus.有效
                        },new KeyValuesQueryItem {
                            ColumnName = "PriceType",
                            IsExact = false,
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        }
                    }
                }
            };
        }
    }
}
