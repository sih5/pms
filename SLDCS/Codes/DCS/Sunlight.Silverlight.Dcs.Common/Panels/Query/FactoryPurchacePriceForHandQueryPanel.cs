﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Web.Entities;
namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class FactoryPurchacePriceForHandQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvType = new ObservableCollection<KeyValuePair>();

        public FactoryPurchacePriceForHandQueryPanel() {
            this.kvType.Add(new KeyValuePair {
                Key = 1,
                Value = CommonUIStrings.Action_Title_Add
            });
            this.kvType.Add(new KeyValuePair {
                Key = 2,
                Value = CommonUIStrings.Action_Title_Price_Maintain
            });
            this.kvType.Add(new KeyValuePair {
                Key = 3,
                Value = CommonUIStrings.Action_Title_Price_Temporary
            });
            this.kvType.Add(new KeyValuePair {
                Key = 4,
                Value = CommonUIStrings.Action_Title_Price_Lower
            });           
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_Price,
                    EntityType = typeof(FactoryPurchacePriceForHand),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "SupplierCode",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierCode,
                        },new QueryItem {
                            ColumnName = "SupplierName",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierName,
                        }
                        ,new CustomControlQueryItem { 
                            ColumnName = "SparePartCode", 
                            Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartCode, 
                            CreateControl=type =>new Controls.MultipleTextQueryControl(type,"SparePartCode") 
                        }
                        ,new QueryItem {
                            Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                            ColumnName = "SparePartName"
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            Title=CommonUIStrings.QueryPane_Title_Price_Type,
                            KeyValueItems = this.kvType
                        } ,new QueryItem {
                            Title="是否可采购",
                            ColumnName = "IsOrderable"
                        }
                    }
                }
            };
        }
    }
}
