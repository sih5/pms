﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class FactoryPurchacePriceQueryPanel : DcsQueryPanelBase {
        public FactoryPurchacePriceQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_VirtualFactoryPurchacePrice,
                    EntityType = typeof(VirtualFactoryPurchacePrice),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "SupplierCode",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierCode,
                        },new QueryItem {
                            ColumnName = "SupplierName",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierName,
                        }
                        ,new CustomControlQueryItem { 
                            ColumnName = "SparePartCode", 
                            Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartCode, 
                            CreateControl=type =>new Controls.MultipleTextQueryControl(type,"SparePartCode") 
                        }
                        ,new QueryItem {
                            Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                            ColumnName = "SparePartName"
                        },new QueryItem {
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                            ColumnName = "HYCode"
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_VirtualFactoryPurchacePrice_CreateTime,
                            DefaultValue =new []{
                                new DateTime(DateTime.Now.Year,DateTime.Now.Month,1), DateTime.Now.Date.AddDays(1)
                            }
                        },new CustomQueryItem {
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_VirtualFactoryPurchacePrice_IsGenerate,
                            ColumnName = "IsGenerate",
                            DataType = typeof(bool)
                        }
                    }
                }
            };
        }
    }
}
