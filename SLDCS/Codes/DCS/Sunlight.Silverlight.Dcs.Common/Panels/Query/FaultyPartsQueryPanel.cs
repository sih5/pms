﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class FaultyPartsQueryPanel : DcsQueryPanelBase {
        public FaultyPartsQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }


        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_FaultyParts,
                    EntityType = typeof(SparePart),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                        },new QueryItem {
                            ColumnName = "Name",
                        },new QueryItem {
                            Title = CommonUIStrings.QueryPanel_Title_FaultyParts_SupplierCode,
                            ColumnName = "SupplierCode",
                        },new QueryItem {
                            Title = CommonUIStrings.QueryPanel_Title_FaultyParts_SupplierName,
                            ColumnName = "SupplierName",
                        },new QueryItem {
                            ColumnName = "ReferenceCode",
                        },new QueryItem {
                            ColumnName = "ReferenceName",
                        }

                    }
                }
            };
        }
    }
}
