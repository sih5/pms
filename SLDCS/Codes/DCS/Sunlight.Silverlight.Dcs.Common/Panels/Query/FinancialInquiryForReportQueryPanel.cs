﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Web.Entities;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class FinancialInquiryForReportQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();

        public FinancialInquiryForReportQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetWarehousesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.StorageCompanyType == (int)DcsCompanyType.分公司).OrderBy(t => t.Name), loadOp => {
                foreach(var entity in loadOp.Entities) {
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name
                    });
                }
                this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title =CommonUIStrings.QueryPanel_Title_FinancialInquiryReport,
                    EntityType = typeof(FinancialInquiryReport),
                    QueryItems = new QueryItem[] {
                         new KeyValuesQueryItem {
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_WarehouseId,
                            ColumnName = "WarehouseId",
                            KeyValueItems = this.kvWarehouses
                         }                       
                        }
                    }
                };
            }, null);
        }
    }
}