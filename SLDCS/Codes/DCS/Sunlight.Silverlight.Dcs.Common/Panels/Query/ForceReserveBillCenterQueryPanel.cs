﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class ForceReserveBillCenterQueryPanel : DcsQueryPanelBase {
       
        private readonly ObservableCollection<KeyValuePair> kvStatus = new ObservableCollection<KeyValuePair>();

        public ForceReserveBillCenterQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetKeyValueItemsQuery().Where(r => r.Name == "ForceReserveBillStatus" && r.Key != (int)DCSForceReserveBillStatus.已确认), LoadBehavior.RefreshCurrent, loadOp1 => {
                if(loadOp1.HasError)
                    return;
                foreach(var partsSalesCategory in loadOp1.Entities)
                    this.kvStatus.Add(new KeyValuePair {
                        Key = partsSalesCategory.Key,
                        Value = partsSalesCategory.Value
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title =CommonUIStrings.QueryPanel_Title_ForceReserveBillCenter,
                    EntityType = typeof(ForceReserveBill),
                    QueryItems = new QueryItem[] {
                         new KeyValuesQueryItem {
                            ColumnName = "Status",
                            IsExact = false,
                            KeyValueItems = this.kvStatus,
                            Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_Status
                        },new CustomQueryItem {
                              Title = CommonUIStrings.QueryPanel_QueryItem_Title_ForceReserveBill_SubVersionCode,
                              ColumnName = "SubVersionCode",
                              DataType = typeof(string)
                        },new CustomQueryItem {
                              Title = CommonUIStrings.QueryPanel_QueryItem_Title_ForceReserveBill_ColVersionCode,
                              ColumnName = "ColVersionCode",
                              DataType = typeof(string)
                        },new CustomQueryItem {
                              Title = CommonUIStrings.QueryPanel_QueryItem_Title_BottomStockForceReserveType_ReserveType,
                              ColumnName = "ReserveType",
                              DataType = typeof(string)
                        },new CustomQueryItem {
                              Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_ReserveTypeSubItem,
                              ColumnName = "ReserveTypeSubItem",
                              DataType = typeof(string)
                        },new CustomQueryItem {
                              Title = CommonUIStrings.DataDeTailPanel_Text_Company_Code,
                              ColumnName = "CompanyCode",
                              DataType = typeof(string)
                        },new CustomQueryItem {
                              Title = CommonUIStrings.DataDeTailPanel_Text_Company_Name,
                              ColumnName = "CompanyName",
                              DataType = typeof(string)
                        },new DateTimeRangeQueryItem {
                               Title=CommonUIStrings.QueryPanel_Title_ProductStandard_CreateTime,
                             ColumnName = "CreateTime",
                        },new DateTimeRangeQueryItem {
                               Title="审批时间",
                              ColumnName = "ApproveTime",
                        }
                    }
                }
            };
        }
    }
}
