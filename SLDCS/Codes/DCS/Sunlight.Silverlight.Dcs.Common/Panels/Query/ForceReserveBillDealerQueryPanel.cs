﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;


namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class ForceReserveBillDealerQueryPanel  : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "ForceReserveBillStatus"
        };

        public ForceReserveBillDealerQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {         
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title =CommonUIStrings.QueryPanel_Title_ForceReserveBillCenter,
                    EntityType = typeof(ForceReserveBill),
                    QueryItems = new QueryItem[] {
                         new KeyValuesQueryItem {
                            ColumnName = "Status",
                            IsExact = false,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_Status
                        },new CustomQueryItem {
                              Title = CommonUIStrings.DataEditPanel_Text_Agency_Name,
                              ColumnName = "CenterName",
                              DataType = typeof(string)
                        },new CustomQueryItem {
                              Title = CommonUIStrings.QueryPanel_QueryItem_Title_ForceReserveBill_SubVersionCode,
                              ColumnName = "SubVersionCode",
                              DataType = typeof(string)
                        },new CustomQueryItem {
                              Title = CommonUIStrings.QueryPanel_QueryItem_Title_ForceReserveBill_ColVersionCode,
                              ColumnName = "ColVersionCode",
                              DataType = typeof(string)
                        },new CustomQueryItem {
                              Title = CommonUIStrings.QueryPanel_QueryItem_Title_BottomStockForceReserveType_ReserveType,
                              ColumnName = "ReserveType",
                              DataType = typeof(string)
                        },new CustomQueryItem {
                              Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_ReserveTypeSubItem,
                              ColumnName = "ReserveTypeSubItem",
                              DataType = typeof(string)
                        },new CustomQueryItem {
                              Title = CommonUIStrings.DataDeTailPanel_Text_Company_Code,
                              ColumnName = "CompanyCode",
                              DataType = typeof(string)
                        },new CustomQueryItem {
                              Title = CommonUIStrings.DataDeTailPanel_Text_Company_Name,
                              ColumnName = "CompanyName",
                              DataType = typeof(string)
                        },new DateTimeRangeQueryItem {
                               Title=CommonUIStrings.QueryPanel_Title_ProductStandard_CreateTime,
                             ColumnName = "CreateTime",
                        },new DateTimeRangeQueryItem {
                               Title="审批时间",
                              ColumnName = "ApproveTime",
                        }
                    }
                }
            };
        }
    }
}
