﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class GoldenTaxClassifyMsgQueryPanel : DcsQueryPanelBase {
        public GoldenTaxClassifyMsgQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(GoldenTaxClassifyMsg),
                    Title = CommonUIStrings.QueryPanel_Title_GoldenTaxClassifyMsg,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "GoldenTaxClassifyCode",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_GoldenTaxClassifyMsg_GoldenTaxClassifyCode,
                        }, new QueryItem {
                            ColumnName = "GoldenTaxClassifyName",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_GoldenTaxClassifyMsg_GoldenTaxClassifyName
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title = CommonUIStrings.QueryPanel_Title_ProductStandard_CreateTime,
                        },new DateTimeRangeQueryItem {
                            ColumnName = "ModifyTime",
                             Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_ModifyTime
                        }
                    }
                }
            };
        }
    }
}
