﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class InOutSettleSummaryReportQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();

        public InOutSettleSummaryReportQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetWarehousesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && (e.Name == CommonUIStrings.Report_Title_InOutSettleSummary_Warehouse_Shuangqiao || e.Name == CommonUIStrings.Report_Title_InOutSettleSummary_Warehouse_Exit)).OrderBy(t => t.Name), loadOp => {
                foreach(var entity in loadOp.Entities) {
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name
                    });
                }
                this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.Report_Title_InOutSettleSummaryReport_Query,
                    EntityType = typeof(InOutSettleSummaryQuery),
                    QueryItems = new QueryItem[] {
                        new KeyValuesQueryItem {
                            Title = CommonUIStrings.Report_Title_InOutSettleSummary_Warehouse,
                            ColumnName = "WarehouseId",
                            KeyValueItems = this.kvWarehouses
                        },new DateTimeRangeQueryItem {
                            ColumnName = "RecordTime",
                            Title=CommonUIStrings.Report_Title_InOutSettleSummary_RecordTime,
                             DefaultValue = new [] {
                                    new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                                }
                        }
                        }
                    }
                };
            }, null);
        }
    }
}