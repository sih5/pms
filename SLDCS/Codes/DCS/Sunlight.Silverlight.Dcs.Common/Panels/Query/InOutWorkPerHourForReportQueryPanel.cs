﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class InOutWorkPerHourForReportQueryPanel : DcsQueryPanelBase {

        public InOutWorkPerHourForReportQueryPanel() {
            Initializer.Register(Initialize);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                      new QueryItemGroup {
                           UniqueId = "Common",
                           Title = CommonUIStrings.QueryPanel_Title_InOutWorkPerHourQuery,
                           EntityType = typeof(InOutWorkPerHour),
                           QueryItems =   new QueryItem[]{
                            new DateTimeQueryItem{
                                   Title=CommonUIStrings.Report_Title_InOutSettleSummary_RecordTime,
                                   ColumnName="CreateTime",
                                   DefaultValue=DateTime.Now.Date.AddDays(-1)
                             }
                           }
                      }
               };
        }
    }
}
