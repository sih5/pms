﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class LogisticCompanyServiceRangeQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
            "MasterData_Status", "LogisticCompany_BusinessDomain","Storage_Center"
        };

        public LogisticCompanyServiceRangeQueryPanel() {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_LogisticCompany,
                    EntityType = typeof(LogisticCompanyServiceRange),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_LogisticCompany_Name,
                            ColumnName = "LogisticCompany.Name",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_LogisticCompany_Code,
                            ColumnName = "LogisticCompany.Code",
                            DataType = typeof(string)
                        }, new KeyValuesQueryItem {
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_Common_Status,
                            ColumnName = "LogisticCompany.Status",
                            KeyValueItems = this.KeyValueManager[kvNames[0]],
                            DefaultValue = (int)DcsMasterDataStatus.有效,
                            IsEnabled = false
                        }, new KeyValuesQueryItem {
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_LogisticCompanyServiceRange_BusinessDomain,
                            ColumnName = "BusinessDomain",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        }, new KeyValuesQueryItem {
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_LogisticCompany_StorageCenter,
                            ColumnName = "LogisticCompany.StorageCenter",
                            KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                        }
                    }
                }
            };
        }
    }
}
