﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class MarketDptPersonnelRelationQueryPanel : DcsQueryPanelBase {

        private readonly string[] kvNames = {
           "BaseData_Status","DepartmentPersonType"
        };

        public MarketDptPersonnelRelationQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }


        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_MarketDptPersonnelRelation,
                    EntityType = typeof(MarketDptPersonnelRelation),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem  {
                            ColumnName= "MarketingDepartment.Code",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_MarketDptPersonnelRelation_Code,
                            DataType=typeof(string)
                        }, new CustomQueryItem  {
                            ColumnName = "MarketingDepartment.Name",
                             Title = CommonUIStrings.QueryPanel_QueryItem_Title_MarketDptPersonnelRelation_Name,
                            DataType=typeof(string)
                        },  new CustomQueryItem {
                            ColumnName = "Personnel.Name",
                            Title = CommonUIStrings.DataGridView_ColumnItem_Title_MarketDptPersonnelRelation_PersonnelId,
                            DataType=typeof(string)
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsBaseDataStatus.有效
                        } ,new KeyValuesQueryItem {
                            ColumnName = "Type",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_MarketDptPersonnelRelation_Type
                        }
                    }
                }
            };
        }
    }
}
