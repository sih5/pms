﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;


namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class MarketingDepartmentForCAMQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        public MarketingDepartmentForCAMQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_MarketingDepartmentForCAM,//销售区域管理同样使用MarketingDepartmentBO
                    EntityType = typeof(MarketingDepartment),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_MarketingDepartment_Code,
                        }, new QueryItem {
                            ColumnName = "Name",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_MarketingDepartment_Name,
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsBaseDataStatus.有效
                        }
                    }
                }
            };
        }
    }
}
