using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;


namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class MarketingDepartmentQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = {
            "MarketingDepartment_Type", "BaseData_Status"
        };

        public MarketingDepartmentQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var branch in loadOp.Entities)
                    this.kvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name
                    });
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var PartsSalesCategory in loadOp.Entities)
                    this.kvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = PartsSalesCategory.Id,
                        Value = PartsSalesCategory.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_MarketingDepartment,
                    EntityType = typeof(MarketingDepartment),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_MarketDptPersonnelRelation_Code
                        }, new QueryItem {
                            ColumnName = "Name",
                             Title = CommonUIStrings.QueryPanel_QueryItem_Title_MarketDptPersonnelRelation_Name
                        }, new QueryItem {
                            ColumnName = "ProvinceName",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_ProvinceName
                        }, new QueryItem {
                            ColumnName = "CityName",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_CityName
                        }, new QueryItem {
                            ColumnName = "CountyName",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_CountyName
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            DefaultValue = (int)DcsBaseDataStatus.有效
                        }, new KeyValuesQueryItem{
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name,
                            ColumnName="PartsSalesCategoryId",
                            KeyValueItems = this.kvPartsSalesCategorys
                        }
                    }
                }
            };
        }
    }
}
