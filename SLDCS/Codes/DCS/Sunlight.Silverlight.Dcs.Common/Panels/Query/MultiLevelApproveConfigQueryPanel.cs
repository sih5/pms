﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System.Collections.ObjectModel;


namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class MultiLevelApproveConfigQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "BaseData_Status","MultiLevelApproveConfigType"
        };

        public MultiLevelApproveConfigQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_MultiLevelApproveConfig_Title_Query,
                    EntityType = typeof(MultiLevelApproveConfig),
                    QueryItems = new[] {
                        new KeyValuesQueryItem {
                            ColumnName = "Type",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_MarketDptPersonnelRelation_Type
                        },
                        new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsBaseDataStatus.有效
                        },
                        new QueryItem {
                            ColumnName = "ApproverName",
                            Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_CheckerName
                        }
                    }
                }
            };
        }
    }
}
