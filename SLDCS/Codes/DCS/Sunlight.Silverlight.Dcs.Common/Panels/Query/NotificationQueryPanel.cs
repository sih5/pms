﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Channels.Panels.Query {
    public class NotificationQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "Notification_Type", "Company_Type","BaseData_Status","NotificationUrgentLevel"
        };
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(Notification),
                    Title = CommonUIStrings.QueryPanel_Title_NotificationQuery,
                    QueryItems = new[] {
                        new QueryItem {
                          ColumnName = "Code"  
                        },new KeyValuesQueryItem {
                            ColumnName = "Type",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            IsExact = false
                        },new QueryItem {
                          ColumnName = "Title",
                        },new DateTimeRangeQueryItem{ 
                               ColumnName = "CreateTime",
                               Title=CommonUIStrings.QueryPanel_QueryItem_Title_Notification_CreateTime,
                               IsExact = false,
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                            }
                        },new KeyValuesQueryItem {
                            ColumnName = "CompanyType",
                            Title=CommonUIStrings.QueryPanel_Title_CompanyType,
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            IsExact = false
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[2]],
                            DefaultValue = (int)DcsBaseDataStatus.有效,
                            IsExact = false
                        },new KeyValuesQueryItem{
                            ColumnName = "UrgentLevel",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_Notification_UrgentLevel,
                            //DefaultValue=(int)DcsNotificationUrgentLevel.普通,
                            KeyValueItems = this.KeyValueManager[this.kvNames[3]],
                            IsExact = false
                        },new QueryItem{
                            ColumnName = "PublishDep",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_Notification_PublishDep
                        },new QueryItem {
                            ColumnName = "CreatorName",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_Notification_CreatorName
                        },new CustomQueryItem {
                            ColumnName = "IsHasFile",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_Notification_IsHasFile,
                            DataType = typeof(bool)
                        }
                    }
                }
            };
        }

        public NotificationQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
