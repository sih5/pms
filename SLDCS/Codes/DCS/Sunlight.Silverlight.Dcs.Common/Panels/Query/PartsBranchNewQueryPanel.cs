﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsBranchNewQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "ABCStrategy_Category", "MasterData_Status","PartsBranch_ProductLifeCycle","PartsWarrantyTerm_ReturnPolicy","PlanPriceCategory_PriceCategory"
        };
        //private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategory = new ObservableCollection<KeyValuePair>();

        public PartsBranchNewQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            //var dcsDomainContext = new DcsDomainContext();
            //dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    foreach(var branch in loadOp.Entities)
            //        this.kvPartsSalesCategory.Add(new KeyValuePair {
            //            Key = branch.Id,
            //            Value = branch.Name
            //        });
            //}, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_PartsBranch,
                    EntityType = typeof(VirtualPartsBranch),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "PartCode",
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        }, new QueryItem {
                            ColumnName = "PartName",
                            Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                        },new CustomQueryItem {
                            ColumnName = "ReferenceCode",
                            DataType =typeof(string),
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                        }  , new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            DefaultValue = (int)DcsBaseDataStatus.有效,
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_Common_Status
                        }                     
                        , new CustomQueryItem {
                            Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_IRGroupCode,
                            ColumnName = "GroupCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                            Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_IRGroupName,
                            ColumnName = "GroupName",
                            DataType = typeof(string)
                        }
                        //, new KeyValuesQueryItem {
                        //    Title = CommonUIStrings.QueryPanel_QueryItem_Title_Branch_PartsSalesCategory,
                        //    ColumnName = "PartsSalesCategoryId",
                        //    KeyValueItems = this.kvPartsSalesCategory
                        //}
                        , new DateTimeRangeQueryItem{
                            ColumnName = "CreateTime",
                            Title = CommonUIStrings.QueryPanel_Title_ProductStandard_CreateTime,
                        } , new DateTimeRangeQueryItem{
                            ColumnName = "ModifyTime",
                             Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_ModifyTime
                        } ,new QueryItem {
                            ColumnName = "IsSalable",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_VirtualPartsBranch_IsSalable
                        },new QueryItem {
                            ColumnName = "IsOrderable",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_VirtualPartsBranch_IsOrderable
                        },new QueryItem {
                            ColumnName = "IsDirectSupply",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_VirtualPartsBranch_IsDirectSupply
                        }
                    }
                }
            };
        }
    }
}
