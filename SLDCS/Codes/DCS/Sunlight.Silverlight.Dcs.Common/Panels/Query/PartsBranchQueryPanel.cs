﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsBranchQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "ABCStrategy_Category", "MasterData_Status","PartsBranch_ProductLifeCycle","PartsWarrantyTerm_ReturnPolicy","PlanPriceCategory_PriceCategory"
        };
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategory = new ObservableCollection<KeyValuePair>();

        public PartsBranchQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var branch in loadOp.Entities)
                    this.kvPartsSalesCategory.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_PartsBranch,
                    EntityType = typeof(PartsBranch),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "PartCode",
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        }, new QueryItem {
                            ColumnName = "PartName"
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            DefaultValue = (int)DcsBaseDataStatus.有效
                        }, new KeyValuesQueryItem {
                            ColumnName = "PartABC",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new QueryItem {
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_PartsWarrantyCategoryName,
                            ColumnName = "PartsWarrantyCategoryName"
                        }, new KeyValuesQueryItem {
                            ColumnName = "ProductLifeCycle",
                            KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                        }, new CustomQueryItem {
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_IRGroupCode,
                            ColumnName = "PartsSalePriceIncreaseRate.GroupCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_IRGroupName,
                            ColumnName = "PartsSalePriceIncreaseRate.GroupName",
                            DataType = typeof(string)
                        }, new KeyValuesQueryItem {
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_Branch_PartsSalesCategory,
                            ColumnName = "PartsSalesCategoryId",
                            KeyValueItems = this.kvPartsSalesCategory
                        },new KeyValuesQueryItem {
                            ColumnName = "PartsReturnPolicy",
                            KeyValueItems = this.KeyValueManager[this.kvNames[3]]
                        }, new DateTimeRangeQueryItem{
                            ColumnName = "CreateTime",
                            //IsExact=false,
                            //DefaultValue=new[]{DateTime.Now.Date.AddMonths(-1), DateTime.Now.Date}
                        },new CustomQueryItem {
                            ColumnName = "SparePart.ReferenceCode",
                            DataType =typeof(string),
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_ReferenceCode
                        },new CustomQueryItem {
                            ColumnName = "SparePart.EnglishName",
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_EnglishName,
                            DataType = typeof(string)
                        },new KeyValuesQueryItem {
                            ColumnName = "PlannedPriceCategory",
                            KeyValueItems = this.KeyValueManager[this.kvNames[4]]
                        },new CustomQueryItem{
                            ColumnName="SparePart.OverseasPartsFigure",
                            DataType =typeof(string),
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_OverseasPartsFigure
                        },new QueryItem {
                            ColumnName = "IsSalable"
                        },new QueryItem {
                            ColumnName = "IsOrderable"
                        },new QueryItem {
                            ColumnName = "IsDirectSupply"
                        }
                    }
                }
            };
        }
    }
}
