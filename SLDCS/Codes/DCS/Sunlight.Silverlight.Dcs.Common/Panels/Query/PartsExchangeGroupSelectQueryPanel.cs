﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsExchangeGroupSelectQueryPanel : DcsQueryPanelBase {
        public PartsExchangeGroupSelectQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }
        private readonly string[] kvNames = {
            "BaseData_Status"
        };
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_PartsExchange,
                    EntityType = typeof(VirtualPartsExchangeGroup),
                    QueryItems = new[] {
                        //new QueryItem {
                        //    ColumnName = "ExGroupCode",
                        //    Title = "互换号"
                        //},
                        new QueryItem {
                            ColumnName = "ExchangeCode",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsExchangeGroup_ExchangeCode
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title = CommonUIStrings.QueryPanel_Title_ProductStandard_CreateTime,
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            IsExact = false,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_Common_Status
                        }
                    }
                }
            };
        }
    }
}
