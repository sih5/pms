﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsExchangeQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
                "BaseData_Status"
                                            };
        public PartsExchangeQueryPanel() {
            //var a = new PartsExchange();
            //a.PartsExchangeHistories.
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            this.QueryItemGroups = new[]{
                new QueryItemGroup{
                    UniqueId="Common",
                    Title = CommonUIStrings.QueryPanel_Title_PartsExchange,
                    EntityType=typeof(PartsExchange),
                    QueryItems = new  []{
                        new QueryItem{
                            ColumnName="ExchangeCode",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsExchangeGroup_ExchangeCode
                        },new QueryItem{
                            ColumnName="ExGroupCode",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsExchangeGroup_ExGroupCode
                        },new KeyValuesQueryItem{
                            ColumnName="Status",
                            KeyValueItems=this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsBaseDataStatus.有效
                        },new CustomQueryItem{
                            ColumnName="PartCode",
                            DataType = typeof(string),
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        },new CustomQueryItem{
                            ColumnName="PartName",
                            DataType = typeof(string),
                            Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName
                        },new DateTimeRangeQueryItem{
                            ColumnName="CreateTime",
                            
                    } }
                }
            };
        }
    }
}
