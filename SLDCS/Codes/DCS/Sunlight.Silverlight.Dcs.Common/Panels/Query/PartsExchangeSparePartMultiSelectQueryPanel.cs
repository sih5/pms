﻿
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsExchangeSparePartMultiSelectQueryPanel : DcsQueryPanelBase {
        private readonly PartsExchangeSparePartViewModel viewModel = new PartsExchangeSparePartViewModel();
        private readonly string[] kvNames = {
            "MasterData_Status","SparePart_MeasureUnit"
        };

        public PartsExchangeSparePartMultiSelectQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.viewModel.Initialize);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(VirtualPartsBranch),
                    Title = CommonUIStrings.QueryPanel_Title_SparePart,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "PartCode",
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        }, new QueryItem {
                            ColumnName = "PartName",
                            Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                        },new ComboQueryItem {  ColumnName = "PartsSalesCategoryId",
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name,
                            SelectedValuePath="Id",
                            DisplayMemberPath="Name",
                            ItemsSource=this.viewModel.KvPartsSalesCategorys,
                            SelectedItemBinding = new Binding("SelectedPartsSalesCategory") {
                                 Source = this.viewModel,
                                 Mode = BindingMode.TwoWay
                            }
                        },new CustomQueryItem{
                            ColumnName="ReferenceCode",
                            DataType = typeof(string),
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_ReferenceCode
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsMasterDataStatus.有效,
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_Common_Status
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                           Title = CommonUIStrings.QueryPanel_Title_ProductStandard_CreateTime,
                        },new DateTimeRangeQueryItem {
                            ColumnName = "ModifyTime",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_ModifyTime
                        }
                    }
                }
            };
        }
        
    }
    public class PartsExchangeSparePartViewModel : ViewModelBase {

        public ObservableCollection<PartsSalesCategory> kvPartsSalesCategorys;

       
        public ObservableCollection<PartsSalesCategory> KvPartsSalesCategorys {
            get {
                if(this.kvPartsSalesCategorys == null)
                    this.kvPartsSalesCategorys = new ObservableCollection<PartsSalesCategory>();
                return kvPartsSalesCategorys;
            }
        }

        public override void Validate() {
            //
        }

        public void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(e => e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.kvPartsSalesCategorys.Clear();
                foreach(var partsSalesCategories in loadOp.Entities)
                    this.KvPartsSalesCategorys.Add(partsSalesCategories);
            }, null);
        }
    }
}
