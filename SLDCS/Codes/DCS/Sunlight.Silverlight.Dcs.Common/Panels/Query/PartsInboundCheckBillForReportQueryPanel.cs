﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsInboundCheckBillForReportQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
            "PartsPurchaseSettle_Status"
        };
        public PartsInboundCheckBillForReportQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_PartsInboundCheckBillForReportNew,
                    EntityType = typeof(PartsInboundCheckBillForReport),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                          Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_Code,
                            ColumnName = "Code",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                             Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartsPurchaseOrderCode,
                            ColumnName = "PartsPurchaseOrderCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SettleBillCode,
                            ColumnName = "SettleBillCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartCode,
                            ColumnName = "SparePartCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierName,
                            ColumnName = "PartsSupplierName",
                            DataType = typeof(string)
                        }, new KeyValuesQueryItem {
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SettleBillStatus,
                            ColumnName = "SettleBillStatus",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_CreateTime,
                        },new DateTimeRangeQueryItem {
                            ColumnName = "PartsPurchaseOrderDate",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartsPurchaseOrderDate,
                        }
                        }
                    }
                };
        }
    }
}