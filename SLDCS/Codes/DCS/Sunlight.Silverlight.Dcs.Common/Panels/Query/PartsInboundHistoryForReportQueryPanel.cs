﻿
using System.Collections.ObjectModel;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsInboundHistoryForReportQueryPanel : DcsQueryPanelBase {

        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
            "OrderBillType"
        };

        public PartsInboundHistoryForReportQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(Dealer),
                    Title = "配件历史入库记录统计表查询",
                    QueryItems = new QueryItem[] {
                         new DateTimeRangeQueryItem {
                            ColumnName = "DateTime",
                            IsExact = false
                        },
                        new CustomQueryItem {
                            Title = "仓库代码",
                            ColumnName = "WarehouseCode",
                            DataType = typeof(string)
                        },
                        new KeyValuesQueryItem {
                            Title = "订单类型",
                            ColumnName = "OrderBillType",
                            KeyValueItems = this.kvBranches,
                        },new CustomQueryItem {
                            Title = "订单号",
                            ColumnName = "OrderBillCode",
                            DataType = typeof(string)
                        },new CustomQueryItem {
                            Title = "发货单位",
                            ColumnName = "CounterpartCompanyId",
                            DataType = typeof(string),
                        }
                    }
                }
            };
        }
    }
}
