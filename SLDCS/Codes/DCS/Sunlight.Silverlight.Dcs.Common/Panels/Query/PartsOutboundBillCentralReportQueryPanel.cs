﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;


namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsOutboundBillCentralReportQueryPanel : DcsQueryPanelBase
    {
        private readonly ObservableCollection<KeyValuePair> kvCompnyType = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvCustomerType = new ObservableCollection<KeyValuePair>();

        public PartsOutboundBillCentralReportQueryPanel()
        {
            this.Initializer.Register(this.Initialize);
            this.kvCustomerType.Add(new KeyValuePair
            {
                Key = 1,
                Value = "分公司"
            });
            this.kvCustomerType.Add(new KeyValuePair
            {
                Key = 2,
                Value = CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_Center
            });
            this.kvCustomerType.Add(new KeyValuePair
            {
                Key = 3,
                Value = CommonUIStrings.QueryPanel_Title_QueryItem_Dealer
            });
            this.kvCustomerType.Add(new KeyValuePair
            {
                Key = 4,
                Value = CommonUIStrings.DataEditView_Notification_Type_CompanyOrAgency
            });
            this.kvCustomerType.Add(new KeyValuePair
            {
                Key = 5,
                Value ="经销商"
            });
            this.kvCustomerType.Add(new KeyValuePair
            {
                Key = 6,
                Value = "零售"
            });
        }
        private void Initialize()
        {      var domainContext = new DcsDomainContext();
        domainContext.Load(domainContext.GetKeyValueItemsQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.Name == "Company_Type" && (e.Key == (int)DcsCompanyType.代理库 || e.Key == (int)DcsCompanyType.服务站兼代理库)).OrderBy(t => t.Name), loadOp =>
            {
                foreach (var entity in loadOp.Entities)
                {
                    this.kvCompnyType.Add(new KeyValuePair
                    {
                        Key = entity.Key,
                        Value = entity.Value
                    });
                }      
                this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_PartsOutboundBillCentral,
                    EntityType = typeof(PartsOutboundBillCentral),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                           Title = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_Code,
                            ColumnName = "CompanyCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_Name,
                            ColumnName = "CompanyName",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_PartsSalesOrderCode,
                            ColumnName = "PartsSalesOrderCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_PartsOutboundBillCode,
                            ColumnName = "Code",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                          Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartCode,
                            ColumnName = "SparePartCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                          Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                            ColumnName = "ReferenceCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_SubmitCompanyName,
                            ColumnName = "SubmitCompanyName",
                            DataType = typeof(string)
                        },new DateTimeRangeQueryItem {
                            ColumnName = "ShippingDate",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_ShippingDate,
                        }, new KeyValuesQueryItem {
                            Title ="企业类型",
                            ColumnName = "CustomerTypeInt",
                            KeyValueItems = this.kvCompnyType
                        }, new KeyValuesQueryItem {
                            Title ="客户类型",
                            ColumnName = "CustomerTypeTy",
                            KeyValueItems = this.kvCustomerType
                        }
                        }
                    }
                };
            }, null);
        }
    }
}