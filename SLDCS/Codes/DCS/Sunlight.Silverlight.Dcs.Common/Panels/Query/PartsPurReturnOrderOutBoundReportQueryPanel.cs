﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;


namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsPurReturnOrderOutBoundReportQueryPanel : DcsQueryPanelBase
    {
        private readonly string[] kvNames = new[] {
            "PartsPurchaseSettle_Status"
        };
        public PartsPurReturnOrderOutBoundReportQueryPanel()
        {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize()
        {            
                this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_PartsOutboundBillCentralReturn,
                    EntityType = typeof(PartsOutboundBillCentral),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_PartsPurReturnOrderCode,
                            ColumnName = "PartsPurReturnOrderCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                             Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SettleBillCode,
                            ColumnName = "SettlementNo",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartCode,
                            ColumnName = "SparePartCode",
                            DataType = typeof(string)
                        }, new KeyValuesQueryItem {
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_SettlementStatus,
                            ColumnName = "SettlementStatus",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            AllowMultiSelect=true
                        },new DateTimeRangeQueryItem {
                            ColumnName = "OutBoundTime",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_PartsOutboundBillTime,
                        }
                        }
                    }
                };
        }
    }
}