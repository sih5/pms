﻿using System;
using System.Collections.ObjectModel;
using System.Net;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsPurchaseBrandPriceQueryPanel : DcsQueryPanelBase {
        private ObservableCollection<KeyValuePair> kvPartsSalesCategory = new ObservableCollection<KeyValuePair>();
        public PartsPurchaseBrandPriceQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoryViewsQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategory in loadOp.Entities)
                    this.kvPartsSalesCategory.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
            }, null);
            this.QueryItemGroups = new[] { 
                new QueryItemGroup{
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_PartsPackingFinish_GetAllPartsPurchasePricing,
                    EntityType = typeof(GetAllPartsPurchasePricing),
                    QueryItems = new QueryItem[]{
                        new ComboQueryItem{
                            ColumnName = "PartsSalesCategoryName",
                            ItemsSource = this.kvPartsSalesCategory,                           
                            DisplayMemberPath = "Value",
                            SelectedValuePath = "Value",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSalesCategoryId
                        }, new CustomQueryItem{
                            ColumnName = "AvailibleTime",
                            DataType = typeof(DateTime),
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_AvailibleTime
                        }, new CustomQueryItem{
                            ColumnName = "PartCode",
                            DataType = typeof(string),
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_SparePartCode,
                            IsEnabled = false
                        },new CustomQueryItem{
                            ColumnName = "PartName",
                            DataType = typeof(string),
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_SparePartName,
                            IsEnabled = false
                        },new CustomQueryItem{
                            ColumnName = "PartsSupplierCode",
                            DataType = typeof(string),
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierCode
                        },new CustomQueryItem{
                            ColumnName = "PartsSupplierName",
                            DataType = typeof(string),
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierName
                        },new CustomQueryItem{
                            ColumnName = "IsOrderable",
                            DataType = typeof(bool),
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_IsOrderable
                        },new CustomQueryItem{
                            ColumnName = "IsSalable",
                            DataType = typeof(bool),
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_IsSalable
                        }
                    }
                }
            };
        }
    }
}
