﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsPurchaseInfoSummaryForReportQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
           "ABCStrategy_Category","PartPriority"
        };
        public PartsPurchaseInfoSummaryForReportQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            Initializer.Register(Initialize);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                      new QueryItemGroup {
                           UniqueId = "Common",
                           Title = CommonUIStrings.QueryPanel_Title_PartsPurchasePlanReport,
                           EntityType = typeof(PartsPurchaseOrderFinishReport),
                           QueryItems =   new QueryItem[]{
                            new DateTimeQueryItem{
                                   Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricingDetail_Time,
                                   ColumnName="CreateTime",
                                   DefaultValue=DateTime.Now.Date
                             }, new CustomQueryItem {
                                Title = CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_OwerOrder,
                                 DataType=typeof(int),
                                ColumnName = "OweOrderNum"                            
                             },new CustomQueryItem {
                                Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartCode,
                                 DataType=typeof(string),
                                ColumnName = "PartCode"                            
                             }, new CustomQueryItem {
                                Title =  CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierName,
                                 DataType=typeof(string),
                                ColumnName = "SupplierName"                            
                             }, new KeyValuesQueryItem {
                                Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartABCNew,
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                                ColumnName = "PartABC"
                              },new KeyValuesQueryItem {
                                Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPriorityBill_Priority,
                                ColumnName = "Priority",
                                KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                           }
                           }
                      }
               };
        }
    }
}
