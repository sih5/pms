﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsPurchaseOrderDetailForReportQueryPanel : DcsQueryPanelBase {
        private ObservableCollection<KeyValuePair> kvFinishi = new ObservableCollection<KeyValuePair>();
        public PartsPurchaseOrderDetailForReportQueryPanel() {
            this.kvFinishi.Add(new KeyValuePair {
                Key = 1,
                Value = CommonUIStrings.Report_QueryPanel_KeyValue_Finishi
            });
            this.kvFinishi.Add(new KeyValuePair {
                Key = 2,
                Value = CommonUIStrings.Report_QueryPanel_KeyValue_PartFinishi
            });
            this.kvFinishi.Add(new KeyValuePair {
                Key = 3,
                Value = CommonUIStrings.Report_QueryPanel_KeyValue_UnFinishi
            });
            Initializer.Register(Initialize);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                      new QueryItemGroup {
                           UniqueId = "Common",
                           Title = CommonUIStrings.QueryPanel_Title_PartsPurchasePlanReport,
                           EntityType = typeof(PartsPurchaseOrderFinishReport),
                           QueryItems =   new QueryItem[]{
                               new DateTimeQueryItem{
                                   Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricingDetail_Time,
                                   ColumnName="CreateTime",
                                   DefaultValue=DateTime.Now.Date
                              }, new KeyValuesQueryItem {
                                 Title = CommonUIStrings.Report_QueryPanel_KeyValue_InBoundStatus,
                                 KeyValueItems = this.kvFinishi,
                                 ColumnName = "InboundFinishStatus"
                              }, new QueryItem {
                                 Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_PartsSupplierName,
                                 ColumnName = "SupplierName"                            
                              }, new CustomQueryItem {
                                Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartCode,
                                ColumnName = "PartCode" ,
                                DataType=typeof(String)
                             }, new DateTimeRangeQueryItem {
                                Title =  CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_PriorityCreateTime,
                                ColumnName = "PriorityCreateTime"                            
                             },new DateTimeRangeQueryItem {
                                Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_ExpectDeliveryTime,
                                ColumnName = "PlanDeliveryTime",
                           }
                           }
                      }
               };
        }
    }
}
