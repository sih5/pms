﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsPurchaseOrderForDirectQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvType = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
            "PartsPurchaseOrder_Status"
        };
        public PartsPurchaseOrderForDirectQueryPanel() {
            this.kvType.Add(new KeyValuePair {
                Key = 1,
                Value = CommonUIStrings.QueryPanel_Title_PartsPurchaseOrder_Status1
            });
            this.kvType.Add(new KeyValuePair {
                Key = 2,
                Value = CommonUIStrings.QueryPanel_Title_PartsPurchaseOrder_Status2
            });
            this.kvType.Add(new KeyValuePair {
                Key = 3,
                Value = CommonUIStrings.QueryPanel_Title_PartsPurchaseOrder_Status3
            });
            this.kvType.Add(new KeyValuePair {
                Key = 4,
                Value = CommonUIStrings.QueryPanel_Title_PartsPurchaseOrder_Status4
            });
            this.kvType.Add(new KeyValuePair {
                Key = 5,
                Value = CommonUIStrings.QueryPanel_Title_PartsPurchaseOrder_Status5
            });
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_PartsPurchaseOrder,
                    EntityType = typeof(PartsPurchaseOrderForDirect),
                    QueryItems = new QueryItem[] {
                        new KeyValuesQueryItem {
                            Title = CommonUIStrings.QueryPanel_Title_PartsPurchaseOrder_Finish,
                            ColumnName = "FinishStatus",
                            KeyValueItems = this.kvType
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title=CommonUIStrings.QueryPanel_Title_PartsPurchaseOrder_Status
                        }, new CustomQueryItem {
                           Title=CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_PartsSupplierName,
                            ColumnName = "PartsSupplierName",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_PartsPurchaseOrderCode,
                            ColumnName = "Code",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_SparePartCode,
                            ColumnName = "SparePartCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_OriginalRequirementBillCode,
                            ColumnName = "OriginalRequirementBillCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_PersonName,
                            ColumnName = "PersonName",
                            DataType = typeof(string)
                        },new DateTimeRangeQueryItem {
                            ColumnName = "SubmitTime",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlanReport_SubmitTime,
                            DefaultValue = new[]{
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                            }
                        }
                        }
                    }
                };
        }
    }
}