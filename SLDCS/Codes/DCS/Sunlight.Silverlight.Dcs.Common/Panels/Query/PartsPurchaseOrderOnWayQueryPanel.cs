﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsPurchaseOrderOnWayQueryPanel : DcsQueryPanelBase {
        private ObservableCollection<KeyValuePair> kvFinishi = new ObservableCollection<KeyValuePair>();
        private ObservableCollection<KeyValuePair> kvOverTime = new ObservableCollection<KeyValuePair>();
        private ObservableCollection<KeyValuePair> kvUndueTime = new ObservableCollection<KeyValuePair>();

        public PartsPurchaseOrderOnWayQueryPanel() {
            Initializer.Register(Initialize);
            this.kvFinishi.Add(new KeyValuePair {
                Key = 1,
                Value = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_KvFinishi_One
            });
            this.kvFinishi.Add(new KeyValuePair {
                Key = 2,
                Value = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_KvFinishi_Two
            });
            this.kvFinishi.Add(new KeyValuePair {
                Key = 3,
                Value = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_KvFinishi_Three
            });
            this.kvOverTime.Add(new KeyValuePair {
                Key = 1,
                Value = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_kvOverTime_One
            });
            this.kvOverTime.Add(new KeyValuePair {
                Key = 2,
                Value = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_kvOverTime_Two
            });
            this.kvOverTime.Add(new KeyValuePair {
                Key = 3,
                Value = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_kvOverTime_Three
            });
            this.kvOverTime.Add(new KeyValuePair {
                Key = 4,
                Value = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_kvOverTime_Four
            });
            this.kvUndueTime.Add(new KeyValuePair {
                Key = 1,
                Value = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_kvUndueTime_One
            });
            this.kvUndueTime.Add(new KeyValuePair {
                Key = 2,
                Value = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_kvUndueTime_Two
            });
            this.kvUndueTime.Add(new KeyValuePair {
                Key = 3,
                Value = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_kvUndueTime_Three
            });
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                      new QueryItemGroup {
                           UniqueId = "Common",
                           Title = CommonUIStrings.QueryPanel_Title_PartsPurchaseOrderFinishOnWay,
                           EntityType = typeof(PartsPurchaseOrderFinish),
                           QueryItems = new QueryItem[]{                    
                              new DateTimeRangeQueryItem {
                               Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_TheoryDeliveryTime,
                             ColumnName = "TheoryDeliveryTime",
                           },  new DateTimeRangeQueryItem {
                               Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_PurchasePlanTime,
                             ColumnName = "PurchasePlanTime",
                           },  new ComboQueryItem{
                            ColumnName = "KvFinishi",
                            ItemsSource = this.kvFinishi,                           
                            DisplayMemberPath = "Value",
                            SelectedValuePath = "Value",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_KvFinishi
                           },  new ComboQueryItem{
                            ColumnName = "KvOverTime",
                            ItemsSource = this.kvOverTime,                           
                            DisplayMemberPath = "Value",
                            SelectedValuePath = "Key",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_KvOverTime
                           },  new ComboQueryItem{
                            ColumnName = "KvUndueTime",
                            ItemsSource = this.kvUndueTime,                           
                            DisplayMemberPath = "Value",
                            SelectedValuePath = "Value",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_KvUndueTime
                           }
                           }
                      }
               };
        }
    }
}
