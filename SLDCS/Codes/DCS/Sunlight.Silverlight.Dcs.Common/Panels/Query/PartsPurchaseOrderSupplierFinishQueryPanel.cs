﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsPurchaseOrderSupplierFinishQueryPanel : DcsQueryPanelBase {
        public PartsPurchaseOrderSupplierFinishQueryPanel() {
            Initializer.Register(Initialize);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                      new QueryItemGroup {
                           UniqueId = "Common",
                           Title = CommonUIStrings.QueryPanel_Title_PartsPurchaseOrderFinish,
                           EntityType = typeof(PartsPurchaseOrderFinish),
                           QueryItems = new QueryItem[]{                    
                             new DateTimeRangeQueryItem {
                               Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_TheoryDeliveryTime,
                             ColumnName = "TheoryDeliveryTime",
                                DefaultValue = new [] {
                                    new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                                }
                           },new DateTimeRangeQueryItem {
                               Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_PurchasePlanTime,
                             ColumnName = "PurchasePlanTime",
                           },new CustomQueryItem {
                             Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_SupplierName,
                            ColumnName = "SupplierName",
                            DataType = typeof(string)
                           }
                       }
                    }
               };
        }
    }
}
