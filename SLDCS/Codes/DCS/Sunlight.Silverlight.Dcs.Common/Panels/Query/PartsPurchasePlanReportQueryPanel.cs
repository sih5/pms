﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query
{
    public class PartsPurchasePlanReportQueryPanel : DcsQueryPanelBase
    {
        private readonly string[] kvNames = new[] {
           "PartsPurchaseOrder_Status","PurchasePlanOrderStatus"
        };
        public PartsPurchasePlanReportQueryPanel()
        {
            this.KeyValueManager.Register(this.kvNames);
            Initializer.Register(Initialize);
        }
        private void Initialize()
        {
            this.QueryItemGroups = new[] {
                      new QueryItemGroup {
                           UniqueId = "Common",
                           Title = CommonUIStrings.QueryPanel_Title_PartsPurchasePlanReport,
                           EntityType = typeof(PartsPurchasePlanReport),
                           QueryItems = new QueryItem[]{
                           new KeyValuesQueryItem {
                              Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlanReport_Status,
                               KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                              ColumnName = "Status"
                           }
                           ,new KeyValuesQueryItem {
                              Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlanReport_PartsPurchaseOrderStatus,
                              ColumnName = "PartsPurchaseOrderStatus",
                               KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                           }
                           ,new DateTimeRangeQueryItem {
                               Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlanReport_PartsPurchasePlanTime,
                             ColumnName = "PartsPurchasePlanTime",
                          },new DateTimeRangeQueryItem {
                               Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartsPurchaseOrderDate,
                             ColumnName = "PartsPurchaseOrderDate",
                          }
                          , new QueryItem {
                              Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartCode,
                              ColumnName = "SparePartCode"                            
                           }, new QueryItem {
                              Title =  CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierName,
                              ColumnName = "PartsSupplierName"                            
                           }, new QueryItem {
                              Title =  CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlanReport_Remark,
                              ColumnName = "Remark"                            
                           }, new QueryItem {
                              Title =  CommonUIStrings.DataGridView_ColumnItem_Title_PartsSupplierRelation_SupplierPartCode,
                              ColumnName = "SupplierPartCode"                            
                           }, new CustomQueryItem {
                              Title =  CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlanReport_IsOnWayNumber,
                              ColumnName = "IsOnWayNumber",
                              DataType = typeof(bool),
                              DefaultValue = null
                           } ,new DateTimeRangeQueryItem {
                               Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlanReport_SubmitTime,
                             ColumnName = "SubmitTime",
                          }
                           }
                      }
               };
        }
    }
}
