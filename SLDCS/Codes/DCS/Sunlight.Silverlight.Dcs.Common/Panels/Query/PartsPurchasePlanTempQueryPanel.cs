﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsPurchasePlanTempQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
           "PartsPurchaseOrder_Status"
        };
        public PartsPurchasePlanTempQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            Initializer.Register(Initialize);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                      new QueryItemGroup {
                           UniqueId = "Common",
                           Title = CommonUIStrings.QueryPanel_Title_PartsPurchasePlanTempForReport,
                           EntityType = typeof(VirtualPartsPurchasePlanTemp),
                            QueryItems = new QueryItem[] {
                           new KeyValuesQueryItem {
                               Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlanReport_PartsPurchaseOrderStatus,
                               ColumnName = "PartsPurchaseOrderStatus",
                               KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                           },new CustomQueryItem {
                               Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_PartsPurchaseOrderCode,
                               ColumnName = "PartsPurchaseOrderCode",
                               DataType = typeof(string)                             
                           },new CustomQueryItem {
                               Title=CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                               ColumnName = "SparePartCode",
                               DataType = typeof(string)
                           }, new CustomQueryItem {
                               Title="包材图号",
                               ColumnName = "PackSparePartCode"  ,
                               DataType = typeof(string)                             
                           }
                           }
                      }
               };
        }
    }
}
