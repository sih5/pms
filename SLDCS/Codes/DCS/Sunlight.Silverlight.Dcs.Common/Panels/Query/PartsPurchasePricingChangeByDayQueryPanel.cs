﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsPurchasePricingChangeByDayQueryPanel : DcsQueryPanelBase {
        public PartsPurchasePricingChangeByDayQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_PartsPurchasePricingChangeByDay,
                    EntityType = typeof(PartsPurchasePricingDetail),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "PartCode"
                        }, new QueryItem {
                            ColumnName = "PartName"
                        }, new QueryItem {
                            ColumnName = "SupplierCode"
                        }, new QueryItem {
                            ColumnName = "SupplierName"
                        }, new CustomQueryItem {
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricingDetail_Time,
                            ColumnName = "ValidFrom",
                            DataType = typeof(DateTime),
                            DefaultValue = DateTime.Now.Date
                        }
                    }
                }
            };
        }
    }
}
