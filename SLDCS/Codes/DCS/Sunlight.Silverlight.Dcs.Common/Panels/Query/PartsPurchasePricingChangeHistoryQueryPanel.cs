﻿using System;
using Sunlight.Silverlight.Core.Model;
using System.Collections.ObjectModel;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System.ServiceModel.DomainServices.Client;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsPurchasePricingChangeHistoryQueryPanel : DcsQueryPanelBase {
        //private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategories = new ObservableCollection<KeyValuePair>();
        public PartsPurchasePricingChangeHistoryQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            //var dcsDomainContext = new DcsDomainContext();
            //dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesWithBranchIdQuery(), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    foreach(var partsSalesCategor in loadOp.Entities)
            //        this.kvPartsSalesCategories.Add(new KeyValuePair {
            //            Key = partsSalesCategor.Id,
            //            Value = partsSalesCategor.Name
            //        });
            //}, null);

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_PartsPurchasePricingChangeHistory,
                    EntityType = typeof(PartsPurchasePricingDetail),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "PartCode"
                        }, new QueryItem {
                            ColumnName = "PartName"
                        }, new CustomQueryItem{ 
                            ColumnName = "SparePart.ReferenceCode", 
                            DataType = typeof(string), 
                           Title = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                        }, new QueryItem {
                            ColumnName = "SupplierPartCode",
                            Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsSupplierRelation_SupplierPartCode,
                        }, new QueryItem {
                            ColumnName = "SupplierCode"
                        }, new QueryItem {
                            ColumnName = "SupplierName"
                        }
                        //, new KeyValuesQueryItem {
                        //    ColumnName = "PartsPurchasePricingChange.PartsSalesCategoryId",
                        //    KeyValueItems = this.kvPartsSalesCategories,
                        //    Title = CommonUIStrings.QueryPanel_QueryItem_Title_Branch_PartsSalesCategory
                        //}
                        , new DateTimeRangeQueryItem {
                            ColumnName = "ValidFrom",
                            DefaultValue =new []{
                                new DateTime(DateTime.Now.Year,DateTime.Now.Month,1), DateTime.Now.Date.AddDays(2)
                            }
                        }
                    }
                }
            };
        }
    }
}
