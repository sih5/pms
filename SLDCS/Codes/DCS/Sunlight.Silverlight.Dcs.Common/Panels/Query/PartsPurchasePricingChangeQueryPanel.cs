﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsPurchasePricingChangeQueryPanel : DcsQueryPanelBase {
        //private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategories = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
            "PartsPurchasePricingChange_Status"
        };

        public PartsPurchasePricingChangeQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            //var dcsDomainContext = new DcsDomainContext();
            //dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesWithBranchIdQuery(), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    foreach(var partsSalesCategor in loadOp.Entities)
            //        this.kvPartsSalesCategories.Add(new KeyValuePair {
            //            Key = partsSalesCategor.Id,
            //            Value = partsSalesCategor.Name
            //        });
            //}, null);

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_PartsPurchasePricingChange,
                    EntityType = typeof(PartsPurchasePricingChange),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code"
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsPartsPurchasePricingChangeStatus.新增
                        }
                        //, new KeyValuesQueryItem {
                        //    ColumnName = "PartsSalesCategoryId",
                        //    KeyValueItems = this.kvPartsSalesCategories,
                        //    Title = CommonUIStrings.QueryPanel_QueryItem_Title_Branch_PartsSalesCategory
                        //}
                        ,new CustomQueryItem {
                            Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartCode,
                            ColumnName = "SparePartCode",
                            DataType = typeof(string)
                        }
                        ,new CustomQueryItem {
                            Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                            ColumnName = "SparePartName",
                            DataType = typeof(string)
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue =new []{
                                new DateTime(DateTime.Now.Year,DateTime.Now.Month,1), DateTime.Now.Date.AddDays(1)
                            }
                        }
                    }
                }
            };
        }
    }
}
