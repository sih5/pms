﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsPurchasePricingQueryPanel : DcsQueryPanelBase {
        //private ObservableCollection<KeyValuePair> kvPartsSalesCategory = new ObservableCollection<KeyValuePair>();

        public PartsPurchasePricingQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            //var dcsDomainContext = new DcsDomainContext();
            //dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    foreach(var partsSalesCategory in loadOp.Entities)
            //        this.kvPartsSalesCategory.Add(new KeyValuePair {
            //            Key = partsSalesCategory.Id,
            //            Value = partsSalesCategory.Name
            //        });
            //}, null);
            this.QueryItemGroups = new[] { 
                new QueryItemGroup{
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_PartsPurchasePricing,
                    EntityType = typeof(PartsPurchasePricing),
                    QueryItems = new QueryItem[]{
                        //new KeyValuesQueryItem{
                        //    ColumnName = "PartsSalesCategoryId",
                        //    KeyValueItems = this.kvPartsSalesCategory,
                        //    Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSalesCategoryId
                        //}, 
                        new CustomQueryItem{
                            ColumnName = "AvailibleTime",
                            DataType = typeof(DateTime),
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_AvailibleTime,
                            DefaultValue = DateTime.Now
                        }
                         ,new CustomControlQueryItem { 
                            ColumnName = "SparePartCode", 
                            Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartCode,
                            CreateControl=type =>new Controls.MultipleTextQueryControl(type,"SparePartCode") 
                        },new CustomQueryItem{
                            ColumnName = "SparePart.Name",
                            DataType = typeof(string),
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_SparePartName
                        },new CustomQueryItem{
                            ColumnName = "SparePart.ReferenceCode",
                            DataType = typeof(string),
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                        },new CustomQueryItem{
                            ColumnName = "SupplierPartCode",
                            DataType = typeof(string),
                             Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsSupplierRelation_SupplierPartCode,
                        },new CustomQueryItem{
                            ColumnName = "PartsSupplier.Code",
                            DataType = typeof(string),
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierCode
                        },new CustomQueryItem{
                            ColumnName = "PartsSupplier.Name",
                            DataType = typeof(string),
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierName
                        },new CustomQueryItem{
                            ColumnName = "SparePart.PartsBranch.IsOrderable",
                            DataType = typeof(bool),
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_IsOrderable
                        },new CustomQueryItem{
                            ColumnName = "SparePart.PartsBranch.IsSalable",
                            DataType = typeof(bool),
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_IsSalable
                        }
                    }
                }
            };
        }
    }
}
