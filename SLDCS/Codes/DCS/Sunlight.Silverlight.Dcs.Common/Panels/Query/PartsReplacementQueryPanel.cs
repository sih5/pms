﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsReplacementQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        public PartsReplacementQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_PartsReplacement,
                    EntityType = typeof(PartsReplacement),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                             Title =CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                            ColumnName = "PartCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                            Title = Utils.GetEntityLocalizedName(typeof(SparePart), "Name"),
                            ColumnName = "PartName",
                            DataType = typeof(string)
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsBaseDataStatus.有效
                        }, new QueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_ReferenceCode,
                            ColumnName = "PartReferenceCode"
                        }, new QueryItem {
                           Title ="SPM是否应用",
                            ColumnName = "IsSPM"
                        }, new QueryItem {
                           Title ="是否接口传输",
                            ColumnName = "IsInterFace"
                        }
                    }
                }
            };
        }
    }
}
