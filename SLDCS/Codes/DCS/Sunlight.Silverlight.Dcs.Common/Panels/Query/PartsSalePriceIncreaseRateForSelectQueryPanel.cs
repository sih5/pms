﻿
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsSalePriceIncreaseRateForSelectQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        public PartsSalePriceIncreaseRateForSelectQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_PartsSalePriceIncreaseRate,
                    EntityType = typeof(PartsSalePriceIncreaseRate),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "GroupCode"
                        },new QueryItem {
                            ColumnName = "Name"
                        }, new KeyValuesQueryItem {
                            ColumnName = "status",
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_Common_Status,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsMasterDataStatus.有效,
                            IsEnabled = false
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        }
                    }
                }
            };
        }
    }
}
