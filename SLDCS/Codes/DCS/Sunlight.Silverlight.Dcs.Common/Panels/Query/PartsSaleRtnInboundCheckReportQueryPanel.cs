﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsSaleRtnInboundCheckReportQueryPanel: DcsQueryPanelBase
    {
        private readonly string[] kvNames = new[] {
            "PartsPurchaseSettle_Status"
        };
        public PartsSaleRtnInboundCheckReportQueryPanel()
        {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize()
        {            
                this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_PartsSaleRtnInboundCheck,
                    EntityType = typeof(PartsSaleRtnInboundCheck),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_PartsSalesReturnBillCode,
                            ColumnName = "PartsSalesReturnBillCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                             Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_PartsSalesOrderCode,
                            ColumnName = "PartsSalesOrderCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                             Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_Code,
                            ColumnName = "Code",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                             Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SettleBillCode,
                            ColumnName = "SettlementNo",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartCode,
                            ColumnName = "SparePartCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_SAPSysInvoiceNumber,
                            ColumnName = "SAPSysInvoiceNumber",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_CounterpartCompanyName,
                            ColumnName = "CounterpartCompanyName",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_SubmitCompanyCode,
                            ColumnName = "CounterpartCompanyCode",
                            DataType = typeof(string)
                        } , new KeyValuesQueryItem {
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SettleBillStatus,
                            ColumnName = "SettlementStatus",
                            AllowMultiSelect=true,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_CreateTime,
                        }
                        }
                    }
                };
        }
    }
}