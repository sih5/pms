﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;
namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsSalesOrderOnWayForReportQueryPanel : DcsQueryPanelBase
    {
        private readonly ObservableCollection<KeyValuePair> kvType = new ObservableCollection<KeyValuePair>();

        public PartsSalesOrderOnWayForReportQueryPanel()
        {
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize()
        {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetKeyValueItemsQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.Name == "Company_Type" && (e.Key == (int)DcsCompanyType.代理库 || e.Key == (int)DcsCompanyType.服务站 || e.Key == (int)DcsCompanyType.服务站兼代理库)).OrderBy(t => t.Name), loadOp => {
                foreach (var entity in loadOp.Entities)
                {
                    this.kvType.Add(new KeyValuePair
                    {
                        Key = entity.Key,
                        Value = entity.Value
                    });
                }
                this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_PartsSalesOrderFinishReportOnWay,
                    EntityType = typeof(PartsSalesOrderFinishReport),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_ShippingCode,
                            ColumnName = "ShippingCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_SubmitCompanyName,
                            ColumnName = "SubmitCompanyName",
                            DataType = typeof(string)
                        },new DateTimeRangeQueryItem {
                            ColumnName = "ApproveTime",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_ApproveTimeNew,
                        },new DateTimeRangeQueryItem {
                            ColumnName = "ShippingDate",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_ShippingDate,
                        }, new KeyValuesQueryItem {
                            ColumnName = "SubmitCompanyType",
                            Title="企业类型",
                            KeyValueItems = this.kvType
                        }
                        }
                    }
                };
            }, null);
        }
    }
}