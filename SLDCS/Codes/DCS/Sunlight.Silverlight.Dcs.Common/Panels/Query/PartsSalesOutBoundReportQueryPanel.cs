﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;


namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsSalesOutBoundReportQueryPanel : DcsQueryPanelBase
    {
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvOrderType = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvGroupNameType = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = new[] {
            "PartsSalesSettlement_Status"
        };
        public PartsSalesOutBoundReportQueryPanel()
        {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize()
        {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalePriceIncreaseRatesQuery().Where(e => e.status == (int)DcsBaseDataStatus.有效).OrderBy(t => t.GroupName), LoadBehavior.RefreshCurrent, loadOps => {
                if(loadOps.HasError)
                    return;
                foreach(var type in loadOps.Entities)
                    this.kvGroupNameType.Add(new KeyValuePair {
                        Key = type.Id,
                        Value = type.GroupName
                    });
            }, null);
            domainContext.Load(domainContext.GetPartsSalesOrderTypesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效).OrderBy(t => t.Name), LoadBehavior.RefreshCurrent, loadOps => {
                if(loadOps.HasError)
                    return;
                foreach(var type in loadOps.Entities)
                    this.kvOrderType.Add(new KeyValuePair {
                        Key = type.Id,
                        Value = type.Name
                    });
            }, null);
            domainContext.Load(domainContext.GetWarehousesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId).OrderBy(t => t.Name), loadOp => {
                foreach (var entity in loadOp.Entities)
                {
                    this.kvWarehouses.Add(new KeyValuePair
                    {
                        Key = entity.Id,
                        Value = entity.Name
                    });
                }
                this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_PartsSalesOrderFinishReportSIH,
                    EntityType = typeof(PartsSalesOrderFinishReport),
                    QueryItems = new QueryItem[] {
                         new CustomControlQueryItem {
                            ColumnName = "SparePartCode",
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                            CreateControl=type =>new Controls.MultipleTextQueryControl(type,"SparePartCode")
                        },new CustomQueryItem {
                          Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_MarketingDepartmentName,
                            ColumnName = "MarketingDepartmentName",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                             Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_SubmitCompanyName,
                            ColumnName = "SubmitCompanyName",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_OldCode,
                            ColumnName = "Code",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_PartsOutboundBillCode,
                            ColumnName = "PartsOutboundBillCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_SparePartName,
                            ColumnName = "SparePartName",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                            ColumnName = "ReferenceCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_Province,
                            ColumnName = "Province",
                            DataType = typeof(string)
                        }, new KeyValuesQueryItem {
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_WarehouseId,
                            ColumnName = "WarehouseId",
                            KeyValueItems = this.kvWarehouses
                        }, new KeyValuesQueryItem {
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_PartsSalesOrderTypeName,
                            ColumnName = "PartsSalesOrderTypeId",
                            KeyValueItems = this.kvOrderType
                        }, new KeyValuesQueryItem {
                            ColumnName = "SettlementStatus",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SettleBillStatus,
                            AllowMultiSelect=true,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new KeyValuesQueryItem {
                            ColumnName = "GroupNameId",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_Sparepart_IncreaseRateGroupId,
                            KeyValueItems=this.kvGroupNameType
                        },new DateTimeRangeQueryItem {
                            ColumnName = "ShippingDate",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_PartsOutboundBillTime,
                        }
                        }
                    }
                };
            }, null);
        }
    }
}