﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query
{
    public class PartsShelvesTaskFinishQueryPanel: DcsQueryPanelBase
    {
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = new[] {
            "ABCStrategy_Category"
        };
        public PartsShelvesTaskFinishQueryPanel()
        {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize()
        {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetWarehousesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId).OrderBy(t => t.Name), loadOp => {
                foreach (var entity in loadOp.Entities)
                {
                    this.kvWarehouses.Add(new KeyValuePair
                    {
                        Key = entity.Id,
                        Value = entity.Name
                    });
                }
                this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_PartsShelvesTaskFinish,
                    EntityType = typeof(PartsShelvesTaskFinish),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_PartsPurchaseOrderCode,
                            ColumnName = "PartsPurchaseOrderCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                             Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_PartsInboundCheckBillCode,
                            ColumnName = "PartsInboundCheckBillCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartCode,
                            ColumnName = "SparePartCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                            ColumnName = "ReferenceCode",
                            DataType = typeof(string)
                        }, new KeyValuesQueryItem {
                           Title=CommonUIStrings.QueryPanel_QueryItem_Title_Sparepart_PartABC,
                            ColumnName = "PartABC",
                             KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new KeyValuesQueryItem {
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_WarehouseId,
                            ColumnName = "WarehouseId",
                            KeyValueItems = this.kvWarehouses
                        },new DateTimeRangeQueryItem {
                            ColumnName = "PackModifyTime",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_ModifyTime,
                        },new DateTimeRangeQueryItem {
                            ColumnName = "ShelvesFinishTime",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsShelvesTaskFinish_ShelvesFinishTime,
                        }
                        }
                    }
                };
            }, null);
        }
    }
}