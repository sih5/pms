﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;
namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsShippingOrderReportQueryPanel : DcsQueryPanelBase {
        public PartsShippingOrderReportQueryPanel()
        {
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
                this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_PartsShippingOrderForReport,
                    EntityType = typeof(PartsShippingOrderForReport),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_ShippingCode,
                            ColumnName = "Code",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                             Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_PartsSalesOrderCode,
                            ColumnName = "PartsSalesOrderCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrderForReport_AppraiserName,
                            ColumnName = "AppraiserName",
                            DataType = typeof(string)
                        },new DateTimeRangeQueryItem {
                            ColumnName = "ShippingDate",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrderForReport_ShippingDateNew,
                        }
                        }
                    }
                };
        }
    }
}