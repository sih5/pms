﻿
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsStockDetailForReportQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvAreaCategory = {
            "Area_Category"
        };
        public PartsStockDetailForReportQueryPanel() {
            this.KeyValueManager.Register(this.kvAreaCategory);
            Initializer.Register(Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetKeyValueItemsQuery().Where(r => r.Name == "BranchList"), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                }
                this.kvBranches.Clear();
                foreach(var branch in loadOp.Entities)
                    this.kvBranches.Add(new KeyValuePair {
                        Key = branch.Key,
                        Value = branch.Value
                    });
            }, null);

            dcsDomainContext.Load(dcsDomainContext.GetWarehousesOrderByNameQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehouse in loadOp.Entities)
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
            }, null);
            QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = CommonUIStrings.QueryPanel_Title_PartsStockDetail,
                        EntityType = typeof(PartsStock),
                        QueryItems = new QueryItem[] {
                            new KeyValuesQueryItem {
                                    Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_BranchName,
                                    ColumnName = "BranchCode",
                                    KeyValueItems = kvBranches,
                                    DefaultValue = int.Parse(BaseApp.Current.CurrentUserData.EnterpriseCode),
                                    IsEnabled =BaseApp.Current.CurrentUserData.UserCode=="ReportAdmin"
                            },
                            new KeyValuesQueryItem {
                                Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_WarehouseId,
                                ColumnName = "WarehouseCode",
                                KeyValueItems = kvWarehouses
                            },
                            new CustomQueryItem {
                                Title =CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                                ColumnName = "SparePartCode",
                                DataType = typeof(string)
                            },
                            new CustomQueryItem {
                                Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                                ColumnName = "SparePartName",
                                DataType = typeof(string)
                            },
                            new KeyValuesQueryItem {
                                Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsStock_AreaCategoryId,
                                ColumnName = "AreaCategoryId",
                                KeyValueItems = this.KeyValueManager[this.kvAreaCategory[0]],
                                DefaultValue = (int)DcsAreaType.保管区
                            },
                            new CustomQueryItem {
                                Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsStock_IsStoreZero,
                                ColumnName = "IsStoreZero",
                                DataType = typeof(bool)
                            },
                            new DateTimeRangeQueryItem {
                                Title = CommonUIStrings.QueryPanel_Title_ProductStandard_CreateTime,
                                ColumnName = "CreateTime",
                                DefaultValue=new [] {
                                      DateTime.Now.AddMonths(-1), DateTime.Now
                                }
                            }
                        }

                    }
                };
        }
    }
}
