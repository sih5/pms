﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query
{
    public class PartsStockStatementReportQueryPanel: DcsQueryPanelBase
    {
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        public PartsStockStatementReportQueryPanel()
        {
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize()
        {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetWarehousesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId).OrderBy(t => t.Name), loadOp => {
                foreach (var entity in loadOp.Entities) {
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name
                    });
                }
                this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_PartsStockDetail_PartsStockStatement,
                    EntityType = typeof(PartsStockStatement),
                    QueryItems = new QueryItem[] {
                         new KeyValuesQueryItem {
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_WarehouseId,
                            ColumnName = "WarehouseId",
                            KeyValueItems = this.kvWarehouses
                        },
                        new CustomQueryItem {
                           Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartCode,
                            ColumnName = "SparePartCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                             Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                            ColumnName = "SparePartName",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                            ColumnName = "ReferenceCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsStockStatement_BeginTime,
                           DataType = typeof(DateTime),
                           ColumnName = "BeginTime",
                           DefaultValue = DateTime.Now.AddDays(-7).Date
                        },new CustomQueryItem {
                            ColumnName = "EndTime",
                            DataType = typeof(DateTime), 
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsStockStatement_EndTime,
                            DefaultValue = DateTime.Now
                        }
                    }
                }
            };
            }, null);
        }
    }
}