﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Web.Entities;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsSupplierForRepairOrderQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "MasterData_Status","PartsSupplier_Type"
        };

        public PartsSupplierForRepairOrderQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_PartsSupplier,
                    EntityType = typeof(VirtualPartsSupplier),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title = CommonUIStrings.QueryPanel_Title_FaultyParts_SupplierCode
                        }, new QueryItem {
                            ColumnName = "Name",
                            Title = CommonUIStrings.QueryPanel_Title_FaultyParts_SupplierName
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsMasterDataStatus.有效,
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_Common_Status
                        }, new KeyValuesQueryItem {
                            ColumnName = "SupplierType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsSupplier_SupplierType
                        }
                    }
                }
            };
        }
    }
}
