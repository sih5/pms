﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsSupplierQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "MasterData_Status","PartsSupplier_Type"
        };

        public PartsSupplierQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_PartsSupplier,
                    EntityType = typeof(PartsSupplier),
                    QueryItems = new QueryItem[]{
                        new CustomQueryItem {
                            ColumnName = "Company.Code",
                             DataType = typeof(string),
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierCode
                        }, new CustomQueryItem {
                            ColumnName = "Company.Name",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierName,
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                            ColumnName = "Company.SupplierCode",
                            Title = CommonUIStrings.QueryPanel_Title_PartsSupplier_SupplierCode,
                            DataType = typeof(string)
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsMasterDataStatus.有效
                        }, new KeyValuesQueryItem {
                            ColumnName = "SupplierType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        }
                    }
                }
            };
        }
    }
}
