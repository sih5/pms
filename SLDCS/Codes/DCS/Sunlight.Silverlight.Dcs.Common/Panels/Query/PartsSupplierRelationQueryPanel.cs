﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsSupplierRelationQueryPanel : DcsQueryPanelBase {
        //private ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        public PartsSupplierRelationQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            //var dcsDomainContext = new DcsDomainContext();
            //dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    foreach(var entity in loadOp.Entities) {
            //        this.kvPartsSalesCategorys.Add(new KeyValuePair {
            //            Key = entity.Id,
            //            Value = entity.Name
            //        });
            //    }
            //}, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_PartsSupplierRelation,
                    EntityType = typeof(PartsSupplierRelation),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                            Title = Utils.GetEntityLocalizedName(typeof(PartsSupplier), "Code"),
                            ColumnName = "PartsSupplier.Code",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                            Title = Utils.GetEntityLocalizedName(typeof(PartsSupplier), "Name"),
                            ColumnName = "PartsSupplier.Name",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                            Title = Utils.GetEntityLocalizedName(typeof(SparePart), "Code"),
                            ColumnName = "SparePart.Code",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                            Title = Utils.GetEntityLocalizedName(typeof(SparePart), "Name"),
                            ColumnName = "SparePart.Name",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                            ColumnName = "SparePart.ReferenceCode",
                            DataType = typeof(string)
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsBaseDataStatus.有效
                        }
                        //, new KeyValuesQueryItem {
                        //    ColumnName = "PartsSalesCategoryId",
                        //    KeyValueItems = this.kvPartsSalesCategorys,
                        //    Title = CommonUIStrings.QueryPanel_QueryItem_Title_Branch_PartsSalesCategory                          
                        //}
                        , new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue =new []{
                                new DateTime(DateTime.Now.Year,DateTime.Now.Month,1), DateTime.Now.Date.AddDays(1)
                            }
                        }
                    }
                }
            };
        }
    }
}
