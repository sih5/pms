﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Web.Entities;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsSupplierWithBusinessCodeQueryPanel: DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "MasterData_Status","PartsSupplier_Type"
        };

        public PartsSupplierWithBusinessCodeQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_PartsSupplier,
                    EntityType = typeof(VirtualPartsSupplier),
                    QueryItems = new QueryItem[]{
                        new CustomQueryItem {
                            ColumnName = "Code",
                             DataType = typeof(string),
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierCode
                        }, new CustomQueryItem {
                            ColumnName = "Name",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierName,
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                            ColumnName = "SupplierCode",
                            Title = CommonUIStrings.QueryPanel_Title_PartsSupplier_SupplierCode,
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                            ColumnName = "BusinessCode",
                            Title = CommonUIStrings.QueryPanel_Title_VirtualPartsSupplier_BusinessCode,
                            DataType = typeof(string)
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsMasterDataStatus.有效,
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_Common_Status
                        }, new KeyValuesQueryItem {
                            ColumnName = "SupplierType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            Title=CommonUIStrings.QueryPanel_Title_VirtualPartsSupplier_BusinessType
                        }
                    }
                }
            };
        }
    }
}
