﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PersonnelQueryPanel : DcsQueryPanelBase {

        public PersonnelQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(Personnel),
                    Title = CommonUIStrings.QueryPanel_Title_Personnel,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Name"
                        }
                    }
                }
            };
        }
    }
}
