﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PersonnelSaleRegionQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };
        public PersonnelSaleRegionQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
           
        }
        private void Initialize() {
            
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_PersonnelSaleRegion,
                    EntityType = typeof(RegionPersonnelRelation),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PersonnelSaleRegion_RegionCode,
                            ColumnName="SalesRegion.RegionCode",
                            DataType=typeof(string)	 	
                        },new CustomQueryItem{
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PersonnelSaleRegion_RegionName,
                            ColumnName="SalesRegion.RegionName",
                            DataType=typeof(string)
                        },new CustomQueryItem
                        {
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PersonnelSaleRegion_PersonnelName,
                            ColumnName="Personnel.Name",
                            DataType=typeof(string)
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsBaseDataStatus.有效
                        }
                    }
            }
        };
        }
    }
}
        
