﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query
{
    public class PersonnelSupplierRelationQueryPanel : DcsQueryPanelBase
    {
        private readonly string[] kvNames = new[] {
           "BaseData_Status"
        };
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();

        public PersonnelSupplierRelationQueryPanel()
        {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize()
        {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesWithBranchIdQuery().Where(r=>r.BranchId==BaseApp.Current.CurrentUserData.EnterpriseId && r.Status==(int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp1 =>
            {
                if (loadOp1.HasError)
                    return;
                foreach (var partsSalesCategory in loadOp1.Entities)
                    this.kvPartsSalesCategorys.Add(new KeyValuePair
                    {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title =CommonUIStrings.QueryPanel_Title_PersonnelSupplierRelation,
                    EntityType = typeof(PersonnelSupplierRelation),
                    QueryItems = new[] {
                        new KeyValuesQueryItem {
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name,
                            ColumnName = "PartsSalesCategoryId",
                            KeyValueItems = this.kvPartsSalesCategorys
                        }, new QueryItem {
                            ColumnName = "PersonName",
                            Title=CommonUIStrings.QueryPanel_Title_PersonnelSupplierRelation_PersonName
                        }, new QueryItem {
                            ColumnName = "SupplierName",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierName,
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            IsExact = false,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsMasterDataStatus.有效
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title = CommonUIStrings.QueryPanel_Title_ProductStandard_CreateTime,
                            DefaultValue =new []{
                                new DateTime(DateTime.Now.Year,DateTime.Now.Month,1), DateTime.Now.Date.AddDays(1)
                            }
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "ModifyTime",
                             Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_ModifyTime
                            //,DefaultValue =new []{
                            //    new DateTime(DateTime.Now.Year,DateTime.Now.Month,1), DateTime.Now.Date.AddDays(1)
                            //}
                        }
                    }
                }
            };
        }
    }
}
