﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PriorityPackingShelvesDetailForReportQueryPanel: DcsQueryPanelBase {
        private ObservableCollection<KeyValuePair> kvFinishi = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = new[] {
           "PartPriority"
        };
        public PriorityPackingShelvesDetailForReportQueryPanel() {
            this.kvFinishi.Add(new KeyValuePair {
                Key = 1,
                Value = CommonUIStrings.Report_QueryPanel_KeyValue_Finishi
            });
            this.kvFinishi.Add(new KeyValuePair {
                Key = 2,
                Value = CommonUIStrings.Report_QueryPanel_KeyValue_PartFinishi
            });
            this.kvFinishi.Add(new KeyValuePair {
                Key = 3,
                Value = CommonUIStrings.Report_QueryPanel_KeyValue_UnFinishi
            });
            Initializer.Register(Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                      new QueryItemGroup {
                           UniqueId = "Common",
                           Title = CommonUIStrings.QueryPanel_Title_PriorityPackingShelvesDetailForReportQuery,
                           EntityType = typeof(PriorityPackingShelvesReport),
                           QueryItems =   new QueryItem[]{
                               new DateTimeQueryItem{
                                   Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricingDetail_Time,
                                   ColumnName="CreateTime",
                                   DefaultValue=DateTime.Now.Date
                              },new KeyValuesQueryItem {
                                Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPriorityBill_Priority,
                                ColumnName = "Priority",
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                             }, new KeyValuesQueryItem {
                                 Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_PackingStatus,
                                 KeyValueItems = this.kvFinishi,
                                 ColumnName = "PackingFinishStatus"
                              }, new KeyValuesQueryItem {
                                 Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsShelvesTaskFinish_ShelvesFinishStatus,
                                 ColumnName = "ShelvesFinishStatus",
                                  KeyValueItems = this.kvFinishi,
                              }, new DateTimeRangeQueryItem {
                                Title =  CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_ModifyTime,
                                ColumnName = "PackingFinishTime"                            
                             },new DateTimeRangeQueryItem {
                                Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsShelvesTaskFinish_ShelvesFinishTime,
                                ColumnName = "ShelvesFinishTime",
                           }
                           }
                      }
               };
        }
    }
}
