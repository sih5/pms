﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PriorityPackingShelvesSumForReportQueryPanel : DcsQueryPanelBase {
        private ObservableCollection<KeyValuePair> kvFinishi = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = new[] {
           "PartPriority"
        };
        public PriorityPackingShelvesSumForReportQueryPanel() {
            this.kvFinishi.Add(new KeyValuePair {
                Key = 1,
                Value = CommonUIStrings.DataGridView_Title_CentralPartsSalesOrder_Month
            });
            this.kvFinishi.Add(new KeyValuePair {
                Key = 2,
                Value = CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_Day
            });         
            Initializer.Register(Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                      new QueryItemGroup {
                           UniqueId = "Common",
                           Title = CommonUIStrings.QueryPanel_Title_PriorityPackingShelvesSumForReportQuery,
                           EntityType = typeof(PriorityPackingShelvesReport),
                           QueryItems =   new QueryItem[]{
                              new KeyValuesQueryItem {
                                Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPriorityBill_Priority,
                                ColumnName = "Priority",
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                             }, new KeyValuesQueryItem {
                                 Title = CommonUIStrings.QueryPanel_QueryItem_Title_MarketDptPersonnelRelation_Type,
                                 KeyValueItems = this.kvFinishi,
                                 ColumnName = "Type"
                              },new DateTimeRangeQueryItem{
                                   Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricingDetail_Time,
                                   ColumnName="CreateTime",
                                    DefaultValue = new[] {
                                    DateTime.Now.Date.AddDays(1 - DateTime.Now.Day), DateTime.Now.Date
                                }
                              }
                           }
                      }
               };
        }
    }
}
