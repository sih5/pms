﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class ProductViewForSelectQueryPanel : DcsQueryPanelBase {

        public ProductViewForSelectQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(ProductView),
                    Title = CommonUIStrings.QueryPanel_Title_Product,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_Product_Code
                        }, new QueryItem {
                            ColumnName = "TerraceName",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_Product_TerraceName
                        }, new QueryItem {
                            ColumnName = "BrandName",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_Product_BrandName
                        }, new QueryItem {
                            ColumnName = "SubBrandName",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_Product_SubBrandName
                        }, new QueryItem {
                            ColumnName = "ProductLine",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_Product_ProductLine
                        }, new QueryItem {
                            ColumnName = "ProductName",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_Product_ProductName
                        }, new CustomQueryItem {
                             ColumnName = "IsSetRelation",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_ProductView_IsSetRelation,
                            DataType = typeof(bool)  
                            }
                    }
                }
            };
        }
    }
}
