﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PurchaseCheckInboundReportQueryPanel: DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
            "ABCStrategy_Category"
        };
        private readonly ObservableCollection<KeyValuePair> kvSettleStatus = new ObservableCollection<KeyValuePair>();
        public PurchaseCheckInboundReportQueryPanel()
        {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetKeyValueItemsQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.Name == "PartsPurchaseSettle_Status").OrderBy(t => t.Key), loadOp =>
            {
                foreach (var entity in loadOp.Entities)
                {
                    this.kvSettleStatus.Add(new KeyValuePair
                    {
                        Key = entity.Key,
                        Value = entity.Value
                    });
                }
                this.kvSettleStatus.Add(new KeyValuePair {
                    Key = 0,
                    Value = CommonUIStrings.KeyValuItem_StaySettlement
                });
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_PartsInboundCheckBillForReportInBound,
                    EntityType = typeof(PartsInboundCheckBillForReport),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                          Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_Code,
                            ColumnName = "Code",
                            DataType = typeof(string)
                        },new CustomQueryItem {
                            ColumnName = "Invoice",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_Invoice,
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                             Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierName,
                            ColumnName = "PartsSupplierName",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierCode,
                            ColumnName = "PartsSupplierCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                            ColumnName = "ReferenceCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                            Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsSupplierRelation_SupplierPartCode,
                            ColumnName = "SupplierPartCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_SparepartCode,
                            ColumnName = "SparePartCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SparePartName,
                            ColumnName = "SparePartName",
                            DataType = typeof(string)
                        }, new KeyValuesQueryItem {
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SettleBillStatus,
                            ColumnName = "SettleBillStatus",
                            AllowMultiSelect=true,
                            KeyValueItems = kvSettleStatus
                        }, new KeyValuesQueryItem {
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartABC,
                            ColumnName = "PartABC",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_CreateTimeNew,
                        }
                        }
                    }
                };
            }, null);
        }
    }
}