﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PurchasePriceWarningOfExpiryReportQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
            "PurchasePriceType"
        };
        public PurchasePriceWarningOfExpiryReportQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title =CommonUIStrings.Report_Title_PurchasePriceWarningOfExpiryReportQuery,
                    EntityType = typeof(PurchasePriceWarningOfExpiry),
                    QueryItems = new[] {
                         new QueryItem {
                            ColumnName = "SparepartCode",
                            Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartCode
                        }, new QueryItem {
                            ColumnName = "SupplierPartCode",
                            Title=CommonUIStrings.DataGridView_ColumnItem_Title_PartsSupplierRelation_SupplierPartCode
                        }, new QueryItem {
                            ColumnName = "PartsSupplierCode",
                            Title=CommonUIStrings.DetailPanel_Title_PartsInsteadInfo_SupplierCode
                        },new QueryItem{
                            ColumnName="PartsSupplierName",
                            Title=CommonUIStrings.DetailPanel_Title_PartsInsteadInfo_SupplierName
                        }, new KeyValuesQueryItem {
                            Title = CommonUIStrings.DataGridView_PurchasePriceWarningOfExpiry_PriceType,
                            ColumnName = "PriceType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        },new QueryItem {
                            ColumnName = "DaysRemaining",
                            Title=CommonUIStrings.DataGridView_PurchasePriceWarningOfExpiry_DaysRemaining
                        }
                    }
                }
            };
        }
    }
}