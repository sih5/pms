﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class ReserveFactorMasterOrderQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvWarehouseName = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
            "BaseData_Status","ABCStrategy_Category"
        };
        public ReserveFactorMasterOrderQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }
        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetWarehousesQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效 && v.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp2 => {
                if(loadOp2.HasError)
                    return;
                this.kvWarehouseName.Clear();
                foreach(var warehouse in loadOp2.Entities) {
                    this.kvWarehouseName.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
                }
                this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title="储备系数查询",
                    EntityType = typeof(ReserveFactorMasterOrder),
                    QueryItems = new QueryItem[] {
                        new KeyValuesQueryItem {
                            ColumnName = "WarehouseId",
                            KeyValueItems = kvWarehouseName,
                            Title=CommonUIStrings.DataDeTailPanel_Text_Company_WarehouseName
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_Status,
                            DefaultValue=(int)DcsBaseDataStatus.有效
                        },new KeyValuesQueryItem {
                            ColumnName = "ABCStrategyId",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            Title="配件属性"
                        }, new QueryItem {
                            ColumnName = "ReserveCoefficient",
                            Title = "储备系数"
                        },new DateTimeRangeQueryItem {
                            Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_CreateTime,
                            ColumnName = "CreateTime",
                        },new DateTimeRangeQueryItem {
                            Title="修改时间",
                            ColumnName = "ModifyTime",
                        }
                    }
                }
            };
            }, null);
        }
    }
}
