﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class ResponsibleMembersQueryPanel : DcsQueryPanelBase {

        private readonly string[] kvNames = {
           "BaseData_Status","ResponsibleMembersResTem"
        };

        public ResponsibleMembersQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }


        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = "责任组人员维护",
                    EntityType = typeof(ResponsibleMember),
                    QueryItems = new QueryItem[] {
                        new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsBaseDataStatus.有效
                        } ,new KeyValuesQueryItem {
                            ColumnName = "ResTem",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            Title = "责任组"
                        }
                    }
                }
            };
        }
    }
}
