﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class ResponsibleUnitQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "MasterData_Status"
        };

        public ResponsibleUnitQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_ResponsibleUnitQuery,
                    EntityType = typeof(ResponsibleUnit),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Name",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_ResponsibleUnit_Name
                        }, new QueryItem {
                            ColumnName = "Code",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_ResponsibleUnit_Code
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsMasterDataStatus.有效
                        }
                    }
                }
            };
        }
    }
}
