﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class RolePermissionsQueryPanel : DcsQueryPanelBase {

        public RolePermissionsQueryPanel()
        {
            Initializer.Register(Initialize);
        }

        private void Initialize()
        {
            this.QueryItemGroups = new[] {
                      new QueryItemGroup {
                           UniqueId = "Common",
                           Title = CommonUIStrings.DataEditView_Title_RolePersonnelReportRole,
                           EntityType = typeof(RolePersonnelReport),
                           QueryItems = new QueryItem[]{
                           new QueryItem {
                              Title = "LV1",
                              ColumnName = "LV1"
                           },new QueryItem {
                              Title = "LV2",
                              ColumnName = "LV2"
                           },new QueryItem {
                              Title = "LV3",
                              ColumnName = "LV3"
                           }, new QueryItem {
                              Title=CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_ActionName,
                              ColumnName = "ActionName",
                           }, new QueryItem {
                              Title =  CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_RoleName,
                              ColumnName = "Name",
                             
                           }
                           }
                      }
               };
        }
    }
}

