﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class RolePersonnelForReportQueryPanel : DcsQueryPanelBase
    {
        
        public RolePersonnelForReportQueryPanel()
        {
            Initializer.Register(Initialize);
        }

        private void Initialize()
        {
            this.QueryItemGroups = new[] {
                      new QueryItemGroup {
                           UniqueId = "Common",
                           Title = CommonUIStrings.DataEditView_Title_RolePersonnelReportRoleList,
                           EntityType = typeof(RolePersonnelReport),
                           QueryItems = new QueryItem[]{
                           new QueryItem {
                              Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_RoleName,
                              ColumnName = "RoleName"
                           },new QueryItem {
                              Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_LoginId,
                              ColumnName = "LoginId"
                           }, new QueryItem {
                              Title =  CommonUIStrings.DataEditPanel_Text_PersonnelSupplierRelation_PersonCode,
                              ColumnName = "Name",
                             
                           }
                           }
                      }
               };
        }
    }
}
