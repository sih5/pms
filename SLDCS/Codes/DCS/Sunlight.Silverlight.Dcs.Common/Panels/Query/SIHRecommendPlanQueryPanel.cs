﻿using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class SIHRecommendPlanQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvWarehouseName = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
            "ABCStrategy_Category"
        };
        public SIHRecommendPlanQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }
        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetWarehousesQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效 && v.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp2 => {
                if(loadOp2.HasError)
                    return;
                this.kvWarehouseName.Clear();
                foreach(var warehouse in loadOp2.Entities) {
                    this.kvWarehouseName.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
                }
                this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = "推荐计划表查询",
                    EntityType = typeof(SIHRecommendPlan),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "SparePartCode",
                            Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartCode,
                        }, new QueryItem {
                            ColumnName = "SparePartName",
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_StorehouseCensus_SparePartName,
                        },new KeyValuesQueryItem {
                            ColumnName = "WarehouseId",
                            KeyValueItems = kvWarehouseName,
                            Title=CommonUIStrings.DataDeTailPanel_Text_Company_WarehouseName
                        },new KeyValuesQueryItem {
                            ColumnName = "PartABC",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title="配件属性"
                        },new DateTimeRangeQueryItem {
                            Title="结转时间",
                            ColumnName = "CarryTime",
                        }
                    }
                }
            };
            }, null);
        }
    }
}
