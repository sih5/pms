﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query
{
    public class SaleAdjustmentReportQueryPanel: DcsQueryPanelBase
    {
        private readonly string[] kvNames = new[] {
            "Company_Type"
        };
        public SaleAdjustmentReportQueryPanel()
        {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_SaleAdjustmentReportQuery,
                    EntityType = typeof(SaleAdjustmentReport),
                    QueryItems = new QueryItem[] {
                        new DateTimeRangeQueryItem {
                            ColumnName = "AdjustTime",
                            Title=CommonUIStrings.QueryPanel_Title_SaleAdjustmentReport_AdjustTime,
                        }, new KeyValuesQueryItem {
                            Title=CommonUIStrings.Report_Title_SaleAdjustment_CustomerType,
                            ColumnName = "CustomerType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        },
                        new CustomQueryItem {
                            Title = CommonUIStrings.DataEditPanel_Text_Agency_Name,
                            ColumnName = "AgencyName",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_SubmitCompanyName,
                            ColumnName = "CustomerName",
                            DataType = typeof(string)
                        },
                        new CustomControlQueryItem {
                            ColumnName = "SparePartCode",
                            Title =CommonUIStrings.Report_Title_SaleAdjustment_SparePartCode,
                            CreateControl=type =>new Controls.MultipleTextQueryControl(type,"SparePartCode"),
                            IsExact =true
                        }
                    }
                }
            };
        }
    }
}