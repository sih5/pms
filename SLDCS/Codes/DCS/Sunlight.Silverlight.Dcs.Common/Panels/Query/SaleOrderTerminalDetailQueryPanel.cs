﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class SaleOrderTerminalDetailQueryPanel : DcsQueryPanelBase {
        public SaleOrderTerminalDetailQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title =CommonUIStrings.QueryPanel_Title_SaleOrderTerminalDetailQuery,
                    EntityType = typeof(SaleOrderTerminalSatisfactionRate),
                    QueryItems = new[] {
                         new QueryItem {
                            ColumnName = "MarketingDepartmentName",
                            Title=CommonUIStrings.DataEditPanel_Text_Company_Market
                        }, new QueryItem {
                            ColumnName = "CenterName",
                            Title=CommonUIStrings.DataEditPanel_Text_Agency_Name
                        }, new QueryItem {
                            ColumnName = "DealerName",
                            Title=CommonUIStrings.Report_QueryPanel_Title_DealerNames
                        },new DateTimeRangeQueryItem {
                            ColumnName = "SubmitTime",
                            Title=CommonUIStrings.Report_QueryPanel_Title_SubmitTime,
                        }
                    }
                }
            };
        }
    }
}