﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class SaleOrderTerminalRateSumForReportQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvYears = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvUnits = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = new[] {
            "AccountPeriodMonth"
        };
        public SaleOrderTerminalRateSumForReportQueryPanel()
        {
            this.KeyValueManager.Register(this.kvNames);
            this.kvUnits.Add(new KeyValuePair {
                Key = 1,
                Value = CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_MarketingDepartmentName
            });
            this.kvUnits.Add(new KeyValuePair {
                Key = 2,
                Value = CommonUIStrings.DataEditView_Notification_Type_Agency
            });
            this.kvUnits.Add(new KeyValuePair {
                Key = 3,
                Value = CommonUIStrings.QueryPanel_Title_QueryItem_Dealer
            });
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
             var domainContext = new DcsDomainContext();
             domainContext.Load(domainContext.GetKeyValueItemsQuery().Where(r => r.Name == "AccountPeriodYear" && r.Status == (int)DcsAccountPeriodStatus.有效).OrderBy(t=>t.Key), loadOp => {
                foreach (var entity in loadOp.Entities)
                {
                    this.kvYears.Add(new KeyValuePair
                    {
                        Key =Int32.Parse(entity.Value),
                        Value = entity.Value
                    });
                }
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title =CommonUIStrings.QueryPanel_Title_SaleOrderTerminalRateSumForReportQuery,
                    EntityType = typeof(SaleOrderTerminalSatisfactionRate),
                    QueryItems = new[] {
                          new KeyValuesQueryItem {
                            ColumnName = "Year",
                            Title=CommonUIStrings.QueryPanel_Title_QueryItem_Year,
                            KeyValueItems = this.kvYears
                        }, new KeyValuesQueryItem {
                            ColumnName = "Month",
                            Title=CommonUIStrings.DataGridView_Title_CentralPartsSalesOrder_Month,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new KeyValuesQueryItem {
                            ColumnName = "Unit",
                            Title=CommonUIStrings.QueryPanel_Title_CentralPartsSalesOrder_StatisticalUnit,
                            KeyValueItems = this.kvUnits
                        },new QueryItem {
                            ColumnName = "MarketingDepartmentName",
                            Title=CommonUIStrings.DataEditPanel_Text_Company_Market
                        }, new QueryItem {
                            ColumnName = "CenterName",
                            Title=CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_CenterName
                        }, new QueryItem {
                            ColumnName = "DealerName",
                            Title=CommonUIStrings.Report_QueryPanel_Title_DealerNames
                        }
                    }
                }
            };
          }, null);
        }
    }
}
