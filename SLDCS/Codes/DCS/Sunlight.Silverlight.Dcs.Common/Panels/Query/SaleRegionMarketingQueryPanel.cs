﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class SaleRegionMarketingQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        public SaleRegionMarketingQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_SaleRegionMarketing,
                    EntityType = typeof(RegionMarketDptRelation),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_SaleRegionMarketing_RegionCode,
                            ColumnName="SalesRegion.RegionCode",
                            DataType=typeof(string)
                        },new CustomQueryItem{
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_SaleRegionMarketing_RegionName,
                            ColumnName="SalesRegion.RegionName",
                            DataType=typeof(string)
                        },new CustomQueryItem
                        {
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_SaleRegionMarketing_Code,
                            ColumnName="MarketingDepartment.Code",
                             DataType=typeof(string)
                        },new CustomQueryItem
                        {
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_SaleRegionMarketing_Name,
                            ColumnName="MarketingDepartment.Name",
                             DataType=typeof(string)
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsBaseDataStatus.有效
                        }
                    }
                }
            };
        }
    }
}

