﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class SalesSatisfactionCentralQueryPanel : DcsQueryPanelBase {
        public SalesSatisfactionCentralQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_SalesSatisfactionRate,
                    EntityType = typeof(SalesSatisfactionRate),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Name",
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_SubmitCompanyName,
                        }
                        ,new DateTimeRangeQueryItem {
                            ColumnName = "ApproveTime",
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_ApproveTimeFir,
                            DefaultValue = new[] {
                               DateTime.Now.AddDays(-7).Date, DateTime.Now.Date
                            }
                        }
                    }
                }
            };
        }
    }
}
