﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class SettlementInvoiceReportQueryPanel : DcsQueryPanelBase {
        public SettlementInvoiceReportQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_SettlementInvoice,
                    EntityType = typeof(SettlementInvoice),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_PartsOutboundBillCode,
                            ColumnName = "PartsOutboundBillCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                             Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SettleBillCode,
                            ColumnName = "Code",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_Invoice,
                            ColumnName = "InvoiceNumber",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_SubmitCompanyName,
                            ColumnName = "SubmitCompanyName",
                            DataType = typeof(string)
                        },new DateTimeRangeQueryItem {
                            ColumnName = "PartsOutboundBillCreateTime",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_SettlementInvoice_PartsOutboundBillCreateTime,
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_SettlementInvoice_CreateTime,
                        }
                        }
                    }
                };
        }
    }
}