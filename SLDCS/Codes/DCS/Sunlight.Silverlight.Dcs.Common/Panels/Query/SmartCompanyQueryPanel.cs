﻿
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class SmartCompanyQueryPanel  : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };
        public SmartCompanyQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }
        private void Initialize() {
                this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title="智能订货企业配置查询",
                    EntityType = typeof(SmartCompany),
                    QueryItems = new QueryItem[] {
                         new QueryItem {
                            ColumnName = "CompanyCode",
                           Title ="订货企业编号",
                        }, new QueryItem {
                            ColumnName = "CompanyName",
                            Title="订货企业名称"
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_Status
                        }
                        ,new DateTimeRangeQueryItem {
                               Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_CreateTime,
                              ColumnName = "CreateTime",
                        }
                    }
                }
            };
        }
    }
}