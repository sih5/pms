﻿
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class SmartProcessMethodQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvWarehouseName = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvOrderProcessMethod = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
            "BaseData_Status"
        };
        public SmartProcessMethodQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }
        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetWarehousesQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效 && v.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp2 => {
                if(loadOp2.HasError)
                    return;
                this.kvWarehouseName.Clear();
                foreach(var warehouse in loadOp2.Entities) {
                    this.kvWarehouseName.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
                }
                dcsDomainContext.Load(dcsDomainContext.GetKeyValueItemsQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效 && v.Name == "PartsSalesOrderProcessDetail_ProcessMethod" && (v.Key == (int)DcsPartsSalesOrderProcessDetailProcessMethod.供应商直发 || v.Key == (int)DcsPartsSalesOrderProcessDetailProcessMethod.转中心库 || v.Key == (int)DcsPartsSalesOrderProcessDetailProcessMethod.转中心库采购 || v.Key == (int)DcsPartsSalesOrderProcessDetailProcessMethod.本部处理)), LoadBehavior.RefreshCurrent, loadOp3 => {
                    if(loadOp3.HasError)
                        return;
                    this.kvOrderProcessMethod.Clear();
                    foreach(var warehouse in loadOp3.Entities) {
                        this.kvOrderProcessMethod.Add(new KeyValuePair {
                            Key = warehouse.Key,
                            Value = warehouse.Value
                        });
                    }
                    this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title="智能订货处理方式配置查询",
                    EntityType = typeof(SmartProcessMethod),
                    QueryItems = new QueryItem[] {
                        new KeyValuesQueryItem {
                            ColumnName = "WarehouseId",
                            KeyValueItems = kvWarehouseName,
                            Title=CommonUIStrings.DataDeTailPanel_Text_Company_WarehouseName
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_Status
                        },new KeyValuesQueryItem {
                            ColumnName = "OrderProcessMethod",
                            KeyValueItems = kvOrderProcessMethod,
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_OrderProcessMethod
                        },new DateTimeRangeQueryItem {
                               Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_CreateTime,
                              ColumnName = "CreateTime",
                        }
                    }
                }
            };
                }, null);
            }, null);
        }
    }
}
