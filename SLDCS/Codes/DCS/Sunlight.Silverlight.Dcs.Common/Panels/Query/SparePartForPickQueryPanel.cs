﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class SparePartForPickQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "MasterData_Status","SparePart_PartType"
        };
        private readonly ObservableCollection<KeyValuePair> kvNameType = new ObservableCollection<KeyValuePair>();

        public SparePartForPickQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetKeyValueItemsQuery().Where(r => r.Name == "SparePart_PartType" && r.Status == (int)DcsAccountPeriodStatus.有效 && (r.Key == (int)DcsSparePartPartType.包材 || r.Key == (int)DcsSparePartPartType.包材辅料)).OrderBy(t => t.Key), loadOp => {
                foreach(var entity in loadOp.Entities) {
                    this.kvNameType.Add(new KeyValuePair {
                        Key = entity.Key,
                        Value = entity.Value
                    });
                }
                this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(SparePart),
                    Title = CommonUIStrings.QueryPanel_Title_SparePart,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        }, new QueryItem {
                            ColumnName = "Name"
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsMasterDataStatus.有效
                        }, new KeyValuesQueryItem {
                            ColumnName = "PartType",
                            KeyValueItems = kvNameType,
                            DefaultValue = (int)DcsSparePartPartType.包材
                        }                       
                        ,new QueryItem{
                            ColumnName="ReferenceCode",
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                        }                     
                        ,new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                        },new DateTimeRangeQueryItem {
                            ColumnName = "ModifyTime"
                        }
                        
                    }
                }
            };
            }, null);
        }
    }
}
