﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class SparePartForQueryQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
           "SparePart_MeasureUnit"
        };

        public SparePartForQueryQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(SparePart),
                    Title = CommonUIStrings.QueryPanel_Title_SparePart,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code"
                        }, new QueryItem {
                            ColumnName = "Name"
                        }, new QueryItem {
                            ColumnName = "ExchangeIdentification",
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsExchangeGroup_ExchangeCode
                        }, new QueryItem {
                            ColumnName = "ExGroupCode",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsExchangeGroup_ExGroupCode
                        }
                        ,new KeyValuesQueryItem{
                            ColumnName="MeasureUnit",
                            KeyValueItems=this.KeyValueManager[this.kvNames[0]]
                        },new QueryItem{
                            ColumnName="Specification"
                        },new QueryItem{
                          ColumnName = "ReferenceCode",
                          Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_ReferenceCode
                        }
                    }
                }
            };
        }
    }
}
