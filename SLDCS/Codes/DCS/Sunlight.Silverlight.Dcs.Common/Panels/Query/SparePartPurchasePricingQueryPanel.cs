﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class SparePartPurchasePricingQueryPanel : DcsQueryPanelBase {
        public SparePartPurchasePricingQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.QueryPanel_Title_SparePartNewPro,
                    EntityType = typeof(SparePart),
                    QueryItems = new[] {
                        new CustomControlQueryItem { 
                            ColumnName = "Code", 
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                            CreateControl=type =>new Controls.MultipleTextQueryControl(type,"Code") 
                        }, new QueryItem {
                            Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                            ColumnName = "Name"
                        },new CustomQueryItem {
                            ColumnName = "Isprice",
                            DataType = typeof(bool),
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Isprice,
                            DefaultValue = false
                        }, new QueryItem {
                           Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                            ColumnName = "ReferenceCode"
                        }, new DateTimeRangeQueryItem {
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_PartCreateTime,
                            ColumnName = "PartCreateTime"
                        }, new DateTimeRangeQueryItem {
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_PCreateTime,
                            ColumnName = "CreateTime"
                        }
                    }
                }
            };
        }
    }
}
