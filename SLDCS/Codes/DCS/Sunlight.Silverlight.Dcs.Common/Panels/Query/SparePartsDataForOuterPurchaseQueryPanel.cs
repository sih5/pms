﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;


namespace Sunlight.Silverlight.Dcs.Common.Panels.Query
{
    public class SparePartsDataForOuterPurchaseQueryPanel: DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "MasterData_Status","SparePart_MeasureUnit"
        };

        public SparePartsDataForOuterPurchaseQueryPanel()
        {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(SparePartOuterPurchase),
                    Title = CommonUIStrings.QueryPanel_Title_SparePart,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        }, new QueryItem {
                            ColumnName = "Name"
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsMasterDataStatus.有效
                        }
                        //,new KeyValuesQueryItem{
                        //    ColumnName="MeasureUnit",
                        //    KeyValueItems=this.KeyValueManager[this.kvNames[1]]
                        //},new QueryItem{
                        //    ColumnName="Specification"
                        //}
                        ,new QueryItem{
                            ColumnName="ReferenceCode",
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_ReferenceCode
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                        },new DateTimeRangeQueryItem {
                            ColumnName = "ModifyTime"
                        }
                    }
                }
            };
        }
    }
}
