﻿
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class StarStatisticsQueryPanel : DcsQueryPanelBase {
        private readonly StarStatisticsQueryPanelViewModel viewModel = new StarStatisticsQueryPanelViewModel();
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvChannelCapability = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = {
            "Company_CityLevel"
        };

        public StarStatisticsQueryPanel() {
            KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.viewModel.Initialize);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetChannelCapabilitiesQuery().Where(entity => entity.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                if(loadOp.HasError)
                    return;
                this.kvChannelCapability.Clear();
                foreach(var item in loadOp.Entities) {
                    kvChannelCapability.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.Name
                    });

                }
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetKeyValueItemsQuery().Where(r => r.Name == "BranchList"), LoadBehavior.RefreshCurrent, loadOpKey => {
                if(loadOpKey.HasError) {
                    if(!loadOpKey.IsErrorHandled)
                        loadOpKey.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOpKey);
                }
                this.kvBranches.Clear();
                foreach(var branch in loadOpKey.Entities)
                    this.kvBranches.Add(new KeyValuePair {
                        Key = branch.Key,
                        Value = branch.Value
                    });
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetCompaniesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var company = loadOp.Entities.SingleOrDefault();
                if(company == null)
                    return;
                this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = CommonUIStrings.QueryPanel_Title_WarehouseStar,
                        EntityType = typeof(Warehouse),
                        QueryItems = new QueryItem[] {
                            new ComboQueryItem {
                                Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_BranchName,
                                ColumnName = "BranchCode",
                                SelectedValuePath = "Key",
                                DisplayMemberPath = "Value",
                                ItemsSource = this.viewModel.KvKeyValueItems,
                                SelectedItemBinding = new Binding("SelectedKeyValueItem") {
                                    Source = this.viewModel,
                                    Mode = BindingMode.TwoWay
                                }
                            },
                            new ComboQueryItem {
                                ColumnName = "PartsSalesCategoryId",
                                Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name,
                                SelectedValuePath = "Id",
                                DisplayMemberPath = "Name",
                                ItemsSource = this.viewModel.PartsSalesCategorys,
                                SelectedItemBinding = new Binding("SelectedPartsSalesCategory") {
                                    Source = this.viewModel,
                                    Mode = BindingMode.TwoWay
                                }

                            },
                            new KeyValuesQueryItem {
                                ColumnName = "CityLevel",
                                KeyValueItems = this.KeyValueManager[kvNames[0]],
                                Title = CommonUIStrings.QueryPanel_QueryItem_Title_Warehouse_CityLevel
                            },
                            new KeyValuesQueryItem {
                                ColumnName = "ChannelCapabilityId",
                                Title = CommonUIStrings.QueryPanel_QueryItem_Title_Warehouse_ChannelCapabilityId,
                                KeyValueItems = this.kvChannelCapability
                            },
                            new ComboQueryItem {
                                ColumnName = "Grade",
                                Title = CommonUIStrings.QueryPanel_QueryItem_Title_Warehouse_Grade,
                                ItemsSource = this.viewModel.BrandGradeMessages,
                                SelectedValuePath = "Grade",
                                DisplayMemberPath = "Grade",
                            }
                            //new CustomQueryItem {
                            //    Title = "星级类别",
                            //    ColumnName = "GradeCoefficientId",
                            //    DataType = typeof(string)
                            //}
                        }
                    }
                };
            }, null);
        }
    }

    public class StarStatisticsQueryPanelViewModel : ViewModelBase {

        private ObservableCollection<PartsSalesCategoryView> kvPartsSalesCategorys;
        private PartsSalesCategoryView partsSalesCategoryView;
        private PagedCollectionView partsSalesCategorys;
        private PagedCollectionView gradeCoefficients;
        private ObservableCollection<KeyValueItem> keyValueItems;
        private KeyValueItem keyValueItem;

        public readonly ObservableCollection<BrandGradeMessage> allBrandGradeMessages = new ObservableCollection<BrandGradeMessage>();
        public ObservableCollection<PartsSalesCategoryView> KvPartsSalesCategorys {
            get {
                if(this.kvPartsSalesCategorys == null)
                    this.kvPartsSalesCategorys = new ObservableCollection<PartsSalesCategoryView>();
                return kvPartsSalesCategorys;
            }
        }
        public ObservableCollection<KeyValueItem> KvKeyValueItems {
            get {
                if(this.keyValueItems == null)
                    this.keyValueItems = new ObservableCollection<KeyValueItem>();
                return keyValueItems;
            }
        }
        public KeyValueItem SelectedKeyValueItem {
            get {
                return this.keyValueItem;
            }
            set {
                if(this.keyValueItem == value)
                    return;
                this.keyValueItem = value;
                this.NotifyOfPropertyChange("SelectedKeyValueItem");
                this.PartsSalesCategorys.Refresh();
            }
        }
        public PagedCollectionView PartsSalesCategorys {
            get {
                if(this.partsSalesCategorys == null) {
                    this.partsSalesCategorys = new PagedCollectionView(this.KvPartsSalesCategorys);
                    this.partsSalesCategorys.Filter = o => ((PartsSalesCategoryView)o).BranchCode == (this.SelectedKeyValueItem != null ? this.SelectedKeyValueItem.Value : String.Empty);
                }
                return this.partsSalesCategorys;
            }
        }
        public PartsSalesCategoryView SelectedPartsSalesCategory {
            get {
                return this.partsSalesCategoryView;
            }
            set {
                if(this.partsSalesCategoryView == value)
                    return;
                this.partsSalesCategoryView = value;
                this.NotifyOfPropertyChange("PartsSalesCategoryView");
                this.BrandGradeMessages.Refresh();
            }
        }
        public PagedCollectionView BrandGradeMessages {
            get {
                if(this.gradeCoefficients == null) {
                    this.gradeCoefficients = new PagedCollectionView(this.allBrandGradeMessages);
                    this.gradeCoefficients.Filter = o => ((BrandGradeMessage)o).BrandId == (this.SelectedPartsSalesCategory != null ? this.SelectedPartsSalesCategory.Id : int.MinValue);
                }
                return this.gradeCoefficients;
            }
        }

        public override void Validate() {
            //
        }

        public void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetBrandGradeMessagesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach (var brandGradeMessage in loadOp.Entities)
                    this.allBrandGradeMessages.Add(brandGradeMessage);
            }, null);
            if(BaseApp.Current.CurrentUserData.UserCode == "ReportAdmin") {
                domainContext.Load(domainContext.GetPartsSalesCategoryViewsQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError)
                        return;
                    this.KvPartsSalesCategorys.Clear();
                    foreach(var partsSalesCategories in loadOp.Entities)
                        this.KvPartsSalesCategorys.Add(partsSalesCategories);
                }, null);
            } else {
                domainContext.Load(domainContext.GetPartsSalesCategoryViewsQuery().Where(e => e.BranchCode == BaseApp.Current.CurrentUserData.EnterpriseCode), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError)
                        return;
                    this.KvPartsSalesCategorys.Clear();
                    foreach(var partsSalesCategories in loadOp.Entities)
                        this.KvPartsSalesCategorys.Add(partsSalesCategories);
                }, null);
            }
            domainContext.Load(domainContext.GetKeyValueItemsQuery().Where(r => r.Name == "BranchList" && r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvKeyValueItems.Clear();
                foreach(var item in loadOp.Entities)
                    this.KvKeyValueItems.Add(item);
            }, null);
        }
    }

}