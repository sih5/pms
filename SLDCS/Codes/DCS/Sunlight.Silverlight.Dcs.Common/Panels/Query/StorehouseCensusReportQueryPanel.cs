﻿
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class StorehouseCensusReportQueryPanel : DcsQueryPanelBase {
        public StorehouseCensusReportQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(StorehouseCensus),
                    Title = CommonUIStrings.QueryPanel_Title_StorehouseCensus,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "SparePartCode",
                            Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartCode,
                        }, new QueryItem {
                            ColumnName = "SparePartName",
                            Title=CommonUIStrings.QueryPanel_QueryItem_Title_StorehouseCensus_SparePartName
                        }
                        ,new QueryItem{
                            ColumnName="ReferenceCode",
                            Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                        }
                    
                    }
                }
            };
        }
    }
}

