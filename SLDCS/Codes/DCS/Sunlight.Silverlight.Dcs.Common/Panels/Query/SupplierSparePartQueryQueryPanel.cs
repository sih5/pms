﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class SupplierSparePartQueryQueryPanel : DcsQueryPanelBase {
        private ObservableCollection<KeyValuePair> kvPartsSalesCategory = new ObservableCollection<KeyValuePair>();

        public SupplierSparePartQueryQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Entities.Count() == default(int))
                    return;
                foreach(var items in loadOp.Entities) {
                    this.kvPartsSalesCategory.Add(new KeyValuePair {
                        Key = items.Id,
                        Value = items.Name,
                        UserObject = items
                    });
                }
            }, null);
            this.QueryItemGroups = new[]{
                new QueryItemGroup{
                    UniqueId = "Common",
                    Title = CommonUIStrings.DataManagementView_Title_SupplierSparePart,
                    EntityType=typeof(PartsSupplierRelationWithPurchasePrice),
                    QueryItems=new QueryItem[]{
                        new  KeyValuesQueryItem{
                            ColumnName = "PartsSalesCategoryId",
                            KeyValueItems = this.kvPartsSalesCategory,
                            Title = CommonUIStrings.DataEditPanel_Text_PartsBranch_PartsSalesCategory
                        },new CustomQueryItem{
                            ColumnName = "ReferenceCode",
                            DataType=typeof(string),
                            Title = CommonUIStrings.QueryPanel_Title_SparePart_ReferenceCode
                        },new CustomQueryItem{
                            ColumnName = "SupplierPartCode",
                            DataType=typeof(string),
                             Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsSupplierRelation_SupplierPartCode,
                        },new CustomQueryItem{
                            ColumnName = "SparePartName",
                            DataType=typeof(string),
                            Title = CommonUIStrings.DataGridView_ColumnItem_Title_SparePart_Name
                        },new CustomQueryItem{
                            ColumnName = "Time",
                            DataType = typeof(DateTime),
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_AvailibleTime
                        }
                    }
                }
            };
        }
    }
}
