﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class TiledRegionQueryPanel : DcsQueryPanelBase {
        public TiledRegionQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(TiledRegion),
                    Title = CommonUIStrings.QueryPanel_Title_TiledRegion,
                    QueryItems = new[] {
                        new CustomQueryItem {
                            Title = CommonUIStrings.QueryPanel_QueryItem_Title_TiledRegion_Name,
                            ColumnName = "Name",
                            DataType = typeof(string)
                        }
                    }
                }
            };
        }
    }
}
