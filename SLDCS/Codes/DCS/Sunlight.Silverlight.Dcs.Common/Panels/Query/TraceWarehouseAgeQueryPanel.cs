﻿using System.Collections.ObjectModel;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class TraceWarehouseAgeQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "TraceProperty"
        };
        private readonly ObservableCollection<KeyValuePair> kvType = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> companyType = new ObservableCollection<KeyValuePair>();

        public TraceWarehouseAgeQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.kvType.Add(new KeyValuePair {
                Key = 1,
                Value = "90天内"
            });
            this.kvType.Add(new KeyValuePair {
                Key = 2,
                Value = "90-180天内"
            });
            this.kvType.Add(new KeyValuePair {
                Key = 3,
                Value = "181-360天内"
            });
            this.kvType.Add(new KeyValuePair {
                Key = 4,
                Value = "361-720天内"
            });
            this.kvType.Add(new KeyValuePair {
                Key = 5,
                Value = "721-1080天内"
            });
            this.kvType.Add(new KeyValuePair {
                Key = 6,
                Value = "更早"
            });
            this.companyType.Add(new KeyValuePair {
                Key = 1,
                Value = "配件公司"
            });
            this.companyType.Add(new KeyValuePair {
                Key = 2,
                Value = "中心库"
            });
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = "配件追溯码库位库龄管理查询",
                    EntityType = typeof(TraceWarehouseAge),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                            ColumnName = "SparePartCode",
                            Title="配件编号",
                            DataType=typeof(string)
                        }, new CustomQueryItem {
                            ColumnName = "WarehouseName",
                            Title="仓库名称",
                            DataType=typeof(string)
                        }, new CustomQueryItem {
                            ColumnName = "WarehouseAreaCode",
                            Title="库区编号",
                            DataType=typeof(string)
                        }, new CustomQueryItem {
                            ColumnName = "StorehouseAreaCode",
                            Title="库位编号",
                            DataType=typeof(string)
                        }, new KeyValuesQueryItem {
                            ColumnName = "TraceProperty",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title="追溯属性"
                        }, new CustomQueryItem {
                            ColumnName = "TraceCode",
                            Title="追溯码",
                            DataType=typeof(string)
                        }, new KeyValuesQueryItem {
                            ColumnName = "KvAge",
                            KeyValueItems = this.kvType,
                            Title="库龄"
                        }, new KeyValuesQueryItem {
                            ColumnName = "CompanyType",
                            KeyValueItems = this.companyType,
                            Title="企业类型"
                        }, new QueryItem {
                            ColumnName = "GreaterThanZero",
                            Title="库存是否大于0"
                        }
                    }
                }
            };
        }
    }
}
