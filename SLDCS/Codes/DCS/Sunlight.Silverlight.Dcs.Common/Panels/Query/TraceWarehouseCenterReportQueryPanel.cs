﻿using System.Collections.ObjectModel;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;


namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class TraceWarehouseCenterReportQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "AccurateTraceStatus"
        };


        public TraceWarehouseCenterReportQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);          
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = "中心库配件入库精确码物流跟踪",
                    EntityType = typeof(TraceWarehouseAge),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                            ColumnName = "SparePartCode",
                            Title="配件图号",
                            DataType=typeof(string)
                        }, new CustomQueryItem {
                            ColumnName = "WarehouseName",
                            Title="仓库名称",
                            DataType=typeof(string)
                        }, new CustomQueryItem {
                            ColumnName = "TraceCode",
                            Title="追溯码",
                            DataType=typeof(string)
                        }, new CustomQueryItem {
                            ColumnName = "SIHLabelCode",
                            Title="SIH标签码",
                            DataType=typeof(string)
                        }, new CustomQueryItem {
                            ColumnName = "OriginalRequirementBillCode",
                            Title="原始单据编号",
                            DataType=typeof(string)
                        }, new CustomQueryItem {
                            ColumnName = "SourceCode",
                            Title="收货单号",
                            DataType=typeof(string)
                        }, new CustomQueryItem {
                            ColumnName = "InBoundCode",
                            Title="入库单号",
                            DataType=typeof(string)
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "InBoundDate",
                            Title="入库时间"
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title="状态"
                        }
                    }
                }
            };
        }
    }
}
