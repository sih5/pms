﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class TraceWarehouseOutCenterQueryPanel: DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        public TraceWarehouseOutCenterQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
                this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = "中心库配件出库精确码物流跟踪",
                    EntityType = typeof(TraceWarehouseAge),
                    QueryItems = new QueryItem[] {
                         new CustomQueryItem {
                            ColumnName="WarehouseName",
                            Title="仓库名称",
                             DataType=typeof(string)
                        }, new CustomQueryItem {
                            ColumnName = "SparePartCode",
                            Title="配件图号",
                            DataType=typeof(string)
                        }, new CustomQueryItem {
                            ColumnName = "TraceCode",
                            Title="追溯码",
                            DataType=typeof(string)
                        },  new CustomQueryItem {
                            ColumnName = "OriginalRequirementBillCode",
                            Title="中心库原始单据编号",
                            DataType=typeof(string)
                        },new CustomQueryItem {
                            ColumnName = "OutCode",
                            Title="中心库出库单号",
                            DataType=typeof(string)
                        }, new CustomQueryItem {
                            ColumnName = "CounterpartCompanyCode",
                            Title="收货单位编号",
                            DataType=typeof(string)
                        },new DateTimeRangeQueryItem {
                            ColumnName = "OutTime",
                            Title="中心库出库时间"
                        }
                    }
                }
            };
        }
    }
}
