﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class TraceWarehouseOutSIHReportQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "AccurateTraceStatus","Parts_OutboundType"
        };
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        public TraceWarehouseOutSIHReportQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }
        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetWarehousesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId).OrderBy(t => t.Name), loadOp => {
                foreach(var entity in loadOp.Entities) {
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name
                    });
                }
                this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = "配件出库精确码物流跟踪",
                    EntityType = typeof(TraceWarehouseAge),
                    QueryItems = new QueryItem[] {
                         new KeyValuesQueryItem {
                            ColumnName="WarehouseId",
                            KeyValueItems =this.kvWarehouses,
                            Title="仓库名称",
                        }, new CustomQueryItem {
                            ColumnName = "SparePartCode",
                            Title="配件图号",
                            DataType=typeof(string)
                        }, new CustomQueryItem {
                            ColumnName = "TraceCode",
                            Title="追溯码",
                            DataType=typeof(string)
                        },  new CustomQueryItem {
                            ColumnName = "OriginalRequirementBillCode",
                            Title="原始单据编号",
                            DataType=typeof(string)
                        },new CustomQueryItem {
                            ColumnName = "OutCode",
                            Title="出库单号",
                            DataType=typeof(string)
                        }, new CustomQueryItem {
                            ColumnName = "CounterpartCompanyName",
                            Title="客户名称",
                            DataType=typeof(string)
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title="状态"
                        },new KeyValuesQueryItem {
                            ColumnName = "OutboundType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            Title="出库类型"
                        },new DateTimeRangeQueryItem {
                            ColumnName = "OutTime",
                            Title="出库时间",
                        }
                    }
                }
            };
            }, null);
        }
    }
}
