﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class VirtualSpartPartByClassQueryPanel : DcsQueryPanelBase {
        public VirtualSpartPartByClassQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title ="配件查询",
                        EntityType = typeof(VirtualPartsStock),
                        QueryItems = new QueryItem[] {
                            new CustomQueryItem{
                                ColumnName = "CompanyCode",
                                Title = CommonUIStrings.DataDeTailPanel_Text_Company_Code,
                                DataType = typeof(string)
                            }, new CustomQueryItem {
                                ColumnName = "CompanyName",
                                Title = CommonUIStrings.DataDeTailPanel_Text_Company_Name,
                                DataType = typeof(string)
                            }, new CustomQueryItem {
                                ColumnName = "SparePartCode",
                                Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_SparePartCode,
                                DataType = typeof(string)
                            }, new CustomQueryItem {
                                ColumnName = "SparePartName",
                                Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_SparePartName,
                                DataType = typeof(string)
                            }
                        }
                    }
                };
        }
    }
}