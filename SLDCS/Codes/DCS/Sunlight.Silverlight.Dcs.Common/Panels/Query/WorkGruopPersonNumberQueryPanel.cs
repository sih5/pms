﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class WorkGruopPersonNumberQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "WorkGroup"
        };

        public WorkGruopPersonNumberQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = CommonUIStrings.Report_Title_WorkGruopPersonNumberManagementQuery,
                    EntityType = typeof(WorkGruopPersonNumber),
                    QueryItems = new[] {
                       new KeyValuesQueryItem {
                            ColumnName = "WorkGroup",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title=CommonUIStrings.Report_Title_WorkGruopPersonNumber_WorkGruop
                        }
                    }
                }
            };
        }
    }
}
