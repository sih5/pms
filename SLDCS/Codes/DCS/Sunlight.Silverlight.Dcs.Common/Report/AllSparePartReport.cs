﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report
{
    [PageMeta("Report", "Sale", "AllSparePartReport", ActionPanelKeys = new[] {
        "AllSparePartReport"
    })]
    public class AllSparePartReport : DcsDataManagementViewBase
    {
        private DataGridViewBase dataGridView;
        public AllSparePartReport()
        {
            this.Initializer.Register(this.Initialize);
            this.Title =CommonUIStrings.QueryPanel_Title_AllSparePartReport;
        }
        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("AllSparePartReport"));
            }
        }
        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "AllSparePartReport"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem)
        {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId)
            {
                case CommonActionKeys.MERGEEXPORT:
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if (filterItem == null)
                        return;
                    var code = filterItem.Filters.Single(e => e.MemberName == "Code").Value as string;
                    var referenceCode = filterItem.Filters.Single(e => e.MemberName == "ReferenceCode").Value as string;
                    var partABC = filterItem.Filters.Single(e => e.MemberName == "PartABC").Value as int?;
                    var increaseRateGroupId = filterItem.Filters.Single(e => e.MemberName == "IncreaseRateGroupId").Value as int?;
                    var isSalable = filterItem.Filters.Single(r => r.MemberName == "IsSalable").Value as bool?;
                    var isOrderable = filterItem.Filters.Single(r => r.MemberName == "IsOrderable").Value as bool?;
                    var isPrimary = filterItem.Filters.Single(r => r.MemberName == "IsPrimary").Value as bool?;
                    var isEffective = filterItem.Filters.Single(r => r.MemberName == "IsEffective").Value as bool?;
                    ShellViewModel.Current.IsBusy = true;
                    if(uniqueId.Equals(CommonActionKeys.EXPORT)) {
                        this.dcsDomainContext.导出所有配件信息(code, referenceCode, partABC, increaseRateGroupId, isSalable, isOrderable, isPrimary, isEffective, loadOp => {
                       if(loadOp.HasError) {
                           ShellViewModel.Current.IsBusy = false;
                           return;
                       }
                       if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                           UIHelper.ShowNotification(loadOp.Value);
                           ShellViewModel.Current.IsBusy = false;
                       }

                       if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                           HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                           ShellViewModel.Current.IsBusy = false;
                       }
                   }, null);
                    } else {
                        this.dcsDomainContext.导出没有价格所有配件信息(code, referenceCode, partABC, increaseRateGroupId, isSalable, isOrderable, isPrimary, isEffective, loadOp => {
                           if(loadOp.HasError) {
                               ShellViewModel.Current.IsBusy = false;
                               return;
                           }
                           if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                               UIHelper.ShowNotification(loadOp.Value);
                               ShellViewModel.Current.IsBusy = false;
                           }

                           if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                               HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                               ShellViewModel.Current.IsBusy = false;
                           }
                       }, null);
                    }
                   
                    break;
                
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId)
            {
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default: return false;
            }
        }
    }
}
