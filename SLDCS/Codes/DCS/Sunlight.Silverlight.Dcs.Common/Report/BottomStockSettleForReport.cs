﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Report {
    [PageMeta("Report", "SparePart", "BottomStockSettleForReport", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class BottomStockSettleForReport : DcsDataManagementViewBase {

        private DataGridViewBase dataGridView;
        public BottomStockSettleForReport() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.QueryPanel_Title_BottomStockSettleForReport;
        }
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("BottomStockSettleForReport"));
            }
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "BottomStockSettleForReport"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            if(filterItem is CompositeFilterItem) {
                var compositeFilterItem = filterItem as CompositeFilterItem;
                if(compositeFilterItem.Filters.Count() == 4) {
                    UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Alert_TimeIsNull, 5);
                    return;
                }
                var compositeFilter = compositeFilterItem.Filters.Single(filter => filter.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                var validFromFilter = compositeFilter.Filters.First(filter => filter.MemberName == "CreateTime");
                var validFromFilterend = compositeFilter.Filters.Last(filter => filter.MemberName == "CreateTime");
                if(validFromFilter == null || validFromFilter.Value == null || validFromFilter.Value.ToString().Equals("0001-01-01 00:00:00")) {
                    UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Alert_TimeIsNull, 5);
                    return;
                }
                if(validFromFilterend == null || validFromFilterend.Value == null || validFromFilterend.Value.ToString().Equals("9999-12-31 23:59:59")) {
                    UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Alert_TimeIsNull, 5);
                    return;
                }  
            }
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
           
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {

                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var type = filterItem.Filters.Single(e => e.MemberName == "Type").Value as int?;
                    var distributionCenterName = filterItem.Filters.Single(r => r.MemberName == "DistributionCenterName").Value as string;
                    var centerName = filterItem.Filters.Single(r => r.MemberName == "CenterName").Value as string;
                    var dealerName = filterItem.Filters.Single(r => r.MemberName == "DealerName").Value as string;
                    DateTime? bCreateTime = null;
                    DateTime? eCreateTime = null;
                    foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                        var dateTime = filter as CompositeFilterItem;
                        if(dateTime != null) {
                            if(dateTime.Filters.First().MemberName == "CreateTime") {
                                bCreateTime = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                eCreateTime = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                            }

                        }
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出保底覆盖率报表(type, distributionCenterName, centerName, dealerName,bCreateTime,eCreateTime, loadOp => {
                        if(loadOp.HasError) {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return true;
                default: return false;
            }
        }
    }
}