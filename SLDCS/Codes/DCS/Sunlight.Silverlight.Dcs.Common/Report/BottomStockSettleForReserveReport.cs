﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Report {
       [PageMeta("Report", "SparePart", "BottomStockSettleForReserve", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class BottomStockSettleForReserveReport : DcsDataManagementViewBase {

        private DataGridViewBase dataGridView;
        public BottomStockSettleForReserveReport() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataEditView_Notification_BottomTitle;
        }
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("BottomStockSettleForReserve"));
            }
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "BottomStockSettleForReserve"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            var type = compositeFilterItem.Filters.SingleOrDefault(s => s.MemberName == "Type");
            if(type == null || type.Value == null) {
                UIHelper.ShowNotification(CommonUIStrings.Query_Type, 5);
                return;
            }
            var reserveType = compositeFilterItem.Filters.SingleOrDefault(s => s.MemberName == "ReserveTypeId");
            if(reserveType == null || reserveType.Value == null) {
                UIHelper.ShowNotification(CommonUIStrings.Query_ReserveType_Validation, 5);
                return;
            }
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
           
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {

                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var type = filterItem.Filters.Single(e => e.MemberName == "Type").Value as int?;
                    var distributionCenterName = filterItem.Filters.Single(r => r.MemberName == "DistributionCenterName").Value as string;
                    var centerName = filterItem.Filters.Single(r => r.MemberName == "CenterName").Value as string;
                    var dealerName = filterItem.Filters.Single(r => r.MemberName == "DealerName").Value as string;
                    var reserveTypeSubItem = filterItem.Filters.Single(r => r.MemberName == "ReserveTypeSubItem").Value as string;
                    var reserveTypeId = filterItem.Filters.Single(e => e.MemberName == "ReserveTypeId").Value as int?;
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出储备类别保底覆盖率报表(type, distributionCenterName, centerName, dealerName, reserveTypeId, reserveTypeSubItem, loadOp => {
                        if(loadOp.HasError) {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return true;
                default: return false;
            }
        }
    }
}