﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report {
    [PageMeta("Report", "SparePart", "CenterStockSummaryReport", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class CenterStockSummaryReport : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        public CenterStockSummaryReport() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.QueryPanel_Title_CenterStockSummary;
        }
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("CenterStockSummaryReport"));
            }
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "CenterStockSummaryReport"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            if(filterItem is CompositeFilterItem) {
                var compositeFilterItem = filterItem as CompositeFilterItem;
                var time = 0;
                foreach(var filter in compositeFilterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                    if(time == 1) {
                        continue;
                    } else {
                        time = 1;
                    }
                    var dateTime = filter as CompositeFilterItem;
                    if(dateTime != null) {
                        if(dateTime.Filters.Count == 0) {
                            UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Alert_TimeIsNull, 5);
                            return;
                        }
                        if(dateTime.Filters.Count > 0 && dateTime.Filters.First().MemberName == "CreateTime") {
                            var validFromFilter = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            var validFromFilterend = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                            if(validFromFilter == null || validFromFilter.Value == null || validFromFilter.Value.ToString().Equals("0001-01-01 00:00:00")) {
                                UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Alert_TimeIsNull, 5);
                                return;
                            }
                            if(validFromFilterend == null || validFromFilterend.Value == null || validFromFilterend.Value.ToString().Equals("9999-12-31 23:59:59")) {
                                UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Alert_TimeIsNull, 5);
                                return;
                            }
                        }
                    }
                }

                var codes = compositeFilterItem.Filters.SingleOrDefault(s => s.MemberName == "MarketingDepartmentName");
                if(codes != null && codes.Value != null) {
                    compositeFilterItem.Filters.Add(new CompositeFilterItem {
                        MemberName = "MarketingDepartmentName",
                        MemberType = typeof(string),
                        Value = string.Join(",", ((IEnumerable<string>)codes.Value).ToArray()),
                        Operator = FilterOperator.Contains
                    });
                    compositeFilterItem.Filters.Remove(codes);
                }
                var centerNames = compositeFilterItem.Filters.SingleOrDefault(s => s.MemberName == "CenterName");
                if(centerNames != null && centerNames.Value != null) {
                    compositeFilterItem.Filters.Add(new CompositeFilterItem {
                        MemberName = "CenterName",
                        MemberType = typeof(string),
                        Value = string.Join(",", ((IEnumerable<string>)centerNames.Value).ToArray()),
                        Operator = FilterOperator.Contains
                    });
                    compositeFilterItem.Filters.Remove(centerNames);
                }
                var dealerNames = compositeFilterItem.Filters.SingleOrDefault(s => s.MemberName == "DealerName");
                if(dealerNames != null && dealerNames.Value != null) {
                    compositeFilterItem.Filters.Add(new CompositeFilterItem {
                        MemberName = "DealerName",
                        MemberType = typeof(string),
                        Value = string.Join(",", ((IEnumerable<string>)dealerNames.Value).ToArray()),
                        Operator = FilterOperator.Contains
                    });
                    compositeFilterItem.Filters.Remove(dealerNames);
                }
                ClientVar.ConvertTime(compositeFilterItem);

            }
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var marketingDepartmentNames = filterItem.Filters.Single(e => e.MemberName == "MarketingDepartmentName").Value as string;
                    var centerNames = filterItem.Filters.Single(e => e.MemberName == "CenterName").Value as string;
                    var dealerNames = filterItem.Filters.Single(r => r.MemberName == "DealerName").Value as string;
                    DateTime? bCreateTime = null;
                    DateTime? eCreateTime = null;
                    string newTypes = string.Empty;
                    foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                        var dateTime = filter as CompositeFilterItem;
                        if(dateTime != null && dateTime.Filters.Count>0) {
                            if(dateTime.Filters.First().MemberName == "CreateTime") {
                                bCreateTime = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                eCreateTime = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                            }
                            if((dateTime.Filters.First().MemberName== "NewType"))
                                newTypes = string.Join(",", dateTime.Filters.Where(r => r.MemberName == "NewType").Select(r => r.Value));

                        }
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出中心库库存汇总(marketingDepartmentNames, centerNames, bCreateTime, eCreateTime, dealerNames, newTypes, loadOp => {
                        if(loadOp.HasError) {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return true;
                default: return false;
            }
        }
    }
}