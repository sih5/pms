﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Report {
    [PageMeta("Report", "Sale", "CentralPartsSalesOrderFinishReport", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class CentralPartsSalesOrderFinishReport: DcsDataManagementViewBase
    {
        private DataGridViewBase dataGridView;
        public CentralPartsSalesOrderFinishReport()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.QueryPanel_Title_CentralPartsSalesOrderFinishReport;
        }
        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("CentralPartsSalesOrderFinishReport"));
            }
        }
        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "CentralPartsSalesOrderFinishReport"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem)
        {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId)
            {

                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if (filterItem == null)
                        return;
                   var code = filterItem.Filters.Single(e => e.MemberName == "Code").Value as string;
                    var referenceCode = filterItem.Filters.Single(e => e.MemberName == "ReferenceCode").Value as string;
                    var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                    var submitCompanyName = filterItem.Filters.Single(e => e.MemberName == "SubmitCompanyName").Value as string;
                    var sparePartCode = filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                    var status = filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                    var partsOutboundBillCode = filterItem.Filters.Single(r => r.MemberName == "PartsOutboundBillCode").Value as string;
                    var contractCode = filterItem.Filters.Single(r => r.MemberName == "ContractCode").Value as string;
                    var partsPurchaseOrderCode = filterItem.Filters.Single(r => r.MemberName == "PartsPurchaseOrderCode").Value as string;
                    var marketingDepartmentName = filterItem.Filters.Single(r => r.MemberName == "MarketingDepartmentName").Value as string;
                    var retailStatus = filterItem.Filters.Single(r => r.MemberName == "RetailStatus").Value as int?;
                    var partsSalesOrderTypeId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesOrderTypeId").Value as int?;
                    DateTime? bCreateTime = null;
                    DateTime? eCreateTime = null;                   
                    DateTime? bApproveTime = null;
                    DateTime? eApproveTime = null;
                    DateTime? bShippingDate = null;
                    DateTime? eShippingDate = null;
                     DateTime? bSubmitTime = null;
                    DateTime? eSubmitTime = null;
                    foreach (var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)))
                    {
                        var dateTime = filter as CompositeFilterItem;
                        if (dateTime != null)
                        {
                            if (dateTime.Filters.First().MemberName == "CreateTime")
                            {
                                bCreateTime = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                eCreateTime = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                            }
                            
                            if (dateTime.Filters.First().MemberName == "ApproveTime")
                            {
                                bApproveTime = dateTime.Filters.First(r => r.MemberName == "ApproveTime").Value as DateTime?;
                                eApproveTime = dateTime.Filters.Last(r => r.MemberName == "ApproveTime").Value as DateTime?;
                            }
                            if (dateTime.Filters.First().MemberName == "ShippingDate")
                            {
                                bShippingDate = dateTime.Filters.First(r => r.MemberName == "ShippingDate").Value as DateTime?;
                                eShippingDate = dateTime.Filters.Last(r => r.MemberName == "ShippingDate").Value as DateTime?;
                            }
                            if(dateTime.Filters.First().MemberName == "SubmitTime") {
                                bSubmitTime = dateTime.Filters.First(r => r.MemberName == "SubmitTime").Value as DateTime?;
                                eSubmitTime = dateTime.Filters.Last(r => r.MemberName == "SubmitTime").Value as DateTime?;
                            }
                        }
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出中心库销售订单完成情况统计(code, referenceCode, bCreateTime, eCreateTime, bApproveTime, eApproveTime, bShippingDate, eShippingDate, warehouseId, submitCompanyName, sparePartCode, status, partsOutboundBillCode, contractCode, partsPurchaseOrderCode, marketingDepartmentName, retailStatus, bSubmitTime, eSubmitTime,partsSalesOrderTypeId, loadOp =>
                    {
                        if (loadOp.HasError)
                        {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if (loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value))
                        {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if (loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                        {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId)
            {
                case CommonActionKeys.EXPORT:
                    return true;
                default: return false;
            }
        }
    }
}