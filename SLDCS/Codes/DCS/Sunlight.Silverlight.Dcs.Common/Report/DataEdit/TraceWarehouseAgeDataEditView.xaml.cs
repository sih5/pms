﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataEdit {
    public partial class TraceWarehouseAgeDataEditView {

        public TraceWarehouseAgeDataEditView() {
            InitializeComponent();
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.查询配件追溯码库位库龄Query(null, null, null, null, null, null, null, null, id,null), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    var trace = new AccurateTrace() {
                        Id = entity.Id,
                        TraceCode = entity.TraceCode,
                        SIHLabelCode=entity.SIHLabelCode
                    };
                    this.SetObjectToEdit(trace);
                    this.TracePropertyString.Text = entity.TracePropertyString;
                    this.SparePartCode.Text = entity.SparePartCode;
                    this.SparePartName.Text = entity.SparePartName;
                    this.WarehouseName.Text = entity.WarehouseName;
                    this.WarehouseAreaCode.Text = entity.WarehouseAreaCode;
                    this.StorehouseAreaCode.Text = entity.StorehouseAreaCode;
                }
            }, null);
        }
        protected override bool OnRequestCanSubmit() {
            return true;
        }
        protected override void OnEditSubmitting() {
            var trace = this.DataContext as AccurateTrace;
            if(trace == null)
                return;
            if(string.IsNullOrEmpty(trace.TraceCode) || trace.TraceCode.Length != 17) {
                UIHelper.ShowNotification("请填写17位的追溯码");
                return;
            }
            var dcsDomainContext = new DcsDomainContext();
            try {
              //  trace.修改追溯码();
                dcsDomainContext.修改追溯码(trace, invokeOp => {
                    ShellViewModel.Current.IsBusy = false;
                    if(invokeOp.HasError) {
                        if(!invokeOp.IsErrorHandled)
                            invokeOp.MarkErrorAsHandled();
                        if(0 == invokeOp.ValidationErrors.Count()) {
                            base.OnEditSubmitting();
                            return;
                        }
                        var error = invokeOp.ValidationErrors.First();
                        if(error != null)
                            UIHelper.ShowAlertMessage(invokeOp.ValidationErrors.First().ErrorMessage);
                        else
                            DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                        return;
                    }
                    base.OnEditSubmitting();
                }, null);
                ExecuteSerivcesMethod("修改成功");

            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            //   base.OnEditSubmitting();
        }
        private void ExecuteSerivcesMethod(string notifyMessage) {
            DomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    DomainContext.RejectChanges();
                    return;
                }
                this.NotifyEditSubmitted();
                this.OnCustomEditSubmitted();
                UIHelper.ShowNotification(notifyMessage);
            }, null);
        }
        public void OnCustomEditSubmitted() {
            this.DataContext = null;
        }
        public new event EventHandler EditSubmitted;
        private void NotifyEditSubmitted() {
            var handler = this.EditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
