﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataEdit {
    public partial class WorkGruopPersonNumberDataEditView {
        private KeyValueManager keyValueManager;
        private ObservableCollection<KeyValuePair> kvProductTypes = new ObservableCollection<KeyValuePair>();
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        public WorkGruopPersonNumberDataEditView() {
              InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.keyValueManager.LoadData(() => {
                this.KvProductTypes.Clear();
                foreach(var item in this.KeyValueManager[this.kvNames[0]]) {
                    this.KvProductTypes.Add(new KeyValuePair {
                        Key = item.Key,
                        Value = item.Value
                    });
                }
            });
            InitializeComponent();
           
        }
        private readonly string[] kvNames = {
            "WorkGroup"
        };
        protected override void OnEditSubmitted() {
            base.OnEditSubmitted();
            this.KvProductTypes.Clear();
        }
        public ObservableCollection<KeyValuePair> KvProductTypes {
            get {
                return this.kvProductTypes ?? (this.kvProductTypes = new ObservableCollection<KeyValuePair>());
            }
        }
        protected override void OnEditSubmitting() {
            var workGruopPersonNumber = this.DataContext as WorkGruopPersonNumber;
            if(workGruopPersonNumber == null)
                return;
            workGruopPersonNumber.ValidationErrors.Clear();
            if(!workGruopPersonNumber.WorkGruop.HasValue){
                UIHelper.ShowNotification(CommonUIStrings.Error_WorkGruopPersonNumber_WorkGruop);
                return;
            }
            if(!workGruopPersonNumber.EmployeeNumber.HasValue) {
                UIHelper.ShowNotification(CommonUIStrings.Error_WorkGruopPersonNumber_EmployeeNumber);
                return;
            }
            if(workGruopPersonNumber.HasValidationErrors)
                return;
            ((IEditableObject)workGruopPersonNumber).EndEdit();
            base.OnEditSubmitting();
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetWorkGruopPersonNumbersQuery().Where(r => r.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity == null)
                    return;
                this.SetObjectToEdit(entity);
            }, null);
        }
        protected override string BusinessName {
            get {
                return CommonUIStrings.Report_Title_WorkGruopPersonNumberManagement;
            }
        }

        public void OnCustomEditSubmitted() {
            this.KvProductTypes.Clear();
        }
        protected override bool OnRequestCanSubmit() {
            return true;
        }

    }
}
