﻿
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;
namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid
{
    public class AllSparePartReportDataGridView : DcsDataGridViewBase
    {
        private readonly string[] kvNames = new[] {
            "ABCStrategy_Category","SparePart_PartType","PartsBranch_PurchaseRoute"
        };
        public AllSparePartReportDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                       Name = "SupplierPartCode",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsSupplierRelation_SupplierPartCode,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_SparePartCode,
                        Name = "Code"
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                        Name = "ReferenceCode" 
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_StorehouseCensus_SparePartName,
                        Name = "Name"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_EnglishName,
                        Name = "EnglishName"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_PurchasePrice,
                        Name = "PurchasePrice",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_MeasureUnit,
                        Name = "MeasureUnit"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_PartsSupplierCode,
                        Name = "PartsSupplierCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierName,
                        Name = "PartsSupplierName"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_Sparepart_IsPrimary,
                        Name = "IsPrimary"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_Htyxq,
                        Name = "Htyxq"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_IsZg,
                        Name = "IsZg"
                    },new ColumnItem {
                        Title=CommonUIStrings.DataDeTailPanel_Text_PartsBranch_BreakTime,
                        Name = "BreakTime"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_IsAccreditPack,
                        Name = "IsAccreditPack"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_PackingCoefficient,
                        Name = "PackingCoefficient"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_MInPackingAmount,
                        Name = "MInPackingAmount"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_OrderCycle,
                        Name = "OrderCycle"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_OrderGoodsCycle,
                        Name = "OrderGoodsCycle"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_ArrivalCycle,
                        Name = "ArrivalCycle"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_RetailGuidePrice,
                        Name = "RetailGuidePrice",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_CenterPrice,
                        Name = "CenterPrice",
                         TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_SalesPrice,
                       Name = "SalesPrice",
                        TextAlignment = TextAlignment.Right,
                    }, new KeyValuesColumnItem {
                        Name = "PartABC",
                        KeyValueItems = this.KeyValueManager[kvNames[0]],
                         Title=CommonUIStrings.QueryPanel_QueryItem_Title_Sparepart_PartABC,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_VirtualPartsBranch_IsSalable,
                        Name = "IsSalable"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_VirtualPartsBranch_IsOrderable,
                        Name = "IsOrderable"
                    },new ColumnItem {
                        Title=CommonUIStrings.DataEditPanel_Title_ImportIsSupplierPutIn,
                        Name = "IsSupplierPutIn"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_Sparepart_IncreaseRateGroupId,
                        Name = "GroupName"
                    }, new KeyValuesColumnItem {
                        Name = "PartType",
                        KeyValueItems = this.KeyValueManager[kvNames[1]],
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_PartType
                    }, new KeyValuesColumnItem {
                        Name = "PurchaseRoute",
                        KeyValueItems = this.KeyValueManager[kvNames[2]],
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_PurchaseRoute
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_PartsWarrantyLong,
                        Name = "PartsWarrantyLong",
                    }
                };
            }
        }
        protected override Type EntityType
        {
            get
            {
                return typeof(AllSparePart);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName)
        {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if (compositeFilterItem != null)
            {
                foreach(var item in compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).Where(filter => filter.MemberName != "increaseRateGroupId" && filter.MemberName != "isEffective"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "code":
                        return filters.Filters.Single(item => item.MemberName == "Code").Value;
                    case "referenceCode":
                        return filters.Filters.Single(item => item.MemberName == "ReferenceCode").Value;
                    case "partABC":
                        return filters.Filters.Single(item => item.MemberName == "PartABC").Value;
                    case "increaseRateGroupId":
                        return filters.Filters.Single(item => item.MemberName == "IncreaseRateGroupId").Value;
                    case "isSalable":
                        return filters.Filters.Single(item => item.MemberName == "IsSalable").Value;
                    case "isOrderable":
                        return filters.Filters.Single(item => item.MemberName == "IsOrderable").Value;
                    case "isPrimary":
                        return filters.Filters.Single(item => item.MemberName == "IsPrimary").Value;
                    case "isEffective":
                        return filters.Filters.Single(item => item.MemberName == "IsEffective").Value;

                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName()
        {
            return "getAllSpareParts";
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
            ((GridViewDataColumn)this.GridView.Columns["SalesPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["PurchasePrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["RetailGuidePrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["CenterPrice"]).DataFormatString = "c2";
        }
    }
}
