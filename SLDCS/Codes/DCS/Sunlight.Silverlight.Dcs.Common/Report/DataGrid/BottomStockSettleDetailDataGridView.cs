﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;
namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class BottomStockSettleDetailDataGridView : DcsDataGridViewBase {     
        public BottomStockSettleDetailDataGridView() {
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                       Name = "DistributionCenterName",
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_MarketingDepartmentName,
                    },new ColumnItem {
                        Title=CommonUIStrings.DataEditPanel_Text_Agency_Name,
                        Name = "CenterName"
                    } ,new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_DealerCode,
                        Name = "DealerCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.Report_QueryPanel_Title_DealerNames,
                        Name = "DealerName"
                    }
                    ,new ColumnItem {
                        Title =CommonUIStrings.DataDeTailPanel_Text_BottomStock_SparePartCode,
                        Name = "SparePartCode" 
                    },new ColumnItem {
                        Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_SparePartName,
                        Name = "SparePartName"
                    },new ColumnItem {
                         Title=CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_ActualStock,
                        Name = "ActualAvailableStock"
                    },new ColumnItem {
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_OnLineQty,
                        Name = "OnwayQty",
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_BottomStock,
                        Name = "StockQty"
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_DifferenceQty,
                        Name = "DifferenceQty"
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_DifferenceMoney2,
                        Name = "DifferenceMoney"
                    }
                };
            }
        }
        protected override Type EntityType {
            get {
                return typeof(BottomStockSettleTableReport);
            }
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                foreach(var item in compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).Where(filter => filter.MemberName != "Type" || filter.MemberName != "OverZero"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch(parameterName) {
                    case "type":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "Type").Value;
                    case "distributionCenterName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "DistributionCenterName").Value;
                    case "centerName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "CenterName").Value;
                    case "dealerName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "DealerName").Value;      
                    case "overZero":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "OverZero").Value;   
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName() {
            return "保底库存需提报计划明细表";
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;         
        }
    }
}