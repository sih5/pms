﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class CenterStockSummaryReportDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "ABCSetting_Type"
        };
        public CenterStockSummaryReportDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {

                    new ColumnItem {
                       Name = "YearMonth",
                       Title =CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_CreateTime,
                    }, new ColumnItem {
                       Name = "MarketingDepartmentName",
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_MarketingDepartmentName,
                    },new ColumnItem {
                        Title=CommonUIStrings.DataEditPanel_Text_Agency_Name,
                        Name = "CenterName"
                    },new ColumnItem {
                        Title=CommonUIStrings.Report_QueryPanel_Title_DealerNames,
                        Name = "DealerName"
                    }, new KeyValuesColumnItem{
                        Name = "NewType",
                        Title = CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_NewType,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Title= CommonUIStrings.Report_DataDeTailPanel_Text_BottomStock_Entries,
                        Name = "Entries"
                    },new ColumnItem {
                        Title=CommonUIStrings.Report_Title_InOutSettleSummary_Qty,
                        Name = "ActualStock"
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_OriginalPriceAmount,
                        Name = "SalesTotal"
                    }
                };
            }
        }
        protected override Type EntityType {
            get {
                return typeof(CenterStockSummary);
            }
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                foreach(var item in compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).Where(filter => filter.MemberName != "CreateTime" && filter.MemberName != "NewType"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters == null)
                return null;
            switch(parameterName) {
                case "bCreateTime":
                    foreach(var filter in filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                        var compositeFilterItem = filter as CompositeFilterItem;
                        {
                            if(compositeFilterItem.Filters.Any(r => r.MemberName == "CreateTime")) {
                                return compositeFilterItem.Filters.Where(r => r.MemberName == "CreateTime").First(r => r.MemberName == "CreateTime").Value;
                            }
                        }
                    }
                    return null;
                case "eCreateTime":
                    foreach(var filter in filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                        var compositeFilterItem = filter as CompositeFilterItem;
                        {
                            if(compositeFilterItem.Filters.Any(r => r.MemberName == "CreateTime")) {
                                return compositeFilterItem.Filters.Where(r => r.MemberName == "CreateTime").Last(r => r.MemberName == "CreateTime").Value;
                            }
                        }
                    }
                    return null;
                case "marketingDepartmentNames":
                    var codes = filters.Filters.SingleOrDefault(s => s.MemberName == "MarketingDepartmentName");
                    if(codes == null || codes.Value == null)
                        return null;
                    var dd = string.Join(",", codes.Value);
                    return dd;
                case "centerNames":
                    var centerName = filters.Filters.SingleOrDefault(s => s.MemberName == "CenterName");
                    if(centerName == null || centerName.Value == null)
                        return null;
                    return centerName.Value.ToString();
                case "dealerNames":
                    var dealerName = filters.Filters.SingleOrDefault(s => s.MemberName == "DealerName");
                    if(dealerName == null || dealerName.Value == null)
                        return null;
                    return dealerName.Value.ToString();
                case "newType":
                    string settlementStatus = string.Empty;
                    foreach(var filter in filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                        var compositeFilterItem = filter as CompositeFilterItem;
                        {
                            if(compositeFilterItem.Filters.Any(r => r.MemberName == "NewType"))
                                settlementStatus = string.Join(",", compositeFilterItem.Filters.Where(r => r.MemberName == "NewType").Select(r => r.Value));
                        }
                    }
                    return settlementStatus;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName() {
            return "中心库库存汇总";
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
            ((GridViewDataColumn)this.GridView.Columns["SalesTotal"]).DataFormatString = "c2";
        }
    }
}