﻿
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;


namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class CentralPartsSalesOrderForSIHReportDataGridView : DcsDataGridViewBase
    {

        public CentralPartsSalesOrderForSIHReportDataGridView()
        {
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get {
                return new[] {
                    new ColumnItem {
                       Name = "Year",
                       Title=CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_Year
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_Title_CentralPartsSalesOrder_Month,
                        Name = "Month"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_Day,
                        Name = "Day" 
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_Title_CentralPartsSalesOrder_OrderAmount,
                        Name = "OrderAmount"
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_Title_CentralPartsSalesOrder_SatisfiedAmount,
                        Name = "SatisfactionAmount",
                         TextAlignment = TextAlignment.Center,
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_Title_CentralPartsSalesOrder_UnSatisfiedAmount,
                        Name = "UnSatisfactionAmount",
                        TextAlignment = TextAlignment.Center,
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_Title_CentralPartsSalesOrder_OneTimeRate,
                        Name = "OneTimeRate",
                         TextAlignment = TextAlignment.Center,
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_Title_CentralPartsSalesOrder_TotalAmount,
                        Name = "TotalOrderAmount",
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_Title_CentralPartsSalesOrder_AppRoveAmount,
                        Name = "ApproveTotalOrderAmount",
                        TextAlignment = TextAlignment.Right
                    }
                };
            }
        }
        protected override Type EntityType
        {
            get
            {
                return typeof(CentralPartsSalesOrder);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName)
        {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if (compositeFilterItem != null)
            {
                foreach(var item in compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).Where(filter => filter.MemberName != "Type" && filter.MemberName != "ApproveTime"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "type":
                        return filters.Filters.Single(item => item.MemberName == "Type").Value;
                    case "bApproveTime":
                        var bOutBoundTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "ApproveTime");
                        return bOutBoundTime == null ? null : bOutBoundTime.Filters.First(item => item.MemberName == "ApproveTime").Value;
                    case "eApproveTime":
                        var eOutBoundTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "ApproveTime");
                        return eOutBoundTime == null ? null : eOutBoundTime.Filters.Last(item => item.MemberName == "ApproveTime").Value;                   
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName()
        {
            return "获取配件公司销售一次满足率日期报表";
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
            ((GridViewDataColumn)this.GridView.Columns["TotalOrderAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["ApproveTotalOrderAmount"]).DataFormatString = "c2";
        }
    }
}