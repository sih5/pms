﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;
namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class CompanyForReportDataGridView : DcsDataGridViewBase {
        public CompanyForReportDataGridView() {
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                       Name = "Name",
                       Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_Name,
                    },new ColumnItem {
                        Title="LV1",
                        Name = "LV1"
                    },new ColumnItem {
                        Title="LV2",
                        Name = "LV2" 
                    },new ColumnItem {
                        Title="LV3",
                        Name = "LV3" 
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_ActionName,
                        Name = "ActionName" 
                    }
                };
            }
        }
        protected override Type EntityType {
            get {
                return typeof(RolePersonnelReport);
            }
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }


        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch(parameterName) {
                    case "name":
                        return filters.Filters.Single(item => item.MemberName == "Name").Value;
                    case "actionName":
                        return filters.Filters.Single(item => item.MemberName == "ActionName").Value;
                    case "lV1":
                        return filters.Filters.Single(item => item.MemberName == "LV1").Value;
                    case "lV2":
                        return filters.Filters.Single(item => item.MemberName == "LV2").Value;
                    case "lV3":
                        return filters.Filters.Single(item => item.MemberName == "LV3").Value;


                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName() {
            return "获取企业权限模板";
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
        }
    }
}