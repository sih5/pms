﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;
namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class DealerInboundCheckForReportDataGridView  : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "Parts_InboundType"
        };
        public DealerInboundCheckForReportDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                       Name = "MarketingDepartmentName",
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_MarketingDepartmentName,
                    }, new ColumnItem {
                       Name = "StorageCompanyName",
                      Title = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_Name,
                    }, new ColumnItem {
                       Name = "ReceivingCompanyCode",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_AuthenticationRepair_DealerCode,
                    }, new ColumnItem {
                       Name = "ReceivingCompanyName",
                       Title = CommonUIStrings.QueryPanel_QueryItem_Title_AuthenticationRepair_DealerName,
                    }, new ColumnItem {
                       Name = "Code",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_Code,
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_InboundType,
                        Name = "InboundType"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_DealerPartsInboundBill_Quantity,
                        Name = "Quantity"
                    }, new ColumnItem {
                       Name = "ConfirmedReceptionTime",
                       Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_CreateTime,
                    },new ColumnItem {
                        Title=CommonUIStrings.DataEditPanel_Text_SparePart_Code,
                        Name = "PartsCode"
                    }, new ColumnItem {
                       Name = "PartsName",
                       Title=CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_SparePartName,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_DealerPartsInboundBill_CostPrice,
                        TextAlignment = TextAlignment.Right,
                        Name = "CostPrice"
                    }
                    ,new ColumnItem {
                       Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_OriginalPrice,
                        TextAlignment = TextAlignment.Right,
                        Name = "OriginalPrice" 
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_OrderPrice,
                        TextAlignment = TextAlignment.Right,
                        Name = "SettlementPrice"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_DealerPartsInboundBill_TotalAmount,
                        TextAlignment = TextAlignment.Right,
                        Name = "TotalAmount"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_Sparepart_IncreaseRateGroupId,
                        Name = "GroupName"
                    }
                };
            }
        }
        protected override Type EntityType {
            get {
                return typeof(DealerPartsInboundBill);
            }
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }


        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch(parameterName) {
                    case "code":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "Code").Value;
                    case "inboundType":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "InboundType").Value;
                    case "marketingDepartmentName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "MarketingDepartmentName").Value;
                    case "storageCompanyName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "StorageCompanyName").Value;
                    case "receivingCompanyName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "ReceivingCompanyName").Value;
                    case "bConfirmedReceptionTime":
                        var bConfirmedReceptionTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "ConfirmedReceptionTime");
                        return bConfirmedReceptionTime == null ? null : bConfirmedReceptionTime.Filters.First(item => item.MemberName == "ConfirmedReceptionTime").Value;
                    case "eConfirmedReceptionTime":
                        var eConfirmedReceptionTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "ConfirmedReceptionTime");
                        return eConfirmedReceptionTime == null ? null : eConfirmedReceptionTime.Filters.Last(item => item.MemberName == "ConfirmedReceptionTime").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName() {
            return "服务站入库单统计";
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
            ((GridViewDataColumn)this.GridView.Columns["SettlementPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OriginalPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["CostPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["TotalAmount"]).DataFormatString = "c2";
        }
    }
}