﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Telerik.Windows.Controls;
namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class DealerPartsOutboundBillReportDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_MarketingDepartmentName,
                       Name = "MarketingDepartmentName"
                    },new ColumnItem {
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_Name,
                       Name = "CenterName"
                    },new ColumnItem {
                         Title = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_Code,
                       Name = "CenterCode"
                    },new ColumnItem {
                         Title = CommonUIStrings.QueryPanel_QueryItem_Title_AuthenticationRepair_DealerCode,
                       Name = "DealerCode"
                    },new ColumnItem {
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_AuthenticationRepair_DealerName,
                       Name = "DealerName"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_CustomerTypeStr,
                       Name = "CustomerType"
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_SubmitCompanyName,
                       Name = "Customer"
                    },new ColumnItem {
                       Name = "Code",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_Code,
                    },new ColumnItem {
                       Name = "OutBoundTime",
                       Title=CommonUIStrings.QueryPanel_QueryItem_Title_DealerPartsOutboundBill_OutBoundTime
                    }, new ColumnItem {
                        Name = "OutInBandtype",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_InboundType,
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                       Name = "ReferenceCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_SparePartCode,
                       Name = "PartsCode" 
                    },new ColumnItem {
                       Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                       Name = "PartsName"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_OutboundAmount,
                       Name = "Quantity",
                       TextAlignment = TextAlignment.Center
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_DealerPartsInboundBill_CostPrice,
                       Name = "CostPrice",
                         TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_OriginalPrice,
                       Name = "ReturnPrice",
                         TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_OrderPrice,
                       Name = "OriginalPrice",
                         TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_DealerPartsInboundBill_TotalAmount,
                       Name = "TotalAmount",
                         TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_Sparepart_IncreaseRateGroupId,
                       Name = "GroupName"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerPartsOutboundBill);
            }
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }     
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch(parameterName) {
                    case "code":
                        return filters.Filters.Single(item => item.MemberName == "Code").Value;
                    case "marketingDepartmentName":
                        return filters.Filters.Single(item => item.MemberName == "MarketingDepartmentName").Value;
                    case "centerName":
                        return filters.Filters.Single(item => item.MemberName == "CenterName").Value;
                    case "outInBandtype":
                        return filters.Filters.Single(item => item.MemberName == "OutInBandtype").Value;
                    case "sparePartCode":
                        return filters.Filters.Single(item => item.MemberName == "PartsCode").Value;
                    case "sparePartName":
                        return filters.Filters.Single(item => item.MemberName == "PartsName").Value;
                    case "dealerName":
                        return filters.Filters.Single(item => item.MemberName == "DealerName").Value;
                    case "bOutBoundTime":
                        var bOutBoundTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "OutBoundTime");
                        return bOutBoundTime == null ? null : bOutBoundTime.Filters.First(item => item.MemberName == "OutBoundTime").Value;
                    case "eOutBoundTime":
                        var eOutBoundTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "OutBoundTime");
                        return eOutBoundTime == null ? null : eOutBoundTime.Filters.Last(item => item.MemberName == "OutBoundTime").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName() {
            return "GetDealerPartsOutboundBills";
        }
        protected override void OnControlsCreated() {

            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
            ((GridViewDataColumn)this.GridView.Columns["CostPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["ReturnPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OriginalPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["TotalAmount"]).DataFormatString = "c2";
        }
    }
}
