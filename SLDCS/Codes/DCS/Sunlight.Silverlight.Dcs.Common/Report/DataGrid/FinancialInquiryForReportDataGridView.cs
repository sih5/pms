﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Web.Entities;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class FinancialInquiryForReportDataGridView : DcsDataGridViewBase {

        public FinancialInquiryForReportDataGridView() {
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                       Name = "WarehouseName",
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_WarehouseId,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_FinancialInquiryReport_WarehouseCode,
                        Name = "WarehouseCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_FinancialInquiryReport_SaleOutBound,
                        Name = "SaleOutBound" ,
                         TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_FinancialInquiryReport_PartsPurchaseReturn,
                        Name = "PartsPurchaseReturn" ,
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_FinancialInquiryReport_Internal,
                        Name = "Internal",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseInBound,
                        Name = "PartsPurchaseInBound",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_SaleReturn,
                        Name = "SaleReturn",
                        TextAlignment = TextAlignment.Right,
                    }
                };
            }
        }
        protected override Type EntityType {
            get {
                return typeof(FinancialInquiryReport);
            }
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch(parameterName) {
                    case "warehouseId":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "WarehouseId").Value;

                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName() {
            return "财务查询统计";
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
            ((GridViewDataColumn)this.GridView.Columns["SaleOutBound"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["PartsPurchaseReturn"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["Internal"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["PartsPurchaseInBound"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SaleReturn"]).DataFormatString = "c2";
        }
    }
}