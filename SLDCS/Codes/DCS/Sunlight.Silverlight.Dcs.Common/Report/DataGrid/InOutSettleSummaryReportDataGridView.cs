﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using System.Windows.Media;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class InOutSettleSummaryReportDataGridView : DcsDataGridViewBase {
        protected override Type EntityType {
            get {
                return typeof(InOutSettleSummaryQuery);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "RecordTimes",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_RecordTime
                    },new ColumnItem {
                        Name = "InPurchaseList",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Purchase
                    },new ColumnItem {
                        Name = "InReturnList",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Return
                    }, new ColumnItem {
                        Name = "InPurchaseListAll",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_All
                    }, new ColumnItem {
                        Name = "InPurchaseInReturnItem",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_InPurchaseListAll
                    },new ColumnItem {
                        Name = "InPurchaseQty",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Purchase
                    },new ColumnItem {
                        Name = "InReturnQty",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Return
                    },new ColumnItem {
                        Name = "InPurchaseQtyAll",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_All
                    },new ColumnItem {
                        Name = "InPurchaseFee",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Purchase,
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Name = "InReturnFee",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Return,
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Name = "InPurchaseFeeAll",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_All,
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Name = "WaitInPurchaseList",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Arrival
                    },new ColumnItem {
                        Name = "WaitInLineList",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_InLine
                    },new ColumnItem {
                        Name = "WaitInPurchaseListAll",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_All
                    }, new ColumnItem {
                        Name = "WaitInPurchaseInLineItem",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_InPurchaseListAll
                    },new ColumnItem {
                        Name = "WaitInArriveFee",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Arrival,
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Name = "WaitInLineFee",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_InLine,
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Name = "WaitInArriveFeeAll",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_All,
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Name = "PackingPurchaseList",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Purchase
                    },new ColumnItem {
                        Name = "PackingReturnList",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Return
                    },new ColumnItem {
                        Name = "PackingPurchaseListAll",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_All
                    }, new ColumnItem {
                        Name = "PackingPurchaseReturnItem",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_InPurchaseListAll
                    },new ColumnItem {
                        Name = "PackingPurchaseQty",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Purchase
                    },new ColumnItem {
                        Name = "PackingReturnQty",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Return
                    },new ColumnItem {
                        Name = "PackingPurchaseQtyAll",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_All
                    },new ColumnItem {
                        Name = "PackingPurchaseFee",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Purchase,
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Name = "PackingReturnFee",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Return,
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Name = "PackingPurchaseFeeAll",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_All,
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Name = "WaitPackPurchaseList",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Purchase
                    },new ColumnItem {
                        Name = "WaitPackReturnList",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Return
                    },new ColumnItem {
                        Name = "WaitPackPurchaseListAll",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_All
                    }, new ColumnItem {
                        Name = "WaitPackPurchaseReturnItem",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_InPurchaseListAll
                    },new ColumnItem {
                        Name = "WaitPackPurchaseFee",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Purchase,
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Name = "WaitPackReturnFee",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Return,
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Name = "WaitPackPurchaseFeeAll",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_All,
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Name = "ShelvesPurchaseList",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Purchase
                    },new ColumnItem {
                        Name = "ShelvesReturnList",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Return
                    },new ColumnItem {
                        Name = "ShelvesPurchaseListAll",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_All
                    }, new ColumnItem {
                        Name = "ShelvesPurchasesReturnItem",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_InPurchaseListAll
                    },new ColumnItem {
                        Name = "ShelvesPurchaseQty",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Purchase
                    },new ColumnItem {
                        Name = "ShelvesReturnQty",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Return
                    },new ColumnItem {
                        Name = "ShelvesPurchaseQtyAll",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_All
                    },new ColumnItem {
                        Name = "ShelvesPurchaseFee",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Purchase,
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Name = "ShelvesReturnFee",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Return,
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Name = "ShelvesPurchaseFeeAll",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_All,
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Name = "WaitShelvesPurchaseLis",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Purchase
                    },new ColumnItem {
                        Name = "WaitShelvesReturnList",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Return
                    },new ColumnItem {
                        Name = "WaitShelvesPurchaseLisAll",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_All
                    }, new ColumnItem {
                        Name = "WaitShelvesPurchaseReturnItem",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_InPurchaseListAll
                    },new ColumnItem {
                        Name = "WaitShelvesPurchaseFee",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Purchase,
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Name = "WaitShelvesReturnFee",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Return,
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Name = "WaitShelvesPurchaseFeeAll",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_All,
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Name = "SaleApproveList",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Item
                    }, new ColumnItem {
                        Name = "SaleApproveItem",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_InPurchaseListAll
                    },new ColumnItem {
                        Name = "SaleApproveQty",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Qty
                    },new ColumnItem {
                        Name = "SaleApproveFee",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Fee,
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Name = "PickingList",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Item
                    }, new ColumnItem {
                        Name = "PickingItem",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_InPurchaseListAll
                    },new ColumnItem {
                        Name = "PickingQty",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Qty
                    },new ColumnItem {
                        Name = "PickingFee",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Fee,
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Name = "OutList",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Item
                    }, new ColumnItem {
                        Name = "OutItem",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_InPurchaseListAll
                    },new ColumnItem {
                        Name = "OutQty",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Qty
                    },new ColumnItem {
                        Name = "OutFee",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Fee,
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Name = "WaitOutList",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Item
                    }, new ColumnItem {
                        Name = "WaitOutItem",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_InPurchaseListAll
                    },new ColumnItem {
                        Name = "WaitOutQty",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Qty
                    },new ColumnItem {
                        Name = "WaitOutFee",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Fee,
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Name = "ShippingFee",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Fee,
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Name = "ShippingWeight",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_ShippingWeight
                    }
                   
                };
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {

            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {

                var cFilterItem = compositeFilterItem.Filters.FirstOrDefault(item => item.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                if(cFilterItem == null)
                    return base.OnRequestFilterDescriptor(queryName);
                foreach(var item in cFilterItem.Filters.Where(filter => filter.MemberName != "RecordTime" && filter.MemberName != "WarehouseId"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch(parameterName) {
                    case "warehouseId":
                        return filters.Filters.Single(item => item.MemberName == "WarehouseId").Value;
                    case "bRecordTime":
                        var eShippingDate = composites.FirstOrDefault(p => p.Filters.First().MemberName == "RecordTime");
                        return eShippingDate == null ? null : eShippingDate.Filters.First(item => item.MemberName == "RecordTime").Value;
                    case "eRecordTime":
                        var bShippingDate = composites.FirstOrDefault(p => p.Filters.First().MemberName == "RecordTime");
                        return bShippingDate == null ? null : bShippingDate.Filters.Last(r => r.MemberName == "RecordTime").Value;


                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName() {
            return "查询出入库统计";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 2000;

            ((GridViewDataColumn)this.GridView.Columns["InPurchaseFeeAll"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["WaitInArriveFeeAll"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["PackingPurchaseFeeAll"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["WaitPackPurchaseFeeAll"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["WaitShelvesPurchaseFeeAll"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["InPurchaseFee"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["InReturnFee"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["WaitInArriveFee"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["WaitInLineFee"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["PackingPurchaseFee"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["PackingReturnFee"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["WaitPackPurchaseFee"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["WaitPackReturnFee"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["ShelvesPurchaseFee"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["ShelvesPurchaseFeeAll"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["ShelvesReturnFee"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["WaitShelvesPurchaseFee"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["WaitShelvesReturnFee"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SaleApproveFee"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["PickingFee"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OutFee"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["WaitOutFee"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["ShippingFee"]).DataFormatString = "c2";


            var gridViewColumnGroupTp = new GridViewColumnGroup();
            gridViewColumnGroupTp.Name = "1";
            gridViewColumnGroupTp.Header = CommonUIStrings.Report_Title_InOutSettleSummary_Inbound;
            this.GridView.ColumnGroups.Add(gridViewColumnGroupTp);
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            var gridViewColumnGroupRole = new GridViewColumnGroup();
            gridViewColumnGroupRole.Name = "11";
            gridViewColumnGroupRole.Header = CommonUIStrings.Report_Title_InOutSettleSummary_Item;
            this.GridView.ColumnGroups.Add(gridViewColumnGroupRole);
            gridViewColumnGroupTp.ChildGroups.Add(gridViewColumnGroupRole);
            this.GridView.Columns["InPurchaseList"].ColumnGroupName = gridViewColumnGroupRole.Name;
            this.GridView.Columns["InReturnList"].ColumnGroupName = gridViewColumnGroupRole.Name;
            this.GridView.Columns["InPurchaseListAll"].ColumnGroupName = gridViewColumnGroupRole.Name;
            this.GridView.Columns["InPurchaseInReturnItem"].ColumnGroupName = gridViewColumnGroupRole.Name;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();

            var gridViewColumnGroupQty = new GridViewColumnGroup();
            gridViewColumnGroupQty.Name = "12";
            gridViewColumnGroupQty.Header = CommonUIStrings.Report_Title_InOutSettleSummary_Qty;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            gridViewColumnGroupTp.ChildGroups.Add(gridViewColumnGroupQty);
            this.GridView.ColumnGroups.Add(gridViewColumnGroupQty);
            this.GridView.Columns["InPurchaseQty"].ColumnGroupName = gridViewColumnGroupQty.Name;
            this.GridView.Columns["InReturnQty"].ColumnGroupName = gridViewColumnGroupQty.Name;
            this.GridView.Columns["InPurchaseQtyAll"].ColumnGroupName = gridViewColumnGroupQty.Name;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();

            var gridViewColumnGroupM = new GridViewColumnGroup();
            gridViewColumnGroupM.Name = "13";
            gridViewColumnGroupM.Header = CommonUIStrings.Report_Title_InOutSettleSummary_Fee;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            gridViewColumnGroupTp.ChildGroups.Add(gridViewColumnGroupM);
            this.GridView.ColumnGroups.Add(gridViewColumnGroupM);
            this.GridView.Columns["InPurchaseFee"].ColumnGroupName = gridViewColumnGroupM.Name;
            this.GridView.Columns["InReturnFee"].ColumnGroupName = gridViewColumnGroupM.Name;
            this.GridView.Columns["InPurchaseFeeAll"].ColumnGroupName = gridViewColumnGroupM.Name;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();

            var gridViewColumnGroupUn = new GridViewColumnGroup();
            gridViewColumnGroupUn.Name = "2";
            gridViewColumnGroupUn.Header = CommonUIStrings.Report_Title_InOutSettleSummary_InboundUn;
            this.GridView.ColumnGroups.Add(gridViewColumnGroupUn);
            SetColumnGroupsHeaderHorizontalAlignmentCenter();

            var gridViewColumnGroupUnTm = new GridViewColumnGroup();
            gridViewColumnGroupUnTm.Name = "21";
            gridViewColumnGroupUnTm.Header = CommonUIStrings.Report_Title_InOutSettleSummary_Item;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            gridViewColumnGroupUn.ChildGroups.Add(gridViewColumnGroupUnTm);
            this.GridView.ColumnGroups.Add(gridViewColumnGroupUnTm);
            this.GridView.Columns["WaitInPurchaseList"].ColumnGroupName = gridViewColumnGroupUnTm.Name;
            this.GridView.Columns["WaitInLineList"].ColumnGroupName = gridViewColumnGroupUnTm.Name;
            this.GridView.Columns["WaitInPurchaseListAll"].ColumnGroupName = gridViewColumnGroupUnTm.Name;
            this.GridView.Columns["WaitInPurchaseInLineItem"].ColumnGroupName = gridViewColumnGroupUnTm.Name;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();

            var gridViewColumnGroupUnM = new GridViewColumnGroup();
            gridViewColumnGroupUnM.Name = "22";
            gridViewColumnGroupUnM.Header = CommonUIStrings.Report_Title_InOutSettleSummary_Fee;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            gridViewColumnGroupUn.ChildGroups.Add(gridViewColumnGroupUnM);
            this.GridView.ColumnGroups.Add(gridViewColumnGroupUnM);
            this.GridView.Columns["WaitInArriveFee"].ColumnGroupName = gridViewColumnGroupUnM.Name;
            this.GridView.Columns["WaitInLineFee"].ColumnGroupName = gridViewColumnGroupUnM.Name;
            this.GridView.Columns["WaitInArriveFeeAll"].ColumnGroupName = gridViewColumnGroupUnM.Name;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();




            var gridViewColumnGroupPk = new GridViewColumnGroup();
            gridViewColumnGroupPk.Name = "3";
            gridViewColumnGroupPk.Header = CommonUIStrings.Report_Title_InOutSettleSummary_Pack;
            this.GridView.ColumnGroups.Add(gridViewColumnGroupPk);
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            var gridViewColumnGroupRPkTm = new GridViewColumnGroup();
            gridViewColumnGroupRPkTm.Name = "31";
            gridViewColumnGroupRPkTm.Header = CommonUIStrings.Report_Title_InOutSettleSummary_Item;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            this.GridView.ColumnGroups.Add(gridViewColumnGroupPk);
            gridViewColumnGroupPk.ChildGroups.Add(gridViewColumnGroupRPkTm);
            this.GridView.ColumnGroups.Add(gridViewColumnGroupRPkTm);
            this.GridView.Columns["PackingPurchaseList"].ColumnGroupName = gridViewColumnGroupRPkTm.Name;
            this.GridView.Columns["PackingReturnList"].ColumnGroupName = gridViewColumnGroupRPkTm.Name;
            this.GridView.Columns["PackingPurchaseListAll"].ColumnGroupName = gridViewColumnGroupRPkTm.Name;
            this.GridView.Columns["PackingPurchaseReturnItem"].ColumnGroupName = gridViewColumnGroupRPkTm.Name;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();

            var gridViewColumnGroupPkQty = new GridViewColumnGroup();
            gridViewColumnGroupPkQty.Name = "32";
            gridViewColumnGroupPkQty.Header = CommonUIStrings.Report_Title_InOutSettleSummary_Qty;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            gridViewColumnGroupPk.ChildGroups.Add(gridViewColumnGroupPkQty);
            this.GridView.ColumnGroups.Add(gridViewColumnGroupPkQty);
            this.GridView.Columns["PackingPurchaseQty"].ColumnGroupName = gridViewColumnGroupPkQty.Name;
            this.GridView.Columns["PackingReturnQty"].ColumnGroupName = gridViewColumnGroupPkQty.Name;
            this.GridView.Columns["PackingPurchaseQtyAll"].ColumnGroupName = gridViewColumnGroupPkQty.Name;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();

            var gridViewColumnGroupPkM = new GridViewColumnGroup();
            gridViewColumnGroupPkM.Name = "33";
            gridViewColumnGroupPkM.Header = CommonUIStrings.Report_Title_InOutSettleSummary_Fee;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            gridViewColumnGroupPk.ChildGroups.Add(gridViewColumnGroupPkM);
            this.GridView.ColumnGroups.Add(gridViewColumnGroupPkM);
            this.GridView.Columns["PackingPurchaseFee"].ColumnGroupName = gridViewColumnGroupPkM.Name;
            this.GridView.Columns["PackingReturnFee"].ColumnGroupName = gridViewColumnGroupPkM.Name;
            this.GridView.Columns["PackingPurchaseFeeAll"].ColumnGroupName = gridViewColumnGroupPkM.Name;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();


            var gridViewColumnGroupUnPk = new GridViewColumnGroup();
            gridViewColumnGroupUnPk.Name = "4";
            gridViewColumnGroupUnPk.Header = CommonUIStrings.Report_Title_InOutSettleSummary_UnPack;
            this.GridView.ColumnGroups.Add(gridViewColumnGroupUnPk);
            SetColumnGroupsHeaderHorizontalAlignmentCenter();

            var gridViewColumnGroupUnPkTm = new GridViewColumnGroup();
            gridViewColumnGroupUnPkTm.Name = "41";
            gridViewColumnGroupUnPkTm.Header = CommonUIStrings.Report_Title_InOutSettleSummary_Item;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            this.GridView.ColumnGroups.Add(gridViewColumnGroupUnPk);
            gridViewColumnGroupUnPk.ChildGroups.Add(gridViewColumnGroupUnPkTm);
            this.GridView.ColumnGroups.Add(gridViewColumnGroupUnPkTm);
            this.GridView.Columns["WaitPackPurchaseList"].ColumnGroupName = gridViewColumnGroupUnPkTm.Name;
            this.GridView.Columns["WaitPackReturnList"].ColumnGroupName = gridViewColumnGroupUnPkTm.Name;
            this.GridView.Columns["WaitPackPurchaseListAll"].ColumnGroupName = gridViewColumnGroupUnPkTm.Name;
            this.GridView.Columns["WaitPackPurchaseReturnItem"].ColumnGroupName = gridViewColumnGroupUnPkTm.Name;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();

            var gridViewColumnGroupUnPkM = new GridViewColumnGroup();
            gridViewColumnGroupUnPkM.Name = "42";
            gridViewColumnGroupUnPkM.Header = CommonUIStrings.Report_Title_InOutSettleSummary_Fee;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            this.GridView.ColumnGroups.Add(gridViewColumnGroupUn);
            gridViewColumnGroupUnPk.ChildGroups.Add(gridViewColumnGroupUnPkM);
            this.GridView.ColumnGroups.Add(gridViewColumnGroupUnPkM);
            this.GridView.Columns["WaitPackPurchaseFee"].ColumnGroupName = gridViewColumnGroupUnPkM.Name;
            this.GridView.Columns["WaitPackReturnFee"].ColumnGroupName = gridViewColumnGroupUnPkM.Name;
            this.GridView.Columns["WaitPackPurchaseFeeAll"].ColumnGroupName = gridViewColumnGroupUnPkM.Name;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();



            var gridViewColumnGroupSh = new GridViewColumnGroup();
            gridViewColumnGroupSh.Name = "5";
            gridViewColumnGroupSh.Header = CommonUIStrings.Report_Title_InOutSettleSummary_Shelves;
            this.GridView.ColumnGroups.Add(gridViewColumnGroupSh);
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            var gridViewColumnGroupShTm = new GridViewColumnGroup();
            gridViewColumnGroupShTm.Name = "51";
            gridViewColumnGroupShTm.Header = CommonUIStrings.Report_Title_InOutSettleSummary_Item;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            this.GridView.ColumnGroups.Add(gridViewColumnGroupSh);
            gridViewColumnGroupSh.ChildGroups.Add(gridViewColumnGroupShTm);
            this.GridView.ColumnGroups.Add(gridViewColumnGroupShTm);
            this.GridView.Columns["ShelvesPurchaseList"].ColumnGroupName = gridViewColumnGroupShTm.Name;
            this.GridView.Columns["ShelvesReturnList"].ColumnGroupName = gridViewColumnGroupShTm.Name;
            this.GridView.Columns["ShelvesPurchaseListAll"].ColumnGroupName = gridViewColumnGroupShTm.Name;
            this.GridView.Columns["ShelvesPurchasesReturnItem"].ColumnGroupName = gridViewColumnGroupShTm.Name;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();

            var gridViewColumnGroupShQty = new GridViewColumnGroup();
            gridViewColumnGroupShQty.Name = "52";
            gridViewColumnGroupShQty.Header = CommonUIStrings.Report_Title_InOutSettleSummary_Qty;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            gridViewColumnGroupSh.ChildGroups.Add(gridViewColumnGroupShQty);
            this.GridView.ColumnGroups.Add(gridViewColumnGroupShQty);
            this.GridView.Columns["ShelvesPurchaseQty"].ColumnGroupName = gridViewColumnGroupShQty.Name;
            this.GridView.Columns["ShelvesReturnQty"].ColumnGroupName = gridViewColumnGroupShQty.Name;
            this.GridView.Columns["ShelvesPurchaseQtyAll"].ColumnGroupName = gridViewColumnGroupShQty.Name;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();

            var gridViewColumnGroupShM = new GridViewColumnGroup();
            gridViewColumnGroupShM.Name = "53";
            gridViewColumnGroupShM.Header = CommonUIStrings.Report_Title_InOutSettleSummary_Fee;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            gridViewColumnGroupSh.ChildGroups.Add(gridViewColumnGroupShM);
            this.GridView.ColumnGroups.Add(gridViewColumnGroupShM);
            this.GridView.Columns["ShelvesPurchaseFee"].ColumnGroupName = gridViewColumnGroupShM.Name;
            this.GridView.Columns["ShelvesReturnFee"].ColumnGroupName = gridViewColumnGroupShM.Name;
            this.GridView.Columns["ShelvesPurchaseFeeAll"].ColumnGroupName = gridViewColumnGroupShM.Name;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();

            var gridViewColumnGroupShUn = new GridViewColumnGroup();
            gridViewColumnGroupShUn.Name = "6";
            gridViewColumnGroupShUn.Header = CommonUIStrings.Report_Title_InOutSettleSummary_UnShelves;
            this.GridView.ColumnGroups.Add(gridViewColumnGroupShUn);
            SetColumnGroupsHeaderHorizontalAlignmentCenter();

            var gridViewColumnGroupShUnTm = new GridViewColumnGroup();
            gridViewColumnGroupShUnTm.Name = "61";
            gridViewColumnGroupShUnTm.Header = CommonUIStrings.Report_Title_InOutSettleSummary_Item;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            this.GridView.ColumnGroups.Add(gridViewColumnGroupUnPk);
            gridViewColumnGroupShUn.ChildGroups.Add(gridViewColumnGroupShUnTm);
            this.GridView.ColumnGroups.Add(gridViewColumnGroupShUnTm);
            this.GridView.Columns["WaitShelvesPurchaseLis"].ColumnGroupName = gridViewColumnGroupShUnTm.Name;
            this.GridView.Columns["WaitShelvesReturnList"].ColumnGroupName = gridViewColumnGroupShUnTm.Name;
            this.GridView.Columns["WaitShelvesPurchaseLisAll"].ColumnGroupName = gridViewColumnGroupShUnTm.Name;
            this.GridView.Columns["WaitShelvesPurchaseReturnItem"].ColumnGroupName = gridViewColumnGroupShUnTm.Name;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();

            var gridViewColumnGroupShUnM = new GridViewColumnGroup();
            gridViewColumnGroupShUnM.Name = "62";
            gridViewColumnGroupShUnM.Header = CommonUIStrings.Report_Title_InOutSettleSummary_Fee;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            this.GridView.ColumnGroups.Add(gridViewColumnGroupUn);
            gridViewColumnGroupShUn.ChildGroups.Add(gridViewColumnGroupShUnM);
            this.GridView.ColumnGroups.Add(gridViewColumnGroupShUnM);
            this.GridView.Columns["WaitShelvesPurchaseFee"].ColumnGroupName = gridViewColumnGroupShUnM.Name;
            this.GridView.Columns["WaitShelvesReturnFee"].ColumnGroupName = gridViewColumnGroupShUnM.Name;
            this.GridView.Columns["WaitShelvesPurchaseFeeAll"].ColumnGroupName = gridViewColumnGroupShUnM.Name;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();


            var gridViewColumnGroupSales = new GridViewColumnGroup();
            gridViewColumnGroupSales.Name = "7";
            gridViewColumnGroupSales.Header = CommonUIStrings.Report_Title_InOutSettleSummary_Approve;
            this.GridView.ColumnGroups.Add(gridViewColumnGroupSales);
            this.GridView.Columns["SaleApproveList"].ColumnGroupName = gridViewColumnGroupSales.Name;
            this.GridView.Columns["SaleApproveQty"].ColumnGroupName = gridViewColumnGroupSales.Name;
            this.GridView.Columns["SaleApproveFee"].ColumnGroupName = gridViewColumnGroupSales.Name;
            this.GridView.Columns["SaleApproveItem"].ColumnGroupName = gridViewColumnGroupSales.Name;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();

            var gridViewColumnGroupPi = new GridViewColumnGroup();
            gridViewColumnGroupPi.Name = "8";
            gridViewColumnGroupPi.Header = CommonUIStrings.Report_Title_InOutSettleSummary_Pickingtask;
            this.GridView.ColumnGroups.Add(gridViewColumnGroupPi);
            this.GridView.Columns["PickingList"].ColumnGroupName = gridViewColumnGroupPi.Name;
            this.GridView.Columns["PickingQty"].ColumnGroupName = gridViewColumnGroupPi.Name;
            this.GridView.Columns["PickingFee"].ColumnGroupName = gridViewColumnGroupPi.Name;
            this.GridView.Columns["PickingItem"].ColumnGroupName = gridViewColumnGroupPi.Name;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();

            var gridViewColumnGroupOut = new GridViewColumnGroup();
            gridViewColumnGroupOut.Name = "9";
            gridViewColumnGroupOut.Header = CommonUIStrings.Report_Title_InOutSettleSummary_OutBound;
            this.GridView.ColumnGroups.Add(gridViewColumnGroupOut);
            this.GridView.Columns["OutList"].ColumnGroupName = gridViewColumnGroupOut.Name;
            this.GridView.Columns["OutQty"].ColumnGroupName = gridViewColumnGroupOut.Name;
            this.GridView.Columns["OutFee"].ColumnGroupName = gridViewColumnGroupOut.Name;
            this.GridView.Columns["OutItem"].ColumnGroupName = gridViewColumnGroupOut.Name;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();

            var gridViewColumnGroupOutUn = new GridViewColumnGroup();
            gridViewColumnGroupOutUn.Name = "10";
            gridViewColumnGroupOutUn.Header = CommonUIStrings.Report_Title_InOutSettleSummary_UnOutBound;
            this.GridView.ColumnGroups.Add(gridViewColumnGroupOutUn);
            this.GridView.Columns["WaitOutList"].ColumnGroupName = gridViewColumnGroupOutUn.Name;
            this.GridView.Columns["WaitOutQty"].ColumnGroupName = gridViewColumnGroupOutUn.Name;
            this.GridView.Columns["WaitOutFee"].ColumnGroupName = gridViewColumnGroupOutUn.Name;
            this.GridView.Columns["WaitOutItem"].ColumnGroupName = gridViewColumnGroupOutUn.Name;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();

            var gridViewColumnGroupOutCar = new GridViewColumnGroup();
            gridViewColumnGroupOutCar.Name = "20";
            gridViewColumnGroupOutCar.Header = CommonUIStrings.Report_Title_InOutSettleSummary_Shipping;
            this.GridView.ColumnGroups.Add(gridViewColumnGroupOutCar);
            this.GridView.Columns["ShippingFee"].ColumnGroupName = gridViewColumnGroupOutCar.Name;
            this.GridView.Columns["ShippingWeight"].ColumnGroupName = gridViewColumnGroupOutCar.Name;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();


        }

        protected void SetColumnGroupsHeaderHorizontalAlignmentCenter() {
            foreach(GridViewColumnGroup item in GridView.ColumnGroups) {
                var temp = (DataTemplate)System.Windows.Markup.XamlReader.Load("<DataTemplate xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\"> <TextBlock Text=\""
                    + item.Header + "\" HorizontalAlignment=\"Center\" VerticalAlignment=\"Center\"/> </DataTemplate>");
                item.HeaderTemplate = temp;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

    }
}