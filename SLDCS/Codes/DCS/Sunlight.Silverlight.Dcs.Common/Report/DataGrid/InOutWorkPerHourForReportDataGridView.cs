﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using System.Windows.Media;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class InOutWorkPerHourForReportDataGridView : DcsDataGridViewBase {
        protected override Type EntityType {
            get {
                return typeof(InOutWorkPerHour);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "CreateTime",
                        Title = CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_Day
                    },new ColumnItem {
                        Name = "TimeFrame",
                        Title = CommonUIStrings.Report_Title_InOutWorkPerHour_TimeFrame
                    },new ColumnItem {
                        Name = "InPersonNum",
                        Title = CommonUIStrings.Report_Title_InOutWorkPerHour_PersonNum
                    }, new ColumnItem {
                        Name = "InOrderLineNum",
                        Title = CommonUIStrings.Report_Title_InOutWorkPerHour_OrderLineNum
                    }, new ColumnItem {
                        Name = "InOrderItemNum",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Item
                    },new ColumnItem {
                        Name = "InQty",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Qty
                    },new ColumnItem {
                        Name = "InFee",
                         TextAlignment = TextAlignment.Right,
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SettlementPriceSums
                    },new ColumnItem {
                        Name = "PackingPersonNum",
                        Title = CommonUIStrings.Report_Title_InOutWorkPerHour_PersonNum
                    },new ColumnItem {
                        Name = "PackingLineNum",
                        Title = CommonUIStrings.Report_Title_InOutWorkPerHour_OrderLineNum,
                    },new ColumnItem {
                        Name = "PackingItemNum",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Item,
                    },new ColumnItem {
                        Name = "PackingQty",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Qty,                       
                    },new ColumnItem {
                        Name = "PackingFee",
                         TextAlignment = TextAlignment.Right,
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SettlementPriceSums
                    },new ColumnItem {
                        Name = "ShelvePersonNum",
                        Title = CommonUIStrings.Report_Title_InOutWorkPerHour_PersonNum
                    },new ColumnItem {
                        Name = "ShelveLineNum",
                        Title = CommonUIStrings.Report_Title_InOutWorkPerHour_OrderLineNum
                    }, new ColumnItem {
                        Name = "ShelveItemNum",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Item
                    },new ColumnItem {
                        Name = "ShelveQty",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Qty,
                    },new ColumnItem {
                        Name = "ShelveFee",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SettlementPriceSums,
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Name = "UnShelvePersonNum",
                        Title = CommonUIStrings.Report_Title_InOutWorkPerHour_PersonNum,
                    },new ColumnItem {
                        Name = "UnShelveLineNum",
                        Title = CommonUIStrings.Report_Title_InOutWorkPerHour_OrderLineNum
                    },new ColumnItem {
                        Name = "UnShelveItemNum",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Item
                    },new ColumnItem {
                        Name = "UnShelveQty",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Qty
                    }, new ColumnItem {
                        Name = "UnShelveFee",
                         TextAlignment = TextAlignment.Right,
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SettlementPriceSums
                    },new ColumnItem {
                        Name = "OutPersonNum",
                        Title = CommonUIStrings.Report_Title_InOutWorkPerHour_PersonNum
                    },new ColumnItem {
                        Name = "OutOrderLineNum",
                        Title = CommonUIStrings.Report_Title_InOutWorkPerHour_OrderLineNum
                    },new ColumnItem {
                        Name = "OutOrderItemNum",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Item
                    },new ColumnItem {
                        Name = "OutQty",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Qty,
                    },new ColumnItem {
                        Name = "OutFee",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SettlementPriceSums,
                        TextAlignment = TextAlignment.Right,
                    }
                };
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {

            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {

                var cFilterItem = compositeFilterItem.Filters.FirstOrDefault(item => item.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                if(cFilterItem == null)
                    return base.OnRequestFilterDescriptor(queryName);
                foreach(var item in cFilterItem.Filters.Where(filter => filter.MemberName != "CreateTime" ))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

      
        protected override string OnRequestQueryName() {
            return "GetInOutWorkPerHoursBy";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 200;

            ((GridViewDataColumn)this.GridView.Columns["InFee"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["PackingFee"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["ShelveFee"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["UnShelveFee"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OutFee"]).DataFormatString = "c2";
          
            var gridViewColumnGroupTp = new GridViewColumnGroup();
            gridViewColumnGroupTp.Name = "1";
            gridViewColumnGroupTp.Header = CommonUIStrings.Report_Title_InOutSettleSummary_Inbound;
            this.GridView.ColumnGroups.Add(gridViewColumnGroupTp);
            this.GridView.Columns["InPersonNum"].ColumnGroupName = gridViewColumnGroupTp.Name;
            this.GridView.Columns["InOrderLineNum"].ColumnGroupName = gridViewColumnGroupTp.Name;
            this.GridView.Columns["InOrderItemNum"].ColumnGroupName = gridViewColumnGroupTp.Name;
            this.GridView.Columns["InQty"].ColumnGroupName = gridViewColumnGroupTp.Name;
            this.GridView.Columns["InFee"].ColumnGroupName = gridViewColumnGroupTp.Name;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();


            var gridViewColumnGroupUn = new GridViewColumnGroup();
            gridViewColumnGroupUn.Name = "2";
            gridViewColumnGroupUn.Header = CommonUIStrings.Report_Title_InOutSettleSummary_Pack;
            this.GridView.ColumnGroups.Add(gridViewColumnGroupUn);
            this.GridView.Columns["PackingPersonNum"].ColumnGroupName = gridViewColumnGroupUn.Name;
            this.GridView.Columns["PackingLineNum"].ColumnGroupName = gridViewColumnGroupUn.Name;
            this.GridView.Columns["PackingItemNum"].ColumnGroupName = gridViewColumnGroupUn.Name;
            this.GridView.Columns["PackingQty"].ColumnGroupName = gridViewColumnGroupUn.Name;
            this.GridView.Columns["PackingFee"].ColumnGroupName = gridViewColumnGroupUn.Name;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();

           
            var gridViewColumnGroupPk = new GridViewColumnGroup();
            gridViewColumnGroupPk.Name = "3";
            gridViewColumnGroupPk.Header = CommonUIStrings.Report_Title_InOutSettleSummary_Shelves;
            this.GridView.ColumnGroups.Add(gridViewColumnGroupPk);
            this.GridView.Columns["ShelvePersonNum"].ColumnGroupName = gridViewColumnGroupPk.Name;
            this.GridView.Columns["ShelveLineNum"].ColumnGroupName = gridViewColumnGroupPk.Name;
            this.GridView.Columns["ShelveItemNum"].ColumnGroupName = gridViewColumnGroupPk.Name;
            this.GridView.Columns["ShelveQty"].ColumnGroupName = gridViewColumnGroupPk.Name;
            this.GridView.Columns["ShelveFee"].ColumnGroupName = gridViewColumnGroupPk.Name;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();

            var gridViewColumnGroupUnPk = new GridViewColumnGroup();
            gridViewColumnGroupUnPk.Name = "4";
            gridViewColumnGroupUnPk.Header = CommonUIStrings.Report_Title_InOutSettleSummary_UnShelve;
            this.GridView.ColumnGroups.Add(gridViewColumnGroupUnPk);
            this.GridView.Columns["UnShelvePersonNum"].ColumnGroupName = gridViewColumnGroupUnPk.Name;
            this.GridView.Columns["UnShelveLineNum"].ColumnGroupName = gridViewColumnGroupUnPk.Name;
            this.GridView.Columns["UnShelveItemNum"].ColumnGroupName = gridViewColumnGroupUnPk.Name;
            this.GridView.Columns["UnShelveQty"].ColumnGroupName = gridViewColumnGroupUnPk.Name;
            this.GridView.Columns["UnShelveFee"].ColumnGroupName = gridViewColumnGroupUnPk.Name;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();


            var gridViewColumnGroupSh = new GridViewColumnGroup();
            gridViewColumnGroupSh.Name = "5";
            gridViewColumnGroupSh.Header = CommonUIStrings.Report_Title_InOutSettleSummary_OutBound;
            this.GridView.ColumnGroups.Add(gridViewColumnGroupSh);
            this.GridView.Columns["OutPersonNum"].ColumnGroupName = gridViewColumnGroupSh.Name;
            this.GridView.Columns["OutOrderLineNum"].ColumnGroupName = gridViewColumnGroupSh.Name;
            this.GridView.Columns["OutOrderItemNum"].ColumnGroupName = gridViewColumnGroupSh.Name;
            this.GridView.Columns["OutQty"].ColumnGroupName = gridViewColumnGroupSh.Name;
            this.GridView.Columns["OutFee"].ColumnGroupName = gridViewColumnGroupSh.Name;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();

        }

        protected void SetColumnGroupsHeaderHorizontalAlignmentCenter() {
            foreach(GridViewColumnGroup item in GridView.ColumnGroups) {
                var temp = (DataTemplate)System.Windows.Markup.XamlReader.Load("<DataTemplate xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\"> <TextBlock Text=\""
                    + item.Header + "\" HorizontalAlignment=\"Center\" VerticalAlignment=\"Center\"/> </DataTemplate>");
                item.HeaderTemplate = temp;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

    }
}