﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class PartsInboundCheckBillForReportDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "PartsSalesSettlement_Status","ABCStrategy_Category"
        };
        public PartsInboundCheckBillForReportDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                       Name = "Code",
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_Code,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierName,
                        Name = "PartsSupplierName"
                    } ,new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartsSupplierCode,
                        Name = "PartsSupplierCode"
                    },new ColumnItem {
                       Title= CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_SparePartCode,
                        Name = "SparePartCode"
                    }
                    ,new ColumnItem {
                         Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsSupplierRelation_SupplierPartCode,
                        Name = "SupplierPartCode" 
                    },new ColumnItem {
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                        Name = "ReferenceCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                        Name = "SparePartName"
                    },new KeyValuesColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartABCNew,
                         KeyValueItems = this.KeyValueManager[kvNames[1]],
                        Name = "PartABC"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_InspectedQuantity,
                        Name = "InspectedQuantity"
                    },new ColumnItem {
                       Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_CreateTime,
                        Name = "CreateTime"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SettlementPrice,
                        Name = "SettlementPrice"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_PartsPurchaseOrderCode,
                        Name = "PartsPurchaseOrderCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartsPurchaseOrderDate,
                        Name = "PartsPurchaseOrderDate"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_PartsSalesOrderTypeName,
                        Name = "PartsPurchaseOrderType"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SettleBillCode,
                        Name = "SettleBillCode"
                    },new KeyValuesColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SettleBillStatusNew,
                        Name = "SettleBillStatus",
                         KeyValueItems = this.KeyValueManager[kvNames[0]],
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_SAPSysInvoiceNumber,
                        Name = "SapCode",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PurchasePrice,
                        Name = "PurchasePrice",
                        TextAlignment = TextAlignment.Right,
                    },
                    new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_DifferPurchasePrice,
                        Name = "DifferPurchasePrice",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_DifferPurchasePriceAll,
                        Name = "DifferPurchasePriceAll",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_BillRemark,
                        Name = "PartsInboundCheckBillRemark",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartsPurchaseOrderRemark,
                        Name = "PartsPurchaseOrderRemark",
                         TextAlignment = TextAlignment.Right,
                    }
                };
            }
        }
        protected override Type EntityType {
            get {
                return typeof(PartsInboundCheckBillForReport);
            }
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }


        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch(parameterName) {
                    case "code":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "Code").Value;
                    case "partsPurchaseOrderCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "PartsPurchaseOrderCode").Value;
                    case "sparePartCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "SparePartCode").Value;
                    case "supplierPartName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "PartsSupplierName").Value;
                    case "settleBillStatus":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "SettleBillStatus").Value;
                    case "settleBillCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "SettleBillCode").Value;
                    case "bCreateTime":
                        var bCreateTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return bCreateTime == null ? null : bCreateTime.Filters.First(item => item.MemberName == "CreateTime").Value;
                    case "eCreateTime":
                        var eCreateTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return eCreateTime == null ? null : eCreateTime.Filters.Last(item => item.MemberName == "CreateTime").Value;
                    case "bPartsPurchaseOrderDate":
                        var bPartsPurchaseOrderDate = composites.FirstOrDefault(p => p.Filters.First().MemberName == "PartsPurchaseOrderDate");
                        return bPartsPurchaseOrderDate == null ? null : bPartsPurchaseOrderDate.Filters.First(item => item.MemberName == "PartsPurchaseOrderDate").Value;
                    case "ePartsPurchaseOrderDate":
                        var ePartsPurchaseOrderDate = composites.FirstOrDefault(p => p.Filters.First().MemberName == "PartsPurchaseOrderDate");
                        return ePartsPurchaseOrderDate == null ? null : ePartsPurchaseOrderDate.Filters.Last(item => item.MemberName == "PartsPurchaseOrderDate").Value;

                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName() {
            return "采购入库及供应商差价报表查询";
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
            ((GridViewDataColumn)this.GridView.Columns["SettlementPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["DifferPurchasePriceAll"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["DifferPurchasePrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["PurchasePrice"]).DataFormatString = "c2";
        }
    }
}