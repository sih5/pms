﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class PartsOutboundBillCentralReportDataGridView : DcsDataGridViewBase
    {
         private readonly string[] kvNames = new[] {
            "ABCStrategy_Category","Company_Type","Parts_SettlementStatus"
        };
         public PartsOutboundBillCentralReportDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                       Name = "MarketingDepartmentName",
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_MarketingDepartmentName,
                    },new ColumnItem {
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_Code,
                        Name = "CompanyCode"
                    } ,new ColumnItem {
                       Title = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_Name,
                        Name = "CompanyName"
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_WarehouseId,
                        Name = "WarehouseName"
                    }
                    ,new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_Province,
                        Name = "Province" 
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_SubmitCompanyCode,
                        Name = "SubmitCompanyCode"
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_SubmitCompanyName,
                        Name = "SubmitCompanyName"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_CustomerTypeStr,
                        Name = "CustomerType"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_PartsSalesOrderCode,
                        Name = "PartsSalesOrderCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_PartsSalesOrderProcessCode,
                        Name = "PartsSalesOrderProcessCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_PartsOutboundBillCode,
                        Name = "Code"
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                        Name = "ReferenceCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_SparePartCode,
                        Name = "SparePartCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                        Name = "SparePartName"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_PartsSalesOrderTypeName,
                        Name = "PartsSalesOrderTypeName"
                    }
                   , new KeyValuesColumnItem {
                        Name = "PartABC",
                        KeyValueItems = this.KeyValueManager[kvNames[0]],
                         Title=CommonUIStrings.QueryPanel_QueryItem_Title_Sparepart_PartABC,
                    }
                    ,new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_OutboundAmount,
                        Name = "OutboundAmount"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_CostPrice,
                        Name = "CostPrice",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_CostPriceAmount,
                        Name = "CostPriceAmount",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_RetailGuidePrice,
                        Name = "OriginalPrice",
                        TextAlignment = TextAlignment.Right,
                    },
                    new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_OriginalPriceAmount,
                        Name = "OriginalPriceAmount",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_ConcessionPrice,
                        Name = "ConcessionPrice",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_ConcessionRate,
                        Name = "ConcessionRate",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_DealerPartsInboundBill_CostPrice,
                        Name = "SettlementPrice",
                         TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_SettlementPriceAmount,
                        Name = "SettlementPriceAmount",
                         TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_ShippingDate,
                        Name = "ShippingDate",
                         TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_QuantityToSettle,
                        Name = "QuantityToSettle",
                         TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartsInboundCheckBillRemark,
                        Name = "Remark",
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_Sparepart_IncreaseRateGroupId,
                        Name = "GroupName",
                    },new KeyValuesColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SettleBillStatus,
                        KeyValueItems = this.KeyValueManager[kvNames[2]],
                       Name = "SettlementStatus"
                    }
                };
            }
        }
        protected override Type EntityType
        {
            get
            {
                return typeof(PartsOutboundBillCentral);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName)
        {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if (compositeFilterItem != null)
            {
                foreach (var item in compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).Where(filter => filter.MemberName != "WarehouseId" || filter.MemberName != "PartsPurchaseOrderCode" || filter.MemberName != "CustomerTypeTy"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();

        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "companyCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "CompanyCode").Value;
                    case "referenceCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "ReferenceCode").Value;
                    case "companyName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "CompanyName").Value;
                    case "submitCompanyName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "SubmitCompanyName").Value;
                    case "sparePartCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "SparePartCode").Value;
                    case "code":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "Code").Value;                 
                    case "partsSalesOrderCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "PartsSalesOrderCode").Value;
                    case "customerTypeInt":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "CustomerTypeInt").Value;
                    case "bShippingDate":
                        var bShippingDate = composites.FirstOrDefault(p => p.Filters.First().MemberName == "ShippingDate");
                        return bShippingDate == null ? null : bShippingDate.Filters.First(item => item.MemberName == "ShippingDate").Value;
                    case "eShippingDate":
                        var eShippingDate = composites.FirstOrDefault(p => p.Filters.First().MemberName == "ShippingDate");
                        return eShippingDate == null ? null : eShippingDate.Filters.Last(item => item.MemberName == "ShippingDate").Value;
                    case "customerTypeTy":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "CustomerTypeTy").Value;

                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName()
        {
            return "GetPartsOutboundBillCentrals";
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
            ((GridViewDataColumn)this.GridView.Columns["CostPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["CostPriceAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OriginalPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OriginalPriceAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["ConcessionPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SettlementPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SettlementPriceAmount"]).DataFormatString = "c2";
        }
    }
}