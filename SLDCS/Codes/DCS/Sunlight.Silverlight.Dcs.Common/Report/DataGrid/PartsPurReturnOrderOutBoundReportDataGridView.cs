﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class PartsPurReturnOrderOutBoundReportDataGridView : DcsDataGridViewBase
    {
         private readonly string[] kvNames = new[] {
            "ABCStrategy_Category","PartsPurchaseSettle_Status"
        };
         public PartsPurReturnOrderOutBoundReportDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                       Name = "PartsSupplierCode",
                       Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierCode,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierName,
                        Name = "PartsSupplierName"
                    } ,new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_PartsPurReturnOrderCode,
                        Name = "PartsPurReturnOrderCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_PartsOutboundBillTime,
                        Name = "OutBoundTime"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_PartsOutboundBillCode,
                        Name = "Code" 
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_WarehouseCode,
                        Name = "WarehouseCode"
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_WarehouseId,
                        Name = "WarehouseName"
                    },new ColumnItem {
                      Title=  CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_SparePartCode,
                        Name = "SparePartCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                        Name = "SparePartName"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_OutboundAmount,
                        Name = "OutboundAmount"
                    }, new KeyValuesColumnItem {
                        Name = "PartABC",
                        KeyValueItems = this.KeyValueManager[kvNames[0]],
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_PartABC
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_InternalAllocationBillReport_CostPrice,
                        Name = "CostPrice",
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_CostPriceAmountCb,
                        Name = "CostPriceAmount",
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_SettlementPrice,
                        Name = "SettlementPrice",
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_SettlementPriceAmountJe,
                        Name = "SettlementPriceAmount",
                        TextAlignment = TextAlignment.Right
                    },new KeyValuesColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_SettlementStatusCk,
                        Name = "SettlementStatus",
                        KeyValueItems = this.KeyValueManager[kvNames[1]],
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SettleBillCode,
                        Name = "SettlementNo",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_VoucherNo,
                        Name = "VoucherNo"
                    }
                };
            }
        }
        protected override Type EntityType
        {
            get
            {
                return typeof(PartsOutboundBillCentral);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }
  
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "SettlementStatus" && filter.MemberName != "WarehouseId" ))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();

        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "partsPurReturnOrderCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "PartsPurReturnOrderCode").Value;
                    case "settlementStatus":
                       // return filters.Filters.SingleOrDefault(item => item.MemberName == "SettlementStatus").Value;
                         string settlementStatus = string.Empty;
                        foreach(var filter in filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var compositeFilterItem = filter as CompositeFilterItem;
                            {
                                if(compositeFilterItem.Filters.Any(r => r.MemberName == "SettlementStatus"))
                                    settlementStatus = string.Join(",", compositeFilterItem.Filters.Where(r => r.MemberName == "SettlementStatus").Select(r => r.Value));
                            }
                        }
                        return settlementStatus;
                    case "settlementNo":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "SettlementNo").Value;
                    case "sparePartCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "SparePartCode").Value;                   
                    case "bOutBoundTime":
                        //var bOutBoundTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "OutBoundTime");
                        //return bOutBoundTime == null ? null : bOutBoundTime.Filters.First(item => item.MemberName == "OutBoundTime").Value;
                           foreach(var filter in filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var compositeFilterItem = filter as CompositeFilterItem;
                            {
                                if(compositeFilterItem.Filters.Any(r => r.MemberName == "OutBoundTime")) {
                                    return compositeFilterItem.Filters.Where(r => r.MemberName == "OutBoundTime").First(r => r.MemberName == "OutBoundTime").Value;
                                }
                            }
                        }
                        return null;
                    case "eOutBoundTime":
                        //var eOutBoundTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "OutBoundTime");
                        //return eOutBoundTime == null ? null : eOutBoundTime.Filters.Last(item => item.MemberName == "OutBoundTime").Value;
                         foreach(var filter in filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var compositeFilterItem = filter as CompositeFilterItem;
                            {
                                if(compositeFilterItem.Filters.Any(r => r.MemberName == "OutBoundTime")) {
                                    return compositeFilterItem.Filters.Where(r => r.MemberName == "OutBoundTime").Last(r => r.MemberName == "OutBoundTime").Value;
                                }
                            }
                        }
                        return null;

                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName()
        {
            return "采购退货统计";
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
            ((GridViewDataColumn)this.GridView.Columns["CostPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["CostPriceAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SettlementPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SettlementPriceAmount"]).DataFormatString = "c2";
        }
    }
}