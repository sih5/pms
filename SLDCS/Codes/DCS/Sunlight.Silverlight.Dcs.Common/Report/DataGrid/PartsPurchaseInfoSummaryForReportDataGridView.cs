﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class PartsPurchaseInfoSummaryForReportDataGridView : DcsDataGridViewBase
    {
        private readonly string[] kvNames = new[] {
            "ABCStrategy_Category","PartPriority"
        };
        public PartsPurchaseInfoSummaryForReportDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                       Name = "SupplierPartCode",
                       Title=CommonUIStrings.DataGridView_ColumnItem_Title_PartsSupplierRelation_SupplierPartCode
                    },new ColumnItem {
                     Title=   CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_SparePartCode,
                        Name = "PartCode"
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                        Name = "ReferenceCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                        Name = "PartName"
                    },new ColumnItem {
                       Title=CommonUIStrings.DataGridView_QueryItem_Title_PartsPackingFinish_PurchaseName,
                        Name = "SupplierName"
                    }, new KeyValuesColumnItem {
                        Name = "PartABC",
                        KeyValueItems = this.KeyValueManager[kvNames[0]],
                         Title=CommonUIStrings.QueryPanel_QueryItem_Title_Sparepart_PartABC,
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_Daily,
                        Name = "DailySaleNum"
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_QtyLowerLimit,
                        Name = "QtyLowerLimit"
                    },new ColumnItem {
                       Title=CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_UseQty,
                        Name = "ActualUseableQty"
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_BottomUseQty,
                        Name = "StockQty"
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_Onway,
                        Name = "SupplyOnLineQty"
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_OverTimeOnWay,
                        Name = "OverdueOnLineQty"
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_UnInbound,
                        Name = "WaitInQty",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_Unpack,
                        Name = "WaitPackQty",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_UnShelve,
                        Name = "WaitShelvesQty",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_OwerOrder,
                        Name = "OwePartNum",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_OwerNum,
                        Name = "OweOrderNum",
                        TextAlignment = TextAlignment.Right,
                    }, new KeyValuesColumnItem {
                        Name = "Priority",
                        KeyValueItems = this.KeyValueManager[kvNames[1]],
                         Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPriorityBill_Priority,
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_PriorityCreateTime,
                        Name = "PriorityCreateTime",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_DurativeDayNum,
                        Name = "DurationDay",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_TotalDurativeDayNum,
                        Name = "TotalDurativeDayNum",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_VirtualPartsBranch_IsOrderable,
                        Name = "IsOrderable",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_VirtualPartsBranch_IsSalable,
                        Name = "IsSalable",
                        TextAlignment = TextAlignment.Right,
                    }
                };
            }
        }
        protected override Type EntityType
        {
            get
            {
                return typeof(PartsPurchaseOrderFinishReport);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                foreach(var item in compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).Where(filter => filter.MemberName != "CreateTime" && filter.MemberName != "OweOrderNum"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "supplierName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "SupplierName").Value;
                    case "partCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "PartCode").Value;
                    case "priority":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "Priority").Value;
                    case "partabc":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "PartABC").Value;
                    case "oweOrderNum":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "OweOrderNum").Value;
                    case "createTime":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "CreateTime").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName()
        {
            return "获取配件优先级分析信息";
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
        }
    }
}