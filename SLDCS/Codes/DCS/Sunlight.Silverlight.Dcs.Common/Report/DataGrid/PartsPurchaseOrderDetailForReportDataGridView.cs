﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;
using System.Collections.ObjectModel;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class PartsPurchaseOrderDetailForReportDataGridView : DcsDataGridViewBase
    {
        private readonly string[] kvNames = new[] {
            "PartPriority"
        };
        private ObservableCollection<KeyValuePair> kvFinishi = new ObservableCollection<KeyValuePair>();
        public PartsPurchaseOrderDetailForReportDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
            this.kvFinishi.Add(new KeyValuePair {
                Key = 1,
                Value = CommonUIStrings.Report_QueryPanel_KeyValue_Finishi
            });
            this.kvFinishi.Add(new KeyValuePair {
                Key = 2,
                Value = CommonUIStrings.Report_QueryPanel_KeyValue_PartFinishi
            });
            this.kvFinishi.Add(new KeyValuePair {
                Key = 3,
                Value = CommonUIStrings.Report_QueryPanel_KeyValue_UnFinishi
            });
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Priority",
                        KeyValueItems = this.KeyValueManager[kvNames[0]],
                         Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPriorityBill_Priority,
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_PriorityCreateTime,
                        Name = "PriorityCreateTime",
                        TextAlignment = TextAlignment.Right,
                    }, new ColumnItem {
                       Name = "InboundPlanCode",
                       Title=CommonUIStrings.Report_QueryPanel_KeyValue_InBoundPlanCode
                    },new ColumnItem {
                     Title=   CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_PartsPurchaseOrderCode,
                        Name = "PartsPurchaseOrderCode"
                    },new ColumnItem {
                     Title=   CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_SparePartCode,
                        Name = "SparePartCode"
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                        Name = "ReferenceCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                        Name = "SparePartName"
                    },new ColumnItem {
                       Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_SupplyShippingQty,
                        Name = "ShippingAmount"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_DealerPartsInboundBill_Quantity,
                        Name = "InspectedQuantity"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_DealerPartsInboundBill_ShouldQuantity,
                        Name = "ShallInBoundQty"
                    },new ColumnItem {
                       Title=CommonUIStrings.DataGridView_Title_PartsPurchase_ShallInboundEntries,
                        Name = "ShallInboundEntries"
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchase_ActualInboundEntries,
                        Name = "ActualInboundEntries"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_SupplyShippingTime,
                        Name = "ShippingDate"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_ExpectDeliveryTime,
                        Name = "PlanDeliveryTime"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_CheckBillCreateTime,
                        Name = "InboundFinishTime",
                        TextAlignment = TextAlignment.Right,
                    },new KeyValuesColumnItem {
                        Title=CommonUIStrings.Report_QueryPanel_KeyValue_InBoundStatus,
                        Name = "InboundFinishStatus",
                        KeyValueItems=this.kvFinishi,
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_PartsSupplierName,
                        Name = "SupplierName",
                        TextAlignment = TextAlignment.Right,
                    }
                };
            }
        }
        protected override Type EntityType
        {
            get
            {
                return typeof(PartsPurchaseOrderDetailReport);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                foreach(var item in compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).Where(filter => filter.MemberName != "CreateTime" ))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "supplierName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "SupplierName").Value;
                    case "partCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "PartCode").Value;
                    case "inboundFinishStatus":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "InboundFinishStatus").Value;                
                    case "createTime":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "CreateTime").Value;
                    case "bPriorityCreateTime":
                        var bPriorityCreateTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "PriorityCreateTime");
                        return bPriorityCreateTime == null ? null : bPriorityCreateTime.Filters.First(r => r.MemberName == "PriorityCreateTime").Value;
                    case "ePriorityCreateTime":
                        var ePriorityCreateTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "PriorityCreateTime");
                        return ePriorityCreateTime == null ? null : ePriorityCreateTime.Filters.Last(item => item.MemberName == "PriorityCreateTime").Value;
                    case "bPlanDeliveryTime":
                        var bPlanDeliveryTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "PlanDeliveryTime");
                        return bPlanDeliveryTime == null ? null : bPlanDeliveryTime.Filters.First(r => r.MemberName == "PlanDeliveryTime").Value;
                    case "ePlanDeliveryTime":
                        var ePlanDeliveryTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "PlanDeliveryTime");
                        return ePlanDeliveryTime == null ? null : ePlanDeliveryTime.Filters.Last(item => item.MemberName == "PlanDeliveryTime").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName()
        {
            return "优先入库明细表";
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
        }
    }
}