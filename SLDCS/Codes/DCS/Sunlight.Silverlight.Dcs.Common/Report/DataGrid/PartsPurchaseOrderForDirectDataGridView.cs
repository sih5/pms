﻿
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;
using System.Collections.ObjectModel;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class PartsPurchaseOrderForDirectDataGridView : DcsDataGridViewBase {
        private readonly ObservableCollection<KeyValuePair> kvType = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
            "PartsPurchaseOrder_Status","PartsShipping_Method","PurchaseInStatus","PartsPurchaseOrderDetail_ShortSupReason"
        };
        public PartsPurchaseOrderForDirectDataGridView() {
            this.kvType.Add(new KeyValuePair {
                Key = 1,
                Value = CommonUIStrings.QueryPanel_Title_PartsPurchaseOrder_Status1
            });
            this.kvType.Add(new KeyValuePair {
                Key = 2,
                Value = CommonUIStrings.QueryPanel_Title_PartsPurchaseOrder_Status2
            });
            this.kvType.Add(new KeyValuePair {
                Key = 3,
                Value = CommonUIStrings.QueryPanel_Title_PartsPurchaseOrder_Status3
            });
            this.kvType.Add(new KeyValuePair {
                Key = 4,
                Value = CommonUIStrings.QueryPanel_Title_PartsPurchaseOrder_Status4
            });
            this.kvType.Add(new KeyValuePair {
                Key = 5,
                Value = CommonUIStrings.QueryPanel_Title_PartsPurchaseOrder_Status5
            });
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                       Name = "PartsSupplierName",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_PartsSupplierName,
                    }
                    , new ColumnItem {
                       Name = "PartsSupplierCode",
                        Title =CommonUIStrings.DetailPanel_Title_PartsInsteadInfo_SupplierCode,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_PartsPurchaseOrderCode,
                        Name = "Code"
                    },new KeyValuesColumnItem {
                        Title =CommonUIStrings.QueryPanel_Title_PartsPurchaseOrder_Status,
                         KeyValueItems = this.KeyValueManager[kvNames[0]],
                        Name = "Status" 
                    }
                    ,new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_Title_PartsPurchaseOrder_PartsSalesOrderTypeName,
                        Name = "PartsSalesOrderTypeName"
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_Remark,
                        Name = "Remark"
                    },new ColumnItem {
                         Title =CommonUIStrings.DataDeTailPanel_Text_BottomStock_CreatorName,
                        Name = "CreatorName",
                    },new ColumnItem {
                         Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_PersonName,
                        Name = "PersonName"
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_OriginalRequirementBillCode,
                        Name = "OriginalRequirementBillCode"
                    },new ColumnItem {
                         Title =CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        Name = "SparePartCode"
                    },new ColumnItem {
                         Title =CommonUIStrings.DataGridView_ColumnItem_Title_PartsSupplierRelation_SupplierPartCode,
                        Name = "SupplierPartCode"
                    },new ColumnItem {
                       Title =CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_SparePartName,
                        Name = "SparePartName"
                    },new ColumnItem {
                         Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_PartABC,                        
                        Name = "ABCStrategy"
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_PlanAmount,
                        Name = "OrderAmount"
                    }
                    ,new ColumnItem {
                       Title =CommonUIStrings.QueryItem_Title_PartsOutboundBillCentral_ConfirmAmount,
                        Name = "ConfirmedAmount"
                    }, new KeyValuesColumnItem {
                        Name = "ShortSupReason",
                        KeyValueItems=this.KeyValueManager[kvNames[3]],
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_ShortSupReason,
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_Quantity,
                        Name = "Quantity"
                    },new ColumnItem {
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_ShipCon,
                        Name = "ShipCon"
                    },new ColumnItem {
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_InspectedQuantity,
                        Name = "InspectedQuantity"
                    },new ColumnItem {
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_OrderShippingTime,
                        Name = "OrderShippingTime"
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_CloserName,
                        Name = "CloserName"
                    },new KeyValuesColumnItem {
                       Title =CommonUIStrings.QueryPanel_Title_PartsPurchaseOrder_Finish,
                        KeyValueItems = this.kvType,
                        Name = "FinishStatus"
                    },new ColumnItem {
                       Title =CommonUIStrings.QueryPanel_Title_PartsPurchaseOrder_IsTransSap,
                        Name = "IsTransSap"
                    },new ColumnItem {
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlanReport_SubmitTime,
                        Name = "SubmitTime"
                    },new ColumnItem {
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_SupplyConfirmTime,
                        Name = "ApproveTime"
                    },new ColumnItem {
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_SupplyShippingTime,
                        Name = "ShippingDate"
                    },new ColumnItem {
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_ExpectedArrivalDate,
                        Name = "ExpectedArrivalDate"
                    },new ColumnItem {
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_ConfirmationTime,
                        Name = "ConfirmationTime"
                    },new ColumnItem {
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_SIHInboundDate,
                        Name = "SIHInboundDate"
                    },new ColumnItem {
                       Title =CommonUIStrings.DataEditView_Notification_Type_LogisticCompany,
                        Name = "LogisticCompany"
                    },new KeyValuesColumnItem {
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrderForReport_ShippingMethod,
                        Name = "ShippingMethod",
                        KeyValueItems = this.KeyValueManager[kvNames[1]],
                    },new ColumnItem {
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrderForReport_DeliveryBillNumber2,
                        Name = "DeliveryBillNumber"
                    },new KeyValuesColumnItem {
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlanReport_InStatus,
                        Name = "InStatus",
                         KeyValueItems = this.KeyValueManager[kvNames[2]],
                    },new ColumnItem {
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_ReceivingCompanyName,
                        Name = "ReceivingCompanyName"
                    },new ColumnItem {
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrderForReport_ReceivingAddress,
                        Name = "ReceivingAddress"
                    },new ColumnItem {
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrderForReport_LinkName,
                        Name = "ContactPerson"
                    },new ColumnItem {
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrderForReport_LinkNumber,
                        Name = "ContactPhone"
                    },new ColumnItem {
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrderForReport_ShipRemark,
                        Name = "ShipRemark"
                    }
                };
            }
        }
        protected override Type EntityType {
            get {
                return typeof(PartsPurchaseOrderForDirect);
            }
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
        //protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
        //    var compositeFilterItem = this.FilterItem as CompositeFilterItem;
        //    var newCompositeFilterItem = new CompositeFilterItem();
        //    if(compositeFilterItem != null) {
        //        foreach(var item in compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).Where(filter => filter.MemberName != "increaseRateGroupId" && filter.MemberName != "isEffective"))
        //            newCompositeFilterItem.Filters.Add(item);
        //    }
        //    return newCompositeFilterItem.ToFilterDescriptor();
        //}

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch(parameterName) {
                    case "code":
                        return filters.Filters.Single(item => item.MemberName == "Code").Value;
                    case "partsSupplierName":
                        return filters.Filters.Single(item => item.MemberName == "PartsSupplierName").Value;
                    case "sparepartCode":
                        return filters.Filters.Single(item => item.MemberName == "SparePartCode").Value;
                    case "finishStatus":
                        return filters.Filters.Single(item => item.MemberName == "FinishStatus").Value;
                    case "status":
                        return filters.Filters.Single(item => item.MemberName == "Status").Value;
                    case "originalRequirementBillCode":
                        return filters.Filters.Single(item => item.MemberName == "OriginalRequirementBillCode").Value;
                    case "personName":
                        return filters.Filters.Single(item => item.MemberName == "PersonName").Value;                 
                    case "bSubmitTime":
                        var bTheoryDeliveryTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "SubmitTime");
                        return bTheoryDeliveryTime == null ? null : bTheoryDeliveryTime.Filters.First(r => r.MemberName == "SubmitTime").Value;
                    case "eSubmitTime":
                        var eTheoryDeliveryTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "SubmitTime");
                        return eTheoryDeliveryTime == null ? null : eTheoryDeliveryTime.Filters.Last(item => item.MemberName == "SubmitTime").Value;

                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName() {
            return "直供采购订单完成情况统计";
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;           
        }
    }
}
