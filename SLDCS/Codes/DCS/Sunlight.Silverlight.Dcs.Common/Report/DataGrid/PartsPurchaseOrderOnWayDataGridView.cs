﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;


namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class PartsPurchaseOrderOnWayDataGridView : DcsDataGridViewBase
    {
        private readonly string[] kvNames = new[] {
            "ABCStrategy_Category"
        };
        public PartsPurchaseOrderOnWayDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                       Name = "PurchasePlanCode",
                       Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_PurchasePlanCode
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_PartsPurchaseOrderCode,
                        Name = "PurchaseCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierName,
                        Name = "SupplierName" 
                    },new ColumnItem {
                         Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsSupplierRelation_SupplierPartCode,
                        Name = "SupplierPartCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_SparePartCode,
                        Name = "SpareCode"
                    },new ColumnItem {
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                        Name = "SihCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_StorehouseCensus_SparePartName,
                        Name = "MapName"
                    }, new KeyValuesColumnItem {
                        Name = "PartABC",
                        KeyValueItems = this.KeyValueManager[kvNames[0]],
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_Sparepart_PartABC,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_PurchasePlanQty,
                        Name = "PurchasePlanQty"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_SupplyConfirmQty,
                        Name = "SupplyConfirmQty"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_SupplyConfirmReason,
                        Name = "SupplyConfirmReason"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_SupplyShippingQty,
                        Name = "SupplyShippingQty"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_InspectedQuantity,
                        Name = "InboundQty"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_InboundForceQty,
                        Name = "InboundForceQty"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_OnLineQty,
                        Name = "OnLineQty"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartsPurchaseOrderDate,
                        Name = "PurchaseCreateTime"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_SupplyConfirmTime,
                        Name = "SupplyConfirmTime"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_SupplyShippingTime,
                        Name = "SupplyShippingTime"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_ExpectDeliveryTime,
                        Name = "ExpectDeliveryTime"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_CreateTime,
                        Name = "InboundTime"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_PlanType,
                        Name = "PlanType"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_WarehouseName,
                        Name = "WarehouseName"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_OrderCycle,
                        Name = "SupplyShippingCycle"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_ArrivalCycle,
                        Name = "SupplyArrivalCycle"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_TheoryDeliveryTime,
                        Name = "TheoryDeliveryTime"
                    },new ColumnItem {
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_KvOverTime,
                        Name = "OverTime"
                    },new ColumnItem {
                        Title=  CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_KvUndueTime,
                        Name = "UndueTime"
                    },new ColumnItem {
                        Title= CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlanReport_Remark,
                        Name = "PurchasePlanRemark"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_KvFinishi,
                        Name = "FinishDescribe"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_Weight,
                        Name = "Weight"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_Volume,
                        Name = "Volume"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_SalesPrice,
                        Name = "Salesprice"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_PersonName,
                        Name = "PersonName"
                    }
                };
            }
        }
        protected override Type EntityType
        {
            get
            {
                return typeof(PartsPurchaseOrderFinish);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                foreach(var item in compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).Where(filter => filter.MemberName != "PurchasePlanTime" || filter.MemberName != "TheoryDeliveryTime")) {
                }
                  
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "kvFinishi":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "KvFinishi").Value;
                    case "kvOverTime":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "KvOverTime").Value;
                    case "kvUndueTime":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "KvUndueTime").Value;
                    case "bPurchasePlanTime":
                        var bPurchasePlanTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "PurchasePlanTime");
                        return bPurchasePlanTime == null ? null : bPurchasePlanTime.Filters.First(r => r.MemberName == "PurchasePlanTime").Value;
                    case "ePurchasePlanTime":
                        var ePurchasePlanTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "PurchasePlanTime");
                        return ePurchasePlanTime == null ? null : ePurchasePlanTime.Filters.Last(item => item.MemberName == "PurchasePlanTime").Value;
                    case "bTheoryDeliveryTime":
                        var bTheoryDeliveryTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "TheoryDeliveryTime");
                        return bTheoryDeliveryTime == null ? null : bTheoryDeliveryTime.Filters.First(r => r.MemberName == "TheoryDeliveryTime").Value;
                    case "eTheoryDeliveryTime":
                        var eTheoryDeliveryTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "TheoryDeliveryTime");
                        return eTheoryDeliveryTime == null ? null : eTheoryDeliveryTime.Filters.Last(item => item.MemberName == "TheoryDeliveryTime").Value;
                   

                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName()
        {
            return "采购单在途明细表";
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
        }
    }
}