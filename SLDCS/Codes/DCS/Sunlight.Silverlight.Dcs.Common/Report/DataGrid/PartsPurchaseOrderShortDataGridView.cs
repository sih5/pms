﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;


namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class PartsPurchaseOrderShortDataGridView : DcsDataGridViewBase
    {
        private readonly string[] kvNames = new[] {
            "ABCStrategy_Category"
        };
        public PartsPurchaseOrderShortDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                       Name = "PurchasePlanCode",
                       Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_PurchasePlanCode
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_PartsPurchaseOrderCode,
                        Name = "PurchaseCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierName,
                        Name = "SupplierName" 
                    },new ColumnItem {
                         Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsSupplierRelation_SupplierPartCode,
                        Name = "SupplierPartCode"
                    },new ColumnItem {
                       Title= CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_SparePartCode,
                        Name = "SpareCode"
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                        Name = "SihCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_StorehouseCensus_SparePartName,
                        Name = "MapName"
                    }, new KeyValuesColumnItem {
                        Name = "PartABC",
                        KeyValueItems = this.KeyValueManager[kvNames[0]],
                         Title=CommonUIStrings.QueryPanel_QueryItem_Title_Sparepart_PartABC,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_PurchasePlanQty,
                        Name = "PurchasePlanQty"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_SupplyConfirmQty,
                        Name = "SupplyConfirmQty"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_SupplyConfirmReason,
                        Name = "SupplyConfirmReason"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_SupplyShippingQty,
                        Name = "SupplyShippingQty"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartsPurchaseOrderDate,
                        Name = "PurchaseCreateTime"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_SupplyConfirmTime,
                        Name = "SupplyConfirmTime"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_InboundQty,
                        Name = "InboundQty"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_OwningQty,
                        Name = "OwningQty"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_PersonName,
                        Name = "PersonName"
                    }
                };
            }
        }
        protected override Type EntityType
        {
            get
            {
                return typeof(PartsPurchaseOrderFinish);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                foreach(var item in compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).Where(filter => filter.MemberName != "PurchasePlanTime" || filter.MemberName != "TheoryDeliveryTime")) {
                }
                  
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "bPurchasePlanTime":
                        var bPurchasePlanTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "PurchasePlanTime");
                        return bPurchasePlanTime == null ? null : bPurchasePlanTime.Filters.First(r => r.MemberName == "PurchasePlanTime").Value;
                    case "ePurchasePlanTime":
                        var ePurchasePlanTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "PurchasePlanTime");
                        return ePurchasePlanTime == null ? null : ePurchasePlanTime.Filters.Last(item => item.MemberName == "PurchasePlanTime").Value;
                    case "bTheoryDeliveryTime":
                        var bTheoryDeliveryTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "TheoryDeliveryTime");
                        return bTheoryDeliveryTime == null ? null : bTheoryDeliveryTime.Filters.First(r => r.MemberName == "TheoryDeliveryTime").Value;
                    case "eTheoryDeliveryTime":
                        var eTheoryDeliveryTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "TheoryDeliveryTime");
                        return eTheoryDeliveryTime == null ? null : eTheoryDeliveryTime.Filters.Last(item => item.MemberName == "TheoryDeliveryTime").Value;
                   

                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName()
        {
            return "采购单短供明细";
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
        }
    }
}