﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Telerik.Windows.Controls;


namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class PartsPurchaseOrderSupplierFinishDataGridView: DcsDataGridViewBase
    {
        public PartsPurchaseOrderSupplierFinishDataGridView()
        {
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                       Name = "Month",
                       Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_Month
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_SupplierName,
                        Name = "SupplierName"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_AllItem,
                        Name = "AllItem" 
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_ArriveItem,
                        Name = "ArriveItem"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_OnTime,
                        Name = "OnTime"
                    },new ColumnItem {
                         Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_OutTime,
                        Name = "OutTime"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_SevenUn,
                        Name = "SevenUn"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_EightUn,
                        Name = "EightUn"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_TwentyUn,
                        Name = "TwentyUn"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_OverTwentyUn,
                        Name = "OverTwentyUn"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_StopAmount,
                        Name = "StopAmount"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_Undue,
                        Name = "Undue"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_Mzl,
                        Name = "Mzl"
                    }
                };
            }
        }
        protected override Type EntityType
        {
            get
            {
                return typeof(PartsPurchaseOrderFinish);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                foreach(var item in compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).Where(filter => filter.MemberName != "PurchasePlanTime" || filter.MemberName != "TheoryDeliveryTime")) {
                }
                  
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "supplierName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "SupplierName").Value;
                    case "bPurchasePlanTime":
                        var bPurchasePlanTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "PurchasePlanTime");
                        return bPurchasePlanTime == null ? null : bPurchasePlanTime.Filters.First(r => r.MemberName == "PurchasePlanTime").Value;
                    case "ePurchasePlanTime":
                        var ePurchasePlanTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "PurchasePlanTime");
                        return ePurchasePlanTime == null ? null : ePurchasePlanTime.Filters.Last(item => item.MemberName == "PurchasePlanTime").Value;
                    case "bTheoryDeliveryTime":
                        var bTheoryDeliveryTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "TheoryDeliveryTime");
                        return bTheoryDeliveryTime == null ? null : bTheoryDeliveryTime.Filters.First(r => r.MemberName == "TheoryDeliveryTime").Value;
                    case "eTheoryDeliveryTime":
                        var eTheoryDeliveryTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "TheoryDeliveryTime");
                        return eTheoryDeliveryTime == null ? null : eTheoryDeliveryTime.Filters.Last(item => item.MemberName == "TheoryDeliveryTime").Value;
                    case "isExport":
                        return false;

                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName()
        {
            return "供应商采购订单完成情况统计";
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
        }
    }
}