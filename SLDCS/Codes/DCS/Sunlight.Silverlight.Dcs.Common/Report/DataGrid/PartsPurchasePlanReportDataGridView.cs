﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid
{
    public class PartsPurchasePlanReportDataGridView : DcsDataGridViewBase
    {
        private readonly string[] kvNames = new[] {
            "ABCStrategy_Category","PartsPurchaseOrderDetail_ShortSupReason","PartsPurchaseOrder_Status","PurchasePlanOrderStatus","PurchaseInStatus"
        };
        public PartsPurchasePlanReportDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                       Name = "Code",
                       Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_PurchasePlanCode
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_PartsPurchaseOrderCode,
                        Name = "PartsPurchaseOrderCode"
                    },new ColumnItem {
                         Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsSupplierRelation_SupplierPartCode,
                        Name = "SupplierPartCode" 
                    },new ColumnItem {
                       Title= CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_SparePartCode,
                        Name = "SparePartCode"
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                        Name = "ReferenceCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_StorehouseCensus_SparePartName,
                        Name = "SparePartName"
                    }, new KeyValuesColumnItem {
                        Name = "PartABC",
                        KeyValueItems = this.KeyValueManager[kvNames[0]],
                         Title=CommonUIStrings.QueryPanel_QueryItem_Title_Sparepart_PartABC,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_PurchasePlanQty,
                        Name = "PlanAmount"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_SupplyConfirmQty,
                        Name = "ConfirmedAmount"
                    }, new KeyValuesColumnItem {
                        Name = "ShortSupReason",
                        KeyValueItems = this.KeyValueManager[kvNames[1]],
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_SupplyConfirmReason
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_SupplyShippingQty,
                        Name = "ShippingAmount"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_InspectedQuantity,
                        Name = "InspectedQuantity"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_InboundForceQty,
                        Name = "CompletionQuantity"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_OnLineQty,
                        Name = "OnWayNumber"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartsPurchaseOrderDate,
                        Name = "PartsPurchaseOrderDate"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_SupplyConfirmTime,
                        Name = "ConfirmationDate"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_SupplyShippingTime,
                        Name = "ShippingDate"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_ExpectDeliveryTime,
                        Name = "ExpectedArrivalDate"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_CreateTime,
                        Name = "PartsInboundCheckBillDate",
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_PlanType,
                        Name = "PartsPlanTypeName",
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlanReport_SubmitTime,
                        Name = "SubmitTime",
                    },new KeyValuesColumnItem {
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlanReport_Status,
                        Name = "Status",
                         KeyValueItems = this.KeyValueManager[kvNames[3]],
                    }, new KeyValuesColumnItem {
                        Name = "PartsPurchaseOrderStatus",
                        KeyValueItems = this.KeyValueManager[kvNames[2]],
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlanReport_PartsPurchaseOrderStatus,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_WarehouseName,
                        Name = "WarehouseName"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlanReport_SubmitterName,
                        Name = "SubmitterName"
                    }, new ColumnItem {
                        Name = "PartsSupplierName",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierName,
                    }, new ColumnItem {
                        Name = "OrderCycle",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_OrderCycle
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_ArrivalCycle,
                        Name = "ArrivalCycle",
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_TheoryDeliveryTime,
                        Name = "TheoreticalArrivalDate",
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlanReport_ShippingStatus,
                        Name = "ShippingStatus",
                    },new KeyValuesColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlanReport_InStatus,
                         KeyValueItems = this.KeyValueManager[kvNames[4]],
                        Name = "InStatus",
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlanReport_PackingMaterialReF,
                        Name = "PackingMaterialReF",
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlanReport_PackingMaterial,
                        Name = "PackingMaterial",
                    }
                    ,new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlanReport_PackingCoefficient,
                        Name = "PackingCoefficient",
                    }
                    ,new ColumnItem {
                        Title= CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlanReport_Remark,
                        Name = "Remark",
                    }
                    ,new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_KvFinishi,
                        Name = "CompletionSituation",
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_PersonName,
                        Name = "PersonName",
                    }
                };
            }
        }
        protected override Type EntityType
        {
            get
            {
                return typeof(PartsPurchasePlanReport);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName)
        {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if (compositeFilterItem != null)
            {
                foreach (var item in compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).Where(filter => filter.MemberName != "IsOnWayNumber" && filter.MemberName != "PartsPurchasePlanTime"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "sparePartCode":
                        return filters.Filters.Single(item => item.MemberName == "SparePartCode").Value;
                    case "partsSupplierName":
                        return filters.Filters.Single(item => item.MemberName == "PartsSupplierName").Value;
                    case "supplierPartCode":
                        return filters.Filters.Single(item => item.MemberName == "SupplierPartCode").Value;
                    case "status":
                        return filters.Filters.Single(item => item.MemberName == "Status").Value;
                    case "partsPurchaseOrderStatus":
                        return filters.Filters.Single(item => item.MemberName == "PartsPurchaseOrderStatus").Value;
                    case "isOnWayNumber":
                        return filters.Filters.Single(item => item.MemberName == "IsOnWayNumber").Value;
                    case "remark":
                        return filters.Filters.Single(item => item.MemberName == "Remark").Value;
                    case "isOrderable":
                        return filters.Filters.Single(item => item.MemberName == "IsOrderable").Value;
                    case "bPartsPurchasePlanTime":
                        var createTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "PartsPurchasePlanTime");
                        return createTime == null ? null : createTime.Filters.First(r => r.MemberName == "PartsPurchasePlanTime").Value;
                    case "ePartsPurchasePlanTime":
                        var createTime1 = composites.FirstOrDefault(p => p.Filters.First().MemberName == "PartsPurchasePlanTime");
                        return createTime1 == null ? null : createTime1.Filters.Last(item => item.MemberName == "PartsPurchasePlanTime").Value;
                    case "bPartsPurchaseOrderDate":
                        var expectedPlaceDate = composites.FirstOrDefault(p => p.Filters.First().MemberName == "PartsPurchaseOrderDate");
                        return expectedPlaceDate == null ? null : expectedPlaceDate.Filters.First(r => r.MemberName == "PartsPurchaseOrderDate").Value;
                    case "ePartsPurchaseOrderDate":
                        var expectedPlaceDate1 = composites.FirstOrDefault(p => p.Filters.First().MemberName == "PartsPurchaseOrderDate");
                        return expectedPlaceDate1 == null ? null : expectedPlaceDate1.Filters.Last(item => item.MemberName == "PartsPurchaseOrderDate").Value;
                    case "bSubmitTime":
                        var bSubmitTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "SubmitTime");
                        return bSubmitTime == null ? null : bSubmitTime.Filters.First(r => r.MemberName == "SubmitTime").Value;
                    case "eSubmitTime":
                        var eSubmitTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "SubmitTime");
                        return eSubmitTime == null ? null : eSubmitTime.Filters.Last(item => item.MemberName == "SubmitTime").Value;

                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName()
        {
            return "GetPartsPurchasePlanReports";
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
        }
    }
}
