﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class PartsPurchasePlanTempDataGridView: DcsDataGridViewBase
    {
        private readonly string[] kvNames = new[] {
            "PartsPurchaseOrder_Status","BaseData_Status"
        };
        public PartsPurchasePlanTempDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                       Name = "PartsPurchaseOrderCode",
                       Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_PartsPurchaseOrderCode
                    }, new KeyValuesColumnItem {
                        Name = "PartsPurchaseOrderStatus",
                        KeyValueItems = this.KeyValueManager[kvNames[0]],
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlanReport_PartsPurchaseOrderStatus,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_WarehouseId,
                        Name = "WarehouseName"
                    },new ColumnItem {
                       Title= CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        Name = "SparePartCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_StorehouseCensus_SparePartName,
                        Name = "SparePartName"
                    },new ColumnItem {
                        Title="包材采购数量",
                        Name = "OrderAmount"
                    },new ColumnItem {
                        Title="包材图号",
                        Name = "PackSparePartCode"
                    },new ColumnItem {
                        Title="包材名称",
                        Name = "PackSparePartName"
                    },new ColumnItem {
                        Title="单位包装数量（包装设置）",
                        Name = "PackNum"
                    },new ColumnItem {
                        Title="备件采购数量",
                        Name = "PackPlanQty"
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[kvNames[1]],
                        Title = "状态"
                    },new ColumnItem {
                        Title="作废时间",
                        Name = "ModifyTime"
                    }
                };
            }
        }
        protected override Type EntityType
        {
            get
            {
                return typeof(VirtualPartsPurchasePlanTemp);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }      
        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "partsPurchaseOrderStatus":
                        return filters.Filters.Single(item => item.MemberName == "PartsPurchaseOrderStatus").Value;
                    case "partsPurchaseOrderCode":
                        return filters.Filters.Single(item => item.MemberName == "PartsPurchaseOrderCode").Value;
                    case "sparePartCode":
                        return filters.Filters.Single(item => item.MemberName == "SparePartCode").Value;
                    case "packSparePartCode":
                        return filters.Filters.Single(item => item.MemberName == "PackSparePartCode").Value;   
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName()
        {
            return "采购订单对应包材明细查询";
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
        }
    }
}
