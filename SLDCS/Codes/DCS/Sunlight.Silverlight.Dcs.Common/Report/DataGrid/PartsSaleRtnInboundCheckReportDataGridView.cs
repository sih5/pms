﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;


namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class PartsSaleRtnInboundCheckReportDataGridView : DcsDataGridViewBase
    {
         private readonly string[] kvNames = new[] {
            "ABCStrategy_Category","PartsPurchaseSettle_Status"
        };
         public PartsSaleRtnInboundCheckReportDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                       Name = "MarketingDepartmentName",
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_MarketingDepartmentName,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_Province,
                        Name = "Province"
                    } ,new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_WarehouseName,
                        Name = "WarehouseName"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_CounterpartCompanyName,
                        Name = "CounterpartCompanyName"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_SubmitCompanyCode,
                        Name = "CounterpartCompanyCode" 
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_PartsSalesReturnBillCodeNew,
                        Name = "PartsSalesReturnBillCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_ReturnWarehouseCode,
                        Name = "ReturnWarehouseCode"
                    } 
                   , new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_ReturnReason,
                        Name = "ReturnReason"
                    }
                    ,new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_Code,
                        Name = "Code"
                    },new ColumnItem {
                     Title=   CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_SparePartCode,
                        Name = "SparePartCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                        Name = "SparePartName"
                    }, new KeyValuesColumnItem {
                        Name = "PartABC",
                        KeyValueItems = this.KeyValueManager[kvNames[0]],
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_PartABC
                    },new KeyValuesColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SettleBillStatus,
                        KeyValueItems = this.KeyValueManager[kvNames[1]],
                        Name = "SettlementStatus",
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_InspectedQuantity,
                        Name = "InspectedQuantity",
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_OriginalPrice,
                        Name = "OriginalPrice",
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_OriginalPriceAll,
                        Name = "OriginalPriceAll",
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_SettlementPrice,
                        Name = "SettlementPrice",    
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_SettlementPriceAll,
                        Name = "SettlementPriceAll",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_IsDiscount,
                        Name = "IsDiscount"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_PriceType,
                        Name = "PriceType"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_CostPrice,
                        Name = "CostPrice",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_CostPriceAll,
                        Name = "CostPriceAll",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SettleBillCode,
                        Name = "SettlementNo"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_SAPSysInvoiceNumber,
                        Name = "SAPSysInvoiceNumber"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_Invoice,
                        Name = "InvoiceNumber"
                    },new ColumnItem {
                       Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_CreateTime,
                        Name = "CreateTime"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_CreatorName,
                        Name = "CreatorName"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_IsRed,
                        Name = "IsRed"
                    }
                };
            }
        }
        protected override Type EntityType
        {
            get
            {
                return typeof(PartsSaleRtnInboundCheck);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "SettlementStatus"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();

        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();

                switch (parameterName)
                {
                    case "partsSalesReturnBillCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "PartsSalesReturnBillCode").Value;
                    case "partsSalesOrderCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "PartsSalesOrderCode").Value;
                    case "settlementNo":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "SettlementNo").Value;
                    case "counterpartCompanyName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "CounterpartCompanyName").Value;
                    case "counterpartCompanyCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "CounterpartCompanyCode").Value;
                    case "sparePartCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "SparePartCode").Value;
                    case "settlementStatus":
                       // return filters.Filters.SingleOrDefault(item => item.MemberName == "SettlementStatus").Value;
                         string settlementStatus = string.Empty;
                        foreach(var filter in filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var compositeFilterItem = filter as CompositeFilterItem;
                            {
                                if(compositeFilterItem.Filters.Any(r => r.MemberName == "SettlementStatus"))
                                    settlementStatus = string.Join(",", compositeFilterItem.Filters.Where(r => r.MemberName == "SettlementStatus").Select(r => r.Value));
                            }
                        }
                        return settlementStatus;
                    case "code":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "Code").Value;
                    case "sAPSysInvoiceNumber":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "SAPSysInvoiceNumber").Value;
                    case "bCreateTime":
                        //var bCreateTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                      //  return bCreateTime == null ? null : bCreateTime.Filters.First(item => item.MemberName == "CreateTime").Value;
                          foreach(var filter in filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var compositeFilterItem = filter as CompositeFilterItem;
                            {
                                if(compositeFilterItem.Filters.Any(r => r.MemberName == "CreateTime")) {
                                    return compositeFilterItem.Filters.Where(r => r.MemberName == "CreateTime").First(r => r.MemberName == "CreateTime").Value;
                                }
                            }
                        }
                        return null;
                    case "eCreateTime":
                          foreach(var filter in filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var compositeFilterItem = filter as CompositeFilterItem;
                            {
                                if(compositeFilterItem.Filters.Any(r => r.MemberName == "CreateTime")) {
                                    return compositeFilterItem.Filters.Where(r => r.MemberName == "CreateTime").Last(r => r.MemberName == "CreateTime").Value;
                                }
                            }
                        }
                        return null;
                        //var eCreateTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        //return eCreateTime == null ? null : eCreateTime.Filters.Last(item => item.MemberName == "CreateTime").Value;

                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName()
        {
            return "销售退货入库单统计";
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
            ((GridViewDataColumn)this.GridView.Columns["CostPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["CostPriceAll"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SettlementPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SettlementPriceAll"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OriginalPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OriginalPriceAll"]).DataFormatString = "c2";
        }
    }
}
