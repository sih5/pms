﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;
namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid
{
    public class PartsSalesOrderFinishReportDataGridView : DcsDataGridViewBase
    {
         private readonly string[] kvNames = new[] {
            "ABCStrategy_Category","PartsSalesOrder_Status","PartsBranch_PurchaseRoute","PartsOutboundPlan_Status","PartsSalesOrderProcessDetail_ProcessMethod"
        };
         public PartsSalesOrderFinishReportDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                       Name = "MarketingDepartmentName",
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_MarketingDepartmentName,
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_WarehouseId,
                        Name = "WarehouseName"
                    }
                    ,new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_Province,
                        Name = "Province" 
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_SubmitCompanyCode,
                        Name = "SubmitCompanyCode"
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_SubmitCompanyName,
                        Name = "SubmitCompanyName"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_ReceivingCompanyName,
                        Name = "ReceivingCompanyName"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_SparePartCode,
                        Name = "SparePartCode"
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                        Name = "ReferenceCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                        Name = "SparePartName"
                    }
                    ,new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_EnglishName,
                        Name = "EnglishName"
                    }, new KeyValuesColumnItem {
                        Name = "PartABC",
                        KeyValueItems = this.KeyValueManager[kvNames[0]],
                         Title=CommonUIStrings.QueryPanel_QueryItem_Title_Sparepart_PartABC,
                    }, new KeyValuesColumnItem {
                        Name = "PurchaseRoute",
                        KeyValueItems = this.KeyValueManager[kvNames[2]],
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_PurchaseRoute
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_IfDirectProvision,
                        Name = "IfDirectProvision"
                    },new KeyValuesColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_OrderProcessMethod,
                        Name = "OrderProcessMethod",
                        KeyValueItems = this.KeyValueManager[kvNames[4]],
                    }
                    ,new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_OrderedQuantity,
                        Name = "OrderedQuantity"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_ApproveQuantity,
                        Name = "ApproveQuantity"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_UnApproveQuantity,
                        Name = "UnApproveQuantity"
                    }
                    //,new ColumnItem {
                    //    Title="已拣货量",
                    //    Name = "PickingQty"
                    //}
                    ,new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_OutboundAmount,
                        Name = "OutboundAmount"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_DifferenceQuantity,
                        Name = "DifferenceQuantity"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_Sparepart_IncreaseRateGroupId,
                        Name = "GroupName"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_OrderPriceDj,
                        Name = "OrderPrice",
                         TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_OrderSumJe,
                        Name = "OrderSum",
                         TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_OriginalPrice,
                        Name = "OriginalPrice",
                         TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_OriginalPriceSum,
                        Name = "OriginalPriceSum",
                         TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_Weight,
                        Name = "Weight",
                         TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_Volume,
                       Name = "Volume",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_PartsSalesOrderCode,
                        Name = "Code"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_PartsSalesOrderProcessCode,
                        Name = "PartsSalesOrderProcessCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_Time,
                        Name = "Time"
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_PartsOutboundBillCode,
                        Name = "PartsOutboundBillCode"
                    },new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[kvNames[1]],
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_StatusNew
                    },new KeyValuesColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_PartsOutboundPlanStatusStr,  
                        Name = "PartsOutboundPlanStatus",
                        KeyValueItems = this.KeyValueManager[kvNames[3]],
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_ApproveTimeFir,
                        Name = "ApproveTime"
                    }
                    ,new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_SalesRemark,
                        Name = "Remark"
                    }
                    ,new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_FirstApproveTime,
                        Name = "FirstApproveTime"
                    }
                    ,new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_ShippingDate,
                        Name = "ShippingDate"
                    } ,new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_ShippingCreateTime,
                        Name = "ShippingCreatTime"
                    },new ColumnItem {
                         Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_PartsSalesOrderTypeName,
                        Name = "PartsSalesOrderTypeName"
                    },new ColumnItem {
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_ContractCode,
                        Name = "ContractCode"
                    },new ColumnItem {
                       Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_CreateTimes,
                        Name = "CreateTime"
                    },new ColumnItem{
                    Title=CommonUIStrings.Report_QueryPanel_Title_SubmitTime,
                    Name="SubmitTime"
                    }
                };
            }
        }
        protected override Type EntityType
        {
            get
            {
                return typeof(PartsSalesOrderFinishReport);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName)
        {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if (compositeFilterItem != null)
            {
                foreach (var item in compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).Where(filter => filter.MemberName != "WarehouseId" || filter.MemberName != "PartsPurchaseOrderCode"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();

        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "code":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "Code").Value;
                    case "referenceCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "ReferenceCode").Value;
                    case "warehouseId":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "WarehouseId").Value;
                    case "submitCompanyName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "SubmitCompanyName").Value;
                    case "sparePartCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "SparePartCode").Value;
                    case "status":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "Status").Value;
                    case "partsOutboundBillCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "PartsOutboundBillCode").Value;
                    case "contractCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "ContractCode").Value;
                    case "partsPurchaseOrderCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "PartsPurchaseOrderCode").Value;
                    case "marketingDepartmentName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "MarketingDepartmentName").Value;
                    case "partsSalesOrderTypeId":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "PartsSalesOrderTypeId").Value;
                    case "bCreateTime":
                        var bCreateTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return bCreateTime == null ? null : bCreateTime.Filters.First(r => r.MemberName == "CreateTime").Value;
                    case "eCreateTime":
                        var eCreateTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return eCreateTime == null ? null : eCreateTime.Filters.Last(item => item.MemberName == "CreateTime").Value;
                    case "bApproveTime":
                        var bApproveTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "ApproveTime");
                        return bApproveTime == null ? null : bApproveTime.Filters.First(r => r.MemberName == "ApproveTime").Value;
                    case "eApproveTime":
                        var eApproveTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "ApproveTime");
                        return eApproveTime == null ? null : eApproveTime.Filters.Last(item => item.MemberName == "ApproveTime").Value;
                    case "bPickingFinishTime":
                        var bPickingFinishTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "PickingFinishTime");
                        return bPickingFinishTime == null ? null : bPickingFinishTime.Filters.First(item => item.MemberName == "PickingFinishTime").Value;
                    case "ePickingFinishTime":
                        var ePickingFinishTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "PickingFinishTime");
                        return ePickingFinishTime == null ? null : ePickingFinishTime.Filters.Last(item => item.MemberName == "PickingFinishTime").Value;
                    case "bShippingDate":
                        var bShippingDate = composites.FirstOrDefault(p => p.Filters.First().MemberName == "ShippingDate");
                        return bShippingDate == null ? null : bShippingDate.Filters.First(item => item.MemberName == "ShippingDate").Value;
                    case "eShippingDate":
                        var eShippingDate = composites.FirstOrDefault(p => p.Filters.First().MemberName == "ShippingDate");
                        return eShippingDate == null ? null : eShippingDate.Filters.Last(item => item.MemberName == "ShippingDate").Value;
                    case "bSubmitTime":
                        var bSubmitTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "SubmitTime");
                        return bSubmitTime == null ? null : bSubmitTime.Filters.First(item => item.MemberName == "SubmitTime").Value;
                    case "eSubmitTime":
                        var eSubmitTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "SubmitTime");
                        return eSubmitTime == null ? null : eSubmitTime.Filters.Last(item => item.MemberName == "SubmitTime").Value;

                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName()
        {
            return "GetPartsSalesOrderFinishReports";
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
            ((GridViewDataColumn)this.GridView.Columns["OrderPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OrderSum"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OriginalPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OriginalPriceSum"]).DataFormatString = "c2";
        }
    }
}