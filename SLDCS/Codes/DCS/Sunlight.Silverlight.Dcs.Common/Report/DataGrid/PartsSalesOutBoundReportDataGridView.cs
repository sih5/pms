﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class PartsSalesOutBoundReportDataGridView : DcsDataGridViewBase
    {
         private readonly string[] kvNames = new[] {
            "ABCStrategy_Category","PartsSalesSettlement_Status","Company_Type"
        };
         public PartsSalesOutBoundReportDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                       Name = "MarketingDepartmentName",
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_MarketingDepartmentName,
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_WarehouseId,
                        Name = "WarehouseName"
                    }
                    ,new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_Province,
                        Name = "Province" 
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_SubmitCompanyCode,
                        Name = "SubmitCompanyCode"
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_SubmitCompanyName,
                        Name = "SubmitCompanyName"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_CustomerTypeStr,
                        Name = "CustomerTypeStr"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_PartsSalesOrderCode,
                        Name = "Code"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_PartsSalesOrderProcessCode,
                        Name = "PartsSalesOrderProcessCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_PartsOutboundBillCode,
                        Name = "PartsOutboundBillCode"
                    }
                    ,new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                        Name = "ReferenceCode"
                    } ,new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_SparepartCode,
                        Name = "SparePartCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_StorehouseCensus_SparePartName,
                        Name = "SparePartName"
                    },new ColumnItem {
                         Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_PartsSalesOrderTypeName,
                        Name = "PartsSalesOrderTypeName"
                    }, new KeyValuesColumnItem {
                        Name = "PartABC",
                        KeyValueItems = this.KeyValueManager[kvNames[0]],
                         Title=CommonUIStrings.QueryPanel_QueryItem_Title_Sparepart_PartABC,
                    }
                    ,new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_OutboundAmount,
                        Name = "OutboundAmount",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_CostPrice,
                        Name = "CostPrice",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_CostPriceAmount,
                        Name = "CostPriceAll",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_RetailGuidePrice,
                        Name = "OriginalPrice",
                        TextAlignment = TextAlignment.Right,
                    },
                    new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_OriginalPriceAmount,
                        Name = "OriginalPriceSum",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_ConcessionPrice,
                        Name = "DiscountAmount",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_ConcessionRate,
                        Name = "DiscountAmountRate",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderOwning_Settlementprice,
                        Name = "Settlementprice",
                         TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderOwning_SettlementpriceAll,
                        Name = "SettlementpriceAll",
                         TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_DealerPartsInboundBill_CostPrice,
                        Name = "SalesPrice",
                         TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_SettlementPriceAmount,
                        Name = "SalesPriceAll",
                         TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_PartsOutboundBillTime,
                        Name = "ShippingDate",
                         TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_QuantityToSettle,
                       Name = "QuantityToSettle",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartsInboundCheckBillRemark,
                        Name = "Remark"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_Sparepart_IncreaseRateGroupId,
                        Name = "GroupName"
                    },new KeyValuesColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SettleBillStatus,
                        Name = "SettlementStatus",
                         KeyValueItems = this.KeyValueManager[kvNames[1]],
                    },new ColumnItem {
                        Name = "IsReplace",                       
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderOwning_IsReplace
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderOwning_ReplaceCode,
                        Name = "ReplaceCode",
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderOwning_IsReplenishment,
                        Name = "IsReplenishment"
                    }
                    ,new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_SAPSysInvoiceNumber,
                        Name = "SapCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_Invoice,
                        Name = "Invoice"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SettleBillCode,
                        Name = "SettlementCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_IfDirectProvision,
                        Name = "IfDirectProvision"
                    }
                };
            }
        }
        protected override Type EntityType
        {
            get
            {
                return typeof(PartsSalesOrderFinishReport);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName)
        {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "WarehouseId"  && filter.MemberName != "SettlementStatus"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();

        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "code":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "Code").Value;
                    case "referenceCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "ReferenceCode").Value;
                    case "warehouseId":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "WarehouseId").Value;
                    case "submitCompanyName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "SubmitCompanyName").Value;
                    case "province":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "Province").Value;                  
                    case "partsOutboundBillCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "PartsOutboundBillCode").Value;
                    case "sparePartName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "SparePartName").Value;             
                    case "marketingDepartmentName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "MarketingDepartmentName").Value;    
                    case"settlementStatus":
                        string settlementStatus = string.Empty;
                        foreach(var filter in filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var compositeFilterItem = filter as CompositeFilterItem;
                            {
                                if(compositeFilterItem.Filters.Any(r => r.MemberName == "SettlementStatus"))
                                    settlementStatus = string.Join(",", compositeFilterItem.Filters.Where(r => r.MemberName == "SettlementStatus").Select(r => r.Value));
                            }
                        }
                        return settlementStatus;
                     //   return filters.Filters.SingleOrDefault(item => item.MemberName == "SettlementStatus").Value;
                    case "partsSalesOrderTypeId":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "PartsSalesOrderTypeId").Value;  
                    case "bShippingDate":
                        //var bShippingDate = composites.FirstOrDefault(p => p.Filters.First().MemberName == "ShippingDate");
                        //return bShippingDate == null ? null : bShippingDate.Filters.First(item => item.MemberName == "ShippingDate").Value;
                          foreach(var filter in filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var compositeFilterItem = filter as CompositeFilterItem;
                            {
                                if(compositeFilterItem.Filters.Any(r => r.MemberName == "ShippingDate")) {
                                    return compositeFilterItem.Filters.Where(r => r.MemberName == "ShippingDate").First(r => r.MemberName == "ShippingDate").Value;
                                }
                            }
                        }
                        return null;
                    case "eShippingDate":
                        //var eShippingDate = composites.FirstOrDefault(p => p.Filters.First().MemberName == "ShippingDate");
                        //return eShippingDate == null ? null : eShippingDate.Filters.Last(item => item.MemberName == "ShippingDate").Value;
                          foreach(var filter in filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var compositeFilterItem = filter as CompositeFilterItem;
                            {
                                if(compositeFilterItem.Filters.Any(r => r.MemberName == "ShippingDate")) {
                                    return compositeFilterItem.Filters.Where(r => r.MemberName == "ShippingDate").Last(r => r.MemberName == "ShippingDate").Value;
                                }
                            }
                        }
                        return null;
                    case "groupNameId":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "GroupNameId").Value;
                    case "sparePartCode":
                        var codes = filters.Filters.SingleOrDefault(s => s.MemberName == "SparePartCode");
                        if(codes == null || codes.Value == null)
                            return null;
                        return codes.Value.ToString(); 

                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName()
        {
            return "GetCentralPartsSalesOutBoundList";
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
            ((GridViewDataColumn)this.GridView.Columns["CostPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["CostPriceAll"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OriginalPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OriginalPriceSum"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["DiscountAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["Settlementprice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SettlementpriceAll"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SalesPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SalesPriceAll"]).DataFormatString = "c2";
        }
    }
}