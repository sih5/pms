﻿
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Telerik.Windows.Controls;
namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid
{
    public class PartsShelvesTaskFinishReportDataGridView : DcsDataGridViewBase
    {

        private readonly string[] kvNames = new[] {
            "ABCStrategy_Category"
        };
        public PartsShelvesTaskFinishReportDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                       Name = "PartsPurchaseOrderCode",
                       Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_PartsPurchaseOrderCode
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_PartsInboundCheckBillCode,
                        Name = "PartsInboundCheckBillCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_WarehouseName,
                        Name = "WarehouseName" 
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_SparePartCode,
                        Name = "SparePartCode"
                    },new ColumnItem {
                         Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsSupplierRelation_SupplierPartCode,
                        Name = "SupplierPartCode"
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                        Name = "ReferenceCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                        Name = "SparePartName"
                    }, new KeyValuesColumnItem {
                        Name = "PartABC",
                        KeyValueItems = this.KeyValueManager[kvNames[0]],
                         Title=CommonUIStrings.QueryPanel_QueryItem_Title_Sparepart_PartABC,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_InspectedQuantity,
                        Name = "InspectedQuantity"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_PackingQty,
                        Name = "PackingQty"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsShelvesTaskFinish_ShelvesAmount,
                        Name = "ShelvesAmount"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_ModifyTime,
                        Name = "PackModifyTime"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsShelvesTaskFinish_CreateTime,
                        Name = "CreateTime"
                    },new ColumnItem {
                         Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsShelvesTaskFinish_ShelvesFinishTime,
                        Name = "ShelvesFinishTime"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_RetailGuidePrice,
                        Name = "RetailGuidePrice",
                        TextAlignment = TextAlignment.Right,
                    }
                };
            }
        }
        protected override Type EntityType
        {
            get
            {
                return typeof(PartsShelvesTaskFinish);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "partsPurchaseOrderCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "PartsPurchaseOrderCode").Value;
                    case "referenceCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "ReferenceCode").Value;
                    case "warehouseId":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "WarehouseId").Value;                
                    case "sparePartCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "SparePartCode").Value;
                    case "partABC":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "PartABC").Value;
                    case "partsInboundCheckBillCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "PartsInboundCheckBillCode").Value;
                    case "bShelvesFinishTime":
                        var bCreateTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "ShelvesFinishTime");
                        return bCreateTime == null ? null : bCreateTime.Filters.First(r => r.MemberName == "ShelvesFinishTime").Value;
                    case "eShelvesFinishTime":
                        var eCreateTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "ShelvesFinishTime");
                        return eCreateTime == null ? null : eCreateTime.Filters.Last(item => item.MemberName == "ShelvesFinishTime").Value;
                    case "bPackModifyTime":
                        var bModifyTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "PackModifyTime");
                        return bModifyTime == null ? null : bModifyTime.Filters.First(r => r.MemberName == "PackModifyTime").Value;
                    case "ePackModifyTime":
                        var eModifyTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "PackModifyTime");
                        return eModifyTime == null ? null : eModifyTime.Filters.Last(item => item.MemberName == "PackModifyTime").Value;
                   

                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName()
        {
            return "GetPartsShelvesTaskFinishs";
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
            ((GridViewDataColumn)this.GridView.Columns["RetailGuidePrice"]).DataFormatString = "c2";
        }
    }
}