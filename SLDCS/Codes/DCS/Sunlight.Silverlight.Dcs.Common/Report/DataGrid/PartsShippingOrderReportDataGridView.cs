﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class PartsShippingOrderReportDataGridView : DcsDataGridViewBase
    {
        private readonly string[] kvNames = new[] {
            "PartsShipping_Method"
        };
        public PartsShippingOrderReportDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                       Name = "Code",
                       Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_ShippingCode
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrderForReport_ShippingDateNew,
                        Name = "ShippingDate"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrderForReport_WarehouseName,
                        Name = "WarehouseName" 
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_PartsSalesOrderTypeName,
                        Name = "PartsSalesOrderTypeName"
                    },new KeyValuesColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrderForReport_ShippingMethod,
                         KeyValueItems = this.KeyValueManager[kvNames[0]],
                        Name = "ShippingMethod"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrderForReport_SettlementPriceAll,
                        Name = "SettlementPriceAll",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_ReceivingCompanyName,
                        Name = "ReceivingCompanyName"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrderForReport_ReceivingAddress,
                        Name = "ReceivingAddress"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrderForReport_LinkName,
                        Name = "LinkName"
                    },new ColumnItem{
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrderForReport_LogisticCompanyName,
                        Name="LogisticCompanyName"
                    },new ColumnItem{
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrderForReport_DeliveryBillNumber,
                        Name="DeliveryBillNumber"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_Weight,
                        Name = "Weight"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_Volume,
                        Name = "Volume"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrderForReport_OldWeight,
                        Name = "OldWeight"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrderForReport_OldVolume,
                        Name = "OldVolume"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrderForReport_AppraiserName,
                        Name = "AppraiserName"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_PartsSalesOrderCode,
                        Name = "PartsSalesOrderCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartsInboundCheckBillRemark,
                        Name = "Remark"
                    }
                };
            }
        }
        protected override Type EntityType
        {
            get
            {
                return typeof(PartsShippingOrderForReport);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch(parameterName) {
                    case "code":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "Code").Value;
                    case "partsSalesOrderCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "PartsSalesOrderCode").Value;
                    case "appraiserName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "AppraiserName").Value;
                    case "eShippingDate":
                        var eShippingDate = composites.FirstOrDefault(p => p.Filters.First().MemberName == "ShippingDate");
                        return eShippingDate == null ? null : eShippingDate.Filters.Last(item => item.MemberName == "ShippingDate").Value;
                    case "bShippingDate":
                        var bShippingDate = composites.FirstOrDefault(p => p.Filters.First().MemberName == "ShippingDate");
                        return bShippingDate == null ? null : bShippingDate.Filters.First(r => r.MemberName == "ShippingDate").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName()
        {
            return "发运单统计查询";
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
            ((GridViewDataColumn)this.GridView.Columns["SettlementPriceAll"]).DataFormatString = "c2";          
        }
    }
}