﻿
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid
{
    public class PartsStockStatementReportDataGridView : DcsDataGridViewBase
    {
        public PartsStockStatementReportDataGridView()
        {
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                       Name = "WarehouseCode",
                       Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_WarehouseCode
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_WarehouseId,
                        Name = "WarehouseName"
                    },new ColumnItem {
                      Title=  CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_SparePartCode,
                        Name = "SparePartCode" 
                    },new ColumnItem {
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                        Name = "SparePartName"
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                        Name = "ReferenceCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsStockStatement_BeginQuantity,
                        Name = "BeginQuantity"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsStockStatement_OutQuantity,
                        Name = "OutQuantity"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsStockStatement_InQuantity,
                        Name = "InQuantity"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsStockStatement_InventoryQuantity,
                        Name = "InventoryQuantity"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsStockStatement_EndQuantity,
                        Name = "EndQuantity"
                    }
                };
            }
        }
        protected override Type EntityType
        {
            get
            {
                return typeof(PartsStockStatement);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if (compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                foreach (var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "BeginTime" && filter.MemberName != "EndTime"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                switch (parameterName)
                {
                    
                    case "warehouseId":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "WarehouseId").Value;                
                    case "sparePartCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "SparePartCode").Value;
                    case "sparePartName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "SparePartName").Value;
                    case "referenceCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "ReferenceCode").Value;
                    case "beginTime":
                       return filters.Filters.SingleOrDefault(item => item.MemberName == "BeginTime").Value;
                    case "endTime":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "EndTime").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override string OnRequestQueryName()
        {
            return "查询库存对账报表";
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
        }
    }
}