﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;
using System.Collections.ObjectModel;


namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class PriorityPackingShelvesDetailForReportDataGridView : DcsDataGridViewBase
    {
        private ObservableCollection<KeyValuePair> kvFinishi = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = new[] {
            "PartPriority"
        };
        public PriorityPackingShelvesDetailForReportDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
            this.kvFinishi.Add(new KeyValuePair {
                Key = 1,
                Value = CommonUIStrings.Report_QueryPanel_KeyValue_Finishi
            });
            this.kvFinishi.Add(new KeyValuePair {
                Key = 2,
                Value = CommonUIStrings.Report_QueryPanel_KeyValue_PartFinishi
            });
            this.kvFinishi.Add(new KeyValuePair {
                Key = 3,
                Value = CommonUIStrings.Report_QueryPanel_KeyValue_UnFinishi
            });
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Priority",
                        KeyValueItems = this.KeyValueManager[kvNames[0]],
                         Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPriorityBill_Priority,
                     }, new ColumnItem {
                       Name = "PartsPurchaseOrderCode",
                       Title= CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_PartsPurchaseOrderCode,
                    },new ColumnItem{
                        Name="InboundCode",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_Code
                    }, new ColumnItem {
                     Title=   CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_SparePartCode,
                        Name = "SparePartCode"
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                        Name = "ReferenceCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                        Name = "SparePartName"
                    },new ColumnItem {
                       Title=CommonUIStrings.QueryPanel_QueryItem_Title_DealerPartsInboundBill_Quantity,
                        Name = "InspectedQuantity"
                    }, new ColumnItem {
                        Name = "PackingQty",
                         Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_PackingQty,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_PackingItme,
                        Name = "ShallPackingEntries"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_ActPackingItme,
                        Name = "ActualPackingEntries"
                    },new ColumnItem {
                       Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsShelvesTaskFinish_ShelvesAmount,
                        Name = "ShelvesAmount"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsShelvesTaskFinish_ShelvesItem,
                        Name = "ShallShelvesEntries"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsShelvesTaskFinish_ActShelvesItem,
                        Name = "ActualShelvesEntries"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_CheckBillCreateTime,
                        Name = "InBoundFinishTime"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_ModifyTime,
                        Name = "PackingFinishTime",
                        TextAlignment = TextAlignment.Right,
                    },new KeyValuesColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_PackingStatus,
                        Name = "PackingFinishStatus",
                        KeyValueItems=this.kvFinishi,
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsShelvesTaskFinish_ShelvesFinishTime,
                        Name = "ShelvesFinishTime",
                        TextAlignment = TextAlignment.Right,
                    },new KeyValuesColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsShelvesTaskFinish_ShelvesFinishStatus,
                        Name = "ShelvesFinishStatus",
                         KeyValueItems=this.kvFinishi,
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_PriorityCreateTime,
                        Name = "PriorityCreateTime",
                        TextAlignment = TextAlignment.Right,
                    }
                };
            }
        }
        protected override Type EntityType
        {
            get
            {
                return typeof(PriorityPackingShelvesReport);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                foreach(var item in compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).Where(filter => filter.MemberName != "CreateTime" ))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "packingFinishStatus":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "PackingFinishStatus").Value;
                    case "shelvesFinishStatus":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "ShelvesFinishStatus").Value;
                    case "priority":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "Priority").Value;                   
                    case "createTime":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "CreateTime").Value;
                    case "bPriorityPackingTime":
                        var bPriorityPackingTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "PackingFinishTime");
                        return bPriorityPackingTime == null ? null : bPriorityPackingTime.Filters.First(r => r.MemberName == "PackingFinishTime").Value;
                    case "ePriorityPackingTime":
                        var ePriorityPackingTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "PackingFinishTime");
                        return ePriorityPackingTime == null ? null : ePriorityPackingTime.Filters.Last(item => item.MemberName == "PackingFinishTime").Value;
                    case "bPriorityShelvesTime":
                        var bPriorityShelvesTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "ShelvesFinishTime");
                        return bPriorityShelvesTime == null ? null : bPriorityShelvesTime.Filters.First(r => r.MemberName == "ShelvesFinishTime").Value;
                    case "ePriorityShelvesTime":
                        var ePriorityShelvesTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "ShelvesFinishTime");
                        return ePriorityShelvesTime == null ? null : ePriorityShelvesTime.Filters.Last(item => item.MemberName == "ShelvesFinishTime").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName()
        {
            return "获取优先包装上架明细表";
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
        }
    }
}