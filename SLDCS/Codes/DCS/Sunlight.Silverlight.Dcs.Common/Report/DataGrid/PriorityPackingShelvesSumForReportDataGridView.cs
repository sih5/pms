﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;
using System.Collections.ObjectModel;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class PriorityPackingShelvesSumForReportDataGridView : DcsDataGridViewBase {
        private ObservableCollection<KeyValuePair> kvFinishi = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = new[] {
            "PartPriority"
        };
        public PriorityPackingShelvesSumForReportDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new ColumnItem{
                        Name="Month",
                        Title=CommonUIStrings.DataGridView_Title_CentralPartsSalesOrder_Month
                    }, new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_Day,
                        Name = "Dates"
                    }, new KeyValuesColumnItem {
                        Name = "Priority",
                        KeyValueItems = this.KeyValueManager[kvNames[0]],
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPriorityBill_Priority,
                     }, new ColumnItem {
                        Name = "StorageNumber",
                        Title= CommonUIStrings.DataGridView_StorageNumber,
                    },new ColumnItem{
                        Name="ActStorageNumber",
                        Title=CommonUIStrings.DataGridView_ActStorageNumber
                    }, new ColumnItem {
                        Title=   CommonUIStrings.DataGridView_StorageNumberItem,
                        Name = "StorageNumberItem"
                    },new ColumnItem {
                        Title =CommonUIStrings.DataGridView_ActStorageNumberItem,
                        Name = "ActStorageNumberItem"
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_CompletionRate,
                        Name = "CompletionRate"
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_CompletionEntryRate,
                        Name = "CompletionEntryRate"
                    }, new ColumnItem {
                        Name = "PackingNumber",
                         Title=CommonUIStrings.DataGridView_PackingNumber,
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_PackedNumber,
                        Name = "PackedNumber"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_PackingItme,
                        Name = "PackingNumberItem"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_ActPackingItme,
                        Name = "PackedNumberItem"
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_PackRate,
                        Name = "PackRate"
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_PackItemRate,
                        Name = "PackItemRate"
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_ShelveNumber,
                        Name = "ShelveNumber",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_ShelvedNumber,
                        Name = "ShelvedNumber",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsShelvesTaskFinish_ShelvesItem,
                        Name = "ShelveNumberItem"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsShelvesTaskFinish_ActShelvesItem,
                        Name = "ShelvedNumberItem",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_ShelveRate,
                        Name = "ShelveRate",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_ShelveItemRate,
                        Name = "ShelveItemRate",
                        TextAlignment = TextAlignment.Right,
                    }
                };
            }
        }
        protected override Type EntityType {
            get {
                return typeof(PriorityPackingShelvesReport);
            }
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                foreach(var item in compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).Where(filter => filter.MemberName != "CreateTime"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch(parameterName) {
                    case "type":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "Type").Value;
                    case "priority":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "Priority").Value;                
                    case "bCreateTime":
                        var bCreateTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return bCreateTime == null ? null : bCreateTime.Filters.First(r => r.MemberName == "CreateTime").Value;
                    case "eCreateTime":
                        var eCreateTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return eCreateTime == null ? null : eCreateTime.Filters.Last(item => item.MemberName == "CreateTime").Value;                  
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName() {
            return "获取优先包装上架明细汇总表";
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
        }
    }
}