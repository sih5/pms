﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class PurchaseCheckInboundReportDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "PartsPurchaseSettle_Status","ABCStrategy_Category"
        };
        public PurchaseCheckInboundReportDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                       Name = "Code",
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_Code,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierName,
                        Name = "PartsSupplierName"
                    } ,new ColumnItem {
                       Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartsSupplierCode,
                        Name = "PartsSupplierCode"
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_SparepartCode,
                        Name = "SparePartCode"
                    },new ColumnItem {
                         Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsSupplierRelation_SupplierPartCode,
                        Name = "SupplierPartCode" 
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                        Name = "ReferenceCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SparePartName,
                        Name = "SparePartName"
                    },new KeyValuesColumnItem {
                         Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartABC,
                         KeyValueItems = this.KeyValueManager[kvNames[1]],
                        Name = "PartABC"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SettlementPrices,
                        Name = "SettlementPrice",
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_InspectedQuantity,
                        Name = "InspectedQuantity",
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SettlementPriceSums,
                        Name = "SettlementPriceSum",
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_CostPrice,
                        Name = "CostPrice",
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_CostPriceAll,
                        Name = "CostPriceAll",
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_CreateTimeNew,
                        Name = "CreateTime"
                    },new KeyValuesColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SettleBillStatusNew,
                        Name = "SettleBillStatus",
                         KeyValueItems = this.KeyValueManager[kvNames[0]],
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SettleBillCode,
                        Name = "SettleBillCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_SAPSysInvoiceNumber,
                        Name = "SapCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_Invoice,
                        Name = "Invoice"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartsInboundCheckBillRemark,
                        Name = "PartsInboundCheckBillRemark"
                    }
                };
            }
        }
        protected override Type EntityType {
            get {
                return typeof(PartsInboundCheckBillForReport);
            }
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                foreach(var item in compositeFilterItem.Filters.Where(filter =>  filter.MemberName != "SettleBillStatus"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();

        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch(parameterName) {
                    case "code":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "Code").Value;
                    case "partsSupplierCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "PartsSupplierCode").Value;
                    case "partsSupplierName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "PartsSupplierName").Value;
                    case "sparePartCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "SparePartCode").Value;
                    case "sparePartName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "SparePartName").Value;
                    case "supplierPartCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "SupplierPartCode").Value;
                    case "settleBillStatus":
                       // return filters.Filters.SingleOrDefault(item => item.MemberName == "SettleBillStatus").Value;
                        string settlementStatus = string.Empty;
                        foreach(var filter in filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var compositeFilterItem = filter as CompositeFilterItem;
                            {
                                if(compositeFilterItem.Filters.Any(r => r.MemberName == "SettleBillStatus"))
                                    settlementStatus = string.Join(",", compositeFilterItem.Filters.Where(r => r.MemberName == "SettleBillStatus").Select(r => r.Value));
                            }
                        }
                        return settlementStatus;
                    case "partABC":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "PartABC").Value;
                    case "invoice":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "Invoice").Value;
                    case "referenceCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "ReferenceCode").Value;
                    case "bCreateTime":
                        //var bCreateTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        //return bCreateTime == null ? null : bCreateTime.Filters.First(item => item.MemberName == "CreateTime").Value;
                        foreach(var filter in filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var compositeFilterItem = filter as CompositeFilterItem;
                            {
                                if(compositeFilterItem.Filters.Any(r => r.MemberName == "CreateTime")) {
                                    return compositeFilterItem.Filters.Where(r => r.MemberName == "CreateTime").First(r => r.MemberName == "CreateTime").Value;
                                }
                            }
                        }
                        return null;
                    case "eCreateTime":
                        //var eCreateTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        //return eCreateTime == null ? null : eCreateTime.Filters.Last(item => item.MemberName == "CreateTime").Value;      
                         foreach(var filter in filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var compositeFilterItem = filter as CompositeFilterItem;
                            {
                                if(compositeFilterItem.Filters.Any(r => r.MemberName == "CreateTime")) {
                                    return compositeFilterItem.Filters.Where(r => r.MemberName == "CreateTime").Last(r => r.MemberName == "CreateTime").Value;
                                }
                            }
                        }
                        return null;

                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName() {
            return "配件采购入库检验单统计";
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
            ((GridViewDataColumn)this.GridView.Columns["SettlementPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SettlementPriceSum"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["CostPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["CostPriceAll"]).DataFormatString = "c2";
        }
    }
}