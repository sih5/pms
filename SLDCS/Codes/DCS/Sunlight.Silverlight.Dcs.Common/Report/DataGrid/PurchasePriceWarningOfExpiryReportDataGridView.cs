﻿
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class PurchasePriceWarningOfExpiryReportDataGridView : DcsDataGridViewBase {

        private readonly string[] kvNames = new[] {
            "PurchasePriceType"
        };
        public PurchasePriceWarningOfExpiryReportDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                       Name = "SparepartCode",
                       Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartCode
                    },new ColumnItem {
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                        Name = "SparepartName"
                    },new ColumnItem {
                        Title=CommonUIStrings.DetailPanel_Title_PartsInsteadInfo_SupplierCode,
                        Name = "PartsSupplierCode" 
                    },new ColumnItem {
                        Title=CommonUIStrings.DetailPanel_Title_PartsInsteadInfo_SupplierName,
                        Name = "PartsSupplierName"
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_PurchasePriceWarningOfExpiry_IsPrimary,
                        Name = "IsPrimary"
                    }, new KeyValuesColumnItem {
                        Name = "PriceType",
                        KeyValueItems = this.KeyValueManager[kvNames[0]],
                        Title = CommonUIStrings.DataGridView_PurchasePriceWarningOfExpiry_PriceType
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_PurchasePrice,
                        Name = "PurchasePrice"
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_PurchasePriceWarningOfExpiry_ValidFrom,
                        Name = "ValidFrom"
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_PurchasePriceWarningOfExpiry_ValidTo,
                        Name = "ValidTo"
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_PurchasePriceWarningOfExpiry_DaysRemainingDay,
                        Name = "DaysRemaining"
                    }
                };
            }
        }
        protected override Type EntityType
        {
            get
            {
                return typeof(PurchasePriceWarningOfExpiry);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "daysRemaining":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "DaysRemaining").Value;
                    case "sparepartCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "SparepartCode").Value;
                    case "partsSupplierCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "PartsSupplierCode").Value;
                    case "supplierPartCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "SupplierPartCode").Value;
                    case "partsSupplierName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "PartsSupplierName").Value;
                    case "priceType":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "PriceType").Value;
                    
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName()
        {
            return "获取采购价格有效到期提醒";
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
            ((GridViewDataColumn)this.GridView.Columns["PurchasePrice"]).DataFormatString = "c2";
        }
    }
}