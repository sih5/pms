﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;
using System.Windows.Media;


namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class RolePermissionsDataGridView : DcsDataGridViewBase {
        protected override Type EntityType {
            get {
                return typeof(RolePersonnelReport);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "LV1",
                        Title = "LV1"
                    },new ColumnItem {
                        Name = "LV2",
                        Title = "LV2"
                    },new ColumnItem {
                        Name = "LV3",
                        Title = "LV3"
                    }, new ColumnItem {
                        Name = "ActionName",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_ActionName,
                    },new ColumnItem {
                        Name = "AccessoriesCenter",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_AccessoriesCenter
                    },new ColumnItem {
                        Name = "BussinessMananger",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_BussinessMananger
                    },new ColumnItem {
                        Name = "AssistantDirector",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_AssistantDirector
                    },new ColumnItem {
                        Name = "SalesMananger",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_SalesMananger
                    },new ColumnItem {
                        Name = "SalesFilmMananger",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_SalesFilmMananger
                    },new ColumnItem {
                        Name = "OutSalesMananger",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_OutSalesMananger
                    },new ColumnItem {
                        Name = "ExtraMananger",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_ExtraMananger
                    },new ColumnItem {
                        Name = "ServiceMananger",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_ServiceMananger
                    },new ColumnItem {
                        Name = "BigMananger",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_BigMananger
                    },new ColumnItem {
                        Name = "PurchaseMananger",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_PurchaseMananger
                    },new ColumnItem {
                        Name = "PlanMananger",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_PlanMananger
                    },new ColumnItem {
                        Name = "AllocationMananger",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_AllocationMananger
                    },new ColumnItem {
                        Name = "Buyer",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_Buyer
                    },new ColumnItem {
                        Name = "MarketMananger",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_MarketMananger
                    },new ColumnItem {
                        Name = "PriceMananger",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_PriceMananger
                    },new ColumnItem {
                        Name = "CommercialMananger",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_CommercialMananger
                    },new ColumnItem {
                        Name = "NetMananger",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_NetMananger
                    },new ColumnItem {
                        Name = "MarketCommissioner",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_MarketCommissioner
                    },new ColumnItem {
                        Name = "IntegratedHead",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_IntegratedHead
                    },new ColumnItem {
                        Name = "SystemAdministrator",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_SystemAdministrator
                    },new ColumnItem {
                        Name = "AssistantSystem",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_AssistantSystemr
                    },new ColumnItem {
                        Name = "PurchaseCommissioner",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_PurchaseCommissioner
                    },new ColumnItem {
                        Name = "GeneralCommissioner",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_GeneralCommissioner
                    },new ColumnItem {
                        Name = "AccessoryTechnician",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_AccessoryTechnician
                    },new ColumnItem {
                        Name = "LogisticsManager",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_LogisticsManager
                    },new ColumnItem {
                        Name = "LogisticsFuManager",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_LogisticsFuManager
                    },new ColumnItem {
                        Name = "InBoundManager",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_InBoundManager
                    },new ColumnItem {
                        Name = "InBoundHead",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_InBoundHead
                    },new ColumnItem {
                        Name = "InBoundMember",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_InBoundMember
                    },new ColumnItem {
                        Name = "PackingManager",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_PackingManager
                    },new ColumnItem {
                        Name = "PackingHead",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_PackingHead
                    },new ColumnItem {
                        Name = "PackingMember",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_PackingMember
                    },new ColumnItem {
                        Name = "PackingEngineer",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_PackingEngineer
                    },new ColumnItem {
                        Name = "ReturnSales",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_ReturnSales
                    },new ColumnItem {
                        Name = "WarehouseManagement",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_WarehouseManagement
                    },new ColumnItem {
                        Name = "WarehouseHead",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_WarehouseHead
                    },new ColumnItem {
                        Name = "WarehouseMember",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_WarehouseMember
                    },new ColumnItem {
                        Name = "WarehouseManager",
                        Title = CommonUIStrings.String1QueryPanel_QueryItem_Title_RolePersonnelReport_WarehouseManager
                    },new ColumnItem {
                        Name = "SupervisorLogistics",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_SupervisorLogistics
                    },new ColumnItem {
                        Name = "LogistManager",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_LogistManager
                    },new ColumnItem {
                        Name = "InventoryHead",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_InventoryHead
                    },new ColumnItem {
                        Name = "InventoryMember",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_InventoryMember
                    },new ColumnItem {
                        Name = "LogisticsWarehouse",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_LogisticsWarehouse
                    },new ColumnItem {
                        Name = "Treasurer",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_Treasurer
                    },new ColumnItem {
                        Name = "TreasurerMember",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_TreasurerMember
                    },new ColumnItem {
                        Name = "ServiceManager",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_ServiceManager
                    }
                   
                };
            }
        }


        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch(parameterName) {
                    case "name":
                        return filters.Filters.Single(item => item.MemberName == "Name").Value;
                    case "actionName":
                        return filters.Filters.Single(item => item.MemberName == "ActionName").Value;
                    case "lV1":
                        return filters.Filters.Single(item => item.MemberName == "LV1").Value;
                    case "lV2":
                        return filters.Filters.Single(item => item.MemberName == "LV2").Value;
                    case "lV3":
                        return filters.Filters.Single(item => item.MemberName == "LV3").Value;


                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                foreach(var item in compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).Where(filter => filter.MemberName != "Name" ))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();

        }
        protected override string OnRequestQueryName() {
            return "获取角色权限清单";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 2000;
            this.GridView.FrozenColumnCount = 1;
            this.GridView.FrozenColumnCount = 2;
            this.GridView.FrozenColumnCount = 3;
            this.GridView.FrozenColumnCount = 4;
            var gridViewColumnGroupTp = new GridViewColumnGroup();
            gridViewColumnGroupTp.Name = "1";
            gridViewColumnGroupTp.Header = CommonUIStrings.QueryPanel__Title_RolePersonnelReport_gridViewColumnGroupTp;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            var gridViewColumnGroupRole = new GridViewColumnGroup();
            gridViewColumnGroupRole.Name = "2";
            gridViewColumnGroupRole.Header = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_RoleName;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            this.GridView.ColumnGroups.Add(gridViewColumnGroupTp);
            gridViewColumnGroupTp.ChildGroups.Add(gridViewColumnGroupRole);
            this.GridView.ColumnGroups.Add(gridViewColumnGroupRole);
            this.GridView.Columns["LV1"].ColumnGroupName = gridViewColumnGroupRole.Name;
            this.GridView.Columns["LV2"].ColumnGroupName = gridViewColumnGroupRole.Name;
            this.GridView.Columns["LV3"].ColumnGroupName = gridViewColumnGroupRole.Name;
            this.GridView.Columns["ActionName"].ColumnGroupName = gridViewColumnGroupRole.Name;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();


            var gridViewColumnGroupFailNumNull = new GridViewColumnGroup();
            gridViewColumnGroupFailNumNull.Name = "4";
            gridViewColumnGroupFailNumNull.Header = "";
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            var gridViewColumnGroupFailNumAcce = new GridViewColumnGroup();
            gridViewColumnGroupFailNumAcce.Name = "5";
            gridViewColumnGroupFailNumAcce.Header = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_AccessoriesCenter;
            gridViewColumnGroupFailNumNull.ChildGroups.Add(gridViewColumnGroupFailNumAcce);
            this.GridView.Columns["AccessoriesCenter"].ColumnGroupName = gridViewColumnGroupFailNumAcce.Name;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();


            var gridViewColumnGroupFailNumBus = new GridViewColumnGroup();
            gridViewColumnGroupFailNumBus.Name = "7";
            gridViewColumnGroupFailNumBus.Header = CommonUIStrings.QueryPanel__Title_RolePersonnelReport_gridViewColumnGroupFailNumBus;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            this.GridView.ColumnGroups.Add(gridViewColumnGroupFailNumBus);
            var gridViewColumnGroupFailNumSpm = new GridViewColumnGroup();
            gridViewColumnGroupFailNumSpm.Name = "70";
            gridViewColumnGroupFailNumSpm.Header = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_BussinessMananger;
            gridViewColumnGroupFailNumBus.ChildGroups.Add(gridViewColumnGroupFailNumSpm);
            this.GridView.Columns["BussinessMananger"].ColumnGroupName = gridViewColumnGroupFailNumSpm.Name;
            var gridViewColumnGroupFailNumDir = new GridViewColumnGroup();
            gridViewColumnGroupFailNumDir.Name = "71";
            gridViewColumnGroupFailNumDir.Header = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_AssistantDirector;
            gridViewColumnGroupFailNumBus.ChildGroups.Add(gridViewColumnGroupFailNumDir);
            this.GridView.Columns["AssistantDirector"].ColumnGroupName = gridViewColumnGroupFailNumDir.Name;
            var gridViewColumnGroupFailNumSale = new GridViewColumnGroup();
            gridViewColumnGroupFailNumSale.Name = "72";
            gridViewColumnGroupFailNumSale.Header = CommonUIStrings.QueryPanel__Title_RolePersonnelReport_gridViewColumnGroupFailNumSale;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            gridViewColumnGroupFailNumBus.ChildGroups.Add(gridViewColumnGroupFailNumSale);
            this.GridView.ColumnGroups.Add(gridViewColumnGroupFailNumSale);
            this.GridView.Columns["SalesMananger"].ColumnGroupName = gridViewColumnGroupFailNumSale.Name;
            this.GridView.Columns["SalesFilmMananger"].ColumnGroupName = gridViewColumnGroupFailNumSale.Name;
            this.GridView.Columns["OutSalesMananger"].ColumnGroupName = gridViewColumnGroupFailNumSale.Name;
            this.GridView.Columns["ExtraMananger"].ColumnGroupName = gridViewColumnGroupFailNumSale.Name;
            this.GridView.Columns["ServiceMananger"].ColumnGroupName = gridViewColumnGroupFailNumSale.Name;
            this.GridView.Columns["BigMananger"].ColumnGroupName = gridViewColumnGroupFailNumSale.Name;

            var gridViewColumnGroupFailNumPlan = new GridViewColumnGroup();
            gridViewColumnGroupFailNumPlan.Name = "73";
            gridViewColumnGroupFailNumPlan.Header = CommonUIStrings.QueryPanel__Title_RolePersonnelReport_gridViewColumnGroupFailNumPlan;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            gridViewColumnGroupFailNumBus.ChildGroups.Add(gridViewColumnGroupFailNumPlan);
            this.GridView.ColumnGroups.Add(gridViewColumnGroupFailNumPlan);
            this.GridView.Columns["PurchaseMananger"].ColumnGroupName = gridViewColumnGroupFailNumPlan.Name;
            this.GridView.Columns["PlanMananger"].ColumnGroupName = gridViewColumnGroupFailNumPlan.Name;
            this.GridView.Columns["AllocationMananger"].ColumnGroupName = gridViewColumnGroupFailNumPlan.Name;
            this.GridView.Columns["Buyer"].ColumnGroupName = gridViewColumnGroupFailNumPlan.Name;

            var gridViewColumnGroupFailNumMarket = new GridViewColumnGroup();
            gridViewColumnGroupFailNumMarket.Name = "74";
            gridViewColumnGroupFailNumMarket.Header = CommonUIStrings.QueryPanel__Title_RolePersonnelReport_gridViewColumnGroupFailNumMarket;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            gridViewColumnGroupFailNumBus.ChildGroups.Add(gridViewColumnGroupFailNumMarket);
            this.GridView.ColumnGroups.Add(gridViewColumnGroupFailNumMarket);
            this.GridView.Columns["MarketMananger"].ColumnGroupName = gridViewColumnGroupFailNumMarket.Name;
            this.GridView.Columns["PriceMananger"].ColumnGroupName = gridViewColumnGroupFailNumMarket.Name;
            this.GridView.Columns["CommercialMananger"].ColumnGroupName = gridViewColumnGroupFailNumMarket.Name;
            this.GridView.Columns["NetMananger"].ColumnGroupName = gridViewColumnGroupFailNumMarket.Name;
            this.GridView.Columns["MarketCommissioner"].ColumnGroupName = gridViewColumnGroupFailNumMarket.Name;

            var gridViewColumnGroupFailNumGeneral = new GridViewColumnGroup();
            gridViewColumnGroupFailNumGeneral.Name = "75";
            gridViewColumnGroupFailNumGeneral.Header = CommonUIStrings.QueryPanel__Title_RolePersonnelReport_gridViewColumnGroupFailNumGeneral;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            gridViewColumnGroupFailNumBus.ChildGroups.Add(gridViewColumnGroupFailNumGeneral);
            this.GridView.ColumnGroups.Add(gridViewColumnGroupFailNumGeneral);
            this.GridView.Columns["IntegratedHead"].ColumnGroupName = gridViewColumnGroupFailNumGeneral.Name;
            this.GridView.Columns["SystemAdministrator"].ColumnGroupName = gridViewColumnGroupFailNumGeneral.Name;
            this.GridView.Columns["AssistantSystem"].ColumnGroupName = gridViewColumnGroupFailNumGeneral.Name;
            this.GridView.Columns["PurchaseCommissioner"].ColumnGroupName = gridViewColumnGroupFailNumGeneral.Name;
            this.GridView.Columns["GeneralCommissioner"].ColumnGroupName = gridViewColumnGroupFailNumGeneral.Name;
            this.GridView.Columns["AccessoryTechnician"].ColumnGroupName = gridViewColumnGroupFailNumGeneral.Name;

            var gridViewColumnGroupFailNumLig = new GridViewColumnGroup();
            gridViewColumnGroupFailNumLig.Name = "8";
            gridViewColumnGroupFailNumLig.Header = CommonUIStrings.QueryPanel__Title_RolePersonnelReport_gridViewColumnGroupFailNumLig;
            this.GridView.ColumnGroups.Add(gridViewColumnGroupFailNumLig);
            var gridViewColumnGroupFailNumLigM = new GridViewColumnGroup();
            gridViewColumnGroupFailNumLigM.Name = "81";
            gridViewColumnGroupFailNumLigM.Header = CommonUIStrings.QueryPanel__Title_RolePersonnelReport_gridViewColumnGroupFailNumLigM;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            gridViewColumnGroupFailNumLig.ChildGroups.Add(gridViewColumnGroupFailNumLigM);
            this.GridView.ColumnGroups.Add(gridViewColumnGroupFailNumLigM);
            this.GridView.Columns["LogisticsManager"].ColumnGroupName = gridViewColumnGroupFailNumLigM.Name;
            this.GridView.Columns["LogisticsFuManager"].ColumnGroupName = gridViewColumnGroupFailNumLigM.Name;

            var gridViewColumnGroupFailNumInBound = new GridViewColumnGroup();
            gridViewColumnGroupFailNumInBound.Name = "82";
            gridViewColumnGroupFailNumInBound.Header = CommonUIStrings.QueryPanel__Title_RolePersonnelReport_gridViewColumnGroupFailNumInBound;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            gridViewColumnGroupFailNumLig.ChildGroups.Add(gridViewColumnGroupFailNumInBound);
            this.GridView.ColumnGroups.Add(gridViewColumnGroupFailNumInBound);
            this.GridView.Columns["InBoundManager"].ColumnGroupName = gridViewColumnGroupFailNumInBound.Name;
            this.GridView.Columns["InBoundHead"].ColumnGroupName = gridViewColumnGroupFailNumInBound.Name;
            this.GridView.Columns["InBoundMember"].ColumnGroupName = gridViewColumnGroupFailNumInBound.Name;

            var gridViewColumnGroupFailNumInPack = new GridViewColumnGroup();
            gridViewColumnGroupFailNumInPack.Name = "83";
            gridViewColumnGroupFailNumInPack.Header = CommonUIStrings.QueryPanel__Title_RolePersonnelReport_gridViewColumnGroupFailNumInPack;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            gridViewColumnGroupFailNumLig.ChildGroups.Add(gridViewColumnGroupFailNumInPack);
            this.GridView.ColumnGroups.Add(gridViewColumnGroupFailNumInPack);
            this.GridView.Columns["PackingManager"].ColumnGroupName = gridViewColumnGroupFailNumInPack.Name;
            this.GridView.Columns["PackingHead"].ColumnGroupName = gridViewColumnGroupFailNumInPack.Name;
            this.GridView.Columns["PackingMember"].ColumnGroupName = gridViewColumnGroupFailNumInPack.Name;
            this.GridView.Columns["PackingEngineer"].ColumnGroupName = gridViewColumnGroupFailNumInPack.Name;
            this.GridView.Columns["ReturnSales"].ColumnGroupName = gridViewColumnGroupFailNumInPack.Name;


            var gridViewColumnGroupFailNumInWare = new GridViewColumnGroup();
            gridViewColumnGroupFailNumInWare.Name = "84";
            gridViewColumnGroupFailNumInWare.Header = CommonUIStrings.QueryPanel__Title_RolePersonnelReport_gridViewColumnGroupFailNumInWare;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            gridViewColumnGroupFailNumLig.ChildGroups.Add(gridViewColumnGroupFailNumInWare);
            this.GridView.ColumnGroups.Add(gridViewColumnGroupFailNumInWare);
            this.GridView.Columns["WarehouseManagement"].ColumnGroupName = gridViewColumnGroupFailNumInWare.Name;
            this.GridView.Columns["WarehouseHead"].ColumnGroupName = gridViewColumnGroupFailNumInWare.Name;
            this.GridView.Columns["WarehouseMember"].ColumnGroupName = gridViewColumnGroupFailNumInWare.Name;

            var gridViewColumnGroupFailNumInCq = new GridViewColumnGroup();
            gridViewColumnGroupFailNumInCq.Name = "85";
            gridViewColumnGroupFailNumInCq.Header = CommonUIStrings.QueryPanel__Title_RolePersonnelReport_gridViewColumnGroupFailNumInCq;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            gridViewColumnGroupFailNumLig.ChildGroups.Add(gridViewColumnGroupFailNumInCq);
            this.GridView.Columns["WarehouseManager"].ColumnGroupName = gridViewColumnGroupFailNumInCq.Name;

            var gridViewColumnGroupFailNumInCqL = new GridViewColumnGroup();
            gridViewColumnGroupFailNumInCqL.Name = "86";
            gridViewColumnGroupFailNumInCqL.Header = CommonUIStrings.QueryPanel__Title_RolePersonnelReport_gridViewColumnGroupFailNumInCqL;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            gridViewColumnGroupFailNumLig.ChildGroups.Add(gridViewColumnGroupFailNumInCqL);
            this.GridView.ColumnGroups.Add(gridViewColumnGroupFailNumInCqL);
            this.GridView.Columns["SupervisorLogistics"].ColumnGroupName = gridViewColumnGroupFailNumInCqL.Name;
            this.GridView.Columns["LogistManager"].ColumnGroupName = gridViewColumnGroupFailNumInCqL.Name;
            this.GridView.Columns["InventoryHead"].ColumnGroupName = gridViewColumnGroupFailNumInCqL.Name;
            this.GridView.Columns["InventoryMember"].ColumnGroupName = gridViewColumnGroupFailNumInCqL.Name;
            this.GridView.Columns["LogisticsWarehouse"].ColumnGroupName = gridViewColumnGroupFailNumInCqL.Name;

            var gridViewColumnGroupFailNumTre = new GridViewColumnGroup();
            gridViewColumnGroupFailNumTre.Name = "9";
            gridViewColumnGroupFailNumTre.Header = CommonUIStrings.QueryPanel__Title_RolePersonnelReport_gridViewColumnGroupFailNumTre;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            this.GridView.ColumnGroups.Add(gridViewColumnGroupFailNumTre);
            var gridViewColumnGroupFailNumTs = new GridViewColumnGroup();
            gridViewColumnGroupFailNumTs.Name = "91";
            gridViewColumnGroupFailNumTs.Header = CommonUIStrings.QueryPanel__Title_RolePersonnelReport_gridViewColumnGroupFailNumTre;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            this.GridView.ColumnGroups.Add(gridViewColumnGroupFailNumTs);
            this.GridView.Columns["Treasurer"].ColumnGroupName = gridViewColumnGroupFailNumTs.Name;
            this.GridView.Columns["TreasurerMember"].ColumnGroupName = gridViewColumnGroupFailNumTs.Name;
            gridViewColumnGroupFailNumTre.ChildGroups.Add(gridViewColumnGroupFailNumTs);

            var gridViewColumnGroupFailNumCen = new GridViewColumnGroup();
            gridViewColumnGroupFailNumCen.Name = "6";
            gridViewColumnGroupFailNumCen.Header = CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_MarketingDepartmentName;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            this.GridView.ColumnGroups.Add(gridViewColumnGroupFailNumCen);
            var gridViewColumnGroupFailNummCen = new GridViewColumnGroup();
            gridViewColumnGroupFailNummCen.Name = "61";
            gridViewColumnGroupFailNummCen.Header = CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_MarketingDepartmentName;
            gridViewColumnGroupFailNumCen.ChildGroups.Add(gridViewColumnGroupFailNummCen);
            this.GridView.Columns["ServiceManager"].ColumnGroupName = gridViewColumnGroupFailNummCen.Name;
            SetColumnGroupsHeaderHorizontalAlignmentCenter();
            foreach(var column in this.GridView.Columns)
                if(!"ActionName".Equals(column.UniqueName) && !"LV1".Equals(column.UniqueName) && !"LV2".Equals(column.UniqueName)&& !"LV3".Equals(column.UniqueName))
                    column.CellStyleSelector = new BaleReportCreateDataGridViewCellStyleSelector(column.UniqueName);

        }

        protected void SetColumnGroupsHeaderHorizontalAlignmentCenter() {
            foreach(GridViewColumnGroup item in GridView.ColumnGroups) {
                var temp = (DataTemplate)System.Windows.Markup.XamlReader.Load("<DataTemplate xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\"> <TextBlock Text=\""
                    + item.Header + "\" HorizontalAlignment=\"Center\" VerticalAlignment=\"Center\"/> </DataTemplate>");
                item.HeaderTemplate = temp;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
        public class BaleReportCreateDataGridViewCellStyleSelector : StyleSelector {
            private readonly string gridViewColumnName;
            private Style redGridViewCellStyle;

            private Style RedGridViewCellStyle {
                get {
                    if(this.redGridViewCellStyle == null) {
                        this.redGridViewCellStyle = new Style {
                            TargetType = typeof(GridViewCell)
                        };
                        this.redGridViewCellStyle.Setters.Add(new Setter(GridViewCell.BackgroundProperty, new SolidColorBrush(Colors.Orange)));
                    }
                    return this.redGridViewCellStyle;
                }
            }


            public BaleReportCreateDataGridViewCellStyleSelector(string gridViewColumnName) {
                this.gridViewColumnName = gridViewColumnName;
            }

            public override Style SelectStyle(object item, DependencyObject container) {
                var rolePersonnelReport = item as RolePersonnelReport;
                if(rolePersonnelReport == null)
                    return null;
                if(this.gridViewColumnName == "AccessoriesCenter" && rolePersonnelReport.AccessoriesCenter != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "BussinessMananger" && rolePersonnelReport.BussinessMananger != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "SalesMananger" && rolePersonnelReport.SalesMananger != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "SalesFilmMananger" && rolePersonnelReport.SalesFilmMananger != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "OutSalesMananger" && rolePersonnelReport.OutSalesMananger != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "ExtraMananger" && rolePersonnelReport.ExtraMananger != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "ServiceMananger" && rolePersonnelReport.ServiceMananger != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "BigMananger" && rolePersonnelReport.BigMananger != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "PurchaseMananger" && rolePersonnelReport.PurchaseMananger != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "PlanMananger" && rolePersonnelReport.PlanMananger != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "AllocationMananger" && rolePersonnelReport.AllocationMananger != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "Buyer" && rolePersonnelReport.Buyer != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "MarketMananger" && rolePersonnelReport.MarketMananger != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "PriceMananger" && rolePersonnelReport.PriceMananger != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "CommercialMananger" && rolePersonnelReport.CommercialMananger != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "NetMananger" && rolePersonnelReport.NetMananger != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "MarketCommissioner" && rolePersonnelReport.MarketCommissioner != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "PurchaseCommissioner" && rolePersonnelReport.PurchaseCommissioner != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "GeneralCommissioner" && rolePersonnelReport.GeneralCommissioner != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "AccessoryTechnician" && rolePersonnelReport.AccessoryTechnician != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "LogisticsManager" && rolePersonnelReport.LogisticsManager != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "LogisticsFuManager" && rolePersonnelReport.LogisticsFuManager != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "InBoundManager" && rolePersonnelReport.InBoundManager != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "InBoundHead" && rolePersonnelReport.InBoundHead != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "InBoundMember" && rolePersonnelReport.InBoundMember != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "PackingManager" && rolePersonnelReport.PackingManager != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "PackingHead" && rolePersonnelReport.PackingHead != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "PackingMember" && rolePersonnelReport.PackingMember != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "PackingEngineer" && rolePersonnelReport.PackingEngineer != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "ReturnSales" && rolePersonnelReport.ReturnSales != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "WarehouseManagement" && rolePersonnelReport.WarehouseManagement != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "WarehouseHead" && rolePersonnelReport.WarehouseHead != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "WarehouseMember" && rolePersonnelReport.WarehouseMember != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "WarehouseManager" && rolePersonnelReport.WarehouseManager != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "SupervisorLogistics" && rolePersonnelReport.SupervisorLogistics != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "LogistManager" && rolePersonnelReport.LogistManager != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "InventoryHead" && rolePersonnelReport.InventoryHead != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "InventoryMember" && rolePersonnelReport.InventoryMember != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "LogisticsWarehouse" && rolePersonnelReport.LogisticsWarehouse != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "Treasurer" && rolePersonnelReport.Treasurer != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "TreasurerMember" && rolePersonnelReport.TreasurerMember != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "ServiceManager" && rolePersonnelReport.ServiceManager != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "AssistantDirector" && rolePersonnelReport.AssistantDirector != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "AssistantSystem" && rolePersonnelReport.AssistantSystem != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "SystemAdministrator" && rolePersonnelReport.SystemAdministrator != "") {
                    return RedGridViewCellStyle;
                } else if(this.gridViewColumnName == "IntegratedHead" && rolePersonnelReport.IntegratedHead != "") {
                    return RedGridViewCellStyle;
                } else return null;

            }
        }
    }
}