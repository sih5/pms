﻿
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class RolePersonnelForReportDataGridView : DcsDataGridViewBase
    {
        public RolePersonnelForReportDataGridView()
        {
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                       Name = "RoleName",
                       Title=CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_RoleNameNew
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_LoginIdNew,
                        Name = "LoginId"
                    },new ColumnItem {
                        Title=CommonUIStrings.DataEditPanel_Text_PersonnelSupplierRelation_PersonCode,
                        Name = "Name" 
                    }
                };
            }
        }
        protected override Type EntityType
        {
            get
            {
                return typeof(RolePersonnelReport);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }
      

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "name":
                        return filters.Filters.Single(item => item.MemberName == "Name").Value;
                    case "loginId":
                        return filters.Filters.Single(item => item.MemberName == "LoginId").Value;
                    case "roleName":
                        return filters.Filters.Single(item => item.MemberName == "RoleName").Value;
                   

                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName()
        {
            return "获取角色人员清单";
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;           
        }
    }
}