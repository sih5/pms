﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class SIHCenterSalesPerDataGridView  : DcsDataGridViewBase {
       
        public SIHCenterSalesPerDataGridView() {
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new ColumnItem {
                       Name = "CarryTime",
                       Title="结转时间",
                    },new ColumnItem {
                       Name = "WarehouseCode",
                       Title=CommonUIStrings.DataDeTailPanel_Text_Company_WarehouseCode,
                    },new ColumnItem {
                       Name = "WarehouseName",
                       Title =CommonUIStrings.DataDeTailPanel_Text_Company_WarehouseName,
                    },new ColumnItem {
                       Name = "SparePartCode",
                       Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartCode,
                    },new ColumnItem {
                       Name = "ReferenceCode",
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_StorehouseCensus_SparePartName,
                        Name = "SparePartName"
                    },new ColumnItem {
                        Title="提报单位编号",
                        Name = "SubmitCompanyCode"
                    },new ColumnItem {
                        Title="提报单位名称",
                        Name = "SubmitCompanyName"
                    },new ColumnItem {
                        Title="单中心库销售占比",
                        Name = "PerQty"
                    }
                };
            }
        }
        protected override Type EntityType {
            get {
                return typeof(SIHCenterPer);
            }
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
        protected override string OnRequestQueryName() {
            return "GetSIHCenterPers";
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 70;
            ((GridViewDataColumn)this.GridView.Columns["CarryTime"]).DataFormatString = "d";
        }
        //protected override object OnRequestQueryParameter(string queryName, string parameterName) {
        //    var filters = this.FilterItem as CompositeFilterItem;
        //    if(filters != null) {
        //        var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem) && r.MemberName != "SparePartCode").Cast<CompositeFilterItem>().ToArray();
        //        switch(parameterName) {
        //            case "warehouseId":
        //                return filters.Filters.SingleOrDefault(item => item.MemberName == "WarehouseId").Value;
        //            case "submitCompanyCode":
        //                return filters.Filters.SingleOrDefault(item => item.MemberName == "SubmitCompanyCode").Value;
        //            case "submitCompanyName":
        //                return filters.Filters.SingleOrDefault(item => item.MemberName == "SubmitCompanyName").Value;
        //            case "sparePartCode":
        //                var codes = filters.Filters.SingleOrDefault(s => s.MemberName == "SparePartCode");
        //                if(codes == null || codes.Value == null)
        //                    return null;
        //                return codes.Value.ToString();
        //            case "sparePartName":
        //                return filters.Filters.SingleOrDefault(item => item.MemberName == "SparePartName").Value;
        //            case "bCreateTime":
        //                var bAdjustTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
        //                return bAdjustTime == null ? null : bAdjustTime.Filters.First(r => r.MemberName == "CreateTime").Value;
        //            case "eCreateTime":
        //                var eAdjustTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
        //                return eAdjustTime == null ? null : eAdjustTime.Filters.Last(item => item.MemberName == "CreateTime").Value;
        //        }
        //    }
        //    return base.OnRequestQueryParameter(queryName, parameterName);
        //}
    }
}