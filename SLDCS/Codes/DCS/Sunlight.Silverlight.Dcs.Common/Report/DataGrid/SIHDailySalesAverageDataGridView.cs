﻿
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class SIHDailySalesAverageDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "ABCStrategy_Category"
        };
        public SIHDailySalesAverageDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new ColumnItem {
                       Name = "CarryTime",
                       Title="结转时间",
                    },new ColumnItem {
                       Name = "WarehouseCode",
                       Title=CommonUIStrings.DataDeTailPanel_Text_Company_WarehouseCode,
                    },new ColumnItem {
                       Name = "WarehouseName",
                       Title =CommonUIStrings.DataDeTailPanel_Text_Company_WarehouseName,
                    },new ColumnItem {
                       Name = "SparePartCode",
                       Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartCode,
                    },new ColumnItem {
                       Name = "ReferenceCode",
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_StorehouseCensus_SparePartName,
                        Name = "SparePartName"
                    }, new KeyValuesColumnItem {
                        Name = "PartABC",
                        KeyValueItems = this.KeyValueManager[kvNames[0]],
                         Title=CommonUIStrings.QueryPanel_QueryItem_Title_Sparepart_PartABC,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_SalesPrice,
                        Name = "SalesPrice",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="前第24个七天订货数量",
                        Name = "TwentyFourWeek",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="前第23个七天订货数量",
                         TextAlignment = TextAlignment.Right,
                        Name = "TwentyThreeWeek"
                    },new ColumnItem {
                        Title="前第22个七天订货数量",
                         TextAlignment = TextAlignment.Right,
                        Name = "TwentyTwoWeek"
                    },new ColumnItem {
                        Title="前第21个七天订货数量",
                        Name = "TwentyOneWeek",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="前第20个七天订货数量",
                         TextAlignment = TextAlignment.Right,
                        Name = "TwentyWeek"
                    },new ColumnItem {
                        Title="前第19个七天订货数量",
                        Name = "NineteenWeek",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="前第18个七天订货数量",
                         TextAlignment = TextAlignment.Right,
                        Name = "EightteenWeek"
                    },new ColumnItem {
                        Title="前第17个七天订货数量",
                        Name = "SeventeenWeek",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="前第16个七天订货数量",
                         TextAlignment = TextAlignment.Right,
                        Name = "SixteenWeek"
                    },new ColumnItem {
                        Title="前第15个七天订货数量",
                        Name = "FifteenWeek",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="前第14个七天订货数量",
                         TextAlignment = TextAlignment.Right,
                        Name = "FourteenWeek"
                    },new ColumnItem {
                        Title="前第13个七天订货数量",
                        TextAlignment = TextAlignment.Right,
                        Name = "ThirtWeek"
                    },new ColumnItem {
                        Title="前第12个七天订货数量",
                         TextAlignment = TextAlignment.Right,
                        Name = "TwelveWeek"
                    },new ColumnItem {
                        Title="前第11个七天订货数量",
                        Name = "ElevenWeek",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="前第10个七天订货数量",
                        Name = "TenWeek",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="前第9个七天订货数量",
                        Name = "NineWeek",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="前第8个七天订货数量",
                        Name = "EightWeek",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="前第7个七天订货数量",
                        Name = "SevenWeek",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="前第6个七天订货数量",
                        Name = "SixWeek",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="前第5个七天订货数量",
                        Name = "FiveWeek",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="前第4个七天订货数量",
                        Name = "FourWeek",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="前第3个七天订货数量",
                        Name = "ThreeWeek",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="前第2个七天订货数量",
                        Name = "TwoWeek",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="前第1个七天订货数量",
                        Name = "OneWeek",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="N天日均销量",
                        Name = "NDaysAvg",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="总频次",
                        Name = "OrderTimes",
                        TextAlignment = TextAlignment.Right,
                    }
                };
            }
        }
        protected override Type EntityType {
            get {
                return typeof(SIHDailySalesAverage);
            }
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
        protected override string OnRequestQueryName() {
            return "GetSIHDailySalesAverages";
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 70;
            ((GridViewDataColumn)this.GridView.Columns["SalesPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["CarryTime"]).DataFormatString = "d";
        }
    }
}