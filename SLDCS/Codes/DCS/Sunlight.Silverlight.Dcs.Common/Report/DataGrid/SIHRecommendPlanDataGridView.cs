﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class SIHRecommendPlanDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "ABCStrategy_Category"
        };
        public SIHRecommendPlanDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new ColumnItem {
                       Name = "CarryTime",
                       Title="结转时间",
                    },new ColumnItem {
                       Name = "WarehouseCode",
                       Title=CommonUIStrings.DataDeTailPanel_Text_Company_WarehouseCode,
                    },new ColumnItem {
                       Name = "WarehouseName",
                       Title =CommonUIStrings.DataDeTailPanel_Text_Company_WarehouseName,
                    },new ColumnItem {
                       Name = "SparePartCode",
                       Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartCode,
                    },new ColumnItem {
                       Name = "ReferenceCode",
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_StorehouseCensus_SparePartName,
                        Name = "SparePartName"
                    },new KeyValuesColumnItem {
                        Name = "PartABC",
                        KeyValueItems = this.KeyValueManager[kvNames[0]],
                         Title=CommonUIStrings.QueryPanel_QueryItem_Title_Sparepart_PartABC,
                    }, new ColumnItem {
                        Title="是否可销售",
                        Name = "IsSalable"
                    },new ColumnItem {
                        Title="是否可采购",
                        Name = "IsOrderable"
                    },new ColumnItem {
                        Title="是否直供",
                        Name = "IsDirectSupply"
                    },new ColumnItem {
                        Title="直供客户数量",
                        Name = "DirecCount",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_SalesPrice,
                        Name = "SalesPrice",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="首选供应商名称",
                        Name = "PartsSupplierName"
                    },new ColumnItem {
                        Title="前24周日最大订货数量",
                        Name = "MaxOrderedQuantity",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="储备系数",
                        Name = "ReserveCoefficient",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="上限系数",
                         TextAlignment = TextAlignment.Right,
                        Name = "UpperLimitCoefficient"
                    },new ColumnItem {
                        Title="下限系数",
                        Name = "LowerLimitcoefficient",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="计划周期",
                         TextAlignment = TextAlignment.Right,
                        Name = "OrderGoodsCycle"
                    },new ColumnItem {
                        Title="到货周期",
                         TextAlignment = TextAlignment.Right,
                        Name = "ArriveCycle"
                    },new ColumnItem {
                        Title="临时天数",
                         TextAlignment = TextAlignment.Right,
                        Name = "TemDays"
                    },new ColumnItem {
                        Title="N天日均销量",
                        Name = "NDaysAvg",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="总频次",
                        Name = "OrderTimes",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="安全天数",
                        Name = "SafeDay",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="标准库存",
                         TextAlignment = TextAlignment.Right,
                        Name = "StandStock"
                    },new ColumnItem {
                        Title="标准库存金额",
                         TextAlignment = TextAlignment.Right,
                        Name = "StandStockFee"
                    },new ColumnItem {
                        Title="库存上限",
                         TextAlignment = TextAlignment.Right,
                        Name = "UpperLimit"
                    },new ColumnItem {
                        Title="库存上限金额",
                         TextAlignment = TextAlignment.Right,
                        Name = "UpperLimitFee"
                    },new ColumnItem {
                        Title="库存下限",
                         TextAlignment = TextAlignment.Right,
                        Name = "LowerLimit"
                    },new ColumnItem {
                        Title="库存下限金额",
                         TextAlignment = TextAlignment.Right,
                        Name = "LowerLimitFee"
                    },new ColumnItem {
                        Title="可用库存",
                        Name = "AvailableStock",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="可用库存金额",
                        Name = "AvailableStockFee",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="采购在途",
                        Name = "OnWayNumber",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="采购在途金额",
                        Name = "OnWayNumberFee",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="欠货",
                        Name = "CurrentShortager",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="理论计划数量",
                        Name = "PlanQty",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="推荐计划数量",
                        Name = "RecommendQty",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="理论库存占比",
                        Name = "StockPer",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="日销量占比",
                        Name = "SalesPer",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="包装倍数",
                        Name = "Pack",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="实际计划数量",
                        Name = "ActQty",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title="实际计划金额",
                        Name = "ActQtyFee",
                        TextAlignment = TextAlignment.Right,
                    }
                };
            }
        }
        protected override Type EntityType {
            get {
                return typeof(SIHRecommendPlan);
            }
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
        protected override string OnRequestQueryName() {
            return "GetSIHRecommendPlans";
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 70;
            ((GridViewDataColumn)this.GridView.Columns["SalesPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["StandStockFee"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["UpperLimitFee"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["LowerLimitFee"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["AvailableStockFee"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OnWayNumberFee"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["ActQtyFee"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["CarryTime"]).DataFormatString = "d";
        }
    }
}