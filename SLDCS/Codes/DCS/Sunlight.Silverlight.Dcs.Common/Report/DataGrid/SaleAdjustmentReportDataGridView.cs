﻿
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid
{
    public class SaleAdjustmentReportDataGridView: DcsDataGridViewBase
    {
        private readonly string[] kvNames = new[] {
            "Company_Type"
        };
        public SaleAdjustmentReportDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "CustomerType",
                        KeyValueItems = this.KeyValueManager[kvNames[0]],
                        Title=CommonUIStrings.Report_Title_SaleAdjustment_CustomerType,
                    },
                    new ColumnItem {
                       Name = "AgencyName",
                       Title=CommonUIStrings.Report_Title_SaleAdjustment_AgencyName
                    },new ColumnItem {
                        Title=CommonUIStrings.Report_Title_SaleAdjustment_CustomerName,
                        Name = "CustomerName"
                    },new ColumnItem {
                        Title=CommonUIStrings.Report_Title_SaleAdjustment_CustomerCode,
                        Name = "CustomerCode" 
                    },new ColumnItem {
                     Title=   CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_SparePartCode,
                        Name = "SparePartCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                        Name = "SparePartName"
                    },new ColumnItem {
                        Title=CommonUIStrings.Report_Title_SaleAdjustment_OldPrice,
                        Name = "OldPrice"
                    },new ColumnItem {
                        Title=CommonUIStrings.Report_Title_SaleAdjustment_Price,
                        Name = "Price"
                    },new ColumnItem {
                       Title=CommonUIStrings.Report_Title_SaleAdjustment_Difference,
                        Name = "Difference"
                    },new ColumnItem {
                        Title=CommonUIStrings.Report_Title_SaleAdjustment_DifferenceRate,
                        Name = "DifferenceRate"
                    },new ColumnItem {
                        Title=CommonUIStrings.Report_Title_SaleAdjustment_PastThreeMonthsPruchase,
                        Name = "PastThreeMonthsPruchase"
                    },new ColumnItem {
                        Title=CommonUIStrings.Report_Title_SaleAdjustment_UsableQty,
                        Name = "UsableQty"
                    },new ColumnItem {
                        Title="常规库存",
                        Name = "NormalStock"
                    },new ColumnItem {
                        Title="保底库存",
                        Name = "StockQty"
                    },new ColumnItem {
                        Title="保底库存补差金额",
                        Name = "StockQtyFee"
                    },new ColumnItem {
                        Title="销售在途",
                        Name = "OnwaySales"
                    },new ColumnItem {
                        Title="欠货在途",
                        Name = "OutBoundAmount"
                    },new ColumnItem {
                        Title=CommonUIStrings.Report_Title_SaleAdjustment_AdjustQty,
                        Name = "AdjustQty"
                    },new ColumnItem {
                        Title=CommonUIStrings.Report_Title_SaleAdjustment_AdjustAmount,
                        Name = "AdjustAmount"
                    },new ColumnItem {
                        Title=CommonUIStrings.Report_Title_SaleAdjustment_AdjustTime,
                        Name = "AdjustTime"
                    }
                };
            }
        }

        protected override Type EntityType
        {
            get
            {
                return typeof(Sunlight.Silverlight.Dcs.Web.SaleAdjustmentReport);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem) && r.MemberName != "SparePartCode").Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "customerType":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "CustomerType").Value;
                    case "agencyName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "AgencyName").Value;
                    case "customerName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "CustomerName").Value;                
                    case "sparePartCodes":
                        var codes = filters.Filters.SingleOrDefault(s => s.MemberName == "SparePartCode");
                        if(codes == null || codes.Value == null)
                            return null;
                        return codes.Value.ToString();                  
                    case "bAdjustTime":
                        var bAdjustTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "AdjustTime");
                        return bAdjustTime == null ? null : bAdjustTime.Filters.First(r => r.MemberName == "AdjustTime").Value;
                    case "eAdjustTime":
                        var eAdjustTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "AdjustTime");
                        return eAdjustTime == null ? null : eAdjustTime.Filters.Last(item => item.MemberName == "AdjustTime").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override string OnRequestQueryName()
        {
            return "GetSaleAdjustmentReport";
        }

        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
        }
    }
}