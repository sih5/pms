﻿
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid
{
    public class SaleAdjustmentSumReportDataGridView: DcsDataGridViewBase
    {
        private readonly string[] kvNames = new[] {
            "Company_Type"
        };
        public SaleAdjustmentSumReportDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "CustomerType",
                        KeyValueItems = this.KeyValueManager[kvNames[0]],
                        Title=CommonUIStrings.Report_Title_SaleAdjustment_CustomerType,
                    },
                    new ColumnItem {
                       Name = "AgencyName",
                       Title=CommonUIStrings.Report_Title_SaleAdjustment_AgencyName
                    },new ColumnItem {
                        Title=CommonUIStrings.Report_Title_SaleAdjustment_CustomerName,
                        Name = "CustomerName"
                    },new ColumnItem {
                        Title=CommonUIStrings.Report_Title_SaleAdjustment_CustomerCode,
                        Name = "CustomerCode" 
                    },new ColumnItem {
                       Title=CommonUIStrings.Report_Title_SaleAdjustment_AdjustTotalAmount,
                        Name = "AdjustAmount"
                    },new ColumnItem {
                        Title=CommonUIStrings.Report_Title_SaleAdjustment_AdjustStartTime,
                        Name = "AdjustTime"
                    },new ColumnItem {
                        Title=CommonUIStrings.Report_Title_SaleAdjustment_BatchNumber,
                        Name = "BatchNumber"
                    }
                };
            }
        }

        protected override Type EntityType
        {
            get
            {
                return typeof(Sunlight.Silverlight.Dcs.Web.SaleAdjustmentReport);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem) && r.MemberName != "SparePartCode").Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "customerType":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "CustomerType").Value;
                    case "agencyName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "AgencyName").Value;
                    case "customerName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "CustomerName").Value;                
                    case "bAdjustTime":
                        var bAdjustTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "AdjustTime");
                        return bAdjustTime == null ? null : bAdjustTime.Filters.First(r => r.MemberName == "AdjustTime").Value;
                    case "eAdjustTime":
                        var eAdjustTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "AdjustTime");
                        return eAdjustTime == null ? null : eAdjustTime.Filters.Last(item => item.MemberName == "AdjustTime").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override string OnRequestQueryName()
        {
            return "GetSaleAdjustmentSumReport";
        }

        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
        }
    }
}