﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class SaleOrderTerminalDetailDataGridView : DcsDataGridViewBase
    {
      
        public SaleOrderTerminalDetailDataGridView()
        {
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                       Name = "MarketingDepartmentName",
                       Title=CommonUIStrings.DataEditPanel_Text_Company_Market
                    },new ColumnItem {
                        Title=CommonUIStrings.DataEditPanel_Text_Agency_Name,
                        Name = "CenterName"
                    },new ColumnItem {
                        Title=CommonUIStrings.Report_QueryPanel_Title_DealerNames,
                        Name = "DealerName" 
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_Codes,
                        Name = "Code"
                    },new ColumnItem {
                        Title=CommonUIStrings.Report_QueryPanel_Title_SubmitTime,
                        Name = "SubmitTime"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_PartsSalesOrderTypeName,
                        Name = "PartsSalesOrderTypeName",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_SparePartCode,
                        Name = "SparePartCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_OrderedQuantity,
                        Name = "OrderedQuantity"
                    }
                };
            }
        }
        protected override Type EntityType
        {
            get
            {
                return typeof(SaleOrderTerminalSatisfactionRate);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch(parameterName) {
                    case "centerName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "CenterName").Value;
                    case "marketingDepartmentName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "MarketingDepartmentName").Value;
                    case "dealerName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "DealerName").Value;
                    case "eSubmitTime":
                        var eSubmitTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "SubmitTime");
                        return eSubmitTime == null ? null : eSubmitTime.Filters.Last(item => item.MemberName == "SubmitTime").Value;
                    case "bSubmitTime":
                        var bSubmitTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "SubmitTime");
                        return bSubmitTime == null ? null : bSubmitTime.Filters.First(r => r.MemberName == "SubmitTime").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName()
        {
            return "终端满足率明细查询";
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
        }
    }
}