﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Telerik.Windows.Controls;


namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class SaleOrderTerminalRateSumForReportDataGridView : DcsDataGridViewBase
    {

        public SaleOrderTerminalRateSumForReportDataGridView()
        {
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_Title_QueryItem_Year,
                        Name = "Year"
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_Title_CentralPartsSalesOrder_Month,
                        Name = "Month"
                    },new ColumnItem {
                       Name = "MarketingDepartmentName",
                       Title=CommonUIStrings.DataEditPanel_Text_Company_Market
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_CenterName,
                        Name = "CenterName"
                    },new ColumnItem {
                        Title=CommonUIStrings.Report_QueryPanel_Title_DealerNames,
                        Name = "DealerName" 
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridCiew_Title_PartsSales_EmergencyOrderNum,
                        Name = "EmergencyOrderNum"
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridCiew_Title_PartsSales_ExEmergencyOrderNum,
                        Name = "ExEmergencyOrderNum"
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridCiew_Title_PartsSales_AB,
                        Name = "AbNum",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridCiew_Title_PartsSales_OrderAmount,
                        Name = "OrderNum"
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridCiew_Title_PartsSales_EmergencyRate,
                        Name = "EmergencyRate"
                    }
                    ,new ColumnItem {
                        Title=CommonUIStrings.DataGridCiew_Title_PartsSales_AbRate,
                        Name = "AbRate"
                    }
                    ,new ColumnItem {
                        Title=CommonUIStrings.DataGridCiew_Title_PartsSales_TerminalRate,
                        Name = "TerminalRate"
                    }
                };
            }
        }
        protected override Type EntityType
        {
            get
            {
                return typeof(SaleOrderTerminalSatisfactionRate);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                foreach(var item in compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).Where(filter => filter.MemberName != "Unit" || filter.MemberName != "Month" || filter.MemberName != "Year")) {
                }

            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch(parameterName) {
                    case "centerName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "CenterName").Value;
                    case "marketingDepartmentName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "MarketingDepartmentName").Value;
                    case "dealerName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "DealerName").Value;
                    case "year":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "Year").Value;                    
                    case "month":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "Month").Value;
                    case"unit":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "Unit").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName()
        {
            return "查询终端满足率汇总报表";
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
        }
    }
}