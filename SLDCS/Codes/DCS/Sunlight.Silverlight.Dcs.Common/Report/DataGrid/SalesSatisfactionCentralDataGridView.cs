﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;
using System.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class SalesSatisfactionCentralDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                       Name = "Name",
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_SubmitCompanyName,
                    }, new ColumnItem {
                        Name = "Type",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_CustomerTypeStr
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_Title_CentralPartsSalesOrder_UnSatisfiedAmount,
                       Name = "UnYcpzs"
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_Title_CentralPartsSalesOrder_SatisfiedAmount,
                       Name = "Ycpzs"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_AllItems,
                       Name = "Totalpz" 
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_Title_CentralPartsSalesOrder_OneTimeRate,
                       Name = "Tmmzl"
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_Title_CentralPartsSalesOrder_AppRoveAmount,
                       Name = "Ycfee"
                    },new ColumnItem {
                        Title=CommonUIStrings.DataGridView_Title_CentralPartsSalesOrder_TotalAmount,
                       Name = "Totalfee"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_SalesSatisfactionRate_Jemzl,
                       Name = "Jemzl"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SalesSatisfactionRate);
            }
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                foreach(var item in compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).Where(filter => filter.MemberName != "ApproveTime"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch(parameterName) {
                    case "name":
                        return filters.Filters.Single(item => item.MemberName == "Name").Value;
                    case "bApproveTime":
                        var createTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "ApproveTime");
                        return createTime == null ? null : createTime.Filters.First(r => r.MemberName == "ApproveTime").Value;
                    case "eApproveTime":
                        var createTime1 = composites.FirstOrDefault(p => p.Filters.First().MemberName == "ApproveTime");
                        return createTime1 == null ? null : createTime1.Filters.Last(item => item.MemberName == "ApproveTime").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName() {
            return "GetSalesSatisfactionCentralRateReport";
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
        }
    }
}
