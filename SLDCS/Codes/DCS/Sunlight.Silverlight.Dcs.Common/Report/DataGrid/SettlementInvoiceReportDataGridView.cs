﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class SettlementInvoiceReportDataGridView  : DcsDataGridViewBase
    {
         private readonly string[] kvNames = new[] {
            "ABCStrategy_Category","PartsBranch_PurchaseRoute","PartsSalesSettlement_Status","SalesOrderType_BusinessType"
        };
         public SettlementInvoiceReportDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                       Name = "MarketingDepartmentName",
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_MarketingDepartmentName,
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_WarehouseId,
                        Name = "WarehouseName"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_Province,
                        Name = "Province" 
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_PartsSalesOrderCode,
                        Name = "PartsSalesOrderCode" 
                    },new ColumnItem {
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_SubmitCompanyName,
                        Name = "SubmitCompanyName"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_PartsOutboundBillCode,
                        Name = "PartsOutboundBillCode"
                    },new ColumnItem {
                      Title=  CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_SparePartCode,
                        Name = "SparePartCode"
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                        Name = "ReferenceCode"
                    },new ColumnItem {
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                        Name = "SparePartName"
                    },new KeyValuesColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_SettlementInvoice_PurchaseRoute,
                         KeyValueItems = this.KeyValueManager[kvNames[1]],
                        Name = "PurchaseRoute"
                    },new KeyValuesColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_SettlementInvoice_PartABC,
                        Name = "PartABC",
                        KeyValueItems = this.KeyValueManager[kvNames[0]],
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_OutboundAmount,
                        Name = "OutboundAmount"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_SettlementInvoice_CostPrice,
                        Name = "CostPrice",
                        TextAlignment = TextAlignment.Right,
                    }
                   , new ColumnItem {
                        Name = "CostPriceAmount",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_InternalAllocationBillReport_CostPriceAll,
                        TextAlignment = TextAlignment.Right,
                    }
                    ,new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_SettlementInvoice_SettlementPrice,
                        Name = "SettlementPrice",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_SettlementInvoice_SettlementPriceAmount,
                        Name = "SettlementPriceAmount",
                        TextAlignment = TextAlignment.Right,
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_PartsOutboundBillTime,
                        Name = "PartsOutboundBillCreateTime"
                    },new KeyValuesColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SettleBillStatus,
                        Name = "Status",
                         KeyValueItems = this.KeyValueManager[kvNames[2]],
                    },
                    new KeyValuesColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_SettlementInvoice_BusinessType,
                        Name = "BusinessType",
                         KeyValueItems = this.KeyValueManager[kvNames[3]],
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartsInboundCheckBillRemark,
                        Name = "Remark"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SettleBillCode,
                        Name = "Code"
                    },new ColumnItem {
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_Invoice,
                        Name = "InvoiceNumber"
                    }
                };
            }
        }
        protected override Type EntityType
        {
            get
            {
                return typeof(SettlementInvoice);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName)
        {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if (compositeFilterItem != null)
            {
                foreach (var item in compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).Where(filter => filter.MemberName != "WarehouseId" || filter.MemberName != "PartsPurchaseOrderCode"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();

        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {                
                    case "submitCompanyName":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "SubmitCompanyName").Value;                            
                    case "invoiceNumber":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "InvoiceNumber").Value;
                    case "code":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "Code").Value;                
                    case "partsOutboundBillCode":
                        return filters.Filters.SingleOrDefault(item => item.MemberName == "PartsOutboundBillCode").Value;
                    case "bOutCreateTime":
                        var bShippingDate = composites.FirstOrDefault(p => p.Filters.First().MemberName == "PartsOutboundBillCreateTime");
                        return bShippingDate == null ? null : bShippingDate.Filters.First(item => item.MemberName == "PartsOutboundBillCreateTime").Value;
                    case "eOutCreateTime":
                        var eShippingDate = composites.FirstOrDefault(p => p.Filters.First().MemberName == "PartsOutboundBillCreateTime");
                        return eShippingDate == null ? null : eShippingDate.Filters.Last(item => item.MemberName == "PartsOutboundBillCreateTime").Value;
                    case "bCreateTime":
                        var bCreateTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return bCreateTime == null ? null : bCreateTime.Filters.First(item => item.MemberName == "CreateTime").Value;
                    case "eCreateTime":
                        var eCreateTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return eCreateTime == null ? null : eCreateTime.Filters.Last(item => item.MemberName == "CreateTime").Value;

                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName()
        {
            return "GetSettlementInvoices";
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
            ((GridViewDataColumn)this.GridView.Columns["CostPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["CostPriceAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SettlementPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SettlementPriceAmount"]).DataFormatString = "c2";
        }
    }
}