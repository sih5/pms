﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;
namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class TraceSIHCodeAgeReportDataGridView : DcsDataGridViewBase
    {
        private readonly string[] kvNames = new[] {
            "TraceProperty"
        };
        public TraceSIHCodeAgeReportDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                       Name = "SparePartCode",
                        Title = "配件编号",
                    },new ColumnItem {
                        Title="配件名称",
                        Name = "SparePartName"
                    },new ColumnItem {
                        Title ="仓库名称",
                        Name = "WarehouseName" 
                    },new ColumnItem {
                        Title="库区编号",
                        Name = "StorehouseAreaCode"
                    },new ColumnItem {
                        Title="库位编号",
                        Name = "WarehouseAreaCode"
                    },new KeyValuesColumnItem {
                        Title="追溯属性",
						KeyValueItems = this.KeyValueManager[kvNames[0]],
                        Name = "TraceProperty",
                    },new ColumnItem {
                        Title="数量",
                        Name = "InQty"
                    },new ColumnItem {
                        Title="经销价",
                        Name = "SalesPrice"
                    },new ColumnItem {
                        Title="SIH标签码",
                        Name = "SIHLabelCode"
                    },new ColumnItem {
                        Title="入库时间",
                        Name = "InBoundDate"
                    },new ColumnItem {
                        Title="库龄",
                        Name = "Age"
                    }
                };
            }
        }
        protected override Type EntityType
        {
            get
            {
                return typeof(TraceWarehouseAge);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return true;
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName)
        {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if (compositeFilterItem != null)
            {
                foreach(var item in compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).Where(filter => filter.MemberName != "KvAge" && filter.MemberName != "GreaterThanZero"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "warehouseName":
                        return filters.Filters.Single(item => item.MemberName == "WarehouseName").Value;
                    case "warehouseAreaCode":
                        return filters.Filters.Single(item => item.MemberName == "WarehouseAreaCode").Value;
                    case "storehouseAreaCode":
                        return filters.Filters.Single(item => item.MemberName == "StorehouseAreaCode").Value;
                    case "sparePartCode":
                        return filters.Filters.Single(item => item.MemberName == "SparePartCode").Value;
                    case "traceProperty":
                        return filters.Filters.Single(item => item.MemberName == "TraceProperty").Value;
                    case "sIHLabelCode":
                        return filters.Filters.Single(item => item.MemberName == "SIHLabelCode").Value;
                    case "greaterThanZero":
                        return filters.Filters.Single(item => item.MemberName == "GreaterThanZero").Value;
                    case "kvAge":
                        return filters.Filters.Single(item => item.MemberName == "KvAge").Value;
                    case "id":
                        return null;

                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName()
        {
            return "查询SIH标签码库位库龄";
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
            ((GridViewDataColumn)this.GridView.Columns["SalesPrice"]).DataFormatString = "c2";
        }
    }
}