﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class TraceWarehouseCenterDataGridView : DcsDataGridViewBase
    {
        private readonly string[] kvNames = new[] {
            "AccurateTraceStatus","Parts_InboundType"
        };
        public TraceWarehouseCenterDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                   new ColumnItem {
                        Name = "SparePartCode",
                        Title = "配件图号",
                    },new ColumnItem {
                        Title="配件名称",
                        Name = "SparePartName"
                    },new ColumnItem {
                        Title="追溯码",
                        Name = "TraceCode"
                    },new ColumnItem {
                        Title="SIH标签码",
                        Name = "SIHLabelCode"
                    },new ColumnItem {
                        Title ="仓库编号",
                        Name = "WarehouseCode" 
                    },new ColumnItem {
                        Title ="仓库名称",
                        Name = "WarehouseName" 
                    },new ColumnItem {
                        Title="原始单据编号",
                        Name = "OriginalRequirementBillCode"
                    },new ColumnItem {
                        Title="收货单号",
                        Name = "SourceCode"
                    },new ColumnItem {
                        Title="入库单号",
                        Name = "InBoundCode"
                    },new ColumnItem {
                        Title="入库时间",
                        Name = "InBoundDate"
                    },new KeyValuesColumnItem {
                        Title="状态",
						KeyValueItems = this.KeyValueManager[kvNames[0]],
                        Name = "Status",
                    },new ColumnItem {
                        Title="入库数量",
                        Name = "InQty"
                    },new ColumnItem {
                        Title="出库数量",
                        Name = "OutQty"
                    }
                };
            }
        }
        protected override Type EntityType
        {
            get
            {
                return typeof(TraceWarehouseAge);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }        

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "status":
                        return filters.Filters.Single(item => item.MemberName == "Status").Value;
                    case "inBoundCode":
                        return filters.Filters.Single(item => item.MemberName == "InBoundCode").Value;
                    case "warehouseName":
                        return filters.Filters.Single(item => item.MemberName == "WarehouseName").Value;
                    case "sparePartCode":
                        return filters.Filters.Single(item => item.MemberName == "SparePartCode").Value;
                    case "sIHLabelCode":
                        return filters.Filters.Single(item => item.MemberName == "SIHLabelCode").Value;
                    case "traceCode":
                        return filters.Filters.Single(item => item.MemberName == "TraceCode").Value;                   
                    case "sourceCode":
                        return filters.Filters.Single(item => item.MemberName == "SourceCode").Value;
                    case "originalRequirementBillCode":
                        return filters.Filters.Single(item => item.MemberName == "OriginalRequirementBillCode").Value;
                    case "bInBoundDate":
                        var bInBoundDate = composites.FirstOrDefault(p => p.Filters.First().MemberName == "InBoundDate");
                        return bInBoundDate == null ? null : bInBoundDate.Filters.First(item => item.MemberName == "InBoundDate").Value;
                    case "eInBoundDate":
                        var eInBoundDate = composites.FirstOrDefault(p => p.Filters.First().MemberName == "InBoundDate");
                        return eInBoundDate == null ? null : eInBoundDate.Filters.Last(item => item.MemberName == "InBoundDate").Value;

                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName()
        {
            return "中心库配件入库精确码物流跟踪";
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
        }
    }
}
