﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class TraceWarehouseOutSIHReportDataGridView  : DcsDataGridViewBase
    {
        private readonly string[] kvNames = new[] {
            "AccurateTraceStatus","Parts_OutboundType"
        };
        public TraceWarehouseOutSIHReportDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                        Title ="仓库编号",
                        Name = "WarehouseCode" 
                    },new ColumnItem {
                        Title ="仓库名称",
                        Name = "WarehouseName" 
                    },new ColumnItem {
                        Name = "SparePartCode",
                        Title = "配件编号",
                    },new ColumnItem {
                        Title="配件名称",
                        Name = "SparePartName"
                    },new ColumnItem {
                        Title="追溯码",
                        Name = "TraceCode"
                    },new ColumnItem {
                        Title="SIH标签码",
                        Name = "SIHLabelCode"
                    },new ColumnItem {
                        Title="原始单据编号",
                        Name = "OriginalRequirementBillCode"
                    },new ColumnItem {
                        Title="出库单号",
                        Name = "OutCode"
                    },new ColumnItem {
                        Title="出库时间",
                        Name = "OutTime"
                    },new ColumnItem {
                        Title="客户名称",
                        Name = "CounterpartCompanyName"
                    },new ColumnItem {
                        Title="客户编号",
                        Name = "CounterpartCompanyCode"
                    },new ColumnItem {
                        Title="收货单号",
                        Name = "ShippingCode"
                    },new KeyValuesColumnItem {
                        Title="出库类型",
						KeyValueItems = this.KeyValueManager[kvNames[1]],
                        Name = "OutboundType",
                    },new KeyValuesColumnItem {
                        Title="状态",
						KeyValueItems = this.KeyValueManager[kvNames[0]],
                        Name = "Status",
                    }
                };
            }
        }
        protected override Type EntityType
        {
            get
            {
                return typeof(TraceWarehouseOut);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName)
        {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if (compositeFilterItem != null)
            {
                foreach(var item in compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).Where(filter => filter.MemberName != "CounterpartCompanyCode" && filter.MemberName != "CounterpartCompanyName"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "status":
                        return filters.Filters.Single(item => item.MemberName == "Status").Value;
                    case "warehouseId":
                        return filters.Filters.Single(item => item.MemberName == "WarehouseId").Value;
                    case "outCode":
                        return filters.Filters.Single(item => item.MemberName == "OutCode").Value;
                    case "counterpartCompanyName":
                        return filters.Filters.Single(item => item.MemberName == "CounterpartCompanyName").Value;
                    case "sparePartCode":
                        return filters.Filters.Single(item => item.MemberName == "SparePartCode").Value;
                    case "outboundType":
                        return filters.Filters.Single(item => item.MemberName == "OutboundType").Value;
                    case "traceCode":
                        return filters.Filters.Single(item => item.MemberName == "TraceCode").Value;
                    case "originalRequirementBillCode":
                        return filters.Filters.Single(item => item.MemberName == "OriginalRequirementBillCode").Value;
                    case "bCreatetime":
                        var bCreateTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "OutTime");
                        return bCreateTime == null ? null : bCreateTime.Filters.First(item => item.MemberName == "OutTime").Value;
                    case "eCreatetime":
                        var eCreateTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "OutTime");
                        return eCreateTime == null ? null : eCreateTime.Filters.Last(item => item.MemberName == "OutTime").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName()
        {
            return "配件出库精确码物流跟踪";
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
        }
    }
}
