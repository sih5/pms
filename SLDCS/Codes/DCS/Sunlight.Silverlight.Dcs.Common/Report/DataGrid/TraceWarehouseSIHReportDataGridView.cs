﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class TraceWarehouseSIHReportDataGridView : DcsDataGridViewBase
    {
        private readonly string[] kvNames = new[] {
            "AccurateTraceStatus","Parts_InboundType"
        };
        public TraceWarehouseSIHReportDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                        Name = "SuplierCode",
                        Title = "对方单位代码",
                    },new ColumnItem {
                        Title="对方单位名称",
                        Name = "SuplierName"
                    },new ColumnItem {
                        Name = "SparePartCode",
                        Title = "配件编号",
                    },new ColumnItem {
                        Title="配件名称",
                        Name = "SparePartName"
                    },new ColumnItem {
                        Title="追溯码",
                        Name = "TraceCode"
                    },new ColumnItem {
                        Title="SIH标签码",
                        Name = "SIHLabelCode"
                    },new ColumnItem {
                        Title ="仓库编号",
                        Name = "WarehouseCode" 
                    },new ColumnItem {
                        Title ="仓库名称",
                        Name = "WarehouseName" 
                    },new ColumnItem {
                        Title="原始单据编号",
                        Name = "OriginalRequirementBillCode"
                    },new ColumnItem {
                        Title="供应商发运单号",
                        Name = "SourceCode"
                    },new ColumnItem {
                        Title="入库时间",
                        Name = "InBoundDate"
                    },new ColumnItem {
                        Title="包装任务单号",
                        Name = "PackingTaskCode"
                    },new KeyValuesColumnItem {
                        Title="入库类型",
						KeyValueItems = this.KeyValueManager[kvNames[1]],
                        Name = "InboundType",
                    },new KeyValuesColumnItem {
                        Title="状态",
						KeyValueItems = this.KeyValueManager[kvNames[0]],
                        Name = "Status",
                    },new ColumnItem {
                        Title="入库数量",
                        Name = "InQty"
                    },new ColumnItem {
                        Title="出库数量",
                        Name = "OutQty"
                    }
                };
            }
        }
        protected override Type EntityType
        {
            get
            {
                return typeof(TraceWarehouseAge);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName)
        {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if (compositeFilterItem != null)
            {
                foreach(var item in compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).Where(filter => filter.MemberName != "CounterpartCompanyCode" && filter.MemberName != "CounterpartCompanyName"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "status":
                        return filters.Filters.Single(item => item.MemberName == "Status").Value;
                    case "counterpartCompanyCode":
                        return filters.Filters.Single(item => item.MemberName == "CounterpartCompanyCode").Value;
                    case "counterpartCompanyName":
                        return filters.Filters.Single(item => item.MemberName == "CounterpartCompanyName").Value;
                    case "sparePartCode":
                        return filters.Filters.Single(item => item.MemberName == "SparePartCode").Value;
                    case "sIHLabelCode":
                        return filters.Filters.Single(item => item.MemberName == "SIHLabelCode").Value;
                    case "traceCode":
                        return filters.Filters.Single(item => item.MemberName == "TraceCode").Value;
                    case "packingTaskCode":
                        return filters.Filters.Single(item => item.MemberName == "PackingTaskCode").Value;
                    case "warehouseId":
                        return filters.Filters.Single(item => item.MemberName == "WarehouseId").Value;
                    case "originalRequirementBillCode":
                        return filters.Filters.Single(item => item.MemberName == "OriginalRequirementBillCode").Value;
                    case "bInBoundDate":
                        var bInBoundDate = composites.FirstOrDefault(p => p.Filters.First().MemberName == "InBoundDate");
                        return bInBoundDate == null ? null : bInBoundDate.Filters.First(item => item.MemberName == "InBoundDate").Value;
                    case "eInBoundDate":
                        var eInBoundDate = composites.FirstOrDefault(p => p.Filters.First().MemberName == "InBoundDate");
                        return eInBoundDate == null ? null : eInBoundDate.Filters.Last(item => item.MemberName == "InBoundDate").Value;

                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName()
        {
            return "配件入库精确码物流跟踪";
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
        }
    }
}
