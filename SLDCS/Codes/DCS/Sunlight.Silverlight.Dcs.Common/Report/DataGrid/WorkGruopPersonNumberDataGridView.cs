﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report.DataGrid {
    public class WorkGruopPersonNumberDataGridView : DcsDataGridViewBase
    {
        private readonly string[] kvNames = new[] {
            "WorkGroup"
        };
        public WorkGruopPersonNumberDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new KeyValuesColumnItem {
                       Name = "WorkGruop",
                       KeyValueItems= this.KeyValueManager[kvNames[0]],
                        Title = CommonUIStrings.Report_Title_WorkGruopPersonNumber_WorkGruop,
                    },new ColumnItem {
                        Title=CommonUIStrings.Report_Title_WorkGruopPersonNumber_EmployeeNumber,
                        Name = "EmployeeNumber"
                    } , new ColumnItem {
                        Name = "CreatorName",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_CreatorName
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title = CommonUIStrings.QueryPanel_Title_ProductStandard_CreateTime,
                    }, new ColumnItem {
                        Name = "ModifierName",
                        Title = CommonUIStrings.DataDeTailPanel_Text_BottomStock_ModifierName
                    }, new ColumnItem {
                        Name = "ModifyTime",
                         Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_ModifyTime
                    }
                };
            }
        }
        protected override Type EntityType
        {
            get
            {
                return typeof(WorkGruopPersonNumber);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return true;
            }
        }    
        protected override string OnRequestQueryName()
        {
            return "GetWorkGruopPersonNumbers";
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 50;
        }
    }
}
