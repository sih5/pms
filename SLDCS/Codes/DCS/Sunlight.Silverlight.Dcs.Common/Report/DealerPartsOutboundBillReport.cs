﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
namespace Sunlight.Silverlight.Dcs.Common.Report {
    [PageMeta("Report", "Sale", "DealerPartsOutboundBillReport", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class DealerPartsOutboundBillReport : DcsDataManagementViewBase
    {
        private DataGridViewBase dataGridView;
        public DealerPartsOutboundBillReport()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.QueryPanel_Title_DealerPartsOutboundBillReport;
        }
        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("DealerPartsOutboundBillReport"));
            }
        }
        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "DealerPartsOutboundBillReport"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem)
        {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId)
            {

                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if (filterItem == null)
                        return;
                    var code = filterItem.Filters.Single(e => e.MemberName == "Code").Value as string;
                    var outInBandtype = filterItem.Filters.Single(e => e.MemberName == "OutInBandtype").Value as string;
                    var sparePartCode = filterItem.Filters.Single(e => e.MemberName == "PartsCode").Value as string;
                     var marketingDepartmentName = filterItem.Filters.Single(e => e.MemberName == "MarketingDepartmentName").Value as string;
                     var centerName = filterItem.Filters.Single(e => e.MemberName == "CenterName").Value as string;
                    var sparePartName = filterItem.Filters.Single(e => e.MemberName == "PartsName").Value as string;
                    var dealerName = filterItem.Filters.Single(e => e.MemberName == "DealerName").Value as string;
                    DateTime? bOutBoundTime = null;
                    DateTime? eOutBoundTime = null;
                    foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                        var dateTime = filter as CompositeFilterItem;
                        if(dateTime != null) {
                            if(dateTime.Filters.First().MemberName == "OutBoundTime") {
                                bOutBoundTime = dateTime.Filters.First(r => r.MemberName == "OutBoundTime").Value as DateTime?;
                                eOutBoundTime = dateTime.Filters.Last(r => r.MemberName == "OutBoundTime").Value as DateTime?;
                            }
                           

                        }
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出服务站出库单(code, outInBandtype,sparePartCode,sparePartName,dealerName,bOutBoundTime,eOutBoundTime, marketingDepartmentName, centerName, loadOp =>
                    {
                        if (loadOp.HasError)
                        {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if (loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value))
                        {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if (loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                        {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId)
            {
                case CommonActionKeys.EXPORT:
                    return true;
                default: return false;
            }
        }
    }
}