﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Report {
     [PageMeta("Report", "SparePart", "InOutWorkPerHourForReport", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class InOutWorkPerHourForReport : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        public InOutWorkPerHourForReport() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.QueryPanel_Title_InOutWorkPerHour;
        }
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("InOutWorkPerHourForReport"));
            }
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "InOutWorkPerHourForReport"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            var compositeFilterItem = new CompositeFilterItem();
            if(filterItem != null) {
                var originalFilterItem = filterItem as CompositeFilterItem;
                if(originalFilterItem != null)
                    foreach(var filter in originalFilterItem.Filters)
                        compositeFilterItem.Filters.Add(filter);
                else
                    compositeFilterItem.Filters.Add(filterItem);
            }
            this.SwitchViewTo(DATA_GRID_VIEW);
            if(compositeFilterItem == null)
                return;
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {

                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var bCreateTime = filterItem.Filters.Single(e => e.MemberName == "CreateTime").Value as DateTime?;

                    this.dcsDomainContext.导出出入库每小时工作量(bCreateTime, loadOp => {
                        if(loadOp.HasError) {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return true;
                default: return false;
            }
        }
    }
}