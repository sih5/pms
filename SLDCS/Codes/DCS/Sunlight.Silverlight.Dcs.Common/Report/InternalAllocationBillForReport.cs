﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;


namespace Sunlight.Silverlight.Dcs.Common.Report {
      [PageMeta("Report", "Financial", "InternalAllocationBillForReport", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class InternalAllocationBillForReport : DcsDataManagementViewBase
    {
        private DataGridViewBase dataGridView;
        public InternalAllocationBillForReport()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.QueryPanel_Title_InternalAllocationBillForReport;
        }
        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("InternalAllocationBillForReport"));
            }
        }
        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "InternalAllocationBillForReport"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            var compositeFilterItem = new CompositeFilterItem();
            if(filterItem != null) {
                var originalFilterItem = filterItem as CompositeFilterItem;
                if(originalFilterItem != null)
                    foreach(var filter in originalFilterItem.Filters)
                        compositeFilterItem.Filters.Add(filter);
                else
                    compositeFilterItem.Filters.Add(filterItem);
            }
            this.SwitchViewTo(DATA_GRID_VIEW);          
            if (compositeFilterItem == null)
                return;
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId)
            {

                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if (filterItem == null)
                        return;
                    var warehouseId = filterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                    var sparePartCode = filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                    DateTime? bPartsOutboundBillTime = null;
                    DateTime? ePartsOutboundBillTime = null;
                    DateTime? bPartsRequisitionSettleBillDate = null;
                    DateTime? ePartsRequisitionSettleBillDate = null;
                    foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                        var dateTime = filter as CompositeFilterItem;
                        if(dateTime != null) {
                            if(dateTime.Filters.First().MemberName == "PartsOutboundBillTime") {
                                bPartsOutboundBillTime = dateTime.Filters.First(r => r.MemberName == "PartsOutboundBillTime").Value as DateTime?;
                                ePartsOutboundBillTime = dateTime.Filters.Last(r => r.MemberName == "PartsOutboundBillTime").Value as DateTime?;
                            }
                            if(dateTime.Filters.First().MemberName == "PartsRequisitionSettleBillDate") {
                                bPartsRequisitionSettleBillDate = dateTime.Filters.First(r => r.MemberName == "PartsRequisitionSettleBillDate").Value as DateTime?;
                                ePartsRequisitionSettleBillDate = dateTime.Filters.Last(r => r.MemberName == "PartsRequisitionSettleBillDate").Value as DateTime?;
                            }
                            

                        }
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出内部领用统计(warehouseId, sparePartCode, bPartsOutboundBillTime, ePartsOutboundBillTime,bPartsRequisitionSettleBillDate,ePartsRequisitionSettleBillDate, loadOp =>
                    {
                        if (loadOp.HasError)
                        {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if (loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value))
                        {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if (loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                        {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId)
            {
                case CommonActionKeys.EXPORT:
                    return true;
                default: return false;
            }
        }
    }
}