﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Report {
    [PageMeta("Report", "Purchase", "PartsInboundCheckBillForReport", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class PartsInboundCheckBillForReportReport : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        public PartsInboundCheckBillForReportReport() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.QueryPanel_Title_PartsInboundCheckBillForReportReport;
        }
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsInboundCheckBillForReport"));
            }
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsInboundCheckBillForReport"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {

                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var code = filterItem.Filters.Single(e => e.MemberName == "Code").Value as string;
                    var partsPurchaseOrderCode = filterItem.Filters.Single(e => e.MemberName == "PartsPurchaseOrderCode").Value as string;
                    var sparePartCode = filterItem.Filters.Single(e => e.MemberName == "SparePartCode").Value as string;
                    var settleBillStatus = filterItem.Filters.Single(e => e.MemberName == "SettleBillStatus").Value as int?;
                    var supplierPartName = filterItem.Filters.Single(r => r.MemberName == "PartsSupplierName").Value as string;
                    var settleBillCode = filterItem.Filters.Single(r => r.MemberName == "SettleBillCode").Value as string;
                    DateTime? bCreateTime = null;
                    DateTime? eCreateTime = null;
                    DateTime? bPartsPurchaseOrderDate = null;
                    DateTime? ePartsPurchaseOrderDate = null;
                    foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                        var dateTime = filter as CompositeFilterItem;
                        if(dateTime != null) {
                            if(dateTime.Filters.First().MemberName == "CreateTime") {
                                bCreateTime = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                eCreateTime = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                            }
                            if(dateTime.Filters.First().MemberName == "PartsPurchaseOrderDate") {
                                bPartsPurchaseOrderDate = dateTime.Filters.First(r => r.MemberName == "PartsPurchaseOrderDate").Value as DateTime?;
                                ePartsPurchaseOrderDate = dateTime.Filters.Last(r => r.MemberName == "PartsPurchaseOrderDate").Value as DateTime?;
                            }

                        }
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出采购入库及供应商差价报表(bCreateTime, eCreateTime, bPartsPurchaseOrderDate, ePartsPurchaseOrderDate, code, partsPurchaseOrderCode, sparePartCode, supplierPartName, settleBillStatus, settleBillCode, loadOp => {
                        if(loadOp.HasError) {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return true;
                default: return false;
            }
        }
    }
}