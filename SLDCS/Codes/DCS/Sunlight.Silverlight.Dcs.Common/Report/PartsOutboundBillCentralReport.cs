﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.Common.Resources;
namespace Sunlight.Silverlight.Dcs.Common.Report {
     [PageMeta("Report", "Sale", "PartsOutboundBillCentralReport", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class PartsOutboundBillCentralReport: DcsDataManagementViewBase
    {
        private DataGridViewBase dataGridView;
        public PartsOutboundBillCentralReport()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.QueryPanel_Title_PartsOutboundBillCentralReport;
        }
        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsOutboundBillCentralReport"));
            }
        }
        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "PartsOutboundBillCentralReport"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem)
        {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId)
            {

                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if (filterItem == null)
                        return;
                    var companyCode = filterItem.Filters.Single(e => e.MemberName == "CompanyCode").Value as string;
                    var referenceCode = filterItem.Filters.Single(e => e.MemberName == "ReferenceCode").Value as string;
                    var sparePartCode = filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                    var companyName = filterItem.Filters.Single(r => r.MemberName == "CompanyName").Value as string;
                    var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                    var partsSalesOrderCode = filterItem.Filters.Single(r => r.MemberName == "PartsSalesOrderCode").Value as string;
                    var submitCompanyName = filterItem.Filters.Single(r => r.MemberName == "SubmitCompanyName").Value as string;
                    var customerTypeInt = filterItem.Filters.Single(r => r.MemberName == "CustomerTypeInt").Value as int?;
                    var customerTypeTy = filterItem.Filters.Single(r => r.MemberName == "CustomerTypeTy").Value as int?;
                      DateTime? bShippingDate = null;
                    DateTime? eShippingDate = null;
                    foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                        var dateTime = filter as CompositeFilterItem;
                        if(dateTime != null) {                           
                            if(dateTime.Filters.First().MemberName == "ShippingDate") {
                                bShippingDate = dateTime.Filters.First(r => r.MemberName == "ShippingDate").Value as DateTime?;
                                eShippingDate = dateTime.Filters.Last(r => r.MemberName == "ShippingDate").Value as DateTime?;
                            }
                        }
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出中心库销售出库单(bShippingDate, eShippingDate, companyCode, companyName, code, sparePartCode, referenceCode, partsSalesOrderCode, submitCompanyName,customerTypeInt,customerTypeTy, loadOp =>
                    {
                        if (loadOp.HasError)
                        {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if (loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value))
                        {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if (loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                        {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId)
            {
                case CommonActionKeys.EXPORT:
                    return true;
                default: return false;
            }
        }
    }
}