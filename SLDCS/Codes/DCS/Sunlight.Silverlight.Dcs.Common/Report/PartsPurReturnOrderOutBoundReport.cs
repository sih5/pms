﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report {
     [PageMeta("Report", "Financial", "PartsPurReturnOrderOutBoundReport", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class PartsPurReturnOrderOutBoundReport : DcsDataManagementViewBase
    {
        private DataGridViewBase dataGridView;
        public PartsPurReturnOrderOutBoundReport()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.QueryPanel_Title_PartsPurReturnOrderOutBoundReport;
        }
        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsPurReturnOrderOutBoundReport"));
            }
        }
        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "PartsPurReturnOrderOutBoundReport"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            var compositeFilterItem = new CompositeFilterItem();
            if(filterItem != null) {
                var originalFilterItem = filterItem as CompositeFilterItem;
                if(originalFilterItem != null)
                    foreach(var filter in originalFilterItem.Filters)
                        compositeFilterItem.Filters.Add(filter);
                else
                    compositeFilterItem.Filters.Add(filterItem);
            }
            this.SwitchViewTo(DATA_GRID_VIEW);          
            if (compositeFilterItem == null)
                return;
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId)
            {

                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if (filterItem == null)
                        return;
                    var sparePartCode = filterItem.Filters.Single(e => e.MemberName == "SparePartCode").Value as string;
                    var partsPurReturnOrderCode = filterItem.Filters.Single(e => e.MemberName == "PartsPurReturnOrderCode").Value as string;
                   // var settlementStatus = filterItem.Filters.Single(e => e.MemberName == "SettlementStatus").Value as int?;
                    var settlementNo = filterItem.Filters.Single(e => e.MemberName == "SettlementNo").Value as string;
                    DateTime? bOutBoundTime = null;
                    DateTime? eOutBoundTime = null;
                    string settlementStatus = string.Empty;
                    foreach (var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)))
                    {
                        var dateTime = filter as CompositeFilterItem;
                        if (dateTime != null)
                        {
                            if(((CompositeFilterItem)dateTime).Filters.Any(r => r.MemberName == "OutBoundTime")) {
                                bOutBoundTime = dateTime.Filters.First(r => r.MemberName == "OutBoundTime").Value as DateTime?;
                                eOutBoundTime = dateTime.Filters.Last(r => r.MemberName == "OutBoundTime").Value as DateTime?;
                            }     
                            if(((CompositeFilterItem)dateTime).Filters.Any(r => r.MemberName == "SettlementStatus"))
                                settlementStatus = string.Join(",", dateTime.Filters.Where(r => r.MemberName == "SettlementStatus").Select(r => r.Value));
                        }
                    }
                  
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出采购退货统计(bOutBoundTime, eOutBoundTime, partsPurReturnOrderCode, settlementStatus, settlementNo, sparePartCode, loadOp =>
                    {
                        if (loadOp.HasError)
                        {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if (loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value))
                        {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if (loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                        {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId)
            {
                case CommonActionKeys.EXPORT:
                    return true;
                default: return false;
            }
        }
    }
}