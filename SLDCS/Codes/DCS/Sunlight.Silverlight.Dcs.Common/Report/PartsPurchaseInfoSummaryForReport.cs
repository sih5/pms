﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report {
    [PageMeta("Report", "SparePart", "PartsPurchaseInfoSummaryForReport", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class PartsPurchaseInfoSummaryForReport : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        public PartsPurchaseInfoSummaryForReport() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.QueryPanel_Title_PartsPurchaseInfoSummaryForReport;
        }
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsPurchaseInfoSummaryForReport"));
            }
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsPurchaseInfoSummaryForReport"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            var compositeFilterItem = new CompositeFilterItem();
            if(filterItem != null) {
                var originalFilterItem = filterItem as CompositeFilterItem;
                if(originalFilterItem != null)
                    foreach(var filter in originalFilterItem.Filters)
                        compositeFilterItem.Filters.Add(filter);
                else
                    compositeFilterItem.Filters.Add(filterItem);
                if(originalFilterItem != null) {
                    var time = originalFilterItem.Filters.FirstOrDefault(r => r.MemberName == "CreateTime");
                    if(time.Value == null) {
                        UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Validation_Company_QueryTimeBegin);
                        return;
                    }
                }
            }
            ClientVar.ConvertTime(compositeFilterItem);
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {

                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var partCode = filterItem.Filters.Single(e => e.MemberName == "PartCode").Value as string;
                    var supplierName = filterItem.Filters.Single(e => e.MemberName == "SupplierName").Value as string;
                    var partabc = filterItem.Filters.Single(e => e.MemberName == "PartABC").Value as int?;
                    var priority = filterItem.Filters.Single(e => e.MemberName == "Priority").Value as int?;
                   // var oweOrderNum = filterItem.Filters.Single(r => r.MemberName == "OweOrderNum").Value as int?;
                    var createTime = filterItem.Filters.Single(r => r.MemberName == "CreateTime").Value as  DateTime?;
                    int? oweOrderNum = null;
                    if(null!=filterItem.Filters.First(e => e.MemberName == "OweOrderNum").Value) {
                        oweOrderNum = Int32.Parse(filterItem.Filters.First(e => e.MemberName == "OweOrderNum").Value.ToString()) as int?;
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出配件优先级分析报表(createTime, partCode, partabc, supplierName, priority, oweOrderNum, loadOp => {
                        if(loadOp.HasError) {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return true;
                default: return false;
            }
        }
    }
}