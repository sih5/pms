﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report {
     [PageMeta("Report", "SparePart", "PartsPurchaseOrderDetailForReport", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class PartsPurchaseOrderDetailForReport : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        public PartsPurchaseOrderDetailForReport() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.QueryPanel_Title_PartsPurchaseDetailForReport;
        }
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsPurchaseOrderDetailForReport"));
            }
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsPurchaseOrderDetailForReport"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            var compositeFilterItem = new CompositeFilterItem();
            if(filterItem != null) {
                var originalFilterItem = filterItem as CompositeFilterItem;
                if(originalFilterItem != null)
                    foreach(var filter in originalFilterItem.Filters)
                        compositeFilterItem.Filters.Add(filter);
                else
                    compositeFilterItem.Filters.Add(filterItem);
                if(originalFilterItem != null) {
                    var time = originalFilterItem.Filters.FirstOrDefault(r => r.MemberName == "CreateTime");
                    if(time.Value == null) {
                        UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Validation_Company_QueryTimeBegin);
                        return;
                    }
                }
            }
            ClientVar.ConvertTime(compositeFilterItem);
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {

                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var partCode = filterItem.Filters.Single(e => e.MemberName == "PartCode").Value as string;
                    var supplierName = filterItem.Filters.Single(e => e.MemberName == "SupplierName").Value as string;
                    var inboundFinishStatus = filterItem.Filters.Single(e => e.MemberName == "InboundFinishStatus").Value as int?;
                    var createTime = filterItem.Filters.Single(r => r.MemberName == "CreateTime").Value as  DateTime?;
                    DateTime? bPriorityCreateTime = null;
                    DateTime? ePriorityCreateTime = null;
                    DateTime? bPlanDeliveryTime = null;
                    DateTime? ePlanDeliveryTime = null;
                    foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                        var dateTime = filter as CompositeFilterItem;
                        if(dateTime != null) {
                            if(dateTime.Filters.First().MemberName == "PriorityCreateTime") {
                                bPriorityCreateTime = dateTime.Filters.First(r => r.MemberName == "PriorityCreateTime").Value as DateTime?;
                                ePriorityCreateTime = dateTime.Filters.Last(r => r.MemberName == "PriorityCreateTime").Value as DateTime?;
                            }
                            if(dateTime.Filters.First().MemberName == "PlanDeliveryTime") {
                                bPlanDeliveryTime = dateTime.Filters.First(r => r.MemberName == "PlanDeliveryTime").Value as DateTime?;
                                ePlanDeliveryTime = dateTime.Filters.Last(r => r.MemberName == "PlanDeliveryTime").Value as DateTime?;
                            }

                        }
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出优先入库明细表(createTime, partCode, supplierName, inboundFinishStatus, bPriorityCreateTime, ePriorityCreateTime, bPlanDeliveryTime, ePlanDeliveryTime, loadOp => {
                        if(loadOp.HasError) {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return true;
                default: return false;
            }
        }
    }
}