﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report {
      [PageMeta("Report", "Purchase", "PartsPurchaseOrderShort", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class PartsPurchaseOrderShortForReport : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        public PartsPurchaseOrderShortForReport() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.QueryPanel_Title_PartsPurchaseOrderShortForReport;
        }
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsPurchaseOrderShort"));
            }
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsPurchaseOrderFinish"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = new CompositeFilterItem();
            if(filterItem != null) {
                var originalFilterItem = filterItem as CompositeFilterItem;
                if(originalFilterItem != null)
                    foreach(var filter in originalFilterItem.Filters)
                        compositeFilterItem.Filters.Add(filter);
                else
                    compositeFilterItem.Filters.Add(filterItem);
            }
            if(compositeFilterItem.Filters.Count()==0) {
                UIHelper.ShowAlertMessage(CommonUIStrings.Management_Validatation_Time);
                return;
            }
            if(filterItem is CompositeFilterItem) {
                var compositeFilter = filterItem as CompositeFilterItem;             
                compositeFilterItem.Filters.Add(compositeFilter);
            }
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {

                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    DateTime? bPurchasePlanTime = null;
                    DateTime? ePurchasePlanTime = null;
                    DateTime? bTheoryDeliveryTime = null;
                    DateTime? eTheoryDeliveryTime = null;
                    foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                        var dateTime = filter as CompositeFilterItem;
                        if(dateTime != null) {
                            if(dateTime.Filters.First().MemberName == "PurchasePlanTime") {
                                bPurchasePlanTime = dateTime.Filters.First(r => r.MemberName == "PurchasePlanTime").Value as DateTime?;
                                ePurchasePlanTime = dateTime.Filters.Last(r => r.MemberName == "PurchasePlanTime").Value as DateTime?;
                            }
                            if(dateTime.Filters.First().MemberName == "TheoryDeliveryTime") {
                                bTheoryDeliveryTime = dateTime.Filters.First(r => r.MemberName == "TheoryDeliveryTime").Value as DateTime?;
                                eTheoryDeliveryTime = dateTime.Filters.Last(r => r.MemberName == "TheoryDeliveryTime").Value as DateTime?;
                            }

                        }
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出采购订单短供明细情况(bPurchasePlanTime, ePurchasePlanTime, bTheoryDeliveryTime, eTheoryDeliveryTime, loadOp => {
                        if(loadOp.HasError) {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return true;
                default: return false;
            }
        }
    }
}