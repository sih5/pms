﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report
{
    [PageMeta("Report", "Purchase", "PartsPurchasePlanReport", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class PartsPurchasePlanReportReport : DcsDataManagementViewBase
    {

        private DataGridViewBase dataGridView;
        public PartsPurchasePlanReportReport()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.QueryPanel_Title_PartsPurchasePlanReportReport;
        }
        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsPurchasePlanReport"));
            }
        }
        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "PartsPurchasePlanReport"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem)
        {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId)
            {

                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if (filterItem == null)
                        return;
                    var sparePartCode = filterItem.Filters.Single(e => e.MemberName == "SparePartCode").Value as string;
                    var partsSupplierName = filterItem.Filters.Single(e => e.MemberName == "PartsSupplierName").Value as string;
                    var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                    var partsPurchaseOrderStatus = filterItem.Filters.Single(e => e.MemberName == "PartsPurchaseOrderStatus").Value as int?;
                    var isOnWayNumber = filterItem.Filters.Single(r => r.MemberName == "IsOnWayNumber").Value as bool?;
                    var remark = filterItem.Filters.Single(e => e.MemberName == "Remark").Value as string;
                    var supplierPartCode = filterItem.Filters.Single(e => e.MemberName == "SupplierPartCode").Value as string;
                    DateTime? bPartsPurchasePlanTime = null;
                    DateTime? ePartsPurchasePlanTime = null;
                    DateTime? bPartsPurchaseOrderDate = null;
                    DateTime? ePartsPurchaseOrderDate = null;
                    DateTime? bSubmitTime = null;
                    DateTime? eSubmitTime = null;

                    foreach (var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)))
                    {
                        var dateTime = filter as CompositeFilterItem;
                        if (dateTime != null)
                        {
                            if (dateTime.Filters.First().MemberName == "PartsPurchasePlanTime")
                            {
                                bPartsPurchasePlanTime = dateTime.Filters.First(r => r.MemberName == "PartsPurchasePlanTime").Value as DateTime?;
                                ePartsPurchasePlanTime = dateTime.Filters.Last(r => r.MemberName == "PartsPurchasePlanTime").Value as DateTime?;
                            }
                            if (dateTime.Filters.First().MemberName == "PartsPurchaseOrderDate")
                            {
                                bPartsPurchaseOrderDate = dateTime.Filters.First(r => r.MemberName == "PartsPurchaseOrderDate").Value as DateTime?;
                                ePartsPurchaseOrderDate = dateTime.Filters.Last(r => r.MemberName == "PartsPurchaseOrderDate").Value as DateTime?;
                            }
                            if(dateTime.Filters.First().MemberName == "SubmitTime") {
                                bSubmitTime = dateTime.Filters.First(r => r.MemberName == "SubmitTime").Value as DateTime?;
                                eSubmitTime = dateTime.Filters.Last(r => r.MemberName == "SubmitTime").Value as DateTime?;
                            }
                        }
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出采购订单统计(sparePartCode, partsSupplierName, status, partsPurchaseOrderStatus, bPartsPurchasePlanTime, ePartsPurchasePlanTime, bPartsPurchaseOrderDate, ePartsPurchaseOrderDate, isOnWayNumber, remark,supplierPartCode,bSubmitTime,eSubmitTime, loadOp =>
                    {
                        if (loadOp.HasError)
                        {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if (loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value))
                        {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if (loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                        {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId)
            {
                case CommonActionKeys.EXPORT:
                    return true;
                default: return false;
            }
        }
    }
}
