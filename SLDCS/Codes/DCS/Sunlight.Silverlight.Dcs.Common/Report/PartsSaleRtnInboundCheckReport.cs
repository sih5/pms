﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report {
     [PageMeta("Report", "Financial", "PartsSaleRtnInboundCheckReport", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class PartsSaleRtnInboundCheckReport : DcsDataManagementViewBase
    {
        private DataGridViewBase dataGridView;
        public PartsSaleRtnInboundCheckReport()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.QueryPanel_Title_PartsSaleRtnInboundCheckReport;
        }
        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsSaleRtnInboundCheckReport"));
            }
        }
        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "PartsSaleRtnInboundCheckReport"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            var compositeFilterItem = new CompositeFilterItem();
            if(filterItem != null) {
                var originalFilterItem = filterItem as CompositeFilterItem;
                if(originalFilterItem != null)
                    foreach(var filter in originalFilterItem.Filters)
                        compositeFilterItem.Filters.Add(filter);
                else
                    compositeFilterItem.Filters.Add(filterItem);
            }
            this.SwitchViewTo(DATA_GRID_VIEW);          
            if (compositeFilterItem == null)
                return;
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId)
            {

                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if (filterItem == null)
                        return;
                    var sparePartCode = filterItem.Filters.Single(e => e.MemberName == "SparePartCode").Value as string;
                    var partsSalesReturnBillCode = filterItem.Filters.Single(e => e.MemberName == "PartsSalesReturnBillCode").Value as string;
                    var settlementNo = filterItem.Filters.Single(e => e.MemberName == "SettlementNo").Value as string;
                    var partsSalesOrderCode = filterItem.Filters.Single(e => e.MemberName == "PartsSalesOrderCode").Value as string;
                    var code = filterItem.Filters.Single(e => e.MemberName == "Code").Value as string;
                    var sAPSysInvoiceNumber = filterItem.Filters.Single(e => e.MemberName == "SAPSysInvoiceNumber").Value as string;
                    var counterpartCompanyName = filterItem.Filters.Single(e => e.MemberName == "CounterpartCompanyName").Value as string;
                    var counterpartCompanyCode = filterItem.Filters.Single(e => e.MemberName == "CounterpartCompanyCode").Value as string;
                    DateTime? bCreateTime = null;
                    DateTime? eCreateTime = null;
                    string settlementStatus = string.Empty;
                    foreach (var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)))
                    {
                        var dateTime = filter as CompositeFilterItem;
                        if (dateTime != null)
                        {
                            if(((CompositeFilterItem)dateTime).Filters.Any(r => r.MemberName == "CreateTime")) {
                                bCreateTime = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                eCreateTime = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                            }  
                            if(((CompositeFilterItem)dateTime).Filters.Any(r => r.MemberName == "SettlementStatus"))
                                settlementStatus = string.Join(",", dateTime.Filters.Where(r => r.MemberName == "SettlementStatus").Select(r => r.Value));
                        }
                    }
                  
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出销售退货入库单统计(bCreateTime, eCreateTime, partsSalesReturnBillCode, partsSalesOrderCode, sparePartCode, code, settlementStatus, settlementNo, sAPSysInvoiceNumber,counterpartCompanyName,counterpartCompanyCode, loadOp =>
                    {
                        if (loadOp.HasError)
                        {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if (loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value))
                        {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if (loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                        {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId)
            {
                case CommonActionKeys.EXPORT:
                    return true;
                default: return false;
            }
        }
    }
}