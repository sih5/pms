﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report {
      [PageMeta("Report", "Sale", "PartsSalesOrderOnWayForReport", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class PartsSalesOrderOnWayForReport: DcsDataManagementViewBase
    {
        private DataGridViewBase dataGridView;
        public PartsSalesOrderOnWayForReport()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.QueryPanel_Title_PartsSalesOrderOnWayForReport;
        }
        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsSalesOrderOnWayForReport"));
            }
        }
        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "PartsSalesOrderOnWayForReport"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem)
        {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId)
            {

                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if (filterItem == null)
                        return;
                    var shippingCode = filterItem.Filters.Single(e => e.MemberName == "ShippingCode").Value as string;                
                    var submitCompanyName = filterItem.Filters.Single(e => e.MemberName == "SubmitCompanyName").Value as string;
                    var submitCompanyType = filterItem.Filters.Single(e => e.MemberName == "SubmitCompanyType").Value as int?;  
                    DateTime? bApproveTime = null;
                    DateTime? eApproveTime = null;
                    DateTime? bShippingDate = null;
                    DateTime? eShippingDate = null;
                    foreach (var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)))
                    {
                        var dateTime = filter as CompositeFilterItem;
                        if (dateTime != null)
                        {                            
                            if (dateTime.Filters.First().MemberName == "ApproveTime")
                            {
                                bApproveTime = dateTime.Filters.First(r => r.MemberName == "ApproveTime").Value as DateTime?;
                                eApproveTime = dateTime.Filters.Last(r => r.MemberName == "ApproveTime").Value as DateTime?;
                            }
                            if (dateTime.Filters.First().MemberName == "ShippingDate")
                            {
                                bShippingDate = dateTime.Filters.First(r => r.MemberName == "ShippingDate").Value as DateTime?;
                                eShippingDate = dateTime.Filters.Last(r => r.MemberName == "ShippingDate").Value as DateTime?;
                            }
                        }
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出销售在途统计(bApproveTime, eApproveTime, bShippingDate, eShippingDate, shippingCode, submitCompanyName,submitCompanyType, loadOp =>
                    {
                        if (loadOp.HasError)
                        {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if (loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value))
                        {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if (loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                        {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId)
            {
                case CommonActionKeys.EXPORT:
                    return true;
                default: return false;
            }
        }
    }
}