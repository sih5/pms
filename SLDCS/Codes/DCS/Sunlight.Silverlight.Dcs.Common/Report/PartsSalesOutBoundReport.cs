﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report {
    [PageMeta("Report", "Sale", "PartsSalesOutBoundReport", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class PartsSalesOutBoundReport : DcsDataManagementViewBase
    {
        private DataGridViewBase dataGridView;
        public PartsSalesOutBoundReport()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.QueryPanel_Title_PartsSalesOutBoundReport;
        }
        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsSalesOutBoundReport"));
            }
        }
        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "PartsSalesOutBoundReport"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem)
        {           
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            var codes = compositeFilterItem.Filters.SingleOrDefault(s => s.MemberName == "SparePartCode");
            if(codes != null && codes.Value != null) {
                compositeFilterItem.Filters.Add(new CompositeFilterItem {
                    MemberName = "SparePartCode",
                    MemberType = typeof(string),
                    Value = string.Join(",", ((IEnumerable<string>)codes.Value).ToArray()),
                    Operator = FilterOperator.Contains
                });
                compositeFilterItem.Filters.Remove(codes);
            }
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId)
            {

                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if (filterItem == null)
                        return;
                    var sparePartCode = filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                    var code = filterItem.Filters.Single(e => e.MemberName == "Code").Value as string;
                    var referenceCode = filterItem.Filters.Single(e => e.MemberName == "ReferenceCode").Value as string;
                    var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                    var submitCompanyName = filterItem.Filters.Single(e => e.MemberName == "SubmitCompanyName").Value as string;
                    var sparePartName = filterItem.Filters.Single(r => r.MemberName == "SparePartName").Value as string;
                 //   var settlementStatus = filterItem.Filters.Single(r => r.MemberName == "SettlementStatus").Value as int?;
                    var partsOutboundBillCode = filterItem.Filters.Single(r => r.MemberName == "PartsOutboundBillCode").Value as string;
                    var province = filterItem.Filters.Single(r => r.MemberName == "Province").Value as string;
                    var marketingDepartmentName = filterItem.Filters.Single(r => r.MemberName == "MarketingDepartmentName").Value as string;
                    var partsSalesOrderTypeId = filterItem.Filters.Single(r => r.MemberName == "PartsSalesOrderTypeId").Value as int?;
                    var groupNameId = filterItem.Filters.Single(r => r.MemberName == "GroupNameId").Value as int?;
                    DateTime? bShippingDate = null;
                    DateTime? eShippingDate = null;
                    string settlementStatus = string.Empty;
                    foreach (var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)))
                    {
                        var dateTime = filter as CompositeFilterItem;
                        if (dateTime != null)
                        {
                            if(((CompositeFilterItem)dateTime).Filters.Any(r => r.MemberName == "ShippingDate")) {
                                bShippingDate = dateTime.Filters.First(r => r.MemberName == "ShippingDate").Value as DateTime?;
                                eShippingDate = dateTime.Filters.Last(r => r.MemberName == "ShippingDate").Value as DateTime?;
                            }                          
                            if(((CompositeFilterItem)dateTime).Filters.Any(r => r.MemberName == "SettlementStatus"))
                                settlementStatus = string.Join(",", dateTime.Filters.Where(r => r.MemberName == "SettlementStatus").Select(r => r.Value));
                        }
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.销售出库单统计导出(warehouseId, bShippingDate, eShippingDate, marketingDepartmentName, province, code, partsOutboundBillCode, sparePartName, referenceCode, submitCompanyName, settlementStatus, partsSalesOrderTypeId,groupNameId, sparePartCode, loadOp =>
                    {
                        if (loadOp.HasError)
                        {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if (loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value))
                        {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if (loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                        {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId)
            {
                case CommonActionKeys.EXPORT:
                    return true;
                default: return false;
            }
        }
    }
}