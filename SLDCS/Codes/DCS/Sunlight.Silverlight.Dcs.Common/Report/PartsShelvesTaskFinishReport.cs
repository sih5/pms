﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report
{
    [PageMeta("Report", "SparePart", "PartsShelvesTaskFinishReport", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]

    public class PartsShelvesTaskFinishReport: DcsDataManagementViewBase
    {
        private DataGridViewBase dataGridView;
        public PartsShelvesTaskFinishReport()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.QueryPanel_Title_PartsShelvesTaskFinishReport;
        }
        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsShelvesTaskFinishReport"));
            }
        }
        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "PartsShelvesTaskFinish"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem)
        {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId)
            {

                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if (filterItem == null)
                        return;
                    var partsPurchaseOrderCode = filterItem.Filters.Single(e => e.MemberName == "PartsPurchaseOrderCode").Value as string;
                    var referenceCode = filterItem.Filters.Single(e => e.MemberName == "ReferenceCode").Value as string;
                    var partABC = filterItem.Filters.Single(e => e.MemberName == "PartABC").Value as int?;
                    var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                    var sparePartCode = filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                    var partsInboundCheckBillCode = filterItem.Filters.Single(r => r.MemberName == "PartsInboundCheckBillCode").Value as string;
                    DateTime? bShelvesFinishTime = null;
                    DateTime? eShelvesFinishTime = null;
                    DateTime? bPackModifyTime = null;
                    DateTime? ePackModifyTime = null;
                    foreach (var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)))
                    {
                        var dateTime = filter as CompositeFilterItem;
                        if (dateTime != null)
                        {
                            if (dateTime.Filters.First().MemberName == "ShelvesFinishTime")
                            {
                                bShelvesFinishTime = dateTime.Filters.First(r => r.MemberName == "ShelvesFinishTime").Value as DateTime?;
                                eShelvesFinishTime = dateTime.Filters.Last(r => r.MemberName == "ShelvesFinishTime").Value as DateTime?;
                            }
                            if (dateTime.Filters.First().MemberName == "PackModifyTime")
                            {
                                bPackModifyTime = dateTime.Filters.First(r => r.MemberName == "PackModifyTime").Value as DateTime?;
                                ePackModifyTime = dateTime.Filters.Last(r => r.MemberName == "PackModifyTime").Value as DateTime?;
                            }
                           
                        }
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出上架完成情况统计(bPackModifyTime, ePackModifyTime, partsInboundCheckBillCode, warehouseId, sparePartCode, referenceCode, partABC, partsPurchaseOrderCode, bShelvesFinishTime, eShelvesFinishTime, loadOp =>
                    {
                        if (loadOp.HasError)
                        {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if (loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value))
                        {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if (loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                        {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId)
            {
                case CommonActionKeys.EXPORT:
                    return true;
                default: return false;
            }
        }
    }
}
