﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using System.Windows.Controls;
using System.Windows;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report
{
    [PageMeta("Report", "SparePart", "PartsStockStatementReport", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]

    public class PartsStockStatementReport: DcsDataManagementViewBase
    {
        private DataGridViewBase dataGridView;
        public PartsStockStatementReport()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.QueryPanel_Title_PartsStockStatementReport;
        }
        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsStockStatementReport"));
            }
        }
        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                 return new[] {
                    "PartsStockStatementReport"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem)
        {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return;

            if(filterItem is CompositeFilterItem) {
                compositeFilterItem = filterItem as CompositeFilterItem;
                var beginTime = compositeFilterItem.Filters.SingleOrDefault(filter => filter.MemberName == "BeginTime");
                if(beginTime != null && beginTime.Value == null) {
                    UIHelper.ShowNotification("时间从不可为空！");
                    return;
                }
                var endTime = compositeFilterItem.Filters.SingleOrDefault(filter => filter.MemberName == "EndTime");
                if(endTime != null && endTime.Value == null) {
                    UIHelper.ShowNotification("时间至不可为空！");
                    return;
                }
            }

            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId)
            {

                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if (filterItem == null)
                        return;
                    
                    var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                    var sparePartCode = filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                    var sparePartName = filterItem.Filters.Single(e => e.MemberName == "SparePartName").Value as string;
                    var referenceCode = filterItem.Filters.Single(e => e.MemberName == "ReferenceCode").Value as string;
                    var beginTime = filterItem.Filters.Single(r => r.MemberName == "BeginTime").Value as DateTime?;
                    var endTime = filterItem.Filters.Single(r => r.MemberName == "EndTime").Value as DateTime?;
                    
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.ExportPartsStockStatement(warehouseId, sparePartCode, sparePartName, referenceCode,beginTime, endTime, loadOp =>
                    {
                        if (loadOp.HasError)
                        {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if (loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value))
                        {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if (loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                        {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId)
            {
                case CommonActionKeys.EXPORT:
                    return true;
                default: return false;
            }
        }
    }
}
