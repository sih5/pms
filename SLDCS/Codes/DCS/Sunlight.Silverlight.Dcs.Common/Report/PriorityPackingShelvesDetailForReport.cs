﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.Common.Resources;


namespace Sunlight.Silverlight.Dcs.Common.Report {
     [PageMeta("Report", "SparePart", "PriorityPackingShelvesDetailForReport", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class PriorityPackingShelvesDetailForReport : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        public PriorityPackingShelvesDetailForReport() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.QueryPanel_Title_PriorityPackingShelvesDetailForReport;
        }
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PriorityPackingShelvesDetailForReport"));
            }
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PriorityPackingShelvesDetailForReport"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            var compositeFilterItem = new CompositeFilterItem();
            if(filterItem != null) {
                var originalFilterItem = filterItem as CompositeFilterItem;
                if(originalFilterItem != null)
                    foreach(var filter in originalFilterItem.Filters)
                        compositeFilterItem.Filters.Add(filter);
                else
                    compositeFilterItem.Filters.Add(filterItem);
                if(originalFilterItem != null) {
                    var time = originalFilterItem.Filters.FirstOrDefault(r => r.MemberName == "CreateTime");
                    if(time.Value == null) {
                        UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Validation_Company_QueryTimeBegin);
                        return;
                    }
                }
            }
            ClientVar.ConvertTime(compositeFilterItem);
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {

                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var packingFinishStatus = filterItem.Filters.Single(e => e.MemberName == "PackingFinishStatus").Value as int?;
                    var shelvesFinishStatus = filterItem.Filters.Single(e => e.MemberName == "ShelvesFinishStatus").Value as int?;
                    var priority = filterItem.Filters.Single(e => e.MemberName == "Priority").Value as int?;
                    var createTime = filterItem.Filters.Single(r => r.MemberName == "CreateTime").Value as  DateTime?;
                    DateTime? bPriorityPackingTime = null;
                    DateTime? ePriorityPackingTime = null;
                    DateTime? bPriorityShelvesTime = null;
                    DateTime? ePriorityShelvesTime = null;
                    foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                        var dateTime = filter as CompositeFilterItem;
                        if(dateTime != null) {
                            if(dateTime.Filters.First().MemberName == "PackingFinishTime") {
                                bPriorityPackingTime = dateTime.Filters.First(r => r.MemberName == "PackingFinishTime").Value as DateTime?;
                                ePriorityPackingTime = dateTime.Filters.Last(r => r.MemberName == "PackingFinishTime").Value as DateTime?;
                            }
                            if(dateTime.Filters.First().MemberName == "ShelvesFinishTime") {
                                bPriorityShelvesTime = dateTime.Filters.First(r => r.MemberName == "ShelvesFinishTime").Value as DateTime?;
                                ePriorityShelvesTime = dateTime.Filters.Last(r => r.MemberName == "ShelvesFinishTime").Value as DateTime?;
                            }

                        }
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出优先包装上架明细表(createTime, priority, packingFinishStatus, shelvesFinishStatus, bPriorityPackingTime, ePriorityPackingTime, bPriorityShelvesTime, ePriorityShelvesTime, loadOp => {
                        if(loadOp.HasError) {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return true;
                default: return false;
            }
        }
    }
}