﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report {
     [PageMeta("Report", "Purchase", "PurchaseCheckInboundReport", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class PurchaseCheckInboundReport  : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        public PurchaseCheckInboundReport() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.QueryPanel_Title_PurchaseCheckInboundReport;
        }
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PurchaseCheckInboundReport"));
            }
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PurchaseCheckInboundReport"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {

                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var code = filterItem.Filters.Single(e => e.MemberName == "Code").Value as string;
                    var sparePartCode = filterItem.Filters.Single(e => e.MemberName == "SparePartCode").Value as string;
                 //   var settleBillStatus = filterItem.Filters.Single(e => e.MemberName == "SettleBillStatus").Value as int?;
                    var partsSupplierCode = filterItem.Filters.Single(r => r.MemberName == "PartsSupplierCode").Value as string;
                    var supplierPartCode = filterItem.Filters.Single(r => r.MemberName == "SupplierPartCode").Value as string;
                    var partsSupplierName = filterItem.Filters.Single(r => r.MemberName == "PartsSupplierName").Value as string;
                    var sparePartName = filterItem.Filters.Single(r => r.MemberName == "SparePartName").Value as string;
                    var partABC = filterItem.Filters.Single(r => r.MemberName == "PartABC").Value as int?;
                    var invoice = filterItem.Filters.Single(r => r.MemberName == "Invoice").Value as string;
                    var referenceCode = filterItem.Filters.Single(r => r.MemberName == "ReferenceCode").Value as string;
                    DateTime? bCreateTime = null;
                    DateTime? eCreateTime = null;
                    string settleBillStatus = string.Empty;
                    foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                        var dateTime = filter as CompositeFilterItem;
                        if(dateTime != null) {                          
                            if(((CompositeFilterItem)dateTime).Filters.Any(r => r.MemberName == "CreateTime")) {
                                bCreateTime = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                eCreateTime = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                            }     
                            if(((CompositeFilterItem)dateTime).Filters.Any(r => r.MemberName == "SettleBillStatus"))
                                settleBillStatus = string.Join(",", dateTime.Filters.Where(r => r.MemberName == "SettleBillStatus").Select(r => r.Value));
                        }
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出配件采购入库检验单统计报表(bCreateTime, eCreateTime, code, partsSupplierCode, partsSupplierName, sparePartCode, supplierPartCode, sparePartName, partABC, settleBillStatus, invoice, referenceCode, loadOp => {
                        if(loadOp.HasError) {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return true;
                default: return false;
            }
        }
    }
}