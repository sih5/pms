﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Report {
    [PageMeta("Report", "SparePart", "PurchasePriceWarning", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class PurchasePriceWarningOfExpiryReport : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        public PurchasePriceWarningOfExpiryReport() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.Report_Title_PurchasePriceWarningOfExpiryReport;
        }
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PurchasePriceWarningOfExpiryReport"));
            }
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PurchasePriceWarningOfExpiryReport"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {

                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var partsSupplierCode = filterItem.Filters.Single(e => e.MemberName == "PartsSupplierCode").Value as string;
                    var supplierPartCode = filterItem.Filters.Single(e => e.MemberName == "SupplierPartCode").Value as string;
                    var daysRemaining = filterItem.Filters.Single(e => e.MemberName == "DaysRemaining").Value as int?;
                    var priceType = filterItem.Filters.Single(e => e.MemberName == "PriceType").Value as int?;
                    var sparepartCode = filterItem.Filters.Single(r => r.MemberName == "SparepartCode").Value as string;
                    var partsSupplierName = filterItem.Filters.Single(r => r.MemberName == "PartsSupplierName").Value as string;
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出采购价格有效到期提醒(daysRemaining, sparepartCode, supplierPartCode, partsSupplierCode, partsSupplierName, priceType, loadOp => {
                        if(loadOp.HasError) {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return true;
                default: return false;
            }
        }
    }
}