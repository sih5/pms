﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report
{
    [PageMeta("Report", "Sale", "SaleAdjustmentReport", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]

    public class SaleAdjustmentReport : DcsDataManagementViewBase
    {
        private DataGridViewBase dataGridView;
        public SaleAdjustmentReport()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.QueryPanel_Title_SaleAdjustmentReport;
        }
        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("SaleAdjustmentReport"));
            }
        }
        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "SaleAdjustmentReport"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem)
        {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return;
            var codes = compositeFilterItem.Filters.SingleOrDefault(s => s.MemberName == "SparePartCode");
                if(codes != null && codes.Value != null){
                    compositeFilterItem.Filters.Add(new CompositeFilterItem {
                        MemberName = "SparePartCode",
                        MemberType = typeof(string),
                        Value = string.Join(",",((IEnumerable<string>)codes.Value).ToArray()),
                        Operator = FilterOperator.Contains
                    });
                    compositeFilterItem.Filters.Remove(codes);
                }
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId)
            {

                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if (filterItem == null)
                        return;
                    var customerType = filterItem.Filters.Single(e => e.MemberName == "CustomerType").Value as int?;
                    var agencyName = filterItem.Filters.Single(e => e.MemberName == "AgencyName").Value as string;
                    var customerName = filterItem.Filters.Single(e => e.MemberName == "CustomerName").Value as string;
                    var sparePartCode = filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                    
                    var adjustTime = filterItem.Filters.FirstOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem) && ((CompositeFilterItem)filter).Filters.Any(e => e.MemberName == "AdjustTime")) as CompositeFilterItem;
                    DateTime? bAdjustTime = null;
                    DateTime? eAdjustTime = null;
                    if (adjustTime != null) {
                        bAdjustTime = adjustTime.Filters.First(r => r.MemberName == "AdjustTime").Value as DateTime?;
                        eAdjustTime = adjustTime.Filters.Last(r => r.MemberName == "AdjustTime").Value as DateTime?;
                    }
                    
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.ExportSaleAdjustment(customerType, agencyName, customerName, sparePartCode, bAdjustTime, eAdjustTime, loadOp =>
                    {
                        if (loadOp.HasError)
                        {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if (loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value))
                        {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if (loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                        {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId)
            {
                case CommonActionKeys.EXPORT:
                    return true;
                default: return false;
            }
        }
    }
}