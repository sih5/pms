﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report {
     [PageMeta("Report", "Sale", "SaleOrderTerminalRateSumForReport", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class SaleOrderTerminalRateSumForReport : DcsDataManagementViewBase
    {
        private DataGridViewBase dataGridView;
        public SaleOrderTerminalRateSumForReport()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.QueryPanel_Title_SaleOrderTerminalRateSumForReport;
        }
        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("SaleOrderTerminalRateSumForReport"));
            }
        }
        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "SaleOrderTerminalRateSumForReport"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem)
        {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            var month = compositeFilterItem.Filters.SingleOrDefault(s => s.MemberName == "Month").Value;
            var year = compositeFilterItem.Filters.SingleOrDefault(s => s.MemberName == "Year").Value;
            if((year == null && month != null) || year == null) {
                UIHelper.ShowAlertMessage(CommonUIStrings.Management_Validatation_Year);
                return;
            }
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();  
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId)
            {

                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if (filterItem == null)
                        return;
                    var centerName = filterItem.Filters.Single(e => e.MemberName == "CenterName").Value as string;
                    var marketingDepartmentName = filterItem.Filters.Single(e => e.MemberName == "MarketingDepartmentName").Value as string;
                    var dealerName = filterItem.Filters.Single(e => e.MemberName == "DealerName").Value as string;
                    var compositeFilterItems = filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).ToArray();
                    var year = filterItem.Filters.Single(e => e.MemberName == "Year").Value as int?;
                    var month = filterItem.Filters.Single(e => e.MemberName == "Month").Value as int?;
                    var unit = filterItem.Filters.Single(e => e.MemberName == "Unit").Value as int?;                   
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出终端满足率汇总报表(centerName, year, month, unit, marketingDepartmentName, dealerName, loadOp =>
                    {
                        if (loadOp.HasError)
                        {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if (loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value))
                        {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if (loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                        {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);

                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId)
            {
                case CommonActionKeys.EXPORT:
                    return true;
                default: return false;
            }
        }
    }
}

