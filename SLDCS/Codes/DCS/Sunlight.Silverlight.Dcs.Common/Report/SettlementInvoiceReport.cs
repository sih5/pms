﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report {
    [PageMeta("Report", "Financial", "SettlementInvoiceReport", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class SettlementInvoiceReport : DcsDataManagementViewBase
    {
        private DataGridViewBase dataGridView;
        public SettlementInvoiceReport()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.QueryPanel_Title_SettlementInvoiceReport;
        }
        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("SettlementInvoiceReport"));
            }
        }
        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "SettlementInvoiceReport"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem)
        {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId)
            {

                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if (filterItem == null)
                        return;
                    var partsOutboundBillCode = filterItem.Filters.Single(r => r.MemberName == "PartsOutboundBillCode").Value as string;
                    var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                    var invoiceNumber = filterItem.Filters.Single(r => r.MemberName == "InvoiceNumber").Value as string;
                    var submitCompanyName = filterItem.Filters.Single(r => r.MemberName == "SubmitCompanyName").Value as string;
                    DateTime? bCreateTime = null;
                    DateTime? eCreateTime = null;
                    DateTime? bOutCreateTime = null;
                     DateTime? eOutCreateTime = null;
                    foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                        var dateTime = filter as CompositeFilterItem;
                        if(dateTime != null) {
                            if(dateTime.Filters.First().MemberName == "CreateTime") {
                                bCreateTime = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                eCreateTime = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                            }
                            if(dateTime.Filters.First().MemberName == "PartsOutboundBillCreateTime") {
                                bOutCreateTime = dateTime.Filters.First(r => r.MemberName == "PartsOutboundBillCreateTime").Value as DateTime?;
                                eOutCreateTime = dateTime.Filters.Last(r => r.MemberName == "PartsOutboundBillCreateTime").Value as DateTime?;
                            }
                        }
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出结算单与出入库单关联发票统计(bOutCreateTime, eOutCreateTime, bCreateTime, eCreateTime, partsOutboundBillCode, code, invoiceNumber, submitCompanyName, loadOp =>
                    {
                        if (loadOp.HasError)
                        {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if (loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value))
                        {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if (loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                        {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId)
            {
                case CommonActionKeys.EXPORT:
                    return true;
                default: return false;
            }
        }
    }
}