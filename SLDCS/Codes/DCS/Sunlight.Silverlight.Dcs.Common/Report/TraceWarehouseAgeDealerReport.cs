﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report {
      [PageMeta("Report", "SparePart", "TraceWarehouseAgeDealerReport", ActionPanelKeys = new[] {
         CommonActionKeys.EXPORT
    })]
    public class TraceWarehouseAgeDealerReport: DcsDataManagementViewBase
    {
        private DataGridViewBase dataGridView;
        public TraceWarehouseAgeDealerReport()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = "服务站配件追溯码库位库龄管理查询";
        }
        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("TraceWarehouseAgeDealer"));
            }
        }
        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "TraceWarehouseAgeDealer"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem)
        {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId) {
               
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if (filterItem == null)
                        return;
                    var sparePartCode = filterItem.Filters.Single(e => e.MemberName == "SparePartCode").Value as string;
                    var traceProperty = filterItem.Filters.Single(r => r.MemberName == "TraceProperty").Value as int?;
                    var traceCode = filterItem.Filters.Single(r => r.MemberName == "TraceCode").Value as string;
                    var greaterThanZero = filterItem.Filters.Single(r => r.MemberName == "GreaterThanZero").Value as bool?;
                    var kvAge = filterItem.Filters.Single(r => r.MemberName == "KvAge").Value as int?;
                    var companyCode = filterItem.Filters.Single(r => r.MemberName == "CompanyCode").Value as string;
                    var companyName = filterItem.Filters.Single(r => r.MemberName == "CompanyName").Value as string;
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出服务站配件追溯码库位库龄( sparePartCode, traceProperty, traceCode, greaterThanZero, kvAge,companyCode,companyName ,loadOp =>
                    {
                        if (loadOp.HasError)
                        {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if (loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value))
                        {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if (loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                        {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId)
            {
                case CommonActionKeys.EXPORT:
                    return true;              
                default: return false;
            }
        }
    }
}
