﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Report.DataEdit;

namespace Sunlight.Silverlight.Dcs.Common.Report {
     [PageMeta("Report", "SparePart", "TraceWarehouseAgeReport", ActionPanelKeys = new[] {
        "TraceWarehouseAge"
    })]
    public class TraceWarehouseAgeReport  : DcsDataManagementViewBase
    {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        public TraceWarehouseAgeReport()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = "配件追溯码库位库龄管理查询";
        }
        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("TraceWarehouseAge"));
            }
        }
        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }
        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "TraceWarehouseAge"
                };
            }
        }
        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("TraceWarehouseAge");
                    ((TraceWarehouseAgeDataEditView)this.dataEditView).EditSubmitted += this.DataEditView_EditSubmitted;
                    dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem)
        {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId) {
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case "ChPrint":
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<TraceWarehouseAge>().FirstOrDefault();
                    if(selectedItem == null)
                        return;
                    var sparePartCodes = selectedItem.SparePartCode;
                    
                    var sparePartName = selectedItem.SparePartName;
                    if(sparePartName.Length > 25) {
                        sparePartName = sparePartName.Substring(0, 23);
                        sparePartName = sparePartName + "...";
                    }
                    var sIHCode = selectedItem.BoxCode;
                    var item = sIHCode.Split('|').ToArray();
                    var quantity = item[1].ToString();
                    var batch = item[2].ToString();
                    if(selectedItem.TraceProperty.HasValue && selectedItem.TraceProperty == (int)DCSTraceProperty.精确追溯) {
                        sparePartCodes = sparePartCodes + " JQ";
                    }
                    SunlightPrinter.ShowPrinter("补打大标签", "TraceCodeChReport", null, true, new Tuple<string, string>("SparePartCode", sparePartCodes), new Tuple<string, string>("SparePartName", sparePartName), new Tuple<string, string>("Quantity", quantity), new Tuple<string, string>("SIHCode", sIHCode), new Tuple<string, string>("BatchCode", batch));

                    break;
                case "PcPrint":
                    var selectedItemPc = this.DataGridView.SelectedEntities.Cast<TraceWarehouseAge>().FirstOrDefault();
                    if(selectedItemPc == null)
                        return;
                    var sparePartCodesPc = selectedItemPc.SparePartCode;
                    var sparePartNamePc = selectedItemPc.SparePartName;
                    if(sparePartNamePc.Length > 18) {
                        sparePartNamePc = sparePartNamePc.Substring(0, 18);
                        sparePartNamePc = sparePartNamePc + "...";
                    }
                    var sIHCodePc = selectedItemPc.SIHLabelCode;
                    var ItemPc = sIHCodePc.Split('|').ToArray();
                    var quantityPc = ItemPc[1].ToString();
                    var batchPc = ItemPc[2].ToString();
                    if(selectedItemPc.TraceProperty.HasValue && selectedItemPc.TraceProperty == (int)DCSTraceProperty.精确追溯) {
                        sparePartCodesPc = sparePartCodesPc + " JQ";
                    }
                    SunlightPrinter.ShowPrinter("补打小标签", "TraceCodePcReport", null, true, new Tuple<string, string>("SparePartCode", sparePartCodesPc), new Tuple<string, string>("SparePartName", sparePartNamePc), new Tuple<string, string>("Quantity", quantityPc), new Tuple<string, string>("SIHCode", sIHCodePc), new Tuple<string, string>("BatchCode", batchPc));

                    break;
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if (filterItem == null)
                        return;
                    var warehouseName = filterItem.Filters.Single(e => e.MemberName == "WarehouseName").Value as string;
                    var warehouseAreaCode = filterItem.Filters.Single(e => e.MemberName == "WarehouseAreaCode").Value as string;
                    var storehouseAreaCode = filterItem.Filters.Single(e => e.MemberName == "StorehouseAreaCode").Value as string;
                    var sparePartCode = filterItem.Filters.Single(e => e.MemberName == "SparePartCode").Value as string;
                    var traceProperty = filterItem.Filters.Single(r => r.MemberName == "TraceProperty").Value as int?;
                    var traceCode = filterItem.Filters.Single(r => r.MemberName == "TraceCode").Value as string;
                    var greaterThanZero = filterItem.Filters.Single(r => r.MemberName == "GreaterThanZero").Value as bool?;
                    var kvAge = filterItem.Filters.Single(r => r.MemberName == "KvAge").Value as int?;
                    var companyType = filterItem.Filters.Single(r => r.MemberName == "CompanyType").Value as int?;
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出配件追溯码库位库龄(warehouseName, warehouseAreaCode, storehouseAreaCode, sparePartCode, traceProperty, traceCode, greaterThanZero, kvAge,companyType, loadOp =>
                    {
                        if (loadOp.HasError)
                        {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if (loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value))
                        {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if (loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                        {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId) {
                case CommonActionKeys.EDIT:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<TraceWarehouseAge>().First().SIHLabelCode!=null;
                case CommonActionKeys.EXPORT:
                    return true;
                case "ChPrint":
                     if(this.DataGridView.SelectedEntities == null)
                        return false;
                     var selectItem = this.DataGridView.SelectedEntities.Cast<TraceWarehouseAge>().ToArray();
                     return selectItem.Length == 1 && !string.IsNullOrEmpty(selectItem.First().BoxCode);
                case "PcPrint":
                     if(this.DataGridView.SelectedEntities == null)
                        return false;
                     var selectItems = this.DataGridView.SelectedEntities.Cast<TraceWarehouseAge>().ToArray();
                     return selectItems.Length == 1 && !string.IsNullOrEmpty( selectItems.First().SIHLabelCode);
                default: return false;
            }
        }
    }
}
