﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report {
    [PageMeta("Report", "SparePart", "TraceWarehouseOutCenterReport", ActionPanelKeys = new[] {
         CommonActionKeys.EXPORT
    })]
    public class TraceWarehouseOutCenterReport: DcsDataManagementViewBase
    {
        private DataGridViewBase dataGridView;
        public TraceWarehouseOutCenterReport()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = "中心库配件出库精确码物流跟踪";
        }
        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("TraceWarehouseOutCenter"));
            }
        }
        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "TraceWarehouseOutCenter"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem)
        {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId) {
               
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if (filterItem == null)
                        return;
                    var sparePartCode = filterItem.Filters.Single(e => e.MemberName == "SparePartCode").Value as string;
                    var outCode = filterItem.Filters.Single(r => r.MemberName == "OutCode").Value as string;
                    var traceCode = filterItem.Filters.Single(r => r.MemberName == "TraceCode").Value as string;
                    var warehouseName = filterItem.Filters.Single(r => r.MemberName == "WarehouseName").Value as string;
                    var originalRequirementBillCode = filterItem.Filters.Single(r => r.MemberName == "OriginalRequirementBillCode").Value as string;
                    var counterpartCompanyCode = filterItem.Filters.Single(r => r.MemberName == "CounterpartCompanyCode").Value as string;
                    DateTime? bOutTime = null;
                    DateTime? eOutTime = null;
                    foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                        var dateTime = filter as CompositeFilterItem;
                        if(dateTime != null) {
                            if(dateTime.Filters.First().MemberName == "OutTime") {
                                bOutTime = dateTime.Filters.First(r => r.MemberName == "OutTime").Value as DateTime?;
                                eOutTime = dateTime.Filters.Last(r => r.MemberName == "OutTime").Value as DateTime?;
                            }                           

                        }
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出中心库配件出库精确码物流跟踪(warehouseName, sparePartCode, traceCode, originalRequirementBillCode, outCode, counterpartCompanyCode, bOutTime, eOutTime, loadOp =>
                    {
                        if (loadOp.HasError)
                        {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if (loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value))
                        {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if (loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                        {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId)
            {
                case CommonActionKeys.EXPORT:
                    return true;              
                default: return false;
            }
        }
    }
}