﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report {
      [PageMeta("Report", "SparePart", "TraceWarehouseOutSIHReport", ActionPanelKeys = new[] {
         CommonActionKeys.EXPORT
    })]
    public class TraceWarehouseOutSIHReport : DcsDataManagementViewBase
    {
        private DataGridViewBase dataGridView;
        public TraceWarehouseOutSIHReport()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = "配件出库精确码物流跟踪";
        }
        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("TraceWarehouseOutSIHReport"));
            }
        }
        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "TraceWarehouseOutSIHReport"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem)
        {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId) {
               
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if (filterItem == null)
                        return;
                    var sparePartCode = filterItem.Filters.Single(e => e.MemberName == "SparePartCode").Value as string;
                    var outCode = filterItem.Filters.Single(r => r.MemberName == "OutCode").Value as string;
                    var traceCode = filterItem.Filters.Single(r => r.MemberName == "TraceCode").Value as string;
                    var outboundType = filterItem.Filters.Single(r => r.MemberName == "OutboundType").Value as int?;
                    var warehouseId = filterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                    var status = filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                    var originalRequirementBillCode = filterItem.Filters.Single(r => r.MemberName == "OriginalRequirementBillCode").Value as string;
                    var counterpartCompanyName = filterItem.Filters.Single(r => r.MemberName == "CounterpartCompanyName").Value as string;
                    DateTime? bCreatetime = null;
                    DateTime? eCreatetime = null;
                    foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                        var dateTime = filter as CompositeFilterItem;
                        if(dateTime != null) {

                            if(dateTime.Filters.First().MemberName == "OutTime") {
                                bCreatetime = dateTime.Filters.First(r => r.MemberName == "OutTime").Value as DateTime?;
                                eCreatetime = dateTime.Filters.Last(r => r.MemberName == "OutTime").Value as DateTime?;
                            }
                        }
                    }
                    
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出配件出库精确码物流跟踪(warehouseId, sparePartCode, traceCode, originalRequirementBillCode, outCode, counterpartCompanyName, outboundType, status,bCreatetime,eCreatetime, loadOp =>
                    {
                        if (loadOp.HasError)
                        {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if (loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value))
                        {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if (loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                        {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId)
            {
                case CommonActionKeys.EXPORT:
                    return true;              
                default: return false;
            }
        }
    }
}