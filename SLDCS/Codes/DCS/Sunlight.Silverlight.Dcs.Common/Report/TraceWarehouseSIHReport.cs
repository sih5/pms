﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Report {
     [PageMeta("Report", "SparePart", "TraceWarehouseSIHReport", ActionPanelKeys = new[] {
         CommonActionKeys.EXPORT
    })]
    public class TraceWarehouseSIHReport: DcsDataManagementViewBase
    {
        private DataGridViewBase dataGridView;
        public TraceWarehouseSIHReport()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = "配件入库精确码物流跟踪";
        }
        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("TraceWarehouseSIHReport"));
            }
        }
        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "TraceWarehouseSIHReport"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem)
        {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId) {
               
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if (filterItem == null)
                        return;
                    var sparePartCode = filterItem.Filters.Single(e => e.MemberName == "SparePartCode").Value as string;
                    var status = filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                    var traceCode = filterItem.Filters.Single(r => r.MemberName == "TraceCode").Value as string;
                    var counterpartCompanyCode = filterItem.Filters.Single(r => r.MemberName == "CounterpartCompanyCode").Value as string;
                    var counterpartCompanyName = filterItem.Filters.Single(r => r.MemberName == "CounterpartCompanyName").Value as string;
                    var sIHLabelCode = filterItem.Filters.Single(r => r.MemberName == "SIHLabelCode").Value as string;
                    var packingTaskCode = filterItem.Filters.Single(r => r.MemberName == "PackingTaskCode").Value as string;
                    var warehouseId = filterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                    var originalRequirementBillCode = filterItem.Filters.Single(r => r.MemberName == "OriginalRequirementBillCode").Value as string;
                    DateTime? bInBoundDate = null;
                    DateTime? eInBoundDate = null;
                    foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                        var dateTime = filter as CompositeFilterItem;
                        if(dateTime != null) {
                            if(dateTime.Filters.First().MemberName == "InBoundDate") {
                                bInBoundDate = dateTime.Filters.First(r => r.MemberName == "InBoundDate").Value as DateTime?;
                                eInBoundDate = dateTime.Filters.Last(r => r.MemberName == "InBoundDate").Value as DateTime?;
                            }                           

                        }
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出配件入库精确码物流跟踪(sparePartCode, counterpartCompanyCode, traceCode, counterpartCompanyName, sIHLabelCode, bInBoundDate, eInBoundDate, packingTaskCode, warehouseId, status, originalRequirementBillCode, loadOp =>
                    {
                        if (loadOp.HasError)
                        {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if (loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value))
                        {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if (loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                        {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId)
            {
                case CommonActionKeys.EXPORT:
                    return true;              
                default: return false;
            }
        }
    }
}
