﻿using System.Linq;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs.Common {
    public class UIStrings : ModelBase {
        public void Refresh(params string[] propertyNames) {
            this.NotifyOfPropertiesChange(propertyNames.Length == 0 ? this.GetType().GetProperties().Select(p => p.Name).ToArray() : propertyNames);
        }

        public string Action_Title_InitialApprove {
            get {
                return Resources.CommonUIStrings.Action_Title_InitialApprove;
            }
        }

        public string DataEditPanel_GroupTitle_BasicInfo {
            get {
                return Resources.CommonUIStrings.DataEditPanel_GroupTitle_BasicInfo;
            }
        }

        public string DataEditPanel_GroupTitle_CompanyInvoiceInfo {
            get {
                return Resources.CommonUIStrings.DataEditPanel_GroupTitle_CompanyInvoiceInfo;
            }
        }

        public string DataEditPanel_GroupTitle_CompanyRegisterInfo {
            get {
                return Resources.CommonUIStrings.DataEditPanel_GroupTitle_CompanyRegisterInfo;
            }
        }

        public string DataEditPanel_Text_Common_Unit_Currency {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Common_Unit_Currency;
            }
        }

        public string DataEditPanel_Text_Common_Unit_MyraidCurrency {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Common_Unit_MyraidCurrency;
            }
        }

        public string DataEditPanel_Text_CompanyInvoiceInfo_BankAccount {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_CompanyInvoiceInfo_BankAccount;
            }
        }

        public string DataEditPanel_Text_CompanyInvoiceInfo_TaxRegisteredAddress {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_CompanyInvoiceInfo_TaxRegisteredAddress;
            }
        }

        public string DataEditPanel_Text_CompanyInvoiceInfo_TaxRegisteredPhone {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_CompanyInvoiceInfo_TaxRegisteredPhone;
            }
        }

        public string DataEditPanel_Text_PartsBranch_IsDirectSupply {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_PartsBranch_IsDirectSupply;
            }
        }

        public string DataEditPanel_Text_PartsBranch_IsManagedByBatch {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_PartsBranch_IsManagedByBatch;
            }
        }

        public string DataEditPanel_Text_PartsBranch_IsManagedBySerial {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_PartsBranch_IsManagedBySerial;
            }
        }

        public string DataEditPanel_Text_PartsBranch_IsOrderable {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_PartsBranch_IsOrderable;
            }
        }

        public string DataEditPanel_Text_PartsBranch_IsSalable {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_PartsBranch_IsSalable;
            }
        }

        public string DataEditPanel_Text_PartsBranch_IsService {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_PartsBranch_IsService;
            }
        }

        public string DataEditPanel_Text_PartsBranch_PartsWarrantyCategoryName {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_PartsBranch_PartsWarrantyCategoryName;
            }
        }

        public string DataEditPanel_Text_PartsReplacement_Attention {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_PartsReplacement_Attention;
            }
        }

        public string DataEditPanel_Text_PartsReplacement_AttentionRepeatNotify {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_PartsReplacement_AttentionRepeatNotify;
            }
        }

        public string DataEditPanel_Text_PartsReplacement_AttentionSamePartsNotify {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_PartsReplacement_AttentionSamePartsNotify;
            }
        }

        public string DataEditPanel_Text_PartsSupplierRelation_IsPrimary {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_PartsSupplierRelation_IsPrimary;
            }
        }

        public string DataEditPanel_Text_Region_City {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Region_City;
            }
        }

        public string DataEditPanel_Text_Region_District {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Region_District;
            }
        }

        public string DataEditPanel_Text_Region_Province {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Region_Province;
            }
        }

        public string DataEditPanel_Text_Region_Region {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Region_Region;
            }
        }

        public string DataEditPanel_Text_SparePart_Feature {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SparePart_Feature;
            }
        }

        public string DataEditPanel_Text_SparePart_IsManagedBySerial {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SparePart_IsManagedBySerial;
            }
        }

        public string DataEditPanel_Text_SparePart_SparePart_IsManagedByBatch {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SparePart_SparePart_IsManagedByBatch;
            }
        }

        public string DataEditPanel_Title_PartsWarrantyCategory {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Title_PartsWarrantyCategory;
            }
        }

        public string DataEditView_GroupTitle_BillingInformation {
            get {
                return Resources.CommonUIStrings.DataEditView_GroupTitle_BillingInformation;
            }
        }

        public string DataEditView_GroupTitle_GeneralInformation {
            get {
                return Resources.CommonUIStrings.DataEditView_GroupTitle_GeneralInformation;
            }
        }

        public string DataEditView_Text_Branch_Code {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_Branch_Code;
            }
        }

        public string DataEditView_Text_Branch_Name {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_Branch_Name;
            }
        }

        public string DataEditView_Text_CompanyInvoiceInfo_IsPartsSalesInvoice {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_CompanyInvoiceInfo_IsPartsSalesInvoice;
            }
        }

        public string DataEditView_Text_CompanyInvoiceInfo_IsVehicleSalesInvoice {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_CompanyInvoiceInfo_IsVehicleSalesInvoice;
            }
        }

        public string DataEditView_Text_ResponsibleUnitProductDetail_ResponsibleUnit {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_ResponsibleUnitProductDetail_ResponsibleUnit;
            }
        }

        public string DetailPanel_GroupTitle_BasicInfo {
            get {
                return Resources.CommonUIStrings.DetailPanel_GroupTitle_BasicInfo;
            }
        }

        public string DetailPanel_GroupTitle_CompanyRegisterInfo {
            get {
                return Resources.CommonUIStrings.DetailPanel_GroupTitle_CompanyRegisterInfo;
            }
        }

        public string DetailPanel_Text_Common_Unit_MyraidCurrency {
            get {
                return Resources.CommonUIStrings.DetailPanel_Text_Common_Unit_MyraidCurrency;
            }
        }

        public string DetailPanel_Text_PartsBranch_PartsWarrantyCategoryName {
            get {
                return Resources.CommonUIStrings.DetailPanel_Text_PartsBranch_PartsWarrantyCategoryName;
            }
        }

        public string DetailPanel_Text_Region_City {
            get {
                return Resources.CommonUIStrings.DetailPanel_Text_Region_City;
            }
        }

        public string DetailPanel_Text_Region_District {
            get {
                return Resources.CommonUIStrings.DetailPanel_Text_Region_District;
            }
        }

        public string DetailPanel_Text_Region_Province {
            get {
                return Resources.CommonUIStrings.DetailPanel_Text_Region_Province;
            }
        }

        public string DetailPanel_Text_Region_Region {
            get {
                return Resources.CommonUIStrings.DetailPanel_Text_Region_Region;
            }
        }

        public string QueryPanel_QueryItem_Title_BranchForCurrent_Name {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_BranchForCurrent_Name;
            }
        }

        public string QueryPanel_QueryItem_Title_Branch_Code {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_Branch_Code;
            }
        }

        public string QueryPanel_QueryItem_Title_TiledRegion_Name {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_TiledRegion_Name;
            }
        }

        public string QueryPanel_Title_Company {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Company;
            }
        }

        public string QueryPanel_Title_PartsSupplier {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_PartsSupplier;
            }
        }

        public string QueryPanel_Title_SparePart {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_SparePart;
            }
        }

        public string QueryPanel_Title_TiledRegion {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_TiledRegion;
            }
        }

        public string Query_Title_Company {
            get {
                return Resources.CommonUIStrings.Query_Title_Company;
            }
        }

        public string DetailPanel_Text_Company_ContactPostCode {
            get {
                return Resources.CommonUIStrings.DetailPanel_Text_Company_ContactPostCode;
            }
        }

        public string DetailPanel_Text_Company_LegalRepresentative {
            get {
                return Resources.CommonUIStrings.DetailPanel_Text_Company_LegalRepresentative;
            }
        }

        public string Detail_Text_Company_ContactMail {
            get {
                return Resources.CommonUIStrings.Detail_Text_Company_ContactMail;
            }
        }

        public string DataEditPanel_Text_Company_LegalRepresentative {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Company_LegalRepresentative;
            }
        }

        public string DetailPanel_Text_Company_ContactAddress {
            get {
                return Resources.CommonUIStrings.DetailPanel_Text_Company_ContactAddress;
            }
        }

        public string DataEditPanel_Text_Company_ContactAddress {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Company_ContactAddress;
            }
        }

        public string DataEditPanel_Text_Company_ContactMail {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Company_ContactMail;
            }
        }

        public string DataEditPanel_Text_Company_ContactPostCode {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Company_ContactPostCode;
            }
        }

        public string Action_Title_Audit {
            get {
                return Resources.CommonUIStrings.Action_Title_Audit;
            }
        }

        public string DataEditPanel_Text_MarketingDepartment_Branch_Name {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_MarketingDepartment_Branch_Name;
            }
        }

        public string DataEditPanel_Text_MarketingDepartment_Code {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_MarketingDepartment_Code;
            }
        }

        public string DataEditPanel_Text_MarketingDepartment_Name {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_MarketingDepartment_Name;
            }
        }

        public string DataGridView_ColumnItem_Title_Branch {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Title_Branch;
            }
        }

        public string DataEditView_PartsPurchasePricingChange_Text_PartsSalesCategoryName {
            get {
                return Resources.CommonUIStrings.DataEditView_PartsPurchasePricingChange_Text_PartsSalesCategoryName;
            }
        }

        public string DataEditPanel_Text_PartsBranch_PartsSalesCategory {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_PartsBranch_PartsSalesCategory;
            }
        }

        public string DataEditPanel_Text_PartsBranch_WarrantySupplyStatus {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_PartsBranch_WarrantySupplyStatus;
            }
        }

        public string QueryPanel_Title_PartsWarrantyCategoryCode {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_PartsWarrantyCategoryCode;
            }
        }

        public string DataEditView_Text_BranchSupplierRelation_BranchCode {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_BranchSupplierRelation_BranchCode;
            }
        }

        public string DataEditView_Text_BranchSupplierRelation_BranchName {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_BranchSupplierRelation_BranchName;
            }
        }

        public string DataGridView_Title_Common_Suppliers {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_Common_Suppliers;
            }
        }

        public string DetailPanel_Text_PartsBranch_PartsSalesCategoryName {
            get {
                return Resources.CommonUIStrings.DetailPanel_Text_PartsBranch_PartsSalesCategoryName;
            }
        }

        public string DetailPanel_Title_PartsSalesCategoryName {
            get {
                return Resources.CommonUIStrings.DetailPanel_Title_PartsSalesCategoryName;
            }
        }

        public string DetailPanel_Title_ServiceProductLine_BranchName {
            get {
                return Resources.CommonUIStrings.DetailPanel_Title_ServiceProductLine_BranchName;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsSalesCategory_Name {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategory_Name;
            }
        }

        public string DataManagementView_Title_EngineProductLine {
            get {
                return Resources.CommonUIStrings.DataManagementView_Title_EngineProductLine;
            }
        }

        public string QueryPanel_Title_EngineProductLine {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_EngineProductLine;
            }
        }

        public string QueryPanel_Title_EngineProductLine_PartsSalesCategory {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_EngineProductLine_PartsSalesCategory;
            }
        }

        public string DataEditView_Validation_EngineProductLine_EngineCodeIsNull {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_EngineProductLine_EngineCodeIsNull;
            }
        }

        public string DataEditView_Validation_EngineProductLine_EngineNameIsNull {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_EngineProductLine_EngineNameIsNull;
            }
        }

        public string QueryPanel_Title_EngineModel {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_EngineModel;
            }
        }

        public string QueryPanel_Title_EngineModel_BranchName {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_EngineModel_BranchName;
            }
        }

        public string DataEditView_Text_EngineProductLine_PartsSalesCategoryName {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_EngineProductLine_PartsSalesCategoryName;
            }
        }

        public string DataEditPanel_Text_EngineProductLine_EngineCode {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_EngineProductLine_EngineCode;
            }
        }

        public string DataEditPanel_Text_PartsSalesCategory {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_PartsSalesCategory;
            }
        }

        public string DataEditPanel_Text_EngineProductLine_EngineName {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_EngineProductLine_EngineName;
            }
        }

        public string QueryPanel_Title_FaultyParts_SupplierName {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_FaultyParts_SupplierName;
            }
        }

        public string DataEditView_Text_TemporarySupplier_Code {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_TemporarySupplier_Code;
            }
        }

        public string DataEditView_Text_TemporarySupplier_Name {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_TemporarySupplier_Name;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrder_SalesCategoryName {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_PartsSalesOrder_SalesCategoryName;
            }
        }

        public string DataEditView_SalesRegion_Code {
            get {
                return Resources.CommonUIStrings.DataEditView_SalesRegion_Code;
            }
        }

        public string DataEditView_SalesRegion_Name {
            get {
                return Resources.CommonUIStrings.DataEditView_SalesRegion_Name;
            }
        }

        public string EditPanel_Text_SalesRegionRegionCode {
            get {
                return Resources.CommonUIStrings.EditPanel_Text_SalesRegionRegionCode;
            }
        }

        public string EditPanel_Text_SalesRegionRegionName {
            get {
                return Resources.CommonUIStrings.EditPanel_Text_SalesRegionRegionName;
            }
        }

        public string DataGridView_ColumnItem_Title_SalesRegion_PartsSalesCategoryName {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Title_SalesRegion_PartsSalesCategoryName;
            }
        }

        public string QueryPanel_Title_SalesRegion {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_SalesRegion;
            }
        }

        public string QueryPanel_QueryItem_Title_SalesRegion_RegionCode {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_SalesRegion_RegionCode;
            }
        }

        public string QueryPanel_QueryItem_Title_SalesRegion_RegionName {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_SalesRegion_RegionName;
            }
        }

        public string QueryPanel_QueryItem_Title_SalesRegion_PartsSalesCategoryName {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_SalesRegion_PartsSalesCategoryName;
            }
        }

        public string DataEditPanel_Text_SalesRegion_RegionCode {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SalesRegion_RegionCode;
            }
        }

        public string DataEditPanel_Text_SalesRegion_RegionName {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SalesRegion_RegionName;
            }
        }

        public string DataGridView_ColumnItem_Title_SalesRegion_RegionCode {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Title_SalesRegion_RegionCode;
            }
        }

        public string DataGridView_ColumnItem_Title_SalesRegion_RegionName {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Title_SalesRegion_RegionName;
            }
        }

        public string DataEditPanel_CompanyLoginPicture_Image {
            get {
                return Resources.CommonUIStrings.DataEditPanel_CompanyLoginPicture_Image;
            }
        }

        public string DataEditPanel_CompanyLoginPicture_Imformation {
            get {
                return Resources.CommonUIStrings.DataEditPanel_CompanyLoginPicture_Imformation;
            }
        }

        public string DataEditView_Error_PersonSubDealer_PersonnelName {
            get {
                return Resources.CommonUIStrings.DataEditView_Error_PersonSubDealer_PersonnelName;
            }
        }

        public string Action_Title_Serviceing {
            get {
                return Resources.CommonUIStrings.Action_Title_Serviceing;
            }
        }

        public string DataEditView_Title_PartsBranchForServiceing {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PartsBranchForServiceing;
            }
        }

        public string DataManagementView_Notification_AvailibleTimeNotNull {
            get {
                return Resources.CommonUIStrings.DataManagementView_Notification_AvailibleTimeNotNull;
            }
        }

        public string DetailPanel_Title_SparePartHistory {
            get {
                return Resources.CommonUIStrings.DetailPanel_Title_SparePartHistory;
            }
        }

        public string DataEditPanel_SparePart_BasicInformation {
            get {
                return Resources.CommonUIStrings.DataEditPanel_SparePart_BasicInformation;
            }
        }

        public string DataEditPanel_Text_SparePart_CADCode {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SparePart_CADCode;
            }
        }

        public string DataEditPanel_Text_SparePart_ShelfLife {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SparePart_ShelfLife;
            }
        }

        public string DataGridView_ColumnItem_Title_SparePart_CADCode {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Title_SparePart_CADCode;
            }
        }

        public string DataGridView_ColumnItem_Title_SparePart_Feature {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Title_SparePart_Feature;
            }
        }

        public string DataGridView_ColumnItem_Title_SparePart_ShelfLife {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Title_SparePart_ShelfLife;
            }
        }

        public string DataEditPanel_Text_PartsSupplier_SupplierType {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_PartsSupplier_SupplierType;
            }
        }

        public string CompanyInvoiceInfo_BankName {
            get {
                return Resources.CommonUIStrings.CompanyInvoiceInfo_BankName;
            }
        }

        public string CompanyInvoiceInfo_TaxRegisteredNumber {
            get {
                return Resources.CommonUIStrings.CompanyInvoiceInfo_TaxRegisteredNumber;
            }
        }

        public string DataEditPanel_Text_BankAccount {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_BankAccount;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsBranchHistory_IsDirectSupply {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranchHistory_IsDirectSupply;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsBranchHistory_IsOrderable {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranchHistory_IsOrderable;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsBranchHistory_IsSalable {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranchHistory_IsSalable;
            }
        }

        public string Notification_ContentDetail {
            get {
                return Resources.CommonUIStrings.Notification_ContentDetail;
            }
        }

        public string DataEditView_Notification_NotificationLimit {
            get {
                return Resources.CommonUIStrings.DataEditView_Notification_NotificationLimit;
            }
        }

        public string DetailPanel_GroupTitle_Attachment {
            get {
                return Resources.CommonUIStrings.DetailPanel_GroupTitle_Attachment;
            }
        }

        public string DataEditView_Notification_Type_Agency {
            get {
                return Resources.CommonUIStrings.DataEditView_Notification_Type_Agency;
            }
        }

        public string DataEditView_Notification_Type_Branch {
            get {
                return Resources.CommonUIStrings.DataEditView_Notification_Type_Branch;
            }
        }

        public string DataEditView_Notification_Type_Company {
            get {
                return Resources.CommonUIStrings.DataEditView_Notification_Type_Company;
            }
        }

        public string DataEditView_Notification_Type_CompanyOrAgency {
            get {
                return Resources.CommonUIStrings.DataEditView_Notification_Type_CompanyOrAgency;
            }
        }

        public string DataEditView_Notification_Type_LogisticCompany {
            get {
                return Resources.CommonUIStrings.DataEditView_Notification_Type_LogisticCompany;
            }
        }

        public string DataEditView_Notification_Type_PartsSupplier {
            get {
                return Resources.CommonUIStrings.DataEditView_Notification_Type_PartsSupplier;
            }
        }

        public string DataEditView_Notification_Type_ResponsibleUnit {
            get {
                return Resources.CommonUIStrings.DataEditView_Notification_Type_ResponsibleUnit;
            }
        }

        public string DataEditView_Notification_Top {
            get {
                return Resources.CommonUIStrings.DataEditView_Notification_Top;
            }
        }

        public string DataEditPanel_Text_Company_ContactMobile {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Company_ContactMobile;
            }
        }

        public string DataEditPanel_Text_Company_CustomerCode {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Company_CustomerCode;
            }
        }

        public string DataEditPanel_Text_Company_LegalRepresentatives {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Company_LegalRepresentatives;
            }
        }

        public string DataEditPanel_Text_Company_RegisteredAddress {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Company_RegisteredAddress;
            }
        }

        public string DataEditPanel_Text_Company_SupplierCode {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Company_SupplierCode;
            }
        }

        public string DataEditPanel_Text_PartsExchange_ExchangeCode {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_PartsExchange_ExchangeCode;
            }
        }

        public string DataEditPanel_Text_PartsExchange_ExchangeName {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_PartsExchange_ExchangeName;
            }
        }

        public string DataEditPanel_Text_Button_AddPartsExchange {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Button_AddPartsExchange;
            }
        }

        public string DataEditPanel_Text_Button_UpdatePartsExchange {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Button_UpdatePartsExchange;
            }
        }

        public string DetailPanel_GroupTitle_Image {
            get {
                return Resources.CommonUIStrings.DetailPanel_GroupTitle_Image;
            }
        }

        public string DetailPanel_GroupTitle_File {
            get {
                return Resources.CommonUIStrings.DetailPanel_GroupTitle_File;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsBranchExtend_PartsWarrantyCategoryName {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranchExtend_PartsWarrantyCategoryName;
            }
        }

        public string DataEditView_Title_MessageTextBlock {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_MessageTextBlock;
            }
        }

        public string DataEditView_Title_MessageTextBox {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_MessageTextBox;
            }
        }

        public string QueryPanel_Title_ProductCode {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_ProductCode;
            }
        }

        public string DataGridView_ColumnItem_Title_Product_BrandCode {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Title_Product_BrandCode;
            }
        }

        public string DataGridView_ColumnItem_Title_Product_BrandName {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Title_Product_BrandName;
            }
        }

        public string DataGridView_ColumnItem_Title_Product_Code {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Title_Product_Code;
            }
        }

        public string DataGridView_ColumnItem_Title_Product_ProductLine {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Title_Product_ProductLine;
            }
        }

        public string DataGridView_ColumnItem_Title_Product_ProductName {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Title_Product_ProductName;
            }
        }

        public string DataGridView_ColumnItem_Title_Product_SubBrandCode {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Title_Product_SubBrandCode;
            }
        }

        public string DataGridView_ColumnItem_Title_Product_SubBrandName {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Title_Product_SubBrandName;
            }
        }

        public string DataGridView_ColumnItem_Title_Product_TerraceCode {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Title_Product_TerraceCode;
            }
        }

        public string DataGridView_ColumnItem_Title_Product_TerraceName {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Title_Product_TerraceName;
            }
        }

        public string QueryPanel_QueryItem_Title_Product_BrandName {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_Product_BrandName;
            }
        }

        public string QueryPanel_QueryItem_Title_Product_Code {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_Product_Code;
            }
        }

        public string QueryPanel_QueryItem_Title_Product_ProductLine {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_Product_ProductLine;
            }
        }

        public string QueryPanel_QueryItem_Title_Product_ProductName {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_Product_ProductName;
            }
        }

        public string QueryPanel_QueryItem_Title_Product_SubBrandName {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_Product_SubBrandName;
            }
        }

        public string QueryPanel_QueryItem_Title_Product_TerraceName {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_Product_TerraceName;
            }
        }

        public string DataEditView_Text_ServiceProdLineProduct_SalesCategoryCode {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_ServiceProdLineProduct_SalesCategoryCode;
            }
        }

        public string DataEditView_Text_ServiceProdLineProduct_SalesCategoryName {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_ServiceProdLineProduct_SalesCategoryName;
            }
        }

        public string DataEditView_Validation_PartsPurchasePricingChange_PartsSalesCategoryNameIsNull {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_PartsPurchasePricingChange_PartsSalesCategoryNameIsNull;
            }
        }

        public string QueryPanel_QueryItem_Title_OverstockPartsStock_CityName {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_CityName;
            }
        }

        public string QueryPanel_QueryItem_Title_OverstockPartsStock_CountyName {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_CountyName;
            }
        }

        public string QueryPanel_QueryItem_Title_OverstockPartsStock_ProvinceName {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_ProvinceName;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsSupplierRelation_SupplierPartCode {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Title_PartsSupplierRelation_SupplierPartCode;
            }
        }

        public string Action_Title_MVSRoute {
            get {
                return Resources.CommonUIStrings.Action_Title_MVSRoute;
            }
        }

        public string DataEditPanel_Text_Address {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Address;
            }
        }

        public string DataEditView_Validation_DetailCountMoreThan400 {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_DetailCountMoreThan400;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsBranch_IRGroupCode {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_IRGroupCode;
            }
        }

        public string QueryPanel_Title_IncreaseRateGroup {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_IncreaseRateGroup;
            }
        }

        public string DataEditView_Validation_PartsSupplierRelation_DefaultOrderWarehouseIsNull {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_PartsSupplierRelation_DefaultOrderWarehouseIsNull;
            }
        }

        public string DataEditView_Validation_PartsSupplierRelation_OrderCycleIsNull {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_PartsSupplierRelation_OrderCycleIsNull;
            }
        }

        public string DataGridView_ColumnItem_Title_EngineModel_IsSetRelation {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Title_EngineModel_IsSetRelation;
            }
        }

        public string DataEditView_Error_PartsBranch_EnglishNameIsNull {
            get {
                return Resources.CommonUIStrings.DataEditView_Error_PartsBranch_EnglishNameIsNull;
            }
        }

        public string DataEditView_Validation_ResponsibleUnit_CodeIsOverLength {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_ResponsibleUnit_CodeIsOverLength;
            }
        }

        public string DataEditView_Validation_ResponsibleUnit_CodeIsNull_Branch {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_ResponsibleUnit_CodeIsNull_Branch;
            }
        }

        public string DataEditView_Validation_ResponsibleUnit_CodeIsNull_Logistic {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_ResponsibleUnit_CodeIsNull_Logistic;
            }
        }

        public string DataEditView_Validation_ResponsibleUnit_CodeIsOverLength_Agency {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_ResponsibleUnit_CodeIsOverLength_Agency;
            }
        }

        public string DataEditView_Validation_ResponsibleUnit_CodeIsOverLength_Branch {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_ResponsibleUnit_CodeIsOverLength_Branch;
            }
        }

        public string DataEditView_Validation_ResponsibleUnit_CodeIsOverLength_Logistic {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_ResponsibleUnit_CodeIsOverLength_Logistic;
            }
        }

        public string DataEditView_Validation_ResponsibleUnit_NameIsNull_Branch {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_ResponsibleUnit_NameIsNull_Branch;
            }
        }

        public string DataEditView_Validation_ResponsibleUnit_NameIsNull_Logistic {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_ResponsibleUnit_NameIsNull_Logistic;
            }
        }

        public string DataEditView_Validation_ResponsibleUnit_StorageCenterIsNull_Logistic {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_ResponsibleUnit_StorageCenterIsNull_Logistic;
            }
        }

        public string QueryPanel_QueryItem_Title_LogisticCompany_StorageCenter {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_LogisticCompany_StorageCenter;
            }
        }

        public string DataEditView_ProductStandard_StandardCode {
            get {
                return Resources.CommonUIStrings.DataEditView_ProductStandard_StandardCode;
            }
        }

        public string DataEditView_ProductStandard_StandardName {
            get {
                return Resources.CommonUIStrings.DataEditView_ProductStandard_StandardName;
            }
        }

        public string DataEditView_Validation_ProductStandard_StandardCodeIsNull_CategoryIsNull {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_ProductStandard_StandardCodeIsNull_CategoryIsNull;
            }
        }

        public string Notification_ReplyContent {
            get {
                return Resources.CommonUIStrings.Notification_ReplyContent;
            }
        }

        public string Action_Title_Batch_Recovery {
            get {
                return Resources.CommonUIStrings.Action_Title_Batch_Recovery;
            }
        }

        public string Action_Title_Batch_Stop {
            get {
                return Resources.CommonUIStrings.Action_Title_Batch_Stop;
            }
        }

        public string Action_Title_Import_Edit {
            get {
                return Resources.CommonUIStrings.Action_Title_Import_Edit;
            }
        }

        public string CompanyInvoiceInfo_DocumentType {
            get {
                return Resources.CommonUIStrings.CompanyInvoiceInfo_DocumentType;
            }
        }

        public string DataDeTailPanel_Text_BottomStock_AbandonerName {
            get {
                return Resources.CommonUIStrings.DataDeTailPanel_Text_BottomStock_AbandonerName;
            }
        }

        public string DataDeTailPanel_Text_BottomStock_AbandonerTime {
            get {
                return Resources.CommonUIStrings.DataDeTailPanel_Text_BottomStock_AbandonerTime;
            }
        }

        public string DataDeTailPanel_Text_BottomStock_CompanyType {
            get {
                return Resources.CommonUIStrings.DataDeTailPanel_Text_BottomStock_CompanyType;
            }
        }

        public string DataDeTailPanel_Text_BottomStock_CreateTime {
            get {
                return Resources.CommonUIStrings.DataDeTailPanel_Text_BottomStock_CreateTime;
            }
        }

        public string DataDeTailPanel_Text_BottomStock_CreatorName {
            get {
                return Resources.CommonUIStrings.DataDeTailPanel_Text_BottomStock_CreatorName;
            }
        }

        public string DataDeTailPanel_Text_BottomStock_ModifierName {
            get {
                return Resources.CommonUIStrings.DataDeTailPanel_Text_BottomStock_ModifierName;
            }
        }

        public string DataDeTailPanel_Text_BottomStock_ModifierTime {
            get {
                return Resources.CommonUIStrings.DataDeTailPanel_Text_BottomStock_ModifierTime;
            }
        }

        public string DataDeTailPanel_Text_BottomStock_Remark {
            get {
                return Resources.CommonUIStrings.DataDeTailPanel_Text_BottomStock_Remark;
            }
        }

        public string DataDeTailPanel_Text_BottomStock_SparePartCode {
            get {
                return Resources.CommonUIStrings.DataDeTailPanel_Text_BottomStock_SparePartCode;
            }
        }

        public string DataDeTailPanel_Text_BottomStock_SparePartName {
            get {
                return Resources.CommonUIStrings.DataDeTailPanel_Text_BottomStock_SparePartName;
            }
        }

        public string DataDeTailPanel_Text_BottomStock_Status {
            get {
                return Resources.CommonUIStrings.DataDeTailPanel_Text_BottomStock_Status;
            }
        }

        public string DataDeTailPanel_Text_BottomStock_StockQty {
            get {
                return Resources.CommonUIStrings.DataDeTailPanel_Text_BottomStock_StockQty;
            }
        }

        public string DataDeTailPanel_Text_Company_Code {
            get {
                return Resources.CommonUIStrings.DataDeTailPanel_Text_Company_Code;
            }
        }

        public string DataDeTailPanel_Text_Company_Name {
            get {
                return Resources.CommonUIStrings.DataDeTailPanel_Text_Company_Name;
            }
        }

        public string DataDeTailPanel_Text_Company_WarehouseCode {
            get {
                return Resources.CommonUIStrings.DataDeTailPanel_Text_Company_WarehouseCode;
            }
        }

        public string DataDeTailPanel_Text_Company_WarehouseName {
            get {
                return Resources.CommonUIStrings.DataDeTailPanel_Text_Company_WarehouseName;
            }
        }

        public string DataDeTailPanel_Text_PartsBranch_BreakTime {
            get {
                return Resources.CommonUIStrings.DataDeTailPanel_Text_PartsBranch_BreakTime;
            }
        }

        public string DataDeTailPanel_Text_PartsBranch_IsAccreditPack {
            get {
                return Resources.CommonUIStrings.DataDeTailPanel_Text_PartsBranch_IsAccreditPack;
            }
        }

        public string DataDeTailPanel_Text_PartsBranch_IsAutoCaclSaleProperty {
            get {
                return Resources.CommonUIStrings.DataDeTailPanel_Text_PartsBranch_IsAutoCaclSaleProperty;
            }
        }

        public string DataDetailPanel_Title_PartsSupplierRelation {
            get {
                return Resources.CommonUIStrings.DataDetailPanel_Title_PartsSupplierRelation;
            }
        }

        public string DataEditPanel_Text_ABCStrategy_OutBound {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_ABCStrategy_OutBound;
            }
        }

        public string DataEditPanel_Text_ABCStrategy_Time {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_ABCStrategy_Time;
            }
        }

        public string DataEditPanel_Text_ABCStrategy_To {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_ABCStrategy_To;
            }
        }

        public string DataEditPanel_Text_Agency_Action_Review {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Agency_Action_Review;
            }
        }

        public string DataEditPanel_Text_Agency_Code {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Agency_Code;
            }
        }

        public string DataEditPanel_Text_Agency_Name {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Agency_Name;
            }
        }

        public string DataEditPanel_Text_Company_Address {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Company_Address;
            }
        }

        public string DataEditPanel_Text_Company_Address_Latitude {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Company_Address_Latitude;
            }
        }

        public string DataEditPanel_Text_Company_Address_LatitudeNew {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Company_Address_LatitudeNew;
            }
        }

        public string DataEditPanel_Text_Company_Address_Longitude {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Company_Address_Longitude;
            }
        }

        public string DataEditPanel_Text_Company_Address_LongitudeNew {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Company_Address_LongitudeNew;
            }
        }

        public string DataEditPanel_Text_Company_ContactPhone {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Company_ContactPhone;
            }
        }

        public string DataEditPanel_Text_Company_ContactPhone_New {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Company_ContactPhone_New;
            }
        }

        public string DataEditPanel_Text_Company_Distance {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Company_Distance;
            }
        }

        public string DataEditPanel_Text_Company_FixedAsset {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Company_FixedAsset;
            }
        }

        public string DataEditPanel_Text_Company_LinkPhone {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Company_LinkPhone;
            }
        }

        public string DataEditPanel_Text_Company_Market {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Company_Market;
            }
        }

        public string DataEditPanel_Text_Company_Markets {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Company_Markets;
            }
        }

        public string DataEditPanel_Text_Company_MinServiceCode {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Company_MinServiceCode;
            }
        }

        public string DataEditPanel_Text_Company_MinServiceName {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Company_MinServiceName;
            }
        }

        public string DataEditPanel_Text_Company_Personel1 {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Company_Personel1;
            }
        }

        public string DataEditPanel_Text_Company_Personel1_Phone {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Company_Personel1_Phone;
            }
        }

        public string DataEditPanel_Text_Company_Personel2 {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Company_Personel2;
            }
        }

        public string DataEditPanel_Text_Company_Personel2_Email {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Company_Personel2_Email;
            }
        }

        public string DataEditPanel_Text_Company_Personel2_Phone {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Company_Personel2_Phone;
            }
        }

        public string DataEditPanel_Text_Company_QRCode {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Company_QRCode;
            }
        }

        public string DataEditPanel_Text_Company_Region {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Company_Region;
            }
        }

        public string DataEditPanel_Text_LogisticCompany_IsExpressCompany {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_LogisticCompany_IsExpressCompany;
            }
        }

        public string DataEditPanel_Text_MarketingDepartment_Codes {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_MarketingDepartment_Codes;
            }
        }

        public string DataEditPanel_Text_MarketingDepartment_Names {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_MarketingDepartment_Names;
            }
        }

        public string DataEditPanel_Text_Notification_UrgentLevel {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Notification_UrgentLevel;
            }
        }

        public string DataEditPanel_Text_PartsPurchasePricingChange_ApprovalComment {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_ApprovalComment;
            }
        }

        public string DataEditPanel_Text_PartsReplacement_NewPartCode {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_PartsReplacement_NewPartCode;
            }
        }

        public string DataEditPanel_Text_PartsReplacement_OldPartCode {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_PartsReplacement_OldPartCode;
            }
        }

        public string DataEditPanel_Text_PersonnelSupplierRelation_PersonCode {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_PersonnelSupplierRelation_PersonCode;
            }
        }

        public string DataEditPanel_Text_PersonnelSupplierRelation_PersonName {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_PersonnelSupplierRelation_PersonName;
            }
        }

        public string DataEditPanel_Text_ResponsibleUnit_Code {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_ResponsibleUnit_Code;
            }
        }

        public string DataEditPanel_Text_ResponsibleUnit_Name {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_ResponsibleUnit_Name;
            }
        }

        public string DataEditPanel_Text_SparePart_Code {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SparePart_Code;
            }
        }

        public string DataEditPanel_Text_SparePart_DeclareElement {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SparePart_DeclareElement;
            }
        }

        public string DataEditPanel_Text_SparePart_ExchangeIdentification {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SparePart_ExchangeIdentification;
            }
        }

        public string DataEditPanel_Text_SparePart_GoldenTaxClassifyCode {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SparePart_GoldenTaxClassifyCode;
            }
        }

        public string DataEditPanel_Text_SparePart_GoldenTaxClassifyName {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SparePart_GoldenTaxClassifyName;
            }
        }

        public string DataEditPanel_Text_SparePart_IsOriginal {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SparePart_IsOriginal;
            }
        }

        public string DataEditPanel_Text_SparePart_Material {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SparePart_Material;
            }
        }

        public string DataEditPanel_Text_SparePart_MInPackingAmount {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SparePart_MInPackingAmount;
            }
        }

        public string DataEditPanel_Text_SparePart_OverseasPartsFigure {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SparePart_OverseasPartsFigure;
            }
        }

        public string DataEditPanel_Text_SparePart_ReferenceCode {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SparePart_ReferenceCode;
            }
        }

        public string DataEditPanel_Title_Enclosure {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Title_Enclosure;
            }
        }

        public string DataEditPanel_Title_PersonnelSupplierRelation {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Title_PersonnelSupplierRelation;
            }
        }

        public string DataEditPanel_Title_SparePart {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Title_SparePart;
            }
        }

        public string DataEditPanel_Validation_Notification_UploadError {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Validation_Notification_UploadError;
            }
        }

        public string DataEditPanel_Validation_Notification_Upload_PiSuccess {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Validation_Notification_Upload_PiSuccess;
            }
        }

        public string DataEditPanel_Validation_Notification_Upload_Success {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Validation_Notification_Upload_Success;
            }
        }

        public string DetailPanel_GroupTitle_Delete {
            get {
                return Resources.CommonUIStrings.DetailPanel_GroupTitle_Delete;
            }
        }

        public string DetailPanel_GroupTitle_Upload {
            get {
                return Resources.CommonUIStrings.DetailPanel_GroupTitle_Upload;
            }
        }

        public string QueryPanel_QueryItem_Title_CenterApproveBill_ApproveWeek {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_CenterApproveBill_ApproveWeek;
            }
        }

        public string QueryPanel_QueryItem_Title_CenterSaleReturnStrategy_ReturnType {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_CenterSaleReturnStrategy_ReturnType;
            }
        }

        public string QueryPanel_QueryItem_Title_ClaimApproverStrategy_FeeFrom {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_ClaimApproverStrategy_FeeFrom;
            }
        }

        public string QueryPanel_QueryItem_Title_ClaimApproverStrategy_FeeTo {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_ClaimApproverStrategy_FeeTo;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsExchangeGroup_ExGroupCode {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsExchangeGroup_ExGroupCode;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsOutboundBillCentral_PartABC {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_PartABC;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsStockStatement_BeginTime {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsStockStatement_BeginTime;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsStockStatement_EndTime {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsStockStatement_EndTime;
            }
        }

        public string DataDeTailPanel_Text_Agency_CompanyQuery {
            get {
                return Resources.CommonUIStrings.DataDeTailPanel_Text_Agency_CompanyQuery;
            }
        }

        public string DataEditView_BusinessName_CenterSaleReturnStrategy {
            get {
                return Resources.CommonUIStrings.DataEditView_BusinessName_CenterSaleReturnStrategy;
            }
        }

        public string DataEditView_BusinessName_ClaimApproverStrategy {
            get {
                return Resources.CommonUIStrings.DataEditView_BusinessName_ClaimApproverStrategy;
            }
        }

        public string DataEditView_BusinessName_DealerInfo {
            get {
                return Resources.CommonUIStrings.DataEditView_BusinessName_DealerInfo;
            }
        }

        public string DataEditView_BusinessName_DealerInfoList {
            get {
                return Resources.CommonUIStrings.DataEditView_BusinessName_DealerInfoList;
            }
        }

        public string DataEditView_KeyValue_Approve {
            get {
                return Resources.CommonUIStrings.DataEditView_KeyValue_Approve;
            }
        }

        public string DataEditView_KeyValue_FinalApprove {
            get {
                return Resources.CommonUIStrings.DataEditView_KeyValue_FinalApprove;
            }
        }

        public string DataEditView_KeyValue_IntilApprove {
            get {
                return Resources.CommonUIStrings.DataEditView_KeyValue_IntilApprove;
            }
        }

        public string DataEditView_Text_CompanyAddress_Abbreviation {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_CompanyAddress_Abbreviation;
            }
        }

        public string DataEditView_Text_CompanyAddress_Address {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_CompanyAddress_Address;
            }
        }

        public string DataEditView_Text_CompanyAddress_CompanyCode {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_CompanyAddress_CompanyCode;
            }
        }

        public string DataEditView_Text_CompanyAddress_CompanyCodeQuery {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_CompanyAddress_CompanyCodeQuery;
            }
        }

        public string DataEditView_Text_CompanyAddress_DealerCode {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_CompanyAddress_DealerCode;
            }
        }

        public string DataEditView_Text_CompanyAddress_DealerName {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_CompanyAddress_DealerName;
            }
        }

        public string DataEditView_Text_CompanyAddress_Establishment {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_CompanyAddress_Establishment;
            }
        }

        public string DataEditView_Text_CompanyAddress_Fax {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_CompanyAddress_Fax;
            }
        }

        public string DataEditView_Text_CompanyAddress_LinkMan {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_CompanyAddress_LinkMan;
            }
        }

        public string DataEditView_Text_CompanyAddress_LinkManPhone {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_CompanyAddress_LinkManPhone;
            }
        }

        public string DataEditView_Title_ExportButton {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_ExportButton;
            }
        }

        public string DataEditView_Title_ImportButton {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_ImportButton;
            }
        }

        public string DataEditView_Title_ImportButtonNew {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_ImportButtonNew;
            }
        }

        public string DataEditView_Validation_CenterApprove_CenterIsnull {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_CenterApprove_CenterIsnull;
            }
        }

        public string DataEditView_Validation_CenterApprove_WeekIsnull {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_CenterApprove_WeekIsnull;
            }
        }

        public string DataEditView_Validation_CenterSaleReturnStrategy_DiscountRate {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_CenterSaleReturnStrategy_DiscountRate;
            }
        }

        public string DataEditView_Validation_CenterSaleReturnStrategy_Time {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_CenterSaleReturnStrategy_Time;
            }
        }

        public string DataEditView_Validation_CenterSaleReturnStrategy_TimeFrom {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_CenterSaleReturnStrategy_TimeFrom;
            }
        }

        public string DataEditView_Validation_CenterSaleReturnStrategy_TimeTo {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_CenterSaleReturnStrategy_TimeTo;
            }
        }

        public string DataEditView_Validation_ClaimApproverStrategy_AfterApproverStatus {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_ClaimApproverStrategy_AfterApproverStatus;
            }
        }

        public string DataEditView_Validation_ClaimApproverStrategy_Fee {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_ClaimApproverStrategy_Fee;
            }
        }

        public string DataEditView_Validation_ClaimApproverStrategy_FeeCompare {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_ClaimApproverStrategy_FeeCompare;
            }
        }

        public string DataEditView_Validation_ClaimApproverStrategy_InitialApproverStatus {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_ClaimApproverStrategy_InitialApproverStatus;
            }
        }

        public string DataEditView_Validation_CompanyAddress_CompanyId {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_CompanyAddress_CompanyId;
            }
        }

        public string DataEditView_Validation_CompanyAddress_CountyId {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_CompanyAddress_CountyId;
            }
        }

        public string DataEditView_Validation_CompanyAddress_DetailAddress {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_CompanyAddress_DetailAddress;
            }
        }

        public string DataEditView_Validation_CompanyAddress_Usage {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_CompanyAddress_Usage;
            }
        }

        public string DataEdit_Text_PartsStockStatement_DiscountRate {
            get {
                return Resources.CommonUIStrings.DataEdit_Text_PartsStockStatement_DiscountRate;
            }
        }

        public string QueryPanel_QueryItem_Title_ClaimApproverStrategy_AfterApproverStatus {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_ClaimApproverStrategy_AfterApproverStatus;
            }
        }

        public string QueryPanel_QueryItem_Title_ClaimApproverStrategy_InitialApproverStatus {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_ClaimApproverStrategy_InitialApproverStatus;
            }
        }

        public string QueryPanel_Title_CenterApproveBillTitle {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_CenterApproveBillTitle;
            }
        }

        public string QueryPanel_Title_DealerProduct {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerProduct;
            }
        }

        public string QueryPanel_Title_DealerProductList {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerProductList;
            }
        }

        public string QueryPanel_Title_DealerService {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerService;
            }
        }

        public string QueryPanel_Title_DealerServiceList {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerServiceList;
            }
        }

        public string QueryPanel_Title_DealerServiceNew {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerServiceNew;
            }
        }

        public string QueryPanel_Title_DealerServiceNewList {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerServiceNewList;
            }
        }

        public string QueryPanel_Title_DealerService_24Hotline {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerService_24Hotline;
            }
        }

        public string QueryPanel_Title_DealerService_24Phone {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerService_24Phone;
            }
        }

        public string QueryPanel_Title_DealerService_Authority {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerService_Authority;
            }
        }

        public string QueryPanel_Title_DealerService_Authorized {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerService_Authorized;
            }
        }

        public string QueryPanel_Title_DealerService_BackboneNetworks {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerService_BackboneNetworks;
            }
        }

        public string QueryPanel_Title_DealerService_Categories {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerService_Categories;
            }
        }

        public string QueryPanel_Title_DealerService_CentralStation {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerService_CentralStation;
            }
        }

        public string QueryPanel_Title_DealerService_Classification {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerService_Classification;
            }
        }

        public string QueryPanel_Title_DealerService_DealerType {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerService_DealerType;
            }
        }

        public string QueryPanel_Title_DealerService_FactorHour {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerService_FactorHour;
            }
        }

        public string QueryPanel_Title_DealerService_FactorMaterial {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerService_FactorMaterial;
            }
        }

        public string QueryPanel_Title_DealerService_InvoiceWorking {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerService_InvoiceWorking;
            }
        }

        public string QueryPanel_Title_DealerService_IsDuty {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerService_IsDuty;
            }
        }

        public string QueryPanel_Title_DealerService_ManagementRate {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerService_ManagementRate;
            }
        }

        public string QueryPanel_Title_DealerService_Marketing {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerService_Marketing;
            }
        }

        public string QueryPanel_Title_DealerService_MaterialFee {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerService_MaterialFee;
            }
        }

        public string QueryPanel_Title_DealerService_OldWarehouse {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerService_OldWarehouse;
            }
        }

        public string QueryPanel_Title_DealerService_OutboundService {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerService_OutboundService;
            }
        }

        public string QueryPanel_Title_DealerService_Relationship {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerService_Relationship;
            }
        }

        public string QueryPanel_Title_DealerService_ReserveAmount {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerService_ReserveAmount;
            }
        }

        public string QueryPanel_Title_DealerService_ServiceRadius {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerService_ServiceRadius;
            }
        }

        public string QueryPanel_Title_DealerService_SettlementStar {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerService_SettlementStar;
            }
        }

        public string QueryPanel_Title_DealerService_StationMaintenance {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerService_StationMaintenance;
            }
        }

        public string QueryPanel_Title_DealerService_Warehouse {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerService_Warehouse;
            }
        }

        public string QueryPanel_Title_Dealer_CarStop {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_CarStop;
            }
        }

        public string QueryPanel_Title_Dealer_CarUse {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_CarUse;
            }
        }

        public string QueryPanel_Title_Dealer_Certificate {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_Certificate;
            }
        }

        public string QueryPanel_Title_Dealer_CertificateAddress {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_CertificateAddress;
            }
        }

        public string QueryPanel_Title_Dealer_CertificateDate {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_CertificateDate;
            }
        }

        public string QueryPanel_Title_Dealer_CertificateFee {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_CertificateFee;
            }
        }

        public string QueryPanel_Title_Dealer_CertificateName {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_CertificateName;
            }
        }

        public string QueryPanel_Title_Dealer_CreateTime {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_CreateTime;
            }
        }

        public string QueryPanel_Title_Dealer_Dangerous {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_Dangerous;
            }
        }

        public string QueryPanel_Title_Dealer_Engine {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_Engine;
            }
        }

        public string QueryPanel_Title_Dealer_FixedAssets {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_FixedAssets;
            }
        }

        public string QueryPanel_Title_Dealer_HoursUnit {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_HoursUnit;
            }
        }

        public string QueryPanel_Title_Dealer_IDCardNo {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_IDCardNo;
            }
        }

        public string QueryPanel_Title_Dealer_IDCardType {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_IDCardType;
            }
        }

        public string QueryPanel_Title_Dealer_InvoiceLimit {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_InvoiceLimit;
            }
        }

        public string QueryPanel_Title_Dealer_InvoiceName {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_InvoiceName;
            }
        }

        public string QueryPanel_Title_Dealer_InvoiceRate {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_InvoiceRate;
            }
        }

        public string QueryPanel_Title_Dealer_InvoiceType {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_InvoiceType;
            }
        }

        public string QueryPanel_Title_Dealer_MainScope {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_MainScope;
            }
        }

        public string QueryPanel_Title_Dealer_Maintenance {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_Maintenance;
            }
        }

        public string QueryPanel_Title_Dealer_Nature {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_Nature;
            }
        }

        public string QueryPanel_Title_Dealer_NumberService {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_NumberService;
            }
        }

        public string QueryPanel_Title_Dealer_OperationScope {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_OperationScope;
            }
        }

        public string QueryPanel_Title_Dealer_Parking {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_Parking;
            }
        }

        public string QueryPanel_Title_Dealer_PartsLibrary {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_PartsLibrary;
            }
        }

        public string QueryPanel_Title_Dealer_Qualification {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_Qualification;
            }
        }

        public string QueryPanel_Title_Dealer_Reception {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_Reception;
            }
        }

        public string QueryPanel_Title_Dealer_Registration {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_Registration;
            }
        }

        public string QueryPanel_Title_Dealer_RegistrationAddress {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_RegistrationAddress;
            }
        }

        public string QueryPanel_Title_Dealer_Representative {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_Representative;
            }
        }

        public string QueryPanel_Title_Dealer_RepresentativePhone {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_RepresentativePhone;
            }
        }

        public string QueryPanel_Title_Dealer_Route {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_Route;
            }
        }

        public string QueryPanel_Title_Dealer_Scope {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_Scope;
            }
        }

        public string QueryPanel_Title_Dealer_Secondary {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_Secondary;
            }
        }

        public string QueryPanel_Title_Dealer_ServiceProduct {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_ServiceProduct;
            }
        }

        public string QueryPanel_Title_Dealer_ServiceProductName {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_ServiceProductName;
            }
        }

        public string QueryPanel_Title_Dealer_ServiceProductType {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_ServiceProductType;
            }
        }

        public string QueryPanel_Title_Dealer_Taxpayers {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_Taxpayers;
            }
        }

        public string QueryPanel_Title_Dealer_Traffic {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_Traffic;
            }
        }

        public string QueryPanel_Title_Dealer_Unit {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Dealer_Unit;
            }
        }

        public string QueryPanel_Title_Distributor {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Distributor;
            }
        }

        public string QueryPanel_Title_DistributorList {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DistributorList;
            }
        }

        public string BusinessName_CompanyAddress {
            get {
                return Resources.CommonUIStrings.BusinessName_CompanyAddress;
            }
        }

        public string BusinessName_DealerMarketDptRelation {
            get {
                return Resources.CommonUIStrings.BusinessName_DealerMarketDptRelation;
            }
        }

        public string BusinessName_ImportGoldenTaxClass {
            get {
                return Resources.CommonUIStrings.BusinessName_ImportGoldenTaxClass;
            }
        }

        public string BusinessName_ImportGoldenTaxClassList {
            get {
                return Resources.CommonUIStrings.BusinessName_ImportGoldenTaxClassList;
            }
        }

        public string BusinessName_ImportMarketDptPersonnelRelations {
            get {
                return Resources.CommonUIStrings.BusinessName_ImportMarketDptPersonnelRelations;
            }
        }

        public string BusinessName_ImportMarketDptPersonnelRelationsList {
            get {
                return Resources.CommonUIStrings.BusinessName_ImportMarketDptPersonnelRelationsList;
            }
        }

        public string BusinessName_MarketDptPersonnelRelations {
            get {
                return Resources.CommonUIStrings.BusinessName_MarketDptPersonnelRelations;
            }
        }

        public string BusinessName_MarketingDepartment {
            get {
                return Resources.CommonUIStrings.BusinessName_MarketingDepartment;
            }
        }

        public string BusinessName_MarketingDepartmentList {
            get {
                return Resources.CommonUIStrings.BusinessName_MarketingDepartmentList;
            }
        }

        public string DataDeTailPanel_Text_PartsBranch_PurchaseRoute {
            get {
                return Resources.CommonUIStrings.DataDeTailPanel_Text_PartsBranch_PurchaseRoute;
            }
        }

        public string DataEditPanel_BusinessName_SparePartDetail {
            get {
                return Resources.CommonUIStrings.DataEditPanel_BusinessName_SparePartDetail;
            }
        }

        public string DataEditPanel_Text_Agency_Claim {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_Agency_Claim;
            }
        }

        public string DataEditPanel_Text_PartsPurchasePricingChange_ClosedQuantity {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_ClosedQuantity;
            }
        }

        public string DataEditPanel_Text_PartsPurchasePricingChange_IsPrimary {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_IsPrimary;
            }
        }

        public string DataEditPanel_Text_PartsPurchasePricingChange_IsPrimaryNew {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_IsPrimaryNew;
            }
        }

        public string DataEditPanel_Text_PartsPurchasePricingChange_ValidFrom {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_ValidFrom;
            }
        }

        public string DataEditPanel_Text_PartsPurchasePricingChange_ValidTo {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_ValidTo;
            }
        }

        public string DataEditPanel_Text_PartsReplacement_OldPartName {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_PartsReplacement_OldPartName;
            }
        }

        public string DataEditPanel_Text_SparePartForBatch_CategoryCode {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SparePartForBatch_CategoryCode;
            }
        }

        public string DataEditPanel_Text_SparePartForBatch_CategoryName {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SparePartForBatch_CategoryName;
            }
        }

        public string DataEditPanel_Text_SparePartForBatch_Factury {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SparePartForBatch_Factury;
            }
        }

        public string DataEditPanel_Text_SparePartForBatch_IMSCompressionNumber {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SparePartForBatch_IMSCompressionNumber;
            }
        }

        public string DataEditPanel_Text_SparePartForBatch_IMSManufacturer {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SparePartForBatch_IMSManufacturer;
            }
        }

        public string DataEditPanel_Text_SparePartForBatch_IsTransfer {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SparePartForBatch_IsTransfer;
            }
        }

        public string DataEditPanel_Text_SparePartForBatch_MInPackingAmount {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SparePartForBatch_MInPackingAmount;
            }
        }

        public string DataEditPanel_Text_SparePartForBatch_NextSubstitute {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SparePartForBatch_NextSubstitute;
            }
        }

        public string DataEditPanel_Text_SparePartForBatch_PartType {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SparePartForBatch_PartType;
            }
        }

        public string DataEditPanel_Text_SparePartForBatch_ShelfLife {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SparePartForBatch_ShelfLife;
            }
        }

        public string DataEditPanel_Text_SparePartForBatch_Specification {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SparePartForBatch_Specification;
            }
        }

        public string DataEditPanel_Text_SparePartForBatch_StandardName {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SparePartForBatch_StandardName;
            }
        }

        public string DataEditPanel_Text_SparePartForBatch_Weight {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SparePartForBatch_Weight;
            }
        }

        public string DataEditPanel_Text_SparePart_PinyinCode {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SparePart_PinyinCode;
            }
        }

        public string DataEditPanel_Title_ImportExchangeIdentification {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Title_ImportExchangeIdentification;
            }
        }

        public string DataEditPanel_Title_ImportFactury {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Title_ImportFactury;
            }
        }

        public string DataEditPanel_Title_ImportGoldenTaxClassify {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Title_ImportGoldenTaxClassify;
            }
        }

        public string DataEditPanel_Title_ImportImportCategoryCode {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Title_ImportImportCategoryCode;
            }
        }

        public string DataEditPanel_Title_NotificationDetail {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Title_NotificationDetail;
            }
        }

        public string DataEditPanel_Title_ReplaceEnglishName {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Title_ReplaceEnglishName;
            }
        }

        public string DataEditPanel_Title_ReplaceFeature {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Title_ReplaceFeature;
            }
        }

        public string DataEditPanel_Title_ReplaceImportEdit {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Title_ReplaceImportEdit;
            }
        }

        public string DataEditPanel_Title_ReplaceIMSCompressionNumber {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Title_ReplaceIMSCompressionNumber;
            }
        }

        public string DataEditPanel_Title_ReplaceIMSManufacturerNumber {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Title_ReplaceIMSManufacturerNumber;
            }
        }

        public string DataEditPanel_Title_ReplaceMInPackingAmount {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Title_ReplaceMInPackingAmount;
            }
        }

        public string DataEditPanel_Title_ReplaceName {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Title_ReplaceName;
            }
        }

        public string DataEditPanel_Title_ReplaceNextSubstitute {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Title_ReplaceNextSubstitute;
            }
        }

        public string DataEditPanel_Title_ReplaceOverseasPartsFigure {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Title_ReplaceOverseasPartsFigure;
            }
        }

        public string DataEditPanel_Title_ReplacePartType {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Title_ReplacePartType;
            }
        }

        public string DataEditPanel_Title_ReplaceProductBrand {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Title_ReplaceProductBrand;
            }
        }

        public string DataEditPanel_Title_ReplaceReferenceCode {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Title_ReplaceReferenceCode;
            }
        }

        public string DataEditPanel_Title_ReplaceSpecification {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Title_ReplaceSpecification;
            }
        }

        public string DataEditPanel_Title_ReplaceStandardName {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Title_ReplaceStandardName;
            }
        }

        public string DataEditPanel_Title_ReplaceWarrantyTransfer {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Title_ReplaceWarrantyTransfer;
            }
        }

        public string DataEditPanel_Title_StandardImport {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Title_StandardImport;
            }
        }

        public string DataEditPanel_Validation_ExportCustomerInfo_CustomerName {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Validation_ExportCustomerInfo_CustomerName;
            }
        }

        public string DataEditPanel_Validation_ExportCustomerInfo_CustomerNo {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Validation_ExportCustomerInfo_CustomerNo;
            }
        }

        public string DataEditPanel_Validation_ExportCustomerInfo_IssuingOffice {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Validation_ExportCustomerInfo_IssuingOffice;
            }
        }

        public string DataEditPanel_Validation_ExportCustomerInfo_Price {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Validation_ExportCustomerInfo_Price;
            }
        }

        public string DataEditPanel_Validation_Notification_AttAchmentIsNull {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Validation_Notification_AttAchmentIsNull;
            }
        }

        public string DataEditPanel_Validation_Notification_AttachmentReply {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Validation_Notification_AttachmentReply;
            }
        }

        public string DataEditPanel_Validation_Notification_File {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Validation_Notification_File;
            }
        }

        public string DataEditPanel_Validation_Notification_PublishDep {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Validation_Notification_PublishDep;
            }
        }

        public string DataEditPanel_Validation_Notification_UrgentLevel {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Validation_Notification_UrgentLevel;
            }
        }

        public string DataEditPanel_Validation_Notification_WordReply {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Validation_Notification_WordReply;
            }
        }

        public string DataEditPanel_Validation_Notification_WordReplyIsnull {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Validation_Notification_WordReplyIsnull;
            }
        }

        public string DataEditPanel_Validation_SparePartForBatch_Assembly {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Validation_SparePartForBatch_Assembly;
            }
        }

        public string DataEditPanel_Validation_SparePartForBatch_GroupABCCategory {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Validation_SparePartForBatch_GroupABCCategory;
            }
        }

        public string DataEditPanel_Validation_SparePartForBatch_LastSubstitute {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Validation_SparePartForBatch_LastSubstitute;
            }
        }

        public string DataEditPanel_Validation_SparePartForBatch_NewAttach {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Validation_SparePartForBatch_NewAttach;
            }
        }

        public string DataEditPanel_Validation_SparePartForBatch_RecoverySparepart {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Validation_SparePartForBatch_RecoverySparepart;
            }
        }

        public string DataEditPanel_Validation_SparePartForBatch_SparePartForImport {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Validation_SparePartForBatch_SparePartForImport;
            }
        }

        public string DataEditPanel_Validation_SparePartForBatch_SparePartForImportList {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Validation_SparePartForBatch_SparePartForImportList;
            }
        }

        public string DataEditPanel_Validation_SparePartForBatch_StopSparepart {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Validation_SparePartForBatch_StopSparepart;
            }
        }

        public string DataEditView_BusinessName_PersonnelSaleRegion {
            get {
                return Resources.CommonUIStrings.DataEditView_BusinessName_PersonnelSaleRegion;
            }
        }

        public string DataEditView_BusinessName_PersonnelSupplierRelation {
            get {
                return Resources.CommonUIStrings.DataEditView_BusinessName_PersonnelSupplierRelation;
            }
        }

        public string DataEditView_BusinessName_ResponsibleUnit {
            get {
                return Resources.CommonUIStrings.DataEditView_BusinessName_ResponsibleUnit;
            }
        }

        public string DataEditView_BusinessName_SalesRegion {
            get {
                return Resources.CommonUIStrings.DataEditView_BusinessName_SalesRegion;
            }
        }

        public string DataEditView_Text_PartsBranch_AutoApproveUpLimit {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_PartsBranch_AutoApproveUpLimit;
            }
        }

        public string DataEditView_Text_PartsBranch_LossType {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_PartsBranch_LossType;
            }
        }

        public string DataEditView_Text_PartsBranch_MInPackingAmount {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_PartsBranch_MInPackingAmount;
            }
        }

        public string DataEditView_Text_PartsBranch_PartABC {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_PartsBranch_PartABC;
            }
        }

        public string DataEditView_Text_PartsBranch_PartsMaterialManageCost {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_PartsBranch_PartsMaterialManageCost;
            }
        }

        public string DataEditView_Text_PartsBranch_PartsReturnPolicy {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_PartsBranch_PartsReturnPolicy;
            }
        }

        public string DataEditView_Text_PartsBranch_PartsWarrantyLong {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_PartsBranch_PartsWarrantyLong;
            }
        }

        public string DataEditView_Text_PartsBranch_ProductLifeCycle {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_PartsBranch_ProductLifeCycle;
            }
        }

        public string DataEditView_Text_PartsBranch_ReplaceList {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_PartsBranch_ReplaceList;
            }
        }

        public string DataEditView_Text_PartsBranch_StockMaximum {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_PartsBranch_StockMaximum;
            }
        }

        public string DataEditView_Text_PartsBranch_StockMinimum {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_PartsBranch_StockMinimum;
            }
        }

        public string DataEditView_Text_PartsSupplierRelation_ArrivalReplenishmentCycle {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_PartsSupplierRelation_ArrivalReplenishmentCycle;
            }
        }

        public string DataEditView_Text_PartsSupplierRelation_DefaultOrderWarehouse {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_PartsSupplierRelation_DefaultOrderWarehouse;
            }
        }

        public string DataEditView_Text_PartsSupplierRelation_EconomicalBatch {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_PartsSupplierRelation_EconomicalBatch;
            }
        }

        public string DataEditView_Text_PartsSupplierRelation_MinBatch {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_PartsSupplierRelation_MinBatch;
            }
        }

        public string DataEditView_Text_PartsSupplierRelation_Month {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_PartsSupplierRelation_Month;
            }
        }

        public string DataEditView_Text_PartsSupplierRelation_OrderCycle {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_PartsSupplierRelation_OrderCycle;
            }
        }

        public string DataEditView_Text_PartsSupplierRelation_PurchasePercentage {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_PartsSupplierRelation_PurchasePercentage;
            }
        }

        public string DataEditView_Text_PartsSupplierRelation_Urgent {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_PartsSupplierRelation_Urgent;
            }
        }

        public string DataEditView_Text_PersonnelSupplierRelation_PersonelCode {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_PersonnelSupplierRelation_PersonelCode;
            }
        }

        public string DataEditView_Text_Personnel_SupplierCode {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_Personnel_SupplierCode;
            }
        }

        public string DataEditView_Title_AddSparepartReplace {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_AddSparepartReplace;
            }
        }

        public string DataEditView_Title_PartsBranchForDirectSupply {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PartsBranchForDirectSupply;
            }
        }

        public string DataEditView_Title_PartsBranchForDirectSupplyList {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PartsBranchForDirectSupplyList;
            }
        }

        public string DataEditView_Title_PartsBranchForImport {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PartsBranchForImport;
            }
        }

        public string DataEditView_Title_PartsBranchForImportList {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PartsBranchForImportList;
            }
        }

        public string DataEditView_Title_PartsBranchForImportUpdate {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PartsBranchForImportUpdate;
            }
        }

        public string DataEditView_Title_PartsBranchForImportUpdateList {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PartsBranchForImportUpdateList;
            }
        }

        public string DataEditView_Title_PartsBranchForPartABC {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PartsBranchForPartABC;
            }
        }

        public string DataEditView_Title_PartsBranchForPartABCList {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PartsBranchForPartABCList;
            }
        }

        public string DataEditView_Title_PartsBranchForPartReturnPolicy {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PartsBranchForPartReturnPolicy;
            }
        }

        public string DataEditView_Title_PartsBranchForPartReturnPolicyList {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PartsBranchForPartReturnPolicyList;
            }
        }

        public string DataEditView_Title_PartsBranchForPartWarranty {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PartsBranchForPartWarranty;
            }
        }

        public string DataEditView_Title_PartsBranchForPartWarrantyList {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PartsBranchForPartWarrantyList;
            }
        }

        public string DataEditView_Title_PartsBranchForReplace {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PartsBranchForReplace;
            }
        }

        public string DataEditView_Title_PartsBranchForReplaceIsOrderable {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PartsBranchForReplaceIsOrderable;
            }
        }

        public string DataEditView_Title_PartsBranchForReplaceIsOrderableList {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PartsBranchForReplaceIsOrderableList;
            }
        }

        public string DataEditView_Title_PartsBranchForReplaceList {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PartsBranchForReplaceList;
            }
        }

        public string DataEditView_Title_PartsBranchForReplaceRemark {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PartsBranchForReplaceRemark;
            }
        }

        public string DataEditView_Title_PartsBranchForReplaceRepair {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PartsBranchForReplaceRepair;
            }
        }

        public string DataEditView_Title_PartsBranchForStockLimit {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PartsBranchForStockLimit;
            }
        }

        public string DataEditView_Title_PartsBranchForStockLimitList {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PartsBranchForStockLimitList;
            }
        }

        public string DataEditView_Title_PartsExchangeForImport {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PartsExchangeForImport;
            }
        }

        public string DataEditView_Title_PartsExchangeForImportList {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PartsExchangeForImportList;
            }
        }

        public string DataEditView_Title_PartsExchangeGroup {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PartsExchangeGroup;
            }
        }

        public string DataEditView_Title_PartsExchangeGroupList {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PartsExchangeGroupList;
            }
        }

        public string DataEditView_Title_PartsPurchasePricingChange_InitialApprover {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PartsPurchasePricingChange_InitialApprover;
            }
        }

        public string DataEditView_Title_PartsReplacementForImport {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PartsReplacementForImport;
            }
        }

        public string DataEditView_Title_PartsReplacementForImportList {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PartsReplacementForImportList;
            }
        }

        public string DataEditView_Title_PartsSupplierRelationForImport {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PartsSupplierRelationForImport;
            }
        }

        public string DataEditView_Title_PartsSupplierRelationForImportList {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PartsSupplierRelationForImportList;
            }
        }

        public string DataEditView_Title_PersonnelSupplierRelation {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PersonnelSupplierRelation;
            }
        }

        public string DataEditView_Title_PersonnelSupplierRelationForImport {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PersonnelSupplierRelationForImport;
            }
        }

        public string DataEditView_Title_PersonnelSupplierRelationForImportList {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_PersonnelSupplierRelationForImportList;
            }
        }

        public string DataEditView_Title_ProductExecutiveStandardForBatchImport {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_ProductExecutiveStandardForBatchImport;
            }
        }

        public string DataEditView_Title_ProductExecutiveStandardForBatchImportList {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_ProductExecutiveStandardForBatchImportList;
            }
        }

        public string DataEditView_Title_ReplaceApproveLimit {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_ReplaceApproveLimit;
            }
        }

        public string DataEditView_Title_UpdateSparepartReplace {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_UpdateSparepartReplace;
            }
        }

        public string DataEditView_ValidationPartsExchanges_SameSparePart {
            get {
                return Resources.CommonUIStrings.DataEditView_ValidationPartsExchanges_SameSparePart;
            }
        }

        public string DataEditView_Validation_PartsBranch_PartABC {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_PartsBranch_PartABC;
            }
        }

        public string DataEditView_Validation_PartsBranch_PartsReturnPolicy {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_PartsBranch_PartsReturnPolicy;
            }
        }

        public string DataEditView_Validation_PartsBranch_Spare {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_PartsBranch_Spare;
            }
        }

        public string DataEditView_Validation_PartsPurchasePricingChange_ApprovalCommentIsnull {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_PartsPurchasePricingChange_ApprovalCommentIsnull;
            }
        }

        public string DataEditView_Validation_PartsPurchasePricingChange_Left {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_PartsPurchasePricingChange_Left;
            }
        }

        public string DataEditView_Validation_PartsPurchasePricingChange_Reject {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_PartsPurchasePricingChange_Reject;
            }
        }

        public string DataEditView_Validation_PartsPurchasePricingChange_SameSparepart {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_PartsPurchasePricingChange_SameSparepart;
            }
        }

        public string DataEditView_Validation_PartsPurchasePricingChange_Success {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_PartsPurchasePricingChange_Success;
            }
        }

        public string DataEditView_Validation_PartsPurchasePricingChange_SupplierPartCode {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_PartsPurchasePricingChange_SupplierPartCode;
            }
        }

        public string DataEditView_Validation_PartsSupplierRelation_Import {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_PartsSupplierRelation_Import;
            }
        }

        public string DataEditView_Validation_PartsSupplierRelation_MinBatch {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_PartsSupplierRelation_MinBatch;
            }
        }

        public string DataEditView_Validation_PartsSupplierRelation_SupplierPartCode {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_PartsSupplierRelation_SupplierPartCode;
            }
        }

        public string DataEditView_Validation_PartsSupplier_Code {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_PartsSupplier_Code;
            }
        }

        public string DataEditView_Validation_PartsSupplier_CodeLong {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_PartsSupplier_CodeLong;
            }
        }

        public string DataEditView_Validation_PartsSupplier_ContactMail {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_PartsSupplier_ContactMail;
            }
        }

        public string DataEditView_Validation_PartsSupplier_ContactPerson {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_PartsSupplier_ContactPerson;
            }
        }

        public string DataEditView_Validation_PartsSupplier_ContactPhone {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_PartsSupplier_ContactPhone;
            }
        }

        public string DataEditView_Validation_PartsSupplier_Name {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_PartsSupplier_Name;
            }
        }

        public string DataEditView_Validation_PersonnelSupplierRelation_Person {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_PersonnelSupplierRelation_Person;
            }
        }

        public string DataEditView_Validation_RegionPersonnelRelation_RegionCode {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_RegionPersonnelRelation_RegionCode;
            }
        }

        public string DataEditView_Validation_RegionPersonnelRelation_RegionName {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_RegionPersonnelRelation_RegionName;
            }
        }

        public string DataEditView_Validation_SparePart_Code {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_SparePart_Code;
            }
        }

        public string DataEditView_Validation_SparePart_MInPackingAmount {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_SparePart_MInPackingAmount;
            }
        }

        public string DataEditView_Validation_SparePart_ReferenceCode {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_SparePart_ReferenceCode;
            }
        }

        public string DataEditView_Validation_TiledRegion_TempCity {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_TiledRegion_TempCity;
            }
        }

        public string DataGridView_Head_Product {
            get {
                return Resources.CommonUIStrings.DataGridView_Head_Product;
            }
        }

        public string DataGridView_Head_RegionPersonnelRelation {
            get {
                return Resources.CommonUIStrings.DataGridView_Head_RegionPersonnelRelation;
            }
        }

        public string DataGridView_Title_AgencyBindRegion {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_AgencyBindRegion;
            }
        }

        public string DataGridView_Title_BottomStock_Status {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_BottomStock_Status;
            }
        }

        public string DataGridView_Title_BranchSupplierRelation_ArrivalCycle {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_BranchSupplierRelation_ArrivalCycle;
            }
        }

        public string DataGridView_Title_CenterApproveBill_ApproveWeek {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_CenterApproveBill_ApproveWeek;
            }
        }

        public string DataGridView_Title_CenterSaleReturnStrategy_TimeFrom {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_CenterSaleReturnStrategy_TimeFrom;
            }
        }

        public string DataGridView_Title_CenterSaleReturnStrategy_TimeTo {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_CenterSaleReturnStrategy_TimeTo;
            }
        }

        public string DataGridView_Title_MarketDptPersonnelRelation_CellNumber {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_MarketDptPersonnelRelation_CellNumber;
            }
        }

        public string DataGridView_Title_NotificationReply_CompanyName {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_NotificationReply_CompanyName;
            }
        }

        public string DataGridView_Title_NotificationReply_ReplyChannel {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_NotificationReply_ReplyChannel;
            }
        }

        public string DataGridView_Title_NotificationReply_ReplyContent {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_NotificationReply_ReplyContent;
            }
        }

        public string DataGridView_Title_NotificationReply_ReplyDate {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_NotificationReply_ReplyDate;
            }
        }

        public string DataGridView_Title_NotificationReply_VirtualPartsBranch {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_NotificationReply_VirtualPartsBranch;
            }
        }

        public string DataGridView_Title_PartsPurchasePricingChange_IsUplodFile {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_PartsPurchasePricingChange_IsUplodFile;
            }
        }

        public string DataGridView_Title_PartsPurchasePricingDetail_IsPrimaryMinPrice {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_IsPrimaryMinPrice;
            }
        }

        public string DataGridView_Title_PartsPurchasePricingDetail_MinPriceSupplier {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_MinPriceSupplier;
            }
        }

        public string DataGridView_Title_PartsPurchasePricingDetail_MinPurchasePrice {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_MinPurchasePrice;
            }
        }

        public string DataGridView_Title_PartsPurchasePricingDetail_OldPriPrice {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_OldPriPrice;
            }
        }

        public string DataGridView_Title_PartsPurchasePricingDetail_OldPriPriceType {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_OldPriPriceType;
            }
        }

        public string DataGridView_Title_PartsPurchasePricingDetail_OldPriSupplierName {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_OldPriSupplierName;
            }
        }

        public string DataGridView_Title_PartsPurchasePricingDetail_PriceRate {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_PriceRate;
            }
        }

        public string DataGridView_Title_PartsPurchasePricingDetail_PriceRateFloat {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_PriceRateFloat;
            }
        }

        public string DataGridView_Title_PartsPurchasePricingDetail_PriceRateNew {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_PriceRateNew;
            }
        }

        public string DataGridView_Title_ProductView_ProductFunctionCode {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_ProductView_ProductFunctionCode;
            }
        }

        public string DataGridView_Title_SparePartExtend_ReferenceCode {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_SparePartExtend_ReferenceCode;
            }
        }

        public string DataGridView_Title_SparePartExtend_SubstandardName {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_SparePartExtend_SubstandardName;
            }
        }

        public string DataGridView_Title_VirtualFactoryPurchacePrice_ContractPrice {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_VirtualFactoryPurchacePrice_ContractPrice;
            }
        }

        public string DataGridView_Title_VirtualFactoryPurchacePrice_Rate {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_VirtualFactoryPurchacePrice_Rate;
            }
        }

        public string DataGridView_Title_VirtualPartsBranch_FirPrin {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_VirtualPartsBranch_FirPrin;
            }
        }

        public string DataGridView_Title_VirtualPartsBranch_PackingCoefficient1 {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_VirtualPartsBranch_PackingCoefficient1;
            }
        }

        public string DataGridView_Title_VirtualPartsBranch_PackingCoefficient2 {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_VirtualPartsBranch_PackingCoefficient2;
            }
        }

        public string DataGridView_Title_VirtualPartsBranch_PackingCoefficient3 {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_VirtualPartsBranch_PackingCoefficient3;
            }
        }

        public string DataGridView_Title_VirtualPartsBranch_RetailGuidePrice {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_VirtualPartsBranch_RetailGuidePrice;
            }
        }

        public string DataGridView_Title_VirtualPartsBranch_SalesPrice {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_VirtualPartsBranch_SalesPrice;
            }
        }

        public string DataGridView_Title_VirtualPartsBranch_SecPrin {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_VirtualPartsBranch_SecPrin;
            }
        }

        public string DataGridView_Title_VirtualPartsBranch_ThidPrin {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_VirtualPartsBranch_ThidPrin;
            }
        }

        public string DataGridView_Title_VirtualPartsPriceHistory_Createtime {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_VirtualPartsPriceHistory_Createtime;
            }
        }

        public string DataGridView_Title_VirtualPartsPriceHistory_Dealersalesprice {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_VirtualPartsPriceHistory_Dealersalesprice;
            }
        }

        public string DataGridView_Title_VirtualPartsPriceHistory_PlannedPrice {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_VirtualPartsPriceHistory_PlannedPrice;
            }
        }

        public string DataGridView_Title_VirtualPartsPriceHistory_SalesPrice {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_VirtualPartsPriceHistory_SalesPrice;
            }
        }

        public string DataGridView_Title_VirtualPartsPurchasePricing_DataSource {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_VirtualPartsPurchasePricing_DataSource;
            }
        }

        public string DataGridView_Title_VirtualPartsPurchasePricing_UsedQty {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_VirtualPartsPurchasePricing_UsedQty;
            }
        }

        public string DataManagementView_Header_PartsBranchManagement {
            get {
                return Resources.CommonUIStrings.DataManagementView_Header_PartsBranchManagement;
            }
        }

        public string DataManagementView_Header_StarStatisticsManagement {
            get {
                return Resources.CommonUIStrings.DataManagementView_Header_StarStatisticsManagement;
            }
        }

        public string DataManagementView_Header_StatisticalReportsMainQualiManagement {
            get {
                return Resources.CommonUIStrings.DataManagementView_Header_StatisticalReportsMainQualiManagement;
            }
        }

        public string DataManagementView_Notification_CreateSuccess {
            get {
                return Resources.CommonUIStrings.DataManagementView_Notification_CreateSuccess;
            }
        }

        public string DataManagementView_Notification_Submit {
            get {
                return Resources.CommonUIStrings.DataManagementView_Notification_Submit;
            }
        }

        public string DataManagementView_Notification_SubmitSuccess {
            get {
                return Resources.CommonUIStrings.DataManagementView_Notification_SubmitSuccess;
            }
        }

        public string DataManagementView_Title_AgentsHistoryOutManagement {
            get {
                return Resources.CommonUIStrings.DataManagementView_Title_AgentsHistoryOutManagement;
            }
        }

        public string DataManagementView_Title_BottomStockManagement {
            get {
                return Resources.CommonUIStrings.DataManagementView_Title_BottomStockManagement;
            }
        }

        public string DataManagementView_Title_CenterApproveBillManagement {
            get {
                return Resources.CommonUIStrings.DataManagementView_Title_CenterApproveBillManagement;
            }
        }

        public string DataManagementView_Title_CenterSaleReturnStrategyManagement {
            get {
                return Resources.CommonUIStrings.DataManagementView_Title_CenterSaleReturnStrategyManagement;
            }
        }

        public string DataManagementView_Title_ClaimSupplierDetailsSplitManagement {
            get {
                return Resources.CommonUIStrings.DataManagementView_Title_ClaimSupplierDetailsSplitManagement;
            }
        }

        public string DataManagementView_Title_CompanyAddressManagement {
            get {
                return Resources.CommonUIStrings.DataManagementView_Title_CompanyAddressManagement;
            }
        }

        public string DataManagementView_Title_FactoryPurchacePriceManagement {
            get {
                return Resources.CommonUIStrings.DataManagementView_Title_FactoryPurchacePriceManagement;
            }
        }

        public string DataManagementView_Title_MarketDptPersonnelRelationManagement {
            get {
                return Resources.CommonUIStrings.DataManagementView_Title_MarketDptPersonnelRelationManagement;
            }
        }

        public string DataManagementView_Title_MarketingDepartmentManagement {
            get {
                return Resources.CommonUIStrings.DataManagementView_Title_MarketingDepartmentManagement;
            }
        }

        public string DataManagementView_Title_PartBnOrBwRateStaManagement {
            get {
                return Resources.CommonUIStrings.DataManagementView_Title_PartBnOrBwRateStaManagement;
            }
        }

        public string DataManagementView_Title_PartsInboundHistoryForReportManagement {
            get {
                return Resources.CommonUIStrings.DataManagementView_Title_PartsInboundHistoryForReportManagement;
            }
        }

        public string DataManagementView_Title_PartsPurchaseAssessmentStaManagement {
            get {
                return Resources.CommonUIStrings.DataManagementView_Title_PartsPurchaseAssessmentStaManagement;
            }
        }

        public string DataManagementView_Title_SparePartDetailsManagement {
            get {
                return Resources.CommonUIStrings.DataManagementView_Title_SparePartDetailsManagement;
            }
        }

        public string DataManagementView_Title_StatisticalReportsOnTheServiceTypeManagement {
            get {
                return Resources.CommonUIStrings.DataManagementView_Title_StatisticalReportsOnTheServiceTypeManagement;
            }
        }

        public string DataManagementView_Validation_Company_CompanyCode {
            get {
                return Resources.CommonUIStrings.DataManagementView_Validation_Company_CompanyCode;
            }
        }

        public string DataManagementView_Validation_Company_OverTime {
            get {
                return Resources.CommonUIStrings.DataManagementView_Validation_Company_OverTime;
            }
        }

        public string DataManagementView_Validation_Company_QueryTimeBegin {
            get {
                return Resources.CommonUIStrings.DataManagementView_Validation_Company_QueryTimeBegin;
            }
        }

        public string DataManagementView_Validation_Company_urgentLevel {
            get {
                return Resources.CommonUIStrings.DataManagementView_Validation_Company_urgentLevel;
            }
        }

        public string DataManagementView_Validation_Service {
            get {
                return Resources.CommonUIStrings.DataManagementView_Validation_Service;
            }
        }

        public string DataManagementView_Validation_SupplierCode {
            get {
                return Resources.CommonUIStrings.DataManagementView_Validation_SupplierCode;
            }
        }

        public string DataManagementView_Validation_VirtualFactoryPurchacePrice_AddPurchacePrice {
            get {
                return Resources.CommonUIStrings.DataManagementView_Validation_VirtualFactoryPurchacePrice_AddPurchacePrice;
            }
        }

        public string DataManagementView_Validation_VirtualFactoryPurchacePrice_sparePartCode {
            get {
                return Resources.CommonUIStrings.DataManagementView_Validation_VirtualFactoryPurchacePrice_sparePartCode;
            }
        }

        public string DetailEditView_Title_CompanyList {
            get {
                return Resources.CommonUIStrings.DetailEditView_Title_CompanyList;
            }
        }

        public string QueryPanel_Title_DealerProduct_Market {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerProduct_Market;
            }
        }

        public string QueryPanel_Title_DealerProduct_OutStatus {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_DealerProduct_OutStatus;
            }
        }

        public string QueryPanel_Title_MarketingDepartment_BussinessType {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_MarketingDepartment_BussinessType;
            }
        }

        public string QueryPanel_Title_MarketingDepartment_Jurisdiction {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_MarketingDepartment_Jurisdiction;
            }
        }

        public string QueryPanel_Title_MarketingDepartment_PostalCode {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_MarketingDepartment_PostalCode;
            }
        }

        public string Report_Title_InOutSettleSummaryReport {
            get {
                return Resources.CommonUIStrings.Report_Title_InOutSettleSummaryReport;
            }
        }

        public string Report_Title_InOutSettleSummaryReport_Query {
            get {
                return Resources.CommonUIStrings.Report_Title_InOutSettleSummaryReport_Query;
            }
        }

        public string Report_Title_InOutSettleSummary_All {
            get {
                return Resources.CommonUIStrings.Report_Title_InOutSettleSummary_All;
            }
        }

        public string Report_Title_InOutSettleSummary_Approve {
            get {
                return Resources.CommonUIStrings.Report_Title_InOutSettleSummary_Approve;
            }
        }

        public string Report_Title_InOutSettleSummary_Arrival {
            get {
                return Resources.CommonUIStrings.Report_Title_InOutSettleSummary_Arrival;
            }
        }

        public string Report_Title_InOutSettleSummary_Fee {
            get {
                return Resources.CommonUIStrings.Report_Title_InOutSettleSummary_Fee;
            }
        }

        public string Report_Title_InOutSettleSummary_Inbound {
            get {
                return Resources.CommonUIStrings.Report_Title_InOutSettleSummary_Inbound;
            }
        }

        public string Report_Title_InOutSettleSummary_InboundUn {
            get {
                return Resources.CommonUIStrings.Report_Title_InOutSettleSummary_InboundUn;
            }
        }

        public string Report_Title_InOutSettleSummary_InLine {
            get {
                return Resources.CommonUIStrings.Report_Title_InOutSettleSummary_InLine;
            }
        }

        public string Report_Title_InOutSettleSummary_Item {
            get {
                return Resources.CommonUIStrings.Report_Title_InOutSettleSummary_Item;
            }
        }

        public string Report_Title_InOutSettleSummary_OutBound {
            get {
                return Resources.CommonUIStrings.Report_Title_InOutSettleSummary_OutBound;
            }
        }

        public string Report_Title_InOutSettleSummary_Pack {
            get {
                return Resources.CommonUIStrings.Report_Title_InOutSettleSummary_Pack;
            }
        }

        public string Report_Title_InOutSettleSummary_Pickingtask {
            get {
                return Resources.CommonUIStrings.Report_Title_InOutSettleSummary_Pickingtask;
            }
        }

        public string Report_Title_InOutSettleSummary_Purchase {
            get {
                return Resources.CommonUIStrings.Report_Title_InOutSettleSummary_Purchase;
            }
        }

        public string Report_Title_InOutSettleSummary_Qty {
            get {
                return Resources.CommonUIStrings.Report_Title_InOutSettleSummary_Qty;
            }
        }

        public string Report_Title_InOutSettleSummary_RecordTime {
            get {
                return Resources.CommonUIStrings.Report_Title_InOutSettleSummary_RecordTime;
            }
        }

        public string Report_Title_InOutSettleSummary_Return {
            get {
                return Resources.CommonUIStrings.Report_Title_InOutSettleSummary_Return;
            }
        }

        public string Report_Title_InOutSettleSummary_Shelves {
            get {
                return Resources.CommonUIStrings.Report_Title_InOutSettleSummary_Shelves;
            }
        }

        public string Report_Title_InOutSettleSummary_Shipping {
            get {
                return Resources.CommonUIStrings.Report_Title_InOutSettleSummary_Shipping;
            }
        }

        public string Report_Title_InOutSettleSummary_ShippingWeight {
            get {
                return Resources.CommonUIStrings.Report_Title_InOutSettleSummary_ShippingWeight;
            }
        }

        public string Report_Title_InOutSettleSummary_UnOutBound {
            get {
                return Resources.CommonUIStrings.Report_Title_InOutSettleSummary_UnOutBound;
            }
        }

        public string Report_Title_InOutSettleSummary_UnPack {
            get {
                return Resources.CommonUIStrings.Report_Title_InOutSettleSummary_UnPack;
            }
        }

        public string Report_Title_InOutSettleSummary_UnShelves {
            get {
                return Resources.CommonUIStrings.Report_Title_InOutSettleSummary_UnShelves;
            }
        }

        public string Report_Title_InOutSettleSummary_Warehouse {
            get {
                return Resources.CommonUIStrings.Report_Title_InOutSettleSummary_Warehouse;
            }
        }

        public string Report_Title_InOutSettleSummary_Warehouse_Exit {
            get {
                return Resources.CommonUIStrings.Report_Title_InOutSettleSummary_Warehouse_Exit;
            }
        }

        public string Report_Title_InOutSettleSummary_Warehouse_Shuangqiao {
            get {
                return Resources.CommonUIStrings.Report_Title_InOutSettleSummary_Warehouse_Shuangqiao;
            }
        }

        public string DataGridView_PurchasePriceWarningOfExpiry_DaysRemaining {
            get {
                return Resources.CommonUIStrings.DataGridView_PurchasePriceWarningOfExpiry_DaysRemaining;
            }
        }

        public string DataGridView_PurchasePriceWarningOfExpiry_DaysRemainingDay {
            get {
                return Resources.CommonUIStrings.DataGridView_PurchasePriceWarningOfExpiry_DaysRemainingDay;
            }
        }

        public string DataGridView_PurchasePriceWarningOfExpiry_IsPrimary {
            get {
                return Resources.CommonUIStrings.DataGridView_PurchasePriceWarningOfExpiry_IsPrimary;
            }
        }

        public string DataGridView_PurchasePriceWarningOfExpiry_PriceType {
            get {
                return Resources.CommonUIStrings.DataGridView_PurchasePriceWarningOfExpiry_PriceType;
            }
        }

        public string DataGridView_PurchasePriceWarningOfExpiry_ValidFrom {
            get {
                return Resources.CommonUIStrings.DataGridView_PurchasePriceWarningOfExpiry_ValidFrom;
            }
        }

        public string DataGridView_PurchasePriceWarningOfExpiry_ValidTo {
            get {
                return Resources.CommonUIStrings.DataGridView_PurchasePriceWarningOfExpiry_ValidTo;
            }
        }

        public string Report_Title_PurchasePriceWarningOfExpiryReport {
            get {
                return Resources.CommonUIStrings.Report_Title_PurchasePriceWarningOfExpiryReport;
            }
        }

        public string Report_Title_PurchasePriceWarningOfExpiryReportQuery {
            get {
                return Resources.CommonUIStrings.Report_Title_PurchasePriceWarningOfExpiryReportQuery;
            }
        }

        public string DataGridView_Title_CentralPartsSalesOrder_AppRoveAmount {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_CentralPartsSalesOrder_AppRoveAmount;
            }
        }

        public string DataGridView_Title_CentralPartsSalesOrder_Month {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_CentralPartsSalesOrder_Month;
            }
        }

        public string DataGridView_Title_CentralPartsSalesOrder_OneTimeRate {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_CentralPartsSalesOrder_OneTimeRate;
            }
        }

        public string DataGridView_Title_CentralPartsSalesOrder_OrderAmount {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_CentralPartsSalesOrder_OrderAmount;
            }
        }

        public string DataGridView_Title_CentralPartsSalesOrder_SatisfiedAmount {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_CentralPartsSalesOrder_SatisfiedAmount;
            }
        }

        public string DataGridView_Title_CentralPartsSalesOrder_TotalAmount {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_CentralPartsSalesOrder_TotalAmount;
            }
        }

        public string DataGridView_Title_CentralPartsSalesOrder_UnSatisfiedAmount {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_CentralPartsSalesOrder_UnSatisfiedAmount;
            }
        }

        public string QueryPanel_KvValue_CentralPartsSalesOrder_Day {
            get {
                return Resources.CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_Day;
            }
        }

        public string QueryPanel_KvValue_CentralPartsSalesOrder_Month {
            get {
                return Resources.CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_Month;
            }
        }

        public string QueryPanel_KvValue_CentralPartsSalesOrder_Year {
            get {
                return Resources.CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_Year;
            }
        }

        public string QueryPanel_Title_CentralPartsSalesOrder {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_CentralPartsSalesOrder;
            }
        }

        public string QueryPanel_Title_CentralPartsSalesOrder_Type {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_CentralPartsSalesOrder_Type;
            }
        }

        public string Report_Title_CentralPartsSalesOrder {
            get {
                return Resources.CommonUIStrings.Report_Title_CentralPartsSalesOrder;
            }
        }

        public string QueryPanel_KvValue_CentralPartsSalesOrder_Center {
            get {
                return Resources.CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_Center;
            }
        }

        public string QueryPanel_KvValue_CentralPartsSalesOrder_CenterName {
            get {
                return Resources.CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_CenterName;
            }
        }

        public string QueryPanel_KvValue_CentralPartsSalesOrder_Dealer {
            get {
                return Resources.CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_Dealer;
            }
        }

        public string QueryPanel_KvValue_CentralPartsSalesOrder_DealerName {
            get {
                return Resources.CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_DealerName;
            }
        }

        public string QueryPanel_Title_CentralPartsSalesOrder_StatisticalUnit {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_CentralPartsSalesOrder_StatisticalUnit;
            }
        }

        public string QueryPanel_Title_CentralPartsSalesOrderForDealer {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_CentralPartsSalesOrderForDealer;
            }
        }

        public string Report_Title_CentralPartsSalesOrderForDealer {
            get {
                return Resources.CommonUIStrings.Report_Title_CentralPartsSalesOrderForDealer;
            }
        }

        public string Report_Title_CentralPartsSalesOrderForSIH {
            get {
                return Resources.CommonUIStrings.Report_Title_CentralPartsSalesOrderForSIH;
            }
        }

        public string Report_Title_CentralPartsSalesOrderForCenter {
            get {
                return Resources.CommonUIStrings.Report_Title_CentralPartsSalesOrderForCenter;
            }
        }

        public string ReportPanel_CentralPartsSalesOrderReportQueryPanel_Title {
            get {
                return Resources.CommonUIStrings.ReportPanel_CentralPartsSalesOrderReportQueryPanel_Title;
            }
        }

        public string Report_Title_CentralPartsSalesOrderForDealerReportQueryPanel {
            get {
                return Resources.CommonUIStrings.Report_Title_CentralPartsSalesOrderForDealerReportQueryPanel;
            }
        }

        public string DataGridView_ColumnItem_Title_Supplier_WaitSettlementAmount {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Title_Supplier_WaitSettlementAmount;
            }
        }

        public string DataGridView_ColumnItem_Supplier_ContactPerson {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Supplier_ContactPerson;
            }
        }

        public string DataGridView_ColumnItem_Supplier_ContactPhone {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Supplier_ContactPhone;
            }
        }

        public string DataGridView_ColumnItem_Supplier_Fax {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Supplier_Fax;
            }
        }

        public string DataGridView_ColumnItem_Supplier_ShortName {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Supplier_ShortName;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsOutboundBillCentral_ShippingCreateTime {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_ShippingCreateTime;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPackingFinish_CreateTime {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_CreateTime;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_StopAmount {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_StopAmount;
            }
        }

        public string QueryPanel_QueryItem_Title_Sparepart_IsEffective {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_Sparepart_IsEffective;
            }
        }

        public string KeyValuItem_StaySettlement {
            get {
                return Resources.CommonUIStrings.KeyValuItem_StaySettlement;
            }
        }

        public string DataEditPanel_Text_SparePart_AssemblyKeyCode {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Text_SparePart_AssemblyKeyCode;
            }
        }

        public string DataEditView_Title_AssemblyPartRequisitionLink {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_AssemblyPartRequisitionLink;
            }
        }

        public string DataEditView_Title_ImportAssemblyPartRequisitionLink {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_ImportAssemblyPartRequisitionLink;
            }
        }

        public string DataEditView_Validation_KeyCodeIsNull {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_KeyCodeIsNull;
            }
        }

        public string DataEditView_Validation_QtyGreaterThanZero {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_QtyGreaterThanZero;
            }
        }

        public string DataEditView_Validation_QtyIsNull {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_QtyIsNull;
            }
        }

        public string DataManagementView_Title_AssemblyPartRequisitionLink {
            get {
                return Resources.CommonUIStrings.DataManagementView_Title_AssemblyPartRequisitionLink;
            }
        }

        public string QueryPanel_Column_KeyCode {
            get {
                return Resources.CommonUIStrings.QueryPanel_Column_KeyCode;
            }
        }

        public string QueryPanel_Title_AssemblyPartRequisitionLink {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_AssemblyPartRequisitionLink;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPriorityBill_Priority {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsPriorityBill_Priority;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_RetailStatus {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_RetailStatus;
            }
        }

        public string QueryPanel_Title_SaleOrderTerminalDetail {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_SaleOrderTerminalDetail;
            }
        }

        public string QueryPanel_Title_SaleOrderTerminalDetailQuery {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_SaleOrderTerminalDetailQuery;
            }
        }

        public string Report_QueryPanel_Title_DealerNames {
            get {
                return Resources.CommonUIStrings.Report_QueryPanel_Title_DealerNames;
            }
        }

        public string Report_QueryPanel_Title_SubmitTime {
            get {
                return Resources.CommonUIStrings.Report_QueryPanel_Title_SubmitTime;
            }
        }

        public string DataGridCiew_Title_PartsSales_AB {
            get {
                return Resources.CommonUIStrings.DataGridCiew_Title_PartsSales_AB;
            }
        }

        public string DataGridCiew_Title_PartsSales_AbRate {
            get {
                return Resources.CommonUIStrings.DataGridCiew_Title_PartsSales_AbRate;
            }
        }

        public string DataGridCiew_Title_PartsSales_EmergencyOrderNum {
            get {
                return Resources.CommonUIStrings.DataGridCiew_Title_PartsSales_EmergencyOrderNum;
            }
        }

        public string DataGridCiew_Title_PartsSales_EmergencyRate {
            get {
                return Resources.CommonUIStrings.DataGridCiew_Title_PartsSales_EmergencyRate;
            }
        }

        public string DataGridCiew_Title_PartsSales_ExEmergencyOrderNum {
            get {
                return Resources.CommonUIStrings.DataGridCiew_Title_PartsSales_ExEmergencyOrderNum;
            }
        }

        public string DataGridCiew_Title_PartsSales_OrderAmount {
            get {
                return Resources.CommonUIStrings.DataGridCiew_Title_PartsSales_OrderAmount;
            }
        }

        public string DataGridCiew_Title_PartsSales_TerminalRate {
            get {
                return Resources.CommonUIStrings.DataGridCiew_Title_PartsSales_TerminalRate;
            }
        }

        public string QueryPanel_Title_QueryItem_Dealer {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_QueryItem_Dealer;
            }
        }

        public string QueryPanel_Title_QueryItem_Year {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_QueryItem_Year;
            }
        }

        public string QueryPanel_Title_SaleOrderTerminalRateSumForReport {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_SaleOrderTerminalRateSumForReport;
            }
        }

        public string QueryPanel_Title_SaleOrderTerminalRateSumForReportQuery {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_SaleOrderTerminalRateSumForReportQuery;
            }
        }

        public string QueryPanel_KvValue_CentralPartsSalesOrder_ActualStock {
            get {
                return Resources.CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_ActualStock;
            }
        }

        public string QueryPanel_KvValue_CentralPartsSalesOrder_BottomStock {
            get {
                return Resources.CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_BottomStock;
            }
        }

        public string QueryPanel_KvValue_CentralPartsSalesOrder_DealerCode {
            get {
                return Resources.CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_DealerCode;
            }
        }

        public string QueryPanel_KvValue_CentralPartsSalesOrder_DIfference {
            get {
                return Resources.CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_DIfference;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SettleBillDate {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_SettleBillDate;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_ApproveQty {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_ApproveQty;
            }
        }

        public string QueryPanel_Title_BottomStockSettleDetailForReport {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_BottomStockSettleDetailForReport;
            }
        }

        public string QueryPanel_Title_BottomStockSettleDetailForReportQuery {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_BottomStockSettleDetailForReportQuery;
            }
        }

        public string QueryPanel_KvValue_CentralPartsSalesOrder_Week {
            get {
                return Resources.CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_Week;
            }
        }

        public string Report_Title_BottomStockSettleTable_AccountFee {
            get {
                return Resources.CommonUIStrings.Report_Title_BottomStockSettleTable_AccountFee;
            }
        }

        public string Report_Title_BottomStockSettleTable_AccountSpeciesNum {
            get {
                return Resources.CommonUIStrings.Report_Title_BottomStockSettleTable_AccountSpeciesNum;
            }
        }

        public string Report_Title_BottomStockSettleTable_BottomCoverage {
            get {
                return Resources.CommonUIStrings.Report_Title_BottomStockSettleTable_BottomCoverage;
            }
        }

        public string Report_Title_BottomStockSettleTable_DifferSpeciesNum {
            get {
                return Resources.CommonUIStrings.Report_Title_BottomStockSettleTable_DifferSpeciesNum;
            }
        }

        public string QueryPanel_Title_BottomStockSettleForReport {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_BottomStockSettleForReport;
            }
        }

        public string QueryPanel_Title_BottomStockSettleForReportQuery {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_BottomStockSettleForReportQuery;
            }
        }

        public string DataGridView_ColumnItem_Title_FirPackingMaterial {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Title_FirPackingMaterial;
            }
        }

        public string DataGridView_ColumnItem_Title_SecPackingMaterial {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Title_SecPackingMaterial;
            }
        }

        public string DataGridView_ColumnItem_Title_ThidPackingMaterial {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Title_ThidPackingMaterial;
            }
        }

        public string Action_Title_Upload_IsSupplier {
            get {
                return Resources.CommonUIStrings.Action_Title_Upload_IsSupplier;
            }
        }

        public string DataEditPanel_Title_ImportIsSupplierPutIn {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Title_ImportIsSupplierPutIn;
            }
        }

        public string DataGridView_QueryItem_Title_PartsPackingFinish_PurchaseName {
            get {
                return Resources.CommonUIStrings.DataGridView_QueryItem_Title_PartsPackingFinish_PurchaseName;
            }
        }

        public string DataGridView_Title_PartsPurchaseInfoSummary_BottomUseQty {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_BottomUseQty;
            }
        }

        public string DataGridView_Title_PartsPurchaseInfoSummary_Daily {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_Daily;
            }
        }

        public string DataGridView_Title_PartsPurchaseInfoSummary_DurativeDayNum {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_DurativeDayNum;
            }
        }

        public string DataGridView_Title_PartsPurchaseInfoSummary_OverTimeOnWay {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_OverTimeOnWay;
            }
        }

        public string DataGridView_Title_PartsPurchaseInfoSummary_OwerNum {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_OwerNum;
            }
        }

        public string DataGridView_Title_PartsPurchaseInfoSummary_OwerOrder {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_OwerOrder;
            }
        }

        public string DataGridView_Title_PartsPurchaseInfoSummary_PriorityCreateTime {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_PriorityCreateTime;
            }
        }

        public string DataGridView_Title_PartsPurchaseInfoSummary_QtyLowerLimit {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_QtyLowerLimit;
            }
        }

        public string DataGridView_Title_PartsPurchaseInfoSummary_TotalDurativeDayNum {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_TotalDurativeDayNum;
            }
        }

        public string DataGridView_Title_PartsPurchaseInfoSummary_UnInbound {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_UnInbound;
            }
        }

        public string DataGridView_Title_PartsPurchaseInfoSummary_Unpack {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_Unpack;
            }
        }

        public string DataGridView_Title_PartsPurchaseInfoSummary_UnShelve {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_UnShelve;
            }
        }

        public string DataGridView_Title_PartsPurchaseInfoSummary_UseQty {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_UseQty;
            }
        }

        public string DataGridView_Title_PartsPurchaseInfoSummary_Onway {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_PartsPurchaseInfoSummary_Onway;
            }
        }

        public string QueryPanel_Title_PartsPurchaseInfoSummaryForReport {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_PartsPurchaseInfoSummaryForReport;
            }
        }

        public string DataGridView_Title_PartsPurchase_ActualInboundEntries {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_PartsPurchase_ActualInboundEntries;
            }
        }

        public string DataGridView_Title_PartsPurchase_ShallInboundEntries {
            get {
                return Resources.CommonUIStrings.DataGridView_Title_PartsPurchase_ShallInboundEntries;
            }
        }

        public string QueryPanel_QueryItem_Title_DealerPartsInboundBill_ShouldQuantity {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_DealerPartsInboundBill_ShouldQuantity;
            }
        }

        public string Report_QueryPanel_KeyValue_Finishi {
            get {
                return Resources.CommonUIStrings.Report_QueryPanel_KeyValue_Finishi;
            }
        }

        public string Report_QueryPanel_KeyValue_InBoundPlanCode {
            get {
                return Resources.CommonUIStrings.Report_QueryPanel_KeyValue_InBoundPlanCode;
            }
        }

        public string Report_QueryPanel_KeyValue_InBoundStatus {
            get {
                return Resources.CommonUIStrings.Report_QueryPanel_KeyValue_InBoundStatus;
            }
        }

        public string Report_QueryPanel_KeyValue_PartFinishi {
            get {
                return Resources.CommonUIStrings.Report_QueryPanel_KeyValue_PartFinishi;
            }
        }

        public string Report_QueryPanel_KeyValue_UnFinishi {
            get {
                return Resources.CommonUIStrings.Report_QueryPanel_KeyValue_UnFinishi;
            }
        }

        public string QueryPanel_Title_PartsPurchaseDetailForReport {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_PartsPurchaseDetailForReport;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPackingFinish_ActPackingItme {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_ActPackingItme;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPackingFinish_PackingItme {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_PackingItme;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPackingFinish_PackingStatus {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_PackingStatus;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPackingFinish_PrioityPackingTime {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsPackingFinish_PrioityPackingTime;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsShelvesTaskFinish_ActShelvesItem {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsShelvesTaskFinish_ActShelvesItem;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsShelvesTaskFinish_PrioityShelvesTime {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsShelvesTaskFinish_PrioityShelvesTime;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsShelvesTaskFinish_ShelvesFinishStatus {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsShelvesTaskFinish_ShelvesFinishStatus;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsShelvesTaskFinish_ShelvesItem {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsShelvesTaskFinish_ShelvesItem;
            }
        }

        public string DataGridView_ActStorageNumber {
            get {
                return Resources.CommonUIStrings.DataGridView_ActStorageNumber;
            }
        }

        public string DataGridView_ActStorageNumberItem {
            get {
                return Resources.CommonUIStrings.DataGridView_ActStorageNumberItem;
            }
        }

        public string DataGridView_CompletionEntryRate {
            get {
                return Resources.CommonUIStrings.DataGridView_CompletionEntryRate;
            }
        }

        public string DataGridView_CompletionRate {
            get {
                return Resources.CommonUIStrings.DataGridView_CompletionRate;
            }
        }

        public string DataGridView_PackedNumber {
            get {
                return Resources.CommonUIStrings.DataGridView_PackedNumber;
            }
        }

        public string DataGridView_PackingNumber {
            get {
                return Resources.CommonUIStrings.DataGridView_PackingNumber;
            }
        }

        public string DataGridView_PackItemRate {
            get {
                return Resources.CommonUIStrings.DataGridView_PackItemRate;
            }
        }

        public string DataGridView_PackRate {
            get {
                return Resources.CommonUIStrings.DataGridView_PackRate;
            }
        }

        public string DataGridView_ShelvedNumber {
            get {
                return Resources.CommonUIStrings.DataGridView_ShelvedNumber;
            }
        }

        public string DataGridView_ShelveItemRate {
            get {
                return Resources.CommonUIStrings.DataGridView_ShelveItemRate;
            }
        }

        public string DataGridView_ShelveNumber {
            get {
                return Resources.CommonUIStrings.DataGridView_ShelveNumber;
            }
        }

        public string DataGridView_ShelveRate {
            get {
                return Resources.CommonUIStrings.DataGridView_ShelveRate;
            }
        }

        public string DataGridView_StorageNumber {
            get {
                return Resources.CommonUIStrings.DataGridView_StorageNumber;
            }
        }

        public string DataGridView_StorageNumberItem {
            get {
                return Resources.CommonUIStrings.DataGridView_StorageNumberItem;
            }
        }

        public string QueryPanel_Title_PriorityPackingShelvesDetailForReport {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_PriorityPackingShelvesDetailForReport;
            }
        }

        public string QueryPanel_Title_PriorityPackingShelvesDetailForReportQuery {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_PriorityPackingShelvesDetailForReportQuery;
            }
        }

        public string QueryPanel_Title_PriorityPackingShelvesSumForReport {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_PriorityPackingShelvesSumForReport;
            }
        }

        public string QueryPanel_Title_PriorityPackingShelvesSumForReportQuery {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_PriorityPackingShelvesSumForReportQuery;
            }
        }

        public string DataEditView_Text_PartsSupplierRelation_OrderGoodsCycle {
            get {
                return Resources.CommonUIStrings.DataEditView_Text_PartsSupplierRelation_OrderGoodsCycle;
            }
        }

        public string DataEditView_Validation_PartsSupplierRelation__OrderGoodsCycle {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_PartsSupplierRelation__OrderGoodsCycle;
            }
        }

        public string QueryPanel_QueryItem_Title_AllSparePart_OrderGoodsCycle {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_OrderGoodsCycle;
            }
        }

        public string Report_Title_InOutSettleSummary_InPurchaseListAll {
            get {
                return Resources.CommonUIStrings.Report_Title_InOutSettleSummary_InPurchaseListAll;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_PersonName {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_PersonName;
            }
        }

        public string DataEditView_Column_Agency_ArrivalCycle {
            get {
                return Resources.CommonUIStrings.DataEditView_Column_Agency_ArrivalCycle;
            }
        }

        public string DataEditView_Column_Agency_OrderCycle {
            get {
                return Resources.CommonUIStrings.DataEditView_Column_Agency_OrderCycle;
            }
        }

        public string DataEditView_Column_Agency_ShippingCycle {
            get {
                return Resources.CommonUIStrings.DataEditView_Column_Agency_ShippingCycle;
            }
        }

        public string DataEditView_Validation_Agency_ArrivalCycle {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_Agency_ArrivalCycle;
            }
        }

        public string DataEditView_Validation_Agency_OrderCycle {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_Agency_OrderCycle;
            }
        }

        public string DataEditView_Validation_Agency_ShippingCycle {
            get {
                return Resources.CommonUIStrings.DataEditView_Validation_Agency_ShippingCycle;
            }
        }

        public string Report_Title_SaleAdjustment_AdjustAmount {
            get {
                return Resources.CommonUIStrings.Report_Title_SaleAdjustment_AdjustAmount;
            }
        }

        public string Report_Title_SaleAdjustment_AdjustQty {
            get {
                return Resources.CommonUIStrings.Report_Title_SaleAdjustment_AdjustQty;
            }
        }

        public string Report_Title_SaleAdjustment_AdjustStartTime {
            get {
                return Resources.CommonUIStrings.Report_Title_SaleAdjustment_AdjustStartTime;
            }
        }

        public string Report_Title_SaleAdjustment_AdjustTime {
            get {
                return Resources.CommonUIStrings.Report_Title_SaleAdjustment_AdjustTime;
            }
        }

        public string Report_Title_SaleAdjustment_AdjustTotalAmount {
            get {
                return Resources.CommonUIStrings.Report_Title_SaleAdjustment_AdjustTotalAmount;
            }
        }

        public string Report_Title_SaleAdjustment_AgencyName {
            get {
                return Resources.CommonUIStrings.Report_Title_SaleAdjustment_AgencyName;
            }
        }

        public string Report_Title_SaleAdjustment_BatchNumber {
            get {
                return Resources.CommonUIStrings.Report_Title_SaleAdjustment_BatchNumber;
            }
        }

        public string Report_Title_SaleAdjustment_CustomerCode {
            get {
                return Resources.CommonUIStrings.Report_Title_SaleAdjustment_CustomerCode;
            }
        }

        public string Report_Title_SaleAdjustment_CustomerName {
            get {
                return Resources.CommonUIStrings.Report_Title_SaleAdjustment_CustomerName;
            }
        }

        public string Report_Title_SaleAdjustment_CustomerType {
            get {
                return Resources.CommonUIStrings.Report_Title_SaleAdjustment_CustomerType;
            }
        }

        public string Report_Title_SaleAdjustment_Difference {
            get {
                return Resources.CommonUIStrings.Report_Title_SaleAdjustment_Difference;
            }
        }

        public string Report_Title_SaleAdjustment_DifferenceRate {
            get {
                return Resources.CommonUIStrings.Report_Title_SaleAdjustment_DifferenceRate;
            }
        }

        public string Report_Title_SaleAdjustment_OldPrice {
            get {
                return Resources.CommonUIStrings.Report_Title_SaleAdjustment_OldPrice;
            }
        }

        public string Report_Title_SaleAdjustment_OnLineQty {
            get {
                return Resources.CommonUIStrings.Report_Title_SaleAdjustment_OnLineQty;
            }
        }

        public string Report_Title_SaleAdjustment_PastThreeMonthsPruchase {
            get {
                return Resources.CommonUIStrings.Report_Title_SaleAdjustment_PastThreeMonthsPruchase;
            }
        }

        public string Report_Title_SaleAdjustment_Price {
            get {
                return Resources.CommonUIStrings.Report_Title_SaleAdjustment_Price;
            }
        }

        public string Report_Title_SaleAdjustment_SparePartCode {
            get {
                return Resources.CommonUIStrings.Report_Title_SaleAdjustment_SparePartCode;
            }
        }

        public string Report_Title_SaleAdjustment_SparePartName {
            get {
                return Resources.CommonUIStrings.Report_Title_SaleAdjustment_SparePartName;
            }
        }

        public string Report_Title_SaleAdjustment_UsableQty {
            get {
                return Resources.CommonUIStrings.Report_Title_SaleAdjustment_UsableQty;
            }
        }

        public string QueryPanel_KvValue_CentralPartsSalesOrder_DifferenceMoney {
            get {
                return Resources.CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_DifferenceMoney;
            }
        }

        public string Report_QueryPanel_Title_OverZero {
            get {
                return Resources.CommonUIStrings.Report_QueryPanel_Title_OverZero;
            }
        }

        public string QueryPanel_Title_CenterStorehouseCensusReport {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_CenterStorehouseCensusReport;
            }
        }

        public string QueryPanel_Title_CenterStorehouseCensusReportQuery {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_CenterStorehouseCensusReportQuery;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchasePlanReport_SubmitTime {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlanReport_SubmitTime;
            }
        }

        public string Error_WorkGruopPersonNumber_EmployeeNumber {
            get {
                return Resources.CommonUIStrings.Error_WorkGruopPersonNumber_EmployeeNumber;
            }
        }

        public string Error_WorkGruopPersonNumber_WorkGruop {
            get {
                return Resources.CommonUIStrings.Error_WorkGruopPersonNumber_WorkGruop;
            }
        }

        public string Report_Title_WorkGruopPersonNumberManagement {
            get {
                return Resources.CommonUIStrings.Report_Title_WorkGruopPersonNumberManagement;
            }
        }

        public string Report_Title_WorkGruopPersonNumberManagementQuery {
            get {
                return Resources.CommonUIStrings.Report_Title_WorkGruopPersonNumberManagementQuery;
            }
        }

        public string Report_Title_WorkGruopPersonNumber_EmployeeNumber {
            get {
                return Resources.CommonUIStrings.Report_Title_WorkGruopPersonNumber_EmployeeNumber;
            }
        }

        public string Report_Title_WorkGruopPersonNumber_WorkGruop {
            get {
                return Resources.CommonUIStrings.Report_Title_WorkGruopPersonNumber_WorkGruop;
            }
        }

        public string QueryPanel_Title_InOutWorkPerHour {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_InOutWorkPerHour;
            }
        }

        public string QueryPanel_Title_InOutWorkPerHourQuery {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_InOutWorkPerHourQuery;
            }
        }

        public string Report_Title_InOutSettleSummary_UnShelve {
            get {
                return Resources.CommonUIStrings.Report_Title_InOutSettleSummary_UnShelve;
            }
        }

        public string Report_Title_InOutWorkPerHour_OrderLineNum {
            get {
                return Resources.CommonUIStrings.Report_Title_InOutWorkPerHour_OrderLineNum;
            }
        }

        public string Report_Title_InOutWorkPerHour_PersonNum {
            get {
                return Resources.CommonUIStrings.Report_Title_InOutWorkPerHour_PersonNum;
            }
        }

        public string Report_Title_InOutWorkPerHour_TimeFrame {
            get {
                return Resources.CommonUIStrings.Report_Title_InOutWorkPerHour_TimeFrame;
            }
        }

        public string QueryPanel_KvValue_CentralPartsSalesOrder_CreateTime {
            get {
                return Resources.CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_CreateTime;
            }
        }

        public string QueryPanel_KvValue_CentralPartsSalesOrder_NewType {
            get {
                return Resources.CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_NewType;
            }
        }

        public string QueryPanel_Title_CenterStockSummary {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_CenterStockSummary;
            }
        }

        public string QueryPanel_Title_CenterStockSummary_Query {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_CenterStockSummary_Query;
            }
        }

        public string Report_DataDeTailPanel_Text_BottomStock_Entries {
            get {
                return Resources.CommonUIStrings.Report_DataDeTailPanel_Text_BottomStock_Entries;
            }
        }

        public string Error_PartsPriceTimeLimit_TimeLimit {
            get {
                return Resources.CommonUIStrings.Error_PartsPriceTimeLimit_TimeLimit;
            }
        }

        public string Error_PartsPriceTimeLimit_Type {
            get {
                return Resources.CommonUIStrings.Error_PartsPriceTimeLimit_Type;
            }
        }

        public string Report_Title_PartsPriceTimeLimitManagement {
            get {
                return Resources.CommonUIStrings.Report_Title_PartsPriceTimeLimitManagement;
            }
        }

        public string Report_Title_PartsPriceTimeLimitManagement_Query {
            get {
                return Resources.CommonUIStrings.Report_Title_PartsPriceTimeLimitManagement_Query;
            }
        }

        public string Report_Title_PartsPriceTimeLimit_TimeLimit {
            get {
                return Resources.CommonUIStrings.Report_Title_PartsPriceTimeLimit_TimeLimit;
            }
        }

        public string Report_Title_PartsPriceTimeLimit_Type {
            get {
                return Resources.CommonUIStrings.Report_Title_PartsPriceTimeLimit_Type;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_AllItems {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_AllItems;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_ArriveItems {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_ArriveItems;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_EightUns {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_EightUns;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_OnTimes {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_OnTimes;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_OutTimes {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_OutTimes;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_OverTwentyUns {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_OverTwentyUns;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_SevenUns {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_SevenUns;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_StopAmounts {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_StopAmounts;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_TwentyUns {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_TwentyUns;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_Undues {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_Undues;
            }
        }

        public string DataManagementView_Title_BottomStockForceReserveType {
            get {
                return Resources.CommonUIStrings.DataManagementView_Title_BottomStockForceReserveType;
            }
        }

        public string QueryPanel_QueryItem_Title_BottomStockForceReserveType_ReserveType {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_BottomStockForceReserveType_ReserveType;
            }
        }

        public string QueryPanel_Title_BottomStockForceReserveType {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_BottomStockForceReserveType;
            }
        }

        public string DataManagementView_Title_BottomStockForceReserveSub {
            get {
                return Resources.CommonUIStrings.DataManagementView_Title_BottomStockForceReserveSub;
            }
        }

        public string QueryPanel_QueryItem_Title_BottomStockForceReserveSub_SubCode {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_BottomStockForceReserveSub_SubCode;
            }
        }

        public string QueryPanel_QueryItem_Title_BottomStockForceReserveSub_SubName {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_BottomStockForceReserveSub_SubName;
            }
        }

        public string QueryPanel_Title_BottomStockForceReserveSub {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_BottomStockForceReserveSub;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsBranch_CheckerName {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_CheckerName;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsBranch_CheckTime {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_CheckTime;
            }
        }

        public string QueryPanel_QueryItem_Title_ForceReserveBill_ColVersionCode {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_ForceReserveBill_ColVersionCode;
            }
        }

        public string QueryPanel_QueryItem_Title_ForceReserveBill_SubVersionCode {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_ForceReserveBill_SubVersionCode;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsBranch_ApproverName {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_ApproverName;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsBranch_ApproveTime {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_ApproveTime;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsBranch_RejecterName {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_RejecterName;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsBranch_RejectTime {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_RejectTime;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsBranch_ReserveTypeSubItem {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_ReserveTypeSubItem;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchasePricing_CenterPartProperty {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_CenterPartProperty;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchasePricing_CenterPrice {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_CenterPrice;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchasePricing_ForceReserveQty {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_ForceReserveQty;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchasePricing_ReserveFee {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_ReserveFee;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchasePricing_SuggestForceReserveQty {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_SuggestForceReserveQty;
            }
        }

        public string QueryPanel_Title_ForceReserveBillCenter {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_ForceReserveBillCenter;
            }
        }

        public string DataManagementView_Title_ForceReserveBillCenter {
            get {
                return Resources.CommonUIStrings.DataManagementView_Title_ForceReserveBillCenter;
            }
        }

        public string DataManagementView_Title_ForceReserveBillDealer {
            get {
                return Resources.CommonUIStrings.DataManagementView_Title_ForceReserveBillDealer;
            }
        }

        public string Report_Title_BottomStockSubVersion {
            get {
                return Resources.CommonUIStrings.Report_Title_BottomStockSubVersion;
            }
        }

        public string Report_Title_BottomStockColVersion {
            get {
                return Resources.CommonUIStrings.Report_Title_BottomStockColVersion;
            }
        }

        public string DataEditView_Title_ImportBottomStockAbandon {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_ImportBottomStockAbandon;
            }
        }

        public string Report_QueryPanel_Title_Quarter {
            get {
                return Resources.CommonUIStrings.Report_QueryPanel_Title_Quarter;
            }
        }

        public string Action_PartsPurchasePricingChange {
            get {
                return Resources.CommonUIStrings.Action_PartsPurchasePricingChange;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_Jemzl {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_Jemzl;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_Tmmzl {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_Tmmzl;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_Totalfee {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_Totalfee;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_Totalpz {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_Totalpz;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_Ycfee {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_Ycfee;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_Ycpzs {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_Ycpzs;
            }
        }

        public string Action_Title_Upload_ReplaceTraceProperty {
            get {
                return Resources.CommonUIStrings.Action_Title_Upload_ReplaceTraceProperty;
            }
        }

        public string Action_Title_Bottom_Edit {
            get {
                return Resources.CommonUIStrings.Action_Title_Bottom_Edit;
            }
        }

        public string Action_Title_Import_Abandon {
            get {
                return Resources.CommonUIStrings.Action_Title_Import_Abandon;
            }
        }

        public string Action_Title_Replace_Confirm {
            get {
                return Resources.CommonUIStrings.Action_Title_Replace_Confirm;
            }
        }

        public string Action_Title_Uploading {
            get {
                return Resources.CommonUIStrings.Action_Title_Uploading;
            }
        }

        public string Action_Title_Upload_Success {
            get {
                return Resources.CommonUIStrings.Action_Title_Upload_Success;
            }
        }

        public string DataEditPanel_Validation_Notification_UploadError2 {
            get {
                return Resources.CommonUIStrings.DataEditPanel_Validation_Notification_UploadError2;
            }
        }

        public string DataEditView_Notification_BottomTitle {
            get {
                return Resources.CommonUIStrings.DataEditView_Notification_BottomTitle;
            }
        }

        public string DataEditView_Notification_Type_Center {
            get {
                return Resources.CommonUIStrings.DataEditView_Notification_Type_Center;
            }
        }

        public string DataEditView_Notification_Type_Dealer {
            get {
                return Resources.CommonUIStrings.DataEditView_Notification_Type_Dealer;
            }
        }

        public string DataEditView_Title_DealerFormateQuery {
            get {
                return Resources.CommonUIStrings.DataEditView_Title_DealerFormateQuery;
            }
        }

        public string Action_Title_Price_Lower {
            get {
                return Resources.CommonUIStrings.Action_Title_Price_Lower;
            }
        }

        public string Action_Title_Price_Maintain {
            get {
                return Resources.CommonUIStrings.Action_Title_Price_Maintain;
            }
        }

        public string Action_Title_Price_Temporary {
            get {
                return Resources.CommonUIStrings.Action_Title_Price_Temporary;
            }
        }

        public string Management_Validatation_Time {
            get {
                return Resources.CommonUIStrings.Management_Validatation_Time;
            }
        }

        public string Management_Validatation_Year {
            get {
                return Resources.CommonUIStrings.Management_Validatation_Year;
            }
        }

        public string QueryItem_Title_PartsOutboundBillCentral_ConfirmAmount {
            get {
                return Resources.CommonUIStrings.QueryItem_Title_PartsOutboundBillCentral_ConfirmAmount;
            }
        }

        public string QueryPanel_KvValue_CentralPartsSalesOrder_DifferenceMoney2 {
            get {
                return Resources.CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_DifferenceMoney2;
            }
        }

        public string QueryPanel_KvValue_CentralPartsSalesOrder_DifferenceQty {
            get {
                return Resources.CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_DifferenceQty;
            }
        }

        public string QueryPanel_MultiLevelApproveConfig_Title_Query {
            get {
                return Resources.CommonUIStrings.QueryPanel_MultiLevelApproveConfig_Title_Query;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsOutboundBillCentral_CloserName {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_CloserName;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsOutboundBillCentral_InspectedQuantity {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_InspectedQuantity;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsOutboundBillCentral_OrderShippingTime {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_OrderShippingTime;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsOutboundBillCentral_PlanAmount {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_PlanAmount;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsOutboundBillCentral_Quantity {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_Quantity;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsOutboundBillCentral_ShipCon {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_ShipCon;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsOutboundBillCentral_ShortSupReason {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_ShortSupReason;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_ConfirmationTime {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_ConfirmationTime;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_ExpectedArrivalDate {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_ExpectedArrivalDate;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_SIHInboundDate {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_SIHInboundDate;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchasePricing_OriginalRequirementBillCode {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_OriginalRequirementBillCode;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_ReturnReason {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_ReturnReason;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsShippingOrderForReport_DeliveryBillNumber2 {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrderForReport_DeliveryBillNumber2;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsShippingOrderForReport_LinkNumber {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrderForReport_LinkNumber;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsShippingOrderForReport_ShipRemark {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrderForReport_ShipRemark;
            }
        }

        public string QueryPanel_Title_PartsPurchaseOrder {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_PartsPurchaseOrder;
            }
        }

        public string QueryPanel_Title_PartsPurchaseOrder_Finish {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_PartsPurchaseOrder_Finish;
            }
        }

        public string QueryPanel_Title_PartsPurchaseOrder_IsTransSap {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_PartsPurchaseOrder_IsTransSap;
            }
        }

        public string QueryPanel_Title_PartsPurchaseOrder_PartsSalesOrderTypeName {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_PartsPurchaseOrder_PartsSalesOrderTypeName;
            }
        }

        public string QueryPanel_Title_PartsPurchaseOrder_Status {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_PartsPurchaseOrder_Status;
            }
        }

        public string QueryPanel_Title_PartsPurchaseOrder_Status1 {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_PartsPurchaseOrder_Status1;
            }
        }

        public string QueryPanel_Title_PartsPurchaseOrder_Status2 {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_PartsPurchaseOrder_Status2;
            }
        }

        public string QueryPanel_Title_PartsPurchaseOrder_Status3 {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_PartsPurchaseOrder_Status3;
            }
        }

        public string QueryPanel_Title_PartsPurchaseOrder_Status4 {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_PartsPurchaseOrder_Status4;
            }
        }

        public string QueryPanel_Title_PartsPurchaseOrder_Status5 {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_PartsPurchaseOrder_Status5;
            }
        }

        public string QueryPanel_Title_Price {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_Price;
            }
        }

        public string QueryPanel_Title_SaleAdjustmentReport {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_SaleAdjustmentReport;
            }
        }

        public string QueryPanel_Title_SaleAdjustmentReportAll {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_SaleAdjustmentReportAll;
            }
        }

        public string QueryPanel_Title_SaleAdjustmentReportQuery {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_SaleAdjustmentReportQuery;
            }
        }

        public string QueryPanel_Title_SaleAdjustmentReportQueryAll {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_SaleAdjustmentReportQueryAll;
            }
        }

        public string QueryPanel_Title_SaleAdjustmentReport_AdjustTime {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_SaleAdjustmentReport_AdjustTime;
            }
        }

        public string QueryPane_Title_Price_Type {
            get {
                return Resources.CommonUIStrings.QueryPane_Title_Price_Type;
            }
        }

        public string Query_ReserveType_Validation {
            get {
                return Resources.CommonUIStrings.Query_ReserveType_Validation;
            }
        }

        public string Query_Type {
            get {
                return Resources.CommonUIStrings.Query_Type;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsBranch_SubmitterName {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_SubmitterName;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsBranch_SubmitTime {
            get {
                return Resources.CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_SubmitTime;
            }
        }

        public string QueryPanel_QueryItem_Title_BottomStockSubVersion_SalePrice {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_BottomStockSubVersion_SalePrice;
            }
        }

        public string Action_Title_Batch_Import_StockMinimum {
            get {
                return Resources.CommonUIStrings.Action_Title_Batch_Import_StockMinimum;
            }
        }

        public string QueryPanel_Title_PartsPurchasePlanTempForReport {
            get {
                return Resources.CommonUIStrings.QueryPanel_Title_PartsPurchasePlanTempForReport;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_OrderAmount {
            get {
                return Resources.CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrderFinish_OrderAmount;
            }
        }       

        public string Action_Title_AdvancedAudit {
            get {
                return Resources.CommonUIStrings.Action_Title_AdvancedAudit;
            }
        }

    }
}
