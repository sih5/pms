﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("Common", "Company", "Agency", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_PAUSE_RESUME_DETAIL_EXPORTDETAIL,"Agency"
    })]
    public class AgencyManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataImportEditView;
        private DataEditViewBase dataEditViewDetail;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private const string DATA_EDIT_VIEW_DETAIL = "_DataEditViewDetail_";
        private const string DATA_EDIT_VIEW_IMPORT = "_DataEditViewImport_";

        public AgencyManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataManagementView_Title_Agency;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("Agency"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("Agency");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private DataEditViewBase DataImportEditView {
            get {
                if(this.dataImportEditView == null) {
                    this.dataImportEditView = DI.GetDataEditView("AgencyImport");
                    this.dataImportEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataImportEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataImportEditView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_DETAIL, () => this.DataEditViewDetail);
            this.RegisterView(DATA_EDIT_VIEW_IMPORT, () => this.DataImportEditView);
        }

        private DataEditViewBase DataEditViewDetail {
            get {
                if(this.dataEditViewDetail == null) {
                    this.dataEditViewDetail = DI.GetDataEditView("AgencyDetail");
                    this.dataEditViewDetail.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditViewDetail.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewDetail;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewDetail = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            //            var newCompositeFilter = new CompositeFilterItem();
            //            if(filterItem is CompositeFilterItem) {
            //                var compositeFilter = filterItem as CompositeFilterItem;
            //                var createTimeFilters = compositeFilter.Filters.FirstOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
            //                if(createTimeFilters != null) {
            //                    var dateTime = createTimeFilters.Filters.ElementAt(1).Value as DateTime?;
            //                    if(dateTime.HasValue)
            //                        createTimeFilters.Filters.ElementAt(1).Value = new DateTime(dateTime.Value.Year, dateTime.Value.Month, dateTime.Value.Day, 23, 59, 59);
            //                }
            //                newCompositeFilter.Filters.Add(compositeFilter);
            //            } else
            //                newCompositeFilter.Filters.Add(filterItem);
            //            this.DataGridView.FilterItem = newCompositeFilter;
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "Agency"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var agency = this.DataEditView.CreateObjectToEdit<Agency>();
                    agency.Status = (int)DcsMasterDataStatus.有效;
                    agency.Company = new Company();
                    agency.Company.Type = (int)DcsCompanyType.代理库;
                    agency.Company.Status = (int)DcsMasterDataStatus.有效;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORT);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<Agency>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废代理库)
                                entity.作废代理库();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.PAUSE:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Pause, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<Agency>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can停用代理库)
                                entity.停用代理库();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_PauseSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.RESUME:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Resume, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<Agency>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can恢复代理库)
                                entity.恢复代理库();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_ResumeSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.DETAIL:
                    this.DataEditViewDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_DETAIL);
                    break;
                case CommonActionKeys.EXPORTDETAIL:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var agencys = this.DataGridView.SelectedEntities;
                    if(agencys != null) {
                        if(agencys.Any()) {
                            var agencyId = this.DataGridView.SelectedEntities.First().GetIdentity() as int?;
                            ShellViewModel.Current.IsBusy = true;
                            this.ExportAgencyDetail(null, null, null, agencyId);
                        } else {
                            var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                            var name = filterItem.Filters.Single(r => r.MemberName == "Name").Value as string;
                            var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                            ShellViewModel.Current.IsBusy = true;
                            this.ExportAgencyDetail(code, name, status, null);
                        }
                    } else {
                        var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var name = filterItem.Filters.Single(r => r.MemberName == "Name").Value as string;
                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        ShellViewModel.Current.IsBusy = true;
                        this.ExportAgencyDetail(code, name, status, null);
                    }

                    break;
            }
        }

        private void ExportAgencyDetail(string code, string name, int? status, int? agencyId) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportAgencyDetailAsync(name, code, status, agencyId);
            this.excelServiceClient.ExportAgencyDetailCompleted -= excelServiceClient_ExportAgencyDetailCompleted;
            this.excelServiceClient.ExportAgencyDetailCompleted += excelServiceClient_ExportAgencyDetailCompleted;
        }

        private void excelServiceClient_ExportAgencyDetailCompleted(object sender, ExportAgencyDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                case CommonActionKeys.IMPORT:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.PAUSE:
                case CommonActionKeys.RESUME:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<Agency>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    if((String.CompareOrdinal(uniqueId, CommonActionKeys.EDIT) == 0) || (String.CompareOrdinal(uniqueId, CommonActionKeys.ABANDON) == 0) || (String.CompareOrdinal(uniqueId, CommonActionKeys.PAUSE) == 0))
                        return entities[0].Status == (int)DcsMasterDataStatus.有效;
                    return (entities[0].Status == (int)DcsMasterDataStatus.停用);
                case CommonActionKeys.EXPORTDETAIL:
                    return true;
                case CommonActionKeys.DETAIL:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitie = this.DataGridView.SelectedEntities.Cast<Agency>().ToArray();
                    if(entitie.Length != 1)
                        return false;
                    return true;
                default:
                    return false;
            }
        }
    }
}
