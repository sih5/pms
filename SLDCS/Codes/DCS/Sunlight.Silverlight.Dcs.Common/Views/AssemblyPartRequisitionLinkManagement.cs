﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("PartsBaseInfo", "SparePart", "AssemblyPartRequisitionLink", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_ABANDON,CommonActionKeys.IMPORT_EXPORT
    })]
    public class AssemblyPartRequisitionLinkManagement : DcsDataManagementViewBase {
        private DataEditViewBase dataEditView;
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditViewImport;
        private const string DATA_EDIT_VIEW_IMPORT = "_DataEditViewImport_";
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("AssemblyPartRequisitionLink"));
            }
        }
        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("AssemblyPartRequisitionLink");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private DataEditViewBase DataEditViewImport {
            get {
                if(this.dataEditViewImport == null) {
                    this.dataEditViewImport = DI.GetDataEditView("AssemblyPartRequisitionLinkForImport");
                    this.dataEditViewImport.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewImport;
            }
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_IMPORT, () => this.DataEditViewImport);

        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewImport = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "AssemblyPartRequisitionLink"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var assemblyPartRequisitionLink = this.DataEditView.CreateObjectToEdit<AssemblyPartRequisitionLink>();
                    assemblyPartRequisitionLink.Status = (int)DcsBaseDataStatus.有效;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<AssemblyPartRequisitionLink>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            ShellViewModel.Current.IsBusy = true;
                            if(entity.Can作废总成件领用关系)
                                entity.作废总成件领用关系();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    ShellViewModel.Current.IsBusy = false;
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                ShellViewModel.Current.IsBusy = false;
                                UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                                this.DataGridView.ExecuteQueryDelayed();
                            }, null);
                        } catch(Exception ex) {
                            ShellViewModel.Current.IsBusy = false;
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORT:
                    ShellViewModel.Current.IsBusy = true;
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<AssemblyPartRequisitionLink>().Select(r => r.Id).ToArray();
                        this.dcsDomainContext.ExportAssemblyPartRequisitionLink(ids, null, null, null, null, null, loadOp1 => {
                            if(loadOp1.HasError)
                                return;
                            if(loadOp1.Value == null || string.IsNullOrEmpty(loadOp1.Value)) {
                                UIHelper.ShowNotification(loadOp1.Value);
                            }
                            if(loadOp1.Value != null && !string.IsNullOrEmpty(loadOp1.Value))
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp1.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }, null);
                    } else {
                        var filterItem1 = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem1 == null)
                            return;
                        var partCode = filterItem1.Filters.Single(r => r.MemberName == "PartCode").Value as string;
                        var keyCode = filterItem1.Filters.Single(r => r.MemberName == "KeyCode").Value as int?;
                        var status = filterItem1.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var createTime = filterItem1.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        this.dcsDomainContext.ExportAssemblyPartRequisitionLink(null, partCode, keyCode, status, createTimeBegin, createTimeEnd, loadOp1 => {
                            if(loadOp1.HasError)
                                return;
                            if(loadOp1.Value == null || string.IsNullOrEmpty(loadOp1.Value)) {
                                UIHelper.ShowNotification(loadOp1.Value);
                            }
                            if(loadOp1.Value != null && !string.IsNullOrEmpty(loadOp1.Value))
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp1.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }, null);
                    }
                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORT);
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                case CommonActionKeys.IMPORT:
                    return true;
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.Entities == null)
                        return false;
                    return this.DataGridView.Entities.Cast<AssemblyPartRequisitionLink>().ToArray().Length > 0;
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<AssemblyPartRequisitionLink>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsMasterDataStatus.有效;
                default:
                    return false;
            }
        }
        public AssemblyPartRequisitionLinkManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataManagementView_Title_AssemblyPartRequisitionLink;
        }
    }
}
