﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("Common", "BottomStock", "BottomStockForceReserveType", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON
    })]
    public class BottomStockForceReserveTypeManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DcsDomainContext dcsDomainContext = new DcsDomainContext();
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("BottomStockForceReserveType"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("BottomStockForceReserveType");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        public BottomStockForceReserveTypeManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataManagementView_Title_BottomStockForceReserveType;
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "BottomStockForceReserveType"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var bottomStockForceReserveType = this.DataEditView.CreateObjectToEdit<BottomStockForceReserveType>();
                    bottomStockForceReserveType.Status = (int)DcsMasterDataStatus.有效;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entitys = this.DataGridView.SelectedEntities.Cast<BottomStockForceReserveType>().ToArray();
                        if(entitys == null || !entitys.Any())
                            return;
                        if(dcsDomainContext == null)
                            return;
                        try {
                            dcsDomainContext.作废保底库存强制储备类别(entitys.Select(r => r.Id).ToArray(), loadOpv => {
                                if(loadOpv.HasError) {
                                    if(!loadOpv.IsErrorHandled)
                                        loadOpv.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOpv);
                                    dcsDomainContext.RejectChanges();
                                    ShellViewModel.Current.IsBusy = false;
                                    return;
                                }
                                UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_AbandonSuccess);
                                dcsDomainContext.RejectChanges();
                                if(DataGridView != null && DataGridView.FilterItem != null) {
                                    DataGridView.ExecuteQueryDelayed();
                                }
                                CheckActionsCanExecute();
                                ShellViewModel.Current.IsBusy = false;
                            }, null);
                            UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities1 = this.DataGridView.SelectedEntities.Cast<BottomStockForceReserveType>().ToArray();
                    if(entities1.Length < 1)
                        return false;
                    return entities1.All(r => r.Status == (int)DcsMasterDataStatus.有效);

                default:
                    return false;
            }
        }
    }
}