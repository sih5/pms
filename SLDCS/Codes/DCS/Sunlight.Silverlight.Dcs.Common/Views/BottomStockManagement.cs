﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Views
{
    [PageMeta("Common", "Dealer", "BottomStock", ActionPanelKeys = new[] {
        "BottomStock"
    })]
    public class BottomStockManagement : DcsDataManagementViewBase
    {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditViewImport;
        private DataEditViewBase dataAbandonViewImport;
        private DcsDomainContext dcsDomainContext = new DcsDomainContext();
        private const string DATA_EDIT_VIEW_IMPORT = "_DataEditViewImport_";
        private const string DATA_ABANDON_VIEW_IMPORT = "_DataAbandonViewImport_";

        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("BottomStock"));
            }
        }

        private DataEditViewBase DataEditView
        {
            get
            {
                if (this.dataEditView == null)
                {
                    this.dataEditView = DI.GetDataEditView("BottomStock");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        //导入
        private DataEditViewBase DataEditViewImport
        {
            get
            {
                if (this.dataEditViewImport == null)
                {
                    this.dataEditViewImport = DI.GetDataEditView("BottomStockForImport");
                    this.dataEditViewImport.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewImport;
            }
        }
        //导入作废
        private DataEditViewBase DataAbandonViewImport {
            get {
                if(this.dataAbandonViewImport == null) {
                    this.dataAbandonViewImport = DI.GetDataEditView("BottomStockForAbandonImport");
                    this.dataAbandonViewImport.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataAbandonViewImport;
            }
        }
        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_IMPORT, () => this.DataEditViewImport);
            this.RegisterView(DATA_ABANDON_VIEW_IMPORT, () => this.DataAbandonViewImport);
        }

        public BottomStockManagement()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataManagementView_Title_BottomStockManagement;
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewImport = null;
        }
        private void DataEditView_EditCancelled(object sender, EventArgs e)
        {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e)
        {
            this.ResetEditView();
            if (this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "BottomStock"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem)
        {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var newCompositeFilterItem = new CompositeFilterItem();
            var compositeFilterItem = filterItem as CompositeFilterItem;
            foreach (var item in compositeFilterItem.Filters)
                newCompositeFilterItem.Filters.Add(item);
            ClientVar.ConvertTime(newCompositeFilterItem);

            var codes = newCompositeFilterItem.Filters.SingleOrDefault(s => s.MemberName == "SparePartCode");
            if (codes != null && codes.Value != null) {
                newCompositeFilterItem.Filters.Remove(codes);
                newCompositeFilterItem.Filters.Add(new CompositeFilterItem {
                    MemberName = "SparePartCode",
                    MemberType = typeof(string),
                    Value = string.Join(",", ((IEnumerable<string>)codes.Value).ToArray()),
                    Operator = FilterOperator.Contains
                });
            }

            this.DataGridView.FilterItem = newCompositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId)
            {
                case CommonActionKeys.ADD:
                    var customerInformation = this.DataEditView.CreateObjectToEdit<BottomStock>();
                    customerInformation.Status = (int)DcsMasterDataStatus.有效;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Abandon, () =>
                    {
                        var entitys = this.DataGridView.SelectedEntities.Cast<BottomStock>().ToArray();
                        if (entitys == null || !entitys.Any())
                            return;
                        if (dcsDomainContext == null)
                            return;
                        try {
                            dcsDomainContext.批量作废保底库存(entitys.Select(r => r.Id).ToArray(), loadOp => {
                                if(loadOp.HasError)
                                    return;
                                this.DataGridView.ExecuteQueryDelayed();
                            }, null);
                            UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORT);
                    break;
                case "ImportAbandon":
                    this.SwitchViewTo(DATA_ABANDON_VIEW_IMPORT);
                    break;
                case "Export"://导出
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any())
                    {
                        var ids = this.DataGridView.SelectedEntities.Cast<BottomStock>().Select(r => r.Id).ToArray();
                        this.ExportBottomStock(ids, null, null, null, null, null, null, null, null, null, null,null);
                    }
                    else
                    {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if (filterItem == null)
                            return;
                        var partsSalesCategoryId = filterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                        var companyType = filterItem.Filters.Single(r => r.MemberName == "CompanyType").Value as int?;
                        var companyCode = filterItem.Filters.Single(r => r.MemberName == "CompanyCode").Value as string;
                        var companyName = filterItem.Filters.Single(r => r.MemberName == "CompanyName").Value as string;
                        var sparePartCode = filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                        var sparePartName = filterItem.Filters.Single(r => r.MemberName == "SparePartName").Value as string;
                        var status = filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var createTimeFilters = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem) && ((CompositeFilterItem)r).Filters.Any(e => e.MemberName == "CreateTime")) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if (createTimeFilters != null && createTimeFilters.Filters.Any())
                        {
                            createTimeBegin = createTimeFilters.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTimeFilters.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        var modifyTimeFilters = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem) && ((CompositeFilterItem)r).Filters.Any(e => e.MemberName == "ModifyTime")) as CompositeFilterItem;
                        DateTime? modifyTimeBegin = null;
                        DateTime? modifyTimeEnd = null;
                        if (modifyTimeFilters != null && modifyTimeFilters.Filters.Any())
                        {
                            modifyTimeBegin = modifyTimeFilters.Filters.First(r => r.MemberName == "ModifyTime").Value as DateTime?;
                            modifyTimeEnd = modifyTimeFilters.Filters.Last(r => r.MemberName == "ModifyTime").Value as DateTime?;
                        }
                        this.ExportBottomStock(null, partsSalesCategoryId, companyType, companyCode, companyName, sparePartCode, sparePartName, createTimeBegin, createTimeEnd, modifyTimeBegin, modifyTimeEnd,status);
                    }
                    break;
            }
        }
        private void ExportBottomStock(int[] ids, int? partsSalesCategoryId, int? companyType, string companyCode, string companyName, string sparePartCode, string sparePartName, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? modifyTimeBegin, DateTime? modifyTimeEnd,int? ststus)
        {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportBottomStockAsync(ids, partsSalesCategoryId, companyType, companyCode, companyName, sparePartCode, sparePartName, createTimeBegin, createTimeEnd, modifyTimeBegin, modifyTimeEnd, ststus);
            this.excelServiceClient.ExportBottomStockCompleted -= ExcelServiceClient_ExportBottomStockCompleted;
            this.excelServiceClient.ExportBottomStockCompleted += ExcelServiceClient_ExportBottomStockCompleted;
        }

        private void ExcelServiceClient_ExportBottomStockCompleted(object sender, ExportBottomStockCompletedEventArgs e)
        {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
        private void ExecuteSerivcesMethod(string notifyMessage)
        {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if (domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp =>
            {
                if (submitOp.HasError)
                {
                    if (!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId)
            {
                case CommonActionKeys.ADD:
                case CommonActionKeys.IMPORT:
                case "ImportAbandon":
                    return true;
                case CommonActionKeys.EDIT:
                    if (this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<BottomStock>().ToArray();
                    if (entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsMasterDataStatus.有效;
                case CommonActionKeys.ABANDON:
                    if (this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities1 = this.DataGridView.SelectedEntities.Cast<BottomStock>().ToArray();
                    if (entities1.Length < 1)
                        return false;
                    return entities1.All(r => r.Status == (int)DcsMasterDataStatus.有效);
                case "Export":
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
    }
}
