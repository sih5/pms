﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
namespace Sunlight.Silverlight.Dcs.Common.Views {
      [PageMeta("Common", "BottomStock", "BottomStockSubVersion", ActionPanelKeys = new[] {
         CommonActionKeys.ABANDON_EXPORT
    })]
    public class BottomStockSubVersionManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        public BottomStockSubVersionManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.Report_Title_BottomStockSubVersion;
        }
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("BottomStockSubVersion"));
            }
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "BottomStockSubVersion"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {

                case CommonActionKeys.EXPORT:
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any())
                    {
                        var ids = this.DataGridView.SelectedEntities.Cast<BottomStockSubVersion>().Select(r => r.Id).ToArray();
                        ShellViewModel.Current.IsBusy = true;
                        this.dcsDomainContext.导出子集版本信息(ids, null, null, null, null, null, null, null, null, null, null,null, loadOp => {
                            if(loadOp.HasError) {
                                ShellViewModel.Current.IsBusy = false;
                                return;
                            }
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                                ShellViewModel.Current.IsBusy = false;
                            }

                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                ShellViewModel.Current.IsBusy = false;
                            }
                        }, null);
                    }
                    else
                    {var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if (filterItem == null)
                        return;
                    var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                    var subVersionCode = filterItem.Filters.Single(e => e.MemberName == "SubVersionCode").Value as string;
                    var colVersionCode = filterItem.Filters.Single(e => e.MemberName == "ColVersionCode").Value as string;
                    var sparePartCode = filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                    var sparePartName = filterItem.Filters.Single(r => r.MemberName == "SparePartName").Value as string;
                    var companyCode = filterItem.Filters.Single(r => r.MemberName == "CompanyCode").Value as string;
                    var companyName = filterItem.Filters.Single(r => r.MemberName == "CompanyName").Value as string;
                    var reserveTypeSubItem = filterItem.Filters.Single(r => r.MemberName == "ReserveTypeSubItem").Value as string;
                    var companyType = filterItem.Filters.Single(e => e.MemberName == "CompanyType").Value as int?;
                    DateTime? bStartTime = null;
                    DateTime? eStartTime = null;
                   
                    foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                        var dateTime = filter as CompositeFilterItem;
                        if(dateTime != null) {

                            if(dateTime.Filters.First().MemberName == "StartTime") {
                                bStartTime = dateTime.Filters.First(r => r.MemberName == "StartTime").Value as DateTime?;
                                eStartTime = dateTime.Filters.Last(r => r.MemberName == "StartTime").Value as DateTime?;
                            }                           
                        }
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出子集版本信息(null, status, subVersionCode, colVersionCode, sparePartCode, sparePartName, companyCode, companyName, bStartTime, eStartTime, companyType,reserveTypeSubItem, loadOp => {
                        if(loadOp.HasError) {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    }
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<BottomStockSubVersion>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废子集版本号)
                                entity.作废子集版本号();
                            this.ExecuteSerivcesMethod(CommonUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
            }
        }
        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                this.DataGridView.ExecuteQueryDelayed();
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return true;
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities1 = this.DataGridView.SelectedEntities.Cast<BottomStockSubVersion>().ToArray();
                    if(entities1.Length != 1)
                        return false;
                    return entities1.All(r => r.Status == (int)DcsMasterDataStatus.有效);
                default: return false;
            }
        }
    }
}