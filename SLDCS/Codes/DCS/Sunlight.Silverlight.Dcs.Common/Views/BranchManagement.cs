﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("Common", "EnterpriseAndInternalOrganization", "Branch", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_PAUSE_RESUME_DETAIL
    })]
    public class BranchManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataDetailView;
        private const string DATA_DETAIL_VIEW = "_dataDetailView_";

        public BranchManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataManagementView_Title_Branch;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("Branch"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("Branch");
                    dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataDetailView {
            get {
                if(this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("BranchDetail");
                    this.dataDetailView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return dataDetailView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataDetailView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "Branch"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var branch = this.DataEditView.CreateObjectToEdit<Branch>();
                    branch.Status = (int)DcsMasterDataStatus.有效;
                    var company = new Company {
                        Code = branch.Code,
                        Name = branch.Name,
                        Type = (int)DcsCompanyType.分公司,
                        ShortName = branch.Abbreviation,
                        Status = (int)DcsMasterDataStatus.有效
                    };
                    branch.Company = company;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.PAUSE:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Pause, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<Branch>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can停用营销分公司)
                                entity.停用营销分公司();
                            this.ExecuteSerivcesMethod(CommonUIStrings.DataManagementView_Notification_PauseSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<Branch>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废营销分公司)
                                entity.作废营销分公司();
                            this.ExecuteSerivcesMethod(CommonUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.RESUME:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Resume, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<Branch>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can恢复营销分公司)
                                entity.恢复营销分公司();
                            this.ExecuteSerivcesMethod(CommonUIStrings.DataManagementView_Notification_ResumeSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.DETAIL:
                    this.DataDetailView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_DETAIL_VIEW);
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.PAUSE:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<Branch>().Any(entity => entity.Status == (int)DcsMasterDataStatus.有效);
                case CommonActionKeys.RESUME:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<Branch>().Any(entity => entity.Status == (int)DcsMasterDataStatus.停用);
                case CommonActionKeys.DETAIL:
                    return this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Count() == 1;
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataDetailView);
        }
    }
}
