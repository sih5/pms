﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("PartsBaseInfo", "SparePartPurchase", "BranchSupplierRelation", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_EXPORT_IMPORT_IMPORTUPDATE
    })]
    public class BranchSupplierRelationManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase forEditDataEditView;
        private DataEditViewBase dataEditViewImport;
        private DataEditViewBase dataEditViewImportUpdate;
        private const string DATA_EDIT_VIEW_IMPORT = "_DataEditViewImport_";
        private const string DATA_EDIT_VIEW_IMPORT_UPDATE = "_DataEditViewImportUpdate_";
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private const string DATA_EDIT_VIEW_SECOND = "_DataEditViewSecond_";

        public BranchSupplierRelationManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataManagementView_Title_BranchSupplierRelation;
        }

        public DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("BranchSupplierRelation"));
            }
        }

        public DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("BranchSupplierRelation");
                    this.dataEditView.EditSubmitted += dataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase ForEditDataEditView {
            get {
                if(this.forEditDataEditView == null) {
                    this.forEditDataEditView = DI.GetDataEditView("BranchSupplierRelationForEdit");
                    this.forEditDataEditView.EditSubmitted += dataEditView_EditSubmitted;
                    this.forEditDataEditView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.forEditDataEditView;
            }
        }

        private DataEditViewBase DataEditViewImport {
            get {
                if(this.dataEditViewImport == null) {
                    this.dataEditViewImport = DI.GetDataEditView("BranchSupplierRelationForImport");
                    this.dataEditViewImport.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.dataEditViewImport;
            }
        }

        //导入修改
        private DataEditViewBase DataEditViewImportUpdate {
            get {
                if(this.dataEditViewImportUpdate == null) {
                    this.dataEditViewImportUpdate = DI.GetDataEditView("BranchSupplierRelationForImportUpdate");
                    this.dataEditViewImportUpdate.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.dataEditViewImportUpdate;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.forEditDataEditView =null;
            this.dataEditViewImport =null;
            this.dataEditViewImportUpdate = null;
        }
        private void dataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void dataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_SECOND, () => this.ForEditDataEditView);
            this.RegisterView(DATA_EDIT_VIEW_IMPORT, () => this.DataEditViewImport);
            this.RegisterView(DATA_EDIT_VIEW_IMPORT_UPDATE, () => this.DataEditViewImportUpdate);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[]{
                    "BranchSupplierRelation"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.ForEditDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.Cast<BranchSupplierRelation>().First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_SECOND);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Abandon, () => this.DataGridView.UpdateSelectedEntities(entity => ((BranchSupplierRelation)entity).Status = (int)DcsBaseDataStatus.作废, () => {
                        UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_AbandonSuccess);
                        this.CheckActionsCanExecute();
                    }));
                    break;
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var partsSupplierCode = filterItem.Filters.Single(r => r.MemberName == "PartsSupplier.Code").Value as string;
                    var partsSupplierName = filterItem.Filters.Single(r => r.MemberName == "PartsSupplier.Name").Value as string;
                    var businessCode = filterItem.Filters.Single(r => r.MemberName == "BusinessCode").Value as string;
                    var businessName = filterItem.Filters.Single(r => r.MemberName == "BusinessName").Value as string;
                    //var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                    var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                    var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    if(createTime != null) {
                        createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                        createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    }
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var entities = this.DataGridView.SelectedEntities.Cast<BranchSupplierRelation>().ToArray();
                        var ids = entities.Select(r => r.Id).ToArray();
                        ShellViewModel.Current.IsBusy = true;
                        this.ExportBranchSupplierRelation(ids, BaseApp.Current.CurrentUserData.EnterpriseId, partsSupplierCode, partsSupplierName, businessCode, businessName, 221, status, createTimeBegin, createTimeEnd);
                    } else {
                        this.ExportBranchSupplierRelation(null, BaseApp.Current.CurrentUserData.EnterpriseId, partsSupplierCode, partsSupplierName, businessCode, businessName, 221, status, createTimeBegin, createTimeEnd);
                    }
                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORT);
                    break;
                case CommonActionKeys.IMPORTUPDATE:
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORT_UPDATE);
                    break;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            var newCompositeFilterItem = filterItem as CompositeFilterItem;
            if(newCompositeFilterItem == null)
                return;
            newCompositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "BranchId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            this.DataGridView.FilterItem = newCompositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                case CommonActionKeys.IMPORT:
                case CommonActionKeys.IMPORTUPDATE:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<BranchSupplierRelation>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsBaseDataStatus.有效;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        private void ExportBranchSupplierRelation(int[] ids, int? branchId, string supplierCode, string supplierName, string bussinessCode, string bussinessName, int? partsSalesCategoryId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportBranchSupplierRelationAsync(ids, branchId, supplierCode, supplierName, bussinessCode, bussinessName, partsSalesCategoryId, status, createTimeBegin, createTimeEnd);
            this.excelServiceClient.ExportBranchSupplierRelationCompleted -= excelServiceClient_ExportBranchSupplierRelationCompleted;
            this.excelServiceClient.ExportBranchSupplierRelationCompleted += excelServiceClient_ExportBranchSupplierRelationCompleted;
        }

        private void excelServiceClient_ExportBranchSupplierRelationCompleted(object sender, ExportBranchSupplierRelationCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
    }
}