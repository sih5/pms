﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;
using System.Collections.ObjectModel;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("Common", "Category", "CenterApproveBill", ActionPanelKeys = new[]{
        CommonActionKeys.ADD_EDIT_ABANDON,
    })]
    public class CenterApproveBillManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private ObservableCollection<KeyValuePair> kvWeeks;
        private ObservableCollection<KeyValuePair> KvWeeks
        {
            get
            {
                return this.kvWeeks ?? (this.kvWeeks = new ObservableCollection<KeyValuePair>());
            }
        }
        public CenterApproveBillManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataManagementView_Title_CenterApproveBillManagement;

            this.KvWeeks.Add(new KeyValuePair
            {
                Key = 1,
                Value = CommonUIStrings.QueryPanel_QueryItem_Title_CenterApproveBill_ApproveWeek_Monday
            });
            this.KvWeeks.Add(new KeyValuePair
            {
                Key = 2,
                Value = CommonUIStrings.QueryPanel_QueryItem_Title_CenterApproveBill_ApproveWeek_Tuesday
            });
            this.KvWeeks.Add(new KeyValuePair
            {
                Key = 3,
                Value = CommonUIStrings.QueryPanel_QueryItem_Title_CenterApproveBill_ApproveWeek_Wednesday
            });
            this.KvWeeks.Add(new KeyValuePair
            {
                Key = 4,
                Value = CommonUIStrings.QueryPanel_QueryItem_Title_CenterApproveBill_ApproveWeek_Thursday
            });
            this.KvWeeks.Add(new KeyValuePair
            {
                Key = 5,
                Value = CommonUIStrings.QueryPanel_QueryItem_Title_CenterApproveBill_ApproveWeek_Friday
            });
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("CenterApproveBill"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("CenterApproveBill");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "CenterApproveBill"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var claimApproverStrategy = this.DataEditView.CreateObjectToEdit<CenterApproveBill>();
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    var entitys = this.DataGridView.SelectedEntities.Cast<CenterApproveBill>().SingleOrDefault();
                    if(entitys == null)
                        return;
                    this.DataEditView.SetObjectToEditById(entitys.Id);
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<CenterApproveBill>().FirstOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.CanAbandonCenterApproveBill)
                                entity.AbandonCenterApproveBill();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
            }
        }


        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            var newFilterItem = new CompositeFilterItem();
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return;
            int? WeekValue;
            if (compositeFilterItem != null)
            {
                foreach (var item in compositeFilterItem.Filters)
                {
                    if (item.MemberName == "ApproveWeek")
                    {
                        WeekValue = item.Value as int?;
                        if (WeekValue.HasValue && WeekValue != -1)
                        {
                            string week = kvWeeks.FirstOrDefault(r => r.Key == WeekValue.Value).Value;
                            newFilterItem.Filters.Add(new FilterItem("ApproveWeek", typeof(string), FilterOperator.IsEqualTo, "星期" + week));
                        }
                        continue;
                    }
                    newFilterItem.Filters.Add(item);
                }
            }
            this.SwitchViewTo(DATA_GRID_VIEW);
            ClientVar.ConvertTime(compositeFilterItem);           
            this.DataGridView.FilterItem = newFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any() || this.DataGridView.SelectedEntities.Count() >1)
                        return false;
                    var entity1 = this.DataGridView.SelectedEntities.Cast<CenterApproveBill>().FirstOrDefault();
                    if (entity1 != null)
                        return entity1.Status == (int)DcsBaseDataStatus.有效;
                    return false;
                case CommonActionKeys.EDIT:
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any() || this.DataGridView.SelectedEntities.Count() >1)
                        return false;
                    var entity = this.DataGridView.SelectedEntities.Cast<CenterApproveBill>().FirstOrDefault();
                    if (entity != null)
                        return entity.Status == (int)DcsBaseDataStatus.有效;
                    return false;
                default:
                    return false;
            }
        }


    }
}
