﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("Report", "Service", "ClaimSupplierDetailsSplit")]
    public class ClaimSupplierDetailsSplitManagement : DcsDataManagementViewBase {
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public ClaimSupplierDetailsSplitManagement() {
            Title = CommonUIStrings.DataManagementView_Title_ClaimSupplierDetailsSplitManagement;
        }

        string addUrlParameter;
        private void excelServiceClient_GetTokenCompleted(object sender, GetTokenCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            if(string.IsNullOrEmpty(e.Result)) {
                UIHelper.ShowAlertMessage(CommonUIStrings.DataManagementView_Validation_Service);
            } else {
                string url = "http://pmsrf.foton.com.cn:8080/BOE/OpenDocument/opendoc/openDocument.jsp?token=" + e.Result + "&sType=wid&sPath=[FTDCS]&sDocName=ClaimSupplierDetailsSplit" + addUrlParameter;
                HtmlPage.Window.Navigate(new Uri(url, UriKind.Absolute), "_blank");
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "ClaimSupplierDetailsSplit"
                };
            }
        }

        protected override void OnExecutingAction(Silverlight.View.ActionPanelBase actionPanel, string uniqueId) {

        }

        protected override void OnExecutingQuery(Silverlight.View.QueryPanelBase queryPanel, FilterItem filterItem) {
            addUrlParameter = "";

            var newCompositeFilterItem = filterItem as CompositeFilterItem;
            if(newCompositeFilterItem == null)
                return;

            var branchCode = newCompositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "BranchCode");
            if(branchCode != null && branchCode.Value != null && !string.IsNullOrWhiteSpace(branchCode.Value.ToString()))
                addUrlParameter += "&lsSBranchCode=" + Uri.EscapeUriString("%" + branchCode.Value.ToString() + "%");

            var categoryId = newCompositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "CategoryID");
            if(categoryId != null && categoryId.Value != null && !string.IsNullOrWhiteSpace(categoryId.Value.ToString()))
                addUrlParameter += "&lsSCategoryID=" + Uri.EscapeUriString(categoryId.Value.ToString());

            var marketName = newCompositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "MarketName");
            if(marketName != null && marketName.Value != null && !string.IsNullOrWhiteSpace(marketName.Value.ToString()))
                addUrlParameter += "&lsSMarketName=" + Uri.EscapeUriString("%" + marketName.Value.ToString() + "%");

            var supplierCode = newCompositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "SupplierCode");
            if(supplierCode != null && supplierCode.Value != null && !string.IsNullOrWhiteSpace(supplierCode.Value.ToString()))
                addUrlParameter += "&lsSSupplierCode=" + Uri.EscapeUriString("%" + supplierCode.Value.ToString() + "%");

            var supplierName = newCompositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "SupplierName");
            if(supplierName != null && supplierName.Value != null && !string.IsNullOrWhiteSpace(supplierName.Value.ToString()))
                addUrlParameter += "&lsSSupplierName=" + Uri.EscapeUriString("%" + supplierName.Value.ToString() + "%");

            var repairTypeId = newCompositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "RepairType");
            if(repairTypeId != null && repairTypeId.Value != null && !string.IsNullOrWhiteSpace(repairTypeId.Value.ToString()))
                addUrlParameter += "&lsSRepairType=" + Uri.EscapeUriString(repairTypeId.Value.ToString());


            var productLineType = newCompositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "ProductLineType");
            if(productLineType != null && productLineType.Value != null && !string.IsNullOrWhiteSpace(productLineType.Value.ToString()))
                addUrlParameter += "&ProductLineType=" + Uri.EscapeUriString(productLineType.Value.ToString());

            var status = newCompositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "Status");
            if(status != null && status.Value != null && !string.IsNullOrWhiteSpace(status.Value.ToString()))
                addUrlParameter += "&lsSStatus=" + Uri.EscapeUriString(status.Value.ToString());

            var productLineId = newCompositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "ProductLineId");
            if(productLineId != null && productLineId.Value != null && !string.IsNullOrWhiteSpace(productLineId.Value.ToString()))
                addUrlParameter += "&lsSProductLineId=" + Uri.EscapeUriString(productLineId.Value.ToString());

            var vin = newCompositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "VIN");
            if(vin != null && vin.Value != null && !string.IsNullOrWhiteSpace(vin.Value.ToString()))
                addUrlParameter += "&lsSVIN=" + Uri.EscapeUriString("%" + vin.Value.ToString() + "%");

            var parameters = filterItem.ToQueryParameters();
            var createBeginTime = parameters.ContainsKey("CreateTime") ? ((System.DateTime?[])(parameters["CreateTime"]))[0] : null;
            var createEndTime = parameters.ContainsKey("CreateTime") ? ((System.DateTime?[])(parameters["CreateTime"]))[1] : null;
            if(createBeginTime == null || createEndTime == null) {
                UIHelper.ShowAlertMessage("创建时间不能为空！");
                return;
            }
            createEndTime = createEndTime.Value.AddDays(1);
            addUrlParameter += "&lsSCreateTimeBegin=Date(" + createBeginTime.Value.Year + "," + createBeginTime.Value.Month + "," + createBeginTime.Value.Day + ")";
            addUrlParameter += "&lsSCreateTimeEnd=Date(" + createEndTime.Value.Year + "," + createEndTime.Value.Month + "," + createEndTime.Value.Day + ")";

            var approveBeginTime = parameters.ContainsKey("ApproveTime") ? ((System.DateTime?[])(parameters["CreateTime"]))[0] : null;
            var approveEndTime = parameters.ContainsKey("ApproveTime") ? ((System.DateTime?[])(parameters["CreateTime"]))[1] : null;
            if(approveBeginTime.HasValue && approveBeginTime != null)
                addUrlParameter += "&lsSApproveTimeBegin=Date(" + approveBeginTime.Value.Year + "," + approveBeginTime.Value.Month + "," + approveBeginTime.Value.Day + ")";
            if(approveEndTime.HasValue && approveEndTime != null) {
                approveEndTime = approveEndTime.Value.AddDays(1);
                addUrlParameter += "&lsSApproveTimeEnd=Date(" + approveEndTime.Value.Year + "," + approveEndTime.Value.Month + "," + approveEndTime.Value.Day + ")";
            }
            var cacuBeginTime = parameters.ContainsKey("CacuTime") ? ((System.DateTime?[])(parameters["CacuTime"]))[0] : null;
            var cacuEndTime = parameters.ContainsKey("CacuTime") ? ((System.DateTime?[])(parameters["CacuTime"]))[1] : null;
            if(cacuBeginTime.HasValue && cacuBeginTime != null)
                addUrlParameter += "&lsSCacuTimeBegin=Date(" + cacuBeginTime.Value.Year + "," + cacuBeginTime.Value.Month + "," + cacuBeginTime.Value.Day + ")";
            if(cacuEndTime.HasValue && cacuEndTime != null) {

                cacuEndTime = cacuEndTime.Value.AddDays(1);

                addUrlParameter += "&lsSCacuTimeEnd=Date(" + cacuEndTime.Value.Year + "," + cacuEndTime.Value.Month + "," + cacuEndTime.Value.Day + ")";
            }


            ShellViewModel.Current.IsBusy = true;
            excelServiceClient.GetTokenAsync("psmsadmin", "psms-admin", "pmsrf.foton.com.cn:6400", "secEnterprise", 1, 100);
            excelServiceClient.GetTokenCompleted -= excelServiceClient_GetTokenCompleted;
            excelServiceClient.GetTokenCompleted += excelServiceClient_GetTokenCompleted;
        }
    }
}
