﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("Common", "Company", "CompanyInvoiceInfo", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_DELETE_DETAIL_EXPORT
    })]
    public class CompanyInvoiceInfoManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataDetailView;
        private const string DATA_DETAIL_VIEW = "_dataDetailView_";
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();

        public CompanyInvoiceInfoManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataManagementView_Title_CompanyInvoiceInfo;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("CompanyInvoiceInfo"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("CompanyInvoiceInfo");
                    dataEditView.EditSubmitted += dataEditView_EditSubmitted;
                    dataEditView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataDetailView {
            get {
                if(this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("CompanyInvoiceInfoDetail");
                    this.dataDetailView.EditCancelled += dataEditView_EditCancelled;
                }
                return dataDetailView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataDetailView = null;
        }
        private void dataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void dataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "CompanyInvoiceInfo"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var companyInvoiceInfo = this.DataEditView.CreateObjectToEdit<CompanyInvoiceInfo>();
                    companyInvoiceInfo.IsPartsSalesInvoice = true;
                    companyInvoiceInfo.IsVehicleSalesInvoice = true;
                    companyInvoiceInfo.InvoiceCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    companyInvoiceInfo.Status = (int)DcsMasterDataStatus.有效;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.DELETE:
                    var delcompanyInvoiceInfo = this.DataGridView.SelectedEntities.Cast<CompanyInvoiceInfo>().SingleOrDefault();
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Delete, () => {
                        try {
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null || !domainContext.CompanyInvoiceInfos.Contains(delcompanyInvoiceInfo))
                                return;
                            domainContext.CompanyInvoiceInfos.Remove(delcompanyInvoiceInfo);
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_DeleteSuccess);
                                this.CheckActionsCanExecute();
                                this.DataGridView.ExecuteQuery();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.DETAIL:
                    this.DataDetailView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    SwitchViewTo(DATA_DETAIL_VIEW);
                    break;
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    var companyCode = filterItem.Filters.Single(e => e.MemberName == "CompanyCode").Value as string;
                    var companyName = filterItem.Filters.Single(e => e.MemberName == "CompanyName").Value as string;
                    var invoiceType = filterItem.Filters.Single(e => e.MemberName == "InvoiceType").Value as int?;
                    var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Count() == 1) {
                        this.dcsDomainContext.ExportCompanyInvoiceInfo((this.DataGridView.SelectedEntities.FirstOrDefault() as CompanyInvoiceInfo).Id, companyCode, companyName, invoiceType, status, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                            }
                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                        }, null);
                    } else {
                        this.dcsDomainContext.ExportCompanyInvoiceInfo(null, companyCode, companyName, invoiceType, status, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                            }
                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                        }, null);
                    }
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.DELETE:
                case CommonActionKeys.DETAIL:
                    return this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Count() == 1;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                compositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "InvoiceCompanyId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });
            }
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataDetailView);
        }
    }
}
