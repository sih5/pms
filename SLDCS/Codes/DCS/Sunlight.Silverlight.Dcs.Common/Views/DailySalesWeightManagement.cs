﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("Common", "SIHSmart", "DailySalesWeight", ActionPanelKeys = new[] {
        "DailySalesWeight"
    })]
    public class DailySalesWeightManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DcsDomainContext dcsDomainContext = new DcsDomainContext();
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("DailySalesWeight"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("DailySalesWeight");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        public DailySalesWeightManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataManagementView_Title_DailySalesWeight;
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "DailySalesWeight"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EXPORT:
                                 
                    var agencys = this.DataGridView.SelectedEntities;
                    if(agencys != null) {
                        var ids = this.DataGridView.SelectedEntities.Cast<DailySalesWeight>().Select(r => r.Id).ToArray();
                        ShellViewModel.Current.IsBusy = true;
                        this.ExportDailySalesWeight(ids, null, null, null, null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null) {
                            this.ExportDailySalesWeight(null, null, null, null, null);
                            return;
                        }     
                        var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                        DateTime? bStartTime = null;
                        DateTime? eStartTime = null;

                        foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var dateTime = filter as CompositeFilterItem;
                            if(dateTime != null) {
                                if(dateTime.Filters.First().MemberName == "CreateTime") {
                                    bStartTime = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    eStartTime = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                            }
                        }
                        ShellViewModel.Current.IsBusy = true;
                        this.ExportDailySalesWeight(null, warehouseId, null, bStartTime, eStartTime);
                    }

                    break;

            }
        }
        private void ExportDailySalesWeight(int[] ids, int? warehouseId, int? times, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportDailySalesWeightAsync(ids, warehouseId, times, createTimeBegin, createTimeEnd);
            this.excelServiceClient.ExportDailySalesWeightCompleted -= excelServiceClient_ExportDailySalesWeightCompleted;
            this.excelServiceClient.ExportDailySalesWeightCompleted += excelServiceClient_ExportDailySalesWeightCompleted;
        }

        private void excelServiceClient_ExportDailySalesWeightCompleted(object sender, ExportDailySalesWeightCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.IMPORT:
                case CommonActionKeys.EXPORT:
                    return true;
                default:
                    return false;
            }
        }
    }
}