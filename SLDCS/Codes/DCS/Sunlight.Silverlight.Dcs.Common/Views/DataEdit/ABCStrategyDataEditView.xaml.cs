﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class ABCStrategyDataEditView {
        public ABCStrategyDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_ABCStrategy;
            }
        }

        protected override void OnEditSubmitting() {
            var aBCStrategy = this.DataContext as ABCStrategy;
            if(aBCStrategy == null)
                return;
            aBCStrategy.ValidationErrors.Clear();
            if(aBCStrategy.Category <= 0)
                aBCStrategy.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_ABCStrategy_CategoryIsNull, new[] {
                    "Category"
                }));
            if(aBCStrategy.ReferenceBaseType <= 0)
                aBCStrategy.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_ABCStrategy_ReferenceBaseTypeIsNull, new[] {
                    "ReferenceBaseType"
                }));
            if(aBCStrategy.Percentage <= 0 || aBCStrategy.Percentage > 1)
                aBCStrategy.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_ABCStrategy_PercentageIsMoreThanOneOrLessThanZero, new[] {
                    "Percentage"
                }));
            if(aBCStrategy.SamplingDuration <= 0)
                aBCStrategy.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_ABCStrategy_SamplingDurationIsLessThanOne, new[] {
                    "SamplingDuration"
                }));
            if(aBCStrategy.HasValidationErrors)
                return;
            ((IEditableObject)aBCStrategy).EndEdit();
            base.OnEditSubmitting();
        }

        private void CreateUI() {
            // 创建主单编辑面板 
            this.Root.Children.Add(DI.GetDataEditPanel("ABCStrategy"));
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetABCStrategiesQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }
    }
}
