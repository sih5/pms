﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class AgencyDataEditView{
        private DataGridViewBase agencyBindBranchForEditDataGridView;
        private EnterpriseInformationForAgencyDataEditPanel enterpriseInformationDataEditPanel;
        private List<TiledRegion> tiledRegions;

        private List<TiledRegion> TiledRegions {
            get {
                return this.tiledRegions ?? (this.tiledRegions = new List<TiledRegion>());
            }
        }

        private ObservableCollection<Branch> branches;
        private ObservableCollection<TiledRegion> centerAuthorizedProvinces;
        private List<TiledRegion> centerAuthorizedProvinceOrigs = new List<TiledRegion>();

        public EnterpriseInformationForAgencyDataEditPanel EnterpriseInformationDataEditPanel {
            get {
                return this.enterpriseInformationDataEditPanel ?? (this.enterpriseInformationDataEditPanel = (EnterpriseInformationForAgencyDataEditPanel)DI.GetDataEditPanel("EnterpriseInformationForAgency"));
            }
        }

        public ObservableCollection<Branch> Branches {
            get {
                return branches ?? (this.branches = new ObservableCollection<Branch>());
            }
            set {
                this.branches = value;
            }
        }

        public ObservableCollection<TiledRegion> CenterAuthorizedProvinces {
            get {
                return centerAuthorizedProvinces ?? (this.centerAuthorizedProvinces = new ObservableCollection<TiledRegion>());
            }
            set {
                this.centerAuthorizedProvinces = value;
            }
        }

        public AgencyDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged -= AgencyDataEditView_DataContextChanged;
            this.DataContextChanged += AgencyDataEditView_DataContextChanged;
        }

        protected override string BusinessName {
            get {
                return CommonUIStrings.DataEditView_Notification_Type_Agency;
            }
        }

        private void AgencyDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var agency = this.DataContext as Agency;
            if (agency != null && agency.Id == default(int)) {
                this.CenterAuthorizedProvinces.Clear();
                foreach (var item in this.centerAuthorizedProvinceOrigs) {
                    item.IsSelected = false;
                    this.CenterAuthorizedProvinces.Add(item);
                }
            }
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetBranchesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效), loadOp => {
                if(loadOp.HasError || loadOp.Entities == null || !loadOp.Entities.Any()) {
                    return;
                }
                this.Branches.Clear();
                foreach(var entity in loadOp.Entities) {
                    this.Branches.Add(entity);
                }
                
                agency = this.DataContext as Agency;
                if(agency == null)
                    return;
                if (agency.AgencyAffiBranches.Any())
                {
                    foreach (var agencyAffiBranch in agency.AgencyAffiBranches)
                    {
                        foreach (var branch in Branches)
                        {
                            if (branch.Id == agencyAffiBranch.BranchId)
                                branch.IsSelected = true;
                        }
                    }
                }
                else
                {
                    //新增界面默认勾选全部
                    if (agency.EntityState == EntityState.New)
                    {
                        foreach (var branch in Branches)
                        {
                            branch.IsSelected = true;
                        }
                    }
                }
            }, null);

        }
        protected override void Reset() {
            var agency = this.DataContext as Agency;
            if(agency != null && (agency.Company != null && this.DomainContext.Companies.Contains(agency.Company))) {
                this.DomainContext.Companies.Detach(agency.Company);
            }
            if (this.DomainContext.Agencies.Contains(agency)) { 
                this.DomainContext.Agencies.Detach(agency);
            }
            foreach (var item in this.CenterAuthorizedProvinces.Where(r => r.IsSelected == true)) {
                item.IsSelected = false;
            }
        }

        private DataGridViewBase AgencyBindBranchForEditDataGridView {
            get {
                if(this.agencyBindBranchForEditDataGridView == null) {
                    this.agencyBindBranchForEditDataGridView = DI.GetDataGridView("AgencyBindRegionForEdit");
                    this.agencyBindBranchForEditDataGridView.DataContext = this;
                    this.agencyBindBranchForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.agencyBindBranchForEditDataGridView;
            }
        }
        private void CreateUI() {
            CreateFirstPanel();
            this.DomainContext.Load(this.DomainContext.GetTiledRegionsQuery().Where(r => string.IsNullOrEmpty(r.CityName) && string.IsNullOrEmpty(r.CountyName)),LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError || loadOp.Entities == null || !loadOp.Entities.Any()) {
                    return;
                }
                this.CenterAuthorizedProvinces.Clear();
                foreach(var entity in loadOp.Entities) {
                    this.CenterAuthorizedProvinces.Add(entity);
                    this.centerAuthorizedProvinceOrigs.Add(entity);
                }
            }, null);
        }

        private void CreateFirstPanel() {
            this.LayoutRoot.Children.Add(DI.GetDataEditPanel("Agency"));
            var dataEditPanel = this.EnterpriseInformationDataEditPanel;
            dataEditPanel.SetValue(Grid.RowProperty, 1);
            this.LayoutRoot.Children.Add(dataEditPanel);
            this.LayoutRoot.Children.Add(this.CreateVerticalLine(1, 0, 0, 2));

            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = CommonUIStrings.DataEditView_Title_Province,
                Content = this.AgencyBindBranchForEditDataGridView
            });
            tabControl.SetValue(Grid.ColumnProperty, 2);
            tabControl.SetValue(Grid.RowSpanProperty, 2);
            this.LayoutRoot.Children.Add(tabControl);
        }

        protected override void OnEditSubmitting() {
            var agency = this.DataContext as Agency;
            if(agency == null)
                return;
            agency.ValidationErrors.Clear();
            if (agency.Code == null)
                agency.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_Agency_CodeIsNull, new[] {
                    "Code"
                }));
            else
                agency.Code = agency.Code.Trim();
            if (agency.Code != null && agency.Code.Length > 11)
                agency.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_ResponsibleUnit_CodeIsOverLength_Agency, new[]{
                    "Code"
                }));
            if(agency.Name == null)
                agency.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_Agency_NameIsNull, new[] {
                    "Name"
                }));
            if(string.IsNullOrEmpty(agency.Company.CityName) || string.IsNullOrEmpty(agency.Company.ProvinceName) || string.IsNullOrEmpty(agency.Company.CountyName)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_Company_CityNameIsNull);
                return;
            }
            if(agency.MarketingDepartmentId == null) {
                //"分销中心是必填项"
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_Agency_Marketing);
                return;
            }
            if(string.IsNullOrEmpty(agency.Company.ContactPerson)){
                //联系人1不可为空
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_Agency_ContactPerson);
                return;
            }
            if(string.IsNullOrEmpty(agency.Company.ContactPhone)){
                //固定电话不可为空
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_Agency_ContactPhone);
                return;
            }
            if(string.IsNullOrEmpty(agency.Company.ContactMail)){
                //联系邮箱不可为空
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_Agency_ContactMail);
                return;
            }
            if(!string.IsNullOrEmpty(agency.Company.CustomerCode)){
                agency.Company.CustomerCode = agency.Company.CustomerCode.Trim();
            }
            if(agency.Company.OrderCycle != null && agency.Company.OrderCycle.Value <= 0) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_Agency_OrderCycle);
                return;
            }
            if(agency.Company.ShippingCycle != null && agency.Company.ShippingCycle.Value <= 0) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_Agency_ShippingCycle);
                return;
            }
            if(agency.Company.ArrivalCycle != null && agency.Company.ArrivalCycle.Value <= 0) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_Agency_ArrivalCycle);
                return;
            }
            if(agency.HasValidationErrors)
                return;
            agency.Company.RegionId = agency.Company.CityId;
            agency.Company.Code = agency.Code;
            agency.Company.Name = agency.Name;
            agency.Company.ShortName = agency.ShortName;
            if(!string.IsNullOrEmpty(agency.Company.CustomerCode)){
                agency.Company.CustomerCode = agency.Company.CustomerCode.Trim();
            }
            if(!string.IsNullOrEmpty(agency.Company.SupplierCode)){
                agency.Company.SupplierCode = agency.Company.SupplierCode.Trim();
            }
            
            if(agency.EntityState == EntityState.New) {
                foreach (var item in agency.AgencyAffiBranches) {
                    agency.AgencyAffiBranches.Remove(item);
                    if (this.DomainContext.AgencyAffiBranches.Contains(item))
                        this.DomainContext.AgencyAffiBranches.Remove(item);
                }
                foreach (var item1 in agency.CenterAuthorizedProvinces) {
                    agency.CenterAuthorizedProvinces.Remove(item1);
                    if (this.DomainContext.CenterAuthorizedProvinces.Contains(item1))
                        this.DomainContext.CenterAuthorizedProvinces.Remove(item1);
                }
                foreach(var branch in Branches.Where(e => e.IsSelected)) {
                    var agencyAffiBranch = new AgencyAffiBranch();
                    agencyAffiBranch.Agency = agency;
                    agencyAffiBranch.BranchId = branch.Id;
                    agency.AgencyAffiBranches.Add(agencyAffiBranch);
                }
                foreach(var centerAuthorizedProvince in CenterAuthorizedProvinces.Where(e => e.IsSelected)) {
                    var add = new CenterAuthorizedProvince();
                    add.ProvinceId = centerAuthorizedProvince.Id;
                    add.ProvinceName = centerAuthorizedProvince.ProvinceName;
                    agency.CenterAuthorizedProvinces.Add(add);
                }
            }
            if(agency.EntityState == EntityState.Modified || agency.EntityState == EntityState.Unmodified) {
                foreach (var pro in agency.CenterAuthorizedProvinces) {
                    agency.CenterAuthorizedProvinces.Remove(pro);
                    if (this.DomainContext.CenterAuthorizedProvinces.Contains(pro))
                        this.DomainContext.CenterAuthorizedProvinces.Remove(pro);
                }
                foreach (var centerAuthorizedProvince in CenterAuthorizedProvinces.Where(e => e.IsSelected)) {
                    var add = new CenterAuthorizedProvince();
                    add.ProvinceId = centerAuthorizedProvince.Id;
                    add.ProvinceName = centerAuthorizedProvince.ProvinceName;
                    agency.CenterAuthorizedProvinces.Add(add);
                }
            }
            ((IEditableObject)agency).EndEdit();
            base.OnEditSubmitting();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetAgencyWithCompanyAndAgencyAffiBranchQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.CenterAuthorizedProvinces.Clear();
                    foreach (var item in this.centerAuthorizedProvinceOrigs) {
                        item.IsSelected = false;
                        this.CenterAuthorizedProvinces.Add(item);
                    }
                    if (entity.CenterAuthorizedProvinces != null && entity.CenterAuthorizedProvinces.Any()) {
                        foreach (var centerAuthorizedProvince in entity.CenterAuthorizedProvinces) {
                            foreach (var province in CenterAuthorizedProvinces) {
                                if (province.Id == centerAuthorizedProvince.ProvinceId)
                                    province.IsSelected = true;
                            }
                        }
                    }
                    this.SetObjectToEdit(entity);
                    SetTiledRegionId();
                }
            }, null);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void SetTiledRegionId() {
            var agency = this.DataContext as Agency;
            if(agency == null)
                return;
            var company = agency.Company;
            if(company == null)
                return;
            if(company.ProvinceName == null)
                return;
            if(this.EnterpriseInformationDataEditPanel.KvProvinceNames.Any()) {
                company.ProvinceId = this.EnterpriseInformationDataEditPanel.KvProvinceNames.First(e => e.Value == company.ProvinceName).Key;
                if(company.CityName == null)
                    return;
                company.CityId = this.EnterpriseInformationDataEditPanel.KvCityNames.First(e => e.Value == company.CityName).Key;
                if(company.CountyName == null)
                    return;
                company.CountyId = this.EnterpriseInformationDataEditPanel.KvCountyNames.First(e => e.Value == company.CountyName).Key;
            } else {
                this.DomainContext.Load(DomainContext.GetTiledRegionsQuery().Where(ex => ex.CountyName != null).OrderBy(ex => ex.ProvinceName), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError)
                        return;
                    foreach(var repairObject in loadOp.Entities)
                        this.TiledRegions.Add(new TiledRegion {
                            Id = repairObject.Id,
                            ProvinceName = repairObject.ProvinceName,
                            CityName = repairObject.CityName,
                            CountyName = repairObject.CountyName,
                        });
                    this.EnterpriseInformationDataEditPanel.KvProvinceNames.Clear();
                    foreach(var item in TiledRegions) {
                        var values = this.EnterpriseInformationDataEditPanel.KvProvinceNames.Select(e => e.Value);
                        if(values.Contains(item.ProvinceName))
                            continue;
                        this.EnterpriseInformationDataEditPanel.KvProvinceNames.Add(new KeyValuePair {
                            Key = item.Id,
                            Value = item.ProvinceName
                        });
                    }
                    company.ProvinceId = this.EnterpriseInformationDataEditPanel.KvProvinceNames.First(e => e.Value == company.ProvinceName).Key;
                    if(company.CityName == null)
                        return;
                    company.CityId = this.EnterpriseInformationDataEditPanel.KvCityNames.First(e => e.Value == company.CityName).Key;
                    if(company.CountyName == null)
                        return;
                    company.CountyId = this.EnterpriseInformationDataEditPanel.KvCountyNames.First(e => e.Value == company.CountyName).Key;
                }, null);
            }
        }
    }
}
