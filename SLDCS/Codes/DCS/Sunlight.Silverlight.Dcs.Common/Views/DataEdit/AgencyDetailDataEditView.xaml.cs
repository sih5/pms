﻿using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using System.Collections.Generic;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class AgencyDetailDataEditView {
        private DataGridViewBase agencyBindBranchForEditDataGridView;
        private FrameworkElement enterpriseInformationDetailPanel;
        private ObservableCollection<TiledRegion> centerAuthorizedProvinces;
        private List<TiledRegion> centerAuthorizedProvinceOrigs = new List<TiledRegion>();

        private ObservableCollection<Branch> branches;

        public FrameworkElement EnterpriseInformationDetailPanel {
            get {
                return this.enterpriseInformationDetailPanel ?? (this.enterpriseInformationDetailPanel = DI.GetDetailPanel("EnterpriseInformationForAgency"));
            }
        }

        public ObservableCollection<TiledRegion> CenterAuthorizedProvinces {
            get {
                return centerAuthorizedProvinces ?? (this.centerAuthorizedProvinces = new ObservableCollection<TiledRegion>());
            }
            set {
                this.centerAuthorizedProvinces = value;
            }
        }

        public ObservableCollection<Branch> Branches {
            get {
                return branches ?? (this.branches = new ObservableCollection<Branch>());
            }
            set {
                this.branches = value;
            }
        }

        public AgencyDetailDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged -= AgencyDataEditView_DataContextChanged;
            this.DataContextChanged += AgencyDataEditView_DataContextChanged;
        }

        private void AgencyDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            this.CenterAuthorizedProvinces.Clear();
            foreach (var item in this.centerAuthorizedProvinceOrigs) {
                item.IsSelected = false;
                this.CenterAuthorizedProvinces.Add(item);
            }
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetBranchesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效), loadOp => {
                if(loadOp.HasError || loadOp.Entities == null || !loadOp.Entities.Any()) {
                    return;
                }
                this.Branches.Clear();
                foreach(var entity in loadOp.Entities) {
                    this.Branches.Add(entity);
                }
                var agency = this.DataContext as Agency;
                if(agency == null)
                    return;
                if(agency.AgencyAffiBranches.Any()) {
                    foreach(var agencyAffiBranch in agency.AgencyAffiBranches) {
                        foreach(var branch in Branches) {
                            if(branch.Id == agencyAffiBranch.BranchId)
                                branch.IsSelected = true;
                        }
                    }
                }
            }, null);
        }

        private DataGridViewBase AgencyBindBranchForEditDataGridView {
            get {
                if(this.agencyBindBranchForEditDataGridView == null) {
                    this.agencyBindBranchForEditDataGridView = DI.GetDataGridView("AgencyBindRegionForDetail");
                    this.agencyBindBranchForEditDataGridView.DataContext = this;
                    this.agencyBindBranchForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.agencyBindBranchForEditDataGridView;
            }
        }

        private void CreateUI() {
            CreateFirstPanel();
        }

        protected override string BusinessName {
            get {
                return CommonUIStrings.DataManagementView_Title_Agency;
            }
        }

        private void CreateFirstPanel() {
            this.LeftRoot.Children.Add(DI.GetDetailPanel("Agency"));
            var datadetailPanel = this.EnterpriseInformationDetailPanel;
            datadetailPanel.SetValue(Grid.RowProperty, 2);
            this.LeftRoot.Children.Add(datadetailPanel);
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(CommonUIStrings.DataEditView_Title_Province, null, () => this.AgencyBindBranchForEditDataGridView);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.SetValue(Grid.ColumnProperty, 1);
            this.LayoutRoot.Children.Add(detailDataEditView);

            this.DomainContext.Load(this.DomainContext.GetTiledRegionsQuery().Where(r => string.IsNullOrEmpty(r.CityName) && string.IsNullOrEmpty(r.CountyName)), LoadBehavior.RefreshCurrent, loadOp => {
                if (loadOp.HasError || loadOp.Entities == null || !loadOp.Entities.Any()) {
                    return;
                }
                this.CenterAuthorizedProvinces.Clear();
                foreach (var entity in loadOp.Entities) {
                    this.CenterAuthorizedProvinces.Add(entity);
                    this.centerAuthorizedProvinceOrigs.Add(entity);
                }
            }, null);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetAgencyWithCompanyAndAgencyAffiBranchQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
                if (entity.CenterAuthorizedProvinces != null && entity.CenterAuthorizedProvinces.Any()) {
                        foreach (var centerAuthorizedProvince in entity.CenterAuthorizedProvinces) {
                            foreach (var province in CenterAuthorizedProvinces) {
                                if (province.Id == centerAuthorizedProvince.ProvinceId)
                                    province.IsSelected = true;
                            }
                        }
                    }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

    }
}
