﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Text;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Web.Entities;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class AssemblyPartRequisitionLinkDataEditView {
        private readonly string[] kvNames = { 
            "SparePart_KeyCode" 
        }; 
        private KeyValueManager keyValueManager;

        public AssemblyPartRequisitionLinkDataEditView() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData();
            this.Initializer.Register(this.CreateUI);
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        public object PartKeyCodes {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void OnEditSubmitting() {
            var assemblyPartRequisitionLink = this.DataContext as AssemblyPartRequisitionLink;
            if(assemblyPartRequisitionLink == null)
                return;
            assemblyPartRequisitionLink.ValidationErrors.Clear();
            if (assemblyPartRequisitionLink.PartId == default(int) || string.IsNullOrEmpty(assemblyPartRequisitionLink.PartCode)) {
                UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Validation_VirtualFactoryPurchacePrice_sparePartCode);
                return;
            }
            if (assemblyPartRequisitionLink.KeyCode == null) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_KeyCodeIsNull);
                return;
            }

            if (assemblyPartRequisitionLink.Qty == null) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_QtyIsNull);
                return;
            }
            if (assemblyPartRequisitionLink.Qty.Value <= 0) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_QtyGreaterThanZero);
                return;
            }
            if (assemblyPartRequisitionLink.HasValidationErrors)
                return;
            ((IEditableObject)assemblyPartRequisitionLink).EndEdit();
            base.OnEditSubmitting();
        }

        protected override string BusinessName {
            get {
                return CommonUIStrings.DataEditView_Title_AssemblyPartRequisitionLink;
            }
        }

        private void CreateUI() {
            var queryWindowSparePart = DI.GetQueryWindow("SparePart");
            queryWindowSparePart.Loaded += QueryWindowSparepart_Loaded;
            queryWindowSparePart.SelectionDecided += this.SpareCode_SelectionDecided;
            this.ptbSparePart.PopupContent = queryWindowSparePart;
        }

        private void QueryWindowSparepart_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if (queryWindow != null) {
                var compositeFilterItem = new CompositeFilterItem();
                compositeFilterItem.Filters.Add(new FilterItem("PartType", typeof(int), FilterOperator.IsNotEqualTo, (int)DcsSparePartPartType.总成件_领用));
                queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
                queryWindow.ExchangeData(null, "RefreshQueryResult", null);
            }
        }

        private void SpareCode_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if (queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var sparePart = queryWindow.SelectedEntities.Cast<SparePart>().FirstOrDefault();
            if (sparePart == null)
                return;
            var assemblyPartRequisitionLink = this.DataContext as AssemblyPartRequisitionLink;
            if (assemblyPartRequisitionLink == null)
                return;
            assemblyPartRequisitionLink.PartId = sparePart.Id;
            assemblyPartRequisitionLink.PartCode = sparePart.Code;
            assemblyPartRequisitionLink.PartName = sparePart.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if (parent != null)
                parent.Close();

        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetAssemblyPartRequisitionLinksQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }
    }
}
