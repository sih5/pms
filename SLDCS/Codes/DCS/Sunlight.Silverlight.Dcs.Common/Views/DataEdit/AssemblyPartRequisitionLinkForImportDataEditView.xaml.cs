﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class AssemblyPartRequisitionLinkForImportDataEditView{
        private DataGridViewBase assemblyPartRequisitionLinkForImportDataGridView;
        private const string EXPORT_DATA_FILE_NAME = "总成件领用关系模板.xlsx";
        private ICommand exportFileCommand;
        public AssemblyPartRequisitionLinkForImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return CommonUIStrings.DataEditView_Title_ImportAssemblyPartRequisitionLink;
            }
        }

        protected DataGridViewBase ImportDataGridView {
            get {
                return this.assemblyPartRequisitionLinkForImportDataGridView ?? (this.assemblyPartRequisitionLinkForImportDataGridView = DI.GetDataGridView("AssemblyPartRequisitionLinkForImport"));
            }
        }

        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = CommonUIStrings.DataEditView_Title_ImportAssemblyPartRequisitionLink,
                Content = this.ImportDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.LayoutRoot.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.ImportAssemblyPartRequisitionLinkAsync(fileName);
            this.ExcelServiceClient.ImportAssemblyPartRequisitionLinkCompleted -= ExcelServiceClient_ImportAssemblyPartRequisitionLinkCompleted;
            this.ExcelServiceClient.ImportAssemblyPartRequisitionLinkCompleted += ExcelServiceClient_ImportAssemblyPartRequisitionLinkCompleted;
        }

        private void ExcelServiceClient_ImportAssemblyPartRequisitionLinkCompleted(object sender, ImportAssemblyPartRequisitionLinkCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!this.HasImportingError) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImport, 5);
            } else {
                if(!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if(!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_SparePartCode,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_SparePartName
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Column_KeyCode,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.Report_Title_InOutSettleSummary_Qty,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataDeTailPanel_Text_BottomStock_Remark
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
    }
}