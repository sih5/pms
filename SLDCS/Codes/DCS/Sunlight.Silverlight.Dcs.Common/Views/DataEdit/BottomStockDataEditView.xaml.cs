﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using System.Collections.ObjectModel;
using Sunlight.Silverlight.Core.Model;
using System.Windows;
using System.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit
{
    public partial class BottomStockDataEditView
    {
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategories = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvWarehouseNames = new ObservableCollection<KeyValuePair>();
        //private ObservableCollection<Warehouse> warehouses;
        public ObservableCollection<KeyValuePair> KvPartsSalesCategories
        {
            get
            {
                return this.kvPartsSalesCategories;
            }
        }
        public ObservableCollection<KeyValuePair> KvWarehouseNames
        {
            get
            {
                return this.kvWarehouseNames;
            }
        }
        //public ObservableCollection<Warehouse> Warehouses
        //{
        //    get
        //    {
        //        return warehouses ?? (this.warehouses = new ObservableCollection<Warehouse>());
        //    }
        //    set
        //    {
        //        this.warehouses = value;
        //    }
        //}

        public BottomStockDataEditView()
        {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            //this.Loaded += PersonnelSupplierRelationDataEditView_Loaded;
        }

        void PersonnelSupplierRelationDataEditView_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            this.Title = CommonUIStrings.DataEditView_Title_AddBottomStock;
        }

        private void CustomerInformationQueryWindow_SelectionDecided(object sender, EventArgs e)
        {
            var queryWindow = sender as DcsQueryWindowBase;
            if (queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var company = queryWindow.SelectedEntities.Cast<Company>().FirstOrDefault();
            if (company == null)
                return;

            var bottomStock = this.DataContext as BottomStock;
            if (bottomStock == null)
                return;
            bottomStock.CompanyID = company.Id;
            bottomStock.CompanyCode = company.Code;
            bottomStock.CompanyName = company.Name;
            bottomStock.CompanyType = company.Type;
            //this.customerId = company.Id;
            this.CustomerCompanyPopoupTextBox.Text = company.Code;
            this.CbCompanyName.Text = company.Name;
            //this.CompanyType = company.Type;

            //仓库名称
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetWarehousesOrderByNameQuery().Where(er => er.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && er.StorageCompanyId == bottomStock.CompanyID && er.Status == (int)DcsBaseDataStatus.有效 && (er.PwmsInterface == false || er.PwmsInterface == null)), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                    return;
                this.KvWarehouseNames.Clear();
                foreach (var warehouse in loadOp.Entities)
                    this.KvWarehouseNames.Add(new KeyValuePair
                    {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
            }, null);

            var parent = queryWindow.ParentOfType<RadWindow>();
            if (parent != null)
                parent.Close();
        }

        private void PartsBranchSupplierName_SelectionDecided(object sender, EventArgs e)
        {
            var queryWindow = sender as DcsQueryWindowBase;
            if (queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var sparePart = queryWindow.SelectedEntities.Cast<SparePart>().FirstOrDefault();
            if (sparePart == null)
                return;

            var bottomStock = this.DataContext as BottomStock;
            if (bottomStock == null)
                return;
            bottomStock.SparePartId = sparePart.Id;
            bottomStock.SparePartCode = sparePart.Code;
            bottomStock.SparePartName = sparePart.Name;

            var parent = queryWindow.ParentOfType<RadWindow>();
            if (parent != null)
                parent.Close();
        }

        private void CreateUI()
        {
            //企业编号
            var customerInformationQueryWindow = DI.GetQueryWindow("CompaniesForBottomStock");
            customerInformationQueryWindow.SelectionDecided += this.CustomerInformationQueryWindow_SelectionDecided;
            this.CustomerCompanyPopoupTextBox.PopupContent = customerInformationQueryWindow;
            //配件
            var queryWindowSparePart = DI.GetQueryWindow("SparePart");
            queryWindowSparePart.SelectionDecided += this.PartsBranchSupplierName_SelectionDecided;
            this.ptbSparePart.PopupContent = queryWindowSparePart;

            //品牌查询
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                    return;
                foreach (var partsSalesCategor in loadOp.Entities)
                    this.kvPartsSalesCategories.Add(new KeyValuePair
                    {
                        Key = partsSalesCategor.Id,
                        Value = partsSalesCategor.Name
                    });
                this.CbPartsSalesCategoryName.SelectedIndex = 0;//默认品牌
            }, null);
            this.isConent = true;
        }


        private ObservableCollection<BottomStock> bottomStocks;
        public ObservableCollection<BottomStock> BottomStocks
        {
            get
            {
                if (this.bottomStocks == null)
                {
                    this.bottomStocks = new ObservableCollection<BottomStock>();
                } return bottomStocks;
            }
        }

        private void LoadEntityToEdit(int id)
        {
            this.DomainContext.Load(this.DomainContext.QueryBottomStocksQuery(null).Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                {
                    if (!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if (entity != null)
                {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        protected override void OnEditSubmitting()
        {
            var bottomStock = this.DataContext as BottomStock;
            if (bottomStock == null)
                return;
            bottomStock.ValidationErrors.Clear();
            if (bottomStock.PartsSalesCategoryId == default(int))
            {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_PartDeleaveInformation_PartsSalesCategoryIdIsNull);
                return;
            }
            if (string.IsNullOrEmpty(bottomStock.CompanyCode))
            {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_BottomStock_CompanyCodeIsNull);
                return;
            }
            if (bottomStock.StockQty == 0|| bottomStock.StockQty ==null)
            {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_BottomStock_StockQty);
                return;
            }
            if (bottomStock.SparePartId == default(int))
            {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_BottomStock_SparepartIsNull);
                return;
            }
            if (bottomStock.CompanyType!=(int)DcsCompanyType.服务站)
            {
                if (bottomStock.WarehouseID == default(int)|| bottomStock.WarehouseID ==null)
                {
                    UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_BottomStock_WarehouseIsNull);
                    return;
                }
            }
            bottomStock.PartsSalesCategoryName = this.CbPartsSalesCategoryName.Text;
            bottomStock.WarehouseName= this.CbWarehouseName.Text;
            bottomStock.Remark = this.CbRemark.Text;
            ((IEditableObject)bottomStock).EndEdit();
            try
            {
                if (this.EditState == DataEditState.Edit)
                {
                    if (bottomStock.Can修改保底库存)
                        bottomStock.修改保底库存();
                }
                else if (this.EditState == DataEditState.New)
                {
                    if (bottomStock.Can新增保底库存)
                        bottomStock.新增保底库存();
                }
            }
            catch (ValidationException ex)
            {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }
        protected override void OnEditCancelled()
        {
            base.OnEditCancelled();
            this.CbCompanyName.Text = "";
            this.CbPartsSalesCategoryName.Text = "";
            this.CbRemark.Text = "";
            this.CbWarehouseName.Text = "";
        }
        protected override void OnEditSubmitted()
        {
            base.OnEditSubmitted();
            this.CbCompanyName.Text = "";
            this.CbPartsSalesCategoryName.Text = "";
            this.CbRemark.Text = "";
            this.CbWarehouseName.Text = "";
        }
        protected override bool OnRequestCanSubmit()
        {
            this.CbPartsSalesCategoryName.SelectedIndex = 0;//默认品牌
            return true;
        }

        protected override string BusinessName
        {
            get
            {
                return CommonUIStrings.DataEditView_Title_ManageBottomStock;
            }
        }

        public override void SetObjectToEditById(object id)
        {
            if (this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
