﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using System.ComponentModel.DataAnnotations;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class BottomStockForAbandonImport {
        private ICommand exportFileCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出作废保底库存模板.xlsx";
        private DataGridViewBase bottomStockForImportForEdit;
        protected DataGridViewBase BottomStockForImportForEdit
        {
            get
            {
                if (this.bottomStockForImportForEdit == null)
                {
                    this.bottomStockForImportForEdit = DI.GetDataGridView("BottomStockForImportForEdit");
                    this.bottomStockForImportForEdit.DataContext = this.DataContext;
                }
                return this.bottomStockForImportForEdit;
            }
        }
        public BottomStockForAbandonImport()
        {
            InitializeComponent();
            this.Initializer.Register(CreateUI);
        }

        protected override string Title
        {
            get
            {
                return CommonUIStrings.DataEditView_Title_ImportBottomStockAbandon;
            }
        }

        private void CreateUI()
        {
            this.ShowSaveButton();
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem
            {
                Header = CommonUIStrings.DataEditView_Title_ImportBottomStockList,
                Content = this.BottomStockForImportForEdit
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        private void UploadFileProcessing(string fileName)
        {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.ImportBottomStockAbandonAsync(fileName);
            this.ExcelServiceClient.ImportBottomStockAbandonCompleted -= ExcelServiceClient_ImportBottomStockAbandonCompleted;
            this.ExcelServiceClient.ImportBottomStockAbandonCompleted += ExcelServiceClient_ImportBottomStockAbandonCompleted; ;
        }

        private void ExcelServiceClient_ImportBottomStockAbandonCompleted(object sender, ImportBottomStockAbandonCompletedEventArgs e)
        {

            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if (!HasImportingError)
            {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImport, 10);
            }
            else
            {
                if (!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if (!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }
        private void InitializeCommand()
        {
            try
            {
                this.exportFileCommand = new Core.Command.DelegateCommand(() =>
                {
                    var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditView_Text_BottomStock_Brand,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataDeTailPanel_Text_Company_Code,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_WarehouseCode
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataGridView_ColumnItem_Title_SparePart_Code,
                                                IsRequired = true
                                            }
                                        };
                    this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp =>
                    {
                        if (loadOp.HasError)
                        {
                            if (!loadOp.IsErrorHandled)
                                loadOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                        }
                        if (!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                            this.ExportFile(loadOp.Value);
                    }, null);
                });
            }
            catch (ValidationException ex)
            {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
        }

        protected override bool IsAutomaticUploadFile
        {
            get
            {
                return true;
            }
        }

        public ICommand ExportTemplateCommand
        {
            get
            {
                if (exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

    }
}