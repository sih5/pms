﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using System.Collections.ObjectModel;
using Sunlight.Silverlight.Core.Model;
using System.Windows;
using System.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class BottomStockForceReserveSubDataEditView {
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategories = new ObservableCollection<KeyValuePair>();
        public ObservableCollection<KeyValuePair> KvPartsSalesCategories {
            get {
                return this.kvPartsSalesCategories;
            }
        }
        public BottomStockForceReserveSubDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CbReserveType_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            var bottomStock = this.DataContext as BottomStockForceReserveSub;
            if(bottomStock == null || bottomStock.Id>0)
                return;
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetBottomStockForceReserveSubsQuery().Where(er => er.BottomStockForceReserveTypeId == bottomStock.BottomStockForceReserveTypeId).OrderByDescending(t => t.Id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Entities.Count() == 0) {
                    bottomStock.SubCode = bottomStock.ReserveType + "001";
                    bottomStock.Serial = 1;
                } else if(loadOp.Entities.First().Serial >= 9 && loadOp.Entities.First().Serial<=99) {
                    bottomStock.SubCode = bottomStock.ReserveType + "0" + (loadOp.Entities.First().Serial + 1);
                    bottomStock.Serial = loadOp.Entities.First().Serial + 1;
                } else if(loadOp.Entities.First().Serial <9) {                   
                    bottomStock.SubCode = bottomStock.ReserveType + "00" + (loadOp.Entities.First().Serial + 1);
                    bottomStock.Serial = loadOp.Entities.First().Serial + 1;
                } else {
                    bottomStock.SubCode = bottomStock.ReserveType + (loadOp.Entities.First().Serial + 1);
                    bottomStock.Serial = loadOp.Entities.First().Serial + 1;
                }
            }, null);
        }
        private void CreateUI() {
            //储备类别
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetBottomStockForceReserveTypesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategor in loadOp.Entities)
                    this.kvPartsSalesCategories.Add(new KeyValuePair {
                        Key = partsSalesCategor.Id,
                        Value = partsSalesCategor.ReserveType
                    });
                this.CbReserveType.SelectedIndex = 0;
            }, null);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetBottomStockForceReserveSubsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            var bottomStock = this.DataContext as BottomStockForceReserveSub;
            if(bottomStock == null)
                return;
            bottomStock.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(bottomStock.SubName)) {
                UIHelper.ShowNotification("请先写子项目名称");
                return;
            }

            ((IEditableObject)bottomStock).EndEdit();
            try {
                if(this.EditState == DataEditState.Edit) {
                    if(bottomStock.Can更新保底库存强制储备类别子项目)
                        bottomStock.更新保底库存强制储备类别子项目();
                } else if(this.EditState == DataEditState.New) {
                    if(bottomStock.Can新增保底库存强制储备类别子项目)
                        bottomStock.新增保底库存强制储备类别子项目();

                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }
        protected override void OnEditCancelled() {
            base.OnEditCancelled();
            this.SubCode.Text = "";
            this.SubName.Text = "";

        }
        protected override void OnEditSubmitted() {
            base.OnEditSubmitted();
            this.SubCode.Text = "";
            this.SubName.Text = "";
        }
        protected override bool OnRequestCanSubmit() {
         //   this.CbReserveType.SelectedIndex = 0;
            return true;
        }

        protected override string BusinessName {
            get {
                return CommonUIStrings.DataManagementView_Title_BottomStockForceReserveSub;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }


    }
}
