﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Text;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Web.Entities;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class BottomStockForceReserveTypeDataEditView {
        public BottomStockForceReserveTypeDataEditView() {
            InitializeComponent();
        }
        protected override bool OnRequestCanSubmit() {
            return true;
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetBottomStockForceReserveTypesQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
        protected override void OnEditSubmitting() {
            var bottomStockForceReserveType = this.DataContext as BottomStockForceReserveType;
            if(bottomStockForceReserveType == null)
                return;
            bottomStockForceReserveType.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(bottomStockForceReserveType.ReserveType)) {
                UIHelper.ShowNotification("储备类别不能为空");
                return;
            } if(string.IsNullOrEmpty(bottomStockForceReserveType.ReserveName)) {
                UIHelper.ShowNotification("储备名称不能为空");
                return;
            }
            ((IEditableObject)bottomStockForceReserveType).EndEdit();          
            try {
                if(this.EditState == DataEditState.Edit) {
                    if(bottomStockForceReserveType.Can修改保底库存强制储备类别)
                        bottomStockForceReserveType.修改保底库存强制储备类别();

                } else if(this.EditState == DataEditState.New) {
                    if(bottomStockForceReserveType.Can新增保底库存强制储备类别) {
                        bottomStockForceReserveType.新增保底库存强制储备类别();
                    }
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        protected override string BusinessName {
            get {
                return "储备类别";
            }
        }

    }
}
