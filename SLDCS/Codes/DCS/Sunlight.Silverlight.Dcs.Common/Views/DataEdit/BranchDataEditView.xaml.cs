﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System.ComponentModel.DataAnnotations;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class BranchDataEditView {
        private EnterpriseInformationDataEditPanel enterpriseInformationDataEditPanel;
        private List<TiledRegion> tiledRegions;

        private List<TiledRegion> TiledRegions {
            get {
                return this.tiledRegions ?? (this.tiledRegions = new List<TiledRegion>());
            }
        }
        public BranchDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_Branch;
            }
        }

        private EnterpriseInformationDataEditPanel EnterpriseInformationDataEditPanel {
            get {
                return this.enterpriseInformationDataEditPanel ?? (this.enterpriseInformationDataEditPanel = (EnterpriseInformationDataEditPanel)DI.GetDataEditPanel("EnterpriseInformation"));

            }
        }

        private void CreateUI() {
            this.EnterpriseInformationDataEditPanel.SetValue(Grid.RowProperty, 2);
            this.Root.Children.Add(this.EnterpriseInformationDataEditPanel);
            this.isConent = true;
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetBranchWithCompanyQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                    SetTiledRegionId();
                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            var branch = this.DataContext as Branch;
            if(branch == null)
                return;

            branch.ValidationErrors.Clear();
            if (branch.Code == null)
                branch.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_ResponsibleUnit_CodeIsNull_Branch, new[] {
                    "Code"
                }));
            else
                branch.Code = branch.Code.Trim();
            if (branch.Code != null && branch.Code.Length > 11)
                branch.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_ResponsibleUnit_CodeIsOverLength_Branch, new[]{
                    "Code"
                }));
            if (branch.Name == null)
                branch.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_ResponsibleUnit_NameIsNull_Branch, new[] {
                    "Name"
                }));

            if(branch.EntityState == EntityState.New) {
                branch.Company.Code = branch.Code;
                branch.Company.Name = branch.Name;
                branch.Company.ShortName = branch.Abbreviation;
            }

            if(string.IsNullOrEmpty(branch.Company.CityName) || string.IsNullOrEmpty(branch.Company.ProvinceName) || string.IsNullOrEmpty(branch.Company.CountyName)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_Company_CityNameIsNull);
                return;
            }
            if(branch.Company.Remark != null) {
                var count = System.Text.Encoding.UTF8.GetByteCount(branch.Company.Remark);
                if(count > 200) {
                    UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_Company_RemarkIsTooLength);
                    return;
                }
            }
            branch.Company.RegionId = branch.Company.CountyId;
            if (branch.HasValidationErrors)
                return;
            ((IEditableObject)branch).EndEdit();
            base.OnEditSubmitting();
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        protected override void Reset() {
            var branch = this.DataContext as Branch;
            if(branch != null && (branch.Company != null && this.DomainContext.Companies.Contains(branch.Company))) {
                this.DomainContext.Companies.Detach(branch.Company);
            }
            if(this.DomainContext.Branches.Contains(branch))
                this.DomainContext.Branches.Detach(branch);
        }

        private void SetTiledRegionId() {
            var branch = this.DataContext as Branch;
            if(branch == null)
                return;
            var company = branch.Company;
            if(company == null)
                return;
            if(company.ProvinceName == null)
                return;
            if(this.EnterpriseInformationDataEditPanel.KvProvinceNames.Any()) {
                company.ProvinceId = this.EnterpriseInformationDataEditPanel.KvProvinceNames.First(e => e.Value == company.ProvinceName).Key;
                if(company.CityName == null)
                    return;
                company.CityId = this.EnterpriseInformationDataEditPanel.KvCityNames.First(e => e.Value == company.CityName).Key;
                if(company.CountyName == null)
                    return;
                company.CountyId = this.EnterpriseInformationDataEditPanel.KvCountyNames.First(e => e.Value == company.CountyName).Key;
            } else {
                this.DomainContext.Load(DomainContext.GetTiledRegionsQuery().Where(ex => ex.CountyName != null).OrderBy(ex => ex.ProvinceName), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError)
                        return;
                    foreach(var repairObject in loadOp.Entities)
                        this.TiledRegions.Add(new TiledRegion {
                            Id = repairObject.Id,
                            ProvinceName = repairObject.ProvinceName,
                            CityName = repairObject.CityName,
                            CountyName = repairObject.CountyName,
                        });
                    this.EnterpriseInformationDataEditPanel.KvProvinceNames.Clear();
                    foreach(var item in TiledRegions) {
                        var values = this.EnterpriseInformationDataEditPanel.KvProvinceNames.Select(e => e.Value);
                        if(values.Contains(item.ProvinceName))
                            continue;
                        this.EnterpriseInformationDataEditPanel.KvProvinceNames.Add(new KeyValuePair {
                            Key = item.Id,
                            Value = item.ProvinceName
                        });
                    }
                    company.ProvinceId = this.EnterpriseInformationDataEditPanel.KvProvinceNames.First(e => e.Value == company.ProvinceName).Key;
                    if(company.CityName == null)
                        return;
                    company.CityId = this.EnterpriseInformationDataEditPanel.KvCityNames.First(e => e.Value == company.CityName).Key;
                    if(company.CountyName == null)
                        return;
                    company.CountyId = this.EnterpriseInformationDataEditPanel.KvCountyNames.First(e => e.Value == company.CountyName).Key;
                }, null);
            }
        }
    }
}
