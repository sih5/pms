﻿using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Dcs.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class BranchDetailDataEditView {
        private FrameworkElement enterpriseInformationDetailPanel;
        private KeyValueManager keyValueManager;
        private readonly string[] kvNames = new[] {
            "MasterData_Status"
        };

        public BranchDetailDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_Branch;
            }
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private FrameworkElement EnterpriseInformationDetailPanel {
            get {
                return this.enterpriseInformationDetailPanel ?? (this.enterpriseInformationDetailPanel = DI.GetDetailPanel("EnterpriseInformation"));
            }
        }

        private void CreateUI() {
            this.EnterpriseInformationDetailPanel.SetValue(Grid.RowProperty, 2);
            this.Root.Children.Add(this.EnterpriseInformationDetailPanel);
            this.KeyValueManager.LoadData();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetBranchWithCompanyQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
