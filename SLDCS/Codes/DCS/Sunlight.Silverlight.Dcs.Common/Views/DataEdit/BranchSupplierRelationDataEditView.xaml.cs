﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class BranchSupplierRelationDataEditView : INotifyPropertyChanged {
        private int partsSalesCategoryId;
        private string partsSalesCategoryName;
        private string remark;

        private ObservableCollection<BranchSupplierRelation> branchSupplierRelations;
        private DataGridViewBase branchSupplierRelationForEditDatGridView;
        private ObservableCollection<KeyValuePair> kvPartsSalesCategories;

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_BranchSupplierRelation;

            }
        }

        public DataGridViewBase BranchSupplierRelationForEditDatGridView {
            get {
                if(this.branchSupplierRelationForEditDatGridView == null) {
                    this.branchSupplierRelationForEditDatGridView = DI.GetDataGridView("BranchSupplierRelationForEdit");
                    this.branchSupplierRelationForEditDatGridView.DataContext = this;
                    this.branchSupplierRelationForEditDatGridView.DomainContext = this.DomainContext;
                }
                return this.branchSupplierRelationForEditDatGridView;
            }
        }

        public ObservableCollection<BranchSupplierRelation> BranchSupplierRelations {
            get {
                if(this.branchSupplierRelations == null)
                    this.branchSupplierRelations = new ObservableCollection<BranchSupplierRelation>();
                return branchSupplierRelations;
            }
        }

        public BranchSupplierRelationDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.Loaded += BranchSupplierRelationDataEditView_Loaded;
        }

        private void BranchSupplierRelationDataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.kvPartsSalesCategories.Clear();
                foreach(var partsSalesCategor in loadOp.Entities)
                    this.kvPartsSalesCategories.Add(new KeyValuePair {
                        Key = partsSalesCategor.Id,
                        Value = partsSalesCategor.Name,
                    });
            }, null);
        }

        protected virtual void CreateUI() {
            this.LayoutRoot.Children.Add(this.CreateVerticalLine(1, 0, 0, 0));
            var detail = new DcsDetailDataEditView();
            detail.Register(CommonUIStrings.DataGridView_Title_Common_Suppliers, null, () => this.BranchSupplierRelationForEditDatGridView);
            Grid.SetColumn(detail, 2);
            this.LayoutRoot.Children.Add(detail);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {
            this.BranchSupplierRelationForEditDatGridView.CommitEdit();
            foreach(var entity in this.BranchSupplierRelations) {
                entity.ValidationErrors.Clear();
            }
            if(this.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_BranchSupplier_BranchIsNull);
                return;
            }
            if(!this.BranchSupplierRelations.Any()) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_BranchSupplier_BranchSupplierRelations);
                return;
            }
            foreach(var entity in this.BranchSupplierRelations) {
                entity.PartsSalesCategoryId = this.PartsSalesCategoryId;
                ((IEditableObject)entity).EndEdit();
            }
            foreach(var entity in this.DomainContext.BranchSupplierRelations.Where(e => e.EntityState == EntityState.New).ToList()) {
                if(!this.DomainContext.BranchSupplierRelations.Contains(entity))
                    this.DomainContext.BranchSupplierRelations.Add(entity);
            }
            if(this.BranchSupplierRelations.Any(r => r.ValidationErrors.Any()))
                return;
            base.OnEditSubmitting();
        }

        protected override void OnEditSubmitted() {
            this.BranchSupplierRelations.Clear();
            this.PartsSalesCategoryId = default(int);
            this.PartsSalesCategoryName = string.Empty;
            this.Remark = string.Empty;
            base.OnEditSubmitted();
        }

        protected override void OnEditCancelled() {
            this.DomainContext.RejectChanges();
            this.BranchSupplierRelations.Clear();
            this.PartsSalesCategoryId = default(int);
            this.PartsSalesCategoryName = string.Empty;
            this.Remark = string.Empty;
            base.OnEditCancelled();
        }

        public int PartsSalesCategoryId {
            get {
                return this.partsSalesCategoryId;
            }
            set {
                this.partsSalesCategoryId = value;
                this.OnPropertyChanged("PartsSalesCategoryId");
            }
        }

        public string Remark {
            get {
                return this.remark;
            }
            set {
                this.remark = value;
                this.OnPropertyChanged("Remark");
            }
        }

        public string PartsSalesCategoryName {
            get {
                return this.partsSalesCategoryName;
            }
            set {
                this.partsSalesCategoryName = value;
                this.OnPropertyChanged("PartsSalesCategoryName");
            }
        }

        public ObservableCollection<KeyValuePair> KvPartsSalesCategories {
            get {
                return this.kvPartsSalesCategories ?? (this.kvPartsSalesCategories = new ObservableCollection<KeyValuePair>());
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void DcsComboBox_SelectionChanged_1(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            if(this.BranchSupplierRelations.Any()) {
                DcsUtils.Confirm(CommonUIStrings.DataEditView_Validation_BranchSupplier_Confirm, () => {
                    foreach(var detail in this.BranchSupplierRelations.ToArray()) {
                        this.BranchSupplierRelations.Remove(detail);
                        this.DomainContext.BranchSupplierRelations.Detach(detail);
                    }
                });
            }
        }
    }
}
