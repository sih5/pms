﻿using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public class BranchSupplierRelationForEditDataEditView : BranchSupplierRelationDataEditView {
        public int PartsSalesCategoryId1;
        protected override void CreateUI() {
            this.LayoutRoot.Children.Add(this.CreateVerticalLine(1, 0, 0, 0));
            var detail = new DcsDetailDataEditView();
            detail.UnregisterButton(detail.InsertButton);
            detail.UnregisterButton(detail.DeleteButton);
            detail.Register(CommonUIStrings.DataGridView_Title_Common_Suppliers, null, () => this.BranchSupplierRelationForEditDatGridView);
            Grid.SetColumn(detail, 2);
            this.LayoutRoot.Children.Add(detail);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetBranchSupplierRelationWithPartsSalesCategoryQuery().Where(entity => entity.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                this.BranchSupplierRelations.Clear();
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    entity.SequeueNumber = 1;
                    this.Remark = entity.Remark;
                    this.PartsSalesCategoryId = entity.PartsSalesCategoryId;
                    this.PartsSalesCategoryId1 = entity.PartsSalesCategoryId;
                    this.BranchSupplierRelations.Add(entity);
                    this.SetObjectToEdit(entity);
                }
                this.BranchSupplierRelationForEditDatGridView.ExchangeData(null, null, PartsSalesCategoryId1);
            }, null);
        }

        //由于继承新增界面，此事件不能删
        private void DcsComboBox_SelectionChanged_1(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            //if(this.BranchSupplierRelations.Any()) {
            //    DcsUtils.Confirm("品牌变更，将清空清单数据，是否变更", () => {
            //        foreach(var detail in this.BranchSupplierRelations.ToArray()) {
            //            this.BranchSupplierRelations.Remove(detail);
            //            this.DomainContext.BranchSupplierRelations.Detach(detail);
            //        }
            //    });
            //}
        }
    }
}
