﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class BranchSupplierRelationForImportUpdateDataEditView {
        private DataGridViewBase branchSupplierRelationForImportDataGridView;
        private const string EXPORT_DATA_FILE_NAME = "导出分公司与供应商模板.xlsx";
        private ICommand exportFileCommand;
        public BranchSupplierRelationForImportUpdateDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return CommonUIStrings.DataEditView_Title_ImportEdit_BranchSupplier;
            }
        }

        protected DataGridViewBase ImportDataGridView {
            get {
                if(this.branchSupplierRelationForImportDataGridView == null) {
                    this.branchSupplierRelationForImportDataGridView = DI.GetDataGridView("BranchSupplierRelationForImport");
                }
                return this.branchSupplierRelationForImportDataGridView;
            }
        }


        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = CommonUIStrings.DataEditView_Title_ImportEdit_BranchSupplierList,
                Content = this.ImportDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.LayoutRoot.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.ImportBranchSupplierRelationForUpdateAsync(fileName);
            this.ExcelServiceClient.ImportBranchSupplierRelationForUpdateCompleted -= ExcelServiceClient_ImportBranchSupplierRelationForUpdateCompleted;
            this.ExcelServiceClient.ImportBranchSupplierRelationForUpdateCompleted += ExcelServiceClient_ImportBranchSupplierRelationForUpdateCompleted;
        }

        private void ExcelServiceClient_ImportBranchSupplierRelationForUpdateCompleted(object sender, ImportBranchSupplierRelationForUpdateCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!this.HasImportingError) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImport, 5);
            } else {
                if(!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if(!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name =CommonUIStrings.QueryPanel_QueryItem_Title_BranchSupplierRelation_BranchCode
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_BranchSupplierRelation_BranchName
                                            },
                                            new ImportTemplateColumn {
                                                Name =CommonUIStrings.QueryPanel_QueryItem_Title_BranchSupplierRelation_PartsSalesCategoryName,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierCode,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierName,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_BranchSupplierRelation_BussinessCode,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_BranchSupplierRelation_BussinessName,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_BranchSupplierRelation_ArrivalCycle,
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_BranchSupplierRelation_ArrivalCyclePur,
                                            },
                                            //new ImportTemplateColumn {
                                            //    Name = "配件管理费率等级名称",
                                            //},
                                            //new ImportTemplateColumn {
                                            //    Name = "配件索赔系数",
                                            //},
                                            //new ImportTemplateColumn {
                                            //    Name = "配件索赔价格类型",
                                            //},
                                            //new ImportTemplateColumn {
                                            //    Name = "是否按工时单价索赔"
                                            //},
                                            //new ImportTemplateColumn {
                                            //    Name = "工时单价",
                                            //},
                                            //new ImportTemplateColumn {
                                            //    Name = "工时系数",
                                            //},
                                            //new ImportTemplateColumn {
                                            //    Name = "是否存在外出费用"
                                            //},  
                                            //new ImportTemplateColumn {
                                            //    Name = "外出里程单价（元/公里）",
                                            //},
                                            //new ImportTemplateColumn {
                                            //    Name = "外出人员单价（元/人天)",
                                            //},
                                            //new ImportTemplateColumn {
                                            //    Name = "旧件运费系数",
                                            //},
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartsInboundCheckBillRemark,
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
    }
}