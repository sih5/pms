﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Core.Model;
using System.Collections.ObjectModel;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Core;
using Telerik.Windows.Controls;
using System.Windows;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class CenterApproveBillDataEditView {
        private  ObservableCollection<KeyValuePair> kvWeeks = new ObservableCollection<KeyValuePair>();
        private ObservableCollection<Warehouse> kvWarehouses;
        public ObservableCollection<KeyValuePair> KvWeeks {
            get {
                return this.kvWeeks ?? (this.kvWeeks = new ObservableCollection<KeyValuePair>());
            }
        }
        public ObservableCollection<Warehouse> KvWarehouses {
            get {
                return this.kvWarehouses ?? (this.kvWarehouses = new ObservableCollection<Warehouse>());
            }
        }
        public CenterApproveBillDataEditView() {
            this.InitializeComponent();
            this.kvWeeks.Add(new KeyValuePair
            {
                Key = 1,
                Value = CommonUIStrings.QueryPanel_QueryItem_Title_CenterApproveBill_ApproveWeek_Monday
            });
            this.kvWeeks.Add(new KeyValuePair
            {
                Key = 2,
                Value = CommonUIStrings.QueryPanel_QueryItem_Title_CenterApproveBill_ApproveWeek_Tuesday
            });
            this.kvWeeks.Add(new KeyValuePair
            {
                Key = 3,
                Value = CommonUIStrings.QueryPanel_QueryItem_Title_CenterApproveBill_ApproveWeek_Wednesday
            });
            this.kvWeeks.Add(new KeyValuePair
            {
                Key = 4,
                Value = CommonUIStrings.QueryPanel_QueryItem_Title_CenterApproveBill_ApproveWeek_Thursday
            });
            this.kvWeeks.Add(new KeyValuePair
            {
                Key = 5,
                Value = CommonUIStrings.QueryPanel_QueryItem_Title_CenterApproveBill_ApproveWeek_Friday
            });
            this.Initializer.Register(this.CreateUI);
        }

        private void WeekComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            var centerApproveBill = this.DataContext as CenterApproveBill;
            if (centerApproveBill == null)
                return;
            try {
                if (!string.IsNullOrEmpty(centerApproveBill.ApproveWeek)) {
                    var key = kvWeeks.FirstOrDefault(r => int.Parse(centerApproveBill.ApproveWeek) == r.Key);
                    if (key != null) {
                        this.WeekComboBox.Text = key.Value;
                    }
                }
            } catch {
                return;
            }
            
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override string BusinessName {
            get {
                return CommonUIStrings.QueryPanel_Title_CenterApproveBillTitle;
            }
        }

        protected override void OnEditSubmitting() {
            var centerApproveBill = this.DataContext as CenterApproveBill;
            if(centerApproveBill == null)
                return;
            centerApproveBill.ValidationErrors.Clear();
            
            if (centerApproveBill.CompanyId == default(int)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_CenterApprove_CenterIsnull);
                return;
            }
            if (string.IsNullOrEmpty(centerApproveBill.ApproveWeek)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_CenterApprove_WeekIsnull);
                return;
            }
            if(centerApproveBill.HasValidationErrors)
                return;
            ((IEditableObject)centerApproveBill).EndEdit();
            try{
                if(EditState == DataEditState.New) {
                    if (centerApproveBill.CanInsertCenterApproveBill)
                    centerApproveBill.InsertCenterApproveBill();
                } else {
                    if (centerApproveBill.CanUpdateCenterApproveBill)
                    centerApproveBill.UpdateCenterApproveBill();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        private void CreateUI() {
            var companyForApproveBillQueryWindow = DI.GetQueryWindow("CompanyForApproveBill");
            companyForApproveBillQueryWindow.Loaded += this.companyForApproveBillQueryWindow_Loaded;
            companyForApproveBillQueryWindow.SelectionDecided += this.companyForApproveBillQueryWindow_SelectionDecided;
            this.CustomerCompanyPopoupTextBox.PopupContent = companyForApproveBillQueryWindow;
           
        }

        private void companyForApproveBillQueryWindow_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if (queryWindow == null)
                return;
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common","Type",(int)DcsCompanyType.代理库
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "Type", false
            });
        }
        private void companyForApproveBillQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if (queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var company = queryWindow.SelectedEntities.Cast<Company>().FirstOrDefault();
            if (company == null)
                return;

            var centerApproveBill = this.DataContext as CenterApproveBill;
            if (centerApproveBill == null)
                return;
            centerApproveBill.CompanyId = company.Id;
            centerApproveBill.CompanyCode = company.Code;
            centerApproveBill.CompanyName = company.Name;
            this.CustomerCompanyPopoupTextBox.Text = company.Name;
            this.DomainContext.Load(this.DomainContext.GetWarehousesByPartsSalesCategoryIdAndOwnerCompanyIdQuery(221, centerApproveBill.CompanyId).Where(r => r.Type != (int)DcsWarehouseType.虚拟库), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                this.KvWarehouses.Clear();
                if(loadOp.Entities != null) {
                    foreach(var item in loadOp.Entities)
                        this.KvWarehouses.Add(item);
                    this.WarehouseComboBox.ItemsSource = KvWarehouses;
                }
            }, null);
            var parent = queryWindow.ParentOfType<RadWindow>();
            if (parent != null)
                parent.Close();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetCenterApproveBillsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if (entity != null) {
                    var key = kvWeeks.FirstOrDefault(r => entity.ApproveWeek.Contains(r.Value));
                    if (key != null) {
                        entity.ApproveWeek = key.Key.ToString();
                    }
                    this.SetObjectToEdit(entity);
                    if (key != null) {
                        this.WeekComboBox.SelectedItem = key;
                    }
                    this.DomainContext.Load(this.DomainContext.GetWarehousesByPartsSalesCategoryIdAndOwnerCompanyIdQuery(221, entity.CompanyId).Where(r => r.Type != (int)DcsWarehouseType.虚拟库), LoadBehavior.RefreshCurrent, loadOp1 => {
                        if(loadOp1.HasError) {
                            loadOp1.MarkErrorAsHandled();
                            return;
                        }
                        this.KvWarehouses.Clear();
                        if(loadOp1.Entities != null) {
                            foreach(var item in loadOp1.Entities)
                                this.KvWarehouses.Add(item);
                        }
                        this.WarehouseComboBox.ItemsSource = KvWarehouses;
                    }, null);
                }
            }, null);
        }
    }
}
