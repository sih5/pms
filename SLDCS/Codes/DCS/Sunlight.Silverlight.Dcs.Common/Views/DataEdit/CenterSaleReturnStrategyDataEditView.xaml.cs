﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Core.Model;
using System.Collections.ObjectModel;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Core;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class CenterSaleReturnStrategyDataEditView {
        private ObservableCollection<KeyValuePair> kvReturnType;
        private KeyValueManager keyValueManager;
        private readonly string[] kvNames = { "PartsSalesReturn_ReturnType" };

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        public CenterSaleReturnStrategyDataEditView() {

            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvNames);

             this.KeyValueManager.LoadData(() => {
                this.KvReturnType.Clear();
                foreach(var item in this.KeyValueManager[this.kvNames[0]]) {
                    this.KvReturnType.Add(new KeyValuePair {
                        Key = item.Key,
                        Value = item.Value
                    });
                }
                this.ReturnTypeComboBox.ItemsSource = KvReturnType;
            });
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override string BusinessName {
            get {
                return CommonUIStrings.DataEditView_BusinessName_CenterSaleReturnStrategy;
            }
        }

        protected override void OnEditSubmitting() {
            var centerSaleReturnStrategy = this.DataContext as CenterSaleReturnStrategy;
            if(centerSaleReturnStrategy == null)
                return;
            centerSaleReturnStrategy.ValidationErrors.Clear();
            if(centerSaleReturnStrategy.TimeFrom == null || centerSaleReturnStrategy.TimeFrom < 0)
                centerSaleReturnStrategy.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_CenterSaleReturnStrategy_TimeFrom, new[] {
                    "TimeFrom"
                }));
            if(centerSaleReturnStrategy.TimeTo == null || centerSaleReturnStrategy.TimeTo < 0)
                centerSaleReturnStrategy.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_CenterSaleReturnStrategy_TimeTo, new[] {
                    "TimeTo"
                }));
            if(centerSaleReturnStrategy.DiscountRate == null || centerSaleReturnStrategy.DiscountRate < 0)
                centerSaleReturnStrategy.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_CenterSaleReturnStrategy_DiscountRate, new[] {
                    "DiscountRate"
                }));
            if (centerSaleReturnStrategy.TimeFrom >= centerSaleReturnStrategy.TimeTo) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_CenterSaleReturnStrategy_Time);
                return;
            }
            if(centerSaleReturnStrategy.HasValidationErrors)
                return;
            centerSaleReturnStrategy.DiscountRate = Math.Round(centerSaleReturnStrategy.DiscountRate.Value, 2);
            ((IEditableObject)centerSaleReturnStrategy).EndEdit();
            try{
                if(EditState == DataEditState.New) {
                    if (centerSaleReturnStrategy.CanInsertCenterSaleReturnStrategy)
                    centerSaleReturnStrategy.InsertCenterSaleReturnStrategy();
                } else {
                    if (centerSaleReturnStrategy.CanUpdateCenterSaleReturnStrategy)
                    centerSaleReturnStrategy.UpdateCenterSaleReturnStrategy();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        private void CreateUI() {

        }
        public ObservableCollection<KeyValuePair> KvReturnType {
            get {
                return this.kvReturnType ?? (this.kvReturnType = new ObservableCollection<KeyValuePair>());
            }
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetCenterSaleReturnStrategiesQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }
    }
}
