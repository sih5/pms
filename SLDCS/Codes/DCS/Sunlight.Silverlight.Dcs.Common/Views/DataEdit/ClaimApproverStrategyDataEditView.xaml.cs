﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.View;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;
using Sunlight.Silverlight.Dcs.Common.Resources;


namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class ClaimApproverStrategyDataEditView{
        private ObservableCollection<KeyValuePair> kvPartsSalesCategorys;
        private ObservableCollection<KeyValuePair> kvAfterApproverStatus;
        private ObservableCollection<KeyValuePair> kvInitialApproverStatus;

        private KeyValueManager keyValueManager;
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        public ObservableCollection<KeyValuePair> KvPartsSalesCategorys
        {
            get
            {
                return this.kvPartsSalesCategorys ?? (this.kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvAfterApproverStatus{
            get {
                return this.kvAfterApproverStatus ?? (this.kvAfterApproverStatus = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvInitialApproverStatus {
            get {
                return this.kvInitialApproverStatus ?? (this.kvInitialApproverStatus = new ObservableCollection<KeyValuePair>());
            }
        }


        public ClaimApproverStrategyDataEditView()
        {
            InitializeComponent();
            this.KeyValueManager.LoadData();
            this.Initializer.Register(this.CreateUI);
        }

        private Company company;
        private void CreateUI()
        {
            
            this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(r => r.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOption =>
            {
                if (loadOption.HasError)
                {
                    if (!loadOption.IsErrorHandled)
                        loadOption.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOption);
                    return;
                }
                if (loadOption.Entities != null && loadOption.Entities.Any())
                {
                    company = loadOption.Entities.First();
                    if (company.Type == (int)DcsCompanyType.服务站)
                    {
                        this.DomainContext.Load(DomainContext.GetPartsSalesCategoriesByDealerServiceInfoDealerIdQuery(BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp =>
                        {
                            if (loadOp.HasError)
                            {
                                if (!loadOp.IsErrorHandled)
                                    loadOp.MarkErrorAsHandled();
                                DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                                return;
                            }
                            foreach (var partsSalesCategory in loadOp.Entities)
                                this.KvPartsSalesCategorys.Add(new KeyValuePair
                                {
                                    Key = partsSalesCategory.Id,
                                    Value = partsSalesCategory.Name,
                                    UserObject = partsSalesCategory
                                });
                            this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
                        }, null);
                    }
                    else
                    {
                        this.DomainContext.Load(DomainContext.GetPartsSalesCategoriesBySalesUnitOwnerCompanyIdQuery(BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOpSalesCategory =>
                        {
                            if (loadOpSalesCategory.HasError)
                            {
                                if (!loadOpSalesCategory.IsErrorHandled)
                                    loadOpSalesCategory.MarkErrorAsHandled();
                                DcsUtils.ShowDomainServiceOperationWindow(loadOpSalesCategory);
                                return;
                            }
                            foreach (var partsSalesCategory in loadOpSalesCategory.Entities)
                                this.KvPartsSalesCategorys.Add(new KeyValuePair
                                {
                                    Key = partsSalesCategory.Id,
                                    Value = partsSalesCategory.Name,
                                    UserObject = partsSalesCategory
                                });
                            this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
                        }, null);
                    }
                }
            }, null);
            this.KvAfterApproverStatus.Add(new KeyValuePair {
                Key = (int)DcsPartsClaimBillStatusNew.审核通过,
                Value = CommonUIStrings.DataEditView_KeyValue_Approve
            });
            this.KvAfterApproverStatus.Add(new KeyValuePair {
                Key = (int)DcsPartsClaimBillStatusNew.终审通过,
                Value = CommonUIStrings.DataEditView_KeyValue_FinalApprove
            });
            this.KvInitialApproverStatus.Add(new KeyValuePair {
                Key = (int)DcsPartsClaimBillStatusNew.初审通过,
                Value = CommonUIStrings.DataEditView_KeyValue_IntilApprove
            });
            this.KvInitialApproverStatus.Add(new KeyValuePair {
                Key = (int)DcsPartsClaimBillStatusNew.终审通过,
                Value = CommonUIStrings.DataEditView_KeyValue_FinalApprove
            });
        }



        protected override string BusinessName {
            get {
                return CommonUIStrings.DataEditView_BusinessName_ClaimApproverStrategy;
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.getClaimApproverStrategyByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            var claimApproverStrategy = this.DataContext as ClaimApproverStrategy;
            if (claimApproverStrategy == null)
                return;

            claimApproverStrategy.ValidationErrors.Clear();

            if(claimApproverStrategy.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_BranchSupplier_BranchIsNull);
                return;
            }
            if(claimApproverStrategy.FeeFrom == null || claimApproverStrategy.FeeTo == null) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_ClaimApproverStrategy_Fee);
                return;
            }
            if(claimApproverStrategy.FeeFrom >= claimApproverStrategy.FeeTo) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_ClaimApproverStrategy_FeeCompare);
                return;
            }
            if(claimApproverStrategy.AfterApproverStatus == null) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_ClaimApproverStrategy_AfterApproverStatus);
                return;
            }
            if(claimApproverStrategy.InitialApproverStatus == null) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_ClaimApproverStrategy_InitialApproverStatus);
                return;
            }

            ((IEditableObject)claimApproverStrategy).EndEdit();
            try{
                if(EditState == DataEditState.New) {
                    if (claimApproverStrategy.CanInsertClaimApproverStrategy)
                    claimApproverStrategy.InsertClaimApproverStrategy();
                } else {
                    if (claimApproverStrategy.CanUpdateClaimApproverStrategy)
                    claimApproverStrategy.UpdateClaimApproverStrategy();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        protected override bool OnRequestCanSubmit()
        {
            this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            return true;
        }


    }
}
