﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class CompanyAddressForBranchDataEditView : INotifyPropertyChanged {
        private readonly string[] kvNames = new[] {
            "Address_usage"
        };

        public readonly List<TiledRegion> TiledRegions = new List<TiledRegion>();
        private ObservableCollection<KeyValuePair> kvProvinceNames, kvCityNames, kvCountyNames;
        private KeyValueManager keyValueManager;

        public CompanyAddressForBranchDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string BusinessName {
            get {
                return CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrderForReport_ReceivingAddress;
            }
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private void CreateUI() {
            var queryWindow = DI.GetQueryWindow("CustomerInformationBySparepart");
            queryWindow.SelectionChanged += queryWindow_SelectionDecided;
            this.popupTextBoxOldPartCode.PopupContent = queryWindow;
            this.provinceNameRadCombox.SelectionChanged += provinceNameRadCombox_SelectionChanged;
            this.cityNameRadCombox.SelectionChanged += cityNameRadCombox_SelectionChanged;
            this.DomainContext.Load(DomainContext.GetTiledRegionsQuery().Where(ex => ex.CountyName != null), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var repairObject in loadOp.Entities)
                    this.TiledRegions.Add(new TiledRegion {
                        Id = repairObject.Id,
                        ProvinceName = repairObject.ProvinceName,
                        CityName = repairObject.CityName,
                        CountyName = repairObject.CountyName,
                    });

                foreach(var item in TiledRegions) {
                    var values = KvProvinceNames.Select(r => r.Value);
                    if(values.Contains(item.ProvinceName))
                        continue;
                    KvProvinceNames.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.ProvinceName
                    });
                }
            }, null);
        }

        private void queryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;

            if(queryWindow.SelectedEntities == null || !queryWindow.SelectedEntities.Any() || queryWindow.SelectedEntities.Cast<PartsClaimPrice>() == null)
                return;
            var companyAddress = this.DataContext as CompanyAddress;
            if(companyAddress == null)
                return;

            var customerInformation = queryWindow.SelectedEntities.Cast<CustomerInformation>().FirstOrDefault();
            if(customerInformation == null)
                return;

            //收获地址应该在选择收获单位之后填充
            this.DomainContext.Load(DomainContext.GetCompaniesQuery().Where(r => r.Id == customerInformation.CustomerCompanyId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var customerCompany = loadOp.Entities.FirstOrDefault();
                if(customerCompany == null)
                    return;
                companyAddress.CompanyId = customerCompany.Id;
                this.CompanyCode = customerCompany.Code;
                companyAddress.Company = customerCompany;
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }, null);

        }

        private string companyCode;
        public string CompanyCode {
            get {
                return this.companyCode;
            }
            set {
                this.companyCode = value;
                this.OnPropertyChanged("CompanyCode");
            }
        }

        public void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public object Usages {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public ObservableCollection<KeyValuePair> KvProvinceNames {
            get {
                return this.kvProvinceNames ?? (this.kvProvinceNames = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvCityNames {
            get {
                return this.kvCityNames ?? (this.kvCityNames = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvCountyNames {
            get {
                return this.kvCountyNames ?? (this.kvCountyNames = new ObservableCollection<KeyValuePair>());
            }
        }

        private void provinceNameRadCombox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var repairOrder = this.DataContext as CompanyAddress;
            if(repairOrder == null)
                return;
            this.KvCityNames.Clear();
            repairOrder.CityId = 0;
            foreach(var item in TiledRegions.Where(ex => ex.ProvinceName == repairOrder.ProvinceName)) {
                var values = KvCityNames.Select(ex => ex.Value);
                if(values.Contains(item.CityName))
                    continue;
                KvCityNames.Add(new KeyValuePair {
                    Key = item.Id,
                    Value = item.CityName
                });
            }
        }

        private void cityNameRadCombox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var repairOrder = this.DataContext as CompanyAddress;
            if(repairOrder == null)
                return;
            this.KvCountyNames.Clear();
            repairOrder.CountyId = 0;
            foreach(var item in TiledRegions.Where(ex => ex.CityName == repairOrder.CityName)) {
                var values = KvCountyNames.Select(ex => ex.Value);
                if(values.Contains(item.CountyName))
                    continue;
                KvCountyNames.Add(new KeyValuePair {
                    Key = item.Id,
                    Value = item.CountyName
                });
            }
        }

        protected override void OnEditSubmitting() {
            var companyAddress = this.DataContext as CompanyAddress;
            if(companyAddress == null)
                return;
            if(companyAddress.CompanyId == default(int)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_CompanyAddress_CompanyId);
                return;
            }
            if(companyAddress.Usage == default(int)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_CompanyAddress_Usage);
                return;
            }
            if(companyAddress.CountyId == default(int)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_CompanyAddress_CountyId);
                return;
            }
            companyAddress.RegionId = companyAddress.CountyId;
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(DomainContext.GetCompanyAddressByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.CompanyCode = entity.Company.Code;
                    this.SetObjectToEdit(entity);
                    SetTiledRegionId();
                }
            }, null);
        }

        private void SetTiledRegionId() {
            var companyAddress = this.DataContext as CompanyAddress;
            if(companyAddress == null)
                return;

            if(companyAddress.ProvinceName == null) {
                companyAddress.ProvinceName = companyAddress.TiledRegion.ProvinceName;
                if(companyAddress.ProvinceName == null)
                    return;
            }
            if(this.KvProvinceNames.Any()) {
                companyAddress.ProvinceId = this.KvProvinceNames.First(e => e.Value == companyAddress.ProvinceName).Key;
                if(companyAddress.CityName == null) {
                    companyAddress.CityName = companyAddress.TiledRegion.CityName;
                    if(companyAddress.CityName == null)
                        return;
                }
                companyAddress.CityId = this.KvCityNames.First(e => e.Value == companyAddress.CityName).Key;
                if(companyAddress.CountyName == null) {
                    companyAddress.CountyName = companyAddress.TiledRegion.CountyName;
                    if(companyAddress.CountyName == null)
                        return;
                }
                companyAddress.CountyId = this.KvCountyNames.First(e => e.Value == companyAddress.CountyName).Key;
            } else {
                this.DomainContext.Load(DomainContext.GetTiledRegionsQuery().Where(ex => ex.CountyName != null).OrderBy(ex => ex.ProvinceName), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError)
                        return;
                    foreach(var repairObject in loadOp.Entities)
                        this.TiledRegions.Add(new TiledRegion {
                            Id = repairObject.Id,
                            ProvinceName = repairObject.ProvinceName,
                            CityName = repairObject.CityName,
                            CountyName = repairObject.CountyName,
                        });
                    this.KvProvinceNames.Clear();
                    foreach(var item in TiledRegions) {
                        var values = this.KvProvinceNames.Select(e => e.Value);
                        if(values.Contains(item.ProvinceName))
                            continue;
                        this.KvProvinceNames.Add(new KeyValuePair {
                            Key = item.Id,
                            Value = item.ProvinceName
                        });
                    }
                    companyAddress.ProvinceId = this.KvProvinceNames.First(e => e.Value == companyAddress.ProvinceName).Key;
                    if(companyAddress.CityName == null)
                        return;
                    companyAddress.CityId = this.KvCityNames.First(e => e.Value == companyAddress.CityName).Key;
                    if(companyAddress.CountyName == null)
                        return;
                    companyAddress.CountyId = this.KvCountyNames.First(e => e.Value == companyAddress.CountyName).Key;
                }, null);
            }
        }
        protected override void Reset() {
            this.CompanyCode = "";
            var companyAddress = this.DataContext as CompanyAddress;
            if(companyAddress == null)
                return;
            if(this.DomainContext.CompanyAddresses.Contains(companyAddress)) {
                this.DomainContext.CompanyAddresses.Detach(companyAddress);
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
    }
}
