﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class CompanyInvoiceInfoDataEditView {
        public CompanyInvoiceInfoDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            var dataEditView = DI.GetDataEditPanel("EnterpriseInvoicingInfo");
            dataEditView.SetValue(Grid.RowProperty, 1);
            this.LayoutRoot.Children.Add(dataEditView);

            var queryWindowCompanyCode = DI.GetQueryWindow("Company");
            queryWindowCompanyCode.SelectionDecided += queryWindowCompanyCode_SelectionDecided;
            this.popupTextBoxCompanyCode.PopupContent = queryWindowCompanyCode;
        }

        private void queryWindowCompanyCode_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var company = queryWindow.SelectedEntities.Cast<Company>().FirstOrDefault();
            if(company == null)
                return;
            var companyInvoiceInfo = this.DataContext as CompanyInvoiceInfo;
            if(companyInvoiceInfo == null)
                return;
            companyInvoiceInfo.CompanyId = company.Id;
            companyInvoiceInfo.CompanyName = company.Name;
            companyInvoiceInfo.CompanyCode = company.Code;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetCompanyInvoiceInfoesQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        protected override void OnEditSubmitting() {
            var companyInvoiceInfo = this.DataContext as CompanyInvoiceInfo;
            if(companyInvoiceInfo == null)
                return;
            companyInvoiceInfo.ValidationErrors.Clear();
            if(string.IsNullOrWhiteSpace(companyInvoiceInfo.CompanyCode))
                companyInvoiceInfo.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_CompanyInvoiceInfo_CompanyCodeIsNull, new[] {
                    "CompanyCode"
                }));
            if(string.IsNullOrWhiteSpace(companyInvoiceInfo.TaxRegisteredNumber))
                companyInvoiceInfo.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_CompanyInvoiceInfo_TaxRegisteredNumberIsNull, new[] {
                    "TaxRegisteredNumber"
                }));
            if(string.IsNullOrWhiteSpace(companyInvoiceInfo.InvoiceTitle))
                companyInvoiceInfo.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_CompanyInvoiceInfo_InvoiceTitleIsNull, new[] {
                    "InvoiceTitle"
                }));
            if(companyInvoiceInfo.HasValidationErrors)
                return;
            ((IEditableObject)companyInvoiceInfo).EndEdit();
            if(this.EditState == DataEditState.New) {
                if(companyInvoiceInfo.Can生成企业开票信息)
                    companyInvoiceInfo.生成企业开票信息();
            } else {
                if(companyInvoiceInfo.Can修改企业开票信息)
                    companyInvoiceInfo.修改企业开票信息();
            }
            base.OnEditSubmitting();
        }

    }
}
