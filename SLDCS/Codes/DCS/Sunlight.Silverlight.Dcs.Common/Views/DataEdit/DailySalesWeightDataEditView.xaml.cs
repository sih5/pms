﻿using System.Windows;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class DailySalesWeightDataEditView {
        private ICommand exportFileCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出日均销量计算参数配置模板.xlsx";

        //private List<DailySalesWeight> dailySalesWeightExtend;
        //public List<DailySalesWeight> DailySalesWeightExtends {
        //    get {
        //        return this.dailySalesWeightExtend ?? (this.dailySalesWeightExtend = new List<DailySalesWeight>());
        //    }
        //}
        //private DataGridViewBase partsReplacementForImportDataGridView;

        //private DataGridViewBase PartsReplacementForImportDataGridView {
        //    get {
        //        return this.partsReplacementForImportDataGridView ?? (this.partsReplacementForImportDataGridView = DI.GetDataGridView("DailySalesWeightImport"));
        //    }
        //}
        public DailySalesWeightDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return "导入日均销量计算参数配置";
            }
        }

        private void CreateUI() {
           // this.ShowSaveButton();
            //var tabControl = new RadTabControl();
            //tabControl.BackgroundVisibility = Visibility.Collapsed;
            //tabControl.Items.Add(new RadTabItem {
            //    Header = "日均销量计算参数配置清单",
            //    Content = this.PartsReplacementForImportDataGridView
            //});
            //tabControl.SetValue(Grid.RowProperty, 1);
            //this.Root.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.ImportDailySalesWeightAsync(fileName);
            this.ExcelServiceClient.ImportDailySalesWeightCompleted -= ExcelServiceClient_ImportDailySalesWeightCompleted;
            this.ExcelServiceClient.ImportDailySalesWeightCompleted += ExcelServiceClient_ImportDailySalesWeightCompleted;
        }

        void ExcelServiceClient_ImportDailySalesWeightCompleted(object sender, ImportDailySalesWeightCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!HasImportingError) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImport, 10);
            } else {
                if(!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if(!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataDeTailPanel_Text_Company_WarehouseCode,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataDeTailPanel_Text_Company_WarehouseName,
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricingDetail_Time,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "权重",
                                                IsRequired = true
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }
 
        private void ExecuteSerivcesMethod(string notifyMessage) {
            DomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    DomainContext.RejectChanges();
                    return;
                }
                this.NotifyEditSubmitted();
                this.OnCustomEditSubmitted();
                UIHelper.ShowNotification(notifyMessage);
            }, null);
        }
        public void OnCustomEditSubmitted() {
            this.DataContext = null;
        }
        public new event EventHandler EditSubmitted;
        private void NotifyEditSubmitted() {
            var handler = this.EditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }
    }
}