﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class DealerFormatDataEditView {


        public DealerFormatDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string BusinessName {
            get {
                return CommonUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrderForReport_ReceivingAddress;
            }
        }

        private void CreateUI() {
            var queryWindow = DI.GetQueryWindow("CompanyForApproveBill");
            queryWindow.SelectionDecided += this.queryWindow_SelectionDecided;
            this.popupTextBoxOldPartCode.PopupContent = queryWindow;
        }

        private void queryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;

            if(queryWindow.SelectedEntities == null || !queryWindow.SelectedEntities.Any() )
                return;
            var dealerFormat = this.DataContext as DealerFormat;
            if(dealerFormat == null)
                return;

            var company = queryWindow.SelectedEntities.Cast<Company>().FirstOrDefault();
            if(company == null)
                return;
            dealerFormat.DealerId = company.Id;
            dealerFormat.DealerCode = company.Code;
            dealerFormat.DealerName = company.Name;
            this.popupTextBoxOldPartCode.Text = company.Code;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

  
        protected override void OnEditSubmitting() {
            var dealerFormat = this.DataContext as DealerFormat;
            if(dealerFormat == null)
                return;
            if(dealerFormat.DealerId== default(int)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_CompanyAddress_CompanyId);
                return;
            }
            if(string.IsNullOrEmpty( dealerFormat.Format)) {
                UIHelper.ShowNotification("服务商业态不允许为空");
                return;
            }
            if(string.IsNullOrEmpty( dealerFormat.Quarter)) {
                UIHelper.ShowNotification("季度不允许为空");
                return;
            }
            if(dealerFormat.Can新增服务商业态) {
                dealerFormat.新增服务商业态();
                ExecuteSerivcesMethod("新增成功"); 
            }
                   }
        private void ExecuteSerivcesMethod(string notifyMessage) {
            DomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    DomainContext.RejectChanges();
                    return;
                }
                this.NotifyEditSubmitted();
                UIHelper.ShowNotification(notifyMessage);
            }, null);
        }
        public void OnCustomEditSubmitted() {
            this.DataContext = null;
        }

        public new event EventHandler EditSubmitted;
        private void NotifyEditSubmitted() {
            var handler = this.EditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        } 
        protected override void Reset() {
            var dealerFormat = this.DataContext as DealerFormat;
            if(dealerFormat == null)
                return;
        }
    }
}
