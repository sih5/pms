﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using System.ComponentModel.DataAnnotations;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class DealerFormatForImportDataEditView {
        private ICommand exportFileCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出服务商业态模板.xlsx";
        private DataGridViewBase dealerBusinessPermitImport;
        public DealerFormatForImportDataEditView()
        {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title
        {
            get
            {
                return "导入服务商业态";
            }
        }
        private DataGridViewBase DealerBusinessPermitImport {
            get {
                return this.dealerBusinessPermitImport ?? (this.dealerBusinessPermitImport = DI.GetDataGridView("DealerBusinessPermitImport"));
            }
        }
        private void CreateUI() {
         
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }
        private void UploadFileProcessing(string fileName)
        {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.ImportDealerFormatAsync(fileName);
            this.ExcelServiceClient.ImportDealerFormatCompleted -= ExcelServiceClient_ImportDealerFormatCompleted;
            this.ExcelServiceClient.ImportDealerFormatCompleted += ExcelServiceClient_ImportDealerFormatCompleted; ;
        }

        private void ExcelServiceClient_ImportDealerFormatCompleted(object sender, ImportDealerFormatCompletedEventArgs e)
        {

            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if (!HasImportingError)
            {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImport, 10);
            }
            else
            {
                if (!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if (!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }
        private void InitializeCommand()
        {
            try
            {
                this.exportFileCommand = new Core.Command.DelegateCommand(() =>
                {
                    var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = "服务商编号",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "服务商名称",
                                            },
                                            new ImportTemplateColumn {
                                                Name ="服务商业态",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "季度",
                                                IsRequired = true
                                            }
                                        };
                    this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp =>
                    {
                        if (loadOp.HasError)
                        {
                            if (!loadOp.IsErrorHandled)
                                loadOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                        }
                        if (!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                            this.ExportFile(loadOp.Value);
                    }, null);
                });
            }
            catch (ValidationException ex)
            {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
        }

        protected override bool IsAutomaticUploadFile
        {
            get
            {
                return true;
            }
        }

        public ICommand ExportTemplateCommand
        {
            get
            {
                if (exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

    }
}