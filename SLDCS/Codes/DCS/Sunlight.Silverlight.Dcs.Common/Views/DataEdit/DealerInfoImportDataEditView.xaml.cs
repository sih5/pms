﻿
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class DealerInfoImportDataEditView {

        private ICommand exportFileCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出经销商基本信息模板.xlsx";
        private DataGridViewBase dealerServiceInfoImport;

        private DataGridViewBase DealerServiceInfoImport {
            get {
                return this.dealerServiceInfoImport ?? (this.dealerServiceInfoImport = DI.GetDataGridView("DealerInfoImport"));
            }
        }
        public DealerInfoImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return CommonUIStrings.QueryPanel_Title_Distributor;
            }
        }

        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = CommonUIStrings.QueryPanel_Title_DistributorList,
                Content = this.DealerServiceInfoImport
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.批量导入修改经销商基本信息Async(BaseApp.Current.CurrentUserData.EnterpriseId, fileName);
            this.ExcelServiceClient.批量导入修改经销商基本信息Completed -= ExcelServiceClient_批量导入修改经销商基本信息Completed;
            this.ExcelServiceClient.批量导入修改经销商基本信息Completed += ExcelServiceClient_批量导入修改经销商基本信息Completed;
        }

        private void ExcelServiceClient_批量导入修改经销商基本信息Completed(object sender, 批量导入修改经销商基本信息CompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!this.HasImportingError) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImport, 5);
            } else {
                if(!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if(!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }


        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditView_Text_CompanyAddress_DealerCode,
                                                IsRequired = true
                                            },new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditView_Text_CompanyAddress_DealerName
                                            },new ImportTemplateColumn{
                                                Name =  CommonUIStrings.DataEditView_Text_CompanyAddress_Abbreviation
                                            },new ImportTemplateColumn{
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_ServiceManager
                                            },new ImportTemplateColumn{
                                                Name = CommonUIStrings.DataEditPanel_Text_Company_CustomerCode
                                            },new ImportTemplateColumn{
                                                Name = CommonUIStrings.QueryPanel_Title_PartsSupplier_SupplierCode
                                            },new ImportTemplateColumn{
                                                Name = CommonUIStrings.DataEditView_Text_CompanyAddress_Establishment
                                            },new ImportTemplateColumn{
                                                Name = CommonUIStrings.DataGrid_QueryItem_Title_ProvinceName
                                            },new ImportTemplateColumn{
                                                Name = CommonUIStrings.DetailPanel_Text_Region_City
                                            },new ImportTemplateColumn{
                                                Name = CommonUIStrings.DataEditPanel_Text_Region_District
                                            },new ImportTemplateColumn{
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_Warehouse_CityLevel
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditView_Text_CompanyAddress_LinkMan
                                            },new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditPanel_Text_Company_ContactPhone
                                            },
                                            new ImportTemplateColumn{
                                                Name = "E-MAIL"
                                            },new ImportTemplateColumn {
                                                Name =  CommonUIStrings.DataEditView_Text_CompanyAddress_LinkManPhone
                                            },
                                            new ImportTemplateColumn{
                                                Name =CommonUIStrings.DataEditView_Text_CompanyAddress_Fax
                                            }, new ImportTemplateColumn{
                                                Name =CommonUIStrings.DataGridView_ColumnItem_Title_Company_ContactPostCode
                                            },new ImportTemplateColumn{
                                                Name =CommonUIStrings.DataEditView_Text_CompanyAddress_Address
                                            },new  ImportTemplateColumn{
                                                Name =CommonUIStrings.DataEditPanel_Text_Company_Address_LatitudeNew
                                            },new  ImportTemplateColumn{
                                                Name =CommonUIStrings.DataEditPanel_Text_Company_Address_LongitudeNew
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_Dealer_Certificate
                                            },
                                            new  ImportTemplateColumn{
                                                Name =CommonUIStrings.QueryPanel_Title_Dealer_CertificateName
                                            },new  ImportTemplateColumn{
                                                Name =CommonUIStrings.QueryPanel_Title_Dealer_CertificateDate
                                            },new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_Dealer_CertificateFee
                                            },new  ImportTemplateColumn{
                                                Name =CommonUIStrings.QueryPanel_Title_Dealer_FixedAssets
                                            },                                            
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_Dealer_Representative
                                            },
                                            new  ImportTemplateColumn{
                                                Name =CommonUIStrings.QueryPanel_Title_Dealer_RepresentativePhone
                                            },new  ImportTemplateColumn{
                                                Name =CommonUIStrings.QueryPanel_Title_Dealer_Nature
                                            },new  ImportTemplateColumn{
                                                Name =CommonUIStrings.QueryPanel_Title_Dealer_Scope
                                            },new  ImportTemplateColumn{
                                                Name =CommonUIStrings.QueryPanel_Title_Dealer_CertificateAddress
                                            },new  ImportTemplateColumn{
                                                Name =CommonUIStrings.QueryPanel_Title_Dealer_IDCardType
                                            },new  ImportTemplateColumn{
                                                Name =CommonUIStrings.QueryPanel_Title_Dealer_IDCardNo
                                            },                                            
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_Dealer_Qualification
                                            },
                                            new  ImportTemplateColumn{
                                                Name =CommonUIStrings.QueryPanel_Title_Dealer_Unit
                                            },new  ImportTemplateColumn{
                                                Name =CommonUIStrings.QueryPanel_Title_Dealer_CreateTime
                                            },new  ImportTemplateColumn{
                                                Name =CommonUIStrings.QueryPanel_Title_Dealer_Traffic
                                            },new  ImportTemplateColumn{
                                                Name =CommonUIStrings.QueryPanel_Title_Dealer_MainScope
                                            },new  ImportTemplateColumn{
                                                Name =CommonUIStrings.QueryPanel_Title_Dealer_OperationScope
                                            },new  ImportTemplateColumn{
                                                Name =CommonUIStrings.QueryPanel_Title_Dealer_Dangerous
                                            },new  ImportTemplateColumn{
                                                Name =CommonUIStrings.QueryPanel_Title_Dealer_Secondary
                                            },new  ImportTemplateColumn{
                                                Name =CommonUIStrings.QueryPanel_Title_Dealer_Route
                                            },new  ImportTemplateColumn{
                                                Name =CommonUIStrings.QueryPanel_Title_Dealer_CarUse
                                            },new  ImportTemplateColumn{
                                                Name =CommonUIStrings.QueryPanel_Title_Dealer_CarStop
                                            },new  ImportTemplateColumn{
                                                Name =CommonUIStrings.QueryPanel_Title_Dealer_NumberService
                                            },new  ImportTemplateColumn{
                                                Name =CommonUIStrings.QueryPanel_Title_Dealer_Maintenance
                                            },new  ImportTemplateColumn{
                                                Name =CommonUIStrings.QueryPanel_Title_Dealer_Reception
                                            },new  ImportTemplateColumn{
                                                Name =CommonUIStrings.QueryPanel_Title_Dealer_Parking
                                            },new ImportTemplateColumn{
                                                Name = CommonUIStrings.QueryPanel_Title_Dealer_PartsLibrary
                                            },                                            
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_Dealer_Registration
                                            },
                                            new ImportTemplateColumn{
                                                Name = CommonUIStrings.QueryPanel_Title_Dealer_Taxpayers
                                            },new ImportTemplateColumn{
                                                Name = CommonUIStrings.QueryPanel_Title_Dealer_InvoiceLimit
                                            },new ImportTemplateColumn{
                                                Name = CommonUIStrings.QueryPanel_Title_Dealer_InvoiceName
                                            },new ImportTemplateColumn{
                                                Name = CommonUIStrings.QueryPanel_Title_Dealer_InvoiceType
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.CompanyInvoiceInfo_BankName
                                            },
                                            new ImportTemplateColumn{
                                                Name = CommonUIStrings.QueryPanel_Title_Dealer_InvoiceRate
                                            },                                            
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditPanel_Text_BankAccount
                                            },
                                            new ImportTemplateColumn{
                                                Name =CommonUIStrings.DataGridView_ColumnItem_Title_CompanyInvoiceInfo_Linkman
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataGridView_ColumnItem_Title_CompanyInvoiceInfo_ContactNumber
                                            },
                                            new ImportTemplateColumn{
                                                Name= CommonUIStrings.DataGridView_ColumnItem_Title_CompanyInvoiceInfo_Fax
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_Dealer_RegistrationAddress
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }
    }
}
