﻿
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    /// <summary>
    /// 导入修改按钮
    /// </summary>
    public partial class DealerServiceInfoImportDataEditView {

        private ICommand exportFileCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出经销商分品牌信息模板.xlsx";
        private DataGridViewBase dealerServiceInfoImport;

        private DataGridViewBase DealerServiceInfoImport {
            get {
                return this.dealerServiceInfoImport ?? (this.dealerServiceInfoImport = DI.GetDataGridView("DealerServiceInfoImport"));
            }
        }
        public DealerServiceInfoImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return CommonUIStrings.QueryPanel_Title_DealerProduct;
            }
        }

        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = CommonUIStrings.QueryPanel_Title_DealerProductList,
                Content = this.DealerServiceInfoImport
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.批量导入修改经销商分公司管理信息Async(fileName);
            this.ExcelServiceClient.批量导入修改经销商分公司管理信息Completed -= ExcelServiceClient_批量导入修改经销商分公司管理信息Completed;
            this.ExcelServiceClient.批量导入修改经销商分公司管理信息Completed += ExcelServiceClient_批量导入修改经销商分公司管理信息Completed;
        }

        private void ExcelServiceClient_批量导入修改经销商分公司管理信息Completed(object sender, 批量导入修改经销商分公司管理信息CompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!this.HasImportingError) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImport, 5);
            } else {
                if(!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if(!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }


        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = Title = CommonUIStrings.QueryPanel_QueryItem_Title_Warehouse_ChannelCapabilityId,
                                            }
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_EngineProductLine_PartsSalesCategory,
                                                IsRequired = true
                                            }
                                            ,
                                            new ImportTemplateColumn {
                                                Name =CommonUIStrings.DataEditView_Text_CompanyAddress_DealerCode,
                                                IsRequired = true
                                            }
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditView_Text_CompanyAddress_DealerName
                                            }
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_BranchSupplierRelation_BussinessCode
                                            }
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_BranchSupplierRelation_BussinessName
                                            }
                                            ,new ImportTemplateColumn {
                                                Name =  CommonUIStrings.DataEditView_Text_CompanyAddress_Abbreviation
                                            }
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_DealerService_Marketing
                                            }
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_DealerProduct_Market
                                            }
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_DealerService_DealerType
                                            }
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_DealerService_Classification
                                            }
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditPanel_Text_Region_Region
                                            }
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_DealerService_Categories
                                            }
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_DealerService_Authority
                                            }
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_DealerService_Authorized
                                            }
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_DealerService_IsDuty
                                            }
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_DealerService_24Hotline
                                            }
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_DealerService_24Phone
                                            } 
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditPanel_Text_Company_ContactPhone
                                            }
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditView_Text_CompanyAddress_Fax
                                            }
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_DealerService_Warehouse
                                            } 
                                            //,
                                            //new ImportTemplateColumn {
                                            //    Name = "维修资质等级"
                                            //}
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_DealerService_Relationship
                                            }
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_DealerService_ReserveAmount
                                            }
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_DealerService_StationMaintenance,
                                                IsRequired = true
                                            }
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_DealerService_ManagementRate
                                            }
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_DealerService_OutboundService
                                            }
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_Warehouse_Grade
                                            }
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_DealerService_SettlementStar
                                            }
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_DealerService_ServiceRadius
                                            }
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_DealerService_BackboneNetworks
                                            } 
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_DealerService_CentralStation
                                            }
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_DealerService_OldWarehouse
                                            }
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_DealerService_MaterialFee
                                            }
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_DealerService_InvoiceWorking
                                            }
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_DealerService_FactorHour
                                            }
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_DealerService_FactorMaterial
                                            }
                                            ,new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_DealerProduct_OutStatus
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }
    }
}
