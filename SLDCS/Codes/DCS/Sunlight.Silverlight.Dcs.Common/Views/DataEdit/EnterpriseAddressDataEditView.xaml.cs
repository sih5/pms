﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;


namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class EnterpriseAddressDataEditView{

        private KeyValueManager keyValueManager;
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private readonly string[] kvNames = new[] {
            "Address_usage"
        };

        private ObservableCollection<KeyValuePair> kvProvinceNames;
        private ObservableCollection<KeyValuePair> kvCityNames;
        private ObservableCollection<KeyValuePair> kvCountyNames;
        public ObservableCollection<KeyValuePair> KvProvinceNames {
            get {
                return this.kvProvinceNames ?? (this.kvProvinceNames = new ObservableCollection<KeyValuePair>());
            }
        }
        public ObservableCollection<KeyValuePair> KvCityNames {
            get {
                return this.kvCityNames ?? (this.kvCityNames = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvCountyNames {
            get {
                return this.kvCountyNames ?? (this.kvCountyNames = new ObservableCollection<KeyValuePair>());
            }
        }

        private List<TiledRegion> TiledRegions = new List<TiledRegion>();

        public object Usages {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public EnterpriseAddressDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(kvNames);
            this.KeyValueManager.LoadData();
            this.Initializer.Register(this.CreateUI);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetTiledRegionsQuery().Where(ex => ex.CountyName != null).OrderBy(ex => ex.ProvinceName), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var repairObject in loadOp.Entities)
                    this.TiledRegions.Add(new TiledRegion {
                        Id = repairObject.Id,
                        ProvinceName = repairObject.ProvinceName,
                        CityName = repairObject.CityName,
                        CountyName = repairObject.CountyName,
                    });

                foreach(var item in TiledRegions) {
                    var values = KvProvinceNames.Select(e => e.Value);
                    if(values.Contains(item.ProvinceName))
                        continue;
                    KvProvinceNames.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.ProvinceName
                    });
                }
            }, null);
        }

        private void CreateUI() {
            this.provinceNameRadCombox.SelectionChanged += provinceNameRadCombox_SelectionChanged;
            this.cityNameRadCombox.SelectionChanged += cityNameRadCombox_SelectionChanged;
        }

        private void provinceNameRadCombox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var company = new Company();
            this.KvCityNames.Clear();
            foreach(var item in TiledRegions.Where(ex => ex.ProvinceName == provinceNameRadCombox.Text).OrderBy(ex => ex.CityName)) {
                var values = KvCityNames.Select(ex => ex.Value);
                if(values.Contains(item.CityName))
                    continue;
                KvCityNames.Add(new KeyValuePair {
                    Key = item.Id,
                    Value = item.CityName
                });
                company.RegionId = item.Id;
            }
        }

        private void cityNameRadCombox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            this.KvCountyNames.Clear();
            foreach(var item in TiledRegions.Where(ex => ex.CityName == cityNameRadCombox.Text).OrderBy(ex => ex.CountyName)) {
                var values = KvCountyNames.Select(ex => ex.Value);
                if(values.Contains(item.CountyName))
                    continue;
                KvCountyNames.Add(new KeyValuePair {
                    Key = item.Id,
                    Value = item.CountyName
                });
            }
        }

        protected override string BusinessName {
            get {
                return CommonUIStrings.BusinessName_CompanyAddress;//DcsUIStrings.BusinessName_EnterpriseAddress;
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetCompanyAddressWithDetailQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                    SetTiledRegionId();
                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            var companyAddress = this.DataContext as CompanyAddress;
            if(companyAddress == null)
                return;
            ((IEditableObject)companyAddress).EndEdit();
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void SetTiledRegionId() {
            var companyAddress = this.DataContext as CompanyAddress;
            if(companyAddress == null)
                return;
            if(companyAddress.ProvinceName == null)
                return;
            if(this.KvProvinceNames.Any()) {
                companyAddress.ProvinceId = this.KvProvinceNames.First(e => e.Value == companyAddress.ProvinceName).Key;
                if(companyAddress.CityName == null)
                    return;
                companyAddress.CityId = this.KvCityNames.First(e => e.Value == companyAddress.CityName).Key;
                if(companyAddress.CountyName == null)
                    return;
                companyAddress.CountyId = this.KvCountyNames.First(e => e.Value == companyAddress.CountyName).Key;
            } else {
                this.DomainContext.Load(DomainContext.GetTiledRegionsQuery().Where(ex => ex.CountyName != null).OrderBy(ex => ex.ProvinceName), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError)
                        return;
                    foreach(var repairObject in loadOp.Entities)
                        this.TiledRegions.Add(new TiledRegion {
                            Id = repairObject.Id,
                            ProvinceName = repairObject.ProvinceName,
                            CityName = repairObject.CityName,
                            CountyName = repairObject.CountyName,
                        });
                    this.KvProvinceNames.Clear();
                    foreach(var item in TiledRegions) {
                        var values = this.KvProvinceNames.Select(e => e.Value);
                        if(values.Contains(item.ProvinceName))
                            continue;
                        this.KvProvinceNames.Add(new KeyValuePair {
                            Key = item.Id,
                            Value = item.ProvinceName
                        });
                    }
                    companyAddress.ProvinceId = this.KvProvinceNames.First(e => e.Value == companyAddress.ProvinceName).Key;
                    if(companyAddress.CityName == null)
                        return;
                    companyAddress.CityId = this.KvCityNames.First(e => e.Value == companyAddress.CityName).Key;
                    if(companyAddress.CountyName == null)
                        return;
                    companyAddress.CountyId = this.KvCountyNames.First(e => e.Value == companyAddress.CountyName).Key;
                }, null);
            }
        }

    }
}
