﻿using System;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class ExportCustomerInfoDataEditView {
        private KeyValueManager keyValueManager;
        private readonly string[] kvNames = {
            "ExportCustPriceType"
        };

        protected KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        public object KvPriceTypes {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public ExportCustomerInfoDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetExportCustomerInfoesQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void CreateUI() {
            var invoiceQueryWindow = DI.GetQueryWindow("Company");
            invoiceQueryWindow.Loaded += this.invoiceQueryWindow_Loaded;
            invoiceQueryWindow.SelectionDecided += this.invoiceQueryWindow_SelectionDecided;
            this.invoicePopoupTextBox.PopupContent = invoiceQueryWindow;
        }

        private void invoiceQueryWindow_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null)
                return;
            queryWindow.ExchangeData(null, "SetAdditionalFilterItemNotQuery", new object[] {
                new FilterItem("Type", typeof(int), FilterOperator.IsNotEqualTo, (int)DcsCompanyType.出口客户)
            });
        }

        private void invoiceQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var company = queryWindow.SelectedEntities.Cast<Company>().FirstOrDefault();
            if(company == null)
                return;
            var exportCustomerInfo = this.DataContext as ExportCustomerInfo;
            if(exportCustomerInfo == null)
                return;
            exportCustomerInfo.InvoiceCompanyId = company.Id;
            exportCustomerInfo.InvoiceCompanyName = company.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        protected override void OnEditSubmitting() {
            var exportCustomerInfo = this.DataContext as ExportCustomerInfo;
            if(exportCustomerInfo == null)
                return;
            exportCustomerInfo.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(exportCustomerInfo.CustomerCode)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditPanel_Validation_ExportCustomerInfo_CustomerNo);
                return;
            }
            if(string.IsNullOrEmpty(exportCustomerInfo.CustomerName)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditPanel_Validation_ExportCustomerInfo_CustomerName);
                return;
            }
            if(exportCustomerInfo.InvoiceCompanyId == default(int)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditPanel_Validation_ExportCustomerInfo_IssuingOffice);
                return;
            }
            if(exportCustomerInfo.PriceType == default(int) || this.cbPriceType.SelectedValue == null) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditPanel_Validation_ExportCustomerInfo_Price);
                return;
            }
            if(exportCustomerInfo.EntityState == EntityState.New) {
                exportCustomerInfo.Company.Code = exportCustomerInfo.CustomerCode;
                exportCustomerInfo.Company.Name = exportCustomerInfo.CustomerName;
            }

            if(exportCustomerInfo.HasValidationErrors)
                return;
            ((IEditableObject)exportCustomerInfo).EndEdit();
            base.OnEditSubmitting();
        }

        protected override void Reset() {
            var exportCustomerInfo = this.DataContext as ExportCustomerInfo;
            if(exportCustomerInfo != null && (exportCustomerInfo.Company != null && this.DomainContext.Companies.Contains(exportCustomerInfo.Company))) {
                this.DomainContext.Companies.Detach(exportCustomerInfo.Company);
            }
            if(this.DomainContext.ExportCustomerInfos.Contains(exportCustomerInfo))
                this.DomainContext.ExportCustomerInfos.Detach(exportCustomerInfo);
        }


        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override string BusinessName {
            get {
                return CommonUIStrings.BusinessName_ExportCustomerInfo;
            }
        }
    }
}