﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class ForceReserveBillCenterForDetailDataEditView  {
        private DataGridViewBase forceReserveBillDetailForEditDataGridView;
        public ForceReserveBillCenterForDetailDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetForceReserveBillsWithDetailByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    FileUploadDataEditPanels.FilePath = entity.Path;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
        private void CreateUI() {
          
            var detailEditView = new DcsDetailDataEditView();
            detailEditView.Register("储备清单", null, () => this.ForceReserveBillDetailForEditDataGridView);           
            this.SubRoot.Children.Add(detailEditView);
            detailEditView.UnregisterButton(detailEditView.InsertButton);
            detailEditView.UnregisterButton(detailEditView.DeleteButton); 
            FileUploadDataEditPanels.SetValue(Grid.ColumnProperty, 8);
            FileUploadDataEditPanels.SetValue(Grid.RowSpanProperty, 4);
            FileUploadDataEditPanels.Margin = new Thickness(20, 0, 0, 0);
            FileUploadDataEditPanels.isHiddenButtons = true;
            this.LayoutRoot.Children.Add(FileUploadDataEditPanels);

        }
        private ForceReserveBillForUploadDataEditPanel fileUploadDataEditPanels;
        public ForceReserveBillForUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (ForceReserveBillForUploadDataEditPanel)DI.GetDataEditPanel("ForceReserveBillForUpload"));
            }
        }     
       
        private DataGridViewBase ForceReserveBillDetailForEditDataGridView {
            get {
                if(this.forceReserveBillDetailForEditDataGridView == null) {
                    this.forceReserveBillDetailForEditDataGridView = DI.GetDataGridView("ForceReserveBillDetailForApprove");
                    this.forceReserveBillDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.forceReserveBillDetailForEditDataGridView;
            }
        }
        protected override void OnEditCancelled() {
            base.OnEditCancelled();
        }

     
        protected override string Title {
            get {
                return "强制储备单详情";
            }
        }

    }
}
