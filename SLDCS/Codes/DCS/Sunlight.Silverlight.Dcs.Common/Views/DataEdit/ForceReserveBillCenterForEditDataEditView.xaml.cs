﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;
using System.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Panels.DataEdit;
using System.ComponentModel.DataAnnotations;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class ForceReserveBillCenterForEditDataEditView  {
        private DataGridViewBase forceReserveBillDetailForEditDataGridView;
        public event EventHandler CustomEditSubmitted;
        public ForceReserveBillCenterForEditDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetForceReserveBillsWithDetailByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    FileUploadDataEditPanels.FilePath = entity.Path;
                    this.SetObjectToEdit(entity);

                }
            }, null);
        }
        private void CreateUI() {          
            var detailEditView = new DcsDetailDataEditView();
            detailEditView.Register("储备清单", null, () => this.ForceReserveBillDetailForEditDataGridView);          
            this.SubRoot.Children.Add(detailEditView);
            FileUploadDataEditPanels.SetValue(Grid.ColumnProperty, 8);
            FileUploadDataEditPanels.SetValue(Grid.RowSpanProperty, 4);
            FileUploadDataEditPanels.Margin = new Thickness(20, 0, 0, 0);
            this.LayoutRoot.Children.Add(FileUploadDataEditPanels);
        }
        private ForceReserveBillForUploadDataEditPanel fileUploadDataEditPanels;
        public ForceReserveBillForUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (ForceReserveBillForUploadDataEditPanel)DI.GetDataEditPanel("ForceReserveBillForUpload"));
            }
        }
             
        private DataGridViewBase ForceReserveBillDetailForEditDataGridView {
            get {
                if(this.forceReserveBillDetailForEditDataGridView == null) {
                    this.forceReserveBillDetailForEditDataGridView = DI.GetDataGridView("ForceReserveBillForEditDetail");
                    this.forceReserveBillDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.forceReserveBillDetailForEditDataGridView;
            }
        }
        protected override void OnEditCancelled() {
            base.OnEditCancelled();
        }

        protected override void OnEditSubmitted() {
            base.OnEditSubmitted();
        }

        protected override void OnEditSubmitting() {
            var forceReserveBill = this.DataContext as ForceReserveBill;
            if(forceReserveBill == null || !this.ForceReserveBillDetailForEditDataGridView.CommitEdit())
                return;
            forceReserveBill.ValidationErrors.Clear();
            foreach(var item in forceReserveBill.ForceReserveBillDetails) {
                item.ValidationErrors.Clear();
            }
            if(!forceReserveBill.ValidateFrom.HasValue) {
                UIHelper.ShowNotification("请选择生效时间");
                return;
            }
            if(forceReserveBill.ReserveType == "L" && forceReserveBill.ForceReserveBillDetails.Where(r => r.SuggestForceReserveQty < r.ForceReserveQty && r.SuggestForceReserveQty != null).Count() > 0) {
                UIHelper.ShowNotification("储备数量不能大于建议强制储备数");
                return;
            }
            forceReserveBill.Path = FileUploadDataEditPanels.FilePath;
            ((IEditableObject)forceReserveBill).EndEdit();
            if(forceReserveBill.Can更新强制储备单) {
                forceReserveBill.更新强制储备单();
            }
            base.OnEditSubmitting();
        }
        protected override string BusinessName {
            get {
                return "修改强制储备";
            }
        }
        private void NotifyEditSubmitted() {
            var handler = this.CustomEditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }
        public void OnCustomEditSubmitted() {
        }
        protected override bool OnRequestCanSubmit() {
            return true;
        }
    }
}
