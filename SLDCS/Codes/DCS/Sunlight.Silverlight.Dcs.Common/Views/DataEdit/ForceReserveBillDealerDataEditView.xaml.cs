﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;
using System.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Panels.DataEdit;
using System.ComponentModel.DataAnnotations;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class ForceReserveBillDealerDataEditView {
        private readonly ObservableCollection<KeyValuePair> kvReserveType = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvReserveTypeSubItem = new ObservableCollection<KeyValuePair>();
        private DataGridViewBase forceReserveBillDetailForEditDataGridView;
        private ObservableCollection<ForceReserveBillDetailTemp> forceReserveBillDetailTempDetails;
        private string reserveType;
        public event EventHandler CustomEditSubmitted;

        public ObservableCollection<ForceReserveBillDetailTemp> ForceReserveBillDetailTempDetails {
            get {
                return this.forceReserveBillDetailTempDetails ?? (this.forceReserveBillDetailTempDetails = new ObservableCollection<ForceReserveBillDetailTemp>());
            }
        }

        public string ReserveType {
            get {
            return this.reserveType??(this.reserveType="");
            }
        }
        public ObservableCollection<KeyValuePair> KvReserveType {
            get {
                return this.kvReserveType;
            }
        }
        public ObservableCollection<KeyValuePair> KvReserveTypeSubItem {
            get {
                return this.kvReserveTypeSubItem;
            }
        }
        public ForceReserveBillDealerDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.Loaded += ForceReserveBillCenterDataEditView_Loaded;
        }
        private void ForceReserveBillCenterDataEditView_Loaded(object sender, System.Windows.RoutedEventArgs e) {           
            ForceReserveBillDetailTempDetails.Clear();
        }
        private void CreateUI() {
            //储备类别
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetBottomStockForceReserveTypesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategor in loadOp.Entities)
                    this.kvReserveType.Add(new KeyValuePair {
                        Key = partsSalesCategor.Id,
                        Value = partsSalesCategor.ReserveType
                    });
            }, null);
            var detailEditView = new DcsDetailDataEditView();
            detailEditView.Register("储备清单", null, () => this.ForceReserveBillDetailForEditDataGridView);
            detailEditView.RegisterButton(new ButtonItem {
                Title = "导入",
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                Command = new DelegateCommand(this.ShowFileDialog)
            });
            detailEditView.RegisterButton(new ButtonItem {
                Title = "导出模板",
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                Command = this.ExportTemplateCommand
            });
            detailEditView.UnregisterButton(detailEditView.InsertButton);
            this.SubRoot.Children.Add(detailEditView);
            FileUploadDataEditPanels.SetValue(Grid.ColumnProperty, 8);
            FileUploadDataEditPanels.SetValue(Grid.RowSpanProperty, 4);
            FileUploadDataEditPanels.Margin = new Thickness(20, 0, 0, 0);
            this.LayoutRoot.Children.Add(FileUploadDataEditPanels);
        }
        private ForceReserveBillForUploadDataEditPanel fileUploadDataEditPanels;
        public ForceReserveBillForUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (ForceReserveBillForUploadDataEditPanel)DI.GetDataEditPanel("ForceReserveBillForUpload"));
            }
        }
        private void ShowFileDialog() {
            var forceReserveBill = this.DataContext as ForceReserveBill;
            if(forceReserveBill == null)
                return;
            if(forceReserveBill.ReserveTypeId == null) {
                UIHelper.ShowNotification("请选择储备类别");
                return;
            }
            this.Uploader.ShowFileDialog();
        }
        //储备类别子项目
        private void txtReserveType_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            var forceReserveBill = this.DataContext as ForceReserveBill;
            if(forceReserveBill == null)
                return;
            reserveType=forceReserveBill.ReserveType;
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetBottomStockForceReserveSubsQuery().Where(er => er.BottomStockForceReserveTypeId == forceReserveBill.ReserveTypeId && er.Status == (int)DcsBaseDataStatus.有效).OrderByDescending(t => t.Id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.kvReserveTypeSubItem.Clear();
                foreach(var partsSalesCategor in loadOp.Entities)
                    this.kvReserveTypeSubItem.Add(new KeyValuePair {
                        Key = partsSalesCategor.Id,
                        Value = partsSalesCategor.SubCode
                    });
            }, null);
        }
        #region 导入清单

        private string strFileName;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private RadUpload uploader;

        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        var forceReserveBill = this.DataContext as ForceReserveBill;
                        this.excelServiceClient.ImpForceReserveBillDealerAsync(e.HandlerData.CustomData["Path"].ToString(), forceReserveBill.ReserveType);
                        this.excelServiceClient.ImpForceReserveBillDealerCompleted -= ExcelServiceClient_ImpForceReserveBillDealerCompleted;
                        this.excelServiceClient.ImpForceReserveBillDealerCompleted += ExcelServiceClient_ImpForceReserveBillDealerCompleted;
                    };
                }
                return this.uploader;
            }
        }

        /// <summary>
        /// 文件上传成功后，调用该方法进行上传后的处理。不支持异步调用
        /// <param>上传文件名 <name>fileName</name> </param>
        /// </summary>
        protected Action<string> UploadFileSuccessedProcessing;

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }

        private void ExcelServiceClient_ImpForceReserveBillDealerCompleted(object sender, ImpForceReserveBillDealerCompletedEventArgs e) {
            var bill = this.DataContext as ForceReserveBill;
            if(bill == null)
                return;
            if(e.rightData != null && e.rightData.Length > 0) {
                foreach(var data in e.rightData) {
                    if(ForceReserveBillDetailTempDetails.Any(t => t.SparePartId == data.SparePartId && t.CompanyId == data.CompanyId)) {
                        continue;
                    }
                    var partsSalesOrderDetail = new ForceReserveBillDetailTemp {
                        SparePartId = data.SparePartId,
                        SparePartCode = data.SparePartCode,
                        SparePartName = data.SparePartName,
                        CompanyCode = data.CompanyCode,
                        CompanyName = data.CompanyName,
                        ForceReserveQty = data.ForceReserveQty,
                        CompanyId = data.CompanyId,
                        CenterPrice = data.CenterPrice,
                        ReserveFee = data.CenterPrice * data.ForceReserveQty,
                        CenterPartProperty=data.CenterPartProperty
                    };                    
                    this.ForceReserveBillDetailTempDetails.Add(partsSalesOrderDetail);
                }
            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowNotification(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));

        }

        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage("请关闭选择的文件后再进行导入");
                    e.Handled = true;
                }
            }
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }

        private ICommand exportFileCommand;

        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private const string EXPORT_DATA_FILE_NAME = "导出服务商强制储备清单模板.xlsx";

        private void InitializeCommand() {
            this.exportFileCommand = new DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                    new ImportTemplateColumn {
                        Name = "企业编号",
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = "企业名称"
                    }, new ImportTemplateColumn {
                        Name = "配件编号",
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = "配件名称"
                    },
                    new ImportTemplateColumn {
                        Name = "储备数量",
                        IsRequired = true
                    }
                };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }

        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }

        #endregion

        private DataGridViewBase ForceReserveBillDetailForEditDataGridView {
            get {
                if(this.forceReserveBillDetailForEditDataGridView == null) {
                    this.forceReserveBillDetailForEditDataGridView = DI.GetDataGridView("ForceReserveBillDetailForEdit");
                    this.forceReserveBillDetailForEditDataGridView.DomainContext = this.DomainContext;
                    this.forceReserveBillDetailForEditDataGridView.DataContext = this;
                }
                return this.forceReserveBillDetailForEditDataGridView;
            }
        }
        protected override void OnEditCancelled() {
            base.OnEditCancelled();
            this.kvReserveType.Clear();
            this.kvReserveTypeSubItem.Clear();
        }

        protected override void OnEditSubmitted() {
            base.OnEditSubmitted();
            this.kvReserveType.Clear();
            this.kvReserveTypeSubItem.Clear();
        }

        protected override void OnEditSubmitting() {
            var forceReserveBill = this.DataContext as ForceReserveBill;
            if(forceReserveBill == null || !this.ForceReserveBillDetailForEditDataGridView.CommitEdit())
                return;
            forceReserveBill.ValidationErrors.Clear();
            foreach(var item in ForceReserveBillDetailTempDetails) {
                item.ValidationErrors.Clear();
            }
            if(forceReserveBill.ReserveTypeId==null) {
                UIHelper.ShowNotification("请选择储备类别");
                return;
            }
            if(forceReserveBill.ReserveTypeId==null) {
                UIHelper.ShowNotification("请选择储备类别子项目");
                return;
            }
            if(!forceReserveBill.ValidateFrom.HasValue) {
                UIHelper.ShowNotification("请选择生效时间");
                return;
            }
            forceReserveBill.Path = FileUploadDataEditPanels.FilePath;       
            try {
                this.DomainContext.新增强制储备单(forceReserveBill, ForceReserveBillDetailTempDetails.ToList(), invokeOp => {
                    if(invokeOp.HasError) {
                        if(!invokeOp.IsErrorHandled)
                            invokeOp.MarkErrorAsHandled();
                        var error = invokeOp.ValidationErrors.FirstOrDefault();
                        if(error != null) {
                            UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage, 5);
                        } else {
                            DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                        }
                        DomainContext.RejectChanges();
                        return;
                    }
                    this.NotifyEditSubmitted();
                }, null);
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }
        protected override string BusinessName {
            get {
                return "新增强制储备单";
            }
        }
        private void NotifyEditSubmitted() {
            var handler = this.CustomEditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }
        public void OnCustomEditSubmitted() {
            this.kvReserveType.Clear();
            this.kvReserveTypeSubItem.Clear();
        }
        protected override bool OnRequestCanSubmit() {
            return true;
        }
    }
}