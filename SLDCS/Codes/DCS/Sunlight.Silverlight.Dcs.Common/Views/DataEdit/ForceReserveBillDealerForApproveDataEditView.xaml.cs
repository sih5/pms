﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class ForceReserveBillDealerForApproveDataEditView {
        private DataGridViewBase forceReserveBillDetailForEditDataGridView;
        private ButtonItem rejectBtn;
        public ForceReserveBillDealerForApproveDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetForceReserveBillsWithDetailByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    FileUploadDataEditPanels.FilePath = entity.Path;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
        private void CreateUI() {
            this.rejectBtn = new ButtonItem {
                Command = new DelegateCommand(this.RejecrCurrentData),
                Title = CommonUIStrings.Action_Title_Reject,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative)
            };
            this.RegisterButton(rejectBtn);
            var detailEditView = new DcsDetailDataEditView();
            detailEditView.Register("储备清单", null, () => this.ForceReserveBillDetailForEditDataGridView);
            this.SubRoot.Children.Add(detailEditView);
            detailEditView.UnregisterButton(detailEditView.InsertButton);
            detailEditView.UnregisterButton(detailEditView.DeleteButton);
            FileUploadDataEditPanels.SetValue(Grid.ColumnProperty, 8);
            FileUploadDataEditPanels.SetValue(Grid.RowSpanProperty, 4);
            FileUploadDataEditPanels.Margin = new Thickness(20, 0, 0, 0);
            FileUploadDataEditPanels.isHiddenButtons = true;
            this.LayoutRoot.Children.Add(FileUploadDataEditPanels);

        }
        private ForceReserveBillForUploadDataEditPanel fileUploadDataEditPanels;
        public ForceReserveBillForUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (ForceReserveBillForUploadDataEditPanel)DI.GetDataEditPanel("ForceReserveBillForUpload"));
            }
        }

        private DataGridViewBase ForceReserveBillDetailForEditDataGridView {
            get {
                if(this.forceReserveBillDetailForEditDataGridView == null) {
                    this.forceReserveBillDetailForEditDataGridView = DI.GetDataGridView("ForceReserveBillDealerForApprove");
                    this.forceReserveBillDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.forceReserveBillDetailForEditDataGridView;
            }
        }
        protected override void OnEditCancelled() {
            base.OnEditCancelled();
        }

        protected override void OnEditSubmitted() {
            base.OnEditSubmitted();
        }
        protected override void OnEditSubmitting() {
            var forceReserveBill = this.DataContext as ForceReserveBill;
            if(forceReserveBill == null || !this.ForceReserveBillDetailForEditDataGridView.CommitEdit())
                return;
            forceReserveBill.ValidationErrors.Clear();
            if(forceReserveBill.CompanyType == (int)DcsCompanyType.服务站) {              
                //L类型储备数量调减为0的条目数不能超过5%，W类型备数量调减为0的条目数不能超过10%
                if(forceReserveBill.ForceReserveBillDetails.Count() > 1 && forceReserveBill.ReserveType == "L" && forceReserveBill.ForceReserveBillDetails.Where(r => r.ForceReserveQty == 0).Count() > forceReserveBill.ForceReserveBillDetails.Count() * 0.05) {
                    UIHelper.ShowNotification("L类型储备数量调减为0的条目数不能超过5%");
                    return;
                }
                if(forceReserveBill.ForceReserveBillDetails.Count() > 1 && forceReserveBill.ReserveType == "W" && forceReserveBill.ForceReserveBillDetails.Where(r => r.ForceReserveQty == 0).Count() > forceReserveBill.ForceReserveBillDetails.Count() * 0.1) {
                    UIHelper.ShowNotification("W类型储备数量调减为0的条目数不能超过10%");
                    return;
                }
            }
            ((IEditableObject)forceReserveBill).EndEdit();
            if(forceReserveBill.Status == (int)DCSForceReserveBillStatus.提交) {
                if(forceReserveBill.Can审核强制储备单) {
                    forceReserveBill.审核强制储备单();
                }
            } else if(forceReserveBill.Status == (int)DCSForceReserveBillStatus.审核通过 ) {
                if(forceReserveBill.Can确认强制储备单) {
                    forceReserveBill.确认强制储备单();
                }
            }
            base.OnEditSubmitting();
        }
        private void RejecrCurrentData() {
            var forceReserveBill = this.DataContext as ForceReserveBill;
            if(forceReserveBill == null)
                return;
            ((IEditableObject)forceReserveBill).EndEdit();
            try {
                if(forceReserveBill.Can驳回强制储备单)
                    forceReserveBill.驳回强制储备单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }
        protected override string Title {
            get {
                var forceReserveBill = this.DataContext as ForceReserveBill;
                if(forceReserveBill == null || forceReserveBill.Status == (int)DCSForceReserveBillStatus.提交) {
                    return "审核强制储备";
                }  else return "确认强制储备";
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }
    }
}
