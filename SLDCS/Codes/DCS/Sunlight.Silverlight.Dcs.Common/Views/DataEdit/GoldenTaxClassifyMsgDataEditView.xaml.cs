﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class GoldenTaxClassifyMsgDataEditView  {
        public GoldenTaxClassifyMsgDataEditView() {
            InitializeComponent();
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetGoldenTaxClassifyMsgsQuery().Where(r => r.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                this.txtGoldenTaxClassifyCode.IsReadOnly = true;
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.SetObjectToEdit(entity);
            }, null);
        }

        protected override void OnEditSubmitting() {
            var goldenTaxClassifyMsg = this.DataContext as GoldenTaxClassifyMsg;
            if(goldenTaxClassifyMsg == null)
                return;
            if(string.IsNullOrEmpty(goldenTaxClassifyMsg.GoldenTaxClassifyCode)) {
                UIHelper.ShowNotification(string.Format(CommonUIStrings.DataEditView_Validation_GoldenTaxClassifyCodeNotNull));
                return;
            }
            if(string.IsNullOrEmpty(goldenTaxClassifyMsg.GoldenTaxClassifyName)) {
                UIHelper.ShowNotification(string.Format(CommonUIStrings.DataEditView_Validation_GoldenTaxClassifyNameNotNull));
                return;
            }
            if(this.EditState == DataEditState.New) {
                this.DomainContext.Load(this.DomainContext.GetGoldenTaxClassifyMsgsQuery().Where(r => r.GoldenTaxClassifyCode == goldenTaxClassifyMsg.GoldenTaxClassifyCode || r.GoldenTaxClassifyName == goldenTaxClassifyMsg.GoldenTaxClassifyName), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    if(loadOp.Entities.Any()) {
                        UIHelper.ShowNotification(string.Format(CommonUIStrings.DataEditView_Validation_GoldenTaxClassifyMsgsIsRepeat));
                        return;
                    } else {
                        ((IEditableObject)goldenTaxClassifyMsg).EndEdit();
                        base.OnEditSubmitting();
                        this.txtGoldenTaxClassifyCode.IsReadOnly = false;
                    }
                }, null);
            } else {
                ((IEditableObject)goldenTaxClassifyMsg).EndEdit();
                base.OnEditSubmitting();
                this.txtGoldenTaxClassifyCode.IsReadOnly = false;
            }
        }

        protected override void OnEditCancelled() {
            this.txtGoldenTaxClassifyCode.IsReadOnly = false;
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
