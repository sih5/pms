﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using System.Collections.ObjectModel;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class LogisticCompanyDataEditView {
        private EnterpriseInformationDataEditPanel enterpriseInformationDataEditPanel;
        private DataGridViewBase logisticCompanyServiceRangeForEditDataGridView;

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_LogisticCompany;
            }
        }

        private List<TiledRegion> tiledRegions;

        private List<TiledRegion> TiledRegions {
            get {
                return this.tiledRegions ?? (this.tiledRegions = new List<TiledRegion>());
            }
        }

        private ObservableCollection<LogisticCompanyServiceRange> logisticCompanyServiceRanges;

        public ObservableCollection<LogisticCompanyServiceRange> LogisticCompanyServiceRanges
        {
            get
            {
                return logisticCompanyServiceRanges ?? (this.logisticCompanyServiceRanges = new ObservableCollection<LogisticCompanyServiceRange>());
            }
            set
            {
                this.logisticCompanyServiceRanges = value;
            }
        }

        private int businessDomain;


        private DataGridViewBase LogisticCompanyServiceRangeForEditDataGridView {
            get {
                if(this.logisticCompanyServiceRangeForEditDataGridView == null) {
                    this.logisticCompanyServiceRangeForEditDataGridView = DI.GetDataGridView("LogisticCompanyServiceRangeForEdit");
                    this.logisticCompanyServiceRangeForEditDataGridView.DomainContext = this.DomainContext;
                    this.logisticCompanyServiceRangeForEditDataGridView.DataContext = this;
                }
                return this.logisticCompanyServiceRangeForEditDataGridView;
            }
        }

        private EnterpriseInformationDataEditPanel EnterpriseInformationDataEditPanel {
            get {
                return this.enterpriseInformationDataEditPanel ?? (this.enterpriseInformationDataEditPanel = (EnterpriseInformationDataEditPanel)DI.GetDataEditPanel("EnterpriseInformation"));

            }
        }

        public LogisticCompanyDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged -= LogisticCompanyDataEditView_DataContextChanged;
            this.DataContextChanged += LogisticCompanyDataEditView_DataContextChanged;
        }

        private void CreateUI() {
            var logisticCompanyDataEditPanel = DI.GetDataEditPanel("LogisticCompany");
            logisticCompanyDataEditPanel.SetValue(Grid.RowProperty, 0);
            logisticCompanyDataEditPanel.SetValue(Grid.ColumnProperty, 0);
            this.Root.Children.Add(logisticCompanyDataEditPanel);
            var dataEditPanel = this.EnterpriseInformationDataEditPanel;
            dataEditPanel.SetValue(Grid.RowProperty, 1);
            dataEditPanel.SetValue(Grid.ColumnProperty, 0);
            this.Root.Children.Add(dataEditPanel);
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(LogisticCompany), "LogisticCompanyServiceRanges"), null, () => this.LogisticCompanyServiceRangeForEditDataGridView);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.SetValue(Grid.RowProperty, 1);
            detailDataEditView.SetValue(Grid.ColumnProperty, 1);
            this.Root.Children.Add(detailDataEditView);
        }

        private void LogisticCompanyDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetBranchesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError || loadOp.Entities == null || !loadOp.Entities.Any())
                {
                    return;
                }
                this.LogisticCompanyServiceRanges.Clear();
                foreach (var entity in loadOp.Entities)
                {
                    //if (entity.Name.Equals("上汽依维柯红岩商用车有限公司")) {
                        this.LogisticCompanyServiceRanges.Add(new LogisticCompanyServiceRange
                        {
                            BranchId = entity.Id,
                            BranchName = entity.Name,
                            BusinessDomain = this.businessDomain == 0 ? (int)DcsLogisticCompanyBusinessDomain.配件发运 : this.businessDomain
                        });
                    //}
                }
                this.businessDomain = (int)DcsLogisticCompanyBusinessDomain.配件发运;
            }, null);

        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetLogisticCompanyWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                {
                    if (!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if (entity != null)
                {
                    if (entity.LogisticCompanyServiceRanges != null && entity.LogisticCompanyServiceRanges.Count() != 0)
                    {
                        this.businessDomain = entity.LogisticCompanyServiceRanges.FirstOrDefault().BusinessDomain;
                    }
                    this.SetObjectToEdit(entity);
                    SetTiledRegionId();
                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            if(!this.LogisticCompanyServiceRangeForEditDataGridView.CommitEdit())
                return;
            var logisticCompany = this.DataContext as LogisticCompany;
            if(logisticCompany == null)
                return;
            logisticCompany.ValidationErrors.Clear();
            if (this.LogisticCompanyServiceRanges != null && this.LogisticCompanyServiceRanges.Count() != 0)
            {
                if (logisticCompany.LogisticCompanyServiceRanges.Count() != 0)
                {
                    var logisticCompanyServiceRange1 = logisticCompany.LogisticCompanyServiceRanges.Where(r => r.BranchId == this.LogisticCompanyServiceRanges.FirstOrDefault().BranchId).FirstOrDefault();
                    logisticCompanyServiceRange1.BusinessDomain = this.LogisticCompanyServiceRanges.FirstOrDefault().BusinessDomain;
                }
                else {
                    logisticCompany.LogisticCompanyServiceRanges.Add(this.LogisticCompanyServiceRanges.FirstOrDefault());
                }
            }

            foreach(var logisticCompanyServiceRange in logisticCompany.LogisticCompanyServiceRanges)
                logisticCompanyServiceRange.ValidationErrors.Clear();
            if(logisticCompany.LogisticCompanyServiceRanges.GroupBy(entity => new {
                entity.BranchId,
                entity.BusinessDomain
            }).Any(array => array.Count() > 1)) {
                UIHelper.ShowAlertMessage(CommonUIStrings.DataEditView_Validation_LogisticCompanyServiceRange_DetailsIsSole);
                return;
            }
            if (string.IsNullOrWhiteSpace(logisticCompany.Code))
                logisticCompany.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_ResponsibleUnit_CodeIsNull_Logistic, new[] {
                    "Code"
                }));
            else
                logisticCompany.Code = logisticCompany.Code.Trim();
            if (logisticCompany.Code != null && logisticCompany.Code.Length > 11)
                logisticCompany.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_ResponsibleUnit_CodeIsOverLength_Logistic, new[]{"Code"
                }));
            if (string.IsNullOrWhiteSpace(logisticCompany.Name))
                logisticCompany.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_ResponsibleUnit_NameIsNull_Logistic, new[] {
                    "Name"
                }));

            if(string.IsNullOrEmpty(logisticCompany.Company.CityName) || string.IsNullOrEmpty(logisticCompany.Company.ProvinceName) || string.IsNullOrEmpty(logisticCompany.Company.CountyName)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_Company_CityNameIsNull);
                return;
            }
            if (logisticCompany.StorageCenter == null)
                logisticCompany.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_ResponsibleUnit_StorageCenterIsNull_Logistic, new[]{
                    "StorageCenter"
                }));
            try {
                    logisticCompany.Company.Code = logisticCompany.Code;
                    logisticCompany.Company.Name = logisticCompany.Name;
                    if(!string.IsNullOrEmpty(logisticCompany.Company.CustomerCode)){
                        logisticCompany.Company.CustomerCode = logisticCompany.Company.CustomerCode.Trim();
                    }
                    if(!string.IsNullOrEmpty(logisticCompany.Company.SupplierCode)){
                        logisticCompany.Company.SupplierCode = logisticCompany.Company.SupplierCode.Trim();
                    }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            if (logisticCompany.HasValidationErrors)
                return;
            ((IEditableObject)logisticCompany).EndEdit();
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void Reset() {
            var logisticCompany = this.DataContext as LogisticCompany;
            if(logisticCompany != null && (logisticCompany.Company != null && this.DomainContext.Companies.Contains(logisticCompany.Company))) {
                this.DomainContext.Companies.Detach(logisticCompany.Company);
            }
            if(this.DomainContext.LogisticCompanies.Contains(logisticCompany))
                this.DomainContext.LogisticCompanies.Detach(logisticCompany);
        }

        private void SetTiledRegionId() {
            var logisticCompany = this.DataContext as LogisticCompany;
            if(logisticCompany == null)
                return;
            var company = logisticCompany.Company;
            if(company == null)
                return;
            if(company.ProvinceName == null)
                return;
            if(this.EnterpriseInformationDataEditPanel.KvProvinceNames.Any()) {
                company.ProvinceId = this.EnterpriseInformationDataEditPanel.KvProvinceNames.First(e => e.Value == company.ProvinceName).Key;
                if(company.CityName == null)
                    return;
                company.CityId = this.EnterpriseInformationDataEditPanel.KvCityNames.First(e => e.Value == company.CityName).Key;
                if(company.CountyName == null)
                    return;
                company.CountyId = this.EnterpriseInformationDataEditPanel.KvCountyNames.First(e => e.Value == company.CountyName).Key;
            } else {
                this.DomainContext.Load(DomainContext.GetTiledRegionsQuery().Where(ex => ex.CountyName != null).OrderBy(ex => ex.ProvinceName), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError)
                        return;
                    foreach(var repairObject in loadOp.Entities)
                        this.TiledRegions.Add(new TiledRegion {
                            Id = repairObject.Id,
                            ProvinceName = repairObject.ProvinceName,
                            CityName = repairObject.CityName,
                            CountyName = repairObject.CountyName,
                        });
                    this.EnterpriseInformationDataEditPanel.KvProvinceNames.Clear();
                    foreach(var item in TiledRegions) {
                        var values = this.EnterpriseInformationDataEditPanel.KvProvinceNames.Select(e => e.Value);
                        if(values.Contains(item.ProvinceName))
                            continue;
                        this.EnterpriseInformationDataEditPanel.KvProvinceNames.Add(new KeyValuePair {
                            Key = item.Id,
                            Value = item.ProvinceName
                        });
                    }
                    company.ProvinceId = this.EnterpriseInformationDataEditPanel.KvProvinceNames.First(e => e.Value == company.ProvinceName).Key;
                    if(company.CityName == null)
                        return;
                    company.CityId = this.EnterpriseInformationDataEditPanel.KvCityNames.First(e => e.Value == company.CityName).Key;
                    if(company.CountyName == null)
                        return;
                    company.CountyId = this.EnterpriseInformationDataEditPanel.KvCountyNames.First(e => e.Value == company.CountyName).Key;
                }, null);
            }
        }
    }
}
