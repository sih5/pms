﻿using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class LogisticCompanyDetailDataEditView {
        private DataGridViewBase logisticCompanyServiceRangeDataGridView;

        private DataGridViewBase LogisticCompanyServiceRangeDataGridView {
            get {
                if(this.logisticCompanyServiceRangeDataGridView == null) {
                    this.logisticCompanyServiceRangeDataGridView = DI.GetDataGridView("LogisticCompanyServiceRange");
                    this.logisticCompanyServiceRangeDataGridView.DomainContext = this.DomainContext;
                }
                return this.logisticCompanyServiceRangeDataGridView;
            }
        }

        protected override string Title {
            get {
                return CommonUIStrings.DataEditView_Title_ViewDetail_LogisticCompany;
            }
            set {
                base.Title = value;
            }
        }

        public LogisticCompanyDetailDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            this.LayoutRoot.Children.Add(DI.GetDetailPanel("LogisticCompany"));
            var tabControl = new RadTabControl();
            tabControl.Items.Add(new RadTabItem {
                Header = Utils.GetEntityLocalizedName(typeof(LogisticCompany), "LogisticCompanyServiceRanges"),
                Content = this.LogisticCompanyServiceRangeDataGridView
            });
            tabControl.SetValue(Grid.ColumnProperty, 1);
            this.LayoutRoot.Children.Add(tabControl);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetLogisticCompanyWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
