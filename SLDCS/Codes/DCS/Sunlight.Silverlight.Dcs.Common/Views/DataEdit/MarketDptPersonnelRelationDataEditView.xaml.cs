﻿using System.Windows.Controls;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Dcs.Common.Resources;
using System.Collections.ObjectModel;
using Sunlight.Silverlight.Dcs.Web;
using System.Linq;
using System.ComponentModel;
using System.ServiceModel.DomainServices.Client;
using System.Collections.Generic;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Core;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class MarketDptPersonnelRelationDataEditView : INotifyPropertyChanged {
        private DataGridViewBase marketDptPersonnelRelationForEditDataGridView;
        private ObservableCollection<MarketDptPersonnelRelation> marketDptPersonnelRelations;
        public event PropertyChangedEventHandler PropertyChanged;
        private string code, marketingDepartmentName;
        private int marketDepartmentId;

        public MarketDptPersonnelRelationDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private DataGridViewBase MarketDptPersonnelRelationForEditDataGridView {
            get {
                if(this.marketDptPersonnelRelationForEditDataGridView == null) {
                    this.marketDptPersonnelRelationForEditDataGridView = DI.GetDataGridView("MarketDptPersonnelRelationForEdit");
                    this.marketDptPersonnelRelationForEditDataGridView.DomainContext = this.DomainContext;
                    this.marketDptPersonnelRelationForEditDataGridView.DataContext = this;
                }
                return this.marketDptPersonnelRelationForEditDataGridView;
            }
        }

        private void CreateUI() {
            var queryWindowMarketDptPersonnelRelation = DI.GetQueryWindow("MarketingDepartmentDropDown");
            queryWindowMarketDptPersonnelRelation.SelectionDecided += this.QueryWindowMarketDptPersonnelRelation_SelectionDecided;
            this.popupTextBox.PopupContent = queryWindowMarketDptPersonnelRelation;
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(CommonUIStrings.DataGridView_Title_Personnel, null, () => this.MarketDptPersonnelRelationForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);
        }

        public ObservableCollection<MarketDptPersonnelRelation> MarketDptPersonnelRelations {
            get {
                return this.marketDptPersonnelRelations ?? (this.marketDptPersonnelRelations = new ObservableCollection<MarketDptPersonnelRelation>());
            }
        }

        protected override string BusinessName {
            get {
                return CommonUIStrings.BusinessName_MarketDptPersonnelRelations;
            }
        }

        protected override void OnEditCancelled() {
            if(MarketDptPersonnelRelations != null)
                this.MarketDptPersonnelRelations.Clear();
            Code = string.Empty;
            MarketingDepartmentName = string.Empty;
            base.OnEditCancelled();
        }

        protected override void OnEditSubmitted() {
            if(MarketDptPersonnelRelations != null)
                this.MarketDptPersonnelRelations.Clear();
            Code = string.Empty;
            MarketingDepartmentName = string.Empty;
            base.OnEditSubmitted();
        }

        protected override void OnEditSubmitting() {
            if(!this.MarketDptPersonnelRelationForEditDataGridView.CommitEdit())
                return;
            if(string.IsNullOrWhiteSpace(this.Code)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_MarketDptPersonnelRelation_CodeIsNull);
                return;
            }

            if(this.MarketDptPersonnelRelations.Count== 0) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_MarketDptPersonnelRelationIsNull);
                return;
            }

            foreach(var entity in this.MarketDptPersonnelRelations)
                entity.ValidationErrors.Clear();
            foreach(var entity in this.MarketDptPersonnelRelations) {
                entity.MarketDepartmentId = this.marketDepartmentId;
                entity.BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                entity.Status = (int)DcsBaseDataStatus.有效;
                ((IEditableObject)entity).EndEdit();
            }
            foreach(var item in this.DomainContext.MarketDptPersonnelRelations.Where(e => e.EntityState == EntityState.New).ToList()) {
                if(!MarketDptPersonnelRelations.Contains(item))
                    this.DomainContext.MarketDptPersonnelRelations.Remove(item);
            }
            if(this.MarketDptPersonnelRelations.Any(r => r.ValidationErrors.Any()))
                return;

            base.OnEditSubmitting();
        }

        private void QueryWindowMarketDptPersonnelRelation_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var marketingDepartmentEntities = queryWindow.SelectedEntities.Cast<MarketingDepartment>().FirstOrDefault();
            if(marketingDepartmentEntities == null)
                return;
            marketDepartmentId = marketingDepartmentEntities.Id;
            Code = marketingDepartmentEntities.Code;
            MarketingDepartmentName = marketingDepartmentEntities.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public string Code {
            get {
                return this.code;
            }
            set {
                this.code = value;
                this.OnPropertyChanged("Code");
            }
        }

        public string MarketingDepartmentName {
            get {
                return this.marketingDepartmentName;
            }
            set {
                this.marketingDepartmentName = value;
                this.OnPropertyChanged("MarketingDepartmentName");
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetMarketDptPersonnelRelationDetailQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                Code = entity.MarketingDepartment.Code;
                MarketingDepartmentName = entity.MarketingDepartment.Name;
                marketDepartmentId = entity.MarketDepartmentId;
                entity.Personnel.SequeueNumber = 1;
                MarketDptPersonnelRelations.Add(entity);
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }


    }
}
