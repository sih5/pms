﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class MarketDptPersonnelRelationImportDataEditView {
        private ICommand exportFileCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出分销中心与人员关系模板.xlsx";
        private DataGridViewBase marketDptPersonnelRelationImportDataGridView;

        private DataGridViewBase MarketDptPersonnelRelationImportDataGridView {
            get {
                return this.marketDptPersonnelRelationImportDataGridView ?? (this.marketDptPersonnelRelationImportDataGridView = DI.GetDataGridView("MarketDptPersonnelRelationImport"));
            }
        }
        public MarketDptPersonnelRelationImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return CommonUIStrings.BusinessName_ImportMarketDptPersonnelRelations;
            }
        }

        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = CommonUIStrings.BusinessName_ImportMarketDptPersonnelRelationsList,
                Content = this.MarketDptPersonnelRelationImportDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.ImportMarketDptPersonnelRelationAsync(fileName);
            this.ExcelServiceClient.ImportMarketDptPersonnelRelationCompleted -= ExcelServiceClient_ImportMarketDptPersonnelRelationCompleted;
            this.ExcelServiceClient.ImportMarketDptPersonnelRelationCompleted += ExcelServiceClient_ImportMarketDptPersonnelRelationCompleted;
        }

        void ExcelServiceClient_ImportMarketDptPersonnelRelationCompleted(object sender, ImportMarketDptPersonnelRelationCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!this.HasImportingError) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImport, 5);
            } else {
                if(!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if(!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditView_Text_BranchSupplierRelation_BranchName,
                                                IsRequired = true
                                            },
                                            // new ImportTemplateColumn {
                                            //    Name = CommonUIStrings.QueryPanel_Title_EngineProductLine_PartsSalesCategory,
                                            //    IsRequired = true
                                            //},
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_MarketDptPersonnelRelation_Code,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_MarketDptPersonnelRelation_Name,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataGridView_ColumnItem_Title_MarketDptPersonnelRelation_PersonnelId,
                                                IsRequired = true
                                            },new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_MarketDptPersonnelRelation_Type,
                                                IsRequired = true
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }
    }
}
