using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class MarketingDepartmentDataEditView {
        private ObservableCollection<DealerMarketDptRelation> dealerMarketDptRelations;

        public MarketingDepartmentDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private MarketingDepartmentDataEditPanel marketingDepartmentDataEditPanel;

        private MarketingDepartmentDataEditPanel MarketingDepartmentDataEditPanel {
            get {
                return this.marketingDepartmentDataEditPanel ?? (this.marketingDepartmentDataEditPanel = new MarketingDepartmentDataEditPanel());
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(this.MarketingDepartmentDataEditPanel);
            this.Loaded += this.MarketingDepartmentDataEditView_Loaded;
        }

        private void MarketingDepartmentDataEditView_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            //避免通过查询退出后，没有执行OnEditCancelled或OnEditSubmitted(仅在新增方式进入后执行)
            if(EditState == DataEditState.New && this.DealerMarketDptRelations.Any())
                this.DealerMarketDptRelations.Clear();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetMarketingDepartmentsWithRelationsAndDetailsQuery(id, BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                //  this.DealerMarketDptRelations.Clear();
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.DealerMarketDptRelations.Clear();
                    foreach(var marketingDptRelation in entity.DealerMarketDptRelations) {
                        if(marketingDptRelation.Status != (int)DcsBaseDataStatus.作废)
                            this.DealerMarketDptRelations.Add(marketingDptRelation);
                    }
                    this.SetObjectToEdit(entity);
                    SetTiledRegionId();
                }
            }, null);
        }

        private List<TiledRegion> tiledRegions;

        private List<TiledRegion> TiledRegions {
            get {
                return this.tiledRegions ?? (this.tiledRegions = new List<TiledRegion>());
            }
        }

        private void SetTiledRegionId() {
            var marketingDepartment = this.DataContext as MarketingDepartment;
            if(marketingDepartment == null)
                return;

            if(marketingDepartment.ProvinceName == null)
                return;
            this.DomainContext.Load(DomainContext.GetTiledRegionsQuery().Where(ex => ex.CountyName != null).OrderBy(ex => ex.ProvinceName), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var repairObject in loadOp.Entities)
                    this.TiledRegions.Add(new TiledRegion {
                        Id = repairObject.Id,
                        ProvinceName = repairObject.ProvinceName,
                        CityName = repairObject.CityName,
                        CountyName = repairObject.CountyName,
                    });
                this.MarketingDepartmentDataEditPanel.KvProvinceNames.Clear();
                foreach(var item in TiledRegions) {
                    var values = this.MarketingDepartmentDataEditPanel.KvProvinceNames.Select(e => e.Value);
                    if(values.Contains(item.ProvinceName))
                        continue;
                    this.MarketingDepartmentDataEditPanel.KvProvinceNames.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.ProvinceName
                    });
                }
                marketingDepartment.ProvinceId = this.MarketingDepartmentDataEditPanel.KvProvinceNames.First(e => e.Value == marketingDepartment.ProvinceName).Key;
                if(marketingDepartment.CityName == null)
                    return;
                marketingDepartment.CityId = this.MarketingDepartmentDataEditPanel.KvCityNames.First(e => e.Value == marketingDepartment.CityName).Key;
                if(marketingDepartment.CountyName == null)
                    return;
                marketingDepartment.CountyId = this.MarketingDepartmentDataEditPanel.KvCountyNames.First(e => e.Value == marketingDepartment.CountyName).Key;
            }, null);
        }


        protected override void OnEditSubmitting() {
            var marketingDepartment = this.DataContext as MarketingDepartment;
            if(marketingDepartment == null)
                return;
            //清理之前校验出现的异常
            marketingDepartment.ValidationErrors.Clear();
            foreach(var relation in marketingDepartment.DealerMarketDptRelations)
                relation.ValidationErrors.Clear();
            foreach(var relation in this.DealerMarketDptRelations)
                relation.ValidationErrors.Clear();


            if(marketingDepartment.BusinessType <= 0)
                marketingDepartment.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_MarketingDepartment_BusinessTypeIsNull, new[] {
                    "BusinessType"
                }));
            if(this.DealerMarketDptRelations.Where(relation => relation.Status != (int)DcsBaseDataStatus.作废).GroupBy(d => d.DealerId).Any(d => d.Count() > 1)) {
                UIHelper.ShowAlertMessage(CommonUIStrings.DataEditView_Validation_DealerMarketDptRelation_DealerIdNotAllowedRepeat);
                return;
            }
            foreach(var relation in this.DealerMarketDptRelations.Where(dptRelation => dptRelation.DealerId == default(int))) {
                relation.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_DealerMarketDptRelation_DealerIdIsNull, new[] {
                    "DealerId"
                }));
            }
            if(marketingDepartment.HasValidationErrors || marketingDepartment.DealerMarketDptRelations.Any(dptRelation => dptRelation.HasValidationErrors) || this.DealerMarketDptRelations.Any(dptRelation => dptRelation.HasValidationErrors)) {
                if(marketingDepartment.DealerMarketDptRelations.Any(dptRelation => dptRelation.DealerId == default(int)) ||
                    this.DealerMarketDptRelations.Any(dptRelation => dptRelation.DealerId == default(int)))
                    UIHelper.ShowAlertMessage(CommonUIStrings.DataEditView_Validation_DealerMarketDptRelation_DealerIdIsNull);
                return;
            }
            //将DataContext实体中的清单与绑定的集合对比。DataContext实体清单中存在，集合中不存在的实体，标记该实体为作废。
            foreach(var item in marketingDepartment.DealerMarketDptRelations.Where(item => !this.DealerMarketDptRelations.Contains(item))) {
                if(item.EntityState == EntityState.New || item.EntityState == EntityState.Detached)
                    //第一次提交失败后，第二次提交时当前主单实体是只读状态。只能通过DomainContext来删除之前新添加的数据
                    this.DomainContext.DealerMarketDptRelations.Remove(item);
                else {
                    item.Status = (int)DcsBaseDataStatus.作废;
                }
            }
            //将绑定的集合与DataContext实体中的清单对比。集合中存在的实体，在DataContext实体清单中不存在，将集合中的实体加到DataContext实体的清单中。
            foreach(var relation in this.DealerMarketDptRelations.Where(entity => entity.EntityState == EntityState.New || entity.EntityState == EntityState.Detached))
                if(!marketingDepartment.DealerMarketDptRelations.Contains(relation)) {
                    marketingDepartment.DealerMarketDptRelations.Add(relation);
                    if(relation.Can新增营销分公司市场部及经销商关系)
                        relation.新增营销分公司市场部及经销商关系(marketingDepartment.BusinessType);
                }
            ((IEditableObject)marketingDepartment).EndEdit();
            base.OnEditSubmitting();
        }



        protected override void Reset() {
            var marketingDepartment = this.DataContext as MarketingDepartment;
            if(marketingDepartment == null)
                return;
            if(this.DomainContext.MarketingDepartments.Contains(marketingDepartment))
                this.DomainContext.MarketingDepartments.Detach(marketingDepartment);
            if(DealerMarketDptRelations != null)
                this.DealerMarketDptRelations.Clear();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_MarketingDepartment;
            }
        }

        public ObservableCollection<DealerMarketDptRelation> DealerMarketDptRelations {
            get {
                return this.dealerMarketDptRelations ?? (this.dealerMarketDptRelations = new ObservableCollection<DealerMarketDptRelation>());
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
