﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class MarketingDepartmentForCAMDataEditView {
        private readonly ObservableCollection<DealerMarketDptRelation> dealerMarketDptRelations = new ObservableCollection<DealerMarketDptRelation>();
        private readonly ObservableCollection<DealerMarketDptRelation> dealerMarketDptRelationsForValidations = new ObservableCollection<DealerMarketDptRelation>();
        private DataGridViewBase dealerMarketDptRelationDataGridView;
        public MarketingDepartmentForCAMDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private DataGridViewBase DealerMarketDptRelationDataGridView {
            get {
                if(this.dealerMarketDptRelationDataGridView == null) {
                    this.dealerMarketDptRelationDataGridView = DI.GetDataGridView("DealerMarketDeptRelationForEdit");
                    this.dealerMarketDptRelationDataGridView.DomainContext = this.DomainContext;
                    this.dealerMarketDptRelationDataGridView.DataContext = this;
                }
                return this.dealerMarketDptRelationDataGridView;
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("MarketingDepartmentForCAM"));
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(CommonUIStrings.DataGridView_Title_MarketingDepartment_DealerMarketDptRelations, null, () => this.DealerMarketDptRelationDataGridView);//Utils.GetEntityLocalizedName(typeof(MarketingDepartment), "DealerMarketDptRelations")CommonUIStrings.DataGridView_Title_MarketingDepartment_DealerMarketDptRelations
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);
            this.Loaded += this.MarketingDepartmentDataEditView_Loaded;
        }

        private void MarketingDepartmentDataEditView_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            //避免通过查询退出后，没有执行OnEditCancelled或OnEditSubmitted(仅在新增方式进入后执行)
            if(EditState == DataEditState.New && this.DealerMarketDptRelations.Any())
                this.DealerMarketDptRelations.Clear();
            this.DomainContext.Load(this.DomainContext.GetDealerMarketDptRelationsQuery().Where(d => d.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp2 => {
                if(loadOp2.HasError) {
                    if(!loadOp2.IsErrorHandled)
                        loadOp2.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp2);
                    return;
                }
                DealerMarketDptRelationsForValidations.Clear();
                if(loadOp2.Entities.Any()) {
                    foreach(var marketingDptRelation in loadOp2.Entities) {
                        this.DealerMarketDptRelationsForValidations.Add(marketingDptRelation);
                    }
                }
            }, null);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetMarketingDepartmentsWithRelationsAndDetailsQuery(id, BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                this.DealerMarketDptRelations.Clear();
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    foreach(var marketingDptRelation in entity.DealerMarketDptRelations.Where(e => e.Dealer != null && e.Status == (int)DcsBaseDataStatus.有效)) {
                        this.DealerMarketDptRelations.Add(marketingDptRelation);
                    }
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            if(!this.DealerMarketDptRelationDataGridView.CommitEdit())
                return;
            var marketingDepartment = this.DataContext as MarketingDepartment;
            if(marketingDepartment == null)
                return;
            //清理之前校验出现的异常
            marketingDepartment.ValidationErrors.Clear();
            foreach(var relation in marketingDepartment.DealerMarketDptRelations) {
                relation.ValidationErrors.Clear();
            }
            foreach(var relation in this.DealerMarketDptRelations)
                relation.ValidationErrors.Clear();


            if(string.IsNullOrEmpty(marketingDepartment.Code)) {
                marketingDepartment.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_MarketingDepartment_CodeIsNull, new[] {
                    "Code"
                }));
            }
            if(string.IsNullOrEmpty(marketingDepartment.Name)) {
                marketingDepartment.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_MarketingDepartment_NameIsNull, new[] {
                    "Name"
                }));
            }

            if(marketingDepartment.BusinessType <= 0)
                marketingDepartment.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_MarketingDepartment_BusinessTypeIsNull, new[] {
                    "BusinessType"
                }));
            if(this.DealerMarketDptRelations.Where(relation => relation.Status != (int)DcsBaseDataStatus.作废).GroupBy(d => d.DealerId).Any(d => d.Count() > 1)) {
                UIHelper.ShowAlertMessage(CommonUIStrings.DataEditView_Validation_DealerMarketDptRelation_DealerIdNotAllowedRepeat);
                return;
            }

            if(marketingDepartment.DealerMarketDptRelations.Any(dptRelation => dptRelation.DealerId == default(int)) ||
                    this.DealerMarketDptRelations.Any(dptRelation => dptRelation.DealerId == default(int))) {
                UIHelper.ShowAlertMessage(CommonUIStrings.DataEditView_Validation_DealerMarketDptRelation_DealerIdIsNull);
                return;
            }

            if(marketingDepartment.HasValidationErrors || marketingDepartment.DealerMarketDptRelations.Any(dptRelation => dptRelation.HasValidationErrors) || this.DealerMarketDptRelations.Any(dptRelation => dptRelation.HasValidationErrors)) {
                return;
            }
            //将DataContext实体中的清单与绑定的集合对比。DataContext实体清单中存在，集合中不存在的实体，标记该实体为作废。
            foreach(var item in marketingDepartment.DealerMarketDptRelations.Where(item => item.Dealer != null && !this.DealerMarketDptRelations.Contains(item))) {
                if(item.EntityState == EntityState.New || item.EntityState == EntityState.Detached) {
                    //第一次提交失败后，第二次提交时当前主单实体是只读状态。只能通过DomainContext来删除之前新添加的数据
                    this.DomainContext.DealerMarketDptRelations.Remove(item);
                } else {
                    item.Status = (int)DcsBaseDataStatus.作废;
                }
            }
            //将绑定的集合与DataContext实体中的清单对比。集合中存在的实体，在DataContext实体清单中不存在，将集合中的实体加到DataContext实体的清单中。
            foreach(var relation in this.DealerMarketDptRelations.Where(entity => entity.EntityState == EntityState.New || entity.EntityState == EntityState.Detached))
                if(!marketingDepartment.DealerMarketDptRelations.Contains(relation)) {
                    marketingDepartment.DealerMarketDptRelations.Add(relation);
                }
            ((IEditableObject)marketingDepartment).EndEdit();
            if(!Validation())
                return;
            base.OnEditSubmitting();
        }


        private bool Validation() {
            bool temp = true;
            var marketingDepartment = this.DataContext as MarketingDepartment;
            if(marketingDepartment == null)
                return false;
            if(marketingDepartment.DealerMarketDptRelations.Any()) {
                foreach(var item in marketingDepartment.DealerMarketDptRelations) {
                    if(this.DealerMarketDptRelationsForValidations.Any(d => d.DealerId == item.DealerId && d.MarketId != item.MarketId)) {
                        UIHelper.ShowAlertMessage(string.Format(CommonUIStrings.DataEditView_Validation_DealerMarketDptRelation_DealerIdIsAlreadyExists, item.Dealer.Code));
                        temp = false;
                        break;
                    }
                }
            }
            return temp;
        }

        protected override void OnEditCancelled() {
            if(DealerMarketDptRelations != null)
                this.DealerMarketDptRelations.Clear();
            base.OnEditCancelled();
        }

        protected override void OnEditSubmitted() {
            if(DealerMarketDptRelations != null)
                this.DealerMarketDptRelations.Clear();
            base.OnEditSubmitted();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override string BusinessName {
            get {
                return CommonUIStrings.BusinessName_DealerMarketDptRelation;//DcsUIStrings.BusinessName_MarketingDepartment;
            }
        }

        public ObservableCollection<DealerMarketDptRelation> DealerMarketDptRelations {
            get {
                return this.dealerMarketDptRelations;
            }
        }

        public ObservableCollection<DealerMarketDptRelation> DealerMarketDptRelationsForValidations {
            get {
                return this.dealerMarketDptRelationsForValidations;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
