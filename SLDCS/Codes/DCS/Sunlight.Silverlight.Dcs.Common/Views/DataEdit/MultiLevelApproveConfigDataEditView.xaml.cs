﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Core.Model;
using System.Collections.ObjectModel;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Core;
using Telerik.Windows.Controls;
using System.Windows;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class MultiLevelApproveConfigDataEditView {

        public ObservableCollection<KeyValuePair> Types {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

         private readonly string[] kvNames = {
            "MultiLevelApproveConfigType"
        };

        public MultiLevelApproveConfigDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData();
        }

        private KeyValueManager keyValueManager;

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override string BusinessName {
            get {
                return "多级审核配置";
            }
        }

        protected override void OnEditSubmitting() {
            var multiLevelApproveConfig = this.DataContext as MultiLevelApproveConfig;
            if(multiLevelApproveConfig == null)
                return;
            multiLevelApproveConfig.ValidationErrors.Clear();
            
            if (multiLevelApproveConfig.ApproverIds == default(int)) {
                UIHelper.ShowNotification("审核人不可为空");
                return;
            }
            if (multiLevelApproveConfig.Type == default(int)) {
                 UIHelper.ShowNotification("单据类型不可为空");
                return;
            }
            if (!multiLevelApproveConfig.MinApproveFee.HasValue || multiLevelApproveConfig.MinApproveFee < 0) {
                 UIHelper.ShowNotification("审核金额下限输入错误");
                return;
            }
            if (!multiLevelApproveConfig.MaxApproveFee.HasValue || multiLevelApproveConfig.MaxApproveFee < 0) {
                 UIHelper.ShowNotification("审核金额上限输入错误");
                return;
            }
            if (multiLevelApproveConfig.MaxApproveFee.Value <= multiLevelApproveConfig.MinApproveFee.Value) {
                 UIHelper.ShowNotification("审核金额上限不可小于审核金额下限");
                return;
            }
            if(multiLevelApproveConfig.HasValidationErrors)
                return;
            ((IEditableObject)multiLevelApproveConfig).EndEdit();

            try{
                if(EditState == DataEditState.New) {
                    if (multiLevelApproveConfig.Can新增多级审核配置)
                    multiLevelApproveConfig.新增多级审核配置();
                } else {
                    if (multiLevelApproveConfig.Can更新多级审核配置)
                    multiLevelApproveConfig.更新多级审核配置();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        private void CreateUI() {
             var queryWindowPerson = DI.GetQueryWindow("PersonnelForCompany");
            queryWindowPerson.SelectionDecided += this.queryWindowPerson_SelectionDecided;
            this.ptbPersonName.PopupContent = queryWindowPerson;
        }

        private void queryWindowPerson_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if (queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var selectedPersonnel = queryWindow.SelectedEntities.Cast<Personnel>().FirstOrDefault();
            if (selectedPersonnel == null)
                return;
            var multiLevelApproveConfig = this.DataContext as MultiLevelApproveConfig;
            if (multiLevelApproveConfig == null)
                return;
            try {
                multiLevelApproveConfig.ApproverIds = selectedPersonnel.Id;
                multiLevelApproveConfig.ApproverNames = selectedPersonnel.Name;
            } finally {
                var parent = queryWindow.ParentOfType<RadWindow>();
                if (parent != null)
                    parent.Close();
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetMultiLevelApproveConfigsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if (entity != null) {
                    entity.ApproverIds = entity.ApproverId;
                    entity.ApproverNames = entity.ApproverName;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
    }
}
