﻿using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Common.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class NotificationDetailDataEditView : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;
        public bool isBranch, isCompany, isLogisticCompany, isAgency, isResponsibleUnit, isPartsSupplier, isCompanyOrAgency;

        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        private FileUploadDataEditPanel fileUploadDataEditPanels;
        protected FileUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (FileUploadDataEditPanel)DI.GetDataEditPanel("FileUpload"));
            }
        }

        protected override string Title {
            get {
                return CommonUIStrings.DataEditPanel_Title_NotificationDetail;
            }
        }

        private DataGridViewBase companyDetailDataGridView;
        public DataGridViewBase CompanyDetailDataGridView {
            get {
                if(this.companyDetailDataGridView == null) {
                    this.companyDetailDataGridView = DI.GetDataGridView("CompanyDetailForEdit");
                    this.companyDetailDataGridView.DomainContext = this.DomainContext;
                }
                return this.companyDetailDataGridView;
            }
        }

        private void CreateUI() {
            var detailEditView = new DcsDetailDataEditView();
            detailEditView.Register(CommonUIStrings.DetailEditView_Title_CompanyList, null, () => this.CompanyDetailDataGridView);
            detailEditView.UnregisterButton(detailEditView.InsertButton);
            detailEditView.UnregisterButton(detailEditView.DeleteButton);
            this.Attachment.Children.Add(FileUploadDataEditPanels);
            this.RootRight.Children.Add(detailEditView);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetNotificationDetailQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity != null) {
                    this.FileUploadDataEditPanels.FilePath = entity.Path;
                    foreach(var item in entity.NotificationLimits) {
                        if(item.DesCompanyTpye == (int)DcsCompanyType.分公司)
                            this.IsBranch = true;
                        if(item.DesCompanyTpye == (int)DcsCompanyType.服务站)
                            this.IsCompany = true;
                        if(item.DesCompanyTpye == (int)DcsCompanyType.物流公司)
                            this.IsLogisticCompany = true;
                        if(item.DesCompanyTpye == (int)DcsCompanyType.代理库)
                            this.IsAgency = true;
                        if(item.DesCompanyTpye == (int)DcsCompanyType.责任单位)
                            this.IsResponsibleUnit = true;
                        if(item.DesCompanyTpye == (int)DcsCompanyType.配件供应商)
                            this.IsPartsSupplier = true;
                        if(item.DesCompanyTpye == (int)DcsCompanyType.服务站兼代理库)
                            this.IsCompanyOrAgency = true;
                    }
                    var i = 1;
                    if(entity.CompanyDetails != null) {
                        foreach(var detail in entity.CompanyDetails)
                            detail.SerialNumber = i++;
                    }
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        protected override void OnEditCancelled() {
            this.IsBranch = false;
            this.IsCompany = false;
            this.IsLogisticCompany = false;
            this.IsAgency = false;
            this.IsResponsibleUnit = false;
            this.IsPartsSupplier = false;
            this.IsCompanyOrAgency = false;
            base.OnEditCancelled();
        }

        public bool IsBranch {
            get {
                return this.isBranch;
            }
            set {
                this.isBranch = value;
                this.OnPropertyChanged("IsBranch");
            }
        }
        public bool IsCompany {
            get {
                return this.isCompany;
            }
            set {
                this.isCompany = value;
                this.OnPropertyChanged("IsCompany");
            }
        }
        public bool IsLogisticCompany {
            get {
                return this.isLogisticCompany;
            }
            set {
                this.isLogisticCompany = value;
                this.OnPropertyChanged("IsLogisticCompany");
            }
        }
        public bool IsAgency {
            get {
                return this.isAgency;
            }
            set {
                this.isAgency = value;
                this.OnPropertyChanged("IsAgency");
            }
        }
        public bool IsResponsibleUnit {
            get {
                return this.isResponsibleUnit;
            }
            set {
                this.isResponsibleUnit = value;
                this.OnPropertyChanged("IsResponsibleUnit");
            }
        }
        public bool IsPartsSupplier {
            get {
                return this.isPartsSupplier;
            }
            set {
                this.isPartsSupplier = value;
                this.OnPropertyChanged("IsPartsSupplier");
            }
        }
        public bool IsCompanyOrAgency {
            get {
                return this.isCompanyOrAgency;
            }
            set {
                this.isCompanyOrAgency = value;
                this.OnPropertyChanged("IsCompanyOrAgency");
            }
        }

        public NotificationDetailDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.FileUploadDataEditPanels.isVisibility = true;
        }
    }
}
