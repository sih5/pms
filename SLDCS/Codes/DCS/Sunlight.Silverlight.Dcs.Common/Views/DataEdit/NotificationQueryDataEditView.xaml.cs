﻿using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Common.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class NotificationQueryDataEditView {
        private FileUploadDataEditPanel fileUploadDataEditPanels;
        protected FileUploadDataEditPanel FileUploadDataEditPanels {
            get {
                if(this.fileUploadDataEditPanels == null) {
                    this.fileUploadDataEditPanels = (FileUploadDataEditPanel)DI.GetDataEditPanel("FileUpload");
                    this.fileUploadDataEditPanels.GenerateNotificationOpLog += fileUploadDataEditPanels_GenerateNotificationOpLog;
                }
                return fileUploadDataEditPanels;
            }
        }

        private void fileUploadDataEditPanels_GenerateNotificationOpLog(object sender, System.EventArgs e) {
            var notification = this.DataContext as Notification;
            if(notification == null)
                return;
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.NotificationOpLogs.Add(new NotificationOpLog() {
                NotificationID = notification.Id,
                OperationType = (int)DcsOperationType.下载附件,
                CustomerCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId,
                CustomerCompanyCode = BaseApp.Current.CurrentUserData.EnterpriseCode,
                CustomerCompanyName = BaseApp.Current.CurrentUserData.EnterpriseName,
                Status = (int)DcsBaseDataStatus.有效
            });
            dcsDomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    dcsDomainContext.RejectChanges();
                    return;
                }
            }, null);
        }

        private void CreateUI() {
            this.File.Children.Add(FileUploadDataEditPanels);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override string Title {
            get {
                return DcsUIStrings.BusinessName_NotificationDetail;
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetNotificationsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.FileUploadDataEditPanels.FilePath = entity.Path;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        public NotificationQueryDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.FileUploadDataEditPanels.isVisibility = true;
        }

    }
}
