﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Common.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class NotificationReplyDataEditView : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;
        public bool isBranch = true, isCompany, isLogisticCompany, isAgency, isResponsibleUnit, isPartsSupplier, isCompanyOrAgency;
        private string textOrFile;
        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
        private CheckBoxDataEditPanel checkBoxDataEditPanels;
        protected CheckBoxDataEditPanel CheckBoxDataEditPanels {
            get {
                return this.checkBoxDataEditPanels ?? (this.checkBoxDataEditPanels = (CheckBoxDataEditPanel)DI.GetDataEditPanel("CheckBox"));
            }
        }

        private FileUploadDataEditPanel fileUploadDataEditPanels;
        protected FileUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (FileUploadDataEditPanel)DI.GetDataEditPanel("FileUpload"));
            }
        }

        private FileUploadDataEditPanel replyFileUploadDataEditPanels;
        protected FileUploadDataEditPanel ReplyFileUploadDataEditPanels {
            get {
                if(replyFileUploadDataEditPanels != null)
                    return this.replyFileUploadDataEditPanels;

                var panel = this.replyFileUploadDataEditPanels = (FileUploadDataEditPanel)DI.GetDataEditPanel("FileUpload");
                panel.PanelTitle.Text = CommonUIStrings.DataEditPanel_Validation_Notification_AttachmentReply;
                //panel.PanelTitle.Foreground = new SolidColorBrush(Colors.Red);
                return panel;
            }
        }

        private DataGridViewBase comapnyDetailDataGridView;
        public DataGridViewBase ComapnyDetailDataGridView {
            get {
                if(this.comapnyDetailDataGridView == null) {
                    this.comapnyDetailDataGridView = DI.GetDataGridView("CompanyDetailForEdit");
                    this.comapnyDetailDataGridView.DomainContext = this.DomainContext;
                }
                return this.comapnyDetailDataGridView;
            }
        }

        public override object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            if(!string.IsNullOrEmpty(subject)) {
                textOrFile = subject;
            }
            return null;
        }

        protected override string Title {
            get {
                return CommonUIStrings.DataEditPanel_Validation_Notification_WordReply;
            }
        }

        private void CreateUI() {
            FileUploadDataEditPanels.isHiddenButtons = true;
            this.FileUploadDataEditPanels.SetValue(Grid.ColumnSpanProperty, 3);
            this.Attachment.Children.Add(FileUploadDataEditPanels);
            this.RootRight2.Children.Add(ReplyFileUploadDataEditPanels);
            this.ReplyFileUploadDataEditPanels.SetValue(Grid.RowProperty, 5);
            this.ReplyFileUploadDataEditPanels.SetValue(Grid.ColumnProperty, 1);
            this.ReplyFileUploadDataEditPanels.SetValue(Grid.ColumnSpanProperty, 3);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_Notification;
            }
        }


        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetNotificationDetailQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity != null) {

                    var notificationReply = entity.NotificationReply.Where(r => r.CompanyId == BaseApp.Current.CurrentUserData.EnterpriseId).LastOrDefault();
                    if(notificationReply != null) {
                        if(notificationReply.ReplyContent != null) {
                            entity.ReplyContent = notificationReply.ReplyContent;
                            this.tbReplyContent.Text = notificationReply.ReplyContent;
                        }
                        entity.ReplyFilePath = notificationReply.ReplyFilePath;
                    }
                    this.FileUploadDataEditPanels.FilePath = entity.Path;
                    this.ReplyFileUploadDataEditPanels.FilePath = entity.ReplyFilePath;
                    foreach(var item in entity.NotificationLimits) {
                        if(item.DesCompanyTpye == (int)DcsCompanyType.分公司)
                            this.IsBranch = true;
                        if(item.DesCompanyTpye == (int)DcsCompanyType.服务站)
                            this.IsCompany = true;
                        if(item.DesCompanyTpye == (int)DcsCompanyType.物流公司)
                            this.IsLogisticCompany = true;
                        if(item.DesCompanyTpye == (int)DcsCompanyType.代理库)
                            this.IsAgency = true;
                        if(item.DesCompanyTpye == (int)DcsCompanyType.责任单位)
                            this.IsResponsibleUnit = true;
                        if(item.DesCompanyTpye == (int)DcsCompanyType.配件供应商)
                            this.IsPartsSupplier = true;
                        if(item.DesCompanyTpye == (int)DcsCompanyType.服务站兼代理库)
                            this.IsCompanyOrAgency = true;
                    }
                    var i = 1;
                    if(entity.CompanyDetails != null) {
                        foreach(var detail in entity.CompanyDetails)
                            detail.SerialNumber = i++;
                    }
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            var notification = this.DataContext as Notification;
            if(notification == null)
                return;
            notification.Path = FileUploadDataEditPanels.FilePath;
            notification.ReplyFilePath = ReplyFileUploadDataEditPanels.FilePath;
            if(notification.Type == default(int))
                notification.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_Notification_TypeIsNull, new[] {
                    "Type"
                }));
            if(string.IsNullOrWhiteSpace(notification.Code)) {
                notification.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_Notification_CodeIsNull, new[] {
                    "Code"
                }));
            }
            if(string.IsNullOrWhiteSpace(notification.Title)) {
                notification.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_Notification_TitleIsNull, new[] {
                    "Title"
                }));
            }
            if(string.IsNullOrWhiteSpace(notification.PublishDep)) {
                notification.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditPanel_Validation_Notification_PublishDep, new[] { 
                    "PublishDep"
                }));
            }
            if(this.textOrFile == CommonActionKeys.UPLOAD && string.IsNullOrWhiteSpace(notification.ReplyFilePath)) {
                notification.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditPanel_Validation_Notification_AttAchmentIsNull, new[] { 
                    "ReplyFilePath"
                }));
                UIHelper.ShowNotification(CommonUIStrings.DataEditPanel_Validation_Notification_AttAchmentIsNull);
                return;
            }
            if(this.textOrFile == CommonActionKeys.REPLY && string.IsNullOrWhiteSpace(notification.ReplyContent)) {
                notification.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditPanel_Validation_Notification_WordReplyIsnull, new[] { 
                    "ReplyContent"
                }));
                UIHelper.ShowNotification(CommonUIStrings.DataEditPanel_Validation_Notification_WordReplyIsnull);
                return;
            }
            if(notification.UrgentLevel == default(int)) {
                notification.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditPanel_Validation_Notification_UrgentLevel, new[] { 
                    "UrgentLevel" 
                }));
            }
            if(notification.HasValidationErrors)
                return;
            if(notification.ValidationErrors.Any())
                return;
            notification.ValidationErrors.Clear();
            ((IEditableObject)notification).EndEdit();
            base.OnEditSubmitting();
        }

        protected override void OnEditCancelled() {
            this.FileUploadDataEditPanels.FilePath = "";
            this.ReplyFileUploadDataEditPanels.FilePath = "";
            this.IsBranch = false;
            this.IsCompany = false;
            this.IsLogisticCompany = false;
            this.IsAgency = false;
            this.IsResponsibleUnit = false;
            this.IsPartsSupplier = false;
            this.IsCompanyOrAgency = false;
            base.OnEditCancelled();
        }

        protected override void OnEditSubmitted() {
            this.FileUploadDataEditPanels.FilePath = "";
            this.IsBranch = false;
            this.IsCompany = false;
            this.IsLogisticCompany = false;
            this.IsAgency = false;
            this.IsResponsibleUnit = false;
            this.IsPartsSupplier = false;
            this.IsCompanyOrAgency = false;
            base.OnEditSubmitted();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        public NotificationReplyDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        public bool IsBranch {
            get {
                return this.isBranch;
            }
            set {
                this.isBranch = value;
                this.OnPropertyChanged("IsBranch");
            }
        }
        public bool IsCompany {
            get {
                return this.isCompany;
            }
            set {
                this.isCompany = value;
                this.OnPropertyChanged("IsCompany");
            }
        }
        public bool IsLogisticCompany {
            get {
                return this.isLogisticCompany;
            }
            set {
                this.isLogisticCompany = value;
                this.OnPropertyChanged("IsLogisticCompany");
            }
        }
        public bool IsAgency {
            get {
                return this.isAgency;
            }
            set {
                this.isAgency = value;
                this.OnPropertyChanged("IsAgency");
            }
        }
        public bool IsResponsibleUnit {
            get {
                return this.isResponsibleUnit;
            }
            set {
                this.isResponsibleUnit = value;
                this.OnPropertyChanged("IsResponsibleUnit");
            }
        }
        public bool IsPartsSupplier {
            get {
                return this.isPartsSupplier;
            }
            set {
                this.isPartsSupplier = value;
                this.OnPropertyChanged("IsPartsSupplier");
            }
        }
        public bool IsCompanyOrAgency {
            get {
                return this.isCompanyOrAgency;
            }
            set {
                this.isCompanyOrAgency = value;
                this.OnPropertyChanged("IsCompanyOrAgency");
            }
        }

        #region 导入企业清单操作
        private string strFileName;
        private const string EXPORT_DATA_FILE_NAME = "导出企业清单模板.xlsx";
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private RadUpload uploader;
        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        ShellViewModel.Current.IsBusy = true;
                        //todo:新增导入企业清单方法
                        this.excelServiceClient.ImportCompanyDetailAsync(e.HandlerData.CustomData["Path"].ToString());
                        this.excelServiceClient.ImportCompanyDetailCompleted -= ExcelServiceClient_ImportCompanyDetailCompleted;
                        this.excelServiceClient.ImportCompanyDetailCompleted += ExcelServiceClient_ImportCompanyDetailCompleted;
                    };
                }
                return this.uploader;
            }
        }

        /// <summary>
        /// 文件上传成功后，调用该方法进行上传后的处理。不支持异步调用
        /// <param>上传文件名 <name>fileName</name> </param>
        /// </summary>
        protected Action<string> UploadFileSuccessedProcessing;
        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //取当前登录用户的HashCode来标记上传文件的唯一性
                //this.UploadFileSuccessedProcessing.Invoke(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }

        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage(CommonUIStrings.DataEditPanel_Validation_Notification_File);
                    e.Handled = true;
                }
            }
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }

        private ICommand exportFileCommand;
        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                    new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_CompanyCode,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_CompanyName
                    }
                };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }

        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }

        private void ExcelServiceClient_ImportCompanyDetailCompleted(object sender, ImportCompanyDetailCompletedEventArgs e) {
            var notification = this.DataContext as Notification;
            if(notification == null)
                return;
            var detailList = notification.CompanyDetails.ToList();
            foreach(var detail in detailList) {
                notification.CompanyDetails.Remove(detail);
            }
            var i = 1;
            if(e.rightData != null && e.rightData.Length > 0) {
                foreach(var data in e.rightData) {
                    var companyDetail = new CompanyDetail {
                        CompanyId = data.CompanyId,
                        Type = data.Type,
                        Name = data.Name,
                        Code = data.Code,
                        SerialNumber = i++
                    };
                    notification.CompanyDetails.Add(companyDetail);
                }
            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowNotification(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));

        }
        #endregion
    }
}
