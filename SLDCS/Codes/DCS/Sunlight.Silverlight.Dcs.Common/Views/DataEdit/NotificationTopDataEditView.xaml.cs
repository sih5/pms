﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.ComponentModel;
using Sunlight.Silverlight.Dcs.Web;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class NotificationTopDataEditView  {
        public NotificationTopDataEditView() {
            InitializeComponent();
        }

        protected override string BusinessName {
            get {
                return CommonUIStrings.DataEditView_Notification_Top;//DcsUIStrings.BusinessName_Notification;
            }
        }

        protected override void OnEditSubmitting() {
            var notification = this.DataContext as Notification;
            if(notification == null)
                return;
            ((IEditableObject)notification).EndEdit();
            base.OnEditSubmitting();
        }


        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetNotificationsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                entity.Top = true;
                entity.TopTime = null;
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

    }
}
