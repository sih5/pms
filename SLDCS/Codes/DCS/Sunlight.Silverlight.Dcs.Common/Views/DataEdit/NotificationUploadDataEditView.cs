﻿
using Sunlight.Silverlight.Dcs.Common.Resources;
namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public class NotificationUploadDataEditView : NotificationReplyDataEditView {
        protected override string Title {
            get {
                return CommonUIStrings.Action_Title_Upload;
            }
        }
    }
}
