﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class OMWMSPrintLableDataEditView : INotifyPropertyChanged {
        private int printNums, printNumber;
        public OMWMSPrintLableDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.Loaded += OMWMSPrintLableDataEditView_Loaded;
        }

        void OMWMSPrintLableDataEditView_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            txtNumber.Value = null;
            txtPrintNumber.Value = null;
            this.DataContext = null;
            this.SpareParts = null;
        }

        private void CreateUI() {
            this.Title = "配件详细信息";
            this.RegisterButton(new ButtonItem {
                Title = "打印标签",
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/print.png", UriKind.Relative),
                Command = new DelegateCommand(this.Print)
            }, true);
            this.HideSaveButton();
            this.HideCancelButton();
        }

        private void Print() {
            if(PrintNumber == 0) {
                UIHelper.ShowNotification("打印张数必填并且必须大于0");
                return;
            }
            SpareParts.First().PrintNumber = PrintNumber;
            SpareParts.First().PrintNums = PrintNums;
            BasePrintWindow printWindow1 = new OMWMSPrintLablePrintWindow {
                Header = "欧曼WMS标签打印",
                SpareParts = SpareParts
            };
            printWindow1.ShowDialog();
        }


        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int[])id));
            else
                this.LoadEntityToEdit((int[])id);
        }

        private void LoadEntityToEdit(int[] id) {
            this.DomainContext.Load(this.DomainContext.GetSparePartsByIdsQuery(id), loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                if(!loadOp.Entities.Any())
                    return;
                foreach(var sparePart in loadOp.Entities) {
                    this.txtCode.Text = sparePart.Code;
                    this.txtName.Text = sparePart.Name;
                    this.SpareParts.Add(sparePart);
                }
            }, null);
        }

        private ObservableCollection<SparePart> spareParts;
        public ObservableCollection<SparePart> SpareParts {
            get {
                return spareParts ?? (this.spareParts = new ObservableCollection<SparePart>());
            }
            set {
                this.spareParts = value;
            }
        }

        public int PrintNums {
            get {
                return this.printNums;
            }
            set {
                this.printNums = value;
                this.OnPropertyChanged("PrintNums");
            }
        }

        public int PrintNumber {
            get {
                return this.printNumber;
            }
            set {
                this.printNumber = value;
                this.OnPropertyChanged("PrintNumber");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
