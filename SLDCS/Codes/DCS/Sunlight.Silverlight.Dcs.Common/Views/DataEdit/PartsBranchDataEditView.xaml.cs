﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class PartsBranchDataEditView {
        public PartsBranchDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsBranch;
            }
        }

        protected override void OnEditSubmitting() {
            var partsBranch = this.DataContext as PartsBranch;
            if(partsBranch == null)
                return;
            partsBranch.ValidationErrors.Clear();
            if(!partsBranch.MinSaleQuantity.HasValue || partsBranch.MinSaleQuantity <= default(int))
                partsBranch.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_PartsBranch_MinSaleQuantityIsNullOrLessThanOne, new[] {
                    "MinSaleQuantity"
                }));
            if(partsBranch.PurchaseCycle.HasValue && partsBranch.PurchaseCycle <= default(int))
                partsBranch.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_PartsBranch_PurchaseCycleLessThanOne, new[] {
                    "PurchaseCycle"
                }));
            if(string.IsNullOrWhiteSpace(partsBranch.PartsWarrantyCategoryName)) {
                partsBranch.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_PartsBranch_PartsWarrantyCategoryNameIsNull, new[] {
                    "PartsWarrantyCategoryName"
                }));
            }
            if(partsBranch.HasValidationErrors)
                return;
            ((IEditableObject)partsBranch).EndEdit();
            base.OnEditSubmitting();
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("PartsBranch"));
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsBranchesQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }
    }
}
