﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class PartsBranchForImportUpdateDataEditView {
        private DataGridViewBase partsBranchForImportForEdit;
        private ICommand exportFileCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出配件分品牌商务信息模板.xlsx";
        protected override string Title {
            get {
                return CommonUIStrings.DataEditView_Title_PartsBranchForImportUpdate;
            }
        }

        protected DataGridViewBase ImportDataGridView {
            get {
                return this.partsBranchForImportForEdit ?? (this.partsBranchForImportForEdit = DI.GetDataGridView("PartsBranchForImportForEdit"));
            }
        }

        public PartsBranchForImportUpdateDataEditView() {
            InitializeComponent();
            this.Initializer.Register(CreateUI);
        }

        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = CommonUIStrings.DataEditView_Title_PartsBranchForImportUpdateList,
                Content = this.ImportDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }
        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.ImportUpdatePartsBranchAsync(fileName);
            this.ExcelServiceClient.ImportUpdatePartsBranchCompleted -= ExcelServiceClient_ImportUpdatePartsBranchCompleted;
            this.ExcelServiceClient.ImportUpdatePartsBranchCompleted += ExcelServiceClient_ImportUpdatePartsBranchCompleted;
        }

        private void ExcelServiceClient_ImportUpdatePartsBranchCompleted(object sender, ImportUpdatePartsBranchCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!HasImportingError) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImport, 10);
            } else {
                if(!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if(!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }
        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }
        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                             new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_EngineProductLine_PartsSalesCategory
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                                                IsRequired = true
                                            },
                                            //new ImportTemplateColumn {
                                            //    Name = "配件名称"
                                            //},
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataGridView_ColumnItem_ResponsibleUnitBranch_BranchName
                                            },
                                             new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_VirtualPartsBranch_IsOrderable
                                            },
                                            new ImportTemplateColumn {
                                                Name =CommonUIStrings.QueryPanel_QueryItem_Title_VirtualPartsBranch_IsSalable,
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_VirtualPartsBranch_IsDirectSupply
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditView_Text_PartsBranch_MInPackingAmount,
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditView_Text_PartsBranch_ProductLifeCycle,
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditView_Text_PartsBranch_LossType,
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditView_Text_PartsBranch_PartABC,
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditView_Text_PartsBranch_PartsReturnPolicy
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditView_Text_PartsBranch_StockMaximum,
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditView_Text_PartsBranch_StockMinimum,
                                            }, 
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditView_Text_PartsBranch_PartsMaterialManageCost,
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditView_Text_PartsBranch_PartsWarrantyLong,
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_IRGroupCode
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_IRGroupName
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditView_Text_PartsBranch_AutoApproveUpLimit
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartsInboundCheckBillRemark,
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
    }
}
