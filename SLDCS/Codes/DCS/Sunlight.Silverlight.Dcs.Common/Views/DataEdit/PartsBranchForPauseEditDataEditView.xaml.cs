﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.DataGrid;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class PartsBranchForPauseEditDataEditView {
        private DataGridViewBase partsBranchForImportDataGridView;
        private const string EXPORT_DATA_FILE_NAME = "导出配件分品牌信息批量替换模板.xlsx";
        private ICommand exportFileCommand;
        private string uniqueId;
        readonly List<ImportTemplateColumn> columnItemsValues = new List<ImportTemplateColumn>();
        private ObservableCollection<PartsBranchExtend> partsBranchExtend;
        public ObservableCollection<PartsBranchExtend> PartsBranchExtends {
            get {
                return this.partsBranchExtend ?? (this.partsBranchExtend = new ObservableCollection<PartsBranchExtend>());
            }
        }

        public PartsBranchForPauseEditDataEditView() {
            InitializeComponent();
            //this.Loaded += PartsBranchForPauseEditDataEditView_Loaded;
            this.Initializer.Register(this.CreateUI);
        }

        //private void PartsBranchForPauseEditDataEditView_Loaded(object sender, RoutedEventArgs e) {
        //    this.PartsBranchExtends.Clear();
        //}

        protected DataGridViewBase ImportDataGridView {
            get {
                if(this.partsBranchForImportDataGridView == null) {
                    this.partsBranchForImportDataGridView = DI.GetDataGridView("PartsBranchForPauseEdit");
                    this.partsBranchForImportDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsBranchForImportDataGridView;
            }
        }

        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = CommonUIStrings.DataEditView_Text_PartsBranch_ReplaceList,
                Content = this.ImportDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.columnItemsValues.Clear();
            switch(this.uniqueId) {
                case "AbandonEdit":
                    this.ExcelServiceClient.批量作废配件营销信息Async(fileName);
                    this.ExcelServiceClient.批量作废配件营销信息Completed -= ExcelServiceClient_批量作废配件营销信息Completed;
                    this.ExcelServiceClient.批量作废配件营销信息Completed += ExcelServiceClient_批量作废配件营销信息Completed;
                    break;
                case "PauseEdit":
                    this.ExcelServiceClient.批量停用配件营销信息Async(fileName);
                    this.ExcelServiceClient.批量停用配件营销信息Completed -= ExcelServiceClient_批量停用配件营销信息Completed;
                    this.ExcelServiceClient.批量停用配件营销信息Completed += ExcelServiceClient_批量停用配件营销信息Completed;

                    break;
                case "ResumeEdit":
                    this.ExcelServiceClient.批量恢复配件营销信息Async(fileName);
                    this.ExcelServiceClient.批量恢复配件营销信息Completed -= ExcelServiceClient_批量恢复配件营销信息Completed;
                    this.ExcelServiceClient.批量恢复配件营销信息Completed += ExcelServiceClient_批量恢复配件营销信息Completed;
                    break;

            }

        }

        void ExcelServiceClient_批量恢复配件营销信息Completed(object sender, 批量恢复配件营销信息CompletedEventArgs e) {
            this.ExcelServiceClient_Completed(e.rightData, e.errorDataFileName, e.errorMessage);
        }

        void ExcelServiceClient_批量作废配件营销信息Completed(object sender, 批量作废配件营销信息CompletedEventArgs e) {
            this.ExcelServiceClient_Completed(e.rightData, e.errorDataFileName, e.errorMessage);
        }

        void ExcelServiceClient_批量停用配件营销信息Completed(object sender, 批量停用配件营销信息CompletedEventArgs e) {
            this.ExcelServiceClient_Completed(e.rightData, e.errorDataFileName, e.errorMessage);
        }
        private void ExcelServiceClient_Completed(PartsBranchExtend[] partsBranchExtends, string errorDataFileName, string errorMessage) {
            this.PartsBranchExtends.Clear();
            if(partsBranchExtends.Any()) {
                var i = 1;
                foreach(var partsBranch in partsBranchExtends) {
                    partsBranch.SerialNumber = i;
                    this.PartsBranchExtends.Add(partsBranch);
                    i++;
                }
            }
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(errorDataFileName)) || (!string.IsNullOrWhiteSpace(errorMessage));
            if(!this.HasImportingError) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImport, 5);
            } else {
                if(!string.IsNullOrEmpty(errorMessage))
                    UIHelper.ShowAlertMessage(errorMessage);
                if(!string.IsNullOrEmpty(errorDataFileName))
                    this.ExportFile(errorDataFileName);
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }
        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((string)id));
            else
                this.LoadEntityToEdit((string)id);
        }

        private void LoadEntityToEdit(string uniqueId) {
            this.uniqueId = uniqueId;
            this.PartsBranchExtends.Clear();
            this.columnItemsValues.Clear();

            switch(uniqueId) {
                case "PauseEdit":
                    this.Title = CommonUIStrings.Action_Title_Batch_Stop;
                    break;
                case "ResumeEdit":

                    this.Title = CommonUIStrings.Action_Title_Batch_Recovery;
                    break;
                case "AbandonEdit":

                    this.Title = CommonUIStrings.Action_Title_Batch_Abandon;
                    break;


            }
            this.columnItemsValues.Add(new ImportTemplateColumn {
                Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                IsRequired = true
            });
            this.columnItemsValues.Add(new ImportTemplateColumn {
                Name = CommonUIStrings.DataDeTailPanel_Text_BottomStock_SparePartName,
            });
            this.columnItemsValues.Add(new ImportTemplateColumn {
                Name = CommonUIStrings.QueryPanel_Title_EngineProductLine_PartsSalesCategory,
                IsRequired = true

            });
            var gridView = this.ImportDataGridView as PartsBranchForPauseEditDataGridView;
            if(gridView == null)
                return;
            //  gridView.ExchangeData(null, uniqueId, null);
        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, this.columnItemsValues, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                }
                if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                    this.ExportFile(loadOp.Value);
            }, null));
        }






    }
}
