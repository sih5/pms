﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class PartsBranchForReplaceRepairClassifyDataEditView {
        private DataGridViewBase replaceDataGridView;
        private ICommand exportFileCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出批量导入修改配件保修分类信息模板.xlsx";

        protected override string Title {
            get {
                return CommonUIStrings.DataEditView_Title_PartsBranchForReplaceRepair;
            }
        }

        private ObservableCollection<PartsBranchExtend> partsBranchExtend;
        public ObservableCollection<PartsBranchExtend> PartsBranchExtends {
            get {
                return this.partsBranchExtend ?? (this.partsBranchExtend = new ObservableCollection<PartsBranchExtend>());
            }
        }

        protected DataGridViewBase ReplaceDataGridView {
            get {
                if(this.replaceDataGridView == null) {
                    this.replaceDataGridView = DI.GetDataGridView("PartsBranchForReplaceRepairClassify");
                    this.replaceDataGridView.DomainContext = this.DomainContext;
                }
                return this.replaceDataGridView;
            }
        }

        public PartsBranchForReplaceRepairClassifyDataEditView() {
            InitializeComponent();
            this.Loaded += DataEditView_Loaded;
            this.Initializer.Register(CreateUI);
        }

        private void DataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.PartsBranchExtends.Clear();
        }

        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = CommonUIStrings.DataEditView_Title_PartsBranchForPartReturnPolicyList,
                Content = this.ReplaceDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }
        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.批量导入修改配件保修分类Async(fileName);
            this.ExcelServiceClient.批量导入修改配件保修分类Completed -= ExcelServiceClient_ImportUpdatePartsBranchCompleted;
            this.ExcelServiceClient.批量导入修改配件保修分类Completed += ExcelServiceClient_ImportUpdatePartsBranchCompleted;
        }

        private void ExcelServiceClient_ImportUpdatePartsBranchCompleted(object sender, 批量导入修改配件保修分类CompletedEventArgs e) {
            this.PartsBranchExtends.Clear();
            if(e.rightData.Any()) {
                var i = 1;
                foreach(var sparePart in e.rightData) {
                    sparePart.SerialNumber = i;
                    this.PartsBranchExtends.Add(sparePart);
                    i++;
                }
            }
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!HasImportingError) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImport, 10);
            } else {
                if(!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if(!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                                                IsRequired = true
                                            }, new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_EngineProductLine_PartsSalesCategory,
                                                IsRequired = true
                                            }, new ImportTemplateColumn {
                                                Name =CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_PartsWarrantyCategoryCode,
                                                IsRequired = true
                                            }, new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_PartsWarrantyCategoryName,
                                                IsRequired = true
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
    }
}
