﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class PartsBranchForServiceingDataEditView : INotifyPropertyChanged {
        private KeyValueManager keyValueManager;
        public event PropertyChangedEventHandler PropertyChanged;
        private ObservableCollection<KeyValuePair> kvPartsSalesCategorys;
        private int partsSalesCategoryId;
        public int PartsSalesCategoryId {
            get {
                return this.partsSalesCategoryId;
            }
            set {
                this.partsSalesCategoryId = value;
                this.OnPropertyChanged("PartsSalesCategoryId");
            }
        }

        public object KvLossType {
            get {
                return this.KeyValueManager[this.kvName[1]];
            }
        }

        public object KvPartsAttribution {
            get {
                return this.KeyValueManager[this.kvName[2]];
            }
        }

        private string partsSalesCategoryName;
        public string PartsSalesCategoryName {
            get {
                return this.partsSalesCategoryName;
            }
            set {
                this.partsSalesCategoryName = value;
                this.OnPropertyChanged("PartsSalesCategoryName");
            }
        }

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        protected override string Title {
            get {
                return CommonUIStrings.DataEditView_Title_PartsBranchForServiceing;
            }
        }
        private readonly string[] kvName = {
             "PartsWarrantyTerm_ReturnPolicy","SparePart_LossType","PartsAttribution"
        };

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public PartsBranchForServiceingDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvName);
        }
        private ObservableCollection<KeyValuePair> kvReturnPolicy;

        private ObservableCollection<KeyValuePair> KvReturnPolicy {
            get {
                return this.kvReturnPolicy ?? (this.kvReturnPolicy = new ObservableCollection<KeyValuePair>());
            }
        }
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        public ObservableCollection<KeyValuePair> KvPartsSalesCategorys {
            get {
                if(kvPartsSalesCategorys == null)
                    kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();
                return this.kvPartsSalesCategorys;
            }

        }

        private void CreateUI() {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(ex => ex.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && ex.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvPartsSalesCategorys.Clear();
                foreach(var item in loadOp.Entities) {
                    this.KvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.Name,
                    });
                }
                this.partsSalesCategory.SelectedIndex = 0;//默认品牌
            }, null);

            this.KeyValueManager.Register(kvName);
            KvReturnPolicy.Clear();
            this.KeyValueManager.LoadData(() => {
                foreach(var item in this.KeyValueManager[kvName[0]]) {
                    if(item.Key != (int)DcsPartsWarrantyTermReturnPolicy.总部调件)
                        KvReturnPolicy.Add(item);
                }
                cbPartsReturnPolicys.ItemsSource = KvReturnPolicy;
            });
            var queryWindowSparePart = DI.GetQueryWindow("SparePart");
            this.ptbSparePart.PopupContent = queryWindowSparePart;
            queryWindowSparePart.SelectionDecided += this.PartsBranchSupplierName_SelectionDecided;
            //var queryWindowPartsWarrantyCategory = DI.GetQueryWindow("PartsWarrantyCategory");
            //this.ptbSpareptbPartsWarrantyCategory.PopupContent = queryWindowPartsWarrantyCategory;
            //queryWindowPartsWarrantyCategory.SelectionDecided += this.PartsBranchPartsWarrantyCategoryName_SelectionDecided;
        }

        //private void PartsBranchPartsWarrantyCategoryName_SelectionDecided(object sender, EventArgs e) {
        //    var queryWindow = sender as DcsQueryWindowBase;
        //    if(queryWindow == null || queryWindow.SelectedEntities == null)
        //        return;

        //    //var partsWarrantyCategory = queryWindow.SelectedEntities.Cast<PartsWarrantyCategory>().FirstOrDefault();
        //    //if(partsWarrantyCategory == null)
        //    //    return;

        //    //var partsBranch = this.DataContext as PartsBranch;
        //    //if(partsBranch == null)
        //    //    return;
        //    //partsBranch.PartsWarrantyCategoryId = partsWarrantyCategory.Id;
        //    //partsBranch.PartsWarrantyCategoryCode = partsWarrantyCategory.Code;
        //    //partsBranch.PartsWarrantyCategoryName = partsWarrantyCategory.Name;

        //    var parent = queryWindow.ParentOfType<RadWindow>();
        //    if(parent != null)
        //        parent.Close();
        //}

        private void PartsBranchSupplierName_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var sparePart = queryWindow.SelectedEntities.Cast<SparePart>().FirstOrDefault();
            if(sparePart == null)
                return;
            var partsBranch = this.DataContext as PartsBranch;
            //手动清除上次添加到DomainContext中的数据
            if (partsBranch != null && this.DomainContext.PartsBranches.Contains(partsBranch))
                this.DomainContext.PartsBranches.Detach(partsBranch);
            this.DomainContext.Load(this.DomainContext.GetPartsBranchesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && r.PartId == sparePart.Id && r.PartsSalesCategoryId == PartsSalesCategoryId), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                {
                    if (!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if (entity != null)
                {
                    this.SetObjectToEdit(entity);
                }
                else
                {
                    var partsBranchs = new PartsBranch();
                    partsBranchs.PartId = sparePart.Id;
                    partsBranchs.PartCode = sparePart.Code;
                    partsBranchs.PartName = sparePart.Name;
                    partsBranchs.PartsSalesCategoryId = PartsSalesCategoryId;
                    partsBranchs.PartsSalesCategoryName = PartsSalesCategoryName;
                    partsBranchs.BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    partsBranchs.BranchName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    partsBranchs.Status = (int)DcsBaseDataStatus.有效;
                    partsBranchs.IsService = false;
                    this.SetValue(DataContextProperty, partsBranchs);
                    this.DomainContext.PartsBranches.Add(partsBranchs);
                }
            }, null);
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        protected override void OnEditSubmitting() {
            var partsBranch = this.DataContext as PartsBranch;
            if(partsBranch == null)
                return;
            if (partsBranch.PartsReturnPolicy <= 0)
            {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_PartsBranch_PartsReturnPolicy);
                return;
            }
            partsBranch.ValidationErrors.Clear();
            if (partsBranch.HasValidationErrors)
                return;
            this.DomainContext.Load(this.DomainContext.GetSalesCenterstrategiesQuery().Where(p => p.PartsSalesCategoryId == partsBranch.PartsSalesCategoryId), loadOp =>
            {
                if (loadOp.HasError)
                    return;
                var entity = loadOp.Entities.FirstOrDefault();
                if (entity != null && entity.IsEngNameRequired == true)
                {
                    this.DomainContext.Load(this.DomainContext.GetSparePartsQuery().Where(p => p.Id == partsBranch.PartId), loadOp1 =>
                    {
                        if (loadOp1.HasError)
                            return;
                        var firstOrDefault = loadOp1.Entities.FirstOrDefault();
                        if (firstOrDefault == null)
                        {
                            UIHelper.ShowNotification(string.Format(CommonUIStrings.DataEditView_Validation_PartsBranch_Spare, partsBranch.PartCode));
                            return;
                        }
                        if (string.IsNullOrEmpty(firstOrDefault.EnglishName))
                        {
                            UIHelper.ShowNotification(string.Format(CommonUIStrings.DataEditView_Error_PartsBranch_EnglishNameIsNull, partsBranch.PartName));
                            return;
                        }
                        else
                        {
                            ((IEditableObject)partsBranch).EndEdit();
                            if (partsBranch.EntityState == EntityState.New)
                                if (partsBranch.Can同步配件营销信息)
                                    partsBranch.同步配件营销信息();
                            base.OnEditSubmitting();
                        }
                    }, null);
                }
                else
                {
                    ((IEditableObject)partsBranch).EndEdit();
                    if (partsBranch.EntityState == EntityState.New)
                        if (partsBranch.Can同步配件营销信息)
                            partsBranch.同步配件营销信息();
                    base.OnEditSubmitting();
                }
            }, null);
        }

        protected override void Reset() {
            var partsBranch = this.DataContext as PartsBranch;
            //手动清除上次添加到DomainContext中的数据
            //if(partsBranch != null && this.DomainContext.PartsBranches.Contains(partsBranch))
            //    this.DomainContext.PartsBranches.Detach(partsBranch);
            PartsSalesCategoryId = 0;
            this.DataContext = null;
            partsSalesCategory.IsEnabled = true;
            ptbSparePart.IsEnabled = true;
            this.partsSalesCategory.SelectedIndex = 0;//默认品牌
        }
        protected override bool OnRequestCanSubmit()
        {
            this.partsSalesCategory.SelectedIndex = 0;//默认品牌
            return true;
        }

        private void DcsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var partsBranch = this.DataContext as PartsBranch;
            if(partsBranch == null)
                return;
            //if(PartsSalesCategoryId != partsBranch.PartsSalesCategoryId && PartsSalesCategoryId != 0)
            //    DcsUtils.Confirm(CommonUIStrings.DataEditView_Confirm_PartsSalesCategoryId, () => {
            //        if(this.DomainContext.PartsBranches.Contains(partsBranch))
            //            this.DomainContext.PartsBranches.Detach(partsBranch);
            //        this.DataContext = null;
            //        partsBranch.PartsSalesCategoryId = PartsSalesCategoryId;
            //        partsBranch.PartsSalesCategoryName = PartsSalesCategoryName;
            //    }, () => {
            //        //不清空数据
            //        PartsSalesCategoryId = partsBranch.PartsSalesCategoryId;
            //    });
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsBranchesRelationWithPartssalescategoryQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && r.Id == id && r.PartsSalesCategory != null), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                {
                    if (!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if (entity != null)
                {
                    this.SetObjectToEdit(entity);
                    var partsBranch = this.DataContext as PartsBranch;
                    if (partsBranch != null)
                    {
                        PartsSalesCategoryId = partsBranch.PartsSalesCategoryId;
                        PartsSalesCategoryName = partsBranch.PartsSalesCategoryName;
                        partsSalesCategory.IsEnabled = false;
                        ptbSparePart.IsEnabled = false;
                    }
                }
            }, null);
        }
    }
}
