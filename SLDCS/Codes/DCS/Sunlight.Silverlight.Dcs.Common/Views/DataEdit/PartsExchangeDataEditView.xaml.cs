﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class PartsExchangeDataEditView {
        public event EventHandler CustomEditSubmitted;
        public int status = 0;

        private ObservableCollection<PartsExchange> oldPartsExchanges;
        public ObservableCollection<PartsExchange> OldPartsExchanges {
            get {
                return this.oldPartsExchanges ?? (this.oldPartsExchanges = new ObservableCollection<PartsExchange>());
            }
        }

        private ObservableCollection<PartsExchange> partsExchanges;

        public ObservableCollection<PartsExchange> PartsExchanges {
            get {
                return this.partsExchanges ?? (this.partsExchanges = new ObservableCollection<PartsExchange>());
            }
        }

        public PartsExchangeDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.Loaded += PartsExchangeDataEditView_Loaded;
        }

        private void PartsExchangeDataEditView_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            if(status == 0) {
                this.textBoxExchangeCode.Text = string.Empty;
            }
            PartsExchanges.Clear();
        }

        private void CreateUI() {
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(CommonUIStrings.DataGridView_Title_SparePart, null, () => this.SparePartForPartsExchangeEditDataGridView);
            detailDataEditView.SetValue(Grid.RowProperty, 1);
            detailDataEditView.SetValue(Grid.ColumnProperty, 0);
            this.LayoutRoot.Children.Add(detailDataEditView);
        }

        private DataGridViewBase sparePartForPartsExchangeEditDataGridView;

        private DataGridViewBase SparePartForPartsExchangeEditDataGridView {
            get {
                if(this.sparePartForPartsExchangeEditDataGridView == null) {
                    this.sparePartForPartsExchangeEditDataGridView = DI.GetDataGridView("SparePartForPartsExchangeEdit");
                    this.sparePartForPartsExchangeEditDataGridView.DomainContext = this.DomainContext;
                    this.sparePartForPartsExchangeEditDataGridView.DataContext = this;
                }
                return this.sparePartForPartsExchangeEditDataGridView;
            }
        }


        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsExchangesQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.textBoxExchangeCode.Text = entity.ExchangeCode;
                }
                this.textBoxExchangeCode.IsReadOnly = true;
                status = 1;
                Title = CommonUIStrings.DataEditView_Title_UpdateSparepartReplace;
                if(entity != null) {
                    this.DomainContext.Load(this.DomainContext.查询配件互换信息Query(entity.PartId).Where(e => e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadop => {
                        if(loadOp.HasError)
                            return;
                        this.PartsExchanges.Clear();
                        this.OldPartsExchanges.Clear();
                        var entities = loadop.Entities.ToList();
                        for(int i = 0; i < entities.Count; i++) {
                            entities[i].SequeueNumber = i + 1;
                            this.PartsExchanges.Add(entities[i]);
                            this.OldPartsExchanges.Add(entities[i]);
                        }
                    }, null);
                }
            }, null);
        }

        protected override string Title {
            get {
                return this.status == 0 ? CommonUIStrings.DataEditView_Title_AddSparepartReplace : CommonUIStrings.DataEditView_Title_UpdateSparepartReplace;
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {
            if(!this.SparePartForPartsExchangeEditDataGridView.CommitEdit())
                return;
            if(this.PartsExchanges.FirstOrDefault() == null) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_PartsExchange_PartsExchangesIsNull);
                return;
            }
            if(this.PartsExchanges.GroupBy(r => r.PartId).Any(r => r.Count() > 1)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_ValidationPartsExchanges_SameSparePart);
                return;
            }
            var param = this.PartsExchanges.ToDictionary(item => item.PartId, item => item.Remark);
            //var partIds = this.PartsExchanges.Select(p => p.PartId).ToArray();
            if(status == 0) {
                try {
                    this.DomainContext.生成配件互换信息(param, invokeOp => {
                        if(invokeOp.HasError) {
                            if(!invokeOp.IsErrorHandled)
                                invokeOp.MarkErrorAsHandled();
                            var error = invokeOp.ValidationErrors.FirstOrDefault();
                            if(error != null) {
                                UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage, 5);
                            } else {
                                DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                            }
                            DomainContext.RejectChanges();
                            return;
                        }
                        this.NotifyEditSubmitted();
                    }, null);
                } catch(ValidationException ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                    return;
                }
            } else if(status == 1) {
                try {
                    this.DomainContext.调整配件互换信息(param, this.textBoxExchangeCode.Text, invokeOp => {
                        if(invokeOp.HasError) {
                            if(!invokeOp.IsErrorHandled)
                                invokeOp.MarkErrorAsHandled();
                            var error = invokeOp.ValidationErrors.FirstOrDefault();
                            if(error != null) {
                                UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage, 5);
                            } else {
                                DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                            }
                            DomainContext.RejectChanges();
                            return;
                        }
                        this.NotifyEditSubmitted();

                    }, null);
                } catch(ValidationException ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                    return;
                }
            }
        }

        private void NotifyEditSubmitted() {
            var handler = this.CustomEditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        protected override void OnEditCancelled() {
            if(this.PartsExchanges.Any())
                this.PartsExchanges.Clear();
            this.textBoxExchangeCode.Text = string.Empty;
            Title = CommonUIStrings.DataEditView_Title_AddSparepartReplace;
            base.OnEditCancelled();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

    }
}
