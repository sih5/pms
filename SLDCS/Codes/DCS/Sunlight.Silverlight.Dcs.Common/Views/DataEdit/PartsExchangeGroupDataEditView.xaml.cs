﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.ServiceModel.DomainServices.Client;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class PartsExchangeGroupDataEditView {
        private DataGridViewBase partsExchangeGroupForPanelDataGridView;
        List<PartsExchangeGroup> partsExchangeGroups = new List<PartsExchangeGroup>();
        private ObservableCollection<PartsExchangeGroup> partsExchangeGroupDetails; 
        private RadUpload uploader;
        protected Action<string> UploadFileSuccessedProcessing;
        private string strFileName;
        private const string EXPORT_DATA_FILE_NAME = "互换号清单模板.xlsx";
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        protected string ErrorFileName {
            get;
            set;
        }
        public ObservableCollection<PartsExchangeGroup> PartsExchangeGroupDetails {
            get {
                return this.partsExchangeGroupDetails ?? (this.partsExchangeGroupDetails = new ObservableCollection<PartsExchangeGroup>());
            }
        }
        private DataGridViewBase PartsExchangeGroupForPanelDataGridView {
            get {
                if(this.partsExchangeGroupForPanelDataGridView == null) {
                    this.partsExchangeGroupForPanelDataGridView = DI.GetDataGridView("PartsExchangeGroupForPanel");
                    this.partsExchangeGroupForPanelDataGridView.DomainContext = this.DomainContext;
                    this.partsExchangeGroupForPanelDataGridView.DataContext = this;
                }
                return this.partsExchangeGroupForPanelDataGridView;
            }
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsExchangeGroupsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                PartsExchangeGroupDetails.Clear();
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity != null) {
                    this.textBoxExchangeCode.IsReadOnly = false;
                    this.textBoxExchangeCode.Text = entity.ExGroupCode;
                    this.textBoxExchangeCode.IsReadOnly = true;
                    this.DomainContext.Load(this.DomainContext.GetPartsExchangeGroupsQuery().Where(e => e.ExGroupCode == entity.ExGroupCode && e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp1 => {
                        if(loadOp1.HasError) {
                            if(!loadOp1.IsErrorHandled)
                                loadOp1.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp1);
                            return;
                        }
                        foreach(var item in loadOp1.Entities) {
                            PartsExchangeGroupDetails.Add(item);
                        }
                    }, null);
                    //this.SetObjectToEdit(entity);
                }
            }, null);
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private void CreateUI() {
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(CommonUIStrings.DataGridView_Title_SparePart, null, () => this.PartsExchangeGroupForPanelDataGridView);

            detailDataEditView.RegisterButton(new ButtonItem {
                Title = "导入",
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                Command = new Sunlight.Silverlight.Core.Command.DelegateCommand(() => this.Uploader.ShowFileDialog())
            });
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = "导出模板",
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                Command = new Sunlight.Silverlight.Core.Command.DelegateCommand(() => {
                    var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_PartsExchangeGroup_ExchangeCode,
                                                IsRequired = true
                                            }

                                        };
                    this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                        if(loadOp.HasError) {
                            if(!loadOp.IsErrorHandled)
                                loadOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                        }
                        if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                            this.ExportFile(loadOp.Value);
                    }, null);
                })
            });
            detailDataEditView.SetValue(Grid.RowProperty, 1);
            detailDataEditView.SetValue(Grid.ColumnProperty, 0);
            this.Root.Children.Add(detailDataEditView);
        }
        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }
        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage(CommonUIStrings.DataEditPanel_Validation_Notification_File);
                    e.Handled = true;
                }
            }
        }
        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        ShellViewModel.Current.IsBusy = true;
                        if(e != null)
                            this.excelServiceClient.ImportPartsExchangeGroupAsync(e.HandlerData.CustomData["Path"].ToString());
                        this.excelServiceClient.ImportPartsExchangeGroupCompleted -= excelServiceClient_ImportPartsExchangeGroupCompleted;
                        this.excelServiceClient.ImportPartsExchangeGroupCompleted += excelServiceClient_ImportPartsExchangeGroupCompleted;
                    };
                }
                return this.uploader;
            }
        }
        void excelServiceClient_ImportPartsExchangeGroupCompleted(object sender, ImportPartsExchangeGroupCompletedEventArgs e) {
            //foreach(var detail in PartsExchangeGroupDetails) {
            //    PartsExchangeGroupDetails.Remove(detail);
            //}
            if(e.rightData != null && e.rightData.Length > 0) {
                foreach(var data in e.rightData) {
                    PartsExchangeGroupDetails.Add(new PartsExchangeGroup {
                        ExchangeCode = data.ExchangeCode,
                        ExGroupCode = this.textBoxExchangeCode.Text
                    });
                }
            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowAlertMessage(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
        }
        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }
        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            this.ErrorFileName = errorFileName;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(ErrorFileName));
        }
        protected override void OnEditSubmitting() {
            partsExchangeGroups = PartsExchangeGroupDetails.ToList();
            if(!this.PartsExchangeGroupForPanelDataGridView.CommitEdit())
                return;
            if(this.PartsExchangeGroupDetails.FirstOrDefault() == null) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_PartsExchange_PartsExchangesIsNull);
                return;
            }
            if(this.PartsExchangeGroupDetails.GroupBy(r => r.ExchangeCode).Any(r => r.Count() > 1)) {
                UIHelper.ShowNotification("存在相同互换识别号,请检查");
                return;
            }
            this.textBoxExchangeCode.IsReadOnly = true;
            this.DomainContext.新增修改配件互换组信息(partsExchangeGroups, invokeOp => {
                if(invokeOp.HasError) {
                    if(!invokeOp.IsErrorHandled)
                        invokeOp.MarkErrorAsHandled();
                    var error = invokeOp.ValidationErrors.FirstOrDefault();
                    if(error != null) {
                        UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage, 5);
                    } else {
                        DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                    }
                    DomainContext.RejectChanges();
                    return;
                } else {
                    UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_PartsPurchasePricingChange_Success);
                }
                this.DomainContext.RejectChanges();
                base.OnEditSubmitting();
            }, null);
        }

        protected override void Reset() {
            base.Reset();
            this.PartsExchangeGroupDetails.Clear();
        }
        protected override bool OnRequestCanSubmit() {
            return true;
        }
        protected override void OnEditCancelled() {
            if(this.PartsExchangeGroupDetails.Any())
                this.PartsExchangeGroupDetails.Clear();
            this.textBoxExchangeCode.Text = string.Empty;
            //Title = CommonUIStrings.DataEditView_Title_AddSparepartReplace;
            base.OnEditCancelled();
        }
        protected override string BusinessName {
            get {
                return "配件互换管理";
            }
        }
        public PartsExchangeGroupDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
    }
}
