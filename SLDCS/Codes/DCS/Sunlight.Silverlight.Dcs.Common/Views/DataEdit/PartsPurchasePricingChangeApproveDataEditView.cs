﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public class PartsPurchasePricingChangeApproveDataEditView : PartsPurchasePricingChangeDataEditView {
        private DataGridViewBase approveDataGridView;

        public override bool IsReadOnly {
            get {
                return true;
            }
        }

        protected override string Title {
            get {
                return CommonUIStrings.DataEditView_Title_Approve_PartsPurchasePricingChange;
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {
            var partsPurchasePricingChange = this.DataContext as PartsPurchasePricingChange;
            if(partsPurchasePricingChange == null)
                return;
            ((IEditableObject)partsPurchasePricingChange).EndEdit();
            try {
                if(partsPurchasePricingChange.Can审核配件采购价格变更申请单)
                    partsPurchasePricingChange.审核配件采购价格变更申请单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        protected override void CreateUI() {
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsPurchasePricingChange), "PartsPurchasePricingDetails"), null, () => this.ApproveDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);
        }

        private DataGridViewBase ApproveDataGridView {
            get {
                if(this.approveDataGridView == null) {
                    this.approveDataGridView = DI.GetDataGridView("PartsPurchasePricingDetailForApprove");
                    this.approveDataGridView.DomainContext = this.DomainContext;
                }
                return this.approveDataGridView;
            }
        }
    }
}
