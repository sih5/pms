﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class PartsPurchasePricingChangeDataEditView {
        //海外品牌数组"海外乘用车", "海外中重卡", "海外轻卡",
        private string[] OverseasPartsSalesCategory = new[] { "海外国际" };
        private DataGridViewBase partsPurchasePricingDetailForEditDataGridView;
        private ObservableCollection<KeyValuePair> kvPartsSalesCategoryNames;
        public new event EventHandler EditSubmitted;
        private ICommand exportFileCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出配件采购价格变更申请单清单模板.xlsx";
        private string strFileName;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private RadUpload uploader;
        private int idNumber = -32768;
        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        var partsPurchasePricingChange = this.DataContext as PartsPurchasePricingChange;
                        if(partsPurchasePricingChange == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.ImpPartsPurchasePricingDetailAsync(e.HandlerData.CustomData["Path"].ToString(), partsPurchasePricingChange.PartsSalesCategoryId, partsPurchasePricingChange.BranchId);
                        this.excelServiceClient.ImpPartsPurchasePricingDetailCompleted -= ExcelServiceClient_ImpPartsPurchasePricingDetailCompleted;
                        this.excelServiceClient.ImpPartsPurchasePricingDetailCompleted += ExcelServiceClient_ImpPartsPurchasePricingDetailCompleted;
                        //this.excelServiceClient.ImportPartsSalesOrderDetailAsync(e.HandlerData.CustomData["Path"].ToString(), partsSalesOrder.IfDirectProvision, partsSalesOrder.BranchId, partsSalesOrder.SalesCategoryId);
                        //this.excelServiceClient.ImportPartsSalesOrderDetailCompleted -= this.ExcelServiceClient_ImportPartsSalesOrderDetailCompleted;
                        //this.excelServiceClient.ImportPartsSalesOrderDetailCompleted += this.ExcelServiceClient_ImportPartsSalesOrderDetailCompleted;
                    };
                }
                return this.uploader;
            }
        }

        /// <summary>
        /// 文件上传成功后，调用该方法进行上传后的处理。不支持异步调用
        /// <param>上传文件名 <name>fileName</name> </param>
        /// </summary>
        protected Action<string> UploadFileSuccessedProcessing;

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //取当前登录用户的HashCode来标记上传文件的唯一性
                //this.UploadFileSuccessedProcessing.Invoke(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }

        private void ExcelServiceClient_ImpPartsPurchasePricingDetailCompleted(object sender, ImpPartsPurchasePricingDetailCompletedEventArgs e) {
            var partsPurchasePricingChange = this.DataContext as PartsPurchasePricingChange;
            if(partsPurchasePricingChange == null)
                return;
            foreach(var detail in partsPurchasePricingChange.PartsPurchasePricingDetails) {
                partsPurchasePricingChange.PartsPurchasePricingDetails.Remove(detail);
            }
            partsPurchasePricingChange.PartsPurchasePricingDetailOrigs.Clear();
            if(e.rightData != null) {
                foreach(var data in e.rightData) {
                    partsPurchasePricingChange.PartsPurchasePricingDetails.Add(new PartsPurchasePricingDetail {
                        Id = idNumber++,
                        PartId = data.PartId,
                        PartCode = data.PartCode,
                        PartName = data.PartName,
                        SupplierPartCode = data.SupplierPartCode,
                        SupplierId = data.SupplierId,
                        SupplierCode = data.SupplierCode,
                        SupplierName = data.SupplierName,
                        PriceType = data.PriceType,
                        Price = decimal.Round(data.Price, 2),
                        LimitQty = data.LimitQty,
                        IsPrimary = data.IsPrimary,
                        Remark = data.Remark,
                        ValidFrom = data.ValidFrom,
                        ValidTo = data.ValidTo,
                        ReferencePrice = data.ReferencePrice,
                        MinPriceSupplierId = data.MinPriceSupplierId,
                        MinPriceSupplier = data.MinPriceSupplier,
                        MinPurchasePrice = data.MinPurchasePrice,
                        IsPrimaryMinPrice = data.IsPrimaryMinPrice,
                        IsNewPart = data.IsNewPart,
                        IfPurchasable = data.IsOrderable,
                        OldPriSupplierId = data.OldPriSupplierId,
                        OldPriSupplierCode = data.OldPriSupplierCode,
                        OldPriSupplierName = data.OldPriSupplierName,
                        OldPriPrice = data.OldPriPrice,
                        OldPriPriceType = data.OldPriPriceType
                    });
                }
                foreach(var detail in partsPurchasePricingChange.PartsPurchasePricingDetails){
                    partsPurchasePricingChange.PartsPurchasePricingDetailOrigs.Add(detail);
                }
            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowAlertMessage(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
        }

        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage(CommonUIStrings.DataEditPanel_Validation_Notification_File);
                    e.Handled = true;
                }
            }
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }

        public ObservableCollection<KeyValuePair> KvPartsSalesCategoryNames {
            get {
                return this.kvPartsSalesCategoryNames ?? (this.kvPartsSalesCategoryNames = new ObservableCollection<KeyValuePair>());
            }
        }

        public PartsPurchasePricingChangeDataEditView() {
            if(this.DomainContext == null) {
                this.DomainContext = new DcsDomainContext();
            }
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged -= this.PartsPurchasePricingChangeDataEditView_DataContextChanged;
            this.DataContextChanged += this.PartsPurchasePricingChangeDataEditView_DataContextChanged;
        }

        private void PartsPurchasePricingChangeDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsPurchasePricingChange = this.DataContext as PartsPurchasePricingChange;
            if(partsPurchasePricingChange == null)
                return;
            if (partsPurchasePricingChange.PartsSalesCategoryId == default(int)) {
                if (cbxPartsSalesCategory.ItemsSource!=null && cbxPartsSalesCategory.ItemsSource.Cast<PartsSalesCategory>().Any()){
                    var selected = cbxPartsSalesCategory.ItemsSource.Cast<PartsSalesCategory>().FirstOrDefault();
                    partsPurchasePricingChange.PartsSalesCategoryId = selected.Id;
                    partsPurchasePricingChange.PartsSalesCategoryName = selected.Name;
                }
            }

            partsPurchasePricingChange.PartsPurchasePricingDetails.EntityAdded -= this.PurchasePricingDetails_EntityAdded;
            partsPurchasePricingChange.PartsPurchasePricingDetails.EntityAdded += this.PurchasePricingDetails_EntityAdded;
            partsPurchasePricingChange.PropertyChanged -=partsPurchasePricingChange_PropertyChanged;
            partsPurchasePricingChange.PropertyChanged +=partsPurchasePricingChange_PropertyChanged;
        }

        private void PurchasePricingDetails_EntityAdded(object sender, EntityCollectionChangedEventArgs<PartsPurchasePricingDetail> e) {
            e.Entity.PropertyChanged -= this.PartsPurchasePricingDetail_PropertyChanged;
            e.Entity.PropertyChanged += this.PartsPurchasePricingDetail_PropertyChanged;
        }

        private void PartsPurchasePricingDetail_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsPurchasePricingChange = this.DataContext as PartsPurchasePricingChange;
            if(partsPurchasePricingChange == null)
                return;

            switch(e.PropertyName) {
                case "PartsSalesCategoryId":
                    var details = partsPurchasePricingChange.PartsPurchasePricingDetails.ToList();
                    foreach(var detail in details)
                        partsPurchasePricingChange.PartsPurchasePricingDetails.Remove(detail);
                    this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesWithBranchIdQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError)
                            return;
                        cbxPartsSalesCategory.ItemsSource = loadOp.Entities;
                        cbxPartsSalesCategory.SelectedItem = loadOp.Entities.FirstOrDefault();
                    }, null);
                    break;
            }

            var partsPurchasePricingDetail = sender as PartsPurchasePricingDetail;
            if(partsPurchasePricingDetail == null)
                return;
            if (e.PropertyName == "Price") { 
                partsPurchasePricingDetail.Price = decimal.Round(partsPurchasePricingDetail.Price, 2);
                if (partsPurchasePricingDetail.ReferencePrice != null && partsPurchasePricingDetail.ReferencePrice != 0)
                    partsPurchasePricingDetail.PriceRate = (Math.Round((partsPurchasePricingDetail.Price) / Convert.ToDecimal(partsPurchasePricingDetail.ReferencePrice), 2) * 100) + "%";
                    //查询最低采购价，最低价供应商，最低价是否首选
                    if (partsPurchasePricingDetail.Price > 0m) { 
                        var domainContext = this.DomainContext as DcsDomainContext;
                        if(domainContext == null)
                            return;
                        domainContext.Load(domainContext.fullOthersForPartsPurchasePricingDetailQuery(partsPurchasePricingChange.PartsSalesCategoryId,partsPurchasePricingDetail.PartId, partsPurchasePricingDetail.SupplierId,partsPurchasePricingDetail.Price), LoadBehavior.RefreshCurrent, loadOp => {
                            if(!loadOp.HasError) {
                                if(loadOp.Entities != null && loadOp.Entities.Any()) {
                                    var entity = loadOp.Entities.First();
                                    partsPurchasePricingDetail.MinPriceSupplier = entity.MinPriceSupplier;
                                    partsPurchasePricingDetail.MinPriceSupplierId = entity.MinPriceSupplierId;
                                    partsPurchasePricingDetail.MinPurchasePrice = entity.MinPurchasePrice;
                                    partsPurchasePricingDetail.IsPrimaryMinPrice = entity.IsPrimaryMinPrice;
                                    partsPurchasePricingDetail.OldPriSupplierId = entity.OldPriSupplierId;
                                    partsPurchasePricingDetail.OldPriSupplierCode = entity.OldPriSupplierCode;
                                    partsPurchasePricingDetail.OldPriSupplierName = entity.OldPriSupplierName;
                                    partsPurchasePricingDetail.OldPriPrice = entity.OldPriPrice;
                                    partsPurchasePricingDetail.OldPriPriceType = entity.OldPriPriceType;
                                } 
                            }
                        }, null);
                    }
            }
            //TODO
            /*将DataGridViewForEdit 类中GridView_AddingNewDataItem事件或GridView_Deleting事件替换为调用函数通过Entity.PropertyChanged执行
             * 原因DataGridViewForEdit提交时如果有必填字段验证非空，取消操作调用this.DomainContext.RejectChanges()失败
            */
            if(e.PropertyName != "PartCode")
                return;

            partsPurchasePricingDetail.Price = default(int);
            partsPurchasePricingDetail.ReferencePrice = default(int);
            partsPurchasePricingDetail.PriceRate = "";
            partsPurchasePricingDetail.ValidFrom = DateTime.Now.Date;
            partsPurchasePricingDetail.ValidTo = DateTime.Now.Date.AddDays(1);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public virtual bool IsReadOnly {
            get {
                return false;
            }
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsPurchasePricingChange;
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }
        protected virtual void CreateUI() {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesWithBranchIdQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                cbxPartsSalesCategory.ItemsSource = loadOp.Entities;
                cbxPartsSalesCategory.SelectedItem = loadOp.Entities.FirstOrDefault();
            }, null);
            //this.DataContextChanged -= PartsPurchasePricingChangeInitialApproveDataEditView_DataContextChanged;
            //this.DataContextChanged += PartsPurchasePricingChangeInitialApproveDataEditView_DataContextChanged;

            this.Root.Children.Add(this.CreateVerticalLine(1));
            FileUploadDataEditPanels.SetValue(Grid.ColumnProperty, 2);
            FileUploadDataEditPanels.FilePath = null;
            this.Attachment.Children.Add(FileUploadDataEditPanels);
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = CommonUIStrings.Action_Title_Import,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                Command = new DelegateCommand(this.ShowFileDialog)
            });
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = CommonUIStrings.DataEditView_Title_ExportButton,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                Command = this.ExportTemplateCommand
            });
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsPurchasePricingChange), "PartsPurchasePricingDetails"), null, () => this.PartsPurchasePricingDetailForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);

        }
        private FileUploadDataEditPanel fileUploadDataEditPanels;
        public FileUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (FileUploadDataEditPanel)DI.GetDataEditPanel("FileUpload"));
            }
        }
        private void ShowFileDialog() {
            var partsPurchasePricingChange = this.DataContext as PartsPurchasePricingChange;
            if(partsPurchasePricingChange == null)
                return;
            if (partsPurchasePricingChange.Code != GlobalVar.ASSIGNED_BY_SERVER && partsPurchasePricingChange.Code.IndexOf("SAP") >= 0) { 
                return;
            }
            if(partsPurchasePricingChange.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_BranchSupplier_BranchIsNull);
                return;
            }
            if (partsPurchasePricingChange.IsNewPart == true || partsPurchasePricingChange.IsPrimaryNotMinPurchasePrice == true || partsPurchasePricingChange.NotPrimaryIsMinPurchasePrice == true ||
                partsPurchasePricingChange.IsPrimaryIsMinPurchasePrice == true || partsPurchasePricingChange.NotPrimaryNotMinPurchasePrice == true) { 
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_PartsPurchasePricingChange_Left);
                return;
            }
            this.Uploader.ShowFileDialog();
        }

        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_SparePartCode,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_SparePartName
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierCode,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierName,
                                            },
                                            new ImportTemplateColumn {
                                                Name =  CommonUIStrings.DataGridView_ColumnItem_Title_PartsSupplierRelation_SupplierPartCode,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_IsZg,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_PurchasePrice,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_ClosedQuantity,
                                                IsRequired = false
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_IsPrimaryNew,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_VirtualPartsBranch_IsOrderable,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_ValidTo,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_ValidFrom,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartsInboundCheckBillRemark,
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }

        protected override void OnEditSubmitting() {
            var partsPurchasePricingChange = this.DataContext as PartsPurchasePricingChange;
            if(partsPurchasePricingChange == null || !this.PartsPurchasePricingDetailForEditDataGridView.CommitEdit())
                return;
            if(partsPurchasePricingChange.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_BranchSupplier_BranchIsNull);
                return;
            }

            foreach (var d in partsPurchasePricingChange.PartsPurchasePricingDetails) {
                partsPurchasePricingChange.PartsPurchasePricingDetails.Remove(d);
            }
            foreach (var d in partsPurchasePricingChange.PartsPurchasePricingDetailOrigs) {
                partsPurchasePricingChange.PartsPurchasePricingDetails.Add(d);
            }

            partsPurchasePricingChange.ValidationErrors.Clear();
            var validationErrors = new List<string>();
            if(!partsPurchasePricingChange.PartsPurchasePricingDetails.Any()) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_PartsPurchasePricingChange_PartsPurchasePricingDetailsIsEmpty);
                return;
            }
            //校验配件和供应商 不能重复
            if(partsPurchasePricingChange.PartsPurchasePricingDetails.GroupBy(x => new {
                x.PartId,
                x.SupplierId
            }).Where(x => x.Count() > 1).Any(sum => sum.Any())) {
                UIHelper.ShowAlertMessage(CommonUIStrings.DataEditView_Validation_PartsPurchasePricingChange_PartIdIsRepeat);
                return;
            }
            //校验同一个配件，最多只能一个首选
            if(partsPurchasePricingChange.PartsPurchasePricingDetails.Where(r => r.IsPrimary == true).GroupBy(x => new {
                x.PartId
            }).Where(x => x.Count() > 1).Any(sum => sum.Any())) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_PartsPurchasePricingChange_SameSparepart);
                return;
            }

            foreach(var partsPurchasePricingDetail in partsPurchasePricingChange.PartsPurchasePricingDetails) {
                if(partsPurchasePricingDetail.LimitQty == null) {
                    partsPurchasePricingDetail.LimitQty = 0;
                }
                if(partsPurchasePricingDetail.IsPrimary == null) {
                    partsPurchasePricingDetail.IsPrimary = false;
                }
                if(partsPurchasePricingDetail.IfPurchasable == null) {
                    partsPurchasePricingDetail.IfPurchasable = false;
                }
                if (partsPurchasePricingChange.Code.IndexOf("SAP") < 0) {
                    partsPurchasePricingDetail.DataSource = (int)DcsPurchasePricingDataSource.手工维护;
                }
                if (string.IsNullOrEmpty(partsPurchasePricingDetail.SupplierPartCode)) {
                    UIHelper.ShowNotification(string.Format(CommonUIStrings.DataEditView_Validation_PartsPurchasePricingChange_SupplierPartCode, partsPurchasePricingDetail.PartCode));
                    return;
                }
            }

            foreach(var partsPurchasePricingDetail in partsPurchasePricingChange.PartsPurchasePricingDetails) {
                //失效事件保留 23小时59分59秒
                partsPurchasePricingDetail.ValidTo = new DateTime(partsPurchasePricingDetail.ValidTo.Year, partsPurchasePricingDetail.ValidTo.Month, partsPurchasePricingDetail.ValidTo.Day, 23, 59, 59);
                partsPurchasePricingDetail.ValidationErrors.Clear();
            }
            var invalidDetails = partsPurchasePricingChange.PartsPurchasePricingDetails.Where(detail => detail.ValidFrom > detail.ValidTo);
            var invalidDetailsPrice = partsPurchasePricingChange.PartsPurchasePricingDetails.Where(detail => detail.Price <= 0);
            foreach(var detail in invalidDetails)
                detail.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_PartsPurchasePricingChange_ValidToEarlierThanValidFrom, new[] {
                    "ValidTo"
                }));
            foreach(var detail in invalidDetailsPrice)
                detail.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_PartsPurchasePricingChange_PriceIsLessThanOne, new[] {
                    "Price"
                }));
            if(partsPurchasePricingChange.PartsPurchasePricingDetails.Any(detail => detail.HasValidationErrors))
                return;
            if(validationErrors.Any()) {
                UIHelper.ShowNotification(string.Join(Environment.NewLine, validationErrors));
                return;
            }
            //var spIds = partsPurchasePricingChange.PartsPurchasePricingDetails.Select(t => t.PartId).Distinct().ToArray();
            //this.DomainContext.Load(this.DomainContext.GetPartsPurchasePricingWithSpIdssQuery(spIds), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    var entitys = loadOp.Entities.ToArray();
            //    if(entitys.Count()>0) {
            //        foreach(var item in partsPurchasePricingChange.PartsPurchasePricingDetails) {
            //            if(entitys.Any(t => t.PartId == item.PartId && t.PartsSupplierId == item.SupplierId && t.PriceType == item.PriceType && t.PurchasePrice == item.Price && t.ValidFrom == t.ValidFrom && t.ValidTo == item.ValidTo)) {
            //                UIHelper.ShowNotification("配件" + item.PartCode + "已存在相同供应商，价格，价格类型，生效时间",5);
            //                return;
            //            }
            //        }
            //    }
               
            //}, null);
            save();
           
        }

        private void save() {
            var partsPurchasePricingChange = this.DataContext as PartsPurchasePricingChange;
            if(partsPurchasePricingChange == null || !this.PartsPurchasePricingDetailForEditDataGridView.CommitEdit())
                return;
            partsPurchasePricingChange.Path = FileUploadDataEditPanels.FilePath;
            ((IEditableObject)partsPurchasePricingChange).EndEdit();
            this.DomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    this.DomainContext.RejectChanges();
                    return;
                }
                this.NotifyEditSubmitted();
                this.OnCustomEditSubmitted();
            }, null);
        }

        private void NotifyEditSubmitted() {
            var handler = this.EditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        public void OnCustomEditSubmitted() {
            this.DataContext = null;
        }

        private DataGridViewBase PartsPurchasePricingDetailForEditDataGridView {
            get {
                if(this.partsPurchasePricingDetailForEditDataGridView == null) {
                    this.partsPurchasePricingDetailForEditDataGridView = DI.GetDataGridView("PartsPurchasePricingDetailForEdit");
                    this.partsPurchasePricingDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsPurchasePricingDetailForEditDataGridView;
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsPurchasePricingChangeWithOthersQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.FileUploadDataEditPanels.FilePath = entity.Path;
                    entity.PartsPurchasePricingDetailOrigs.Clear();
                    foreach (var partsPurchasePricingDetail in entity.PartsPurchasePricingDetails) {
                        entity.PartsPurchasePricingDetailOrigs.Add(partsPurchasePricingDetail);
                        if (partsPurchasePricingDetail.IsPrimary == null) { 
                            partsPurchasePricingDetail.IsPrimary = false;
                        }
                    }
                    entity.IsNewPart = false;
                    entity.IsPrimaryIsMinPurchasePrice = false;
                    entity.IsPrimaryNotMinPurchasePrice = false;
                    entity.NotPrimaryIsMinPurchasePrice = false;
                    entity.NotPrimaryNotMinPurchasePrice = false;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        private void CbxPartsSalesCategory_OnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            var partsPurchasePricingChange = this.DataContext as PartsPurchasePricingChange;
            if(partsPurchasePricingChange == null)
                return;
            //修改页面不用清理清单
            if(partsPurchasePricingChange.Code != null && !partsPurchasePricingChange.Code.Equals(GlobalVar.ASSIGNED_BY_SERVER))
                return;
            var details = partsPurchasePricingChange.PartsPurchasePricingDetails.ToList();
            foreach(var detail in details)
                partsPurchasePricingChange.PartsPurchasePricingDetails.Remove(detail);

        }

        private void PartsPurchasePricingChangeInitialApproveDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var PartsPurchasePricingChange = this.DataContext as PartsPurchasePricingChange;
            if (PartsPurchasePricingChange == null)
                return;
            PartsPurchasePricingChange.PropertyChanged -= partsPurchasePricingChange_PropertyChanged;
            PartsPurchasePricingChange.PropertyChanged += partsPurchasePricingChange_PropertyChanged;

        }

        private void partsPurchasePricingChange_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var partsPurchasePricingChange = this.DataContext as PartsPurchasePricingChange;
            if (partsPurchasePricingChange == null)
                return;
            switch (e.PropertyName)
            {
                case "IsNewPart":
                case "IsPrimaryNotMinPurchasePrice":
                case "NotPrimaryIsMinPurchasePrice":
                case "IsPrimaryIsMinPurchasePrice":
                case "NotPrimaryNotMinPurchasePrice":
                    var isNewPart = partsPurchasePricingChange.IsNewPart;//是否是新配件
                    var isPrimaryNotMinPurchasePrice = partsPurchasePricingChange.IsPrimaryNotMinPurchasePrice;//是首选供应商、不是最低采购价
                    var notPrimaryIsMinPurchasePrice = partsPurchasePricingChange.NotPrimaryIsMinPurchasePrice;//不是首选供应商、是最低采购价
                    var isPrimaryIsMinPurchasePrice = partsPurchasePricingChange.IsPrimaryIsMinPurchasePrice;//是首选供应商、是最低采购价
                    var notPrimaryNotMinPurchasePrice = partsPurchasePricingChange.NotPrimaryNotMinPurchasePrice;//不是首选供应商、不是最低采购价

                    //先清理掉清单，重新筛选，再添加
                    var details = partsPurchasePricingChange.PartsPurchasePricingDetails.ToList();

                    foreach (var detail in details) {
                        if(partsPurchasePricingChange.PartsPurchasePricingDetails.Contains(detail))
                        partsPurchasePricingChange.PartsPurchasePricingDetails.Remove(detail);
                    }

                    var localDetails = new ObservableCollection<PartsPurchasePricingDetail>();;
                    
                    if (isNewPart??false){
                        var data1 = partsPurchasePricingChange.PartsPurchasePricingDetailOrigs.Where(r => r.IsNewPart == true);
                        foreach (var data in data1)
                        {
                            if (!localDetails.Contains(data))
                            localDetails.Add(data);
                        }
                    }
                    if (isPrimaryNotMinPurchasePrice ?? false)
                    {
                        var data2 = partsPurchasePricingChange.PartsPurchasePricingDetailOrigs.Where(r => r.IsPrimary == true && r.MinPurchasePrice != r.Price && r.IsPrimary != null && r.MinPurchasePrice!=null);
                        foreach (var data in data2)
                        {
                            if (!localDetails.Contains(data))
                            localDetails.Add(data);
                        }
                    }
                    if (notPrimaryIsMinPurchasePrice ?? false)
                    {
                        var data3 = partsPurchasePricingChange.PartsPurchasePricingDetailOrigs.Where(r => r.IsPrimary == false && (r.MinPurchasePrice == r.Price|| r.MinPurchasePrice == null) && r.IsPrimary != null );
                        foreach (var data in data3)
                        {
                            if (!localDetails.Contains(data))
                            localDetails.Add(data);
                        }
                    }
                    if (isPrimaryIsMinPurchasePrice ?? false)
                    {
                        var data4 = partsPurchasePricingChange.PartsPurchasePricingDetailOrigs.Where(r => r.IsPrimary == true && (r.MinPurchasePrice == r.Price || r.MinPurchasePrice == null) && r.IsPrimary != null );
                        foreach (var data in data4)
                        {
                            if (!localDetails.Contains(data))
                            localDetails.Add(data);
                        }
                    }
                    if (notPrimaryNotMinPurchasePrice ?? false)
                    {
                        var data5 = partsPurchasePricingChange.PartsPurchasePricingDetailOrigs.Where(r => r.IsPrimary == false && r.MinPurchasePrice != r.Price && r.IsPrimary != null && r.MinPurchasePrice != null);
                        foreach (var data in data5)
                        {
                            if (!localDetails.Contains(data))
                            localDetails.Add(data);
                        }
                    }

                    if (isNewPart != null && isPrimaryNotMinPurchasePrice!= null && notPrimaryIsMinPurchasePrice!=null && isPrimaryIsMinPurchasePrice!= null && notPrimaryNotMinPurchasePrice!=null
                         &&!(bool)isNewPart && !(bool)isPrimaryNotMinPurchasePrice && !(bool)notPrimaryIsMinPurchasePrice && !(bool)isPrimaryIsMinPurchasePrice && !(bool)notPrimaryNotMinPurchasePrice)
                    {
                        localDetails = partsPurchasePricingChange.PartsPurchasePricingDetailOrigs;
                    }
                    foreach (var orig in localDetails)
                    {
                        partsPurchasePricingChange.PartsPurchasePricingDetails.Add(orig);
                    }

                    break;
            }
        }

    }
}
