﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class PartsPurchasePricingChangeDetailDataEditView {
        private DataGridViewBase partsPurchasePricingDetailForEditDataGridView;

        public PartsPurchasePricingChangeDetailDataEditView() {
            if(this.DomainContext == null) {
                this.DomainContext = new DcsDomainContext();
            }
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public virtual bool IsReadOnly {
            get {
                return false;
            }
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsPurchasePricingChange;
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }
        protected virtual void CreateUI() {

            this.Root.Children.Add(this.CreateVerticalLine(1));
            FileUploadDataEditPanels.SetValue(Grid.ColumnProperty, 2);
            FileUploadDataEditPanels.FilePath = null;
            FileUploadDataEditPanels.isHiddenButtons = true;
            this.Attachment.Children.Add(FileUploadDataEditPanels);
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsPurchasePricingChange), "PartsPurchasePricingDetails"), null, () => this.PartsPurchasePricingDetailForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            //detailDataEditView.IsEnabled = false;
            this.Root.Children.Add(detailDataEditView);

        }
        private FileUploadDataEditPanel fileUploadDataEditPanels;
        public FileUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (FileUploadDataEditPanel)DI.GetDataEditPanel("FileUpload"));
            }
        }
    
        private DataGridViewBase PartsPurchasePricingDetailForEditDataGridView {
            get {
                if(this.partsPurchasePricingDetailForEditDataGridView == null) {
                    this.partsPurchasePricingDetailForEditDataGridView = DI.GetDataGridView("PartsPurchasePricingDetailForEdit");
                    this.partsPurchasePricingDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsPurchasePricingDetailForEditDataGridView;
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsPurchasePricingChangeWithOthersQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.FileUploadDataEditPanels.FilePath = entity.Path;
                    foreach (var partsPurchasePricingDetail in entity.PartsPurchasePricingDetails) {
                        if (partsPurchasePricingDetail.IsPrimary == null) { 
                            partsPurchasePricingDetail.IsPrimary = false;
                        }
                    }
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
    }
}
