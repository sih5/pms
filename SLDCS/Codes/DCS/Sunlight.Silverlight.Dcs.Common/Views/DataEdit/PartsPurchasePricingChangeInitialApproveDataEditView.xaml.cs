﻿using System.ComponentModel;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using System.Windows;
using System.Collections.ObjectModel;
using Sunlight.Silverlight.Dcs.Common.Panels.DataEdit;
using Sunlight.Silverlight.Core.Model;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class PartsPurchasePricingChangeInitialApproveDataEditView
    {
        private DataGridViewBase approveDataGridView;
        private ObservableCollection<PartsPurchasePricingDetail> partsPurchasePricingDetails = new ObservableCollection<PartsPurchasePricingDetail>();
        private ObservableCollection<PartsPurchasePricingDetail> partsPurchasePricingDetailOrigs = new ObservableCollection<PartsPurchasePricingDetail>();

        public PartsPurchasePricingChangeInitialApproveDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }
       
        protected override void OnEditSubmitting()
        {
            var partsPurchasePricingChange = this.DataContext as PartsPurchasePricingChange;
            if (partsPurchasePricingChange == null)
                return;
            var approvalId = partsPurchasePricingChange.ApprovalId ?? 1;
            partsPurchasePricingChange.ApprovalId = approvalId;
            if (approvalId == 0 && string.IsNullOrEmpty(partsPurchasePricingChange.ApproveMemo))
            {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_PartsPurchasePricingChange_ApprovalCommentIsnull);
                return;
            }
            FillApproveMemo(partsPurchasePricingChange);

            partsPurchasePricingChange.Path = FileUploadDataEditPanels.FilePath;

            partsPurchasePricingChange.ValidationErrors.Clear();
            foreach (var partsPurchasePricingDetail in partsPurchasePricingChange.PartsPurchasePricingDetails)
            {
                partsPurchasePricingDetail.ValidationErrors.Clear();
            }
            if (partsPurchasePricingChange.ValidationErrors.Any())
                return;

            ((IEditableObject)partsPurchasePricingChange).EndEdit();
            try {
                if (partsPurchasePricingChange.Status == (int)DcsPartsPurchasePricingChangeStatus.提交) {
                    if (partsPurchasePricingChange.Can初审配件采购价格变更申请单)
                        partsPurchasePricingChange.初审配件采购价格变更申请单();
                } else if (partsPurchasePricingChange.Status == (int)DcsPartsPurchasePricingChangeStatus.初审通过) {
                    if (partsPurchasePricingChange.Can审核配件采购价格变更申请单New)
                        partsPurchasePricingChange.审核配件采购价格变更申请单New();
                } else {
                    UIHelper.ShowNotification("单据状态错误");
                    return;
                }
                ExecuteSerivcesMethod(CommonUIStrings.DataEditView_Validation_PartsPurchasePricingChange_Success);
            } catch (ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            //base.OnEditSubmitting();
        }

        protected override void Reset() {
            var partsPurchasePricingChange = this.DataContext as PartsPurchasePricingChange;
            if(partsPurchasePricingChange != null && this.DomainContext.PartsPurchasePricingChanges.Contains(partsPurchasePricingChange))
                this.DomainContext.PartsPurchasePricingChanges.Detach(partsPurchasePricingChange);
        }

        private FileUploadDataEditPanel fileUploadDataEditPanels;
        public FileUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (FileUploadDataEditPanel)DI.GetDataEditPanel("FileUpload"));
            }
        }

        private void CreateUI() {
            InitializeComponent();
            ApproveDataGridView.IsEnabled = true;
            this.LayoutRoot.Children.Add(this.CreateVerticalLine(1));

            this.DataContextChanged += PartsPurchasePricingChangeInitialApproveDataEditView_DataContextChanged;
            FileUploadDataEditPanels.Margin = new Thickness(5,30,0,0);
            FileUploadDataEditPanels.SetValue(Grid.RowProperty, 9);
            FileUploadDataEditPanels.SetValue(Grid.ColumnSpanProperty, 2);
            FileUploadDataEditPanels.FilePath = null;
            FileUploadDataEditPanels.isHiddenButtons = true;
            FileUploadDataEditPanels.HorizontalAlignment = HorizontalAlignment.Right;
            this.LeftRoot.Children.Add(FileUploadDataEditPanels);

            //var dataEditPanel = DI.GetDataEditPanel("PartsPurchasePricingChangeInitialApprove");
            //dataEditPanel.SetValue(Grid.RowProperty, 0);
            //dataEditPanel.SetValue(Grid.ColumnProperty, 0);
            //this.Root.Children.Add(dataEditPanel);

            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsPurchasePricingChange), "PartsPurchasePricingDetails"), null, () => this.ApproveDataGridView);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.SetValue(Grid.RowProperty, 1);
            detailDataEditView.SetValue(Grid.ColumnProperty, 0);
            this.Root.Children.Add(detailDataEditView);

            this.RegisterButton(new ButtonItem {
                Command = new DelegateCommand(this.RejectCurrentData),
                Title = CommonUIStrings.Action_Title_Reject,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative)
            }, true);
        }

        private void FillApproveMemo(PartsPurchasePricingChange partsPurchasePricingChange) {
            if (partsPurchasePricingChange.Status == (int)DcsPartsPurchasePricingChangeStatus.提交) {
                partsPurchasePricingChange.InitialApproveComment = partsPurchasePricingChange.ApproveMemo;
            } else if (partsPurchasePricingChange.Status == (int)DcsPartsPurchasePricingChangeStatus.初审通过) {
                partsPurchasePricingChange.CheckComment = partsPurchasePricingChange.ApproveMemo;
            } else if (partsPurchasePricingChange.Status == (int)DcsPartsPurchasePricingChangeStatus.审核通过) {
                partsPurchasePricingChange.ApprovalComment = partsPurchasePricingChange.ApproveMemo;
            }
        }

        private void RejectCurrentData() {
            var partsPurchasePricingChange = this.DataContext as PartsPurchasePricingChange;
            if (partsPurchasePricingChange == null)
                return;
            if (string.IsNullOrEmpty(partsPurchasePricingChange.ApproveMemo)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_PartsPurchasePricingChange_ApprovalCommentIsnull);
                return;
            }
            FillApproveMemo(partsPurchasePricingChange);

            partsPurchasePricingChange.ApprovalId = 0;
            partsPurchasePricingChange.ValidationErrors.Clear();
            foreach (var partsPurchasePricingDetail in partsPurchasePricingChange.PartsPurchasePricingDetails)
            {
                partsPurchasePricingDetail.ValidationErrors.Clear();
            }
            ((IEditableObject)partsPurchasePricingChange).EndEdit();
            try {
                if(partsPurchasePricingChange.Can驳回配件采购价格变更申请单)
                    partsPurchasePricingChange.驳回配件采购价格变更申请单();
                 ExecuteSerivcesMethod(CommonUIStrings.DataEditView_Validation_PartsPurchasePricingChange_Reject); 
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            //base.OnEditSubmitting();
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            DomainContext.SubmitChanges(submitOp => {
                if (submitOp.HasError) {
                    if (!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    DomainContext.RejectChanges();
                    return;
                }
                this.NotifyEditSubmitted();
                this.OnCustomEditSubmitted();
                UIHelper.ShowNotification(notifyMessage);
            }, null);
        } 
        public void OnCustomEditSubmitted() { 
            this.DataContext = null; 
        } 
 
        public new event EventHandler EditSubmitted; 
        private void NotifyEditSubmitted() { 
            var handler = this.EditSubmitted; 
            if (handler != null) 
                handler(this, EventArgs.Empty); 
        } 

        private void PartsPurchasePricingChangeInitialApproveDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var PartsPurchasePricingChange = this.DataContext as PartsPurchasePricingChange;
            if (PartsPurchasePricingChange == null)
                return;
            PartsPurchasePricingChange.PropertyChanged -= partsPurchasePricingChange_PropertyChanged;
            PartsPurchasePricingChange.PropertyChanged += partsPurchasePricingChange_PropertyChanged;

        }

        private void partsPurchasePricingChange_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var partsPurchasePricingChange = this.DataContext as PartsPurchasePricingChange;
            if (partsPurchasePricingChange == null)
                return;
            switch (e.PropertyName)
            {
                case "IsNewPart":
                case "IsPrimaryNotMinPurchasePrice":
                case "NotPrimaryIsMinPurchasePrice":
                case "IsPrimaryIsMinPurchasePrice":
                case "NotPrimaryNotMinPurchasePrice":
                    var isNewPart = partsPurchasePricingChange.IsNewPart ?? false;//是否是新配件
                    var isPrimaryNotMinPurchasePrice = partsPurchasePricingChange.IsPrimaryNotMinPurchasePrice ?? false;//是首选供应商、不是最低采购价
                    var notPrimaryIsMinPurchasePrice = partsPurchasePricingChange.NotPrimaryIsMinPurchasePrice ?? false;//不是首选供应商、是最低采购价
                    var isPrimaryIsMinPurchasePrice = partsPurchasePricingChange.IsPrimaryIsMinPurchasePrice ?? false;//是首选供应商、是最低采购价
                    var notPrimaryNotMinPurchasePrice = partsPurchasePricingChange.NotPrimaryNotMinPurchasePrice ?? false;//不是首选供应商、不是最低采购价

                    this.PartsPurchasePricingDetails.Clear();

                    var localDetails = new ObservableCollection<PartsPurchasePricingDetail>();;
                    
                    if (isNewPart){
                        var data1 = this.partsPurchasePricingDetailOrigs.Where(r => r.IsNewPart == true);
                        foreach (var data in data1)
                        {
                            if (localDetails.Where(r => r.Id == data.Id).Count() == 0)
                            localDetails.Add(data);
                        }
                    }
                    if (isPrimaryNotMinPurchasePrice)
                    {
                        var data2 = this.partsPurchasePricingDetailOrigs.Where(r => r.IsPrimary == true && r.MinPurchasePrice != r.Price && r.IsPrimary != null && r.MinPurchasePrice!=null);
                        foreach (var data in data2)
                        {
                            if (localDetails.Where(r => r.Id == data.Id).Count() == 0)
                            localDetails.Add(data);
                        }
                    }
                    if (notPrimaryIsMinPurchasePrice)
                    {
                        var data3 = this.partsPurchasePricingDetailOrigs.Where(r => r.IsPrimary == false && (r.MinPurchasePrice == r.Price|| r.MinPurchasePrice == null) && r.IsPrimary != null );
                        foreach (var data in data3)
                        {
                            if (localDetails.Where(r => r.Id == data.Id).Count() == 0)
                            localDetails.Add(data);
                        }
                    }
                    if (isPrimaryIsMinPurchasePrice)
                    {
                        var data4 = this.partsPurchasePricingDetailOrigs.Where(r => r.IsPrimary == true && (r.MinPurchasePrice == r.Price || r.MinPurchasePrice == null) && r.IsPrimary != null );
                        foreach (var data in data4)
                        {
                            if (localDetails.Where(r => r.Id == data.Id).Count() == 0)
                            localDetails.Add(data);
                        }
                    }
                    if (notPrimaryNotMinPurchasePrice)
                    {
                        var data5 = this.partsPurchasePricingDetailOrigs.Where(r => r.IsPrimary == false && r.MinPurchasePrice != r.Price && r.IsPrimary != null && r.MinPurchasePrice != null);
                        foreach (var data in data5)
                        {
                            if (localDetails.Where(r => r.Id == data.Id).Count() == 0)
                            localDetails.Add(data);
                        }
                    }
                    if (!isNewPart && !isPrimaryNotMinPurchasePrice && !notPrimaryIsMinPurchasePrice && !isPrimaryIsMinPurchasePrice && !notPrimaryNotMinPurchasePrice)
                    {
                        localDetails = this.partsPurchasePricingDetailOrigs;
                    }
                    foreach (var orig in localDetails)
                    {
                        this.PartsPurchasePricingDetails.Add(orig);
                    }

                    break;
            }
        }

        public ObservableCollection<PartsPurchasePricingDetail> PartsPurchasePricingDetails
        {
            get
            {
                return this.partsPurchasePricingDetails ?? (this.partsPurchasePricingDetails = new ObservableCollection<PartsPurchasePricingDetail>());
            }
        }


        private void LoadEntityToEdit(int id) {
            this.partsPurchasePricingDetailOrigs.Clear();
            this.PartsPurchasePricingDetails.Clear();
            this.DomainContext.Load(this.DomainContext.GetPartsPurchasePricingChangeWithOthersQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    entity.IsNewPart = false;
                    entity.IsPrimaryIsMinPurchasePrice = false;
                    entity.IsPrimaryNotMinPurchasePrice = false;
                    entity.NotPrimaryIsMinPurchasePrice = false;
                    entity.NotPrimaryNotMinPurchasePrice = false;
                    entity.ApprovalId = 1;
                    entity.ApprovalComment = null;
                    foreach (var partsPurchasePricingDetail in entity.PartsPurchasePricingDetails.OrderBy(r => r.Id)) {
                        this.PartsPurchasePricingDetails.Add(partsPurchasePricingDetail);
                        this.partsPurchasePricingDetailOrigs.Add(partsPurchasePricingDetail);
                        if (partsPurchasePricingDetail.IsPrimary == null) partsPurchasePricingDetail.IsPrimary = false;
                    }
                    FileUploadDataEditPanels.FilePath = entity.Path;
                    if (entity.Status == (int)DcsPartsPurchasePricingChangeStatus.提交) {
                        this.Title = "初审配件采购价格变更申请单";
                    }
                    if (entity.Status == (int)DcsPartsPurchasePricingChangeStatus.初审通过) {
                        this.Title = "审核配件采购价格变更申请单";
                    }
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private DataGridViewBase ApproveDataGridView {
            get {
                if(this.approveDataGridView == null) {
                    this.approveDataGridView = DI.GetDataGridView("PartsPurchasePricingDetailForApprove");
                    this.approveDataGridView.DomainContext = this.DomainContext;
                    this.approveDataGridView.DataContext = this;
                }
                return this.approveDataGridView;
            }
        }
    }
}
