﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class PartsReplacementDataEditView {
        public PartsReplacementDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("PartsReplacement"));
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsReplacementsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsReplacement;
            }
        }

        protected override void OnEditSubmitting() {
            var partsReplacement = this.DataContext as PartsReplacement;
            if(partsReplacement == null)
                return;
            partsReplacement.ValidationErrors.Clear();
            if(partsReplacement.OldPartId == partsReplacement.NewPartId) {
                UIHelper.ShowAlertMessage(CommonUIStrings.DataEditView_Validation_PartsReplacement_NewSparePartAndOldSparePartIsSame);
                return;
            }
            if(partsReplacement.HasValidationErrors)
                return;
            ((IEditableObject)partsReplacement).EndEdit();
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
