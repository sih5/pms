﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using System.Windows;
using Telerik.Windows.Controls;
﻿using System;


namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class PartsSupplierDataEditView {
        private KeyValueManager keyValueManager;
        private EnterpriseInformationForPartsSupplierDataEditPanel enterpriseInformationDataEditPanel;
        private readonly string[] kvName = {
             "PartsSupplier_Type"
        };
        private List<TiledRegion> tiledRegions;

        private List<TiledRegion> TiledRegions {
            get {
                return this.tiledRegions ?? (this.tiledRegions = new List<TiledRegion>());
            }
        }

        public EnterpriseInformationForPartsSupplierDataEditPanel EnterpriseInformationDataEditPanel {
            get {
                return this.enterpriseInformationDataEditPanel ?? (this.enterpriseInformationDataEditPanel = (EnterpriseInformationForPartsSupplierDataEditPanel)DI.GetDataEditPanel("EnterpriseInformationForPartsSupplier"));

            }
        }

        public PartsSupplierDataEditView() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvName);
            this.KeyValueManager.LoadData();
            this.Initializer.Register(this.CreateUI);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsSupplier;
            }
        }

        protected override void OnEditSubmitting() {
            var partsSupplier = this.DataContext as PartsSupplier;
            if(partsSupplier == null)
                return;
            partsSupplier.ValidationErrors.Clear();
            if(partsSupplier.Company.Code == null) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_PartsSupplier_Code);
                return;
            }
            if(string.IsNullOrEmpty(partsSupplier.Company.ContactPerson)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_PartsSupplier_ContactPerson);
                return;
            }
            if(string.IsNullOrEmpty(partsSupplier.Company.ContactPhone)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_PartsSupplier_ContactPhone);
                return;
            }
            if(string.IsNullOrEmpty(partsSupplier.Company.ContactMail)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_PartsSupplier_ContactMail);
                return;
            }
            partsSupplier.Company.Code = partsSupplier.Company.Code.Trim();
            if (partsSupplier.Company.Code.Length > 11) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_PartsSupplier_CodeLong);
                return;
            }
            if(partsSupplier.Company.Name == null) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_PartsSupplier_Name);
                return;
            }
            if(string.IsNullOrEmpty(partsSupplier.Company.CityName) || string.IsNullOrEmpty(partsSupplier.Company.ProvinceName) || string.IsNullOrEmpty(partsSupplier.Company.CountyName)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_Company_CityNameIsNull);
                return;
            }

            partsSupplier.Name = partsSupplier.Company.Name;
            partsSupplier.Code = partsSupplier.Company.Code;
            if(!string.IsNullOrEmpty(partsSupplier.Company.CustomerCode)){
                partsSupplier.Company.CustomerCode = partsSupplier.Company.CustomerCode.Trim();
            }
            if(!string.IsNullOrEmpty(partsSupplier.Company.SupplierCode)){
                partsSupplier.Company.SupplierCode = partsSupplier.Company.SupplierCode.Trim();
            }
            

            ((IEditableObject)partsSupplier).EndEdit();
            try {
                if(this.EditState == DataEditState.Edit)
                    if(partsSupplier.Can修改配件供应商基本信息)
                        partsSupplier.修改配件供应商基本信息();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        private void CreateUI() {
            //this.Root.Children.Add(DI.GetDataEditPanel("PartsSupplier"));
            this.isConent = true;
            EnterpriseInformationDataEditPanel.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(EnterpriseInformationDataEditPanel);
            this.SupplierTypeCombobox.ItemsSource = this.KeyValueManager[this.kvName[0]];
            this.radButtonSearch.Click += this.RadButtonSearch_Click;

            var customerInfo = DI.GetQueryWindow("CustomerInfoForSupplier");
            this.popupTextBoxCode.PopupContent = customerInfo;
            customerInfo.SelectionDecided += popupTextBoxCode_SelectionDecided;
        }

        private void popupTextBoxCode_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if (queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var virtualIdNameForTmp = queryWindow.SelectedEntities.Cast<VirtualIdNameForTmp>().FirstOrDefault();
            if (virtualIdNameForTmp == null)
                return;
            var partsSupplier = this.DataContext as PartsSupplier;
            if (partsSupplier == null)
                return;
            if (partsSupplier.Company != null) {
                partsSupplier.Company.Code = virtualIdNameForTmp.Code;
                partsSupplier.Company.Name = virtualIdNameForTmp.Name;
            }
            var parent = queryWindow.ParentOfType<RadWindow>();
            if (parent != null)
                parent.Close();
        }

        private void RadButtonSearch_Click(object sender, RoutedEventArgs e) {
            var partsSupplier = this.DataContext as PartsSupplier;
            if (partsSupplier == null)
                return;
            this.DomainContext.Load(this.DomainContext.getSupplierNameByCodeForTmpQuery(partsSupplier.Code), LoadBehavior.RefreshCurrent, loadOp => {
                if (loadOp.HasError)
                    return;
                if (partsSupplier.Id != 0 && loadOp.Entities.Count() >= 1) {
                        partsSupplier.Company.Name = loadOp.Entities.FirstOrDefault().Name;
                }
            }, null);
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsSupplierByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                    SetTiledRegionId();
                }
            }, null);
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        protected override void Reset() {
            var partsSupplier = this.DataContext as PartsSupplier;
            if(partsSupplier != null && (partsSupplier.Company != null && this.DomainContext.Companies.Contains(partsSupplier.Company))) {
                this.DomainContext.Companies.Detach(partsSupplier.Company);
            }
            if(this.DomainContext.PartsSuppliers.Contains(partsSupplier))
                this.DomainContext.PartsSuppliers.Detach(partsSupplier);
        }
        private void SetTiledRegionId() {
            var partsSupplier = this.DataContext as PartsSupplier;
            if(partsSupplier == null)
                return;
            var company = partsSupplier.Company;
            if(company == null)
                return;
            if(company.ProvinceName == null)
                return;
            if(this.EnterpriseInformationDataEditPanel.KvProvinceNames.Any()) {
                company.ProvinceId = this.EnterpriseInformationDataEditPanel.KvProvinceNames.First(e => e.Value == company.ProvinceName).Key;
                if(company.CityName == null)
                    return;
                company.CityId = this.EnterpriseInformationDataEditPanel.KvCityNames.First(e => e.Value == company.CityName).Key;
                if(company.CountyName == null)
                    return;
                company.CountyId = this.EnterpriseInformationDataEditPanel.KvCountyNames.First(e => e.Value == company.CountyName).Key;
            } else {
                this.DomainContext.Load(DomainContext.GetTiledRegionsQuery().Where(ex => ex.CountyName != null).OrderBy(ex => ex.ProvinceName), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError)
                        return;
                    foreach(var repairObject in loadOp.Entities)
                        this.TiledRegions.Add(new TiledRegion {
                            Id = repairObject.Id,
                            ProvinceName = repairObject.ProvinceName,
                            CityName = repairObject.CityName,
                            CountyName = repairObject.CountyName,
                        });
                    this.EnterpriseInformationDataEditPanel.KvProvinceNames.Clear();
                    foreach(var item in TiledRegions) {
                        var values = this.EnterpriseInformationDataEditPanel.KvProvinceNames.Select(e => e.Value);
                        if(values.Contains(item.ProvinceName))
                            continue;
                        this.EnterpriseInformationDataEditPanel.KvProvinceNames.Add(new KeyValuePair {
                            Key = item.Id,
                            Value = item.ProvinceName
                        });
                    }
                    company.ProvinceId = this.EnterpriseInformationDataEditPanel.KvProvinceNames.First(e => e.Value == company.ProvinceName).Key;
                    if(company.CityName == null)
                        return;
                    company.CityId = this.EnterpriseInformationDataEditPanel.KvCityNames.First(e => e.Value == company.CityName).Key;
                    if(company.CountyName == null)
                        return;
                    company.CountyId = this.EnterpriseInformationDataEditPanel.KvCountyNames.First(e => e.Value == company.CountyName).Key;
                }, null);
            }
        }
    }
}
