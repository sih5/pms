﻿
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class PartsSupplierDetailDataEditView {
        private KeyValueManager keyValueManager;
        private readonly string[] kvNames = {
            "PartsSupplier_Type"
        };

        public PartsSupplierDetailDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
        }
        private void CreateUI() {
            //this.Root.Children.Add(DI.GetDataEditPanel("PartsSupplier"));
            this.isConent = true;
            var dataEditPanel = DI.GetDetailPanel("EnterpriseInformationForPartsSupplier");
            dataEditPanel.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(dataEditPanel);
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        protected override string Title {
            get {
                return CommonUIStrings.DataEditView_Title_PartsSupplierDetail;
            }
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsSupplierByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
    }
}
