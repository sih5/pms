﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Text;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Web.Entities;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class PartsSupplierRelationDataEditView {
        private ObservableCollection<KeyValuePair> kvPartsSalesCategories;
        public ObservableCollection<KeyValuePair> KvPartsSalesCategories {
            get {
                return this.kvPartsSalesCategories ?? (this.kvPartsSalesCategories = new ObservableCollection<KeyValuePair>());
            }
        }

        //默认订货仓库
        private ObservableCollection<KeyValuePair> kvDefaultOrderWarehouses;
        public ObservableCollection<KeyValuePair> KvDefaultOrderWarehouses {
            get {
                return this.kvDefaultOrderWarehouses ?? (this.kvDefaultOrderWarehouses = new ObservableCollection<KeyValuePair>());
            }
        }

        public PartsSupplierRelationDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void OnEditSubmitting() {
            var partsSupplierRelation = this.DataContext as PartsSupplierRelation;
            if(partsSupplierRelation == null)
                return;
            partsSupplierRelation.ValidationErrors.Clear();
            var errorString = new StringBuilder();
            if(partsSupplierRelation.EconomicalBatch.HasValue && partsSupplierRelation.EconomicalBatch < 1)
                partsSupplierRelation.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_PartsSupplierRelation_EconomicalBatchLessThanOne, new[] {
                    "EconomicalBatch"
                }));
            if(partsSupplierRelation.MinBatch.HasValue && partsSupplierRelation.MinBatch < 1)
                partsSupplierRelation.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_PartsSupplierRelation_MinBatchLessThanOne, new[] {
                    "MinBatch"
                }));
            if(partsSupplierRelation.PurchasePercentage.HasValue && (partsSupplierRelation.PurchasePercentage <= 0 || partsSupplierRelation.PurchasePercentage > 1))
                partsSupplierRelation.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_PartsSupplierRelation_PurchasePercentageLessThanOne, new[] {
                    "PurchasePercentage"
                }));
            if(partsSupplierRelation.PartId == default(int))
                errorString.AppendLine(CommonUIStrings.DataEditView_Validation_PartsSupplierRelation_SparePartCodeIsNull);
            if(partsSupplierRelation.DefaultOrderWarehouseId == default(int))
                errorString.AppendLine(CommonUIStrings.DataEditView_Validation_PartsSupplierRelation_DefaultOrderWarehouseIsNull);
            if(partsSupplierRelation.SupplierId == default(int))
                errorString.AppendLine(CommonUIStrings.DataEditView_Validation_PartsSupplierRelation_PartsSupplierCodeIsNull);
            if(partsSupplierRelation.OrderCycle == null || partsSupplierRelation.OrderCycle < 0)
                errorString.AppendLine(CommonUIStrings.DataEditView_Validation_PartsSupplierRelation_OrderCycleIsNull);
            if(partsSupplierRelation.OrderGoodsCycle != null  && partsSupplierRelation.OrderGoodsCycle.Value < 0)
                errorString.AppendLine(CommonUIStrings.DataEditView_Validation_PartsSupplierRelation__OrderGoodsCycle);
            if(partsSupplierRelation.MinBatch == null)
                errorString.AppendLine(CommonUIStrings.DataEditView_Validation_PartsSupplierRelation_MinBatch);
            if(string.IsNullOrEmpty(partsSupplierRelation.SupplierPartCode))
                errorString.AppendLine(CommonUIStrings.DataEditView_Validation_PartsSupplierRelation_SupplierPartCode);
            if(partsSupplierRelation.HasValidationErrors || !string.IsNullOrWhiteSpace(errorString.ToString())) {
                if(!string.IsNullOrWhiteSpace(errorString.ToString()))
                    UIHelper.ShowAlertMessage(errorString.ToString());
                return;
            }
            ((IEditableObject)partsSupplierRelation).EndEdit();
            base.OnEditSubmitting();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsSupplierRelation;
            }
        }
        protected override bool OnRequestCanSubmit()
        {
            this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            return true;
        }

        private void CreateUI() {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategor in loadOp.Entities)
                    this.KvPartsSalesCategories.Add(new KeyValuePair {
                        Key = partsSalesCategor.Id,
                        Value = partsSalesCategor.Name
                    });
                this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            }, null);
            this.DomainContext.Load(this.DomainContext.GetWarehousesOrderByNameQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效 && e.Type != (int)DcsWarehouseType.虚拟库), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehouse in loadOp.Entities)
                    this.KvDefaultOrderWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
            }, null);
            var queryWindowSparePart = DI.GetQueryWindow("SparePart");
            queryWindowSparePart.SelectionDecided += this.PartsSupplierRelationSpareCode_SelectionDecided;
            this.ptbSparePart.PopupContent = queryWindowSparePart;
            var queryWindowPartsSupplier = DI.GetQueryWindow("PartsSupplierForSelect");
            queryWindowPartsSupplier.SelectionDecided += this.PartsSupplierRelationPartsSupplierCode_SelectionDecided;
            queryWindowPartsSupplier.Loaded += this.PartsSupplierRelationPartsSupplierCode_Loaded;
            this.ptbPartsSupplier.PopupContent = queryWindowPartsSupplier;
        }

        private void PartsSupplierRelationSpareCode_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var sparePart = queryWindow.SelectedEntities.Cast<SparePart>().FirstOrDefault();
            if(sparePart == null)
                return;

            var partsSupplierRelation = this.DataContext as PartsSupplierRelation;
            if(partsSupplierRelation == null)
                return;
            this.DomainContext.Load(this.DomainContext.GetSparePartsQuery().Where(entity => entity.Id == sparePart.Id), loadOp => {
                if(!loadOp.HasError)
                    partsSupplierRelation.SparePart = loadOp.Entities.FirstOrDefault();
                else {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }, null);
        }

        private void PartsSupplierRelationPartsSupplierCode_Loaded(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            var partsSupplierRelation = this.DataContext as PartsSupplierRelation;
            if(queryWindow == null || partsSupplierRelation == null)
                return;
            if(partsSupplierRelation.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_PartDeleaveInformation_PartsSalesCategoryIdIsNull);
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
                return;
            }

            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem("PartsSalesCategoryId", typeof(int), FilterOperator.IsEqualTo, partsSupplierRelation.PartsSalesCategoryId));

            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }

        private void PartsSupplierRelationPartsSupplierCode_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var partsSupplier = queryWindow.SelectedEntities.Cast<VirtualPartsSupplier>().FirstOrDefault();
            if(partsSupplier == null)
                return;
            var partsSupplierRelation = this.DataContext as PartsSupplierRelation;
            if(partsSupplierRelation == null)
                return;
            this.DomainContext.Load(this.DomainContext.GetPartsSuppliersQuery().Where(entity => entity.Id == partsSupplier.Id), loadOp => {
                if(!loadOp.HasError)
                    partsSupplierRelation.PartsSupplier = loadOp.Entities.FirstOrDefault();
                else {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }, null);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsSupplierRelationsWithDetailsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }
    }
}
