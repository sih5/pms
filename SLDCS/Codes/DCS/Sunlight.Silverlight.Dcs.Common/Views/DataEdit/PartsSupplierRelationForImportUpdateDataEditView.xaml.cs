﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;


namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class PartsSupplierRelationForImportUpdateDataEditView {
        private DataGridViewBase partsSupplierRelationForImportDataGridView;
        private ICommand exportFileCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出配件与供应商关系模板.xlsx";

        public PartsSupplierRelationForImportUpdateDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return CommonUIStrings.DataEditView_Title_PartsSupplierRelationForImport;
            }
        }

        protected DataGridViewBase ImportDataGridView {
            get {
                if(this.partsSupplierRelationForImportDataGridView == null) {
                    this.partsSupplierRelationForImportDataGridView = DI.GetDataGridView("PartsSupplierRelationForImport");
                    this.partsSupplierRelationForImportDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsSupplierRelationForImportDataGridView;
            }
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = CommonUIStrings.DataEditView_Title_PartsSupplierRelationForImportList,
                Content = this.ImportDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.LayoutRoot.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.ImpUpdatePartsSupplierRelationAsync(fileName);
            this.ExcelServiceClient.ImpUpdatePartsSupplierRelationCompleted -= ExcelServiceClient_ImpUpdatePartsSupplierRelationCompleted;
            this.ExcelServiceClient.ImpUpdatePartsSupplierRelationCompleted += ExcelServiceClient_ImpUpdatePartsSupplierRelationCompleted;
        }

        private void ExcelServiceClient_ImpUpdatePartsSupplierRelationCompleted(object sender, ImpUpdatePartsSupplierRelationCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!this.HasImportingError) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImport, 5);
            } else {
                if(!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if(!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                    new ImportTemplateColumn {
                        Name = CommonUIStrings.DataEditView_Text_BranchSupplierRelation_BranchCode,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = CommonUIStrings.DataEditView_Text_BranchSupplierRelation_BranchName
                    },
                    new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_Title_EngineProductLine_PartsSalesCategory,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = CommonUIStrings.DataDeTailPanel_Text_BottomStock_SparePartName,
                    },
                    new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierCode,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierName,
                    },
                    new ImportTemplateColumn {
                        Name =CommonUIStrings.DataGridView_ColumnItem_Title_PartsSupplierRelation_SupplierPartCode,
                        IsRequired = true
                    },
                    //new ImportTemplateColumn {
                    //    Name = "是否首选供应商",
                    //},
                    new ImportTemplateColumn {
                        Name = CommonUIStrings.DataEditView_Text_PartsSupplierRelation_PurchasePercentage,
                    },
                    new ImportTemplateColumn {
                        Name = CommonUIStrings.DataEditView_Text_PartsSupplierRelation_EconomicalBatch,
                    },
                    new ImportTemplateColumn {
                        Name = CommonUIStrings.DataEditView_Text_PartsSupplierRelation_MinBatch,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = CommonUIStrings.DataEditView_Text_PartsSupplierRelation_OrderCycle,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = CommonUIStrings.DataEditView_Text_PartsSupplierRelation_Month,
                    },
                    new ImportTemplateColumn {
                        Name = CommonUIStrings.DataEditView_Text_PartsSupplierRelation_Urgent,
                    },
                    new ImportTemplateColumn {
                        Name = CommonUIStrings.DataEditView_Text_PartsSupplierRelation_ArrivalReplenishmentCycle,
                    },
                    new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartsInboundCheckBillRemark,
                    },
                    new ImportTemplateColumn {
                        Name = CommonUIStrings.DataEditView_Text_PartsSupplierRelation_OrderGoodsCycle,
                        IsRequired = true
                    }
                };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
    }
}
