﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class PersonnelSaleRegionDataEditView {
        private DataGridViewBase personnelSaleRegionForPersonnelDataGridView;
        private string regionName;
        public new event EventHandler EditSubmitted;
        private ObservableCollection<KeyValuePair> kvRegionCodes = new ObservableCollection<KeyValuePair>();

        public int SalesRegionIdForEdit {
            get;
            set;
        }

        public ObservableCollection<KeyValuePair> KvRegionCodes {
            get {
                if(kvRegionCodes == null)
                    kvRegionCodes = new ObservableCollection<KeyValuePair>();
                return this.kvRegionCodes;
            }
        }
        public string RegionName {
            get {
                return this.regionName;
            }
            set {
                this.regionName = value;
                this.OnPropertyChanged("RegionName");
            }
        }

        private void OnPropertyChanged(string p) {
            throw new System.NotImplementedException();
        }
        private void LoadEntityToEdit(int id) {
            RegionPersonnelRelations.Clear();
            this.DomainContext.Load(this.DomainContext.GetRegionPersonnelRelationWithDetailQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                entity.Personnel.SequeueNumber = 1;
                SalesRegionIdForEdit = entity.SalesRegionId;
                RegionPersonnelRelations.Add(entity);
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private ObservableCollection<RegionPersonnelRelation> regionPersonnelRelations;
        public ObservableCollection<RegionPersonnelRelation> RegionPersonnelRelations {
            get {
                return this.regionPersonnelRelations ?? (this.regionPersonnelRelations = new ObservableCollection<RegionPersonnelRelation>());
            }
        }

        private DataGridViewBase PersonnelSaleRegionForPersonnelDataGridView {
            get {
                if(this.personnelSaleRegionForPersonnelDataGridView == null) {
                    this.personnelSaleRegionForPersonnelDataGridView = DI.GetDataGridView("PersonnelSaleRegionForPersonnel");
                    this.personnelSaleRegionForPersonnelDataGridView.DomainContext = this.DomainContext;
                    this.personnelSaleRegionForPersonnelDataGridView.DataContext = this;
                }
                return this.personnelSaleRegionForPersonnelDataGridView;
            }
        }

        private void PersonnelSaleRegionDataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.RegionPersonnelRelations.Clear();
            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            this.KvRegionCodes.Clear();
            domainContext.Load(domainContext.GetSalesRegionsQuery().Where(s => s.Status == (int)DcsBaseDataStatus.有效 && s.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var SalesRegion in loadOp.Entities)
                    this.KvRegionCodes.Add(new KeyValuePair {
                        Key = SalesRegion.Id,
                        Value = SalesRegion.RegionCode,
                        UserObject = SalesRegion
                    });
            }, null);

            this.regionCodeRadCombox.SelectionChanged += regionCodeRadCombox_SelectionChanged;
        }

        private void CreateUI() {
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(CommonUIStrings.DataGridView_Title_PersonnelSaleRegionForPersonnel, null, () => this.PersonnelSaleRegionForPersonnelDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);
        }

        protected override void OnEditSubmitting() {
            if(!this.PersonnelSaleRegionForPersonnelDataGridView.CommitEdit())
                return;

            var regionPersonnelRelation = this.DataContext as RegionPersonnelRelation;

            if(regionPersonnelRelation == null)
                return;

            if(this.regionCodeRadCombox.SelectedValue == null) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_RegionPersonnelRelation_RegionCode);
                return;
            }

            if(this.regionNameRadTextBox.Text == string.Empty) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_RegionPersonnelRelation_RegionName);
                return;
            }

            foreach(var entity in this.RegionPersonnelRelations)
                entity.ValidationErrors.Clear();
            foreach(var entity in this.RegionPersonnelRelations) {
                entity.SalesRegionId = this.SalesRegionIdForEdit;
                entity.BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                entity.Status = (int)DcsBaseDataStatus.有效;
                ((IEditableObject)entity).EndEdit();
            }
            if(this.DomainContext.RegionPersonnelRelations.Contains(regionPersonnelRelation))
                this.DomainContext.RegionPersonnelRelations.Detach(regionPersonnelRelation);
            this.DomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    this.DomainContext.RejectChanges();
                    return;
                }
                this.NotifyEditSubmitted();
                this.OnCustomEditSubmitted();
            }, null);

        }

        private void NotifyEditSubmitted() {
            var handler = this.EditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        public void OnCustomEditSubmitted() {
            this.DataContext = null;
        }

        protected override void Reset() {
            if(RegionPersonnelRelations != null)
                this.RegionPersonnelRelations.Clear();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override string BusinessName {
            get {
                return CommonUIStrings.DataEditView_BusinessName_PersonnelSaleRegion;//DcsUIStrings.BusinessName_PersonnelSaleRegion;
            }
        }

        public PersonnelSaleRegionDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.Loaded += this.PersonnelSaleRegionDataEditView_Loaded;
        }

        public void regionCodeRadCombox_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            var regionCode = sender as RadComboBox;
            if(regionCode == null)
                return;
            var item = regionCode.SelectedItem as KeyValuePair;
            if(item == null)
                return;
            var salesRegion = item.UserObject as SalesRegion;
            if(item != null)
                regionNameRadTextBox.Text = salesRegion.RegionName;
        }


    }
}
