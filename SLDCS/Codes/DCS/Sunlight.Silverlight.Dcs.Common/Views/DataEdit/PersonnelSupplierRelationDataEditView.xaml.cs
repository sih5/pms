﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using System.Collections.ObjectModel;
using Sunlight.Silverlight.Core.Model;
using System.Windows;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit
{
    public partial class PersonnelSupplierRelationDataEditView
    {
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategories = new ObservableCollection<KeyValuePair>();
        public ObservableCollection<KeyValuePair> KvPartsSalesCategories
        {
            get
            {
                return this.kvPartsSalesCategories;
            }
        }

        public PersonnelSupplierRelationDataEditView()
        {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            //this.Loaded += PersonnelSupplierRelationDataEditView_Loaded;
        }

        void PersonnelSupplierRelationDataEditView_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            this.Title = CommonUIStrings.DataEditView_Title_PersonnelSupplierRelation;
        }

        private void CreateUI()
        {
            //1.品牌查询
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                    return;
                foreach (var partsSalesCategor in loadOp.Entities)
                    this.kvPartsSalesCategories.Add(new KeyValuePair
                    {
                        Key = partsSalesCategor.Id,
                        Value = partsSalesCategor.Name
                    });
                this.CbPartsSalesCategoryName.SelectedIndex = 0;//默认品牌
            }, null);
            //2.人员查询
            var queryWindowPerson = DI.GetQueryWindow("PersonnelForCompany");
            queryWindowPerson.SelectionDecided += this.queryWindowPerson_SelectionDecided;
            //queryWindowPerson.Loaded += queryWindowPerson_Loaded;
            this.ptbPersonName.PopupContent = queryWindowPerson;
            //3.供应商查询
            var queryWindow = DI.GetQueryWindow("BranchSupplierRelation");
            queryWindow.SelectionDecided += this.QueryWindow_SelectionDecided;
            queryWindow.Loaded += queryWindow_Loaded;
            this.ptbSupplierCode.PopupContent = queryWindow;

            this.isConent = true;
        }

        ////2.人员查询
        //void queryWindowPerson_Loaded(object sender, RoutedEventArgs e)
        //{
        //    var queryWindow = sender as DcsQueryWindowBase;
        //    var personnelSupplierRelation = this.DataContext as PersonnelSupplierRelation;
        //    if (queryWindow == null || personnelSupplierRelation == null)
        //        return;
        //    //queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
        //    //    "Common", "CorporationName", BaseApp.Current.CurrentUserData.EnterpriseName
        //    //});
        //    var compositeFilterItem = new CompositeFilterItem();
        //    compositeFilterItem.Filters.Add(new FilterItem
        //    {
        //        MemberName = "CorporationId",
        //        MemberType = typeof(int),
        //        Operator = FilterOperator.IsEqualTo,
        //        Value = BaseApp.Current.CurrentUserData.EnterpriseId
        //    });
        //    queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        //}
        private void queryWindowPerson_SelectionDecided(object sender, EventArgs e)
        {
            var queryWindow = sender as QueryWindowBase;
            if (queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var selectedPersonnel = queryWindow.SelectedEntities.Cast<Personnel>().FirstOrDefault();
            if (selectedPersonnel == null)
            {
                var parent = queryWindow.ParentOfType<RadWindow>();
                if (parent != null)
                    parent.Close();
                return;
            }

            var personnelSupplierRelation = this.DataContext as PersonnelSupplierRelation;
            if (personnelSupplierRelation == null)
                return;
            try
            {
                if (personnelSupplierRelation.PersonId == selectedPersonnel.Id)
                    return;
                personnelSupplierRelation.PersonId = selectedPersonnel.Id;
                personnelSupplierRelation.PersonCode = selectedPersonnel.LoginId;
                personnelSupplierRelation.PersonName = selectedPersonnel.Name;
            }
            finally
            {
                var parent = queryWindow.ParentOfType<RadWindow>();
                if (parent != null)
                    parent.Close();
            }
        }
        //3.供应商查询
        private void queryWindow_Loaded(object sender, RoutedEventArgs e)
        {
            var queryWindow = sender as DcsQueryWindowBase;
            var personnelSupplierRelation = this.DataContext as PersonnelSupplierRelation;
            if (queryWindow == null || personnelSupplierRelation == null)
                return;
            if (personnelSupplierRelation.PartsSalesCategoryId == default(int))
            {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_PartsPurchasePricingChange_PartsSalesCategoryNameIsNull);
                var parent = queryWindow.ParentOfType<RadWindow>();
                if (parent != null)
                    parent.Close();
                return;
            }
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "Branch.Name", BaseApp.Current.CurrentUserData.EnterpriseName
            });
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem("PartsSalesCategoryId", typeof(int), FilterOperator.IsEqualTo, personnelSupplierRelation.PartsSalesCategoryId));
            compositeFilterItem.Filters.Add(new FilterItem
            {
                MemberName = "BranchId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }
        private void QueryWindow_SelectionDecided(object sender, EventArgs e)
        {
            var queryWindow = sender as QueryWindowBase;
            if (queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var branchSupplierRelation = queryWindow.SelectedEntities.Cast<BranchSupplierRelation>().FirstOrDefault();
            if (branchSupplierRelation == null)
            {
                var parent = queryWindow.ParentOfType<RadWindow>();
                if (parent != null)
                    parent.Close();
                return;
            }
            var personnelSupplierRelation = this.DataContext as PersonnelSupplierRelation;
            if (personnelSupplierRelation == null)
                return;
            try
            {
                if (personnelSupplierRelation.SupplierId == branchSupplierRelation.Id)
                    return;
                personnelSupplierRelation.SupplierId = branchSupplierRelation.SupplierId;
                personnelSupplierRelation.SupplierCode = branchSupplierRelation.PartsSupplier.Code;
                personnelSupplierRelation.SupplierName = branchSupplierRelation.PartsSupplier.Name;
            }
            finally
            {
                var parent = queryWindow.ParentOfType<RadWindow>();
                if (parent != null)
                    parent.Close();
            }
        }


        private ObservableCollection<PersonnelSupplierRelation> personnelSupplierRelations;
        public ObservableCollection<PersonnelSupplierRelation> PersonnelSupplierRelations
        {
            get
            {
                if (this.personnelSupplierRelations == null)
                {
                    this.personnelSupplierRelations = new ObservableCollection<PersonnelSupplierRelation>();
                } return personnelSupplierRelations;
            }
        }

        private void LoadEntityToEdit(int id)
        {
            this.DomainContext.Load(this.DomainContext.QueryPersonnelSupplierRelationsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                {
                    if (!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if (entity != null)
                {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        protected override void OnEditSubmitting()
        {
            var personnelSupplierRelation = this.DataContext as PersonnelSupplierRelation;
            if (personnelSupplierRelation == null)
                return;
            personnelSupplierRelation.ValidationErrors.Clear();

            if (personnelSupplierRelation.PartsSalesCategoryId == default(int))
            {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_PartsPurchasePricingChange_PartsPurchasePricingDetails_PartsSalesCategoryIsEmpty);
                return;
            }
            if (personnelSupplierRelation.SupplierId == default(int))
            {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_BranchSupplierRelation_SupplierIsNotNull);
                return;
            }
            if (personnelSupplierRelation.PersonId == default(int))
            {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_PersonnelSupplierRelation_Person);
                return;
            }
            personnelSupplierRelation.PartsSalesCategoryName = this.CbPartsSalesCategoryName.Text;
            ((IEditableObject)personnelSupplierRelation).EndEdit();
            try
            {
                if (this.EditState == DataEditState.Edit)
                {
                    if (personnelSupplierRelation.Can修改人员与供应商关系)
                        personnelSupplierRelation.修改人员与供应商关系();
                }
                else if (this.EditState == DataEditState.New)
                {
                    if (personnelSupplierRelation.Can新增人员与供应商关系)
                        personnelSupplierRelation.新增人员与供应商关系();
                }
            }
            catch (ValidationException ex)
            {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }
        protected override bool OnRequestCanSubmit()
        {

            this.CbPartsSalesCategoryName.SelectedIndex = 0;//默认品牌
            return true;
        }

        protected override string BusinessName
        {
            get
            {
                return CommonUIStrings.DataEditView_BusinessName_PersonnelSupplierRelation;
            }
        }

        public override void SetObjectToEditById(object id)
        {
            if (this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
