﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using System.ComponentModel.DataAnnotations;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit
{
    public partial class PersonnelSupplierRelationForImportDataEditView
    {

        private ICommand exportFileCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出人员与供应商关系模板.xlsx";
        private DataGridViewBase personnelSupplierRelationForImportForEdit;
        protected DataGridViewBase PersonnelSupplierRelationForImportForEdit
        {
            get
            {
                if (this.personnelSupplierRelationForImportForEdit == null)
                {
                    this.personnelSupplierRelationForImportForEdit = DI.GetDataGridView("PersonnelSupplierRelationForImportForEdit");
                    this.personnelSupplierRelationForImportForEdit.DataContext = this.DataContext;
                }
                return this.personnelSupplierRelationForImportForEdit;
            }
        }
        public PersonnelSupplierRelationForImportDataEditView()
        {
            InitializeComponent();
            this.Initializer.Register(CreateUI);
        }

        protected override string Title
        {
            get
            {
                return CommonUIStrings.DataEditView_Title_PersonnelSupplierRelationForImport;
            }
        }

        private void CreateUI()
        {
            this.ShowSaveButton();
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem
            {
                Header = CommonUIStrings.DataEditView_Title_PersonnelSupplierRelationForImportList,
                Content = this.PersonnelSupplierRelationForImportForEdit
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        private void UploadFileProcessing(string fileName)
        {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.ImportPersonnelSupplierRelationAsync(fileName);
            this.ExcelServiceClient.ImportPersonnelSupplierRelationCompleted -= ExcelServiceClient_ImportPersonnelSupplierRelationCompleted;
            this.ExcelServiceClient.ImportPersonnelSupplierRelationCompleted += ExcelServiceClient_ImportPersonnelSupplierRelationCompleted;
        }

        private void ExcelServiceClient_ImportPersonnelSupplierRelationCompleted(object sender, ImportPersonnelSupplierRelationCompletedEventArgs e)
        {
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if (!HasImportingError)
            {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImport, 10);
            }
            else
            {
                if (!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if (!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }
        private void InitializeCommand()
        {
            try
            {
                this.exportFileCommand = new Core.Command.DelegateCommand(() =>
                {
                    var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditView_Text_BottomStock_Brand,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditView_Text_PersonnelSupplierRelation_PersonelCode,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierCode,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartsInboundCheckBillRemark
                                            }
                                        };
                    this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp =>
                    {
                        if (loadOp.HasError)
                        {
                            if (!loadOp.IsErrorHandled)
                                loadOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                        }
                        if (!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                            this.ExportFile(loadOp.Value);
                    }, null);
                });
            }
            catch (ValidationException ex)
            {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
        }

        protected override bool IsAutomaticUploadFile
        {
            get
            {
                return true;
            }
        }

        public ICommand ExportTemplateCommand
        {
            get
            {
                if (exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

    }
}