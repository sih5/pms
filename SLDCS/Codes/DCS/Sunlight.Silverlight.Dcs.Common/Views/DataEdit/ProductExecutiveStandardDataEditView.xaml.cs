﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class ProductExecutiveStandardDataEditView {
        public ProductExecutiveStandardDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void OnEditSubmitting() {
            //var productStandard = this.DataContext as ProductStandard;

            //if(productStandard == null)
            //    return;

            //productStandard.ValidationErrors.Clear();
            //if(string.IsNullOrEmpty(productStandard.StandardCode))
            //    productStandard.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_ProductStandard_StandardCodeIsNull, new[] {
            //        "StandardCode"
            //    }));
            //if(string.IsNullOrEmpty(productStandard.StandardName))
            //    productStandard.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_ProductStandard_StandardNameIsNull, new[] {
            //        "StandardName"
            //    }));

            //if(productStandard.HasValidationErrors)
            //    return;
            //productStandard.Status = (int)DcsBaseDataStatus.有效;
            //((IEditableObject)productStandard).EndEdit();
            base.OnEditSubmitting();
        }

        private void CreateUI() {
            // 创建主单编辑面板 
            //this.Root.Children.Add(DI.GetDataEditPanel("ABCStrategy"));
        }

        private void LoadEntityToEdit(int id) {
            //this.DomainContext.Load(this.DomainContext.GetProductStandardsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError) {
            //        if(!loadOp.IsErrorHandled)
            //            loadOp.MarkErrorAsHandled();
            //        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
            //        return;
            //    }
            //    var entity = loadOp.Entities.SingleOrDefault();
            //    if(entity != null)
            //        this.SetObjectToEdit(entity);
            //}, null);
        }
    }
}
