﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;


namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class ProductExecutiveStandardForImportDataEditView {
        private DataGridViewBase productStandardForImportDataGridView;
        private ICommand exportFileCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出产品执行标准管理模板.xlsx";

        public ProductExecutiveStandardForImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return CommonUIStrings.DataEditView_Title_ProductExecutiveStandardForBatchImport;
            }
        }

        protected DataGridViewBase ImportDataGridView {
            get {
                if(this.productStandardForImportDataGridView == null) {
                    this.productStandardForImportDataGridView = DI.GetDataGridView("ProductStandardForImport");
                    this.productStandardForImportDataGridView.DomainContext = this.DomainContext;
                }
                return this.productStandardForImportDataGridView;
            }
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = CommonUIStrings.DataEditView_Title_ProductExecutiveStandardForBatchImportList,
                Content = this.ImportDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.LayoutRoot.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        private void UploadFileProcessing(string fileName) {
            if(ShellViewModel.Current.IsBusy) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_PartsSupplierRelation_Import, 5);
                return;
            }
            //ShellViewModel.Current.IsBusy = true;
            //this.ExcelServiceClient.ImportProductStandardAsync(fileName);
            //this.ExcelServiceClient.ImportProductStandardCompleted -= ExcelServiceClient_ImportProductStandardCompleted;
            //this.ExcelServiceClient.ImportProductStandardCompleted += ExcelServiceClient_ImportProductStandardCompleted;
        }

        //private void ExcelServiceClient_ImportProductStandardCompleted(object sender, ImportProductStandardCompletedEventArgs e) {
        //    ShellViewModel.Current.IsBusy = false;
        //    this.ImportComplete();
        //    this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
        //    if(!this.HasImportingError) {
        //        UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImport, 5);
        //    } else {
        //        if(!string.IsNullOrEmpty(e.errorMessage))
        //            UIHelper.ShowAlertMessage(e.errorMessage);
        //        if(!string.IsNullOrEmpty(e.errorDataFileName))
        //            this.ExportFile(e.errorDataFileName);
        //        UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
        //    }
        //}

        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                    new ImportTemplateColumn {
                        Name = CommonUIStrings.DataEditView_ProductStandard_StandardCode,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = CommonUIStrings.DataEditView_ProductStandard_StandardName,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartsInboundCheckBillRemark
                    }
                };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
    }
}
