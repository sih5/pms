﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class ReplaceStockMinimumDataEditView  {
        private DataGridViewBase replaceDataGridView;
        private ICommand exportFileCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出配件分品牌商务信息模板.xlsx";

        protected override string Title {
            get {
                return CommonUIStrings.DataEditView_Title_ReplaceApproveLimit;
            }
        }

        private ObservableCollection<PartsBranchExtend> partsBranchExtend;
        public ObservableCollection<PartsBranchExtend> PartsBranchExtends {
            get {
                return this.partsBranchExtend ?? (this.partsBranchExtend = new ObservableCollection<PartsBranchExtend>());
            }
        }

        protected DataGridViewBase ReplaceDataGridView {
            get {
                if(this.replaceDataGridView == null) {
                    this.replaceDataGridView = DI.GetDataGridView("ReplaceStockMinimum");
                    this.replaceDataGridView.DomainContext = this.DomainContext;
                    this.replaceDataGridView.DataContext = this;
                }
                return this.replaceDataGridView;
            }
        }

        public ReplaceStockMinimumDataEditView() {
            InitializeComponent();
            this.Loaded += DataEditView_Loaded;
            this.Initializer.Register(CreateUI);
        }

        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = "批量修改库存低限",
                Content = this.ReplaceDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        private void DataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.PartsBranchExtends.Clear();
        }

        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.批量替换库存低限Async(fileName);
            this.ExcelServiceClient.批量替换库存低限Completed -= ExcelServiceClient_ImportUpdatePartsBranchCompleted;
            this.ExcelServiceClient.批量替换库存低限Completed += ExcelServiceClient_ImportUpdatePartsBranchCompleted;
        }

        private void ExcelServiceClient_ImportUpdatePartsBranchCompleted(object sender, 批量替换库存低限CompletedEventArgs e) {
            this.PartsBranchExtends.Clear();
            if(e.rightData.Any()) {
                var i = 1;
                foreach(var sparePart in e.rightData) {
                    sparePart.SerialNumber = i;
                    this.PartsBranchExtends.Add(sparePart);
                    i++;
                }
            }
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!HasImportingError) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImport, 10);
            } else {
                if(!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if(!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                                                IsRequired = true
                                            }, new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Name,
                                                
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditView_Text_PartsBranch_StockMinimum,
                                                IsRequired = true
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
    }
}
