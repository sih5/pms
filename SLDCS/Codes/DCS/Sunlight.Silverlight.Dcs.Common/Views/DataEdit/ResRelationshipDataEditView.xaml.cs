﻿using System.Windows.Controls;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Dcs.Common.Resources;
using System.Collections.ObjectModel;
using Sunlight.Silverlight.Dcs.Web;
using System.Linq;
using System.ComponentModel;
using System.ServiceModel.DomainServices.Client;
using System.Collections.Generic;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Core;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class ResRelationshipDataEditView: INotifyPropertyChanged {
        private KeyValueManager keyValueManager;
        public event PropertyChangedEventHandler PropertyChanged;
        private readonly string[] kvName = {
             "ResponsibleMembersResTem"
        };
        public ResRelationshipDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvName);
            this.KeyValueManager.LoadData();
        }

       
        private void CreateUI() {          
            this.SupplierTypeCombobox.ItemsSource = this.KeyValueManager[this.kvName[0]];
        }
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }     
        protected override string BusinessName {
            get {
                return "责任关系绑定";
            }
        }

        protected override void OnEditCancelled() {
            base.OnEditCancelled();
        }

        protected override void OnEditSubmitted() {         
            base.OnEditSubmitted();
        }

        protected override void OnEditSubmitting() {
            var resRelationship = this.DataContext as ResRelationship;
            if(resRelationship == null )
                return;
            resRelationship.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(resRelationship.ResType)) {
                UIHelper.ShowNotification("请填写责任类型");
                return;
            }
            if(resRelationship.ResTem == default(int)) {
                UIHelper.ShowNotification("请选择责任组");
                return;
            }
            base.OnEditSubmitting();
        }
      

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

      
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetResRelationshipsQuery().Where(t=>t.Id==id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }


    }
}