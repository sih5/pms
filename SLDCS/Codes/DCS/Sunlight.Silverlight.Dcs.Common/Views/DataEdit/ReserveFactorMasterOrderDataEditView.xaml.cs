﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Sunlight.Silverlight.ViewModel;
using System.Windows.Browser;
using System.Collections.Generic;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class ReserveFactorMasterOrderDataEditView {
        private readonly string[] kvNames = {
           "ABCStrategy_Category"
        };
        private RadUpload uploader;
        private ICommand exportFileCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出储备系数清单模板.xlsx";
        private string strFileName;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private ObservableCollection<KeyValuePair> kvABCStrategys;
        private DataGridViewBase reserveFactorOrderDetailForEditDataGridView;

        private ObservableCollection<Warehouse> kvWarehouses;
        private KeyValueManager keyValueManager;
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        public ObservableCollection<KeyValuePair> KvABCStrategys {
            get {
                return this.kvABCStrategys ?? (this.kvABCStrategys = new ObservableCollection<KeyValuePair>());
            }
        }
        protected override string BusinessName {
            get {
                return "储备系数";
            }
        }
        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                   // this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        var partsPurchasePricingChange = this.DataContext as ReserveFactorMasterOrder;
                        if(partsPurchasePricingChange == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.Import储备系数清单Async(e.HandlerData.CustomData["Path"].ToString());
                        this.excelServiceClient.Import储备系数清单Completed -= Import储备系数清单Completed;
                        this.excelServiceClient.Import储备系数清单Completed += Import储备系数清单Completed;
                    };
                }
                return this.uploader;
            }
        }
        /// <summary>
        /// 文件上传成功后，调用该方法进行上传后的处理。不支持异步调用
        /// <param>上传文件名 <name>fileName</name> </param>
        /// </summary>
        protected Action<string> UploadFileSuccessedProcessing;

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //取当前登录用户的HashCode来标记上传文件的唯一性
                //this.UploadFileSuccessedProcessing.Invoke(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }
        private void Import储备系数清单Completed(object sender, Import储备系数清单CompletedEventArgs e) {
            var reserveFactorMasterOrder = this.DataContext as ReserveFactorMasterOrder;
            if(reserveFactorMasterOrder == null)
                return;
            if(e.rightData != null) {
                foreach(var data in e.rightData) {
                    var oldDetail = reserveFactorMasterOrder.ReserveFactorOrderDetails.Where(t => t.PriceCap == data.PriceCap && t.PriceFloor == data.PriceFloor && t.UpperLimitCoefficient == data.UpperLimitCoefficient && t.LowerLimitCoefficient == data.LowerLimitCoefficient).FirstOrDefault();
                    if(oldDetail!=null) {
                        continue;
                    }
                    reserveFactorMasterOrder.ReserveFactorOrderDetails.Add(new ReserveFactorOrderDetail {
                        PriceCap = data.PriceCap,
                        PriceFloor = data.PriceFloor,
                        UpperLimitCoefficient = data.UpperLimitCoefficient,
                        LowerLimitCoefficient = data.LowerLimitCoefficient
                    });
                }                
            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowAlertMessage(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
        }

        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage(CommonUIStrings.DataEditPanel_Validation_Notification_File);
                    e.Handled = true;
                }
            }
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }

        public ObservableCollection<Warehouse> KvWarehouses {
            get {
                return this.kvWarehouses ?? (this.kvWarehouses = new ObservableCollection<Warehouse>());
            }
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetReserveFactorMasterOrderWithDetailsQuery().Where(t => t.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
        private void CreateUI() {
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(ReserveFactorMasterOrder), "储备系数清单"), null, this.ReserveFactorOrderDetailForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = CommonUIStrings.Action_Title_Import,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                Command = new DelegateCommand(this.ShowFileDialog)
            });
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = CommonUIStrings.DataEditView_Title_ExportButton,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                Command = this.ExportTemplateCommand
            });

            this.Root.Children.Add(detailDataEditView);

            KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData(() => {
                foreach(var keyValuePair in this.KeyValueManager[this.kvNames[0]]) {
                    this.KvABCStrategys.Add(keyValuePair);
                }
            });
            KvABCStrategy.ItemsSource = KvABCStrategys;
            this.DomainContext.Load(this.DomainContext.GetWarehousesByPartsSalesCategoryIdAndOwnerCompanyIdQuery(221, BaseApp.Current.CurrentUserData.EnterpriseId).Where(r => r.Type != (int)DcsWarehouseType.虚拟库), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                this.KvWarehouses.Clear();
                if(loadOp.Entities != null) {
                    foreach(var item in loadOp.Entities)
                        this.KvWarehouses.Add(item);
                    this.WarehouseComboBox.ItemsSource = KvWarehouses;
                }
            }, null);
        }
        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                             new ImportTemplateColumn {
                                                Name = "价格下限(服务站价)",
                                                IsRequired = true
                                            },new ImportTemplateColumn {
                                                Name ="价格上限",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "下限系数",
                                                IsRequired = true
                                            },                                           
                                            new ImportTemplateColumn {
                                                Name = "上限系数",
                                                IsRequired = true
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }
        private void ShowFileDialog() {           
            this.Uploader.ShowFileDialog();
        }
        private DataGridViewBase ReserveFactorOrderDetailForEditDataGridView {
            get {
                if(this.reserveFactorOrderDetailForEditDataGridView == null) {
                    this.reserveFactorOrderDetailForEditDataGridView = DI.GetDataGridView("ReserveFactorOrderDetailForEdit");
                    this.reserveFactorOrderDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.reserveFactorOrderDetailForEditDataGridView;
            }
        }
        protected override void OnEditSubmitting() {
            if(!this.ReserveFactorOrderDetailForEditDataGridView.CommitEdit())
                return;

            var reserveFactorMasterOrder = this.DataContext as ReserveFactorMasterOrder;
            if(reserveFactorMasterOrder == null)
                return;
            if(reserveFactorMasterOrder.WarehouseId == null) {
                UIHelper.ShowNotification("请先选择仓库");
                return;
            }
            if(reserveFactorMasterOrder.ABCStrategyId == null) {
                UIHelper.ShowNotification("请选择配件属性");
                return;
            }
            if(reserveFactorMasterOrder.ReserveCoefficient == null) {
                UIHelper.ShowNotification("请填写储备系数");
                return;
            }
            if(reserveFactorMasterOrder.ReserveCoefficient.Value != 0 && reserveFactorMasterOrder.ReserveCoefficient.Value != 1) {
                UIHelper.ShowNotification("储备系数只能是1或者0");
                return;
            }
            reserveFactorMasterOrder.ValidationErrors.Clear();
            var detais = reserveFactorMasterOrder.ReserveFactorOrderDetails;
            foreach(var relation in reserveFactorMasterOrder.ReserveFactorOrderDetails) {
                if(relation.PriceFloor == null || relation.PriceCap == null) {
                    UIHelper.ShowNotification("价格区间未填写完整");
                    return;
                }
                if(relation.PriceFloor > relation.PriceCap) {
                    UIHelper.ShowNotification("价格上限不能小于价格下限");
                    return;
                }
                if(detais.Any(t => (t.Id != relation.Id && (t.PriceFloor < relation.PriceCap && relation.PriceCap < t.PriceCap) || t.PriceFloor < relation.PriceFloor && relation.PriceFloor < t.PriceCap))) {
                    UIHelper.ShowNotification("价格区间重复，请重新填写");
                    return;
                }

                relation.ValidationErrors.Clear();
            }
            reserveFactorMasterOrder.WarehouseCode = KvWarehouses.Where(y => y.Id == reserveFactorMasterOrder.WarehouseId).First().Code;
            ((IEditableObject)reserveFactorMasterOrder).EndEdit();
            try {
                if(this.EditState == DataEditState.New) {
                    if(reserveFactorMasterOrder.Can新增储备系数主单)
                        reserveFactorMasterOrder.新增储备系数主单();
                } else {
                    if(reserveFactorMasterOrder.Can修改储备系数主单)
                        reserveFactorMasterOrder.修改储备系数主单();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        public ReserveFactorMasterOrderDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.LoadData();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

    }
}
