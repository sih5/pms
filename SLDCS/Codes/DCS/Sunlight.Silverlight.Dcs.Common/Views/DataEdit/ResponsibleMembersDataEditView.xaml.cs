﻿using System.Windows.Controls;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Dcs.Common.Resources;
using System.Collections.ObjectModel;
using Sunlight.Silverlight.Dcs.Web;
using System.Linq;
using System.ComponentModel;
using System.ServiceModel.DomainServices.Client;
using System.Collections.Generic;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Core;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class ResponsibleMembersDataEditView: INotifyPropertyChanged {
        private DataGridViewBase marketDptPersonnelRelationForEditDataGridView;
        private KeyValueManager keyValueManager;
        public event PropertyChangedEventHandler PropertyChanged;
        private readonly string[] kvName = {
             "ResponsibleMembersResTem"
        };
        public ResponsibleMembersDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvName);
            this.KeyValueManager.LoadData();
        }

        private DataGridViewBase MarketDptPersonnelRelationForEditDataGridView {
            get {
                if(this.marketDptPersonnelRelationForEditDataGridView == null) {
                    this.marketDptPersonnelRelationForEditDataGridView = DI.GetDataGridView("ResponsibleDetailsForEdit");
                    this.marketDptPersonnelRelationForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.marketDptPersonnelRelationForEditDataGridView;
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(CommonUIStrings.DataGridView_Title_Personnel, null, () => this.MarketDptPersonnelRelationForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);
            this.SupplierTypeCombobox.ItemsSource = this.KeyValueManager[this.kvName[0]];
        }
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }     
        protected override string BusinessName {
            get {
                return "责任组人员维护";
            }
        }

        protected override void OnEditCancelled() {
            base.OnEditCancelled();
        }

        protected override void OnEditSubmitted() {         
            base.OnEditSubmitted();
        }

        protected override void OnEditSubmitting() {
            if(!this.MarketDptPersonnelRelationForEditDataGridView.CommitEdit())
                return;
            var responsibleMembers = this.DataContext as ResponsibleMember;
            if(responsibleMembers == null || responsibleMembers.ResponsibleDetails.Count()==0)
                return;
            responsibleMembers.ValidationErrors.Clear();
            foreach(var item in responsibleMembers.ResponsibleDetails) {
                item.ValidationErrors.Clear();
            }
            if(responsibleMembers.ResTem == default(int)) {
                UIHelper.ShowNotification("请选择责任组");
                return;
            }
            base.OnEditSubmitting();
        }
      

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

      
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetResponsibleMembersWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }


    }
}
