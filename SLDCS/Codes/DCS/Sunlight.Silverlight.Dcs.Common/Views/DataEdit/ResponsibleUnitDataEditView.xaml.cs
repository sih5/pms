﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class ResponsibleUnitDataEditView {
        private EnterpriseInformationDataEditPanel enterpriseInformationDataEditPanel;
        private DataGridViewBase responsibleUnitBranchDataGridView;

        private EnterpriseInformationDataEditPanel EnterpriseInformationDataEditPanel {
            get {
                return this.enterpriseInformationDataEditPanel ?? (this.enterpriseInformationDataEditPanel = (EnterpriseInformationDataEditPanel)DI.GetDataEditPanel("EnterpriseInformation"));

            }
        }
        private List<TiledRegion> tiledRegions;

        private List<TiledRegion> TiledRegions {
            get {
                return this.tiledRegions ?? (this.tiledRegions = new List<TiledRegion>());
            }
        }


        private DataGridViewBase ResponsibleUnitBranchDataGridView {
            get {
                if(this.responsibleUnitBranchDataGridView == null) {
                    this.responsibleUnitBranchDataGridView = DI.GetDataGridView("ResponsibleUnitBranch");
                    this.responsibleUnitBranchDataGridView.DomainContext = this.DomainContext;
                }
                return this.responsibleUnitBranchDataGridView;
            }
        }

        public ResponsibleUnitDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            var responsibleUnitDataEditPanel = DI.GetDataEditPanel("ResponsibleUnit");
            this.EnterpriseInformationDataEditPanel.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(responsibleUnitDataEditPanel);
            this.Root.Children.Add(this.EnterpriseInformationDataEditPanel);
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(CommonUIStrings.DataEditView_Text_ResponsibleUnitBranch, null, () => this.ResponsibleUnitBranchDataGridView);
            detailDataEditView.SetValue(Grid.RowProperty, 0);
            detailDataEditView.SetValue(Grid.ColumnProperty, 1);
            detailDataEditView.SetValue(Grid.RowSpanProperty, 5);
            this.Root.Children.Add(detailDataEditView);
        }

        protected override void OnEditSubmitting() {
            var responsibleUnit = this.DataContext as ResponsibleUnit;
            if(responsibleUnit == null || !this.ResponsibleUnitBranchDataGridView.CommitEdit())
                return;
            responsibleUnit.ValidationErrors.Clear();
            var company = responsibleUnit.Company;
            if(company == null)
                return;
            company.Name = responsibleUnit.Name;
            company.Code = responsibleUnit.Code;
            if(!string.IsNullOrEmpty(company.CustomerCode)){
                company.CustomerCode = company.CustomerCode.Trim();
            }
            if(!string.IsNullOrEmpty(company.SupplierCode)){
                company.SupplierCode = company.SupplierCode.Trim();
            } 
            responsibleUnit.ValidationErrors.Clear();
            if (string.IsNullOrWhiteSpace(responsibleUnit.Code))
                responsibleUnit.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_ResponsibleUnit_CodeIsNull, new[] {
                    "Code"
                }));
            else
                responsibleUnit.Code = responsibleUnit.Code.Trim();
            if (responsibleUnit.Code !=null && responsibleUnit.Code.Length > 11)
                responsibleUnit.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_ResponsibleUnit_CodeIsOverLength, new[]{"Code"
                }));
            if(string.IsNullOrWhiteSpace(responsibleUnit.Name))
                responsibleUnit.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_ResponsibleUnit_NameIsNull, new[] {
                    "Name"
                }));
            if(string.IsNullOrEmpty(responsibleUnit.Company.CityName) || string.IsNullOrEmpty(responsibleUnit.Company.ProvinceName) || string.IsNullOrEmpty(responsibleUnit.Company.CountyName)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_Company_CityNameIsNull);
                return;
            }
            ////判断
            if(responsibleUnit.ResponsibleUnitBranches.GroupBy(entity => new {
                entity.BranchId,
            }).Any(array => array.Count() > 1)) {
                UIHelper.ShowAlertMessage(CommonUIStrings.DataEditView_Validation_ResponsibleUnitBranch_BranchNameIsSole);
                return;
            }

            if(responsibleUnit.HasValidationErrors)
                return;
            ((IEditableObject)responsibleUnit).EndEdit();
            base.OnEditSubmitting();
        }

        protected override string BusinessName {
            get {
                return CommonUIStrings.DataEditView_BusinessName_ResponsibleUnit;
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetResponsibleUnitWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    //this.ResponsibleUnitBranchDataGridView.DataContext = entity;

                    this.SetObjectToEdit(entity);
                    SetTiledRegionId();
                }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void SetTiledRegionId() {
            var responsibleUnit = this.DataContext as ResponsibleUnit;
            if(responsibleUnit == null)
                return;
            var company = responsibleUnit.Company;
            if(company == null)
                return;
            if(company.ProvinceName == null)
                return;
            if(this.EnterpriseInformationDataEditPanel.KvProvinceNames.Any()) {
                company.ProvinceId = this.EnterpriseInformationDataEditPanel.KvProvinceNames.First(e => e.Value == company.ProvinceName).Key;
                if(company.CityName == null)
                    return;
                company.CityId = this.EnterpriseInformationDataEditPanel.KvCityNames.First(e => e.Value == company.CityName).Key;
                if(company.CountyName == null)
                    return;
                company.CountyId = this.EnterpriseInformationDataEditPanel.KvCountyNames.First(e => e.Value == company.CountyName).Key;
            } else {
                this.DomainContext.Load(DomainContext.GetTiledRegionsQuery().Where(ex => ex.CountyName != null).OrderBy(ex => ex.ProvinceName), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError)
                        return;
                    foreach(var repairObject in loadOp.Entities)
                        this.TiledRegions.Add(new TiledRegion {
                            Id = repairObject.Id,
                            ProvinceName = repairObject.ProvinceName,
                            CityName = repairObject.CityName,
                            CountyName = repairObject.CountyName,
                        });
                    this.EnterpriseInformationDataEditPanel.KvProvinceNames.Clear();
                    foreach(var item in TiledRegions) {
                        var values = this.EnterpriseInformationDataEditPanel.KvProvinceNames.Select(e => e.Value);
                        if(values.Contains(item.ProvinceName))
                            continue;
                        this.EnterpriseInformationDataEditPanel.KvProvinceNames.Add(new KeyValuePair {
                            Key = item.Id,
                            Value = item.ProvinceName
                        });
                    }
                    company.ProvinceId = this.EnterpriseInformationDataEditPanel.KvProvinceNames.First(e => e.Value == company.ProvinceName).Key;
                    if(company.CityName == null)
                        return;
                    // company.CityId = this.EnterpriseInformationDataEditPanel.KvCityNames.First(e => e.Value == company.CityName).Key;
                    var tempCityId = this.EnterpriseInformationDataEditPanel.KvCityNames.FirstOrDefault(e => e.Value == company.CityName);
                    if(tempCityId == null) {
                        UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_TiledRegion_TempCity);
                        return;
                    }
                    company.CityId = tempCityId.Key;
                    if(company.CountyName == null)
                        return;
                    company.CountyId = this.EnterpriseInformationDataEditPanel.KvCountyNames.First(e => e.Value == company.CountyName).Key;
                }, null);
            }
        }

        protected override void Reset() {
            var responsibleUnit = this.DataContext as ResponsibleUnit;
            if(responsibleUnit != null && (responsibleUnit.Company != null && this.DomainContext.Companies.Contains(responsibleUnit.Company))) {
                this.DomainContext.Companies.Detach(responsibleUnit.Company);
            }
            if(this.DomainContext.ResponsibleUnits.Contains(responsibleUnit))
                this.DomainContext.ResponsibleUnits.Detach(responsibleUnit);
        }
    }
}
