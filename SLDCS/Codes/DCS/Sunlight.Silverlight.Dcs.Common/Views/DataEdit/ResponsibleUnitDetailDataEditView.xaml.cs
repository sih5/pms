﻿using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class ResponsibleUnitDetailDataEditView {
        private DataGridViewBase responsibleUnitBranchDataGridView;
        private readonly string[] kvNames = {
            "MasterData_Status"
        };
        private KeyValueManager keyValueManager = new KeyValueManager();
        private FrameworkElement enterpriseInformationDetailPanel;
        private FrameworkElement EnterpriseInformationDetailPanel {
            get {
                return this.enterpriseInformationDetailPanel ?? (this.enterpriseInformationDetailPanel = DI.GetDetailPanel("EnterpriseInformation"));
            }
        }

        private DataGridViewBase ResponsibleUnitBranchDataGridView {
            get {
                if(this.responsibleUnitBranchDataGridView == null) {
                    this.responsibleUnitBranchDataGridView = DI.GetDataGridView("ResponsibleUnitBranch");
                    this.responsibleUnitBranchDataGridView.DomainContext = this.DomainContext;
                }
                return this.responsibleUnitBranchDataGridView;
            }
        }

        public ResponsibleUnitDetailDataEditView() {
            InitializeComponent();
            keyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            var responsibleUnitDetailDetailPanel = DI.GetDetailPanel("ResponsibleUnitDetail");
            this.EnterpriseInformationDetailPanel.SetValue(Grid.RowProperty, 1);
            var tabControl = new RadTabControl();
            tabControl.Items.Add(new RadTabItem {
                Header = CommonUIStrings.DataEditView_Text_ResponsibleUnitBranch,
                Content = this.ResponsibleUnitBranchDataGridView
            });
            tabControl.SetValue(Grid.ColumnProperty, 1);
            tabControl.SetValue(Grid.RowProperty, 0);
            tabControl.SetValue(Grid.RowSpanProperty, 5);
            this.Root.Children.Add(tabControl);
            this.Root.Children.Add(responsibleUnitDetailDetailPanel);
            this.Root.Children.Add(this.EnterpriseInformationDetailPanel);
            keyValueManager.LoadData();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetResponsibleUnitWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                this.DataContext = null;
                this.SetObjectToEdit(entity);
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override string BusinessName {
            get {
                return CommonUIStrings.DataEditView_BusinessName_ResponsibleUnit;
            }
        }

        public object KeyValueManager {
            get {
                return this.keyValueManager[this.kvNames[0]];
            }
        }
    }
}
