﻿
using System;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class ResponsibleUnitForSetupDataEditView {
        private DataGridViewBase responsibleUnitProductDetailForEditDataGridView;
        private DataGridViewBase ResponsibleUnitProductDetailForEditDataGridView {
            get {
                if(this.responsibleUnitProductDetailForEditDataGridView == null) {
                    this.responsibleUnitProductDetailForEditDataGridView = DI.GetDataGridView("ResponsibleUnitProductDetailForEdit");
                    this.responsibleUnitProductDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.responsibleUnitProductDetailForEditDataGridView;
            }
        }
        public ResponsibleUnitForSetupDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return CommonUIStrings.DataEditView_Title_Setup_ResponsibleUnitProductDetail;
            }
        }

        protected override void OnEditSubmitting() {
            var responsibleUnit = this.DataContext as ResponsibleUnit;
            if(responsibleUnit == null || !this.ResponsibleUnitProductDetailForEditDataGridView.CommitEdit())
                return;
            if(responsibleUnit.ResponsibleUnitProductDetails.Any(v => v.ProductId == default(int))) {
                UIHelper.ShowAlertMessage(CommonUIStrings.DataEditView_Validation_ResponsibleUnitProductDetail_ProductIsNull);
                return;
            }
            ((IEditableObject)responsibleUnit).EndEdit();
            foreach(var entity in responsibleUnit.ResponsibleUnitProductDetails) {
                if(entity.EntityState == EntityState.New) {
                    ((IEditableObject)entity).EndEdit();
                    try {
                        if(entity.Can新增责任单位产品关系)
                            entity.新增责任单位产品关系();
                    } catch(Exception ex) {
                        UIHelper.ShowAlertMessage(ex.Message);
                    }
                }
            }

            base.OnEditSubmitting();
        }

        private void CreateUI() {
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(CommonUIStrings.DataEditView_GroupTitle_ResponsibleUnitProductDetail, null, () => this.ResponsibleUnitProductDetailForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnSpanProperty, 3);
            detailDataEditView.SetValue(Grid.RowProperty, 1);
            this.LayoutRoot.Children.Add(detailDataEditView);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetResponsibleUnitWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();

                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
