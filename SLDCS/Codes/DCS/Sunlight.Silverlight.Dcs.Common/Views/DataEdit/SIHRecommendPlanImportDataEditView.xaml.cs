﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class SIHRecommendPlanImportDataEditView {
        private const string EXPORT_DATA_FILE_NAME = "导出推荐计划表模板.xlsx";
        private ICommand exportFileCommand;
        public SIHRecommendPlanImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return "导入推荐计划表实际计划数量";
            }
        }



        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        private void CreateUI() {
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.ImportSIHRecommendPlanAsync(fileName,0);
            this.ExcelServiceClient.ImportSIHRecommendPlanCompleted -= ExcelServiceClient_ImportSIHRecommendPlanCompleted;
            this.ExcelServiceClient.ImportSIHRecommendPlanCompleted += ExcelServiceClient_ImportSIHRecommendPlanCompleted;
        }

        private void ExcelServiceClient_ImportSIHRecommendPlanCompleted(object sender, ImportSIHRecommendPlanCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!this.HasImportingError) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImport, 5);
            } else {
                if(!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if(!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name ="结转时间",
                                                IsRequired = true
                                            },new ImportTemplateColumn {
                                                Name = "仓库编号",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "仓库名称"
                                            },
                                            new ImportTemplateColumn {
                                                Name = "配件编号",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "配件名称"
                                            },
                                            new ImportTemplateColumn {
                                                Name = "实际计划数量",
                                                IsRequired = true
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
    }
}