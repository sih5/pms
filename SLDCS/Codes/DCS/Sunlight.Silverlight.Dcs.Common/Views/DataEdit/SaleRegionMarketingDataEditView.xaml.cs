﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class SaleRegionMarketingDataEditView : INotifyPropertyChanged {
        private ObservableCollection<KeyValuePair> kvRegionCodes = new ObservableCollection<KeyValuePair>();
        private string regionName;

        public int SalesRegionIdForEdit {
            get;
            set;
        }

        private ObservableCollection<RegionMarketDptRelation> regionMarketDptRelations;

        public ObservableCollection<RegionMarketDptRelation> RegionMarketDptRelations {
            get {
                if(this.regionMarketDptRelations == null) {
                    this.regionMarketDptRelations = new ObservableCollection<RegionMarketDptRelation>();
                } return regionMarketDptRelations;
            }
        }

        private ObservableCollection<SalesRegion> salesRegion;

        public ObservableCollection<SalesRegion> SalesRegion {
            get {
                if(this.salesRegion == null) {
                    this.salesRegion = new ObservableCollection<SalesRegion>();
                } return salesRegion;
            }
        }

        private ObservableCollection<MarketingDepartment> marketingDepartment;

        public ObservableCollection<MarketingDepartment> MarketingDepartment {
            get {
                if(this.marketingDepartment == null) {
                    this.marketingDepartment = new ObservableCollection<MarketingDepartment>();
                } return marketingDepartment;
            }
        }

        private DataGridViewBase saleRegionMarketingDetailForEditDataGridView;

        public ObservableCollection<KeyValuePair> KvRegionCodes {
            get {
                if(kvRegionCodes == null)
                    kvRegionCodes = new ObservableCollection<KeyValuePair>();
                return this.kvRegionCodes;
            }
        }

        protected override string BusinessName {
            get {
                return CommonUIStrings.DataManagementView_Title_SaleRegionMarketing;
            }
        }

        public DataGridViewBase SaleRegionMarketingDetailForEditDataGridView {
            get {
                if(this.saleRegionMarketingDetailForEditDataGridView == null) {
                    this.saleRegionMarketingDetailForEditDataGridView = DI.GetDataGridView("SaleRegionMarketingDetailForEdit");
                    this.saleRegionMarketingDetailForEditDataGridView.DataContext = this;
                    this.saleRegionMarketingDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.saleRegionMarketingDetailForEditDataGridView;
            }
        }

        private void SaleRegionMarketingDataEditView_Loaded(object sender, RoutedEventArgs e) {
            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            this.KvRegionCodes.Clear();
            domainContext.Load(domainContext.GetSalesRegionsQuery().Where(s => s.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var SalesRegion in loadOp.Entities)
                    this.KvRegionCodes.Add(new KeyValuePair {
                        Key = SalesRegion.Id,
                        Value = SalesRegion.RegionCode,
                        UserObject = SalesRegion
                    });
            }, null);
        }

        public void regionCodeRadCombox_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            var regionCode = sender as RadComboBox;
            if(regionCode == null)
                return;
            var item = regionCode.SelectedItem as KeyValuePair;
            if(item == null)
                return;
            var salesRegion = item.UserObject as SalesRegion;
            if(item != null)
                regionNameRadTextBox.Text = salesRegion.RegionName;
        }

        public SaleRegionMarketingDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.Loaded += SaleRegionMarketingDataEditView_Loaded;
        }

        protected virtual void CreateUI() {
            var detail = new DcsDetailDataEditView();
            detail.Register(CommonUIStrings.DataGridView_Title_Common_MarketingDepartment, null, () => this.SaleRegionMarketingDetailForEditDataGridView);
            Grid.SetColumn(detail, 2);
            this.LayoutRoot.Children.Add(detail);
            this.regionCodeRadCombox.SelectionChanged += regionCodeRadCombox_SelectionChanged;
        }

        protected override void OnEditSubmitting() {
            if(!this.SaleRegionMarketingDetailForEditDataGridView.CommitEdit())
                return;
            if(string.IsNullOrWhiteSpace(this.RegionName)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_SaleRegion_CodeIsNull);
                return;
            }

            if(!this.RegionMarketDptRelations.Any()) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_Marketing_DetailNotNull);
                return;
            }
            foreach(var entity in this.RegionMarketDptRelations)
                entity.ValidationErrors.Clear();
            foreach(var entity in this.RegionMarketDptRelations) {
                entity.SalesRegionId = this.SalesRegionIdForEdit;
                entity.BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                entity.Status = (int)DcsBaseDataStatus.有效;
                ((IEditableObject)entity).EndEdit();
            }
            foreach(var item in this.DomainContext.RegionMarketDptRelations.Where(e => e.EntityState == EntityState.New).ToList()) {
                if(!RegionMarketDptRelations.Contains(item))
                    this.DomainContext.RegionMarketDptRelations.Remove(item);
            }
            if(this.RegionMarketDptRelations.Any(r => r.ValidationErrors.Any()))
                return;
            base.OnEditSubmitting();
        }

        protected override void OnEditCancelled() {
            if(RegionMarketDptRelations != null)
                this.RegionMarketDptRelations.Clear();
            this.SalesRegionIdForEdit = default(int);
            this.RegionName = string.Empty;
            base.OnEditCancelled();
        }

        protected override void OnEditSubmitted() {
            if(RegionMarketDptRelations != null)
                this.RegionMarketDptRelations.Clear();
            this.SalesRegionIdForEdit = default(int);
            this.RegionName = string.Empty;
            base.OnEditSubmitted();
        }

        private void LoadEntityToEdit(int id) {

            this.DomainContext.Load(this.DomainContext.GetSaleRegionMarketingWithMarketQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                this.RegionMarketDptRelations.Clear();
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    SalesRegionIdForEdit = entity.SalesRegionId;
                    entity.MarketingDepartment.SequeueNumber = loadOp.Entities.Count();
                    RegionMarketDptRelations.Add(entity);
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        public string RegionName {
            get {
                return this.regionName;
            }
            set {
                this.regionName = value;
                this.OnPropertyChanged("RegionName");
            }
        }

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
