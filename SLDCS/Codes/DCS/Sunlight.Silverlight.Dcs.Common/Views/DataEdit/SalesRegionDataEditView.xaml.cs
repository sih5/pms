﻿
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class SalesRegionDataEditView {
        public SalesRegionDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override string BusinessName {
            get {
                return CommonUIStrings.DataEditView_BusinessName_SalesRegion;
            }
        }

        protected override void OnEditSubmitting() {
            var salesRegion = this.DataContext as SalesRegion;
            if(salesRegion == null)
                return;
            salesRegion.ValidationErrors.Clear();

            if(salesRegion.HasValidationErrors)
                return;
            ((IEditableObject)salesRegion).EndEdit();
            base.OnEditSubmitting();
        }

        private void CreateUI() {
            // 创建主单编辑面板 
            this.isConent = true;
            this.Root.Children.Add(DI.GetDataEditPanel("SalesRegion"));
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetSalesRegionsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }
    }
}
