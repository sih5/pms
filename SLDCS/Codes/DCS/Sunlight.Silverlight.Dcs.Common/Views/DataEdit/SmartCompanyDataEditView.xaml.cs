﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class SmartCompanyDataEditView {
        private DataGridViewBase smartCompanyForEditDataGridView;
        private ObservableCollection<SmartCompany> smartCompany;
        public SmartCompanyDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
        private DataGridViewBase SmartCompanyForEditDataGridView {
            get {
                if(this.smartCompanyForEditDataGridView == null) {
                    this.smartCompanyForEditDataGridView = DI.GetDataGridView("SmartCompanyForEdit");
                    this.smartCompanyForEditDataGridView.DomainContext = this.DomainContext;
                    this.smartCompanyForEditDataGridView.DataContext = this;
                }
                return this.smartCompanyForEditDataGridView;
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(this.CreateVerticalLine(1, 0, 0, 0));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register("智能订货企业清单", null, () => this.SmartCompanyForEditDataGridView);
            Grid.SetColumn(detailDataEditView, 2);
            this.Root.Children.Add(detailDataEditView);
        }
        public ObservableCollection<SmartCompany> SmartCompanys {
            get {
                if(this.smartCompany == null) {
                    this.smartCompany = new ObservableCollection<SmartCompany>();
                } return smartCompany;
            }
        }
       
        protected override void OnEditSubmitting() {
            if(!this.SmartCompanyForEditDataGridView.CommitEdit())
                return;
            var detai = this.SmartCompanyForEditDataGridView.Entities.Cast<SmartCompany>().ToArray();
            if(!detai.Any()) {
                return;
            }
            foreach(var entity in detai)
                entity.ValidationErrors.Clear();

            if(this.DomainContext.IsBusy)
                return;
            ShellViewModel.Current.IsBusy = true;
            this.DomainContext.新增智能订货企业配置(detai, invokeOp => {
                if(invokeOp.HasError) {
                    ShellViewModel.Current.IsBusy = false;
                    if(!invokeOp.IsErrorHandled)
                        invokeOp.MarkErrorAsHandled();
                    var error = invokeOp.ValidationErrors.First();
                    if(error != null)
                        UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage);
                    else
                        DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                    return;
                }
                ShellViewModel.Current.IsBusy = false;
            }, null);
            base.OnEditSubmitting();
        }

        protected override string BusinessName {
            get {
                return "智能订货企业配置";
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }


        protected override void OnEditSubmitted() {
            if(this.SmartCompanys != null)
                SmartCompanys.Clear();
            base.OnEditSubmitted();
        }

        protected override void OnEditCancelled() {
            base.OnEditCancelled();
            if(this.SmartCompanys != null)
                SmartCompanys.Clear();
        }
    }
}
