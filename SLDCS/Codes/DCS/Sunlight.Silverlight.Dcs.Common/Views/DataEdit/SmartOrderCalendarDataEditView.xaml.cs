﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Core.Model;
using System.Collections.ObjectModel;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Core;
using Telerik.Windows.Controls;
using System.Windows;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class SmartOrderCalendarDataEditView {
        private ObservableCollection<Warehouse> kvWarehouses;
        public SmartOrderCalendarDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
        public ObservableCollection<Warehouse> KvWarehouses {
            get {
                return this.kvWarehouses ?? (this.kvWarehouses = new ObservableCollection<Warehouse>());
            }
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        protected override string BusinessName {
            get {
                return CommonUIStrings.QueryPanel_Title_SmartOrderCalendarTitle;
            }
        }

        protected override void OnEditSubmitting() {
            var smartOrderCalendar = this.DataContext as SmartOrderCalendar;
            if(smartOrderCalendar == null)
                return;
            smartOrderCalendar.ValidationErrors.Clear();

            if(smartOrderCalendar.WarehouseId ==null) {
                UIHelper.ShowNotification("请先选择仓库");
                return;
            }
            if(smartOrderCalendar.Times==null) {
                UIHelper.ShowNotification("请选择日期");
                return;
            }
            smartOrderCalendar.WarehouseCode = KvWarehouses.Where(y => y.Id == smartOrderCalendar.WarehouseId).First().Code;
            if(smartOrderCalendar.HasValidationErrors)
                return;
            ((IEditableObject)smartOrderCalendar).EndEdit();
            try {
                if(EditState == DataEditState.New) {
                    if(smartOrderCalendar.Can新增智能订货日历)
                        smartOrderCalendar.新增智能订货日历();
                } else {
                    if(smartOrderCalendar.Can修改智能订货日历)
                        smartOrderCalendar.修改智能订货日历();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        private void CreateUI() {
            this.DomainContext.Load(this.DomainContext.GetWarehousesByPartsSalesCategoryIdAndOwnerCompanyIdQuery(221,BaseApp.Current.CurrentUserData.EnterpriseId ).Where(r => r.Type != (int)DcsWarehouseType.虚拟库), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                this.KvWarehouses.Clear();
                if(loadOp.Entities != null) {
                    foreach(var item in loadOp.Entities)
                        this.KvWarehouses.Add(item);
                    this.WarehouseComboBox.ItemsSource = KvWarehouses;
                }
            }, null);

        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetSmartOrderCalendarsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
    }
}
