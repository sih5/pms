﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using System.Collections.ObjectModel;
using Sunlight.Silverlight.Core.Model;
using System.Windows;
using System.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class SmartProcessMethodDataEditView {
        private ObservableCollection<Warehouse> kvWarehouses;
        private ObservableCollection<KeyValuePair> kvOrderProcessMethods;
        public ObservableCollection<Warehouse> KvWarehouses {
            get {
                return this.kvWarehouses ?? (this.kvWarehouses = new ObservableCollection<Warehouse>());
            }
        }
        public ObservableCollection<KeyValuePair> KvOrderProcessMethods {
            get {
                return this.kvOrderProcessMethods ?? (this.kvOrderProcessMethods = new ObservableCollection<KeyValuePair>());
            }
        }
        public SmartProcessMethodDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
        private void CreateUI() {
            this.DomainContext.Load(this.DomainContext.GetWarehousesByPartsSalesCategoryIdAndOwnerCompanyIdQuery(221, BaseApp.Current.CurrentUserData.EnterpriseId).Where(r => r.Type != (int)DcsWarehouseType.虚拟库), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                this.KvWarehouses.Clear();
                if(loadOp.Entities != null) {
                    foreach(var item in loadOp.Entities)
                        this.KvWarehouses.Add(item);
                    this.WarehouseComboBox.ItemsSource = KvWarehouses;
                }
            }, null);
             this.DomainContext.Load(this.DomainContext.GetKeyValueItemsQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效 && v.Name == "PartsSalesOrderProcessDetail_ProcessMethod" && (v.Key == (int)DcsPartsSalesOrderProcessDetailProcessMethod.供应商直发 || v.Key == (int)DcsPartsSalesOrderProcessDetailProcessMethod.转中心库 || v.Key == (int)DcsPartsSalesOrderProcessDetailProcessMethod.转中心库采购 || v.Key == (int)DcsPartsSalesOrderProcessDetailProcessMethod.本部处理)), LoadBehavior.RefreshCurrent, loadOp3 => {
                    if(loadOp3.HasError)
                        return;
                    this.KvOrderProcessMethods.Clear();
                    foreach(var warehouse in loadOp3.Entities) {
                        this.KvOrderProcessMethods.Add(new KeyValuePair {
                            Key = warehouse.Key,
                            Value = warehouse.Value
                        });
                    }
             }, null);
        }

      
        protected override void OnEditSubmitting() {
            var smartProcessMethod = this.DataContext as SmartProcessMethod;
            if(smartProcessMethod == null)
                return;
            smartProcessMethod.ValidationErrors.Clear();
            if(smartProcessMethod.WarehouseId==null) {
                UIHelper.ShowNotification("请选择仓库");
                return;
            } 
            if(smartProcessMethod.OrderProcessMethod == null) {
                UIHelper.ShowNotification("请选择订单处理方式");
                return;
            }
            smartProcessMethod.WarehouseCode = KvWarehouses.Where(t => t.Id == smartProcessMethod.WarehouseId).First().Code;
            ((IEditableObject)smartProcessMethod).EndEdit();
            try {
                if(smartProcessMethod.Can新增智能订货处理方式配置)
                    smartProcessMethod.新增智能订货处理方式配置();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }
        protected override void OnEditCancelled() {
            base.OnEditCancelled();

        }
        protected override void OnEditSubmitted() {
            base.OnEditSubmitted();
        }
        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override string BusinessName {
            get {
                return "智能订货处理方式配置";
            }
        }      


    }
}
