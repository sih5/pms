﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class SparePartDataEditView {

        public SparePartDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void LoadEntityToEdit(int id) {
            //获取配件信息，包含组合件信息。获取关系：组合件.ParentId=配件信息.Id
            this.DomainContext.Load(this.DomainContext.GetSparePartsWithParentCombinedPartsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("SparePart"));
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_SparePart;
            }
        }

        protected override void OnEditSubmitting() {
            var sparePart = this.DataContext as SparePart;
            if(sparePart == null)
                return;
            sparePart.ValidationErrors.Clear();

            if(sparePart.PartType <= default(int))
                sparePart.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_SparePart_PartTypeIsNull, new[] {
                    "PartType"
                }));
            if(string.IsNullOrWhiteSpace(sparePart.MeasureUnit))
                sparePart.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_SparePart_MeasureUnitIsNull, new[] {
                    "MeasureUnit"
                }));
            if(sparePart.MInPackingAmount == null) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_SparePart_MInPackingAmount);
                return;
            }
            if(string.IsNullOrEmpty(sparePart.ReferenceCode)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_SparePart_ReferenceCode);
                return;
            }
            //if(string.IsNullOrEmpty(sparePart.GoldenTaxClassifyCode)) {
            //    UIHelper.ShowNotification("金税分类编码不允许为空");
            //    return;
            //}
            foreach(var combined in sparePart.ChildCombinedParts.Where(combined => combined.PartId == default(int))) {
                combined.ValidationErrors.Clear();
                combined.ValidationErrors.Add(new ValidationResult(CommonUIStrings.DataEditView_Validation_CombinedPart_PartIdIsNull, new[] {
                    "PartId"
                }));
                if(!combined.HasValidationErrors)
                    ((IEditableObject)combined).EndEdit();
            }
            if(sparePart.HasValidationErrors || sparePart.ChildCombinedParts.Any(combined => combined.HasValidationErrors))
                return;
            sparePart.Code = sparePart.Code.Trim().ToUpper();//变成大写
            if(sparePart.Code.Length > 25) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_SparePart_Code);
                return;
            }
            ((IEditableObject)sparePart).EndEdit();
            try {
                if (this.EditState == DataEditState.Edit) {
                    if (sparePart.Can修改配件基础信息)
                        sparePart.修改配件基础信息();
                }
                else if (this.EditState == DataEditState.New) {
                    if (sparePart.Can新增配件基础信息)
                        sparePart.新增配件基础信息();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
