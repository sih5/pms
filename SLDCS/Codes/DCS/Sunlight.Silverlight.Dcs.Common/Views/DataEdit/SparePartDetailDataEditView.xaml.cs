﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Dcs.Common.Panels.DataEdit;
using System.Windows;
using System.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit
{
    public partial class SparePartDetailDataEditView 
    {
        public SparePartDetailDataEditView()
        {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.FileUploadDataEditPanels.isVisibility = true;
        }
        protected override string BusinessName
        {
            get
            {
                return CommonUIStrings.DataEditPanel_BusinessName_SparePartDetail;
            }
        }
        private void LoadEntityToEdit(int id)
        {
            //获取配件信息，包含组合件信息。获取关系：组合件.ParentId=配件信息.Id
            this.DomainContext.Load(this.DomainContext.GetSparePartsWithParentCombinedPartsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                {
                    if (!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if (entity != null)
                {
                    this.FileUploadDataEditPanels.FilePath = entity.Path;
                    this.SetObjectToEdit(entity);
                }                  
            }, null);
        }
        public override void SetObjectToEditById(object id)
        {
            if (this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private void CreateUI()
        {
            this.Root.Children.Add(FileUploadDataEditPanels);
            FileUploadDataEditPanels.SetValue(Grid.RowProperty, 10);
            FileUploadDataEditPanels.SetValue(Grid.ColumnProperty, 1);
            FileUploadDataEditPanels.HorizontalAlignment = HorizontalAlignment.Center;
        }
        private SparePartForUploadDataEditPanel fileUploadDataEditPanels;
        protected SparePartForUploadDataEditPanel FileUploadDataEditPanels
        {
            get
            {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (SparePartForUploadDataEditPanel)DI.GetDataEditPanel("SparePartForUpload"));
            }
        }
    }
}
