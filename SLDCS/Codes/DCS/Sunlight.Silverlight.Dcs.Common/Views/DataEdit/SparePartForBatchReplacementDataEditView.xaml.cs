﻿
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.DataGrid;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class SparePartForBatchReplacementDataEditView {
        private DataGridViewBase sparePartsForImportDataGridView;
        private const string EXPORT_DATA_FILE_NAME = "导出配件信息批量替换模板.xlsx";
        private ICommand exportFileCommand;
        private string uniqueId;
        readonly List<ImportTemplateColumn> columnItemsValues = new List<ImportTemplateColumn>();
        private ObservableCollection<SparePartExtend> sparePartExtend;

        public ObservableCollection<SparePartExtend> SparePartExtends {
            get {
                return this.sparePartExtend ?? (this.sparePartExtend = new ObservableCollection<SparePartExtend>());
            }
        }
        private ObservableCollection<SparePartExtend> spareParts;

        public ObservableCollection<SparePartExtend> SpareParts {
            get {
                return this.spareParts ?? (this.spareParts = new ObservableCollection<SparePartExtend>());
            }
        }

        public SparePartForBatchReplacementDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected DataGridViewBase ImportDataGridView {
            get {
                if(this.sparePartsForImportDataGridView == null) {
                    this.sparePartsForImportDataGridView = DI.GetDataGridView("SparePartForBatchReplacement");
                    this.sparePartsForImportDataGridView.DomainContext = this.DomainContext;
                }
                return this.sparePartsForImportDataGridView;
            }
        }

        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = CommonUIStrings.DataEditView_Text_PartsBranch_ReplaceList,
                Content = this.ImportDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.columnItemsValues.Clear();
            switch(this.uniqueId) {
                case "ReplaceName":
                    this.ExcelServiceClient.批量替换配件名称Async(fileName);
                    this.ExcelServiceClient.批量替换配件名称Completed -= ExcelServiceClient_批量替换配件名称Completed;
                    this.ExcelServiceClient.批量替换配件名称Completed += ExcelServiceClient_批量替换配件名称Completed;
                    break;
                case "ReplaceSpecification":
                    this.ExcelServiceClient.批量替换规格型号Async(fileName);
                    this.ExcelServiceClient.批量替换规格型号Completed -= ExcelServiceClient_批量替换规格型号Completed;
                    this.ExcelServiceClient.批量替换规格型号Completed += ExcelServiceClient_批量替换规格型号Completed;
                    break;
                case "ReplaceEnglishName":
                    this.ExcelServiceClient.批量替换英文名称Async(fileName);
                    this.ExcelServiceClient.批量替换英文名称Completed -= ExcelServiceClient_批量替换英文名称Completed;
                    this.ExcelServiceClient.批量替换英文名称Completed += ExcelServiceClient_批量替换英文名称Completed;
                    break;
                case "ReplacePartType":
                    this.ExcelServiceClient.批量替换配件类型Async(fileName);
                    this.ExcelServiceClient.批量替换配件类型Completed -= ExcelServiceClient_批量替换配件类型Completed;
                    this.ExcelServiceClient.批量替换配件类型Completed += ExcelServiceClient_批量替换配件类型Completed;
                    break;
                case "ReplaceTraceProperty":
                    this.ExcelServiceClient.批量修改追溯属性Async(fileName);
                    this.ExcelServiceClient.批量修改追溯属性Completed -= ExcelServiceClient_批量修改追溯属性Completed;
                    this.ExcelServiceClient.批量修改追溯属性Completed += ExcelServiceClient_批量修改追溯属性Completed;
                    break;
                case "ReplaceFeature":
                    this.ExcelServiceClient.批量替换特征说明Async(fileName);
                    this.ExcelServiceClient.批量替换特征说明Completed -= ExcelServiceClient_批量替换特征说明Completed;
                    this.ExcelServiceClient.批量替换特征说明Completed += ExcelServiceClient_批量替换特征说明Completed;
                    break;
                case "ReplaceReferenceCode":
                    this.ExcelServiceClient.批量替换零部件图号Async(fileName);
                    this.ExcelServiceClient.批量替换零部件图号Completed -= ExcelServiceClient_批量替换零部件图号Completed;
                    this.ExcelServiceClient.批量替换零部件图号Completed += ExcelServiceClient_批量替换零部件图号Completed;
                    break;
                case "ReplaceMInPackingAmount":
                    this.ExcelServiceClient.批量替换最小包装数量Async(fileName);
                    this.ExcelServiceClient.批量替换最小包装数量Completed -= ExcelServiceClient_批量替换最小包装数量Completed;
                    this.ExcelServiceClient.批量替换最小包装数量Completed += ExcelServiceClient_批量替换最小包装数量Completed;
                    break;
                case "ReplaceNextSubstitute":
                    this.ExcelServiceClient.批量替换下一替代件Async(fileName);
                    this.ExcelServiceClient.批量替换下一替代件Completed -= ExcelServiceClient_批量替换下一替代件Completed;
                    this.ExcelServiceClient.批量替换下一替代件Completed += ExcelServiceClient_批量替换下一替代件Completed;
                    break;
                case "ReplaceIMSCompressionNumber":
                    this.ExcelServiceClient.批量替换IMS压缩号Async(fileName);
                    this.ExcelServiceClient.批量替换IMS压缩号Completed -= ExcelServiceClient_批量替换IMS压缩号Completed;
                    this.ExcelServiceClient.批量替换IMS压缩号Completed += ExcelServiceClient_批量替换IMS压缩号Completed;
                    break;
                case "ReplaceIMSManufacturerNumber":
                    this.ExcelServiceClient.批量替换IMS厂商号Async(fileName);
                    this.ExcelServiceClient.批量替换IMS厂商号Completed -= ExcelServiceClient_批量替换IMS厂商号Completed;
                    this.ExcelServiceClient.批量替换IMS厂商号Completed += ExcelServiceClient_批量替换IMS厂商号Completed;
                    break;
                case "ReplaceProductBrand":
                    this.ExcelServiceClient.批量替换产品商标Async(fileName);
                    this.ExcelServiceClient.批量替换产品商标Completed -= ExcelServiceClient_批量替换产品商标Completed;
                    this.ExcelServiceClient.批量替换产品商标Completed += ExcelServiceClient_批量替换产品商标Completed;
                    break;
                case "ReplaceStandardName":
                    this.ExcelServiceClient.批量替换标准名称Async(fileName);
                    this.ExcelServiceClient.批量替换标准名称Completed -= ExcelServiceClient_批量替换标准名称Completed;
                    this.ExcelServiceClient.批量替换标准名称Completed += ExcelServiceClient_批量替换标准名称Completed;
                    break;
                case "ReplaceImportEdit":
                    this.ExcelServiceClient.批量导入修改配件基本信息Async(fileName);
                    this.ExcelServiceClient.批量导入修改配件基本信息Completed -= ExcelServiceClient_批量导入修改配件基本信息Completed;
                    this.ExcelServiceClient.批量导入修改配件基本信息Completed += ExcelServiceClient_批量导入修改配件基本信息Completed;
                    break;
                case "ImportEdit":
                    this.ExcelServiceClient.批量停用配件信息Async(fileName);
                    this.ExcelServiceClient.批量停用配件信息Completed -= ExcelServiceClient_批量停用配件信息Completed;
                    this.ExcelServiceClient.批量停用配件信息Completed += ExcelServiceClient_批量停用配件信息Completed;
                    break;
                case "ResumeEdit":
                    this.ExcelServiceClient.批量恢复配件信息Async(fileName);
                    this.ExcelServiceClient.批量恢复配件信息Completed += ExcelServiceClient_批量恢复配件信息Completed;
                    break;
                case "ReplaceOverseasPartsFigure":
                    this.ExcelServiceClient.批量替换海外配件图号Async(fileName);
                    this.ExcelServiceClient.批量替换海外配件图号Completed += ExcelServiceClient_批量替换海外配件图号Completed;
                    break;
                case "ImportGoldenTaxClassify":
                    this.ExcelServiceClient.批量导入修改金税分类编码Async(fileName);
                    this.ExcelServiceClient.批量导入修改金税分类编码Completed += ExcelServiceClient_批量导入修改金税分类编码Completed;
                    break;
                case "StandardImport":
                    this.ExcelServiceClient.批量替换产品执行标准代码Async(fileName);
                    this.ExcelServiceClient.批量替换产品执行标准代码Completed += ExcelServiceClient_批量替换产品执行标准代码Completed;
                    break;
                case "ImportExchangeIdentification":
                    this.ExcelServiceClient.批量替换配件互换识别号Async(fileName);
                    this.ExcelServiceClient.批量替换配件互换识别号Completed += ExcelServiceClient_批量替换配件互换识别号Completed;
                    break;
                case "ImportFactury":
                    this.ExcelServiceClient.批量替换配件厂商Async(fileName);
                    this.ExcelServiceClient.批量替换配件厂商Completed += ExcelServiceClient_批量替换配件厂商Completed;
                    break;
                case "ImportImportCategoryCode":
                    this.ExcelServiceClient.批量替换配件分类编码Async(fileName);
                    this.ExcelServiceClient.批量替换配件分类编码Completed += ExcelServiceClient_批量替换配件分类编码Completed;
                    break;
                case "ReplaceWarrantyTransfer":
                    this.ExcelServiceClient.批量替换保外调拨Async(fileName);
                    this.ExcelServiceClient.批量替换保外调拨Completed += ExcelServiceClient_批量替换保外调拨Completed;
                    break;
                case "ImportIsSupplier":
                    this.ExcelServiceClient.批量替换是否供应商投放Async(fileName);
                    this.ExcelServiceClient.批量替换是否供应商投放Completed += ExcelServiceClient_批量替换是否供应商投放Completed;
                    break;
                case "ReplaceSafeDays":
                    this.ExcelServiceClient.批量修改安全天数最大值Async(fileName);
                    this.ExcelServiceClient.批量修改安全天数最大值Completed += ExcelServiceClient_批量修改安全天数最大值Completed;
                    break;
                case "ReplaceWarehousDays":
                    this.ExcelServiceClient.批量修改库房天数Async(fileName);
                    this.ExcelServiceClient.批量修改库房天数Completed += ExcelServiceClient_批量修改库房天数Completed;
                    break;
                case "ReplaceTemDays":
                    this.ExcelServiceClient.批量修改临时天数Async(fileName);
                    this.ExcelServiceClient.批量修改临时天数Completed += ExcelServiceClient_批量修改临时天数Completed;
                    break;
            }

        }
        private void ExcelServiceClient_批量修改安全天数最大值Completed(object sender, 批量修改安全天数最大值CompletedEventArgs e) {
            this.ExcelServiceClient_Completed(e.rightData, e.errorDataFileName, e.errorMessage);
        }

        private void ExcelServiceClient_批量修改库房天数Completed(object sender, 批量修改库房天数CompletedEventArgs e) {
            this.ExcelServiceClient_Completed(e.rightData, e.errorDataFileName, e.errorMessage);
        }

        private void ExcelServiceClient_批量修改临时天数Completed(object sender, 批量修改临时天数CompletedEventArgs e) {
            this.ExcelServiceClient_Completed(e.rightData, e.errorDataFileName, e.errorMessage);
        }


        private void ExcelServiceClient_批量替换是否供应商投放Completed(object sender, 批量替换是否供应商投放CompletedEventArgs e) {
            this.ExcelServiceClient_Completed(e.rightData, e.errorDataFileName, e.errorMessage);
        }
        private void ExcelServiceClient_批量替换保外调拨Completed(object sender, 批量替换保外调拨CompletedEventArgs e) {
            this.ExcelServiceClient_Completed(e.rightData, e.errorDataFileName, e.errorMessage);
        }

        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((string)id));
            else
                this.LoadEntityToEdit((string)id);
        }

        private void LoadEntityToEdit(string uniqueId) {
            this.uniqueId = uniqueId;
            this.SparePartExtends.Clear();
            this.SpareParts.Clear();
            this.columnItemsValues.Clear();
            this.PauseButton.Visibility = Visibility.Collapsed;
            this.ReuseButton.Visibility = Visibility.Collapsed;
            switch(uniqueId) {
                case "ReplaceWarrantyTransfer":
                    this.Title = CommonUIStrings.DataEditPanel_Title_ReplaceWarrantyTransfer;
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        IsRequired = true
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_IsTransfer,
                        IsRequired = true
                    });
                    break;
                case "ReplaceName":
                    this.Title = CommonUIStrings.DataEditPanel_Title_ReplaceName;
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        IsRequired = true
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.DataDeTailPanel_Text_BottomStock_SparePartName,
                        IsRequired = true
                    });
                    break;
                case "StandardImport":
                    this.Title = CommonUIStrings.DataEditPanel_Title_StandardImport;
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        IsRequired = true
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.DataDeTailPanel_Text_BottomStock_SparePartName,
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_Title_ProductStandard_StandardCode,
                    });
                    break;
                case "ImportEdit":
                    this.PauseButton.Visibility = Visibility.Visible;
                    this.Title = CommonUIStrings.Action_Title_Batch_Stop;
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        IsRequired = true
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.DataDeTailPanel_Text_BottomStock_SparePartName,
                        IsRequired = false
                    });
                    break;
                case "ResumeEdit":
                    this.ReuseButton.Visibility = Visibility.Visible;
                    this.Title = CommonUIStrings.Action_Title_Batch_Recovery;
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        IsRequired = true
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.DataDeTailPanel_Text_BottomStock_SparePartName,
                        IsRequired = false
                    });
                    break;
                case "ReplaceSpecification":
                    this.Title = CommonUIStrings.DataEditPanel_Title_ReplaceSpecification;
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        IsRequired = true
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_Specification,
                        //IsRequired = true
                    });
                    break;
                case "ReplaceEnglishName":
                    this.Title = CommonUIStrings.DataEditPanel_Title_ReplaceEnglishName;
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        IsRequired = true
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_EnglishName,
                        //IsRequired = true
                    });
                    break;
                case "ReplacePartType":
                    this.Title = CommonUIStrings.DataEditPanel_Title_ReplacePartType;
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        IsRequired = true
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_PartType,
                        IsRequired = true
                    });
                    break;
                case "ReplaceFeature":
                    this.Title = CommonUIStrings.DataEditPanel_Title_ReplaceFeature;
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        IsRequired = true
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name =CommonUIStrings.DataEditPanel_Text_SparePart_Feature,
                        //IsRequired = true
                    });
                    break;
                case "ReplaceReferenceCode":
                    this.Title = CommonUIStrings.DataEditPanel_Title_ReplaceReferenceCode;
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        IsRequired = true
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                        IsRequired = true
                    });
                    break;
                case "ReplaceMInPackingAmount":
                    this.Title = CommonUIStrings.DataEditPanel_Title_ReplaceMInPackingAmount;
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        IsRequired = true
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_MInPackingAmount,
                        //IsRequired = true
                    });
                    break;
                case "ReplaceNextSubstitute":
                    this.Title = CommonUIStrings.DataEditPanel_Title_ReplaceNextSubstitute;
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        IsRequired = true
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_NextSubstitute,
                        //IsRequired = true
                    });
                    break;
                case "ReplaceIMSCompressionNumber":
                    this.Title = CommonUIStrings.DataEditPanel_Title_ReplaceIMSCompressionNumber;
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        IsRequired = true
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_IMSCompressionNumber,
                        //IsRequired = true
                    });
                    break;
                case "ReplaceIMSManufacturerNumber":
                    this.Title = CommonUIStrings.DataEditPanel_Title_ReplaceIMSManufacturerNumber;
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        IsRequired = true
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_IMSManufacturer,
                        //IsRequired = true
                    });
                    break;
                case "ReplaceProductBrand":
                    this.Title = CommonUIStrings.DataEditPanel_Title_ReplaceProductBrand;
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        IsRequired = true
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_SparePartProductBrand,
                        //IsRequired = true
                    });
                    break;
                case "ReplaceStandardName":
                    this.Title = CommonUIStrings.DataEditPanel_Title_ReplaceStandardName;
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        IsRequired = true
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.DataDeTailPanel_Text_BottomStock_SparePartName,
                        IsRequired = true
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_StandardName,
                        //IsRequired = true
                    });
                    break;
                case "ReplaceImportEdit":
                    this.Title = CommonUIStrings.DataEditPanel_Title_ReplaceImportEdit;
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        IsRequired = true
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.DataDeTailPanel_Text_BottomStock_SparePartName,
                        IsRequired = false
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_PartsExchangeGroup_ExchangeCode
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.DataEditPanel_Text_SparePart_OverseasPartsFigure
                    });
                    //this.columnItemsValues.Add(new ImportTemplateColumn {
                    //    Name = "产品商标"
                    //});
                    //this.columnItemsValues.Add(new ImportTemplateColumn {
                    //    Name = "IMS压缩号"
                    //});
                    this.columnItemsValues.Add(new ImportTemplateColumn
                    {
                        Name = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_IMSManufacturer
                    });
                    //this.columnItemsValues.Add(new ImportTemplateColumn {
                    //    Name = "产品执行标准代码",
                    //});
                    //this.columnItemsValues.Add(new ImportTemplateColumn {
                    //    Name = "产品执行标准名称"
                    //});
                    //this.columnItemsValues.Add(new ImportTemplateColumn {
                    //    Name = "研究院图号"
                    //});
                    //this.columnItemsValues.Add(new ImportTemplateColumn {
                    //    Name = "规格型号"
                    //});
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_EnglishName
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_PartType
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_Title_FaultyParts_MeasureUnit
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_MInPackingAmount
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_ShelfLife
                    });
                    //this.columnItemsValues.Add(new ImportTemplateColumn {
                    //    Name = CommonUIStrings.DataEditPanel_Validation_SparePartForBatch_GroupABCCategory
                    //});
                    //this.columnItemsValues.Add(new ImportTemplateColumn {
                    //    Name = "上一替代件"
                    //});
                    //this.columnItemsValues.Add(new ImportTemplateColumn {
                    //    Name = "下一替代件"
                    //});
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_Weight
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_Volume
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name =CommonUIStrings.DataEditPanel_Text_SparePart_Feature
                    });
                    //this.columnItemsValues.Add(new ImportTemplateColumn {
                    //    Name = CommonUIStrings.DataEditPanel_Validation_SparePartForBatch_Assembly
                    //});
                    //this.columnItemsValues.Add(new ImportTemplateColumn {
                    //    Name = "厂家"
                    //});
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.DataEditPanel_Text_SparePart_IsOriginal
                    });
                    //this.columnItemsValues.Add(new ImportTemplateColumn {
                    //    Name = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_CategoryCode
                    //});
                    //this.columnItemsValues.Add(new ImportTemplateColumn {
                    //    Name = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_CategoryName
                    //});
                    this.columnItemsValues.Add(new ImportTemplateColumn
                    {
                        Name = CommonUIStrings.DataEditPanel_Text_SparePart_PinyinCode
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn
                    {
                        Name = CommonUIStrings.DataEditPanel_Text_SparePart_Material
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn
                    {
                        Name = CommonUIStrings.DataEditPanel_Text_SparePart_DeclareElement
                    });
                    break;
                case "ReplaceOverseasPartsFigure":
                    this.Title = CommonUIStrings.DataEditPanel_Title_ReplaceOverseasPartsFigure;
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        IsRequired = true
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.DataEditPanel_Text_SparePart_OverseasPartsFigure,
                        IsRequired = true
                    });
                    break;
                case "ImportGoldenTaxClassify":
                    this.Title = CommonUIStrings.DataEditPanel_Title_ImportGoldenTaxClassify;
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        IsRequired = true
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_GoldenTaxClassifyMsg_GoldenTaxClassifyCode,
                        IsRequired = true
                    });
                    break;
                case "ImportExchangeIdentification":
                    this.Title = CommonUIStrings.DataEditPanel_Title_ImportExchangeIdentification;
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        IsRequired = true
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.DataDeTailPanel_Text_BottomStock_SparePartName
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_PartsExchangeGroup_ExchangeCode
                    });
                    break;
                case "ImportFactury":
                    this.Title = CommonUIStrings.DataEditPanel_Title_ImportFactury;
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        IsRequired = true
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_Factury
                    });
                    break;
                case "ImportImportCategoryCode":
                    this.Title = CommonUIStrings.DataEditPanel_Title_ImportImportCategoryCode;
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        IsRequired = true
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_CategoryCode
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_CategoryName
                    });
                    break;
                case "ImportIsSupplier":
                    this.Title = CommonUIStrings.Action_Title_Upload_IsSupplier;
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        IsRequired = true
                    });                  
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.DataEditPanel_Title_ImportIsSupplierPutIn,
                        IsRequired = true
                    });
                    break;
                case "ReplaceTraceProperty":
                    this.Title = "批量修改追溯属性";
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        IsRequired = true
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = "追溯属性",
                        IsRequired = true
                    });
                    break;
                case "ReplaceSafeDays":
                    this.Title = "批量修改安全天数最大值";
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        IsRequired = true
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = "安全天数最大值",
                        IsRequired = true
                    });
                    break;
                case "ReplaceWarehousDays":
                    this.Title = "批量修改库房天数";
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        IsRequired = true
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = "库房天数",
                        IsRequired = true
                    });
                    break;
                case "ReplaceTemDays":
                    this.Title = "批量修改临时天数";
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        IsRequired = true
                    });
                    this.columnItemsValues.Add(new ImportTemplateColumn {
                        Name = "临时天数",
                        IsRequired = true
                    });
                    break;
            }
            var gridView = this.ImportDataGridView as SparePartForBatchReplacementDataGridView;
            if(gridView == null)
                return;
            gridView.ExchangeData(null, uniqueId, null);
        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, this.columnItemsValues, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                }
                if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                    this.ExportFile(loadOp.Value);
            }, null));
        }

        private void ExcelServiceClient_批量替换最小包装数量Completed(object sender, 批量替换最小包装数量CompletedEventArgs e) {
            this.ExcelServiceClient_Completed(e.rightData, e.errorDataFileName, e.errorMessage);
        }

        private void ExcelServiceClient_批量替换下一替代件Completed(object sender, 批量替换下一替代件CompletedEventArgs e) {
            this.ExcelServiceClient_Completed(e.rightData, e.errorDataFileName, e.errorMessage);
        }

        private void ExcelServiceClient_批量替换零部件图号Completed(object sender, 批量替换零部件图号CompletedEventArgs e) {
            this.ExcelServiceClient_Completed(e.rightData, e.errorDataFileName, e.errorMessage);
        }

        private void ExcelServiceClient_批量替换IMS压缩号Completed(object sender, 批量替换IMS压缩号CompletedEventArgs e) {
            this.ExcelServiceClient_Completed(e.rightData, e.errorDataFileName, e.errorMessage);
        }

        private void ExcelServiceClient_批量替换IMS厂商号Completed(object sender, 批量替换IMS厂商号CompletedEventArgs e) {
            this.ExcelServiceClient_Completed(e.rightData, e.errorDataFileName, e.errorMessage);
        }

        private void ExcelServiceClient_批量替换特征说明Completed(object sender, 批量替换特征说明CompletedEventArgs e) {
            this.ExcelServiceClient_Completed(e.rightData, e.errorDataFileName, e.errorMessage);
        }

        private void ExcelServiceClient_批量替换配件类型Completed(object sender, 批量替换配件类型CompletedEventArgs e) {
            this.ExcelServiceClient_Completed(e.rightData, e.errorDataFileName, e.errorMessage);
        }
        private void ExcelServiceClient_批量修改追溯属性Completed(object sender, 批量修改追溯属性CompletedEventArgs e) {
            this.ExcelServiceClient_Completed(e.rightData, e.errorDataFileName, e.errorMessage);
        }

        private void ExcelServiceClient_批量替换英文名称Completed(object sender, 批量替换英文名称CompletedEventArgs e) {
            this.ExcelServiceClient_Completed(e.rightData, e.errorDataFileName, e.errorMessage);
        }

        private void ExcelServiceClient_批量替换规格型号Completed(object sender, 批量替换规格型号CompletedEventArgs e) {
            this.ExcelServiceClient_Completed(e.rightData, e.errorDataFileName, e.errorMessage);
        }

        private void ExcelServiceClient_批量替换配件名称Completed(object sender, 批量替换配件名称CompletedEventArgs e) {
            this.ExcelServiceClient_Completed(e.rightData, e.errorDataFileName, e.errorMessage);
        }

        private void ExcelServiceClient_批量替换产品商标Completed(object sender, 批量替换产品商标CompletedEventArgs e) {
            this.ExcelServiceClient_Completed(e.rightData, e.errorDataFileName, e.errorMessage);
        }
        private void ExcelServiceClient_批量替换标准名称Completed(object sender, 批量替换标准名称CompletedEventArgs e) {
            this.ExcelServiceClient_Completed(e.rightData, e.errorDataFileName, e.errorMessage);
        }
        private void ExcelServiceClient_批量导入修改配件基本信息Completed(object sender, 批量导入修改配件基本信息CompletedEventArgs e) {
            this.ExcelServiceClient_Completed(e.rightData, e.errorDataFileName, e.errorMessage);
        }
        private void ExcelServiceClient_批量恢复配件信息Completed(object sender, 批量恢复配件信息CompletedEventArgs e) {
            this.ExcelServiceClient_Completed(e.rightData, e.errorDataFileName, e.errorMessage);
        }

        private void ExcelServiceClient_批量停用配件信息Completed(object sender, 批量停用配件信息CompletedEventArgs e) {
            this.ExcelServiceClient_Completed(e.rightData, e.errorDataFileName, e.errorMessage);
        }
        private void ExcelServiceClient_批量替换海外配件图号Completed(object sender, 批量替换海外配件图号CompletedEventArgs e) {
            this.ExcelServiceClient_Completed(e.rightData, e.errorDataFileName, e.errorMessage);
        }

        private void ExcelServiceClient_批量导入修改金税分类编码Completed(object sender, 批量导入修改金税分类编码CompletedEventArgs e) {
            this.ExcelServiceClient_Completed(e.rightData, e.errorDataFileName, e.errorMessage);
        }


        private void ExcelServiceClient_批量替换产品执行标准代码Completed(object sender, 批量替换产品执行标准代码CompletedEventArgs e) {
            this.ExcelServiceClient_Completed(e.rightData, e.errorDataFileName, e.errorMessage);
        }

        private void ExcelServiceClient_批量替换配件互换识别号Completed(object sender, 批量替换配件互换识别号CompletedEventArgs e) {
            this.ExcelServiceClient_Completed(e.rightData, e.errorDataFileName, e.errorMessage);
        }
        private void ExcelServiceClient_批量替换配件厂商Completed(object sender, 批量替换配件厂商CompletedEventArgs e) {
            this.ExcelServiceClient_Completed(e.rightData, e.errorDataFileName, e.errorMessage);
        }
        private void ExcelServiceClient_批量替换配件分类编码Completed(object sender, 批量替换配件分类编码CompletedEventArgs e) {
            this.ExcelServiceClient_Completed(e.rightData, e.errorDataFileName, e.errorMessage);
        }
        private void ExcelServiceClient_Completed(SparePartExtend[] sparePartExtends, string errorDataFileName, string errorMessage) {
            this.SparePartExtends.Clear();
            this.SpareParts.Clear();
            if(sparePartExtends.Any()) {
                var i = 1;
                foreach(var sparePart in sparePartExtends) {
                    sparePart.SerialNumber = i;
                    this.SparePartExtends.Add(sparePart);
                    this.SpareParts.Add(sparePart);
                    i++;
                }
            }
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(errorDataFileName)) || (!string.IsNullOrWhiteSpace(errorMessage));
            if(!this.HasImportingError) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImport, 5);
            } else {
                if(!string.IsNullOrEmpty(errorMessage))
                    UIHelper.ShowAlertMessage(errorMessage);
                if(!string.IsNullOrEmpty(errorDataFileName))
                    this.ExportFile(errorDataFileName);
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }

        private void ReuseButton_Click(object sender, RoutedEventArgs e) {
            var entitieExtends = this.SpareParts;

            if(entitieExtends.Count <= 0) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditPanel_Validation_SparePartForBatch_RecoverySparepart);
                return;
            }
            var spareParExtends = entitieExtends.Cast<SparePartExtend>();

            var codes = spareParExtends.Select(v => v.Code).ToArray();
            this.DomainContext.Load(this.DomainContext.GetSparePartsByCode2Query(codes), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    if(loadOp.IsErrorHandled) {
                        loadOp.MarkErrorAsHandled();
                        return;
                    }
                var spareParts = loadOp.Entities.ToList();
                if(spareParts == null)
                    return;
                try {
                    this.DomainContext.批量恢复配件信息(spareParts, invokeOp => {
                        if(invokeOp.HasError) {
                            if(!invokeOp.IsErrorHandled)
                                invokeOp.MarkErrorAsHandled();
                            var error = invokeOp.ValidationErrors.FirstOrDefault();
                            if(error != null) {
                                UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage, 5);
                            } else {
                                DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                            }
                            DomainContext.RejectChanges();
                            return;
                        } else {
                            UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_PartsPurchasePricingChange_Success);
                            this.SpareParts.Clear();

                        }

                    }, null);
                } catch(ValidationException ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                    return;
                }

            }, null);

        }

        private void PauseButton_Click(object sender, RoutedEventArgs e) {
            var entitieExtends = this.SpareParts;

            if(entitieExtends.Count <= 0) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditPanel_Validation_SparePartForBatch_StopSparepart);
                return;
            }
            var spareParExtends = entitieExtends.Cast<SparePartExtend>();

            var codes = spareParExtends.Select(v => v.Code).ToArray();
            this.DomainContext.Load(this.DomainContext.GetSparePartsByCode2Query(codes), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    if(loadOp.IsErrorHandled) {
                        loadOp.MarkErrorAsHandled();
                        return;
                    }
                var spareParts = loadOp.Entities.ToList();
                if(spareParts == null)
                    return;

                try {
                    this.DomainContext.批量停用配件信息(spareParts, invokeOp => {
                        if(invokeOp.HasError) {
                            if(!invokeOp.IsErrorHandled)
                                invokeOp.MarkErrorAsHandled();
                            var error = invokeOp.ValidationErrors.FirstOrDefault();
                            if(error != null) {
                                UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage, 5);
                            } else {
                                DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                            }
                            DomainContext.RejectChanges();
                            return;
                        } else {
                            UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_PartsPurchasePricingChange_Success);
                            this.SpareParts.Clear();
                        }

                    }, null);
                } catch(ValidationException ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                    return;
                }
            }, null);
        }
    }
}