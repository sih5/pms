﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class SparePartForImportDataEditView {
        private DataGridViewBase sparePartsForImportDataGridView;
        private const string EXPORT_DATA_FILE_NAME = "导出配件信息模板.xlsx";
        private ICommand exportFileCommand;
        public SparePartForImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return CommonUIStrings.DataEditPanel_Validation_SparePartForBatch_SparePartForImport;
            }
        }

        protected DataGridViewBase ImportDataGridView {
            get {
                return this.sparePartsForImportDataGridView ?? (this.sparePartsForImportDataGridView = DI.GetDataGridView("SparePartForImport"));
            }
        }

        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = CommonUIStrings.DataEditPanel_Validation_SparePartForBatch_SparePartForImportList,
                Content = this.ImportDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.ImportSparepartAsync(fileName);
            this.ExcelServiceClient.ImportSparepartCompleted -= ExcelServiceClient_ImportSparepartCompleted;
            this.ExcelServiceClient.ImportSparepartCompleted += ExcelServiceClient_ImportSparepartCompleted;
        }

        private void ExcelServiceClient_ImportSparepartCompleted(object sender, ImportSparepartCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!this.HasImportingError) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImport, 5);
            } else {
                if(!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if(!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name =CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataDeTailPanel_Text_BottomStock_SparePartName,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name =CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_ReferenceCode,
                                                IsRequired=true 
                                            }, new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_PartsExchangeGroup_ExchangeCode
                                            }, new ImportTemplateColumn {
                                                Name =  CommonUIStrings.QueryPanel_QueryItem_Title_GoldenTaxClassifyMsg_GoldenTaxClassifyCode,
                                                 IsRequired = true
                                            },new ImportTemplateColumn{
                                                 Name=CommonUIStrings.QueryPanel_QueryItem_Title_GoldenTaxClassifyMsg_GoldenTaxClassifyName,
                                                  IsRequired = true
                                            },new ImportTemplateColumn{
                                                Name=CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_OverseasPartsFigure
                                            },new ImportTemplateColumn {
                                              Name = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_SparePartProductBrand,
                                            },new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_IMSCompressionNumber
                                            },
                                             new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_IMSManufacturer
                                            },new ImportTemplateColumn{
                                                Name=CommonUIStrings.QueryPanel_Title_ProductStandard_StandardCode
                                            },new ImportTemplateColumn {
                                              Name = CommonUIStrings.DataEditView_ProductStandard_StandardName
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditPanel_Text_SparePart_CADCode,
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_Specification,
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_EnglishName,
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_PartType,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_Title_FaultyParts_MeasureUnit,
                                                IsRequired = true
                                            },new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_MInPackingAmount
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_ShelfLife,
                                            },new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditPanel_Validation_SparePartForBatch_GroupABCCategory,
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditPanel_Validation_SparePartForBatch_LastSubstitute,
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_NextSubstitute,
                                            },
                                            new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_Weight,
                                            },new ImportTemplateColumn {
                                                Name = CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_Volume,
                                            },
                                            new ImportTemplateColumn {
                                                Name =CommonUIStrings.DataEditPanel_Text_SparePart_Feature,
                                            }, new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditPanel_Validation_SparePartForBatch_Assembly,
                                            }, new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_Factury,  
                                            }, new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditPanel_Text_SparePart_IsOriginal,
                                            }, new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_IsTransfer,
                                            }, new ImportTemplateColumn {
                                                Name = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_CategoryCode,
                                            },new ImportTemplateColumn{
                                                 Name=CommonUIStrings.DataEditPanel_Text_SparePartForBatch_CategoryName
                                            }

                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
    }
}