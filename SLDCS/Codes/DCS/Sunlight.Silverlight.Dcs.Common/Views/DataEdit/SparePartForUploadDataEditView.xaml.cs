﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.QueryWindow;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Common.Panels.DataEdit;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit
{
    public partial class SparePartForUploadDataEditView 
    {
        public SparePartForUploadDataEditView()
        {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI()
        {
            this.Attachment.Children.Add(FileUploadDataEditPanels);
        }
        private void LoadEntityToEdit(int id)
        {
            //获取配件信息，包含组合件信息。获取关系：组合件.ParentId=配件信息.Id
            this.DomainContext.Load(this.DomainContext.GetSparePartsWithParentCombinedPartsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if (loadOp.HasError)
                {
                    if (!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if (entity != null)
                {
                    this.FileUploadDataEditPanels.FilePath = entity.Path;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
        public override void SetObjectToEditById(object id)
        {
            if (this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private SparePartForUploadDataEditPanel fileUploadDataEditPanels;
        protected SparePartForUploadDataEditPanel FileUploadDataEditPanels
        {
            get
            {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (SparePartForUploadDataEditPanel)DI.GetDataEditPanel("SparePartForUpload"));
            }
        }
        protected override string Title
        {
            get
            {
                return CommonUIStrings.Action_Title_Upload_Attachments;
            }
        }
        protected override void OnEditSubmitting()
        {
            var sparePart = this.DataContext as SparePart;
            if (sparePart == null)
                return;
            sparePart.ValidationErrors.Clear();
            sparePart.Path = FileUploadDataEditPanels.FilePath;
            if (string.IsNullOrWhiteSpace(sparePart.Path))
            {
                UIHelper.ShowNotification(CommonUIStrings.DataEditPanel_Validation_SparePartForBatch_NewAttach);
                return;
            }
            ((IEditableObject)sparePart).EndEdit();
            try
            {
                if (sparePart.Can上传配件附件)
                    sparePart.上传配件附件();
            }
            catch (Exception ex)
            {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }
        protected override bool OnRequestCanSubmit()
        {
            return true;
        }
    }
}
