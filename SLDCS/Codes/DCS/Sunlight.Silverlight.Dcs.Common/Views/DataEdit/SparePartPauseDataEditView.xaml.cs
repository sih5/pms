﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class SparePartPauseDataEditView {
        protected bool isPaurse;

        public SparePartPauseDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("SparePartPause"));
        }

        protected void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetSparePartsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            this.isPaurse = true;
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override string Title {
            get {
                return CommonUIStrings.DataEditView_Title_Pause_SparePart;
            }
        }

        protected override void OnEditSubmitting() {
            if(this.isPaurse) {
                var sparePart = this.DataContext as SparePart;
                if(sparePart == null)
                    return;
                sparePart.ValidationErrors.Clear();
                ((IEditableObject)sparePart).EndEdit();
                //try {
                //    if(sparePart.Can停用配件基础信息)
                //        sparePart.停用配件基础信息();
                //} catch(ValidationException ex) {
                //    UIHelper.ShowAlertMessage(ex.Message);
                //    return;
                //}
                this.DomainContext.停用配件基础信息(sparePart, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    this.DomainContext.RejectChanges();
                    base.OnEditSubmitting();
                }, null);
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }
    }
}
