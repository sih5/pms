﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class SparePartResume2DataEditView {
         protected bool isResume;

         public SparePartResume2DataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("SparePartResume2"));
        }

        protected void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetSparePartsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            this.isResume = true;
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override string Title {
            get {
                return CommonUIStrings.DataEditView_Title_Resume_SparePart;
            }
        }

        protected override void OnEditSubmitting() {
            if(this.isResume) {
                var sparePart = this.DataContext as SparePart;
                if(sparePart == null)
                    return;
                sparePart.ValidationErrors.Clear();
                ((IEditableObject)sparePart).EndEdit();
                this.DomainContext.恢复配件基础信息(sparePart, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    this.DomainContext.RejectChanges();
                    base.OnEditSubmitting();
                }, null);
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }
    }
}
