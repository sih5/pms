﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public class SparePartResumeDataEditView : SparePartPauseDataEditView {
        protected override string Title {
            get {
                return CommonUIStrings.DataEditView_Title_Resume_SparePart;
            }
        }

        protected override void OnEditSubmitting() {
            //var sparePart = this.DataContext as SparePart;
            //if(sparePart == null)
            //    return;
            //sparePart.ValidationErrors.Clear();
            //((IEditableObject)sparePart).EndEdit();
            //try {
            //    if(sparePart.Can恢复配件基础信息)
            //        sparePart.恢复配件基础信息();
            //} catch(ValidationException ex) {
            //    UIHelper.ShowAlertMessage(ex.Message);
            //    return;
            //}
            //base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            this.isPaurse = false;
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
