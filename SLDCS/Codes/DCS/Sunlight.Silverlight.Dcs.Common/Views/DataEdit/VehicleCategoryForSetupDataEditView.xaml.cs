﻿
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class VehicleCategoryForSetupDataEditView {
        private DataGridViewBase vehicleCategoryProductDetailDataGridView;

        public VehicleCategoryForSetupDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private DataGridViewBase VehicleCategoryProductDetailDataGridView {
            get {
                if(this.vehicleCategoryProductDetailDataGridView == null) {
                    this.vehicleCategoryProductDetailDataGridView = DI.GetDataGridView("VehicleCategoryProductDetailForEdit");
                    this.vehicleCategoryProductDetailDataGridView.DomainContext = this.DomainContext;
                }
                return this.vehicleCategoryProductDetailDataGridView;
            }
        }

        private void CreateUI() {
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(CommonUIStrings.DataGridView_Title_Common_Products, null, () => this.VehicleCategoryProductDetailDataGridView);
            detailDataEditView.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(detailDataEditView);
        }

        private void LoadEntityToEdit(int id) {
            //this.DomainContext.Load(this.DomainContext.GetVehicleCategoryWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError) {
            //        if(!loadOp.IsErrorHandled)
            //            loadOp.MarkErrorAsHandled();
            //        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
            //        return;
            //    }
            //    var entity = loadOp.Entities.SingleOrDefault();
            //    if(entity != null)
            //        this.SetObjectToEdit(entity);
            //}, null);
        }

        protected override void OnEditSubmitting() {
            //var vehicleCategory = this.DataContext as VehicleCategory;
            //if(vehicleCategory == null || !this.VehicleCategoryProductDetailDataGridView.CommitEdit())
            //    return;
            //vehicleCategory.ValidationErrors.Clear();
            //if(vehicleCategory.VehicleCategoryProductDetails.Any(e => e.ProductId == default(int))) {
            //    UIHelper.ShowAlertMessage(CommonUIStrings.DataEditView_Validation_VehicleCategory_ProductInfoIsNull);
            //    return;
            //}
            //foreach(var detail in vehicleCategory.VehicleCategoryProductDetails) {
            //    detail.ValidationErrors.Clear();
            //    ((IEditableObject)detail).EndEdit();
            //}
            //((IEditableObject)vehicleCategory).EndEdit();
            base.OnEditSubmitting();
        }

        protected override string Title {
            get {
                return CommonUIStrings.DataEditView_Title_Setup_VehicleCategory;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
