﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class ABCStrategyDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames ={
           "ABCStrategy_Category", "ABCStrategy_ReferenceBaseType", "BaseData_Status"
        };

        public ABCStrategyDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(ABCStrategy);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    }, 
                    new KeyValuesColumnItem {
                        Name = "ReferenceBaseType",
                         KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Name = "Percentage"
                    }, new ColumnItem {
                        Name = "SamplingDuration"
                    },new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "YeardOutBoundStartTime"
                    }, new ColumnItem {
                        Name = "YeardOutBoundEndTime"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem  {
                        Name = "AbandonTime"
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "BranchName",
                        IsSortDescending = false,
                        IsDefaultGroup = true
                    }, new KeyValuesColumnItem {
                        Name = "Category",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetABCStrategies";
        }
    }
}
