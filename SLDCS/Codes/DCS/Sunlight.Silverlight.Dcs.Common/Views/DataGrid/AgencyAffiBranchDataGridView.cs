﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using SelectionMode = System.Windows.Controls.SelectionMode;
using Sunlight.Silverlight.Dcs.Common.Resources;


namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    /// <summary>
    /// 选择代理库
    /// </summary>
    public class AgencyAffiBranchDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "MasterData_Status"
        };

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Agency.Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "Agency.Code",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_Code,
                    }, new ColumnItem {
                        Name = "Agency.Name",
                       Title = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_Name,
                    }, new ColumnItem {
                        Name = "Agency.ShortName"
                    },new ColumnItem{
                        Name="Agency.Company.ProvinceName"
                    },new ColumnItem{
                        Name="Agency.Company.CityName"
                    },new ColumnItem{
                        Name="Agency.Company.CountyName"
                    }, new ColumnItem {
                        Name = "Agency.CreatorName"
                    }, new ColumnItem {
                        Name = "Agency.CreateTime"
                    }, new ColumnItem {
                        Name = "Agency.ModifierName"
                    }, new ColumnItem {
                        Name = "Agency.ModifyTime"
                    }, new ColumnItem {
                        Name = "Branch.Name"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(AgencyAffiBranch);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
            ((GridViewDataColumn)this.GridView.Columns["Agency.CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["Agency.ModifyTime"]).DataFormatString = "d";
        }

        protected override string OnRequestQueryName() {
            return "根据营销分公司查询代理库";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filter = this.FilterItem as CompositeFilterItem;
            if(filter == null)
                return null;
            var compositeFilterItem = filter.Filters.SingleOrDefault(item => item.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
            switch(parameterName) {
                case "branchId":
                    var filterItem2 = filter.Filters.SingleOrDefault(item => item.MemberName == "BranchId");
                    if(filterItem2 != null) {
                        return filterItem2.Value;
                    }
                    if(compositeFilterItem != null) {
                        var branch = compositeFilterItem.Filters.SingleOrDefault(e => e.MemberName == "BranchId");
                        return branch != null ? branch.Value : null;
                    }
                    break;
            }
            return null;
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "BranchId"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        public AgencyAffiBranchDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
