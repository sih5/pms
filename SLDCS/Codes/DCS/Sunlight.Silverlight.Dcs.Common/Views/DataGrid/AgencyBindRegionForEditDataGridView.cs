﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class AgencyBindRegionForEditDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "IsSelected",
                        Title = CommonUIStrings.DataGridView_Title_AgencyBindRegion
                    }, new ColumnItem {
                        Name = "ProvinceName",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.IsReadOnly = false;
            this.DataPager.PageSize = 17;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(TiledRegion);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("CenterAuthorizedProvinces");
        }
       
    }
}
