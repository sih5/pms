﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class AgencyDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames ={
           "MasterData_Status"
        };

        public AgencyDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(Agency);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "Code",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_Code,
                        IsSortDescending = false
                    }, new ColumnItem {
                        Name = "Name",
                       Title = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_Name,
                    },new ColumnItem {
                        Name = "MarketingDepartment.Name",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_MarketingDepartmentName,
                    }, new ColumnItem {
                        Name = "WarehouseName",
                       Title = "订货仓库",
                    }, new ColumnItem {
                        Name = "Remark"
                    },new ColumnItem {
                        Name = "Company.OrderCycle",
                        Title =CommonUIStrings.DataEditView_Column_Agency_OrderCycle,
                    },new ColumnItem {
                        Name = "Company.ShippingCycle",
                        Title =CommonUIStrings.DataEditView_Column_Agency_ShippingCycle,
                    },new ColumnItem {
                        Name = "Company.ArrivalCycle",
                        Title =CommonUIStrings.DataEditView_Column_Agency_ArrivalCycle,
                    }
                    , new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "Company.ModifierName"
                    }, new ColumnItem {
                        Name = "Company.ModifyTime"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetAgencyCompany";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }
    }
}
