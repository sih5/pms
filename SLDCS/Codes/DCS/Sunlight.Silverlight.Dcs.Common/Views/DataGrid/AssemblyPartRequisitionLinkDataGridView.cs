﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class AssemblyPartRequisitionLinkDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "BaseData_Status","SparePart_KeyCode"
        };

        public AssemblyPartRequisitionLinkDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = CommonUIStrings.DataDeTailPanel_Text_BottomStock_Status
                    },new ColumnItem {
                        Name = "PartCode",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_SparePartCode
                    },new ColumnItem {
                        Name = "PartName",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_SparePartName
                    },new KeyValuesColumnItem {
                        Name = "KeyCode",
                        Title = CommonUIStrings.QueryPanel_Column_KeyCode,
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                    },new ColumnItem {
                        Name = "Qty",
                        Title = CommonUIStrings.Report_Title_InOutSettleSummary_Qty
                    },new ColumnItem {
                        Name = "Remark",
                        Title = CommonUIStrings.DataDeTailPanel_Text_BottomStock_Remark
                    },new ColumnItem {
                        Name = "CreatorName"
                    },new ColumnItem {
                        Name = "CreateTime"
                    },new ColumnItem {
                        Name = "ModifierName"
                    },new ColumnItem {
                        Name = "ModifyTime"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetAssemblyPartRequisitionLinks";
        }

        protected override Type EntityType {
            get {
                return typeof(AssemblyPartRequisitionLink);
            }
        }
        
    }
}
