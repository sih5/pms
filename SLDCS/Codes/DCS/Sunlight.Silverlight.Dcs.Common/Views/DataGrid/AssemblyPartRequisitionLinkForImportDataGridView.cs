﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class AssemblyPartRequisitionLinkForImportDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem{
                        Title =CommonUIStrings.DataDeTailPanel_Text_BottomStock_SparePartCode,
                        Name="PartCode"
                    },new ColumnItem{
                        Title =CommonUIStrings.DataDeTailPanel_Text_BottomStock_SparePartName,
                        Name="PartName"
                    },new ColumnItem{
                        Title =CommonUIStrings.QueryPanel_Column_KeyCode,
                        Name="KeyCode"
                    }, new ColumnItem {
                        Title =CommonUIStrings.Report_Title_InOutSettleSummary_Qty,
                        Name = "Qty"
                    }, new ColumnItem {
                        Title =CommonUIStrings.DataDeTailPanel_Text_BottomStock_Remark,
                        Name = "Remark"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(AssemblyPartRequisitionLink);
            }
        }
    }
}