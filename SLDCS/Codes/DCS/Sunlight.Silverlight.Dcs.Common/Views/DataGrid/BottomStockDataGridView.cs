﻿using System.Linq;
using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class BottomStockDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
             "BaseData_Status","Company_Type"
        };
        public BottomStockDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override Type EntityType {
            get {
                return typeof(BottomStock);
            }
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title=CommonUIStrings.DataGridView_Title_BottomStock_Status
                    },
                    new ColumnItem {
                        Name = "PartsSalesCategoryName",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name
                    }, new ColumnItem {
                         Name = "CompanyCode",
                       Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_CompanyCode
                    }, new ColumnItem {
                        Name = "CompanyName",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_CompanyName
                    },new KeyValuesColumnItem {
                        Name = "CompanyType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_CompanyType
                    }, new ColumnItem {
                         Name = "WarehouseCode",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_WarehouseCode
                    }, new ColumnItem {
                        Name = "WarehouseName",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_WarehouseId,
                    }, new ColumnItem {
                         Name = "SparePartCode",
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartCode,
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                    }, new ColumnItem {
                        Name = "StockQty",
                        Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_StockQty
                    }, new ColumnItem {
                        Name = "CreatorName",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_CreatorName
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title = CommonUIStrings.QueryPanel_Title_ProductStandard_CreateTime,
                    }, new ColumnItem {
                        Name = "ModifierName",
                        Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_ModifierName
                    }, new ColumnItem {
                        Name = "ModifyTime",
                         Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_ModifyTime
                    },new ColumnItem {
                        Name = "AbandonerName",
                        Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_AbandonerName
                    },new ColumnItem {
                        Name = "AbandonTime",
                        Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_AbandonerTime
                    }, new ColumnItem {
                        Name = "Remark",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartsInboundCheckBillRemark
                    }
                };
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null) {
                switch (parameterName) {
                    case "sparePartCodes":
                        var codes = filters.Filters.SingleOrDefault(s => s.MemberName == "SparePartCode");
                        if (codes == null || codes.Value == null)
                            return null;
                        return codes.Value.ToString();
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override string OnRequestQueryName() {
            return "QueryBottomStocks";
        }
        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "BottomStock"
                    }
                };
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ModifyTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["AbandonTime"]).DataFormatString = "d";
        }
    }
}
