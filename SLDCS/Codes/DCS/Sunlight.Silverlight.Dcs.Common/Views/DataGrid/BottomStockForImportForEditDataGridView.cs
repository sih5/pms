﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class BottomStockForImportForEditDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem {
                        Name = "SequeueNumber",
                        Title=CommonUIStrings.DataGridView_Title_SequeueNumber
                    },new ColumnItem{
                        Name="PartsSalesCategoryName",
                        Title = CommonUIStrings.DataEditView_Text_BottomStock_Brand
                    },new ColumnItem{
                        Name="CompanyCode",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_CompanyCode
                    },new ColumnItem{
                        Name="WarehouseCode",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_WarehouseCode
                    },new ColumnItem{
                        Name="SparePartCode",
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartCode,
                    },new ColumnItem{
                        Name="StockQty",
                        Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_StockQty
                    },new ColumnItem{
                        Name="Remark",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartsInboundCheckBillRemark
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(BottomStock);
            }
        }
    }
}