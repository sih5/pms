﻿using System.Linq;
using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class BottomStockForceReserveSubDataGridView  : DcsDataGridViewBase {
        private readonly string[] kvNames = {
             "BaseData_Status"
        };
        public BottomStockForceReserveSubDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override Type EntityType {
            get {
                return typeof(BottomStockForceReserveSub);
            }
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title=CommonUIStrings.DataGridView_Title_BottomStock_Status
                    },new ColumnItem {
                        Name = "ReserveType",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStockForceReserveType_ReserveType
                    },new ColumnItem {
                        Name = "SubCode",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStockForceReserveSub_SubCode
                    },new ColumnItem {
                        Name = "SubName",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStockForceReserveSub_SubName
                    }, new ColumnItem {
                        Name = "CreatorName",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_CreatorName
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title = CommonUIStrings.QueryPanel_Title_ProductStandard_CreateTime,
                    }, new ColumnItem {
                        Name = "ModifierName",
                        Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_ModifierName
                    }, new ColumnItem {
                        Name = "ModifyTime",
                         Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_ModifyTime
                    }
                };
            }
        }


        protected override string OnRequestQueryName() {
            return "GetBottomStockForceReserveSubs";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ModifyTime"]).DataFormatString = "d";
        }
    }
}