﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;
namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class BottomStockSubVersionDataGridView : DcsDataGridViewBase {
        protected readonly string[] KvNames = {
           "BaseData_Status","Company_Type"
        };

        public BottomStockSubVersionDataGridView() {
            this.KeyValueManager.Register(this.KvNames);

        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new KeyValuesColumnItem {
                        Name="Status",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_Common_Status,
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]]
                    },new ColumnItem {
                        Name = "CompanyCode",
                        Title =CommonUIStrings.DataDeTailPanel_Text_Company_Code
                    }, new ColumnItem {
                        Name = "CompanyName",
                        Title=CommonUIStrings.DataDeTailPanel_Text_Company_Name
                    }, new KeyValuesColumnItem {
                        Name="CompanyType",
                        Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_CompanyType,
                        KeyValueItems = this.KeyValueManager[this.KvNames[1]]
                    },new ColumnItem {
                        Name = "SparePartCode",
                        Title =CommonUIStrings.Report_Title_SaleAdjustment_SparePartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title=CommonUIStrings.Report_Title_SaleAdjustment_SparePartName
                    }, new ColumnItem {
                        Name = "SubVersionCode",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_ForceReserveBill_SubVersionCode
                    }, new ColumnItem {
                        Name = "ColVersionCode",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_ForceReserveBill_ColVersionCode
                    }, new ColumnItem {
                        Name = "ReserveType",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_BottomStockForceReserveType_ReserveType
                    }, new ColumnItem {
                        Name = "ReserveTypeSubItem",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_ReserveTypeSubItem
                    }, new ColumnItem {
                        Name = "ReserveQty",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_ForceReserveQty
                    }, new ColumnItem {
                        Name = "SalePrice",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStockSubVersion_SalePrice
                    }, new ColumnItem {
                        Name = "ReserveFee",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_ReserveFee
                    }, new ColumnItem {
                        Name = "StartTime",
                        Title= CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_ValidTo
                    }, new ColumnItem {
                        Name = "EndTime",
                         Title= CommonUIStrings.DataGridView_PurchasePriceWarningOfExpiry_ValidTo
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetBottomStockSubVersionForQuery";
        }    

        protected override Type EntityType {
            get {
                return typeof(BottomStockSubVersion);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
        }
    }
}
