﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class BranchDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "MasterData_Status"
        };

        public BranchDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_Branch_Code,
                        Name = "Code"
                    },new ColumnItem {
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_Branch_Name,
                        Name = "Name"
                    },new ColumnItem {
                        Name = "MainBrand"
                    },new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "CreatorName"
                    },new ColumnItem {
                        Name = "CreateTime"
                    },new ColumnItem {
                        Name = "ModifierName"
                    },new ColumnItem {
                        Name = "ModifyTime"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetBranches";
        }

        protected override Type EntityType {
            get {
                return typeof(Branch);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
        }
    }
}
