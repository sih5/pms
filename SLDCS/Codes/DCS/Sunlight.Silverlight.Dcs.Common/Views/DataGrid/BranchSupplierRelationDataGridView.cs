﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class BranchSupplierRelationDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = { 
            "BaseData_Status","BranchSupplierRelation_ClaimPriceCategory"
        };

        public BranchSupplierRelationDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                   new KeyValuesColumnItem{
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem{
                        Name = "PartsSupplier.Code",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_PartsSupplierCode
                    }, new ColumnItem{
                        Name = "PartsSupplier.Name",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_PartsSupplierName,
                    },new ColumnItem {
                        Name = "BusinessCode"
                    },new ColumnItem {
                        Name = "BusinessName"
                    },new ColumnItem {
                        Name = "ArrivalCycle",
                        Title=CommonUIStrings.DataGridView_Title_BranchSupplierRelation_ArrivalCycle
                    },
                    //new ColumnItem{
                    //    Name = "PartsManagementCostGrade.Name",
                    //    Title = CommonUIStrings.DataGrid_QueryItem_Title_PartsManagementCostGrade_Name
                    //}, new ColumnItem{
                    //    Name = "PartsClaimCoefficient",
                    //    Title = CommonUIStrings.DataGrid_QueryItem_Title_BranchSupplierRelation_PartsClaimCoefficient
                    //}, new KeyValuesColumnItem{
                    //    Name = "ClaimPriceCategory",
                    //    KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                    //    Title = CommonUIStrings.DataGrid_QueryItem_Title_BranchSupplierRelation_ClaimPriceCategory
                    //}, 
                    new ColumnItem{
                        Name = "PurchasingCycle"
                    }
                    //, new ColumnItem{
                    //    Name = "IfSPByLabor"
                    //},new ColumnItem{
                    //    Name = "LaborUnitPrice"
                    //},new ColumnItem{
                    //    Name = "LaborCoefficient"
                    //},new ColumnItem{
                    //    Name = "IfHaveOutFee"
                    //},new ColumnItem{
                    //    Name = "OutServiceCarUnitPrice",
                    //    Title="外出里程单价(元/公里)"
                    //},new ColumnItem{
                    //    Name = "OutSubsidyPrice",
                    //   Title="外出人员单价(元/人天)"
                    //},new ColumnItem{
                    //    Name = "OldPartTransCoefficient"
                    //}
                    , new ColumnItem{
                        Name = "Remark"
                    }, new ColumnItem{
                        Name = "CreatorName"
                    }, new ColumnItem{
                        Name = "CreateTime"
                    }, new ColumnItem{
                        Name = "ModifierName"
                    }, new ColumnItem{
                        Name = "ModifyTime"
                    }, new ColumnItem{
                        Name = "AbandonerName"
                    }, new ColumnItem{
                        Name = "AbandonTime"
                    }, new ColumnItem{
                        Name = "Branch.Code",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_BranchSupplierRelation_BranchCode
                    }, new ColumnItem{
                        Name = "Branch.Name",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_BranchSupplierRelation_BranchName
                    }, new ColumnItem{
                        Name = "PartsSalesCategory.Name",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_BranchSupplierRelation_PartsSalesCategoryName
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(BranchSupplierRelation);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetBranchSupplierRelationWithPartsSalesCategoryByPersonelId";
        }
    }
}
