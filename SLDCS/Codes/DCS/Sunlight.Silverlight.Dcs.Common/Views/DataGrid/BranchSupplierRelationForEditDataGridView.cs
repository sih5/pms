﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class BranchSupplierRelationForEditDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = { 
            "BranchSupplierRelation_ClaimPriceCategory"
        };
        private RadWindow radQueryWindow;
        private QueryWindowBase partsSupplierQueryWindow;
        private DcsMultiPopupsQueryWindowBase partsSupplierQueryWindowForMultiSelect;
        //private readonly ObservableCollection<KeyValuePair> kvPartsManagementCostGrades = new ObservableCollection<KeyValuePair>();

        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = this.PartsSupplierQueryWindowForMultiSelect,
                    Header = CommonUIStrings.QueryPanel_Title_PartsSupplier,
                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                });
            }
        }

        private QueryWindowBase PartsSupplierQueryWindow {
            get {
                if(this.partsSupplierQueryWindow == null) {
                    this.partsSupplierQueryWindow = DI.GetQueryWindow("PartsSupplierDropDown");
                    this.partsSupplierQueryWindow.SelectionDecided += partsSupplierQueryWindow_SelectionDecided;
                }
                return this.partsSupplierQueryWindow;
            }
        }

        public DcsMultiPopupsQueryWindowBase PartsSupplierQueryWindowForMultiSelect {
            get {
                if(this.partsSupplierQueryWindowForMultiSelect == null) {
                    this.partsSupplierQueryWindowForMultiSelect = DI.GetQueryWindow("PartsSupplierForMulti") as DcsMultiPopupsQueryWindowBase;
                    var supplierQueryWindowForMultiSelect = this.partsSupplierQueryWindowForMultiSelect;
                    if(supplierQueryWindowForMultiSelect != null) {
                        supplierQueryWindowForMultiSelect.SelectionDecided += this.partsSupplierQueryWindowForMultiSelect_SelectionDecided;
                        supplierQueryWindowForMultiSelect.Loaded += this.partssupplierQueryWindowForMultiSelect_Loaded;
                    }
                }
                return this.partsSupplierQueryWindowForMultiSelect;
            }
        }

        private void partssupplierQueryWindowForMultiSelect_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var domainContext = this.DomainContext as DcsDomainContext;
            var branchSupplierRelationDataEditView = this.DataContext as BranchSupplierRelationDataEditView;
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null)
                return;
            if(domainContext == null || branchSupplierRelationDataEditView == null)
                return;
            if(branchSupplierRelationDataEditView.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_PartsPurchasePricingChange_PartsSalesCategoryNameIsNull);
                var parent = queryWindow.Parent as RadWindow;
                if(parent != null)
                    parent.Close();
                return;
            }
            //domainContext.Load(domainContext.GetPartsManagementCostGradesQuery().Where(ex => ex.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && ex.PartsSalesCategoryId == branchSupplierRelationDataEditView.PartsSalesCategoryId), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        if(!loadOp.IsErrorHandled) {
            //            loadOp.MarkErrorAsHandled();
            //            return;
            //        }
            //    this.kvPartsManagementCostGrades.Clear();
            //    foreach(var entity in loadOp.Entities) {
            //        this.kvPartsManagementCostGrades.Add(new KeyValuePair {
            //            Key = entity.Id,
            //            Value = entity.Name,
            //            UserObject = entity
            //        });
            //    }
            //}, null);

        }

        private void partsSupplierQueryWindowForMultiSelect_SelectionDecided(object sender, EventArgs e) {
            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var branchSupplierRelationDataEditView = this.DataContext as BranchSupplierRelationDataEditView;
            if(branchSupplierRelationDataEditView == null)
                return;
            var partsSuppliers = queryWindow.SelectedEntities.Cast<PartsSupplier>().ToArray();
            if(partsSuppliers == null || !partsSuppliers.Any())
                return;
            //if(branchSupplierRelationDataEditView.BranchSupplierRelations.Any(obj => partsSuppliers.Select(r=>r.Id).Contains(obj.PartsSupplier.Id ))) {
            //    UIHelper.ShowNotification(string.Format(CommonUIStrings.DataEditView_Validation_BranchSupplierRelation_SupplierNotRepeat, partsSupplier.Code));
            //    return;
            //}
            var details = branchSupplierRelationDataEditView.BranchSupplierRelations.Select(r => r.SupplierId).ToArray();
            var neIds = partsSuppliers.Where(r => !details.Contains(r.Id)).Select(r => r.Id).ToArray();
            var currentUserData = BaseApp.Current.CurrentUserData;
            domainContext.Load(domainContext.GetBranchesQuery().Where(r => r.Id == currentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    if(!loadOp.IsErrorHandled) {
                        loadOp.MarkErrorAsHandled();
                        return;
                    }
                var branch = loadOp.Entities.FirstOrDefault();
                domainContext.Load(domainContext.GetPartsSuppliersWithCompanyByIdsQuery(neIds), loadOp2 => {
                    if(loadOp2.HasError)
                        if(!loadOp2.IsErrorHandled) {
                            loadOp2.MarkErrorAsHandled();
                            return;
                        }
                    var entities = loadOp2.Entities.ToArray();
                    if(entities != null && entities.Any()) {
                        foreach(var partsSupplier in entities) {
                            var maxSequeueNumber = branchSupplierRelationDataEditView.BranchSupplierRelations.Any() ? branchSupplierRelationDataEditView.BranchSupplierRelations.Max(entity => entity.SequeueNumber) + 1 : 1;
                            var branchSupplierRelation = new BranchSupplierRelation {
                                SequeueNumber = maxSequeueNumber,
                                Status = (int)DcsBaseDataStatus.有效,
                                PartsSalesCategoryId = branchSupplierRelationDataEditView.PartsSalesCategoryId,
                                IfHaveOutFee = false,
                                IfSPByLabor = false,
                                OldPartTransCoefficient = 1,
                                LaborCoefficient = 1
                                //SupplierCode = partsSupplier.Code,
                                //SupplierName = partsSupplier.Name
                            };
                            if(branch != null) {
                                branchSupplierRelation.Branch = branch;
                            }
                            branchSupplierRelation.PartsSupplier = partsSupplier;
                            branchSupplierRelationDataEditView.BranchSupplierRelations.Add(branchSupplierRelation);
                        }
                    }
                    var parent = queryWindow.Parent as RadWindow;
                    if(parent != null)
                        parent.Close();
                }, null);
            }, null);
        }

        private void partsSupplierQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var branchSupplierRelation = queryWindow.DataContext as BranchSupplierRelation;
            if(branchSupplierRelation == null)
                return;
            var partsSupplier = queryWindow.SelectedEntities.Cast<PartsSupplier>().FirstOrDefault();
            if(partsSupplier == null)
                return;
            var branchSupplierRelationDataEditView = this.DataContext as BranchSupplierRelationDataEditView;
            if(branchSupplierRelationDataEditView == null)
                return;
            if(branchSupplierRelationDataEditView.BranchSupplierRelations.Any(obj => obj.PartsSupplier.Id == partsSupplier.Id)) {
                UIHelper.ShowNotification(string.Format(CommonUIStrings.DataEditView_Validation_BranchSupplierRelation_SupplierNotRepeat, partsSupplier.Code));
                return;
            }
            domainContext.Load(domainContext.GetPartsSuppliersWithCompanyQuery().Where(r => r.Id == partsSupplier.Id), loadOp => {
                if(loadOp.HasError)
                    if(!loadOp.IsErrorHandled) {
                        loadOp.MarkErrorAsHandled();
                        return;
                    }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity != null)
                    branchSupplierRelation.PartsSupplier = entity;
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }, null);
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("BranchSupplierRelations");
        }

        protected override Type EntityType {
            get {
                return typeof(BranchSupplierRelation);
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name = "SequeueNumber",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_BranchSupplierRelation_SequeueNumber,
                        IsReadOnly = true
                    }, new DropDownTextBoxColumnItem{
                        Name = "PartsSupplier.Code",
                        DropDownContent = this.PartsSupplierQueryWindow,
                        IsEditable = false,
                    }, new ColumnItem{
                        Name = "PartsSupplier.Name",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "BusinessCode"
                    },new ColumnItem {
                        Name = "BusinessName"
                    }, new ColumnItem{
                        Name = "ArrivalCycle",
                        Title=CommonUIStrings.DataGridView_Title_BranchSupplierRelation_ArrivalCycle
                    }
                    //, new ColumnItem{
                    //    Name = "PartsManagementCostGrade.Name",
                    //    Title = CommonUIStrings.DataGrid_QueryItem_Title_PartsManagementCostGrade_Name
                    //}, new ColumnItem{
                    //    Name = "PartsClaimCoefficient",
                    //    Title = CommonUIStrings.DataGrid_QueryItem_Title_BranchSupplierRelation_PartsClaimCoefficient
                    //}, new KeyValuesColumnItem{
                    //    Name = "ClaimPriceCategory",
                    //    KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                    //    Title = CommonUIStrings.DataGrid_QueryItem_Title_BranchSupplierRelation_ClaimPriceCategory
                    //}
                    , new ColumnItem{
                        Name = "PurchasingCycle"
                    }
                    //, new ColumnItem{
                    //    Name = "IfSPByLabor",
                    //},new ColumnItem{
                    //    Name = "LaborUnitPrice"
                    //},new ColumnItem{
                    //    Name = "LaborCoefficient"
                    //},new ColumnItem{
                    //    Name = "IfHaveOutFee"
                    //},new ColumnItem{
                    //    Name = "OutServiceCarUnitPrice"
                    //},new ColumnItem{
                    //    Name = "OutSubsidyPrice"
                    //},new ColumnItem{
                    //    Name = "OldPartTransCoefficient"
                    //}
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
            this.GridView.Deleted += GridView_Deleted;
            //this.GridView.Columns["PartsManagementCostGrade.Name"].CellEditTemplate =
            //                                         (DataTemplate)XamlReader.
            //                                         Load("<DataTemplate xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation' xmlns:telerik='http://schemas.telerik.com/2008/xaml/presentation'><telerik:RadComboBox IsEditable='False' DisplayMemberPath='Value' SelectedValuePath='Key' SelectedValue='{Binding PartsManagementCostGradeId}' /></DataTemplate>");
            //this.GridView.PreparingCellForEdit += GridView_PreparingCellForEdit;
            this.GridView.BeginningEdit += this.GridView_BeginningEdit;
            //this.GridView.CellValidating += GridView_CellValidating;
            this.GridView.EnableColumnVirtualization = false;
            this.GridView.EnableRowVirtualization = false;
        }

        //private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
        //    if(e.Cell.Column.UniqueName.Equals("PartsManagementCostGrade.Name")) {
        //        var branchSupplierRelation = e.Row.DataContext as BranchSupplierRelation;
        //        if(branchSupplierRelation == null)
        //            return;
        //        var comboBox = e.EditingElement as RadComboBox;
        //        if(comboBox == null)
        //            return;
        //        var partsManagementCostGrade = comboBox.SelectedItem as KeyValuePair;
        //        if(partsManagementCostGrade == null)
        //            return;
        //        branchSupplierRelation.PartsManagementCostGrade = partsManagementCostGrade.UserObject as PartsManagementCostGrade;
        //    }
        //}

        //private void GridView_PreparingCellForEdit(object sender, GridViewPreparingCellForEditEventArgs e) {
        //    if(e.Column.UniqueName.Equals("PartsManagementCostGrade.Name")) {
        //        var detail = e.Row.DataContext as BranchSupplierRelation;
        //        if(detail == null)
        //            return;
        //        var comboBox = e.EditingElement as RadComboBox;
        //        if(comboBox == null)
        //            return;
        //        comboBox.ItemsSource = this.kvPartsManagementCostGrades;
        //    }
        //}
        //是否按工时单价索赔为是时工时系数不可编辑，为否时工时单价不可编辑，是否存在外出费用为否时外出里程单价,外出人员单价不可编辑
        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            var branchSupplierRelation = e.Cell.DataContext as BranchSupplierRelation;
            if(branchSupplierRelation == null)
                return;
            switch(e.Cell.Column.UniqueName) {
                case "LaborCoefficient":
                    if(branchSupplierRelation.IfSPByLabor) {
                        e.Cancel = true;
                        branchSupplierRelation.LaborCoefficient = 1;
                    } else {
                        e.Cancel = false;
                    }
                    break;
                case "LaborUnitPrice":
                    if(!branchSupplierRelation.IfSPByLabor) {
                        e.Cancel = true;
                        branchSupplierRelation.LaborUnitPrice = default(decimal);
                    } else {
                        e.Cancel = false;
                    }
                    break;
                case "OutServiceCarUnitPrice":
                case "OutSubsidyPrice":
                    if(!branchSupplierRelation.IfHaveOutFee) {
                        e.Cancel = true;
                        branchSupplierRelation.OutSubsidyPrice = default(decimal);
                        branchSupplierRelation.OutServiceCarUnitPrice = default(decimal);
                    }
                    break;

            }
        }


        private void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var branchSupplierRelationDataEditView = this.DataContext as BranchSupplierRelationDataEditView;
            if(branchSupplierRelationDataEditView == null)
                return;
            var nc = 1;
            foreach(var branchSupplierRelation in branchSupplierRelationDataEditView.BranchSupplierRelations) {
                branchSupplierRelation.SequeueNumber = nc++;
            }
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            RadQueryWindow.ShowDialog();
            e.Cancel = true;
        }

        public BranchSupplierRelationForEditDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        //由于新增修改共用一个GridView，实现此方法用于修改时加载清单中配件管理费的数据源
        public override object ExchangeData(IBaseView sender, string subject, params object[] contents) {
        //    var domainContext = this.DomainContext as DcsDomainContext;
        //    if(domainContext == null)
        //        return null;
        //    domainContext.Load(domainContext.GetPartsManagementCostGradesQuery().Where(ex => ex.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && ex.PartsSalesCategoryId == (int)contents[0]), LoadBehavior.RefreshCurrent, loadOp => {
        //        if(loadOp.HasError)
        //            if(!loadOp.IsErrorHandled) {
        //                loadOp.MarkErrorAsHandled();
        //                return;
        //            }
        //        this.kvPartsManagementCostGrades.Clear();
        //        foreach(var entity in loadOp.Entities) {
        //            this.kvPartsManagementCostGrades.Add(new KeyValuePair {
        //                Key = entity.Id,
        //                Value = entity.Name,
        //                UserObject = entity
        //            });
        //        }
        //    }, null);
           return null;
        }


    }
}
