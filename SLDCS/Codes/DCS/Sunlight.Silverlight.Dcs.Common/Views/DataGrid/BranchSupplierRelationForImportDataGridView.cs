﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class BranchSupplierRelationForImportDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = { 
            "BranchSupplierRelation_ClaimPriceCategory"
        };

        public BranchSupplierRelationForImportDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name="SequeueNumber",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_BranchSupplierRelation_SequeueNumber
                    }, new ColumnItem{
                        Name = "PartsSupplier.Code",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_PartsSupplierCode
                    }, new ColumnItem{
                        Name = "PartsSupplier.Name",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_PartsSupplierName,
                    },new ColumnItem {
                        Name = "BusinessCode"
                    },new ColumnItem {
                        Name = "BusinessName"
                    } , new ColumnItem{
                        Name = "ArrivalCycle",
                        Title=CommonUIStrings.DataGridView_Title_BranchSupplierRelation_ArrivalCycle
                    }
                    //, new ColumnItem{
                    //    Name = "PartsManagementCostGrade.Name",
                    //    Title = CommonUIStrings.DataGrid_QueryItem_Title_PartsManagementCostGrade_Name
                    //}, new ColumnItem{
                    //    Name = "PartsClaimCoefficient",
                    //    Title = CommonUIStrings.DataGrid_QueryItem_Title_BranchSupplierRelation_PartsClaimCoefficient
                    //}, new KeyValuesColumnItem{
                    //    Name = "ClaimPriceCategory",
                    //    KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                    //    Title = CommonUIStrings.DataGrid_QueryItem_Title_BranchSupplierRelation_ClaimPriceCategory
                    //}
                    , new ColumnItem{
                        Name = "PurchasingCycle"
                    }
                    //, new ColumnItem{
                    //    Name = "IfSPByLabor"
                    //},new ColumnItem{
                    //    Name = "LaborUnitPrice"
                    //},new ColumnItem{
                    //    Name = "LaborCoefficient"
                    //},new ColumnItem{
                    //    Name = "IfHaveOutFee"
                    //},new ColumnItem{
                    //    Name = "OutServiceCarUnitPrice",
                    //    Title="外出里程单价(元/公里)"
                    //},new ColumnItem{
                    //    Name = "OutSubsidyPrice",
                    //   Title="外出人员单价(元/人天)"
                    //},new ColumnItem{
                    //    Name = "OldPartTransCoefficient"
                    //}
                    , new ColumnItem{
                        Name = "Remark"
                    },new ColumnItem{
                        Name = "Branch.Code",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_BranchSupplierRelation_BranchCode
                    }, new ColumnItem{
                        Name = "Branch.Name",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_BranchSupplierRelation_BranchName
                    }, new ColumnItem{
                        Name = "PartsSalesCategory.Name",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_BranchSupplierRelation_PartsSalesCategoryName
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(BranchSupplierRelation);
            }
        }
    }
}
