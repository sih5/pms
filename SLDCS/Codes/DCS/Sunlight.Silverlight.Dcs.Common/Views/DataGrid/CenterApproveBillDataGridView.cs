﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using SelectionMode = System.Windows.Controls.SelectionMode;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class CenterApproveBillDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames ={
           "BaseData_Status"
        };

        public CenterApproveBillDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(CenterApproveBill);
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionMode = SelectionMode.Single;
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "MarketingDepartment.Name",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_MarketingDepartmentName,
                    },
                    new ColumnItem {
                        Name = "CompanyCode",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_Code,
                    }, new ColumnItem {
                        Name = "CompanyName",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_Name,
                    }, new ColumnItem {
                        Name = "WarehouseName",
                        Title = CommonUIStrings.DataDeTailPanel_Text_Company_WarehouseName,
                    }, new ColumnItem {
                        Name = "ApproveWeek",
                        Title = CommonUIStrings.DataGridView_Title_CenterApproveBill_ApproveWeek
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_Common_Status
                    }
                    , new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetCenterApproveBillWithMarket";
        }
    }
}
