﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class CenterSaleReturnStrategyDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames ={
           "BaseData_Status","PartsSalesReturn_ReturnType"
        };

        public CenterSaleReturnStrategyDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(CenterSaleReturnStrategy);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, 
                    new KeyValuesColumnItem {
                        Name = "ReturnType",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_CenterSaleReturnStrategy_ReturnType,
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },
                    new ColumnItem {
                        Name = "TimeFrom",
                        Title = CommonUIStrings.DataGridView_Title_CenterSaleReturnStrategy_TimeFrom
                    }, new ColumnItem {
                        Name = "TimeTo",
                        Title = CommonUIStrings.DataGridView_Title_CenterSaleReturnStrategy_TimeTo
                    }, new ColumnItem {
                        Name = "DiscountRate",
                        Title = CommonUIStrings.DataEdit_Text_PartsStockStatement_DiscountRate
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetCenterSaleReturnStrategies";
        }
    }
}
