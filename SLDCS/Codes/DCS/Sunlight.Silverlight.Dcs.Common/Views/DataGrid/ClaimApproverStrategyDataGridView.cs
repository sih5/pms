﻿
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class ClaimApproverStrategyDataGridView : DcsDataGridViewBase
    {
        private readonly string[] kvNames = new[] {
            "PartsClaimBill_StatusNew"
        };
        public ClaimApproverStrategyDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["FeeFrom"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["FeeTo"]).DataFormatString = "c2";
        }

        protected override bool ShowCheckBox
        {
            get
            {
                return true;
            }
        }

        protected override bool UsePaging
        {
            get
            {
                return true;
            }
        }

        protected override string OnRequestQueryName() {
            return "查询三包索赔单审批策略";
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_ClaimApproverStrategy_AfterApproverStatus,
                        Name = "AfterApproverStatus",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new KeyValuesColumnItem {
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_ClaimApproverStrategy_InitialApproverStatus,
                        Name = "InitialApproverStatus",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_ClaimApproverStrategy_FeeFrom,
                        Name = "FeeFrom"
                    }, new ColumnItem {
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_ClaimApproverStrategy_FeeTo,
                        Name = "FeeTo"
                    }, new ColumnItem {
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_CreatorName,
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Title = CommonUIStrings.QueryPanel_Title_ProductStandard_CreateTime,
                        Name = "CreateTime",
                        MaskType = Sunlight.Silverlight.Core.Model.MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Title = CommonUIStrings.DataDeTailPanel_Text_BottomStock_ModifierName,
                        Name = "ModifierName"
                    }, new ColumnItem {
                         Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_ModifyTime,
                        Name = "ModifyTime",
                        MaskType = Sunlight.Silverlight.Core.Model.MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name,
                        Name = "PartsSalesCategoryName"
                    }
                };
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override Type EntityType
        {
            get
            {
                return typeof(ClaimApproverStrategy);
            }
        }

    }
}