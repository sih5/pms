﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class CombinedPartDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "SparePart_PartType","MasterData_Status","IsOrNot"
        };
        public CombinedPartDataGridView() {
            this.DataContextChanged += PartsBranchDataGridView_DataContextChanged;
            this.KeyValueManager.Register(this.kvNames);
        }

        private void PartsBranchDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var sparePart = e.NewValue as SparePart;
            if(sparePart == null || sparePart.Id == default(int))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "SparePartId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                };
            this.FilterItem.Value = sparePart.Id;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems=this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "Name"
                    },
                    //new ColumnItem {
                    //    Name = "IMSCompressionNumber"
                    //},
                    new ColumnItem {
                        Name = "IMSManufacturerNumber"
                    },new ColumnItem {
                        Name = "ReferenceCode",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_ReferenceCode
                    },new ColumnItem {
                        Name = "OverseasPartsFigure",
                        Title = CommonUIStrings.DataEditPanel_Text_SparePart_OverseasPartsFigure
                    }, new ColumnItem {
                        Name = "ExchangeIdentification",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsExchangeGroup_ExchangeCode
                    },
                    //new ColumnItem {
                    //    Name = "ProductBrand",
                    //    Title="产品商标"
                    //},
                    new ColumnItem {
                        Name = "MInPackingAmount",
                        Title=CommonUIStrings.DataEditPanel_Text_SparePartForBatch_MInPackingAmount
                    },
                    //new ColumnItem {
                    //    Name = "TotalNumber",
                    //    Title=CommonUIStrings.DataEditPanel_Validation_SparePartForBatch_Assembly
                    //}, new ColumnItem {
                    //    Name = "Factury",
                    //    Title=CommonUIStrings.DataEditPanel_Text_SparePartForBatch_Factury
                    //},
                    new KeyValuesColumnItem {
                        Name = "IsOriginal",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]],
                        Title=CommonUIStrings.DataEditPanel_Text_SparePart_IsOriginal
                    },
                    //new ColumnItem {
                    //    Name = "CategoryCode",
                    //    Title=CommonUIStrings.DataEditPanel_Text_SparePartForBatch_CategoryCode
                    //}, new ColumnItem {
                    //    Name = "CategoryName",
                    //    Title=CommonUIStrings.DataEditPanel_Text_SparePartForBatch_CategoryName
                    //}, new ColumnItem {
                    //    Name = "CADCode",
                    //    Title=CommonUIStrings.DataGridView_ColumnItem_Title_SparePart_CADCode
                    //},
                     new ColumnItem {
                        Name = "PinyinCode",
                        Title = CommonUIStrings.DataEditPanel_Text_SparePart_PinyinCode
                    },
                    new ColumnItem {
                        Name = "Material",
                        Title = CommonUIStrings.DataEditPanel_Text_SparePart_Material
                    },
                     new ColumnItem {
                        Name = "DeclareElement",
                        Title = CommonUIStrings.DataEditPanel_Text_SparePart_DeclareElement
                    },
                    new KeyValuesColumnItem {
                        Name = "PartType",
                        KeyValueItems=this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "EnglishName"
                    },
                    //new ColumnItem {
                    //    Name = "Specification"
                    //},
                    new ColumnItem {
                        Name = "MeasureUnit"
                    }, new ColumnItem {
                        Name = "ShelfLife",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_SparePart_ShelfLife
                    },
                    //new ColumnItem {
                    //    Name = "LastSubstitute"
                    //}, new ColumnItem {
                    //    Name = "NextSubstitute"
                    //},
                    new ColumnItem {
                        Name = "Weight"
                    }, new ColumnItem {
                        Name = "Feature",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_SparePart_Feature
                    }, new ColumnItem {
                        Name = "GoldenTaxClassifyCode",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_GoldenTaxClassifyMsg_GoldenTaxClassifyCode,
                    }, new ColumnItem {
                        Name = "GoldenTaxClassifyName",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_GoldenTaxClassifyMsg_GoldenTaxClassifyName
                    },
                    //new ColumnItem {
                    //    Name = "StandardCode",
                    //    Title ="产品执行标准代码"
                    //}, new ColumnItem {
                    //    Name = "StandardName",
                    //    Title ="产品执行标准名称"
                    //},
                    new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }
            };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SparePartHistory);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsBranchWithSparePartHistorys";
        }
    }
}
