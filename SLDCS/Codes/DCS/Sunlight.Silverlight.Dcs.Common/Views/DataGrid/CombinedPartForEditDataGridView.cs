﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class CombinedPartForEditDataGridView : DcsDataGridViewBase {
        private QueryWindowBase sparePartDropDownQueryWindow;

        private QueryWindowBase SparePartDropDownQueryWindow {
            get {
                if(this.sparePartDropDownQueryWindow == null) {
                    this.sparePartDropDownQueryWindow = DI.GetQueryWindow("SparePartDropDown");
                    this.sparePartDropDownQueryWindow.SelectionDecided += this.SparePartDropDownQueryWindow_SelectionDecided;
                    this.sparePartDropDownQueryWindow.Loaded += this.SparePartDropDownQueryWindow_Loaded;
                }
                return this.sparePartDropDownQueryWindow;
            }
        }

        private void SparePartDropDownQueryWindow_Loaded(object sender, RoutedEventArgs routedEventArgs) {
            var queryWindowSparePart = sender as QueryWindowBase;
            var sparepart = this.DataContext as SparePart;
            if(queryWindowSparePart == null || sparepart == null)
                return;
            queryWindowSparePart.ExchangeData(null, "SetAdditionalFilterItem", new object[] {
                new FilterItem("Id", typeof(int), FilterOperator.IsNotEqualTo, sparepart.Id)
            });
        }

        private void SparePartDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var sparePart = queryWindow.SelectedEntities.Cast<SparePart>().FirstOrDefault();
            if(sparePart == null)
                return;
            var combinedPart = queryWindow.DataContext as CombinedPart;
            if(combinedPart == null)
                return;
            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.Load(domainContext.GetSparePartsQuery().Where(ex => ex.Id == sparePart.Id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                combinedPart.ChildSparePart = loadOp.Entities.SingleOrDefault();
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }, null);
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(CombinedPart);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new DropDownTextBoxColumnItem {
                        Name = "ChildSparePart.Code",
                        DropDownContent = this.SparePartDropDownQueryWindow,
                        IsEditable = false
                    }, new ColumnItem {
                        Name = "ChildSparePart.Name",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Quantity",
                        MaskType = MaskType.Numeric,
                        FormatString = "n0",
                        TextAlignment = TextAlignment.Right
                    }
                };
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("ChildCombinedParts");
        }
    }
}
