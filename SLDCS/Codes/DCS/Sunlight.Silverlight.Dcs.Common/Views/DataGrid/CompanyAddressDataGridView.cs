﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class CompanyAddressDataGridView : DcsDataGridViewBase {
        protected readonly string[] kvNames = {
            "Address_usage", "BaseData_Status"
        };

        public CompanyAddressDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new KeyValuesColumnItem {
                        Name = "Usage",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "ContactPerson"
                    }, new ColumnItem {
                        Name = "ContactPhone"
                    }, new ColumnItem {
                        Name = "TiledRegion.ProvinceName"
                    }, new ColumnItem {
                        Name = "TiledRegion.CityName"
                    }, new ColumnItem {
                        Name = "TiledRegion.CountyName"
                    }, new ColumnItem {
                        Name = "DetailAddress"
                    },  new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(CompanyAddress);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetCompanyAddressWithRegion";
        }
    }
}
