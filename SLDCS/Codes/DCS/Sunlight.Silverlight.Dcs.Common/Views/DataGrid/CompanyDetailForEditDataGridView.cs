﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class CompanyDetailForEditDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Company_Type"
        };

        public CompanyDetailForEditDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        private DcsMultiPopupsQueryWindowBase companyQueryWindow;
        private DcsMultiPopupsQueryWindowBase CompanyQueryWindow {
            get {
                if(this.companyQueryWindow == null) {
                    this.companyQueryWindow = DI.GetQueryWindow("CompanyMulti") as DcsMultiPopupsQueryWindowBase;
                    this.companyQueryWindow.SelectionDecided += this.CompanyMultiQueryWindow_SelectionDecided;
                }
                return this.companyQueryWindow;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(CompanyDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name="SerialNumber",
                        IsReadOnly=true
                    }, new ColumnItem{
                        Name = "Code",
                        IsReadOnly = true
                    }, new ColumnItem{
                        Name = "Name",
                        IsReadOnly = true
                    },new KeyValuesColumnItem {
                        Name = "Type",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("CompanyDetails");
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.AddingNewDataItem -= GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
            this.GridView.Deleting += GridView_Deleting;
        }

        private void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            var notification = this.DataContext as Notification;
            if(notification == null)
                return;
            var serialNumber = 0;
            foreach(var item in notification.CompanyDetails.Where(entity => !e.Items.Contains(entity)))
                item.SerialNumber = ++serialNumber;
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            var notification = this.DataContext as Notification;
            if(notification == null)
                return;
            RadQueryWindow.ShowDialog();
        }

        private RadWindow radQueryWindow;
        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = this.CompanyQueryWindow,
                    Header = "查询企业",
                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                });
            }
        }

        private void CompanyMultiQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var companys = queryWindow.SelectedEntities.Cast<Company>().ToArray();
            if(companys == null)
                return;
            var notification = this.DataContext as Notification;
            if(notification == null)
                return;
            if(notification.CompanyDetails.Any(r => companys.Any(s => s.Id == r.CompanyId))) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_CompanyDetails_CompanyIdIsAlreadyHave);
                return;
            }
            foreach(var company in companys) {
                var companyDetail = new CompanyDetail {
                    CompanyId = company.Id,
                    Code = company.Code,
                    Name = company.Name,
                    Type = company.Type,
                    SerialNumber = this.GridView.Items.Count > 0 ? this.GridView.Items.Cast<CompanyDetail>().Max(entity => entity.SerialNumber) + 1 : 1,
                };
                notification.CompanyDetails.Add(companyDetail);
            }
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }
    }
}
