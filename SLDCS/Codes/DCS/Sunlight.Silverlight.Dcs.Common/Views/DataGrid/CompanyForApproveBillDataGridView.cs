﻿
using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class CompanyForApproveBillDataGridView : DcsDataGridViewBase
    {
        private readonly string[] kvNames = {
             "Company_Type"
        };
        public CompanyForApproveBillDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                      new ColumnItem {
                        Name = "Code",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_CompanyCode
                    }, new ColumnItem {
                        Name = "Name",
                      Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_CompanyName
                    }, new KeyValuesColumnItem {
                        Name = "Type",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                      Title =  CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_CompanyType
                    }
                };
            }
        }
        protected override string OnRequestQueryName() {
            return "GetCompaniesForApproveBill";
        }

        protected override Type EntityType {

            get {
                return typeof(Company);
            }
        }
    }
}
