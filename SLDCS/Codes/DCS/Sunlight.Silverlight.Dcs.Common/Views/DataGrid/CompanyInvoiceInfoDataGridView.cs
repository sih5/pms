﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class CompanyInvoiceInfoDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsSupplier_TaxpayerKind","Invoice_Type"
        };

        public CompanyInvoiceInfoDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "CompanyCode"
                    }, new ColumnItem {
                        Name = "CompanyName"
                    }, new ColumnItem {
                        Name = "TaxRegisteredNumber",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_CompanyInvoiceInfo_TaxRegisteredNumber,
                    }, new ColumnItem {
                        Name = "InvoiceTitle"
                    }, new KeyValuesColumnItem {
                        Name = "TaxpayerQualification",
                        KeyValueItems = this.KeyValueManager[kvNames[0]]
                    }, new KeyValuesColumnItem {
                        Name = "InvoiceType",
                        KeyValueItems = this.KeyValueManager[kvNames[1]]
                    }, new ColumnItem {
                        Name = "InvoiceAmountQuota"
                    }, new ColumnItem {
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_CompanyInvoiceInfo_InvoiceTax,
                        Name = "InvoiceTax"//开票税率
                    }, new ColumnItem {
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_CompanyInvoiceInfo_Linkman,
                        Name = "Linkman"//财务联系人
                    }, new ColumnItem {
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_CompanyInvoiceInfo_ContactNumber,
                        Name = "ContactNumber"//财务联系电话
                    }, new ColumnItem {
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_CompanyInvoiceInfo_Fax,
                        Name = "Fax"//财务传真
                    }, new ColumnItem {
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_CompanyInvoiceInfo_TaxRegisteredAddress,
                        Name = "TaxRegisteredAddress"
                    }, new ColumnItem {
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_CompanyInvoiceInfo_TaxRegisteredPhone,
                        Name = "TaxRegisteredPhone"
                    }, new ColumnItem {
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_CompanyInvoiceInfo_BankName,
                        Name = "BankName"
                    }, new ColumnItem {
                        Name = "BankAccount",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_CompanyInvoiceInfo_BankAccount,
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetCompanyInvoiceInfoes";
        }

        protected override Type EntityType {
            get {
                return typeof(CompanyInvoiceInfo);
            }
        }
    }
}
