﻿
namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class CompanySecondDataGridView : CompanyDataGridView {
        protected override string OnRequestQueryName() {
            return "GetCompaniesNotContainedIn";
        }
    }
}
