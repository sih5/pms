﻿using System;
using System.Linq;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class CustomerInfoDataGridView : DcsDataGridViewBase {
        public CustomerInfoDataGridView() {
            //this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_CompanyCode
                    },new ColumnItem {
                        Name = "Name",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_CompanyName
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_VirtualFactoryPurchacePrice_CreateTime
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "getCustomerTmp";
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null) {
                var date = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName) {
                    case "code":
                        return filters.Filters.Single(item => item.MemberName == "Code").Value;
                    case "name":
                        return filters.Filters.Single(item => item.MemberName == "Name").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualIdNameForTmp);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
