﻿using System;
using System.Linq;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class CustomerInfoForSupplierDataGridView : DcsDataGridViewBase {
        public CustomerInfoForSupplierDataGridView() {
            //this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        Title = "企业编号"
                    },new ColumnItem {
                        Name = "Name",
                        Title = "企业名称"
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title = "传输时间"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "getSupplierTmp";
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null) {
                var date = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName) {
                    case "code":
                        return filters.Filters.Single(item => item.MemberName == "Code").Value;
                    case "name":
                        return filters.Filters.Single(item => item.MemberName == "Name").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualIdNameForTmp);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
