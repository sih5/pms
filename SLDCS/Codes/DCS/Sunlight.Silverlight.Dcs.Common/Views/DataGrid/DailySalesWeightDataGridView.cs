﻿using System.Linq;
using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class DailySalesWeightDataGridView : DcsDataGridViewBase {
     
        public DailySalesWeightDataGridView() {
        }
        protected override Type EntityType {
            get {
                return typeof(DailySalesWeight);
            }
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    
                    new ColumnItem {
                        Name = "WarehouseCode",
                        Title=CommonUIStrings.DataDeTailPanel_Text_Company_WarehouseCode
                    }, new ColumnItem {
                         Name = "WarehouseName",
                       Title=CommonUIStrings.DataDeTailPanel_Text_Company_WarehouseName
                    }, new ColumnItem {
                        Name = "Times",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricingDetail_Time
                    }, new ColumnItem {
                         Name = "Weight",
                        Title="权重"
                    }, new ColumnItem {
                        Name = "CreatorName",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_CreatorName
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title = CommonUIStrings.QueryPanel_Title_ProductStandard_CreateTime,
                    },new ColumnItem {
                        Name = "ModifierName",
                        Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_ModifierName
                    },new ColumnItem {
                        Name = "ModifyTime",
                        Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_ModifierTime
                    }
                };
            }
        }


        protected override string OnRequestQueryName() {
            return "GetDailySalesWeights";
        }
       
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ModifyTime"]).DataFormatString = "d";
        }
    }
}
