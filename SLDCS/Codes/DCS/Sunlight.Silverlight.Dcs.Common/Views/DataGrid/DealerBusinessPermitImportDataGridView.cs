﻿
using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class DealerBusinessPermitImportDataGridView : DcsDataGridViewBase {

        private readonly string[] kvNames = {
        "DcsServiceProductLineViewProductLineType"
        };

        public DealerBusinessPermitImportDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new ColumnItem {
                        Name = "Dealer.Code",
                    },new ColumnItem {
                        Name = "Dealer.Name",
                    }, new ColumnItem {
                        Name = "ServiceProductLineCode",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_ProductView_ServiceProductLine,
                    }, new ColumnItem {
                        Name = "ServiceProductLineName",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_ProductView_ServiceProductLine,
                    },new KeyValuesColumnItem {
                        Name = "ProductLineType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = CommonUIStrings.QueryPanel_Title_Dealer_ServiceProductType
                    } ,new ColumnItem {
                        Name = "PartsSalesCategoryName",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerBusinessPermit);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }
    }
}