﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class DealerForDetailDataGridView : DcsDataGridViewBase {
        public DealerForDetailDataGridView() {
            this.DataContextChanged += DealerDataGridView_DataContextChanged;
        }

        private void DealerDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var dealerMarketDptRelation = e.NewValue as MarketingDepartment;
            if(dealerMarketDptRelation == null || dealerMarketDptRelation.Id == default(int))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "BranchId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                };
            this.FilterItem.Value = dealerMarketDptRelation.BranchId;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Dealer.Code"
                    }, new ColumnItem {
                        Name = "Dealer.Name"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerMarketDptRelation);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetDealerMarketDptRelationWithDealer";
        }
    }
}
