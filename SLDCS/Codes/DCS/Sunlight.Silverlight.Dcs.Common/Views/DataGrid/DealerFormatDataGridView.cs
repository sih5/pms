﻿using System.Linq;
using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class DealerFormatDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
             "BaseData_Status",
        };
        public DealerFormatDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override Type EntityType {
            get {
                return typeof(DealerFormat);
            }
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    
                    new ColumnItem {
                        Name = "DealerCode",
                        Title=CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_DealerCode
                    }, new ColumnItem {
                         Name = "DealerName",
                       Title=CommonUIStrings.Report_QueryPanel_Title_DealerNames
                    }, new ColumnItem {
                        Name = "Format",
                        Title="服务商业态"
                    }, new ColumnItem {
                         Name = "Quarter",
                        Title="季度"
                    },new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title=CommonUIStrings.DataGridView_Title_BottomStock_Status
                    }, new ColumnItem {
                        Name = "CreatorName",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_CreatorName
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title = CommonUIStrings.QueryPanel_Title_ProductStandard_CreateTime,
                    },new ColumnItem {
                        Name = "AbandonerName",
                        Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_AbandonerName
                    },new ColumnItem {
                        Name = "AbandonTime",
                        Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_AbandonerTime
                    }
                };
            }
        }


        protected override string OnRequestQueryName() {
            return "GetDealerFormats";
        }
       
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["AbandonTime"]).DataFormatString = "d";
        }
    }
}
