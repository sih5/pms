﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class DealerInfoImportDataGridView : DcsDataGridViewBase {


        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        Title=CommonUIStrings.DataEditView_Text_CompanyAddress_DealerCode
                    },new ColumnItem {
                        Name = "Name",
                        Title=CommonUIStrings.DataEditView_Text_CompanyAddress_DealerName
                    },
                    new ColumnItem{
                        Name = "ShortName",
                        Title =  CommonUIStrings.DataEditView_Text_CompanyAddress_Abbreviation
                    },
                    new ColumnItem{
                        Name = "Manager",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_ServiceManager
                    },new ColumnItem{
                        Name = "CustomerCode",
                        Title = CommonUIStrings.DataEditPanel_Text_Company_CustomerCode
                    },new ColumnItem{
                        Name = "SupplierCode",
                        Title = CommonUIStrings.QueryPanel_Title_PartsSupplier_SupplierCode
                    },new ColumnItem{
                        Name = "FoundDate",
                        Title = CommonUIStrings.DataEditView_Text_CompanyAddress_Establishment
                    },new ColumnItem{
                        Name = "ProvinceName",
                        Title = CommonUIStrings.DataGrid_QueryItem_Title_ProvinceName
                    },new ColumnItem{
                        Name = "CityName",
                        Title = CommonUIStrings.DetailPanel_Text_Region_City
                    },new ColumnItem{
                        Name = "CountyName",
                        Title = CommonUIStrings.DataEditPanel_Text_Region_District
                    },new ColumnItem{
                        Name = "CityLevel",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_Warehouse_CityLevel
                    },

                    new ColumnItem {
                        Name = "ContactPerson",
                        Title = CommonUIStrings.DataEditView_Text_CompanyAddress_LinkMan
                    },new ColumnItem {
                        Name = "ContactPhone",
                        Title=CommonUIStrings.DataEditPanel_Text_Company_ContactPhone
                    },
                    new ColumnItem{
                        Name = "ContactMail",
                        Title = "E-MAIL"
                    },new ColumnItem {
                        Name = "ContactMobile",
                        Title= CommonUIStrings.DataEditView_Text_CompanyAddress_LinkManPhone
                    },new ColumnItem{
                        Name = "Fax",
                        Title = CommonUIStrings.DataEditView_Text_CompanyAddress_Fax
                    },new ColumnItem{
                        Name = "ContactPostCode",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_Company_ContactPostCode
                    },new ColumnItem{
                        Name = "BusinessAddress",
                        Title = CommonUIStrings.DataEditView_Text_CompanyAddress_Address
                    },new ColumnItem{
                        Name = "BusinessAddressLongitude",
                        Title =CommonUIStrings.DataEditPanel_Text_Company_Address_LatitudeNew
                     },new ColumnItem{
                        Name = "BusinessAddressLatitude",
                        Title = CommonUIStrings.DataEditPanel_Text_Company_Address_LongitudeNew
                    },
                    new ColumnItem {
                        Name = "RegisterCode",
                         Title=CommonUIStrings.QueryPanel_Title_Dealer_Certificate
                    },
                    new ColumnItem{
                        Name = "RegisterName",
                        Title = CommonUIStrings.QueryPanel_Title_Dealer_CertificateName
                    },
                    new ColumnItem{
                        Name = "RegisterDate",
                        Title = CommonUIStrings.QueryPanel_Title_Dealer_CertificateDate
                    },new ColumnItem {
                        Name = "RegisterCapital",
                        Title=CommonUIStrings.QueryPanel_Title_Dealer_CertificateFee
                    },new ColumnItem{
                        Name = "FixedAsset",
                        Title = CommonUIStrings.QueryPanel_Title_Dealer_FixedAssets
                    },
                    
                    new ColumnItem {
                        Name = "LegalRepresentative",
                         Title=CommonUIStrings.QueryPanel_Title_Dealer_Representative
                    },
                    new ColumnItem{
                        Name = "LegalRepresentTel",
                        Title = CommonUIStrings.QueryPanel_Title_Dealer_RepresentativePhone
                    },new ColumnItem{
                        Name = "CorporateNature",
                        Title = CommonUIStrings.QueryPanel_Title_Dealer_Nature
                    },new ColumnItem{
                        Name = "BusinessScope",
                        Title  =CommonUIStrings.QueryPanel_Title_Dealer_Scope
                     },new ColumnItem{
                        Name = "RegisteredAddress",
                        Title = CommonUIStrings.QueryPanel_Title_Dealer_CertificateAddress
                    },new ColumnItem{
                        Name = "IdDocumentType",
                        Title = CommonUIStrings.QueryPanel_Title_Dealer_IDCardType
                    },new ColumnItem{
                        Name = "IdDocumentNumber",
                        Title = CommonUIStrings.QueryPanel_Title_Dealer_IDCardNo
                    },
                    new ColumnItem {
                        Name = "RepairQualification",
                        Title=CommonUIStrings.QueryPanel_Title_Dealer_Qualification
                    },
                    new ColumnItem{
                        Name = "OwnerCompany",
                        Title = CommonUIStrings.QueryPanel_Title_Dealer_Unit
                    },new ColumnItem{
                        Name = "BuildTime",
                        Title = CommonUIStrings.QueryPanel_Title_Dealer_CreateTime
                    },new ColumnItem{
                        Name = "TrafficRestrictionsdescribe",
                        Title = CommonUIStrings.QueryPanel_Title_Dealer_Traffic
                    },new ColumnItem{
                        Name = "MainBusinessAreas",
                        Title = CommonUIStrings.QueryPanel_Title_Dealer_MainScope
                    },new ColumnItem{
                        Name = "AndBusinessAreas",
                        Title = CommonUIStrings.QueryPanel_Title_Dealer_OperationScope
                    },new ColumnItem{
                        Name = "DangerousRepairQualification",
                        Title = CommonUIStrings.QueryPanel_Title_Dealer_Dangerous
                    },new ColumnItem{
                        Name = "HasBranch",
                        Title = CommonUIStrings.QueryPanel_Title_Dealer_Secondary
                    },new ColumnItem{
                        Name = "VehicleTravelRoute",
                        Title = CommonUIStrings.QueryPanel_Title_Dealer_Route
                    },new ColumnItem{
                        Name = "VehicleUseSpeciality",
                        Title = CommonUIStrings.QueryPanel_Title_Dealer_CarUse
                    },new ColumnItem{
                        Name = "VehicleDockingStation",
                        Title = CommonUIStrings.QueryPanel_Title_Dealer_CarStop
                    },new ColumnItem{
                        Name = "EmployeeNumber",
                        Title = CommonUIStrings.QueryPanel_Title_Dealer_NumberService
                    },new ColumnItem{
                        Name = "RepairingArea",
                        Title = CommonUIStrings.QueryPanel_Title_Dealer_Maintenance
                    },new ColumnItem{
                        Name = "ReceptionRoomArea",
                        Title = CommonUIStrings.QueryPanel_Title_Dealer_Reception
                    },new ColumnItem{
                        Name = "ParkingArea",
                        Title = CommonUIStrings.QueryPanel_Title_Dealer_Parking
                    },new ColumnItem{
                        Name = "PartWarehouseArea",
                        Title = CommonUIStrings.QueryPanel_Title_Dealer_PartsLibrary
                    },
                    
                    new ColumnItem {
                        Name = "TaxRegisteredNumber",
                        Title=CommonUIStrings.QueryPanel_Title_Dealer_Registration
                    },
                    new ColumnItem{
                        Name = "TaxpayerQualification",
                        Title = CommonUIStrings.QueryPanel_Title_Dealer_Taxpayers
                    },new ColumnItem{
                        Name = "InvoiceAmountQuota",
                        Title = CommonUIStrings.QueryPanel_Title_Dealer_InvoiceLimit
                    },new ColumnItem{
                        Name = "InvoiceTitle",
                        Title = CommonUIStrings.QueryPanel_Title_Dealer_InvoiceName
                    },new ColumnItem{
                        Name = "InvoiceType",
                        Title = CommonUIStrings.QueryPanel_Title_Dealer_InvoiceType
                    },
                    new ColumnItem {
                        Name = "BankName",
                        Title=CommonUIStrings.CompanyInvoiceInfo_BankName
                    },
                    new ColumnItem{
                        Name = "InvoiceTax",
                        Title = CommonUIStrings.QueryPanel_Title_Dealer_InvoiceRate
                    },
                    new ColumnItem {
                        Name = "BankAccount",
                        Title=CommonUIStrings.DataEditPanel_Text_BankAccount
                    },
                    new ColumnItem{
                        Name = "Linkman",
                        Title =CommonUIStrings.DataGridView_ColumnItem_Title_CompanyInvoiceInfo_Linkman
                    },
                    new ColumnItem {
                        Name = "ContactNumber",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_CompanyInvoiceInfo_ContactNumber
                    },
                    new ColumnItem{
                        Name = "AccountFax",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_CompanyInvoiceInfo_Fax
                    },
                    new ColumnItem {
                        Name = "TaxRegisteredAddress",
                        Title=CommonUIStrings.QueryPanel_Title_Dealer_RegistrationAddress
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerExtend);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }
    }
}
