using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class DealerMarketDeptRelationForEditDataGridView : DcsDataGridViewBase {
        private QueryWindowBase dealerDropDownQueryWindow;

        private QueryWindowBase DealerDropDownQueryWindow {
            get {
                if(this.dealerDropDownQueryWindow == null) {
                    this.dealerDropDownQueryWindow = DI.GetQueryWindow("DealerDropDown");
                    this.dealerDropDownQueryWindow.SelectionDecided += this.dealerDropDownQueryWindow_SelectionDecided;
                }
                return this.dealerDropDownQueryWindow;
            }
        }

        private void dealerDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var dealerMarketDptRelations = this.GridView.Items.Cast<DealerMarketDptRelation>();
            var dealer = queryWindow.SelectedEntities.Cast<Dealer>().FirstOrDefault();
            var dealerMarketDptRelation = queryWindow.DataContext as DealerMarketDptRelation;
            var domainContext = this.DomainContext as DcsDomainContext;
            if(dealerMarketDptRelation == null || dealer == null || domainContext == null)
                return;
            if(dealerMarketDptRelations.Any(item => item.DealerId == dealer.Id)) {
                UIHelper.ShowAlertMessage(CommonUIStrings.DataEditView_Validation_DealerMarketDptRelation_DealerIdNotAllowedRepeat);
                return;
            }
            domainContext.Load(domainContext.GetDealersQuery().Where(d => d.Id == dealer.Id), loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                dealerMarketDptRelation.Dealer = loadOp.Entities.SingleOrDefault();
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }, null);
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.NewObject = new DealerMarketDptRelation {
                Status = (int)DcsBaseDataStatus.有效,
                BranchId = BaseApp.Current.CurrentUserData.EnterpriseId
            };
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            this.GridView.CellValidating += GridView_CellValidating;
        }

        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
            var dealerMarketDptRelation = e.Cell.DataContext as DealerMarketDptRelation;
            //var dealerMarketDptRelations = this.GridView.Items.Cast<DealerMarketDptRelation>();
            if(dealerMarketDptRelation == null)
                return;
            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "Dealer.Code":
                    if(e.NewValue == null) {
                        e.IsValid = false;
                        e.ErrorMessage = string.Format("经销商编号和名称不能为空");
                    }
                    //if(dealerMarketDptRelations.Where(d => !ReferenceEquals(d, e.Cell.DataContext)).Any(d => d.Id == dealerMarketDptRelation.Id)) {
                    //    e.IsValid = false;
                    //    e.ErrorMessage = string.Format(CommonUIStrings.DataEditView_Validation_DealerMarketDptRelation_DealerIdNotAllowedRepeat);
                    //}
                    break;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("DealerMarketDptRelations");
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new DropDownTextBoxColumnItem {
                        Name = "Dealer.Code",
                        DropDownContent = this.DealerDropDownQueryWindow,
                        IsEditable = false
                    }, new ColumnItem {
                        Name = "Dealer.Name",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerMarketDptRelation);
            }
        }
    }
}
