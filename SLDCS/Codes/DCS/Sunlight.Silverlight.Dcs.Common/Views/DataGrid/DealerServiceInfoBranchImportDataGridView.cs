﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class DealerServiceInfoBranchImportDataGridView : DcsDataGridViewBase {

        private readonly string[] kvNames = {
        "DcsServiceProductLineViewProductLineType",
        "DcsDealerServiceInfoServiceStationType",
        "DcsSaleServiceLayout",
        "DcsServicePermission",
        "DcsInvoiceType",
        "DcsInvoiceType",
        "DlrSerInfo_TrunkNetworkType",
        "IsOrNot"
        };

        public DealerServiceInfoBranchImportDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new ColumnItem {
                        Name = "ChannelCapability.Name",
                        Title= CommonUIStrings.QueryPanel_QueryItem_Title_Warehouse_ChannelCapabilityId,
                    },new ColumnItem {
                        Name = "Dealer.Code",
                        Title = CommonUIStrings.DataEditView_Text_CompanyAddress_DealerCode
                    },new ColumnItem {
                        Name = "Dealer.Name",
                        Title = CommonUIStrings.DataEditView_Text_CompanyAddress_DealerName
                    },
                    new ColumnItem {
                        Name = "BusinessCode",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_BranchSupplierRelation_BussinessCode
                    },new ColumnItem {
                        Name = "BusinessName",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_BranchSupplierRelation_BussinessName
                    },new ColumnItem {
                        Name = "Dealer.ShortName",
                        Title= CommonUIStrings.DataEditView_Text_CompanyAddress_Abbreviation
                    },new ColumnItem {
                        Name = "BusinessDivision",
                        Title=CommonUIStrings.QueryPanel_Title_DealerService_Marketing
                    },new ColumnItem {
                        Name = "MarketingDepartment.Name",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_MarketingDepartmentName,
                    },new KeyValuesColumnItem {
                        Name = "ServiceStationType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title=CommonUIStrings.QueryPanel_Title_DealerService_DealerType
                    },new ColumnItem {
                        Name = "Area",
                        Title=CommonUIStrings.DataEditPanel_Text_Region_Region
                    },new ColumnItem {
                        Name = "RegionType",
                        Title=CommonUIStrings.QueryPanel_Title_DealerService_Categories
                    },new ColumnItem {
                        Name = "ServicePermission",
                        Title=CommonUIStrings.QueryPanel_Title_DealerService_Authority
                    },new ColumnItem {
                        Name = "AccreditTime",
                        Title=CommonUIStrings.QueryPanel_Title_DealerService_Authorized
                    },new ColumnItem {
                        Name = "IsOnDuty",
                        Title=CommonUIStrings.QueryPanel_Title_DealerService_IsDuty
                    },new ColumnItem {
                         Name = "HotLine",
                        Title=CommonUIStrings.QueryPanel_Title_DealerService_24Hotline
                    },new ColumnItem {
                        Name = "HourPhone24",
                        Title=CommonUIStrings.QueryPanel_Title_DealerService_24Phone
                    },new ColumnItem {
                        Name = "Fix",
                        Title=CommonUIStrings.DataEditPanel_Text_Company_ContactPhone
                    },new ColumnItem {
                        Name = "Fax",
                        Title=CommonUIStrings.DataEditView_Text_CompanyAddress_Fax
                    },new ColumnItem {
                        Name = "Warehouse.Name",
                        Title=CommonUIStrings.QueryPanel_Title_DealerService_Warehouse
                    }
                    ,new KeyValuesColumnItem {
                        Name = "SaleandServicesiteLayout",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]],
                        Title=CommonUIStrings.QueryPanel_Title_DealerService_Relationship
                    },new ColumnItem {
                        Name = "PartReserveAmount",
                        Title=CommonUIStrings.QueryPanel_Title_DealerService_ReserveAmount
                    },new KeyValuesColumnItem {
                        Name = "RepairAuthorityGrade",
                        KeyValueItems = this.KeyValueManager[this.kvNames[3]],
                        Title=CommonUIStrings.QueryPanel_Title_DealerService_StationMaintenance
                    },new ColumnItem {
                        Name = "PartsManagementCostGrade.Name",
                        Title=CommonUIStrings.QueryPanel_Title_DealerService_ManagementRate
                    },new ColumnItem{
                        Name="Grade",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_Warehouse_Grade
                    },new ColumnItem {
                        Name = "GradeCoefficient.Grade",
                        Title=CommonUIStrings.QueryPanel_Title_DealerService_SettlementStar
                    },
                    new ColumnItem {
                        Name = "OutServiceradii",
                        Title=CommonUIStrings.QueryPanel_Title_DealerService_ServiceRadius
                    }, new KeyValuesColumnItem {
                        Name = "TrunkNetworkType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[6]],
                        Title = CommonUIStrings.QueryPanel_Title_DealerService_BackboneNetworks
                    },new KeyValuesColumnItem {
                        Name = "CenterStack",
                        KeyValueItems = this.KeyValueManager[this.kvNames[7]],
                        Title = CommonUIStrings.QueryPanel_Title_DealerService_CentralStation
                    },new ColumnItem {
                        Name = "UsedPartsWarehouse.Name",
                        Title=CommonUIStrings.QueryPanel_Title_DealerService_OldWarehouse
                    },new KeyValuesColumnItem {
                        Name = "MaterialCostInvoiceType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[4]],
                        Title=CommonUIStrings.QueryPanel_Title_DealerService_MaterialFee
                    },new KeyValuesColumnItem {
                        Name = "LaborCostCostInvoiceType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[5]],
                        Title=CommonUIStrings.QueryPanel_Title_DealerService_InvoiceWorking
                    },new ColumnItem {
                        Name = "LaborCostInvoiceRatio",
                        Title=CommonUIStrings.QueryPanel_Title_DealerService_FactorHour
                    },new ColumnItem {
                        Name = "InvoiceTypeInvoiceRatio",
                        Title=CommonUIStrings.QueryPanel_Title_DealerService_FactorMaterial
                    },new ColumnItem {
                        Name = "Remark",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartsInboundCheckBillRemark
                    },new KeyValuesColumnItem {
                        Name = "ExternalState",
                        Title=CommonUIStrings.QueryPanel_Title_DealerProduct_OutStatus
                    },new ColumnItem {
                        Name = "PartsSalesCategory.Name",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerServiceInfo);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }
    }
}
