﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;
namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class EnterpriseAddressDataGridView : DcsDataGridViewBase {
        protected readonly string[] kvNames = {
            "Address_usage", "BaseData_Status"
        };

        public EnterpriseAddressDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem {
                        Name = "Company.Code"
                    }, new ColumnItem {
                        Name = "Company.Name"
                    },new KeyValuesColumnItem {
                        Name = "Usage",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "ContactPerson"
                    }, new ColumnItem {
                        Name = "ContactPhone"
                    }, new ColumnItem {
                        Name = "ProvinceName"
                    }, new ColumnItem {
                        Name = "CityName"
                    }, new ColumnItem {
                        Name = "CountyName"
                    }, new ColumnItem {
                        Name = "DetailAddress"
                    },  new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(CompanyAddress);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ModifyTime"]).DataFormatString = "d";
        }

        protected override string OnRequestQueryName() {
            return "GetCompanyAddressWithDetail";
        }
    }
}
