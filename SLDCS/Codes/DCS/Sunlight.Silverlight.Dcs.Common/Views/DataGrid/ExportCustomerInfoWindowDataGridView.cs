﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class ExportCustomerInfoWindowDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = { "ExportCustPriceType", "BaseData_Status" };

        public ExportCustomerInfoWindowDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(ExportCustomerInfo);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem {
                        Name = "CustomerCode"
                    },new ColumnItem {
                        Name = "CustomerName"
                    },  new KeyValuesColumnItem {
                        Name = "PriceType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetExportCustomerInfoes";
        }
    }
}
