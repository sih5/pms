﻿using System;
using System.Linq;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class FactoryPurchacePriceDataGridView : DcsDataGridViewBase {
        protected readonly string[] kvNames = {
            "PurchasePriceType"
        };
        public FactoryPurchacePriceDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualFactoryPurchacePrice);
            }
        }
       
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    
                    new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierCode,
                        Name = "SupplierCode"
                    },  new ColumnItem {
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierName,
                        Name = "SupplierName"
                    }, new ColumnItem {
                        Name = "SparePartCode",
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartCode,
                    }, new ColumnItem {
                        Name = "HYCode",
                       Title = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                    }, new ColumnItem {
                        Name = "SparePartName",
                       Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                    }, new ColumnItem {
                        Name = "ContractPrice",
                        Title = CommonUIStrings.DataGridView_Title_VirtualFactoryPurchacePrice_ContractPrice
                    },
                    new ColumnItem {
                        Name = "IsOrderable",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_VirtualPartsBranch_IsOrderable,
                    },
                    new ColumnItem {
                        Name = "IsGenerate",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_VirtualFactoryPurchacePrice_IsGenerate,
                    }
                    , new KeyValuesColumnItem{
                        Name = "IsTemp",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_IsZg,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }
                    , new ColumnItem {
                        Name = "Rate",
                        Title = CommonUIStrings.DataGridView_Title_VirtualFactoryPurchacePrice_Rate
                    }
                    , new ColumnItem {
                        Title = CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_ValidTo,
                        Name = "ValidFrom"
                    }, new ColumnItem {
                        Title = CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_ValidFrom,
                        Name = "ValidTo"
                    }, new ColumnItem {
                        Name = "UserCode",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_RolePersonnelReport_LoginId,
                    }, new ColumnItem {
                        Name = "LimitQty",
                        Title = CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_ClosedQuantity,
                    }, new ColumnItem {
                        Name = "SupplierPartCode",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsSupplierRelation_SupplierPartCode,
                    }, new ColumnItem {
                        Name = "Source",
                        Title = CommonUIStrings.DataGridView_Title_VirtualPartsPurchasePricing_DataSource,
                    }
                    , new ColumnItem {
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_VirtualFactoryPurchacePrice_CreateTime,
                        Name = "CreateTime"
                    }
                };
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.DataPager.PageSize = 200;
            ((GridViewDataColumn)this.GridView.Columns["ContractPrice"]).DataFormatString = "c2"; 
            ((GridViewDataColumn)this.GridView.Columns["Rate"]).DataFormatString = "p"; 
        }

        protected override string OnRequestQueryName() {
            return "查询SAP采购价";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var date = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem) && r.MemberName != "SparePartCode").Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "supplierCode":
                        return filters.Filters.Single(item => item.MemberName == "SupplierCode").Value;
                    case "supplierName":
                        return filters.Filters.Single(item => item.MemberName == "SupplierName").Value;
                    case "sparePartCode": 
                        var codes = filters.Filters.SingleOrDefault(s => s.MemberName == "SparePartCode");  
                        if(codes == null || codes.Value == null)  
                            return null;  
                        return codes.Value.ToString();  
                    case "sparePartName":
                        return filters.Filters.Single(item => item.MemberName == "SparePartName").Value;
                    case "hyCode":
                        return filters.Filters.Single(item => item.MemberName == "HYCode").Value;
                    case "bCreateTime":
                        var createTime = date.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return createTime == null ? null : createTime.Filters.First(r => r.MemberName == "CreateTime").Value;
                    case "eCreateTime":
                        var createTime1 = date.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return createTime1 == null ? null : createTime1.Filters.Last(item => item.MemberName == "CreateTime").Value;
                    case "isGenerate":
                        return filters.Filters.Single(item => item.MemberName == "IsGenerate").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
    }
}
