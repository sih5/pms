﻿using System;
using System.Linq;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;
using Sunlight.Silverlight.Dcs.Web.Entities;
using System.Collections.ObjectModel;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class FactoryPurchacePriceForHandDataGridView : DcsDataGridViewBase {
        private readonly ObservableCollection<KeyValuePair> kvType = new ObservableCollection<KeyValuePair>();

        protected readonly string[] kvNames = {
            "PurchasePriceType"
        };
        public FactoryPurchacePriceForHandDataGridView() {
            this.kvType.Add(new KeyValuePair {
                Key = 1,
                Value = "新增"
            });
            this.kvType.Add(new KeyValuePair {
                Key = 2,
                Value = "价格可维护"
            });
            this.kvType.Add(new KeyValuePair {
                Key = 3,
                Value = "临时价格更新"
            });
            this.kvType.Add(new KeyValuePair {
                Key = 4,
                Value = "最低采购价"
            });           
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(FactoryPurchacePriceForHand);
            }
        }
       
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {                                       
                      new ColumnItem {
                        Name = "SparePartCode",
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartCode,
                    }, new ColumnItem {
                        Name = "HYCode",
                       Title = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                    }, new ColumnItem {
                        Name = "SparePartName",
                       Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                    }, new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierCode,
                        Name = "SupplierCode"
                    },  new ColumnItem {
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierName,
                        Name = "SupplierName"
                    },                    
                    new ColumnItem {
                        Name = "IsPrimary",
                        Title = "是否首选供应商",
                    },                    
                    new ColumnItem {
                        Name = "IsOrderable",
                        Title = "是否可采购",
                    },new ColumnItem {
                        Name = "PartsSupplierNamePri",
                        Title = "现首选供应商名称",
                    },new ColumnItem {
                        Name = "PartsSupplierCodePri",
                        Title = "现首选供应商编号",
                    }, new ColumnItem {
                        Name = "ContractPrice",
                        Title = "价格"
                    }
                    , new KeyValuesColumnItem{
                        Name = "PriceType",
                        Title = "价格类型",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }
                    , new ColumnItem {
                        Name = "Rate",
                        Title = CommonUIStrings.DataGridView_Title_VirtualFactoryPurchacePrice_Rate
                    }
                    , new ColumnItem {
                        Title = CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_ValidTo,
                        Name = "ValidFrom"
                    }, new ColumnItem {
                        Title = CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_ValidFrom,
                        Name = "ValidTo"
                    } , new KeyValuesColumnItem{
                        Name = "Status",
                        Title = "待处理类型",
                        KeyValueItems = this.kvType
                    }, new ColumnItem {
                        Name = "Source",
                        Title = CommonUIStrings.DataGridView_Title_VirtualPartsPurchasePricing_DataSource,
                    }                   
                };
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.DataPager.PageSize = 200;
            ((GridViewDataColumn)this.GridView.Columns["ContractPrice"]).DataFormatString = "c2"; 
            ((GridViewDataColumn)this.GridView.Columns["Rate"]).DataFormatString = "p"; 
        }

        protected override string OnRequestQueryName() {
            return "查询采购价待处理";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var date = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem) && r.MemberName != "SparePartCode").Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "supplierCode":
                        return filters.Filters.Single(item => item.MemberName == "SupplierCode").Value;
                    case "supplierName":
                        return filters.Filters.Single(item => item.MemberName == "SupplierName").Value;
                    case "sparePartCode": 
                        var codes = filters.Filters.SingleOrDefault(s => s.MemberName == "SparePartCode");  
                        if(codes == null || codes.Value == null)  
                            return null;  
                        return codes.Value.ToString();  
                    case "sparePartName":
                        return filters.Filters.Single(item => item.MemberName == "SparePartName").Value;                  
                    case "status":
                        return filters.Filters.Single(item => item.MemberName == "Status").Value;
                    case "isOrderable":
                        return filters.Filters.Single(item => item.MemberName == "IsOrderable").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
    }
}
