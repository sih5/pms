﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;



namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class FaultyPartsDataGridView : DcsDataGridViewBase {


        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartCode,
                        Name = "Code"
                    },new ColumnItem {
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                        Name = "Name"
                    },new ColumnItem {
                        Title = CommonUIStrings.QueryPanel_Title_FaultyParts_SupplierCode,
                        Name = "SupplierCode"
                    },new ColumnItem {
                        Title = CommonUIStrings.QueryPanel_Title_FaultyParts_SupplierName,
                        Name = "SupplierName",
                    },new ColumnItem {
                        Title= CommonUIStrings.QueryPanel_Title_FaultyParts_ReferenceCode,
                        Name = "ReferenceCode"
                    }, new ColumnItem {
                         Title= CommonUIStrings.QueryPanel_Title_FaultyParts_ReferenceName,
                        Name = "ReferenceName"
                    }, new ColumnItem {
                         Title= CommonUIStrings.QueryPanel_Title_FaultyParts_MeasureUnit,
                        Name = "MeasureUnit",
                    }, new ColumnItem {
                         Title= CommonUIStrings.QueryPanel_Title_FaultyParts_Feature,
                        Name = "Feature"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(FaultyPart);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询祸首件2";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filterItem = this.FilterItem as CompositeFilterItem;
            if(filterItem == null)
                return null;
            var compositeFilterItem = filterItem.Filters.SingleOrDefault(item => item.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
            switch(parameterName.ToUpper()) {
                case "SPAREPARTCODE":
                    var codeFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "Code");
                    return codeFilterItem == null ? null : codeFilterItem.Value;
                case "SPAREPARTNAME":
                    var nameFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "Name");
                    return nameFilterItem == null ? null : nameFilterItem.Value;
                case "REFERENCECODE":
                    var referenceCodeFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "ReferenceCode");
                    return referenceCodeFilterItem == null ? null : referenceCodeFilterItem.Value;
                case "REFERENCENAME":
                    var referenceNameFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "ReferenceName");
                    return referenceNameFilterItem == null ? null : referenceNameFilterItem.Value;
                case "PARTSSALECATEGORYID":
                    var categoryFilterItem = compositeFilterItem.Filters.SingleOrDefault(e => e.MemberName == "partsSalecategoryId");
                    return categoryFilterItem == null ? null : categoryFilterItem.Value;
                case "SUPPLIERNAME":
                    var supplierCodeFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "SupplierName");
                    return supplierCodeFilterItem == null ? null : supplierCodeFilterItem.Value;
                case "SUPPLIERCODE":
                    var supplierNameFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "SupplierCode");
                    return supplierNameFilterItem == null ? null : supplierNameFilterItem.Value;
                case "PARTSID":
                    var partsIdFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "partsId");
                    return partsIdFilterItem == null ? null : partsIdFilterItem.Value;
                case "BRANCHID":
                    var branchIdFilterItem = compositeFilterItem.Filters.SingleOrDefault(e => e.MemberName == "branchId");
                    return branchIdFilterItem == null ? null : branchIdFilterItem.Value;
                default:
                    return null;
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var newCompositeFilterItem = new CompositeFilterItem();
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }
}
