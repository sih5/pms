﻿
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Resources;
using System.Collections.ObjectModel;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class ForceReserveBillDealerDataGridView : DcsDataGridViewBase {
        protected readonly string[] KvNames = {
           "ForceReserveBillStatus"
        };
        public readonly ObservableCollection<KeyValuePair> KvSubNames = new ObservableCollection<KeyValuePair>();

        public ForceReserveBillDealerDataGridView() {
            this.KeyValueManager.Register(this.KvNames);

         this.Initializer.Register(this.Initialize);

        }
        private void Initialize() {
            var dcsDomainContext = this.DomainContext as DcsDomainContext;
            if(dcsDomainContext == null)
                return;
            dcsDomainContext.Load(dcsDomainContext.GetBottomStockForceReserveSubsQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesOrderType in loadOp.Entities)
                    this.KvSubNames.Add(new KeyValuePair {
                        Key = partsSalesOrderType.Id,
                        Value = partsSalesOrderType.SubName
                    });
            }, null);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new KeyValuesColumnItem {
                        Name="Status",
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]]
                    },new ColumnItem {
                        Name = "CompanyCode",
                        Title =CommonUIStrings.DataDeTailPanel_Text_Company_Code
                    }, new ColumnItem {
                        Name = "CompanyName",
                        Title=CommonUIStrings.DataDeTailPanel_Text_Company_Name
                    }, new ColumnItem {
                        Name = "SubVersionCode",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_ForceReserveBill_SubVersionCode
                    }, new ColumnItem {
                        Name = "ColVersionCode",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_ForceReserveBill_ColVersionCode
                    }, new ColumnItem {
                        Name = "IsAttach",
                        Title =CommonUIStrings.DataGridView_Title_PartsPurchasePricingChange_IsUplodFile
                    }, new ColumnItem {
                        Name = "ReserveType",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_BottomStockForceReserveType_ReserveType
                    },new ColumnItem{
                        Name="ReserveTypeSubItem",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_ReserveTypeSubItem
                    }, new KeyValuesColumnItem {
                        Name="ReserveTypeSubItemId",
                        KeyValueItems = this.KvSubNames,
                        Title ="储备类别子项目名称"
                    },new ColumnItem {
                        Name = "ItemQty",
                        Title = "品种数"
                    },new ColumnItem {
                        Name = "FeeAmount",
                        Title = "储备总金额"
                    },new ColumnItem {
                        Name = "ValidateFrom",
                        Title = "生效时间"
                    }, new ColumnItem{
                        Name = "SubmitterName",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_SubmitterName
                    },new ColumnItem {
                        Name = "SubmitTime",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_SubmitTime
                    }, new ColumnItem{
                        Name = "CheckerName",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_CheckerName
                    },new ColumnItem {
                        Name = "CheckTime",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_CheckTime
                    },new ColumnItem {
                        Name = "ApproverName",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_ApproverName
                    }, new ColumnItem {
                        Name = "ApproveTime",
                        Title= CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_ApproveTime
                    }, new ColumnItem {
                        Name = "RejecterName",
                         Title= CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_RejecterName
                    }, new ColumnItem {
                        Name = "RejectTime",
                         Title= CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_RejectTime
                    }, new ColumnItem {
                        Name = "CreatorName",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_CreatorName
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title = CommonUIStrings.QueryPanel_Title_ProductStandard_CreateTime,
                    }, new ColumnItem {
                        Name = "ModifierName",
                        Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_ModifierName
                    }, new ColumnItem {
                        Name = "ModifyTime",
                         Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_ModifyTime
                    }, new ColumnItem {
                        Name = "AbandonerName",
                        Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_AbandonerName
                    }, new ColumnItem {
                        Name = "AbandonTime",
                         Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_AbandonerTime
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetForceReserveBillsForDealer";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "ForceReserveBillDetail"
                    }
                };
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                foreach(var item in compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).Where(filter => filter.MemberName != "CenterName"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch(parameterName) {
                    case "centerName":
                        return filters.Filters.Single(item => item.MemberName == "CenterName").Value;
                    case "status":
                        return filters.Filters.Single(item => item.MemberName == "Status").Value;
                    case "subVersionCode":
                        return filters.Filters.Single(item => item.MemberName == "SubVersionCode").Value;
                    case "colVersionCode":
                        return filters.Filters.Single(item => item.MemberName == "ColVersionCode").Value;
                    case "reserveType":
                        return filters.Filters.Single(item => item.MemberName == "ReserveType").Value;
                    case "reserveTypeSubItem":
                        return filters.Filters.Single(item => item.MemberName == "ReserveTypeSubItem").Value;
                    case "companyCode":
                        return filters.Filters.Single(item => item.MemberName == "CompanyCode").Value;
                    case "companyName":
                        return filters.Filters.Single(item => item.MemberName == "CompanyName").Value;
                   
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override Type EntityType {
            get {
                return typeof(ForceReserveBill);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
        }
    }
}
