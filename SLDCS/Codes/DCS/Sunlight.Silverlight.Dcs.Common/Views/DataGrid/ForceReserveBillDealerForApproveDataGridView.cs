﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using FilterOperator = Sunlight.Silverlight.Core.Model.FilterOperator;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class ForceReserveBillDealerForApproveDataGridView : DcsDataGridViewBase {
        protected readonly string[] KvNames = {
            "ABCSetting_Type"
        };
        public ForceReserveBillDealerForApproveDataGridView() {
            this.KeyValueManager.Register(this.KvNames);
        }     
        protected override Type EntityType {
            get {
                return typeof(ForceReserveBillDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name="SparePartCode",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_SparePartCode,
                        IsReadOnly=true
                    }, new ColumnItem{
                        Name = "SparePartName",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_SparePartName,
                        IsReadOnly = true
                    }, new ColumnItem{
                        Name = "SuggestForceReserveQty",
                        Title="强制储备数量",
                         IsReadOnly = true
                    }, new ColumnItem{
                        Name = "ForceReserveQty",
                         Title="储备数量"
                    }, new ColumnItem{
                        Name = "CenterPrice",
                         Title="储备价格",
                        IsReadOnly = true
                    }, new KeyValuesColumnItem{
                        Name = "CenterPartProperty",
                        IsReadOnly = true,
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_CenterPartProperty,
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]]
                    }
                };
            }
        }
   
        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("ForceReserveBillDetails");
        }
        protected override bool UsePaging {
            get {
                return false;
            }
        }
        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            var bill = this.DataContext as ForceReserveBill;
            if(bill == null)
                return;
            bool isCanEdit = false;
             if(bill.ReserveType == "T" || bill.Status == (int)DCSForceReserveBillStatus.已确认) {
                isCanEdit = true;
            }
            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "ForceReserveQty":
                    if(isCanEdit)
                        e.Cancel = true;
                    break;
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.BeginningEdit += GridView_BeginningEdit;
        }      
    }
}
