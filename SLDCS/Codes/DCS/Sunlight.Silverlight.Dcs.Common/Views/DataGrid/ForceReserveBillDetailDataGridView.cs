﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;


namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class ForceReserveBillDetailDataGridView  : DcsDataGridViewBase {
        protected readonly string[] KvNames = {
            "ABCSetting_Type"
        };

        public ForceReserveBillDetailDataGridView() {
            this.KeyValueManager.Register(this.KvNames);
            this.DataContextChanged += this.PartsBranchDataGridView_DataContextChanged;
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_SparePartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_SparePartName
                    }, new ColumnItem {
                        Name = "SuggestForceReserveQty",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_SuggestForceReserveQty
                    }, new ColumnItem {
                        Name = "ForceReserveQty",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_ForceReserveQty
                    }, new ColumnItem {
                        Name = "CenterPrice",
                        Title="储备价格"
                    }, new ColumnItem {
                        Name = "ReserveFee",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_ReserveFee
                    }, new KeyValuesColumnItem {
                        Name = "CenterPartProperty",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_CenterPartProperty,
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]]
                    }
                };
            }
        }

        private void PartsBranchDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var sparePart = e.NewValue as ForceReserveBill;
            if(sparePart == null || sparePart.Id == default(int))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "ForceReserveBillId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                };
            this.FilterItem.Value = sparePart.Id;
            this.ExecuteQueryDelayed();
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override string OnRequestQueryName() {
            return "GetForceReserveBillDetails";
        }

        protected override Type EntityType {
            get {
                return typeof(ForceReserveBillDetail);
            }
        }
    }
}