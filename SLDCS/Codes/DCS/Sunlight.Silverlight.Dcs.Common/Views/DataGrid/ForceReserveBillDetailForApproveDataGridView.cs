﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using FilterOperator = Sunlight.Silverlight.Core.Model.FilterOperator;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class ForceReserveBillDetailForApproveDataGridView : DcsDataGridViewBase {
        protected readonly string[] KvNames = {
            "ABCSetting_Type"
        };
        public ForceReserveBillDetailForApproveDataGridView() {
            this.KeyValueManager.Register(this.KvNames);
            this.DataContextChanged += this.PartsBranchDataGridView_DataContextChanged;
        }     
        protected override Type EntityType {
            get {
                return typeof(ForceReserveBillDetailQuery);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name="SparePartCode",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_SparePartCode,
                        IsReadOnly=true
                    }, new ColumnItem{
                        Name = "SparePartName",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_SparePartName,
                        IsReadOnly = true
                    }, new ColumnItem{
                        Name = "SuggestForceReserveQty",
                        Title="强制储备数量",
                         IsReadOnly = true
                    }, new ColumnItem{
                        Name = "ForceReserveQty",
                         Title="储备数量"
                    }, new ColumnItem{
                        Name = "CenterPrice",
                         Title="储备价格",
                        IsReadOnly = true
                    }, new KeyValuesColumnItem{
                        Name = "CenterPartProperty",
                        IsReadOnly = true,
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_CenterPartProperty,
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]]
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetForceReserveBillDetailsOrder";
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                foreach(var item in compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).Where(filter => filter.MemberName != "forceReserveBillId" ))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }


        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch(parameterName) {
                    case "forceReserveBillId":
                        return filters.Filters.Single(item => item.MemberName == "ForceReserveBillId").Value;                  
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override bool UsePaging {
            get {
                return false;
            }
        }
        private void PartsBranchDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var sparePart = e.NewValue as ForceReserveBill;
            if(sparePart == null || sparePart.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "ForceReserveBillId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = sparePart.Id
            });         
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.RowStyleSelector = this.GridViewStyleSelector();
            this.GridView.BeginningEdit += GridView_BeginningEdit;
        }
        private GridViewStyleSelector GridViewStyleSelector() {
            return new GridViewStyleSelector {
                ExecuteFunc = (item, container) => {
                    var bill = this.DataContext as ForceReserveBill;
                    if(item is ForceReserveBillDetailQuery) {
                        var detail = item as ForceReserveBillDetailQuery;
                        if(detail.SuggestForceReserveQty!=detail.ForceReserveQty&&bill.ReserveType=="L")
                            return this.Resources["lightRedDataBackground"] as Style;                       
                    }
                    return null;
                }
            };
        }
        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            var bill = this.DataContext as ForceReserveBill;
            if(bill == null)
                return;
            bool isCanEdit = false;
            if(bill.CompanyType == (int)DcsCompanyType.代理库) {
                isCanEdit = true;
            } else if(bill.ReserveType == "T" || bill.Status == (int)DCSForceReserveBillStatus.已确认) {
                isCanEdit = true;
            }
            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "ForceReserveQty":
                    if(isCanEdit)
                        e.Cancel = true;
                    break;
            }
        }
    }
}
