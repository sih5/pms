﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class ForceReserveBillDetailForEditDataGridView : DcsDataGridViewBase {
        protected readonly string[] KvNames = {
            "ABCSetting_Type"
        };
        public ForceReserveBillDetailForEditDataGridView() {
            this.KeyValueManager.Register(this.KvNames);
        }

        private DcsMultiPopupsQueryWindowBase virtualPartsStockQueryWindow;

        private DcsMultiPopupsQueryWindowBase VirtualPartsStockQueryWindow {
            get {
                if(this.virtualPartsStockQueryWindow == null) {
                    this.virtualPartsStockQueryWindow = DI.GetQueryWindow("VirtualSpartPartByClass") as DcsMultiPopupsQueryWindowBase;
                    this.virtualPartsStockQueryWindow.SelectionDecided += this.VirtualPartsStockQueryWindow_SelectionDecided;
                }
                return this.virtualPartsStockQueryWindow;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(ForceReserveBillDetailTemp);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name="CompanyCode",
                        Title=CommonUIStrings.DataDeTailPanel_Text_Company_Code,
                        IsReadOnly=true
                    }, new ColumnItem{
                        Name = "CompanyName",
                        Title=CommonUIStrings.DataDeTailPanel_Text_Company_Name,
                        IsReadOnly = true
                    },new ColumnItem{
                        Name="SparePartCode",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_SparePartCode,
                        IsReadOnly=true
                    }, new ColumnItem{
                        Name = "SparePartName",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_SparePartName,
                        IsReadOnly = true
                    }, new ColumnItem{
                        Name = "SuggestForceReserveQty",
                        Title="强制储备数量",
                         IsReadOnly = true
                    }, new ColumnItem{
                        Name = "ForceReserveQty",
                         Title="储备数量",
                    }, new ColumnItem{
                        Name = "CenterPrice",
                         Title="储备价格",
                        IsReadOnly = true
                    }, new KeyValuesColumnItem{
                        Name = "CenterPartProperty",
                        IsReadOnly = true,
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_CenterPartProperty,
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]]
                    }
                };
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("ForceReserveBillDetailTempDetails");
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.AddingNewDataItem -= GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            var forceReserveBill = this.DataContext as ForceReserveBillCenterDataEditView;
            if(forceReserveBill == null)
                return;
            if(forceReserveBill.ReserveType == "") {
                UIHelper.ShowNotification("请选择储备类别");
                return;
            }
            if(forceReserveBill.ReserveType != "L") {
                return;
            }
            RadQueryWindow.ShowDialog();
        }

        private RadWindow radQueryWindow;
        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = this.VirtualPartsStockQueryWindow,
                    Header = "配件信息查询",
                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                });
            }
        }

        private void VirtualPartsStockQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            var dataEditView = this.DataContext as ForceReserveBillCenterDataEditView;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var virtualPartsStocks = queryWindow.SelectedEntities.Cast<ForceReserveBillDetailQuery>().ToArray();
            if(!virtualPartsStocks.Any())
                return;
            foreach(var virtualPartsStock in virtualPartsStocks) {
                if(dataEditView.ForceReserveBillDetailTempDetails.Any(t => t.SparePartId == virtualPartsStock.SparePartId && t.CompanyId == virtualPartsStock.CompanyId)) {
                    continue;
                }
                var borrowBillDetail = new ForceReserveBillDetailTemp {
                    SparePartId = virtualPartsStock.SparePartId,
                    SparePartCode = virtualPartsStock.SparePartCode,
                    SparePartName = virtualPartsStock.SparePartName,
                    CompanyId = virtualPartsStock.CompanyId,
                    CompanyCode = virtualPartsStock.CompanyCode,
                    CompanyName = virtualPartsStock.CompanyName,
                    SuggestForceReserveQty = virtualPartsStock.SuggestForceReserveQty,
                    ForceReserveQty = virtualPartsStock.SuggestForceReserveQty,
                    CenterPartProperty = virtualPartsStock.CenterPartProperty,
                    CenterPrice = virtualPartsStock.CenterPrice
                };
                dataEditView.ForceReserveBillDetailTempDetails.Add(borrowBillDetail);
            }
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }
    }
}