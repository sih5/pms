﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class GoldenTaxClassifyMsgDataGridView : DcsDataGridViewBase {
        protected override Type EntityType {
            get {
                return typeof(GoldenTaxClassifyMsg);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title =  CommonUIStrings.QueryPanel_QueryItem_Title_GoldenTaxClassifyMsg_GoldenTaxClassifyCode,
                        Name = "GoldenTaxClassifyCode"
                    }, new ColumnItem {
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_GoldenTaxClassifyMsg_GoldenTaxClassifyName,
                        Name = "GoldenTaxClassifyName"
                    }, new ColumnItem {
                        Name = "CreatorName",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_CreatorName
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title = CommonUIStrings.QueryPanel_Title_ProductStandard_CreateTime,
                    }, new ColumnItem {
                        Name = "ModifierName",
                        Title = CommonUIStrings.DataDeTailPanel_Text_BottomStock_ModifierName
                    }, new ColumnItem {
                        Name = "ModifyTime",
                         Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_ModifyTime
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetGoldenTaxClassifyMsgs";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
        }
    }
}
