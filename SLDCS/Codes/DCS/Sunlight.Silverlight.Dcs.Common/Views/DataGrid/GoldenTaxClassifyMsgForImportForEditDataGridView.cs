﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class GoldenTaxClassifyMsgForImportForEditDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name="GoldenTaxClassifyCode",
                        Title =  CommonUIStrings.QueryPanel_QueryItem_Title_GoldenTaxClassifyMsg_GoldenTaxClassifyCode,
                    },new ColumnItem{
                        Name="GoldenTaxClassifyName",
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_GoldenTaxClassifyMsg_GoldenTaxClassifyName
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(GoldenTaxClassifyMsg);
            }
        }
    }
}
