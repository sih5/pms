﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class LogisticCompanyDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "MasterData_Status","Storage_Center"
        };

        public LogisticCompanyDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(LogisticCompany);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "Name"
                    }, new ColumnItem {
                        Name = "Abbreviation"
                     }, new KeyValuesColumnItem {
                        Name = "StorageCenter",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_LogisticCompany_StorageCenter,
                        KeyValueItems=this.KeyValueManager[this.kvNames[1]]
                     }, new ColumnItem {
                        Name = "IsExpressCompany",
                        Title=CommonUIStrings.DataEditPanel_Text_LogisticCompany_IsExpressCompany
                    },  new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetLogisticCompanies";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
        }
    }
}
