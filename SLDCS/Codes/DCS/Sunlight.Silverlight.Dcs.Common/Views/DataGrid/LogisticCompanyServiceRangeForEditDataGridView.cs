﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class LogisticCompanyServiceRangeForEditDataGridView : DcsDataGridViewBase {
        //private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        //private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
            "LogisticCompany_BusinessDomain"
        };

        public LogisticCompanyServiceRangeForEditDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
            //this.Initializer.Register(this.Initialize);
        }

        //private void Initialize() {
            //dcsDomainContext.Load(dcsDomainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    foreach(var branch in loadOp.Entities)
            //        this.kvBranches.Add(new KeyValuePair {
            //            Key = branch.Id,
            //            Value = branch.Name
            //        });
            //}, null);
        //}

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_Branch_Name,
                        Name = "BranchName",
                        IsReadOnly = true
                    }, new KeyValuesColumnItem {
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_LogisticCompanyServiceRange_BusinessDomain,
                        Name = "BusinessDomain",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(LogisticCompanyServiceRange);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("LogisticCompanyServiceRanges");
        }
    }
}
