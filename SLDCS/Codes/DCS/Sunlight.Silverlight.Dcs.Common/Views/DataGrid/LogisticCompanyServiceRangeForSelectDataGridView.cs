﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class LogisticCompanyServiceRangeForSelectDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "LogisticCompany_BusinessDomain", "MasterData_Status", "Storage_Center"
        };

        public LogisticCompanyServiceRangeForSelectDataGridView() {
            this.KeyValueManager.Register(kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "LogisticCompany.Status",
                        KeyValueItems = this.KeyValueManager[kvNames[1]]
                    },new ColumnItem {
                        Name = "LogisticCompany.Code"
                    }, new ColumnItem {
                        Name = "LogisticCompany.Name"
                    }, new KeyValuesColumnItem {
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_LogisticCompanyServiceRange_BusinessDomain,
                        Name = "BusinessDomain",
                        KeyValueItems = this.KeyValueManager[kvNames[0]]
                    }, new KeyValuesColumnItem {
                        Name = "LogisticCompany.StorageCenter",
                        KeyValueItems = this.KeyValueManager[kvNames[2]]
                    },  new ColumnItem {
                        Name = "LogisticCompany.CreatorName"
                    }, new ColumnItem {
                        Name = "LogisticCompany.CreateTime"
                    }, new ColumnItem {
                        Name = "LogisticCompany.ModifierName"
                    }, new ColumnItem {
                        Name = "LogisticCompany.ModifyTime"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(LogisticCompanyServiceRange);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetLogisticCompanyServiceRangesWithLogisticCompany";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.IsReadOnly = true;
        }
    }
}
