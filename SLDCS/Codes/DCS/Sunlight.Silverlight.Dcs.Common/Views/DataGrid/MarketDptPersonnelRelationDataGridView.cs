﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class MarketDptPersonnelRelationDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
           "BaseData_Status","DepartmentPersonType"
        };

        public MarketDptPersonnelRelationDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                          KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "MarketingDepartment.Code",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_MarketDptPersonnelRelation_Code
                    },new ColumnItem {
                        Name = "MarketingDepartment.Name",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_MarketDptPersonnelRelation_Name
                    }, new KeyValuesColumnItem {
                        Name = "Type",
                        Title =  CommonUIStrings.QueryPanel_QueryItem_Title_MarketDptPersonnelRelation_Type,
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Name = "Personnel.Name",
                    }, new ColumnItem {
                        Name = "Personnel.CellNumber",
                        Title = CommonUIStrings.DataGridView_Title_MarketDptPersonnelRelation_CellNumber
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem{
                        Name="ModifyTime"
                    },new ColumnItem{
                        Name="AbandonerName"
                    },new ColumnItem{
                        Name="AbandonTime"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(MarketDptPersonnelRelation);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override string OnRequestQueryName() {
            return "GetMarketDptPersonnelRelationDetail";
        }

    }
}
