﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;


namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class MarketDptPersonnelRelationForEditDataGridView : DcsDataGridViewBase {
        public MarketDptPersonnelRelationForEditDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        private readonly string[] kvNames = {
           "BaseData_Status","DepartmentPersonType"
        };
        public DcsMultiPopupsQueryWindowBase personnelQueryWindow;
        public QueryWindowBase dropDownQueryWindow;
        private RadWindow radQueryWindow;
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Personnel.SequeueNumber",
                        Title=CommonUIStrings.DataGridView_Title_SequeueNumber,
                        IsReadOnly = true
                    },new DropDownTextBoxColumnItem {
                        Name = "Personnel.Name",
                      DropDownContent = this.DropDownQueryWindow
                    }, new ColumnItem {
                        Name = "Personnel.CellNumber",
                        IsReadOnly = true
                    },new KeyValuesColumnItem {
                        Name = "Type",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_MarketDptPersonnelRelation_Type
                    }
                };
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(MarketDptPersonnelRelation);
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.CanUserInsertRows = true;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.IsReadOnly = false;
            this.GridView.Deleted += this.GridView_Deleted;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var maxSerialNumber = this.GridView.Items.Count > 0 ? this.GridView.Items.Cast<MarketDptPersonnelRelation>().Max(entity => entity.Personnel.SequeueNumber) + 1 : 1;
            e.NewObject = new Personnel {
                SequeueNumber = maxSerialNumber
            };
            RadQueryWindow.ShowDialog();
            e.Cancel = true;
        }

        public QueryWindowBase DropDownQueryWindow {
            get {
                if(this.dropDownQueryWindow == null) {
                    this.dropDownQueryWindow = DI.GetQueryWindow("PersonnelDropDown");
                    this.dropDownQueryWindow.SelectionDecided += dropDownQueryWindow_SelectionDecided;
                    this.dropDownQueryWindow.Loaded += personnelQueryWindow_Loaded;
                }
                return dropDownQueryWindow;
            }
        }

        private void dropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var maxSerialNumber = this.GridView.Items.Count > 0 ? this.GridView.Items.Cast<MarketDptPersonnelRelation>().Count() + 1 : 1;
            var queryWindowpersonnel = queryWindow.SelectedEntities.Cast<Personnel>().FirstOrDefault();
            if(queryWindowpersonnel == null)
                return;
            var marketDptPersonnelRelationQ = queryWindow.DataContext as MarketDptPersonnelRelation;
            var dataEditView = this.DataContext as MarketDptPersonnelRelationDataEditView;
            if(dataEditView == null)
                return;
            if(dataEditView.MarketDptPersonnelRelations.Any(ex => ex.PersonnelId == queryWindowpersonnel.Id)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Error_PersonSubDealer_PersonnelName);
                return;
            }
            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.Load(domainContext.GetPersonnelsQuery().Where(d => d.Id == queryWindowpersonnel.Id), loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                if(marketDptPersonnelRelationQ != null) {
                    var personnels = loadOp.Entities.SingleOrDefault();
                    if(personnels != null) {
                        personnels.SequeueNumber = maxSerialNumber;
                        marketDptPersonnelRelationQ.Personnel = personnels;
                    }
                }
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }, null);
        }


        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = this.PersonnelQueryWindow,
                    Header = CommonUIStrings.QueryPanel_Title_Personnel,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen
                });
            }
        }

        private void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var dataEditView = this.DataContext as MarketDptPersonnelRelationDataEditView;
            if(dataEditView == null)
                return;
            var serialNumber = 1;
            foreach(var item in dataEditView.MarketDptPersonnelRelations.Where(entity => !e.Items.Contains(entity))) {
                item.Personnel.SequeueNumber = serialNumber;
                serialNumber++;
            }
            //删除市场部与人员关系
            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            foreach(var item in e.Items.Cast<MarketDptPersonnelRelation>().Where(item => domainContext.MarketDptPersonnelRelations.Contains(item))) {
                if(item.Can作废市场部与人员关系)
                    item.作废市场部与人员关系();
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("MarketDptPersonnelRelations");
        }

        public DcsMultiPopupsQueryWindowBase PersonnelQueryWindow {
            get {
                if(this.personnelQueryWindow == null) {
                    this.personnelQueryWindow = DI.GetQueryWindow("PersonnelForMultiSelect") as DcsMultiPopupsQueryWindowBase;
                    this.personnelQueryWindow.SelectionDecided += PersonnelQueryWindow_SelectionDecided;
                    this.personnelQueryWindow.Loaded += personnelQueryWindow_Loaded;
                }
                return personnelQueryWindow;
            }
        }

        private void personnelQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null)
                return;
            var filterItem = new FilterItem();
            filterItem.MemberName = "CorporationId";
            filterItem.MemberType = typeof(int);
            filterItem.Operator = FilterOperator.IsEqualTo;
            filterItem.Value = BaseApp.Current.CurrentUserData.EnterpriseId;
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", filterItem);
            queryWindow.ExchangeData(null, "RefreshQueryResult", null);
        }

        private void PersonnelQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var queryWindowpersonnel = queryWindow.SelectedEntities.Cast<Personnel>();
            if(queryWindowpersonnel == null)
                return;
            var marketDptPersonnelRelationQ = queryWindow.DataContext as MarketDptPersonnelRelation;
            var dataEditView = this.DataContext as MarketDptPersonnelRelationDataEditView;
            if(dataEditView == null)
                return;
            if(dataEditView.MarketDptPersonnelRelations.Any(ex => queryWindowpersonnel.Any(r => r.Id == ex.PersonnelId))) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Error_PersonSubDealer_PersonnelName);
                return;
            }
            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            var maxSerialNumber = dataEditView.MarketDptPersonnelRelations.Count;
            foreach(var item in queryWindowpersonnel) {
                domainContext.Load(domainContext.GetPersonnelsQuery().Where(d => d.Id == item.Id), loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    var personnels = loadOp.Entities.SingleOrDefault();
                    maxSerialNumber++;
                    if(personnels != null) {
                        personnels.SequeueNumber = maxSerialNumber;
                        dataEditView.MarketDptPersonnelRelations.Add(new MarketDptPersonnelRelation {
                            PersonnelId = item.Id,
                            Status = (int)DcsBaseDataStatus.有效,
                            Personnel = personnels
                        });
                    }
                }, null);
            }
        }

    }
}
