﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class MarketDptPersonnelRelationImportDataGridView : DcsDataGridViewBase {

        private readonly string[] kvNames = {
        "DepartmentPersonType"
        };

        public MarketDptPersonnelRelationImportDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "MarketingDepartment.Code",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_MarketDptPersonnelRelation_Code
                    },new ColumnItem {
                        Name = "MarketingDepartment.Name",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_MarketDptPersonnelRelation_Name
                    }, new ColumnItem {
                        Name = "Personnel.Name",
                    },new KeyValuesColumnItem {
                        Name = "Type",
                        Title =  CommonUIStrings.QueryPanel_QueryItem_Title_MarketDptPersonnelRelation_Type,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Branch.Name",
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(MarketDptPersonnelRelation);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }
    }
}
