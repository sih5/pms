using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class MarketingDepartmentDataGridView : DcsDataGridViewBase {
        protected readonly string[] KvNames = {
            "MarketingDepartment_Type", "BaseData_Status"
        };

        public MarketingDepartmentDataGridView() {
            this.KeyValueManager.Register(this.KvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.KvNames[1]]
                    }, new ColumnItem {
                        Name = "Code",
                        IsSortDescending = false,
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_MarketDptPersonnelRelation_Code
                    }, new ColumnItem {
                        Name = "Name",
                         Title = CommonUIStrings.QueryPanel_QueryItem_Title_MarketDptPersonnelRelation_Name
                    }, new ColumnItem{
                        Name="PartsSalesCategory.Name"
                    }, new KeyValuesColumnItem {
                        Name = "BusinessType",
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]]
                    },new ColumnItem {
                        Name = "Phone"
                    }, new ColumnItem {
                        Name = "Fax"
                    }, new ColumnItem {
                        Name = "PostCode"
                    }, new ColumnItem {
                        Name = "ProvinceName",
                        Title=CommonUIStrings.DataGrid_QueryItem_Title_ProvinceName
                    }, new ColumnItem {
                        Name = "CityName",
                        Title=CommonUIStrings.DataGrid_QueryItem_Title_CityName
                    }, new ColumnItem {
                        Name = "CountyName",
                        Title=CommonUIStrings.DataGrid_QueryItem_Title_CountyName
                    },new ColumnItem{
                       Name="CreatorName"
                    },new ColumnItem{
                       Name="CreateTime"
                    },new ColumnItem{
                       Name="ModifierName"
                    },new ColumnItem{
                       Name="ModifyTime"
                    }, new ColumnItem {
                        Name = "Branch.Name"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetMarketingDepartmentsWithDetails";
        }

        protected override Type EntityType {
            get {
                return typeof(MarketingDepartment);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
        }
    }
}
