﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class MarketingDepartmentForCAMDataGridView : DcsDataGridViewBase {
        protected readonly string[] KvNames = {
            "MarketingDepartment_Type", "BaseData_Status"
        };

        public MarketingDepartmentForCAMDataGridView() {
            this.KeyValueManager.Register(this.KvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.KvNames[1]]
                    },new ColumnItem {
                        Name = "Code",
                        IsSortDescending = false,
                        Title= CommonUIStrings.DataGridView_ColumnItem_Title_MarketingDepartment_Code
                    }, new ColumnItem {
                        Name = "Name",
                        Title= CommonUIStrings.DataGridView_ColumnItem_Title_MarketingDepartment_Name
                    }, new KeyValuesColumnItem {
                        Name = "BusinessType",
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]]
                    },  new ColumnItem {
                        Name = "Address"
                    }, new ColumnItem {
                        Name = "Phone"
                    }, new ColumnItem {
                        Name = "Fax"
                    }, new ColumnItem {
                        Name = "PostCode"
                    }, new ColumnItem {
                        Name = "TiledRegion.ProvinceName"
                    }, new ColumnItem {
                        Name = "TiledRegion.CityName"
                    }, new ColumnItem {
                        Name = "TiledRegion.CountyName"
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "Branch.Name",
                        Title= CommonUIStrings.DataGridView_ColumnItem_Title_MarketingDepartment_BranchName
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetMarketingDepartmentsWithDetails";
        }

        protected override Type EntityType {
            get {
                return typeof(MarketingDepartment);
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "Dealer"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
