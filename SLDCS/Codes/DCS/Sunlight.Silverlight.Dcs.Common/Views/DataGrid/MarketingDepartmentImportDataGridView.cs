﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class MarketingDepartmentImportDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        IsSortDescending = false,
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_MarketDptPersonnelRelation_Code
                    }, new ColumnItem {
                        Name = "Name",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_MarketDptPersonnelRelation_Name
                    }, new ColumnItem {
                        Name = "BusinessType",
                    }, new ColumnItem {
                        Name = "Address",
                        Title = CommonUIStrings.QueryPanel_Title_MarketingDepartment_Jurisdiction
                    }, new ColumnItem {
                        Name = "Phone"
                    }, new ColumnItem {
                        Name = "Fax"
                    }, new ColumnItem {
                        Name = "PostCode"
                    }, new ColumnItem {
                        Name = "ProvinceName",
                        Title=CommonUIStrings.DataGrid_QueryItem_Title_ProvinceName
                    }, new ColumnItem {
                        Name = "CityName",
                        Title=CommonUIStrings.DataGrid_QueryItem_Title_CityName
                    }, new ColumnItem {
                        Name = "CountyName",
                        Title=CommonUIStrings.DataEditPanel_Text_Region_District
                    }, new ColumnItem {
                        Name = "Remark"
                    },new ColumnItem{
                        Name="PartsSalesCategory.Name"
                    }, new ColumnItem {
                        Name = "Branch.Name"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(MarketingDepartment);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }
    }
}
