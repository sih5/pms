﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using SelectionMode = System.Windows.Controls.SelectionMode;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class MultiLevelApproveConfigDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames ={
           "BaseData_Status","MultiLevelApproveConfigType"
        };

        public MultiLevelApproveConfigDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(MultiLevelApproveConfig);
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionMode = SelectionMode.Single;
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Type",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title = "类型"
                    },
                    new ColumnItem {
                        Name = "ApproverName",
                        Title = "审核人",
                    },
                    new ColumnItem {
                        Name = "MinApproveFee",
                        Title = "审核金额下限",
                    }, new ColumnItem {
                        Name = "MaxApproveFee",
                        Title = "审核金额上限",
                    }, new ColumnItem {
                        Name = "IsFinished",
                        Title = "是否结束"
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_Common_Status
                    }
                    , new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetMultiLevelApproveConfigs";
        }
    }
}
