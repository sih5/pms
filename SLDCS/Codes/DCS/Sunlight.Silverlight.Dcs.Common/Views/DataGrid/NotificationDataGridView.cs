﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Channels.Views.DataGrid {
    public class NotificationDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
             "Notification_Type","BaseData_Status","NotificationUrgentLevel"
        };

        protected override Type EntityType {
            get {
                return typeof(Notification);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        IsSortable = false
                    },new ColumnItem {
                        Name = "Code",
                        IsSortable = false
                     }, new KeyValuesColumnItem {
                        Name = "Type",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        IsSortable = false
                    }, new ColumnItem {
                        Name = "Title",
                        IsSortable = false
                    }, new ColumnItem {
                        Name = "CreatorName",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_Notification_CreatorName,
                        IsSortable = false
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_Notification_CreateTime,
                        IsSortable = false
                    },new ColumnItem {
                        Name = "Top",
                        IsSortable = false
                    },new ColumnItem {
                        Name = "TopModifierName",
                        IsSortable = false
                    }, new ColumnItem {
                        Name = "TopTime",
                        IsSortable = false
                    }, new KeyValuesColumnItem{
                        Name = "UrgentLevel",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]],
                        IsSortable = false,
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_Notification_UrgentLevel,
                    }, new ColumnItem{
                        Name = "PublishDep",
                         Title = CommonUIStrings.QueryPanel_QueryItem_Title_Notification_PublishDep
                    }, new ColumnItem{
                        Name = "HavePath",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_Notification_IsHasFile,
                    },  new ColumnItem {
                        Name = "AbandonerName",
                        IsSortable = false
                    }, new ColumnItem {
                        Name = "AbandonTime",
                        IsSortable = false
                    }, new ColumnItem {
                        Name = "ModifierName",
                        IsSortable = false
                    }, new ColumnItem {
                        Name = "ModifyTime",
                        IsSortable = false
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.RowLoaded += GridView_RowLoaded;
        }

        private void GridView_RowLoaded(object sender, RowLoadedEventArgs e) {

            var notification = e.Row.DataContext as Notification;
            if(notification == null)
                return;
            GridViewRow gridViewRow = this.GridView.GetRowForItem(notification);

            if(notification.UrgentLevel == (int)DcsNotificationUrgentLevel.重要) {
                if(gridViewRow != null) {
                    gridViewRow.Background = new SolidColorBrush(Colors.Red);
                }
            } else {
                if(gridViewRow != null)
                    gridViewRow.Background = new SolidColorBrush(Colors.White);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetNotificationWithNotificationLimits";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "NotificationReply"
                    }
                };
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var filterItem = filters.Filters;
                switch(parameterName) {
                    case "isHasFile":
                        var isHasFile = filterItem.SingleOrDefault(item => item.MemberName == "IsHasFile");
                        if(isHasFile == null)
                            break;
                        return isHasFile.Value;
                    case "companyType":
                        var companyType = filterItem.SingleOrDefault(item => item.MemberName == "CompanyType");
                        if(companyType == null)
                            return null;
                        return companyType.Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "IsHasFile" && filter.MemberName != "CompanyType"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        public NotificationDataGridView() {
            this.KeyValueManager.Register(this.kvNames);

        }
    }
}
