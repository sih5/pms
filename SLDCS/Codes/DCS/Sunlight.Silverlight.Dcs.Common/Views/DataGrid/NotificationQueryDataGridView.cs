﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using System.Windows.Media;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class NotificationQueryDataGridView : DcsDataGridViewBase {
        private int type;
        private readonly string[] kvNames = {
             "Notification_Type","NotificationUrgentLevel"
        };

        protected override Type EntityType {
            get {
                return typeof(Notification);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        IsSortable = false
                     }, new KeyValuesColumnItem {
                        Name = "Type",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        IsSortable = false
                    }, new ColumnItem {
                        Name = "Title",
                        IsSortable = false
                    }, new ColumnItem {
                        Name = "CreatorName",
                       Title = CommonUIStrings.QueryPanel_QueryItem_Title_Notification_CreatorName,
                        IsSortable = false
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_Notification_CreateTime,
                        IsSortable = false
                    },new ColumnItem {
                        Name = "Top",
                        IsSortable = false
                    },new ColumnItem {
                        Name = "TopModifierName",
                        IsSortable = false
                    }, new ColumnItem {
                        Name = "TopTime",
                        IsSortable = false
                    }, new KeyValuesColumnItem{
                        Name = "UrgentLevel",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        IsSortable = false,
                       Title = CommonUIStrings.QueryPanel_QueryItem_Title_Notification_UrgentLevel,
                    }, new ColumnItem{
                        Name = "PublishDep",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_Notification_PublishDep
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.RowLoaded += GridView_RowLoaded;
        }

        private void GridView_RowLoaded(object sender, RowLoadedEventArgs e) {

            var notification = e.Row.DataContext as Notification;
            if(notification == null)
                return;
            GridViewRow gridViewRow = this.GridView.GetRowForItem(notification);

            if(notification.UrgentLevel == (int)DcsNotificationUrgentLevel.重要) {
                if(gridViewRow != null) {
                    gridViewRow.Background = new SolidColorBrush(Colors.Red);
                }
            } else {
                if(gridViewRow != null)
                    gridViewRow.Background = new SolidColorBrush(Colors.White);
            }
        }

        protected override string OnRequestQueryName() {
            return "目标企业查询公告";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "NotificationReply"
                    }
                };
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "desCompanyId":
                        return BaseApp.Current.CurrentUserData.EnterpriseId;
                    case "desCompanyTpye":
                        return type;
                    case "isHasFile":
                        var filterItem = filters.Filters.SingleOrDefault(item => item.MemberName == "IsHasFile");
                        if(filterItem == null)
                            break;
                        return filterItem.Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "IsHasFile"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        public NotificationQueryDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetCompaniesQuery().Where(e => e.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var entity = loadOp.Entities.FirstOrDefault();
                type = entity.Type;
            }, null);

        }
    }
}
