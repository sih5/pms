﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class NotificationReplyDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames ={
                "NotificationAsReplyChannel" 
                                          };

        public NotificationReplyDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += PartsExchangeHistoryDataGridView_DataContextChanged;
        }

        private void PartsExchangeHistoryDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsExchange = e.NewValue as Notification;
            if(partsExchange == null || partsExchange.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "NotificationId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsExchange.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override Type EntityType {
            get {
                return typeof(NotificationReply);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem{
                        Name="SerialNumber",
                        Title=CommonUIStrings.DataGridView_Title_SequeueNumber,
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "CompanyCode",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_CompanyCode
                    }, new ColumnItem {
                        Name = "CompanyName",
                        Title =CommonUIStrings.DataGridView_Title_NotificationReply_CompanyName
                    }, new KeyValuesColumnItem {
                        Name = "ReplyChannel",
                        Title =CommonUIStrings.DataGridView_Title_NotificationReply_ReplyChannel,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "ReplyContent",
                        Title =CommonUIStrings.DataGridView_Title_NotificationReply_ReplyContent
                    }, new ColumnItem {
                        Name = "ReplyDate",
                        Title =CommonUIStrings.DataGridView_Title_NotificationReply_ReplyDate
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetNotificationRepliesWithSerialNumber";
        }


        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "NotificationReplyReplyFilePath"
                    }
                };
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
