﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartDeleaveHistoryDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames ={
                "BaseData_Status" 
                                          };

        public PartDeleaveHistoryDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += PartDeleaveHistoryDataGridView_DataContextChanged;
        }

        private void PartDeleaveHistoryDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partDeleaveInformation = e.NewValue as PartDeleaveInformation;
            if(partDeleaveInformation == null || partDeleaveInformation.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartDeleaveInformationId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partDeleaveInformation.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }
        protected override Type EntityType {
            get {
                return typeof(PartDeleaveHistory);
            }
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "OldPartCode",
                         Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartDeleaveInformation_OldPartCode
                    },  new ColumnItem {
                        Name = "DeleavePartCode",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartDeleaveInformation_DeleavePartCode
                    },  new ColumnItem {
                        Name = "DeleavePartName",
                         Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartDeleaveInformation_DeleavePartName
                    },  new ColumnItem {
                        Name = "DeleaveAmount",
                          Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartDeleaveInformation_DeleaveAmount
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    },  new ColumnItem {
                         Name = "Branch.Name",
                         Title = CommonUIStrings.DataGridView_ColumnItem_ResponsibleUnitBranch_BranchName
                    },  new ColumnItem {
                        Name = "PartsSalesCategory.Name",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_partssalescategory_Name
                    }
                };
            }
        }
        protected override string OnRequestQueryName() {
            return "GetPartDeleaveHistoryWithDetail";
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
