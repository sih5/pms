﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsBranchDataGridView : DcsDataGridViewBase {
        protected readonly string[] KvNames = {
            "ABCStrategy_Category", "PartsBranch_ProductLifeCycle","SparePart_LossType","PartsWarrantyTerm_ReturnPolicy","PartsAttribution","PlanPriceCategory_PriceCategory","MasterData_Status"
        };

        public PartsBranchDataGridView() {
            this.KeyValueManager.Register(this.KvNames);

        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new KeyValuesColumnItem {
                        Name="Status",
                        KeyValueItems = this.KeyValueManager[this.KvNames[6]]
                    },new ColumnItem {
                        Name = "PartCode",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code
                    }, new ColumnItem {
                        Name = "PartName"
                    }, new ColumnItem {
                        Name = "SparePart.ReferenceCode",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_ReferenceCode
                    }, new ColumnItem {
                        Name = "SparePart.ExchangeIdentification",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsExchangeGroup_ExchangeCode
                    },new ColumnItem{
                        Name="SparePart.OverseasPartsFigure",
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_OverseasPartsFigure
                    },
                    new ColumnItem{
                      Name = "SparePart.ProductBrand",
                      Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_SparePartProductBrand
                    },new ColumnItem {
                        Name = "SparePart.Feature",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_SparePartFeature
                    },new ColumnItem {
                        Name = "SparePart.EnglishName",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_EnglishName
                    }, new ColumnItem {
                        Name = "IsOrderable"
                    }, new ColumnItem {
                        Name = "IsSalable"
                    }, new ColumnItem {
                        Name = "IsDirectSupply"
                    }, new ColumnItem {
                        Name = "MinSaleQuantity"
                    }, new KeyValuesColumnItem {
                        Name = "ProductLifeCycle",
                        KeyValueItems = this.KeyValueManager[this.KvNames[1]]
                    }, new KeyValuesColumnItem {
                        Name = "LossType",
                        KeyValueItems = this.KeyValueManager[this.KvNames[2]]
                    }, new KeyValuesColumnItem {
                        Name = "PartsAttribution",
                        KeyValueItems = this.KeyValueManager[this.KvNames[4]]
                    }, new ColumnItem {
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_PartsWarrantyCategoryName,
                        Name = "PartsWarrantyCategoryName"
                    }, new KeyValuesColumnItem {
                        Name = "PartsReturnPolicy",
                        KeyValueItems = this.KeyValueManager[this.KvNames[3]]
                    }, new ColumnItem {
                        Name = "PartsSalePriceIncreaseRate.GroupCode",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_IRGroupCode
                    }, new ColumnItem {
                        Name = "PartsSalePriceIncreaseRate.GroupName",
                          Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_IRGroupName
                    }, new KeyValuesColumnItem {
                        Name = "PartABC",
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]]
                    }, new ColumnItem {
                        Name = "StockMaximum"
                    }, new ColumnItem {
                        Name = "StockMinimum"
                    },new ColumnItem {
                        Name = "PartsMaterialManageCost",
                        TextAlignment=TextAlignment.Right
                    },new ColumnItem {
                        Name = "PartsWarrantyLong"
                    },new KeyValuesColumnItem {
                        Name = "PlannedPriceCategory",
                        KeyValueItems = this.KeyValueManager[this.KvNames[5]]
                    }, new ColumnItem {
                        Name = "AutoApproveUpLimit"
                    },new ColumnItem {
                        Name = "SparePart.StandardCode"
                    },new ColumnItem {
                        Name = "SparePart.StandardName"
                    },new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime"
                    }, new ColumnItem {
                        Name = "PartsSalesCategory.Name",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_partssalescategory_Name
                    }, new ColumnItem {
                        Name = "BranchName"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsBranchesRelationWithPartssalescategory";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsBranch","PartsBranchHistory"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsBranch);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["PartsMaterialManageCost"]).DataFormatString = "c2";
        }
    }
}
