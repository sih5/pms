﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsBranchDetailDataGridView : DcsDataGridViewBase {
        protected readonly string[] kvNames = {
            "ABCStrategy_Category","PartsBranch_ProductLifeCycle","SparePart_LossType","PartsWarrantyTerm_ReturnPolicy"
        };
        public PartsBranchDetailDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += this.PartsBranchDetailDataGridView_DataContextChanged;
        }

        private void PartsBranchDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsBranch = e.NewValue as PartsBranch;
            if(partsBranch == null || partsBranch.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsBranchId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsBranch.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new ColumnItem {
                        Name = "PartCode"
                    }, new ColumnItem {
                        Name = "PartName"
                    }, new ColumnItem {
                        Name = "ReferenceCode"
                    }, new ColumnItem {
                        Name = "IsOrderable",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranchHistory_IsOrderable,
                    }, new ColumnItem {
                        Name = "IsSalable",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranchHistory_IsSalable,
                    }, new ColumnItem {
                        Name = "IsDirectSupply",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranchHistory_IsDirectSupply,
                    }, new ColumnItem {
                        Name = "MinSaleQuantity"
                    }, new KeyValuesColumnItem {
                        Name = "ProductLifeCycle",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new KeyValuesColumnItem {
                        Name = "LossType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    }, new ColumnItem {
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_PartsWarrantyCategoryName,
                        Name = "PartsWarrantyCategoryName"
                    }, new KeyValuesColumnItem {
                        Name = "PartsReturnPolicy",
                        KeyValueItems = this.KeyValueManager[this.kvNames[3]]
                    }, new KeyValuesColumnItem {
                        Name = "PartABC",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "StockMaximum"
                    }, new ColumnItem {
                        Name = "StockMinimum"
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "BranchName"
                    }, new ColumnItem {
                        Name = "PartsSalesCategory.Name",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_PartsSalesCategoryName
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsBranchHistoryWithPartssalescategory";
        }

        protected override Type EntityType {
            get {
                return typeof(PartsBranchHistory);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
