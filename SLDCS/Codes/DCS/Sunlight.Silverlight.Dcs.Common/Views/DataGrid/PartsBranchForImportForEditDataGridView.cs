﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsBranchForImportForEditDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                  new ColumnItem{
                        Name="PartCode",
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code
                    }
                    //,new ColumnItem{
                    //    Name="PartName",
                    //    Title=Utils.GetEntityLocalizedName(typeof(PartsBranch),"PartName")
                    //}
                    ,new ColumnItem{
                        Name="IsOrderable",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsBranch),"IsOrderable")
                    },new ColumnItem{
                        Name="IsSalable",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsBranch),"IsSalable")
                    },new ColumnItem{
                        Name="IsDirectSupply",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsBranch),"IsDirectSupply")
                    },new ColumnItem{
                        Name="MinSaleQuantity",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsBranch),"MinSaleQuantity")
                    },new ColumnItem{
                        Name="ProductLifeCycle",
                        Title=CommonUIStrings.DataEditView_Text_PartsBranch_ProductLifeCycle
                    },new ColumnItem{
                        Name="LossType",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsBranch),"LossType")
                    },new ColumnItem{
                        Name="PartsAttribution",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsBranch),"PartsAttribution")
                    },new ColumnItem{
                        Name="PartsWarrantyCategoryName",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranchExtend_PartsWarrantyCategoryName
                    },new ColumnItem {
                        Name = "PartsReturnPolicy",
                        Title = CommonUIStrings.DataEditView_Text_PartsBranch_PartsReturnPolicy
                    },new ColumnItem{
                        Name="StockMaximum",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsBranch),"StockMaximum")
                    },new ColumnItem{
                        Name="StockMinimum",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsBranch),"StockMinimum")
                    },new ColumnItem{
                        Name="PartsMaterialManageCost",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsBranch),"PartsMaterialManageCost")
                    },new ColumnItem{
                        Name="PartsWarrantyLong",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsBranch),"PartsWarrantyLong")
                    },new ColumnItem {
                        Name = "IncreaseRateGroupCode",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_IRGroupCode
                    },new ColumnItem {
                        Name = "IncreaseRateGroupName",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_IRGroupName
                    },new ColumnItem {
                        Name = "AutoApproveUpLimit",
                        Title = CommonUIStrings.DataEditView_Text_PartsBranch_AutoApproveUpLimit
                    },new ColumnItem{
                        Name="Remark",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsBranch),"Remark")
                    },  new ColumnItem{
                        Name="PartsSalesCategoryName",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_PartsSalesCategoryName
                    },new ColumnItem{
                        Name="BranchName",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsBranch),"BranchName")
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsBranchExtend);
            }
        }
    }
}
