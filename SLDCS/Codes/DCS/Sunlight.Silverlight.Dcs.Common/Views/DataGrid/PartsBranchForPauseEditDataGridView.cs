﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsBranchForPauseEditDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = CommonUIStrings.DataGridView_Title_SequeueNumber,
                        Name = "SerialNumber"
                    },new ColumnItem {
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        Name = "PartCode"
                    }, new ColumnItem {
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                        Name = "PartName"
                     },new ColumnItem{
                         Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name,
                         Name="PartsSalesCategoryName"
                     }
                 };
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;

        }
        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsBranchExtends");
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
        protected override bool UsePaging {
            get {
                return false;
            }
        }
        protected override Type EntityType {
            get {
                return typeof(PartsBranchExtend);
            }
        }
    }
}
