﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsBranchHistoryGridView : DcsDataGridViewBase {
        protected readonly string[] kvNames = {
            "ABCStrategy_Category","PartsBranch_ProductLifeCycle","SparePart_LossType","PartsWarrantyTerm_ReturnPolicy","MasterData_Status"
        };
        public PartsBranchHistoryGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += this.PartsBranchHistoryDataGridView_DataContextChanged;
        }

        private void PartsBranchHistoryDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsBranch = e.NewValue as PartsBranch;
            var compositeFilterItem = new CompositeFilterItem();
            if(partsBranch != null && partsBranch.Id != default(int)) {
                compositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "PartsBranchId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                    Value = partsBranch.Id
                });
            }
            var virtualpPartsBranch = e.NewValue as VirtualPartsBranch;
            if(virtualpPartsBranch != null && virtualpPartsBranch.Id != default(int)) {
                compositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "PartsBranchId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                    Value = virtualpPartsBranch.Id
                });
            }
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[4]]
                    },new ColumnItem {
                        Name = "PartCode",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code
                    }, new ColumnItem {
                        Name = "PartName"
                    },new ColumnItem {
                        Name = "ReferenceCode",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_ReferenceCode
                    }, new ColumnItem {
                        Name = "IsOrderable",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranchHistory_IsOrderable,
                    }, new ColumnItem {
                        Name = "IsSalable",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranchHistory_IsSalable,
                    }, new ColumnItem {
                        Name = "IsDirectSupply",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranchHistory_IsDirectSupply,
                    }, new ColumnItem {
                        Name = "MinSaleQuantity"
                    }, new KeyValuesColumnItem {
                        Name = "ProductLifeCycle",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new KeyValuesColumnItem {
                        Name = "LossType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    }, new ColumnItem {
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_PartsWarrantyCategoryName,
                        Name = "PartsWarrantyCategoryName"
                    }, new KeyValuesColumnItem {
                        Name = "PartsReturnPolicy",
                        KeyValueItems = this.KeyValueManager[this.kvNames[3]]
                    },new ColumnItem {
                        Name = "PartsSalePriceIncreaseRate.GroupCode",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_IRGroupCode
                    }, new ColumnItem {
                        Name = "PartsSalePriceIncreaseRate.GroupName",
                          Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_IRGroupName
                    }, new KeyValuesColumnItem {
                        Name = "PartABC",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "StockMaximum"
                    },new ColumnItem {
                        Name = "StockMinimum"
                    },new ColumnItem {
                        Name = "PartsMaterialManageCost",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranchHistory_PartsMaterialManageCost,
                        TextAlignment=TextAlignment.Right
                    },new ColumnItem {
                        Name = "PartsWarrantyLong",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranchHistory_PartsWarrantyLong
                    },new ColumnItem {
                        Name = "AutoApproveUpLimit",
                        Title = CommonUIStrings.DataEditView_Text_PartsBranch_AutoApproveUpLimit
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "BranchName"
                    },  new ColumnItem {
                        Name = "PartsSalesCategory.Name",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_PartsSalesCategoryName
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsBranchHistoryWithPartssalescategory";
        }

        protected override Type EntityType {
            get {
                return typeof(PartsBranchHistory);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["PartsMaterialManageCost"]).DataFormatString = "c2";
        }
    }
}
