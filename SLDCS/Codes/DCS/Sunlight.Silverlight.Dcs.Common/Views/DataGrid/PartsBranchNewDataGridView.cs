﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsBranchNewDataGridView : DcsDataGridViewBase {
        protected readonly string[] KvNames = {
            "ABCStrategy_Category", "PartsBranch_ProductLifeCycle","SparePart_LossType","PartsWarrantyTerm_ReturnPolicy","PartsAttribution","PlanPriceCategory_PriceCategory","MasterData_Status","IsOrNot","PartsBranch_PurchaseRoute","PackingUnitType"
        };

        public PartsBranchNewDataGridView() {
            this.KeyValueManager.Register(this.KvNames);

        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new KeyValuesColumnItem {
                        Name="Status",
                        KeyValueItems = this.KeyValueManager[this.KvNames[6]],
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_Common_Status
                    },new ColumnItem {
                        Name = "PartCode",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code
                    }, new ColumnItem {
                        Name = "PartName",
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                    }                  
                    , new ColumnItem {
                        Name = "ReferenceCode",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                    }                  
                    ,new ColumnItem {
                        Name = "EnglishName",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_EnglishName
                    }, new ColumnItem {
                        Name = "IsOrderable",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_VirtualPartsBranch_IsOrderable
                    }, new ColumnItem {
                        Name = "IsSalable",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_VirtualPartsBranch_IsSalable
                    }, new ColumnItem {
                        Name = "IsDirectSupply",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_VirtualPartsBranch_IsDirectSupply
                    },new KeyValuesColumnItem {
                        Name="PartABC",
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]],
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_PartABC
                    }, new ColumnItem {
                        Name = "IsAccreditPack",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_IsAccreditPack,
                    }, new ColumnItem {
                        Name = "IsAutoCaclSaleProperty",
                        Title = CommonUIStrings.DataDeTailPanel_Text_PartsBranch_IsAutoCaclSaleProperty
                    }
                    //, new ColumnItem {
                    //    Name = "MinSaleQuantity",
                    //    Title = "保底库存"
                    //}                 
                    , new ColumnItem {
                        Name = "GroupName",
                          Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_IRGroupName
                    },new KeyValuesColumnItem {
                        Name = "MainPackingType",
                        KeyValueItems = this.KeyValueManager[this.KvNames[9]],
                        Title = CommonUIStrings.DataGridView_Title_NotificationReply_VirtualPartsBranch
                    }
                    ,new ColumnItem {
                        Name = "PackingCoefficient1",
                        Title =CommonUIStrings.DataGridView_Title_VirtualPartsBranch_PackingCoefficient1
                    },new ColumnItem {
                        Name = "FirPrin",
                        Title = CommonUIStrings.DataGridView_Title_VirtualPartsBranch_FirPrin
                    },new ColumnItem {
                        Name = "FirPackingMaterial",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_FirPackingMaterial
                    },new ColumnItem {
                        Name = "PackingCoefficient2",
                        Title = CommonUIStrings.DataGridView_Title_VirtualPartsBranch_PackingCoefficient2
                    },new ColumnItem {
                        Name = "SecPrin",
                        Title = CommonUIStrings.DataGridView_Title_VirtualPartsBranch_SecPrin
                    },new ColumnItem {
                        Name = "SecPackingMaterial",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_SecPackingMaterial
                    },new ColumnItem {
                        Name = "PackingCoefficient3",
                        Title = CommonUIStrings.DataGridView_Title_VirtualPartsBranch_PackingCoefficient3
                    },new ColumnItem {
                        Name = "ThidPrin",
                        Title = CommonUIStrings.DataGridView_Title_VirtualPartsBranch_ThidPrin
                    },new ColumnItem {
                        Name = "ThidPackingMaterial",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_ThidPackingMaterial
                    }, new ColumnItem {
                        Name = "StockMaximum",
                        Title = CommonUIStrings.DataEditView_Text_PartsBranch_StockMaximum
                    }, new ColumnItem {
                        Name = "StockMinimum",
                        Title = CommonUIStrings.DataEditView_Text_PartsBranch_StockMinimum
                    }                   
                    ,new ColumnItem {
                        Name = "PartsWarrantyLong",
                        Title = CommonUIStrings.DataEditView_Text_PartsBranch_PartsWarrantyLong
                    }, new ColumnItem {
                        Name = "AutoApproveUpLimit",
                        Title = CommonUIStrings.DataEditView_Text_PartsBranch_AutoApproveUpLimit
                    }, new ColumnItem {
                        Name = "BreakTime",
                         Title=CommonUIStrings.DataDeTailPanel_Text_PartsBranch_BreakTime,
                    }, new KeyValuesColumnItem {
                        Name = "PurchaseRoute",
                        KeyValueItems = this.KeyValueManager[this.KvNames[8]],
                        Title = CommonUIStrings.DataDeTailPanel_Text_PartsBranch_PurchaseRoute
                    },new ColumnItem {
                        Name = "Remark",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartsInboundCheckBillRemark
                    }
                    , new ColumnItem {
                        Name = "CreatorName",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_CreatorName
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title = CommonUIStrings.QueryPanel_Title_ProductStandard_CreateTime,
                    }, new ColumnItem {
                        Name = "ModifierName",
                        Title = CommonUIStrings.DataDeTailPanel_Text_BottomStock_ModifierName
                    }, new ColumnItem {
                        Name = "ModifyTime",
                         Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_ModifyTime
                    }, new ColumnItem {
                        Name = "AbandonerName",
                        Title = CommonUIStrings.DataDeTailPanel_Text_BottomStock_AbandonerName
                    }, new ColumnItem {
                        Name = "AbandonTime",
                        Title = CommonUIStrings.DataDeTailPanel_Text_BottomStock_AbandonerTime
                    }, new ColumnItem {
                        Name = "PartsSalesCategoryName",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_partssalescategory_Name
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVirtualPartsBranchesRelationWithPartssalescategory";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsBranch","PartsBranchHistory"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPartsBranch);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            //((GridViewDataColumn)this.GridView.Columns["PartsMaterialManageCost"]).DataFormatString = "c2";
        }
    }
}
