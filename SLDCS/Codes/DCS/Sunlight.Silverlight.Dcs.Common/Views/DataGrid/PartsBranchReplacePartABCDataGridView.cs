﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid
{
    public class PartsBranchReplacePartABCDataGridView : DcsDataGridViewBase
    {
        protected readonly string[] kvNames = {
            "ABCStrategy_Category"
        };
        public PartsBranchReplacePartABCDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] { 
                     new ColumnItem{
                        Name="SerialNumber",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_BranchSupplierRelation_SequeueNumber
                    },new ColumnItem{
                        Name="PartCode",
                       Title =CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_SparePartCode
                    },new ColumnItem{
                        Name="PartsSalesCategoryName",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_PartsSalesCategoryName
                    },new KeyValuesColumnItem {
                        Name = "PartABC",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBillCentral_PartABC,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }
                };
            }
        }

        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }

        protected override bool UsePaging
        {
            get
            {
                return false;
            }
        }

        protected override Type EntityType
        {
            get
            {
                return typeof(PartsBranchExtend);
            }
        }

        protected override Binding OnRequestDataSourceBinding()
        {
            return new Binding("PartsBranchExtends");
        }
    }
}
