﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsExchangeDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        public PartsExchangeDataGridView() {
            this.KeyValueManager.Register(this.kvNames);

        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPartsExchange);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_Common_Status
                    }, new ColumnItem {
                        Name = "ExGroupCode",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsExchangeGroup_ExGroupCode
                    },new ColumnItem {
                        Name = "ExchangeCode",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsExchangeGroup_ExchangeCode
                    }, new ColumnItem {
                        Name = "PartCode",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code
                    }, new ColumnItem {
                        Name = "PartName",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_SparePart_Name
                    },  new ColumnItem {
                        Name = "Remark",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartsInboundCheckBillRemark
                    }, new ColumnItem {
                        Name = "CreatorName",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_CreatorName
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title = CommonUIStrings.QueryPanel_Title_ProductStandard_CreateTime,
                    }, new ColumnItem {
                        Name = "ModifierName",
                        Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_ModifierName
                    }, new ColumnItem {
                        Name = "ModifyTime",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_ModifyTime
                    }, new ColumnItem {
                        Name = "AbandonerName",
                        Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_AbandonerName
                    }, new ColumnItem {
                        Name = "AbandonTime",
                        Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_AbandonerTime
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "查询配件互换信息带互换组";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filterItem = this.FilterItem as CompositeFilterItem;
            if(filterItem == null)
                return null;
            var composites = filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
            switch(parameterName) {
                case "partCode":
                    var partCodeFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "PartCode");
                    return partCodeFilterItem == null ? null : partCodeFilterItem.Value;
                case "partName":
                    var partNameFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "PartName");
                    return partNameFilterItem == null ? null : partNameFilterItem.Value;
                case "exchangeCode":
                    var exchangeCodeFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "ExchangeCode");
                    return exchangeCodeFilterItem == null ? null : exchangeCodeFilterItem.Value;
                case "exGroupCode":
                    var exGroupCodeFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "ExGroupCode");
                    return exGroupCodeFilterItem == null ? null : exGroupCodeFilterItem.Value;
                case "status":
                    var statusFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "Status");
                    return statusFilterItem == null ? null : statusFilterItem.Value;
                case "beginCreaTime":
                    var beginCreateTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                    return beginCreateTime == null ? null : beginCreateTime.Filters.First(r => r.MemberName == "CreateTime").Value;
                case "endCreaTime":
                    var endCreateTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                    return endCreateTime == null ? null : endCreateTime.Filters.Last(r => r.MemberName == "CreateTime").Value;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            return null;
        }
        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsExchangeHistory"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionMode = SelectionMode.Single;
        }

        public void ExportSelectionLocalData(string fileName, bool showHeaders = true, bool showFooters = true, bool showGroupFooters = true, bool exportAllPage = false) {
            if(this.SelectedEntities == null && !this.SelectedEntities.Any()) {
                return;
            }
            var options = new GridViewExportOptions {
                ShowColumnHeaders = showHeaders,
                ShowColumnFooters = showFooters,
                ShowGroupFooters = showGroupFooters,
                Items = this.SelectedEntities,
            };
            if(exportAllPage) {
                var tempPageSize = this.DataPager.PageSize;
                var tempPageIndex = this.DataPager.PageIndex;
                this.DataPager.PageSize = int.MaxValue;
                try {
                    this.ExportData(options, fileName);
                } finally {
                    this.DataPager.PageSize = tempPageSize;
                    this.DataPager.PageIndex = tempPageIndex;
                }
            } else
                this.ExportData(options, fileName);
        }
        private void ExportData(GridViewExportOptions options, string fileName = "") {
            var dialog = new SaveFileDialog {
                DefaultFileName = string.IsNullOrEmpty(fileName) ? "" : string.Format(fileName.Replace('/', '_') + "_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")),
                DefaultExt = "xls",
                Filter = string.Format("{0} (*.{1})|*.{1}", DcsUIStrings.ExcelDocument, "xls"),
            };
            var result = dialog.ShowDialog();
            if(result.HasValue && result.Value)
                using(var stream = dialog.OpenFile()) {
                    this.GridView.Export(stream, options);
                }
        }
    }
}
