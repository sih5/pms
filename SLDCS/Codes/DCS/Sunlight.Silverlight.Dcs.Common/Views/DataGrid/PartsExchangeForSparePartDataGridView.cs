﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsExchangeForSparePartDataGridView : DcsDataGridViewBase {

        protected readonly string[] KvNames = {
           "PartsWarrantyTerm_ReturnPolicy"
        };

        public PartsExchangeForSparePartDataGridView() {
            this.KeyValueManager.Register(this.KvNames);
            this.DataContextChanged += this.PartsExchangeForSparePartDataGridView_DataContextChanged;
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "ExchangeCode"
                    },new ColumnItem{
                        Name="Code"}
                    , new ColumnItem{
                        Name="Name"}                  
                    , new ColumnItem {
                        Name = "IsOrderable"
                    }, new ColumnItem {
                        Name = "IsSalable"
                    }, new ColumnItem {
                        Name = "IsDirectSupply"
                    }, new KeyValuesColumnItem {
                        Name = "PartsReturnPolicy",
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]]
                    }, new ColumnItem {
                        Name = "Feature"
                    } ,new ColumnItem {
                        Name = "PartsSalesCategoryName"}
                };
            }
        }

        private void PartsExchangeForSparePartDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            //var sparePart = e.NewValue as SparePart;
            //if(sparePart == null || sparePart.Id == default(int))
            //    return;

            //if(this.FilterItem == null)
            //    this.FilterItem = new FilterItem {
            //        MemberName = "PartId",
            //        MemberType = typeof(int),
            //        Operator = FilterOperator.IsEqualTo,
            //    };
            //this.FilterItem.Value = sparePart.Id;
            this.ExecuteQueryDelayed();
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            switch (parameterName)
            {
                case "partCode":
                    var sparePart = this.DataContext as SparePart;
                    return sparePart.Code;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override string OnRequestQueryName() {
            return "GetPartsExchangeWithOtherInfoes";
        }

        protected override Type EntityType {
            get {
                return typeof(PartsExchangeWithOtherInfo);
            }
        }
    }
}
