﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsExchangeForSpareParttDataGridView : DcsDataGridViewBase {

        protected readonly string[] KvNames = {
            "ABCStrategy_Category", "PartsBranch_ProductLifeCycle","SparePart_LossType","PartsWarrantyTerm_ReturnPolicy","PartsAttribution"
        };

        public PartsExchangeForSpareParttDataGridView() {
            this.KeyValueManager.Register(this.KvNames);
            this.DataContextChanged += this.PartsBranchDataGridView_DataContextChanged;
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "BranchName"
                    },new ColumnItem{
                        Name="PartsSalesCategory.Name",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_PartsSalesCategoryName
                    }, new KeyValuesColumnItem {
                        Name = "PartABC",
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]]
                    }, new ColumnItem {
                        Name = "WarrantySupplyStatus",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_WarrantySupplyStatus
                    }, new ColumnItem {
                        Name = "IsOrderable"
                    }, new ColumnItem {
                        Name = "PurchaseCycle"
                    }, new ColumnItem {
                        Name = "IsService"
                    }, new ColumnItem {
                        Name = "IsSalable"
                    }, new ColumnItem {
                        Name = "IsDirectSupply"
                    }, new ColumnItem {
                        Name = "MinSaleQuantity"
                    }, new KeyValuesColumnItem {
                        Name = "ProductLifeCycle",
                        KeyValueItems = this.KeyValueManager[this.KvNames[3]]
                    }
                };
            }
        }

        private void PartsBranchDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var sparePart = e.NewValue as SparePart;
            if(sparePart == null || sparePart.Id == default(int))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "PartId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                };
            this.FilterItem.Value = sparePart.Id;
            this.ExecuteQueryDelayed();
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override string OnRequestQueryName() {
            return "GetPartsBranchesRelationWithPartssalescategory";
        }

        protected override Type EntityType {
            get {
                return typeof(PartsBranch);
            }
        }
    }
}
