﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsExchangeGroupDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "BaseData_Status"
        };

        public PartsExchangeGroupDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "ExGroupCode",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsExchangeGroup_ExGroupCode
                    },new ColumnItem {
                        Name = "ExchangeCode",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsExchangeGroup_ExchangeCode
                    },new ColumnItem {
                        Name = "Remark"
                    },new ColumnItem {
                        Name = "CreatorName"
                    },new ColumnItem {
                        Name = "CreateTime"
                    },new ColumnItem {
                        Name = "ModifierName"
                    },new ColumnItem {
                        Name = "ModifyTime"
                    },new ColumnItem {
                        Name = "AbandonerName"
                    },new ColumnItem {
                        Name = "AbandonTime"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsExchangeGroups";
        }

        protected override Type EntityType {
            get {
                return typeof(PartsExchangeGroup);
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsExchangeGroup"
                    }
                };
            }
        }
    }
}
