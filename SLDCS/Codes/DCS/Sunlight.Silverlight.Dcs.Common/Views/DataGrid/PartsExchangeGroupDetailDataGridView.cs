﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsExchangeGroupDetailDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code
                    },new ColumnItem {
                        Name = "SparePartName",
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                    },new ColumnItem {
                        Name = "ReferenceCode",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_ReferenceCode
                    },new ColumnItem {
                        Name = "ExchangeCode",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsExchangeGroup_ExchangeCode
                    },new ColumnItem {
                        Name = "CreatorName",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_CreatorName
                    },new ColumnItem {
                        Name = "CreateTime",
                        Title = CommonUIStrings.QueryPanel_Title_ProductStandard_CreateTime,
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVirtualPartsExchangeGroups";
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPartsExchangeGroup);
            }
        }
        public PartsExchangeGroupDetailDataGridView() {
            this.DataContextChanged += PartsExchangeGroupDetailDataGridView_DataContextChanged;
        }

        private void PartsExchangeGroupDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsExchangeGroup = e.NewValue as PartsExchangeGroup;
            if(partsExchangeGroup == null)
                return;
            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "PartsExchangeGroupId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                };
            this.FilterItem.Value = partsExchangeGroup.Id;
            this.ExecuteQueryDelayed();
        }
    }
}
