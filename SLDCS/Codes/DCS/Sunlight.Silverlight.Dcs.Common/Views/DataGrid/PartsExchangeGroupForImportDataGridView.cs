﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsExchangeGroupForImportDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem{
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsExchangeGroup_ExGroupCode,
                        Name="ExGroupCode"
                    }, new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsExchangeGroup_ExchangeCode,
                        Name = "ExchangeCode"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsExchangeGroup);
            }
        }
    }
}