﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsExchangeGroupForPanelDataGridView : DcsDataGridViewBase {
        private RadWindow radQueryWindow;
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new DropDownTextBoxColumnItem {
                        Name = "ExchangeCode",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsExchangeGroup_ExchangeCode,
                        DropDownContent = this.PartsExchangeGroupDropDownQueryWindow
                    },new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        private DcsMultiPopupsQueryWindowBase partsExchangeGroupSelectQueryWindow;
        private DcsMultiPopupsQueryWindowBase PartsExchangeGroupSelectQueryWindow {
            get {
                this.partsExchangeGroupSelectQueryWindow = DI.GetQueryWindow("PartsExchangeGroupSelect") as DcsMultiPopupsQueryWindowBase;
                this.partsExchangeGroupSelectQueryWindow.SelectionDecided += partsExchangeGroupSelectQueryWindow_SelectionDecided;
                this.partsExchangeGroupSelectQueryWindow.Loaded += partsExchangeGroupSelectQueryWindow_Loaded;
                return this.partsExchangeGroupSelectQueryWindow;
            }
        }

        void partsExchangeGroupSelectQueryWindow_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var dealerPerTrainAut = this.DataContext as DealerPerTrainAut;
            if(dealerPerTrainAut == null)
                return;
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null)
                return;
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "Status", (int)DcsMasterDataStatus.有效
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "Status", false
            });
        }
        private QueryWindowBase partsExchangeGroupDropDownQueryWindow;
        private QueryWindowBase PartsExchangeGroupDropDownQueryWindow {
            get {
                this.partsExchangeGroupDropDownQueryWindow = DI.GetQueryWindow("PartsExchangeGroupSelect") as DcsMultiPopupsQueryWindowBase;
                this.partsExchangeGroupDropDownQueryWindow.SelectionDecided += partsExchangeGroupSelectQueryWindow_SelectionDecided;
                this.partsExchangeGroupDropDownQueryWindow.Loaded += partsExchangeGroupSelectQueryWindow_Loaded;
                return this.partsExchangeGroupDropDownQueryWindow;
            }
        }
        private void partsExchangeGroupSelectQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            var dataEditView = this.DataContext as PartsExchangeGroupDataEditView;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var partsExchangeGroups = queryWindow.SelectedEntities.Cast<VirtualPartsExchangeGroup>();
            if(partsExchangeGroups == null || dataEditView == null)
                return;
            if(dataEditView.PartsExchangeGroupDetails.GroupBy(r => r.ExchangeCode).Any(r => r.Count() > 1)) {
                UIHelper.ShowNotification("存在相同互换识别号,请检查");
                return;
            }
            foreach(var partsExchangeGroup in partsExchangeGroups) {
                //partsExchangeGroup.ExGroupCode = dataEditView.textBoxExchangeCode.Text;
                dataEditView.PartsExchangeGroupDetails.Add(new PartsExchangeGroup {
                    ExGroupCode = dataEditView.textBoxExchangeCode.Text,
                    ExchangeCode = partsExchangeGroup.ExchangeCode,
                });
            }
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }
        private RadWindow RadQueryWindow {
            get {
                this.radQueryWindow = new RadWindow {
                    Content = this.PartsExchangeGroupSelectQueryWindow,
                    Header = CommonUIStrings.QueryPanel_Title_SparePart,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen
                };
                return this.radQueryWindow;
            }
        }
        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            RadQueryWindow.ShowDialog();
        }
        private void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            var domainContext = this.DomainContext as DcsDomainContext;
            var dataEditView = this.DataContext as PartsExchangeGroupDataEditView;
            if(domainContext == null || dataEditView == null)
                return;
            if(dataEditView.PartsExchangeGroupDetails.Contains(e.Items.Cast<PartsExchangeGroup>().First())) {
                dataEditView.PartsExchangeGroupDetails.Remove(e.Items.Cast<PartsExchangeGroup>().First());
            }
        }
        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsExchangeGroupDetails");
        }

        protected override Type EntityType {
            get {
                return typeof(PartsExchangeGroup);
            }
        }
        protected override void OnControlsCreated() {
            this.GridView.AddingNewDataItem -= GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
            this.GridView.Deleting -= GridView_Deleting;
            this.GridView.Deleting += GridView_Deleting;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
        }
    }
}
