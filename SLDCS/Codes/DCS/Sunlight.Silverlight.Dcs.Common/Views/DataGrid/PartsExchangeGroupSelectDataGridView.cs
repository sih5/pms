﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsExchangeGroupSelectDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "BaseData_Status"
        };

        public PartsExchangeGroupSelectDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new KeyValuesColumnItem {
                        Name = "Status",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_Common_Status,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "ExchangeCode",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsExchangeGroup_ExchangeCode
                    }
                    ,new ColumnItem {
                        Name = "CreateTime",
                        Title = CommonUIStrings.QueryPanel_Title_ProductStandard_CreateTime,
                    }
                    
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVirtualPartsExchangeGroupAndPartsExchanges";
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPartsExchangeGroup);
            }
        }

        //protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
        //    get {
        //        return new TabControlRowDetailsTemplateProvider {
        //            DetailPanelNames = new[] {
        //                "PartsExchangeGroup"
        //            }
        //        };
        //    }
        //}
    }
}
