﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.Common.Resources;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsExchangeHistoryDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames ={
                "BaseData_Status" 
                                          };

        public PartsExchangeHistoryDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += PartsExchangeHistoryDataGridView_DataContextChanged;
        }

        private void PartsExchangeHistoryDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsExchange = e.NewValue as VirtualPartsExchange;
            if(partsExchange == null || partsExchange.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsExchangeId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsExchange.PartsExchangeId
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override Type EntityType {
            get {
                return typeof(PartsExchangeHistory);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "ExchangeCode",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsExchangeGroup_ExchangeCode
                    }, new ColumnItem {
                        Name = "SparePart.Code",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code
                    }, new ColumnItem {
                        Name = "SparePart.Name"
                    },  new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsExchangeHistoryWithDetails";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
        }
    }
}
