﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsExchangeSparePartMultiSelectDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "MasterData_Status", "SparePart_PartType","SparePart_MeasureUnit"
        };

        public PartsExchangeSparePartMultiSelectDataGridView() {
            this.KeyValueManager.Register(kvNames);
            this.KeyValueManager.LoadData(() => {
                foreach(var item in this.KeyValueManager[kvNames[2]])
                    KvMeasureUnit.Add(item);
            });
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPartsBranch);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title =  Utils.GetEntityLocalizedName(typeof(SparePart), "Status")
                    }, new ColumnItem {
                        Name = "PartCode",
                        IsSortDescending = false,
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code
                    }, new ColumnItem {
                        Name = "PartName",
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                    },new ColumnItem {
                        Title = CommonUIStrings.DataGridView_Title_VirtualPartsBranch_SalesPrice,
                        Name = "SalesPrice"
                    },new ColumnItem {
                        Title = CommonUIStrings.DataGridView_Title_VirtualPartsBranch_RetailGuidePrice,
                        Name = "RetailGuidePrice"
                    },new ColumnItem {
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_ReferenceCode,
                        Name = "ReferenceCode"
                    },new ColumnItem {
                        Name = "CADCode",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_SparePart_CADCode
                    },  new KeyValuesColumnItem {
                        Name = "PartType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title =  Utils.GetEntityLocalizedName(typeof(SparePart), "PartType")
                    }, new ColumnItem {
                        Name = "MeasureUnit",
                        Title =  Utils.GetEntityLocalizedName(typeof(SparePart), "MeasureUnit")
                    }, new ColumnItem {
                        Name = "Specification",
                        Title =  Utils.GetEntityLocalizedName(typeof(SparePart), "Specification")
                    }, new ColumnItem {
                        Name = "Feature",
                        Title =  Utils.GetEntityLocalizedName(typeof(SparePart), "Feature")
                    },new ColumnItem {
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name,
                        Name = "PartsSalesCategoryName"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "查询互换件配件信息";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Multiple;
        }

        private ObservableCollection<KeyValuePair> kvMeasureUnit;
        private ObservableCollection<KeyValuePair> KvMeasureUnit {
            get {
                return this.kvMeasureUnit ?? (this.kvMeasureUnit = new ObservableCollection<KeyValuePair>());
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            if(FilterItem == null)
                return base.OnRequestFilterDescriptor(queryName);
            var newFilterItem = new CompositeFilterItem();
            var compositeFilterItem = FilterItem as CompositeFilterItem;
            if(compositeFilterItem == null) {
                compositeFilterItem = new CompositeFilterItem();
                compositeFilterItem.Filters.Add(this.FilterItem);
            }
            foreach(var item in compositeFilterItem.Filters) {
                if(item.MemberName == "MeasureUnit") {
                    var measureUnitValue = item.Value as int?;
                    if(measureUnitValue.HasValue && measureUnitValue != -1) {
                        var firstOrDefault = this.KvMeasureUnit.FirstOrDefault(r => r.Key == measureUnitValue.Value);
                        if(firstOrDefault != null) {
                            var measureUnitName = firstOrDefault.Value;
                            newFilterItem.Filters.Add(new FilterItem("MeasureUnit", typeof(string), Core.Model.FilterOperator.IsEqualTo, measureUnitName));
                        }
                    }
                    continue;
                }
                newFilterItem.Filters.Add(item);
            }
            return newFilterItem.ToFilterDescriptor();
        }
    }
}
