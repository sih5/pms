﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsPriceHistoryQueryDataGridView : DcsDataGridViewBase {
        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = true;
            base.OnControlsCreated();
        }
        private readonly string[] kvNames = {
            "PartsAttribution"
        };
        public PartsPriceHistoryQueryDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {                  
                   
                    new ColumnItem {
                        Name = "PartCode",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code
                    },
                    new ColumnItem {
                        Name = "PartName",
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                    },
                    new ColumnItem {
                        Name = "Measureunit",
                        Title = CommonUIStrings.QueryPanel_Title_FaultyParts_MeasureUnit
                    },
                    new ColumnItem {
                        Name = "ExchangeCode",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsExchangeGroup_ExGroupCode
                    },
                    new ColumnItem {
                        Name = "IsOrderable",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_VirtualPartsBranch_IsOrderable
                    },
                    new ColumnItem {
                        Name = "IsSalable",                                                                                                          
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_VirtualPartsBranch_IsSalable
                    },
                    new ColumnItem {
                        Name = "PurchasePrice",
                        Title = CommonUIStrings.DataGridView_Title_VirtualFactoryPurchacePrice_ContractPrice
                    },
                    new ColumnItem {
                        Name = "SalesPrice",
                        Title = CommonUIStrings.DataGridView_Title_VirtualPartsPriceHistory_SalesPrice
                    },
                      new ColumnItem {
                        Name = "RetailGuidePrice",
                        Title = CommonUIStrings.DataGridView_Title_VirtualPartsBranch_RetailGuidePrice
                    },
                      new ColumnItem {
                        Name = "Dealersalesprice",
                        Title =CommonUIStrings.DataGridView_Title_VirtualPartsPriceHistory_Dealersalesprice
                    },
                      new ColumnItem {
                        Name = "Createtime",
                        Title =CommonUIStrings.DataGridView_Title_VirtualPartsPriceHistory_Createtime
                    },
                      new ColumnItem {
                        Name = "SupplierCode",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierCode,
                    },
                      new ColumnItem {
                        Name = "SupplierName",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierName,
                    },
                      new ColumnItem {
                        Name = "PlannedPrice",
                        Title = CommonUIStrings.DataGridView_Title_VirtualPartsPriceHistory_PlannedPrice                           
                    }, new KeyValuesColumnItem {
                        Name = "PartsAttribution",
                        Title = CommonUIStrings.DataEditView_Text_PartsBranch_PartABC,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "BranchCode",
                        Title = CommonUIStrings.QueryPanel_Title_DealerService_Marketing
                    },
                    new ColumnItem {
                        Name = "PartsSalesCategoryName",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name
                    }

                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPartsPriceHistory);
            }
        }
        protected override string OnRequestQueryName() {
            return "配件价格查询";
        }

    }
}
