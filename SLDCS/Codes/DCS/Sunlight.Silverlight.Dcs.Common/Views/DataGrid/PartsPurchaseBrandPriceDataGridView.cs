﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsPurchaseBrandPriceDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = { 
            "PartsPurchasePricingChange_Status","PurchasePriceType"
        };

        public PartsPurchaseBrandPriceDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new KeyValuesColumnItem{
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem{
                        Name = "PartCode",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_SparePartCode
                    }, new ColumnItem{
                        Name = "PartName",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_SparePartName
                    }, new ColumnItem{
                        Name = "PartsSupplierCode",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_PartsSupplierCode
                    }, new ColumnItem{
                        Name = "PartsSupplierName",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_PartsSupplierName
                    }, new KeyValuesColumnItem{
                        Name = "PriceType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_PriceType
                    }, new ColumnItem{
                        Name = "PurchasePrice",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_PurchasePrice
                    }, new ColumnItem{
                        Name = "ValidFrom",
                        Title = CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_ValidTo
                    }, new ColumnItem{
                        Name = "ValidTo",
                        Title = CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_ValidFrom
                    },  new ColumnItem{
                        Name = "Remark"
                    }, new ColumnItem{
                        Name = "CreatorName"
                    }, new ColumnItem{
                        Name = "CreateTime"
                    }, new ColumnItem{
                        Name = "ModifierName"
                    }, new ColumnItem{
                        Name = "ModifyTime"
                    }, new ColumnItem{
                        Name = "AbandonerName"
                    }, new ColumnItem{
                        Name = "AbandonTime"
                    }, new ColumnItem{
                        Name = "BranchName",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_BranchName
                    }, new ColumnItem{
                        Name = "PartsSalesCategoryName",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_PartsSalesCategoryName
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(GetAllPartsPurchasePricing);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询配件采购价格品牌";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filterItem = this.FilterItem as CompositeFilterItem;
            if(filterItem == null)
                return null;
            switch(parameterName) {
                case "availibleTime":
                    var timeFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "AvailibleTime");
                    if(timeFilterItem == null)
                        return null;
                    return Convert.ToDateTime(timeFilterItem.Value).Date;
                case "partsSalesCategoryName":
                    var partsSalesCategoryFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "PartsSalesCategoryName");
                    return partsSalesCategoryFilterItem == null ? null : partsSalesCategoryFilterItem.Value;
                default:
                    return null;
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                var param = new[] { "PartsSalesCategoryName", "AvailibleTime" };
                foreach(var item in compositeFilterItem.Filters.Where(e => !param.Contains(e.MemberName))) {
                    newCompositeFilterItem.Filters.Add(item);
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }
}
