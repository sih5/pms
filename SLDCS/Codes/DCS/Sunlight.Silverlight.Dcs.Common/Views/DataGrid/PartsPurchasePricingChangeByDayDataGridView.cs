﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsPurchasePricingChangeByDayDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsPurchasePricingChange_Status"
        };

        public PartsPurchasePricingChangeByDayDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurchasePricingDetail);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "PartsPurchasePricingChange.Code"
                    }, new ColumnItem {
                        Name = "PartCode",
                        IsDefaultGroup = true
                    }, new ColumnItem {
                        Name = "PartName"
                    }, new ColumnItem {
                        Name = "SupplierCode"
                    }, new ColumnItem {
                        Name = "SupplierName"
                    }, new ColumnItem {
                        Name = "Price"
                    }, new ColumnItem {
                        Name = "ValidFrom"
                    }, new ColumnItem {
                        Name = "ValidTo"
                    }, new KeyValuesColumnItem {
                        Name = "PartsPurchasePricingChange.Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "PartsPurchasePricingChange.CreatorName"
                    }, new ColumnItem {
                        Name = "PartsPurchasePricingChange.CreateTime"
                    }, new ColumnItem {
                        Name = "PartsPurchasePricingChange.ModifierName"
                    }, new ColumnItem {
                        Name = "PartsPurchasePricingChange.ModifyTime"
                    }, new ColumnItem {
                        Name = "PartsPurchasePricingChange.AbandonerName"
                    }, new ColumnItem {
                        Name = "PartsPurchasePricingChange.AbandonTime"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "查询配件采购价格按天";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                var validFrom = compositeFilterItem.Filters.FirstOrDefault(filter => filter.MemberName == "ValidFrom");
                if(validFrom != null) {
                    switch(parameterName) {
                        case "time":
                            return validFrom.Value;
                    }
                }
            }
            return null;
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var composite = this.FilterItem as CompositeFilterItem;
            var newComposite = new CompositeFilterItem();
            if(composite != null)
                foreach(var filterItem in composite.Filters.Where(filter => filter.MemberName != "ValidFrom"))
                    newComposite.Filters.Add(filterItem);
            return newComposite.ToFilterDescriptor();
        }
    }
}
