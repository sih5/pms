﻿using System;
using System.Linq;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsPurchasePricingChangeDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsPurchasePricingChange_Status"
        };

        public PartsPurchasePricingChangeDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurchasePricingChange);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        IsSortable = false,
                        Name = "IsUplodFile",
                        Title = CommonUIStrings.DataGridView_Title_PartsPurchasePricingChange_IsUplodFile
                    },new ColumnItem {
                        Name = "Code"
                    },  new ColumnItem {
                        Name = "ApprovalComment",
                        Title =CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_ApprovalComment
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    },
                    new ColumnItem {
                        Name = "CheckerName",
                        Title = "审核人"
                    }, new ColumnItem {
                        Name = "CheckTime",
                        Title = "审核时间"
                    }, new ColumnItem {
                        Name = "CheckComment",
                        Title = "审核意见"
                    }
                    , new ColumnItem {
                        Name = "ApproverName"
                    }, new ColumnItem {
                        Name = "ApproveTime"
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime"
                    }, new ColumnItem {
                        Name = "Branch.Name"
                    }, new ColumnItem {
                        Name = "PartsSalesCategory.Name",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_PartsSalesCategoryName
                    },
                    new ColumnItem {
                        Name = "InitialApproverName",
                        Title = "初审人"
                    }, new ColumnItem {
                        Name = "InitialApproveTime",
                        Title = "初审时间"
                    },
                    new ColumnItem {
                        Name = "InitialApproveComment",
                        Title = "初审意见"
                    }
                };
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ModifyTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ApproveTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["AbandonTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["InitialApproveTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["CheckTime"]).DataFormatString = "d";
        }
        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsPurchasePricingDetail"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsPurchasePricingChangesWithBranch";
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName)
        {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if (compositeFilterItem == null)
            {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            }
            else
            {
                foreach (var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "SparePartCode"  && filter.MemberName != "SparePartName"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                switch (parameterName)
                {
                    case "sparePartCode":
                        return filters.Filters.Single(item => item.MemberName == "SparePartCode").Value;
                    case "sparePartName":
                        return filters.Filters.Single(item => item.MemberName == "SparePartName").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
    }
}
