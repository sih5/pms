﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsPurchasePricingChangeForExprotDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new DropDownTextBoxColumnItem {
                        Name = "PartCode"
                    }, new ColumnItem {
                        Name = "PartName"
                    }, new DropDownTextBoxColumnItem {
                        Name = "SupplierCode"
                    }, new ColumnItem {
                        Name = "SupplierName"
                    //}, new KeyValuesColumnItem{
                    //    Name = "PriceType",
                    //    KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Price"
                    }, new ColumnItem {
                        Name = "ValidFrom"
                    }, new ColumnItem {
                        Name = "ValidTo"
                    //}, new ColumnItem {
                    //    Name = "ReferencePrice"
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurchasePricingDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }
    }
}
