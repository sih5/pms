﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsPurchasePricingChangeHistoryDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsPurchasePricingChange_Status", "PurchasePriceType"
        };

        public PartsPurchasePricingChangeHistoryDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurchasePricingDetail);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "PartsPurchasePricingChange.Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "PartsPurchasePricingChange.Code",
                        IsDefaultGroup = true
                    }, new ColumnItem {
                        Name = "PartCode"
                    }, new ColumnItem {
                        Name = "PartName"
                    }, new ColumnItem {
                        Name = "SparePart.ReferenceCode",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                    }, new ColumnItem {
                        Name = "SupplierCode"
                    }, new ColumnItem {
                        Name = "SupplierName"
                    }, new ColumnItem {
                        Name = "SupplierPartCode",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsSupplierRelation_SupplierPartCode,
                    }, new ColumnItem {
                        Name = "IsPrimary",
                        Title = CommonUIStrings.DataGridView_PurchasePriceWarningOfExpiry_IsPrimary,
                    }, new ColumnItem {
                        Name = "IfPurchasable",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_VirtualPartsBranch_IsOrderable,
                    }, new KeyValuesColumnItem{
                        Name = "PriceType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Name = "Price"
                    }, new ColumnItem {
                        Name = "ValidFrom"
                    }, new ColumnItem {
                        Name = "ValidTo"
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "PartsPurchasePricingChange.CreatorName"
                    }, new ColumnItem {
                        Name = "PartsPurchasePricingChange.CreateTime"
                    }, new ColumnItem {
                        Name = "PartsPurchasePricingChange.ModifierName"
                    }, new ColumnItem {
                        Name = "PartsPurchasePricingChange.ModifyTime"
                    }, new ColumnItem {
                        Name = "PartsPurchasePricingChange.ApproverName"
                    }, new ColumnItem {
                        Name = "PartsPurchasePricingChange.ApproveTime"
                    }, new ColumnItem {
                        Name = "PartsPurchasePricingChange.PartsSalesCategoryName",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_PartsSalesCategoryName
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "查询配件采购价格变更履历1";
        }

        //protected override object OnRequestQueryParameter(string queryName, string parameterName) {
        //    var compositeFilterItem = this.FilterItem as CompositeFilterItem;
        //    if(compositeFilterItem != null) {
        //        var dateRange = compositeFilterItem.Filters.FirstOrDefault(filter => filter is CompositeFilterItem) as CompositeFilterItem;
        //        if(dateRange != null) {
        //            switch(parameterName) {
        //                case "validFrom":
        //                    return dateRange.Filters.ElementAt(0).Value as DateTime?;
        //                case "validTo":
        //                    return dateRange.Filters.ElementAt(1).Value as DateTime?;
        //            }
        //        }
        //    }
        //    return null;
        //}

        //protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
        //    var composite = this.FilterItem as CompositeFilterItem;
        //    var newComposite = new CompositeFilterItem();
        //    if(composite != null)
        //        foreach(var filterItem in composite.Filters.Where(filter => filter.GetType() != typeof(CompositeFilterItem)))
        //            newComposite.Filters.Add(filterItem);
        //    return newComposite.ToFilterDescriptor();
        //}
    }
}
