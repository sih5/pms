﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsPurchasePricingDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = { 
            "PartsPurchasePricingChange_Status","PurchasePriceType","PurchasePricingDataSource"
        };

        public PartsPurchasePricingDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new KeyValuesColumnItem{
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_Common_Status
                    },new ColumnItem{
                        Name = "PartCode",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_SparePartCode
                    }, new ColumnItem{
                        Name = "PartName",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_SparePartName
                    }, new ColumnItem{
                        Name = "ReferenceCode",
                       Title = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                    }, new ColumnItem{
                        Name = "PartsSupplierCode",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_PartsSupplierCode
                    }, new ColumnItem{
                        Name = "PartsSupplierName",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_PartsSupplierName
                    }, new ColumnItem{
                        Name = "SupplierPartCode",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsSupplierRelation_SupplierPartCode,
                    }, new ColumnItem{
                        Name = "IsPrimary",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_Sparepart_IsPrimary
                    }, new KeyValuesColumnItem{
                        Name = "PriceType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_PriceType
                    }, new ColumnItem{
                        Name = "PurchasePrice",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_PurchasePrice
                    }
                    , new ColumnItem{
                        Name = "LimitQty",
                        Title = CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_ClosedQuantity
                    }, new ColumnItem{
                        Name = "UsedQty",
                        Title = CommonUIStrings.DataGridView_Title_VirtualPartsPurchasePricing_UsedQty
                    }
                    , new ColumnItem{
                        Name = "ValidFrom",
                        Title = CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_ValidTo
                    }, new ColumnItem{
                        Name = "ValidTo",
                        Title = CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_ValidFrom
                    },  new ColumnItem{
                        Name = "Remark",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartsInboundCheckBillRemark
                    }
                    , new KeyValuesColumnItem{
                        Name = "DataSource",
                        Title = CommonUIStrings.DataGridView_Title_VirtualPartsPurchasePricing_DataSource,
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    }, new ColumnItem{
                        Name = "CreatorName",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_CreatorName
                    }, new ColumnItem{
                        Name = "CreateTime",
                        Title = CommonUIStrings.QueryPanel_Title_ProductStandard_CreateTime,
                    }, new ColumnItem{
                        Name = "ModifierName",
                        Title = CommonUIStrings.DataDeTailPanel_Text_BottomStock_ModifierName
                    }, new ColumnItem{
                        Name = "ModifyTime",
                         Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_ModifyTime
                    }, new ColumnItem{
                        Name = "AbandonerName",
                        Title = CommonUIStrings.DataDeTailPanel_Text_BottomStock_AbandonerName
                    }, new ColumnItem{
                        Name = "AbandonTime",
                        Title = CommonUIStrings.DataDeTailPanel_Text_BottomStock_AbandonerTime
                    }, new ColumnItem{
                        Name = "BranchName",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_BranchName
                    }, new ColumnItem{
                        Name = "PartsSalesCategoryName",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_PartsSalesCategoryName
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPartsPurchasePricing);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询配件采购价格2";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filterItem = this.FilterItem as CompositeFilterItem;
            if(filterItem == null)
                return null;
            switch(parameterName) {
                case "partsSalesCategoryId":
                    //var partsSalesCategoryFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "PartsSalesCategoryId");
                    return 221;
                case "availibleTime":
                    var timeFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "AvailibleTime");
                    if(timeFilterItem == null)
                        return null;
                    return Convert.ToDateTime(timeFilterItem.Value).Date;
                case "isOrderable":
                    var isOrderableFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "SparePart.PartsBranch.IsOrderable");
                    return isOrderableFilterItem == null ? null : isOrderableFilterItem.Value;
                case "isSalable":
                    var isSalableFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "SparePart.PartsBranch.IsSalable");
                    return isSalableFilterItem == null ? null : isSalableFilterItem.Value;
                case "partCode":
                    var codes = filterItem.Filters.SingleOrDefault(s => s.MemberName == "SparePartCode");  
                        if(codes == null || codes.Value == null)  
                            return null;  
                        return codes.Value.ToString();  
                case "partName":
                    var partNameFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "SparePart.Name");
                    return partNameFilterItem == null ? null : partNameFilterItem.Value;
                case "supplierCode":
                    var supplierCodeFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "PartsSupplier.Code");
                    return supplierCodeFilterItem == null ? null : supplierCodeFilterItem.Value;
                case "supplierName":
                    var supplierNameFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "PartsSupplier.Name");
                    return supplierNameFilterItem == null ? null : supplierNameFilterItem.Value;
                case "referenceCode":
                    var referenceCodeFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "SparePart.ReferenceCode");
                    return referenceCodeFilterItem == null ? null : referenceCodeFilterItem.Value;
                case "supplierPartCode":
                    var supplierPartCodeFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "SupplierPartCode");
                    return supplierPartCodeFilterItem == null ? null : supplierPartCodeFilterItem.Value;
                case "partsSupplierId":
                case "partsIds":
                case "brachId":
                    return null;
                default:
                    return null;
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                var param = new[] { "SparePartCode", "SparePart.Name", "AvailibleTime", "SparePart.PartsBranch.IsOrderable", "SparePart.PartsBranch.IsSalable", "PartsSupplier.Code", "PartsSupplier.Name","SparePart.ReferenceCode","SupplierPartCode" };
                foreach(var item in compositeFilterItem.Filters.Where(e => !param.Contains(e.MemberName))) {
                    newCompositeFilterItem.Filters.Add(item);
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }
}
