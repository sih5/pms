﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsPurchasePricingDetailDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = { 
            "PurchasePriceType","PurchasePricingDataSource"
        };

        public PartsPurchasePricingDetailDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += this.PartsPurchasePricingDetailDataGridView_DataContextChanged;
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "PartCode"
                    }, new ColumnItem {
                        Name = "PartName"
                    } , new ColumnItem {
                        Name = "IsPrimary",
                        Title=CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_IsPrimaryNew,
                        TextAlignment = TextAlignment.Center
                    }, new ColumnItem {
                        Name = "Price",
                        TextAlignment = TextAlignment.Right
                    }, new KeyValuesColumnItem{
                        Name = "PriceType",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_IsZg,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    } , new ColumnItem {
                        Name = "IfPurchasable",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_VirtualPartsBranch_IsOrderable,
                        TextAlignment = TextAlignment.Center
                    }, new ColumnItem {
                        Name = "SupplierName"
                    }, new ColumnItem {
                        Name = "ReferencePrice",
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "PriceRate",
                        Title = CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_PriceRate,
                        TextAlignment = TextAlignment.Right
                    }
                    , new ColumnItem {
                        Name = "OldPriSupplierName",
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_OldPriSupplierName,
                        IsReadOnly = true
                    } , new ColumnItem {
                        Name = "OldPriPrice",
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_OldPriPrice,
                        IsReadOnly = true
                    } , new KeyValuesColumnItem {
                        Name = "OldPriPriceType",
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_OldPriPriceType,
                        IsReadOnly = true,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }
                    , new ColumnItem {
                        Name = "MinPurchasePrice",
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_MinPurchasePrice,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    } , new ColumnItem {
                        Name = "MinPriceSupplier",
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_MinPriceSupplier,
                        IsReadOnly = true
                    } , new ColumnItem {
                        Name = "IsPrimaryMinPrice",
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_IsPrimaryMinPrice,
                        TextAlignment = TextAlignment.Center,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "LimitQty",
                        Title=CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_ClosedQuantity,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "SupplierCode"
                    },new ColumnItem {
                        Name = "SupplierPartCode",
                        Title =CommonUIStrings.DataGridView_ColumnItem_Title_PartsSupplierRelation_SupplierPartCode,
                    }, new ColumnItem {
                        Name = "ValidFrom"
                    }, new ColumnItem {
                        Name = "ValidTo"
                    }, new KeyValuesColumnItem{
                        Name = "DataSource",
                        Title = CommonUIStrings.DataGridView_Title_VirtualPartsPurchasePricing_DataSource,
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurchasePricingDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsPurchasePricingDetailsWithSparePart";
        }

        private void PartsPurchasePricingDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsPurchasePricingChange = e.NewValue as PartsPurchasePricingChange;
            if(partsPurchasePricingChange == null || partsPurchasePricingChange.Id == default(int))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "ParentId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo
                };
            this.FilterItem.Value = partsPurchasePricingChange.Id;
            this.ExecuteQueryDelayed();
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
    }
}
