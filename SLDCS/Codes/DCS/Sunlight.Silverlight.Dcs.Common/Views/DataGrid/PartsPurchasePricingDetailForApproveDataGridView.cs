﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.QueryWindow;
using Sunlight.Silverlight.Dcs.Common.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsPurchasePricingDetailForApproveDataGridView : PartsPurchasePricingDetailForEditDataGridView {

          protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new DropDownTextBoxColumnItem {
                        Name = "PartCode",
                        IsEditable = false
                    }, new ColumnItem {
                        Name = "PartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "IsPrimary",
                        Title=CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_IsPrimaryNew,
                        TextAlignment = TextAlignment.Center
                    }, new ColumnItem {
                        Name = "Price",
                        TextAlignment = TextAlignment.Right
                    }, new KeyValuesColumnItem{
                        Name = "PriceType",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_IsZg,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "IfPurchasable",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_VirtualPartsBranch_IsOrderable,
                        TextAlignment = TextAlignment.Center
                    }, new ColumnItem {
                        Name = "SupplierName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ReferencePrice",
                        IsReadOnly = true,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "PriceRate",
                        IsReadOnly = true,
                        Title = CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_PriceRate,
                        TextAlignment = TextAlignment.Right
                    }
                    , new ColumnItem {
                        Name = "OldPriSupplierName",
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_OldPriSupplierName,
                        IsReadOnly = true
                    } , new ColumnItem {
                        Name = "OldPriPrice",
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_OldPriPrice,
                        IsReadOnly = true
                    } , new KeyValuesColumnItem {
                        Name = "OldPriPriceType",
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_OldPriPriceType,
                        IsReadOnly = true,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }
                    , new ColumnItem {
                        Name = "MinPurchasePrice",
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_MinPurchasePrice,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    } , new ColumnItem {
                        Name = "MinPriceSupplier",
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_MinPriceSupplier,
                        IsReadOnly = true
                    } , new ColumnItem {
                        Name = "IsPrimaryMinPrice",
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_IsPrimaryMinPrice,
                        TextAlignment = TextAlignment.Center,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "LimitQty",
                        Title=CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_ClosedQuantity,
                        TextAlignment = TextAlignment.Right
                    }
                    , new DropDownTextBoxColumnItem {
                        Name = "SupplierCode",
                        IsEditable = false
                    }
                    , new ColumnItem {
                        Name = "SupplierPartCode",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_PartsSupplierRelation_SupplierPartCode,
                    }, new ColumnItem {
                        Name = "ValidFrom"
                    }, new ColumnItem {
                        Name = "ValidTo"
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.DataPager.PageSize = 100;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.CanUserInsertRows = false;
            this.GridView.CanUserDeleteRows = false;
            ((GridViewDataColumn)this.GridView.Columns["Price"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["ReferencePrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OldPriPrice"]).DataFormatString = "c2";
        }
    }
}
