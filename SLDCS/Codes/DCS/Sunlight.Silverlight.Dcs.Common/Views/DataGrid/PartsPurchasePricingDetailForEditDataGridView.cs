﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.QueryWindow;
using Sunlight.Silverlight.Dcs.Common.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsPurchasePricingDetailForEditDataGridView : DcsDataGridViewBase {
        protected readonly string[] kvNames = { 
            "PurchasePriceType","PurchasePricingDataSource"
        };
        private QueryWindowBase sparePartDropDownQueryWindow;
        private QueryWindowBase partsSupplierDropDownQueryWindow;
        private int idNumber = -32768;
        private QueryWindowBase SparePartDropDownQueryWindow {
            get {
                if(this.sparePartDropDownQueryWindow == null) {
                    this.sparePartDropDownQueryWindow = DI.GetQueryWindow("SparePartDropDown");
                    this.sparePartDropDownQueryWindow.SelectionDecided += this.SparePartDropDownQueryWindow_SelectionDecided;
                }
                return this.sparePartDropDownQueryWindow;
            }
        }

        void partsSupplierDropDownQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as PartsSupplierDropDownByPartIdQueryWindow;
            var window = sender as QueryWindowBase;
            var queryid = window.DataContext as PartsPurchasePricingDetail;
            if(queryWindow == null)
                return;

            //if(string.IsNullOrWhiteSpace(queryid.OverseasPartsFigureQuery)) {
            //    var filterItem = new FilterItem();
            //    filterItem.MemberName = "PartId";
            //    filterItem.MemberType = typeof(int);
            //    filterItem.Operator = FilterOperator.IsEqualTo;
            //    filterItem.Value = queryid.PartId;
            //    queryWindow.ExchangeData(null, "SetAdditionalFilterItem", filterItem);
            //} else {
            //    var filterItem = new FilterItem();
            //    filterItem.MemberName = "OverseasPartsFigure";
            //    filterItem.MemberType = typeof(int);
            //    filterItem.Operator = FilterOperator.IsEqualTo;
            //    filterItem.Value = queryid.OverseasPartsFigureQuery;
            //    queryWindow.ExchangeData(null, "SetAdditionalFilterItem", filterItem);
            //}
        }

        private QueryWindowBase PartsSupplierDropDownQueryWindow {
            get {
                if(this.partsSupplierDropDownQueryWindow == null) {
                    this.partsSupplierDropDownQueryWindow = DI.GetQueryWindow("PartsSupplierDropDownByPartId");
                    this.partsSupplierDropDownQueryWindow.Loaded += partsSupplierDropDownQueryWindow_Loaded;
                    this.partsSupplierDropDownQueryWindow.SelectionDecided += this.PartsSupplierDropDownQueryWindow_SelectionDecided;
                }
                return this.partsSupplierDropDownQueryWindow;
            }
        }

        private void SparePartDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var partsPurchasePricingChange = this.DataContext as PartsPurchasePricingChange;
            if(partsPurchasePricingChange == null) {
                return;
            }
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var sparePart = queryWindow.SelectedEntities.Cast<SparePart>().FirstOrDefault();
            if(sparePart == null)
                return;

            var partsPurchasePricingDetail = queryWindow.DataContext as PartsPurchasePricingDetail;
            if(partsPurchasePricingDetail == null)
                return;
            partsPurchasePricingDetail.PartId = sparePart.Id;
            partsPurchasePricingDetail.PartName = sparePart.Name;
            partsPurchasePricingDetail.PartCode = sparePart.Code;
            partsPurchasePricingDetail.SupplierCode = null;
            partsPurchasePricingDetail.SupplierId = 0;
            partsPurchasePricingDetail.SupplierName = null;
            partsPurchasePricingDetail.ValidTo = DateTime.Now.AddYears(1);
            partsPurchasePricingDetail.OverseasPartsFigureQuery = sparePart.OverseasPartsFigure;
            if(sparePart.Code.StartsWith("W") || sparePart.Code.StartsWith("w")) {
                partsPurchasePricingDetail.SupplierPartCode = sparePart.ReferenceCode;
            } else {
                partsPurchasePricingDetail.SupplierPartCode = sparePart.Code;
            }
        }

        private void PartsSupplierDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var partsPurchasePricingChange = this.DataContext as PartsPurchasePricingChange;
            if(partsPurchasePricingChange == null) {
                return;
            }
            var partsSupplier = queryWindow.SelectedEntities.Cast<PartsSupplier>().FirstOrDefault();
            if(partsSupplier == null)
                return;

            var partsPurchasePricingDetail = queryWindow.DataContext as PartsPurchasePricingDetail;
            if(partsPurchasePricingDetail == null)
                return;
            partsPurchasePricingDetail.SupplierId = partsSupplier.Id;
            partsPurchasePricingDetail.SupplierCode = partsSupplier.Code;
            partsPurchasePricingDetail.SupplierName = partsSupplier.Name;

            partsPurchasePricingDetail.Price = 0;
            partsPurchasePricingDetail.MinPurchasePrice = null;
            partsPurchasePricingDetail.MinPriceSupplierId = null;
            partsPurchasePricingDetail.MinPriceSupplier = string.Empty;
            partsPurchasePricingDetail.IsPrimaryMinPrice = null;

            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            //查询首选供应商价格
            domainContext.Load(domainContext.GetPrimaryPriceByPartIdQuery(partsPurchasePricingDetail.PartId), LoadBehavior.RefreshCurrent, loadOp => {
                if(!loadOp.HasError) {
                    if(loadOp.Entities != null && loadOp.Entities.Any()) {
                        partsPurchasePricingDetail.ReferencePrice = loadOp.Entities.First().PurchasePrice;
                    } else {
                        partsPurchasePricingDetail.ReferencePrice = 0;
                    }
                }
            }, null);
            

            //自动带出配件code
            if(string.IsNullOrWhiteSpace(partsPurchasePricingDetail.OverseasPartsFigureQuery) || partsPurchasePricingDetail.OverseasPartsFigureQuery.ToUpper() == "NULL")
                return;
            domainContext.Load(domainContext.GetSparePartsQuery().Where(r => r.OverseasPartsFigure == partsPurchasePricingDetail.OverseasPartsFigureQuery), LoadBehavior.RefreshCurrent, loadOp => {
                if(!loadOp.HasError)
                    if(loadOp.Entities != null && loadOp.Entities.Any()) {
                        partsPurchasePricingDetail.PartId = loadOp.Entities.First().Id;
                        partsPurchasePricingDetail.PartName = loadOp.Entities.First().Name;
                        partsPurchasePricingDetail.PartCode = loadOp.Entities.First().Code;
                    }
            }, null);
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsPurchasePricingDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.DataPager.PageSize = 100;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            //TODO:Denotes that RadGridView will perform validation only in view mode. 
            this.GridView.ValidatesOnDataErrors = GridViewValidationMode.InViewMode;
            this.GridView.AddingNewDataItem -= this.GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            this.GridView.Deleted += this.GridView_Deleted;
            this.GridView.BeginningEdit += GridView_BeginningEdit;

            ((GridViewDataColumn)this.GridView.Columns["Price"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["ReferencePrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["ValidFrom"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ValidTo"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["MinPurchasePrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["LimitQty"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["OldPriPrice"]).DataFormatString = "c2";
        }
        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e)
        {
            var partsPurchasePricingChange = this.DataContext as PartsPurchasePricingChange;
            if(partsPurchasePricingChange == null)
                return;
            var detail = e.Row.DataContext as PartsPurchasePricingDetail;
            if (detail == null)
                return;
            bool isCanEdit = partsPurchasePricingChange.Code != GlobalVar.ASSIGNED_BY_SERVER && partsPurchasePricingChange.Code.IndexOf("SAP") >= 0;
            
            switch (e.Cell.DataColumn.DataMemberBinding.Path.Path)
            {
                case "Price":
                    if (detail.PartId == 0 || detail.SupplierId == 0 || isCanEdit)
                        e.Cancel = true;
                    break;
                case "PartCode":
                case "SupplierCode":
                case "PriceType":
                case "LimitQty":
                case "Remark":
                     if (isCanEdit)
                        e.Cancel = true;
                    break;
            }
        }

        public PartsPurchasePricingDetailForEditDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += PartsPurchasePricingDetailForEditDataGridView_DataContextChanged;
        }

        private void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var entitys = e.Items.Cast<PartsPurchasePricingDetail>();
            var partsPurchasePricingChange = this.DataContext as PartsPurchasePricingChange;
            if(partsPurchasePricingChange == null)
                return;
            foreach (var entity in entitys) {
                if (partsPurchasePricingChange.PartsPurchasePricingDetailOrigs.Contains(entity))
                    partsPurchasePricingChange.PartsPurchasePricingDetailOrigs.Remove(entity);
            }

        }
        private void PartsPurchasePricingDetailForEditDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsPurchasePricingChange = this.DataContext as PartsPurchasePricingChange;
            if(partsPurchasePricingChange == null)
                return;
            foreach(var partsPurchasePricingDetail in partsPurchasePricingChange.PartsPurchasePricingDetails) {
                partsPurchasePricingDetail.PropertyChanged -= PartsPurchasePricingDetail_PropertyChanged;
                partsPurchasePricingDetail.PropertyChanged += PartsPurchasePricingDetail_PropertyChanged;
            }

        }


        private void PartsPurchasePricingDetail_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            var partsPurchasePricingChange = this.DataContext as PartsPurchasePricingChange;
            if(partsPurchasePricingChange == null)
                return;
            var partsPurchasePricingDetail = sender as PartsPurchasePricingDetail;
            if(partsPurchasePricingDetail == null)
                return;
            if (e.PropertyName == "Price") { 
                partsPurchasePricingDetail.Price = decimal.Round(partsPurchasePricingDetail.Price, 2);
                if (partsPurchasePricingDetail.ReferencePrice != null && partsPurchasePricingDetail.ReferencePrice != 0)
                    partsPurchasePricingDetail.PriceRate = Math.Round((((partsPurchasePricingDetail.Price - (partsPurchasePricingDetail.ReferencePrice ?? 0)) / Convert.ToDecimal(partsPurchasePricingDetail.ReferencePrice)) * 100), 2) + "%";
                    //查询最低采购价，最低价供应商，最低价是否首选
                    if (partsPurchasePricingDetail.Price > 0m) { 
                        var domainContext = this.DomainContext as DcsDomainContext;
                        if(domainContext == null)
                            return;
                        domainContext.Load(domainContext.fullOthersForPartsPurchasePricingDetailQuery(partsPurchasePricingChange.PartsSalesCategoryId,partsPurchasePricingDetail.PartId, partsPurchasePricingDetail.SupplierId,partsPurchasePricingDetail.Price), LoadBehavior.RefreshCurrent, loadOp => {
                            if(!loadOp.HasError) {
                                if(loadOp.Entities != null && loadOp.Entities.Any()) {
                                    var entity = loadOp.Entities.First();
                                    partsPurchasePricingDetail.MinPriceSupplier = entity.MinPriceSupplier;
                                    partsPurchasePricingDetail.MinPriceSupplierId = entity.MinPriceSupplierId;
                                    partsPurchasePricingDetail.MinPurchasePrice = entity.MinPurchasePrice;
                                    partsPurchasePricingDetail.IsPrimaryMinPrice = entity.IsPrimaryMinPrice;
                                    partsPurchasePricingDetail.OldPriSupplierId = entity.OldPriSupplierId;
                                    partsPurchasePricingDetail.OldPriSupplierCode = entity.OldPriSupplierCode;
                                    partsPurchasePricingDetail.OldPriSupplierName = entity.OldPriSupplierName;
                                    partsPurchasePricingDetail.OldPriPrice = entity.OldPriPrice;
                                    partsPurchasePricingDetail.OldPriPriceType = entity.OldPriPriceType;

                                } 
                            }
                        }, null);
                    }
            }
        }


        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var partsPurchasePricingChange = this.DataContext as PartsPurchasePricingChange;
            if(partsPurchasePricingChange == null) {
                return;
            }
            if (partsPurchasePricingChange.IsNewPart == true || partsPurchasePricingChange.IsPrimaryNotMinPurchasePrice == true || partsPurchasePricingChange.NotPrimaryIsMinPurchasePrice == true ||
                partsPurchasePricingChange.IsPrimaryIsMinPurchasePrice == true || partsPurchasePricingChange.NotPrimaryNotMinPurchasePrice == true) { 
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_PartsPurchasePricingChange_Left);
                e.Cancel = true;
                return;
            }
            if (partsPurchasePricingChange.Code !=  GlobalVar.ASSIGNED_BY_SERVER && partsPurchasePricingChange.Code.IndexOf("SAP") >= 0) { 
                e.Cancel = true;
                return;
            }
            if(partsPurchasePricingChange.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_PartsPurchasePricingChange_PartsSalesCategoryNameIsNull);
                e.Cancel = true;
                return;
            }
            var partsPurchasePricingDetail = new PartsPurchasePricingDetail {
                Id = idNumber ++,
                PriceType = (int)DcsPurchasePriceType.正式价格,
                LimitQty = 0,
                IsPrimary = false,
                IfPurchasable = true
            };
            partsPurchasePricingDetail.PropertyChanged += PartsPurchasePricingDetail_PropertyChanged;
            e.NewObject = partsPurchasePricingDetail;
            partsPurchasePricingChange.PartsPurchasePricingDetailOrigs.Add(partsPurchasePricingDetail);
        }


        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new DropDownTextBoxColumnItem {
                        Name = "PartCode",
                        DropDownContent = this.SparePartDropDownQueryWindow,
                        IsEditable = false
                    }, new ColumnItem {
                        Name = "PartName",
                        IsReadOnly = true
                    }, new DropDownTextBoxColumnItem {
                        Name = "SupplierCode",
                        DropDownContent = this.PartsSupplierDropDownQueryWindow,
                        IsEditable = false
                    }, new ColumnItem {
                        Name = "SupplierName",
                        IsReadOnly = true
                    }
                    , new ColumnItem {
                        Name = "SupplierPartCode",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_PartsSupplierRelation_SupplierPartCode,
                    }
                    , new KeyValuesColumnItem{
                        Name = "PriceType",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_IsZg,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Price",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ReferencePrice",
                        IsReadOnly = true,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "PriceRate",
                        IsReadOnly = true,
                        Title = CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_PriceRate,
                        TextAlignment = TextAlignment.Right
                    } , new ColumnItem {
                        Name = "LimitQty",
                        Title=CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_ClosedQuantity,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "IfPurchasable",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_VirtualPartsBranch_IsOrderable,
                        TextAlignment = TextAlignment.Center
                    } , new ColumnItem {
                        Name = "IsPrimary",
                        Title=CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_IsPrimaryNew,
                        TextAlignment = TextAlignment.Center
                    } , new ColumnItem {
                        Name = "OldPriSupplierName",
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_OldPriSupplierName,
                        IsReadOnly = true
                    } , new ColumnItem {
                        Name = "OldPriPrice",
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_OldPriPrice,
                        IsReadOnly = true
                    } , new KeyValuesColumnItem {
                        Name = "OldPriPriceType",
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_OldPriPriceType,
                        IsReadOnly = true,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }
                    , new ColumnItem {
                        Name = "MinPurchasePrice",
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_MinPurchasePrice,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    } , new ColumnItem {
                        Name = "MinPriceSupplier",
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_MinPriceSupplier,
                        IsReadOnly = true
                    } , new ColumnItem {
                        Name = "IsPrimaryMinPrice",
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_IsPrimaryMinPrice,
                        TextAlignment = TextAlignment.Center,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ValidFrom"
                    }, new ColumnItem {
                        Name = "ValidTo"
                    }
                    , new KeyValuesColumnItem{
                        Name = "DataSource",
                        Title = CommonUIStrings.DataGridView_Title_VirtualPartsPurchasePricing_DataSource,
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurchasePricingDetail);
            }
        }
    }
}
