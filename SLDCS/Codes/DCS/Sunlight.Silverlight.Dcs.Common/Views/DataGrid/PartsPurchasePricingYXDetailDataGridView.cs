﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsPurchasePricingYXDetailDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = { 
            "PurchasePriceType"
        };

        public PartsPurchasePricingYXDetailDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += this.PartsPurchasePricingDetailDataGridView_DataContextChanged;
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "PartCode"
                    }, new ColumnItem {
                        Name = "PartName"
                    }, new ColumnItem {
                        Name = "SupplierCode"
                    }, new ColumnItem {
                        Name = "SupplierName"
                    }, new ColumnItem {
                        Name = "SparePart.OverseasPartsFigure",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_OverseasPartsFigure
                    }, new KeyValuesColumnItem{
                        Name = "PriceType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Price",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ValidFrom"
                    }, new ColumnItem {
                        Name = "ValidTo"
                    }, new ColumnItem {
                        Name = "ReferencePrice",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "Remark"
                    },new ColumnItem {
                        Name = "PriceRate",
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_PriceRateNew
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurchasePricingDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsPurchasePricingDetailsWithSparePart";
        }

        private void PartsPurchasePricingDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsPurchasePricingChange = e.NewValue as PartsPurchasePricingChange;
            if(partsPurchasePricingChange == null || partsPurchasePricingChange.Id == default(int))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "ParentId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo
                };
            this.FilterItem.Value = partsPurchasePricingChange.Id;
            this.ExecuteQueryDelayed();
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
    }
}
