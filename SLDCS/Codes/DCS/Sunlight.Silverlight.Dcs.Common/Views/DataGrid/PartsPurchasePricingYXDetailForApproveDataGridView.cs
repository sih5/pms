﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsPurchasePricingYXDetailForApproveDataGridView : PartsPurchasePricingYXDetailUpdateForEditDataGridView {
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.DataPager.PageSize = 100;
            this.GridView.CanUserInsertRows = false;
            this.GridView.CanUserDeleteRows = false;
        }
    }
}
