﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.QueryWindow;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsPurchasePricingYXDetailUpdateForEditDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = { 
            "PurchasePriceType"
        };
        private QueryWindowBase sparePartDropDownQueryWindow;
        private QueryWindowBase partsSupplierDropDownQueryWindow;
        private QueryWindowBase SparePartDropDownQueryWindow {
            get {
                if(this.sparePartDropDownQueryWindow == null) {
                    this.sparePartDropDownQueryWindow = DI.GetQueryWindow("SparePartDropDown");
                    this.sparePartDropDownQueryWindow.SelectionDecided += this.SparePartDropDownQueryWindow_SelectionDecided;
                }
                return this.sparePartDropDownQueryWindow;
            }
        }

        void partsSupplierDropDownQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as PartsSupplierByPartIdWithPartsSalesCategoryQueryWindow;
            var window = sender as QueryWindowBase;
            var queryid = window.DataContext as PartsPurchasePricingDetail;
            if(queryWindow == null)
                return;
            var filterItem = new FilterItem();

            if(!string.IsNullOrWhiteSpace(queryid.OverseasPartsFigureQuery)) {
                var domainContext = this.DomainContext as DcsDomainContext;
                if(domainContext == null)
                    return;
                domainContext.Load(domainContext.GetSparePartsQuery().Where(r => r.OverseasPartsFigure == queryid.OverseasPartsFigureQuery), LoadBehavior.RefreshCurrent, loadOp => {
                    if(!loadOp.HasError) {
                        if(loadOp.Entities != null && loadOp.Entities.Any()) {
                            queryid.PartId = loadOp.Entities.First().Id;
                            queryid.PartName = loadOp.Entities.First().Name;
                            queryid.PartCode = loadOp.Entities.First().Code;
                        } else {
                            queryid.PartId = default(int);
                            queryid.PartName = null;
                            queryid.PartCode = null;
                        }
                        var compositeFilterItem = new CompositeFilterItem();
                        compositeFilterItem.Filters.Add(new FilterItem("PartId", typeof(int), FilterOperator.IsEqualTo, queryid.PartId));
                        compositeFilterItem.Filters.Add(new FilterItem("PartsSalesCategoryId", typeof(int), FilterOperator.IsEqualTo, queryid.PartsPurchasePricingChange.PartsSalesCategoryId));
                        compositeFilterItem.Filters.Add(new FilterItem("OverseasPartsFigure", typeof(string), FilterOperator.IsEqualTo, queryid.OverseasPartsFigureQuery));
                        queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);

                    }
                }, null);
            } else {
                var compositeFilterItem = new CompositeFilterItem();
                compositeFilterItem.Filters.Add(new FilterItem("PartId", typeof(int), FilterOperator.IsEqualTo, queryid.PartId));
                compositeFilterItem.Filters.Add(new FilterItem("PartsSalesCategoryId", typeof(int), FilterOperator.IsEqualTo, queryid.PartsPurchasePricingChange.PartsSalesCategoryId));
                queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
            }
        }


        private QueryWindowBase PartsSupplierDropDownQueryWindow {
            get {
                if(this.partsSupplierDropDownQueryWindow == null) {
                    this.partsSupplierDropDownQueryWindow = DI.GetQueryWindow("PartsSupplierByPartIdWithPartsSalesCategory");
                    this.partsSupplierDropDownQueryWindow.Loaded += partsSupplierDropDownQueryWindow_Loaded;
                    this.partsSupplierDropDownQueryWindow.SelectionDecided += this.PartsSupplierDropDownQueryWindow_SelectionDecided;
                }
                return this.partsSupplierDropDownQueryWindow;
            }
        }

        private void SparePartDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var partsPurchasePricingChange = this.DataContext as PartsPurchasePricingChange;
            if(partsPurchasePricingChange == null) {
                return;
            }
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var sparePart = queryWindow.SelectedEntities.Cast<SparePart>().FirstOrDefault();
            if(sparePart == null)
                return;

            var partsPurchasePricingDetail = queryWindow.DataContext as PartsPurchasePricingDetail;
            if(partsPurchasePricingDetail == null)
                return;
            partsPurchasePricingDetail.PartId = sparePart.Id;
            partsPurchasePricingDetail.PartName = sparePart.Name;
            partsPurchasePricingDetail.PartCode = sparePart.Code;
            partsPurchasePricingDetail.SupplierCode = null;
            partsPurchasePricingDetail.SupplierId = 0;
            partsPurchasePricingDetail.SupplierName = null;
            partsPurchasePricingDetail.ValidTo = DateTime.Now.AddYears(1);
            partsPurchasePricingDetail.OverseasPartsFigureQuery = sparePart.OverseasPartsFigure;
        }

        private void PartsSupplierDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var partsPurchasePricingChange = this.DataContext as PartsPurchasePricingChange;
            if(partsPurchasePricingChange == null) {
                return;
            }
            var partsSupplier = queryWindow.SelectedEntities.Cast<PartsSupplier>().FirstOrDefault();
            if(partsSupplier == null)
                return;

            var partsPurchasePricingDetail = queryWindow.DataContext as PartsPurchasePricingDetail;
            if(partsPurchasePricingDetail == null)
                return;
            partsPurchasePricingDetail.SupplierId = partsSupplier.Id;
            partsPurchasePricingDetail.SupplierCode = partsSupplier.Code;
            partsPurchasePricingDetail.SupplierName = partsSupplier.Name;

            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.Load(domainContext.GetPartsPurchasePricingsQuery().Where(ex => ex.PartId == partsPurchasePricingDetail.PartId && ex.PartsSupplierId == partsPurchasePricingDetail.SupplierId && ex.Status == (int)DcsPartsPurchasePricingStatus.生效 && ex.PartsSalesCategoryId == partsPurchasePricingChange.PartsSalesCategoryId).OrderByDescending(entity => entity.ValidFrom), LoadBehavior.RefreshCurrent, loadOp => {
                if(!loadOp.HasError) {
                    if(loadOp.Entities != null && loadOp.Entities.Any()) {
                        partsPurchasePricingDetail.ReferencePrice = loadOp.Entities.First().PurchasePrice;
                    } else {
                        partsPurchasePricingDetail.ReferencePrice = 0;
                    }
                }
            }, null);
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsPurchasePricingDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.DataPager.PageSize = 100;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            //TODO:Denotes that RadGridView will perform validation only in view mode. 
            this.GridView.ValidatesOnDataErrors = GridViewValidationMode.InViewMode;
            this.GridView.AddingNewDataItem -= this.GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            this.GridView.CellValidating += GridView_CellValidating;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            this.GridView.RowLoaded += GridView_RowLoaded;
            ((GridViewDataColumn)this.GridView.Columns["Price"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["ValidFrom"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ValidTo"]).DataFormatString = "d";
        }

        public PartsPurchasePricingYXDetailUpdateForEditDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
            if(e.Cell.Column.UniqueName.Equals("Price")) {
                var partsPurchasePricingDetail = e.Cell.ParentRow.DataContext as PartsPurchasePricingDetail;
                if(partsPurchasePricingDetail == null)
                    return;
                if(Convert.ToDecimal(partsPurchasePricingDetail.ReferencePrice) == 0) {
                    partsPurchasePricingDetail.PriceRate = "0";
                } else {
                    partsPurchasePricingDetail.PriceRate = (Math.Round((partsPurchasePricingDetail.Price - Convert.ToDecimal(partsPurchasePricingDetail.ReferencePrice)) / Convert.ToDecimal(partsPurchasePricingDetail.ReferencePrice), 2) * 100) + "%";
                }
            } else if(e.Cell.Column.UniqueName.Equals("PartCode")) {
                var partsPurchasePricingDetail = e.Row.DataContext as PartsPurchasePricingDetail;
                if(partsPurchasePricingDetail == null)
                    return;
                var girdViewRow = this.GridView.GetRowForItem(partsPurchasePricingDetail);
                if(partsPurchasePricingDetail.ReferencePrice == 0) {
                    girdViewRow.Background = new SolidColorBrush(Color.FromArgb(255, 255, 167, 180));
                } else {
                    girdViewRow.Background = new SolidColorBrush(Colors.White);
                }
            } else if(e.Cell.Column.UniqueName.Equals("SupplierCode")) {
                var partsPurchasePricingDetail = e.Row.DataContext as PartsPurchasePricingDetail;
                if(partsPurchasePricingDetail == null)
                    return;
                var girdViewRow = this.GridView.GetRowForItem(partsPurchasePricingDetail);
                if(partsPurchasePricingDetail.ReferencePrice == 0) {
                    girdViewRow.Background = new SolidColorBrush(Color.FromArgb(255, 255, 167, 180));
                } else {
                    girdViewRow.Background = new SolidColorBrush(Colors.White);
                }
            }
        }
        void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            if(e.Cell.Column.UniqueName.Equals("Price")) {
                var partsPurchasePricingDetail = e.Cell.ParentRow.DataContext as PartsPurchasePricingDetail;
                if(partsPurchasePricingDetail == null)
                    return;
                var girdViewRow = this.GridView.GetRowForItem(partsPurchasePricingDetail);
                if(partsPurchasePricingDetail.ReferencePrice == 0) {
                    girdViewRow.Background = new SolidColorBrush(Color.FromArgb(255, 255, 167, 180));
                } else {
                    girdViewRow.Background = new SolidColorBrush(Colors.White);
                }
                if(Convert.ToDecimal(partsPurchasePricingDetail.ReferencePrice) == 0) {
                    partsPurchasePricingDetail.PriceRate = "0";
                } else {
                    partsPurchasePricingDetail.PriceRate = (Math.Round((partsPurchasePricingDetail.Price - Convert.ToDecimal(partsPurchasePricingDetail.ReferencePrice)) / Convert.ToDecimal(partsPurchasePricingDetail.ReferencePrice), 2) * 100) + "%";
                }
            } else if(e.Cell.Column.UniqueName.Equals("PartCode")) {
                var partsPurchasePricingDetail = e.Cell.ParentRow.DataContext as PartsPurchasePricingDetail;
                if(partsPurchasePricingDetail == null)
                    return;
                var girdViewRow = this.GridView.GetRowForItem(partsPurchasePricingDetail);
                if(partsPurchasePricingDetail.ReferencePrice == 0) {
                    girdViewRow.Background = new SolidColorBrush(Color.FromArgb(255, 255, 167, 180));
                } else {
                    girdViewRow.Background = new SolidColorBrush(Colors.White);
                }
            } else if(e.Cell.Column.UniqueName.Equals("SupplierCode")) {
                var partsPurchasePricingDetail = e.Cell.ParentRow.DataContext as PartsPurchasePricingDetail;
                if(partsPurchasePricingDetail == null)
                    return;
                var girdViewRow = this.GridView.GetRowForItem(partsPurchasePricingDetail);
                if(partsPurchasePricingDetail.ReferencePrice == 0) {
                    girdViewRow.Background = new SolidColorBrush(Color.FromArgb(255, 255, 167, 180));
                } else {
                    girdViewRow.Background = new SolidColorBrush(Colors.White);
                }
            }
        }

        private void GridView_RowLoaded(object sender, RowLoadedEventArgs e) {
            var partsPurchasePricingDetail = e.Row.DataContext as PartsPurchasePricingDetail;
            if(partsPurchasePricingDetail == null)
                return;
            var girdViewRow = this.GridView.GetRowForItem(partsPurchasePricingDetail);
            if(partsPurchasePricingDetail.ReferencePrice == 0) {
                girdViewRow.Background = new SolidColorBrush(Color.FromArgb(255, 255, 167, 180));
            } else {
                girdViewRow.Background = new SolidColorBrush(Colors.White);
            }
            partsPurchasePricingDetail.PropertyChanged -= PartsPurchasePricingDetail_PropertyChanged;
            partsPurchasePricingDetail.PropertyChanged += PartsPurchasePricingDetail_PropertyChanged;
        }

        private void PartsPurchasePricingDetail_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            var partsPurchasePricingDetail = sender as PartsPurchasePricingDetail;
            if(partsPurchasePricingDetail == null)
                return;
            switch(e.PropertyName) {
                case "Price":
                    partsPurchasePricingDetail.Price = decimal.Round(partsPurchasePricingDetail.Price, 2);
                    break;
            }
        }
        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var partsPurchasePricingChange = this.DataContext as PartsPurchasePricingChange;
            if(partsPurchasePricingChange == null) {
                return;
            }
            if(partsPurchasePricingChange.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_PartsPurchasePricingChange_PartsSalesCategoryNameIsNull);
                e.Cancel = true;
                return;
            }
            e.NewObject = new PartsPurchasePricingDetail {
                PriceType = (int)DcsPurchasePriceType.正式价格
            };
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new DropDownTextBoxColumnItem {
                        Name = "PartCode",
                        DropDownContent = this.SparePartDropDownQueryWindow,
                        IsEditable = false
                    }, new ColumnItem {
                        Name = "PartName",
                        IsReadOnly = true
                    }, new DropDownTextBoxColumnItem {
                        Name = "SupplierCode",
                        DropDownContent = this.PartsSupplierDropDownQueryWindow,
                        IsEditable = false
                    }, new ColumnItem {
                        Name = "SupplierName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "OverseasPartsFigureQuery",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_OverseasPartsFigure
                    }, new KeyValuesColumnItem{
                        Name = "PriceType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Price",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ValidFrom"
                    }, new ColumnItem {
                        Name = "ValidTo"
                    }, new ColumnItem {
                        Name = "ReferencePrice",
                        IsReadOnly = true,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "PriceRate",
                        Title=CommonUIStrings.DataGridView_Title_PartsPurchasePricingDetail_PriceRateFloat,
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurchasePricingDetail);
            }
        }
    }
}
