﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;
using FilterOperator = Sunlight.Silverlight.Core.Model.FilterOperator;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsReplacementDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        public PartsReplacementDataGridView() {
            this.KeyValueManager.Register(this.kvNames);

        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                var sparePartCompositeFilterItem = new CompositeFilterItem();
                var filters = compositeFilterItem.Filters;
                var partCodeFilter = filters.FirstOrDefault(e => e.MemberName == "PartCode");
                var partNameFilter = filters.FirstOrDefault(e => e.MemberName == "PartName");
                CompositeFilterItem partCodeComposite = null;
                CompositeFilterItem partNameComposite = null;
                if(partCodeFilter != null) {
                    partCodeComposite = new CompositeFilterItem();
                    partCodeComposite.Filters.Add(new FilterItem {
                        MemberName = "OldPartCode",
                        MemberType = typeof(string),
                        Operator = FilterOperator.IsEqualTo,
                        Value = partCodeFilter.Value
                    });
                    partCodeComposite.Filters.Add(new FilterItem {
                        MemberName = "NewPartCode",
                        MemberType = typeof(string),
                        Operator = FilterOperator.IsEqualTo,
                        Value = partCodeFilter.Value
                    });
                    partCodeComposite.LogicalOperator = LogicalOperator.Or;
                }
                if(partNameFilter != null) {
                    partNameComposite = new CompositeFilterItem();
                    partNameComposite.Filters.Add(new FilterItem {
                        MemberName = "OldPartName",
                        MemberType = typeof(string),
                        Operator = FilterOperator.IsEqualTo,
                        Value = partNameFilter.Value
                    });
                    partNameComposite.Filters.Add(new FilterItem {
                        MemberName = "NewPartName",
                        MemberType = typeof(string),
                        Operator = FilterOperator.IsEqualTo,
                        Value = partNameFilter.Value
                    });
                    partNameComposite.LogicalOperator = LogicalOperator.Or;
                }
                if(partCodeComposite != null)
                    sparePartCompositeFilterItem.Filters.Add(partCodeComposite);
                if(partNameComposite != null)
                    sparePartCompositeFilterItem.Filters.Add(partNameComposite);
                newCompositeFilterItem.Filters.Add(sparePartCompositeFilterItem);
                foreach(var item in filters.Where(filter => filter.MemberName != "PartCode" && filter.MemberName != "PartName"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override Type EntityType {
            get {
                return typeof(PartsReplacement);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "OldPartCode",
                        Title =CommonUIStrings.DataEditPanel_Text_PartsReplacement_OldPartCode
                    }, new ColumnItem {
                        Name = "OldPartName"
                    }, new ColumnItem {
                        Name = "OldMInAmount",
                        Title="原件最小销售数量"
                    }, new ColumnItem {
                        Name = "NewPartCode",
                         Title =CommonUIStrings.DataEditPanel_Text_PartsReplacement_NewPartCode
                    }, new ColumnItem {
                        Name = "NewPartName"
                    }, new ColumnItem {
                        Name = "RepMInAmount",
                        Title="替换件最小销售数量"
                    }, new ColumnItem {
                        Name = "IsSPM",
                        Title="SPM是否应用"
                    }, new ColumnItem {
                        Name = "IsInterFace",
                        Title="是否接口传输"
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsReplacementsByReferenceCode";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsReplacement"
                    }
                };
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            switch(parameterName) {
                case "referenceCode":
                    var compositeFilterItem = this.FilterItem as CompositeFilterItem;
                    if(compositeFilterItem != null) {
                        var filterItem = compositeFilterItem.Filters.SingleOrDefault(e => e.MemberName == "PartReferenceCode");
                        var referenceCode = filterItem == null ? null : filterItem.Value;
                        if(filterItem != null)
                            compositeFilterItem.Filters.Remove(filterItem);
                        return referenceCode;
                    }
                    return this.FilterItem.MemberName == "PartReferenceCode" ? this.FilterItem.Value : null;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
    }
}
