﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsReplacementDetailDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "BaseData_Status", "PartsReplacement_ReplacementType"
        };

        public PartsReplacementDetailDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += this.PartsReplacementDetailDataGridView_DataContextChanged;
        }

        private void PartsReplacementDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsReplacement = e.NewValue as PartsReplacement;
            if(partsReplacement == null || partsReplacement.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "NewPartId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsReplacement.NewPartId
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override Type EntityType {
            get {
                return typeof(PartsReplacement);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "OldPartCode",
                        IsSortDescending = false,
                        IsDefaultGroup = true
                    }, new ColumnItem {
                        Name = "OldPartName"
                    }, new ColumnItem {
                        Name = "NewPartCode"
                    }, new ColumnItem {
                        Name = "NewPartName"
                    }, new KeyValuesColumnItem {
                        Name = "ReplacementType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Name = "OldPartQuantity",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "NewPartQuantity",
                        TextAlignment = TextAlignment.Right
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsReplacements";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.ShowGroupPanel = true;
        }

    }
}
