﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using FilterOperator = Sunlight.Silverlight.Core.Model.FilterOperator;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsReplacementForSparePartDataGridView : DcsDataGridViewBase {
        public PartsReplacementForSparePartDataGridView() {
            this.DataContextChanged += this.PartsReplacementForSparePartDataGridView_DataContextChanged;
        }

        private void PartsReplacementForSparePartDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var sparePart = e.NewValue as SparePart;
            if(sparePart == null || sparePart.Id == default(int))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "OldPartId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                };
            this.FilterItem.Value = sparePart.Id;
            this.ExecuteQueryDelayed();
        }

        protected override string OnRequestQueryName() {
            return "GetPartsReplacements";
        }

        protected override Type EntityType {
            get {
                return typeof(PartsReplacement);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "NewPartCode"
                    }, new ColumnItem {
                        Name = "NewPartName"
                    }, new ColumnItem {
                        Name = "OldPartQuantity",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "NewPartQuantity",
                        TextAlignment = TextAlignment.Right
                    }
                };
            }
        }
    }
}
