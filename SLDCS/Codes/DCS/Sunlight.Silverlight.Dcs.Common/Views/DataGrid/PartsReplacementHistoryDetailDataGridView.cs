﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsReplacementHistoryDetailDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };


        protected override Type EntityType {
            get {
                return typeof(PartsReplacementHistory);
            }
        }

        public PartsReplacementHistoryDetailDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += PartsReplacementHistoryDetailDataGridView_DataContextChanged;
        }

        void PartsReplacementHistoryDetailDataGridView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var partsReplacement = e.NewValue as PartsReplacement;
            if(partsReplacement == null || partsReplacement.OldPartId == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsReplacementId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsReplacement.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "OldPartCode",
                          Title =CommonUIStrings.DataEditPanel_Text_PartsReplacement_OldPartCode
                    }, new ColumnItem {
                        Name = "OldPartName"
                    }, new ColumnItem {
                        Name = "NewPartCode",
                         Title =CommonUIStrings.DataEditPanel_Text_PartsReplacement_NewPartCode
                    }, new ColumnItem {
                        Name = "NewPartName"
                    },  new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsReplacementHistories";
        }


    }
}

