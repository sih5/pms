﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsSalePriceIncreaseRateForSelectDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = { "BaseData_Status" };

        public PartsSalePriceIncreaseRateForSelectDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalePriceIncreaseRate);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "status",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_Common_Status,
                        KeyValueItems=this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "GroupCode"
                    }, new ColumnItem {
                        Name = "GroupName"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    } ,new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    } ,new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime"
                    }
            };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsSalePriceIncreaseRates";
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            this.GridView.ShowGroupPanel = false;
        }
    }
}
