﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsSupplierByPartIdWithPartsSalesCategoryDataGridView : DcsDataGridViewBase {
         protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "Code",
                        IsSortDescending = false
                    }, new ColumnItem {
                        Name = "Name"
                    }, new ColumnItem {
                        Name = "ShortName"
                    }, new ColumnItem {
                        Name = "Company.ProvinceName",
                        Title = CommonUIStrings.DataGrid_QueryItem_Title_ProvinceName
                    }, new ColumnItem {
                        Name = "Company.CityName",
                        Title = CommonUIStrings.DataGrid_QueryItem_Title_CityName
                    }, new ColumnItem {
                        Name = "Company.CountyName",
                        Title = CommonUIStrings.DataGrid_QueryItem_Title_CountyName
                    }, new ColumnItem {
                        Name = "Company.ContactPerson"
                    }, new ColumnItem {
                        Name = "Company.ContactPhone"
                    }, new ColumnItem {
                        Name = "Company.Fax"
                    }, new ColumnItem {
                        Name = "Company.ContactMail",
                        Title = CommonUIStrings.DataGrid_QueryItem_Title_ContactMail
                    }, new ColumnItem {
                        Name = "Company.LegalRepresentative",
                        Title = CommonUIStrings.DataGrid_QueryItem_Title_LegalRepresentative
                    }, new ColumnItem {
                        Name = "Company.BusinessScope",
                        Title = CommonUIStrings.DataGrid_QueryItem_Title_BusinessScope
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }
        private readonly string[] kvNames = {
            "MasterData_Status"
        };

        public PartsSupplierByPartIdWithPartsSalesCategoryDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override Type EntityType {
            get {
                return typeof(PartsSupplier);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsSuppliersWithCompanyByPartIdWithPartsSalesCategory";
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            //var filters = this.FilterItem as CompositeFilterItem;
            //if(filters != null) {
            //    switch(parameterName) {
            //        case "partsSalesCategoryId":
            //            if(filters.Filters.Any(item => item.MemberName == "PartsSalesCategoryId"))
            //                return filters.Filters.Single(item => item.MemberName == "PartsSalesCategoryId").Value;
            //            return 0;
            //        case "partId":
            //            if(filters.Filters.Any(item => item.MemberName == "PartId"))
            //                return filters.Filters.Single(item => item.MemberName == "PartId").Value;
            //            return 0;
            //        case "overseasPartsFigure":
            //            if(filters.Filters.Any(item => item.MemberName == "OverseasPartsFigure"))
            //                return filters.Filters.Single(item => item.MemberName == "OverseasPartsFigure").Value;
            //            return null;
            //    }
            //}
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                var cFilterItem = compositeFilterItem.Filters.FirstOrDefault(item => item.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                if(cFilterItem == null)
                    return null;
                FilterItem filter;
                switch(parameterName) {
                    case "partsSalesCategoryId":
                        filter = cFilterItem.Filters.SingleOrDefault(item => item.MemberName == "PartsSalesCategoryId");
                        return filter == null ? 0 : filter.Value;
                    case "partId":
                        filter = cFilterItem.Filters.SingleOrDefault(item => item.MemberName == "PartId");
                        return filter == null ? 0 : filter.Value;
                    case "overseasPartsFigure":
                        filter = cFilterItem.Filters.SingleOrDefault(item => item.MemberName == "OverseasPartsFigure");
                        return filter;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                foreach(var item in compositeFilterItem.Filters.Where(filter =>  filter.GetType() != typeof(CompositeFilterItem)))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.ShowGroupPanel = false;
        }
    
    }
}
