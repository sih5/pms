﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsSupplierDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Code",
                        IsSortDescending = false
                    }, new ColumnItem {
                        Name = "Company.SupplierCode",
                            Title = CommonUIStrings.QueryPanel_Title_PartsSupplier_SupplierCode
                    }, new ColumnItem {
                        Name = "Name"
                    }, new ColumnItem {
                        Name = "ShortName"
                    }, new ColumnItem {
                        Name = "Company.ProvinceName",
                        Title = CommonUIStrings.DataGrid_QueryItem_Title_ProvinceName
                    }, new ColumnItem {
                        Name = "Company.CityName",
                        Title = CommonUIStrings.DataGrid_QueryItem_Title_CityName
                    }, new ColumnItem {
                        Name = "Company.CountyName",
                        Title = CommonUIStrings.DataGrid_QueryItem_Title_CountyName
                    }, new ColumnItem {
                        Name = "Company.ContactPerson"
                    }, new ColumnItem {
                        Name = "Company.ContactPhone"
                    }, new ColumnItem {
                        Name = "Company.Fax"
                    }, new ColumnItem {
                        Name = "Company.ContactMail",
                        Title = CommonUIStrings.DataGrid_QueryItem_Title_ContactMail
                    }, new ColumnItem {
                        Name = "Company.LegalRepresentative",
                        Title = CommonUIStrings.DataGrid_QueryItem_Title_LegalRepresentative
                    }, new ColumnItem {
                        Name = "Company.BusinessScope",
                        Title = CommonUIStrings.DataGrid_QueryItem_Title_BusinessScope
                    },new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }
        private readonly string[] kvNames = {
            "MasterData_Status"
        };

        public PartsSupplierDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override Type EntityType {
            get {
                return typeof(PartsSupplier);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsSuppliersWithCompany";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.ShowGroupPanel = false;
        }
    }
}
