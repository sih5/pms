﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web.Entities;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsSupplierForFaultPartsDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_Common_Status
                    }, new ColumnItem {
                        Name = "Code",
                        IsSortDescending = false,
                        Title = CommonUIStrings.QueryPanel_Title_FaultyParts_SupplierCode
                    }, new ColumnItem {
                        Name = "Name",
                        Title = CommonUIStrings.QueryPanel_Title_FaultyParts_SupplierName
                    }, new ColumnItem {
                        Name = "ShortName",
                        Title =  CommonUIStrings.DataEditView_Text_CompanyAddress_Abbreviation
                    }, new ColumnItem {
                        Name = "ProvinceName",
                        Title = CommonUIStrings.DataGrid_QueryItem_Title_ProvinceName
                    }, new ColumnItem {
                        Name = "CityName",
                        Title = CommonUIStrings.DataGrid_QueryItem_Title_CityName
                    }, new ColumnItem {
                        Name = "CountyName",
                        Title = CommonUIStrings.DataGrid_QueryItem_Title_CountyName
                    }, new ColumnItem {
                        Name = "ContactPerson",
                        Title = CommonUIStrings.DataEditView_Text_CompanyAddress_LinkMan
                    }, new ColumnItem {
                        Name = "ContactPhone",
                        Title = CommonUIStrings.DataEditPanel_Text_Company_LinkPhone
                    }, new ColumnItem {
                        Name = "Fax",
                        Title = CommonUIStrings.DataEditView_Text_CompanyAddress_Fax
                    }, new ColumnItem {
                        Name = "ContactMail",
                        Title = CommonUIStrings.DataGrid_QueryItem_Title_ContactMail
                    }, new ColumnItem {
                        Name = "LegalRepresentative",
                        Title = CommonUIStrings.DataGrid_QueryItem_Title_LegalRepresentative
                    }, new ColumnItem {
                        Name = "BusinessScope",
                        Title = CommonUIStrings.DataGrid_QueryItem_Title_BusinessScope
                    },  new ColumnItem {
                        Name = "Remark",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartsInboundCheckBillRemark
                    }
                };
            }
        }
        private readonly string[] kvNames = {
            "MasterData_Status"
        };

        public PartsSupplierForFaultPartsDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override Type EntityType {
            get {
                return typeof(VirtualPartsSupplier);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsSuppliersWithVirtualPartsSupplier";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.ShowGroupPanel = false;
        }
    }
}


