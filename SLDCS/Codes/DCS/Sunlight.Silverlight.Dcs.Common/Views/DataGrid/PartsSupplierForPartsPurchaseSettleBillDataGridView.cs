﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Web.Entities;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsSupplierForPartsPurchaseSettleBillDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_Common_Status
                    },  new ColumnItem {
                        Name = "Code",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierCode
                    }, new ColumnItem {
                        Name = "Name",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierName
                    }, new ColumnItem {
                        Name = "WaitSettlementAmount",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_Supplier_WaitSettlementAmount
                    }, new ColumnItem {
                        Name = "ShortName",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Supplier_ShortName
                    }, new ColumnItem {
                        Name = "ProvinceName",
                        Title = CommonUIStrings.DataGrid_QueryItem_Title_ProvinceName
                    }, new ColumnItem {
                        Name = "CityName",
                        Title = CommonUIStrings.DataGrid_QueryItem_Title_CityName
                    }, new ColumnItem {
                        Name = "CountyName",
                        Title = CommonUIStrings.DataGrid_QueryItem_Title_CountyName
                    }, new ColumnItem {
                        Name = "ContactPerson",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Supplier_ContactPerson
                    }, new ColumnItem {
                        Name = "ContactPhone",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Supplier_ContactPhone
                    }, new ColumnItem {
                        Name = "Fax",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Supplier_Fax
                    }, new ColumnItem {
                        Name = "ContactMail",
                        Title = CommonUIStrings.DataGrid_QueryItem_Title_ContactMail
                    }, new ColumnItem {
                        Name = "LegalRepresentative",
                        Title = CommonUIStrings.DataGrid_QueryItem_Title_LegalRepresentative
                    }, new ColumnItem {
                        Name = "BusinessScope",
                        Title = CommonUIStrings.DataGrid_QueryItem_Title_BusinessScope
                    }, new ColumnItem {
                        Name = "Remark",
                        Title = CommonUIStrings.DataEditView_ProductStandard_Remark
                    }
                };
            }
        }
        private readonly string[] kvNames = {
            "MasterData_Status"
        };

        public PartsSupplierForPartsPurchaseSettleBillDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override Type EntityType {
            get {
                return typeof(VirtualPartsSupplier);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询配件供应商基本信息关联相关业务";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.ShowGroupPanel = false;
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if (compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                foreach (var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "Company.Code" && filter.MemberName != "Company.Name"
                     && filter.MemberName != "Company.SupplierCode" && filter.MemberName != "SupplierType"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var date = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "code":
                        return filters.Filters.Single(item => item.MemberName == "Company.Code").Value;
                    case "name":
                        return filters.Filters.Single(item => item.MemberName == "Company.Name").Value;
                    case "mdmSupplierCode":
                        return filters.Filters.Single(item => item.MemberName == "Company.SupplierCode").Value;
                    case "supplierType":
                        return filters.Filters.Single(item => item.MemberName == "SupplierType").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
    }
}
