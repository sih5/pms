﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web.Entities;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsSupplierForRepairOrderDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_Common_Status
                    },new ColumnItem {
                        Name = "Code",
                        IsSortDescending = false,
                        Title = CommonUIStrings.QueryPanel_Title_FaultyParts_SupplierCode
                    }, new ColumnItem {
                        Name = "Name",
                        Title = CommonUIStrings.QueryPanel_Title_FaultyParts_SupplierName
                    }, new ColumnItem {
                        Name = "ShortName",
                        Title =  CommonUIStrings.DataEditView_Text_CompanyAddress_Abbreviation
                    }, new ColumnItem {
                        Name = "ProvinceName",
                        Title = CommonUIStrings.DataGrid_QueryItem_Title_ProvinceName
                    }, new ColumnItem {
                        Name = "CityName",
                        Title = CommonUIStrings.DataGrid_QueryItem_Title_CityName
                    }, new ColumnItem {
                        Name = "CountyName",
                        Title = CommonUIStrings.DataGrid_QueryItem_Title_CountyName
                    }, new ColumnItem {
                        Name = "ContactPerson",
                        Title = CommonUIStrings.DataEditView_Text_CompanyAddress_LinkMan
                    }, new ColumnItem {
                        Name = "ContactPhone",
                        Title = CommonUIStrings.DataEditPanel_Text_Company_LinkPhone
                    }, new ColumnItem {
                        Name = "Fax",
                        Title = CommonUIStrings.DataEditView_Text_CompanyAddress_Fax
                    }, new ColumnItem {
                        Name = "ContactMail",
                        Title = CommonUIStrings.DataGrid_QueryItem_Title_ContactMail
                    }, new ColumnItem {
                        Name = "LegalRepresentative",
                        Title = CommonUIStrings.DataGrid_QueryItem_Title_LegalRepresentative
                    }, new ColumnItem {
                        Name = "BusinessScope",
                        Title = CommonUIStrings.DataGrid_QueryItem_Title_BusinessScope
                    },  new ColumnItem {
                        Name = "Remark",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartsInboundCheckBillRemark
                    }
                };
            }
        }
        private readonly string[] kvNames = {
            "MasterData_Status"
        };
        public PartsSupplierForRepairOrderDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override Type EntityType {
            get {
                return typeof(VirtualPartsSupplier);
            }
        }
        protected override string OnRequestQueryName() {
            return "GetPartsSuppliersByBranchId";
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                var cFilterItem = compositeFilterItem.Filters.FirstOrDefault(item => item.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                if(cFilterItem == null)
                    return null;
                FilterItem filter;
                switch(parameterName) {
                    case "partsSalesCategoryId":
                        filter = cFilterItem.Filters.SingleOrDefault(item => item.MemberName == "PartsSalesCategoryId");
                        return filter == null ? 0 : filter.Value;
                    case "branchId":
                        filter = cFilterItem.Filters.SingleOrDefault(item => item.MemberName == "BranchId");
                        return filter == null ? 0 : filter.Value;
                }
            }
            return null;
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return base.OnRequestFilterDescriptor(queryName);
            var cFilterItem = compositeFilterItem.Filters.FirstOrDefault(item => item.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
            if(cFilterItem == null)
                return base.OnRequestFilterDescriptor(queryName);
            var newCompositeFilterItem = new CompositeFilterItem();
            foreach(var filter in compositeFilterItem.Filters) {
                var fileitem = filter as CompositeFilterItem;
                if(fileitem == null)
                    newCompositeFilterItem.Filters.Add(filter);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.ShowGroupPanel = false;
        }
    }
}
