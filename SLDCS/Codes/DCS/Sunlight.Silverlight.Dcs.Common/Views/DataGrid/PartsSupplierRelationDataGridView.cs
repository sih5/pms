﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsSupplierRelationDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        public PartsSupplierRelationDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "SparePart.Code",
                        IsSortDescending = false
                        //,
                        //IsDefaultGroup = true
                    }, new ColumnItem {
                        Name = "SparePart.Name"
                    }, new ColumnItem {
                        Name = "SparePart.ReferenceCode",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                    }, new ColumnItem {
                        Name = "PartsSupplier.Code"
                    }, new ColumnItem {
                        Name = "PartsSupplier.Name"
                    }, new ColumnItem {
                        Name = "SupplierPartCode",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsSupplierRelation_SupplierPartCode
                    },  new ColumnItem {
                        Name = "IsPrimary"
                    }, new ColumnItem {
                        Name = "PurchasePercentage",
                        TextAlignment = TextAlignment.Right,
                    }, new ColumnItem {
                        Name = "EconomicalBatch",
                        TextAlignment = TextAlignment.Right,
                    }, new ColumnItem {
                        Name = "MinBatch",
                        TextAlignment = TextAlignment.Right,
                    },  new ColumnItem {
                        Name = "OrderCycle",
                        Title=CommonUIStrings.DataEditView_Text_PartsSupplierRelation_OrderCycle
                    },  new ColumnItem {
                        Name = "OrderGoodsCycle",
                        Title=CommonUIStrings.DataEditView_Text_PartsSupplierRelation_OrderGoodsCycle
                    },  new ColumnItem {
                        Name = "Remark"
                    },new ColumnItem {
                        Name = "CreateTime"
                    },new ColumnItem {
                        Name = "CreatorName"
                    },new ColumnItem {
                        Name = "ModifyTime"
                    },new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "Branch.Code",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_BranchSupplierRelation_BranchCode
                    }, new ColumnItem {
                        Name = "Branch.Name"
                    },new ColumnItem{
                        Name = "PartsSalesCategoryName",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategory
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSupplierRelation);
            }
        }

        protected override Core.IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsSupplierRelationHistory"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsSupplierRelationsWithDetails";
        }
    }
}
