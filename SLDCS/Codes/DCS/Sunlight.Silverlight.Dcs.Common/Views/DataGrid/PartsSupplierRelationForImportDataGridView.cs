﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsSupplierRelationForImportDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SequeueNumber",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_SequeueNumber
                    }, new ColumnItem {
                        Name = "SparePart.Code",
                        IsSortDescending = false,
                        IsDefaultGroup = true
                    }, new ColumnItem { 
                        Name = "SparePart.Name"
                    }, new ColumnItem {
                        Name = "PartsSupplier.Code"
                    }, new ColumnItem {
                        Name = "PartsSupplier.Name"
                    }, new ColumnItem {
                        Name = "SupplierPartCode",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsSupplierRelation_SupplierPartCode,
                    },
                    //new ColumnItem {
                    //    Name = "IsPrimary"
                    //},
                    new ColumnItem {
                        Name = "PurchasePercentage",
                        TextAlignment = TextAlignment.Right,
                    }, new ColumnItem {
                        Name = "EconomicalBatch",
                        TextAlignment = TextAlignment.Right,
                    }, new ColumnItem {
                        Name = "MinBatch",
                        TextAlignment = TextAlignment.Right,
                    }, new ColumnItem {
                        Name = "MonthlyArrivalPeriod",
                        TextAlignment = TextAlignment.Right,
                    }, new ColumnItem {
                        Name = "EmergencyArrivalPeriod",
                        TextAlignment = TextAlignment.Right,
                    }, new ColumnItem {
                        Name = "ArrivalReplenishmentCycle",
                        TextAlignment = TextAlignment.Right,
                    }, new ColumnItem {
                        Name = "OrderCycle",
                        TextAlignment = TextAlignment.Right,
                        Title=CommonUIStrings.DataEditView_Text_PartsSupplierRelation_OrderCycle
                    }, new ColumnItem {
                        Name = "OrderGoodsCycle",
                        TextAlignment = TextAlignment.Right,
                        Title=CommonUIStrings.DataEditView_Text_PartsSupplierRelation_OrderGoodsCycle
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "DefaultOrderWarehouseName"
                    }, new ColumnItem {
                        Name = "Branch.Code",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_BranchSupplierRelation_BranchCode
                    }, new ColumnItem {
                        Name = "Branch.Name"
                    }, new ColumnItem{
                        Name = "PartsSalesCategory.Name",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategory
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSupplierRelation);
            }
        }
    }
}