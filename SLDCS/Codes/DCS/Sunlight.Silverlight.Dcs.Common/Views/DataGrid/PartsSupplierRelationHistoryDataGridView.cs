﻿using System;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsSupplierRelationHistoryDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        public PartsSupplierRelationHistoryDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += PartsSupplierRelationHistoryDataGridView_DataContextChanged;
        }

        private void PartsSupplierRelationHistoryDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsSupplierRelation = e.NewValue as PartsSupplierRelation;
            if(partsSupplierRelation == null || partsSupplierRelation.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsSupplierRelationId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsSupplierRelation.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override System.Collections.Generic.IEnumerable<Core.Model.ColumnItem> ColumnItems {
            get {
                return new[] {
                       new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "SparePart.Code",
                        IsSortDescending = false,
                        IsDefaultGroup = true
                    }, new ColumnItem {
                        Name = "SparePart.Name"
                    }, new ColumnItem {
                        Name = "SparePart.ReferenceCode",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                    }, new ColumnItem {
                        Name = "PartsSupplier.Code"
                    }, new ColumnItem {
                        Name = "PartsSupplier.Name"
                    }, new ColumnItem {
                        Name = "SupplierPartCode"
                    }, new ColumnItem {
                        Name = "SupplierPartName"
                    }, new ColumnItem {
                        Name = "IsPrimary"
                    }, new ColumnItem {
                        Name = "PurchasePercentage",
                        TextAlignment = TextAlignment.Right,
                    }, new ColumnItem {
                        Name = "EconomicalBatch",
                        TextAlignment = TextAlignment.Right,
                    }, new ColumnItem {
                        Name = "MinBatch",
                        TextAlignment = TextAlignment.Right,
                    },  new ColumnItem {
                        Name = "Remark"
                    },new ColumnItem {
                        Name = "CreateTime"
                    },new ColumnItem {
                        Name = "CreatorName"
                    },new ColumnItem {
                        Name = "Branch.Code",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_BranchSupplierRelation_BranchCode
                    }, new ColumnItem {
                        Name = "Branch.Name"
                    }, new ColumnItem{
                        Name = "PartsSalesCategoryName",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategory
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSupplierRelationHistory);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsSupplierRelationHistoriesWithDetails";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.DataPager.PageSize = 10;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
    }
}
