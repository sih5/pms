﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsSupplierWithDetailsDataGridView : PartsSupplierDataGridView {
        private readonly string[] kvNames = {
          "MasterData_Status","PartsSupplier_Type","SupplierType"
        };

        public PartsSupplierWithDetailsDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "Company.Code",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_PartsSupplierCode,
                        IsSortDescending = false
                    }, new ColumnItem {
                        Name = "Company.Name",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_PartsSupplierName
                    }
                    //, new KeyValuesColumnItem {
                    //    Name = "MDMSupplierType",
                    //    Title ="供应商类型",
                    //    KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    //}
                    , new ColumnItem {
                        Name = "ShortName"
                    }, new ColumnItem {
                       Name  = "Company.ProvinceName"
                    }, new ColumnItem {
                       Name  = "Company.CityName"
                    }, new ColumnItem {
                       Name  = "Company.CountyName"
                    }, new ColumnItem {
                        Name = "Company.ContactAddress",
                        Title = CommonUIStrings.DataEditView_Text_CompanyAddress_Address
                    }, new ColumnItem {
                        Name = "Company.ContactPerson",
                        Title = CommonUIStrings.DataEditPanel_Text_Company_Personel1
                    }, new ColumnItem {
                        Name = "Company.ContactPhone",
                        Title = "短信电话"
                    }, new ColumnItem {
                        Name = "Company.ContactMobile",
                        Title = CommonUIStrings.DataEditPanel_Text_Company_Personel1_Phone
                    }, new ColumnItem {
                        Name = "Company.Fax"
                    }, new ColumnItem {
                        Name = "Company.ContactMail",
                        Title = CommonUIStrings.DataEditPanel_Text_Company_ContactMail,
                    }, new ColumnItem {
                        Name = "Company.BusinessLinkName",
                        Title = CommonUIStrings.DataEditPanel_Text_Company_Personel2
                    }, new ColumnItem {
                        Name = "Company.BusinessContactMethod",
                        Title = CommonUIStrings.DataEditPanel_Text_Company_Personel2_Phone
                    }, new ColumnItem {
                        Name = "Company.BusinessLinkerEMail",
                        Title = CommonUIStrings.DataEditPanel_Text_Company_Personel2_Email
                    }, new ColumnItem {
                        Name = "Company.LegalRepresentative"
                    }, new ColumnItem {
                        Name = "Company.BusinessScope"
                    }, new KeyValuesColumnItem {
                        Name = "SupplierType",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsSupplier_SupplierType,
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }
                };
            }
        }
        protected override Type EntityType {
            get {
                return typeof(PartsSupplier);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsSuppliersWithCompany";
        }

        //protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
        //    get {
        //        return new TabControlRowDetailsTemplateProvider {
        //            DetailPanelNames = new[] {
        //                "PartsSupplier"
        //            }
        //        };
        //    }
        //}

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }
    }
}
