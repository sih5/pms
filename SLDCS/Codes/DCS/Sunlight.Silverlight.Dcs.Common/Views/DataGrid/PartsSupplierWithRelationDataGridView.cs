﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PartsSupplierWithRelationDataGridView : DcsDataGridViewBase {
        public PartsSupplierWithRelationDataGridView() {
            this.DataContextChanged += this.PartsSupplierWithRelationDataGridView_DataContextChanged;
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSupplierRelation);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "PartsSupplier.Code"
                    }, new ColumnItem {
                        Name = "PartsSupplier.Name"
                    }, new ColumnItem {
                        Name = "SupplierPartCode"
                    }, new ColumnItem {
                        Name = "SupplierPartName"
                    }, new ColumnItem {
                        Name = "IsPrimary"
                    }, new ColumnItem {
                        Name = "PurchasePercentage"
                    }, new ColumnItem {
                        Name = "EconomicalBatch"
                    }, new ColumnItem {
                        Name = "MinBatch"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsSupplierRelationsWithPartsSupplier";
        }

        private void PartsSupplierWithRelationDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var sparePart = e.NewValue as SparePart;
            if(sparePart == null || sparePart.Id == default(int))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "PartId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                };
            this.FilterItem.Value = sparePart.Id;
            this.ExecuteQueryDelayed();
        }
    }
}
