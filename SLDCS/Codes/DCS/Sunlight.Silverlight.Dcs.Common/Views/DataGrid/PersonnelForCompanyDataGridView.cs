﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid
{
    public class PersonnelForCompanyDataGridView : DcsDataGridViewBase
    {
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                        Name = "CorporationName"
                    }, new ColumnItem {
                        Name = "LoginId"
                    }, new ColumnItem {
                        Name = "Name"
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType
        {
            get
            {
                return typeof(Personnel);
            }
        }

        protected override string OnRequestQueryName()
        {
            return "查询本企业人员信息";
        }
    }
}