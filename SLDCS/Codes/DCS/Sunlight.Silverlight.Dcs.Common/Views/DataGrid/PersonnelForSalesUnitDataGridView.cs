﻿using System.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PersonnelForSalesUnitDataGridView : PersonnelDataGridView {
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionMode = SelectionMode.Single;
        }
    }
}
