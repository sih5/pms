﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PersonnelSaleRegionDataGridView : DcsDataGridViewBase {
        protected readonly string[] kvNames = {
            "BaseData_Status"
        };
        public PersonnelSaleRegionDataGridView() {
            this.KeyValueManager.Register(kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(RegionPersonnelRelation);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetRegionPersonnelRelationWithDetail";
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name ="SalesRegion.RegionCode",
                        Title=CommonUIStrings.DataEditView_SalesRegion_Code
                    },new ColumnItem{
                        Name="SalesRegion.RegionName",
                        Title=CommonUIStrings.DataEditView_SalesRegion_Name
                    },new ColumnItem{
                        Name="Personnel.Name",
                        Title=CommonUIStrings.DataGridView_QueryItem_Title_PersonnelName
                    },new ColumnItem{
                        Name="CreatorName"
                    },new ColumnItem{
                        Name="CreateTime"
                    },new ColumnItem{
                        Name="ModifierName"
                    },new ColumnItem{
                        Name="ModifyTime"
                    },new ColumnItem{
                        Name="AbandonerName"
                    },new ColumnItem{
                        Name="AbandonTime"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }
    }
}
