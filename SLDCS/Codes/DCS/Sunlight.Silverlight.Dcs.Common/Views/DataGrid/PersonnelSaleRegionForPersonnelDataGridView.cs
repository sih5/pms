﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PersonnelSaleRegionForPersonnelDataGridView : DcsDataGridViewBase {
        private DcsMultiPopupsQueryWindowBase companyPersonnelQueryWindow;
        private QueryWindowBase personnelDropDownQueryWindow;
        private RadWindow radQueryWindow;

        private QueryWindowBase PersonnelDropDownQueryWindow {
            get {
                if(this.personnelDropDownQueryWindow == null) {
                    this.personnelDropDownQueryWindow = DI.GetQueryWindow("PersonnelDropDown");
                    this.personnelDropDownQueryWindow.SelectionDecided += personnelDropDownQueryWindow_SelectionDecided;
                    this.personnelDropDownQueryWindow.Loaded += personnelDropDownQueryWindow_Loaded;
                }
                return this.personnelDropDownQueryWindow;
            }
        }

        private void personnelDropDownQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;
            var filterItem = new FilterItem();
            filterItem.MemberName = "CorporationId";
            filterItem.MemberType = typeof(int);
            filterItem.Operator = FilterOperator.IsEqualTo;
            filterItem.Value = BaseApp.Current.CurrentUserData.EnterpriseId;
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", filterItem);
            queryWindow.ExchangeData(null, "RefreshQueryResult", null);
        }

        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = this.CompanyPersonnelQueryWindow,
                    Header = CommonUIStrings.DataGridView_Head_RegionPersonnelRelation,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen
                });
            }
        }
        private DcsMultiPopupsQueryWindowBase CompanyPersonnelQueryWindow {
            get {
                if(this.companyPersonnelQueryWindow == null) {
                    this.companyPersonnelQueryWindow = DI.GetQueryWindow("PersonnelForMultiSelect") as DcsMultiPopupsQueryWindowBase;
                    this.companyPersonnelQueryWindow.SelectionDecided += this.CompanyPersonnelQueryWindow_SelectionDecided;
                    this.companyPersonnelQueryWindow.Loaded += companyPersonnelQueryWindow_Loaded;
                }
                return this.companyPersonnelQueryWindow;
            }
        }

        public void companyPersonnelQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null)
                return;
            var filterItem = new FilterItem();
            filterItem.MemberName = "CorporationId";
            filterItem.MemberType = typeof(int);
            filterItem.Operator = FilterOperator.IsEqualTo;
            filterItem.Value = BaseApp.Current.CurrentUserData.EnterpriseId;
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", filterItem);
            queryWindow.ExchangeData(null, "RefreshQueryResult", null);
        }

        private void personnelDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var maxSerialNumber = this.GridView.Items.Count > 0 ? this.GridView.Items.Cast<RegionPersonnelRelation>().Count() + 1 : 1;
            var queryWindowpersonnel = queryWindow.SelectedEntities.Cast<Personnel>().FirstOrDefault();
            if(queryWindowpersonnel == null)
                return;

            var dataEditView = this.DataContext as PersonnelSaleRegionDataEditView;
            if(dataEditView == null)
                return;
            if(dataEditView.RegionPersonnelRelations.Any(ex => ex.PersonnelId == queryWindowpersonnel.Id)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Error_PersonSubDealer_PersonnelName);
                return;
            }
            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.Load(domainContext.GetPersonnelsQuery().Where(d => d.Id == queryWindowpersonnel.Id), loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var personnels = loadOp.Entities.SingleOrDefault();
                if(personnels != null) {
                    personnels.SequeueNumber = maxSerialNumber;
                    dataEditView.RegionPersonnelRelations.Add(new RegionPersonnelRelation {
                        PersonnelId = queryWindowpersonnel.Id,
                        Personnel = personnels
                    });
                }
            }, null);

        }

        private void CompanyPersonnelQueryWindow_SelectionDecided(object sender, EventArgs e) {

            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var queryWindowpersonnel = queryWindow.SelectedEntities.Cast<Personnel>();
            if(queryWindowpersonnel == null)
                return;
            var regionPersonnelRelation = queryWindow.DataContext as RegionPersonnelRelation;
            var dataEditView = this.DataContext as PersonnelSaleRegionDataEditView;
            if(dataEditView == null)
                return;
            if(dataEditView.RegionPersonnelRelations.Any(ex => queryWindowpersonnel.Any(r => r.Id == ex.PersonnelId))) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Error_PersonSubDealer_PersonnelName);
                return;
            }
            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            var maxSerialNumber = dataEditView.RegionPersonnelRelations.Count;
            foreach(var item in queryWindowpersonnel) {
                domainContext.Load(domainContext.GetPersonnelsQuery().Where(d => d.Id == item.Id), loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    var personnels = loadOp.Entities.SingleOrDefault();
                    maxSerialNumber++;
                    if(personnels != null) {
                        personnels.SequeueNumber = maxSerialNumber;
                        dataEditView.RegionPersonnelRelations.Add(new RegionPersonnelRelation {
                            PersonnelId = item.Id,
                            Status = (int)DcsBaseDataStatus.有效,
                            Personnel = personnels
                        });
                    }
                }, null);
            }
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("RegionPersonnelRelations");
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem{
                        Name = "Personnel.SequeueNumber",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_SequeueNumber,
                        IsReadOnly = true
                    },new DropDownTextBoxColumnItem {
                        Name = "Personnel.Name",
                        DropDownContent = this.PersonnelDropDownQueryWindow,
                        IsEditable = false
                    },new ColumnItem {
                        Name = "Personnel.CorporationName",
                    }, new ColumnItem {
                        Name = "Personnel.CellNumber",
                        IsReadOnly=true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(RegionPersonnelRelation);
            }
        }

        protected override void OnControlsCreated() {
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
            this.GridView.Deleted -= GridView_Deleted;
            this.GridView.Deleted += GridView_Deleted;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
        }
        private void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            //var dataEditView = this.DataContext as PersonnelSaleRegionDataEditView;
            //if(dataEditView == null)
            //    return;
            //var serialNumber = 1;
            //foreach(var item in dataEditView.RegionPersonnelRelations.Where(entity => !e.Items.Contains(entity))) {
            //    item.Personnel.SequeueNumber = serialNumber;
            //    serialNumber++;
            //}

            var dataEditView = this.DataContext as PersonnelSaleRegionDataEditView;
            if(dataEditView == null)
                return;
            var serialNumber = 1;
            foreach(var personSalesCenterLink in dataEditView.RegionPersonnelRelations) {
                personSalesCenterLink.Personnel.SequeueNumber = serialNumber;
                serialNumber++;
            }
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {

            RadQueryWindow.ShowDialog();
            e.Cancel = true;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
    }
}
