﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid  {
    public class PersonnelSupplierRelationDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        protected override Type EntityType {
            get {
                return typeof(PersonnelSupplierRelation);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }


        public PersonnelSupplierRelationDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new KeyValuesColumnItem {
                        Name = "Status",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_Common_Status,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem{
                        Name="SupplierCode",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierCode,
                    },new ColumnItem{
                        Name="SupplierName",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierName,
                    },new ColumnItem{
                        Name="PersonCode",
                        Title=CommonUIStrings.DataEditPanel_Text_PersonnelSupplierRelation_PersonCode
                    },new ColumnItem{
                        Name="PersonName",
                        Title=CommonUIStrings.QueryPanel_Title_PersonnelSupplierRelation_PersonName
                    },new ColumnItem{
                        Name="CreatorName",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_CreatorName
                    },new ColumnItem{
                        Name="CreateTime",
                        Title = CommonUIStrings.QueryPanel_Title_ProductStandard_CreateTime,
                    },new ColumnItem{
                        Name="ModifierName",
                        Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_ModifierName
                    },new ColumnItem{
                        Name="ModifyTime",
                         Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_ModifyTime
                    },new ColumnItem{
                        Name="AbandonerName",
                        Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_AbandonerName
                    },new ColumnItem{
                        Name="AbandonTime",
                        Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_AbandonerTime
                    },new ColumnItem{
                        Name="Remark",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBillForReport_PartsInboundCheckBillRemark
                    }, new ColumnItem {
                        Name ="PartsSalesCategoryName",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "QueryPersonnelSupplierRelations";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PersonnelSupplierRelation"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ModifyTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["AbandonTime"]).DataFormatString = "d";
        }
    }
}
