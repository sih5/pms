﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class PersonnelSupplierRelationForImportForEditDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem {
                        Name = "SequeueNumber",
                        Title=CommonUIStrings.DataGridView_Title_SequeueNumber
                    },new ColumnItem{
                        Name="Personnel.LoginId",
                        Title = CommonUIStrings.DataEditView_Text_PersonnelSupplierRelation_PersonelCode
                    },new ColumnItem{
                        Name="PartsSupplier.Code",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierCode,
                    },new ColumnItem{
                        Name="Remark",
                        Title=Utils.GetEntityLocalizedName(typeof(PersonnelSupplierRelation),"Remark")
                    },new ColumnItem{
                        Name="PartsSalesCategory.Name",
                        Title = CommonUIStrings.DataEditView_Text_BottomStock_Brand
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PersonnelSupplierRelation);
            }
        }
    }
}