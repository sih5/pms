﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class ProductExecutiveStandardDataGridView : DcsDataGridViewBase {
        protected readonly string[] KvNames = {
            "MasterData_Status"
        };

        public ProductExecutiveStandardDataGridView() {
            this.KeyValueManager.Register(this.KvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]]
                    }, new ColumnItem {
                        Name = "StandardCode",
                    }, new ColumnItem {
                        Name = "StandardName",
                    },new ColumnItem {
                        Name = "Remark",
                    }, new ColumnItem {
                        Name = "CreatorName",
                    },new ColumnItem {
                        Name = "CreateTime",
                    }, new ColumnItem {
                        Name = "ModifierName",
                    },new ColumnItem {
                        Name = "ModifyTime",
                    }, new ColumnItem {
                        Name = "AbandonerName",
                    }, new ColumnItem {
                        Name = "AbandonTime",
                    }
                };
            }
        }

        protected override Type EntityType
        {
            get
            {
                //return typeof(ProductStandard);
                return null;
            }
        }

        protected override string OnRequestQueryName() {
            return "GetProductStandards";
        }
    }
}
