﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class ProductViewForSelectDataGridView : DcsDataGridViewBase {
        protected readonly string[] KvNames = {
            "MasterData_Status"
        };

        public ProductViewForSelectDataGridView() {
            this.KeyValueManager.Register(this.KvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]]
                    },new ColumnItem {
                        Name = "BrandCode",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_Product_BrandCode
                    },new ColumnItem {
                        Name = "BrandName",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_Product_BrandName
                    },new ColumnItem {
                        Name = "SubBrandCode",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_Product_SubBrandCode
                    },new ColumnItem {
                        Name = "SubBrandName",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_Product_SubBrandName
                    }, new ColumnItem {
                        Name = "TerraceCode",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_Product_TerraceCode
                    },new ColumnItem {
                        Name = "TerraceName",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_Product_TerraceName
                    }, new ColumnItem {
                        Name = "ProductLine",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_Product_ProductLine
                    }, new ColumnItem {
                        Name = "ProductName",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_Product_ProductName
                    }, new ColumnItem{
                        Name = "ProductFunctionCode",
                        Title = CommonUIStrings.DataGridView_Title_ProductView_ProductFunctionCode
                    }, new ColumnItem {
                        Name = "Code",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_Product_Code
                    },new ColumnItem {
                        Name = "IsSetRelation",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_ProductView_IsSetRelation
                    }, new ColumnItem {
                        Name = "ServiceProductLineName",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_ProductView_ServiceProductLine
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(ProductView);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetProductViewWithServiceProductLineView";
        }
    }
}
