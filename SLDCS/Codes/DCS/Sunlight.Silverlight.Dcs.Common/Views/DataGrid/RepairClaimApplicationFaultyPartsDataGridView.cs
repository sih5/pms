﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class RepairClaimApplicationFaultyPartsDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                       Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartCode,
                        Name = "Code"
                    },new ColumnItem {
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                        Name = "Name"
                    },new ColumnItem {
                        Title = CommonUIStrings.QueryPanel_Title_FaultyParts_SupplierCode,
                        Name = "SupplierCode"
                    },new ColumnItem {
                        Title = CommonUIStrings.QueryPanel_Title_FaultyParts_SupplierName,
                        Name = "SupplierName",
                    },new ColumnItem {
                        Title= CommonUIStrings.QueryPanel_Title_FaultyParts_ReferenceCode,
                        Name = "ReferenceCode"
                    }, new ColumnItem {
                         Title= CommonUIStrings.QueryPanel_Title_FaultyParts_ReferenceName,
                        Name = "ReferenceName"
                    }, new ColumnItem {
                         Title= CommonUIStrings.QueryPanel_Title_FaultyParts_MeasureUnit,
                        Name = "MeasureUnit",
                    }, new ColumnItem {
                         Title= CommonUIStrings.QueryPanel_Title_FaultyParts_Feature,
                        Name = "Feature"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(FaultyPart);
            }
        }

        protected override string OnRequestQueryName() {
            return "维修索赔查询祸首件";
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filterItem = this.FilterItem as CompositeFilterItem;
            if(filterItem == null)
                return null;
            switch(parameterName) {
                case "partsSalecategoryId":
                    var categoryFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "partsSalecategoryId");
                    return categoryFilterItem == null ? null : categoryFilterItem.Value;
                case "supplierName":
                    var supplierCodeFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "supplierCode");
                    return supplierCodeFilterItem == null ? null : supplierCodeFilterItem.Value;
                case "supplierCode":
                    var supplierNameFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "supplierName");
                    return supplierNameFilterItem == null ? null : supplierNameFilterItem.Value;
                case "partsId":
                    var partsIdFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "partsId");
                    return partsIdFilterItem == null ? null : partsIdFilterItem.Value;
                default:
                    return null;
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                var param = new[] { "Code", "Name", "SupplierCode", "SupplierName", "ReferenceCode", "MeasureUnit", "Feature" };
                foreach(var filterItem in compositeFilterItem.Filters.Where(filter => param.Contains(filter.MemberName))) {
                    newCompositeFilterItem.Filters.Add(filterItem);
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }
}
