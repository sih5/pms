﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class ReplaceApproveLimitDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartCode,
                        Name = "PartCode"
                    },new ColumnItem {
                        Title= CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name,
                        Name = "PartsSalesCategoryName"
                    },new ColumnItem {
                        Title = CommonUIStrings.DataEditView_Text_PartsBranch_AutoApproveUpLimit,
                        Name = "AutoApproveUpLimit"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsBranch);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsBranchExtends");
        }

        protected override string OnRequestQueryName() {
            return "GetPartsBranches";
        }
    }
}
