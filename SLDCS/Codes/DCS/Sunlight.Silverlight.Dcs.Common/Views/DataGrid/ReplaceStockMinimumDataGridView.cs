﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class ReplaceStockMinimumDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartCode,
                        Name = "PartCode"
                    },new ColumnItem {
                        Title= CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                        Name = "PartName"
                    },new ColumnItem {
                        Title = CommonUIStrings.DataEditView_Text_PartsBranch_StockMinimum,
                        Name = "StockMinimum"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsBranch);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsBranchExtends");
        }

       
    }
}
