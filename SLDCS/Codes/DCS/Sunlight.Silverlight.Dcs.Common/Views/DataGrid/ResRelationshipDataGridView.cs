﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Core;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class ResRelationshipDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
           "BaseData_Status","ResponsibleMembersResTem"
        };

        public ResRelationshipDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                          KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "ResType",
                        Title =  "责任类型"
                    }, new KeyValuesColumnItem {
                        Name = "ResTem",
                        Title =  "责任组",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    } ,new ColumnItem {
                        Name = "CreatorName",
                        Title =  "创建人"
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title =  "创建时间"
                    }, new ColumnItem {
                        Name = "ModifierName",
                        Title =  "修改人"
                    }, new ColumnItem{
                        Name="ModifyTime",
                        Title =  "修改时间"
                    },new ColumnItem{
                        Name="AbandonerName",
                        Title =  "作废人"
                    },new ColumnItem{
                        Name="AbandonTime",
                        Title =  "作废时间"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(ResRelationship);
            }
        }      
 
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override string OnRequestQueryName() {
            return "GetResRelationships";
        }

    }
}
