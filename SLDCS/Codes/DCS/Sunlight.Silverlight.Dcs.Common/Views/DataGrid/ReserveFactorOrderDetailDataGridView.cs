﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class ReserveFactorOrderDetailDataGridView  : DcsDataGridViewBase {
        public ReserveFactorOrderDetailDataGridView() {
            this.DataContextChanged += this.PartsSupplierWithRelationDataGridView_DataContextChanged;
        }

        protected override Type EntityType {
            get {
                return typeof(ReserveFactorOrderDetail);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "PriceFloor",
                        Title="价格下限(服务站价)"
                    },new ColumnItem {
                        Name = "PriceCap",
                        Title="价格上限"
                    }, new ColumnItem {
                        Name = "LowerLimitCoefficient",
                        Title="下限系数"
                    }, new ColumnItem {
                        Name = "UpperLimitCoefficient",
                        Title="上限系数"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetReserveFactorOrderDetails";
        }

        private void PartsSupplierWithRelationDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var sparePart = e.NewValue as ReserveFactorMasterOrder;
            if(sparePart == null || sparePart.Id == default(int))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "ReserveFactorMasterOrderId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                };
            this.FilterItem.Value = sparePart.Id;
            this.ExecuteQueryDelayed();
        }
    }
}
