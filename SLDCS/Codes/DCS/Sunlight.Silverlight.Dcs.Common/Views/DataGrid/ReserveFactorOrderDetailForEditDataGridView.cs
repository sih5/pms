﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.QueryWindow;
using Sunlight.Silverlight.Dcs.Common.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class ReserveFactorOrderDetailForEditDataGridView : DcsDataGridViewBase {
        private int idNumber = -32768;
        public ReserveFactorOrderDetailForEditDataGridView() {
        }
        protected override Type EntityType {
            get {
                return typeof(ReserveFactorOrderDetail);
            }
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "PriceFloor",
                        Title="价格下限(服务站价)"
                    },new ColumnItem {
                        Name = "PriceCap",
                        Title="价格上限"
                    }, new ColumnItem {
                        Name = "LowerLimitCoefficient",
                        Title="下限系数"
                    }, new ColumnItem {
                        Name = "UpperLimitCoefficient",
                        Title="上限系数"
                    }
                };
            }
        }


        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("ReserveFactorOrderDetails");
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.DataPager.PageSize = 100;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.ValidatesOnDataErrors = GridViewValidationMode.InViewMode;
            this.GridView.AddingNewDataItem -= this.GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            this.GridView.Deleted += this.GridView_Deleted;

            ((GridViewDataColumn)this.GridView.Columns["PriceCap"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["PriceFloor"]).DataFormatString = "c2";
        }
        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var reserveFactorMasterOrder = this.DataContext as ReserveFactorMasterOrder;
            if(reserveFactorMasterOrder == null) {
                return;
            }
            var reserveFactorOrderDetail = new ReserveFactorOrderDetail {
                Id = idNumber++

            };
            e.NewObject = reserveFactorOrderDetail;

            reserveFactorMasterOrder.ReserveFactorOrderDetails.Add(reserveFactorOrderDetail);
        }
        private void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var entitys = e.Items.Cast<ReserveFactorOrderDetail>();
            var partsPurchasePricingChange = this.DataContext as ReserveFactorMasterOrder;
            if(partsPurchasePricingChange == null)
                return;
            foreach(var entity in entitys) {
                if(partsPurchasePricingChange.ReserveFactorOrderDetails.Contains(entity))
                    partsPurchasePricingChange.ReserveFactorOrderDetails.Remove(entity);
            }

        }
    }
}