﻿

using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class ResponsibleDetailDataGridView : DcsDataGridViewBase {
        public ResponsibleDetailDataGridView() { 
            this.DataContextChanged += PartsSalesOrderDetailDataGridView_DataContextChanged; 
        } 
 
        private void PartsSalesOrderDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var responsibleMember = e.NewValue as ResponsibleMember;
            if(responsibleMember == null) 
                return; 
            var compositeFilterItem = new CompositeFilterItem(); 
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "ResponMemberId", 
                MemberType = typeof(int), 
                Operator = FilterOperator.IsEqualTo,
                Value = responsibleMember.Id 
            });             
            this.FilterItem = compositeFilterItem; 
            this.ExecuteQueryDelayed(); 
        } 
 
        protected override IEnumerable<ColumnItem> ColumnItems { 
            get { 
                return new[] {  
                    new ColumnItem{ 
                        Name = "PersonelCode", 
                        Title = "人员编号"
                    }, new ColumnItem{ 
                        Name = "PersonelName", 
                        Title = "人员名称"
                    } 
                }; 
            } 
        } 
 
        protected override Type EntityType { 
            get {
                return typeof(ResponsibleDetail); 
            } 
        } 
 
        protected override string OnRequestQueryName() { 
            return "GetResponsibleDetails"; 
        } 
 
        protected override void OnControlsCreated() { 
            base.OnControlsCreated(); 
            this.GridView.IsReadOnly = true; 
            this.GridView.ShowGroupPanel = true; 
            this.GridView.SelectionMode = SelectionMode.Single; 
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell; 
            
        } 
        protected override bool ShowCheckBox { 
            get { 
                return false; 
            } 
        } 
    } 
} 
