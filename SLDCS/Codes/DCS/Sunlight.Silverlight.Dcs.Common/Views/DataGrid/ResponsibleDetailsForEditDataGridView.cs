﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class ResponsibleDetailsForEditDataGridView: DcsDataGridViewBase {
        private int idNumber = -32768; 
        public ResponsibleDetailsForEditDataGridView() {
        }
       
        public DcsMultiPopupsQueryWindowBase personnelQueryWindow;
        public QueryWindowBase dropDownQueryWindow;
        private RadWindow radQueryWindow;
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "PersonelCode",
                        Title="人员编号",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "PersonelName",
                        Title="人员名称",
                    }
                };
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(ResponsibleDetail);
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.CanUserInsertRows = true;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.IsReadOnly = false;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var maxSerialNumber = this.GridView.Items.Count > 0 ? this.GridView.Items.Cast<ResponsibleDetail>().Max(entity => entity.Id) + 1 : 1;
            e.NewObject = new ResponsibleDetail {
                Id = idNumber++
            };
            RadQueryWindow.ShowDialog();
            e.Cancel = true;
        }

        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = this.PersonnelQueryWindow,
                    Header = CommonUIStrings.QueryPanel_Title_Personnel,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen
                });
            }
        }


        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("ResponsibleDetails");
        }

        public DcsMultiPopupsQueryWindowBase PersonnelQueryWindow {
            get {
                if(this.personnelQueryWindow == null) {
                    this.personnelQueryWindow = DI.GetQueryWindow("PersonnelForMultiSelect") as DcsMultiPopupsQueryWindowBase;
                    this.personnelQueryWindow.SelectionDecided += PersonnelQueryWindow_SelectionDecided;
                    this.personnelQueryWindow.Loaded += personnelQueryWindow_Loaded;
                }
                return personnelQueryWindow;
            }
        }

        private void personnelQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null)
                return;
            var filterItem = new FilterItem();
            filterItem.MemberName = "CorporationId";
            filterItem.MemberType = typeof(int);
            filterItem.Operator = FilterOperator.IsEqualTo;
            filterItem.Value = BaseApp.Current.CurrentUserData.EnterpriseId;
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", filterItem);
            queryWindow.ExchangeData(null, "RefreshQueryResult", null);
        }

        private void PersonnelQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var queryWindowpersonnel = queryWindow.SelectedEntities.Cast<Personnel>();
            if(queryWindowpersonnel == null)
                return;
            var responsibleMember = this.DataContext as ResponsibleMember;
            if(responsibleMember == null)
                return;           
            if(responsibleMember.ResponsibleDetails.Any(ex => queryWindowpersonnel.Any(r => r.Id == ex.PersonelId))) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Error_PersonSubDealer_PersonnelName);
                return;
            }
            foreach(var item in queryWindowpersonnel) {
                var responsibleDetail = new ResponsibleDetail {
                    PersonelId=item.Id,
                    PersonelName=item.Name,
                    PersonelCode=item.LoginId
                };
                responsibleMember.ResponsibleDetails.Add(responsibleDetail);
           }
        }

    }
}