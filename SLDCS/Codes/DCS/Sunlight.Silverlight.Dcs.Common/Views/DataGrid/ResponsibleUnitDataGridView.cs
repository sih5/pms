﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class ResponsibleUnitDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "MasterData_Status"
        };

        public ResponsibleUnitDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Code",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_ResponsibleUnit_Code
                    }, new ColumnItem {
                        Name = "Name",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_ResponsibleUnit_Name
                    }, new ColumnItem {
                        Name = "ClaimRange"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override Type EntityType {
            get {
                return typeof(ResponsibleUnit);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetResponsibleUnits";
        }
    }
}