﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class ResponsibleUnitProductDetailForEditDataGridView : DcsDataGridViewBase {
        private DcsMultiPopupsQueryWindowBase productDropDownQueryWindow;
        private RadWindow radQueryWindow;

        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = ProductDropDownQueryWindow,
                    Header = CommonUIStrings.DataGridView_Head_Product,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen
                });
            }
        }

        private DcsMultiPopupsQueryWindowBase ProductDropDownQueryWindow {
            get {
                if(this.productDropDownQueryWindow == null) {
                    this.productDropDownQueryWindow = DI.GetQueryWindow("ProductAllSelect") as DcsMultiPopupsQueryWindowBase;
                    this.productDropDownQueryWindow.SelectionDecided += this.ProductDropDownQueryWindow_SelectionDecided;
                }
                return this.productDropDownQueryWindow;
            }
        }

        private void ProductDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            //var product = queryWindow.SelectedEntities.Cast<Product>();
            //if(product == null)
            //    return;
            var details = this.GridView.Items.Cast<ResponsibleUnitProductDetail>();
            if(details == null)
                return;
            var responsibleUnit = this.DataContext as ResponsibleUnit;
            var responsibleUnitProductDetail = details as ResponsibleUnitProductDetail[] ?? details.ToArray();
            //if(responsibleUnitProductDetail.Any(v => product.Any(r => r.Id == v.ProductId))) {
            //    var errorProduct = product.First(r => responsibleUnitProductDetail.Any(v => r.Id == v.ProductId));
            //    UIHelper.ShowNotification(string.Format(CommonUIStrings.DataGridView_Error_ServProdLineProductDetail_ProductIsAlreadyExists, errorProduct.Code));
            //    return;
            //}
            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            //foreach(var item in product) {
            //    domainContext.Load(domainContext.GetProductsQuery().Where(ex => ex.Id == item.Id), LoadBehavior.RefreshCurrent, loadOp => {
            //        if(loadOp.HasError) {
            //            if(!loadOp.IsErrorHandled)
            //                loadOp.MarkErrorAsHandled();
            //            DcsUtils.ShowDomainServiceOperationWindow(loadOp);
            //            return;
            //        }
            //        var entity = loadOp.Entities.SingleOrDefault();
            //        if(entity == null)
            //            return;
            //        var detail = new ResponsibleUnitProductDetail();
            //        detail.ProductId = entity.Id;
            //        detail.Product = entity;
            //        responsibleUnit.ResponsibleUnitProductDetails.Add(detail);
            //    }, null);
            //}
        }

        private void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            if(e.Items.Any()) {
                var domainContext = this.DomainContext as DcsDomainContext;
                if(domainContext == null)
                    return;
                foreach(var detail in e.Items.Cast<ResponsibleUnitProductDetail>())
                    if(domainContext.ResponsibleUnitProductDetails.Contains(detail))
                        domainContext.ResponsibleUnitProductDetails.Remove(detail);
            }
        }

        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            switch(e.Cell.Column.UniqueName) {
                case "Product.Code":
                    var responsibleUnitProductDetail = e.Row.DataContext as ResponsibleUnitProductDetail;
                    if(responsibleUnitProductDetail == null || responsibleUnitProductDetail.EntityState != EntityState.New)
                        e.Cancel = true;
                    break;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Product.BrandCode",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_Product_BrandCode,
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "Product.BrandName",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_Product_BrandName,
                        IsReadOnly=true
                    },new ColumnItem {
                        Name = "Product.SubBrandCode",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_Product_SubBrandCode,
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "Product.SubBrandName",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_Product_SubBrandName,
                        IsReadOnly=true
                    },new ColumnItem {
                        Name = "Product.TerraceCode",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_Product_TerraceCode,
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "Product.TerraceName",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_Product_TerraceName,
                        IsReadOnly=true
                    },new ColumnItem {
                        Name = "Product.ProductLine",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_Product_ProductLine,
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "Product.ProductName",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_Product_ProductName,
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "Product.Code",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_Product_Code,
                        IsReadOnly=true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(ResponsibleUnitProductDetail);
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.CanUserInsertRows = true;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.BeginningEdit += this.GridView_BeginningEdit;
            this.GridView.Deleting += this.GridView_Deleting;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            RadQueryWindow.ShowDialog();
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("ResponsibleUnitProductDetails");
        }
    }
}
