﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class SaleRegionMarketingDetailForEditDataGridView : DcsDataGridViewBase {
        private DcsMultiPopupsQueryWindowBase marketingDepartmentQueryWindowForMultiSelect;
        private QueryWindowBase dropDownQueryWindow;
        private RadWindow radQueryWindow;
        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = this.MarketingDepartmentQueryWindowForMultiSelect,
                    Header = CommonUIStrings.DataManagementView_Title_SaleRegionMarketing,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen
                });
            }
        }
        public DcsMultiPopupsQueryWindowBase MarketingDepartmentQueryWindowForMultiSelect {
            get {
                if(this.marketingDepartmentQueryWindowForMultiSelect == null) {
                    this.marketingDepartmentQueryWindowForMultiSelect = DI.GetQueryWindow("MarketingDepartmentMulit") as DcsMultiPopupsQueryWindowBase;
                    this.marketingDepartmentQueryWindowForMultiSelect.SelectionDecided += marketingDepartmentQueryWindow_SelectionDecided;
                    this.marketingDepartmentQueryWindowForMultiSelect.Loaded += marketingDepartmentQueryWindowForMultiSelect_Loaded;
                }
                return this.marketingDepartmentQueryWindowForMultiSelect;
            }
        }

        public QueryWindowBase DropDownQueryWindow {
            get {
                if(this.dropDownQueryWindow == null) {
                    this.dropDownQueryWindow = DI.GetQueryWindow("MarketingDepartmentDropDown");
                    this.dropDownQueryWindow.SelectionDecided += dropDownQueryWindow_SelectionDecided;
                    this.dropDownQueryWindow.Loaded += marketingDepartmentQueryWindowForMultiSelect_Loaded;

                }
                return this.dropDownQueryWindow;
            }
        }

        private void marketingDepartmentQueryWindowForMultiSelect_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;
            var filterItem = new FilterItem();
            filterItem.MemberName = "BranchId";
            filterItem.MemberType = typeof(int);
            filterItem.Operator = FilterOperator.IsEqualTo;
            filterItem.Value = BaseApp.Current.CurrentUserData.EnterpriseId;
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", filterItem);
            queryWindow.ExchangeData(null, "RefreshQueryResult", null);
        }

        private void dropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var queryWindowmarketingDepartment = queryWindow.SelectedEntities.Cast<MarketingDepartment>().FirstOrDefault();
            if(queryWindowmarketingDepartment == null)
                return;
            var regionMarketDptRelation = queryWindow.DataContext as RegionMarketDptRelation;
            var dataEditView = this.DataContext as SaleRegionMarketingDataEditView;
            if(dataEditView == null)
                return;
            if(dataEditView.RegionMarketDptRelations.Any(ex => ex.MarketDepartmentId == queryWindowmarketingDepartment.Id)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Error_SaleRegionMarketing_MarketingDepartmentName);
                return;
            }
            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.Load(domainContext.GetMarketingDepartmentsQuery().Where(d => d.Id == queryWindowmarketingDepartment.Id), loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                if(regionMarketDptRelation != null) {
                    var entity = loadOp.Entities.SingleOrDefault();
                    if(entity != null)
                        entity.SequeueNumber = regionMarketDptRelation.MarketingDepartment.SequeueNumber;
                    regionMarketDptRelation.MarketingDepartment = entity;
                }
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }, null);
        }


        private void marketingDepartmentQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var queryWindowmarketingDepartment = queryWindow.SelectedEntities.Cast<MarketingDepartment>();
            if(queryWindowmarketingDepartment == null)
                return;
            var dataEditView = this.DataContext as SaleRegionMarketingDataEditView;
            if(dataEditView == null)
                return;

            if(dataEditView.RegionMarketDptRelations.Any(ex => queryWindowmarketingDepartment.Any(r => r.Id == ex.MarketDepartmentId))) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Error_SaleRegionMarketing_MarketingDepartmentName);
                return;
            }
            //var maxSerialNumber = this.GridView.Items.Count > 0 ? this.GridView.Items.Cast<RegionMarketDptRelation>().Count() + 1 : 1;

            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            var maxSerialNumber = dataEditView.RegionMarketDptRelations.Count;
            foreach(var marketingDepartment in queryWindowmarketingDepartment) {
                domainContext.Load(domainContext.GetMarketingDepartmentsQuery().Where(entity => entity.Id == marketingDepartment.Id), loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }

                    var entity = loadOp.Entities.SingleOrDefault();
                    maxSerialNumber++;
                    if(entity != null) {
                        entity.SequeueNumber = maxSerialNumber;
                        dataEditView.RegionMarketDptRelations.Add(new RegionMarketDptRelation {
                            MarketDepartmentId = entity.Id,
                            Status = (int)DcsBaseDataStatus.有效,
                            MarketingDepartment = entity,
                        });
                    }
                }, null);
            }
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }
        protected override Type EntityType {
            get {
                return typeof(RegionMarketDptRelation);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("RegionMarketDptRelations");
        }

        protected override bool ShowCheckBox {
            get {
                return false; ;
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.IsReadOnly = false;
            this.GridView.Deleted -= GridView_Deleted;
            this.GridView.Deleted += GridView_Deleted;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
        }

        void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var dataEditView = this.DataContext as SaleRegionMarketingDataEditView;
            if(dataEditView == null)
                return;
            var serialNumber = 1;
            foreach(var regionMarketDptRelation in dataEditView.RegionMarketDptRelations) {
                regionMarketDptRelation.MarketingDepartment.SequeueNumber = serialNumber;
                serialNumber++;
            }
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var maxSerialNumber = this.GridView.Items.Count > 0 ? this.GridView.Items.Cast<RegionMarketDptRelation>().Max(entity => entity.MarketingDepartment.SequeueNumber) + 1 : 1;
            e.NewObject = new RegionMarketDptRelation {
                SequeueNumber = maxSerialNumber
            };
            e.Cancel = true;
            RadQueryWindow.ShowDialog();

        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "MarketingDepartment.SequeueNumber",
                        Title=CommonUIStrings.DataGridView_Title_SequeueNumber
                    },new DropDownTextBoxColumnItem {
                        Name = "MarketingDepartment.Code",
                        DropDownContent = this.DropDownQueryWindow
                    }, new ColumnItem {
                        Name = "MarketingDepartment.Name",
                        IsReadOnly=true
                    }
                };
            }
        }
    }
}
