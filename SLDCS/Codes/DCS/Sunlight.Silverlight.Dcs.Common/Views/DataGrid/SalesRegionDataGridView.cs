﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class SalesRegionDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames ={
           "BaseData_Status"
        };

        public SalesRegionDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(SalesRegion);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "RegionCode",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_SalesRegion_RegionCode
                    }, new ColumnItem {
                        Name = "RegionName",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_SalesRegion_RegionName
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem  {
                        Name = "AbandonTime"
                    }, new ColumnItem {
                        Name = "PartsSalesCategory.Name",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_SalesRegion_PartsSalesCategoryName
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetSalesRegionWithPartsSalesCategory";
        }
    }
}
