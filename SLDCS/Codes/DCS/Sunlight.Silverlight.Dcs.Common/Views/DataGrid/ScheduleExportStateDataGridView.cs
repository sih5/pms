﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class ScheduleExportStateDataGridView : DcsDataGridViewBase {
        protected override Type EntityType {
            get {
                return typeof(ScheduleExportState);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "ScheduleTime",
                    },
                    new ColumnItem {
                        Name = "Remark",
                        IsSortable = false,
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetScheduleExportStatesFilterEnterpriseCreator";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();

            var hyperlinkColumn = new GridViewHyperlinkColumn {
                DataMemberBinding = new Binding("FilePath") {
                    Converter = new Sunlight.Silverlight.Dcs.ImageConverter.DownloadFileUriConverter()
                },
                ContentBinding = new Binding("FilePath") {
                    Converter = new Sunlight.Silverlight.Dcs.ImageConverter.FileNameConverter()
                },
                TargetName = "_blank",
            };
            this.GridView.Columns.Insert(1, hyperlinkColumn);
        }
    }
}
