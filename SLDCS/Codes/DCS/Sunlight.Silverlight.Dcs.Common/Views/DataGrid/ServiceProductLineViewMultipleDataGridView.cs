﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid
{
    public class ServiceProductLineViewMultipleDataGridView : DcsDataGridViewBase
    {
        protected readonly string[] kvNames = {
            "ServiceProductLineView_ProductLineType"
        };

        public ServiceProductLineViewMultipleDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "ProductLineType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = CommonUIStrings.QueryPanel_Title_Dealer_ServiceProductType
                    },new ColumnItem {
                        Name = "ProductLineCode",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_EngineProductLine_EngineCode
                    }, new ColumnItem {
                        Name = "ProductLineName",
                        Title =  CommonUIStrings.QueryPanel_QueryItem_Title_ServiceProductLineView_ProductLineName
                    },new ColumnItem {
                        Name = "PartsSalesCategory.Name",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategory
                    }
                };
            }
        }

        protected override Type EntityType
        {
            get
            {
                return typeof(ServiceProductLineView);
            }
        }

        protected override string OnRequestQueryName()
        {
            return "GetServiceProductLineViewWithPartsSalesCategory";
        }

        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.RowLoaded += GridView_RowLoaded;
        }

        private List<int> ids = new List<int>();
        public override object ExchangeData(Core.View.IBaseView sender, string subject, params object[] contents){
            ids = contents[0] as List<int> ?? new List<int>();
            return true;
        }
        private void GridView_RowLoaded(object sender, RowLoadedEventArgs e){
            var serviceProductLine = e.Row.DataContext as ServiceProductLineView;
            if (serviceProductLine == null)
                return;
            if (ids.Contains(serviceProductLine.ProductLineId)){
                e.Row.IsSelected = true;
            }
        }
    }
}
