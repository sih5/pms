﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class SmartCompanyForEditDataGridView : DcsDataGridViewBase {
        private DcsMultiPopupsQueryWindowBase sparePartForPartsPurchaseOrderDropDownQueryWindow;
        private RadWindow radQueryWindow;
        private ObservableCollection<SmartCompany> smartCompany;
        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = this.SparePartForPartsPurchaseOrderDropDownQueryWindow,
                    Header = "选择企业",
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen
                });
            }
        }

        private DcsMultiPopupsQueryWindowBase SparePartForPartsPurchaseOrderDropDownQueryWindow {
            get {
                if(this.sparePartForPartsPurchaseOrderDropDownQueryWindow == null) {
                    this.sparePartForPartsPurchaseOrderDropDownQueryWindow = DI.GetQueryWindow("SmartCompany") as DcsMultiPopupsQueryWindowBase;
                    this.sparePartForPartsPurchaseOrderDropDownQueryWindow.SelectionDecided += this.SparePartForPartsPurchaseOrderDropDownQueryWindow_SelectionDecided;
                }
                return this.sparePartForPartsPurchaseOrderDropDownQueryWindow;
            }
        }
        public ObservableCollection<SmartCompany> SmartCompanys {
            get {
                if(this.smartCompany == null) {
                    this.smartCompany = new ObservableCollection<SmartCompany>();
                } return smartCompany;
            }
        }
        private void SparePartForPartsPurchaseOrderDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var companies = queryWindow.SelectedEntities.Cast<Company>();
            if(companies == null)
                return;
            foreach(var company in companies) {
                if(SmartCompanys.Any(t=>t.CompanyId==company.Id)) {
                    continue;
                }
                var smartCompany = new SmartCompany();
                smartCompany.CompanyId = company.Id;
                smartCompany.CompanyCode = company.Code;
                smartCompany.CompanyName = company.Name;
                smartCompany.Status = (int)DcsBaseDataStatus.有效;
                SmartCompanys.Add(smartCompany);
            }
            this.GridView.ItemsSource = SmartCompanys;
            this.Entities = SmartCompanys;
           // this.GridView.
            this.GridView.IsReadOnly = false;
            this.RadQueryWindow.Close();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "CompanyCode",
                        IsReadOnly = true,
                        Title="订货企业编号"
                    },new ColumnItem {
                        Name = "CompanyName",
                        IsReadOnly = true,
                        Title="订货企业名称"
                    }
                };
            }
        }
        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;

            this.RadQueryWindow.ShowDialog();
        }
        

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SmartCompany);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.ValidatesOnDataErrors = GridViewValidationMode.InViewMode;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
        }   
        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("SmartCompanys");
        }
    }
}