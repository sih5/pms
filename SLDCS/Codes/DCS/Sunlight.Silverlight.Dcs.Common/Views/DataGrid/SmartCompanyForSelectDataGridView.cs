﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class SmartCompanyForSelectDataGridView  : DcsDataGridViewBase {
        protected override string OnRequestQueryName() {
            return "查询可新增的智能企业";
        }     
        protected override Type EntityType {
            get {
                return typeof(Company);
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        public SmartCompanyForSelectDataGridView() {
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new ColumnItem {
                        Name = "Code",
                        Title="企业编号"
                    }, new ColumnItem {
                        Name = "Name",
                        Title="企业名称"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}