﻿using System.Linq;
using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class SmartOrderCalendarDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "BaseData_Status"
        };
        public SmartOrderCalendarDataGridView() {
            this.KeyValueManager.Register(kvNames);
        }
        protected override Type EntityType {
            get {
                return typeof(SmartOrderCalendar);
            }
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    
                    new ColumnItem {
                        Name = "WarehouseCode",
                        Title=CommonUIStrings.DataDeTailPanel_Text_Company_WarehouseCode
                    }, new ColumnItem {
                         Name = "WarehouseName",
                       Title=CommonUIStrings.DataDeTailPanel_Text_Company_WarehouseName
                    }, new ColumnItem {
                        Name = "Times",
                        Title=CommonUIStrings.QueryPanel_KvValue_CentralPartsSalesOrder_Day
                    }, new KeyValuesColumnItem {
                         Name = "Status",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_Common_Status,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "CreatorName",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_CreatorName
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title = CommonUIStrings.QueryPanel_Title_ProductStandard_CreateTime,
                    },new ColumnItem {
                        Name = "ModifierName",
                        Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_ModifierName
                    },new ColumnItem {
                        Name = "ModifyTime",
                        Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_ModifierTime
                    },new ColumnItem {
                        Name = "AbandonerName",
                        Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_AbandonerName
                    },new ColumnItem {
                        Name = "AbandonTime",
                        Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_AbandonerTime
                    }
                };
            }
        }


        protected override string OnRequestQueryName() {
            return "GetSmartOrderCalendars";
        }
       
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ModifyTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["AbandonTime"]).DataFormatString = "d";
        }
    }
}
