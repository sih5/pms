﻿using System.Linq;
using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class SmartProcessMethodDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "BaseData_Status","PartsSalesOrderProcessDetail_ProcessMethod"
        };
        public SmartProcessMethodDataGridView() {
            this.KeyValueManager.Register(kvNames);
        }
        protected override Type EntityType {
            get {
                return typeof(SmartProcessMethod);
            }
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    
                    new ColumnItem {
                        Name = "WarehouseCode",
                        Title=CommonUIStrings.DataDeTailPanel_Text_Company_WarehouseCode
                    }, new ColumnItem {
                         Name = "WarehouseName",
                       Title=CommonUIStrings.DataDeTailPanel_Text_Company_WarehouseName
                    }, new KeyValuesColumnItem {
                        Name = "OrderProcessMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrderFinishReport_OrderProcessMethod
                    }, new KeyValuesColumnItem {
                         Name = "Status",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_Common_Status,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "CreatorName",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_CreatorName
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title = CommonUIStrings.QueryPanel_Title_ProductStandard_CreateTime,
                    },new ColumnItem {
                        Name = "AbandonerName",
                        Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_AbandonerName
                    },new ColumnItem {
                        Name = "AbandonTime",
                        Title=CommonUIStrings.DataDeTailPanel_Text_BottomStock_AbandonerTime
                    }
                };
            }
        }


        protected override string OnRequestQueryName() {
            return "GetSmartProcessMethods";
        }
       
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["AbandonTime"]).DataFormatString = "d";
        }
    }
}