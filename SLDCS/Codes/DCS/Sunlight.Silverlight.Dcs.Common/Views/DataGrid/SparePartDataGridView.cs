﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class SparePartDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "MasterData_Status", "SparePart_PartType","SparePart_MeasureUnit"
        };

        public SparePartDataGridView() {
            this.KeyValueManager.Register(kvNames);
            this.KeyValueManager.LoadData(() => {
                foreach(var item in this.KeyValueManager[kvNames[2]])
                    KvMeasureUnit.Add(item);
            });
        }

        protected override Type EntityType {
            get {
                return typeof(SparePart);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Code",
                        IsSortDescending = false
                    }, new ColumnItem {
                        Name = "Name"
                    },new ColumnItem{
                        Name="ReferenceCode",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                    }                  
                   ,  new KeyValuesColumnItem {
                        Name = "PartType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Name = "MeasureUnit"
                    }, new ColumnItem {
                        Name = "Specification"
                    }, new ColumnItem {
                        Name = "Feature"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetSparePartWithPartsExchangeGroup";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Multiple;
        }

        private ObservableCollection<KeyValuePair> kvMeasureUnit;
        private ObservableCollection<KeyValuePair> KvMeasureUnit {
            get {
                return this.kvMeasureUnit ?? (this.kvMeasureUnit = new ObservableCollection<KeyValuePair>());
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filterItem = this.FilterItem as CompositeFilterItem;
            if(filterItem == null)
                return null;
            switch(parameterName) {
                case "exGroupCode":
                    //var exGroupCodeFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "ExGroupCode");
                    //return exGroupCodeFilterItem.Value;
                    return null;
                default:
                    return null;
            }
        }


        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            if(FilterItem == null)
                return base.OnRequestFilterDescriptor(queryName);
            var newFilterItem = new CompositeFilterItem();
            var compositeFilterItem = FilterItem as CompositeFilterItem;
            if(compositeFilterItem == null) {
                compositeFilterItem = new CompositeFilterItem();
                compositeFilterItem.Filters.Add(this.FilterItem);
            }
            foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "ExGroupCode")) {
                if(item.MemberName == "MeasureUnit") {
                    var measureUnitValue = item.Value as int?;
                    if(measureUnitValue.HasValue && measureUnitValue != -1) {
                        var firstOrDefault = this.KvMeasureUnit.FirstOrDefault(r => r.Key == measureUnitValue.Value);
                        if(firstOrDefault != null) {
                            var measureUnitName = firstOrDefault.Value;
                            newFilterItem.Filters.Add(new FilterItem("MeasureUnit", typeof(string), Core.Model.FilterOperator.IsEqualTo, measureUnitName));
                        }
                    }
                    continue;
                }
                newFilterItem.Filters.Add(item);
            }
            return newFilterItem.ToFilterDescriptor();
        }
    }
}
