﻿
using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class SparePartForBatchReplacementDataGridView : DcsDataGridViewBase {
        private string uniqueId;
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = CommonUIStrings.DataGridView_Title_SequeueNumber,
                        Name = "SerialNumber"
                    },new ColumnItem {
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        Name = "Code"
                    }, new ColumnItem {
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                        Name = "Name"
                     }, new ColumnItem {
                        Name = "ReferenceCode",
                       Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                    },new ColumnItem{
                       Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsExchangeGroup_ExchangeCode,
                       Name="ExchangeIdentification"
                     },new ColumnItem{
                       Title=CommonUIStrings.DataEditPanel_Text_SparePart_OverseasPartsFigure,
                       Name="OverseasPartsFigure"
                     }, new ColumnItem {
                        Name = "SubstandardName",
                        Title=CommonUIStrings.DataGridView_Title_SparePartExtend_SubstandardName      
                    },
                    //new ColumnItem{
                    //   Name = "ProductBrand",
                    //   Title = "产品商标"
                    //},
                    //new ColumnItem{
                    //    Name="Specification",
                    //    Title = Utils.GetEntityLocalizedName(typeof(SparePart), "Specification"),
                    //},
                    new ColumnItem{
                        Name="EnglishName",
                        Title = Utils.GetEntityLocalizedName(typeof(SparePart), "EnglishName"),
                    }, new ColumnItem {
                        Title = Utils.GetEntityLocalizedName(typeof(SparePart), "PartType"),
                        Name = "PartTypeStr"
                    }, new ColumnItem {
                        Title=CommonUIStrings.DataEditPanel_Text_SparePartForBatch_MInPackingAmount,
                        Name = "MInPackingAmountStr"
                    }, new ColumnItem {
                        Title =CommonUIStrings.DataEditPanel_Text_SparePart_Feature,
                        Name = "Feature"
                    },
                    //new ColumnItem {
                    //    Title = "上一替代件",
                    //    Name = "LastSubstitute"
                    //}, new ColumnItem {
                    //    Title = "下一替代件",
                    //    Name = "NextSubstitute"
                    //}, new ColumnItem {
                    //    Title = "IMS压缩号",
                    //    Name = "IMSCompressionNumber"
                    //},
                    new ColumnItem {
                        Title = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_IMSManufacturer,
                        Name = "IMSManufacturerNumber"
                    },
                    //new ColumnItem {
                    //    Title = "研究院图号",
                    //    Name = "CADCode"
                    //},
                    new ColumnItem {
                        Title = CommonUIStrings.QueryPanel_Title_FaultyParts_MeasureUnit,
                        Name = "MeasureUnit"
                    }, new ColumnItem {
                        Title = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_ShelfLife,
                        Name = "ShelfLife"
                    },
                    //new ColumnItem {
                    //    Title = CommonUIStrings.DataEditPanel_Validation_SparePartForBatch_GroupABCCategory,
                    //    Name = "GroupABCCategory"
                    //},
                    new ColumnItem {
                        Title = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_Weight,
                        Name = "Weight"
                    }, new ColumnItem {
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_Volume,
                        Name = "Volume"
                    },
                    //new ColumnItem {
                    //    Title = CommonUIStrings.DataEditPanel_Validation_SparePartForBatch_Assembly,
                    //    Name = "TotalNumber"
                    //}, new ColumnItem {
                    //    Title = "厂家",
                    //    Name = "Factury"
                    //},
                    new ColumnItem {
                        Title = CommonUIStrings.DataEditPanel_Text_SparePart_IsOriginal,
                        Name = "IsOriginal"
                    },
                    //new ColumnItem {
                    //    Title = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_CategoryCode,
                    //    Name = "CategoryCode"
                    //},new ColumnItem{
                    //    Title=CommonUIStrings.DataEditPanel_Text_SparePartForBatch_CategoryName,
                    //    Name="CategoryName"

                    // }, new ColumnItem {
                    //    Title = "产品执行标准代码",
                    //    Name = "StandardCode"
                    //},new ColumnItem{
                    //    Title="产品执行标准名称",
                    //    Name="StandardName"

                    // },
                    new ColumnItem{
                        Title=CommonUIStrings.DataEditPanel_Text_SparePart_PinyinCode,
                        Name="PinyinCode"

                     },
                    new ColumnItem{
                        Title=CommonUIStrings.DataEditPanel_Text_SparePart_Material,
                        Name="Material"

                     },
                    new ColumnItem{
                        Title=CommonUIStrings.DataEditPanel_Text_SparePart_DeclareElement,
                        Name="DeclareElement"

                     },
                    new ColumnItem{
                        Title= CommonUIStrings.QueryPanel_QueryItem_Title_GoldenTaxClassifyMsg_GoldenTaxClassifyCode,
                        Name="GoldenTaxClassifyCode"

                     },
                    new ColumnItem{
                        Title= CommonUIStrings.DataEditPanel_Title_ImportIsSupplierPutIn,
                        Name="IsSupplierPutInStr"                        

                     },
                    new ColumnItem{
                        Title= "追溯属性",
                        Name="TracePropertyStr"                        

                     }
                };
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.Columns["Name"].IsVisible = (uniqueId == "ReplaceName");
            this.GridView.Columns["ReferenceCode"].IsVisible = (uniqueId == "ReplaceReferenceCode");
            //this.GridView.Columns["Specification"].IsVisible = (uniqueId == "ReplaceSpecification");
            this.GridView.Columns["EnglishName"].IsVisible = (uniqueId == "ReplaceEnglishName");
            this.GridView.Columns["PartTypeStr"].IsVisible = (uniqueId == "ReplacePartType");
            this.GridView.Columns["MInPackingAmountStr"].IsVisible = (uniqueId == "ReplaceMInPackingAmount");
            //this.GridView.Columns["Feature"].IsVisible = (uniqueId == "ReplaceFeature");
            //this.GridView.Columns["NextSubstitute"].IsVisible = (uniqueId == "ReplaceNextSubstitute");
            //this.GridView.Columns["IMSCompressionNumber"].IsVisible = (uniqueId == "ReplaceIMSCompressionNumber");
            this.GridView.Columns["IMSManufacturerNumber"].IsVisible = (uniqueId == "ReplaceIMSManufacturerNumber");
            //this.GridView.Columns["ProductBrand"].IsVisible = (uniqueId == "ReplaceProductBrand");
            //this.GridView.Columns["CategoryName"].IsVisible = (uniqueId == "ReplaceImportEdit");
            this.GridView.Columns["OverseasPartsFigure"].IsVisible = (uniqueId == "ReplaceOverseasPartsFigure");
            this.GridView.Columns["GoldenTaxClassifyCode"].IsVisible = (uniqueId == "ImportGoldenTaxClassify");
            this.GridView.Columns["IsSupplierPutInStr"].IsVisible = (uniqueId == "ImportIsSupplier");
            this.GridView.Columns["TracePropertyStr"].IsVisible = (uniqueId == "ReplaceTraceProperty");
        }
        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("SparePartExtends");
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
        public override object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            uniqueId = subject;
            if(this.GridView == null)
                return null;
            switch(subject) {
                case "ReplaceName":
                case "ImportEdit":
                case "ResumeEdit":
                    this.GridView.Columns["Name"].IsVisible = true;
                    this.GridView.Columns["SubstandardName"].IsVisible = false;
                    this.GridView.Columns["ReferenceCode"].IsVisible = false;
                    //this.GridView.Columns["Specification"].IsVisible = false;
                    this.GridView.Columns["EnglishName"].IsVisible = false;
                    this.GridView.Columns["PartTypeStr"].IsVisible = false;
                    this.GridView.Columns["MInPackingAmountStr"].IsVisible = false;
                    this.GridView.Columns["Feature"].IsVisible = false;
                    //this.GridView.Columns["NextSubstitute"].IsVisible = false;
                    this.GridView.Columns["IMSManufacturerNumber"].IsVisible = false;
                    //this.GridView.Columns["IMSCompressionNumber"].IsVisible = false;
                    //this.GridView.Columns["ProductBrand"].IsVisible = false;
                    //this.GridView.Columns["LastSubstitute"].IsVisible = false;
                    //this.GridView.Columns["CADCode"].IsVisible = false;
                    this.GridView.Columns["MeasureUnit"].IsVisible = false;
                    this.GridView.Columns["ShelfLife"].IsVisible = false;
                    //this.GridView.Columns["GroupABCCategory"].IsVisible = false;
                    this.GridView.Columns["Weight"].IsVisible = false;
                    this.GridView.Columns["Volume"].IsVisible = false;
                    //this.GridView.Columns["TotalNumber"].IsVisible = false;
                    //this.GridView.Columns["Factury"].IsVisible = false;
                    this.GridView.Columns["IsOriginal"].IsVisible = false;
                    //this.GridView.Columns["CategoryCode"].IsVisible = false;
                    //this.GridView.Columns["CategoryName"].IsVisible = false;
                    this.GridView.Columns["OverseasPartsFigure"].IsVisible = false;
                    this.GridView.Columns["GoldenTaxClassifyCode"].IsVisible = false;
                    this.GridView.Columns["ExchangeIdentification"].IsVisible = false;
                    this.GridView.Columns["PinyinCode"].IsVisible = false;
                    this.GridView.Columns["Material"].IsVisible = false;
                    this.GridView.Columns["DeclareElement"].IsVisible = false;
                    this.GridView.Columns["IsSupplierPutInStr"].IsVisible = false;
                    break;
                case "ReplaceSpecification":
                    this.GridView.Columns["Name"].IsVisible = false;
                    this.GridView.Columns["SubstandardName"].IsVisible = false;
                    this.GridView.Columns["ReferenceCode"].IsVisible = false;
                    //this.GridView.Columns["Specification"].IsVisible = true;
                    this.GridView.Columns["EnglishName"].IsVisible = false;
                    this.GridView.Columns["PartTypeStr"].IsVisible = false;
                    this.GridView.Columns["MInPackingAmountStr"].IsVisible = false;
                    this.GridView.Columns["Feature"].IsVisible = false;
                    //this.GridView.Columns["NextSubstitute"].IsVisible = false;
                    this.GridView.Columns["IMSManufacturerNumber"].IsVisible = false;
                    //this.GridView.Columns["IMSCompressionNumber"].IsVisible = false;
                    //this.GridView.Columns["ProductBrand"].IsVisible = false;
                    //this.GridView.Columns["LastSubstitute"].IsVisible = false;
                    //this.GridView.Columns["CADCode"].IsVisible = false;
                    this.GridView.Columns["MeasureUnit"].IsVisible = false;
                    this.GridView.Columns["ShelfLife"].IsVisible = false;
                    //this.GridView.Columns["GroupABCCategory"].IsVisible = false;
                    this.GridView.Columns["Weight"].IsVisible = false;
                    this.GridView.Columns["Volume"].IsVisible = false;
                    //this.GridView.Columns["TotalNumber"].IsVisible = false;
                    //this.GridView.Columns["Factury"].IsVisible = false;
                    this.GridView.Columns["IsOriginal"].IsVisible = false;
                    //this.GridView.Columns["CategoryCode"].IsVisible = false;
                    //this.GridView.Columns["CategoryName"].IsVisible = false;
                    this.GridView.Columns["OverseasPartsFigure"].IsVisible = false;
                    this.GridView.Columns["GoldenTaxClassifyCode"].IsVisible = false;
                    this.GridView.Columns["ExchangeIdentification"].IsVisible = false;
                    this.GridView.Columns["PinyinCode"].IsVisible = false;
                    this.GridView.Columns["Material"].IsVisible = false;
                    this.GridView.Columns["DeclareElement"].IsVisible = false;
                    this.GridView.Columns["IsSupplierPutInStr"].IsVisible = false;
                    break;
                case "ReplaceEnglishName":
                    this.GridView.Columns["Name"].IsVisible = false;
                    this.GridView.Columns["SubstandardName"].IsVisible = false;
                    this.GridView.Columns["ReferenceCode"].IsVisible = false;
                    //this.GridView.Columns["Specification"].IsVisible = false;
                    this.GridView.Columns["EnglishName"].IsVisible = true;
                    this.GridView.Columns["PartTypeStr"].IsVisible = false;
                    this.GridView.Columns["MInPackingAmountStr"].IsVisible = false;
                    this.GridView.Columns["Feature"].IsVisible = false;
                    //this.GridView.Columns["NextSubstitute"].IsVisible = false;
                    this.GridView.Columns["IMSManufacturerNumber"].IsVisible = false;
                    //this.GridView.Columns["IMSCompressionNumber"].IsVisible = false;
                    //this.GridView.Columns["ProductBrand"].IsVisible = false;
                    this.GridView.Columns["ReferenceCode"].IsVisible = false;

                    //this.GridView.Columns["LastSubstitute"].IsVisible = false;
                    //this.GridView.Columns["CADCode"].IsVisible = false;
                    this.GridView.Columns["MeasureUnit"].IsVisible = false;
                    this.GridView.Columns["ShelfLife"].IsVisible = false;
                    //this.GridView.Columns["GroupABCCategory"].IsVisible = false;
                    this.GridView.Columns["Weight"].IsVisible = false;
                    this.GridView.Columns["Volume"].IsVisible = false;
                    //this.GridView.Columns["TotalNumber"].IsVisible = false;
                    //this.GridView.Columns["Factury"].IsVisible = false;
                    this.GridView.Columns["IsOriginal"].IsVisible = false;
                    //this.GridView.Columns["CategoryCode"].IsVisible = false;
                    //this.GridView.Columns["CategoryName"].IsVisible = false;
                    this.GridView.Columns["OverseasPartsFigure"].IsVisible = false;
                    this.GridView.Columns["GoldenTaxClassifyCode"].IsVisible = false;
                    this.GridView.Columns["ExchangeIdentification"].IsVisible = false;
                    this.GridView.Columns["PinyinCode"].IsVisible = false;
                    this.GridView.Columns["Material"].IsVisible = false;
                    this.GridView.Columns["DeclareElement"].IsVisible = false;
                    this.GridView.Columns["IsSupplierPutInStr"].IsVisible = false;
                    break;
                case "ReplacePartType":
                    this.GridView.Columns["Name"].IsVisible = false;
                    this.GridView.Columns["SubstandardName"].IsVisible = false;
                    this.GridView.Columns["ReferenceCode"].IsVisible = false;
                    //this.GridView.Columns["Specification"].IsVisible = false;
                    this.GridView.Columns["EnglishName"].IsVisible = false;
                    this.GridView.Columns["PartTypeStr"].IsVisible = true;
                    this.GridView.Columns["MInPackingAmountStr"].IsVisible = false;
                    this.GridView.Columns["Feature"].IsVisible = false;
                    //this.GridView.Columns["NextSubstitute"].IsVisible = false;
                    this.GridView.Columns["IMSManufacturerNumber"].IsVisible = false;
                    //this.GridView.Columns["IMSCompressionNumber"].IsVisible = false;
                    //this.GridView.Columns["ProductBrand"].IsVisible = false;


                    //this.GridView.Columns["LastSubstitute"].IsVisible = false;
                    //this.GridView.Columns["CADCode"].IsVisible = false;
                    this.GridView.Columns["MeasureUnit"].IsVisible = false;
                    this.GridView.Columns["ShelfLife"].IsVisible = false;
                    //this.GridView.Columns["GroupABCCategory"].IsVisible = false;
                    this.GridView.Columns["Weight"].IsVisible = false;
                    this.GridView.Columns["Volume"].IsVisible = false;
                    //this.GridView.Columns["TotalNumber"].IsVisible = false;
                    //this.GridView.Columns["Factury"].IsVisible = false;
                    this.GridView.Columns["IsOriginal"].IsVisible = false;
                    //this.GridView.Columns["CategoryCode"].IsVisible = false;
                    //this.GridView.Columns["CategoryName"].IsVisible = false;
                    this.GridView.Columns["OverseasPartsFigure"].IsVisible = false;
                    this.GridView.Columns["GoldenTaxClassifyCode"].IsVisible = false;
                    this.GridView.Columns["ExchangeIdentification"].IsVisible = false;
                    this.GridView.Columns["PinyinCode"].IsVisible = false;
                    this.GridView.Columns["Material"].IsVisible = false;
                    this.GridView.Columns["DeclareElement"].IsVisible = false;
                    this.GridView.Columns["IsSupplierPutInStr"].IsVisible = false;
                    break;
                case "ReplaceFeature":
                    this.GridView.Columns["Name"].IsVisible = false;
                    this.GridView.Columns["SubstandardName"].IsVisible = false;
                    this.GridView.Columns["ReferenceCode"].IsVisible = false;
                    //this.GridView.Columns["Specification"].IsVisible = false;
                    this.GridView.Columns["EnglishName"].IsVisible = false;
                    this.GridView.Columns["PartTypeStr"].IsVisible = false;
                    this.GridView.Columns["MInPackingAmountStr"].IsVisible = false;
                    this.GridView.Columns["Feature"].IsVisible = true;
                    //this.GridView.Columns["NextSubstitute"].IsVisible = false;
                    this.GridView.Columns["IMSManufacturerNumber"].IsVisible = false;
                    //this.GridView.Columns["IMSCompressionNumber"].IsVisible = false;
                    //this.GridView.Columns["ProductBrand"].IsVisible = false;


                    //this.GridView.Columns["LastSubstitute"].IsVisible = false;
                    //this.GridView.Columns["CADCode"].IsVisible = false;
                    this.GridView.Columns["MeasureUnit"].IsVisible = false;
                    this.GridView.Columns["ShelfLife"].IsVisible = false;
                    //this.GridView.Columns["GroupABCCategory"].IsVisible = false;
                    this.GridView.Columns["Weight"].IsVisible = false;
                    this.GridView.Columns["Volume"].IsVisible = false;
                    //this.GridView.Columns["TotalNumber"].IsVisible = false;
                    //this.GridView.Columns["Factury"].IsVisible = false;
                    this.GridView.Columns["IsOriginal"].IsVisible = false;
                    //this.GridView.Columns["CategoryCode"].IsVisible = false;
                    //this.GridView.Columns["CategoryName"].IsVisible = false;
                    this.GridView.Columns["OverseasPartsFigure"].IsVisible = false;
                    this.GridView.Columns["GoldenTaxClassifyCode"].IsVisible = false;
                    this.GridView.Columns["ExchangeIdentification"].IsVisible = false;
                    this.GridView.Columns["PinyinCode"].IsVisible = false;
                    this.GridView.Columns["Material"].IsVisible = false;
                    this.GridView.Columns["DeclareElement"].IsVisible = false;
                    this.GridView.Columns["IsSupplierPutInStr"].IsVisible = false;
                    break;
                case "ReplaceReferenceCode":
                    this.GridView.Columns["Name"].IsVisible = false;
                    this.GridView.Columns["SubstandardName"].IsVisible = false;
                    this.GridView.Columns["ReferenceCode"].IsVisible = true;
                    //this.GridView.Columns["Specification"].IsVisible = false;
                    this.GridView.Columns["EnglishName"].IsVisible = false;
                    this.GridView.Columns["PartTypeStr"].IsVisible = false;
                    this.GridView.Columns["MInPackingAmountStr"].IsVisible = false;
                    this.GridView.Columns["Feature"].IsVisible = false;
                    //this.GridView.Columns["NextSubstitute"].IsVisible = false;
                    this.GridView.Columns["IMSManufacturerNumber"].IsVisible = false;
                    //this.GridView.Columns["IMSCompressionNumber"].IsVisible = false;
                    //this.GridView.Columns["ProductBrand"].IsVisible = false;


                    //this.GridView.Columns["LastSubstitute"].IsVisible = false;
                    //this.GridView.Columns["CADCode"].IsVisible = false;
                    this.GridView.Columns["MeasureUnit"].IsVisible = false;
                    this.GridView.Columns["ShelfLife"].IsVisible = false;
                    //this.GridView.Columns["GroupABCCategory"].IsVisible = false;
                    this.GridView.Columns["Weight"].IsVisible = false;
                    this.GridView.Columns["Volume"].IsVisible = false;
                    //this.GridView.Columns["TotalNumber"].IsVisible = false;
                    //this.GridView.Columns["Factury"].IsVisible = false;
                    this.GridView.Columns["IsOriginal"].IsVisible = false;
                    //this.GridView.Columns["CategoryCode"].IsVisible = false;
                    //this.GridView.Columns["CategoryName"].IsVisible = false;
                    this.GridView.Columns["OverseasPartsFigure"].IsVisible = false;
                    this.GridView.Columns["GoldenTaxClassifyCode"].IsVisible = false;
                    this.GridView.Columns["ExchangeIdentification"].IsVisible = false;
                    //this.GridView.Columns["StandardCode"].IsVisible = false;
                    //this.GridView.Columns["StandardName"].IsVisible = false;
                    this.GridView.Columns["PinyinCode"].IsVisible = false;
                    this.GridView.Columns["Material"].IsVisible = false;
                    this.GridView.Columns["DeclareElement"].IsVisible = false;
                    this.GridView.Columns["IsSupplierPutInStr"].IsVisible = false;
                    break;
                case "ReplaceOverseasPartsFigure":
                    this.GridView.Columns["Name"].IsVisible = false;
                    this.GridView.Columns["SubstandardName"].IsVisible = false;
                    this.GridView.Columns["ReferenceCode"].IsVisible = false;
                    //this.GridView.Columns["Specification"].IsVisible = false;
                    this.GridView.Columns["EnglishName"].IsVisible = false;
                    this.GridView.Columns["PartTypeStr"].IsVisible = false;
                    this.GridView.Columns["MInPackingAmountStr"].IsVisible = false;
                    this.GridView.Columns["Feature"].IsVisible = false;
                    //this.GridView.Columns["NextSubstitute"].IsVisible = false;
                    this.GridView.Columns["IMSManufacturerNumber"].IsVisible = false;
                    //this.GridView.Columns["IMSCompressionNumber"].IsVisible = false;
                    //this.GridView.Columns["ProductBrand"].IsVisible = false;


                    //this.GridView.Columns["LastSubstitute"].IsVisible = false;
                    //this.GridView.Columns["CADCode"].IsVisible = false;
                    this.GridView.Columns["MeasureUnit"].IsVisible = false;
                    this.GridView.Columns["ShelfLife"].IsVisible = false;
                    //this.GridView.Columns["GroupABCCategory"].IsVisible = false;
                    this.GridView.Columns["Weight"].IsVisible = false;
                    this.GridView.Columns["Volume"].IsVisible = false;
                    //this.GridView.Columns["TotalNumber"].IsVisible = false;
                    //this.GridView.Columns["Factury"].IsVisible = false;
                    this.GridView.Columns["IsOriginal"].IsVisible = false;
                    //this.GridView.Columns["CategoryCode"].IsVisible = false;
                   // this.GridView.Columns["CategoryName"].IsVisible = false;
                    this.GridView.Columns["OverseasPartsFigure"].IsVisible = true;
                    this.GridView.Columns["GoldenTaxClassifyCode"].IsVisible = false;
                    this.GridView.Columns["ExchangeIdentification"].IsVisible = false;
                    this.GridView.Columns["PinyinCode"].IsVisible = false;
                    this.GridView.Columns["Material"].IsVisible = false;
                    this.GridView.Columns["DeclareElement"].IsVisible = false;
                    this.GridView.Columns["IsSupplierPutInStr"].IsVisible = false;
                    break;
                case "ReplaceMInPackingAmount":
                    this.GridView.Columns["Name"].IsVisible = false;
                    this.GridView.Columns["SubstandardName"].IsVisible = false;
                    this.GridView.Columns["ReferenceCode"].IsVisible = false;
                    //this.GridView.Columns["Specification"].IsVisible = false;
                    this.GridView.Columns["EnglishName"].IsVisible = false;
                    this.GridView.Columns["PartTypeStr"].IsVisible = false;
                    this.GridView.Columns["MInPackingAmountStr"].IsVisible = true;
                    this.GridView.Columns["Feature"].IsVisible = false;
                    //this.GridView.Columns["NextSubstitute"].IsVisible = false;
                    this.GridView.Columns["IMSManufacturerNumber"].IsVisible = false;
                    //this.GridView.Columns["IMSCompressionNumber"].IsVisible = false;
                    //this.GridView.Columns["ProductBrand"].IsVisible = false;


                    //this.GridView.Columns["LastSubstitute"].IsVisible = false;
                    //this.GridView.Columns["CADCode"].IsVisible = false;
                    this.GridView.Columns["MeasureUnit"].IsVisible = false;
                    this.GridView.Columns["ShelfLife"].IsVisible = false;
                    //this.GridView.Columns["GroupABCCategory"].IsVisible = false;
                    this.GridView.Columns["Weight"].IsVisible = false;
                    this.GridView.Columns["Volume"].IsVisible = false;
                    //this.GridView.Columns["TotalNumber"].IsVisible = false;
                    //this.GridView.Columns["Factury"].IsVisible = false;
                    this.GridView.Columns["IsOriginal"].IsVisible = false;
                    //this.GridView.Columns["CategoryCode"].IsVisible = false;
                    //this.GridView.Columns["CategoryName"].IsVisible = false;
                    this.GridView.Columns["OverseasPartsFigure"].IsVisible = false;
                    this.GridView.Columns["GoldenTaxClassifyCode"].IsVisible = false;
                    this.GridView.Columns["ExchangeIdentification"].IsVisible = false;
                    this.GridView.Columns["PinyinCode"].IsVisible = false;
                    this.GridView.Columns["Material"].IsVisible = false;
                    this.GridView.Columns["DeclareElement"].IsVisible = false;
                    this.GridView.Columns["IsSupplierPutInStr"].IsVisible = false;
                    break;
                case "ReplaceNextSubstitute":
                    this.GridView.Columns["Name"].IsVisible = false;
                    this.GridView.Columns["SubstandardName"].IsVisible = false;
                    this.GridView.Columns["ReferenceCode"].IsVisible = false;
                    //this.GridView.Columns["Specification"].IsVisible = false;
                    this.GridView.Columns["EnglishName"].IsVisible = false;
                    this.GridView.Columns["PartTypeStr"].IsVisible = false;
                    this.GridView.Columns["MInPackingAmountStr"].IsVisible = false;
                    this.GridView.Columns["Feature"].IsVisible = false;
                    //this.GridView.Columns["NextSubstitute"].IsVisible = true;
                    this.GridView.Columns["IMSManufacturerNumber"].IsVisible = false;
                    //this.GridView.Columns["IMSCompressionNumber"].IsVisible = false;
                    //this.GridView.Columns["ProductBrand"].IsVisible = false;

                    //this.GridView.Columns["LastSubstitute"].IsVisible = false;
                    //this.GridView.Columns["CADCode"].IsVisible = false;
                    this.GridView.Columns["MeasureUnit"].IsVisible = false;
                    this.GridView.Columns["ShelfLife"].IsVisible = false;
                    //this.GridView.Columns["GroupABCCategory"].IsVisible = false;
                    this.GridView.Columns["Weight"].IsVisible = false;
                    this.GridView.Columns["Volume"].IsVisible = false;
                    //this.GridView.Columns["TotalNumber"].IsVisible = false;
                    //this.GridView.Columns["Factury"].IsVisible = false;
                    this.GridView.Columns["IsOriginal"].IsVisible = false;
                    //this.GridView.Columns["CategoryCode"].IsVisible = false;
                    //this.GridView.Columns["CategoryName"].IsVisible = false;
                    this.GridView.Columns["OverseasPartsFigure"].IsVisible = false;
                    this.GridView.Columns["GoldenTaxClassifyCode"].IsVisible = false;
                    this.GridView.Columns["ExchangeIdentification"].IsVisible = false;
                    this.GridView.Columns["PinyinCode"].IsVisible = false;
                    this.GridView.Columns["Material"].IsVisible = false;
                    this.GridView.Columns["DeclareElement"].IsVisible = false;
                    this.GridView.Columns["IsSupplierPutInStr"].IsVisible = false;
                    break;
                case "ReplaceIMSCompressionNumber":
                    this.GridView.Columns["Name"].IsVisible = false;
                    this.GridView.Columns["SubstandardName"].IsVisible = false;
                    this.GridView.Columns["ReferenceCode"].IsVisible = false;
                    //this.GridView.Columns["Specification"].IsVisible = false;
                    this.GridView.Columns["EnglishName"].IsVisible = false;
                    this.GridView.Columns["PartTypeStr"].IsVisible = false;
                    this.GridView.Columns["MInPackingAmountStr"].IsVisible = false;
                    this.GridView.Columns["Feature"].IsVisible = false;
                    //this.GridView.Columns["NextSubstitute"].IsVisible = false;
                    this.GridView.Columns["IMSManufacturerNumber"].IsVisible = false;
                    //this.GridView.Columns["IMSCompressionNumber"].IsVisible = true;
                    //this.GridView.Columns["ProductBrand"].IsVisible = false;


                    //this.GridView.Columns["LastSubstitute"].IsVisible = false;
                    //this.GridView.Columns["CADCode"].IsVisible = false;
                    this.GridView.Columns["MeasureUnit"].IsVisible = false;
                    this.GridView.Columns["ShelfLife"].IsVisible = false;
                    //this.GridView.Columns["GroupABCCategory"].IsVisible = false;
                    this.GridView.Columns["Weight"].IsVisible = false;
                    this.GridView.Columns["Volume"].IsVisible = false;
                    //this.GridView.Columns["TotalNumber"].IsVisible = false;
                    //this.GridView.Columns["Factury"].IsVisible = false;
                    this.GridView.Columns["IsOriginal"].IsVisible = false;
                    //this.GridView.Columns["CategoryCode"].IsVisible = false;
                    //this.GridView.Columns["CategoryName"].IsVisible = false;
                    this.GridView.Columns["OverseasPartsFigure"].IsVisible = false;
                    this.GridView.Columns["GoldenTaxClassifyCode"].IsVisible = false;
                    this.GridView.Columns["ExchangeIdentification"].IsVisible = false;
                    this.GridView.Columns["PinyinCode"].IsVisible = false;
                    this.GridView.Columns["Material"].IsVisible = false;
                    this.GridView.Columns["DeclareElement"].IsVisible = false;
                    this.GridView.Columns["IsSupplierPutInStr"].IsVisible = false;
                    break;
                case "ReplaceIMSManufacturerNumber":
                    this.GridView.Columns["Name"].IsVisible = false;
                    this.GridView.Columns["SubstandardName"].IsVisible = false;
                    this.GridView.Columns["ReferenceCode"].IsVisible = false;
                    //this.GridView.Columns["Specification"].IsVisible = false;
                    this.GridView.Columns["EnglishName"].IsVisible = false;
                    this.GridView.Columns["PartTypeStr"].IsVisible = false;
                    this.GridView.Columns["MInPackingAmountStr"].IsVisible = false;
                    this.GridView.Columns["Feature"].IsVisible = false;
                    //this.GridView.Columns["NextSubstitute"].IsVisible = false;
                    //this.GridView.Columns["IMSCompressionNumber"].IsVisible = false;
                    this.GridView.Columns["IMSManufacturerNumber"].IsVisible = true;
                    //this.GridView.Columns["ProductBrand"].IsVisible = false;


                    //this.GridView.Columns["LastSubstitute"].IsVisible = false;
                    //this.GridView.Columns["CADCode"].IsVisible = false;
                    this.GridView.Columns["MeasureUnit"].IsVisible = false;
                    this.GridView.Columns["ShelfLife"].IsVisible = false;
                    //this.GridView.Columns["GroupABCCategory"].IsVisible = false;
                    this.GridView.Columns["Weight"].IsVisible = false;
                    this.GridView.Columns["Volume"].IsVisible = false;
                    //this.GridView.Columns["TotalNumber"].IsVisible = false;
                    //this.GridView.Columns["Factury"].IsVisible = false;
                    this.GridView.Columns["IsOriginal"].IsVisible = false;
                    //this.GridView.Columns["CategoryCode"].IsVisible = false;
                    //this.GridView.Columns["CategoryName"].IsVisible = false;
                    this.GridView.Columns["OverseasPartsFigure"].IsVisible = false;
                    this.GridView.Columns["GoldenTaxClassifyCode"].IsVisible = false;
                    this.GridView.Columns["ExchangeIdentification"].IsVisible = false;
                    this.GridView.Columns["PinyinCode"].IsVisible = false;
                    this.GridView.Columns["Material"].IsVisible = false;
                    this.GridView.Columns["DeclareElement"].IsVisible = false;
                    this.GridView.Columns["IsSupplierPutInStr"].IsVisible = false;
                    break;
                case "ReplaceProductBrand":
                    this.GridView.Columns["Name"].IsVisible = false;
                    this.GridView.Columns["SubstandardName"].IsVisible = false;
                    this.GridView.Columns["ReferenceCode"].IsVisible = false;
                    //this.GridView.Columns["Specification"].IsVisible = false;
                    this.GridView.Columns["EnglishName"].IsVisible = false;
                    this.GridView.Columns["PartTypeStr"].IsVisible = false;
                    this.GridView.Columns["MInPackingAmountStr"].IsVisible = false;
                    this.GridView.Columns["Feature"].IsVisible = false;
                    //this.GridView.Columns["NextSubstitute"].IsVisible = false;
                    //this.GridView.Columns["IMSCompressionNumber"].IsVisible = false;
                    this.GridView.Columns["IMSManufacturerNumber"].IsVisible = false;
                    //this.GridView.Columns["ProductBrand"].IsVisible = true;


                    //this.GridView.Columns["LastSubstitute"].IsVisible = false;
                    //this.GridView.Columns["CADCode"].IsVisible = false;
                    this.GridView.Columns["MeasureUnit"].IsVisible = false;
                    this.GridView.Columns["ShelfLife"].IsVisible = false;
                    //this.GridView.Columns["GroupABCCategory"].IsVisible = false;
                    this.GridView.Columns["Weight"].IsVisible = false;
                    this.GridView.Columns["Volume"].IsVisible = false;
                    //this.GridView.Columns["TotalNumber"].IsVisible = false;
                    //this.GridView.Columns["Factury"].IsVisible = false;
                    this.GridView.Columns["IsOriginal"].IsVisible = false;
                    //this.GridView.Columns["CategoryCode"].IsVisible = false;
                    //this.GridView.Columns["CategoryName"].IsVisible = false;
                    this.GridView.Columns["OverseasPartsFigure"].IsVisible = false;
                    this.GridView.Columns["GoldenTaxClassifyCode"].IsVisible = false;
                    this.GridView.Columns["ExchangeIdentification"].IsVisible = false;
                    this.GridView.Columns["PinyinCode"].IsVisible = false;
                    this.GridView.Columns["Material"].IsVisible = false;
                    this.GridView.Columns["DeclareElement"].IsVisible = false;
                    this.GridView.Columns["IsSupplierPutInStr"].IsVisible = false;
                    break;
                case "ReplaceStandardName":
                    this.GridView.Columns["Name"].IsVisible = true;
                    this.GridView.Columns["SubstandardName"].IsVisible = false;
                    this.GridView.Columns["ReferenceCode"].IsVisible = false;
                    //this.GridView.Columns["Specification"].IsVisible = false;
                    this.GridView.Columns["EnglishName"].IsVisible = false;
                    this.GridView.Columns["PartTypeStr"].IsVisible = false;
                    this.GridView.Columns["MInPackingAmountStr"].IsVisible = false;
                    this.GridView.Columns["Feature"].IsVisible = false;
                    //this.GridView.Columns["NextSubstitute"].IsVisible = false;
                    //this.GridView.Columns["IMSCompressionNumber"].IsVisible = false;
                    this.GridView.Columns["IMSManufacturerNumber"].IsVisible = false;
                    //this.GridView.Columns["ProductBrand"].IsVisible = false;


                    //this.GridView.Columns["LastSubstitute"].IsVisible = false;
                    //this.GridView.Columns["CADCode"].IsVisible = false;
                    this.GridView.Columns["MeasureUnit"].IsVisible = false;
                    this.GridView.Columns["ShelfLife"].IsVisible = false;
                    //this.GridView.Columns["GroupABCCategory"].IsVisible = false;
                    this.GridView.Columns["Weight"].IsVisible = false;
                    this.GridView.Columns["Volume"].IsVisible = false;
                    //this.GridView.Columns["TotalNumber"].IsVisible = false;
                    //this.GridView.Columns["Factury"].IsVisible = false;
                    this.GridView.Columns["IsOriginal"].IsVisible = false;
                    //this.GridView.Columns["CategoryCode"].IsVisible = false;
                    //this.GridView.Columns["CategoryName"].IsVisible = false;
                    this.GridView.Columns["OverseasPartsFigure"].IsVisible = false;
                    this.GridView.Columns["GoldenTaxClassifyCode"].IsVisible = false;
                    this.GridView.Columns["ExchangeIdentification"].IsVisible = false;
                    this.GridView.Columns["PinyinCode"].IsVisible = false;
                    this.GridView.Columns["Material"].IsVisible = false;
                    this.GridView.Columns["DeclareElement"].IsVisible = true;
                    this.GridView.Columns["IsSupplierPutInStr"].IsVisible = false;
                    break;
                case "ReplaceImportEdit":
                    this.GridView.Columns["Name"].IsVisible = true;
                    this.GridView.Columns["ReferenceCode"].IsVisible = true;
                    //this.GridView.Columns["ProductBrand"].IsVisible = true;
                    //this.GridView.Columns["IMSCompressionNumber"].IsVisible = true;
                    this.GridView.Columns["IMSManufacturerNumber"].IsVisible = true;
                    //this.GridView.Columns["CADCode"].IsVisible = true;
                    //this.GridView.Columns["Specification"].IsVisible = true;
                    this.GridView.Columns["EnglishName"].IsVisible = true;
                    this.GridView.Columns["PartTypeStr"].IsVisible = true;
                    this.GridView.Columns["MeasureUnit"].IsVisible = true;
                    this.GridView.Columns["MInPackingAmountStr"].IsVisible = true;
                    this.GridView.Columns["ShelfLife"].IsVisible = true;
                    //this.GridView.Columns["GroupABCCategory"].IsVisible = true;
                    //this.GridView.Columns["LastSubstitute"].IsVisible = true;
                    //this.GridView.Columns["NextSubstitute"].IsVisible = true;
                    this.GridView.Columns["Weight"].IsVisible = true;
                    this.GridView.Columns["Volume"].IsVisible = true;
                    this.GridView.Columns["Feature"].IsVisible = true;
                    //this.GridView.Columns["TotalNumber"].IsVisible = true;
                    //this.GridView.Columns["Factury"].IsVisible = true;
                    this.GridView.Columns["IsOriginal"].IsVisible = true;
                    //this.GridView.Columns["CategoryCode"].IsVisible = true;
                    //this.GridView.Columns["CategoryName"].IsVisible = true;
                    this.GridView.Columns["SubstandardName"].IsVisible = false;
                    this.GridView.Columns["OverseasPartsFigure"].IsVisible = true;
                    this.GridView.Columns["GoldenTaxClassifyCode"].IsVisible = false;
                    this.GridView.Columns["ExchangeIdentification"].IsVisible = false;
                    this.GridView.Columns["PinyinCode"].IsVisible = false;
                    this.GridView.Columns["Material"].IsVisible = false;
                    this.GridView.Columns["DeclareElement"].IsVisible = false;
                    this.GridView.Columns["IsSupplierPutInStr"].IsVisible = false;
                    break;
                case "ImportGoldenTaxClassify":
                    this.GridView.Columns["Name"].IsVisible = true;
                    this.GridView.Columns["ReferenceCode"].IsVisible = false;
                    //this.GridView.Columns["ProductBrand"].IsVisible = true;
                    //this.GridView.Columns["IMSCompressionNumber"].IsVisible = true;
                    this.GridView.Columns["IMSManufacturerNumber"].IsVisible = false;
                    //this.GridView.Columns["CADCode"].IsVisible = true;
                    //this.GridView.Columns["Specification"].IsVisible = true;
                    this.GridView.Columns["EnglishName"].IsVisible = false;
                    this.GridView.Columns["PartTypeStr"].IsVisible = false;
                    this.GridView.Columns["MeasureUnit"].IsVisible = false;
                    this.GridView.Columns["MInPackingAmountStr"].IsVisible = false;
                    this.GridView.Columns["ShelfLife"].IsVisible = false;
                    //this.GridView.Columns["GroupABCCategory"].IsVisible = true;
                    //this.GridView.Columns["LastSubstitute"].IsVisible = true;
                    //this.GridView.Columns["NextSubstitute"].IsVisible = true;
                    this.GridView.Columns["Weight"].IsVisible = false;
                    this.GridView.Columns["Volume"].IsVisible = false;
                    this.GridView.Columns["Feature"].IsVisible = false;
                    //this.GridView.Columns["TotalNumber"].IsVisible = true;
                    //this.GridView.Columns["Factury"].IsVisible = true;
                    this.GridView.Columns["IsOriginal"].IsVisible = false;
                    //this.GridView.Columns["CategoryCode"].IsVisible = true;
                    //this.GridView.Columns["CategoryName"].IsVisible = true;
                    this.GridView.Columns["SubstandardName"].IsVisible = false;
                    this.GridView.Columns["OverseasPartsFigure"].IsVisible = false;
                    this.GridView.Columns["GoldenTaxClassifyCode"].IsVisible = true;
                    this.GridView.Columns["ExchangeIdentification"].IsVisible = false;
                    this.GridView.Columns["PinyinCode"].IsVisible = false;
                    this.GridView.Columns["Material"].IsVisible = false;
                    this.GridView.Columns["DeclareElement"].IsVisible = false;
                    this.GridView.Columns["IsSupplierPutInStr"].IsVisible = false;
                    break;

                case "ImportExchangeIdentification":
                    this.GridView.Columns["Name"].IsVisible = true;
                    this.GridView.Columns["SubstandardName"].IsVisible = false;
                    this.GridView.Columns["ReferenceCode"].IsVisible = false;
                    //this.GridView.Columns["Specification"].IsVisible = false;
                    this.GridView.Columns["EnglishName"].IsVisible = false;
                    this.GridView.Columns["PartTypeStr"].IsVisible = false;
                    this.GridView.Columns["MInPackingAmountStr"].IsVisible = false;
                    this.GridView.Columns["Feature"].IsVisible = false;
                    //this.GridView.Columns["NextSubstitute"].IsVisible = false;
                    this.GridView.Columns["IMSManufacturerNumber"].IsVisible = false;
                    //this.GridView.Columns["IMSCompressionNumber"].IsVisible = false;
                    //this.GridView.Columns["ProductBrand"].IsVisible = false;
                    //this.GridView.Columns["LastSubstitute"].IsVisible = false;
                    //this.GridView.Columns["CADCode"].IsVisible = false;
                    this.GridView.Columns["MeasureUnit"].IsVisible = false;
                    this.GridView.Columns["ShelfLife"].IsVisible = false;
                    //this.GridView.Columns["GroupABCCategory"].IsVisible = false;
                    this.GridView.Columns["Weight"].IsVisible = false;
                    this.GridView.Columns["Volume"].IsVisible = false;
                    //this.GridView.Columns["TotalNumber"].IsVisible = false;
                    //this.GridView.Columns["Factury"].IsVisible = false;
                    this.GridView.Columns["IsOriginal"].IsVisible = false;
                    //this.GridView.Columns["CategoryCode"].IsVisible = false;
                    //this.GridView.Columns["CategoryName"].IsVisible = false;
                    this.GridView.Columns["OverseasPartsFigure"].IsVisible = false;
                    this.GridView.Columns["GoldenTaxClassifyCode"].IsVisible = false;
                    //this.GridView.Columns["StandardCode"].IsVisible = false;
                    //this.GridView.Columns["StandardName"].IsVisible = false;
                    this.GridView.Columns["PinyinCode"].IsVisible = false;
                    this.GridView.Columns["Material"].IsVisible = false;
                    this.GridView.Columns["DeclareElement"].IsVisible = false;
                    this.GridView.Columns["IsSupplierPutInStr"].IsVisible = false;
                    break;
                case "ImportIsSupplier":
                    this.GridView.Columns["Name"].IsVisible = true;
                    this.GridView.Columns["ReferenceCode"].IsVisible = false;
                    this.GridView.Columns["IMSManufacturerNumber"].IsVisible = false;            
                    this.GridView.Columns["EnglishName"].IsVisible = false;
                    this.GridView.Columns["PartTypeStr"].IsVisible = false;
                    this.GridView.Columns["MeasureUnit"].IsVisible = false;
                    this.GridView.Columns["MInPackingAmountStr"].IsVisible = false;
                    this.GridView.Columns["ShelfLife"].IsVisible = false;
                    this.GridView.Columns["Weight"].IsVisible = false;
                    this.GridView.Columns["Volume"].IsVisible = false;
                    this.GridView.Columns["Feature"].IsVisible = false;
                    this.GridView.Columns["IsOriginal"].IsVisible = false;
                    this.GridView.Columns["SubstandardName"].IsVisible = false;
                    this.GridView.Columns["OverseasPartsFigure"].IsVisible = false;
                    this.GridView.Columns["GoldenTaxClassifyCode"].IsVisible = false;
                    this.GridView.Columns["ExchangeIdentification"].IsVisible = false;
                    this.GridView.Columns["PinyinCode"].IsVisible = false;
                    this.GridView.Columns["Material"].IsVisible = false;
                    this.GridView.Columns["DeclareElement"].IsVisible = false;
                    this.GridView.Columns["IsSupplierPutInStr"].IsVisible = true;
                    break;
                case "ReplaceTraceProperty":
                     this.GridView.Columns["Name"].IsVisible = false;
                    this.GridView.Columns["SubstandardName"].IsVisible = false;
                    this.GridView.Columns["ReferenceCode"].IsVisible = false;
                    this.GridView.Columns["EnglishName"].IsVisible = false;
                    this.GridView.Columns["PartTypeStr"].IsVisible = false;
                    this.GridView.Columns["MInPackingAmountStr"].IsVisible = false;
                    this.GridView.Columns["Feature"].IsVisible = false;
                    this.GridView.Columns["IMSManufacturerNumber"].IsVisible = false;
                    this.GridView.Columns["MeasureUnit"].IsVisible = false;
                    this.GridView.Columns["ShelfLife"].IsVisible = false;
                    this.GridView.Columns["Weight"].IsVisible = false;
                    this.GridView.Columns["Volume"].IsVisible = false;
                    this.GridView.Columns["IsOriginal"].IsVisible = false;
                    this.GridView.Columns["OverseasPartsFigure"].IsVisible = false;
                    this.GridView.Columns["GoldenTaxClassifyCode"].IsVisible = false;
                    this.GridView.Columns["ExchangeIdentification"].IsVisible = false;
                    this.GridView.Columns["PinyinCode"].IsVisible = false;
                    this.GridView.Columns["Material"].IsVisible = false;
                    this.GridView.Columns["DeclareElement"].IsVisible = false;
                    this.GridView.Columns["IsSupplierPutInStr"].IsVisible = false;
                    this.GridView.Columns["TracePropertyStr"].IsVisible = true;
                    break;
            }
            return null;
        }
        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SparePartExtend);
            }
        }
    }
}
