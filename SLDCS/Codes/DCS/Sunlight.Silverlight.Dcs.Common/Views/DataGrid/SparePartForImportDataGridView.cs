﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class SparePartForImportDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        Name = "Code"
                    }, new ColumnItem {
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                        Name = "Name"
                    }, new ColumnItem {
                        Name = "RreferenceCode",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_ReferenceCode
                    }, new ColumnItem {
                        Name = "ExchangeIdentification",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsExchangeGroup_ExchangeCode
                    }, new ColumnItem {
                        Name = "GoldenTaxClassifyCode",
                        Title= CommonUIStrings.QueryPanel_QueryItem_Title_GoldenTaxClassifyMsg_GoldenTaxClassifyCode,
                    },new ColumnItem{
                        Name="GoldenTaxClassifyName",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_GoldenTaxClassifyMsg_GoldenTaxClassifyName
                   }, new ColumnItem{
                        Name="OverseasPartsFigure",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_OverseasPartsFigure
                    },
                    new ColumnItem{
                        Name = "ProductBrand", 
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsBranch_SparePartProductBrand,
                    },new ColumnItem {
                        Name = "IMSCompressionNumber",
                        Title=CommonUIStrings.DataEditPanel_Text_SparePartForBatch_IMSCompressionNumber
                    },new ColumnItem {
                        Name = "IMSManufacturerNumber",
                        Title=CommonUIStrings.DataEditPanel_Text_SparePartForBatch_IMSManufacturer
                    },new ColumnItem {
                        Name = "CADCode",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_SparePart_CADCode
                    },new ColumnItem{
                        Name="Specification",
                        Title = Utils.GetEntityLocalizedName(typeof(SparePart), "Specification"),
                    },new ColumnItem{
                        Name="EnglishName",
                        Title = Utils.GetEntityLocalizedName(typeof(SparePart), "EnglishName"),
                    }, new ColumnItem {
                        Title = Utils.GetEntityLocalizedName(typeof(SparePart), "PartType"),
                        Name = "PartTypeStr"
                    }, new ColumnItem {
                        Title = Utils.GetEntityLocalizedName(typeof(SparePart), "MeasureUnit"),
                        Name = "MeasureUnit"
                    },new ColumnItem {
                        Name = "MInPackingAmount",
                        Title=CommonUIStrings.DataEditPanel_Text_SparePartForBatch_MInPackingAmount
                    }, new ColumnItem {
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_SparePart_ShelfLife,
                        Name = "ShelfLife"
                    },new ColumnItem {
                        Name = "GroupABCCategory",
                        Title=CommonUIStrings.DataEditPanel_Validation_SparePartForBatch_GroupABCCategory
                    }, new ColumnItem {
                        Title = Utils.GetEntityLocalizedName(typeof(SparePart), "LastSubstitute"),
                        Name = "LastSubstitute"
                    }, new ColumnItem {
                        Title = Utils.GetEntityLocalizedName(typeof(SparePart), "NextSubstitute"),
                        Name = "NextSubstitute"
                    }, new ColumnItem {
                        Title = Utils.GetEntityLocalizedName(typeof(SparePart), "Weight"),
                        Name = "Weight",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric
                    }, new ColumnItem {
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_Volume,
                        Name = "Volume",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric
                    }, new ColumnItem {
                        Title =CommonUIStrings.DataEditPanel_Text_SparePart_Feature,
                        Name = "Feature"
                   },new ColumnItem {
                        Name = "TotalNumber",
                        Title=CommonUIStrings.DataEditPanel_Validation_SparePartForBatch_Assembly
                    }, new ColumnItem {
                        Name = "Factury",
                        Title=CommonUIStrings.DataEditPanel_Text_SparePartForBatch_Factury
                    }, new ColumnItem {
                        Name = "IsOriginal",
                        Title=CommonUIStrings.DataEditPanel_Text_SparePart_IsOriginal
                    }, new ColumnItem {
                        Name = "IsNotWarrantyTransfer",
                        Title=CommonUIStrings.DataEditPanel_Text_SparePartForBatch_IsTransfer
                    }, new ColumnItem {
                        Name = "CategoryCode",
                        Title=CommonUIStrings.DataEditPanel_Text_SparePartForBatch_CategoryCode
                    },new ColumnItem{
                        Name="CategoryName",
                        Title=CommonUIStrings.DataEditPanel_Text_SparePartForBatch_CategoryName
                   }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SparePartExtend);
            }
        }
    }
}
