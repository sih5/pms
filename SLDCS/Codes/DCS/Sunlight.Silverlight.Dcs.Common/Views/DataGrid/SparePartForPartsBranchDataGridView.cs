﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class SparePartForPartsBranchDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "MasterData_Status", "SparePart_PartType"
        };

        public SparePartForPartsBranchDataGridView() {
            this.KeyValueManager.Register(kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(SparePart);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Code",
                        IsSortDescending = false
                    }, new ColumnItem {
                        Name = "Name"
                    },new ColumnItem{
                        Name="ReferenceCode",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_PartsBranch_ReferenceCode
                    }, new ColumnItem {
                        Name = "CADCode",
                        Title=CommonUIStrings.DataGridView_ColumnItem_Title_SparePart_CADCode
                    },  new KeyValuesColumnItem {
                        Name = "PartType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Name = "MeasureUnit"
                    }, new ColumnItem {
                        Name = "Specification"
                    }, new ColumnItem {
                        Name = "Feature"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetSparePartForPartsBranchByDirectSupply";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Multiple;
        }
    }
}
