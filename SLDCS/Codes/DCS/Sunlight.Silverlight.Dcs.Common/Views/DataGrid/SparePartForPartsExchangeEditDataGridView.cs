﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class SparePartForPartsExchangeEditDataGridView : DcsDataGridViewBase {

        private DcsMultiPopupsQueryWindowBase sparePartMultiSelectQueryWindow;
        private QueryWindowBase sparePartDropDownQueryWindow;
        private RadWindow radQueryWindow;
        private RadWindow RadQueryWindow {
            get {
                this.radQueryWindow = new RadWindow {
                    Content = this.SparePartMultiSelectQueryWindow,
                    Header = CommonUIStrings.QueryPanel_Title_SparePart,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen
                };
                return this.radQueryWindow;
            }
        }
        private DcsMultiPopupsQueryWindowBase SparePartMultiSelectQueryWindow {
            get {
                this.sparePartMultiSelectQueryWindow = DI.GetQueryWindow("PartsExchangeSparePartMultiSelect") as DcsMultiPopupsQueryWindowBase;
                this.sparePartMultiSelectQueryWindow.SelectionDecided += sparePartMultiSelectQueryWindow_SelectionDecided;
                return this.sparePartMultiSelectQueryWindow;
            }
        }

        private QueryWindowBase SparePartDropDownQueryWindow {
            get {
                if(this.sparePartDropDownQueryWindow == null) {
                    this.sparePartDropDownQueryWindow = DI.GetQueryWindow("PartsExchangeSparePartDropDown");
                    this.sparePartDropDownQueryWindow.SelectionDecided += sparePartDropDownQueryWindow_SelectionDecided;
                }
                return this.sparePartDropDownQueryWindow;
            }
        }

        private void sparePartDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var sparePart = queryWindow.SelectedEntities.Cast<VirtualPartsBranch>().FirstOrDefault();
            var domainContext = this.DomainContext as DcsDomainContext;
            var dataEditView = this.DataContext as PartsExchangeDataEditView;
            var partsExchange = queryWindow.DataContext as PartsExchange;
            if(partsExchange == null || domainContext == null || sparePart == null || dataEditView == null)
                return;
            if(dataEditView.PartsExchanges.Any(r => sparePart.Id == r.PartId)) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_PartsExchangeHistories_SparePartIdIsAlreadyHave);
                return;
            }
            partsExchange.PartId = sparePart.PartId;
            domainContext.Load(domainContext.GetSparePartsQuery().Where(r => r.Id == partsExchange.PartId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled) {
                        loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                }
                var itemSparePart = loadOp.Entities.SingleOrDefault();
                if(itemSparePart != null)
                    partsExchange.SparePart = itemSparePart;
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }, null);
        }

        private void sparePartMultiSelectQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            var dataEditView = this.DataContext as PartsExchangeDataEditView;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var sparePart = queryWindow.SelectedEntities.Cast<VirtualPartsBranch>();
            if(sparePart == null || dataEditView == null)
                return;
            if(dataEditView.PartsExchanges.Any(r => sparePart.Any(s => s.Id == r.PartId))) {
                UIHelper.ShowNotification(CommonUIStrings.DataEditView_Validation_PartsExchangeHistories_SparePartIdIsAlreadyHave);
                return;
            }
            var domainContext = this.DomainContext as DcsDomainContext;
            var sparePartIds = sparePart.Select(r => r.PartId).Distinct().ToArray();
            domainContext.Load(domainContext.GetSparePartsByIdsQuery(sparePartIds), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled) {
                        loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                }
                var spareParts = loadOp.Entities.ToArray();
                foreach(var spare in sparePart) {
                    var serialNumber = dataEditView.PartsExchanges.Count();
                    serialNumber++;
                    var sp = spareParts.SingleOrDefault(r => r.Id == spare.PartId);
                    if(sp == null)
                        continue;
                    var parts = new PartsExchange {
                        SequeueNumber = serialNumber,
                        PartId = sp.Id,
                        SparePart = sp,
                        PartsSalesCategoryName = spare.PartsSalesCategoryName,
                        SalesPrice = spare.SalesPrice,
                        RetailGuidePrice = spare.RetailGuidePrice,
                        ExchangeCode = GlobalVar.ASSIGNED_BY_SERVER,
                        Status = (int)DcsBaseDataStatus.有效
                    };
                    dataEditView.PartsExchanges.Add(parts);
                }
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }, null);


        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var maxSerialNumber = this.GridView.Items.Count > 0 ? this.GridView.Items.Cast<PartsExchange>().Max(entity => entity.SequeueNumber) + 1 : 1;
            e.NewObject = new PartsExchange {
                SequeueNumber = maxSerialNumber
            };
            e.Cancel = true;
            RadQueryWindow.ShowDialog();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem{
                        Name = "SequeueNumber",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_SequeueNumber,
                        IsReadOnly = true
                    },new DropDownTextBoxColumnItem {
                        Name = "SparePart.Code",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        DropDownContent = this.SparePartDropDownQueryWindow,
                        IsEditable = false
                    },new ColumnItem {
                        Name = "SparePart.Name",
                        IsReadOnly=true
                    }, 
                    new ColumnItem{
                     Name = "PartsSalesCategoryName",
                     Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name
                    },
                    new ColumnItem{
                      Name = "SalesPrice",
                      Title = CommonUIStrings.DataGridView_Title_VirtualPartsBranch_SalesPrice
                    },
                    new ColumnItem{
                      Name = "RetailGuidePrice",
                      Title = CommonUIStrings.DataGridView_Title_VirtualPartsBranch_RetailGuidePrice
                    },
                    new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsExchange);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsExchanges");
        }

        protected override void OnControlsCreated() {
            this.GridView.AddingNewDataItem -= GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
            this.GridView.Deleting -= GridView_Deleting;
            this.GridView.Deleting += GridView_Deleting;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
        }

        private void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            var domainContext = this.DomainContext as DcsDomainContext;
            var dataEditView = this.DataContext as PartsExchangeDataEditView;
            if(domainContext == null || dataEditView == null)
                return;
            if(dataEditView.status == 1) {
                e.Cancel = true;
                if(dataEditView.OldPartsExchanges.Contains(e.Items.Cast<PartsExchange>().First())) {
                    DcsUtils.Confirm("是否作废所选单据？", () => {
                        if(dataEditView.PartsExchanges.Contains(e.Items.Cast<PartsExchange>().First())) {
                            dataEditView.PartsExchanges.Remove(e.Items.Cast<PartsExchange>().First());
                            var serialNumber = 1;
                            foreach(var partsExchange1 in dataEditView.PartsExchanges) {
                                partsExchange1.SequeueNumber = serialNumber;
                                serialNumber++;
                            }
                        }
                        dataEditView.OldPartsExchanges.Remove(e.Items.Cast<PartsExchange>().First());
                        var partsExchange = e.Items.Cast<PartsExchange>().First() as PartsExchange;
                        if(domainContext != null) {
                            if(partsExchange.Can作废配件互换信息)
                                partsExchange.作废配件互换信息();
                            this.DomainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    this.DomainContext.RejectChanges();
                                }
                            }, null);
                        }

                    });
                } else {
                    if(dataEditView.PartsExchanges.Contains(e.Items.Cast<PartsExchange>().First())) {
                        dataEditView.PartsExchanges.Remove(e.Items.Cast<PartsExchange>().First());
                        var serialNumber = 1;
                        foreach(var partsExchange1 in dataEditView.PartsExchanges) {
                            partsExchange1.SequeueNumber = serialNumber;
                            serialNumber++;
                        }
                    }
                }
            } else {
                if(dataEditView.PartsExchanges.Contains(e.Items.Cast<PartsExchange>().First())) {
                    dataEditView.PartsExchanges.Remove(e.Items.Cast<PartsExchange>().First());
                    var serialNumber = 1;
                    foreach(var partsExchange1 in dataEditView.PartsExchanges) {
                        partsExchange1.SequeueNumber = serialNumber;
                        serialNumber++;
                    }
                }
            }
        }
    }
}
