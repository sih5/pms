﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;
using System.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class SparePartPurchasePricingDataGridView : DcsDataGridViewBase {
        protected override string OnRequestQueryName() {
            return "getPartsPurchasePricingLists";
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "PartCreateTime",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_PartCreateTime,
                    },
                    new ColumnItem {
                        Name = "Code",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code
                    }, new ColumnItem {
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                        Name = "Name"
                    }, new ColumnItem {
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                        Name = "ReferenceCode"
                    }, new ColumnItem {
                        Name = "CompanyCode",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierCode,
                    }, new ColumnItem {
                        Name = "CompanyName",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierName,
                    }, new ColumnItem {
                        Name = "Purchaseprice",
                        Title = CommonUIStrings.DataGridView_Title_VirtualFactoryPurchacePrice_ContractPrice
                    }, new ColumnItem {
                        Name = "Createtime",
                        Title=CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_PCreateTime,
                    }, new ColumnItem {
                        Name = "Isprice",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Isprice,
                    }, new ColumnItem {
                        Name = "Isprimary",
                        Title = CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_IsPrimaryNew
                    },new ColumnItem{
                        Name = "ValidTo",
                        Title = CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_ValidFrom
                    },new ColumnItem{
                        Name = "CreatorName",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_CreatorName
                    },new ColumnItem{
                        Name = "ModifierName",
                        Title = CommonUIStrings.DataDeTailPanel_Text_BottomStock_ModifierName
                    },new ColumnItem{
                        Name = "ModifyTime",
                         Title=CommonUIStrings.QueryPanel_QueryItem_Title_BottomStock_ModifyTime
                    }
                };
            }
        }
        protected override Type EntityType {
            get {
                return typeof(PartsPurchasePricingForQuery);
            }
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem) && r.MemberName != "Code").Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "code":
                        var codes = filters.Filters.SingleOrDefault(s => s.MemberName == "Code");
                        if(codes == null || codes.Value == null)  
                            return null;  
                        return codes.Value.ToString();  
                    case "name":
                        return filters.Filters.Single(item => item.MemberName == "Name").Value;
                    case "referenceCode":
                        return filters.Filters.Single(item => item.MemberName == "ReferenceCode").Value;
                    case "isprice":
                        return filters.Filters.Single(item => item.MemberName == "Isprice").Value;                  
                    case "bCreateTime":
                        var createTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return createTime == null ? null : createTime.Filters.First(r => r.MemberName == "CreateTime").Value;
                    case "eCreateTime":
                        var createTime1 = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return createTime1 == null ? null : createTime1.Filters.Last(item => item.MemberName == "CreateTime").Value;   
                    case "bPartCreateTime":
                        var partCreateTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "PartCreateTime");
                        return partCreateTime == null ? null : partCreateTime.Filters.First(r => r.MemberName == "PartCreateTime").Value;
                    case "ePartCreateTime":
                        var partCreateTime1 = composites.FirstOrDefault(p => p.Filters.First().MemberName == "PartCreateTime");
                        return partCreateTime1 == null ? null : partCreateTime1.Filters.Last(item => item.MemberName == "PartCreateTime").Value;       
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            base.OnControlsCreated();    
        }
    }
}
