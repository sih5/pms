﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;
using System.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class SparePartWithDetailsDataGridView : DcsDataGridViewBase {

        private readonly string[] kvNames = new[] {
            "MasterData_Status", "SparePart_PartType","ABCGroupCategory","IsOrNot","SparePart_KeyCode","TraceProperty"
        };

        public SparePartWithDetailsDataGridView() {
            this.KeyValueManager.Register(kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_SparePart_Code
                    },
                    new ColumnItem {
                        Name = "Name"
                    },
                    new ColumnItem {
                        Name = "IsOrderable",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_VirtualPartsBranch_IsOrderable
                    },
                    new ColumnItem {
                        Name = "IsSalable",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_VirtualPartsBranch_IsSalable
                    },
                    new ColumnItem {
                        Name = "ReferenceCode",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_Agency_ReferenceCode,
                    },
                    //new ColumnItem {
                    //    Name = "ExchangeIdentification",
                    //    Title = "互换识别号"
                    //},
                    //new ColumnItem {
                    //    Name = "ExGroupCode",
                    //    Title = "互换号"
                    //},
                    //new ColumnItem {
                    //    Name = "OverseasPartsFigure",
                    //    Title = "海外配件图号"
                    //},
                    //new ColumnItem {
                    //    Name = "ProductBrand",
                    //    Title = "产品商标"
                    //},
                    //new ColumnItem {
                    //    Name = "IMSCompressionNumber",
                    //    Title = "IMS压缩号"
                    //},
                    //new ColumnItem {
                    //    Name = "IMSManufacturerNumber",
                    //    Title = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_IMSManufacturer
                    //},
                    new ColumnItem {
                        Name = "GoldenTaxClassifyCode",
                        Title =  CommonUIStrings.QueryPanel_QueryItem_Title_GoldenTaxClassifyMsg_GoldenTaxClassifyCode,
                    },
                    new ColumnItem {
                        Name = "GoldenTaxClassifyName",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_GoldenTaxClassifyMsg_GoldenTaxClassifyName
                    },
                    //new ColumnItem {
                    //    Name = "StandardCode",
                    //},
                    //new ColumnItem {
                    //    Name = "StandardName",
                    //},
                    //new ColumnItem {
                    //    Name = "CADCode",
                    //    Title = CommonUIStrings.DataGridView_ColumnItem_Title_SparePart_CADCode
                    //},
                    //new ColumnItem {
                    //    Name = "Specification"
                    //},
                    new ColumnItem {
                        Name = "EnglishName"
                    },
                    //new KeyValuesColumnItem {
                    //    Name = "OMSparePartMark",
                    //    KeyValueItems = this.KeyValueManager[this.kvNames[3]],
                    //    Title = "欧曼配件标记"
                    //},
                    new KeyValuesColumnItem {
                        Name = "PartType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },
                    new ColumnItem {
                        Name = "MeasureUnit"
                    },
                    new ColumnItem {
                        Name = "MInPackingAmount",
                       Title=CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_MInPackingAmount,
                    },
                    //new ColumnItem {
                    //    Name = "ShelfLife",
                    //    Title = CommonUIStrings.DataGridView_ColumnItem_Title_SparePart_ShelfLife
                    //},
                    //new KeyValuesColumnItem {
                    //    Name = "GroupABCCategory",
                    //    KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    //},
                    //new ColumnItem {
                    //    Name = "LastSubstitute"
                    //},
                    //new ColumnItem {
                    //    Name = "NextSubstitute"
                    //},
                    new ColumnItem {
                        Name = "Weight"
                    },
                    new ColumnItem {
                        Name = "Volume"
                    },
                    new ColumnItem {
                        Name = "Feature",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_SparePart_Feature
                    },
                    //new ColumnItem {
                    //    Name = "TotalNumber",
                    //    Title = CommonUIStrings.DataEditPanel_Validation_SparePartForBatch_Assembly
                    //},
                    //new ColumnItem {
                    //    Name = "Factury",
                    //    Title = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_Factury
                    //},
                    new KeyValuesColumnItem {
                        Name = "IsOriginal",
                        KeyValueItems = this.KeyValueManager[this.kvNames[3]],
                        Title = CommonUIStrings.DataEditPanel_Text_SparePart_IsOriginal
                    },
                    //new ColumnItem {
                    //    Name = "IsNotWarrantyTransfer",
                    //    Title = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_IsTransfer
                    //},
                    //new ColumnItem {
                    //    Name = "CategoryCode",
                    //    Title = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_CategoryCode
                    //},
                    //new ColumnItem {
                    //    Name = "CategoryName",
                    //    Title = CommonUIStrings.DataEditPanel_Text_SparePartForBatch_CategoryName
                    //},
                     new ColumnItem {
                        Name = "PinyinCode",
                        Title = CommonUIStrings.DataEditPanel_Text_SparePart_PinyinCode
                    },
                    new ColumnItem {
                        Name = "Material",
                        Title = CommonUIStrings.DataEditPanel_Text_SparePart_Material
                    },
                     new ColumnItem {
                        Name = "DeclareElement",
                        Title = CommonUIStrings.DataEditPanel_Text_SparePart_DeclareElement
                    },
                     new KeyValuesColumnItem {
                        Name = "AssemblyKeyCode",
                        Title = CommonUIStrings.DataEditPanel_Text_SparePart_AssemblyKeyCode,
                        KeyValueItems = this.KeyValueManager[this.kvNames[4]]
                    },
                    new KeyValuesColumnItem {
                        Name = "TraceProperty",
                        Title="追溯属性",
                        KeyValueItems = this.KeyValueManager[this.kvNames[5]]
                    },
                     new ColumnItem {
                        Name = "IsSupplierPutIn",
                        Title = CommonUIStrings.DataEditPanel_Title_ImportIsSupplierPutIn
                    },
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },
                     new ColumnItem {
                        Name = "SafeDays",
                        Title = "安全天数最大值"
                    },
                     new ColumnItem {
                        Name = "WarehousDays",
                        Title = "库房天数"
                    },
                     new ColumnItem {
                        Name = "TemDays",
                        Title = "临时天数"
                    },
                    new ColumnItem {
                        Name = "CreatorName"
                    },
                    new ColumnItem {
                        Name = "CreateTime"
                    },
                    new ColumnItem {
                        Name = "ModifierName"
                    },
                    new ColumnItem {
                        Name = "ModifyTime"
                    },
                    new ColumnItem {
                        Name = "AbandonerName"
                    },
                    new ColumnItem {
                        Name = "AbandonTime"
                    }
                };
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsSupplierWithRelation", "PartsReplacementForSparePart", "PartsBranchForSparePart", "CombinedPart","PartsExchangeForSparePart"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetSparePartWithPartsExchangeGroup";
        }

        protected override Type EntityType {
            get {
                return typeof(SparePart);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filterItem = this.FilterItem as CompositeFilterItem;
            if(filterItem == null)
                return null;
            switch(parameterName) {
                case "exGroupCode":
                    //var exGroupCodeFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "ExGroupCode");
                    //return exGroupCodeFilterItem.Value;
                    return null;
                case "codes":
                    var codes = filterItem.Filters.SingleOrDefault(s => s.MemberName == "Code"); 
                    if(codes == null || codes.Value == null) 
                        return null; 
                    return codes.Value.ToString(); 
                default:
                    return null;
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                foreach(var filterItem in compositeFilterItem.Filters.Where(filter => filter.MemberName != "ExGroupCode")) {
                    newCompositeFilterItem.Filters.Add(filterItem);
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }
}
