﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class SupplierSparePartQueryDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name = "SupplierPartCode",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsSupplierRelation_SupplierPartCode,
                    },new ColumnItem{
                        Name = "SparePartName",
                        Title=CommonUIStrings.Query_QueryItem_Title_PartsExchange_SparePartName,
                    },new ColumnItem{
                        Name = "ReferenceCode",
                        Title =CommonUIStrings.DataGridView_Title_SparePartExtend_ReferenceCode
                    },new ColumnItem{
                        Name = "SupplierCode",
                        Title =CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierCode,
                    },new ColumnItem{
                        Name = "SupplierName",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PartsSupplierName,
                    },new ColumnItem{
                        Name = "PriceType",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_PriceType
                    },new ColumnItem{
                        Name = "PurchasePrice",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_PurchasePrice
                    },new ColumnItem{
                        Name = "ValidFrom",
                        Title = CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_ValidTo
                    },new ColumnItem{
                        Name = "ValidTo",
                        Title = CommonUIStrings.DataEditPanel_Text_PartsPurchasePricingChange_ValidFrom
                    },new ColumnItem{
                        Name = "CreatorName",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsSaleRtnInboundCheck_CreatorName
                    },new ColumnItem{
                        Name = "CreateTime",
                        Title = CommonUIStrings.QueryPanel_Title_ProductStandard_CreateTime,
                    },new ColumnItem{
                        Name = "PartsSalesCategoryName",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSupplierRelationWithPurchasePrice);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询配件与供应商关系及采购价";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "supplierId":
                        return BaseApp.Current.CurrentUserData.EnterpriseId;
                    case "time":
                        var timeFilterItem = filters.Filters.SingleOrDefault(e => e.MemberName == "Time");
                        if(timeFilterItem == null || timeFilterItem.Value == null)
                            return null;
                        return Convert.ToDateTime(timeFilterItem.Value).Date;
                    default:
                        return null;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                var param = new[] { "Time" };
                foreach(var item in compositeFilterItem.Filters.Where(e => !param.Contains(e.MemberName))) {
                    newCompositeFilterItem.Filters.Add(item);
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }
}
