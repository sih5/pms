﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class TiledRegionDataGridView : DcsDataGridViewBase {
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var regionNameComposite = new CompositeFilterItem {
                LogicalOperator = LogicalOperator.Or
            };
            if(compositeFilterItem != null) {
                var name = compositeFilterItem.Filters.FirstOrDefault(v => v.MemberName.Equals("Name")).Value;
                regionNameComposite.Filters.Add(new FilterItem {
                    MemberName = "RegionName",
                    MemberType = typeof(string),
                    Operator = Sunlight.Silverlight.Core.Model.FilterOperator.Contains,
                    Value = name
                });
                regionNameComposite.Filters.Add(new FilterItem {
                    MemberName = "ProvinceName",
                    MemberType = typeof(string),
                    Operator = Sunlight.Silverlight.Core.Model.FilterOperator.Contains,
                    Value = name
                });
                regionNameComposite.Filters.Add(new FilterItem {
                    MemberName = "CityName",
                    MemberType = typeof(string),
                    Operator = Sunlight.Silverlight.Core.Model.FilterOperator.Contains,
                    Value = name
                });
                regionNameComposite.Filters.Add(new FilterItem {
                    MemberName = "CountyName",
                    MemberType = typeof(string),
                    Operator = Sunlight.Silverlight.Core.Model.FilterOperator.Contains,
                    Value = name
                });
            }
            return regionNameComposite.ToFilterDescriptor();
        }

        protected override Type EntityType {
            get {
                return typeof(TiledRegion);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "RegionName"
                    }, new ColumnItem {
                        Name = "ProvinceName"
                    }, new ColumnItem {
                        Name = "CityName"
                    }, new ColumnItem {
                        Name = "CountyName"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetTiledRegions";
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
    }
}
