﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataGrid {
    public class VirtualSpartPartByClassDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "ABCSetting_Type"
        };

        public VirtualSpartPartByClassDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Multiple;
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {                    
                       new ColumnItem{
                        Name = "CompanyCode",
                        Title = CommonUIStrings.DataDeTailPanel_Text_Company_Code
                    }, new ColumnItem {
                        Name = "CompanyName",
                        Title = CommonUIStrings.DataDeTailPanel_Text_Company_Name
                    }, new ColumnItem {
                        Name = "SparePartCode",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_SparePartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title = CommonUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricing_SparePartName
                    }, new ColumnItem {
                        Name = "SuggestForceReserveQty",
                        Title = "强制储备数量"
                    }, new ColumnItem {
                        Name = "CenterPrice",
                        Title = CommonUIStrings.QueryPanel_QueryItem_Title_AllSparePart_CenterPrice
                    }, new KeyValuesColumnItem {
                        Name = "CenterPartProperty",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = "中心库配件属性"
                    },
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(ForceReserveBillDetailQuery);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询配件中心库属性";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "companyCode":
                        var filterCategory = filters.Filters.SingleOrDefault(item => item.MemberName == "CompanyCode");
                        if(filterCategory != null)
                            return filterCategory.Value;
                        return null;
                    case "companyName":
                        var filtermanagerId = filters.Filters.SingleOrDefault(item => item.MemberName == "CompanyName");
                        if(filtermanagerId != null)
                            return filtermanagerId.Value;
                        return null;
                    case "sparePartCode":
                        var sparePartCode = filters.Filters.SingleOrDefault(item => item.MemberName == "SparePartCode");
                        if(sparePartCode != null)
                            return sparePartCode.Value;
                        return null;
                    case "sparePartName":
                        var sparePartName = filters.Filters.SingleOrDefault(item => item.MemberName == "SparePartName");
                        if(sparePartName != null)
                            return sparePartName.Value;
                        return null;
                }
            }
            return null;
        }

        //protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
        //    var compositeFilterItem = this.FilterItem as CompositeFilterItem;
        //    var newCompositeFilterItem = new CompositeFilterItem();

        //    if(compositeFilterItem == null) {
        //        newCompositeFilterItem.Filters.Add(this.FilterItem);
        //    } else {
        //        foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "WarehouseAreaCategory"))
        //            newCompositeFilterItem.Filters.Add(item);
        //    }
        //    return newCompositeFilterItem.ToFilterDescriptor();
        //}
    }
}
