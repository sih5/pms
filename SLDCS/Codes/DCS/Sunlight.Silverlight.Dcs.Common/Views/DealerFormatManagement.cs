﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Views {
     [PageMeta("Common", "Company", "DealerFormat", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_ABANDON_IMPORT_EXPORT
    })]
    public class DealerFormatManagement: DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditViewImport;
        private const string DATA_EDIT_VIEW_IMPORT = "_DataEditViewImport_";
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private DcsDomainContext dcsDomainContext = new DcsDomainContext();


        public DealerFormatManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = "服务站/经销商业态管理";
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("DealerFormat"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("DealerFormat");
                    ((DealerFormatDataEditView)this.dataEditView).EditSubmitted += this.DataEditView_EditSubmitted; 
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_IMPORT, () => this.DataEditViewImport);
        }
        //导入
        private DataEditViewBase DataEditViewImport {
            get {
                if(this.dataEditViewImport == null) {
                    this.dataEditViewImport = DI.GetDataEditView("DealerFormatForImport");
                    this.dataEditViewImport.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewImport;
            }
        }
   
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);          
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "DealerFormat"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var agency = this.DataEditView.CreateObjectToEdit<DealerFormat>();
                    agency.Status = (int)DcsMasterDataStatus.有效;
                 
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;            
                case CommonActionKeys.ABANDON:
                     DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Abandon, () => {
                         var entitys = this.DataGridView.SelectedEntities.Cast<DealerFormat>().ToArray();
                        if(entitys == null || !entitys.Any())
                            return;
                        if(dcsDomainContext == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        try {
                            dcsDomainContext.作废服务商业态(entitys.Select(r => r.Id).ToArray(), loadOpv => {
                                if(loadOpv.HasError) {
                                    if(!loadOpv.IsErrorHandled)
                                        loadOpv.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOpv);
                                    dcsDomainContext.RejectChanges();
                                    ShellViewModel.Current.IsBusy = false;
                                    return;
                                }
                                UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_AbandonSuccess);
                                dcsDomainContext.RejectChanges();
                                if(DataGridView != null && DataGridView.FilterItem != null) {
                                    DataGridView.ExecuteQueryDelayed();
                                }
                                CheckActionsCanExecute();
                                ShellViewModel.Current.IsBusy = false;
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
               
                case CommonActionKeys.EXPORT:
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any())
                    {
                        var ids = this.DataGridView.SelectedEntities.Cast<DealerFormat>().Select(r => r.Id).ToArray();
                        this.ExportDealerFormat(ids, null, null, null, null, null, null,null);
                    }
                    else
                    {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if (filterItem == null)
                            return;
                        var dealerCode = filterItem.Filters.Single(r => r.MemberName == "DealerCode").Value as string;
                        var dealerName = filterItem.Filters.Single(r => r.MemberName == "DealerName").Value as string;
                        var format = filterItem.Filters.Single(r => r.MemberName == "Format").Value as string;
                        var quarter = filterItem.Filters.Single(r => r.MemberName == "Quarter").Value as string;
                        var status = filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var createTimeFilters = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem) && ((CompositeFilterItem)r).Filters.Any(e => e.MemberName == "CreateTime")) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if (createTimeFilters != null && createTimeFilters.Filters.Any())
                        {
                            createTimeBegin = createTimeFilters.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTimeFilters.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        this.ExportDealerFormat(null, status, dealerCode, dealerName, format, quarter, createTimeBegin, createTimeEnd);
                    }
                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORT);
                    break;
            }
        }

        private void ExportDealerFormat(int[] ids, int? status, string dealerCode, string dealerName, string format, string quarter, DateTime? bCreateTime, DateTime? eCreateTime) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportDealerFormatAsync(ids, status, dealerCode, dealerName, format, quarter, bCreateTime, eCreateTime);
            this.excelServiceClient.ExportDealerFormatCompleted -= excelServiceClient_ExportDealerFormatCompleted;
            this.excelServiceClient.ExportDealerFormatCompleted += excelServiceClient_ExportDealerFormatCompleted;
        }

        private void excelServiceClient_ExportDealerFormatCompleted(object sender, ExportDealerFormatCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.ADD:
                case CommonActionKeys.IMPORT:
                    return true;
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities1 = this.DataGridView.SelectedEntities.Cast<DealerFormat>().ToArray();
                    if(entities1.Length < 1)
                        return false;
                    return entities1.All(r => r.Status == (int)DcsBaseDataStatus.有效);
                default:
                    return false;
            }
        }
    }
}
