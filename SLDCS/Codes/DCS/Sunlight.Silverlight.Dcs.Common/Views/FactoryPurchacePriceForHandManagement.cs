﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using System.ComponentModel;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Panels.Query;
using Sunlight.Silverlight.Dcs.Common.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.Controls;
using System.Windows.Controls;
using Sunlight.Silverlight.Dcs.Web.Entities;

namespace Sunlight.Silverlight.Dcs.Common.Views {
      [PageMeta("PartsBaseInfo", "SparePartPurchase", "FactoryPurchacePriceForHand", ActionPanelKeys = new[] {
        "FactoryPurchacePriceForHand"
    })]
    public class FactoryPurchacePriceForHandManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        public FactoryPurchacePriceForHandManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = "采购价格待处理管理";
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("FactoryPurchacePriceForHand"));
            }
        }


        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "FactoryPurchacePriceForHand"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            var codes = compositeFilterItem.Filters.SingleOrDefault(s => s.MemberName == "SparePartCode");
            var supplierCode = compositeFilterItem.Filters.Single(r => r.MemberName == "SupplierCode");
            var supplierName = compositeFilterItem.Filters.Single(r => r.MemberName == "SupplierName");
            if(codes != null && codes.Value != null && codes.Value.ToString() == "-----"){
                codes.Value = null;
            }
            if(supplierCode.Value != null && supplierCode.Value.ToString() == "-----"){
                supplierCode.Value = null;
            }
            if(supplierName.Value != null && supplierName.Value.ToString() == "-----"){
                supplierName.Value = null;
            }
            if (codes != null && codes.Value != null) {
                compositeFilterItem.Filters.Add(new CompositeFilterItem {
                    MemberName = "SparePartCode",
                    MemberType = typeof(string),
                    Value = string.Join(",", ((IEnumerable<string>)codes.Value).ToArray()),
                    Operator = FilterOperator.Contains
                });
                compositeFilterItem.Filters.Remove(codes);
            }
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case "Create":
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Validation_VirtualFactoryPurchacePrice_AddPurchacePrice, () => {
                        var entitys = this.DataGridView.SelectedEntities.Cast<FactoryPurchacePriceForHand>().ToArray();
                        if(entitys == null || !entitys.Any())
                            return;
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(domainContext == null)
                            return;
                        domainContext.待处理生成采购价变更申请(entitys, invokeOp => {
                            if(invokeOp.HasError) {
                                if(!invokeOp.IsErrorHandled)
                                    invokeOp.MarkErrorAsHandled();
                                var error = invokeOp.ValidationErrors.First();
                                if(error != null)
                                    UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage);
                                else
                                    DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                                return;
                            }
                            UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_CreateSuccess);
                            this.DataGridView.ExecuteQueryDelayed();
                        }, null);
                    });
                    break;
                case "ExactExport":
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var supplierCode = filterItem.Filters.Single(r => r.MemberName == "SupplierCode").Value as string;
                        var supplierName = filterItem.Filters.Single(r => r.MemberName == "SupplierName").Value as string;
                        var sparePartCode = filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                        var sparePartName = filterItem.Filters.Single(r => r.MemberName == "SparePartName").Value as string;
                        var status = filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var isOrderable = filterItem.Filters.Single(r => r.MemberName == "IsOrderable").Value as bool?;                            
                        ShellViewModel.Current.IsBusy = true;
                        this.dcsDomainContext.导出查询采购价待处理(supplierCode, supplierName, sparePartCode, sparePartName, status,isOrderable, loadOp => {
                       if(loadOp.HasError) {
                           ShellViewModel.Current.IsBusy = false;
                           return;
                       }
                       if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                           UIHelper.ShowNotification(loadOp.Value);
                           ShellViewModel.Current.IsBusy = false;
                       }
                       if (loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                        {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                   }, null);
                   

                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case "Create":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    if(!this.DataGridView.SelectedEntities.Any())
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<FactoryPurchacePriceForHand>().ToArray();
                    return entities.All(r => r.Status !=1 && r.Id!=null&& r.SupplierId!=0);
                case "ExactExport":
                    return true;
                default:
                    return false;
            }
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private void ExportFactoryPurchacePrice(int[] ids,string supplierCode,string supplierName,string sparePartCode, string sparePartName,string hyCode,bool? isGenerate,DateTime? createTimeBegin,DateTime? createTimeEnd,bool? isExactExport)
        {
            ShellViewModel.Current.IsBusy = false;
            this.excelServiceClient.ExportFactoryPurchacePriceAsync(ids, supplierCode, supplierName, sparePartCode, sparePartName, hyCode, isGenerate, createTimeBegin, createTimeEnd, isExactExport);
            this.excelServiceClient.ExportFactoryPurchacePriceCompleted -= this.ExcelServiceClient_ExportFactoryPurchacePriceCompleted;
            this.excelServiceClient.ExportFactoryPurchacePriceCompleted += this.ExcelServiceClient_ExportFactoryPurchacePriceCompleted;
        }

        private void ExcelServiceClient_ExportFactoryPurchacePriceCompleted(object sender, ExportFactoryPurchacePriceCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
       
    }
}
