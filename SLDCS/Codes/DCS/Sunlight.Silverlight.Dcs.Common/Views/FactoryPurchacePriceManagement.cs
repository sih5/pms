﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using System.ComponentModel;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Panels.Query;
using Sunlight.Silverlight.Dcs.Common.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.Controls;
using System.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("PartsBaseInfo", "SparePartPurchase", "FactoryPurchacePrice", ActionPanelKeys = new[] {
        "FactoryPurchacePrice"
    })]
    public class FactoryPurchacePriceManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        public FactoryPurchacePriceManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataManagementView_Title_FactoryPurchacePriceManagement;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("FactoryPurchacePrice"));
            }
        }


        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "FactoryPurchacePrice"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            var codes = compositeFilterItem.Filters.SingleOrDefault(s => s.MemberName == "SparePartCode");
            var supplierCode = compositeFilterItem.Filters.Single(r => r.MemberName == "SupplierCode");
            var supplierName = compositeFilterItem.Filters.Single(r => r.MemberName == "SupplierName");
            if(codes != null && codes.Value != null && codes.Value.ToString() == "-----"){
                codes.Value = null;
            }
            if(supplierCode.Value != null && supplierCode.Value.ToString() == "-----"){
                supplierCode.Value = null;
            }
            if(supplierName.Value != null && supplierName.Value.ToString() == "-----"){
                supplierName.Value = null;
            }
            if ((codes == null || codes.Value == null) && (supplierCode.Value == null || string.IsNullOrEmpty(supplierCode.Value.ToString())) && (supplierName.Value == null || string.IsNullOrEmpty(supplierName.Value.ToString()))) {
                UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Validation_SupplierCode);
                return;
            }
            if (codes != null && codes.Value != null) {
                compositeFilterItem.Filters.Add(new CompositeFilterItem {
                    MemberName = "SparePartCode",
                    MemberType = typeof(string),
                    Value = string.Join(",", ((IEnumerable<string>)codes.Value).ToArray()),
                    Operator = FilterOperator.Contains
                });
                compositeFilterItem.Filters.Remove(codes);
            }
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case "Create":
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Validation_VirtualFactoryPurchacePrice_AddPurchacePrice, () => {
                        var entitys = this.DataGridView.SelectedEntities.Cast<VirtualFactoryPurchacePrice>().ToArray();
                        if (entitys == null || !entitys.Any())
                            return;
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if (domainContext == null)
                            return;
                        domainContext.生成采购价变更申请(entitys, invokeOp => {
                            if (invokeOp.HasError) {
                                if (!invokeOp.IsErrorHandled)
                                    invokeOp.MarkErrorAsHandled();
                                var error = invokeOp.ValidationErrors.First();
                                if (error != null)
                                    UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage);
                                else
                                    DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                                return;
                            }
                            UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_CreateSuccess);
                            this.DataGridView.DataContext = null;
                            if (this.DataGridView.FilterItem != null) {
                                var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                                var sparePartCodeFilter = filterItem.Filters.Single(r => r.MemberName == "SparePartCode");
                                var supplierCodeFilter = filterItem.Filters.Single(r => r.MemberName == "SupplierCode");
                                var supplierNameFilter = filterItem.Filters.Single(r => r.MemberName == "SupplierName");

                                if (sparePartCodeFilter.Value != null && !string.IsNullOrEmpty(sparePartCodeFilter.Value.ToString())) {
                                    sparePartCodeFilter.Value = "-----";
                                }
                                if (supplierCodeFilter.Value != null && !string.IsNullOrEmpty(supplierCodeFilter.Value.ToString())) { 
                                    supplierCodeFilter.Value = "-----";
                                }
                                if (supplierNameFilter.Value != null && !string.IsNullOrEmpty(supplierNameFilter.Value.ToString())) { 
                                    supplierNameFilter.Value = "-----";
                                }

                                var queryPanel = (FactoryPurchacePriceQueryPanel)this.GetQueryPanel("FactoryPurchacePrice");
                                if (queryPanel != null) {
                                    var control = queryPanel.FindChildrenByType<MultipleTextQueryControl>();
                                    var textBox = queryPanel.FindChildrenByType<TextBox>();
                                    if (control.Any()) {
                                        control.First().SetFilterValue(null);
                                    }
                                    if (textBox.Any()) {
                                        var boxs = textBox.ToArray();
                                        var box0 = boxs[0];
                                        var box1 = boxs[1];
                                        foreach (var item in textBox) {
                                            if (item == box0 || item == box1) {
                                                item.Text = string.Empty;
                                            }
                                        }
                                    }
                                }
                                this.DataGridView.ExecuteQueryDelayed();
                            }
                        }, null);
                    });
                    break;
                case "ExactExport":
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<VirtualFactoryPurchacePrice>().Select(r => r.Id).ToArray();
                        this.ExportFactoryPurchacePrice(ids, null, null, null, null, null, null, null, null, null);
                    } else {

                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var supplierCode = filterItem.Filters.Single(r => r.MemberName == "SupplierCode").Value as string;
                        var supplierName = filterItem.Filters.Single(r => r.MemberName == "SupplierName").Value as string;
                        var sparePartCode = filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                        var sparePartName = filterItem.Filters.Single(r => r.MemberName == "SparePartName").Value as string;
                        var hyCode = filterItem.Filters.Single(r => r.MemberName == "HYCode").Value as string;
                        var isGenerate = filterItem.Filters.Single(r => r.MemberName == "IsGenerate").Value as bool?;
                        var createTime = filterItem.Filters.FirstOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem) && ((CompositeFilterItem)filter).Filters.Any(e => e.MemberName == "CreateTime")) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        
                        ShellViewModel.Current.IsBusy = true;
                        if (uniqueId == CommonActionKeys.EXPORT) {
                            this.ExportFactoryPurchacePrice(null, supplierCode, supplierName, sparePartCode, sparePartName, hyCode,isGenerate, createTimeBegin, createTimeEnd,null);
                        } else {
                            if (string.IsNullOrEmpty(sparePartCode)) {
                                ShellViewModel.Current.IsBusy = false;
                                UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Validation_VirtualFactoryPurchacePrice_sparePartCode);
                                return;
                            }
                             this.ExportFactoryPurchacePrice(null, supplierCode, supplierName, sparePartCode, sparePartName, hyCode,isGenerate, createTimeBegin, createTimeEnd,true);
                        }
                    }

                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case "Create":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    if(!this.DataGridView.SelectedEntities.Any())
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<VirtualFactoryPurchacePrice>().ToArray();
                    return entities.Any();
                case "ExactExport":
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return false;
                    var sparePartCode = filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any() && !string.IsNullOrEmpty(sparePartCode); 
                default:
                    return false;
            }
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private void ExportFactoryPurchacePrice(int[] ids,string supplierCode,string supplierName,string sparePartCode, string sparePartName,string hyCode,bool? isGenerate,DateTime? createTimeBegin,DateTime? createTimeEnd,bool? isExactExport)
        {
            ShellViewModel.Current.IsBusy = false;
            this.excelServiceClient.ExportFactoryPurchacePriceAsync(ids, supplierCode, supplierName, sparePartCode, sparePartName, hyCode, isGenerate, createTimeBegin, createTimeEnd, isExactExport);
            this.excelServiceClient.ExportFactoryPurchacePriceCompleted -= this.ExcelServiceClient_ExportFactoryPurchacePriceCompleted;
            this.excelServiceClient.ExportFactoryPurchacePriceCompleted += this.ExcelServiceClient_ExportFactoryPurchacePriceCompleted;
        }

        private void ExcelServiceClient_ExportFactoryPurchacePriceCompleted(object sender, ExportFactoryPurchacePriceCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
       
    }
}
