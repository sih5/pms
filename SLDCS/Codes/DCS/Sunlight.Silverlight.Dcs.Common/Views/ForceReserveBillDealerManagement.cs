﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
namespace Sunlight.Silverlight.Dcs.Common.Views {
      [PageMeta("Common", "BottomStock", "ForceReserveBillDealer", ActionPanelKeys = new[] {
        "ForceReserveBillDealer"
    })]
    public class ForceReserveBillDealerManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataDetailView;
        private DataEditViewBase dataApproveView;
        private DataEditViewBase dataForEditView;
        private DataEditViewBase dataAuditView;
        private const string DATA_APPROVE_VIEW = "_dataApproveView_";
        private const string DATA_DETAIL_VIEW = "_DataDetailView_";
        private const string DATA_FOREDIT_VIEW = "_dataForEditView_";
        private const string DATA_AUDIT_VIEW = "_dataForAuditView_";
        private DcsDomainContext dcsDomainContext = new DcsDomainContext();
        private DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("ForceReserveBillDealer");
                    this.dataGridView.RowDoubleClick += DataGridView_RowDoubleClick;
                }
                return this.dataGridView;
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("ForceReserveBillDealer");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
          //审批
        private DataEditViewBase DataApproveView {
            get {
                if(this.dataApproveView == null) {
                    this.dataApproveView = DI.GetDataEditView("ForceReserveBillCenterForApprove");
                    this.dataApproveView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataApproveView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataApproveView;
            }
        }
          //确认，审核
        private DataEditViewBase DataAuditView {
            get {
                if(this.dataAuditView == null) {
                    this.dataAuditView = DI.GetDataEditView("ForceReserveBillDealerForApprove");
                    this.dataAuditView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataAuditView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataAuditView;
            }
        }
        private DataEditViewBase DataForEditView {
            get {
                if(this.dataForEditView == null) {
                    this.dataForEditView = DI.GetDataEditView("ForceReserveBillDealerForEdit");
                    this.dataForEditView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataForEditView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataForEditView;
            }
        }
        private DataEditViewBase DataEditViewDetail {
            get {
                if(this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("ForceReserveBillCenterForDetail");
                    this.dataDetailView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataDetailView;
            }
        }
        private void DataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                this.DataEditViewDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                this.SwitchViewTo(DATA_DETAIL_VIEW);
            }
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_FOREDIT_VIEW, () => this.DataForEditView);
            this.RegisterView(DATA_APPROVE_VIEW, () => this.DataApproveView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataEditViewDetail);
            this.RegisterView(DATA_AUDIT_VIEW, () => this.DataAuditView);
        }

        public ForceReserveBillDealerManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataManagementView_Title_ForceReserveBillDealer;
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "ForceReserveBillDealer"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var bottomStockForceReserveType = this.DataEditView.CreateObjectToEdit<ForceReserveBill>();
                    bottomStockForceReserveType.CompanyId = 0;
                    bottomStockForceReserveType.CompanyCode = "2";
                    bottomStockForceReserveType.CompanyName = "2";
                    bottomStockForceReserveType.CompanyType = (int)DcsCompanyType.服务站;
                    bottomStockForceReserveType.Status = (int)DCSForceReserveBillStatus.新建;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataForEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_FOREDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entitys = this.DataGridView.SelectedEntities.Cast<ForceReserveBill>().ToArray();
                        if(entitys == null || !entitys.Any())
                            return;
                        if(dcsDomainContext == null)
                            return;
                        try {
                            dcsDomainContext.作废强制储备单(entitys.Select(r => r.Id).ToArray(), loadOpv => {
                                if(loadOpv.HasError) {
                                    if(!loadOpv.IsErrorHandled)
                                        loadOpv.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOpv);
                                    dcsDomainContext.RejectChanges();
                                    ShellViewModel.Current.IsBusy = false;
                                    return;
                                }
                                UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_AbandonSuccess);
                                dcsDomainContext.RejectChanges();
                                if(DataGridView != null && DataGridView.FilterItem != null) {
                                    DataGridView.ExecuteQueryDelayed();
                                }
                                CheckActionsCanExecute();
                                ShellViewModel.Current.IsBusy = false;
                            }, null);
                            UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.SUBMIT:
                    DcsUtils.Confirm("确定要提交所选数据吗？", () => {
                        var entitys = this.DataGridView.SelectedEntities.Cast<ForceReserveBill>().ToArray();
                        if(entitys == null || !entitys.Any())
                            return;
                        if(dcsDomainContext == null)
                            return;
                        try {
                            dcsDomainContext.提交强制储备单(entitys.Select(r => r.Id).ToArray(), loadOpv => {
                                if(loadOpv.HasError) {
                                    if(!loadOpv.IsErrorHandled)
                                        loadOpv.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOpv);
                                    dcsDomainContext.RejectChanges();
                                    ShellViewModel.Current.IsBusy = false;
                                    return;
                                }
                                UIHelper.ShowNotification("所选数据提交成功");
                                dcsDomainContext.RejectChanges();
                                if(DataGridView != null && DataGridView.FilterItem != null) {
                                    DataGridView.ExecuteQueryDelayed();
                                }
                                CheckActionsCanExecute();
                                ShellViewModel.Current.IsBusy = false;
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.AUDIT:
                    var audits = this.DataGridView.SelectedEntities.Cast<ForceReserveBill>().ToArray();
                    if(audits.Length==1){
                        this.DataAuditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                        this.SwitchViewTo(DATA_AUDIT_VIEW);
                    } else {
                        DcsUtils.Confirm("确定要批量审核所选数据吗?", () => {                         
                            if(dcsDomainContext == null)
                                return;
                            try {
                                dcsDomainContext.批量审核强制储备单(audits.Select(r => r.Id).ToArray(), loadOpv => {
                                    if(loadOpv.HasError) {
                                        if(!loadOpv.IsErrorHandled)
                                            loadOpv.MarkErrorAsHandled();
                                        DcsUtils.ShowDomainServiceOperationWindow(loadOpv);
                                        dcsDomainContext.RejectChanges();
                                        ShellViewModel.Current.IsBusy = false;
                                        return;
                                    }
                                    UIHelper.ShowNotification("所选数据审核成功");
                                    dcsDomainContext.RejectChanges();
                                    if(DataGridView != null && DataGridView.FilterItem != null) {
                                        DataGridView.ExecuteQueryDelayed();
                                    }
                                    CheckActionsCanExecute();
                                    ShellViewModel.Current.IsBusy = false;
                                }, null);
                            } catch(Exception ex) {
                                UIHelper.ShowAlertMessage(ex.Message);
                            }
                        });
                    }
                    break;
                case CommonActionKeys.APPROVE:
                    var apps = this.DataGridView.SelectedEntities.Cast<ForceReserveBill>().ToArray();
                    if(apps.Length == 1) {
                        this.DataApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                        this.SwitchViewTo(DATA_APPROVE_VIEW);
                    } else {
                        DcsUtils.Confirm("确定要批量审批所选数据吗?", () => {
                            if(dcsDomainContext == null)
                                return;
                            try {
                                dcsDomainContext.批量审批强制储备单(apps.Select(r => r.Id).ToArray(), loadOpv => {
                                    if(loadOpv.HasError) {
                                        if(!loadOpv.IsErrorHandled)
                                            loadOpv.MarkErrorAsHandled();
                                        DcsUtils.ShowDomainServiceOperationWindow(loadOpv);
                                        dcsDomainContext.RejectChanges();
                                        ShellViewModel.Current.IsBusy = false;
                                        return;
                                    }
                                    UIHelper.ShowNotification("所选数据审批成功");
                                    dcsDomainContext.RejectChanges();
                                    if(DataGridView != null && DataGridView.FilterItem != null) {
                                        DataGridView.ExecuteQueryDelayed();
                                    }
                                    CheckActionsCanExecute();
                                    ShellViewModel.Current.IsBusy = false;
                                }, null);
                            } catch(Exception ex) {
                                UIHelper.ShowAlertMessage(ex.Message);
                            }
                        });
                    }
                    break;
                case CommonActionKeys.CONFIRM:
                    var con = this.DataGridView.SelectedEntities.Cast<ForceReserveBill>().ToArray();
                    if(con.Length == 1) {
                        this.DataAuditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                        this.SwitchViewTo(DATA_AUDIT_VIEW);
                    } else {
                        DcsUtils.Confirm("确定要批量确认所选数据吗?", () => {
                            if(dcsDomainContext == null)
                                return;
                            try {
                                dcsDomainContext.批量确认强制储备单(con.Select(r => r.Id).ToArray(), loadOpv => {
                                    if(loadOpv.HasError) {
                                        if(!loadOpv.IsErrorHandled)
                                            loadOpv.MarkErrorAsHandled();
                                        DcsUtils.ShowDomainServiceOperationWindow(loadOpv);
                                        dcsDomainContext.RejectChanges();
                                        ShellViewModel.Current.IsBusy = false;
                                        return;
                                    }
                                    UIHelper.ShowNotification("所选数据确认成功");
                                    dcsDomainContext.RejectChanges();
                                    if(DataGridView != null && DataGridView.FilterItem != null) {
                                        DataGridView.ExecuteQueryDelayed();
                                    }
                                    CheckActionsCanExecute();
                                    ShellViewModel.Current.IsBusy = false;
                                }, null);
                                UIHelper.ShowNotification("所选数据确认成功");
                            } catch(Exception ex) {
                                UIHelper.ShowAlertMessage(ex.Message);
                            }
                        });
                    }
                    break;
                case CommonActionKeys.EXPORT:
                     if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any())
                    {
                        var ids = this.DataGridView.SelectedEntities.Cast<ForceReserveBill>().Select(r => r.Id).ToArray();
                        ShellViewModel.Current.IsBusy = true;
                        this.dcsDomainContext.导出服务站强制储备信息主单(ids, null, null, null, null, null, null, null, null, null, null, null,null, loadOp => {
                            if(loadOp.HasError) {
                                ShellViewModel.Current.IsBusy = false;
                                return;
                            }
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                                ShellViewModel.Current.IsBusy = false;
                            }

                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                ShellViewModel.Current.IsBusy = false;
                            }
                        }, null);
                    }
                    else
                    {var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if (filterItem == null)
                        return;
                    var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                    var subVersionCode = filterItem.Filters.Single(e => e.MemberName == "SubVersionCode").Value as string;
                    var colVersionCode = filterItem.Filters.Single(e => e.MemberName == "ColVersionCode").Value as string;
                    var reserveTypeSubItem = filterItem.Filters.Single(r => r.MemberName == "ReserveTypeSubItem").Value as string;
                    var companyCode = filterItem.Filters.Single(r => r.MemberName == "CompanyCode").Value as string;
                    var companyName = filterItem.Filters.Single(r => r.MemberName == "CompanyName").Value as string;
                    var reserveType = filterItem.Filters.Single(r => r.MemberName == "ReserveType").Value as string;
                    var centerName = filterItem.Filters.Single(r => r.MemberName == "CenterName").Value as string;
                     DateTime? bCreateTime = null;
                     DateTime? eCreateTime = null;
                     DateTime? bApproveTime = null;
                     DateTime? eApproveTime = null;
                    foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                        var dateTime = filter as CompositeFilterItem;
                        if(dateTime != null) {
                            if(dateTime.Filters.First().MemberName == "CreateTime") {
                                bCreateTime = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                eCreateTime = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                            }
                            if(dateTime.Filters.First().MemberName == "ApproveTime") {
                                bApproveTime = dateTime.Filters.First(r => r.MemberName == "ApproveTime").Value as DateTime?;
                                eApproveTime = dateTime.Filters.Last(r => r.MemberName == "ApproveTime").Value as DateTime?;
                            }
                           
                        }
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出服务站强制储备信息主单(null, status, subVersionCode, colVersionCode, reserveTypeSubItem, companyCode, companyName, bCreateTime, eCreateTime, bApproveTime, eApproveTime, reserveType,centerName, loadOp =>
                    {
                        if(loadOp.HasError) {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    }
                    break;  
                case CommonActionKeys.MERGEEXPORT:
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any())
                    {
                        var ids = this.DataGridView.SelectedEntities.Cast<ForceReserveBill>().Select(r => r.Id).ToArray();
                        ShellViewModel.Current.IsBusy = true;
                        this.dcsDomainContext.导出服务站强制储备信息(ids, null, null, null, null, null, null, null, null, null, null, null,null, loadOp => {
                            if(loadOp.HasError) {
                                ShellViewModel.Current.IsBusy = false;
                                return;
                            }
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                                ShellViewModel.Current.IsBusy = false;
                            }

                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                ShellViewModel.Current.IsBusy = false;
                            }
                        }, null);
                    }
                    else
                    {var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if (filterItem == null)
                        return;
                    var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                    var subVersionCode = filterItem.Filters.Single(e => e.MemberName == "SubVersionCode").Value as string;
                    var colVersionCode = filterItem.Filters.Single(e => e.MemberName == "ColVersionCode").Value as string;
                    var reserveTypeSubItem = filterItem.Filters.Single(r => r.MemberName == "ReserveTypeSubItem").Value as string;
                    var companyCode = filterItem.Filters.Single(r => r.MemberName == "CompanyCode").Value as string;
                    var companyName = filterItem.Filters.Single(r => r.MemberName == "CompanyName").Value as string;
                    var reserveType = filterItem.Filters.Single(r => r.MemberName == "ReserveType").Value as string;
                    var centerName = filterItem.Filters.Single(r => r.MemberName == "CenterName").Value as string;
                     DateTime? bCreateTime = null;
                     DateTime? eCreateTime = null;
                     DateTime? bApproveTime = null;
                     DateTime? eApproveTime = null;
                    foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                        var dateTime = filter as CompositeFilterItem;
                        if(dateTime != null) {
                            if(dateTime.Filters.First().MemberName == "CreateTime") {
                                bCreateTime = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                eCreateTime = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                            }
                            if(dateTime.Filters.First().MemberName == "ApproveTime") {
                                bApproveTime = dateTime.Filters.First(r => r.MemberName == "ApproveTime").Value as DateTime?;
                                eApproveTime = dateTime.Filters.Last(r => r.MemberName == "ApproveTime").Value as DateTime?;
                            }
                           
                        }
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出服务站强制储备信息(null, status, subVersionCode, colVersionCode, reserveTypeSubItem, companyCode, companyName, bCreateTime, eCreateTime, bApproveTime, eApproveTime, reserveType, centerName,loadOp => {
                        if(loadOp.HasError) {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    }
                    break;               
            }
        }
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public void ExportPickingTask(int[] ids, int? status, string subVersionCode, string colVersionCode, string reserveTypeSubItem, string companyCode, string companyName, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bApproveTime, DateTime? eApproveTime, int? orderTypeId, string reserveType) {
            this.excelServiceClient.ExportForceReserveBillAsync(ids, status, subVersionCode, colVersionCode, reserveTypeSubItem, companyCode, companyName, bCreateTime, eCreateTime, bApproveTime, eApproveTime, null, 2, reserveType);
            this.excelServiceClient.ExportForceReserveBillCompleted -= ExcelServiceClient_ExportForceReserveBillCompleted;
            this.excelServiceClient.ExportForceReserveBillCompleted += ExcelServiceClient_ExportForceReserveBillCompleted;
        }
        private void ExcelServiceClient_ExportForceReserveBillCompleted(object sender, ExportForceReserveBillCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;               
                case CommonActionKeys.EDIT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<ForceReserveBill>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DCSForceReserveBillStatus.新建;
                case CommonActionKeys.SUBMIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities1 = this.DataGridView.SelectedEntities.Cast<ForceReserveBill>().ToArray();
                    if(entities1.Length < 1)
                        return false;
                    return entities1.All(r => r.Status == (int)DCSForceReserveBillStatus.新建);
                case CommonActionKeys.AUDIT:
                      if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitiesAu = this.DataGridView.SelectedEntities.Cast<ForceReserveBill>().ToArray();
                    if(entitiesAu.Length < 1)
                        return false;
                    return entitiesAu.All(r => r.Status == (int)DCSForceReserveBillStatus.提交);
                case CommonActionKeys.CONFIRM:
                      if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitiesCo = this.DataGridView.SelectedEntities.Cast<ForceReserveBill>().ToArray();
                    if(entitiesCo.Length < 1)
                        return false;
                    return entitiesCo.All(r => r.Status == (int)DCSForceReserveBillStatus.审核通过&&r.ReserveType!="T" && r.CompanyType==(int)DcsCompanyType.服务站);
                case CommonActionKeys.APPROVE:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitiesApp = this.DataGridView.SelectedEntities.Cast<ForceReserveBill>().ToArray();
                    if(entitiesApp.Length < 1)
                        return false;
                    return entitiesApp.All(r => (r.Status == (int)DCSForceReserveBillStatus.已确认  && r.ReserveType!="T") || (r.Status == (int)DCSForceReserveBillStatus.审核通过 && r.ReserveType=="T"));
                case CommonActionKeys.MERGEEXPORT:
                    return true;
                case CommonActionKeys.EXPORT:
                    return true;
                default:
                    return false;
            }
        }
    }
}