﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("Common", "Category", "GoldenTaxClassifyMsg", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_IMPORT_EXPORT
    })]
    public class GoldenTaxClassifyMsgMamagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditViewImport;
        private const string DATA_EDIT_VIEW_IMPORT = "_DataEditViewImport_";

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_IMPORT, () => this.DataEditViewImport);
        }
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("GoldenTaxClassifyMsg"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("GoldenTaxClassifyMsg");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        public GoldenTaxClassifyMsgMamagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataManagementView_Title_GoldenTaxClassifyMsg;
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "GoldenTaxClassifyMsg"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    this.DataEditView.CreateObjectToEdit<GoldenTaxClassifyMsg>();
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<GoldenTaxClassifyMsg>().Select(r => r.Id).ToArray();
                        this.导出金税分类信息(ids, null, null, null, null, null, null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var goldenTaxClassifyCode = filterItem.Filters.Single(r => r.MemberName == "GoldenTaxClassifyCode").Value as string;
                        var goldenTaxClassifyName = filterItem.Filters.Single(r => r.MemberName == "GoldenTaxClassifyName").Value as string;
                        var createTime = filterItem.Filters.FirstOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem) && ((CompositeFilterItem)filter).Filters.Any(e => e.MemberName == "CreateTime")) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }

                        var modifyTime = filterItem.Filters.FirstOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem) && ((CompositeFilterItem)filter).Filters.Any(e => e.MemberName == "ModifyTime")) as CompositeFilterItem;
                        DateTime? modifyTimeBegin = null;
                        DateTime? modifyTimeEnd = null;
                        if(modifyTime != null) {
                            modifyTimeBegin = modifyTime.Filters.First(r => r.MemberName == "ModifyTime").Value as DateTime?;
                            modifyTimeEnd = modifyTime.Filters.Last(r => r.MemberName == "ModifyTime").Value as DateTime?;
                        }
                        this.导出金税分类信息(new int[] { }, goldenTaxClassifyCode, goldenTaxClassifyName, createTimeBegin, createTimeEnd, modifyTimeBegin, modifyTimeEnd);
                    }
                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORT);
                    break;
            }
        }
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private void 导出金税分类信息(int[] ids, string goldenTaxClassifyCode, string goldenTaxClassifyName, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? modifyTimeBegin, DateTime? modifyTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.导出金税分类信息Async(ids, goldenTaxClassifyCode, goldenTaxClassifyName, createTimeBegin, createTimeEnd, modifyTimeBegin, modifyTimeEnd);
            this.excelServiceClient.导出金税分类信息Completed -= excelServiceClient_导出金税分类信息Completed;
            this.excelServiceClient.导出金税分类信息Completed += excelServiceClient_导出金税分类信息Completed;
        }

        private void excelServiceClient_导出金税分类信息Completed(object sender, 导出金税分类信息CompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                case CommonActionKeys.EDIT:
                case CommonActionKeys.IMPORT:
                    return true;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var composite = filterItem as CompositeFilterItem;
            ClientVar.ConvertTime(composite);
            this.DataGridView.FilterItem = composite;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewImport = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        //导入修改
        private DataEditViewBase DataEditViewImport {
            get {
                if(this.dataEditViewImport == null) {
                    this.dataEditViewImport = DI.GetDataEditView("GoldenTaxClassifyMsgForImport");
                    this.dataEditViewImport.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewImport;
            }
        }
    }
}
