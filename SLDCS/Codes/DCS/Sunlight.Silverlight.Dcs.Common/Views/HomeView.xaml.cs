﻿using System;
using Sunlight.Silverlight.Core.View;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("Home", null, null)]
    public partial class HomeView : IRootView {
        public object UniqueId {
            get {
                return 0;
            }
        }

        public HomeView() {
            this.InitializeComponent();
        }

        public object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            throw new NotSupportedException();
        }
    }
}
