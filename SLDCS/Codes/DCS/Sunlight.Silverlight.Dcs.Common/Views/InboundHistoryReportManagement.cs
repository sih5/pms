﻿using System;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("Report", "Purchase", "InboundHistory")]
    public class InboundHistoryReportManagement : DcsDataManagementViewBase {
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        public InboundHistoryReportManagement() {
            this.Title = CommonUIStrings.DataManagementView_Title_SparePart;
            this.Loaded -= InboundHistoryReportManagement_Loaded;
            this.Loaded += InboundHistoryReportManagement_Loaded;
        }

        void InboundHistoryReportManagement_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            ShellViewModel.Current.IsMenuOpen = true;
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.GetTokenAsync("psmsadmin", "psms-admin", "172.24.7.87:6400", "secEnterprise", 1, 100);
            this.excelServiceClient.GetTokenCompleted -= excelServiceClient_GetTokenCompleted;
            this.excelServiceClient.GetTokenCompleted += excelServiceClient_GetTokenCompleted;
        }

        private void excelServiceClient_GetTokenCompleted(object sender, GetTokenCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            if(string.IsNullOrEmpty(e.Result)) {
                UIHelper.ShowAlertMessage(CommonUIStrings.DataManagementView_Validation_Service);
            } else {
                string url = "http://pmsrf.foton.com.cn:8080/BOE/OpenDocument/opendoc/openDocument.jsp?token=" + e.Result + "&iDocID=6932&sType=wid";
                HtmlPage.Window.Navigate(new Uri(url, UriKind.Absolute), "_blank");
            }
            ShellViewModel.Current.PageUri = new Uri("/home", UriKind.Relative);
        }


        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {

        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
        }
    }
}
