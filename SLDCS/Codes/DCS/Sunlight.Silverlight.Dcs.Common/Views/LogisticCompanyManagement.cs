﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("Common", "Company", "LogisticCompany", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_PAUSE_RESUME_DETAIL
    })]
    public class LogisticCompanyManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditViewDetail;
        private const string DATA_EDIT_VIEW_DETAIL = "_DataEditViewDetail_";

        public LogisticCompanyManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataManagementView_Title_LogisticCompany;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("LogisticCompany"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("LogisticCompany");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataEditViewDetail {
            get {
                if(this.dataEditViewDetail == null) {
                    this.dataEditViewDetail = DI.GetDataEditView("LogisticCompanyDetail");
                    this.dataEditViewDetail.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditViewDetail.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewDetail;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_DETAIL, () => this.DataEditViewDetail);
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewDetail = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "LogisticCompany"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var logisticCompany = this.DataEditView.CreateObjectToEdit<LogisticCompany>();
                    logisticCompany.Status = (int)DcsMasterDataStatus.有效;
                    logisticCompany.Company = new Company();
                    logisticCompany.Company.Type = (int)DcsCompanyType.物流公司;
                    logisticCompany.Company.Status = (int)DcsMasterDataStatus.有效;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.PAUSE:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Pause, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<LogisticCompany>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can停用物流公司)
                                entity.停用物流公司();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_PauseSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.RESUME:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Resume, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<LogisticCompany>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can恢复物流公司)
                                entity.恢复物流公司();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_ResumeSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<LogisticCompany>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废物流公司)
                                entity.作废物流公司();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.DETAIL:
                    this.DataEditViewDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_DETAIL);
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.PAUSE:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entity = this.DataGridView.SelectedEntities.Cast<LogisticCompany>().ToArray();
                    return entity.Length == 1 && entity[0].Status == (int)DcsMasterDataStatus.有效;
                case CommonActionKeys.RESUME:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<LogisticCompany>().ToArray();
                    return entities.Length == 1 && entities[0].Status == (int)DcsMasterDataStatus.停用;
                case CommonActionKeys.DETAIL:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitie = this.DataGridView.SelectedEntities.Cast<LogisticCompany>().ToArray();
                    return entitie.Length == 1;
                default:
                    return false;
            }
        }
    }
}
