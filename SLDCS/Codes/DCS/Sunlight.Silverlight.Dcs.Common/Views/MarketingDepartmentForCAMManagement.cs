﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("Common", "EnterpriseAndInternalOrganization", "MarketingDepartmentForCAM", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_EXPORT
    })]
    public class MarketingDepartmentForCAMManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;

        public MarketingDepartmentForCAMManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataManagementView_Title_SalesRegion;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("MarketingDepartmentForCAM"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("MarketingDepartmentForCAM");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }
        private void ResetEditView() {
            this.dataEditView = null;           
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "MarketingDepartmentForCAM"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var marketingDepartment = this.DataEditView.CreateObjectToEdit<MarketingDepartment>();
                    marketingDepartment.BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    marketingDepartment.Status = (int)DcsBaseDataStatus.有效;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var temp = this.DataGridView.SelectedEntities.Cast<MarketingDepartment>().SingleOrDefault();
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(domainContext == null)
                            return;
                        domainContext.Load(domainContext.GetMarketingDepartmentsWithRelationsAndDetailsQuery(temp.Id, BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                            if(loadOp.HasError) {
                                if(!loadOp.IsErrorHandled)
                                    loadOp.MarkErrorAsHandled();
                                DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                                return;
                            }
                            var entity = loadOp.Entities.SingleOrDefault();
                            if(entity == null)
                                return;
                            try {
                                entity.Status = (int)DcsBaseDataStatus.作废;
                                foreach(var item in entity.DealerMarketDptRelations) {
                                    item.Status = (int)DcsBaseDataStatus.作废;
                                }
                                domainContext.SubmitChanges(submitOp => {
                                    if(submitOp.HasError) {
                                        if(!submitOp.IsErrorHandled)
                                            submitOp.MarkErrorAsHandled();
                                        DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                        domainContext.RejectChanges();
                                        return;
                                    }
                                    UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_AbandonSuccess);
                                    this.CheckActionsCanExecute();
                                }, null);
                            } catch(Exception ex) {
                                UIHelper.ShowAlertMessage(ex.Message);
                            }
                        }, null);
                    });
                    break;
                case CommonActionKeys.EXPORT:
                    ((DcsDataGridViewBase)this.DataGridView).ExportData();
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<MarketingDepartment>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    if(entities[0].BranchId != BaseApp.Current.CurrentUserData.EnterpriseId)
                        return false;
                    return entities[0].Status == (int)DcsBaseDataStatus.有效;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
    }
}
