using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("Common", "EnterpriseAndInternalOrganization", "MarketingDepartment", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_EXPORT_IMPORT
    })]
    public class MarketingDepartmentManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase importDataEditView;
        private const string DATA_IMPORT_VIEW = "_DataImportView_";
        private ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        public MarketingDepartmentManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataManagementView_Title_MarketingDepartmentManagement;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("MarketingDepartment"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("MarketingDepartment");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase ImportDataEditView {
            get {
                if(this.importDataEditView == null) {
                    this.importDataEditView = DI.GetDataEditView("MarketingDepartmentImport");
                    this.importDataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.importDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.importDataEditView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_IMPORT_VIEW, () => this.ImportDataEditView);
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.importDataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "MarketingDepartment"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                compositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "BranchId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });
            }
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var marketingDepartment = this.DataEditView.CreateObjectToEdit<MarketingDepartment>();
                    marketingDepartment.BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    marketingDepartment.Status = (int)DcsBaseDataStatus.有效;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<MarketingDepartment>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废营销分公司市场部)
                                entity.作废营销分公司市场部();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.IMPORT:
                    //导入
                    this.SwitchViewTo(DATA_IMPORT_VIEW);
                    break;
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                    var name = filterItem.Filters.Single(r => r.MemberName == "Name").Value as string;
                    var branchId = filterItem.Filters.Single(r => r.MemberName == "BranchId").Value as int?;
                    var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                    ShellViewModel.Current.IsBusy = true;
                    this.ExportMarketingDepartment(code, name, branchId, partsSalesCategoryId, null);
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<MarketingDepartment>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    if(entities[0].BranchId != BaseApp.Current.CurrentUserData.EnterpriseId)
                        return false;
                    return entities[0].Status == (int)DcsBaseDataStatus.有效;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.IMPORT:
                    return true;
                default:
                    return false;
            }
        }

        private void ExportMarketingDepartment(string code, string name, int? branchId, int? partsSalesCategoryId, int? businessType) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportMarketingDepartmentAsync(code, name, branchId, partsSalesCategoryId, businessType);
            this.excelServiceClient.ExportMarketingDepartmentCompleted -= excelServiceClient_ExportMarketingDepartmentCompleted;
            this.excelServiceClient.ExportMarketingDepartmentCompleted += excelServiceClient_ExportMarketingDepartmentCompleted;
        }

        private void excelServiceClient_ExportMarketingDepartmentCompleted(object sender, ExportMarketingDepartmentCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

    }
}
