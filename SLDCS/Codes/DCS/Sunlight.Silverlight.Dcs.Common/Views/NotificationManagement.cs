﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("NotificationInfo", "Notification", "Notification", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_DETAIL,"Notification"
    })]
    public class NotificationManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase replyDataEditView;
        private DataEditViewBase uploadDataEditView;
        private DataEditViewBase dataDetailView;
        private const string DATA_DETAIL_VIEW = "_DataDetailView_";
        protected const string Reply_DATA_EDIT_VIEW = "_ReplyDataEditView_";
        protected const string Upload_DATA_EDIT_VIEW = "_UploadDataEditView_";

        private DataEditViewBase dataTopView;
        public const string DATA_TOP_VIEW = "_dataTopView_";

        private DataEditViewBase DataEditViewTop {
            get {
                if(this.dataTopView == null) {
                    this.dataTopView = DI.GetDataEditView("NotificationTop");
                    this.dataTopView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataTopView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataTopView;
            }
        }

        private DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("Notification");
                    this.dataGridView.RowDoubleClick += DataGridView_RowDoubleClick;
                }
                return this.dataGridView;
            }
        }

        private void DataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            this.DataEditViewDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
            this.SwitchViewTo(DATA_DETAIL_VIEW);
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("Notification");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase ReplyDataEditView {
            get {
                if(this.replyDataEditView == null) {
                    this.replyDataEditView = DI.GetDataEditView("NotificationReply");
                    this.replyDataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.replyDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.replyDataEditView;
            }
        }

        private DataEditViewBase UploadDataEditView {
            get {
                if(this.uploadDataEditView == null) {
                    this.uploadDataEditView = DI.GetDataEditView("NotificationUpload");
                    this.uploadDataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.uploadDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.uploadDataEditView;
            }
        }

        private DataEditViewBase DataEditViewDetail {
            get {
                if(this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("NotificationDetail");
                    this.dataDetailView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataDetailView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataDetailView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(Reply_DATA_EDIT_VIEW, () => this.ReplyDataEditView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataEditViewDetail);
            this.RegisterView(DATA_TOP_VIEW, () => this.DataEditViewTop);
            this.RegisterView(Upload_DATA_EDIT_VIEW, () => this.UploadDataEditView);
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.replyDataEditView = null;
            this.uploadDataEditView = null;
            this.dataDetailView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "Notification"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var ddomainContext = new DcsDomainContext();
                    ddomainContext.Load(ddomainContext.GetCompaniesQuery().Where(e => e.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError)
                            return;
                        var company = loadOp.Entities.FirstOrDefault();
                        var notification = this.DataEditView.CreateObjectToEdit<Notification>();
                        notification.Status = (int)DcsBaseDataStatus.有效;
                        notification.CompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                        notification.CompanyName = BaseApp.Current.CurrentUserData.EnterpriseName;
                        notification.CompanyCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                        notification.TopModifierId = BaseApp.Current.CurrentUserData.UserId;
                        notification.TopModifierName = BaseApp.Current.CurrentUserData.UserName;
                        notification.Top = false;
                        notification.CompanyType = company.Type;
                        notification.Code = GlobalVar.ASSIGNED_BY_SERVER;
                        notification.UrgentLevel = (int)DcsNotificationUrgentLevel.普通;
                        this.SwitchViewTo(DATA_EDIT_VIEW);
                    }, null);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Abandon, () => this.DataGridView.UpdateSelectedEntities(entity => ((Notification)entity).Status = (int)DcsBaseDataStatus.作废, () => {
                        UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_AbandonSuccess);
                        this.CheckActionsCanExecute();
                    }));
                    break;
                case CommonActionKeys.DETAIL:
                    this.DataEditViewDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_DETAIL_VIEW);
                    break;
                case "Top":
                    this.DataEditViewTop.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_TOP_VIEW);
                    break;
                case CommonActionKeys.UPLOAD:
                case CommonActionKeys.REPLY:
                    var domainContext2 = new DcsDomainContext();
                    var deadtime2 = DateTime.Now.AddMonths(-2);
                    domainContext2.Load(domainContext2.GetNotificationsQuery().Where(e => e.Id == this.DataGridView.SelectedEntities.Cast<Notification>().
                        ToArray()[0].Id && e.CreateTime >= deadtime2), LoadBehavior.RefreshCurrent, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(!loadOp.Entities.Any()) {
                                UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Validation_Company_OverTime);
                                return;
                            }
                            switch(uniqueId) {
                                case CommonActionKeys.UPLOAD:
                                    this.UploadDataEditView.ExchangeData(null, uniqueId, null);
                                    this.UploadDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                                    this.SwitchViewTo(Upload_DATA_EDIT_VIEW);
                                    break;
                                case CommonActionKeys.REPLY:
                                    this.ReplyDataEditView.ExchangeData(null, uniqueId, null);
                                    this.ReplyDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                                    this.SwitchViewTo(Reply_DATA_EDIT_VIEW);
                                    break;

                            }
                        }, null);
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case "Top":
                case CommonActionKeys.DETAIL:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<Notification>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return true;
                case CommonActionKeys.UPLOAD:
                case CommonActionKeys.REPLY:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities2 = this.DataGridView.SelectedEntities.Cast<Notification>().ToArray();
                    if(entities2.Length != 1)
                        return false;
                    return entities2[0].Status == (int)DcsBaseDataStatus.有效;
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            if(filterItem is CompositeFilterItem) {
                compositeFilterItem = filterItem as CompositeFilterItem;
                var urgentLevel = compositeFilterItem.Filters.SingleOrDefault(filter => filter.MemberName == "UrgentLevel");
                if(urgentLevel == null || urgentLevel.Value == null) {
                    UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Validation_Company_urgentLevel);
                    return;
                }
            }
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "CompanyId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        public NotificationManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataManagementView_Title_Notification;
        }
    }
}
