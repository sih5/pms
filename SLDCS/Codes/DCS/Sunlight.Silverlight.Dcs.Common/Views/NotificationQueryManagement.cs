﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("NotificationInfo", "Notification", "NotificationQuery", ActionPanelKeys = new[] {
        "NotificationQuery",
    })]
    public class NotificationQueryManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase replyDataEditView;
        private DataEditViewBase uploadDataEditView;
        protected const string Reply_DATA_EDIT_VIEW = "_ReplyDataEditView_";
        protected const string Upload_DATA_EDIT_VIEW = "_UploadDataEditView_";
        private DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("NotificationQuery");
                    this.dataGridView.RowDoubleClick += DataGridView_RowDoubleClick;
                }
                return this.dataGridView;
            }
        }

        private DataEditViewBase ReplyDataEditView {
            get {
                if(this.replyDataEditView == null) {
                    this.replyDataEditView = DI.GetDataEditView("NotificationReply");
                    this.replyDataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.replyDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.replyDataEditView;
            }
        }

        private DataEditViewBase UploadDataEditView {
            get {
                if(this.uploadDataEditView == null) {
                    this.uploadDataEditView = DI.GetDataEditView("NotificationUpload");
                    this.uploadDataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.uploadDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.uploadDataEditView;
            }
        }

        private void DataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
            this.SwitchViewTo(DATA_EDIT_VIEW);
            this.CreateNotificationOpLog();
        }
        //生成公告操作日志
        private void CreateNotificationOpLog() {
            var notification = this.DataGridView.SelectedEntities.First() as Notification;
            if(notification == null)
                return;
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.NotificationOpLogs.Add(new NotificationOpLog() {
                NotificationID = notification.Id,
                OperationType = (int)DcsOperationType.查看公告,
                CustomerCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId,
                CustomerCompanyCode = BaseApp.Current.CurrentUserData.EnterpriseCode,
                CustomerCompanyName = BaseApp.Current.CurrentUserData.EnterpriseName,
                Status = (int)DcsBaseDataStatus.有效
            });
            dcsDomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    dcsDomainContext.RejectChanges();
                    return;
                }
            }, null);
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("NotificationQuery");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(Reply_DATA_EDIT_VIEW, () => this.ReplyDataEditView);
            this.RegisterView(Upload_DATA_EDIT_VIEW, () => this.UploadDataEditView);

        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.replyDataEditView = null;
            this.uploadDataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "Notification"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.DETAIL:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    this.CreateNotificationOpLog();
                    break;
                case CommonActionKeys.UPLOAD:
                case CommonActionKeys.REPLY:
                    var domainContext2 = new DcsDomainContext();
                    var deadtime2 = DateTime.Now.AddMonths(-2);
                    domainContext2.Load(domainContext2.GetNotificationsQuery().Where(e => e.Id == this.DataGridView.SelectedEntities.Cast<Notification>().
                        ToArray()[0].Id && e.CreateTime >= deadtime2), LoadBehavior.RefreshCurrent, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(!loadOp.Entities.Any()) {
                                UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Validation_Company_OverTime);
                                return;
                            }
                            switch(uniqueId) {
                                case CommonActionKeys.UPLOAD:
                                    this.UploadDataEditView.ExchangeData(null, uniqueId, null);
                                    this.UploadDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                                    this.SwitchViewTo(Upload_DATA_EDIT_VIEW);
                                    break;
                                case CommonActionKeys.REPLY:
                                    this.ReplyDataEditView.ExchangeData(null, uniqueId, null);
                                    this.ReplyDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                                    this.SwitchViewTo(Reply_DATA_EDIT_VIEW);
                                    break;

                            }
                        }, null);
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.DETAIL:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitie = this.DataGridView.SelectedEntities.Cast<Notification>().ToArray();
                    if(entitie.Length != 1)
                        return false;
                    return true;
                case CommonActionKeys.UPLOAD:
                case CommonActionKeys.REPLY:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities2 = this.DataGridView.SelectedEntities.Cast<Notification>().ToArray();
                    if(entities2.Length != 1)
                        return false;
                    return entities2[0].Status == (int)DcsBaseDataStatus.有效;
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        public NotificationQueryManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataManagementView_Title_NotificationQuery;
        }
    }
}
