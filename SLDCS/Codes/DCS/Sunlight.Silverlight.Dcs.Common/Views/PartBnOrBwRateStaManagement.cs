﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("Report", "Dealer", "PartBnOrBwRateSta")]
    public class PartBnOrBwRateStaManagement : DcsDataManagementViewBase {
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        public PartBnOrBwRateStaManagement() {
            this.Title = CommonUIStrings.DataManagementView_Title_PartBnOrBwRateStaManagement;
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartBnOrBwRateSta"
                };
            }
        }

        string addUrlParameter;
        protected override void OnExecutingAction(Silverlight.View.ActionPanelBase actionPanel, string uniqueId) {

        }

        protected override void OnExecutingQuery(Silverlight.View.QueryPanelBase queryPanel, Core.Model.FilterItem filterItem) {
            addUrlParameter = "";

            var newCompositeFilterItem = filterItem as CompositeFilterItem;
            if(newCompositeFilterItem == null)
                return;

            var branchCode = newCompositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "BranchCode");
            if(branchCode != null && branchCode.Value != null && !string.IsNullOrWhiteSpace(branchCode.Value.ToString()))
                addUrlParameter += "&lsSBranchCode=" + Uri.EscapeUriString("%" + branchCode.Value.ToString() + "%");

            var partsSalesCategoryId = newCompositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSalesCategoryId");
            if(partsSalesCategoryId != null && partsSalesCategoryId.Value != null && !string.IsNullOrWhiteSpace(partsSalesCategoryId.Value.ToString()))
                addUrlParameter += "&lsSPartsSalesCategoryId=" + Uri.EscapeUriString(partsSalesCategoryId.Value.ToString());
            var parameters = filterItem.ToQueryParameters();
            var queryTimeBegin = parameters.ContainsKey("QueryTime") ? ((System.DateTime?[])(parameters["QueryTime"]))[0] : null;
            var queryTimeEnd = parameters.ContainsKey("QueryTime") ? ((System.DateTime?[])(parameters["QueryTime"]))[1] : null;
            if(queryTimeBegin == null || queryTimeEnd == null) {
                UIHelper.ShowAlertMessage(CommonUIStrings.DataManagementView_Validation_Company_QueryTimeBegin);
                return;
            }
            queryTimeEnd = queryTimeEnd.Value.AddDays(1);
            addUrlParameter += "&lsSQueryTimeBegin=Date(" + queryTimeBegin.Value.Year + "," + queryTimeBegin.Value.Month + "," + queryTimeBegin.Value.Day + ")";
            addUrlParameter += "&lsSQueryTimeEnd=Date(" + queryTimeEnd.Value.Year + "," + queryTimeEnd.Value.Month + "," + queryTimeEnd.Value.Day + ")";

            ShellViewModel.Current.IsBusy = true;
            excelServiceClient.GetTokenAsync("psmsadmin", "psms-admin", "pmsrf.foton.com.cn:6400", "secEnterprise", 1, 100);
            excelServiceClient.GetTokenCompleted -= excelServiceClient_GetTokenCompleted;
            excelServiceClient.GetTokenCompleted += excelServiceClient_GetTokenCompleted;
        }

        private void excelServiceClient_GetTokenCompleted(object sender, GetTokenCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            if(string.IsNullOrEmpty(e.Result)) {
                UIHelper.ShowAlertMessage(CommonUIStrings.DataManagementView_Validation_Service);
            } else {
                string url = "http://pmsrf.foton.com.cn:8080/BOE/OpenDocument/opendoc/openDocument.jsp?token=" + e.Result + "&sType=wid&sPath=[FTDCS]&sDocName=PartBnOrBwRateSta" + addUrlParameter;
                HtmlPage.Window.Navigate(new Uri(url, UriKind.Absolute), "_blank");
            }
        }
    }
}
