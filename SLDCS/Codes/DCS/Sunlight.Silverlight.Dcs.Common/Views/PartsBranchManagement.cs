﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("PartsBaseInfo", "SparePart", "PartsBranch", ActionPanelKeys = new[] {
        "PartsBranch",CommonActionKeys.ADD_EDIT_ABANDON_PAUSE_RESUME_EXPORT
    })]
    public class PartsBranchManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditViewForPurchasing;
        private DataEditViewBase dataEditViewForMarketing;
        private DataEditViewBase dataEditViewForServiceing;
        private DataEditViewBase dataEditViewImport;
        private DataEditViewBase dataEditViewImportUpdate;
        private DataEditViewBase replaceApproveLimitDataEditView;
        private DataEditViewBase replaceStockMinimumDataEditView;
        private DataEditViewBase dataEditViewPartABCEdit;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private const string DATA_EDIT_VIEW_MARKETING = "_DataEditViewMarketing_";
        private const string DATA_EDIT_VIEW_PURCHASING = "_DataEditViewPurchasing_";
        private const string DATA_EDIT_VIEW_SERVICEING = "_DataEditViewServiceing_";
        private const string DATA_EDIT_VIEW_IMPORT = "_DataEditViewImport_";
        private const string DATA_EDIT_VIEW_IMPORT_UPDATE = "_DataEditViewImportUpdate_";
        private const string REPLACE_APPROVE_LIMIT_DATA_EDIT_VIEW = "_replaceApproveLimitDataEditView_";
        private const string REPLACE_APPROVE_STOCKMINIMUM_DATA_EDIT_VIEW = "_replaceStockMinimumDataEditView_";

        private const string DATA_EDIT_VIEW_REPLACEISORDERABLE = "_DataEditViewIsOrderable_";
        private const string DATA_EDIT_VIEW_RELPACEISSALABLEANDDIRECTSUPPLY = "_DataEditViewIsSalableAndDirectSupply_";
        private const string DATA_EDIT_VIEW_REPLACEINCREASERATEGROUP = "_DataEditViewIncreaseRateGroup_";
        private const string DATA_EDIT_VIEW_REPLACEPARTSWARRANTYCATEGORY = "_DataEditViewPartsWarrantyCategory_";
        private const string DATA_EDIT_VIEW_REPLACEPARTSRETURNPOLICY = "_DataEditViewPartsReturnPolicy_";
        private const string DATA_EDIT_VIEW_REPLACELOSSTYPEANDSTOCKLIMIT = "_DataEditViewLossTypeAndStockLimit_";
        private const string DATA_EDIT_VIEW_REPLACEREMARK = "_DataEditViewRemark_";
        private const string DATA_EDIT_VIEW_REPLACERE_REPLACE_REPAIR_CLASSIFY = "_DataEditViewReplaceRepairClassify_";
        private const string DATA_EDIT_VIEW_PAUSEEDIT = "_DataEditViewPauseEdit_";
        private const string DATA_EDIT_VIEW_PARTABC = "_DataEditViewPartABCEdit_";


        public PartsBranchManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataManagementView_Title_PartsBranch;
        }
        //编辑销售属性
        public DataEditViewBase DataEditViewForMarketing {
            get {
                if(this.dataEditViewForMarketing == null) {
                    this.dataEditViewForMarketing = DI.GetDataEditView("PartsBranchForMarketing");
                    this.dataEditViewForMarketing.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditViewForMarketing.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewForMarketing;
            }
        }
        //批量导入配件ABC类型
        public DataEditViewBase DataEditViewPartABCEdit
        {
            get
            {
                if (this.dataEditViewPartABCEdit == null)
                {
                    this.dataEditViewPartABCEdit = DI.GetDataEditView("PartsBranchForReplacePartABC");
                    this.dataEditViewPartABCEdit.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditViewPartABCEdit.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewPartABCEdit;
            }
        }

        private DataEditViewBase ReplaceStockMinimumDataEditView {
            get {
                if(this.replaceStockMinimumDataEditView == null) {
                    this.replaceStockMinimumDataEditView = DI.GetDataEditView("ReplaceStockMinimum");
                    this.replaceStockMinimumDataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.replaceStockMinimumDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.replaceStockMinimumDataEditView;
            }
        }
        private DataEditViewBase ReplaceApproveLimitDataEditView {
            get {
                if(this.replaceApproveLimitDataEditView == null) {
                    this.replaceApproveLimitDataEditView = DI.GetDataEditView("ReplaceApproveLimit");
                    this.replaceApproveLimitDataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.replaceApproveLimitDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.replaceApproveLimitDataEditView;
            }
        }

        //编辑服务属性
        public DataEditViewBase DataEditViewForServiceing {
            get {
                if(this.dataEditViewForServiceing == null) {
                    this.dataEditViewForServiceing = DI.GetDataEditView("PartsBranchForServiceing");
                    this.dataEditViewForServiceing.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditViewForServiceing.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewForServiceing;
            }
        }
        //编辑采购属性
        public DataEditViewBase DataEditViewForPurchasing {
            get {
                if(this.dataEditViewForPurchasing == null) {
                    this.dataEditViewForPurchasing = DI.GetDataEditView("PartsBranchForPurchasing");
                    this.dataEditViewForPurchasing.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditViewForPurchasing.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewForPurchasing;
            }
        }
        //导入
        private DataEditViewBase DataEditViewImport {
            get {
                if(this.dataEditViewImport == null) {
                    this.dataEditViewImport = DI.GetDataEditView("PartsBranchForImport");
                    this.dataEditViewImport.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewImport;
            }
        }
        //导入修改
        private DataEditViewBase DataEditViewImportUpdate {
            get {
                if(this.dataEditViewImportUpdate == null) {
                    this.dataEditViewImportUpdate = DI.GetDataEditView("PartsBranchForImportUpdate");
                    this.dataEditViewImportUpdate.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewImportUpdate;
            }
        }
        //批量替换可否采购
        private DataEditViewBase dataEditViewIsOrderable;
        private DataEditViewBase DataEditViewIsOrderable {
            get {
                if(this.dataEditViewIsOrderable == null) {
                    this.dataEditViewIsOrderable = DI.GetDataEditView("PartsBranchForReplaceIsOrderable");
                    this.dataEditViewIsOrderable.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewIsOrderable;
            }
        }
        //批量替换可否销售及直供
        private DataEditViewBase dataEditViewIsSalableAndDirectSupply;
        private DataEditViewBase DataEditViewIsSalableAndDirectSupply {
            get {
                if(this.dataEditViewIsSalableAndDirectSupply == null) {
                    this.dataEditViewIsSalableAndDirectSupply = DI.GetDataEditView("PartsBranchForReplaceIsSalableAndDirectSupply");
                    this.dataEditViewIsSalableAndDirectSupply.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewIsSalableAndDirectSupply;
            }
        }
        //批量替换价格类型
        private DataEditViewBase dataEditViewIncreaseRateGroup;
        private DataEditViewBase DataEditViewIncreaseRateGroup {
            get {
                if(this.dataEditViewIncreaseRateGroup == null) {
                    this.dataEditViewIncreaseRateGroup = DI.GetDataEditView("PartsBranchForReplaceIncreaseRateGroup");
                    this.dataEditViewIncreaseRateGroup.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewIncreaseRateGroup;
            }
        }
        //批量替换保险分类
        private DataEditViewBase dataEditViewPartsWarrantyCategory;
        private DataEditViewBase DataEditViewPartsWarrantyCategory {
            get {
                if(this.dataEditViewPartsWarrantyCategory == null) {
                    this.dataEditViewPartsWarrantyCategory = DI.GetDataEditView("PartsBranchForReplacePartsWarrantyCategory");
                    this.dataEditViewPartsWarrantyCategory.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewPartsWarrantyCategory;
            }
        }
        //批量替换旧件返回政策
        private DataEditViewBase dataEditViewPartsReturnPolicy;
        private DataEditViewBase DataEditViewPartsReturnPolicy {
            get {
                if(this.dataEditViewPartsReturnPolicy == null) {
                    this.dataEditViewPartsReturnPolicy = DI.GetDataEditView("PartsBranchForReplacePartsReturnPolicy");
                    this.dataEditViewPartsReturnPolicy.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewPartsReturnPolicy;
            }
        }
        //批量替换损耗类型及高低限
        private DataEditViewBase dataEditViewLossTypeAndStockLimit;
        private DataEditViewBase DataEditViewLossTypeAndStockLimit {
            get {
                if(this.dataEditViewLossTypeAndStockLimit == null) {
                    this.dataEditViewLossTypeAndStockLimit = DI.GetDataEditView("PartsBranchForReplaceLossTypeAndStockLimit");
                    this.dataEditViewLossTypeAndStockLimit.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewLossTypeAndStockLimit;
            }
        }
        //批量替换备注
        private DataEditViewBase dataEditViewRemark;
        private DataEditViewBase DataEditViewRemark {
            get {
                if(this.dataEditViewRemark == null) {
                    this.dataEditViewRemark = DI.GetDataEditView("PartsBranchForReplaceRemark");
                    this.dataEditViewRemark.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewRemark;
            }
        }

        //批量导入修改配件保修分类
        private DataEditViewBase dataEditViewReplaceRepairClassify;
        private DataEditViewBase DataEditViewReplaceRepairClassify {
            get {
                if(this.dataEditViewReplaceRepairClassify == null) {
                    this.dataEditViewReplaceRepairClassify = DI.GetDataEditView("PartsBranchForReplaceRepairClassify");
                    this.dataEditViewReplaceRepairClassify.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewReplaceRepairClassify;
            }
        }

        //批量作废，
        private DataEditViewBase dataEditViewPauseEdit;
        private DataEditViewBase DataEditViewPauseEdit {
            get {
                if(this.dataEditViewPauseEdit == null) {
                    this.dataEditViewPauseEdit = DI.GetDataEditView("PartsBranchForPauseEdit");
                    this.dataEditViewPauseEdit.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewPauseEdit;
            }
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsBranchNew"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsBranch");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_PURCHASING, () => this.DataEditViewForPurchasing);
            this.RegisterView(DATA_EDIT_VIEW_MARKETING, () => this.DataEditViewForMarketing);
            this.RegisterView(DATA_EDIT_VIEW_SERVICEING, () => this.DataEditViewForServiceing);
            this.RegisterView(DATA_EDIT_VIEW_IMPORT, () => this.DataEditViewImport);
            this.RegisterView(DATA_EDIT_VIEW_IMPORT_UPDATE, () => this.DataEditViewImportUpdate);
            this.RegisterView(DATA_EDIT_VIEW_RELPACEISSALABLEANDDIRECTSUPPLY, () => this.DataEditViewIsSalableAndDirectSupply);
            this.RegisterView(DATA_EDIT_VIEW_REPLACEISORDERABLE, () => this.DataEditViewIsOrderable);
            this.RegisterView(DATA_EDIT_VIEW_REPLACEPARTSRETURNPOLICY, () => this.DataEditViewPartsReturnPolicy);
            this.RegisterView(DATA_EDIT_VIEW_REPLACEREMARK, () => this.DataEditViewRemark);
            this.RegisterView(DATA_EDIT_VIEW_REPLACEINCREASERATEGROUP, () => this.DataEditViewIncreaseRateGroup);
            this.RegisterView(DATA_EDIT_VIEW_REPLACELOSSTYPEANDSTOCKLIMIT, () => this.DataEditViewLossTypeAndStockLimit);
            this.RegisterView(DATA_EDIT_VIEW_REPLACEPARTSWARRANTYCATEGORY, () => this.DataEditViewPartsWarrantyCategory);
            this.RegisterView(DATA_EDIT_VIEW_PAUSEEDIT, () => this.DataEditViewPauseEdit);
            this.RegisterView(REPLACE_APPROVE_LIMIT_DATA_EDIT_VIEW, () => this.ReplaceApproveLimitDataEditView);
            this.RegisterView(DATA_EDIT_VIEW_REPLACERE_REPLACE_REPAIR_CLASSIFY, () => this.DataEditViewReplaceRepairClassify);
            this.RegisterView(DATA_EDIT_VIEW_PARTABC, () => this.DataEditViewPartABCEdit);
            this.RegisterView(REPLACE_APPROVE_STOCKMINIMUM_DATA_EDIT_VIEW, () => this.ReplaceStockMinimumDataEditView);
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewForPurchasing = null;
            this.dataEditViewForMarketing = null;
            this.dataEditViewForServiceing = null;
            this.dataEditViewImport = null;
            this.dataEditViewImportUpdate = null;
            this.replaceApproveLimitDataEditView = null;
            this.dataEditViewPartABCEdit = null;
            this.replaceStockMinimumDataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var composite = filterItem as CompositeFilterItem;
            if(composite != null) {
                composite.Filters.Add(new FilterItem {
                    MemberName = "PartsSalesCategoryId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                    Value = 221
                });
                ClientVar.ConvertTime(composite);
            }

            this.DataGridView.FilterItem = composite;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<VirtualPartsBranch>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.Load(domainContext.GetPartsBranchesQuery().Where(r => r.Id == entity.Id), LoadBehavior.RefreshCurrent, loadOp => {
                                if(loadOp.HasError)
                                    return;
                                if(loadOp.Entities != null && loadOp.Entities.Any()) {
                                    var partsBranch = loadOp.Entities.FirstOrDefault();
                                    if(partsBranch.Can作废配件营销信息)
                                        partsBranch.作废配件营销信息();
                                    domainContext.SubmitChanges(submitOp => {
                                        if(submitOp.HasError) {
                                            if(!submitOp.IsErrorHandled)
                                                submitOp.MarkErrorAsHandled();
                                            DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                            domainContext.RejectChanges();
                                            return;
                                        }
                                        UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_AbandonSuccess);
                                        this.DataGridView.ExecuteQueryDelayed();
                                        this.CheckActionsCanExecute();
                                    }, null);
                                }
                            }, null);

                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<VirtualPartsBranch>().Select(e => e.Id).ToArray();
                        this.ExportPartsBranch(ids, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,null,null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var partCode = filterItem.Filters.Single(r => r.MemberName == "PartCode").Value as string;
                        var referenceCode = filterItem.Filters.Single(r => r.MemberName == "ReferenceCode").Value as string;
                        var partName = filterItem.Filters.Single(r => r.MemberName == "PartName").Value as string;
                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        //var plannedPriceCategory = filterItem.Filters.Single(e => e.MemberName == "PlannedPriceCategory").Value as int?;
                        //var partAbc = filterItem.Filters.Single(e => e.MemberName == "PartABC").Value as int?;
                        //var partsWarrantyCategoryName = filterItem.Filters.Single(r => r.MemberName == "PartsWarrantyCategoryName").Value as string;
                       // var productLifeCycle = filterItem.Filters.Single(e => e.MemberName == "ProductLifeCycle").Value as int?;
                        //var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                        var partsSalePriceIncreaseRateGroupCode = filterItem.Filters.Single(r => r.MemberName == "GroupCode").Value as string;
                        var partsSalePriceIncreaseRateGroupName = filterItem.Filters.Single(r => r.MemberName == "GroupName").Value as string;
                        //var partsEnglishName = filterItem.Filters.Single(r => r.MemberName == "EnglishName").Value as string;
                        //var partsReturnPolicy = filterItem.Filters.Single(e => e.MemberName == "PartsReturnPolicy").Value as int?;
                        var isSalable = filterItem.Filters.Single(e => e.MemberName == "IsSalable").Value as bool?;
                        var isOrderable = filterItem.Filters.Single(e => e.MemberName == "IsOrderable").Value as bool?;
                        var isDirectSupply = filterItem.Filters.Single(e => e.MemberName == "IsDirectSupply").Value as bool?;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;                      
                        DateTime? modifyTimeBegin = null;
                        DateTime? modifyTimeEnd = null;                      

                        foreach (var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)))
                        {
                            var dateTime = filter as CompositeFilterItem;
                            if (dateTime != null)
                            {
                                if (dateTime.Filters.First().MemberName == "CreateTime")
                                {
                                    createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                                if (dateTime.Filters.First().MemberName == "ModifyTime")
                                {
                                    modifyTimeBegin = dateTime.Filters.First(r => r.MemberName == "ModifyTime").Value as DateTime?;
                                    modifyTimeEnd = dateTime.Filters.Last(r => r.MemberName == "ModifyTime").Value as DateTime?;
                                }
                            }
                        }
                        this.ExportPartsBranch(null, partCode, partName, referenceCode, null, null, null, status, 221, null, createTimeBegin, createTimeEnd, partsSalePriceIncreaseRateGroupCode, partsSalePriceIncreaseRateGroupName, null, isSalable, isOrderable, isDirectSupply, null, modifyTimeBegin, modifyTimeEnd);
                    }
                    break;
                case "Purchasing"://编辑采购属性
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        this.DataEditViewForPurchasing.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    }
                    this.SwitchViewTo(DATA_EDIT_VIEW_PURCHASING);
                    break;
                case "Serviceing"://编辑服务属性
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        this.DataEditViewForServiceing.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    }
                    this.SwitchViewTo(DATA_EDIT_VIEW_SERVICEING);
                    break;
                case "Marketing"://编辑销售属性
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        this.DataEditViewForMarketing.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    }
                    this.SwitchViewTo(DATA_EDIT_VIEW_MARKETING);
                    break;
                case "Import":
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORT);
                    break;
                case "ImportEdit":
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORT_UPDATE);
                    break;
                case "ReplaceIsOrderable":
                    this.SwitchViewTo(DATA_EDIT_VIEW_REPLACEISORDERABLE);
                    break;
                case "ReplaceIsSalableAndDirectSupply":
                    this.SwitchViewTo(DATA_EDIT_VIEW_RELPACEISSALABLEANDDIRECTSUPPLY);
                    break;
                case "ReplaceIncreaseRateGroup":
                    this.SwitchViewTo(DATA_EDIT_VIEW_REPLACEINCREASERATEGROUP);
                    break;
                case "ReplacePartsWarrantyCategory":
                    this.SwitchViewTo(DATA_EDIT_VIEW_REPLACEPARTSWARRANTYCATEGORY);
                    break;
                case "ReplacePartsReturnPolicy":
                    this.SwitchViewTo(DATA_EDIT_VIEW_REPLACEPARTSRETURNPOLICY);
                    break;
                case "ReplaceLossTypeAndStockLimit":
                    this.SwitchViewTo(DATA_EDIT_VIEW_REPLACELOSSTYPEANDSTOCKLIMIT);
                    break;
                case "ReplaceRemark":
                    this.SwitchViewTo(DATA_EDIT_VIEW_REPLACEREMARK);
                    break;
                case "PrintLabel":
                    var selected = this.DataGridView.SelectedEntities.Cast<VirtualPartsBranch>();
                    if(selected == null)
                        return;
                    PartsBranchs.Clear();
                    var detail = "";
                    foreach(var partsBranch in selected) {
                        if(detail == "") {
                            detail = partsBranch.PartId.ToString(CultureInfo.InvariantCulture);
                            continue;
                        }
                        detail = detail + "," + partsBranch.PartId;
                    }
                    BasePrintWindow printWindow = new PartsBranchPrintLabelPrintWindow {
                        Header = CommonUIStrings.Action_Title_Batch_Print_Label,
                        PartsBranch = detail
                    };
                    printWindow.ShowDialog();
                    break;
                case "UsedPrintLabel":
                    var usedSelected = this.DataGridView.SelectedEntities.Cast<VirtualPartsBranch>();
                    if(usedSelected == null)
                        return;
                    PartsBranchs.Clear();
                    var usedDetail = "";
                    foreach(var partsBranch in usedSelected) {
                        if(usedDetail == "") {
                            usedDetail = partsBranch.PartId.ToString(CultureInfo.InvariantCulture);
                            continue;
                        }
                        usedDetail = usedDetail + "," + partsBranch.PartId;
                    }
                    BasePrintWindow usedPrintWindow = new PartsBranchPrintLabel4PrintWindow {
                        Header = CommonUIStrings.Action_Title_Batch_Print_Label,
                        PartsBranch = usedDetail
                    };
                    usedPrintWindow.ShowDialog();
                    break;
                case "WmsLabel":
                case "UsedWmsLabel":
                case "StandardPrintLabel":
                case "WmsLabelOM"://wms标签打印欧曼
                    var spareParts = this.DataGridView.SelectedEntities.Cast<VirtualPartsBranch>().Select(r => r.PartId).ToArray();
                    if(uniqueId.Equals("WmsLabel")) {
                        this.SupplierPartsTagPrinttingDataEditView.SetObjectToEditById(spareParts);
                        LabelPrintWindow.ShowDialog();
                    } else if(uniqueId.Equals("UsedWmsLabel")) {
                        this.usedSupplierPartsTagPrinttingDataEditView.SetObjectToEditById(spareParts);
                        UsedLabelPrintWindow.ShowDialog();
                    } else if(uniqueId.Equals("StandardPrintLabel")) {
                        this.StandardPartsTagPrinttingDataEditView.SetObjectToEditById(spareParts);
                        StandardPrintLabelPrintWindow.ShowDialog();
                    } else if(uniqueId.Equals("WmsLabelOM")) {
                        this.OMWMSPrintLable.SetObjectToEditById(spareParts);
                        OMWMSPrintLableWindow.ShowDialog();
                    }
                    break;
                case CommonActionKeys.PAUSE:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Pause, () => {
                        var entity1 = this.DataGridView.SelectedEntities.Cast<VirtualPartsBranch>().SingleOrDefault();
                        if(entity1 == null)
                            return;
                        try {
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.Load(domainContext.GetPartsBranchesQuery().Where(r => r.Id == entity1.Id), LoadBehavior.RefreshCurrent, loadOp => {
                                if(loadOp.Entities != null && loadOp.Entities.Any()) {
                                    var partsBranch = loadOp.Entities.FirstOrDefault();
                                    if(partsBranch.Can停用配件营销信息)
                                        partsBranch.停用配件营销信息();
                                    domainContext.SubmitChanges(submitOp => {
                                        if(submitOp.HasError) {
                                            if(!submitOp.IsErrorHandled)
                                                submitOp.MarkErrorAsHandled();
                                            DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                            domainContext.RejectChanges();
                                            return;
                                        }
                                        UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_PauseSuccess);
                                        this.DataGridView.ExecuteQueryDelayed();
                                        this.CheckActionsCanExecute();
                                    }, null);
                                }
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.RESUME:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Resume, () => {
                        var entity2 = this.DataGridView.SelectedEntities.Cast<VirtualPartsBranch>().SingleOrDefault();
                        if(entity2 == null)
                            return;
                        try {
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.Load(domainContext.GetPartsBranchesQuery().Where(r => r.Id == entity2.Id), LoadBehavior.RefreshCurrent, loadOp => {
                                if(loadOp.Entities != null && loadOp.Entities.Any()) {
                                    var partsBranch = loadOp.Entities.FirstOrDefault();
                                    if(partsBranch.Can恢复配件营销信息)
                                        partsBranch.恢复配件营销信息();
                                    domainContext.SubmitChanges(submitOp => {
                                        if(submitOp.HasError) {
                                            if(!submitOp.IsErrorHandled)
                                                submitOp.MarkErrorAsHandled();
                                            DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                            domainContext.RejectChanges();
                                            return;
                                        }
                                        UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_ResumeSuccess);
                                        this.DataGridView.ExecuteQueryDelayed();
                                        this.CheckActionsCanExecute();
                                    }, null);
                                }
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.REPLACE_REPAIR_CLASSIFY:
                    this.SwitchViewTo(DATA_EDIT_VIEW_REPLACERE_REPLACE_REPAIR_CLASSIFY);
                    break;
                case "PauseEdit":
                case "ResumeEdit":
                case "AbandonEdit":
                    this.DataEditViewPauseEdit.SetObjectToEditById(uniqueId);
                    this.SwitchViewTo(DATA_EDIT_VIEW_PAUSEEDIT);
                    break;
                case "ReplaceApproveLimit":
                    this.SwitchViewTo(REPLACE_APPROVE_LIMIT_DATA_EDIT_VIEW);
                    break;
                case "ReplacePartABC":
                    this.SwitchViewTo(DATA_EDIT_VIEW_PARTABC);
                    break;
                case "ReplaceStockMinimum":
                    this.SwitchViewTo(REPLACE_APPROVE_STOCKMINIMUM_DATA_EDIT_VIEW);
                    break;
            }
        }

        private ObservableCollection<PartsBranch> partsBranchs;
        public ObservableCollection<PartsBranch> PartsBranchs {
            get {
                return partsBranchs ?? (this.partsBranchs = new ObservableCollection<PartsBranch>());
            }
        }

        //标签打印（带标准）
        private RadWindow standardPrintLabelPrintWindow;
        private DataEditViewBase standardPartsTagPrinttingDataEditView;

        private DataEditViewBase StandardPartsTagPrinttingDataEditView {
            get {
                return this.standardPartsTagPrinttingDataEditView ?? (this.standardPartsTagPrinttingDataEditView = DI.GetDataEditView("StandardPartsTagPrintting"));
            }
        }

        private RadWindow StandardPrintLabelPrintWindow {
            get {
                return this.standardPrintLabelPrintWindow ?? (this.standardPrintLabelPrintWindow = new RadWindow {
                    Content = this.StandardPartsTagPrinttingDataEditView,
                    Header = CommonUIStrings.DataManagementView_Header_PartsBranchManagement,
                    WindowState = WindowState.Normal,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                    AllowDrop = false,
                    ResizeMode = ResizeMode.NoResize,
                    CanClose = true
                });
            }
        }
        //onwms标签打印
        private DataEditViewBase oMWMSPrintLable;
        private RadWindow oMWMSPrintLableWindow;


        private DataEditViewBase OMWMSPrintLable {
            get {
                return this.oMWMSPrintLable ?? (this.oMWMSPrintLable = DI.GetDataEditView("OMWMSPrintLable"));
            }
        }

        private RadWindow OMWMSPrintLableWindow {
            get {
                return this.oMWMSPrintLableWindow ?? (this.oMWMSPrintLableWindow = new RadWindow {
                    Content = this.OMWMSPrintLable,
                    Header = "欧曼Wms标签打印",
                    WindowState = WindowState.Normal,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                    AllowDrop = false,
                    ResizeMode = ResizeMode.NoResize,
                    CanClose = true
                });
            }
        }
        //Wms标签打印
        private RadWindow labelPrintWindow;
        private DataEditViewBase supplierPartsTagPrinttingDataEditView;

        private DataEditViewBase SupplierPartsTagPrinttingDataEditView {
            get {
                return this.supplierPartsTagPrinttingDataEditView ?? (this.supplierPartsTagPrinttingDataEditView = DI.GetDataEditView("SupplierPartsTagPrintting"));
            }
        }
        private RadWindow LabelPrintWindow {
            get {
                return this.labelPrintWindow ?? (this.labelPrintWindow = new RadWindow {
                    Content = this.SupplierPartsTagPrinttingDataEditView,
                    Header = CommonUIStrings.DataManagementView_Header_PartsBranchManagement,
                    WindowState = WindowState.Normal,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                    AllowDrop = false,
                    ResizeMode = ResizeMode.NoResize,
                    CanClose = true
                });
            }
        }

        //Wms标签打印旧
        private RadWindow usedlabelPrintWindow;
        private DataEditViewBase usedsupplierPartsTagPrinttingDataEditView;

        private DataEditViewBase usedSupplierPartsTagPrinttingDataEditView {
            get {
                return this.usedsupplierPartsTagPrinttingDataEditView ?? (this.usedsupplierPartsTagPrinttingDataEditView = DI.GetDataEditView("UsedSupplierPartsTagPrintting"));
            }
        }
        private RadWindow UsedLabelPrintWindow {
            get {
                return this.usedlabelPrintWindow ?? (this.usedlabelPrintWindow = new RadWindow {
                    Content = this.usedSupplierPartsTagPrinttingDataEditView,
                    Header = CommonUIStrings.DataManagementView_Header_PartsBranchManagement,
                    WindowState = WindowState.Normal,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                    AllowDrop = false,
                    ResizeMode = ResizeMode.NoResize,
                    CanClose = true
                });
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case "Purchasing":
                case "Serviceing":
                case "Marketing":
                case "ReplaceStockMinimum":
                    return true;
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case "WmsLabel":
                case "UsedWmsLabel":
                case "StandardPrintLabel":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<VirtualPartsBranch>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsBaseDataStatus.有效;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.IMPORT:
                case "ImportEdit":
                case "ReplaceIsOrderable":
                case "ReplaceIsSalableAndDirectSupply":
                case "ReplaceIncreaseRateGroup":
                case "ReplacePartsWarrantyCategory":
                case "ReplacePartsReturnPolicy":
                case "ReplaceLossTypeAndStockLimit":
                case "ReplaceRemark":
                case "PauseEdit":
                case "ResumeEdit":
                case "AbandonEdit":
                case "ReplaceApproveLimit":
                case CommonActionKeys.REPLACE_REPAIR_CLASSIFY:
                case "ReplacePartABC":
                    return true;
                case "PrintLabel":
                case "UsedPrintLabel":
                case "WmsLabelOM":
                
                    return this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any();
                case CommonActionKeys.PAUSE:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var pauseentities = this.DataGridView.SelectedEntities.Cast<VirtualPartsBranch>().ToArray();
                    if(pauseentities.Length != 1)
                        return false;
                    return pauseentities[0].Status == (int)DcsMasterDataStatus.有效;

                case CommonActionKeys.RESUME:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var resumeentities = this.DataGridView.SelectedEntities.Cast<VirtualPartsBranch>().ToArray();
                    if(resumeentities.Length != 1)
                        return false;
                    return resumeentities[0].Status == (int)DcsMasterDataStatus.停用;
                default:
                    return false;
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsBranchNew"
                };
            }
        }

        private void ExportPartsBranch(int[] ids, string partCode, string partName, string referenceCode, int? partAbc, string partsWarrantyCategoryName, int? productLifeCycle, int? status, int? partsSalesCategoryId, int? plannedPriceCategory, DateTime? createTimeBegin, DateTime? createTimeEnd, string partsSalePriceIncreaseRateGroupCode, string partsSalePriceIncreaseRateGroupName, int? partsReturnPolicy, bool? isSalable, bool? isOrderable, bool? isDirectSupply, string partsEnglishName,DateTime? modifyTimeBegin,DateTime? modifyTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportForPartsBranchAsync(ids, BaseApp.Current.CurrentUserData.EnterpriseId, partCode, partName, partAbc, partsWarrantyCategoryName, productLifeCycle, status, partsSalesCategoryId, plannedPriceCategory, referenceCode, createTimeBegin, createTimeEnd, partsSalePriceIncreaseRateGroupCode, partsSalePriceIncreaseRateGroupName, partsReturnPolicy, isSalable, isOrderable, isDirectSupply, partsEnglishName, modifyTimeBegin, modifyTimeEnd);
            this.excelServiceClient.ExportForPartsBranchCompleted -= excelServiceClient_ExportForPartsBranchCompleted;
            this.excelServiceClient.ExportForPartsBranchCompleted += excelServiceClient_ExportForPartsBranchCompleted;
        }

        private void excelServiceClient_ExportForPartsBranchCompleted(object sender, ExportForPartsBranchCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

    }
}
