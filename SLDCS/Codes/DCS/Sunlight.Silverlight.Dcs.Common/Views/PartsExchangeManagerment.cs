﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Common.Views.DataGrid;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("PartsBaseInfo", "SparePart", "PartsExchange", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_EXPORT_IMPORT,"PartsExchange"
    })]
    public class PartsExchangeManagerment : DcsDataManagementViewBase {
        private PartsExchangeDataEditView dataEditView;
        private DataEditViewBase importDataEditView;
        private PartsExchangeDataGridView dataGridView;
        private const string IMPORT_DATA_EDIT_VIEW = "_IMPORT_DATA_EDIT_VIEW_";
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public PartsExchangeManagerment() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataManagementView_Title_PartsExchange;
        }

        private PartsExchangeDataGridView DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = new PartsExchangeDataGridView());
            }
        }

        private PartsExchangeDataEditView DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsExchange") as PartsExchangeDataEditView;
                    //this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.CustomEditSubmitted += dataEditView_CustomEditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.importDataEditView = null;
        }
        private void dataEditView_CustomEditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }
        private DataEditViewBase ImportDataEditView {
            get {
                if(this.importDataEditView == null) {
                    this.importDataEditView = DI.GetDataEditView("PartsExchangeForImport");
                    this.importDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.importDataEditView;
            }
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(IMPORT_DATA_EDIT_VIEW, () => this.ImportDataEditView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsExchange"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    this.DataEditView.CreateObjectToEdit<VirtualPartsExchange>();
                    this.DataEditView.textBoxExchangeCode.Text = string.Empty;
                    this.DataEditView.status = 0;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<VirtualPartsExchange>().SingleOrDefault();
                        if(entity == null)
                            return;
                        entity.Status = (int)DcsBaseDataStatus.作废;
                        try {
                            if(entity.Can作废配件互换信息虚拟)
                                entity.作废配件互换信息虚拟();
                            this.ExecuteSerivcesMethod(CommonUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "ExportByPrice":
                case CommonActionKeys.EXPORT:
                    ShellViewModel.Current.IsBusy = true;
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        if(uniqueId == CommonActionKeys.EXPORT) {
                            this.DataGridView.ExportSelectionLocalData("配件互换信息导出");
                            ShellViewModel.Current.IsBusy = false;
                        } else {
                            this.ExportPartsExchangeByPrice(this.DataGridView.SelectedEntities.Cast<VirtualPartsExchange>().Select(r => r.Id).ToArray(), null, null, null, null, null, null);
                        }
                    } else {
                        var filterItem1 = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem1 == null)
                            return;
                        var exchangeCode = filterItem1.Filters.Single(r => r.MemberName == "ExchangeCode").Value as string;
                        var partCode = filterItem1.Filters.Single(r => r.MemberName == "PartCode").Value as string;
                        var partName = filterItem1.Filters.Single(r => r.MemberName == "PartName").Value as string;
                        var exGroupCode = filterItem1.Filters.Single(r => r.MemberName == "ExGroupCode").Value as string;
                        var status = filterItem1.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var createTime = filterItem1.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        if(uniqueId == CommonActionKeys.EXPORT) {
                            this.dcsDomainContext.ExportPartsExchange(null, exchangeCode, exGroupCode, partCode, partName, status, createTimeBegin, createTimeEnd, loadOp1 => {
                                if(loadOp1.HasError)
                                    return;
                                if(loadOp1.Value == null || string.IsNullOrEmpty(loadOp1.Value)) {
                                    UIHelper.ShowNotification(loadOp1.Value);
                                }
                                if(loadOp1.Value != null && !string.IsNullOrEmpty(loadOp1.Value))
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp1.Value));
                                ShellViewModel.Current.IsBusy = false;
                            }, null);
                        } else {
                            this.ExportPartsExchangeByPrice(null, exchangeCode, partCode, partName, status, createTimeBegin, createTimeEnd);
                        }
                    }

                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(IMPORT_DATA_EDIT_VIEW);
                    break;
            }
        }
        //导出清单
        private void ExportPartsExchangeByPrice(int[] ids, string exchangeCode,string partCode, string partName, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            excelServiceClient.ExportPartsExchangeByPriceAsync(ids, exchangeCode, partCode, partName, status, createTimeBegin, createTimeEnd);
            excelServiceClient.ExportPartsExchangeByPriceCompleted -= excelServiceClient_ExportPartsExchangeByPriceCompleted;
            excelServiceClient.ExportPartsExchangeByPriceCompleted += excelServiceClient_ExportPartsExchangeByPriceCompleted;
        }

        void excelServiceClient_ExportPartsExchangeByPriceCompleted(object sender, ExportPartsExchangeByPriceCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                case CommonActionKeys.IMPORT:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<VirtualPartsExchange>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsBaseDataStatus.有效;
                case CommonActionKeys.EXPORT:
                case "ExportByPrice":
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }

    }
}