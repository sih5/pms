﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("Report", "SparePart", "PartsInboundHistory")]
    public class PartsInboundHistoryForReportManagement : DcsDataManagementViewBase {
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public PartsInboundHistoryForReportManagement() {
            this.Title = CommonUIStrings.DataManagementView_Title_PartsInboundHistoryForReportManagement;
        }

        string addUrlParameter;
        private void excelServiceClient_GetTokenCompleted(object sender, GetTokenCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            if(string.IsNullOrEmpty(e.Result)) {
                UIHelper.ShowAlertMessage(CommonUIStrings.DataManagementView_Validation_Service);
            } else {
                string url = "http://pmsrf.foton.com.cn:8080/BOE/OpenDocument/opendoc/openDocument.jsp?token=" + e.Result + "&sType=wid&sPath=[FTDCS]&sDocName=PartsInboundHistory" + addUrlParameter;
                HtmlPage.Window.Navigate(new Uri(url, UriKind.Absolute), "_blank");
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsInboundHistoryForReport"
                };
            }
        }

        protected override void OnExecutingAction(Silverlight.View.ActionPanelBase actionPanel, string uniqueId) {

        }

        protected override void OnExecutingQuery(Silverlight.View.QueryPanelBase queryPanel, FilterItem filterItem) {
            this.addUrlParameter = "";

            var newCompositeFilterItem = filterItem as CompositeFilterItem;
            if(newCompositeFilterItem == null)
                return;

            var code = newCompositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "Code");
            if(code != null && code.Value != null && !string.IsNullOrWhiteSpace(code.Value.ToString()))
                addUrlParameter += "&lsSCode=" + Uri.EscapeUriString(code.Value.ToString());

            var status = newCompositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "Status");
            if(status != null && status.Value != null && !string.IsNullOrWhiteSpace(status.Value.ToString()))
                addUrlParameter += "&lsSStatus=" + Uri.EscapeUriString(status.Value.ToString());

            var name = newCompositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "Name");
            if(name != null && name.Value != null && !string.IsNullOrWhiteSpace(name.Value.ToString()))
                addUrlParameter += "&lsSName=" + Uri.EscapeUriString(name.Value.ToString());

            if(newCompositeFilterItem.Filters.Any(filter => filter.GetType() == typeof(CompositeFilterItem))) {
                var createTimeFilter = newCompositeFilterItem.Filters.Single(filter => filter.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                if(createTimeFilter == null)
                    return;
                var dateTime = createTimeFilter.Filters.ElementAt(0).Value as DateTime?;
                if(dateTime.HasValue)
                    this.addUrlParameter += "&lsSCreateTimeBegin=Date(" + dateTime.Value.Year + "," + dateTime.Value.Month + "," + dateTime.Value.Day + ")";
                dateTime = createTimeFilter.Filters.ElementAt(1).Value as DateTime?;
                if(dateTime.HasValue)
                    this.addUrlParameter += "&lsSCreateTimeEnd=Date(" + dateTime.Value.Year + "," + dateTime.Value.Month + "," + dateTime.Value.Day + ")";
            }

            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.GetTokenAsync("psmsadmin", "psms-admin", "pmsrf.foton.com.cn:6400", "secEnterprise", 1, 100);
            this.excelServiceClient.GetTokenCompleted -= excelServiceClient_GetTokenCompleted;
            this.excelServiceClient.GetTokenCompleted += excelServiceClient_GetTokenCompleted;
        }
    }
}
