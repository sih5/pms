﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsBaseInfo", "SparePart", "PartsPriceHistoryQuery", ActionPanelKeys = new[] { CommonActionKeys.EXPORT })]
    public class AgentsHistoryOutManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        public AgentsHistoryOutManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataManagementView_Title_AgentsHistoryOutManagement;
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsPriceHistoryQuery"));
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsPriceHistoryQuery"
                };
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var branchId = filterItem.Filters.Single(e => e.MemberName == "BranchId").Value as int?;
                    var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                    var partCode = filterItem.Filters.Single(r => r.MemberName == "PartCode").Value as string;
                    var partName = filterItem.Filters.Single(r => r.MemberName == "PartName").Value as string;
                    var isZeroSalesPrice = filterItem.Filters.Single(e => e.MemberName == "IsZeroSalesPrice").Value as int?;
                    var isZeroRetailPrice = filterItem.Filters.Single(e => e.MemberName == "IsZeroRetailPrice").Value as int?;
                    var isZeroPlannedPrice = filterItem.Filters.Single(e => e.MemberName == "IsZeroPlannedPrice").Value as int?;
                    var isZeroPurchasePrice = filterItem.Filters.Single(e => e.MemberName == "IsZeroPurchasePrice").Value as int?;
                    var supplierCode = filterItem.Filters.Single(r => r.MemberName == "SupplierCode").Value as string;
                    var supplierName = filterItem.Filters.Single(r => r.MemberName == "SupplierName").Value as string;
                
                    var compositeFilterItems = filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).ToArray();
                    DateTime? startCreateTime = null;
                    DateTime? endCreateTime = null;
                    DateTime? startSubmitTime = null;
                    DateTime? endSubmitTime = null;
                    foreach(var dateTimeFilterItem in compositeFilterItems) {
                        var dateTime = dateTimeFilterItem as CompositeFilterItem;
                        if(dateTime != null) {
                            if(dateTime.Filters.Any(r => r.MemberName == "Pcreatetime")) {
                                startCreateTime = dateTime.Filters.First(r => r.MemberName == "Pcreatetime").Value as DateTime?;
                                endCreateTime = dateTime.Filters.Last(r => r.MemberName == "Pcreatetime").Value as DateTime?;
                            }
                            if(dateTime.Filters.Any(r => r.MemberName == "Screatetime")) {
                                startSubmitTime = dateTime.Filters.First(r => r.MemberName == "Screatetime").Value as DateTime?;
                                endSubmitTime = dateTime.Filters.Last(r => r.MemberName == "Screatetime").Value as DateTime?;
                            }
                        }
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出配件价格查询(branchId, partsSalesCategoryId, partCode, partName, isZeroSalesPrice, isZeroRetailPrice, isZeroPlannedPrice, isZeroPurchasePrice, supplierCode, supplierName, startCreateTime, endCreateTime, startSubmitTime, endSubmitTime, loadOp => {
                        if(loadOp.HasError) {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }


        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();    
        }
    }
}
