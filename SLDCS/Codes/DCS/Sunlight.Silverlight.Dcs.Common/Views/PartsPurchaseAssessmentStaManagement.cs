﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Views {

    [PageMeta("Report", "Financial", "PartsPurchaseAssessmentSta")]
    public class PartsPurchaseAssessmentStaManagement : DcsDataManagementViewBase {
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        public PartsPurchaseAssessmentStaManagement() {
            Title = CommonUIStrings.DataManagementView_Title_PartsPurchaseAssessmentStaManagement;
        }

        string addUrlParameter;
        private void excelServiceClient_GetTokenCompleted(object sender, GetTokenCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            if(string.IsNullOrEmpty(e.Result)) {
                UIHelper.ShowAlertMessage(CommonUIStrings.DataManagementView_Validation_Service);
            } else {
                string url = "http://pmsrf.foton.com.cn:8080/BOE/OpenDocument/opendoc/openDocument.jsp?token=" + e.Result + "&sType=wid&sPath=[FTDCS]&sDocName=PartsPurchaseAssessmentSta" + addUrlParameter;
                HtmlPage.Window.Navigate(new Uri(url, UriKind.Absolute), "_blank");
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsPurchaseAssessmentSta"
                };
            }
        }

        protected override void OnExecutingAction(Silverlight.View.ActionPanelBase actionPanel, string uniqueId) {

        }

        protected override void OnExecutingQuery(Silverlight.View.QueryPanelBase queryPanel, FilterItem filterItem) {
            addUrlParameter = "";

            var newCompositeFilterItem = filterItem as CompositeFilterItem;
            if(newCompositeFilterItem == null)
                return;
            var queryTime = newCompositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "QueryTime");
            if(queryTime != null && queryTime.Value != null && !string.IsNullOrWhiteSpace(queryTime.Value.ToString())) {
                var dataArray = ClientVar.GetDateTimeArray(2015, 4);
                var a = dataArray[int.Parse(queryTime.Value.ToString())].Value + "-01";
                var data = Convert.ToDateTime(a);
                var timeLast = data.AddDays(-1);
                var timeEnd = data.AddMonths(1).AddDays(-1).Day;
                addUrlParameter += "&lsSTimeLast=" + timeLast.Year.ToString() + "-" + (timeLast.Month < 10 ? ("0" + timeLast.Month.ToString()) : timeLast.Month.ToString()) + "-" + timeLast.Day.ToString() + " 23:59:59";
                addUrlParameter += "&lsSTimeBegin=" + dataArray[int.Parse(queryTime.Value.ToString())].Value.ToString() + "-01 00:00:00";
                addUrlParameter += "&lsSTimeEnd=" + dataArray[int.Parse(queryTime.Value.ToString())].Value + "-" + timeEnd.ToString() + " 23:59:59";
            } else {
                UIHelper.ShowAlertMessage("请选择查询时间");
                return;
            }
            var partsSalesCategoryId = newCompositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSalesCateGoryId");
            if(partsSalesCategoryId != null && partsSalesCategoryId.Value != null && !string.IsNullOrWhiteSpace(partsSalesCategoryId.Value.ToString()))
                addUrlParameter += "&lsSPartsSalesCateGoryId=" + Uri.EscapeUriString(partsSalesCategoryId.Value.ToString());
            var type = newCompositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "Type");
            if(partsSalesCategoryId != null && type.Value != null && !string.IsNullOrWhiteSpace(type.Value.ToString()))
                addUrlParameter += "&lsSType=" + Uri.EscapeUriString(type.Value.ToString());
            var branchCode = newCompositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "BranchCode");
            if(branchCode != null && branchCode.Value != null && !string.IsNullOrWhiteSpace(branchCode.Value.ToString()))
                addUrlParameter += "&lsSBranchCode=" + Uri.EscapeUriString("%" + branchCode.Value.ToString() + "%");



            ShellViewModel.Current.IsBusy = true;
            excelServiceClient.GetTokenAsync("psmsadmin", "psms-admin", "pmsrf.foton.com.cn:6400", "secEnterprise", 1, 100);
            excelServiceClient.GetTokenCompleted -= excelServiceClient_GetTokenCompleted;
            excelServiceClient.GetTokenCompleted += excelServiceClient_GetTokenCompleted;
        }
    }
}