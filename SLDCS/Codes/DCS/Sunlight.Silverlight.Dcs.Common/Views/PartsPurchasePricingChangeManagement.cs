﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("PartsBaseInfo", "SparePartPurchase", "PartsPurchasePricingChange", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_SUBMIT_APPROVE_EXPORT_EXPORTDETAIL,"PartsPurchasePricingChange"
    })]
    public class PartsPurchasePricingChangeManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditViewApprove;
        private const string DATA_EDIT_VIEW_APPROVE = "_DataEditViewApprove_";
        private const string DATA_EDIT_VIEW_INITIALAPPROVE = "_DataEditViewInitialApprove_";
        private DataEditViewBase dataDetailView; 
        private const string DATA_DETAIL_VIEW = "_DataDetailView_"; 
        public PartsPurchasePricingChangeManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataManagementView_Title_PartsPurchasePricingChange;
        }

        private DataGridViewBase DataGridView {
            get {
                if (this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("PartsPurchasePricingChange");
                    this.dataGridView.RowDoubleClick += DataGridView_RowDoubleClick; 
                }
                return this.dataGridView;
            }
        }

        private DataEditViewBase DataEditViewDetail { 
            get { 
                if (this.dataDetailView == null) { 
                    this.dataDetailView = DI.GetDataEditView("PartsPurchasePricingChangeDetail"); 
                    this.dataDetailView.EditCancelled += this.DataEditView_EditCancelled; 
                } 
                return this.dataDetailView; 
            } 
        }
        private void DataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) { 
            if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) { 
                this.DataEditViewDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity()); 
                this.SwitchViewTo(DATA_DETAIL_VIEW); 
            } 
        } 
        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsPurchasePricingChange");
                    ((PartsPurchasePricingChangeDataEditView)this.dataEditView).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        //
        private DataEditViewBase DataEditViewApprove {
            get {
                if(this.dataEditViewApprove == null) {
                    this.dataEditViewApprove = DI.GetDataEditView("PartsPurchasePricingChangeApprove");
                    ((PartsPurchasePricingChangeApproveDataEditView)this.dataEditViewApprove).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditViewApprove.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewApprove;
            }
        }

        private DataEditViewBase dataEditViewInitialApprove;
        private DataEditViewBase DataEditViewInitialApprove {
            get {
                if(this.dataEditViewInitialApprove == null) {
                    this.dataEditViewInitialApprove = DI.GetDataEditView("PartsPurchasePricingChangeInitialApprove");
                    ((PartsPurchasePricingChangeInitialApproveDataEditView)this.dataEditViewInitialApprove).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditViewInitialApprove.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewInitialApprove;
            }
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_APPROVE, () => this.DataEditViewApprove);
            this.RegisterView(DATA_EDIT_VIEW_INITIALAPPROVE, () => this.DataEditViewInitialApprove);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.dataDetailView); 
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewApprove = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsPurchasePricingChange"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.ResetEditView(); 
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            //compositeFilterItem.Filters.Add(new FilterItem {
            //    MemberName = "BranchId",
            //    Operator = FilterOperator.IsEqualTo,
            //    MemberType = typeof(int),
            //    Value = BaseApp.Current.CurrentUserData.EnterpriseId
            //});

            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {

                case CommonActionKeys.ADD:
                    var partsPurchasePricingChange = this.DataEditView.CreateObjectToEdit<PartsPurchasePricingChange>();
                    partsPurchasePricingChange.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    partsPurchasePricingChange.BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    partsPurchasePricingChange.Status = (int)DcsPartsPurchasePricingChangeStatus.新增;
                    partsPurchasePricingChange.IsNewPart = false;
                    partsPurchasePricingChange.IsPrimaryIsMinPurchasePrice = false;
                    partsPurchasePricingChange.IsPrimaryNotMinPurchasePrice = false;
                    partsPurchasePricingChange.NotPrimaryIsMinPurchasePrice = false;
                    partsPurchasePricingChange.NotPrimaryNotMinPurchasePrice = false;
                    var ss=this.DataEditView as PartsPurchasePricingChangeDataEditView;
                    ss.FileUploadDataEditPanels.FilePath = null;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;

                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsPurchasePricingChange>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废配件采购价格变更申请单)
                                entity.作废配件采购价格变更申请单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.SUBMIT:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Notification_Submit, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsPurchasePricingChange>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can提交配件采购价格变更申请单)
                                entity.提交配件采购价格变更申请单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_SubmitSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "FirstApprove":
                    //初审
                    this.DataEditViewInitialApprove.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_INITIALAPPROVE);
                    break;
                case WorkflowActionKey.INITIALAPPROVE:
                    //审核
                    this.DataEditViewInitialApprove.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_INITIALAPPROVE);
                    break;
                case WorkflowActionKey.FINALAPPROVE:
                    //审批
                    this.DataEditViewApprove.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_APPROVE);
                    break;
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.EXPORTDETAIL:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsPurchasePricingChange>().Select(r => r.Id).ToArray();
                        if(uniqueId == CommonActionKeys.EXPORT) {
                            this.ExportPartsPurchasePricingChange(ids, null, null, null, null, null,null,null);
                        } else {
                            ShellViewModel.Current.IsBusy = true;
                            (this.DataGridView.DomainContext as DcsDomainContext).导出配件采购价格变更申请清单(ids, null, null, null, null, null,null,null, loadOp => {
                                ShellViewModel.Current.IsBusy = false;
                                if(loadOp.HasError)
                                    return;
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                }
                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            }, null);

                        }
                    } else {

                        var filtes = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filtes == null)
                            return;
                        var code = filtes.Filters.SingleOrDefault(e => e.MemberName == "Code").Value as string;
                        //var partsSalesCategoryId = filtes.Filters.SingleOrDefault(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                        var status = filtes.Filters.SingleOrDefault(e => e.MemberName == "Status").Value as int?;
                        var createTime = filtes.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        var sparePartCode = filtes.Filters.SingleOrDefault(e => e.MemberName == "SparePartCode").Value as string;
                        var sparePartName = filtes.Filters.SingleOrDefault(e => e.MemberName == "SparePartName").Value as string;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        if(uniqueId == CommonActionKeys.EXPORT) {
                            this.ExportPartsPurchasePricingChange(null, 221, code, status,sparePartCode,sparePartName, createTimeBegin, createTimeEnd);
                        } else {
                            ShellViewModel.Current.IsBusy = true;
                            (this.DataGridView.DomainContext as DcsDomainContext).导出配件采购价格变更申请清单(null, 221, code, status,sparePartCode,sparePartName, createTimeBegin, createTimeEnd, loadOp => {
                                ShellViewModel.Current.IsBusy = false;
                                if(loadOp.HasError)
                                    return;
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                }
                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            }, null);

                        }
                    }
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.SUBMIT:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    var partsPurchasePricingChange = this.DataGridView.SelectedEntities.Cast<PartsPurchasePricingChange>().FirstOrDefault();
                    if(partsPurchasePricingChange == null)
                        return false;
                    return partsPurchasePricingChange.Status == (int)DcsPartsPurchasePricingChangeStatus.新增;
                case "FirstApprove":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities11 = this.DataGridView.SelectedEntities.Cast<PartsPurchasePricingChange>().ToArray();
                    if(entities11.Length != 1)
                        return false;
                    return entities11[0].Status == (int)DcsPartsPurchasePricingChangeStatus.提交;
                case WorkflowActionKey.INITIALAPPROVE:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities1 = this.DataGridView.SelectedEntities.Cast<PartsPurchasePricingChange>().ToArray();
                    if(entities1.Length != 1)
                        return false;
                    return entities1[0].Status == (int)DcsPartsPurchasePricingChangeStatus.初审通过;
                case WorkflowActionKey.FINALAPPROVE:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsPurchasePricingChange>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsPartsPurchasePricingChangeStatus.审核通过;
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.EXPORTDETAIL:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private void ExportPartsPurchasePricingChange(int[] ids, int? partsSalesCategoryId, string code, int? status,string sparePartCode,string sparePartName, DateTime? creatorTimeBegin, DateTime? creatorTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsPurchasePricingChangeAsync(ids, partsSalesCategoryId, code, status,sparePartCode,sparePartName, creatorTimeBegin, creatorTimeEnd);
            this.excelServiceClient.ExportPartsPurchasePricingChangeCompleted -= ExcelServiceClient_ExportPartsPurchasePricingChangeCompleted;
            this.excelServiceClient.ExportPartsPurchasePricingChangeCompleted += ExcelServiceClient_ExportPartsPurchasePricingChangeCompleted;
        }

        private void ExcelServiceClient_ExportPartsPurchasePricingChangeCompleted(object sender, ExportPartsPurchasePricingChangeCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
    }
}
