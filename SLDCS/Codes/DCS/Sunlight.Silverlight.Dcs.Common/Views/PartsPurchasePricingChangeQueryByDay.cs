﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("Common", "SparePartPurchase", "PartsPurchasePricingQueryByDay", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class PartsPurchasePricingChangeQueryByDay : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;

        public PartsPurchasePricingChangeQueryByDay() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataManagementView_Title_PartsPurchasePricingChangeByDay;
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsPurchasePricingChangeByDay"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            if(filterItem is CompositeFilterItem) {
                var compositeFilterItem = filterItem as CompositeFilterItem;
                var validFromFilter = compositeFilterItem.Filters.FirstOrDefault(filter => filter.MemberName == "ValidFrom");
                if(validFromFilter == null || validFromFilter.Value == null) {
                    UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Alert_TimeIsNull, 5);
                    return;
                }
                var dateTime = validFromFilter.Value as DateTime?;
                if(dateTime.HasValue)
                    validFromFilter.Value = new DateTime(dateTime.Value.Year, dateTime.Value.Month, dateTime.Value.Day, 23, 59, 59);
            }
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    ((DcsDataGridViewBase)this.DataGridView).ExportData();
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            if(uniqueId == CommonActionKeys.EXPORT)
                return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
            return false;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsPurchasePricingChangeByDay"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
    }
}
