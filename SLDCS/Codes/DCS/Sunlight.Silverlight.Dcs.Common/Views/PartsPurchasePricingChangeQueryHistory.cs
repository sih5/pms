﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("PartsBaseInfo", "SparePartPurchase", "PartsPurchasePricingChangeQuery", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class PartsPurchasePricingChangeQueryHistory : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();

        public PartsPurchasePricingChangeQueryHistory() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataManagementView_Title_PartsPurchasePricingChangeHistory;
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsPurchasePricingChangeHistory"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            CompositeFilterItem compositeFilter;

            if(!(filterItem is CompositeFilterItem)) {
                compositeFilter = new CompositeFilterItem();
                compositeFilter.Filters.Add(filterItem);
            } else
                compositeFilter = filterItem as CompositeFilterItem;
            ClientVar.ConvertTime(compositeFilter);
            //compositeFilter.Filters.Add(new FilterItem {
            //    MemberName = "PartsPurchasePricingChange.BranchId",
            //    MemberType = typeof(int),
            //    Operator = FilterOperator.IsEqualTo,
            //    Value = BaseApp.Current.CurrentUserData.EnterpriseId
            //});
            this.DataGridView.FilterItem = compositeFilter;
            this.DataGridView.ExecuteQueryDelayed();


        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var supplierCode = filterItem.Filters.Single(r => r.MemberName == "SupplierCode").Value as string;
                    var supplierName = filterItem.Filters.Single(r => r.MemberName == "SupplierName").Value as string;
                    var partCode = filterItem.Filters.Single(r => r.MemberName == "PartCode").Value as string;
                    var partName = filterItem.Filters.Single(r => r.MemberName == "PartName").Value as string;
                    var referenceCode = filterItem.Filters.Single(r => r.MemberName == "SparePart.ReferenceCode").Value as string;
                    var supplierPartCode = filterItem.Filters.Single(r => r.MemberName == "SupplierPartCode").Value as string;
                    //var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsPurchasePricingChange.PartsSalesCategoryId").Value as int?;
                    var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    DateTime? validFromBegin = null;
                    DateTime? validFromEnd = null;
                    if(createTime != null) {
                        validFromBegin = createTime.Filters.First(r => r.MemberName == "ValidFrom").Value as DateTime?;
                        validFromEnd = createTime.Filters.Last(r => r.MemberName == "ValidFrom").Value as DateTime?;
                    }
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var detailId = this.DataGridView.SelectedEntities.Cast<PartsPurchasePricingDetail>().Select(r => r.Id).ToArray();
                        this.dcsDomainContext.ExportPartsPurchasePricingDetail(detailId, BaseApp.Current.CurrentUserData.EnterpriseId, null, null, null, null, null, null, null,null,null, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                            }
                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                        }, null);
                    } else {
                        this.dcsDomainContext.ExportPartsPurchasePricingDetail(null, BaseApp.Current.CurrentUserData.EnterpriseId, partCode, partName,referenceCode, supplierCode, supplierName,supplierPartCode, 221, validFromBegin, validFromEnd, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                            }
                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                        }, null);
                    }
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            if(uniqueId == CommonActionKeys.EXPORT)
                return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
            return false;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsPurchasePricingChangeHistory"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
    }
}
