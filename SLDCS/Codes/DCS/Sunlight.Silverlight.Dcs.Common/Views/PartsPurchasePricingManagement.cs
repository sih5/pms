﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("PartsBaseInfo", "SparePartPurchase", "PartsPurchasePricing", ActionPanelKeys = new[]{
        "PartsPurchasePricing"
    })]
    public class PartsPurchasePricingManagement : DcsDataManagementViewBase {
        private ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private DataGridViewBase dataGridView;
        public PartsPurchasePricingManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataManagementView_Title_PartsPurchasePricing;
        }

        public DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsPurchasePricing"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[]{
                    "PartsPurchasePricing"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case "ExactExport":
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if (filterItem == null)
                        return false;
                    var sparePartCode = filterItem.Filters.Single(e => e.MemberName == "SparePartCode").Value as string;
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any() && !string.IsNullOrEmpty(sparePartCode);
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                case "ExactExport":
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any())
                    {
                        var ids = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchasePricing>().Select(r => r.Id).ToArray();
                        this.ExportPartsPurchasePricing(ids, BaseApp.Current.CurrentUserData.EnterpriseId, null, null, null, null, null, null, null, null, null, null,null);
                    }
                    else
                    {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if (filterItem == null)
                            return;
                        //var partsSalesCategoryId = filterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                        var availibleTime = filterItem.Filters.Single(r => r.MemberName == "AvailibleTime").Value as DateTime?;
                        var sparePartCode = filterItem.Filters.Single(e => e.MemberName == "SparePartCode").Value as string;
                        var spareParName = filterItem.Filters.Single(e => e.MemberName == "SparePart.Name").Value as string;
                        var referenceCode = filterItem.Filters.Single(e => e.MemberName == "SparePart.ReferenceCode").Value as string;
                        var partsSupplierCode = filterItem.Filters.Single(e => e.MemberName == "PartsSupplier.Code").Value as string;
                        var partsSupplieName = filterItem.Filters.Single(r => r.MemberName == "PartsSupplier.Name").Value as string;
                        var isOrderable = filterItem.Filters.Single(r => r.MemberName == "SparePart.PartsBranch.IsOrderable").Value as bool?;
                        var isSalable = filterItem.Filters.Single(r => r.MemberName == "SparePart.PartsBranch.IsSalable").Value as bool?;
                        var supplierPartCode = filterItem.Filters.Single(e => e.MemberName == "SupplierPartCode").Value as string;

                        if (uniqueId.Equals("ExactExport")) {
                            if (string.IsNullOrEmpty(sparePartCode)) {
                                UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Validation_VirtualFactoryPurchacePrice_sparePartCode);
                                return;
                            }
                            this.ExportPartsPurchasePricing(null, BaseApp.Current.CurrentUserData.EnterpriseId, 221, sparePartCode, spareParName, partsSupplierCode, partsSupplieName, isOrderable, isSalable, referenceCode, supplierPartCode, availibleTime, true);
                        } else {
                            this.ExportPartsPurchasePricing(null, BaseApp.Current.CurrentUserData.EnterpriseId, 221, sparePartCode, spareParName, partsSupplierCode, partsSupplieName, isOrderable, isSalable, referenceCode, supplierPartCode, availibleTime, false);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        private void ExportPartsPurchasePricing(int[] ids, int branchId, int? partsSalesCategoryId, string sparePartCode, string sparePartName, string partsSupplierCode, string partsSupplierName, bool? isOrderable, bool? isSalable, string referenceCode, string supplierPartCode, DateTime? availibleTime,bool? isExactExport)
        {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsPurchasePricingAsync(null, branchId, partsSalesCategoryId, sparePartCode, sparePartName, partsSupplierCode, partsSupplierName, isOrderable, isSalable, referenceCode, supplierPartCode, availibleTime,isExactExport);
            this.excelServiceClient.ExportPartsPurchasePricingCompleted -= ExcelServiceClient_ExportPartsPurchasePricingCompleted;
            this.excelServiceClient.ExportPartsPurchasePricingCompleted += ExcelServiceClient_ExportPartsPurchasePricingCompleted;
        }

        private void ExcelServiceClient_ExportPartsPurchasePricingCompleted(object sender, ExportPartsPurchasePricingCompletedEventArgs e)
        {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var newCompositeFilterItem = filterItem as CompositeFilterItem;
            if(newCompositeFilterItem == null)
                return;
            if(filterItem is CompositeFilterItem) {
                newCompositeFilterItem = filterItem as CompositeFilterItem;
                var AvailibleTime = newCompositeFilterItem.Filters.SingleOrDefault(filter => filter.MemberName == "AvailibleTime");
                if(AvailibleTime != null && AvailibleTime.Value == null) {
                    UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_AvailibleTimeNotNull);
                    return;
                }
            }
            newCompositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "BranchId",
                Operator = FilterOperator.IsEqualTo,
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });

            var codes = newCompositeFilterItem.Filters.SingleOrDefault(s => s.MemberName == "SparePartCode"); 
                if(codes != null && codes.Value != null){  
                    newCompositeFilterItem.Filters.Add(new CompositeFilterItem {  
                        MemberName = "SparePartCode",  
                        MemberType = typeof(string),  
                        Value =string.Join(",",((IEnumerable<string>)codes.Value).ToArray()),  
                        Operator = FilterOperator.Contains  
                    });  
                    newCompositeFilterItem.Filters.Remove(codes);  
                }  
            this.DataGridView.FilterItem = newCompositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
