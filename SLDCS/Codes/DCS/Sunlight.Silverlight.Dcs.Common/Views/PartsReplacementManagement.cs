﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("PartsBaseInfo", "SparePart", "PartsReplacement", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_EXPORT_IMPORT
    })]
    public class PartsReplacementManagement : DcsDataManagementViewBase {
        private DataEditViewBase dataEditView;
        private DataEditViewBase importDataEditView;
        private DataGridViewBase dataGridView;
        private const string IMPORT_DATA_EDIT_VIEW = "_IMPORT_DATA_EDIT_VIEW_";
        private ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        public PartsReplacementManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataManagementView_Title_PartsReplacement;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsReplacement"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsReplacement");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase ImportDataEditView {
            get {
                if(this.importDataEditView == null) {
                    this.importDataEditView = DI.GetDataEditView("PartsReplacementForImport");
                    this.importDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.importDataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.importDataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(IMPORT_DATA_EDIT_VIEW, () => this.ImportDataEditView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsReplacement"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var partsReplacement = this.DataEditView.CreateObjectToEdit<PartsReplacement>();
                    partsReplacement.Status = (int)DcsBaseDataStatus.有效;
                    partsReplacement.IsInterFace = false;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsReplacement>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废配件替互换信息)
                                entity.作废配件替互换信息();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORT:
               var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var oldPartCode = filterItem.Filters.Single(r => r.MemberName == "PartCode").Value as string;
                    var oldPartName = filterItem.Filters.Single(r => r.MemberName == "PartName").Value as string;
                    var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                    var IsSPM = filterItem.Filters.Single(e => e.MemberName == "IsSPM").Value as bool?;
                    var isInterFace = filterItem.Filters.Single(e => e.MemberName == "IsInterFace").Value as bool?;
                    ShellViewModel.Current.IsBusy = true;
                    this.ExportPartsReplacement(isInterFace,IsSPM, oldPartCode, oldPartName, status);
                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(IMPORT_DATA_EDIT_VIEW);
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                case CommonActionKeys.IMPORT:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsReplacement>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsBaseDataStatus.有效;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }


        private void ExportPartsReplacement(bool? isInterFace,bool? isSPM,string oldPartCode, string oldPartName, int? status) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsReplacementAsync(isInterFace,isSPM, oldPartCode, oldPartName, status);
            this.excelServiceClient.ExportPartsReplacementCompleted -= excelServiceClient_ExportPartsReplacementCompleted;
            this.excelServiceClient.ExportPartsReplacementCompleted += excelServiceClient_ExportPartsReplacementCompleted;
        }

        private void excelServiceClient_ExportPartsReplacementCompleted(object sender, ExportPartsReplacementCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
    }
}
