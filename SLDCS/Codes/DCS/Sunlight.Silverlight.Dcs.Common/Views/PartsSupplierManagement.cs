﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("Common", "Company", "PartsSupplier", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_PAUSE_RESUME_EXPORT_DETAIL
    })]
    public class PartsSupplierManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditViewDetail;
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        private const string DATA_EDIT_VIEW_DETAIL = "_DataEditViewDetail_";
        public PartsSupplierManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataManagementView_Title_PartsSupplier;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsSupplierWithDetails"));
            }
        }
        private DataEditViewBase DataEditViewDetail {
            get {
                if(this.dataEditViewDetail == null) {
                    this.dataEditViewDetail = DI.GetDataEditView("PartsSupplierDetail");
                    this.dataEditViewDetail.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewDetail;
            }
        }
        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsSupplier");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_DETAIL, () => this.DataEditViewDetail);
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewDetail = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var partsSupplier = this.DataEditView.CreateObjectToEdit<PartsSupplier>();
                    partsSupplier.Status = (int)DcsMasterDataStatus.有效;
                    var company = new Company {
                        Code = partsSupplier.Code,
                        Name = partsSupplier.Name,
                        Type = (int)DcsCompanyType.配件供应商,
                        ShortName = partsSupplier.ShortName,
                        Status = (int)DcsMasterDataStatus.有效
                    };
                    partsSupplier.Company = company;

                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsSupplier>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废配件供应商基本信息)
                                entity.作废配件供应商基本信息();
                            this.ExecuteSerivcesMethod(CommonUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.PAUSE:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Pause, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsSupplier>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can停用配件供应商基本信息)
                                entity.停用配件供应商基本信息();
                            this.ExecuteSerivcesMethod(CommonUIStrings.DataManagementView_Notification_PauseSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.RESUME:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Resume, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsSupplier>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can恢复配件供应商基本信息)
                                entity.恢复配件供应商基本信息();
                            this.ExecuteSerivcesMethod(CommonUIStrings.DataManagementView_Notification_ResumeSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORT:
                    //如果选中一条数据 合并导出参数为 旧件入库单ID 
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var id = this.DataGridView.SelectedEntities.Cast<PartsSupplier>().Select(r => r.Id).ToArray();
                        this.dcsDomainContext.ExportPartsSupplier(id, null, null, null, null, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                            }
                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                        }, null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Company.Code") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Company.Code").Value as string;
                        var name = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Company.Name") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Company.Name").Value as string;
                        var status = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Status") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var supplierType = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SupplierType") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SupplierType").Value as int?;
                        this.dcsDomainContext.ExportPartsSupplier(new int[] { }, code, name, status, supplierType, loadOp1 => {
                            if(loadOp1.HasError)
                                return;
                            if(loadOp1.Value == null || string.IsNullOrEmpty(loadOp1.Value)) {
                                UIHelper.ShowNotification(loadOp1.Value);
                            }
                            if(loadOp1.Value != null && !string.IsNullOrEmpty(loadOp1.Value))
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp1.Value));
                        }, null);
                    }
                    break;
                case CommonActionKeys.DETAIL:
                    this.DataEditViewDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_DETAIL);
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.PAUSE:
                case CommonActionKeys.RESUME:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsSupplier>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    if(String.CompareOrdinal(uniqueId, CommonActionKeys.RESUME) == 0)
                        return entities[0].Status == (int)DcsMasterDataStatus.停用;
                    return entities[0].Status == (int)DcsMasterDataStatus.有效;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.DETAIL:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<PartsSupplier>().ToArray().Length == 1;

                default:
                    return false;
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsSupplier"
                };
            }
        }
    }
}
