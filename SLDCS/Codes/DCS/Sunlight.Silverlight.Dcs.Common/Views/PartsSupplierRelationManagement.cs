﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("PartsBaseInfo", "SparePartPurchase", "PartsSupplierRelation", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_EXPORT_IMPORT_IMPORTUPDATE
    })]
    public class PartsSupplierRelationManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditViewImport, importUpdateDataEditView;
        private const string DATA_EDIT_VIEW_IMPORT = "_DataEditViewImport_";
        private const string DATA_IMPORTUPDATE_VIEW = "_DataImportUpdateView_";

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public PartsSupplierRelationManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataManagementView_Title_PartsSupplierRelation;
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            if(filterItem is CompositeFilterItem) {
                var compositeFilterItem = filterItem as CompositeFilterItem;
                compositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "BranchId",
                    Operator = FilterOperator.IsEqualTo,
                    MemberType = typeof(int),
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });
                ClientVar.ConvertTime(compositeFilterItem);
            }
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var partsSupplierRelation = this.DataEditView.CreateObjectToEdit<PartsSupplierRelation>();
                    partsSupplierRelation.EconomicalBatch = 1;
                    partsSupplierRelation.MinBatch = 1;
                    partsSupplierRelation.IsPrimary = false;
                    partsSupplierRelation.OrderCycle = 30;
                    partsSupplierRelation.Status = (int)DcsBaseDataStatus.有效;
                    partsSupplierRelation.BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Abandon, () => this.DataGridView.UpdateSelectedEntities(entity => ((PartsSupplierRelation)entity).Status = (int)DcsBaseDataStatus.作废, () => {
                        UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_AbandonSuccess);
                        this.CheckActionsCanExecute();
                        this.DataGridView.ExecuteQueryDelayed();
                    }));
                    break;
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsSupplierRelation>().Select(r => r.Id).ToArray();
                        this.ExportPartsSupplierRelation(ids, null, null, null, null, null, null, null, null, null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var partsSupplierCode = filterItem.Filters.Single(r => r.MemberName == "PartsSupplier.Code").Value as string;
                        var partsSupplierName = filterItem.Filters.Single(r => r.MemberName == "PartsSupplier.Name").Value as string;
                        var sparePartCode = filterItem.Filters.Single(r => r.MemberName == "SparePart.Code").Value as string;
                        var sparePartName = filterItem.Filters.Single(r => r.MemberName == "SparePart.Name").Value as string;
                        var overseasPartsFigure = filterItem.Filters.Single(r => r.MemberName == "SparePart.ReferenceCode").Value as string;
                        var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        //var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        ShellViewModel.Current.IsBusy = true;
                        this.ExportPartsSupplierRelation(new int[] { }, partsSupplierCode, partsSupplierName, sparePartCode, sparePartName, null, 221, status, createTimeBegin, createTimeEnd);
                    }
                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORT);
                    break;
                case CommonActionKeys.IMPORTUPDATE:
                    this.SwitchViewTo(DATA_IMPORTUPDATE_VIEW);
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                case CommonActionKeys.IMPORT:
                case CommonActionKeys.IMPORTUPDATE:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsSupplierRelation>().ToArray();
                    if(entities.Length != 1 && uniqueId == CommonActionKeys.EDIT)
                        return false;
                    return entities.All(r => r.Status == (int)DcsBaseDataStatus.有效);
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsSupplierRelation"
                };
            }
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsSupplierRelation"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsSupplierRelation");
                    this.dataEditView.EditSubmitted += this.dataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataEditViewImport {
            get {
                if(this.dataEditViewImport == null) {
                    this.dataEditViewImport = DI.GetDataEditView("PartsSupplierRelationForImport");
                    this.dataEditViewImport.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.dataEditViewImport;
            }
        }

        private DataEditViewBase ImportUpdateDataEditView {
            get {
                if(this.importUpdateDataEditView == null) {
                    this.importUpdateDataEditView = DI.GetDataEditView("PartsSupplierRelationForImportUpdate");
                    this.importUpdateDataEditView.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.importUpdateDataEditView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_IMPORT, () => this.DataEditViewImport);
            this.RegisterView(DATA_IMPORTUPDATE_VIEW, () => this.ImportUpdateDataEditView);
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewImport=null;
            this.importUpdateDataEditView = null;
        }
        private void dataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void dataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void ExportPartsSupplierRelation(int[] ids, string supplierCode, string supplierName, string partCode, string partName,string overseasPartsFigure, int? partsSalesCategoryId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsSupplierRelationAsync(ids, BaseApp.Current.CurrentUserData.EnterpriseId, supplierCode, supplierName, partCode, partName, overseasPartsFigure, partsSalesCategoryId, status, createTimeBegin, createTimeEnd);
            this.excelServiceClient.ExportPartsSupplierRelationCompleted -= excelServiceClient_ExportPartsSupplierRelationCompleted;
            this.excelServiceClient.ExportPartsSupplierRelationCompleted += excelServiceClient_ExportPartsSupplierRelationCompleted;
        }

        private void excelServiceClient_ExportPartsSupplierRelationCompleted(object sender, ExportPartsSupplierRelationCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

    }
}