﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Views
{
    [PageMeta("Common", "EnterpriseAndInternalOrganization", "PersonnelSupplierRelation", ActionPanelKeys = new[] {
        "PersonnelSupplierRelation"
    })]
    public class PersonnelSupplierRelationManagement : DcsDataManagementViewBase
    {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditViewImport;

        private const string DATA_EDIT_VIEW_IMPORT = "_DataEditViewImport_";

        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PersonnelSupplierRelation"));
            }
        }

        private DataEditViewBase DataEditView
        {
            get
            {
                if (this.dataEditView == null)
                {
                    this.dataEditView = DI.GetDataEditView("PersonnelSupplierRelation");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataEditViewImport
        {
            get
            {
                if (this.dataEditViewImport == null)
                {
                    this.dataEditViewImport = DI.GetDataEditView("PersonnelSupplierRelationForImport");
                    this.dataEditViewImport.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewImport;
            }
        }

        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_IMPORT, () => this.DataEditViewImport);
        }

        public PersonnelSupplierRelationManagement()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataEditView_BusinessName_PersonnelSupplierRelation;
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewImport = null;
        }
        private void DataEditView_EditCancelled(object sender, EventArgs e)
        {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e)
        {
            this.ResetEditView();
            if (this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "PersonnelSupplierRelation"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem)
        {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var newCompositeFilterItem = new CompositeFilterItem();
            var compositeFilterItem = filterItem as CompositeFilterItem;
            foreach (var item in compositeFilterItem.Filters)
                newCompositeFilterItem.Filters.Add(item);
            ClientVar.ConvertTime(newCompositeFilterItem);
            this.DataGridView.FilterItem = newCompositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId)
            {
                case CommonActionKeys.ADD:
                    var customerInformation = this.DataEditView.CreateObjectToEdit<PersonnelSupplierRelation>();
                    customerInformation.Status = (int)DcsMasterDataStatus.有效;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Abandon, () =>
                    {
                        var entity = this.DataGridView.SelectedEntities.Cast<PersonnelSupplierRelation>().SingleOrDefault();
                        if (entity == null)
                            return;
                        try
                        {
                            if (entity.Can作废人员与供应商关系)
                                entity.作废人员与供应商关系();
                            this.ExecuteSerivcesMethod(CommonUIStrings.DataManagementView_Notification_AbandonSuccess);
                        }
                        catch (Exception ex)
                        {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORT);
                    break;
                case "MergeExport"://合并导出
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any())
                    {
                        var ids = this.DataGridView.SelectedEntities.Cast<PersonnelSupplierRelation>().Select(r => r.Id).ToArray();
                        this.ExportPersonnelSupplierRelation(ids, null, null, null, null, null, null, null, null);
                    }
                    else
                    {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if (filterItem == null)
                            return;
                        var partsSalesCategoryId = filterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                        var personName = filterItem.Filters.Single(r => r.MemberName == "PersonName").Value as string;
                        var supplierName = filterItem.Filters.Single(r => r.MemberName == "SupplierName").Value as string;
                        var status = filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var createTimeFilters = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem) && ((CompositeFilterItem)r).Filters.Any(e => e.MemberName == "CreateTime")) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if (createTimeFilters != null && createTimeFilters.Filters.Any())
                        {
                            createTimeBegin = createTimeFilters.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTimeFilters.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        var modifyTimeFilters = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem) && ((CompositeFilterItem)r).Filters.Any(e => e.MemberName == "ModifyTime")) as CompositeFilterItem;
                        DateTime? modifyTimeBegin = null;
                        DateTime? modifyTimeEnd = null;
                        if (modifyTimeFilters != null && modifyTimeFilters.Filters.Any())
                        {
                            modifyTimeBegin = modifyTimeFilters.Filters.First(r => r.MemberName == "ModifyTime").Value as DateTime?;
                            modifyTimeEnd = modifyTimeFilters.Filters.Last(r => r.MemberName == "ModifyTime").Value as DateTime?;
                        }
                        this.ExportPersonnelSupplierRelation(null, partsSalesCategoryId, personName, supplierName, status, createTimeBegin, createTimeEnd, modifyTimeBegin, modifyTimeEnd);
                    }
                    break;
            }
        }
        private void ExportPersonnelSupplierRelation(int[] ids, int? partsSalesCategoryId, string personName, string supplierName, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? modifyTimeBegin, DateTime? modifyTimeEnd)
        {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPersonnelSupplierRelationAsync(ids, partsSalesCategoryId, personName, supplierName, status, createTimeBegin, createTimeEnd, modifyTimeBegin, modifyTimeEnd);
            this.excelServiceClient.ExportPersonnelSupplierRelationCompleted -= ExcelServiceClient_ExportPersonnelSupplierRelationCompleted;
            this.excelServiceClient.ExportPersonnelSupplierRelationCompleted += ExcelServiceClient_ExportPersonnelSupplierRelationCompleted;
        }

        private void ExcelServiceClient_ExportPersonnelSupplierRelationCompleted(object sender, ExportPersonnelSupplierRelationCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        private void ExecuteSerivcesMethod(string notifyMessage)
        {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if (domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp =>
            {
                if (submitOp.HasError)
                {
                    if (!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId)
            {
                case CommonActionKeys.ADD:
                case CommonActionKeys.IMPORT:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if (this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PersonnelSupplierRelation>().ToArray();
                    if (entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsMasterDataStatus.有效;
                case "MergeExport":
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
    }
}
