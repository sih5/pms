﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using System.Windows.Browser;

namespace Sunlight.Silverlight.Dcs.Common.Views
{
    [PageMeta("PartsBaseInfo", "SparePart", "ProductExecutiveStandard", ActionPanelKeys = new[] {
        "ProductExecutiveStandard"
    })]
    public class ProductExecutiveStandardManagement : DcsDataManagementViewBase
    {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private const string IMPORT_DATA_EDIT_VIEW = "_IMPORT_DATA_EDIT_VIEW_";
        private const string BATCH_IMPORT_DATA_EDIT_VIEW = "_BATCH_IMPORT_DATA_EDIT_VIEW_";
        private DataEditViewBase importDataEditView;
        private DataEditViewBase batchImportDataEditView;
        public ProductExecutiveStandardManagement()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataManagementView_Title_ProductExecutiveStandard;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("ProductExecutiveStandard"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if (this.dataEditView == null)
                {
                    this.dataEditView = DI.GetDataEditView("ProductExecutiveStandard");
                    ((ProductExecutiveStandardDataEditView)this.dataEditView).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase ImportDataEditView {
            get {
                if (this.importDataEditView == null)
                {
                    this.importDataEditView = DI.GetDataEditView("ProductExecutiveStandardForImport");
                    this.importDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.importDataEditView;
            }
        }

        private DataEditViewBase BatchImportDataEditView {
            get {
                if (this.batchImportDataEditView == null)
                {
                    this.batchImportDataEditView = DI.GetDataEditView("ProductExecutiveStandardForBatchImport");
                    this.batchImportDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.batchImportDataEditView;
            }
        }

        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(IMPORT_DATA_EDIT_VIEW, () => this.ImportDataEditView);
            this.RegisterView(BATCH_IMPORT_DATA_EDIT_VIEW, () => this.BatchImportDataEditView);
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.importDataEditView=null;
            this.batchImportDataEditView=null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e)
        {
            this.ResetEditView();
            if (this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e)
        {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "ProductExecutiveStandard"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem)
        {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId)
            {
                case CommonActionKeys.ADD:
                    //var regionPersonnelRelation = this.DataEditView.CreateObjectToEdit<ProductStandard>();
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(IMPORT_DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.BATCH_IMPORT:
                    this.SwitchViewTo(BATCH_IMPORT_DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Abandon, () =>
                    {
                        //var entity = this.DataGridView.SelectedEntities.Cast<ProductStandard>().SingleOrDefault();
                        //if (entity == null)
                        //    return;
                        try
                        {
                            //if (entity.Can作废产品执行标准)
                            //    entity.作废产品执行标准();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;

                            if (domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp =>
                            {
                                if (submitOp.HasError)
                                {
                                    if (!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        }
                        catch (Exception ex)
                        {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if (filterItem == null)
                        return;
                    var productExecutiveStandard = this.DataGridView.SelectedEntities;
                    if (productExecutiveStandard != null && productExecutiveStandard.Any())
                    {
                        //var ids = this.DataGridView.SelectedEntities.Cast<ProductStandard>().Select(i => i.Id).ToArray();
                        //this.ExportProductStandard(ids, null, null, null, null, null, null);
                    }
                    else
                    {
                        var standardCode = filterItem.Filters.Single(r => r.MemberName == "StandardCode").Value as string;
                        var standardName = filterItem.Filters.Single(r => r.MemberName == "StandardName").Value as string;
                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;

                        var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if (createTime != null)
                        {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        this.ExportProductStandard(null, standardCode, standardName, null, status, createTimeBegin, createTimeEnd);
                    }
                    break;
            }
        }
        private void ExportProductStandard(int[] ids, string standardCode, string standardName, string remark, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd)
        {
            //ShellViewModel.Current.IsBusy = true;
            //this.excelServiceClient.ExportProductStandardAsync(ids, standardCode, standardName, remark, status, createTimeBegin, createTimeEnd);
            //this.excelServiceClient.ExportProductStandardCompleted -= ExcelServiceClient_ExportProductStandardCompleted;
            //this.excelServiceClient.ExportProductStandardCompleted += ExcelServiceClient_ExportProductStandardCompleted;
        }

        //private void ExcelServiceClient_ExportProductStandardCompleted(object sender, ExportProductStandardCompletedEventArgs e)
        //{
        //    ShellViewModel.Current.IsBusy = false;
        //    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        //}

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId)
            {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.IMPORT:
                    return true;
                case CommonActionKeys.BATCH_IMPORT:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if (this.DataGridView.SelectedEntities == null)
                        return false;
                    //var entities = this.DataGridView.SelectedEntities.Cast<ProductStandard>().ToArray();
                    //if (entities.Length != 1)
                    //    return false;
                    //return entities[0].Status == (int)DcsBaseDataStatus.有效;
                    return false;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
    }
}
