﻿namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    /// <summary>
    /// 选择代理库
    /// 注：需要传入营销分公司id，否则查不到数据。
    /// 例： new FilterItem("BranchId", typeof(int), FilterOperator.IsEqualTo, value）
    /// </summary>
    public class AgencyAffiBranchQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "AgencyAffiBranch";
            }
        }

        public override string QueryPanelKey {
            get {
                return "AgencyAffiBranch";
            }
        }
    }
}
