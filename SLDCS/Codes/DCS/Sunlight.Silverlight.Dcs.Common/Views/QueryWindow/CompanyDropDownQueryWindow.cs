﻿using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    /// <summary>
    /// 选择企业
    /// </summary>
    public class CompanyDropDownQueryWindow : DcsDropDownQueryWindowBase {
        public CompanyDropDownQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string Title {
            get {
                return CommonUIStrings.QueryPanel_Title_Company;
            }
        }

        public override string DataGridViewKey {
            get {
                return "Company";
            }
        }

        public override string QueryPanelKey {
            get {
                return "Company";
            }
        }
    }
}
