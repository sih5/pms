﻿

namespace Sunlight.Silverlight.Dcs.Financial.Views.QueryWindow {
    public class CompanyForApproveBillQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "CompanyForApproveBill";
            }
        }

        public override string QueryPanelKey {
            get {
                return "CompanyForApproveBill";
            }
        }
    }
}
