﻿using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    /// <summary>
    /// 选择企业(多选)
    /// </summary>
    public class CompanyMultiQueryWindow : DcsMultiPopupsQueryWindowBase {
        public CompanyMultiQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string DataGridViewKey {
            get {
                return "CompanyNew";
            }
        }

        public override string QueryPanelKey {
            get {
                return "Company";
            }
        }
    }
}
