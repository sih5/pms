﻿using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    /// <summary>
    /// 选择企业
    /// </summary>
    public class CompanyQueryWindow : DcsQueryWindowBase {
        public CompanyQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string DataGridViewKey {
            get {
                return "Company";
            }
        }

        public override string QueryPanelKey {
            get {
                return "Company";
            }
        }
    }
}
