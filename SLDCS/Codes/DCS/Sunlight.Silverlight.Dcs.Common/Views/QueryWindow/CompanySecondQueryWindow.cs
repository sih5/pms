﻿using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    public class CompanySecondQueryWindow : DcsQueryWindowBase {
        public CompanySecondQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string DataGridViewKey {
            get {
                return "CompanySecond";
            }
        }

        public override string QueryPanelKey {
            get {
                return "Company";
            }
        }
    }
}
