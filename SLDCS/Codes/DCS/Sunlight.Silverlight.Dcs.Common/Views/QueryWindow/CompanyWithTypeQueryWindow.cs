﻿using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    /// <summary>
    /// 选择企业
    /// </summary>
    public class CompanyWithTypeQueryWindow : DcsQueryWindowBase {
        public CompanyWithTypeQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string DataGridViewKey {
            get {
                return "CompanyWithType";
            }
        }

        public override string QueryPanelKey {
            get {
                return "CompanyWithType";
            }
        }
    }
}
