﻿using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    /// <summary>
    /// 选择企业
    /// </summary>
    public class CustomerInfoForSupplierQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "CustomerInfoForSupplier";
            }
        }

        public override string QueryPanelKey {
            get {
                return "CustomerInfo";
            }
        }
    }
}
