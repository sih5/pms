﻿
using Sunlight.Silverlight.Dcs.Common.Resources;
namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    public class EngineModelDropDownQueryWindow : DcsDropDownQueryWindowBase {
        /// <summary>
        /// 选择发动机型号
        /// </summary>
        public override string Title {
            get {
                return CommonUIStrings.QueryPanel_Title_EngineModel;
            }
        }

        public override string DataGridViewKey {
            get {
                return "EngineModel";
            }
        }

        public override string QueryPanelKey {
            get {
                return "EngineModel";
            }
        }
    }
}
