﻿
namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    public class EngineModelMultiQueryWindow : DcsMultiPopupsQueryWindowBase {
        /// <summary>
        /// 选择发动机型号
        /// </summary>
        public override string DataGridViewKey {
            get {
                return "EngineModel";
            }
        }

        public override string QueryPanelKey {
            get {
                return "EngineModel";
            }
        }
    }
}
