﻿using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    /// <summary>
    /// 选择企业
    /// </summary>
    public class ExportCustomerInfoQueryWindow : DcsQueryWindowBase {
        public ExportCustomerInfoQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsBaseDataStatus.有效);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string DataGridViewKey {
            get {
                return "ExportCustomerInfoWindow";
            }
        }

        public override string QueryPanelKey {
            get {
                return "ExportCustomerInfoWindow";
            }
        }
    }
}
