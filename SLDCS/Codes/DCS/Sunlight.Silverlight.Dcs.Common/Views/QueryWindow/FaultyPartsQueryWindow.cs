﻿using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    /// <summary>
    /// 选择祸首件
    /// </summary>
    public class FaultyPartsQueryWindow : DcsQueryWindowBase {

        public FaultyPartsQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string DataGridViewKey {
            get {
                return "FaultyParts";
            }
        }

        public override string QueryPanelKey {
            get {
                return "FaultyParts";
            }
        }
    }
}