﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    public class GoldenTaxClassifyQueryWindow : DcsQueryWindowBase {
        public override string DataGridViewKey {
            get {
                return "GoldenTaxClassifyMsg";
            }
        }

        public override string QueryPanelKey {
            get {
                return "GoldenTaxClassifyMsg";
            }
        }
    }
}
