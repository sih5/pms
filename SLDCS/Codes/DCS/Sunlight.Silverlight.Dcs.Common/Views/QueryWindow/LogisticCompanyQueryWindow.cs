﻿namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    /// <summary>
    /// 选择物流公司
    /// </summary>
    public class LogisticCompanyQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "LogisticCompanyServiceRangeForSelect";
            }
        }

        public override string QueryPanelKey {
            get {
                return "LogisticCompanyServiceRange";
            }
        }
    }
}
