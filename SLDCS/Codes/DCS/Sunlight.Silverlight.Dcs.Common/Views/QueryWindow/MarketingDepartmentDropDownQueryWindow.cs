﻿using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    /// <summary>
    /// 营销公司市场部选择
    /// </summary>
    public class MarketingDepartmentDropDownQueryWindow : DcsDropDownQueryWindowBase {
        public MarketingDepartmentDropDownQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsBaseDataStatus.有效);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
            this.SetDefaultQueryItemValue("Common", "BranchID", BaseApp.Current.CurrentUserData.EnterpriseId);
            this.SetDefaultQueryItemEnable("Common", "BranchID", false);
        }

        public override string Title {
            get {
                return CommonUIStrings.QueryPanel_Title_MarketingDepartment;
            }
        }

        public override string DataGridViewKey {
            get {
                return "MarketingDepartment";
            }
        }

        public override string QueryPanelKey {
            get {
                return "MarketingDepartment";
            }
        }
    }
}
