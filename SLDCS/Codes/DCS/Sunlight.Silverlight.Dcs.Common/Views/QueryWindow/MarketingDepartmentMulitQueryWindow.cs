﻿using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    /// <summary>
    /// 营销公司市场部选择
    /// </summary>
    public class MarketingDepartmentMulitQueryWindow : DcsMultiPopupsQueryWindowBase {
        public MarketingDepartmentMulitQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsBaseDataStatus.有效);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
            this.SetDefaultQueryItemValue("Common", "BranchID", BaseApp.Current.CurrentUserData.EnterpriseId);
            this.SetDefaultQueryItemEnable("Common", "BranchID", false);
        }

        public override string QueryPanelKey {
            get {
                return "MarketingDepartment";
            }
        }

        public override string DataGridViewKey {
            get {
                return "MarketingDepartment";
            }
        }
    }
}
