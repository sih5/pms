﻿using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    /// <summary>
    /// 营销公司市场部选择
    /// </summary>
    public class MarketingDepartmentQueryWindow : DcsQueryWindowBase {
        public MarketingDepartmentQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsBaseDataStatus.有效);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string DataGridViewKey {
            get {
                return "MarketingDepartment";
            }
        }

        public override string QueryPanelKey {
            get {
                return "MarketingDepartment";
            }
        }
    }
}
