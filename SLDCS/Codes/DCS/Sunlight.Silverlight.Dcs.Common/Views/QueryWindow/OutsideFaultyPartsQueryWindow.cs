﻿using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    /// <summary>
    /// 保外选择祸首件
    /// </summary>
    public class OutsideFaultyPartsQueryWindow : DcsQueryWindowBase {

        public OutsideFaultyPartsQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string DataGridViewKey {
            get {
                return "OutsideFaultyParts";
            }
        }

        public override string QueryPanelKey {
            get {
                return "OutsideFaultyParts";
            }
        }
    }
}
