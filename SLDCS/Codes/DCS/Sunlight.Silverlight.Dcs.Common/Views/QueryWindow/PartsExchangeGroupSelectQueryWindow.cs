﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    public class PartsExchangeGroupSelectQueryWindow : DcsMultiPopupsQueryWindowBase {
        public PartsExchangeGroupSelectQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string QueryPanelKey {
            get {
                return "PartsExchangeGroupSelect";
            }
        }

        public override string DataGridViewKey {
            get {
                return "PartsExchangeGroupSelect";
            }
        }
    }
}
