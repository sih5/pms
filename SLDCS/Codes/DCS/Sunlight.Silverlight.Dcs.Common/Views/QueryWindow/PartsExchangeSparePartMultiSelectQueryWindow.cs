﻿using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    public class PartsExchangeSparePartMultiSelectQueryWindow : DcsMultiPopupsQueryWindowBase {
        public PartsExchangeSparePartMultiSelectQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string QueryPanelKey {
            get {
                return "PartsExchangeSparePartMultiSelect";
                //return "SparePart";
            }
        }

        public override string DataGridViewKey {
            get {
                return "PartsExchangeSparePartMultiSelect";
                //return "SparePart";
            }
        }
    }
}
