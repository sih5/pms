﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    public class PartsPurchaseBrandPriceQueryWindow : DcsQueryWindowBase {
        /// <summary>
        /// 跨库查询各品牌采购价格
        /// </summary>
        public override string DataGridViewKey {
            get {
                return "PartsPurchaseBrandPrice";
            }
        }

        public override string QueryPanelKey {
            get {
                return "PartsPurchaseBrandPrice";
            }
        }
    }
}
