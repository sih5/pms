﻿using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    /// <summary>
    /// 选择配件供应商
    /// </summary>
    public class PartsSupplierDropDownQueryWindow : DcsDropDownQueryWindowBase {
        public PartsSupplierDropDownQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string Title {
            get {
                return CommonUIStrings.QueryPanel_Title_PartsSupplier;
            }
        }

        public override string DataGridViewKey {
            get {
                return "PartsSupplier";
            }
        }

        public override string QueryPanelKey {
            get {
                return "PartsSupplier";
            }
        }
    }
}
