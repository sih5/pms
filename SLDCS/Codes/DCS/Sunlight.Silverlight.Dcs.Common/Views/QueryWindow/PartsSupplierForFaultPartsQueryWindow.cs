﻿using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    /// <summary>
    /// 选择祸首件配件供应商
    /// </summary>
    public class PartsSupplierForFaultPartsQueryWindow : DcsQueryWindowBase {
        public PartsSupplierForFaultPartsQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string DataGridViewKey {
            get {
                return "PartsSupplierForFaultParts";
            }
        }

        public override string QueryPanelKey {
            get {
                return "PartsSupplierForFaultParts";
            }
        }
    }
}

