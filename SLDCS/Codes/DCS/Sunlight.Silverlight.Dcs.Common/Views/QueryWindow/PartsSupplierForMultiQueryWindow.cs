﻿using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    public class PartsSupplierForMultiQueryWindow : DcsMultiPopupsQueryWindowBase {
        public PartsSupplierForMultiQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string DataGridViewKey {
            get {
                return "PartsSupplierForMulti";
            }
        }

        public override string QueryPanelKey {
            get {
                return "PartsSupplier";
            }
        }
    }
}
