﻿using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    public class PartsSupplierForPartsPurchaseRtnSettleBillQueryWindow : DcsQueryWindowBase {
        public PartsSupplierForPartsPurchaseRtnSettleBillQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string DataGridViewKey {
            get {
                return "PartsSupplierForBase";
            }
        }

        public override string QueryPanelKey {
            get {
                return "PartsSupplier";
            }
        }
    }
}
