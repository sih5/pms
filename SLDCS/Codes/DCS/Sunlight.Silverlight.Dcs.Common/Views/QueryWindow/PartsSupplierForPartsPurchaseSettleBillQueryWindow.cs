﻿using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    /// <summary>
    /// 选择配件供应商
    /// </summary>
    public class PartsSupplierForPartsPurchaseSettleBillQueryWindow : DcsQueryWindowBase {
        public PartsSupplierForPartsPurchaseSettleBillQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string DataGridViewKey {
            get {
                return "PartsSupplierForPartsPurchaseSettleBill";
            }
        }

        public override string QueryPanelKey {
            get {
                return "PartsSupplier";
            }
        }
    }
}