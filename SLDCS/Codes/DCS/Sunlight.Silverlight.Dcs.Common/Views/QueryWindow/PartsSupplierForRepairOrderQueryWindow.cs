﻿using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    /// <summary>
    /// 保内，保外选择配件供应商
    /// </summary>
    public class PartsSupplierForRepairOrderQueryWindow : DcsDropDownQueryWindowBase {
        public PartsSupplierForRepairOrderQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }
        public override string DataGridViewKey {
            get {
                return "PartsSupplierForRepairOrder";
            }
        }

        public override string QueryPanelKey {
            get {
                return "PartsSupplierForRepairOrder";
            }
        }

        public override string Title {
            get {
                return "选择供应商";
            }
        }
    }
}
