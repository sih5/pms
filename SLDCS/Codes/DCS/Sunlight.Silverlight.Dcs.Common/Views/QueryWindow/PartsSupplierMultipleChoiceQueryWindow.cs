﻿using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    public class PartsSupplierMultipleChoiceQueryWindow : DcsMultiPopupsQueryWindowBase {
        public PartsSupplierMultipleChoiceQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string QueryPanelKey {
            get {
                return "PartsSupplier";
            }
        }

        public override string DataGridViewKey {
            get {
                return "PartsSupplier";
            }
        }
    }
}
