﻿using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    /// <summary>
    /// 选择配件供应商
    /// </summary>
    public class PartsSupplierQueryWindow : DcsQueryWindowBase {
        public PartsSupplierQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string DataGridViewKey {
            get {
                return "PartsSupplier";
            }
        }

        public override string QueryPanelKey {
            get {
                return "PartsSupplier";
            }
        }
    }
}
