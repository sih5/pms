﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    /// <summary>
    /// 人员选择
    /// </summary>
    public class PersonnelDropDownQueryWindow : DcsDropDownQueryWindowBase {
        public PersonnelDropDownQueryWindow() {
            this.SetDefaultFilterItem(new FilterItem("Status", typeof(int), FilterOperator.IsEqualTo, (int)DcsMasterDataStatus.有效));
        }

        public override string DataGridViewKey {
            get {
                return "Personnel";
            }
        }

        public override string QueryPanelKey {
            get {
                return "Personnel";
            }
        }

        public override string Title {
            get {
                return CommonUIStrings.QueryPanel_Title_Personnel;
            }
        }
    }
}
