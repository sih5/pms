﻿using System;

using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow
{
    /**
     * 只选择本企业的人员
     * **/
    public class PersonnelForCompanyQueryWindow : DcsQueryWindowBase
    {

        public override string DataGridViewKey
        {
            get
            {
                return "PersonnelForCompany";
            }
        }

        public override string QueryPanelKey
        {
            get
            {
                return "Personnel";

            }
        }
    }
}

