﻿
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    /// <summary>
    /// 人员选择(多选)
    /// </summary>
    public class PersonnelForMultiSelectQueryWindow : DcsMultiPopupsQueryWindowBase {
        public PersonnelForMultiSelectQueryWindow() {
            this.SetDefaultFilterItem(new FilterItem("Status", typeof(int), FilterOperator.IsEqualTo, (int)DcsMasterDataStatus.有效));
        }

        public override string QueryPanelKey {
            get {
                return "Personnel";
            }
        }
 
        public override string DataGridViewKey {
            get {
                return "Personnel";
            }
        }
    }
}
