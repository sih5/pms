﻿
namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    public class PersonnelForSalesUnitQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "PersonnelForSalesUnit";
            }
        }

        public override string QueryPanelKey {
            get {
                return "Personnel";

            }
        }
    }
}
