﻿using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    public class ProductAllSelectQueryWindow : DcsMultiPopupsQueryWindowBase {

        public ProductAllSelectQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
        }

        public override string QueryPanelKey {
            get {
                return "ProductForSelect";
            }
        }

        public override string DataGridViewKey {
            get {
                return "ProductForSelect";
            }
        }


    }
}
