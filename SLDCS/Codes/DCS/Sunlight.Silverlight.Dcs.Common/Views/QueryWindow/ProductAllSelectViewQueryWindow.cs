﻿using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    public class ProductAllSelectViewQueryWindow : DcsMultiPopupsQueryWindowBase {

        public ProductAllSelectViewQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
        }

        public override string QueryPanelKey {
            get {
                return "ProductViewForSelect";
            }
        }

        public override string DataGridViewKey {
            get {
                return "ProductViewForSelect";
            }
        }
    }
}