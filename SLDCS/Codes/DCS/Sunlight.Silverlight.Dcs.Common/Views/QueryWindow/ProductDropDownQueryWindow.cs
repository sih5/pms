﻿using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    public class ProductDropDownQueryWindow : DcsDropDownQueryWindowBase {
        public ProductDropDownQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
        }

        public override string DataGridViewKey {
            get {
                return "ProductForSelect";
            }
        }

        public override string QueryPanelKey {
            get {
                return "ProductForSelect";
            }
        }

        public override string Title {
            get {
                return CommonUIStrings.QueryPanel_Title_ProductCode;
            }
        }
    }
}
