﻿using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    public class ProductQueryWindow : DcsQueryWindowBase {
        public ProductQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
        }

        public override string DataGridViewKey {
            get {
                return "ProductForSelect";
            }
        }

        public override string QueryPanelKey {
            get {
                return "ProductForSelect";
            }
        }
    }
}
