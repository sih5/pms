﻿using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    public class ProductStandardQueryWindow : DcsQueryWindowBase {
        public ProductStandardQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsBaseDataStatus.有效);
        }

        public override string DataGridViewKey {
            get {
                return "ProductExecutiveStandard";
            }
        }

        public override string QueryPanelKey {
            get {
                return "ProductExecutiveStandard";
            }
        }
    }
}
