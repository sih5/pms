﻿using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    /// <summary>
    /// 维修索赔申请管理选择祸首件
    /// </summary>
    public class RepairClaimApplicationFaultyPartsQueryWindow : DcsQueryWindowBase {
        public RepairClaimApplicationFaultyPartsQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string DataGridViewKey {
            get {
                return "RepairClaimApplicationFaultyParts";
            }
        }

        public override string QueryPanelKey {
            get {
                return "FaultyParts";
            }
        }
    }
}
