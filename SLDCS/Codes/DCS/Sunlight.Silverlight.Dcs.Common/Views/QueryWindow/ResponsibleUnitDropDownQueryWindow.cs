﻿using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    /// <summary>
    /// 选择责任单位
    /// </summary>
    public class ResponsibleUnitDropDownQueryWindow : DcsDropDownQueryWindowBase {

        public override string Title {
            get {
                return CommonUIStrings.QueryPanel_Title_ResponsibleUnit;
            }
        }

        public override string DataGridViewKey {
            get {
                return "ResponsibleUnit";
            }
        }

        public override string QueryPanelKey {
            get {
                return "ResponsibleUnit";
            }
        }
    }
}
