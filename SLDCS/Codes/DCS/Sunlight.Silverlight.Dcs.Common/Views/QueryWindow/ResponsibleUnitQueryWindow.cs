﻿namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    /// <summary>
    /// 选择责任单位
    /// </summary>
    public class ResponsibleUnitQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "ResponsibleUnit";
            }
        }

        public override string QueryPanelKey {
            get {
                return "ResponsibleUnit";
            }
        }
    }
}
