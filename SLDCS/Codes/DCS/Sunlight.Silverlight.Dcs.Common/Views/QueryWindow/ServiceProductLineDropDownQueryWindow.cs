﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    public class ServiceProductLineDropDownQueryWindow : DcsDropDownQueryWindowBase {

        public ServiceProductLineDropDownQueryWindow() {
            this.SetDefaultFilterItem(new FilterItem {
                MemberName = "BranchId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
        }

        public override string DataGridViewKey {
            get {
                return "ServiceProductLine";
            }
        }

        public override string QueryPanelKey {
            get {
                return "ServiceProductLine";
            }
        }

        public override string Title {
            get {
                return CommonUIStrings.QueryPanel_Title_ServiceProductLine;
            }
        }
    }
}
