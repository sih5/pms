﻿using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    public class ServiceProductLineViewMultipleQueryWindow : DcsMultiPopupsQueryWindowBase{

        public ServiceProductLineViewMultipleQueryWindow(){
            this.SetDefaultFilterItem(new FilterItem{
                MemberName = "BranchId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
        }

        public override string DataGridViewKey {
            get {
                return "ServiceProductLineViewMultiple";
            }
        }

        public override string QueryPanelKey {
            get {
                return "ServiceProductLineView";
            }
        }
    }
}
