﻿using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    public class ServiceProductLineViewQueryWindow : DcsQueryWindowBase {

        public ServiceProductLineViewQueryWindow() {
            this.SetDefaultFilterItem(new FilterItem {
                MemberName = "BranchId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
        }

        public override string DataGridViewKey {
            get {
                return "ServiceProductLineView";
            }
        }

        public override string QueryPanelKey {
            get {
                return "ServiceProductLineView";
            }
        }
    }
}
