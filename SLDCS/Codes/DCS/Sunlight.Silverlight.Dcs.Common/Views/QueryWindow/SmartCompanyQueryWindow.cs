﻿

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    public class SmartCompanyQueryWindow : DcsMultiPopupsQueryWindowBase {
        public SmartCompanyQueryWindow() {
        }

        public override string QueryPanelKey {
            get {
                return "CompanyForApproveBill";

            }
        }

        public override string DataGridViewKey {
            get {
                return "SmartCompanyForSelect";
            }
        }
    }
}
