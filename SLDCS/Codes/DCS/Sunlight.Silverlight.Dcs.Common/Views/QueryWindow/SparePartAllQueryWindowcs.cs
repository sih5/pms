﻿using System.Collections.Generic;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    /// <summary>
    /// 选择配件信息(多选)
    /// </summary>
    public class SparePartAllQueryWindowcs : DcsMultiSelectQueryWindowBase {
        public SparePartAllQueryWindowcs() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }
        public override IEnumerable<Entity> SelectedEntities {
                get {
                    return this.DataGridViewses["SparePart"].SelectedEntities;
                }
            }

            public override string[] DataGridViewKeies {
                get {
                    return new[] { 
                    "SparePart"
                };
                }
            }

            public override string[] GridViewTitles {
                get {
                    return new[] {
                    ""
                };
                }
            }

            public override string QueryPanelKey {
                get {
                    return "SparePart";
                }
            }
        }
    }
}
