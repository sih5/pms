﻿using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    /// <summary>
    /// 选择配件信息
    /// </summary>
    public class SparePartDropDownQueryWindow : DcsDropDownQueryWindowBase {
        public SparePartDropDownQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string Title {
            get {
                return CommonUIStrings.QueryPanel_Title_SparePart;
            }
        }

        public override string DataGridViewKey {
            get {
                return "SparePart";
            }
        }

        public override string QueryPanelKey {
            get {
                return "SparePartForWindow";
            }
        }
    }
}
