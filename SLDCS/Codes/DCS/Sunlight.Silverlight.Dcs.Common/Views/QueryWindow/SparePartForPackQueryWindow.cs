﻿using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    public class SparePartForPackQueryWindow : DcsQueryWindowBase {
        public SparePartForPackQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
          //  this.SetDefaultQueryItemEnable("Common", "PartType", false);
        }

        public override string DataGridViewKey {
            get {
                return "SparePart";
            }
        }

        public override string QueryPanelKey {
            get {
                return "SparePartForPick";
            }
        }
    }
}
