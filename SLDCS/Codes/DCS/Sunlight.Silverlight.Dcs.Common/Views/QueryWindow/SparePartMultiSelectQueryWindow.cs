﻿using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    public class SparePartMultiSelectQueryWindow : DcsMultiPopupsQueryWindowBase {
        public SparePartMultiSelectQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string QueryPanelKey {
            get {
                return "SparePartForWindow";
            }
        }

        public override string DataGridViewKey {
            get {
                return "SparePart";
            }
        }
    }
}
