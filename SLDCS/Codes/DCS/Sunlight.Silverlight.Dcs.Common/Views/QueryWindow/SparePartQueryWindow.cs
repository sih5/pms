﻿using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    /// <summary>
    /// 选择配件信息
    /// </summary>
    public class SparePartQueryWindow : DcsQueryWindowBase {
        public SparePartQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string DataGridViewKey {
            get {
                return "SparePart";
            }
        }

        public override string QueryPanelKey {
            get {
                return "SparePartForWindow";
            }
        }
    }
}
