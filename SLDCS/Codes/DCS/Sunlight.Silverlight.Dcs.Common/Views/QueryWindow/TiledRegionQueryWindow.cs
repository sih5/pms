﻿namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    /// <summary>
    /// 区域选择
    /// </summary>
    public class TiledRegionQueryWindow : DcsQueryWindowBase {
        public override string DataGridViewKey {
            get {
                return "TiledRegion";
            }
        }

        public override string QueryPanelKey {
            get {
                return "TiledRegion";
            }
        }
    }
}
