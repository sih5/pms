﻿
namespace Sunlight.Silverlight.Dcs.Common.Views.QueryWindow {
    public class VirtualSpartPartByClassQueryWindow : DcsMultiPopupsQueryWindowBase {
        public override string QueryPanelKey {
            get {
                return "VirtualSpartPartByClass";
            }
        }

        public override string DataGridViewKey {
            get {
                return "VirtualSpartPartByClass";
            }
        }
    }
}
