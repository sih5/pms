﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Views {
      [PageMeta("Common", "SIHSmart", "ReserveFactorMasterOrder", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON,CommonActionKeys.EXPORT
    })]
    public class ReserveFactorMasterOrderManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DcsDomainContext dcsDomainContext = new DcsDomainContext();
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("ReserveFactorMasterOrder"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("ReserveFactorMasterOrder");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        public ReserveFactorMasterOrderManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = "储备系数管理";
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "ReserveFactorMasterOrder"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var reserveFactorMasterOrder = this.DataEditView.CreateObjectToEdit<ReserveFactorMasterOrder>();
                    reserveFactorMasterOrder.Status = (int)DcsBaseDataStatus.有效;
                    reserveFactorMasterOrder.ReserveCoefficient = 1;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null) {
                        this.Export导出储备系数(null, null, null, null, null, null, null, null, null);
                        return;
                    }                   
                    var agencys = this.DataGridView.SelectedEntities;
                    if(agencys != null) {
                        var ids = this.DataGridView.SelectedEntities.Cast<ReserveFactorMasterOrder>().Select(r => r.Id).ToArray();
                        this.Export导出储备系数(ids, null, null, null, null, null,null,null,null);
                    } else {
                        var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                        var aBCStrategyId = filterItem.Filters.Single(e => e.MemberName == "ABCStrategyId").Value as int?;
                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        var reserveCoefficient = filterItem.Filters.Single(e => e.MemberName == "ReserveCoefficient").Value as int?;
                        DateTime? bStartTime = null;
                        DateTime? eStartTime = null;
                        DateTime? bModifyTime = null;
                        DateTime? eModifyTime = null;
                        foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var dateTime = filter as CompositeFilterItem;
                            if(dateTime != null) {
                                if(dateTime.Filters.First().MemberName == "CreateTime") {
                                    bStartTime = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    eStartTime = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                                if(dateTime.Filters.First().MemberName == "ModifyTime") {
                                    bModifyTime = dateTime.Filters.First(r => r.MemberName == "ModifyTime").Value as DateTime?;
                                    eModifyTime = dateTime.Filters.Last(r => r.MemberName == "ModifyTime").Value as DateTime?;
                                }
                            }
                        }
                        this.Export导出储备系数(null, warehouseId, status, aBCStrategyId, reserveCoefficient, bStartTime, eStartTime, bModifyTime, eModifyTime);
                    }

                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<ReserveFactorMasterOrder>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废储备系数主单)
                                entity.作废储备系数主单();
                            this.ExecuteSerivcesMethod(CommonUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
            }
        }
        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }
       
        private void Export导出储备系数(int[] ids, int? warehouseId, int? status, int? aBCStrategyId, int? reserveCoefficient, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bModifyTime, DateTime? eModifyTime) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.导出储备系数Async(ids, warehouseId, status, aBCStrategyId, reserveCoefficient, bCreateTime, eCreateTime, bModifyTime, eModifyTime);
            this.excelServiceClient.导出储备系数Completed -= excelServiceClient_Export导出储备系数Completed;
            this.excelServiceClient.导出储备系数Completed += excelServiceClient_Export导出储备系数Completed;
        }

        private void excelServiceClient_Export导出储备系数Completed(object sender, 导出储备系数CompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.filename));
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:               
                case CommonActionKeys.EXPORT:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<ReserveFactorMasterOrder>().ToArray();
                    if(entities.Length != 1)
                        return false;                   
                    return entities[0].Status == (int)DcsMasterDataStatus.有效;
                default:
                    return false;
            }
        }
    }
}