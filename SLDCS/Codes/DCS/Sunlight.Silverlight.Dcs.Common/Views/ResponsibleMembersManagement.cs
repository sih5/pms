﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("Common", "EnterpriseAndInternalOrganization", "ResponsibleMembers", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_EXPORT
    })]
    public class ResponsibleMembersManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        public ResponsibleMembersManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = "责任组人员维护";
        }

        public DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null)
                    this.dataGridView = DI.GetDataGridView("ResponsibleMembers");
                return this.dataGridView;
            }
        }

        public DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("ResponsibleMembers");
                    this.dataEditView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }


        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[]{
                    "ResponsibleMembers"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var responsibleMember = this.DataEditView.CreateObjectToEdit<ResponsibleMember>();
                    responsibleMember.Status = (int)DcsBaseDataStatus.有效;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<ResponsibleMember>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废责任组人员维护)
                                entity.作废责任组人员维护();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;

                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var id = this.DataGridView.SelectedEntities.Cast<ResponsibleMember>().First().Id;
                        this.ExportResponsibleMembersWithDetails(id, null, null);
                    } else {
                        var resTem = filterItem.Filters.Single(e => e.MemberName == "ResTem").Value as int?;
                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        ShellViewModel.Current.IsBusy = true;
                        this.ExportResponsibleMembersWithDetails(null, resTem, status);
                    }
                    break;
                default:
                    break;
            }
        }


        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();

            ResetEditView();
        }


        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<ResponsibleMember>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsBaseDataStatus.有效;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        private void ExportResponsibleMembersWithDetails(int? id, int? resTem, int? status) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportResponsibleMembersWithDetailsAsync(id, resTem, status);
            this.excelServiceClient.ExportResponsibleMembersWithDetailsCompleted -= excelServiceClient_ExportResponsibleMembersWithDetailsCompleted;
            this.excelServiceClient.ExportResponsibleMembersWithDetailsCompleted += excelServiceClient_ExportResponsibleMembersWithDetailsCompleted;
        }

        private void excelServiceClient_ExportResponsibleMembersWithDetailsCompleted(object sender, ExportResponsibleMembersWithDetailsCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
    }
}