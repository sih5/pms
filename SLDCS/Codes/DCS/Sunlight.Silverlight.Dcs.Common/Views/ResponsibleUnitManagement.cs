﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("Common", "Company", "ResponsibleUnit", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_PAUSE_RESUME_DETAIL,"ResponsibleUnit"
    })]
    public class ResponsibleUnitManagement : DcsDataManagementViewBase {
        private const string DETAIL_DATA_EDIT_VIEW = "_DetailDataEditView_";
        private const string SETUP_DETAIL_DATA_EDIT_VIEW = "_ProductDetailDataEditView_";
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase detailDataEditView;
        private DataEditViewBase forSetupDetailDataEditView;

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("ResponsibleUnit"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("ResponsibleUnit");
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DetailDataEditView {
            get {
                if(this.detailDataEditView == null) {
                    this.detailDataEditView = DI.GetDataEditView("ResponsibleUnitDetail");
                    this.detailDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.detailDataEditView;
            }
        }

        private DataEditViewBase ForSetupDetailDataEditView {
            get {
                if(this.forSetupDetailDataEditView == null) {
                    this.forSetupDetailDataEditView = DI.GetDataEditView("ResponsibleUnitForSetup");
                    this.forSetupDetailDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                    this.forSetupDetailDataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.forSetupDetailDataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.detailDataEditView=null;
            this.forSetupDetailDataEditView=null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        public ResponsibleUnitManagement() {
            this.Initializer.Register(this.CreateUI);
            this.Title = CommonUIStrings.DataManagementView_Title_ResponsibleUnit;
        }

        private void CreateUI() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DETAIL_DATA_EDIT_VIEW, () => this.DetailDataEditView);
            this.RegisterView(SETUP_DETAIL_DATA_EDIT_VIEW, () => this.ForSetupDetailDataEditView);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            var compositeFilterItem = new CompositeFilterItem();
            if(filterItem != null) {
                var originalFilterItem = filterItem as CompositeFilterItem;
                if(originalFilterItem != null)
                    foreach(var filter in originalFilterItem.Filters)
                        compositeFilterItem.Filters.Add(filter);
                else
                    compositeFilterItem.Filters.Add(filterItem);
            }
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var responsibleUnit = this.DataEditView.CreateObjectToEdit<ResponsibleUnit>();
                    responsibleUnit.Status = (int)DcsMasterDataStatus.有效;
                    responsibleUnit.Company = new Company();
                    responsibleUnit.Company.Type = (int)DcsCompanyType.责任单位;
                    responsibleUnit.Company.Status = (int)DcsMasterDataStatus.有效;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.DETAIL:
                    this.DetailDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DETAIL_DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<ResponsibleUnit>().SingleOrDefault();
                        if(entity == null)
                            return;
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(domainContext == null)
                            return;
                        try {
                            if(entity.Can作废责任单位)
                                entity.作废责任单位();
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.PAUSE:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Pause, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<ResponsibleUnit>().SingleOrDefault();
                        if(entity == null)
                            return;
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(domainContext == null)
                            return;
                        try {
                            if(entity.Can停用责任单位)
                                entity.停用责任单位();
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_PauseSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.RESUME:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Resume, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<ResponsibleUnit>().SingleOrDefault();
                        if(entity == null)
                            return;
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(domainContext == null)
                            return;
                        try {
                            if(entity.Can恢复责任单位)
                                entity.恢复责任单位();
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_ResumeSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "Setup":
                    this.ForSetupDetailDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(SETUP_DETAIL_DATA_EDIT_VIEW);
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.DETAIL:
                case CommonActionKeys.PAUSE:
                case CommonActionKeys.RESUME:
                case "Setup":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<ResponsibleUnit>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    if(uniqueId.Equals(CommonActionKeys.DETAIL))
                        return true;
                    if(uniqueId.Equals(CommonActionKeys.RESUME))
                        return entities[0].Status == (int)DcsMasterDataStatus.停用;
                    return entities[0].Status == (int)DcsMasterDataStatus.有效;
                default:
                    return false;
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "ResponsibleUnit"
                };
            }
        }
    }
}
