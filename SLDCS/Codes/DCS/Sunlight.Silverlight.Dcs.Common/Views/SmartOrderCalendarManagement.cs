﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("Common", "SIHSmart", "SmartOrderCalendar", ActionPanelKeys = new[] {
        "SmartOrderCalendar"
    })]
    public class SmartOrderCalendarManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataImportEditView;
        private const string DATA_IMPORT_VIEW = "_DataImportView_";
        private DcsDomainContext dcsDomainContext = new DcsDomainContext();
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("SmartOrderCalendar"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("SmartOrderCalendar");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private DataEditViewBase ImportDataEditView {
            get {
                if(this.dataImportEditView == null) {
                    this.dataImportEditView = DI.GetDataEditView("SmartOrderCalendarImport");
                    this.dataImportEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataImportEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataImportEditView;
            }
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_IMPORT_VIEW, () => this.ImportDataEditView);
        }

        public SmartOrderCalendarManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.QueryPanel_Title_SmartOrderCalendarTitle;
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "SmartOrderCalendar"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var sparePart = this.DataEditView.CreateObjectToEdit<SmartOrderCalendar>();
                    sparePart.Status = (int)DcsBaseDataStatus.有效;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                     this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(DATA_IMPORT_VIEW);
                    break;
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null) {
                        this.Export智能订货日历(null,null, null, null, null, null);
                        return;
                    }                   
                    var agencys = this.DataGridView.SelectedEntities;
                    if(agencys != null) {
                        var ids = this.DataGridView.SelectedEntities.Cast<SmartOrderCalendar>().Select(r => r.Id).ToArray();
                        ShellViewModel.Current.IsBusy = true;
                        this.Export智能订货日历(ids, null, null, null, null,null);
                    } else {
                        var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        DateTime? bStartTime = null;
                        DateTime? eStartTime = null;

                        foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var dateTime = filter as CompositeFilterItem;
                            if(dateTime != null) {
                                if(dateTime.Filters.First().MemberName == "CreateTime") {
                                    bStartTime = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    eStartTime = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                            }
                        }
                        ShellViewModel.Current.IsBusy = true;
                        this.Export智能订货日历(null, warehouseId,status, null, bStartTime, eStartTime);
                    }

                    break;
                case CommonActionKeys.ABANDON:
                     DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Abandon, () => {
                         var entity = this.DataGridView.SelectedEntities.Cast<SmartOrderCalendar>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废OrderCalendar)
                                entity.作废OrderCalendar();
                            this.ExecuteSerivcesMethod(CommonUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;

            }
        }
        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }
        private void Export智能订货日历(int[] ids, int? warehouseId,int?status, int? times, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.Export智能订货日历Async(ids, warehouseId,status,  createTimeBegin, createTimeEnd);
            this.excelServiceClient.Export智能订货日历Completed -= excelServiceClient_Export智能订货日历Completed;
            this.excelServiceClient.Export智能订货日历Completed += excelServiceClient_Export智能订货日历Completed;
        }

        private void excelServiceClient_Export智能订货日历Completed(object sender, Export智能订货日历CompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                case CommonActionKeys.IMPORT:
                case CommonActionKeys.EXPORT:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<SmartOrderCalendar>().ToArray();
                    if(entities.Length != 1)
                        return false;                   
                    return entities[0].Status == (int)DcsMasterDataStatus.有效;
                default:
                    return false;
            }
        }
    }
}