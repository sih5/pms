﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("Common", "SIHSmart", "SmartProcessMethod", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_ABANDON,CommonActionKeys.EXPORT
    })]
    public class SmartProcessMethodManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DcsDomainContext dcsDomainContext = new DcsDomainContext();
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("SmartProcessMethod"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("SmartProcessMethod");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        public SmartProcessMethodManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = "智能订货处理方式管理";
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "SmartProcessMethod"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var smartProcessMethod = this.DataEditView.CreateObjectToEdit<SmartProcessMethod>();
                    smartProcessMethod.Status = (int)DcsBaseDataStatus.有效;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;

                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null) {
                        this.导出智能订货处理方式配置(null, null, null, null, null, null);
                        return;
                    }
                    var agencys = this.DataGridView.SelectedEntities;
                    if(agencys != null) {
                        var ids = this.DataGridView.SelectedEntities.Cast<SmartProcessMethod>().Select(r => r.Id).ToArray();
                        this.导出智能订货处理方式配置(ids, null, null, null, null, null);
                    } else {
                        var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                        var orderProcessMethod = filterItem.Filters.Single(e => e.MemberName == "OrderProcessMethod").Value as int?;
                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        DateTime? bStartTime = null;
                        DateTime? eStartTime = null;

                        foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var dateTime = filter as CompositeFilterItem;
                            if(dateTime != null) {
                                if(dateTime.Filters.First().MemberName == "CreateTime") {
                                    bStartTime = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    eStartTime = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                            }
                        }
                        this.导出智能订货处理方式配置(null, warehouseId, orderProcessMethod, status, bStartTime, eStartTime);
                    }

                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<SmartProcessMethod>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废智能订货处理方式配置)
                                entity.作废智能订货处理方式配置();
                            this.ExecuteSerivcesMethod(CommonUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;

            }
        }
        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }
        private void 导出智能订货处理方式配置(int[] ids, int? warehouseId, int? orderProcessMethod, int? status, DateTime? bCreateTime, DateTime? eCreateTime) {
            ShellViewModel.Current.IsBusy = true;
            this.dcsDomainContext.导出智能订货处理方式配置(ids, warehouseId, orderProcessMethod, status, bCreateTime, eCreateTime, loadOp => {
                if(loadOp.HasError) {
                    ShellViewModel.Current.IsBusy = false;
                    return;
                }
                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                    UIHelper.ShowNotification(loadOp.Value);
                    ShellViewModel.Current.IsBusy = false;
                }

                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                    ShellViewModel.Current.IsBusy = false;
                }
            }, null);
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                case CommonActionKeys.EXPORT:
                    return true;
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<SmartProcessMethod>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsMasterDataStatus.有效;
                default:
                    return false;
            }
        }
    }
}