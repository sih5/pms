﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("PartsBaseInfo", "SparePartPurchase", "SparePartDetails", ActionPanelKeys = new[] {
        "SparePartDetails"
    })]
    public class SparePartDetailsManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        public SparePartDetailsManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataManagementView_Title_SparePartDetailsManagement;
        }
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("SparePartPurchasePricing"));
            }
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "SparePartPurchasePricing"
                };
            }
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);

        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return;
            var codes = compositeFilterItem.Filters.SingleOrDefault(s => s.MemberName == "Code");
            if (codes != null && codes.Value != null) {
                compositeFilterItem.Filters.Add(new CompositeFilterItem {
                    MemberName = "Code",
                    MemberType = typeof(string),
                    Value = string.Join(",", ((IEnumerable<string>)codes.Value).ToArray()),
                    Operator = FilterOperator.Contains
                });
                compositeFilterItem.Filters.Remove(codes);
            }  
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                case "ExactExport":
                    ShellViewModel.Current.IsBusy = true;
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;                   
                    var partCode = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                    var partName = filterItem.Filters.Single(r => r.MemberName == "Name").Value as string;
                    var referenceCode = filterItem.Filters.Single(r => r.MemberName == "ReferenceCode").Value as string;
                    DateTime? validFromBegin = null;
                    DateTime? validFromEnd = null;
                    DateTime? partCreateTimeBegin = null;
                    DateTime? partCreateTimeEnd = null;
                    
                    var createTime = filterItem.Filters.FirstOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem) && ((CompositeFilterItem)filter).Filters.Any(e => e.MemberName == "CreateTime")) as CompositeFilterItem;  
                    if(createTime != null) {  
                        validFromBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;  
                        validFromEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;  
                    }  
                    var partCreateTime = filterItem.Filters.FirstOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem) && ((CompositeFilterItem)filter).Filters.Any(e => e.MemberName == "PartCreateTime")) as CompositeFilterItem;  
                    if(partCreateTime != null) {  
                        partCreateTimeBegin = partCreateTime.Filters.First(r => r.MemberName == "PartCreateTime").Value as DateTime?;  
                        partCreateTimeEnd = partCreateTime.Filters.Last(r => r.MemberName == "PartCreateTime").Value as DateTime?;  
                    }

                    var isprice = filterItem.Filters.Single(e => e.MemberName == "Isprice").Value as bool?;
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var detailId = this.DataGridView.SelectedEntities.Cast<PartsPurchasePricingForQuery>().Select(r => r.Id).ToArray();
                        this.ExportPartsPurchasePricingForQuery(detailId, null, null, null, null, null,null,null,null,null);
                    }

                    if (uniqueId.Equals("ExactExport")) {
                        if (string.IsNullOrEmpty(partCode)) { 
                            ShellViewModel.Current.IsBusy = false;
                            UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Validation_VirtualFactoryPurchacePrice_sparePartCode);
                            return;
                        }
                         this.ExportPartsPurchasePricingForQuery(null, partCode, partName, validFromBegin, validFromEnd,partCreateTimeBegin,partCreateTimeEnd, isprice,true,referenceCode);               
                    } else { 
                         this.ExportPartsPurchasePricingForQuery(null, partCode, partName, validFromBegin, validFromEnd,partCreateTimeBegin,partCreateTimeEnd, isprice,false,referenceCode);                   
                    }

                    ShellViewModel.Current.IsBusy = false;
                    break;
            }
        }
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public void ExportPartsPurchasePricingForQuery(int[] ids, string code, string name, DateTime? bCreateTime, DateTime? eCreateTime,DateTime? bPartCreateTime, DateTime? ePartCreateTime, bool? isprice,bool? isExactExport,string referenceCode)
        {

            this.excelServiceClient.ExportPartsPurchasePricingForQueryAsync(ids, code, name, bCreateTime, eCreateTime,bPartCreateTime,ePartCreateTime, isprice,isExactExport,referenceCode);
            this.excelServiceClient.ExportPartsPurchasePricingForQueryCompleted -= ExcelServiceClient_ExportDealerPartsTransferOrderAndDetailCompleted;
            this.excelServiceClient.ExportPartsPurchasePricingForQueryCompleted += ExcelServiceClient_ExportDealerPartsTransferOrderAndDetailCompleted;
        }
        private void ExcelServiceClient_ExportDealerPartsTransferOrderAndDetailCompleted(object sender, ExportPartsPurchasePricingForQueryCompletedEventArgs e)
        {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case "ExactExport":
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return false;                   
                    var partCode = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any() && !string.IsNullOrEmpty(partCode);
                default:
                    return false;
            }
        }
    }
}
