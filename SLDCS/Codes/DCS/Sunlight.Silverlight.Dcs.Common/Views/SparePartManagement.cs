﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("PartsBaseInfo", "SparePart", "SparePart", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_PAUSE_RESUME_EXPORT_IMPORT,"SparePart"
    })]
    public class SparePartManagement : DcsDataManagementViewBase {
        private DataEditViewBase dataEditView;
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditViewPause;
        private DataEditViewBase dataEditViewResume;
        private DataEditViewBase dataEditViewImport;
        private DataEditViewBase uploadDataEditView;
        private DataEditViewBase dataDetailView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private const string DATA_EDIT_VIEW_PAUSE = "_DataEditViewPause_";
        private const string DATA_EDIT_VIEW_RESUME = "_DataEditViewResume_";
        private const string DATA_EDIT_VIEW_IMPORT = "_DataEditViewImport_";
        private const string DATA_EDIT_VIEW_IMPORTREPLACE = "_DataEditViewImportReplace_";
        protected const string Upload_DATA_EDIT_VIEW = "_UploadDataEditView_";
        private const string DATA_DETAIL_VIEW = "_DataDetailView_";
        private readonly string[] kvNames = { "SparePart_MeasureUnit" };
        private ObservableCollection<KeyValuePair> kvMeasureUnit;
        private ObservableCollection<KeyValuePair> KvMeasureUnit {
            get {
                return this.kvMeasureUnit ?? (this.kvMeasureUnit = new ObservableCollection<KeyValuePair>());
            }
        }
        private KeyValueManager keyValueManager;
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        public SparePartManagement() {
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData(() => {
                foreach(var item in this.KeyValueManager[kvNames[0]])
                    KvMeasureUnit.Add(item);
            });
            this.Initializer.Register(this.Initialize);
            this.Title = CommonUIStrings.DataManagementView_Title_SparePart;
        }
        private DataEditViewBase DataEditViewDetail
        {
            get
            {
                if (this.dataDetailView == null)
                {
                    this.dataDetailView = DI.GetDataEditView("SparePartDetail");
                    this.dataDetailView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataDetailView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_PAUSE, () => this.DataEditViewPause);
            this.RegisterView(DATA_EDIT_VIEW_RESUME, () => this.DataEditViewResume);
            this.RegisterView(DATA_EDIT_VIEW_IMPORT, () => this.DataEditViewImport);
            this.RegisterView(DATA_EDIT_VIEW_IMPORTREPLACE, () => this.SparePartForBatchReplacement);
            this.RegisterView(Upload_DATA_EDIT_VIEW, () => this.UploadDataEditView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataEditViewDetail);

        }

        private DataGridViewBase DataGridView {
            get {

                if (this.dataGridView == null)
                {
                    this.dataGridView = DI.GetDataGridView("SparePartWithDetails");
                    this.dataGridView.RowDoubleClick += DataGridView_RowDoubleClick;
                }
                return this.dataGridView;
            }
        }
        private void DataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e)
        {
            if (this.DataGridView.SelectedEntities !=null && this.DataGridView.SelectedEntities.Any()) { 
                this.DataEditViewDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                this.SwitchViewTo(DATA_DETAIL_VIEW);
            }
        }
        private DataEditViewBase sparePartForBatchReplacement;
        private DataEditViewBase SparePartForBatchReplacement {
            get {
                if(this.sparePartForBatchReplacement == null) {
                    this.sparePartForBatchReplacement = DI.GetDataEditView("SparePartForBatchReplacement");
                    this.sparePartForBatchReplacement.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.sparePartForBatchReplacement;
            }
        }
        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("SparePart");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataEditViewImport {
            get {
                if(this.dataEditViewImport == null) {
                    this.dataEditViewImport = DI.GetDataEditView("SparePartForImport");
                    this.dataEditViewImport.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewImport;
            }
        }

        private DataEditViewBase DataEditViewPause {
            get {
                if(this.dataEditViewPause == null) {
                    this.dataEditViewPause = DI.GetDataEditView("SparePartPause");
                    this.dataEditViewPause.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditViewPause.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewPause;
            }
        }

        private DataEditViewBase DataEditViewResume {
            get {
                if(this.dataEditViewResume == null) {
                    this.dataEditViewResume = DI.GetDataEditView("SparePartResume2");
                    this.dataEditViewResume.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditViewResume.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewResume;
            }
        }
        //附件上传
        private DataEditViewBase UploadDataEditView
        {
            get
            {
                if (this.uploadDataEditView == null)
                {
                    this.uploadDataEditView = DI.GetDataEditView("SparePartForUpload");
                    this.uploadDataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.uploadDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.uploadDataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewPause=null;
            this.dataEditViewResume = null;
            this.dataEditViewImport = null;
            this.uploadDataEditView = null;
            this.dataDetailView = null;
        }
        private void data_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "SparePart"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            var newFilterItem = new CompositeFilterItem();
            var compositeFilterItem = filterItem as CompositeFilterItem;
            int? MeasureUnitValue;
            if(compositeFilterItem != null) {
                foreach(var item in compositeFilterItem.Filters) {
                    if(item.MemberName == "MeasureUnit") {
                        MeasureUnitValue = item.Value as int?;
                        if(MeasureUnitValue.HasValue && MeasureUnitValue != -1) {
                            string MeasureUnitName = KvMeasureUnit.FirstOrDefault(r => r.Key == MeasureUnitValue.Value).Value;
                            newFilterItem.Filters.Add(new FilterItem("MeasureUnit", typeof(string), FilterOperator.IsEqualTo, MeasureUnitName));
                        }
                        continue;
                    }
                    newFilterItem.Filters.Add(item);
                }

                var codes = newFilterItem.Filters.SingleOrDefault(s => s.MemberName == "Code");
                if(codes != null && codes.Value != null){ 
                    newFilterItem.Filters.Add(new CompositeFilterItem { 
                        MemberName = "Code", 
                        MemberType = typeof(string), 
                        Value =string.Join(",",((IEnumerable<string>)codes.Value).ToArray()), 
                        Operator = FilterOperator.Contains 
                    }); 
                    newFilterItem.Filters.Remove(codes); 
                } 
            }
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = newFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var sparePart = this.DataEditView.CreateObjectToEdit<SparePart>();
                    sparePart.Status = (int)DcsMasterDataStatus.有效;
                    sparePart.PartType = (int)DcsSparePartPartType.配件;
                    sparePart.MInPackingAmount = 1;
                    //sparePart.Factury = "红岩";
                    sparePart.IsOriginal = 1;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(CommonUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<SparePart>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废配件基础信息)
                                entity.作废配件基础信息();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORT:
                case "ExactExport":
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<SparePart>().Select(r => r.Id).ToArray();
                        this.ExportSparePart(ids, null, null, null, null, null, null, null, null, null, null, null,null);
                    } else {

                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var name = filterItem.Filters.Single(r => r.MemberName == "Name").Value as string;
                        var referenceCode = filterItem.Filters.Single(r => r.MemberName == "ReferenceCode").Value as string;
                        //var overseasPartsFigure = filterItem.Filters.Single(r => r.MemberName == "OverseasPartsFigure").Value as string;
                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        var createTime = filterItem.Filters.FirstOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem) && ((CompositeFilterItem)filter).Filters.Any(e => e.MemberName == "CreateTime")) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        var modifierTime = filterItem.Filters.FirstOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem) && ((CompositeFilterItem)filter).Filters.Any(e => e.MemberName == "ModifyTime")) as CompositeFilterItem;
                        DateTime? modifierTimeBegin = null;
                        DateTime? modifierTimeTimeEnd = null;
                        if(modifierTime != null) {
                            modifierTimeBegin = modifierTime.Filters.First(r => r.MemberName == "ModifyTime").Value as DateTime?;
                            modifierTimeTimeEnd = modifierTime.Filters.Last(r => r.MemberName == "ModifyTime").Value as DateTime?;
                        }
                        //var exchangeIdentification = filterItem.Filters.Single(r => r.MemberName == "ExchangeIdentification").Value as string;
                        //var exGroupCode = filterItem.Filters.Single(r => r.MemberName == "ExGroupCode").Value as string;
                        ShellViewModel.Current.IsBusy = true;
                        if (uniqueId == CommonActionKeys.EXPORT) {
                            this.ExportSparePart(null, code, name, status, createTimeBegin, createTimeEnd, modifierTimeBegin, modifierTimeTimeEnd, referenceCode, null, null, null,null);
                        } else { 
                            if (string.IsNullOrEmpty(code)) {
                                ShellViewModel.Current.IsBusy = false;
                                UIHelper.ShowNotification(CommonUIStrings.DataManagementView_Validation_VirtualFactoryPurchacePrice_sparePartCode);
                                return;
                            }
                            this.ExportSparePart(null, code, name, status, createTimeBegin, createTimeEnd, modifierTimeBegin, modifierTimeTimeEnd, referenceCode, null, null, null,true);
                        }
                    }
                    break;
                case "ExportNoGoldenTaxClassify"://无金税分类导出
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<SparePart>().Select(r => r.Id).ToArray();
                        this.ExportNoGoldenTaxClassify(ids,null, null, null, null, null, null, null, null);
                    } else {

                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var name = filterItem.Filters.Single(r => r.MemberName == "Name").Value as string;
                        var referenceCode = filterItem.Filters.Single(r => r.MemberName == "ReferenceCode").Value as string;
                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        var createTime = filterItem.Filters.FirstOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem) && ((CompositeFilterItem)filter).Filters.Any(e => e.MemberName == "CreateTime")) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        var modifierTime = filterItem.Filters.FirstOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem) && ((CompositeFilterItem)filter).Filters.Any(e => e.MemberName == "ModifyTime")) as CompositeFilterItem;
                        DateTime? modifierTimeBegin = null;
                        DateTime? modifierTimeTimeEnd = null;
                        if(modifierTime != null) {
                            modifierTimeBegin = modifierTime.Filters.First(r => r.MemberName == "ModifyTime").Value as DateTime?;
                            modifierTimeTimeEnd = modifierTime.Filters.Last(r => r.MemberName == "ModifyTime").Value as DateTime?;
                        }
                        ShellViewModel.Current.IsBusy = true;
                        this.ExportNoGoldenTaxClassify(null, code, name, referenceCode, status, createTimeBegin, createTimeEnd, modifierTimeBegin, modifierTimeTimeEnd);
                    }
                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORT);
                    break;
                case CommonActionKeys.PAUSE:
                    this.DataEditViewPause.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_PAUSE);
                    break;
                case CommonActionKeys.RESUME:
                    this.DataEditViewResume.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_RESUME);
                    break;
                case "ReplaceName":
                case "ReplaceSpecification":
                case "ReplaceEnglishName":
                case "ReplacePartType":
                case "ReplaceFeature":
                case "ReplaceReferenceCode":
                case "ReplaceMInPackingAmount":
                case "ReplaceNextSubstitute":
                case "ReplaceIMSCompressionNumber":
                case "ReplaceIMSManufacturerNumber":
                case "ReplaceProductBrand":
                case "ReplaceStandardName":
                case "ReplaceOverseasPartsFigure":
                case "ReplaceImportEdit":
                case "ImportEdit":
                case "ResumeEdit":
                case "ImportGoldenTaxClassify":
                case "StandardImport":
                case "ImportExchangeIdentification":
                case "ImportFactury":
                case "ImportImportCategoryCode":
                case "ReplaceWarrantyTransfer":
                case "ReplaceTraceProperty":
                case "ImportIsSupplier":
                case "ReplaceSafeDays":
                case "ReplaceWarehousDays":
                case "ReplaceTemDays":
                    this.SparePartForBatchReplacement.SetObjectToEditById(uniqueId);
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORTREPLACE);
                    break;
                case CommonActionKeys.UPLOAD://附件上传
                    this.UploadDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.Cast<SparePart>().First().Id);
                    this.SwitchViewTo(Upload_DATA_EDIT_VIEW);
                    break;
            }
        }

        private void ExportSparePart(int[] ids, string code, string name, int? status, DateTime? createDateTimeStart, DateTime? createDateTimeEnd, DateTime? modifyDateTimeStart, DateTime? modifyDateTimeEnd, string ReferenceCode, string OverseasPartsFigure, string exchangeIdentification, string exGroupCode,bool? isExactExport) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportSparePartAsync(ids, code, name, status, createDateTimeStart, createDateTimeEnd, modifyDateTimeStart, modifyDateTimeEnd, ReferenceCode, OverseasPartsFigure, exchangeIdentification, exGroupCode,isExactExport);
            this.excelServiceClient.ExportSparePartCompleted -= excelServiceClient_ExportSparePartCompleted;
            this.excelServiceClient.ExportSparePartCompleted += excelServiceClient_ExportSparePartCompleted;
        }

        private void excelServiceClient_ExportSparePartCompleted(object sender, ExportSparePartCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        private void ExportNoGoldenTaxClassify(int[] ids, string code, string name, string ReferenceCode, int? status, DateTime? createDateTimeStart, DateTime? createDateTimeEnd, DateTime? modifyDateTimeStart, DateTime? modifyDateTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportNoGoldenTaxClassifyAsync(ids, code, name, ReferenceCode, status, createDateTimeStart, createDateTimeEnd, modifyDateTimeStart, modifyDateTimeEnd);
            this.excelServiceClient.ExportNoGoldenTaxClassifyCompleted -= excelServiceClient_ExportNoGoldenTaxClassifyCompleted;
            this.excelServiceClient.ExportNoGoldenTaxClassifyCompleted += excelServiceClient_ExportNoGoldenTaxClassifyCompleted;
        }

        private void excelServiceClient_ExportNoGoldenTaxClassifyCompleted(object sender, ExportNoGoldenTaxClassifyCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                case CommonActionKeys.IMPORT:
                case "ReplaceName":
                case "ReplaceSpecification":
                case "ReplaceEnglishName":
                case "ReplacePartType":
                case "ReplaceFeature":
                case "ReplaceReferenceCode":
                case "ReplaceMInPackingAmount":
                case "ReplaceNextSubstitute":
                case "ReplaceIMSCompressionNumber":
                case "ReplaceIMSManufacturerNumber":
                case "ReplaceProductBrand":
                case "ReplaceStandardName":
                case "ReplaceOverseasPartsFigure":
                case "ReplaceImportEdit":
                case "ImportEdit":
                case "ResumeEdit":
                case "ImportGoldenTaxClassify":
                case "StandardImport":
                case "ImportExchangeIdentification":
                case "ImportFactury":
                case "ImportImportCategoryCode":
                case "ReplaceWarrantyTransfer":
                case "ReplaceTraceProperty":
                case "ImportIsSupplier":
                case "ReplaceSafeDays":
                case "ReplaceWarehousDays":
                case "ReplaceTemDays":
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.PAUSE:
                case CommonActionKeys.RESUME:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<SparePart>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    if((String.CompareOrdinal(uniqueId, CommonActionKeys.RESUME) == 0))
                        return entities[0].Status == (int)DcsMasterDataStatus.停用;
                    return entities[0].Status == (int)DcsMasterDataStatus.有效;
                case CommonActionKeys.EXPORT:
                case "ExportNoGoldenTaxClassify":
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case "ExactExport":
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return false;
                    var sparePartCode = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any() && !string.IsNullOrEmpty(sparePartCode); 
                case CommonActionKeys.UPLOAD://附件上传
                    if (this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return true;
                default:
                    return false;
            }
        }
    }
}
