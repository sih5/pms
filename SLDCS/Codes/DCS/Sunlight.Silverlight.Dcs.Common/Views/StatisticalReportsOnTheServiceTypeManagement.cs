﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Common.Views {
    [PageMeta("Report", "ServiceInfo", "StatisticalReportsOnTheServiceType")]
    public class StatisticalReportsOnTheServiceTypeManagement : DcsDataManagementViewBase {
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public StatisticalReportsOnTheServiceTypeManagement() {
            Title = CommonUIStrings.DataManagementView_Title_StatisticalReportsOnTheServiceTypeManagement;
        }

        string addUrlParameter;
        private void excelServiceClient_GetTokenCompleted(object sender, GetTokenCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            if(string.IsNullOrEmpty(e.Result)) {
                UIHelper.ShowAlertMessage(CommonUIStrings.DataManagementView_Validation_Service);
            } else {
                string url = "http://pmsrf.foton.com.cn:8080/BOE/OpenDocument/opendoc/openDocument.jsp?token=" + e.Result + "&sType=wid&sPath=[FTDCS]&sDocName=StatisticalReportsOnTheServiceType" + addUrlParameter;
                HtmlPage.Window.Navigate(new Uri(url, UriKind.Absolute), "_blank");
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "StatisticalReportsOnTheServiceType"
                };
            }
        }

        protected override void OnExecutingAction(Silverlight.View.ActionPanelBase actionPanel, string uniqueId) {

        }

        protected override void OnExecutingQuery(Silverlight.View.QueryPanelBase queryPanel, FilterItem filterItem) {
            addUrlParameter = "";

            var newCompositeFilterItem = filterItem as CompositeFilterItem;
            if(newCompositeFilterItem == null)
                return;

            var branchCode = newCompositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "BranchCode");
            if(branchCode == null) {
                UIHelper.ShowAlertMessage(CommonUIStrings.DataManagementView_Validation_Company_CompanyCode);
                return;
            }
            if(branchCode.Value != null && !string.IsNullOrWhiteSpace(branchCode.Value.ToString()))
                addUrlParameter += "&lsSBranchCode=" + Uri.EscapeUriString("%" + branchCode.Value + "%");


            var serviceStationType = newCompositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "ServiceStationType");
            if(serviceStationType != null && serviceStationType.Value != null && !string.IsNullOrWhiteSpace(serviceStationType.Value.ToString()))
                addUrlParameter += "&lsSServiceStationType=" + Uri.EscapeUriString(serviceStationType.Value.ToString());

            var cityLevel = newCompositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "CityLevel");
            if(cityLevel != null && cityLevel.Value != null && !string.IsNullOrWhiteSpace(cityLevel.Value.ToString()))
                addUrlParameter += "&lsSCityLevel=" + Uri.EscapeUriString(cityLevel.Value.ToString());

            var partsSalesCategoryId = newCompositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSalesCategoryId");
            if(partsSalesCategoryId != null && partsSalesCategoryId.Value != null && !string.IsNullOrWhiteSpace(partsSalesCategoryId.Value.ToString()))
                addUrlParameter += "&lsSPartsSalesCategoryId=" + Uri.EscapeUriString(partsSalesCategoryId.Value.ToString());

            excelServiceClient.GetTokenAsync("psmsadmin", "psms-admin", "pmsrf.foton.com.cn:6400", "secEnterprise", 1, 100);
            excelServiceClient.GetTokenCompleted -= excelServiceClient_GetTokenCompleted;
            excelServiceClient.GetTokenCompleted += excelServiceClient_GetTokenCompleted;
        }
    }
}