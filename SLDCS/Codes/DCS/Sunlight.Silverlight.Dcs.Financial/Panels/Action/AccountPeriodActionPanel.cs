﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Action
{
    public class AccountPeriodActionPanel : DcsActionPanelBase
    {
        public AccountPeriodActionPanel()
        {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "AccountPeriod",
                Title = FinancialUIStrings.ActionPanel_Title_AccountPeriod,
                ActionItems = new[] {
                    new ActionItem {
                        UniqueId = "Add",
                        Title = FinancialUIStrings.Action_Title_Add,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Add.png"),
                        CanExecute = false
                    },
                     new ActionItem {
                        UniqueId = "Close",
                        Title = FinancialUIStrings.Action_Title_Close,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Abandon.png"),
                        CanExecute = false
                    },new ActionItem {
                        UniqueId = "Open",
                        Title = FinancialUIStrings.Action_Title_Open,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Revocation.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        UniqueId = "MergeExport",
                        Title = FinancialUIStrings.Action_Title_MergeExport,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/MergeExport.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
