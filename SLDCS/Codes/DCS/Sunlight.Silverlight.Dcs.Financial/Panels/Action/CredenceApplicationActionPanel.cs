﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Action {
    public class CredenceApplicationActionPanel : DcsActionPanelBase {
        public CredenceApplicationActionPanel() {
         this.ActionItemGroup = new ActionItemGroup {
             UniqueId = "CredenceApplication",
             Title = FinancialUIStrings.QueryPanel_TitleCredenceApplication,
                ActionItems = new[] {
                    new ActionItem {
                        UniqueId = "AdvancedAudit",
                        Title=FinancialUIStrings.QueryPanel_TitleCredenceApplication_AdvancedAudit,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Service/FinalApprove.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        UniqueId = "AdvancedApprove",
                        Title=FinancialUIStrings.QueryPanel_TitleCredenceApplication_AdvancedApprove,
                         ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Service/FinalApprove.png"),
                        CanExecute = false
                    }
                }
            };
        
        }
    }
}
