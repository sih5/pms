﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Action {
    public class FinancialSnapshotSetActionPanel : DcsActionPanelBase {
        public FinancialSnapshotSetActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = FinancialUIStrings.ActionPanel_Title_FinancialSnapshotSet,
                ActionItems = new[] {
                    new ActionItem {
                        UniqueId = "HistoricalDataBase",
                        Title = FinancialUIStrings.Action_Title_HistoricalDataBase,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Query.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        UniqueId = "HistoryStockSum",
                        Title =  FinancialUIStrings.Action_Title_HistoryStockSum,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Query.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
