﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Action {
    public class FundMonthlySettleBillActionPanel : DcsActionPanelBase {
        public FundMonthlySettleBillActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "FundMonthlySettleBill",
                Title = FinancialUIStrings.ActionPanel_Title_General,
                ActionItems = new[] {
                    new ActionItem {
                        UniqueId = "Detail",
                        Title =  FinancialUIStrings.Action_Title_Detail,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Query.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
