﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Action {
    public class PaymentBillActionPanel : DcsActionPanelBase {
        public PaymentBillActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "PaymentBill",
                Title = FinancialUIStrings.ActionPanel_Title_General,
                ActionItems = new[] {
                    new ActionItem {
                        Title = FinancialUIStrings.Action_Title_Beneficiary,
                        UniqueId = "Beneficiary",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Beneficiary.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
