﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Action {
    public class PlannedPriceAppActionPanel : DcsActionPanelBase {
        public PlannedPriceAppActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "PlannedPriceApp",
                Title = FinancialUIStrings.ActionPanel_Title_General,
                ActionItems = new[] {
                     new ActionItem {
                        Title = FinancialUIStrings.Action_Title_WarehouseDetail,
                        UniqueId = "WarehouseDetail",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/WarehouseDetail.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}

