﻿
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.DataEdit {
    public partial class AccountGroupDataEditPanel {

        public AccountGroupDataEditPanel() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            var queryWindow = DI.GetQueryWindow("CompaniesAndAgencies");
            queryWindow.SelectionDecided += queryWindow_SelectionDecided;
            ptbCompany.PopupContent = queryWindow;
            //var dcsDomainContext = new DcsDomainContext();
            //dcsDomainContext.Load(dcsDomainContext.GetCompaniesAndAgenciesByLoginCompanyQuery(), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    this.KvCompanies.Clear();
            //    foreach(var company in loadOp.Entities) {
            //        this.kvCompanies.Add(new KeyValuePair {
            //            Key = company.Id,
            //            Value = company.Name,
            //            UserObject = company
            //        });
            //    }
            //}, null);
        }

        private void queryWindow_SelectionDecided(object sender, System.EventArgs e) {
            var accountGroup = this.DataContext as AccountGroup;
            if(accountGroup == null)
                return;
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var company = queryWindow.SelectedEntities.Cast<Company>().FirstOrDefault();
            if(company == null)
                return;
            accountGroup.SalesCompanyId = company.Id;
            accountGroup.SalesCompanyName = company.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }
    }
}
