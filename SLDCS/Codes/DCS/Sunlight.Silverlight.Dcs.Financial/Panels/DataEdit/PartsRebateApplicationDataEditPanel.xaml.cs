﻿

using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.DataEdit {
    public partial class PartsRebateApplicationDataEditPanel {
        private readonly string[] kvNames = {
            "PartsRebateDirection"
        };
        private ObservableCollection<KeyValuePair> kvAccountGroups;
        private ObservableCollection<KeyValuePair> kvPartsSalesCategorys;
        private ObservableCollection<KeyValuePair> kvPartsRebateTypes;
        public ObservableCollection<KeyValuePair> KvAccountGroups {
            get {
                return this.kvAccountGroups ?? (this.kvAccountGroups = new ObservableCollection<KeyValuePair>());
            }
        }
        public ObservableCollection<KeyValuePair> KvPartsSalesCategorys {
            get {
                return this.kvPartsSalesCategorys ?? (this.kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>());
            }
        }
        public ObservableCollection<KeyValuePair> KvPartsRebateTypes {
            get {
                return this.kvPartsRebateTypes ?? (this.kvPartsRebateTypes = new ObservableCollection<KeyValuePair>());
            }
        }
        public object KvRebateDirection {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
        public PartsRebateApplicationDataEditPanel() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(kvNames);
            this.KeyValueManager.LoadData();
            this.DataContextChanged += PartsRebateApplicationDataEditPanel_DataContextChanged;
        }

        private void PartsRebateApplicationDataEditPanel_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var partsRebateApplication = this.DataContext as PartsRebateApplication;
            if(partsRebateApplication == null)
                return;
            this.KvAccountGroups.Clear();
            partsRebateApplication.PropertyChanged -= partsRebateApplication_PropertyChanged;
            partsRebateApplication.PropertyChanged += partsRebateApplication_PropertyChanged;
        }
        private DcsDomainContext domainContext;

        private DcsDomainContext DomainContext {
            get {
                return this.domainContext ?? (this.domainContext = new DcsDomainContext());
            }
        }

        private void partsRebateApplication_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            var partsRebateApplication = this.DataContext as PartsRebateApplication;
            if(partsRebateApplication == null)
                return;
            switch(e.PropertyName) {
                case "PartsSalesCategoryId":
                    DomainContext.Load(DomainContext.GetAccountGroupsByPartsSalesCategoryIdQuery(partsRebateApplication.PartsSalesCategoryId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError)
                            return;
                        this.KvAccountGroups.Clear();
                        foreach(var accountGroup in loadOp.Entities) {
                            this.KvAccountGroups.Add(new KeyValuePair {
                                Key = accountGroup.Id,
                                Value = accountGroup.Name,
                                UserObject = accountGroup
                            });
                        }
                    }, null);
                    break;
            }
        }
        private void CreateUI() {
            DomainContext.Load(DomainContext.GetPartsSalesCategoriesQuery().Where(v => v.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && v.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvPartsSalesCategorys.Clear();
                foreach(var item in loadOp.Entities) {
                    this.KvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.Name,
                        UserObject = item
                    });
                }
            }, null);
            this.cbxPartsSalesCategory.SelectionChanged += cbxPartsSalesCategory_SelectionChanged;
            var queryWindow = DI.GetQueryWindow("CustomerInformationBySparepart");
            queryWindow.SelectionDecided += queryWindow_SelectionDecided;
            this.ptbCustomerCompany.PopupContent = queryWindow;
            radMaskedTextBox1.SelectionStart = 0;
            radMaskedTextBox1.Value = null;
        }

        void cbxPartsSalesCategory_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var partsRebateApplication = this.DataContext as PartsRebateApplication;
            if(partsRebateApplication == null)
                return;
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsRebateTypesQuery().Where(v => v.PartsSalesCategoryId == partsRebateApplication.PartsSalesCategoryId && v.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvPartsRebateTypes.Clear();
                foreach(var accountGroup in loadOp.Entities) {
                    this.KvPartsRebateTypes.Add(new KeyValuePair {
                        Key = accountGroup.Id,
                        Value = accountGroup.Name,
                        UserObject = accountGroup
                    });
                }
            }, null);
        }

        void queryWindow_SelectionDecided(object sender, System.EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var customerInformation = queryWindow.SelectedEntities.Cast<CustomerInformation>().FirstOrDefault();
            if(customerInformation == null)
                return;
            var partsRebateApplication = this.DataContext as PartsRebateApplication;
            if(partsRebateApplication == null)
                return;
            partsRebateApplication.CustomerCompanyId = customerInformation.CustomerCompany.Id;
            partsRebateApplication.CustomerCompanyCode = customerInformation.CustomerCompany.Code;
            partsRebateApplication.CustomerCompanyName = customerInformation.CustomerCompany.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void radMaskedTextBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e) {
            var partsRebateApplication = this.DataContext as PartsRebateApplication;
            if(partsRebateApplication == null || radMaskedTextBox1.Value == null)
                return;
            if((e.PlatformKeyCode == 8 || e.PlatformKeyCode == 46) && partsRebateApplication.Amount == default(decimal)) {
                radMaskedTextBox1.SelectionStart = 0;
                radMaskedTextBox1.Value = null;
            }
        }
    }
}
