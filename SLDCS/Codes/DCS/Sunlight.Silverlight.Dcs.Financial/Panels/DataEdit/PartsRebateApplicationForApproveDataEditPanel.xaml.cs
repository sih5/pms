﻿
namespace Sunlight.Silverlight.Dcs.Financial.Panels.DataEdit {
    public partial class PartsRebateApplicationForApproveDataEditPanel {
        public PartsRebateApplicationForApproveDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }

        private readonly string[] kvNames = {
            "PartsRebateDirection"
        };
    }
}
