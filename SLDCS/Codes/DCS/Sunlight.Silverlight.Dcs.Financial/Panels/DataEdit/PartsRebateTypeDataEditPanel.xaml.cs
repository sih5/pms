﻿
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.DataEdit {
    public partial class PartsRebateTypeDataEditPanel {
        public PartsRebateTypeDataEditPanel() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.Loaded += PartsRebateTypeDataEditPanel_Loaded;
        }

        private void PartsRebateTypeDataEditPanel_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(ex => ex.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && ex.Status != (int)DcsBaseDataStatus.作废), LoadBehavior.RefreshCurrent, loadOp => {
                if (loadOp.HasError)
                    return;
                this.KvPartsSalesCategory.Clear();
                foreach (var company in loadOp.Entities)
                {
                    this.KvPartsSalesCategory.Add(new KeyValuePair
                    {
                        Key = company.Id,
                        Value = company.Name,
                        UserObject = company
                    });
                }
                this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            }, null);
        }

        private ObservableCollection<KeyValuePair> kvPartsSalesCategory;
        private void CreateUI() {
            //var dcsDomainContext = new DcsDomainContext();
            //dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(ex => ex.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && ex.Status != (int)DcsBaseDataStatus.作废), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    this.KvPartsSalesCategory.Clear();
            //    foreach(var company in loadOp.Entities) {
            //        this.KvPartsSalesCategory.Add(new KeyValuePair {
            //            Key = company.Id,
            //            Value = company.Name,
            //            UserObject = company
            //        });
            //    }
            //}, null);
        }
        public ObservableCollection<KeyValuePair> KvPartsSalesCategory {
            get {
                return this.kvPartsSalesCategory ?? (this.kvPartsSalesCategory = new ObservableCollection<KeyValuePair>());
            }
        }

    }
}
