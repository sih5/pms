﻿
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using System.Linq;
using Sunlight.Silverlight.Dcs.Financial.Resources;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.DataEdit {
    public partial class PlannedPriceAppYXDataEditPanel {
        private ObservableCollection<KeyValuePair> kvPartsSalesCategorys;
        public ObservableCollection<KeyValuePair> KvPartsSalesCategorys {
            get {
                return this.kvPartsSalesCategorys ?? (this.kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>());
            }
        }
        private int partsSalesCategoryOld;
        public PlannedPriceAppYXDataEditPanel() {
            InitializeComponent();
            this.Loaded += PlannedPriceAppYXDataEditPanel_Loaded;
            this.cbxPartsSalesCategory.SelectionChanged += CbxPartsSalesCategory_SelectionChanged;
        }

        private void CbxPartsSalesCategory_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            var plannedPriceApp = this.DataContext as PlannedPriceApp;
            if(this.partsSalesCategoryOld != default(int) && plannedPriceApp.PartsSalesCategoryId != default(int) && plannedPriceApp.PlannedPriceAppDetails.Any()) {
                if(plannedPriceApp.PartsSalesCategoryId != this.partsSalesCategoryOld)
                    DcsUtils.Confirm(FinancialUIStrings.DataEditPanel_Confirm_PlannedPriceApp, () => {
                        foreach(var item in plannedPriceApp.PlannedPriceAppDetails) {
                            plannedPriceApp.PlannedPriceAppDetails.Remove(item);
                        }
                        this.partsSalesCategoryOld = plannedPriceApp.PartsSalesCategoryId;
                    }, () => {
                        plannedPriceApp.PartsSalesCategoryId = this.partsSalesCategoryOld;
                    });
            } else {
                this.partsSalesCategoryOld = plannedPriceApp.PartsSalesCategoryId;
            }
        }

        void PlannedPriceAppYXDataEditPanel_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效 && v.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvPartsSalesCategorys.Clear();
                foreach(var item in loadOp.Entities) {
                    this.KvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.Name,
                        UserObject = item
                    });
                }
            }, null);
        }
    }
}
