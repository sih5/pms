﻿
using System.ComponentModel;
using System.Windows;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.DataEdit {
    public partial class PsRebateAppForBatchApproveDataEditPanel : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;
        private string batchApprovalComment;

        public string BatchApprovalComment {
            get {
                return this.batchApprovalComment;
            }
            set {
                this.batchApprovalComment = value;
                this.OnPropertyChanged("BatchApprovalComment");
            }
        }

        public PsRebateAppForBatchApproveDataEditPanel() {
            InitializeComponent();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e) {
            var parent = this.ParentOfType<RadWindow>();
            if(parent != null) {
                parent.DialogResult = false;
                parent.Close();
            }
            this.BatchApprovalComment = string.Empty;
        }

        private void Submit_Click(object sender, RoutedEventArgs e) {
            var parent = this.ParentOfType<RadWindow>();
            if(parent != null) {
                parent.DialogResult = true;
                parent.Close();
            }
            this.BatchApprovalComment = string.Empty;
        }

        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

    }
}
