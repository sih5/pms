﻿
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Financial.Views.DataGrid;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Detail {
    public class PaymentBeneficiaryLisDetailPanel : PaymentBeneficiaryListForViewDataGridView, IDetailPanel {

        public System.Uri Icon {
            get {
                return null;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
        }

        public string Title {
            get {
                return Utils.GetEntityLocalizedName(typeof(PaymentBeneficiaryList), FinancialUIStrings.DetailPanel_Title_PaymentBeneficiaryList);
            }
        }
    }
}
