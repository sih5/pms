﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query {
    public class AccountDetailQueryQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvAccountGroups = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = {
            "AccountPayment_BusinessType"
        };

        public AccountDetailQueryQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(kvNames);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetCustomerAccountsQuery().Where(r => r.CustomerCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var accountGroup = loadOp.Entities.Select(r => r.AccountGroupId).ToArray();
                dcsDomainContext.Load(dcsDomainContext.GetAccountGroupsByAccountGroupIdQuery(accountGroup), LoadBehavior.RefreshCurrent, loadOp1 => {
                    if(loadOp1.HasError)
                        return;
                    foreach(var accountgroup in loadOp1.Entities)
                        this.kvAccountGroups.Add(new KeyValuePair {
                            Key = accountgroup.Id,
                            Value = accountgroup.Name
                        });
                }, null);
            }, null);
            this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = FinancialUIStrings.QueryPanel_Title_CustomerAccountHisDetail,
                        EntityType = typeof(CustomerAccountHisDetail),
                        QueryItems = new[] {
                            new QueryItem {
                                ColumnName = "SourceCode",
                                Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccountHisDetail_SourceCode
                            },
                            new KeyValuesQueryItem {
                                ColumnName = "BusinessType",
                                KeyValueItems = this.KeyValueManager[kvNames[0]]
                            },
                            new DateTimeRangeQueryItem {
                                ColumnName = "Process",
                                Title = FinancialUIStrings.QueryPanel_QueryItem_Title_ProcessDate,
                                DefaultValue = new[] {
                                    new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                                }
                            },
                            new KeyValuesQueryItem {
                                ColumnName = "CustomerAccount.AccountGroupId",
                                Title = FinancialUIStrings.QueryPanel_QueryItem_Title_AccountGroup,
                                KeyValueItems = this.kvAccountGroups,
                            }
                        }
                    }
                };
        }
    }
}
