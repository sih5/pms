﻿
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query {
    public class AccountGroupQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
           "MasterData_Status"
        };

        public AccountGroupQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = FinancialUIStrings.QueryPanel_Title_AccountGroup,
                    EntityType = typeof(AccountGroup),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code"
                        },new QueryItem {
                            ColumnName = "Name"
                        },new QueryItem {
                            ColumnName = "SalesCompanyName",
                            Title=FinancialUIStrings.QueryPanel_QueryItem_Title__Company_Name
                        },new DateTimeRangeQueryItem {
                           ColumnName = "CreateTime"
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsMasterDataStatus.有效
                        }
                    }
                }
            };
        }

    }
}
