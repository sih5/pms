﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query {
    public class AccountPayableHistoryDetailQueryPanel : DcsQueryPanelBase {
        //private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = new[] {
            "AccountPayOut_BusinessType"
        };

        public AccountPayableHistoryDetailQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            //var dcsDomainContext = new DcsDomainContext();
            //dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    foreach(var partsSalesCategory in loadOp.Entities)
            //        this.kvPartsSalesCategoryName.Add(new KeyValuePair {
            //            Key = partsSalesCategory.Id,
            //            Value = partsSalesCategory.Name
            //        });
            //}, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "common",
                    Title = FinancialUIStrings.QueryPanel_Title_AccountPayableHistoryDetail,
                    EntityType = typeof(AccountPayableHistoryDetail),
                    QueryItems = new[] {
                        new CustomQueryItem {
                            ColumnName = "SupplierAccount.Company.Name",
                            DataType = typeof(string),
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_SupplierCompany_Name
                        },
                        new CustomQueryItem {
                            ColumnName = "SupplierAccount.Company.Code",
                            DataType = typeof(string),
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_SupplierCompany_Code
                        },
                        new QueryItem {
                            ColumnName = "SourceCode",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_AccountPayableHistoryDetail_Code
                        },
                        new KeyValuesQueryItem {
                            ColumnName = "BusinessType",
                            Title = FinancialUIStrings.DataGridView_ColumnItem_Title_BusinessType,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        },
                        new DateTimeRangeQueryItem {
                            ColumnName = "ProcessDate",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_ProcessDate,
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now
                            }
                        }
                        //,
                        //new KeyValuesQueryItem {
                        //    ColumnName = "SupplierAccount.PartsSalesCategoryId",
                        //    Title = FinancialUIStrings.QueryPanel_QueryItem_Title_Common_PartsSalesCategory,
                        //    KeyValueItems = this.kvPartsSalesCategoryName,
                        //}
                    }
                }
            };
        }

    }
}
