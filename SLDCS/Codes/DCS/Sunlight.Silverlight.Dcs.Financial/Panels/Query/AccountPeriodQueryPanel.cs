﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query
{

    public class AccountPeriodQueryPanel : DcsQueryPanelBase
    {
        private readonly string[] kvNames = {
            "AccountPeriodYear", "AccountPeriodMonth","AccountPeriodStatus"
        };
        public AccountPeriodQueryPanel()
        {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize()
        {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                  UniqueId = "Common",
                    Title = FinancialUIStrings.QueryPanel_Title_AccountPeriod,
                    EntityType = typeof(AccountPeriod),
                    QueryItems = new QueryItem[] {
                         new KeyValuesQueryItem {
                            Title =FinancialUIStrings.QueryPanel_QueryItem_Title_AccountPeriod_Year,
                            ColumnName = "Year",
                              KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        },new KeyValuesQueryItem {
                            ColumnName = "Month",
                            Title =FinancialUIStrings.QueryPanel_QueryItem_Title_AccountPeriod_Month,
                              KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        },new KeyValuesQueryItem {
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_Status,
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                              DefaultValue = new[] {
                                DateTime.Now.AddDays(1 - DateTime.Now.Day).Date, DateTime.Now.Date
                                }
                        }
                    }
                }
            };
        }
    }
}
