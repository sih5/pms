﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query {
    public class AgencyCustomerAccountQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "AccountPayment_BusinessType"
        };

        public AgencyCustomerAccountQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(kvNames);
        }
        private void Initialize() {
                this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = FinancialUIStrings.QueryPanel_Title_AgencyCustomerAccount,
                        EntityType = typeof(VirtualAgencyCustomerAccount),
                        QueryItems = new QueryItem[] {
                            new QueryItem {
                                ColumnName = "DealerCode",
                                Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccount_DealerCode,
                                IsExact = false
                            }, new QueryItem {
                                ColumnName = "DealerName",
                                Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccount_DealerName,
                                IsExact = false
                            }, new QueryItem {
                                ColumnName = "AgencyCode",
                                Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccount_AgencyCode,
                                IsExact = false
                            }, new QueryItem {
                                ColumnName = "AgencyName",
                                Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccount_AgencyName,
                                IsExact = false
                            }, new DateTimeRangeQueryItem {
                                ColumnName = "ProcessDate",
                                Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccount_ProcessDate,
                                DefaultValue = new[] {
                                    new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now
                                }
                            }, new KeyValuesQueryItem {
                                ColumnName = "BusinessType",
                                KeyValueItems = this.KeyValueManager[kvNames[0]],
                                Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccount_BusinessType,
                            }
                         }
                }
            };
        }
    }
}

