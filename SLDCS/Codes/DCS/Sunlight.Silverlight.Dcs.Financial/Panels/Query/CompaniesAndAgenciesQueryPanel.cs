﻿
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query {
    public class CompaniesAndAgenciesQueryPanel : DcsQueryPanelBase {


        public CompaniesAndAgenciesQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = FinancialUIStrings.QueryPanel_Title_AccountGroup,
                    EntityType = typeof(Company),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_Company_SalesCompanyCode
                        },new QueryItem {
                            ColumnName = "Name",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_Company_SalesCompanyName
                        }
                    }
                }
            };
        }

    }
}
