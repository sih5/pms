﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query {
    public class CompaniesAndDealerQueryPanel : DcsQueryPanelBase {
        public CompaniesAndDealerQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = "服务站查询",
                    EntityType = typeof(Company),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title = FinancialUIStrings.DataGridView_ColumnItem_Title_AgencyCustomerAccount_DealerCode
                        },new QueryItem {
                            ColumnName = "Name",
                            Title = FinancialUIStrings.DataGridView_ColumnItem_Title_AgencyCustomerAccount_DealerName
                        }
                    }
                }
            };
        }

    }
}
