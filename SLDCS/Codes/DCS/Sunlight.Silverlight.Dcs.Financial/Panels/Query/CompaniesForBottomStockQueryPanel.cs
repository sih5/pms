﻿
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System.Collections.ObjectModel;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query {
    public class CompaniesForBottomStockQueryPanel : DcsQueryPanelBase {


        private readonly string[] kvNames = new[] {
           "Company_Type"
        };
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();
        public CompaniesForBottomStockQueryPanel() {

            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = FinancialUIStrings.QueryPanel_Title_CompaniesForBottomStock,
                    EntityType = typeof(Company),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_Companies_Code
                        },new QueryItem {
                            ColumnName = "Name",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title__Company_Name
                        }, new KeyValuesQueryItem {
                            ColumnName = "Type",
                            IsExact = false,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_Companies_Type
                        }
                    }
                }
            };
        }

    }
}
