﻿
using System.Collections.ObjectModel;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query {
    public class CredenceApplicationQueryPanel : DcsQueryPanelBase {
      //  private readonly ObservableCollection<KeyValuePair> kvTypes = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = new[] {
           "CredenceApplication_Status","Credit_Type"
        };

        public CredenceApplicationQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
            //this.KeyValueManager.LoadData(() => {
            //    this.kvTypes.Clear();
            //    foreach(var kvPair in this.KeyValueManager[kvNames[0]].Where(e => e.Key == (int)DcsCredenceApplicationStatus.新增 || e.Key == (int)DcsCredenceApplicationStatus.有效 || e.Key == (int)DcsCredenceApplicationStatus.作废 || e.Key == (int)DcsCredenceApplicationStatus.失效 || e.Key == (int)DcsCredenceApplicationStatus.初审通过 || e.Key == (int)DcsCredenceApplicationStatus.已提交))
            //    {
            //        kvTypes.Add(kvPair);
            //    }
            //});
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = FinancialUIStrings.QueryPanel_Title_CredenceApplication,
                    EntityType = typeof(VirtualCredenceApplication),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                            ColumnName = "CompanyCode",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Code,
                            DataType=typeof(string)
                        },new CustomQueryItem {
                            ColumnName = "CompanyName",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Name,
                            DataType=typeof(string)
                        },new DateTimeRangeQueryItem {
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CredenceApplication_ApproveTime,
                            ColumnName = "ApproveTime"                          
                        },new QueryItem {
                            ColumnName = "BusinessCode",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CredenceApplication_BusinessCode
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                             KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_Status,
                            DefaultValue = (int)DcsCredenceApplicationStatus.有效
                        },new KeyValuesQueryItem {
                            ColumnName = "CreditType",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CredenceApplication_CreditType,
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"                          
                        }
                    }
                }
            };
        }
    }
}
