﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query {
    public class CustomerAccountBalanceQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvAccountGroups = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = { "Company_Type" };

        public CustomerAccountBalanceQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(kvNames);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetAccountGroupsQuery().Where(e => e.SalesCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var accountgroup in loadOp.Entities)
                    this.kvAccountGroups.Add(new KeyValuePair {
                        Key = accountgroup.Id,
                        Value = accountgroup.Name
                    });
                this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = FinancialUIStrings.QueryPanel_Title_CustomerAccountHistory,
                        EntityType = typeof(VirtualCustomerAccountHistory),
                        QueryItems = new [] {
                            new QueryItem {
                                ColumnName = "Name",
                                Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Name,
                                IsExact = false
                            }, new QueryItem {
                                ColumnName = "Code",
                                Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Code,
                                IsExact = false
                            },
                        }
                    }
                };
            }, null);
        }
    }
}
