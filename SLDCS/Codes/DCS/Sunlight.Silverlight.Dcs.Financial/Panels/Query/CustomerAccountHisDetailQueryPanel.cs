﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query {
    public class CustomerAccountHisDetailQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvAccountGroups = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
            "AccountPayment_BusinessType","Serial_Type"
        };
        public CustomerAccountHisDetailQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(kvNames);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetAccountGroupsQuery().Where(e => e.SalesCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var accountgroup in loadOp.Entities)
                    this.kvAccountGroups.Add(new KeyValuePair {
                        Key = accountgroup.Id,
                        Value = accountgroup.Name
                    });
                this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = FinancialUIStrings.QueryPanel_Title_CustomerAccountHisDetail,
                    EntityType = typeof(VirtualCustomerAccountHisDetails),
                    QueryItems = new[] {
                        new CustomQueryItem {
                            ColumnName = "CompanyName",
                            DataType = typeof(string),
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Name
                        },new CustomQueryItem {
                            ColumnName = "CompanyCode",
                            DataType = typeof(string),
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Code
                        },new QueryItem {
                            ColumnName = "SourceCode",
                            Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccountHisDetail_SourceCode
                        },new CustomQueryItem {
                            ColumnName = "BusinessCode",
                            DataType = typeof(string),
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CredenceApplication_BusinessCode
                        }
                        ,new KeyValuesQueryItem {
                            ColumnName = "BusinessType",
                            KeyValueItems = this.KeyValueManager[kvNames[0]],
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccount_BusinessType
                        }, new KeyValuesQueryItem {
                                ColumnName = "AccountGroupId",
                                Title = FinancialUIStrings.QueryPanel_QueryItem_Title_AccountGroup,
                                KeyValueItems = this.kvAccountGroups,
                            },
                        new DateTimeRangeQueryItem {
                            ColumnName = "Process",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_ProcessDate,
                           DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, 1, 1), DateTime.Now.Date
                            }
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "InvoiceDate",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccountHisDetail_InvoiceDate
                        }  ,new KeyValuesQueryItem {
                            ColumnName = "SerialType",
                            KeyValueItems = this.KeyValueManager[kvNames[1]],
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccountHisDetail_SerialType
                        }
                    }
                }
            };
            }, null);
        }
    }
}
