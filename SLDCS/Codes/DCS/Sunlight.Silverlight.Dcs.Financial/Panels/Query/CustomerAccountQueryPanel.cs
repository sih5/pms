﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Web.Entities;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query {
    public class CustomerAccountQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvAccountGroups = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = {
            "MasterData_Status","Company_Type"
        };

        public CustomerAccountQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(kvNames);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetAccountGroupsQuery().Where(e => e.SalesCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var accountgroup in loadOp.Entities)
                    this.kvAccountGroups.Add(new KeyValuePair {
                        Key = accountgroup.Id,
                        Value = accountgroup.Name
                    });
                this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = FinancialUIStrings.QueryPanel_Title_CustomerAccount,
                        EntityType = typeof(VirtualCustomerAccount),
                        QueryItems = new QueryItem[] {
                            new QueryItem {
                                ColumnName = "CompanyName",
                                Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Name,
                                IsExact = false
                            }, new QueryItem {
                                ColumnName = "CompanyCode",
                                Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Code,
                                IsExact = false
                            }, 
                            new KeyValuesQueryItem {
                                ColumnName = "AccountGroupId",
                                Title = FinancialUIStrings.QueryPanel_QueryItem_Title_AccountGroup,
                                KeyValueItems = this.kvAccountGroups,
                            }, new QueryItem {
                                ColumnName = "BusinessCode",
                                Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CredenceApplication_BusinessCode
                            }
                            , new KeyValuesQueryItem {
                                ColumnName = "CompanyType",
                                Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccountHistory_Type,
                                KeyValueItems = this.KeyValueManager[kvNames[1]]
                            }
                            ,new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                //DefaultValue = new[] {
                                //    new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now
                                //}
                            }, new KeyValuesQueryItem {
                                ColumnName = "Status",
                                KeyValueItems = this.KeyValueManager[kvNames[0]],
                                DefaultValue = (int)DcsMasterDataStatus.有效
                            },new CustomQueryItem {
                                ColumnName="IsZeroForAccountBalance",
                                DataType=typeof(bool),
                                Title= FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccount_IsZeroForAccountBalance
                            }
                        }
                    }
                };
            }, null);

        }
    }
}
