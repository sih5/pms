﻿
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query {
    public class CustomerInformationBySparepartForTransferQueryPanel : DcsQueryPanelBase {

        public CustomerInformationBySparepartForTransferQueryPanel() {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private readonly string[] kvNames = new string[]{
            "Company_Type"
        };

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = FinancialUIStrings.QueryPanel_Title_CustomerInformation,
                    EntityType = typeof(VirtualCustomerInformationForTransfer),
                    QueryItems = new QueryItem[]  {
                        new CustomQueryItem {
                            ColumnName = "CompanyCode",
                            DataType = typeof(string),
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Code
                        },new CustomQueryItem {
                            ColumnName = "CompanyName",
                            DataType = typeof(string),
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Name
                        }, new KeyValuesQueryItem {
                            ColumnName = "CompanyType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            IsEnabled = true,
                            Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Type
                        }
                    }
                }
            };
        }
    }
}
