﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query {
    public class CustomerOpenAccountAppQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "WorkflowOfSimpleApproval_Status"
        };

        public CustomerOpenAccountAppQueryPanel() {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = FinancialUIStrings.QueryPanel_Title_CustomerOpenAccountApp,
                    EntityType = typeof(VirtualCustomerOpenAccountApp),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                            ColumnName = "CompanyName",
                            DataType = typeof(string),
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerOpenAccountApp_Name,
                            IsExact = false
                        }, new CustomQueryItem {
                            ColumnName = "CompanyCode",
                            DataType = typeof(string),
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerOpenAccountApp_Code,
                            IsExact = false
                        },  new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        }, new CustomQueryItem {
                            ColumnName = "BusinessCode",
                            DataType = typeof(string),
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CredenceApplication_BusinessCode,
                            IsExact = false
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_Status,
                            DefaultValue = (int)DcsWorkflowOfSimpleApprovalStatus.新建
                        }
                    }
                }
            };
        }
    }
}
