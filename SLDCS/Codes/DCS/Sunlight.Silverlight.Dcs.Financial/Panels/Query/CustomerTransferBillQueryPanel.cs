﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query {
    public class CustomerTransferBillQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
           "WorkflowOfSimpleApproval_Status","CustomerTransferBill_BusinessType"
        };

        public CustomerTransferBillQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = FinancialUIStrings.QueryPanel_Title_CustomerTransferBill,
                    EntityType = typeof(Branch),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_OutboundCustomerCompany_Name,
                            ColumnName = "OutboundCustomerCompany.Name",
                            DataType = typeof(string)
                        },new CustomQueryItem {
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_OutboundCustomerCompany_Code,
                            ColumnName = "OutboundCustomerCompany.Code",
                            DataType = typeof(string)
                        },new CustomQueryItem {
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_InboundCustomerCompany_Name,
                            ColumnName = "InboundCustomerCompany.Name",
                            DataType = typeof(string)
                        },new CustomQueryItem {
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_InboundCustomerCompany_Code,
                            ColumnName = "InboundCustomerCompany.Code",
                            DataType = typeof(string)
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                             DefaultValue = new[] {
                                    new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now
                               }
                        }, new DateTimeRangeQueryItem {
                            Title= FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccountHisDetail_InvoiceDate,
                            ColumnName = "InvoiceDate"
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            IsExact = false,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsMasterDataStatus.有效
                        },new KeyValuesQueryItem {
                            ColumnName = "BusinessType",
                            IsExact = false,
                            Title= FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccount_BusinessType,
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        },new QueryItem{
                            ColumnName = "Code",
                            Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerOpenAccountApp_Code
                        }
                    }
                }
            };
        }
    }
}
