﻿
using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query {
    public class FinancialSnapshotSetQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
           "Snapsho_Status"
        };

        public FinancialSnapshotSetQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = FinancialUIStrings.QueryPanel_Title_FinancialSnapshotSet,
                    EntityType = typeof(FinancialSnapshotSet),
                    QueryItems = new [] {
                        new QueryItem {
                            ColumnName = "Code"
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsCredenceApplicationStatus.有效
                        },new DateTimeRangeQueryItem {
                            ColumnName = "SnapshotTime",
                             DefaultValue = new[] {
                                    new DateTime(DateTime.Now.Year, 1, 1),   new DateTime(DateTime.Now.Year, 12, 31)
                                },
                          IsLessThanOrEqualToMaxValue = true
                        }
                    }
                }
            };
        }
    }
}
