﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query {
    public class FinancialSnapshotSetsQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = {
            "Company_Type"
        };

        public FinancialSnapshotSetsQueryPanel() {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetWarehousesOrderByNameQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehouse in loadOp.Entities)
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategory in loadOp.Entities)
                    this.kvPartsSalesCategoryName.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = FinancialUIStrings.Action_Title_HistoricalDataBase,
                    EntityType = typeof(PartsHistoryStock),
                    QueryItems = new QueryItem[] {
                        new KeyValuesQueryItem {
                            ColumnName = "WarehouseId",
                            KeyValueItems = this.kvWarehouses,
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_Warehouse_Name
                        },
                        new CustomQueryItem {
                            ColumnName = "SparePart.Name",
                            DataType = typeof(string),
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_SparePart_Name,
                        },
                        new CustomQueryItem {
                            ColumnName = "SnapshoTime",
                            DataType = typeof(DateTime),
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccountHistory_TheDate
                        },
                        new CustomQueryItem {
                            ColumnName = "SparePart.Code",
                            DataType = typeof(string),
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                        },
                        new KeyValuesQueryItem {
                            ColumnName = "PartsSalesCategoryId",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_Common_PartsSalesCategory,
                            KeyValueItems = this.kvPartsSalesCategoryName
                        },
                        new KeyValuesQueryItem {
                            ColumnName = "StorageCompanyType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_PartsHistoryStock_StorageCompanyType
                        }, new CustomQueryItem {
                            ColumnName = "GreaterThanZero",
                            DataType = typeof(bool),
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_PartsHistoryStock_GreaterThanZero,
                            DefaultValue = true
                        },
                    }
                }
            };
        }
    }
}
