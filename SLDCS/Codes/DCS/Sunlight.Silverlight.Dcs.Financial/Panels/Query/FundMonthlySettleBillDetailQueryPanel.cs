﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query
{

    public class FundMonthlySettleBillDetailQueryPanel : DcsQueryPanelBase
    {

        //private readonly string[] kvNames = {
        //    "AccountPeriodYear", "AccountPeriodMonth","BaseData_Status"
        //};
        
        public FundMonthlySettleBillDetailQueryPanel()
        {
            this.Initializer.Register(this.Initialize);
            //this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize()
        {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                  UniqueId = "Common",
                    Title = FinancialUIStrings.QueryPanel_Title_FundMonthlySettleBillDetail,
                    EntityType = typeof(FundMonthlySettleBillDetail),
                    QueryItems = new QueryItem[] {
                         new QueryItem {
                            ColumnName = "CustomerCompanyCode",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Code
                        },new QueryItem {
                            ColumnName = "CustomerCompanyName",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Name
                        }
                    }
                }
            };
        }
    }
}
