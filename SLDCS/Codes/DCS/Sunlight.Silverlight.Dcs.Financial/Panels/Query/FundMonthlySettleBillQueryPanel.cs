﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query
{

    public class FundMonthlySettleBillQueryPanel : DcsQueryPanelBase
    {
        private readonly string[] kvNames = {
            "AccountPeriodYear", "AccountPeriodMonth","BaseData_Status"
        };
        public FundMonthlySettleBillQueryPanel()
        {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize()
        {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                  UniqueId = "Common",
                    Title = FinancialUIStrings.QueryPanel_Title_FundMonthlySettleBillDetail,
                    EntityType = typeof(AccountPeriod),
                    QueryItems = new QueryItem[] {
                         new KeyValuesQueryItem {
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_AccountPeriod_MonthlySettleYear,
                            ColumnName = "MonthlySettleYear",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            
                        },new KeyValuesQueryItem {
                            ColumnName = "MonthlySettleMonth",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_AccountPeriod_MonthlySettleMonth,
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        },new KeyValuesQueryItem {
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_Status,
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[2]],
                            DefaultValue =(int) DcsBaseDataStatus.有效
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                            DateTime.Now.AddDays(1 - DateTime.Now.Day).Date, DateTime.Now.Date
                            }
                        }
                    }
                }
            };
        }
    }
}
