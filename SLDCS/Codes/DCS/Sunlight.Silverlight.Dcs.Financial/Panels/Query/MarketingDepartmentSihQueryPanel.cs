using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query {
    public class MarketingDepartmentSihQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = {
            "MarketingDepartment_Type", "BaseData_Status"
        };

        public MarketingDepartmentSihQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var branch in loadOp.Entities)
                    this.kvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name
                    });
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var PartsSalesCategory in loadOp.Entities)
                    this.kvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = PartsSalesCategory.Id,
                        Value = PartsSalesCategory.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = "分销中心查询",
                    EntityType = typeof(MarketingDepartment),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title = "分销中心编号"
                        }, new QueryItem {
                            ColumnName = "Name",
                             Title ="分销中心名称"
                        }, new QueryItem {
                            ColumnName = "ProvinceName",
                            Title = "省"
                        }, new QueryItem {
                            ColumnName = "CityName",
                            Title = "市"
                        }, new QueryItem {
                            ColumnName = "CountyName",
                            Title ="县"
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            DefaultValue = (int)DcsBaseDataStatus.有效
                        }, new KeyValuesQueryItem{
                            Title = "品牌",
                            ColumnName="PartsSalesCategoryId",
                            KeyValueItems = this.kvPartsSalesCategorys
                        }
                    }
                }
            };
        }
    }
}
