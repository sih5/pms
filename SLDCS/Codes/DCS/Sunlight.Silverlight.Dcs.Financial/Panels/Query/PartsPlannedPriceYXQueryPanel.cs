﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Core;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query {
    public class PartsPlannedPriceYXQueryPanel : DcsQueryPanelBase {

        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();

        public PartsPlannedPriceYXQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategory in loadOp.Entities)
                    this.kvPartsSalesCategoryName.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = FinancialUIStrings.QueryPanel_Title_PartsPlannedPrice,
                    EntityType = typeof(GetAllPartsPlannedPrice),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                            Title = Utils.GetEntityLocalizedName(typeof(SparePart), "Name"),
                            ColumnName = "Name",
                            IsExact = false,
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                            Title = Utils.GetEntityLocalizedName(typeof(SparePart), "Code"),
                            ColumnName = "Code",
                            IsExact = false,
                            DataType = typeof(string)
                        }, new KeyValuesQueryItem {
                                ColumnName = "PartsSalesCategory",
                                Title = FinancialUIStrings.QueryPanel_QueryItem_Title_Common_PartsSalesCategory,
                                KeyValueItems = this.kvPartsSalesCategoryName
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        }
                    }
                }
            };
        }
    }
}
