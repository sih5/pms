﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query {
    public class PartsRebateAccountForSelectQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvAccountGroups = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
             "PartsRebateAccount_SourceType"
        };
        public PartsRebateAccountForSelectQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var branch in loadOp.Entities)
                    this.kvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name
                    });
            }, null);

            dcsDomainContext.Load(dcsDomainContext.GetAccountGroupsQuery().Where(e => e.SalesCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var accountgroup in loadOp.Entities)
                    this.kvAccountGroups.Add(new KeyValuePair {
                        Key = accountgroup.Id,
                        Value = accountgroup.Name
                    });
            }, null);

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = FinancialUIStrings.QueryPanel_Title_PartsRebateAccount,
                    EntityType = typeof(VirtualPartsRebateAccount),
                    QueryItems = new QueryItem[] {
                            new KeyValuesQueryItem {
                            ColumnName = "BranchId",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_Common_Branch,
                            KeyValueItems=this.kvBranches,
                            IsEnabled = false,
                            DefaultValue = BaseApp.Current.CurrentUserData.EnterpriseId
                         }, new KeyValuesQueryItem {
                                ColumnName = "AccountGroupId",
                                Title = FinancialUIStrings.QueryPanel_QueryItem_Title_AccountGroup,
                                KeyValueItems = this.kvAccountGroups,
                        },new CustomQueryItem {
                            ColumnName = "CompanyCode",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Code,
                            DataType=typeof(string)
                        },new CustomQueryItem {
                            ColumnName = "CompanyName",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Name,
                            DataType=typeof(string)
                        },new QueryItem {
                            ColumnName = "BusinessCode",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CredenceApplication_BusinessCode
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        }
                    }
                }
            };
        }
    }
}
