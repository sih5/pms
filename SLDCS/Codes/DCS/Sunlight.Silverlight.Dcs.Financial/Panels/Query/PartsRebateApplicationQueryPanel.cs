﻿
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query {
    public class PartsRebateApplicationQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
           "WorkflowOfSimpleApproval_Status"
        };
        //private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategory = new ObservableCollection<KeyValuePair>();

        public PartsRebateApplicationQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            //var dcsDomainContext = new DcsDomainContext();
            //dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    foreach(var accountgroup in loadOp.Entities)
            //        this.kvPartsSalesCategory.Add(new KeyValuePair {
            //            Key = accountgroup.Id,
            //            Value = accountgroup.Name
            //        });
            //    }, null);
                this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = FinancialUIStrings.QueryPanel_Title_PartsRebateApplication,
                        EntityType = typeof(VirtualPartsRebateApplication),
                        QueryItems = new [] {
                            // new KeyValuesQueryItem {
                            //    ColumnName = "PartsSalesCategoryId",
                            //    Title = "品牌",
                            //    KeyValueItems = this.kvPartsSalesCategory,
                            //},
                            new QueryItem {
                                ColumnName = "Code",
                                Title= FinancialUIStrings.QueryPanel_QueryItem_Title_PartsRebateApplication_Code
                            }, new QueryItem {
                                ColumnName = "CustomerCompanyCode",
                                Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Code
                            }, new QueryItem {
                                ColumnName = "CustomerCompanyName",
                                Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Name
                            }, new CustomQueryItem {
                                DataType = typeof(string),
                                ColumnName = "AccountGroupName",
                                Title = FinancialUIStrings.QueryPanel_QueryItem_Title_AccountGroupName
                            },new QueryItem {
                                ColumnName = "BusinessCode",
                                Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CredenceApplication_BusinessCode
                            }, new CustomQueryItem {
                                DataType = typeof(string),
                                ColumnName = "PartsRebateTypeName",
                                Title = FinancialUIStrings.QueryPanel_QueryItem_Title_PartsRebateTypeName
                            }, new KeyValuesQueryItem {
                                ColumnName = "Status",
                                KeyValueItems = this.KeyValueManager[kvNames[0]],
                                DefaultValue = (int)DcsWorkflowOfSimpleApprovalStatus.新建,
                                Title = FinancialUIStrings.QueryPanel_QueryItem_Title_Status
                            },new QueryItem {
                                ColumnName = "CreatorName"
                            }, new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime"
                            }
                        }
                    }
                };
            
        }
    }
}
