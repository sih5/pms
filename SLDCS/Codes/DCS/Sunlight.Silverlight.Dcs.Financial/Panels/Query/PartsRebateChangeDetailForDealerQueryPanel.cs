﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query {
    public class PartsRebateChangeDetailForDealerQueryPanel : DcsQueryPanelBase {
        private readonly PartsRebateChangeDetailForDealerViewModel viewModel = new PartsRebateChangeDetailForDealerViewModel();

        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = new[] {
           "PartsRebateChangeDetail_SourceType"
        };

        public PartsRebateChangeDetailForDealerQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
            this.Initializer.Register(this.viewModel.Initialize);
        }
        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var branch in loadOp.Entities)
                    this.kvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = FinancialUIStrings.QueryPanel_Title_PartsRebateChangeDetail,
                    EntityType = typeof(PartsRebateChangeDetail),
                    QueryItems = new [] {
                            new ComboQueryItem {
                            ColumnName = "BranchId",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_Common_BranchName,
                            SelectedValuePath="Id",
                            DisplayMemberPath="Name",
                            ItemsSource=this.viewModel.Branchs,
                            SelectedItemBinding = new Binding("SelectedBranch") {
                                    Source = this.viewModel,
                                    Mode = BindingMode.TwoWay
                                }
                        }, new ComboQueryItem {
                            ColumnName = "AccountGroupId",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_AccountGroup,
                             SelectedValuePath="Id",
                            DisplayMemberPath="Name",
                            ItemsSource=this.viewModel.AccountGroups
                        },new CustomQueryItem {
                            ColumnName = "CompanyCode",
                            IsEnabled=false,
                            DefaultValue=BaseApp.Current.CurrentUserData.EnterpriseCode,
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Code,
                            DataType=typeof(string)
                        },new CustomQueryItem {
                            ColumnName = "CompanyName",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Name,
                            DataType=typeof(string)
                        },new QueryItem{
                            ColumnName ="SourceCode"
                        },new KeyValuesQueryItem {
                            ColumnName = "SourceType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                             DefaultValue = new[] {
                                    new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now
                               }
                        }
                    }
                }
            };
        }
    }

    public class PartsRebateChangeDetailForDealerViewModel : ViewModelBase {
        private ObservableCollection<Branch> branchs;
        public ObservableCollection<Branch> Branchs {
            get {
                return this.branchs ?? (this.branchs = new ObservableCollection<Branch>());
            }
        }

        private ObservableCollection<AccountGroup> allAccountGroups;
        public ObservableCollection<AccountGroup> AllAccountGroups {
            get {
                return this.allAccountGroups ?? (this.allAccountGroups = new ObservableCollection<AccountGroup>());
            }
        }

        //根据所选择的分公司过滤账户组
        private Branch selectedBranch;
        public Branch SelectedBranch {
            get {
                return this.selectedBranch;
            }
            set {
                if(this.selectedBranch == value)
                    return;
                this.selectedBranch = value;
                this.NotifyOfPropertyChange("Branch");
                this.AccountGroups.Refresh();
            }
        }
        private PagedCollectionView accountGroups;
        public PagedCollectionView AccountGroups {
            get {
                if(this.accountGroups == null) {
                    this.accountGroups = new PagedCollectionView(this.AllAccountGroups);
                    this.accountGroups.Filter = o => ((AccountGroup)o).SalesCompanyId == (this.SelectedBranch != null ? this.SelectedBranch.Id : int.MinValue);
                }
                return this.accountGroups;
            }
        }


        public override void Validate() {
            //
        }

        public void Initialize() {
            var domainContext = new DcsDomainContext();
            //查询所有的分公司
            domainContext.Load(domainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.Branchs.Clear();
                foreach(var branch in loadOp.Entities)
                    this.Branchs.Add(branch);
            }, null);
            //查询所有的账户组
            domainContext.Load(domainContext.GetAccountGroupsQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp1 => {
                if(loadOp1.HasError)
                    return;
                foreach(var entity in loadOp1.Entities) {
                    this.allAccountGroups.Add(entity);
                }
            }, null);

        }
    }

}
