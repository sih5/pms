﻿
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query {
    public class PartsRebateTypeQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
           "BaseData_Status"
        };
        public PartsRebateTypeQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        //private ObservableCollection<KeyValuePair> kvPartsSalesCategories = new ObservableCollection<KeyValuePair>();

        private void Initialize() {
            //var domainContext = new DcsDomainContext();
            //domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(r => r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError) {
            //        loadOp.MarkErrorAsHandled();
            //        return;
            //    }
            //    if(loadOp.Entities != null) {
            //        this.kvPartsSalesCategories.Clear();
            //        foreach(var partsSalesCatefory in loadOp.Entities) {
            //            this.kvPartsSalesCategories.Add(new KeyValuePair {
            //                Key = partsSalesCatefory.Id,
            //                Value = partsSalesCatefory.Name
            //            });
            //        }
            //    }
            //}, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = FinancialUIStrings.QueryPanel_Title_PartsRebateType,
                    EntityType = typeof(PartsRebateType),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code"
                        },new QueryItem {
                            ColumnName = "Name"
                        }
                        //,new KeyValuesQueryItem {
                        //   ColumnName = "PartsSalesCategoryId",
                        //   Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategoryCode,
                        //   KeyValueItems = this.kvPartsSalesCategories
                        //}
                        ,new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsBaseDataStatus.有效
                        },new DateTimeRangeQueryItem {
                           ColumnName = "CreateTime"
                        }
                    }
                }
            };
        }

    }
}
