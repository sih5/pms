﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query {
    public class PayOutBillQueryPanel : DcsQueryPanelBase {
        //private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = new[]{
            "WorkflowOfSimpleApproval_Status","PayOutBill_PaymentMethod"
         };
        public PayOutBillQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(kvNames);
        }
        private void Initialize() {
            //var dcsDomainContext = new DcsDomainContext();
            //dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    foreach(var partsSalesCategory in loadOp.Entities)
            //        this.kvPartsSalesCategoryName.Add(new KeyValuePair {
            //            Key = partsSalesCategory.Id,
            //            Value = partsSalesCategory.Name
            //        });
            //}, null);
            this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = FinancialUIStrings.QueryPanel_Title_SupplierAccount,
                        EntityType = typeof(SupplierAccount),
            QueryItems = new QueryItem[] { new CustomQueryItem {
                                ColumnName = "Company.Name",
                                DataType = typeof(string),
                                Title = FinancialUIStrings.QueryPanel_QueryItem_Title_SupplierCompany_Name,
                                IsExact = false
                            }, new CustomQueryItem {
                                ColumnName = "Company.Code",
                                DataType = typeof(string),
                                Title = FinancialUIStrings.QueryPanel_QueryItem_Title_SupplierCompany_Code,
                                IsExact = false
                            },new KeyValuesQueryItem {
                                ColumnName = "Status",
                                KeyValueItems = this.KeyValueManager[kvNames[0]],
                                DefaultValue = (int)DcsMasterDataStatus.有效
                            }
                            //, new KeyValuesQueryItem {
                            //    ColumnName = "PartsSalesCategoryId",
                            //    Title = FinancialUIStrings.QueryPanel_QueryItem_Title_Common_PartsSalesCategory,
                            //    KeyValueItems = this.kvPartsSalesCategoryName,
                            //}
                            , new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                DefaultValue = new[] {
                                    new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now
                                    }
                            },new KeyValuesQueryItem{
                               ColumnName ="PaymentMethod", 
                            KeyValueItems = this.KeyValueManager[kvNames[1]],
                               Title=FinancialUIStrings.QueryPanel_QueryItem_Title_PaymentMethod
                            }
                        }
                    }
                };
        }
    }
}
