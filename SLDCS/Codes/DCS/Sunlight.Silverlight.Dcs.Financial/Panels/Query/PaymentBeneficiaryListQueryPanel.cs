﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query {
    public class PaymentBeneficiaryListQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "WorkflowOfSimpleApproval_Status"
        };
        public PaymentBeneficiaryListQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(kvNames);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = FinancialUIStrings.QueryPanel_Title_PaymentBeneficiaryList,
                    EntityType = typeof(PaymentBeneficiaryList),
                   QueryItems = new QueryItem[]{
                        new CustomQueryItem {
                            ColumnName = "Company.Name",
                            DataType = typeof(string),
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_Company_Name
                        },new CustomQueryItem {
                            ColumnName = "Company.Code",
                            DataType = typeof(string),
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_Company_Code
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                             DefaultValue = new[] {
                                    new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now
                               }
                        },new QueryItem {                   
                            ColumnName = "PaymentBillCode",
                            IsExact=true
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[kvNames[0]]
                        }
                    }
                }
            };
        }
    }
}
