﻿
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query {
    public class PaymentBillQueryPanel : DcsQueryPanelBase {
        public PaymentBillQueryPanel() {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private readonly string[] kvNames = {
            "PaymentBill_Status", "PaymentBill_PaymentType"
        };

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = FinancialUIStrings.QueryPanel_Title_PaymentBill,
                    EntityType = typeof(VirtualPaymentBill),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                            ColumnName = "CustomerCompanyName",
                            DataType = typeof(string),
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_PaymentBill_Name
                        },
                        new CustomQueryItem {
                            ColumnName = "CustomerCompanyCode",
                            DataType = typeof(string),
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_PaymentBill_Code
                        },
                        new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        },
                        new DateTimeRangeQueryItem {
                            ColumnName = "InvoiceDate",
                            Title= FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccountHisDetail_InvoiceDate
                        },
                        new CustomQueryItem {
                            ColumnName = "BusinessCode",
                            DataType = typeof(string),
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CredenceApplication_BusinessCode
                        },
                        new KeyValuesQueryItem {
                            ColumnName = "PaymentMethod",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_PaymentBill_PaymentMethod
                        },
                        new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_Status
                        },
                        new QueryItem {
                            ColumnName = "CreatorName"
                        }
                    }
                }
            };
        }
    }
}