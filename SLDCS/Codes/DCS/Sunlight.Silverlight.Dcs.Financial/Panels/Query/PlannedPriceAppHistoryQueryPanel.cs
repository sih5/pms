﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query {
    public class PlannedPriceAppHistoryQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "MasterData_Status"
        };

        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();

        public PlannedPriceAppHistoryQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(kvNames);
        }
        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategory in loadOp.Entities)
                    this.kvPartsSalesCategoryName.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = FinancialUIStrings.QueryPanel_Title_PlannedPriceAppDetail,
                        EntityType = typeof(PlannedPriceAppDetail),
                        QueryItems = new QueryItem[] {
                            new QueryItem{
                                ColumnName = "SparePartName",
                                Title = FinancialUIStrings.QueryPanel_QueryItem_Title_SparePart_Name
                            },new QueryItem{
                                ColumnName = "SparePartCode",
                                Title = FinancialUIStrings.QueryPanel_QueryItem_Title_SparePart_Code
                            }, new DateTimeRangeQueryItem {
                                ColumnName = "PlannedPriceApp.CreateTime",
                                Title = FinancialUIStrings.QueryPanel_QueryItem_Title_PlannedPriceApp_CreateTime,
                                DefaultValue = new[] {
                                    new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                                }
                            }, new KeyValuesQueryItem {
                                ColumnName = "PlannedPriceApp.PartsSalesCategoryId",
                                Title = FinancialUIStrings.QueryPanel_QueryItem_Title_Common_PartsSalesCategory,
                                KeyValueItems = this.kvPartsSalesCategoryName
                        }
                        }
                    }
                };
        }
    }
}
