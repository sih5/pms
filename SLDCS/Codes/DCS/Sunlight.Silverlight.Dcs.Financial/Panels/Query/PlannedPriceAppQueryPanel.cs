﻿using System;
using System.Collections.ObjectModel;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System.ServiceModel.DomainServices.Client;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query {
    public class PlannedPriceAppQueryPanel : DcsQueryPanelBase {

        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = {
            "PlannedPriceApp_Status"
        };

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategory in loadOp.Entities)
                    this.kvPartsSalesCategoryName.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = FinancialUIStrings.QueryPanel_Title_PlannedPriceApp,
                    EntityType = typeof(PlannedPriceApp),
                    QueryItems = new[] {
                         new QueryItem {
                            ColumnName = "Code",
                            Title = FinancialUIStrings.DataEditPanel_Text_PlannedPriceApp_Code
                        }, new KeyValuesQueryItem {
                                ColumnName = "PartsSalesCategoryId",
                                Title = FinancialUIStrings.QueryPanel_QueryItem_Title_Common_PartsSalesCategory,
                                KeyValueItems = this.kvPartsSalesCategoryName
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, 1, 1), new DateTime(DateTime.Now.Year,12,31)
                            }
                        }
                    }
                }
            };
        }

        public PlannedPriceAppQueryPanel() {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
        }

    }
}
