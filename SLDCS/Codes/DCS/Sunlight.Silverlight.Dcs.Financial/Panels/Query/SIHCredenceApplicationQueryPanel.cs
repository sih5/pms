﻿using System.Collections.ObjectModel;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query {
    public class SIHCredenceApplicationQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
           "SIHCreditInfoStatus"
        };

        public SIHCredenceApplicationQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = FinancialUIStrings.QueryPanel_Title_SIHCredenceApplication,
                    EntityType = typeof(SIHCreditInfo),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                            ColumnName = "Code",
                            Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccountHisDetail_SourceCode,
                            DataType=typeof(string)
                        },new CustomQueryItem {
                            ColumnName = "CompanyCode",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccount_AgencyCode,
                            DataType=typeof(string)
                        },new CustomQueryItem {
                            ColumnName = "CompanyName",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccount_AgencyName,
                            DataType=typeof(string)
                        },new CustomQueryItem {
                            ColumnName = "DealerCode",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccount_DealerCode,
                            DataType=typeof(string)
                        },new CustomQueryItem {
                            ColumnName = "DealerName",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccount_DealerName,
                            DataType=typeof(string)
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                             KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_Status
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"                          
                        }
                    }
                }
            };
        }
    }
}
