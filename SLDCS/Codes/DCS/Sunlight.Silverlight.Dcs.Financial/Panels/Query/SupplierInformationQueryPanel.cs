﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query {
    public class SupplierInformationQueryPanel:DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };
        public SupplierInformationQueryPanel()
        {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(kvNames);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = FinancialUIStrings.QueryPanel_Title_SupplierInformation,
                        EntityType = typeof(BranchSupplierRelation),
            QueryItems = new QueryItem[]
                        { new CustomQueryItem 
                           {
                                ColumnName = "Company.Name",
                                DataType = typeof(string),
                                Title = FinancialUIStrings.QueryPanel_QueryItem_Title_SupplierCompany_Name,
                                IsExact = false
                            }, new CustomQueryItem {
                                ColumnName = "Company.Code",
                                DataType = typeof(string),
                                Title = FinancialUIStrings.QueryPanel_QueryItem_Title_SupplierCompany_Code,
                                IsExact = false
                            }
                        }
                    }
            };
        }
        
    }
}
