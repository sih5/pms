﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Panels.Query {
    public class SupplierOpenAccountAppQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "WorkflowOfSimpleApproval_Status"
        };

        public SupplierOpenAccountAppQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(SupplierOpenAccountApp),
                    Title = FinancialUIStrings.QueryPanel_Title_SupplierOpenAccountApp,
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                            ColumnName = "Company.Name",
                            DataType = typeof(string),
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_SupplierOpenAccountApp_SupplierCompanyName,
                            IsExact = true
                        },
                        new CustomQueryItem {
                            ColumnName = "Company.Code",
                            DataType = typeof(string),
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_SupplierOpenAccountApp_SupplierCompanyCode,
                            IsExact = true
                        },
                        new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        },
                        new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsWorkflowOfSimpleApprovalStatus.新建
                        }
                   }
                }
            };
        }
    }
}
