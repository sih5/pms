﻿using System.Linq;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs.Financial {
    public class UIStrings : ModelBase {
        public void Refresh(params string[] propertyNames) {
            this.NotifyOfPropertiesChange(propertyNames.Length == 0 ? this.GetType().GetProperties().Select(p => p.Name).ToArray() : propertyNames);
        }   

            public string DataEditPanel_Text_Common_Button_Add {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_Common_Button_Add;
            }
        }

        public string DataEditPanel_Text_Common_Button_Delete {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_Common_Button_Delete;
            }
        }

        public string DataEditPanel_Text_Common_Span_To {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_Common_Span_To;
            }
        }

        public string DataEditPanel_Text_Common_Unit_Currency {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_Common_Unit_Currency;
            }
        }

        public string DataEditPanel_Text_Common_Unit_Km {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_Common_Unit_Km;
            }
        }

        public string DataEditPanel_Text_Common_Unit_Month {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_Common_Unit_Month;
            }
        }

        public string DataEditPanel_Text_Common_Button_MoveDown {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_Common_Button_MoveDown;
            }
        }

        public string DataEditPanel_Text_Common_Button_MoveUp {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_Common_Button_MoveUp;
            }
        }

        public string DataEditPanel_Text_Common_BranchName {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_Common_BranchName;
            }
        }

        public string DetailPanel_Text_Common_BranchName {
            get {
                return Resources.FinancialUIStrings.DetailPanel_Text_Common_BranchName;
            }
        }

        public string QueryPanel_Title_CustomerInformationBySparepart {
            get {
                return Resources.FinancialUIStrings.QueryPanel_Title_CustomerInformationBySparepart;
            }
        }

        public string DataEditPanel_Text_PlannedPriceApp_Code {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_PlannedPriceApp_Code;
            }
        }

        public string DataEditView_Title_WarehouseCostChangeBill {
            get {
                return Resources.FinancialUIStrings.DataEditView_Title_WarehouseCostChangeBill;
            }
        }

        public string DataEditView_Title_WarehouseCostChangeBill_WarehouseCostChangeDetail {
            get {
                return Resources.FinancialUIStrings.DataEditView_Title_WarehouseCostChangeBill_WarehouseCostChangeDetail;
            }
        }

        public string DataEditView_Title_PlannedPriceApp_PlannedPriceAppDetail {
            get {
                return Resources.FinancialUIStrings.DataEditView_Title_PlannedPriceApp_PlannedPriceAppDetail;
            }
        }

        public string DataEditPanel_Text_CustomerTransferBill_InboundAccountGroupName {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_CustomerTransferBill_InboundAccountGroupName;
            }
        }

        public string DataEditPanel_Text_CustomerTransferBill_InboundCustomerCompany_Name {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_CustomerTransferBill_InboundCustomerCompany_Name;
            }
        }

        public string DataEditPanel_Text_CustomerTransferBill_OutboundAccountGroupName {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_CustomerTransferBill_OutboundAccountGroupName;
            }
        }

        public string DataEditPanel_Text_CustomerTransferBill_OutboundCustomerCompany_Name {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_CustomerTransferBill_OutboundCustomerCompany_Name;
            }
        }

        public string DataEditPanel_Text_PaymentBill_BankAccount {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_PaymentBill_BankAccount;
            }
        }

        public string DataEditPanel_Text_PaymentBill_CustomerCompany {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_PaymentBill_CustomerCompany;
            }
        }

        public string DataEdit_Title_CustomerInformation {
            get {
                return Resources.FinancialUIStrings.DataEdit_Title_CustomerInformation;
            }
        }

        public string DataEditPanel_Text_CredenceApplication_Code {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_CredenceApplication_Code;
            }
        }

        public string DataEditPanel_Text_CredenceApplication_GuarantorCompany {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_CredenceApplication_GuarantorCompany;
            }
        }

        public string DataEditPanel_Text_CredenceApplication_MortgageAssets {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_CredenceApplication_MortgageAssets;
            }
        }

        public string DataEditPanel_Text_CredenceApplication_SalesCompany {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_CredenceApplication_SalesCompany;
            }
        }

        public string DataEditPanel_Text_ServiceActivity_CustomerCompany {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_ServiceActivity_CustomerCompany;
            }
        }

        public string DataEditPanel_Text_CredenceApplication_AccountGroup {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_CredenceApplication_AccountGroup;
            }
        }

        public string DataEditPanel_Text_AccountGroup_Name {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_AccountGroup_Name;
            }
        }

        public string DataEditPanel_Text_Company_Name {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_Company_Name;
            }
        }

        public string DataEditPanel_Text_CustomerOpenAccountApp_AccountInitialDeposit {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_CustomerOpenAccountApp_AccountInitialDeposit;
            }
        }

        public string DataEditPanel_Text_AccountGroup_SalesCompany {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_AccountGroup_SalesCompany;
            }
        }

        public string DataEditPanel_Title_PlannedPriceApp_PartsSalesCategory {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Title_PlannedPriceApp_PartsSalesCategory;
            }
        }

        public string DataEditPanel_Text_PartsRebateType_PartsSalesCategory {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_PartsRebateType_PartsSalesCategory;
            }
        }

        public string DataEditPanel_Text_PartsRebateApplication_AccountGroup {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_PartsRebateApplication_AccountGroup;
            }
        }

        public string DataEditPanel_Text_PartsRebateApplication_CustomerCompanyName {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_PartsRebateApplication_CustomerCompanyName;
            }
        }

        public string DataEditPanel_Text_PartsRebateApplication_PartsRebateType {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_PartsRebateApplication_PartsRebateType;
            }
        }

        public string DataEditPanel_Text_PartsRebateApplication_PartsSalesCategory {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_PartsRebateApplication_PartsSalesCategory;
            }
        }

        public string DataEditPanel_Text_PayOutBill_SupplierCompany {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_PayOutBill_SupplierCompany;
            }
        }

        public string DataEditPanel_Text_PayOutBill_PaymentMethod {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_PayOutBill_PaymentMethod;
            }
        }

        public string Action_Title_MergeExport {
            get {
                return Resources.FinancialUIStrings.Action_Title_MergeExport;
            }
        }

        public string QueryPanel_QueryItem_Title_CustomerCompany_Type {
            get {
                return Resources.FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Type;
            }
        }

        public string DataEditPanel_Text_SupplierOpenAccountApp_PartsSalesCategory {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_SupplierOpenAccountApp_PartsSalesCategory;
            }
        }

        public string DataEditPanel_Text_SupplierOpenAccountApp_SupplierCompany {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_SupplierOpenAccountApp_SupplierCompany;
            }
        }

        public string DataEditView_Validation_PaymentBeneficiaryLists_Amount1 {
            get {
                return Resources.FinancialUIStrings.DataEditView_Validation_PaymentBeneficiaryLists_Amount1;
            }
        }

        public string DataEditView_Validation_PaymentBeneficiaryLists_Amount2 {
            get {
                return Resources.FinancialUIStrings.DataEditView_Validation_PaymentBeneficiaryLists_Amount2;
            }
        }

        public string DataEditView_Notification_CompletedImport {
            get {
                return Resources.FinancialUIStrings.DataEditView_Notification_CompletedImport;
            }
        }

        public string DataEditView_Notification_CompletedImportWithError {
            get {
                return Resources.FinancialUIStrings.DataEditView_Notification_CompletedImportWithError;
            }
        }

        public string DataEditPanel_Text_Button_Cancel {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_Button_Cancel;
            }
        }

        public string DataEditPanel_Text_Button_Submit {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_Button_Submit;
            }
        }

        public string DataEditView_Validation_CredenceApplication_ExpireDateIsNull {
            get {
                return Resources.FinancialUIStrings.DataEditView_Validation_CredenceApplication_ExpireDateIsNull;
            }
        }

        public string DataEditView_Validation_DetailIsNull {
            get {
                return Resources.FinancialUIStrings.DataEditView_Validation_DetailIsNull;
            }
        }

        public string Action_Title_Add {
            get {
                return Resources.FinancialUIStrings.Action_Title_Add;
            }
        }

        public string Action_Title_Close {
            get {
                return Resources.FinancialUIStrings.Action_Title_Close;
            }
        }

        public string Action_Title_HistoricalDataBase {
            get {
                return Resources.FinancialUIStrings.Action_Title_HistoricalDataBase;
            }
        }

        public string Action_Title_HistoryStockSum {
            get {
                return Resources.FinancialUIStrings.Action_Title_HistoryStockSum;
            }
        }

        public string Action_Title_Open {
            get {
                return Resources.FinancialUIStrings.Action_Title_Open;
            }
        }

        public string ActionPanel_Title_FinancialSnapshotSet {
            get {
                return Resources.FinancialUIStrings.ActionPanel_Title_FinancialSnapshotSet;
            }
        }

        public string DataEditPanel_Confirm_PlannedPriceApp {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Confirm_PlannedPriceApp;
            }
        }

        public string DataEditPanel_Text_AccountPeriod_Year {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_AccountPeriod_Year;
            }
        }

        public string DataEditPanel_Text_CredenceApplication_Annex {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_CredenceApplication_Annex;
            }
        }

        public string DataEditPanel_Text_CredenceApplication_CreditType {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_CredenceApplication_CreditType;
            }
        }

        public string DataEditPanel_Text_CredenceApplication_Quota {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_CredenceApplication_Quota;
            }
        }

        public string DataEditPanel_Title_AccountPeriod_Month {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Title_AccountPeriod_Month;
            }
        }

        public string DataEditPanel_Title_Delete {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Title_Delete;
            }
        }

        public string DataEditPanel_Title_Upload {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Title_Upload;
            }
        }

        public string DataEditPanel_Text_AccountPeriod_MonthlySettleMonth {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_AccountPeriod_MonthlySettleMonth;
            }
        }

        public string DataEditPanel_Text_AccountPeriod_MonthlySettleYear {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_AccountPeriod_MonthlySettleYear;
            }
        }

        public string DataEditPanel_Text_CustomerTransferBill_BusinessType {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_CustomerTransferBill_BusinessType;
            }
        }

        public string DataEditPanel_Text_CustomerTransferBill_InvoiceDate {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_CustomerTransferBill_InvoiceDate;
            }
        }

        public string DataEditPanel_Text_PaymentBill_InvoiceDate {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_PaymentBill_InvoiceDate;
            }
        }

        public string DataEditPanel_Text_PaymentBill_PaymentMethod {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Text_PaymentBill_PaymentMethod;
            }
        }

        public string DataEditView_Title_Detail_Export {
            get {
                return Resources.FinancialUIStrings.DataEditView_Title_Detail_Export;
            }
        }

        public string DataEditView_Title_Detail_Import {
            get {
                return Resources.FinancialUIStrings.DataEditView_Title_Detail_Import;
            }
        }

        public string DataEditView_Validation_AccountPeriod_PleaseChooseTheMonth {
            get {
                return Resources.FinancialUIStrings.DataEditView_Validation_AccountPeriod_PleaseChooseTheMonth;
            }
        }

        public string DataEditView_Validation_AccountPeriod_PleaseChooseTheYear {
            get {
                return Resources.FinancialUIStrings.DataEditView_Validation_AccountPeriod_PleaseChooseTheYear;
            }
        }

        public string DataEditView_Validation_CredenceApplication_ChooseCreditType {
            get {
                return Resources.FinancialUIStrings.DataEditView_Validation_CredenceApplication_ChooseCreditType;
            }
        }

        public string Action_Title_Reject {
            get {
                return Resources.FinancialUIStrings.Action_Title_Reject;
            }
        }

        public string DataEditView_Title_CredenceApplication {
            get {
                return Resources.FinancialUIStrings.DataEditView_Title_CredenceApplication;
            }
        }

        public string DataEditView_Title_CredenceApplicationFirst {
            get {
                return Resources.FinancialUIStrings.DataEditView_Title_CredenceApplicationFirst;
            }
        }

        public string DataGridView_ColumnItem_CheckerName {
            get {
                return Resources.FinancialUIStrings.DataGridView_ColumnItem_CheckerName;
            }
        }

        public string DataGridView_ColumnItem_CheckerTime {
            get {
                return Resources.FinancialUIStrings.DataGridView_ColumnItem_CheckerTime;
            }
        }

        public string DataGridView_ColumnItem_UpperApproverName {
            get {
                return Resources.FinancialUIStrings.DataGridView_ColumnItem_UpperApproverName;
            }
        }

        public string DataGridView_ColumnItem_UpperApproveTime {
            get {
                return Resources.FinancialUIStrings.DataGridView_ColumnItem_UpperApproveTime;
            }
        }

        public string DataGridView_ColumnItem_UpperCheckerName {
            get {
                return Resources.FinancialUIStrings.DataGridView_ColumnItem_UpperCheckerName;
            }
        }

        public string DataGridView_ColumnItem_UpperCheckTime {
            get {
                return Resources.FinancialUIStrings.DataGridView_ColumnItem_UpperCheckTime;
            }
        }

        public string DataGridView_Column_InitApprover {
            get {
                return Resources.FinancialUIStrings.DataGridView_Column_InitApprover;
            }
        }

        public string DataGridView_Column_InitApproveTime {
            get {
                return Resources.FinancialUIStrings.DataGridView_Column_InitApproveTime;
            }
        }

        public string DataManagementView_Title_SIHCredenceApplicationManagement {
            get {
                return Resources.FinancialUIStrings.DataManagementView_Title_SIHCredenceApplicationManagement;
            }
        }

        public string QueryPanel_Title_SIHCredenceApplication {
            get {
                return Resources.FinancialUIStrings.QueryPanel_Title_SIHCredenceApplication;
            }
        }

        public string DataGridView_ColumnItem_Title_SIHCreditInfo_ATotalFee {
            get {
                return Resources.FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfo_ATotalFee;
            }
        }

        public string DataGridView_ColumnItem_Title_SIHCreditInfo_BTotalFee {
            get {
                return Resources.FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfo_BTotalFee;
            }
        }

        public string DataGridView_ColumnItem_Title_SIHCreditInfo_ChangeAmount {
            get {
                return Resources.FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfo_ChangeAmount;
            }
        }

        public string DataGridView_ColumnItem_Title_SIHCreditInfo_RejecterName {
            get {
                return Resources.FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfo_RejecterName;
            }
        }

        public string DataGridView_ColumnItem_Title_SIHCreditInfo_RejectTime {
            get {
                return Resources.FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfo_RejectTime;
            }
        }

        public string DataEditPanel_GroupTitle_PartsSalesOrderProcess {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_GroupTitle_PartsSalesOrderProcess;
            }
        }

        public string DataGridView_ColumnItem_Title_SIHCreditInfoDetail_ASIHCredit {
            get {
                return Resources.FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfoDetail_ASIHCredit;
            }
        }

        public string DataGridView_ColumnItem_Title_SIHCreditInfoDetail_BSIHCredit {
            get {
                return Resources.FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfoDetail_BSIHCredit;
            }
        }

        public string DataGridView_ColumnItem_Title_SIHCreditInfoDetail_ExpireTime {
            get {
                return Resources.FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfoDetail_ExpireTime;
            }
        }

        public string DataGridView_ColumnItem_Title_SIHCreditInfoDetail_MarketingDepartmentCode {
            get {
                return Resources.FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfoDetail_MarketingDepartmentCode;
            }
        }

        public string DataGridView_ColumnItem_Title_SIHCreditInfoDetail_ValidationTime {
            get {
                return Resources.FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfoDetail_ValidationTime;
            }
        }

        public string DataEdit_Title_SIHCreditInfo {
            get {
                return Resources.FinancialUIStrings.DataEdit_Title_SIHCreditInfo;
            }
        }

        public string DataGridView_ColumnItem_Title_SIHCreditInfoDetail_ThisSIHCredit {
            get {
                return Resources.FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfoDetail_ThisSIHCredit;
            }
        }

        public string DataGridView_ColumnItem_Title_SIHCreditInfoDetail_MarketingDepartmentName {
            get {
                return Resources.FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfoDetail_MarketingDepartmentName;
            }
        }

        public string DataManagementView_Notification_SubmitSuccess {
            get {
                return Resources.FinancialUIStrings.DataManagementView_Notification_SubmitSuccess;
            }
        }

        public string DataEditPanel_Title_Credence_Company {
            get {
                return Resources.FinancialUIStrings.DataEditPanel_Title_Credence_Company;
            }
        }

        public string QueryPanel_TitleCredenceApplication {
            get {
                return Resources.FinancialUIStrings.QueryPanel_TitleCredenceApplication;
            }
        }

        public string QueryPanel_TitleCredenceApplication_AdvancedApprove {
            get {
                return Resources.FinancialUIStrings.QueryPanel_TitleCredenceApplication_AdvancedApprove;
            }
        }

        public string QueryPanel_TitleCredenceApplication_AdvancedAudit {
            get {
                return Resources.FinancialUIStrings.QueryPanel_TitleCredenceApplication_AdvancedAudit;
            }
        }

        public string Upload_Success {
            get {
                return Resources.FinancialUIStrings.Upload_Success;
            }
        }

        public string Upload_Tips {
            get {
                return Resources.FinancialUIStrings.Upload_Tips;
            }
        }

        public string Upload_Validation_Error1 {
            get {
                return Resources.FinancialUIStrings.Upload_Validation_Error1;
            }
        }

        public string Upload_Validation_Error2 {
            get {
                return Resources.FinancialUIStrings.Upload_Validation_Error2;
            }
        }

}
}
