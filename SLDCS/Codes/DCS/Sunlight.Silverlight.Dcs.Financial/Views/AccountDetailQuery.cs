﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Financial.Views {
    [PageMeta("PartsFinancialAndControls", "PartFunctions", "AccountDetailQuery", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class AccountDetailQuery : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;

        public DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("AccountDetailQuery"));
            }
        }

        public AccountDetailQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = FinancialUIStrings.DataManagementView_Title_AccountDetailQuery;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var accountGroupId = filterItem.Filters.Single(r => r.MemberName == "CustomerAccount.AccountGroupId").Value as int?;
                    var businessType = filterItem.Filters.Single(e => e.MemberName == "BusinessType").Value as int?;
                    var sourceCode = filterItem.Filters.Single(r => r.MemberName == "SourceCode").Value as string;
                    var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    DateTime? begianDate = null;
                    DateTime? endDate = null;
                    if(createTime != null) {
                        begianDate = createTime.Filters.First(r => r.MemberName == "Process").Value as DateTime?;
                        endDate = createTime.Filters.Last(r => r.MemberName == "Process").Value as DateTime?;
                    }
                    var ids = new int[] { };
                    if(this.DataGridView.SelectedEntities != null) {
                        var customerAccountHisDetails = this.DataGridView.SelectedEntities.Cast<CustomerAccountHisDetail>();
                        ids = customerAccountHisDetails.Select(r => r.Id).ToArray();
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.ExportCustomerAccountHisDetail(ids, accountGroupId, sourceCode, businessType, begianDate, endDate);
                    break;
            }
        }


        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();

                default:
                    return false;
            }
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private void ExportCustomerAccountHisDetail(int[] ids, int? accountGroupId, string sourceCode, int? businessType, DateTime? processDateBegin, DateTime? processDateEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportCustomerAccountHisDetailAsync(ids, (int)DcsCompanyType.服务站, accountGroupId, BaseApp.Current.CurrentUserData.EnterpriseId, null, null, sourceCode, businessType, processDateBegin, processDateEnd);
            this.excelServiceClient.ExportCustomerAccountHisDetailCompleted += excelServiceClient_ExportCustomerAccountHisDetailCompleted;
            this.excelServiceClient.ExportCustomerAccountHisDetailCompleted += excelServiceClient_ExportCustomerAccountHisDetailCompleted;
        }

        private void excelServiceClient_ExportCustomerAccountHisDetailCompleted(object sender, ExportCustomerAccountHisDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }


        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var composite = filterItem as CompositeFilterItem;
            if(composite == null)
                return;
            ClientVar.ConvertTime(composite);
            this.DataGridView.FilterItem = composite;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "AccountDetailQuery"
                };
            }
        }
    }
}
