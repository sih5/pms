﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs.Financial.Views {
    [PageMeta("Common", "Dealer", "AccountGroup", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_PAUSE_RESUME
    })]
    public class AccountGroupManagement : DcsDataManagementViewBase {

        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;


        public AccountGroupManagement() {
            this.Title = FinancialUIStrings.DataManagementView_Title_AccountGroup;
            this.Initializer.Register(this.Initialize);
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("AccountGroup"));
            }
        }


        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }
        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("AccountGroup");
                    dataEditView.EditSubmitted += dataEditView_EditSubmitted;
                    dataEditView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void dataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void dataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "AccountGroup"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.PAUSE:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<AccountGroup>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsMasterDataStatus.有效;
                case CommonActionKeys.RESUME:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitie = this.DataGridView.SelectedEntities.Cast<AccountGroup>().ToArray();
                    if(entitie.Length != 1)
                        return false;
                    return entitie[0].Status == (int)DcsMasterDataStatus.停用;
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var accountGroup = this.DataEditView.CreateObjectToEdit<AccountGroup>();
                    accountGroup.SalesCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    accountGroup.SalesCompanyName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    accountGroup.Status = (int)DcsMasterDataStatus.有效;
                    accountGroup.AccountType = (int)DcsAccountGroupAccountType.配件销售;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(FinancialUIStrings.DataManagementView_Confirm_Approve, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<AccountGroup>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废账户组)
                                entity.作废账户组();
                            this.ExecuteSerivcesMethod(FinancialUIStrings.DataManagementView_Notification_ApproveSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.PAUSE:
                    DcsUtils.Confirm(FinancialUIStrings.DataManagementView_Confirm_Pause, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<AccountGroup>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            entity.Status = (int)DcsMasterDataStatus.停用;
                            if(entity.Can停用账户组)
                                entity.停用账户组();
                            this.ExecuteSerivcesMethod(FinancialUIStrings.DataManagementView_Notification_PauseSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.RESUME:
                     DcsUtils.Confirm(FinancialUIStrings.DataManagementView_Confirm_Resume, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<AccountGroup>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            entity.Status = (int)DcsMasterDataStatus.有效;
                            if(entity.Can恢复账户组)
                                entity.恢复账户组();
                            this.ExecuteSerivcesMethod(FinancialUIStrings.DataManagementView_Notification_ResumeSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
            }
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, Core.Model.FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
