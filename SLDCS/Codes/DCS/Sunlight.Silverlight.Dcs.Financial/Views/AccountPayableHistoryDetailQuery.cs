﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Financial.Views {
    [PageMeta("PartsFinancialAndControls", "ToPay", "SupplierAccountHisDetail", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class AccountPayableHistoryDetailQuery : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        public DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("AccountPayableHistoryDetail"));
            }
        }
        public AccountPayableHistoryDetailQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = FinancialUIStrings.DataManagementView_Title_AccountPayableHistoryDetail;
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<AccountPayableHistoryDetail>().Select(e => e.Id).ToArray();
                        this.ExportSupplierAccountHisDetail(ids, null, BaseApp.Current.CurrentUserData.EnterpriseId, null, null, null, null, null, null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var companyName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SupplierAccount.Company.Name") == null ? null : filterItem.Filters.SingleOrDefault(r => r.MemberName == "SupplierAccount.Company.Name").Value as string;
                        var companyCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SupplierAccount.Company.Code") == null ? null : filterItem.Filters.SingleOrDefault(r => r.MemberName == "SupplierAccount.Company.Code").Value as string;
                        var sourceCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SourceCode") == null ? null : filterItem.Filters.SingleOrDefault(r => r.MemberName == "SourceCode").Value as string;
                        var businessType = filterItem.Filters.SingleOrDefault(r => r.MemberName == "BusinessType") == null ? null : filterItem.Filters.SingleOrDefault(r => r.MemberName == "BusinessType").Value as int?;
                        var partsSalesCategoryId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SupplierAccount.PartsSalesCategoryId") == null ? null : filterItem.Filters.SingleOrDefault(r => r.MemberName == "SupplierAccount.PartsSalesCategoryId").Value as int?;
                        var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "ProcessDate").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "ProcessDate").Value as DateTime?;
                        }
                        this.ExportSupplierAccountHisDetail(null, partsSalesCategoryId, BaseApp.Current.CurrentUserData.EnterpriseId, companyName, companyCode, sourceCode, businessType, createTimeBegin, createTimeEnd);
                    }
                    break;
            }
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private void ExportSupplierAccountHisDetail(int[] ids, int? partsSalesCategoryId, int buyerCompanyId, string companyName, string companyCode, string sourceCode, int? businessType, DateTime? processDateBegin, DateTime? processDateEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportAccountPayableHistoryDetailAsync(ids, partsSalesCategoryId, buyerCompanyId, companyName, companyCode, sourceCode, businessType, processDateBegin, processDateEnd);
            this.excelServiceClient.ExportAccountPayableHistoryDetailCompleted -= ExcelServiceClient_ExportAccountPayableHistoryDetailCompleted;
            this.excelServiceClient.ExportAccountPayableHistoryDetailCompleted += ExcelServiceClient_ExportAccountPayableHistoryDetailCompleted;
        }

        private void ExcelServiceClient_ExportAccountPayableHistoryDetailCompleted(object sender, ExportAccountPayableHistoryDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var newCompositeFilter = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                newCompositeFilter = filterItem as CompositeFilterItem;
                ClientVar.ConvertTime(newCompositeFilter);
            } else
                newCompositeFilter.Filters.Add(filterItem);
            newCompositeFilter.Filters.Add(new FilterItem {
                MemberName = "SupplierAccount.BuyerCompanyId",
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId,
                Operator = FilterOperator.IsEqualTo
            });
            this.DataGridView.FilterItem = newCompositeFilter;
            this.DataGridView.ExecuteQueryDelayed();

            //CompositeFilterItem compositeFilter;
            //if(!(filterItem is CompositeFilterItem)) {
            //    compositeFilter = new CompositeFilterItem();
            //    compositeFilter.Filters.Add(filterItem);
            //} else
            //    compositeFilter = filterItem as CompositeFilterItem;
            //ClientVar.ConvertTime(compositeFilter);
            //var compositeFilters = new CompositeFilterItem();
            ////compositeFilters.LogicalOperator = LogicalOperator.And;
            ////compositeFilters.Filters.Add(new FilterItem {
            ////    MemberName = "SupplierAccountId",
            ////    MemberType = typeof(string),
            ////    Value = "SupplierAccount.Id",
            ////    Operator = FilterOperator.IsEqualTo
            ////});
            //compositeFilters.Filters.Add(new FilterItem {
            //    MemberName = "SupplierAccount.BuyerCompanyId",
            //    MemberType = typeof(int),
            //    Value = BaseApp.Current.CurrentUserData.EnterpriseId,
            //    Operator = FilterOperator.IsEqualTo
            //});
            ////compositeFilters.Filters.Add(new FilterItem {
            ////    MemberName = "SupplierAccount.Company.Id",
            ////    MemberType = typeof(int),
            ////    Value = SupplierAccount.SupplierCompanyId,
            ////    Operator = FilterOperator.IsEqualTo
            ////});
            //compositeFilter.Filters.Add(compositeFilters);
            //this.DataGridView.FilterItem = compositeFilter;
            //this.DataGridView.ExecuteQueryDelayed();
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "AccountPayableHistoryDetail"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
    }
}
