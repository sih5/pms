﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using System.Collections.ObjectModel;
using Sunlight.Silverlight.ViewModel;
using System.Windows.Browser;

namespace Sunlight.Silverlight.Dcs.Financial.Views
{
    [PageMeta("PartsFinancialAndControls", "StockAccounting", "AccountPeriod", ActionPanelKeys = new[] {
    "AccountPeriod"
    })]
    public class AccountPeriodManagement : DcsDataManagementViewBase
    {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;

        private readonly string[] kvNames = {
                   "AccountPeriodYear","AccountPeriodMonth"
        };
        private ObservableCollection<KeyValuePair> kvYear;
        private ObservableCollection<KeyValuePair> KvYear
        {
            get
            {
                return this.kvYear ?? (this.kvYear = new ObservableCollection<KeyValuePair>());
            }
        }
        private ObservableCollection<KeyValuePair> kvMonth;
        private ObservableCollection<KeyValuePair> KvMonth
        {
            get
            {
                return this.kvMonth ?? (this.kvMonth = new ObservableCollection<KeyValuePair>());
            }
        }
        private KeyValueManager keyValueManager;
        private KeyValueManager KeyValueManager
        {
            get
            {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        public AccountPeriodManagement()
        {
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData(() =>
            {
                foreach (var item in this.KeyValueManager[kvNames[0]])
                    KvYear.Add(item);
                foreach (var item in this.KeyValueManager[kvNames[1]])
                    KvMonth.Add(item);
            });


            this.Initializer.Register(this.Initialize);
            this.Title = FinancialUIStrings.DataManagementView_Title_AccountPeriod;
        }
        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }
        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("AccountPeriod"));
            }
        }
        private DataEditViewBase DataEditView
        {
            get
            {
                if (this.dataEditView == null)
                {
                    this.dataEditView = DI.GetDataEditView("AccountPeriod");
                    dataEditView.EditSubmitted += dataEditView_EditSubmitted;
                    dataEditView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void dataEditView_EditSubmitted(object sender, EventArgs e)
        {
            this.ResetEditView();
            if (this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void dataEditView_EditCancelled(object sender, EventArgs e)
        {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }
        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "AccountPeriod"
                };
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId)
            {
                case CommonActionKeys.ADD:

                    return true;
                case "Open":
                    if (this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    var entity = this.DataGridView.SelectedEntities.Cast<AccountPeriod>().SingleOrDefault();
                    if ((int)DcsAccountPeriodStatus.已关闭 == entity.Status)
                    {
                        return true;
                    }
                    else return false;
                case "Close":
                    if (this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    var closeentity = this.DataGridView.SelectedEntities.Cast<AccountPeriod>().SingleOrDefault();
                    if ((int)DcsAccountPeriodStatus.有效 == closeentity.Status)
                    {
                        return true;
                    }
                    else return false;
                case CommonActionKeys.MERGEEXPORT:
                    return true;
                default:
                    return false;
            }
        }
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId)
            {
                case "Add":
                    var accountPeriod = this.DataEditView.CreateObjectToEdit<AccountPeriod>();
                    accountPeriod.Status = (int)DcsAccountPeriodStatus.有效;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case "Close":
                    DcsUtils.Confirm(FinancialUIStrings.DataManagementView_Confirm_Close, () =>
                    {
                        var entity = this.DataGridView.SelectedEntities.Cast<AccountPeriod>().SingleOrDefault();
                        if (entity == null)
                            return;
                        try
                        {
                            if (entity.Can关闭)
                                entity.关闭();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if (domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp =>
                            {
                                if (submitOp.HasError)
                                {
                                    if (!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(FinancialUIStrings.DataManagementView_Notification_CloseSuccess);
                                this.DataGridView.ExecuteQueryDelayed();
                                this.CheckActionsCanExecute();
                            }, null);
                        }
                        catch (Exception ex)
                        {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "Open":
                    DcsUtils.Confirm(FinancialUIStrings.DataManagementView_Confirm_Open, () =>
                    {
                        var entity = this.DataGridView.SelectedEntities.Cast<AccountPeriod>().SingleOrDefault();
                        if (entity == null)
                            return;
                        try
                        {
                            if (entity.Can开启)
                                entity.开启();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if (domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp =>
                            {
                                if (submitOp.HasError)
                                {
                                    if (!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(FinancialUIStrings.DataManagementView_Notification_OpenSuccess);
                                this.DataGridView.ExecuteQueryDelayed();
                                this.CheckActionsCanExecute();
                            }, null);
                        }
                        catch (Exception ex)
                        {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any())
                    {
                        var ids = this.DataGridView.SelectedEntities.Cast<AccountPeriod>().Select(r => r.Id).ToArray();
                        this.ExportAccountPeriod(ids, null, null, null, null, null);
                    }
                    else
                    {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if (filterItem == null)
                            return;
                        var year = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Year") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Year").Value as string;
                        var month = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Month") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Month").Value as string;
                        var status = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Status") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                      
                        DateTime? bCreateTime = null;
                        DateTime? eCreateTime = null;
                       

                        foreach (var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)))
                        {
                            var dateTime = filter as CompositeFilterItem;
                            if (dateTime != null)
                            {
                                if (dateTime.Filters.First().MemberName == "CreateTime")
                                {
                                    bCreateTime = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    eCreateTime = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                               
                            }
                        }

                        this.ExportAccountPeriod(null, year, month, status,  bCreateTime, eCreateTime);
                    }
                    break;
            }
        }
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public void ExportAccountPeriod(int[] ids, string year, string month,int? status, DateTime? bCreateTime, DateTime? eCreateTime)
        {

            this.excelServiceClient.ExportAccountPeriodAsync(ids, year, month, status,  bCreateTime, eCreateTime);
            this.excelServiceClient.ExportAccountPeriodCompleted -= ExcelServiceClient_ExportAccountPeriodCompleted;
            this.excelServiceClient.ExportAccountPeriodCompleted += ExcelServiceClient_ExportAccountPeriodCompleted;
        }
        private void ExcelServiceClient_ExportAccountPeriodCompleted(object sender, ExportAccountPeriodCompletedEventArgs e)
        {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem)
        {
            
            var newFilterItem = new CompositeFilterItem();
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return;
            int? YearValue;
            int? MonthValue;
            if (compositeFilterItem != null)
            {
                foreach (var item in compositeFilterItem.Filters)
                {
                    if (item.MemberName == "Year")
                    {
                        YearValue = item.Value as int?;
                        if (YearValue.HasValue && YearValue != -1)
                        {
                            string YearName = KvYear.FirstOrDefault(r => r.Key == YearValue.Value).Value;
                            newFilterItem.Filters.Add(new FilterItem("Year", typeof(string), FilterOperator.IsEqualTo, YearName));
                        }
                        continue;
                    }
                    if (item.MemberName == "Month")
                    {
                        MonthValue = item.Value as int?;
                        if (MonthValue.HasValue && MonthValue != -1)
                        {
                            string MonthName = KvMonth.FirstOrDefault(r => r.Key == MonthValue.Value).Value;
                            newFilterItem.Filters.Add(new FilterItem("Month", typeof(string), FilterOperator.IsEqualTo, MonthName));
                        }
                        continue;
                    }
                    newFilterItem.Filters.Add(item);
                }
            }
            this.SwitchViewTo(DATA_GRID_VIEW);
            ClientVar.ConvertTime(compositeFilterItem);           
            this.DataGridView.FilterItem = newFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
