﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Financial.Views {
    [PageMeta("PartsFinancialAndControls", "PartFunctions", "AgencyCustomerAccount", ActionPanelKeys = new[] {
       CommonActionKeys.EXPORT})]
    public class AgencyCustomerAccountManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        public DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("AgencyCustomerAccount"));
            }
        }

        public AgencyCustomerAccountManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = FinancialUIStrings.DataManagementView_Title_AgencyCustomerAccount;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var composite = filterItem as CompositeFilterItem;
            if(composite == null)
                return;
            ClientVar.ConvertTime(composite);
            this.DataGridView.FilterItem = composite;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "AgencyCustomerAccount"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<VirtualAgencyCustomerAccount>().Select(r => r.Id).ToArray();
                        this.ExportAgencyCustomerAccount(ids, null, null, null, null, null, null, null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var dealerCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "DealerCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "DealerCode").Value as string;
                        var dealerName = filterItem.Filters.SingleOrDefault(e => e.MemberName == "DealerName") == null ? null : filterItem.Filters.Single(e => e.MemberName == "DealerName").Value as string;
                        var agencyCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "AgencyCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "AgencyCode").Value as string;
                        var agencyName = filterItem.Filters.SingleOrDefault(e => e.MemberName == "AgencyName") == null ? null : filterItem.Filters.Single(e => e.MemberName == "AgencyName").Value as string;
                        var businessType = filterItem.Filters.SingleOrDefault(e => e.MemberName == "BusinessType") == null ? null : filterItem.Filters.Single(r => r.MemberName == "BusinessType").Value as int?;
                        var processDate = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? processDateBegin = null;
                        DateTime? processDateEnd = null;
                        if(processDate != null) {
                            processDateBegin = processDate.Filters.First(r => r.MemberName == "ProcessDate").Value as DateTime?;
                            processDateEnd = processDate.Filters.Last(r => r.MemberName == "ProcessDate").Value as DateTime?;
                        }
                        ShellViewModel.Current.IsBusy = true;
                        this.ExportAgencyCustomerAccount(null, dealerCode, dealerName, agencyCode, agencyName, businessType, processDateBegin, processDateEnd);

                    }
                    break;
            }
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private void ExportAgencyCustomerAccount(int[] ids, string dealerCode, string dealerName, string agencyCode, string agencyName, int? businessType, DateTime? processDateBegin, DateTime? processDateEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportAgencyCustomerAccountAsync(ids, dealerCode, dealerName, businessType, agencyCode, agencyName, processDateBegin, processDateEnd);
            this.excelServiceClient.ExportAgencyCustomerAccountCompleted -= ExcelServiceClientOnExportAgencyCustomerCompleted;
            this.excelServiceClient.ExportAgencyCustomerAccountCompleted += ExcelServiceClientOnExportAgencyCustomerCompleted;

        }

        private void ExcelServiceClientOnExportAgencyCustomerCompleted(object sender, ExportAgencyCustomerAccountCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
    }
}
