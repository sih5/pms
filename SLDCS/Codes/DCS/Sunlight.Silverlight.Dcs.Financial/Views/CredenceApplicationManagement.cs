﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Financial.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Financial.Views {
    [PageMeta("PartsFinancialAndControls", "PartFunctions", "CredenceApplication", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_INITIALAPPROVE_AUDIT_APPROVE_EXPORT_IMPORT_SUBMIT,"CredenceApplication"
    })]
    public class CredenceApplicationManagement : DcsDataManagementViewBase {
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditViewImport;
        private DataEditViewBase dataApproveView;
        private DataEditViewBase dataFirApproveView;
        private const string DATA_EDIT_VIEW_IMPORT = "_DataEditViewImport_";
        private const string DATA_APPROVE_VIEW = "_dataApproveView_";
        private const string DATA_FIRAPPROVE_VIEW = "_dataFirApproveView_";
        private DataEditViewBase dataDetailView;
        private const string DATA_DETAIL_VIEW = "_DataDetailView_";

        public CredenceApplicationManagement() {
            this.Title = FinancialUIStrings.DataManagementView_Title_CredenceApplication;
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_IMPORT, () => this.DataEditViewImport);
            this.RegisterView(DATA_APPROVE_VIEW, () => this.DataApproveView);
            this.RegisterView(DATA_FIRAPPROVE_VIEW, () => this.DataFirApproveView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.dataDetailView);
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("CredenceApplication");
                    dataEditView.EditSubmitted += dataEditView_EditSubmitted;
                    dataEditView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataEditViewImport {
            get {
                if(this.dataEditViewImport == null) {
                    this.dataEditViewImport = DI.GetDataEditView("CredenceApplicationForImport");
                    this.dataEditViewImport.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.dataEditViewImport;
            }
        }
        private DataEditViewBase DataApproveView {
            get {
                if(this.dataApproveView == null) {
                    this.dataApproveView = DI.GetDataEditView("CredenceApplicationForApprove");
                    ((CredenceApplicationForApproveDataEditView)this.dataApproveView).EditSubmitted += this.dataEditView_EditSubmitted;
                    this.dataApproveView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataApproveView;
            }
        }

        private DataEditViewBase DataFirApproveView {
            get {
                if(this.dataFirApproveView == null) {
                    this.dataFirApproveView = DI.GetDataEditView("CredenceApplicationForFirApprove");
                    ((CredenceApplicationForFirApproveDataEditView)this.dataFirApproveView).EditSubmitted += this.dataEditView_EditSubmitted;
                    this.dataFirApproveView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataFirApproveView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewImport = null;
            this.dataApproveView = null;
            this.dataFirApproveView = null;
        }
        private void dataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void dataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("CredenceApplication");
                    this.dataGridView.RowDoubleClick += DataGridView_RowDoubleClick;
                }
                return this.dataGridView;
            }
        }
        private DataEditViewBase DataEditViewDetail {
            get {
                if(this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("CredenceApplicationDetail");
                    this.dataDetailView.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.dataDetailView;
            }
        }
        private void DataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                this.DataEditViewDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                this.SwitchViewTo(DATA_DETAIL_VIEW);
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                case CommonActionKeys.IMPORT:
                    return true;
                case CommonActionKeys.EDIT:

                case CommonActionKeys.SUBMIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<VirtualCredenceApplication>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsCredenceApplicationStatus.新增;
                case CommonActionKeys.INITIALAPPROVE:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitiesIn = this.DataGridView.SelectedEntities.Cast<VirtualCredenceApplication>().ToArray();
                    if(entitiesIn.Length != 1)
                        return false;
                    return entitiesIn[0].Status == (int)DcsCredenceApplicationStatus.已提交 && entitiesIn[0].CreditType == (int)DcsCredit_Type.临时授信;
                case CommonActionKeys.AUDIT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitiesAud = this.DataGridView.SelectedEntities.Cast<VirtualCredenceApplication>().ToArray();
                    if(entitiesAud.Length != 1)
                        return false;
                    if(entitiesAud[0].CreditType == (int)DcsCredit_Type.正常授信) {
                        return entitiesAud[0].Status == (int)DcsCredenceApplicationStatus.已提交;
                    }
                    return entitiesAud[0].Status == (int)DcsCredenceApplicationStatus.初审通过;
                case CommonActionKeys.APPROVE:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitiesApp = this.DataGridView.SelectedEntities.Cast<VirtualCredenceApplication>().ToArray();
                    if(entitiesApp.Length != 1)
                        return false;
                    if(entitiesApp[0].CompanyType == (int)DcsCompanyType.服务站兼代理库 || entitiesApp[0].CompanyType == (int)DcsCompanyType.代理库) {
                        return entitiesApp[0].Status == (int)DcsCredenceApplicationStatus.新增;
                    }
                    return entitiesApp[0].Status == (int)DcsCredenceApplicationStatus.审核通过;
                case "AdvancedAudit":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitiesAd = this.DataGridView.SelectedEntities.Cast<VirtualCredenceApplication>().ToArray();
                    if(entitiesAd.Length != 1)
                        return false;
                    return entitiesAd[0].Status == (int)DcsCredenceApplicationStatus.审批通过 && entitiesAd[0].CreditType == (int)DcsCredit_Type.临时授信;
                case "AdvancedApprove":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitiesAp = this.DataGridView.SelectedEntities.Cast<VirtualCredenceApplication>().ToArray();
                    if(entitiesAp.Length != 1)
                        return false;
                    return entitiesAp[0].Status == (int)DcsCredenceApplicationStatus.高级审核通过 && entitiesAp[0].CreditType == (int)DcsCredit_Type.临时授信;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var credenceApplication = this.DataEditView.CreateObjectToEdit<CredenceApplication>();
                    credenceApplication.ApplyCondition = (int)DcsCredenceApplicationApplyCondition.自由支配使用;
                    credenceApplication.SalesCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    credenceApplication.GuarantorCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    credenceApplication.GuarantorCompanyName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    credenceApplication.ValidationDate = DateTime.Now;
                    credenceApplication.ExpireDate = new DateTime(DateTime.Now.Year, 12, 31);
                    credenceApplication.Status = (int)DcsCredenceApplicationStatus.新增;
                    credenceApplication.ConsumedAmount = 0;
                    //credenceApplication.Amount
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case "AdvancedAudit":
                case "AdvancedApprove":
                case CommonActionKeys.APPROVE:
                    this.DataApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_APPROVE_VIEW);
                    break;
                case CommonActionKeys.INITIALAPPROVE:
                case CommonActionKeys.AUDIT:
                    this.DataFirApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_FIRAPPROVE_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(FinancialUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<VirtualCredenceApplication>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废虚拟信用申请单据)
                                entity.作废虚拟信用申请单据();
                            this.ExecuteSerivcesMethod(FinancialUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var customerCompanyCode = filterItem.Filters.Single(r => r.MemberName == "CompanyCode").Value as string;
                    var customerCompanyName = filterItem.Filters.Single(r => r.MemberName == "CompanyName").Value as string;
                    var businessCode = filterItem.Filters.Single(r => r.MemberName == "BusinessCode").Value as string;
                    var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                    var creditType = filterItem.Filters.Single(e => e.MemberName == "CreditType").Value as int?;
                    var compositeFilterItems = filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).ToArray();
                    DateTime? approveTimeBegin = null;
                    DateTime? approveTimeEnd = null;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    foreach(var dateTimeFilterItem in compositeFilterItems) {
                        var dateTime = dateTimeFilterItem as CompositeFilterItem;
                        if(dateTime != null) {
                            if(dateTime.Filters.Any(r => r.MemberName == "CreateTime")) {
                                createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                            }
                            if(dateTime.Filters.Any(r => r.MemberName == "ApproveTime")) {
                                approveTimeBegin = dateTime.Filters.First(r => r.MemberName == "ApproveTime").Value as DateTime?;
                                approveTimeEnd = dateTime.Filters.Last(r => r.MemberName == "ApproveTime").Value as DateTime?;
                            }
                        }
                    }
                    ShellViewModel.Current.IsBusy = true;
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any())
                        this.excelServiceClient.ExportVirtualCredenceApplicationAsync(this.DataGridView.SelectedEntities.Cast<VirtualCredenceApplication>().Select(r => r.Id).ToArray(), BaseApp.Current.CurrentUserData.EnterpriseId, null, null, null, null, null, null, null, null, null);
                    else
                        this.excelServiceClient.ExportVirtualCredenceApplicationAsync(null, BaseApp.Current.CurrentUserData.EnterpriseId, customerCompanyCode, customerCompanyName, status, approveTimeBegin, approveTimeEnd, createTimeBegin, createTimeEnd, businessCode, creditType);

                    this.excelServiceClient.ExportVirtualCredenceApplicationCompleted -= excelServiceClient_ExportVirtualCredenceApplicationCompleted;
                    this.excelServiceClient.ExportVirtualCredenceApplicationCompleted += excelServiceClient_ExportVirtualCredenceApplicationCompleted;
                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORT);
                    break;
                case CommonActionKeys.SUBMIT:
                    //此处重写Invoke方法
                    DcsUtils.Confirm(FinancialUIStrings.DataManagementView_Confirm_Submit, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<VirtualCredenceApplication>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can提交虚拟信用申请单据)
                                entity.提交虚拟信用申请单据();
                            this.ExecuteSerivcesMethod(FinancialUIStrings.DataManagementView_Notification_SubmitSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
            }
        }

        private void excelServiceClient_ExportVirtualCredenceApplicationCompleted(object sender, ExportVirtualCredenceApplicationCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.DataGridView.ExecuteQuery();
                this.CheckActionsCanExecute();
            }, null);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                "CredenceApplication"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilters = filterItem as CompositeFilterItem;
            if(compositeFilters != null) {
                var createTimeFilters = compositeFilters.Filters.Where(filter => filter is CompositeFilterItem && (filter as CompositeFilterItem).Filters.All(item => item.MemberName == "EffectiveTime")).SingleOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                if(createTimeFilters != null) {
                    var compositeFilter = new CompositeFilterItem();
                    compositeFilter.LogicalOperator = LogicalOperator.And;
                    compositeFilter.Filters.Add(new FilterItem {
                        MemberName = "ValidationDate",
                        Operator = FilterOperator.IsGreaterThanOrEqualTo,
                        MemberType = typeof(DateTime),
                        Value = createTimeFilters.Filters.ElementAt(0).Value
                    });
                    compositeFilter.Filters.Add(new FilterItem {
                        MemberName = "ExpireDate",
                        Operator = FilterOperator.IsLessThanOrEqualTo,
                        MemberType = typeof(DateTime),
                        Value = createTimeFilters.Filters.ElementAt(1).Value
                    });
                    compositeFilter.Filters.Add(new FilterItem {
                        MemberName = "GuarantorCompanyId",
                        Operator = FilterOperator.IsEqualTo,
                        MemberType = typeof(int),
                        Value = BaseApp.Current.CurrentUserData.EnterpriseId
                    });
                    compositeFilters.Filters.Add(compositeFilter);
                }
            }
            //过滤掉EffectiveTime
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilters != null) {
                var filterItems = compositeFilters.Filters.Where
                    (item => (item.GetType() == typeof(FilterItem) && item.MemberName != "EffectiveTime") || (item.GetType() == typeof(CompositeFilterItem) && ((CompositeFilterItem)item).Filters.All(member => member.MemberName != "EffectiveTime")));
                foreach(var item in filterItems)
                    newCompositeFilterItem.Filters.Add(item);
            }
            newCompositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "SalesCompanyId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            ClientVar.ConvertTime(newCompositeFilterItem);
            this.DataGridView.FilterItem = newCompositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }


    }
}
