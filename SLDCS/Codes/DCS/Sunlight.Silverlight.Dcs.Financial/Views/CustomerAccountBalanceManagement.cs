﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Financial.Views {
     [PageMeta("PartsFinancialAndControls", "PartFunctions", "CustomerAccountBalance", ActionPanelKeys = new[] {
       CommonActionKeys.EXPORT})]
    public class CustomerAccountBalanceManagement : DcsDataManagementViewBase {
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        private DataGridViewBase dataGridView;
        public DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("CustomerAccountBalance"));
            }
        }

        public CustomerAccountBalanceManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = "服务站在中心库账面余额";
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        //获取当前登陆企业
        private int currentCompanyType;
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var dcsDomainContext = new DcsDomainContext();
            var newCompositeFilter = filterItem as CompositeFilterItem;
            dcsDomainContext.Load(dcsDomainContext.GetCompaniesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效 && e.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    if(!loadOp.IsErrorHandled) {
                        loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowLoadError(loadOp);
                        return;
                    }
                var company = loadOp.Entities.SingleOrDefault();
                if(company != null)
                    currentCompanyType = company.Type;
                if(currentCompanyType == (int)DcsCompanyType.代理库) {
                    newCompositeFilter.Filters.Add(new FilterItem {
                        MemberName = "SalesCompanyId",
                        MemberType = typeof(int),
                        Value = BaseApp.Current.CurrentUserData.EnterpriseId,
                        Operator = FilterOperator.IsEqualTo
                    });
                }
                this.DataGridView.FilterItem = newCompositeFilter;
                this.DataGridView.ExecuteQueryDelayed();
            }, null);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "CustomerAccountBalance"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                     if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                         var ids = this.DataGridView.SelectedEntities.Cast<VirtualCustomerAccountHistory>().Select(r => r.Id).ToArray();
                        ShellViewModel.Current.IsBusy = true;
                        this.dcsDomainContext.ExportCustomerAccounts(ids, null, null, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                                ShellViewModel.Current.IsBusy = false;
                            }
                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                ShellViewModel.Current.IsBusy = false;
                            }
                        }, null);
                    } else {
                        var compositeFilterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(compositeFilterItem != null) {
                            var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                            if(filterItem == null)
                                return;
                            var companyName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Name") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Name").Value as string;
                            var companyCode = filterItem.Filters.SingleOrDefault(e => e.MemberName == "Code") == null ? null : filterItem.Filters.Single(e => e.MemberName == "Code").Value as string;

                            ShellViewModel.Current.IsBusy = true;
                            this.dcsDomainContext.ExportCustomerAccounts(null, companyName, companyCode, loadOp => {
                                if(loadOp.HasError)
                                    return;
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                    ShellViewModel.Current.IsBusy = false;
                                }
                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                    ShellViewModel.Current.IsBusy = false;
                                }
                            }, null);
                        }
                    }
                    break;
            }
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private void ExportCustomerAccountHistoryForAgency(int[] ids, string companyName, string companyCode, int? companyType, int? accountGroupId, DateTime? theDate) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportCustomerAccountHistoryForAgencyAsync(ids, currentCompanyType, accountGroupId, companyCode, companyName, companyType, theDate);
            this.excelServiceClient.ExportCustomerAccountHistoryForAgencyCompleted -= ExcelServiceClient_ExportCustomerAccountHistoryForAgencyCompleted;
            this.excelServiceClient.ExportCustomerAccountHistoryForAgencyCompleted += ExcelServiceClient_ExportCustomerAccountHistoryForAgencyCompleted;

        }

        private void ExcelServiceClient_ExportCustomerAccountHistoryForAgencyCompleted(object sender, ExportCustomerAccountHistoryForAgencyCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
    }
}
