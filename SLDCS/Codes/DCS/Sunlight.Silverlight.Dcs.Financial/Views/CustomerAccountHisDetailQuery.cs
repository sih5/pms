﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Financial.Views {
    [PageMeta("PartsFinancialAndControls", "PartFunctions", "CustomerAccountHisDetail", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class CustomerAccountHisDetailQuery : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;

        public DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("CustomerAccountHisDetail"));
            }
        }

        public CustomerAccountHisDetailQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = FinancialUIStrings.DataManagementView_Title_CustomerAccountHisDetail;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var name = filterItem.Filters.Single(r => r.MemberName == "CompanyName").Value as string;
                    var code = filterItem.Filters.Single(r => r.MemberName == "CompanyCode").Value as string;
                    var accountGroupId = filterItem.Filters.Single(r => r.MemberName == "AccountGroupId").Value as int?;
                    var businessType = filterItem.Filters.Single(e => e.MemberName == "BusinessType").Value as int?;
                    var sourceCode = filterItem.Filters.Single(r => r.MemberName == "SourceCode").Value as string;
                    var serialType = filterItem.Filters.Single(e => e.MemberName == "SerialType").Value as int?;
                    DateTime? begianDate = null;
                    DateTime? endDate = null;
                    DateTime? invoiceDateBegin = null;
                    DateTime? invoiceDateEnd = null;
                    foreach (var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)))
                    {
                        var dateTime = filter as CompositeFilterItem;
                        if (dateTime != null)
                        {
                            if (dateTime.Filters.First().MemberName == "Process")
                            {
                                begianDate = dateTime.Filters.First(r => r.MemberName == "Process").Value as DateTime?;
                                endDate = dateTime.Filters.Last(r => r.MemberName == "Process").Value as DateTime?;
                            }
                            if (dateTime.Filters.First().MemberName == "InvoiceDate")
                            {
                                invoiceDateBegin = dateTime.Filters.First(r => r.MemberName == "InvoiceDate").Value as DateTime?;
                                invoiceDateEnd = dateTime.Filters.Last(r => r.MemberName == "InvoiceDate").Value as DateTime?;
                            }
                        }
                    }

                    var ids = new int[] { };
                    if(this.DataGridView.SelectedEntities != null) {
                        var customerAccountHisDetails = this.DataGridView.SelectedEntities.Cast<VirtualCustomerAccountHisDetails>();
                        ids = customerAccountHisDetails.Select(r => r.ID).ToArray();
                    }
                    var businessCode = filterItem.Filters.Single(r => r.MemberName == "BusinessCode").Value as string;
                    ShellViewModel.Current.IsBusy = true;
                    this.ExportCustomerAccountForHisDetail(ids, accountGroupId, name, code, sourceCode, businessType, begianDate, endDate, businessCode, invoiceDateBegin, invoiceDateEnd, serialType);
                    break;
            }
        }
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private void ExportCustomerAccountForHisDetail(int[] ids, int? accountGroupId, string companyName, string companyCode, string sourceCode, int? businessType, DateTime? processDateBegin, DateTime? processDateEnd, string businessCode, DateTime? invoiceDateBegin, DateTime? invoiceDateEnd, int? serialType)
        {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportCustomerAccountForHisDetailAsync(ids, (int)DcsCompanyType.分公司, accountGroupId, BaseApp.Current.CurrentUserData.EnterpriseId, companyName, companyCode, sourceCode, businessType, processDateBegin, processDateEnd, businessCode, invoiceDateBegin, invoiceDateEnd, serialType);
            this.excelServiceClient.ExportCustomerAccountForHisDetailCompleted += excelServiceClient_ExportCustomerAccountForHisDetailCompleted;
            this.excelServiceClient.ExportCustomerAccountForHisDetailCompleted += excelServiceClient_ExportCustomerAccountForHisDetailCompleted;
        }

        private void excelServiceClient_ExportCustomerAccountForHisDetailCompleted(object sender, ExportCustomerAccountForHisDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var composite = filterItem as CompositeFilterItem;
            if(composite == null)
                return;
            ClientVar.ConvertTime(composite);
            this.DataGridView.FilterItem = composite;
            this.DataGridView.ExecuteQueryDelayed();
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();

                default:
                    return false;
            }
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "CustomerAccountHisDetail"
                };
            }
        }
    }
}
