﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Financial.Views {
    [PageMeta("PartsFinancialAndControls", "PartFunctions", "CustomerAccountHistory", ActionPanelKeys = new[] {
       CommonActionKeys.EXPORT})]
    public class CustomerAccountHistoryQuery : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        public DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("CustomerAccountHistory"));
            }
        }

        public CustomerAccountHistoryQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = FinancialUIStrings.DataManagementView_Title_CustomerAccountHistory;
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetCompaniesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效 && e.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    if(!loadOp.IsErrorHandled) {
                        loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowLoadError(loadOp);
                        return;
                    }
                var company = loadOp.Entities.SingleOrDefault();
                if(company != null)
                    currentCompanyType = company.Type;
            }, null);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        //获取当前登陆企业
        private int currentCompanyType;
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var newCompositeFilter = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                newCompositeFilter = filterItem as CompositeFilterItem;
                ClientVar.ConvertTime(newCompositeFilter);
            } else {
                newCompositeFilter.Filters.Add(filterItem);
            }
            if(currentCompanyType == (int)DcsCompanyType.分公司) {
                newCompositeFilter.Filters.Add(new FilterItem {
                    MemberName = "AccountGroup.SalesCompanyId",
                    MemberType = typeof(int),
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId,
                    Operator = FilterOperator.IsEqualTo
                });
            }
            if(currentCompanyType == (int)DcsCompanyType.代理库 || currentCompanyType == (int)DcsCompanyType.服务站兼代理库) {
                newCompositeFilter.Filters.Add(new FilterItem {
                    MemberName = "AccountGroup.SalesCompanyId",
                    MemberType = typeof(int),
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId,
                    Operator = FilterOperator.IsEqualTo
                });
                newCompositeFilter.Filters.Add(new FilterItem {
                    MemberName = "CustomerCompanyId",
                    MemberType = typeof(int),
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId,
                    Operator = FilterOperator.IsEqualTo
                });
            }
            if(currentCompanyType == (int)DcsCompanyType.服务站) {
                newCompositeFilter.Filters.Add(new FilterItem {
                    MemberName = "CustomerCompanyId",
                    MemberType = typeof(int),
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId,
                    Operator = FilterOperator.IsEqualTo
                });
            }
            this.DataGridView.FilterItem = newCompositeFilter;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "CustomerAccountHistory"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<CustomerAccountHistory>().Select(r => r.Id).ToArray();
                        this.ExportCustomerAccountHistory(ids, null, null, null, null, null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var companyName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Name") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Name").Value as string;
                        var companyCode = filterItem.Filters.SingleOrDefault(e => e.MemberName == "Code") == null ? null : filterItem.Filters.Single(e => e.MemberName == "Code").Value as string;
                        var accountGroupId = filterItem.Filters.SingleOrDefault(e => e.MemberName == "AccountGroupId") == null ? null : filterItem.Filters.Single(e => e.MemberName == "AccountGroupId").Value as int?;
                        var companyType = filterItem.Filters.SingleOrDefault(e => e.MemberName == "Type") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Type").Value as int?;
                        var dateTimeFilter = filterItem.Filters.SingleOrDefault(e => e.MemberName == "TheDate");
                        var dataArray = ClientVar.GetDateTimeArray();
                        DateTime? theDate = null;
                        if(dateTimeFilter != null && dateTimeFilter.Value != null) {
                            theDate = Convert.ToDateTime(dataArray[int.Parse(dateTimeFilter.Value.ToString())].Value);
                            theDate = theDate.Value.AddMonths(1);
                        }
                        ShellViewModel.Current.IsBusy = true;
                        this.ExportCustomerAccountHistory(new int[] { }, companyName, companyCode, companyType, accountGroupId, theDate);
                    }
                    break;
            }
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private void ExportCustomerAccountHistory(int[] ids, string companyName, string companyCode, int? companyType, int? accountGroupId, DateTime? theDate) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportCustomerAccountHistoryAsync(ids, currentCompanyType, accountGroupId, companyCode, companyName, companyType, theDate);
            this.excelServiceClient.ExportCustomerAccountHistoryCompleted -= ExcelServiceClientOnExportCustomerAccountHistoryCompleted;
            this.excelServiceClient.ExportCustomerAccountHistoryCompleted += ExcelServiceClientOnExportCustomerAccountHistoryCompleted;

        }

        private void ExcelServiceClientOnExportCustomerAccountHistoryCompleted(object sender, ExportCustomerAccountHistoryCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
    }
}
