﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web.Entities;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Financial.Views {
    [PageMeta("PartsFinancialAndControls", "PartFunctions", "CustomerAccount", ActionPanelKeys = new[] {
       CommonActionKeys.EXPORT})]
    public class CustomerAccountQuery : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        public DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("CustomerAccountAddRebateAccount"));
            }
        }

        public CustomerAccountQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = FinancialUIStrings.DataManagementView_Title_CustomerAccount;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }


        private int? paramAccountGroupId;
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);

            CompositeFilterItem compositeFilter;
            if(!(filterItem is CompositeFilterItem)) {
                compositeFilter = new CompositeFilterItem();
                compositeFilter.Filters.Add(filterItem);
            } else
                compositeFilter = filterItem as CompositeFilterItem;
            paramAccountGroupId = compositeFilter.Filters.SingleOrDefault(e => e.MemberName == "AccountGroupId") == null ? null : compositeFilter.Filters.Single(e => e.MemberName == "AccountGroupId").Value as int?;
            ClientVar.ConvertTime(compositeFilter);
            this.DataGridView.FilterItem = compositeFilter;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "CustomerAccount"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<VirtualCustomerAccount>().Select(r => r.Id).ToArray();
                        this.ExportCustomerAccount(ids, null, null, null, null, null, null, null, null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var companyName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "CompanyName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "CompanyName").Value as string;
                        var companyCode = filterItem.Filters.SingleOrDefault(e => e.MemberName == "CompanyCode") == null ? null : filterItem.Filters.Single(e => e.MemberName == "CompanyCode").Value as string;
                        var accountGroupId = paramAccountGroupId;
                        var status = filterItem.Filters.SingleOrDefault(e => e.MemberName == "Status") == null ? null : filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        var companyType = filterItem.Filters.SingleOrDefault(e => e.MemberName == "CompanyType") == null ? null : filterItem.Filters.Single(r => r.MemberName == "CompanyType").Value as int?;
                        var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        var businessCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "BusinessCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "BusinessCode").Value as string;
                        ShellViewModel.Current.IsBusy = true;
                        this.ExportCustomerAccount(null, companyName, companyCode, companyType, accountGroupId, status, createTimeBegin, createTimeEnd, businessCode);

                    }
                    break;
            }
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private void ExportCustomerAccount(int[] ids, string companyName, string companyCode, int? companyType, int? accountGroupId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, string businessCode) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportCustomerAccountAsync(ids, BaseApp.Current.CurrentUserData.EnterpriseId, companyName, companyCode, companyType, accountGroupId, status, createTimeBegin, createTimeEnd, businessCode);
            this.excelServiceClient.ExportCustomerAccountCompleted -= ExcelServiceClientOnExportCustomerAccountCompleted;
            this.excelServiceClient.ExportCustomerAccountCompleted += ExcelServiceClientOnExportCustomerAccountCompleted;

        }

        private void ExcelServiceClientOnExportCustomerAccountCompleted(object sender, ExportCustomerAccountCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
    }
}
