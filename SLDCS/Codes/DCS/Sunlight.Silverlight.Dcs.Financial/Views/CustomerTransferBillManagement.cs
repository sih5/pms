﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Financial.Views {
    [PageMeta("PartsFinancialAndControls", "PartFunctions", "CustomerTransferBill", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_APPROVE_BATCHAPPROVAL_EXPORT_IMPORT
    })]
    public class CustomerTransferBillManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditViewImport;
        private const string DATA_EDIT_VIEW_IMPORT = "_DataEditViewImport_";


        public CustomerTransferBillManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = FinancialUIStrings.DataManagementView_Title_CustomerTransferBill;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("CustomerTransferBill"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("CustomerTransferBill");
                    dataEditView.EditSubmitted += dataEditView_EditSubmitted;
                    dataEditView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewImport = null;
        }
        private void dataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void dataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }
        private DataEditViewBase DataEditViewImport {
            get {
                if(this.dataEditViewImport == null) {
                    this.dataEditViewImport = DI.GetDataEditView("CustomerTransferBillForImport");
                    this.dataEditViewImport.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.dataEditViewImport;
            }
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "CustomerTransferBill"
                };
            }
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_IMPORT, () => this.DataEditViewImport);
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                case CommonActionKeys.IMPORT:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.APPROVE:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<CustomerTransferBill>().Any(entity => entity.Status == (int)DcsWorkflowOfSimpleApprovalStatus.新建);
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.BATCHAPPROVAL:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() == 0)
                        return false;
                    var virtualPaymentBills = this.DataGridView.SelectedEntities.Cast<CustomerTransferBill>().ToArray();
                    int businessType = 0;

                    for(int i = 0; i < virtualPaymentBills.Count(); i++) {
                        if(virtualPaymentBills[i].Status != (int)DcsWorkflowOfSimpleApprovalStatus.新建) {
                            return false;
                        }
                        if(null == virtualPaymentBills[i].BusinessType) {
                            virtualPaymentBills[i].BusinessType = 0;
                        }
                        if(i == 0) {
                            businessType = virtualPaymentBills[i].BusinessType.Value;
                        } else {
                            if(businessType != virtualPaymentBills[i].BusinessType.Value) {
                                return false;
                            }
                        }
                    }
                    return true;              
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var customerTransferBill = this.DataEditView.CreateObjectToEdit<CustomerTransferBill>();
                    customerTransferBill.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    customerTransferBill.Status = (int)DcsWorkflowOfSimpleApprovalStatus.新建;
                    customerTransferBill.SalesCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.APPROVE:
                    DcsUtils.Confirm(FinancialUIStrings.DataManagementView_Confirm_Approve, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<CustomerTransferBill>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can审核客户转账单)
                                entity.审核客户转账单(DateTime.Now);
                            this.ExecuteSerivcesMethod(FinancialUIStrings.DataManagementView_Notification_ApproveSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.BATCHAPPROVAL:
                    DcsUtils.Confirm(FinancialUIStrings.DataManagementView_Confirm_BatchApproval, () => {
                        ShellViewModel.Current.IsBusy = true;
                        var ids = this.DataGridView.SelectedEntities.Cast<CustomerTransferBill>().Select(e => e.Id).ToArray();
                        var id = this.DataGridView.SelectedEntities.First().GetIdentity();
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(domainContext == null)
                            return;
                        domainContext.批量审核客户转账单(ids, loadOp => {
                            if(loadOp.HasError) {
                                if(!loadOp.IsErrorHandled)
                                    loadOp.MarkErrorAsHandled();
                                ShellViewModel.Current.IsBusy = false;
                                DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                                return;
                            }
                            this.CheckActionsCanExecute();
                            ShellViewModel.Current.IsBusy = false;
                            UIHelper.ShowNotification(FinancialUIStrings.DataManagementView_Notification_BatchApprovalSuccess);
                            this.DataGridView.ExecuteQueryDelayed();
                        }, null);
                    });
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(FinancialUIStrings.DataManagementView_Confirm_Abandon, () => this.DataGridView.UpdateSelectedEntities(entity => ((CustomerTransferBill)entity).Status = (int)DcsWorkflowOfSimpleApprovalStatus.作废, () => {
                        UIHelper.ShowNotification(FinancialUIStrings.DataManagementView_Notification_AbandonSuccess);
                        this.CheckActionsCanExecute();
                    }));
                    break;
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<CustomerTransferBill>().Select(e => e.Id).ToArray();
                        this.ExportCustomerTransferBill(ids, BaseApp.Current.CurrentUserData.EnterpriseId, null, null, null, null, null, null, null,null,null,null,null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var outboundCustomerCompanyName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "OutboundCustomerCompany.Name") == null ? null : filterItem.Filters.Single(r => r.MemberName == "OutboundCustomerCompany.Name").Value as string;
                        var outboundCustomerCompanyCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "OutboundCustomerCompany.Code") == null ? null : filterItem.Filters.Single(r => r.MemberName == "OutboundCustomerCompany.Code").Value as string;
                        var inboundCustomerCompanyName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "InboundCustomerCompany.Name") == null ? null : filterItem.Filters.Single(r => r.MemberName == "InboundCustomerCompany.Name").Value as string;
                        var inboundCustomerCompanyCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "InboundCustomerCompany.Code") == null ? null : filterItem.Filters.Single(r => r.MemberName == "InboundCustomerCompany.Code").Value as string;
                        var status = filterItem.Filters.SingleOrDefault(e => e.MemberName == "Status") == null ? null : filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        var businessType = filterItem.Filters.SingleOrDefault(e => e.MemberName == "BusinessType") == null ? null : filterItem.Filters.Single(e => e.MemberName == "BusinessType").Value as int?;
                        var code = filterItem.Filters.SingleOrDefault(e => e.MemberName == "Code") == null ? null : filterItem.Filters.Single(e => e.MemberName == "Code").Value as string;
                        
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        DateTime? invoiceDateBegin = null;
                        DateTime? invoiceDateEnd = null;
                        foreach (var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)))
                        {
                            var dateTime = filter as CompositeFilterItem;
                            if (dateTime != null)
                            {
                                if (dateTime.Filters.First().MemberName == "CreateTime")
                                {
                                    createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                                if (dateTime.Filters.First().MemberName == "InvoiceDate")
                                {
                                    invoiceDateBegin = dateTime.Filters.First(r => r.MemberName == "InvoiceDate").Value as DateTime?;
                                    invoiceDateEnd = dateTime.Filters.Last(r => r.MemberName == "InvoiceDate").Value as DateTime?;
                                }
                            }
                        }
                        this.ExportCustomerTransferBill(null, BaseApp.Current.CurrentUserData.EnterpriseId, outboundCustomerCompanyName, outboundCustomerCompanyCode, inboundCustomerCompanyName, inboundCustomerCompanyCode, status, createTimeBegin, createTimeEnd, invoiceDateBegin, invoiceDateEnd, businessType,code);
                    }
                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORT);
                    break;
            }
        }
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private void ExportCustomerTransferBill(int[] ids, int salesCompanyId, string outboundCustomerCompanyName, string outboundCustomerCompanyCode, string inboundCustomerCompanyName, string inboundCustomerCompanyCode, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? invoiceDateBegin, DateTime? invoiceDateEnd, int? businessType,string code)
        {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportCustomerTransferBillAsync(ids, salesCompanyId, outboundCustomerCompanyName, outboundCustomerCompanyCode, inboundCustomerCompanyName, inboundCustomerCompanyCode, status, createTimeBegin, createTimeEnd, invoiceDateBegin, invoiceDateEnd, businessType,code);
            this.excelServiceClient.ExportCustomerTransferBillCompleted -= ExcelServiceClientOnExportCustomerTransferBillCompleted;
            this.excelServiceClient.ExportCustomerTransferBillCompleted += ExcelServiceClientOnExportCustomerTransferBillCompleted;

        }

        private void ExcelServiceClientOnExportCustomerTransferBillCompleted(object sender, ExportCustomerTransferBillCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }


        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "SalesCompanyId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            if(filterItem != null) {
                var originalFilterItem = filterItem as CompositeFilterItem;

                if(originalFilterItem != null)
                    foreach(var filter in originalFilterItem.Filters)
                        compositeFilterItem.Filters.Add(filter);
                else
                    compositeFilterItem.Filters.Add(filterItem);

                if(originalFilterItem != null) {
                    var createTimeFilters = originalFilterItem.Filters.FirstOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    if(createTimeFilters == null) {
                        UIHelper.ShowNotification(FinancialUIStrings.DataManagementView_Validation_CustomerTransferBill_CreateTimeIsNull);
                        return;
                    }
                    var status = originalFilterItem.Filters.FirstOrDefault(r => r.MemberName == "Status");
                    if(status == null) {
                        UIHelper.ShowNotification(FinancialUIStrings.DataManagementView_Validation_CustomerTransferBill_StatusIsNull);
                        return;
                    }
                }
            }
            ClientVar.ConvertTime(compositeFilterItem);
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
