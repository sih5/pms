﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataEdit {
    public partial class AccountGroupDataEditView {
        public AccountGroupDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetAccountGroupByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    entity.CompanyName = entity.Company.Name;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        private void CreateUI() {
            this.LayoutRoot.Children.Add(DI.GetDataEditPanel("AccountGroup"));
        }

        protected override void OnEditSubmitting() {
            var accountGroup = this.DataContext as AccountGroup;
            if(accountGroup == null)
                return;
            accountGroup.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(accountGroup.Name))
                accountGroup.ValidationErrors.Add(new ValidationResult(FinancialUIStrings.DataEditView_Validation_AccountGroup_NameIsNull, new[] { "Name" }));
            if(accountGroup.HasValidationErrors)
                return;
            if(EditState == DataEditState.New)
                accountGroup.Code = GlobalVar.ASSIGNED_BY_SERVER;
            ((IEditableObject)accountGroup).EndEdit();
            try {
                if(EditState == DataEditState.New) {
                    if(accountGroup.Can生成账户组)
                        accountGroup.生成账户组();
                } else if(EditState == DataEditState.Edit) {
                    if(accountGroup.Can修改账户组)
                        accountGroup.修改账户组();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_AccountGroup;
            }
        }
    }
}
