﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataEdit
{
    public partial class AccountPeriodDataEditView 
    {
        private KeyValueManager keyValueManager;
        private readonly string[] kvNames = {
            "AccountPeriodYear","AccountPeriodMonth"
        };
        public AccountPeriodDataEditView()
        {
            InitializeComponent();        
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData();
        }
        protected KeyValueManager KeyValueManager
        {
            get
            {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        public object KvYear
        {
            get
            {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
        public object KvMonth {
            get
            {
                return this.KeyValueManager[this.kvNames[1]];
            }
        }
        protected override void OnEditSubmitting()
        {
            var accountPeriod = this.DataContext as AccountPeriod;
            if (accountPeriod == null)
            {
                return;
            }
            if ("".Equals(accountPeriod.Year))
            {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_AccountPeriod_PleaseChooseTheYear);
                return;
            }
            if ("".Equals(accountPeriod.Month))
            {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_AccountPeriod_PleaseChooseTheMonth);
                return;
            }
            ((IEditableObject)accountPeriod).EndEdit();
            try
            {
                if (accountPeriod.Can新增账期)
                    accountPeriod.新增账期();
            }
            catch (ValidationException ex)
            {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }
    }
}
