﻿using System;
using System.Windows;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Controls;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Financial.Panels.DataEdit;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataEdit {

    public partial class CredenceApplicationDataEditView : INotifyPropertyChanged {
        private readonly string[] kvNames = {
            "CredenceApplication_ApplyCondition","Credit_Type"
        };
        private KeyValueManager keyValueManager;
        public event PropertyChangedEventHandler PropertyChanged;
        private ObservableCollection<KeyValuePair> kvAccountGroups;
        private string customerCompanyName;

        private FileUploadForCredenceDataEditPanel fileUploadDataEditPanels;
        public FileUploadForCredenceDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (FileUploadForCredenceDataEditPanel)DI.GetDataEditPanel("FileUploadForCredence"));
            }
        }

        public CredenceApplicationDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(kvNames);
            this.KeyValueManager.LoadData();
            this.Loaded += this.CredenceApplicationDataEditView_Loaded;
            this.DataContextChanged += CredenceApplicationDataEditView_DataContextChanged;
            this.Initializer.Register(this.CreateUI);
        }

        private void CredenceApplicationDataEditView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var credenceApplication = this.DataContext as CredenceApplication;
            if(credenceApplication == null)
                return;
            credenceApplication.PropertyChanged -= credenceApplication_PropertyChanged;
            credenceApplication.PropertyChanged += credenceApplication_PropertyChanged;
            this.GetQuota();
        }

        private void credenceApplication_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var credenceApplication = this.DataContext as CredenceApplication;
            if(credenceApplication == null)
                return;
            switch(e.PropertyName) {
                case "AccountGroupId":
                case "CustomerCompanyId":
                    this.GetQuota();
                    break;

            }
        }

        protected KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_CredenceApplication;
            }
        }

        public object KvApplyCondition {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
        public object KvCreditTypes {
            get {
                return this.KeyValueManager[this.kvNames[1]];
            }
        }

        public string CustomerCompanyName {
            get {
                return this.customerCompanyName;
            }
            set {
                this.customerCompanyName = value;
                this.OnPropertyChanged("CustomerCompanyName");
            }
        }

        private decimal quota;
        public decimal Quota {
            get {
                return this.quota;
            }
            set {
                this.quota = value;
                this.OnPropertyChanged("Quota");
            }
        }



        public void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public ObservableCollection<KeyValuePair> KvAccountGroups {
            get {
                return this.kvAccountGroups ?? (this.kvAccountGroups = new ObservableCollection<KeyValuePair>());
            }
        }

        private void CredenceApplicationDataEditView_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            this.DomainContext.Load(this.DomainContext.GetAccountGroupsQuery().Where(v => v.SalesCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && v.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvAccountGroups.Clear();
                foreach(var accountGroup in loadOp.Entities) {
                    this.KvAccountGroups.Add(new KeyValuePair {
                        Key = accountGroup.Id,
                        Value = accountGroup.Name,
                        UserObject = accountGroup
                    });
                }
                this.txtAccountGroup.SelectedIndex = 0;//默认账户组
            }, null);
            this.CustomerCompanyName = "";
            this.RadMaskedTextBox1.Value = null;
        }

        protected override void OnEditSubmitting() {
            var credenceApplication = this.DataContext as CredenceApplication;
            if(credenceApplication == null)
                return;
            credenceApplication.Path = FileUploadDataEditPanels.FilePath;
            credenceApplication.ValidationErrors.Clear();
            if(credenceApplication.SalesCompanyId == default(int)) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_CredenceApplication_SalesCompanyIsNull);
                return;
            }
            if(credenceApplication.CreditType == null) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_CredenceApplication_ChooseCreditType);
                return;
            }
            if(credenceApplication.ExpireDate == default(DateTime)) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_CredenceApplication_ExpireDateIsNull);
                return;
            }
            if(credenceApplication.AccountGroupId == default(int))
                credenceApplication.ValidationErrors.Add(new ValidationResult(FinancialUIStrings.DataEditView_Validation_CredenceApplication_AccountGroupIsNull, new[] {
                    "AccountGroupId"
                }));
            if(credenceApplication.CustomerCompanyId == default(int)) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_CredenceApplication_CustomerCompanyIsNull);
                return;
            }

            //给实体的信用额度赋值
            if(this.RadMaskedTextBox1.Value == null)
                credenceApplication.CredenceLimit = default(decimal);
            //if(credenceApplication.CredenceLimit == default(int))
            //    credenceApplication.ValidationErrors.Add(new ValidationResult(FinancialUIStrings.DataEditView_Validation_CredenceApplication_CredenceLimitIsNull, new[] {
            //        "CredenceLimit"
            //    }));
            if(credenceApplication.HasValidationErrors)
                return;
            if(EditState == DataEditState.New)
                credenceApplication.Code = GlobalVar.ASSIGNED_BY_SERVER;
            ((IEditableObject)credenceApplication).EndEdit();
            try {
                if(EditState == DataEditState.Edit) {
                    if(credenceApplication.Can修改信用申请单)
                        credenceApplication.修改信用申请单();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetCredenceApplicationByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(r => r.Id == entity.CustomerCompanyId), LoadBehavior.RefreshCurrent, loadOp1 => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    var item = loadOp1.Entities.SingleOrDefault();
                    if(item != null)
                        this.CustomerCompanyName = item.Name;
                }, null);
                if(entity != null) {
                    FileUploadDataEditPanels.FilePath = entity.Path;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        private void CreateUI() {
            var queryWindow = DI.GetQueryWindow("CustomerInformationBySparepart");
            queryWindow.SelectionDecided += this.CustomerInfomationBySparepartQueryWindow_SelectionDecided;
            this.popupTextBoxUsedPartsCustomerInformationBySparepart.PopupContent = queryWindow;

            FileUploadDataEditPanels.FilePath = null;
            FileUploadDataEditPanels.SetValue(Grid.RowProperty, 8);
            FileUploadDataEditPanels.SetValue(Grid.ColumnProperty, 1);
            FileUploadDataEditPanels.SetValue(Grid.ColumnSpanProperty, 4);
            this.LayoutRoot.Children.Add(FileUploadDataEditPanels);
        }

        private void CustomerInfomationBySparepartQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var customerInformation = queryWindow.SelectedEntities.Cast<CustomerInformation>().FirstOrDefault();
            if(customerInformation == null)
                return;
            var credenceApplication = this.DataContext as CredenceApplication;
            if(credenceApplication == null)
                return;
            credenceApplication.CustomerCompanyId = customerInformation.CustomerCompany.Id;
            this.CustomerCompanyName = customerInformation.CustomerCompany.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void GetQuota() {
            var credenceApplication = this.DataContext as CredenceApplication;
            if(credenceApplication == null)
                return;
            if(credenceApplication.AccountGroupId == 0 || credenceApplication.CustomerCompanyId == 0)
                return;
            this.DomainContext.Load(DomainContext.GetCustomerAccountsQuery().Where(r => r.AccountGroupId == credenceApplication.AccountGroupId && r.CustomerCompanyId == credenceApplication.CustomerCompanyId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.Quota = entity.CustomerCredenceAmount;
            }, null);
        }

        private void radMaskedTextBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e) {
            var credenceApplication = this.DataContext as CredenceApplication;
            if(credenceApplication == null || RadMaskedTextBox1.Value == null)
                return;
            if((e.PlatformKeyCode == 8 || e.PlatformKeyCode == 46) && credenceApplication.CredenceLimit == default(decimal)) {
                RadMaskedTextBox1.SelectionStart = 0;
                RadMaskedTextBox1.Value = null;
            }
        }

        protected override void Reset() {
            var credenceApplication = this.DataContext as CredenceApplication;
            if(credenceApplication == null)
                return;
            if(this.DomainContext.CredenceApplications.Contains(credenceApplication)) {
                this.DomainContext.CredenceApplications.Detach(credenceApplication);
            }
        }

        protected override void OnEditCancelled() {
            this.DataContext = null;
            // this.Reset();
            base.OnEditCancelled();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        private void CreditType_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            var credenceApplication = this.DataContext as CredenceApplication;
            if(credenceApplication == null)
                return;
            if(credenceApplication.CreditType == (int)DcsCredit_Type.临时授信)
                credenceApplication.ExpireDate = DateTime.Now.AddDays(2);
            else
                credenceApplication.ExpireDate = new DateTime(DateTime.Now.Year, 12, 31);

        }
    }
}