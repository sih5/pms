﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Dcs.Financial.Panels.DataEdit;
namespace Sunlight.Silverlight.Dcs.Financial.Views.DataEdit {
    public partial class CredenceApplicationForFirApproveDataEditView : INotifyPropertyChanged {
        private readonly string[] kvNames = {
            "CredenceApplication_ApplyCondition","Credit_Type"
        };
        private KeyValueManager keyValueManager;
        private ButtonItem rejectBtn;
        private string customerCompanyName;
        public event PropertyChangedEventHandler PropertyChanged;
        private ObservableCollection<KeyValuePair> kvAccountGroups;

        private FileUploadForCredenceDataEditPanel fileUploadDataEditPanels;
        public FileUploadForCredenceDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (FileUploadForCredenceDataEditPanel)DI.GetDataEditPanel("FileUploadForCredence"));
            }
        }

        public CredenceApplicationForFirApproveDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(kvNames);
            this.KeyValueManager.LoadData();
            this.Initializer.Register(this.CreateUI);
            this.Loaded += this.CredenceApplicationDataEditView_Loaded;
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        public object KvCreditTypes {
            get {
                return this.KeyValueManager[this.kvNames[1]];
            }
        }
        public ObservableCollection<KeyValuePair> KvAccountGroups {
            get {
                return this.kvAccountGroups ?? (this.kvAccountGroups = new ObservableCollection<KeyValuePair>());
            }
        }

        private void CredenceApplicationDataEditView_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            this.DomainContext.Load(this.DomainContext.GetAccountGroupsQuery().Where(v => v.SalesCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && v.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvAccountGroups.Clear();
                foreach(var accountGroup in loadOp.Entities) {
                    this.KvAccountGroups.Add(new KeyValuePair {
                        Key = accountGroup.Id,
                        Value = accountGroup.Name,
                        UserObject = accountGroup
                    });
                }
            }, null);
            //this.CustomerCompanyName = "";
            //this.RadMaskedTextBox1.Value = null;
        }
        private void CreateUI() {
            this.rejectBtn = new ButtonItem {
                Command = new DelegateCommand(this.RejecrCurrentData),
                Title = FinancialUIStrings.Action_Title_Reject,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative)
            };
            this.RegisterButton(rejectBtn);

            FileUploadDataEditPanels.FilePath = null;
            FileUploadDataEditPanels.SetValue(Grid.RowProperty, 8);
            FileUploadDataEditPanels.SetValue(Grid.ColumnProperty, 1);
            FileUploadDataEditPanels.SetValue(Grid.ColumnSpanProperty, 4);
            FileUploadDataEditPanels.isHiddenButtons = true;
            this.LayoutRoot.Children.Add(FileUploadDataEditPanels);
        }
        protected override string Title {
            get {
                var credenceApplication = this.DataContext as CredenceApplication;
                if(credenceApplication == null || (credenceApplication.Status == (int)DcsCredenceApplicationStatus.已提交 && credenceApplication.CreditType == (int)DcsCredit_Type.临时授信)) {
                    return FinancialUIStrings.DataEditView_Title_CredenceApplicationFirst;
                } else {
                    return "审核信用单";
                }
            }
        }

        private decimal quota;
        public decimal Quota {
            get {
                return this.quota;
            }
            set {
                this.quota = value;
                this.OnPropertyChanged("Quota");
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetCredenceApplicationByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                this.DomainContext.Load(this.DomainContext.getCurrentCustomerAccountQuery(entity.Id), LoadBehavior.RefreshCurrent, loadOp4 => {
                    if(loadOp4.HasError) {
                        if(!loadOp4.IsErrorHandled)
                            loadOp4.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp4);
                        return;
                    }

                    if(loadOp4 != null && loadOp4.Entities.Count() > 0) {
                        var entity2 = loadOp4.Entities.First();
                        if(entity2.SIHCredit.HasValue) {
                            this.SIHCredit.Text = entity2.SIHCredit.ToString();
                            this.SIH.Text = entity2.SIHCredit.ToString();
                        }
                        if(entity2.ZcLimit.HasValue) {
                            this.ZcLimit.Text = entity2.ZcLimit.ToString();
                        }
                        if(entity2.LsLimit.HasValue)
                            this.LsLimit.Text = entity2.LsLimit.ToString();
                        if(entity2.ZsNow.HasValue)
                            this.ZsNow.Text = entity2.ZsNow.ToString();
                        if(entity2.LsNow.HasValue)
                            this.LsNow.Text = entity2.LsNow.ToString();
                        if(entity2.HjDif.HasValue)
                            this.HjDif.Text = entity2.HjDif.ToString();
                        if(entity2.HjNow.HasValue)
                            this.HjNow.Text = entity2.HjNow.ToString();
                        if(entity2.HjLimit.HasValue)
                            this.HjLimit.Text = entity2.HjLimit.ToString();
                        if(entity2.DifZc.HasValue)
                            this.DifZc.Text = entity2.DifZc.ToString();

                        this.DifLs.Text = ((entity2.LsLimit ?? 0) + (entity2.LsNow ?? 0)).ToString();
                    }
                }, null);
                this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(r => r.Id == entity.CustomerCompanyId), LoadBehavior.RefreshCurrent, loadOp1 => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    var item = loadOp1.Entities.SingleOrDefault();
                    if(item != null)
                        this.CustomerCompanyName = item.Name;
                }, null);
                if(entity.AccountGroupId == 0 || entity.CustomerCompanyId == 0)
                    return;
                this.DomainContext.Load(DomainContext.GetCustomerAccountsQuery().Where(r => r.AccountGroupId == entity.AccountGroupId && r.CustomerCompanyId == entity.CustomerCompanyId), LoadBehavior.RefreshCurrent, loadOp1 => {
                    if(loadOp1.HasError) {
                        if(!loadOp1.IsErrorHandled)
                            loadOp1.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp1);
                        return;
                    }
                    var customerAccounts = loadOp1.Entities.SingleOrDefault();
                    if(customerAccounts != null)
                        this.Quota = customerAccounts.CustomerCredenceAmount;
                }, null);
                if(entity != null) {
                    FileUploadDataEditPanels.FilePath = entity.Path;
                    this.SetObjectToEdit(entity);
                }
            }, null);

        }
        protected override void OnEditSubmitting() {

            var credenceApplication = this.DataContext as CredenceApplication;
            if(credenceApplication == null)
                return;
            try {
                ((IEditableObject)credenceApplication).EndEdit();
                if(credenceApplication.Status == (int)DcsCredenceApplicationStatus.已提交 && credenceApplication.CreditType == (int)DcsCredit_Type.临时授信) {
                    if(credenceApplication.Can初审信用申请单)
                        credenceApplication.初审信用申请单();
                    ExecuteSerivcesMethod("操作成功");
                } else if(credenceApplication.Status == (int)DcsCredenceApplicationStatus.初审通过 || (credenceApplication.Status == (int)DcsCredenceApplicationStatus.已提交 && credenceApplication.CreditType == (int)DcsCredit_Type.正常授信)) {
                    if(credenceApplication.Can审核信用申请单) {
                        credenceApplication.审核信用申请单();
                        ExecuteSerivcesMethod("操作成功");
                    }
                }

            } catch(Exception ex) {
                UIHelper.ShowAlertMessage(ex.Message);
            }
        }
        private void ExecuteSerivcesMethod(string notifyMessage) {
            DomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    DomainContext.RejectChanges();
                    return;
                }
                this.NotifyEditSubmitted();
                UIHelper.ShowNotification(notifyMessage);
            }, null);
        }
        public void OnCustomEditSubmitted() {
            this.DataContext = null;
        }

        public new event EventHandler EditSubmitted;
        private void NotifyEditSubmitted() {
            var handler = this.EditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }
        //驳回
        private void RejecrCurrentData() {
            var credenceApplication = this.DataContext as CredenceApplication;
            if(credenceApplication == null)
                return;
            ((IEditableObject)credenceApplication).EndEdit();
            try {
                if(credenceApplication.Can驳回信用申请单)
                    credenceApplication.驳回信用申请单();
                ExecuteSerivcesMethod("驳回成功");
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
        }
        public string CustomerCompanyName {
            get {
                return this.customerCompanyName;
            }
            set {
                this.customerCompanyName = value;
                this.OnPropertyChanged("CustomerCompanyName");
            }
        }
        public void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        protected override bool OnRequestCanSubmit() {
            return true;
        }
    }
}