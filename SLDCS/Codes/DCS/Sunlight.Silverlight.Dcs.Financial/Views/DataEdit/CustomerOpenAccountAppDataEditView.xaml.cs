﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataEdit {
    public partial class CustomerOpenAccountAppDataEditView : INotifyPropertyChanged {
        private string customerCompanyName;

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private ObservableCollection<KeyValuePair> kvAccountGroups;
        public ObservableCollection<KeyValuePair> KvAccountGroups {
            get {
                return this.kvAccountGroups ?? (this.kvAccountGroups = new ObservableCollection<KeyValuePair>());
            }
        }

        public string CustomerCompanyName {
            get {
                return this.customerCompanyName;
            }
            set {
                this.customerCompanyName = value;
                this.OnPropertyChanged("CustomerCompanyName");
            }
        }

        public CustomerOpenAccountAppDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.Loaded += this.CustomerOpenAccountAppDataEditView_Loaded;
        }

        private void CustomerOpenAccountAppDataEditView_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            if(EditState == DataEditState.New) {
                this.accountInitialDeposittxt.Value = null;
            }
        }

        private void responsibleUnitQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var customerInformation = queryWindow.SelectedEntities.Cast<CustomerInformation>().FirstOrDefault();
            if(customerInformation == null)
                return;
            var customerOpenAccountApp = this.DataContext as CustomerOpenAccountApp;
            if(customerOpenAccountApp == null)
                return;

            customerOpenAccountApp.CustomerCompanyId = customerInformation.CustomerCompany.Id;
            this.CustomerCompanyName = customerInformation.CustomerCompany.Name;

            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_CustomerOpenAccountApp;
            }
        }

        private void CreateUI() {
            DomainContext.Load(DomainContext.GetAccountGroupsQuery().Where(v => v.Status == (int)DcsMasterDataStatus.有效 && v.SalesCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvAccountGroups.Clear();
                foreach(var accountGroup in loadOp.Entities) {
                    this.KvAccountGroups.Add(new KeyValuePair {
                        Key = accountGroup.Id,
                        Value = accountGroup.Name,
                        UserObject = accountGroup
                    });
                }
                if(this.EditState == DataEditState.Edit)
                    return;
                var customerOpenAccountApp = this.DataContext as CustomerOpenAccountApp;
                if(customerOpenAccountApp == null)
                    return;
                if(KvAccountGroups.Count == 1) {
                    customerOpenAccountApp.AccountGroupId = loadOp.Entities.First().Id;
                }
            }, null);
            var queryWindow = DI.GetQueryWindow("CustomerInformationBySparepart");
            queryWindow.SelectionDecided += this.responsibleUnitQueryWindow_SelectionDecided;
            this.popupTextBoxCustomerCompany.PopupContent = queryWindow;
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetCustomerOpenAccountAppsWithCustomerCompanyQuery().Where(r => r.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();

                if(entity != null) {
                    this.SetObjectToEdit(entity);
                    this.CustomerCompanyName = entity.Company.Name;
                }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void OnEditSubmitting() {
            var customerOpenAccountApp = this.DataContext as CustomerOpenAccountApp;
            if(customerOpenAccountApp == null)
                return;
            customerOpenAccountApp.ValidationErrors.Clear();

            if(customerOpenAccountApp.CustomerCompanyId == default(int)) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_CredenceApplication_CustomerCompanyIsNull);
                return;
            }
            if(customerOpenAccountApp.AccountGroupId == default(int)) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_CredenceApplication_AccountGroupIsNull);
                return;
            }
            if(customerOpenAccountApp.AccountInitialDeposit < 0) {
                customerOpenAccountApp.ValidationErrors.Add(new ValidationResult(FinancialUIStrings.DataEditView_Validation_CustomerOpenAccountApp_AccountInitialDepositIsNotValid, new[] {
                    "AccountInitialDeposit"
                }));
                return;
            }
            if(customerOpenAccountApp.ValidationErrors.Any())
                return;

            ((IEditableObject)customerOpenAccountApp).EndEdit();
            base.OnEditSubmitting();
        }

        private void radMaskedTextBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e) {
            var customerOpenAccountApp = this.DataContext as CustomerOpenAccountApp;
            if(customerOpenAccountApp == null)
                return;
            if((e.PlatformKeyCode == 8 || e.PlatformKeyCode == 46) && customerOpenAccountApp.AccountInitialDeposit == default(decimal)) {
                accountInitialDeposittxt.SelectionStart = 0;
            }
        }

        protected override void OnEditCancelled() {
            this.CustomerCompanyName = string.Empty;
            this.DataContext = null;
            base.OnEditCancelled();
        }

        protected override void OnEditSubmitted() {
            this.CustomerCompanyName = string.Empty;
            base.OnEditSubmitted();
        }
    }
}
