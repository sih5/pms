﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataEdit {
    public partial class CustomerTransferBillDataEditView : INotifyPropertyChanged
    {
        private readonly string[] kvNames = {
            "CustomerTransferBill_BusinessType"
        };
        private KeyValueManager keyValueManager;
        private ObservableCollection<KeyValuePair> kvAccountGroups;
        private ObservableCollection<Summary> kvSummaries;
        private DateTime? invoiceDate ;
        public CustomerTransferBillDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(kvNames);
            this.KeyValueManager.LoadData();
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_CustomerTransferBill;
            }
        }

        public ObservableCollection<KeyValuePair> KvAccountGroups {
            get {
                return this.kvAccountGroups ?? (this.kvAccountGroups = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<Summary> KvSummaries {
            get {
                return this.kvSummaries ?? (this.kvSummaries = new ObservableCollection<Summary>());
            }
        }
        public object KvBusinessTypes {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
        protected KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        private void CreateUI() {
            var queryWindow = DI.GetQueryWindow("CustomerInformationBySparepartForTransfer");
            queryWindow.SelectionDecided += this.QueryWindow_SelectionDecided;
            this.popupTextBoxOutboundCustomerCompanyName.PopupContent = queryWindow;

            var customerInformationqueryWindow = DI.GetQueryWindow("CustomerInformationBySparepartForTransfer");
            customerInformationqueryWindow.SelectionDecided += this.CustomerInformationqueryWindowForInbound_SelectionDecided;
            this.popupTextBoxOutboundInboundCustomerCompanyName.PopupContent = customerInformationqueryWindow;
            RadMaskedTextBox1.SelectionStart = 0;
            RadMaskedTextBox1.Text = "";
            this.Loaded += CustomerTransferBillDataEditView_Loaded;
        }

        private void CustomerTransferBillDataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.DomainContext.Load(this.DomainContext.GetAccountGroupsQuery().Where(r => r.SalesCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvAccountGroups.Clear();
                foreach(var entity in loadOp.Entities) {
                    this.KvAccountGroups.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name
                    });
                }
                //不能把DataContext的是否为空判断放在前面，编辑时，如果在DomainContext首次初始化，此处的DataContext为空会跳过查询
                if(this.EditState == DataEditState.Edit)
                    return;
                var customerTransferBill = this.DataContext as CustomerTransferBill;
                if(customerTransferBill == null)
                    return;
                //如果只有一条则默认
                if(KvAccountGroups.Count == 1) {
                    customerTransferBill.OutboundAccountGroupId = loadOp.Entities.First().Id;
                    customerTransferBill.InboundAccountGroupId = loadOp.Entities.First().Id;
                }
            }, null);
            this.DomainContext.Load(this.DomainContext.GetSummariesQuery().Where(r => r.OwnerCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && r.Purpose == (int)DcsSummaryPurpose.收款), LoadBehavior.RefreshCurrent, loadOption => {
                if(loadOption.HasError)
                    return;
                foreach(var entity in loadOption.Entities)
                    if(!KvSummaries.Contains(entity))
                        this.KvSummaries.Add(entity);
            }, null);
        }

        protected override void OnEditSubmitting() {
            var customerTransferBill = this.DataContext as CustomerTransferBill;
            if(customerTransferBill == null)
                return;
            if (this.InvoiceDate.HasValue)
            {
                this.InvoiceDate = new DateTime(this.InvoiceDate.Value.Year, this.InvoiceDate.Value.Month, this.InvoiceDate.Value.Day, 00, 00, 00);
                customerTransferBill.InvoiceDate = this.InvoiceDate.Value;
            } else if(!customerTransferBill.InvoiceDate.HasValue)
            {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_CustomerTransferBill_ChooseInvoiceDate);
                return;
            }
            if(customerTransferBill.BusinessType == null) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_CustomerTransferBill_ChooseBusinessType);
                return;
            }
            customerTransferBill.ValidationErrors.Clear();
            if(customerTransferBill.OutboundCustomerCompanyId == default(int)) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_CustomerTransferBill_OutboundCustomerCompanyIdIsNull);
                return;
            }
            if(customerTransferBill.InboundCustomerCompanyId == default(int)) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_CustomerTransferBill_InboundCustomerCompanyIdIsNull);
                return;
            }
            if(customerTransferBill.OutboundAccountGroupId == default(int)) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_CustomerTransferBill_OutboundAccountGroupIdIsNull);
                return;
            }
            if(customerTransferBill.InboundAccountGroupId == default(int)) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_CustomerTransferBill_InboundAccountGroupIdIsNull);
                return;
            }
            if(customerTransferBill.Amount <= 0) {
                customerTransferBill.ValidationErrors.Add(new ValidationResult(FinancialUIStrings.DataEditView_Validation_CustomerTransferBill_AmountIsNotValid, new[] { "Amount" }));
                return;
            }
            if(string.IsNullOrEmpty(customerTransferBill.Summary))
                customerTransferBill.ValidationErrors.Add(new ValidationResult(FinancialUIStrings.DataEditView_Validation_CustomerTransferBill_SummaryIsNull, new[] { "Summary" }));
            if(customerTransferBill.ValidationErrors.Any())
                return;
            base.OnEditSubmitting();
        }
        public DateTime? InvoiceDate
        {
            get
            {
                return this.invoiceDate;
            }
            set
            {
                this.invoiceDate = value;
                this.OnPropertyChanged("InvoiceDate");
            }
        }
        
        private void QueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null || !queryWindow.SelectedEntities.Any())
                return;

            var customerInformation = queryWindow.SelectedEntities.Cast<VirtualCustomerInformationForTransfer>().FirstOrDefault();
            if(customerInformation == null)
                return;
            var customerTransferBill = this.DataContext as CustomerTransferBill;
            if(customerTransferBill == null)
                return;
            this.DomainContext.Load(this.DomainContext.GetCompanyByIdQuery((int)customerInformation.CustomerCompanyId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity != null) {
                    customerTransferBill.OutboundCustomerCompany = entity;
                    customerTransferBill.OutboundCustomerCompanyId = entity == null ? default(int) : entity.Id;
                }
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }, null);
        }

        private void CustomerInformationqueryWindowForInbound_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null || !queryWindow.SelectedEntities.Any())
                return;

            var customerInformation = queryWindow.SelectedEntities.Cast<VirtualCustomerInformationForTransfer>().FirstOrDefault();
            if(customerInformation == null)
                return;
            var customerTransferBill = this.DataContext as CustomerTransferBill;
            if(customerTransferBill == null)
                return;
            this.DomainContext.Load(this.DomainContext.GetCompanyByIdQuery((int)customerInformation.CustomerCompanyId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity != null) {
                    customerTransferBill.InboundCustomerCompany = entity;
                    customerTransferBill.InboundCustomerCompanyId = entity == null ? default(int) : entity.Id;
                }
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }, null);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetCustomerTransferBillWithDetailsBtIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();

                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void radMaskedTextBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e) {
            var customerTransferBill = this.DataContext as CustomerTransferBill;
            if(customerTransferBill == null || RadMaskedTextBox1.Value == null)
                return;
            if((e.PlatformKeyCode == 8 || e.PlatformKeyCode == 46) && customerTransferBill.Amount == default(decimal)) {
                RadMaskedTextBox1.SelectionStart = 0;
                RadMaskedTextBox1.Value = null;
            }
        }

        protected override void OnEditCancelled() {
            this.DataContext = null;
            base.OnEditCancelled();
        }
    }
}
