﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataEdit {
    public partial class CustomerTransferBillForImportDataEditView  {
        private DataGridViewBase credenceApplicationForImportDataGridView;
        private const string EXPORT_DATA_FILE_NAME = "导出客户转账单模板.xlsx";
        private ICommand exportFileCommand;
        public CustomerTransferBillForImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
        protected override string Title {
            get {
                return FinancialUIStrings.DataEditView_Title_Import_CustomerTransferBill;
            }
        }
        protected DataGridViewBase ImportDataGridView {
            get {
                if(this.credenceApplicationForImportDataGridView == null) {
                    this.credenceApplicationForImportDataGridView = DI.GetDataGridView("CustomerTransferBillForImport");
                }
                return this.credenceApplicationForImportDataGridView;
            }
        }
        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }
        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = FinancialUIStrings.DataEditView_Title_Import_CustomerTransferBillDetail,
                Content = this.ImportDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.LayoutRoot.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.ImportCustomerTransferBillAsync(fileName);
            this.ExcelServiceClient.ImportCustomerTransferBillCompleted -= ExcelServiceClientOnImportCredenceApplicationCompleted;
            this.ExcelServiceClient.ImportCustomerTransferBillCompleted += ExcelServiceClientOnImportCredenceApplicationCompleted;
        }

        private void ExcelServiceClientOnImportCredenceApplicationCompleted(object sender, ImportCustomerTransferBillCompletedEventArgs e) {
            var dataEditView = this.DataContext as CustomerTransferBillForImportDataEditView;
            if(dataEditView == null)
                return;
           
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!this.HasImportingError) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Notification_CompletedImport, 5);
            } else {
                if(!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if(!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                 Name= FinancialUIStrings.DataGridView_ColumnItem_Title_BusinessType,
                                                IsRequired = true
                                            },new ImportTemplateColumn {
                                                Name = FinancialUIStrings.DataGridView_ColumnItem_Title_OutboundAccountGroup_Name,
                                                IsRequired = true
                                            },new ImportTemplateColumn {
                                                Name = FinancialUIStrings.DataGridView_ColumnItem_Title_OutboundCustomerCompany_Code,
                                                IsRequired = true
                                            },new ImportTemplateColumn {
                                                Name = FinancialUIStrings.DataGridView_ColumnItem_Title_OutboundCustomerCompany_Name
                                            },new ImportTemplateColumn {
                                                Name = FinancialUIStrings.DataEditPanel_Text_CustomerTransferBill_InboundAccountGroupName,
                                                IsRequired = true
                                            },new ImportTemplateColumn {
                                                Name = FinancialUIStrings.DataGridView_ColumnItem_Title_InboundCustomerCompany_Code,
                                                IsRequired = true
                                            },new ImportTemplateColumn {
                                                Name = FinancialUIStrings.DataGridView_ColumnItem_Title_InboundCustomerCompany_Name
                                            },new ImportTemplateColumn {
                                                Name = FinancialUIStrings.DataGridView_ColumnItem_Title_Amount,
                                                IsRequired = true
                                            },new ImportTemplateColumn {
                                                Name = FinancialUIStrings.DataGridView_ColumnItem_Title_Abstract,
                                                IsRequired = true
                                            },new ImportTemplateColumn {
                                                Name = FinancialUIStrings.DataEditPanel_Text_CustomerTransferBill_InvoiceDate,
                                                IsRequired = true
                                            },new ImportTemplateColumn {
                                                Name = FinancialUIStrings.DataGridView_ColumnItem_Title_Operator
                                            },new ImportTemplateColumn {
                                                Name = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_Remark,
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }


    }
}
