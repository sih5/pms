﻿
using System;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataEdit {
    public partial class FinancialSnapshotSetDataEditView {
        public FinancialSnapshotSetDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_FinancialSnapshotSet;
            }
        }
        private void CreateUI() {
            this.LayoutRoot.Children.Add(DI.GetDataEditPanel("FinancialSnapshotSet"));
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetFinancialSnapshotSetByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
        protected override void OnEditSubmitting() {
            var financialSnapshotSet = this.DataContext as FinancialSnapshotSet;
            if(financialSnapshotSet == null)
                return;

            financialSnapshotSet.ValidationErrors.Clear();
            if(financialSnapshotSet.SnapshotTime == null) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_FinancialSnapshotSet_SnapshotTime);
                return;
            }
            //if(financialSnapshotSet.PartsSalesCategoryName == null) {
            //    UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_FinancialSnapshotSet_PartsSalesCategoryId);
            //    return;
            //}
            financialSnapshotSet.SnapshotTime = new DateTime(financialSnapshotSet.SnapshotTime.Value.Year, financialSnapshotSet.SnapshotTime.Value.Month, financialSnapshotSet.SnapshotTime.Value.Day, 23, 59, 59);
            if(financialSnapshotSet.SnapshotTime < System.DateTime.Now) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_SnapshotTimeLessCurrentTime);
                return;
            }
            ((IEditableObject)financialSnapshotSet).EndEdit();
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
