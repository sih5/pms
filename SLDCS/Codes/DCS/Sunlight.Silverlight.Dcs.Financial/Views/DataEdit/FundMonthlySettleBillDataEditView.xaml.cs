﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataEdit
{
    public partial class FundMonthlySettleBillDataEditView 
    {
        private KeyValueManager keyValueManager;
        private readonly string[] kvNames = {
            "AccountPeriodYear","AccountPeriodMonth"
        };
        public FundMonthlySettleBillDataEditView()
        {
            InitializeComponent();        
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData();
        }
        protected KeyValueManager KeyValueManager
        {
            get
            {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        public object KvYear
        {
            get
            {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
        public object KvMonth
        {
            get
            {
                return this.KeyValueManager[this.kvNames[1]];
            }
        }
        protected override void OnEditSubmitting()
        {
            var fundMonthlySettleBill = this.DataContext as FundMonthlySettleBill;
            if (fundMonthlySettleBill == null)
            {
                return;
            }
            if ("".Equals(fundMonthlySettleBill.MonthlySettleYear))
            {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_ChooseMonthlySettleYear);
                return;
            }
            if ("".Equals(fundMonthlySettleBill.MonthlySettleMonth))
            {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_ChooseMonthlySettleMonth);
                return;
            }
            if ("2018".Equals(fundMonthlySettleBill.MonthlySettleYear) && int.Parse(fundMonthlySettleBill.MonthlySettleMonth) < 10)
            {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_MonthlySettleDateNotLessCertainDate);
                return;
            }
            ((IEditableObject)fundMonthlySettleBill).EndEdit();
            try
            {
                if (fundMonthlySettleBill.Can新增资金月结)
                    fundMonthlySettleBill.新增资金月结();
            }
            catch (ValidationException ex)
            {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }
    }
}
