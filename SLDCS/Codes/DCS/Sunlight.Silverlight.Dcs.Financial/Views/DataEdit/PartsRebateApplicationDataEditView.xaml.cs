﻿
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataEdit {
    public partial class PartsRebateApplicationDataEditView {
        private KeyValueManager keyValueManager;
        private ObservableCollection<KeyValuePair> kvAccountGroups;
        private ObservableCollection<KeyValuePair> kvPartsSalesCategorys;
        private ObservableCollection<KeyValuePair> kvPartsRebateTypes;
        private readonly string[] kvNames = {
            "PartsRebateDirection"
        };
        public ObservableCollection<KeyValuePair> KvAccountGroups {
            get {
                return this.kvAccountGroups ?? (this.kvAccountGroups = new ObservableCollection<KeyValuePair>());
            }
        }
        public ObservableCollection<KeyValuePair> KvPartsSalesCategorys {
            get {
                return this.kvPartsSalesCategorys ?? (this.kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>());
            }
        }
        public ObservableCollection<KeyValuePair> KvPartsRebateTypes {
            get {
                return this.kvPartsRebateTypes ?? (this.kvPartsRebateTypes = new ObservableCollection<KeyValuePair>());
            }
        }
        protected KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        public object KvRebateDirection {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
        protected override bool OnRequestCanSubmit()
        {
            this.cbxPartsSalesCategory.SelectedIndex = 0;//默认品牌
            return true;
        }


        public PartsRebateApplicationDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(kvNames);
            this.KeyValueManager.LoadData();
            this.DataContextChanged += PartsRebateApplicationDataEditView_DataContextChanged;
        }

        private void PartsRebateApplicationDataEditView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var partsRebateApplication = this.DataContext as PartsRebateApplication;
            if(partsRebateApplication == null)
                return;
            this.KvAccountGroups.Clear();
            partsRebateApplication.PropertyChanged -= partsRebateApplication_PropertyChanged;
            partsRebateApplication.PropertyChanged += partsRebateApplication_PropertyChanged;
        }

        private void partsRebateApplication_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            var partsRebateApplication = this.DataContext as PartsRebateApplication;
            if(partsRebateApplication == null)
                return;
            switch(e.PropertyName) {
                case "PartsSalesCategoryId":
                    DomainContext.Load(DomainContext.GetAccountGroupsByPartsSalesCategoryIdQuery(partsRebateApplication.PartsSalesCategoryId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError)
                            return;
                        this.KvAccountGroups.Clear();
                        foreach(var accountGroup in loadOp.Entities) {
                            this.KvAccountGroups.Add(new KeyValuePair {
                                Key = accountGroup.Id,
                                Value = accountGroup.Name,
                                UserObject = accountGroup
                            });
                        }
                    }, null);
                    break;
            }
        }

        private void CreateUI() {
            // this.LayoutRoot.Children.Add(DI.GetDataEditPanel("PartsRebateApplication"));
            DomainContext.Load(DomainContext.GetPartsSalesCategoriesQuery().Where(v => v.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && v.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvPartsSalesCategorys.Clear();
                foreach(var item in loadOp.Entities) {
                    this.KvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.Name,
                        UserObject = item
                    });
                }
                this.cbxPartsSalesCategory.SelectedIndex = 0;//默认品牌
            }, null);
            this.cbxPartsSalesCategory.SelectionChanged += cbxPartsSalesCategory_SelectionChanged;
            var queryWindow = DI.GetQueryWindow("CustomerInformationBySparepart");
            queryWindow.SelectionDecided += queryWindow_SelectionDecided;
            this.ptbCustomerCompany.PopupContent = queryWindow;
            radMaskedTextBox1.SelectionStart = 0;
            radMaskedTextBox1.Value = null;
        }

        private void cbxPartsSalesCategory_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var partsRebateApplication = this.DataContext as PartsRebateApplication;
            if(partsRebateApplication == null)
                return;
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsRebateTypesQuery().Where(v => v.PartsSalesCategoryId == partsRebateApplication.PartsSalesCategoryId && v.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvPartsRebateTypes.Clear();
                foreach(var accountGroup in loadOp.Entities) {
                    this.KvPartsRebateTypes.Add(new KeyValuePair {
                        Key = accountGroup.Id,
                        Value = accountGroup.Name,
                        UserObject = accountGroup
                    });
                }
            }, null);
        }

        private void queryWindow_SelectionDecided(object sender, System.EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var customerInformation = queryWindow.SelectedEntities.Cast<CustomerInformation>().FirstOrDefault();
            if(customerInformation == null)
                return;
            var partsRebateApplication = this.DataContext as PartsRebateApplication;
            if(partsRebateApplication == null)
                return;
            partsRebateApplication.CustomerCompanyId = customerInformation.CustomerCompany.Id;
            partsRebateApplication.CustomerCompanyCode = customerInformation.CustomerCompany.Code;
            partsRebateApplication.CustomerCompanyName = customerInformation.CustomerCompany.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void radMaskedTextBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e) {
            var partsRebateApplication = this.DataContext as PartsRebateApplication;
            if(partsRebateApplication == null || radMaskedTextBox1.Value == null)
                return;
            if((e.PlatformKeyCode == 8 || e.PlatformKeyCode == 46) && partsRebateApplication.Amount == default(decimal)) {
                radMaskedTextBox1.SelectionStart = 0;
                radMaskedTextBox1.Value = null;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsRebateApplication;
            }
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsRebateApplicationByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    DomainContext.Load(DomainContext.GetAccountGroupsByPartsSalesCategoryIdQuery(entity.PartsSalesCategoryId), LoadBehavior.RefreshCurrent, loadOp1 => {
                        if(loadOp1.HasError)
                            return;
                        this.KvAccountGroups.Clear();
                        foreach(var accountGroup in loadOp1.Entities) {
                            this.KvAccountGroups.Add(new KeyValuePair {
                                Key = accountGroup.Id,
                                Value = accountGroup.Name,
                                UserObject = accountGroup
                            });
                        }
                    }, null);
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
        protected override void OnEditSubmitting() {
            var partsRebateApplication = this.DataContext as PartsRebateApplication;
            if(partsRebateApplication == null)
                return;
            partsRebateApplication.ValidationErrors.Clear();
            if(partsRebateApplication.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_CredenceApplication_PartsSalesCategoryIsNull);
                return;
            }
            if(partsRebateApplication.CustomerCompanyId == default(int)) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_CredenceApplication_CustomerCompanyIsNull);
                return;
            }
            if(partsRebateApplication.Amount <= 0) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_PartsRebateApplication_AmountThan0);
                return;
            }
            if(partsRebateApplication.PartsRebateTypeId == default(int)) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_CredenceApplication_PartsRebateTypeIsNull);
                return;
            }
            if(partsRebateApplication.AccountGroupId == default(int)) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_CredenceApplication_AccountGroupIsNull);
                return;
            }
            if(partsRebateApplication.RebateDirection == default(int)) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_PartsRebateApplication_RebateDirectionNotIsNull);
                return;
            }
            if(partsRebateApplication.HasValidationErrors)
                return;
            ((IEditableObject)partsRebateApplication).EndEdit();
            base.OnEditSubmitting();
        }

        protected override void OnEditCancelled() {
            this.DataContext = null;
            base.OnEditCancelled();
        }
    }
}
