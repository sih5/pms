﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataEdit {
    public partial class PartsRebateApplicationForApproveDataEditView {
        public PartsRebateApplicationForApproveDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return FinancialUIStrings.DataEditPanel_Title_PartsRebateApplication;
            }
        }

        private void CreateUI() {
            this.LayoutRoot.Children.Add(DI.GetDataEditPanel("PartsRebateApplicationForApprove"));
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsRebateApplicationByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            var partsRebateApplication = this.DataContext as PartsRebateApplication;
            if(partsRebateApplication == null)
                return;
            ((IEditableObject)partsRebateApplication).EndEdit();
            try {
                if(partsRebateApplication.Can审核配件返利申请单)
                    partsRebateApplication.审核配件返利申请单();

            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();

        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }
    }
}
