﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class PartsRebateApplicationForImportDataEditView {
        private DataGridViewBase partsRebateApplicationForImportDataGridView;
        private const string EXPORT_DATA_FILE_NAME = "导出配件返利申请单模板.xlsx";
        private ICommand exportFileCommand;
        public PartsRebateApplicationForImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return FinancialUIStrings.DataEditView_Title_Import_PartsRebateApplication;
            }
        }

        protected DataGridViewBase ImportDataGridView {
            get {
                if(this.partsRebateApplicationForImportDataGridView == null) {
                    this.partsRebateApplicationForImportDataGridView = DI.GetDataGridView("PartsRebateApplicationForImport");
                    this.partsRebateApplicationForImportDataGridView.DataContext = this.DataContext;
                }
                return this.partsRebateApplicationForImportDataGridView;
            }
        }

        private ObservableCollection<PartsRebateApplication> partsRebateApplications;

        public ObservableCollection<PartsRebateApplication> PartsRebateApplications {
            get {
                if(this.partsRebateApplications == null) {
                    this.partsRebateApplications = new ObservableCollection<PartsRebateApplication>();
                } return partsRebateApplications;
            }
        }

        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = FinancialUIStrings.DataEditView_Title_Detail_PartsRebateApplicationDetail,
                Content = this.ImportDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.ImportPartsRebateApplicationAsync(BaseApp.Current.CurrentUserData.EnterpriseId, fileName);
            this.ExcelServiceClient.ImportPartsRebateApplicationCompleted -= ExcelServiceClientOnImportPartsRebateApplicationCompleted;
            this.ExcelServiceClient.ImportPartsRebateApplicationCompleted += ExcelServiceClientOnImportPartsRebateApplicationCompleted;
        }

        private void ExcelServiceClientOnImportPartsRebateApplicationCompleted(object sender, ImportPartsRebateApplicationCompletedEventArgs e) {
            var dataEditView = this.DataContext as PartsRebateApplicationForImportDataEditView;
            if(dataEditView == null)
                return;
            if(e.rightData != null) {
                foreach(var data in e.rightData) {
                    dataEditView.PartsRebateApplications.Add(new PartsRebateApplication {
                        PartsSalesCategoryId = data.PartsSalesCategoryId,
                        Motive = data.Motive,
                        CustomerCompanyCode = data.CustomerCompanyCode,
                        CustomerCompanyName = data.CustomerCompanyName,
                        AccountGroupId = data.AccountGroupId,
                        Remark = data.Remark,
                        RebateDirection = data.RebateDirection,
                        PartsRebateTypeId = data.PartsRebateTypeId,
                        Amount = data.Amount
                    });
                }
            }
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!this.HasImportingError) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Notification_CompletedImport, 5);
            } else {
                if(!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if(!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Code,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Name,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = FinancialUIStrings.DataGridView_ColumnItem_Title_Amount,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategory,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsRebateApplication_PartsRebateType,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = FinancialUIStrings.DataGridView_ColumnItem_Title_PaymentBeneficiaryList_AccountGroup,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = FinancialUIStrings.DataEditPanel_Text_PartsRebateApplication_RebateDirection,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = FinancialUIStrings.DataEditPanel_Text_PartsRebateApplication_Motive,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_Remark,
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
    }
}