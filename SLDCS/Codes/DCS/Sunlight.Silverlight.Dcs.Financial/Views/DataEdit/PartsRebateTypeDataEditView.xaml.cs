﻿using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataEdit {
    public partial class PartsRebateTypeDataEditView {
        public PartsRebateTypeDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsRebateTypeByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
        protected override void OnEditSubmitting() {
            var partsRebateType = this.DataContext as PartsRebateType;
            if(partsRebateType == null)
                return;
            partsRebateType.ValidationErrors.Clear();
            //if(string.IsNullOrEmpty(accountGroup.Name))
            //    accountGroup.ValidationErrors.Add(new ValidationResult(FinancialUIStrings.DataEditView_Validation_AccountGroup_NameIsNull, new[] { "Name" }));
            //if(accountGroup.HasValidationErrors)
            //    return;
            ((IEditableObject)partsRebateType).EndEdit();
            //try {
            //    if(EditState == DataEditState.New) {
            //        if(accountGroup.Can生成账户组)
            //            accountGroup.生成账户组();
            //    } else if(EditState == DataEditState.Edit) {
            //        if(accountGroup.Can修改账户组)
            //            accountGroup.修改账户组();
            //    }
            //} catch(ValidationException ex) {
            //    UIHelper.ShowAlertMessage(ex.Message);
            //    return;
            //}
            base.OnEditSubmitting();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsRebateType;
            }
        }
        private void CreateUI() {
            this.LayoutRoot.Children.Add(DI.GetDataEditPanel("PartsRebateType"));
        }
    }
}
