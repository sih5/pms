﻿
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataEdit {
    public partial class PayOutBillDataEditView : INotifyPropertyChanged {
        private KeyValueManager keyValueManager;
        private ObservableCollection<KeyValuePair> kvBankAccounts;
        private ObservableCollection<KeyValuePair> kvPartsSalesCategorys;
        private ObservableCollection<KeyValuePair> kvSummarys;
        private string supplierCompanyName;
        public event PropertyChangedEventHandler PropertyChanged;
        private readonly string[] kvNames = {
           "WorkflowOfSimpleApproval_Status" ,"PayOutBill_PaymentMethod"
        };

        public ObservableCollection<KeyValuePair> KvPartsSalesCategorys {
            get {
                return this.kvPartsSalesCategorys ?? (this.kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>());
            }
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PayOutBill;
            }
        }

        public PayOutBillDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(kvNames);
            this.KeyValueManager.LoadData();

            this.Loaded += this.PayOutBillDataEditView_Loaded;
        }


        protected KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        public object KvPaymentMethods {
            get {
                return this.KeyValueManager[this.kvNames[1]];
            }
        }

        public string SupplierCompanyName {
            get {
                return this.supplierCompanyName;
            }
            set {
                this.supplierCompanyName = value;
                this.OnPropertyChanged("SupplierCompanyName");
            }
        }

        public ObservableCollection<KeyValuePair> KvSummarys {
            get {
                return this.kvSummarys ?? (this.kvSummarys = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvBankAccounts {
            get {
                return this.kvBankAccounts ?? (this.kvBankAccounts = new ObservableCollection<KeyValuePair>());
            }
        }
        protected override bool OnRequestCanSubmit()
        {
            this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            return true;
        }

        private void CreateUI() {
            var queryWindow = DI.GetQueryWindow("SupplierInformation");
            queryWindow.SelectionDecided += this.responsibleUnitQueryWindow_SelectionDecided;
            this.popupTextBoxSupplierCompany.PopupContent = queryWindow;
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(v => v.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && v.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvPartsSalesCategorys.Clear();
                foreach(var item in loadOp.Entities) {
                    this.KvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.Name,
                        UserObject = item
                    });
                }
                this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            }, null);
            radMaskedTextBox1.SelectionStart = 0;
            radMaskedTextBox1.Value = null;
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPayOutBillWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                    this.SupplierCompanyName = entity.Company.Name;
                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            var payOutBill = this.DataContext as PayOutBill;
            if(payOutBill == null)
                return;
            payOutBill.ValidationErrors.Clear();
            if(payOutBill.SupplierCompanyId == default(int)) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_PayOutBill_SupplierCompanyIdIsNull);
                return;
            }
            if(payOutBill.PaymentMethod == default(int)) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_PayOutBill_PaymentMethodIsNull);
                return;
            }
            if(payOutBill.PartsSalesCategoryId == null) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_PayOutBill_PartsSalesCategoryIdIsNull);
                return;
            }
            if(payOutBill.Summary == null) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_CustomerTransferBill_SummaryIsNull);
                return;
            }
            if(payOutBill.HasValidationErrors)
                return;
            if(payOutBill.EntityState == EntityState.New)
                payOutBill.Status = (int)DcsWorkflowOfSimpleApprovalStatus.新建;
            ((IEditableObject)payOutBill).EndEdit();
            base.OnEditSubmitting();
        }

        private void PayOutBillDataEditView_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            this.SupplierCompanyName = string.Empty;
            this.KvBankAccounts.Clear();
            this.DomainContext.Load(this.DomainContext.GetBankAccountsQuery().Where(v => v.Status == (int)DcsMasterDataStatus.有效 && v.OwnerCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var bankAccount in loadOp.Entities) {
                    this.KvBankAccounts.Add(new KeyValuePair {
                        Key = bankAccount.Id,
                        Value = bankAccount.BankAccountNumber,
                        UserObject = bankAccount
                    });
                }
            }, null);
            this.KvSummarys.Clear();
            this.DomainContext.Load(this.DomainContext.GetSummariesQuery().Where(v => v.Purpose == (int)DcsSummaryPurpose.付款 && v.OwnerCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var summary in loadOp.Entities) {
                    this.KvSummarys.Add(new KeyValuePair {
                        Key = summary.Id,
                        Value = summary.Content,
                        UserObject = summary
                    });
                }
            }, null);
            radMaskedTextBox1.Value = null;
        }

        private void responsibleUnitQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var supplierInformation = queryWindow.SelectedEntities.Cast<BranchSupplierRelation>().FirstOrDefault();
            if(supplierInformation == null)
                return;
            var payoutBill = this.DataContext as PayOutBill;
            if(payoutBill == null)
                return;
            payoutBill.SupplierCompanyId = supplierInformation.Company.Id;
            this.SupplierCompanyName = supplierInformation.Company.Name;

            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        public void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void radMaskedTextBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e) {
            var payOutBill = this.DataContext as PayOutBill;
            if(payOutBill == null || radMaskedTextBox1.Value == null)
                return;
            if((e.PlatformKeyCode == 8 || e.PlatformKeyCode == 46) && payOutBill.Amount == default(decimal)) {
                radMaskedTextBox1.SelectionStart = 0;
                radMaskedTextBox1.Value = null;
            }
        }

        protected override void OnEditCancelled() {
            this.DataContext = null;
            base.OnEditCancelled();
        }
    }
}
