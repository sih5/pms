﻿
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataEdit {
    public partial class PaymentBeneficiaryListDataEditView {
        public PaymentBeneficiaryListDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += PaymentBeneficiaryListDataEditView_DataContextChanged;
        }

        private DataGridViewBase paymentBeneficiaryListForEditDataGridView;

        private DataGridViewBase PaymentBeneficiaryListForEditDataGridView {
            get {
                if(this.paymentBeneficiaryListForEditDataGridView == null) {
                    this.paymentBeneficiaryListForEditDataGridView = DI.GetDataGridView("PaymentBeneficiaryListForEdit");
                    this.paymentBeneficiaryListForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.paymentBeneficiaryListForEditDataGridView;
            }
        }

        protected override string Title {
            get {
                return FinancialUIStrings.DataEditView_Title_PaymentBeneficiaryList;
            }
        }

        private void PaymentBeneficiaryListDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var paymentBill = this.DataContext as PaymentBill;
            if(paymentBill == null)
                return;
            paymentBill.PaymentBeneficiaryLists.EntityAdded += (entity, ev) => {
                ev.Entity.PropertyChanged += this.paymentBeneficiaryList_PropertyChanged;
            };
        }

        private void paymentBeneficiaryList_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var paymentBeneficiaryList = (PaymentBeneficiaryList)sender;
            if(paymentBeneficiaryList == null)
                return;
            if(e.PropertyName != "AccountGroupId")
                return;
            var paymentBill = this.DataContext as PaymentBill;
            if(paymentBill == null)
                return;
            paymentBeneficiaryList.Code = GlobalVar.ASSIGNED_BY_SERVER;
            paymentBeneficiaryList.PaymentBillId = paymentBill.Id;
            paymentBeneficiaryList.PaymentBillCode = paymentBill.Code;
            paymentBeneficiaryList.SalesCompanyId = paymentBill.SalesCompanyId;
            paymentBeneficiaryList.CustomerCompanyId = paymentBill.CustomerCompanyId;
            paymentBeneficiaryList.Summary = paymentBill.Summary;
            paymentBeneficiaryList.BillNumber = paymentBill.BillNumber;
            paymentBeneficiaryList.Operator = paymentBill.Operator;
            paymentBeneficiaryList.Status = (int)DcsWorkflowOfSimpleApprovalStatus.已审核;
        }

        private void CreateUI() {
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PaymentBeneficiaryList), FinancialUIStrings.DataEditPanel_Title_PaymentBeneficiaryList), null, () => {
                var view = this.PaymentBeneficiaryListForEditDataGridView;
                return view;
            });
            detailDataEditView.SetValue(Grid.RowProperty, 2);
            detailDataEditView.SetValue(Grid.ColumnSpanProperty, 4);
            this.LayoutRoot.Children.Add(detailDataEditView);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPaymentBillWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            var paymentBill = this.DataContext as PaymentBill;
            if(paymentBill == null || !this.PaymentBeneficiaryListForEditDataGridView.CommitEdit())
                return;
            paymentBill.ValidationErrors.Clear();
            foreach(var paymentBeneficiaryList in paymentBill.PaymentBeneficiaryLists)
                paymentBeneficiaryList.ValidationErrors.Clear();
            if(paymentBill.PaymentBeneficiaryLists.Sum(r => r.Amount) != paymentBill.Amount) {
                UIHelper.ShowAlertMessage(string.Format(FinancialUIStrings.DataEditView_Validation_PaymentBill_Amount));
                return;
            }
            if(paymentBill.PaymentBeneficiaryLists.GroupBy(r => r.AccountGroupId).Count() != paymentBill.PaymentBeneficiaryLists.Count()) {
                UIHelper.ShowAlertMessage(string.Format(FinancialUIStrings.DataEditView_Validation_PaymentBill_AccountGroup));
                return;
            }
            if(paymentBill.ValidationErrors.Any() || paymentBill.PaymentBeneficiaryLists.Any(v => v.HasValidationErrors))
                return;
            if(paymentBill.Amount > 0) {
                if(paymentBill.PaymentBeneficiaryLists.Any(r => r.Amount <= 0)) {
                    UIHelper.ShowAlertMessage(string.Format(FinancialUIStrings.DataEditView_Validation_PaymentBeneficiaryLists_Amount1));
                    return;
                }
            }
            if(paymentBill.Amount < 0) {
                if(paymentBill.PaymentBeneficiaryLists.Any(r => r.Amount >= 0)) {
                    UIHelper.ShowAlertMessage(string.Format(FinancialUIStrings.DataEditView_Validation_PaymentBeneficiaryLists_Amount2));
                    return;
                }
            }
            ((IEditableObject)paymentBill).EndEdit();
            try {
                if(paymentBill.Can分割来款单)
                    paymentBill.分割来款单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }
    }
}
