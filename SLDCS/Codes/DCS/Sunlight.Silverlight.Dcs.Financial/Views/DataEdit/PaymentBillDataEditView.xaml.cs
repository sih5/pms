﻿
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using System.Windows;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Dcs.Financial.Resources;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataEdit {
    public partial class PaymentBillDataEditView : INotifyPropertyChanged {
        private KeyValueManager keyValueManager;
        private ObservableCollection<KeyValuePair> kvBankAccounts;
        private ObservableCollection<KeyValuePair> kvAccountGroups;
        private string customerCompanyName;
        public event PropertyChangedEventHandler PropertyChanged;
        private DateTime? invoiceDate;
        private readonly string[] kvNames = {
            "PaymentBill_PaymentType"
        };

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PaymentBill;
            }
        }

        public PaymentBillDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(kvNames);
            this.KeyValueManager.LoadData();

            this.Loaded += this.PaymentBillDataEditView_Loaded;
        }
        public DateTime? InvoiceDate {
            get {
                return this.invoiceDate;
            }
            set {
                this.invoiceDate = value;
                this.OnPropertyChanged("InvoiceDate");
            }
        }

        protected KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        public object KvPaymentMethods {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public string CustomerCompanyName {
            get {
                return this.customerCompanyName;
            }
            set {
                this.customerCompanyName = value;
                this.OnPropertyChanged("CustomerCompanyName");
            }
        }

        public ObservableCollection<KeyValuePair> KvBankAccounts {
            get {
                return this.kvBankAccounts ?? (this.kvBankAccounts = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvAccountGroups {
            get {
                return this.kvAccountGroups ?? (this.kvAccountGroups = new ObservableCollection<KeyValuePair>());
            }
        }

        private void CreateUI() {
            var queryWindow = DI.GetQueryWindow("CustomerInformationBySparepart");
            queryWindow.SelectionDecided += this.responsibleUnitQueryWindow_SelectionDecided;
            this.popupTextBoxCustomerCompany.PopupContent = queryWindow;
            RadMaskedTextBox1.SelectionStart = 0;
            this.DomainContext.Load(this.DomainContext.GetBankAccountsQuery().Where(v => v.Status == (int)DcsMasterDataStatus.有效 && v.OwnerCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var bankAccount in loadOp.Entities) {
                    this.KvBankAccounts.Add(new KeyValuePair {
                        Key = bankAccount.Id,
                        Value = bankAccount.BankAccountNumber,
                        UserObject = bankAccount
                    });
                }
                var paymentBill = this.DataContext as PaymentBill;
                if(null != paymentBill && this.KvBankAccounts.Count() > 0) {
                    paymentBill.BankAccountId = this.KvBankAccounts.First().Key;
                }
            }, null);
            this.DomainContext.Load(this.DomainContext.GetAccountGroupsQuery().Where(v => v.SalesCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && v.Status != (int)DcsPaymentBillStatus.作废), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var accountGroup in loadOp.Entities) {
                    this.KvAccountGroups.Add(new KeyValuePair {
                        Key = accountGroup.Id,
                        Value = accountGroup.Name,
                        UserObject = accountGroup
                    });
                }
                this.comboBoxAccountGroup.SelectedIndex = 0;//默认账户组
            }, null);
        }
        protected override bool OnRequestCanSubmit() {
            this.comboBoxAccountGroup.SelectedIndex = 0;//默认账户组
            return true;
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPaymentBillWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                    this.CustomerCompanyName = entity.CustomerCompany.Name;

                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            var paymentBill = this.DataContext as PaymentBill;
            if(paymentBill == null)
                return;
            paymentBill.ValidationErrors.Clear();
            if(this.InvoiceDate.HasValue) {
                this.InvoiceDate = new DateTime(this.InvoiceDate.Value.Year, this.InvoiceDate.Value.Month, this.InvoiceDate.Value.Day, 00, 00, 00);
                paymentBill.InvoiceDate = this.InvoiceDate.Value;
            } else if(!paymentBill.InvoiceDate.HasValue) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_CustomerTransferBill_ChooseInvoiceDate);
                return;
            }
            if(paymentBill.CustomerCompanyId == default(int)) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_PaymentBill_CustomerCompanyIdIsNull);
                return;
            }
            if(!paymentBill.AccountGroupId.HasValue) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_CredenceApplication_AccountGroupIsNull);
                return;
            }
            if(paymentBill.PaymentMethod == (int)DcsPaymentBill_PaymentType.正常来款_承兑) {
                if(paymentBill.Drawer == null) {
                    UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_PaymentBill_DrawerIsNull);
                    return;
                }
                if(!paymentBill.IssueDate.HasValue) {
                    UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_PaymentBill_IssueDateIsNull);
                    return;
                }
                if(!paymentBill.DueDate.HasValue) {
                    UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_PaymentBill_DueDateIsNull);
                    return;
                }
                if(paymentBill.PayBank == null) {
                    UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_PaymentBill_PayBankIsNull);
                    return;
                }
                if(paymentBill.MoneyOrderNum == null) {
                    UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_PaymentBill_MoneyOrderNumIsNull);
                    return;
                }
            }
            if(paymentBill.PaymentMethod == default(int))
                paymentBill.ValidationErrors.Add(new ValidationResult(FinancialUIStrings.DataEditView_Validation_PaymentBill_PaymentMethodIsNull, new[] {
                    "PaymentMethod"
                }));
            if(paymentBill.HasValidationErrors)
                return;
            if(paymentBill.EntityState == EntityState.New) {
                paymentBill.Status = (int)DcsPaymentBillStatus.新建;
                if(paymentBill.AccountGroupId != null && (!paymentBill.PaymentBeneficiaryLists.Any())) {
                    paymentBill.PaymentBeneficiaryLists.Add(new PaymentBeneficiaryList {
                        Code = GlobalVar.ASSIGNED_BY_SERVER,
                        SalesCompanyId = paymentBill.SalesCompanyId,
                        AccountGroupId = (int)paymentBill.AccountGroupId,
                        PaymentBillId = paymentBill.Id,
                        PaymentBillCode = paymentBill.Code,
                        CustomerCompanyId = paymentBill.CustomerCompanyId,
                        Amount = paymentBill.Amount,
                        Summary = paymentBill.Summary,
                        BillNumber = paymentBill.BillNumber,
                        Operator = paymentBill.Operator,
                        Status = (int)DcsWorkflowOfSimpleApprovalStatus.新建
                    });
                }
            } else {
                if(paymentBill.AccountGroupId != null) {
                    if(!paymentBill.PaymentBeneficiaryLists.Any()) {
                        paymentBill.PaymentBeneficiaryLists.Add(new PaymentBeneficiaryList {
                            Code = GlobalVar.ASSIGNED_BY_SERVER,
                            SalesCompanyId = paymentBill.SalesCompanyId,
                            AccountGroupId = (int)paymentBill.AccountGroupId,
                            PaymentBillId = paymentBill.Id,
                            PaymentBillCode = paymentBill.Code,
                            CustomerCompanyId = paymentBill.CustomerCompanyId,
                            Amount = paymentBill.Amount,
                            Summary = paymentBill.Summary,
                            BillNumber = paymentBill.BillNumber,
                            Operator = paymentBill.Operator,
                            Status = (int)DcsWorkflowOfSimpleApprovalStatus.新建
                        });
                    } else {
                        foreach(var detail in paymentBill.PaymentBeneficiaryLists) {
                            detail.ValidationErrors.Clear();
                            if(!detail.HasValidationErrors) {
                                detail.AccountGroupId = (int)paymentBill.AccountGroupId;
                                detail.CustomerCompanyId = paymentBill.CustomerCompanyId;
                                detail.Amount = paymentBill.Amount;
                                detail.Summary = paymentBill.Summary;
                                detail.BillNumber = paymentBill.BillNumber;
                                detail.Operator = paymentBill.Operator;
                                ((IEditableObject)detail).EndEdit();
                            }
                        }
                    }
                }
            }
            ((IEditableObject)paymentBill).EndEdit();
            base.OnEditSubmitting();

        }

        protected override void OnEditSubmitted() {
            this.DataContext = null;
            popupTextBoxCustomerCompany.Text = string.Empty;
            base.OnEditSubmitted();
        }
        protected override void OnEditCancelled() {
            this.DataContext = null;
            popupTextBoxCustomerCompany.Text = string.Empty;
            base.OnEditCancelled();
        }
        private void PaymentBillDataEditView_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            RadMaskedTextBox1.Value = null;
        }

        private void responsibleUnitQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var customerInformation = queryWindow.SelectedEntities.Cast<CustomerInformation>().FirstOrDefault();
            if(customerInformation == null)
                return;
            var paymentBill = this.DataContext as PaymentBill;
            if(paymentBill == null)
                return;
            paymentBill.CustomerCompanyId = customerInformation.CustomerCompany.Id;
            this.CustomerCompanyName = customerInformation.CustomerCompany.Name;

            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        public void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void radMaskedTextBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e) {
            var paymentBill = this.DataContext as PaymentBill;
            if(paymentBill == null || RadMaskedTextBox1.Value == null)
                return;
            if((e.PlatformKeyCode == 8 || e.PlatformKeyCode == 46) && paymentBill.Amount == default(decimal)) {
                RadMaskedTextBox1.SelectionStart = 0;
                RadMaskedTextBox1.Value = null;
            }
        }

        private int comboxIndex;
        private void AccountGroup_OnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            var comboBox = sender as RadComboBox;
            if(comboBox == null)
                return;
            var keyValuePair = comboBox.SelectedItem as KeyValuePair;
            if(keyValuePair == null)
                return;
            comboxIndex = comboBox.SelectedIndex;
        }
        private void AccountGroup_OnKeyUp(object sender, KeyEventArgs e) {
            if(e.PlatformKeyCode == 8) {
                comboBoxAccountGroup.SelectedIndex = comboxIndex;
            }
        }

        private void DcsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var paymentBill = this.DataContext as PaymentBill;
            if(paymentBill == null || paymentBill.PaymentMethod == 0)
                return;
            if(paymentBill.PaymentMethod == (int)DcsPaymentBill_PaymentType.正常来款_承兑) {
                Drawer.Visibility = Visibility.Visible;
                boxDrawer.Visibility = Visibility.Visible;
                IssueDate.Visibility = Visibility.Visible;
                IssueDateTime.Visibility = Visibility.Visible;
                DueDate.Visibility = Visibility.Visible;
                DueDateTime.Visibility = Visibility.Visible;
                PayBank.Visibility = Visibility.Visible;
                boxPayBank.Visibility = Visibility.Visible;
                MoneyOrderNum.Visibility = Visibility.Visible;
                boxMoneyOrderNum.Visibility = Visibility.Visible;
            } else {
                Drawer.Visibility = Visibility.Collapsed;
                boxDrawer.Visibility = Visibility.Collapsed;
                IssueDate.Visibility = Visibility.Collapsed;
                IssueDateTime.Visibility = Visibility.Collapsed;
                DueDate.Visibility = Visibility.Collapsed;
                DueDateTime.Visibility = Visibility.Collapsed;
                PayBank.Visibility = Visibility.Collapsed;
                boxPayBank.Visibility = Visibility.Collapsed;
                MoneyOrderNum.Visibility = Visibility.Collapsed;
                boxMoneyOrderNum.Visibility = Visibility.Collapsed;
            }
        }
    }
}
