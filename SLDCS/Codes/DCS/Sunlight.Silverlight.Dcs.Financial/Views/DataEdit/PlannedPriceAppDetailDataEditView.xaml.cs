﻿
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataEdit {
    public partial class PlannedPriceAppDetailDataEditView {

        private DataGridViewBase plannedPriceAppDetailForEditDataGridView;

        public PlannedPriceAppDetailDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return FinancialUIStrings.DataEditView_Title_Detail_PlannedPriceApp;
            }
        }

        private DataGridViewBase PlannedPriceAppDetailForEditDataGridView {
            get {
                if(this.plannedPriceAppDetailForEditDataGridView == null) {
                    this.plannedPriceAppDetailForEditDataGridView = DI.GetDataGridView("PlannedPriceAppDetail");
                    this.plannedPriceAppDetailForEditDataGridView.DomainContext = this.DomainContext;
                    return this.plannedPriceAppDetailForEditDataGridView;
                }
                return this.plannedPriceAppDetailForEditDataGridView;
            }
        }

        private void CreateUI() {
            var dataEditPanel = DI.GetDataEditPanel("PlannedPriceAppDetail");
            dataEditPanel.SetValue(Grid.RowProperty, 0);
            this.Root.Children.Add(dataEditPanel);

            this.PlannedPriceAppDetailForEditDataGridView.SetValue(Grid.RowProperty, 0);
            ((RadTabItem)this.RTCPlannedPriceAppDetail.Items[0]).Content = PlannedPriceAppDetailForEditDataGridView;
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPlannedPriceAppWithPlannedPriceAppDetailByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();

                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
