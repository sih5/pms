﻿
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;


namespace Sunlight.Silverlight.Dcs.Financial.Views.DataEdit {
    public partial class PlannedPriceAppWarehouseDetailDataEditView {
        private ObservableCollection<WarehouseCostChangeBill> warehouseCostChangeBills = new ObservableCollection<WarehouseCostChangeBill>();
        private DataGridViewBase warehouseCostChangeBillDataGridView;
        private DataGridViewBase warehouseCostChangeDetailDataGridView;

        public PlannedPriceAppWarehouseDetailDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return FinancialUIStrings.DataEditView_Title_Detail_WarehouseCostChangeBill;
            }
        }

        protected override void OnEditCancelled() {
            this.WarehouseCostChangeDetailDataGridView.DataContext = null;
            base.OnEditCancelled();
        }



        private DataGridViewBase WarehouseCostChangeDetailDataGridView {
            get {
                if(this.warehouseCostChangeDetailDataGridView == null) {
                    this.warehouseCostChangeDetailDataGridView = DI.GetDataGridView("WarehouseCostChangeDetail");
                    this.warehouseCostChangeDetailDataGridView.DomainContext = this.DomainContext;
                    return this.warehouseCostChangeDetailDataGridView;
                }
                return this.warehouseCostChangeDetailDataGridView;
            }
        }

        private DataGridViewBase WarehouseCostChangeBillDataGridView {
            get {
                if(this.warehouseCostChangeBillDataGridView == null) {
                    this.warehouseCostChangeBillDataGridView = DI.GetDataGridView("WarehouseCostChangeBill");
                    this.warehouseCostChangeBillDataGridView.DomainContext = this.DomainContext;
                    this.WarehouseCostChangeBillDataGridView.DataContext = this;
                }
                return this.warehouseCostChangeBillDataGridView;
            }
        }

        private void CreateUI() {
            this.WarehouseCostChangeBillDataGridView.SetValue(Grid.RowProperty, 0);
            ((RadTabItem)this.RTCWarehouseCostChangeBill.Items[0]).Content = WarehouseCostChangeBillDataGridView;
            this.WarehouseCostChangeDetailDataGridView.SetValue(Grid.RowProperty, 2);
            ((RadTabItem)this.RTCWarehouseCostChangeDetail.Items[0]).Content = WarehouseCostChangeDetailDataGridView;
            this.BtnShowDetail.Click += this.BtnShowDetail_Click;
        }

        private void BtnShowDetail_Click(object sender, System.Windows.RoutedEventArgs e) {
            if(this.WarehouseCostChangeBillDataGridView.SelectedEntities != null) {
                var selectItem = this.WarehouseCostChangeBillDataGridView.SelectedEntities.Cast<WarehouseCostChangeBill>().ToArray();
                if(selectItem == null || !selectItem.Any()) {
                    UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_PlannedPriceApp_WarehouseCostChangeBillIsNull);
                    return;
                }
                var warehouseCostChangeBill = selectItem.FirstOrDefault();
                if(warehouseCostChangeBill == null)
                    return;
                this.WarehouseCostChangeDetailDataGridView.DataContext = warehouseCostChangeBill;
            }
        }

        private void LoadEntityToEdit(int id) {
            this.WarehouseCostChangeBills.Clear();
            this.DomainContext.Load(this.DomainContext.GetWarehouseCostChangeBillsQuery().Where(r => r.PlannedPriceAppId == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                if(!loadOp.Entities.Any())
                    return;
                foreach(var warehouseCostChangeBill in loadOp.Entities)
                    this.WarehouseCostChangeBills.Add(warehouseCostChangeBill);
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public ObservableCollection<WarehouseCostChangeBill> WarehouseCostChangeBills {
            get {
                return warehouseCostChangeBills ?? (this.warehouseCostChangeBills = new ObservableCollection<WarehouseCostChangeBill>());
            }
        }

    }
}
