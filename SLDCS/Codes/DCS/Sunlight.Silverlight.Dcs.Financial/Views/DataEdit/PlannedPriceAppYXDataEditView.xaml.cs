﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;


namespace Sunlight.Silverlight.Dcs.Financial.Views.DataEdit {
    public partial class PlannedPriceAppYXDataEditView {
        private DataGridViewBase plannedPriceAppDetailForEditDataGridView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        protected Action<string> UploadFileSuccessedProcessing;
        private ICommand exportFileCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出配件计划价申请管理清单模板.xlsx";
        private string strFileName;
        private RadUpload uploader;
        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    //this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        var plannedPriceApp = this.DataContext as PlannedPriceApp;
                        if(plannedPriceApp == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.ImportPlannedPriceAppDetailAsync(new PlannedPriceAppExtend {
                            OwnerCompanyId = plannedPriceApp.OwnerCompanyId,
                            OwnerCompanyCode = plannedPriceApp.OwnerCompanyCode,
                            OwnerCompanyName = plannedPriceApp.OwnerCompanyName,
                            PartsSalesCategoryId = plannedPriceApp.PartsSalesCategoryId,
                            PartsSalesCategoryName = plannedPriceApp.PartsSalesCategoryName,
                            PlannedExecutionTime = plannedPriceApp.PlannedExecutionTime,
                            RecordStatus = (int)DcsRecordStatus.未计账,
                            Status = plannedPriceApp.Status,
                            Remark = plannedPriceApp.Remark
                        }, e.HandlerData.CustomData["Path"].ToString());
                        this.excelServiceClient.ImportPlannedPriceAppDetailCompleted -= ExcelServiceClient_ImportPlannedPriceAppDetailCompleted;
                        this.excelServiceClient.ImportPlannedPriceAppDetailCompleted += ExcelServiceClient_ImportPlannedPriceAppDetailCompleted;
                    };
                }
                return this.uploader;
            }
        }

        private DcsDetailDataEditView detailEditView;

        private DcsDetailDataEditView DetailEditView {
            get {
                if(this.detailEditView == null) {
                    this.detailEditView = new DcsDetailDataEditView();
                    detailEditView.Register(Utils.GetEntityLocalizedName(typeof(PlannedPriceApp), "PlannedPriceAppDetails"), null, this.PlannedPriceAppDetailForEditDataGridView);
                    detailEditView.SetValue(Grid.ColumnProperty, 2);
                }
                return this.detailEditView;
            }
        }

        private ButtonItem importButton;
        public ButtonItem ImportButton {
            get {
                return this.importButton ?? (this.importButton = new ButtonItem {
                    Title = FinancialUIStrings.DataEditView_Title_Detail_Import,
                    Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                    Command = new DelegateCommand(() => {
                        var t = this.DataContext as PlannedPriceApp;
                        if(t != null && t.PartsSalesCategoryId != default(int)) {
                            this.Uploader.ShowFileDialog();
                        } else {
                            UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_PlannedPriceApp_PartsSalesCategoryIdIsNull);
                        }
                    })
                });
            }
        }

        private ButtonItem exportButton;
        public ButtonItem ExportButton {
            get {
                return this.exportButton ?? (this.exportButton = new ButtonItem {
                    Title = FinancialUIStrings.DataEditView_Title_Detail_Export,
                    Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                    Command = this.ExportTemplateCommand
                });
            }
        }

        private void ExcelServiceClient_ImportPlannedPriceAppDetailCompleted(object sender, ImportPlannedPriceAppDetailCompletedEventArgs e) {
            this.HideSaveButton();
            var plannedPriceApp = this.DataContext as PlannedPriceApp;
            if(plannedPriceApp == null)
                return;
            foreach(var detail in plannedPriceApp.PlannedPriceAppDetails) {
                plannedPriceApp.PlannedPriceAppDetails.Remove(detail);
            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowAlertMessage(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName)) {
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
                UIHelper.ShowAlertMessage(FinancialUIStrings.DataEditView_Validation_PlannedPriceApp_ImportedCorrectData);
            } else {
                UIHelper.ShowAlertMessage(FinancialUIStrings.DataEditView_Validation_PlannedPriceApp_ImportedSuccess);
            }
        }

        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage(FinancialUIStrings.DataEditView_Validation_PlannedPriceApp_CloseTheFile);
                    e.Handled = true;
                }
            }
        }

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //取当前登录用户的HashCode来标记上传文件的唯一性
                //this.UploadFileSuccessedProcessing.Invoke(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }


        public PlannedPriceAppYXDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PlannedPriceApp;
            }
        }

        private DataGridViewBase PlannedPriceAppDetailForEditDataGridView {
            get {
                if(this.plannedPriceAppDetailForEditDataGridView == null) {
                    this.plannedPriceAppDetailForEditDataGridView = DI.GetDataGridView("PlannedPriceAppDetailForEdit");
                    this.plannedPriceAppDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.plannedPriceAppDetailForEditDataGridView;
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }
        private void CreateUI() {
            this.DetailEditView.RegisterButton(this.ImportButton);
            this.DetailEditView.RegisterButton(this.ExportButton);
            this.RegisterButton(new ButtonItem {
                Command = new DelegateCommand(this.PlannedPrice),
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/HistoryQuery.png", UriKind.Relative),
                Title = FinancialUIStrings.DataEditView_Validation_PlannedPriceApp_SeeBrandPrice
            }, true);
            var dataEditPanel = DI.GetDataEditPanel("PlannedPriceAppYX");
            this.Root.Children.Add(dataEditPanel);
            this.Root.Children.Add(this.CreateVerticalLine(1));
            this.Root.Children.Add(DetailEditView);
        }

        private void PlannedPrice() {
            if(this.PlannedPriceAppDetailForEditDataGridView.SelectedEntities == null) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_PlannedPriceApp_SelectPriceInformation);
                return;
            }
            this.PlannedPriceAppDetailWindow.ShowDialog();
        }

        private RadWindow plannedPriceAppDetailWindow;
        private RadWindow PlannedPriceAppDetailWindow {
            get {
                return this.plannedPriceAppDetailWindow ?? (this.plannedPriceAppDetailWindow = new RadWindow {
                    Content = this.PlannedPriceAppDetailQueryWindow,
                    Header = FinancialUIStrings.DataEditView_Title_PlannedPriceAppDetailQueryWindow,
                    WindowState = WindowState.Normal,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                    AllowDrop = false,
                    ResizeMode = ResizeMode.NoResize,
                    CanClose = true
                });
            }
        }

        private QueryWindowBase plannedPriceAppDetailQueryWindow;
        private QueryWindowBase PlannedPriceAppDetailQueryWindow {
            get {
                if(plannedPriceAppDetailQueryWindow == null) {
                    this.plannedPriceAppDetailQueryWindow = DI.GetQueryWindow("PartsPlannedPriceYX");
                    this.plannedPriceAppDetailQueryWindow.Loaded += PlannedPriceAppDetailQueryWindow_Loaded;
                    ;
                }
                return this.plannedPriceAppDetailQueryWindow;
            }
        }

        private void PlannedPriceAppDetailQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;
            var plannedPriceApp = this.DataContext as PlannedPriceApp;
            if(plannedPriceApp == null)
                return;
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] { "Common", "Code", this.PlannedPriceAppDetailForEditDataGridView.SelectedEntities.Cast<PlannedPriceAppDetail>().FirstOrDefault().SparePartCode });
        }

        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = FinancialUIStrings.QueryPanel_QueryItem_Title_SparePart_Code,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = FinancialUIStrings.QueryPanel_QueryItem_Title_SparePart_Name,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = FinancialUIStrings.DataEditPanel_Text_PlannedPriceApp_RequestedPrice,
                                                IsRequired = true
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPlannedPriceAppWithPlannedPriceAppDetailByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                this.DetailEditView.UnregisterButton(this.ImportButton);
                this.DetailEditView.UnregisterButton(this.ExportButton);
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => {
                    this.LoadEntityToEdit((int)id);
                    this.Root.ChildrenOfType<DcsDetailDataEditView>().First().UnregisterButton(ImportButton);

                });
            else {
                this.LoadEntityToEdit((int)id);
                this.Root.ChildrenOfType<DcsDetailDataEditView>().First().UnregisterButton(ImportButton);
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.PlannedPriceAppDetailForEditDataGridView.CommitEdit())
                return;

            var plannedPriceApp = this.DataContext as PlannedPriceApp;
            var time = DateTime.Now.ToString("yyyy-MM-dd");
            if(plannedPriceApp == null)
                return;
            if(!plannedPriceApp.PlannedPriceAppDetails.Any()) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_DetailIsNull);
                return;
            }
            //if(plannedPriceApp.PlannedPriceAppDetails.Count > 400) {
            //    UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_DetailCountMoreThan400);
            //    return;
            //}
            if(plannedPriceApp.PlannedPriceAppDetails.GroupBy(entity => new {
                entity.SparePartCode,
                entity.SparePartName
            }).Any(array => array.Count() > 1)) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_PlannedPriceApp_SparePartCodeRepeat);
                return;
            }
            plannedPriceApp.ValidationErrors.Clear();
            if (plannedPriceApp.PlannedExecutionTime < Convert.ToDateTime(time))
                plannedPriceApp.ValidationErrors.Add(new ValidationResult(FinancialUIStrings.DataEditView_Validation_PlannedPriceApp_PlannedExecutionTimeNotGreaterThanNow, new[] {
                    "PlannedExecutionTime"
                }));
            if(plannedPriceApp.PartsSalesCategoryId == default(int))
                plannedPriceApp.ValidationErrors.Add(new ValidationResult(FinancialUIStrings.DataEditView_Validation_PlannedPriceApp_PartsSalesCategoryIdIsNull, new[] {
                        "PartsSalesCategoryId"
                    }));
            if(plannedPriceApp.HasValidationErrors)
                return;
            ((IEditableObject)plannedPriceApp).EndEdit();
            if(plannedPriceApp.EntityState == EntityState.New)
                if(plannedPriceApp.Can同步生成计划价申请单) {
                    plannedPriceApp.同步生成计划价申请单();
                }
            base.OnEditSubmitting();
        }

        public GridViewStyleSelector GridViewStyleSelector() {
            var codes = new List<string>();
            var codesN = new List<string>();
            var plannedPriceApp = this.DataContext as PlannedPriceApp;
            if(plannedPriceApp != null) {
                codes.AddRange(plannedPriceApp.PlannedPriceAppDetails.Select(entity => entity.SparePartCode));
                foreach(var t in codes) {
                    if(!codesN.Contains(t))
                        codesN.Add(t);
                }
                foreach(var t in codes) {
                    if(!codesN.Contains(t))
                        codesN.Add(t);
                }
            }
            return new GridViewStyleSelector {
                ExecuteFunc = (item, container) => {
                    if(item is PlannedPriceAppDetail) {
                        var plannedPriceAppDetail = item as PlannedPriceAppDetail;
                        foreach(var t in codesN) {
                            if(plannedPriceAppDetail.SparePartCode == t || plannedPriceAppDetail.RequestedPrice < 0) {
                                return this.Resources["ErrorDataBackground"] as Style;
                            } else {
                                return this.Resources["CorrectDataBackground"] as Style;
                            }
                        }
                    }
                    return null;
                }
            };
        }

        protected override void Reset() {
            this.DetailEditView.RegisterButton(this.ImportButton);
            this.DetailEditView.RegisterButton(this.ExportButton);
            this.ShowSaveButton();
        }

    }
}
