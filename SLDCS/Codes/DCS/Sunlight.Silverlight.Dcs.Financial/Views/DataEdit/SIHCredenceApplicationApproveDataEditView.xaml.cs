﻿using System;
using System.Windows;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Controls;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Financial.Panels.DataEdit;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataEdit {

    public partial class SIHCredenceApplicationApproveDataEditView : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;
        private DataGridViewBase sihCredenceApplicationDataGridView;
        private ButtonItem rejectBtn;
        private string message;
        private FileUploadForCredenceDataEditPanel fileUploadDataEditPanels;
        public FileUploadForCredenceDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (FileUploadForCredenceDataEditPanel)DI.GetDataEditPanel("FileUploadForCredence"));
            }
        }

        public SIHCredenceApplicationApproveDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
        protected override string Title {
            get {
                var sihCreditInfo = this.DataContext as SIHCreditInfo;
                message = sihCreditInfo == null || sihCreditInfo.Status == (int)DCSSIHCreditInfoStatus.提交 && sihCreditInfo.Type == (int)DCSSIHCreditInfoType.配件中心 ? "初审" :
                          sihCreditInfo.Status == (int)DCSSIHCreditInfoStatus.提交 && sihCreditInfo.Type == (int)DCSSIHCreditInfoType.分销中心 ? "审核" :
                          sihCreditInfo.Status == (int)DCSSIHCreditInfoStatus.初审通过 ? "审核" :
                          sihCreditInfo.Status == (int)DCSSIHCreditInfoStatus.审核通过 ? "审批" :
                          sihCreditInfo.Status == (int)DCSSIHCreditInfoStatus.审批通过 ? "高级审核" : "";
                return message + "SIH授信(服务商)信用申请单";
            }
        }

        private DataGridViewBase SIHCredenceApplicationDataGridView {
            get {
                if(this.sihCredenceApplicationDataGridView == null) {
                    this.sihCredenceApplicationDataGridView = DI.GetDataGridView("SIHCredenceApplicationDetailApprove");
                    this.sihCredenceApplicationDataGridView.DomainContext = this.DomainContext;
                }
                return this.sihCredenceApplicationDataGridView;
            }
        }

        public void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetSIHCreditInfoByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    FileUploadDataEditPanels.FilePath = entity.Path;
                    this.MarketingDepartmentName.Text = entity.SIHCreditInfoDetails.First().MarketingDepartmentName;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        private void CreateUI() {
            this.rejectBtn = new ButtonItem {
                Command = new Sunlight.Silverlight.Core.Command.DelegateCommand(this.RejecrCurrentData),
                Title = FinancialUIStrings.Action_Title_Reject,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative)
            };
            this.RegisterButton(rejectBtn);

            FileUploadDataEditPanels.FilePath = null;
            FileUploadDataEditPanels.SetValue(Grid.RowProperty, 2);
            FileUploadDataEditPanels.SetValue(Grid.ColumnProperty, 1);
            FileUploadDataEditPanels.SetValue(Grid.ColumnSpanProperty, 4);
            FileUploadDataEditPanels.IsEnabled = true;
            FileUploadDataEditPanels.isHiddenButtons = true;
            this.LayoutRoot.Children.Add(FileUploadDataEditPanels);

            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register("SIH授信(服务商)信用申请单清单", null, () => this.SIHCredenceApplicationDataGridView);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.SetValue(Grid.RowProperty, 4);
            detailDataEditView.SetValue(Grid.ColumnSpanProperty, 8);
            detailDataEditView.Width = 1200;
            this.LayoutRoot.Children.Add(detailDataEditView);
        }

        protected override void OnEditCancelled() {
            this.DataContext = null;
            // this.Reset();
            base.OnEditCancelled();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {
            var domainContext = new DcsDomainContext();
            var sihCreditInfo = this.DataContext as SIHCreditInfo;
            if(sihCreditInfo == null)
                return;
            this.DomainContext.批量审核SIH授信服务商申请单(new int[] { sihCreditInfo.Id }, sihCreditInfo.Status, true, invokeOp => {
                if(invokeOp.HasError) {
                    if(!invokeOp.IsErrorHandled)
                        invokeOp.MarkErrorAsHandled();
                    var error = invokeOp.ValidationErrors.FirstOrDefault();
                    if(error != null) {
                        UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage, 5);
                    } else {
                        DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                    }
                    DomainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(message + "成功");
                base.OnEditSubmitting();
            }, null);
        }
        //驳回
        private void RejecrCurrentData() {
            var domainContext = new DcsDomainContext();
            var sihCreditInfo = this.DataContext as SIHCreditInfo;
            if(sihCreditInfo == null)
                return;
            ((IEditableObject)sihCreditInfo).EndEdit();
            this.DomainContext.批量审核SIH授信服务商申请单(new int[] { sihCreditInfo.Id }, sihCreditInfo.Status, false, invokeOp => {
                if(invokeOp.HasError) {
                    if(!invokeOp.IsErrorHandled)
                        invokeOp.MarkErrorAsHandled();
                    var error = invokeOp.ValidationErrors.FirstOrDefault();
                    if(error != null) {
                        UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage, 5);
                    } else {
                        DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                    }
                    DomainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification("驳回成功");
                base.OnEditSubmitting();
            }, null);
        }
    }
}