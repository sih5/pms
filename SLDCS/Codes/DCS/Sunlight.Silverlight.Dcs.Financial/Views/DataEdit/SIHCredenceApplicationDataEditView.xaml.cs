﻿using System;
using System.Windows;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Controls;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Financial.Panels.DataEdit;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataEdit {

    public partial class SIHCredenceApplicationDataEditView : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;
        private DataGridViewBase sihCredenceApplicationDataGridView;
        private string marketingDepartmentName;
        private FileUploadForCredenceDataEditPanel fileUploadDataEditPanels;
        public FileUploadForCredenceDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (FileUploadForCredenceDataEditPanel)DI.GetDataEditPanel("FileUploadForCredence"));
            }
        }

        public SIHCredenceApplicationDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.marketingDepartmentName = "";
        }
        protected override string BusinessName {
            get {
                return "SIH授信(服务商)信用申请单";
            }
        }

        private DataGridViewBase SIHCredenceApplicationDataGridView {
            get {
                if(this.sihCredenceApplicationDataGridView == null) {
                    this.sihCredenceApplicationDataGridView = DI.GetDataGridView("SIHCredenceApplicationDetailForEdit");
                    this.sihCredenceApplicationDataGridView.DomainContext = this.DomainContext;
                }
                return this.sihCredenceApplicationDataGridView;
            }
        }


        public void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void OnEditSubmitting() {
            var sihCreditInfo = this.DataContext as SIHCreditInfo;
            if(sihCreditInfo == null)
                return;
            if(!this.SIHCredenceApplicationDataGridView.CommitEdit())
                return;
            sihCreditInfo.Path = FileUploadDataEditPanels.FilePath;
            sihCreditInfo.IsAttach = !string.IsNullOrWhiteSpace(sihCreditInfo.Path);
            sihCreditInfo.ValidationErrors.Clear();
            foreach(var item in sihCreditInfo.SIHCreditInfoDetails)
                item.ValidationErrors.Clear();
            if(!sihCreditInfo.SIHCreditInfoDetails.Any()) {
                UIHelper.ShowNotification("SIH授信(服务商)信用申请单清单不能为空");
                return;
            }
            foreach(var SIHCreditInfoDetail in sihCreditInfo.SIHCreditInfoDetails) {
                if(SIHCreditInfoDetail.DealerId == default(int)) {
                    UIHelper.ShowNotification("服务站为必填");
                    return;
                }
                if(!SIHCreditInfoDetail.ThisSIHCredit.HasValue) {
                    UIHelper.ShowNotification("存在本次SIH授信额度（服务商）为空的清单");
                    return;
                }
                if(!SIHCreditInfoDetail.ValidationTime.HasValue) {
                    UIHelper.ShowNotification("存在生效时间为空的清单");
                    return;
                }
                if(SIHCreditInfoDetail.ValidationTime.Value.Date < DateTime.Now.Date) {
                    UIHelper.ShowNotification("生效时间必须大于等于当前时间");
                    return;
                }
                if(SIHCreditInfoDetail.ExpireTime.HasValue && SIHCreditInfoDetail.ValidationTime.Value.Date >= SIHCreditInfoDetail.ExpireTime.Value.Date) {
                    UIHelper.ShowNotification("失效时间必须大于生效时间");
                    return;
                }
                if(sihCreditInfo.Type == (int)DCSSIHCreditInfoType.分销中心) {
                    if((SIHCreditInfoDetail.SIHCreditZB ?? 0) < SIHCreditInfoDetail.ThisSIHCredit.Value) {
                        UIHelper.ShowNotification("本次SIH授信额度（服务商）不能大于配件公司授信的额度");
                        return;
                    }
                    if(SIHCreditInfoDetail.ValidationTime < SIHCreditInfoDetail.ValidationTimeCheck) {
                        UIHelper.ShowNotification(string.Format("服务站{0},生效时间不能小于配件公司授信的时间 {1}", SIHCreditInfoDetail.DealerCode, SIHCreditInfoDetail.ValidationTimeCheck));
                        return;
                    }
                    if(SIHCreditInfoDetail.ExpireTimeCheck.HasValue && (!SIHCreditInfoDetail.ExpireTime.HasValue || SIHCreditInfoDetail.ExpireTime > SIHCreditInfoDetail.ExpireTimeCheck)) {
                        UIHelper.ShowNotification(string.Format("服务站{0},失效时间不能大于配件公司授信的时间 {1}", SIHCreditInfoDetail.DealerCode, SIHCreditInfoDetail.ExpireTimeCheck));
                        return;
                    }
                }
            }
            if(sihCreditInfo.HasValidationErrors || sihCreditInfo.SIHCreditInfoDetails.Any(e => e.HasValidationErrors))
                return;
            ((IEditableObject)sihCreditInfo).EndEdit();
            if(EditState == DataEditState.New)
                sihCreditInfo.Code = GlobalVar.ASSIGNED_BY_SERVER;
            base.OnEditSubmitting();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetSIHCreditInfoByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    FileUploadDataEditPanels.FilePath = entity.Path;
                    MarketingDepartmentName = entity.SIHCreditInfoDetails.First().MarketingDepartmentName;
                    entity.MarketingDepartmentId = entity.SIHCreditInfoDetails.First().MarketingDepartmentId;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        private void CreateUI() {
            var queryWindow = DI.GetQueryWindow("MarketingDepartmentSih");
            queryWindow.SelectionDecided += this.MarketingDepartmentQueryWindow_SelectionDecided;
            this.popupTextBoxMarketingDepartmentName.PopupContent = queryWindow;

            FileUploadDataEditPanels.FilePath = null;
            FileUploadDataEditPanels.SetValue(Grid.RowProperty, 1);
            FileUploadDataEditPanels.SetValue(Grid.ColumnProperty, 1);
            FileUploadDataEditPanels.SetValue(Grid.ColumnSpanProperty, 4);
            this.LayoutRoot.Children.Add(FileUploadDataEditPanels);

            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register("SIH授信(服务商)信用申请单清单", null, () => this.SIHCredenceApplicationDataGridView);
            detailDataEditView.SetValue(Grid.RowProperty, 2);
            detailDataEditView.SetValue(Grid.ColumnSpanProperty, 8);
            detailDataEditView.Width = 1200;
            this.LayoutRoot.Children.Add(detailDataEditView);
        }

        private void MarketingDepartmentQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var marketingDepartment = queryWindow.SelectedEntities.Cast<MarketingDepartment>().FirstOrDefault();
            if(marketingDepartment == null)
                return;
            var sihCreditInfo = this.DataContext as SIHCreditInfo;
            if(sihCreditInfo == null)
                return;
            sihCreditInfo.MarketingDepartmentId = marketingDepartment.Id;
            this.MarketingDepartmentName = marketingDepartment.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        public string MarketingDepartmentName {
            get {
                return this.marketingDepartmentName;
            }
            set {
                this.marketingDepartmentName = value;
                this.OnPropertyChanged("MarketingDepartmentName");
            }
        }

        protected override void Reset() {
            var credenceApplication = this.DataContext as CredenceApplication;
            if(credenceApplication == null)
                return;
            if(this.DomainContext.CredenceApplications.Contains(credenceApplication)) {
                this.DomainContext.CredenceApplications.Detach(credenceApplication);
            }
        }

        protected override void OnEditCancelled() {
            this.DataContext = null;
            // this.Reset();
            base.OnEditCancelled();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }
    }
}