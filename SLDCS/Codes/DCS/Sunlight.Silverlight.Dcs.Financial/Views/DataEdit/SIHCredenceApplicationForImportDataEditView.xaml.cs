﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataEdit {
    public partial class SIHCredenceApplicationForImportDataEditView {
        private const string EXPORT_DATA_FILE_NAME = "导出SIH授信（服务商）信用申请单模板.xlsx";
        private ICommand exportFileCommand;
        public SIHCredenceApplicationForImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return "导入SIH授信（服务商）信用申请单";
            }
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        private void CreateUI() {
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.ImportSIHCreditInfoAsync(fileName);
            this.ExcelServiceClient.ImportSIHCreditInfoCompleted -= ExcelServiceClientOnImportSIHCreditInfoCompleted;
            this.ExcelServiceClient.ImportSIHCreditInfoCompleted += ExcelServiceClientOnImportSIHCreditInfoCompleted;
        }

        private void ExcelServiceClientOnImportSIHCreditInfoCompleted(object sender, ImportSIHCreditInfoCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!this.HasImportingError) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Notification_CompletedImport, 5);
            } else {
                if(!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if(!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccount_DealerCode,
                                                IsRequired = true
                                            },new ImportTemplateColumn {
                                                Name = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccount_DealerName
                                            },new ImportTemplateColumn {
                                                Name = FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfoDetail_ThisSIHCredit,
                                                IsRequired = true
                                            },new ImportTemplateColumn {
                                                Name = FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfoDetail_ValidationTime,
                                                IsRequired = true
                                            },new ImportTemplateColumn {
                                                Name = FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfoDetail_ExpireTime
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }


    }
}
