﻿
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataEdit {
    public partial class SupplierOpenAccountAppDataEditView : INotifyPropertyChanged {
        private KeyValueManager keyValueManager;
        private ObservableCollection<KeyValuePair> kvPartsSalesCategorys;
        private string supplierCompanyName;
        public event PropertyChangedEventHandler PropertyChanged;
        private readonly string[] kvNames = {
           "WorkflowOfSimpleApproval_Status" 
        };

        public ObservableCollection<KeyValuePair> KvPartsSalesCategorys {
            get {
                return this.kvPartsSalesCategorys ?? (this.kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>());
            }
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_SupplierOpenAccountApp;
            }
        }

        public SupplierOpenAccountAppDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(kvNames);
            this.KeyValueManager.LoadData();

            this.Loaded += this.SupplierOpenAccountAppDataEditView_Loaded;
        }


        protected KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        public string SupplierCompanyName {
            get {
                return this.supplierCompanyName;
            }
            set {
                this.supplierCompanyName = value;
                this.OnPropertyChanged("SupplierCompanyName");
            }
        }

        private void CreateUI() {
            var queryWindow = DI.GetQueryWindow("SupplierInformation");
            queryWindow.SelectionDecided += this.responsibleUnitQueryWindow_SelectionDecided;
            this.popupTextBoxSupplierCompany.PopupContent = queryWindow;
            RadMaskedTextBox1.SelectionStart = 0;
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(v => v.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && v.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvPartsSalesCategorys.Clear();
                foreach(var item in loadOp.Entities) {
                    this.KvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.Name,
                        UserObject = item
                    });
                }
                this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            }, null);
        }
        protected override bool OnRequestCanSubmit()
        {
            this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            return true;
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetSupplierOpenAccountAppWithDetailsQuery().Where(v => v.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                    this.RadMaskedTextBox1.Text = entity.AccountInitialDeposit.ToString();
                    this.SupplierCompanyName = entity.Company.Name;
                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            var payOutBill = this.DataContext as SupplierOpenAccountApp;
            if(payOutBill == null)
                return;
            payOutBill.ValidationErrors.Clear();
            if(payOutBill.SupplierCompanyId == default(int)) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_PayOutBill_SupplierCompanyIdIsNull);
                return;
            }
            if(payOutBill.PartsSalesCategoryId == null) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_PayOutBill_PartsSalesCategoryIdIsNull);
                return;
            }
            if(payOutBill.AccountInitialDeposit < 0) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_SupplierOpenAccountApp_AccountInitialDepositThan0);
                return;
            }

            if(payOutBill.HasValidationErrors)
                return;
            if(payOutBill.EntityState == EntityState.New)
                payOutBill.Status = (int)DcsWorkflowOfSimpleApprovalStatus.新建;
            ((IEditableObject)payOutBill).EndEdit();
            base.OnEditSubmitting();
        }

        private void SupplierOpenAccountAppDataEditView_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            this.SupplierCompanyName = string.Empty;
            if(EditState == DataEditState.New) {
                this.RadMaskedTextBox1.Text = null;
            }
        }

        private void responsibleUnitQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var supplierInformation = queryWindow.SelectedEntities.Cast<BranchSupplierRelation>().FirstOrDefault();
            if(supplierInformation == null)
                return;
            var payoutBill = this.DataContext as SupplierOpenAccountApp;
            if(payoutBill == null)
                return;
            payoutBill.SupplierCompanyId = supplierInformation.Company.Id;
            this.SupplierCompanyName = supplierInformation.Company.Name;

            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        public void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        protected override void OnEditCancelled() {
            this.DataContext = null;
            // this.Reset();
            base.OnEditCancelled();
        }

        private void radMaskedTextBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e) {
            var payOutBill = this.DataContext as SupplierOpenAccountApp;
            if(payOutBill == null || RadMaskedTextBox1.Value == null)
                return;
            if((e.PlatformKeyCode == 8 || e.PlatformKeyCode == 46) && payOutBill.AccountInitialDeposit == default(decimal)) {
                RadMaskedTextBox1.SelectionStart = 0;
                RadMaskedTextBox1.Value = null;
            }
        }
    }
}
