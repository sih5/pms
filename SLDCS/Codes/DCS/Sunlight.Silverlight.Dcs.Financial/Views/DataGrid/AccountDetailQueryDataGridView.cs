﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using FilterOperator = Sunlight.Silverlight.Core.Model.FilterOperator;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class AccountDetailQueryDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "AccountPayment_BusinessType"
        };

        public AccountDetailQueryDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                      new ColumnItem {
                        Name = "CustomerAccount.Company.Code",
                        Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Code
                    }, new ColumnItem {
                        Name = "CustomerAccount.Company.Name",
                        Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Name
                    }, new ColumnItem {
                        Name = "CustomerAccount.AccountGroup.Name",
                        Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_AccountGroupName
                    }, new ColumnItem {
                        Name = "ChangeAmount"
                    }, new ColumnItem {
                        Name = "ProcessDate"
                    }, new KeyValuesColumnItem {
                        Name = "BusinessType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "SourceCode"
                    }, new ColumnItem {
                        Name = "Summary"
                    }, new ColumnItem {
                        Name = "BeforeChangeAmount"
                    }, new ColumnItem {
                        Name = "AfterChangeAmount"
                    }, new ColumnItem {
                        Name = "Debit"
                    }, new ColumnItem {
                        Name = "Credit"
                    }
                };
            }
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {

           var filters = this.FilterItem as CompositeFilterItem;
           if(filters != null) {
               var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
               switch(parameterName) {
                   case "customerCompanyId":
                       return BaseApp.Current.CurrentUserData.EnterpriseId;
                   case "sourceCode":
                       return filters.Filters.Single(item => item.MemberName == "SourceCode").Value;
               }
           }
            return OnRequestQueryParameter(queryName, parameterName);
        }
        protected override Type EntityType {

            get {
                return typeof(CustomerAccountHisDetail);
            }
        }
        protected override string OnRequestQueryName() {
            return "GetCustomerAccountHisDetailsForWithCompany";
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null)
                foreach(var filter in compositeFilterItem.Filters) {
                    var timeFileItem = filter as CompositeFilterItem;
                    if(timeFileItem == null) {
                        newCompositeFilterItem.Filters.Add(filter);
                    } else {
                        var time = new CompositeFilterItem();
                        time.Operator = FilterOperator.IsLessThan;
                        time.Filters.Add(new FilterItem {
                            MemberName = "ProcessDate",
                            MemberType = typeof(DateTime),
                            Operator = FilterOperator.IsGreaterThanOrEqualTo,
                            Value = timeFileItem.Filters.ElementAt(0).Value as DateTime?
                        });
                        time.Filters.Add(new FilterItem {
                            MemberName = "ProcessDate",
                            MemberType = typeof(DateTime),
                            Operator = FilterOperator.IsLessThan,
                            Value = timeFileItem.Filters.ElementAt(1).Value as DateTime?
                        });
                        newCompositeFilterItem.Filters.Add(time);
                    }
                }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["ChangeAmount"]).DataFormatString = "c2";
        }
    }
}
