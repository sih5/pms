﻿using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class AccountGroupDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "MasterData_Status"
        };
        public AccountGroupDataGridView() {
            this.KeyValueManager.Register(kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[kvNames[0]]
                    },new ColumnItem {
                        Name = "SalesCompanyName",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_AccountGroup_SalesCompanyName
                    },new ColumnItem {
                        Name = "Code"
                    },new ColumnItem {
                        Name = "Name"
                    },new ColumnItem {
                        Name = "CreatorName"
                    },new ColumnItem {
                        Name = "CreateTime"
                    },new ColumnItem {
                        Name = "ModifierName"
                    },new ColumnItem {
                        Name = "ModifyTime"
                    },new ColumnItem {
                        Name = "AbandonerName"
                    },new ColumnItem {
                        Name = "AbandonTime"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
        }

        protected override string OnRequestQueryName() {
            return "GetAccountGroupWithCompanies";

        }


        protected override System.Type EntityType {
            get {
                return typeof(AccountGroup);
            }
        }
    }
}
