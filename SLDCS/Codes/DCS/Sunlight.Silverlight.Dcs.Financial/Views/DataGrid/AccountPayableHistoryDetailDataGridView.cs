﻿using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class AccountPayableHistoryDetailDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "AccountPayOut_BusinessType"
        };
        public AccountPayableHistoryDetailDataGridView() {
            this.KeyValueManager.Register(kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[]{
                    new ColumnItem{
                       Name="SupplierAccount.Company.Name",
                       Title=FinancialUIStrings.DataGridView_ColumnItem_Title_Supplier_Name
                    },new ColumnItem{
                        Name="SupplierAccount.Company.SupplierCode",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_Supplier_SupplierCode
                    },new ColumnItem{
                        Name="SupplierAccount.Company.Code",
                        Title=FinancialUIStrings.DataGridView_ColumnItem_Title_Supplier_Code
                    },new ColumnItem{
                        Name="SupplierAccount.PartsSalesCategory.Name",
                        Title=FinancialUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategoryName
                    },new ColumnItem{
                        Name="ChangeAmount",
                        Title=FinancialUIStrings.DataGridView_ColumnItem_Title_ChangeAmount
                    },new ColumnItem{
                        Name="ProcessDate",
                        Title=FinancialUIStrings.DataGridView_ColumnItem_Title_ProcessDate
                    },new KeyValuesColumnItem{
                        Name="BusinessType",
                        Title=FinancialUIStrings.DataGridView_ColumnItem_Title_BusinessType,
                        KeyValueItems = this.KeyValueManager[kvNames[0]]
                    },new ColumnItem{
                        Name="SourceCode",
                        Title=FinancialUIStrings.DataGridView_ColumnItem_Title_SourceCode
                    },new ColumnItem{
                        Name="Summary",
                        Title=FinancialUIStrings.DataGridView_ColumnItem_Title_Summary
                    }, new ColumnItem {
                        Name = "BeforeChangeAmount"
                    }, new ColumnItem {
                        Name = "AfterChangeAmount"
                    }, new ColumnItem {
                        Name = "Debit"
                    }, new ColumnItem {
                        Name = "Credit"
                    }

                };

            }
        }
        protected override System.Type EntityType {
            get {
                return typeof(AccountPayableHistoryDetail);
            }
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filter = this.FilterItem as CompositeFilterItem;
            if(filter == null)
                return null;
            switch(parameterName) {
                case "BuyCompanyId":
                    return BaseApp.Current.CurrentUserData.EnterpriseId;
            }
            return OnRequestQueryParameter(queryName, parameterName);
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.DataPager.PageSize = 300;
            ((GridViewDataColumn)this.GridView.Columns["ProcessDate"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["Credit"]).DataFormatString = "N2";
            ((GridViewDataColumn)this.GridView.Columns["Debit"]).DataFormatString = "N2";
            ((GridViewDataColumn)this.GridView.Columns["AfterChangeAmount"]).DataFormatString = "N2";
            ((GridViewDataColumn)this.GridView.Columns["BeforeChangeAmount"]).DataFormatString = "N2";
            ((GridViewDataColumn)this.GridView.Columns["ChangeAmount"]).DataFormatString = "c2";
        }
        protected override string OnRequestQueryName() {
            return "GetAccountPayableHistoryDetailWithCompany";
        }
    }
}
