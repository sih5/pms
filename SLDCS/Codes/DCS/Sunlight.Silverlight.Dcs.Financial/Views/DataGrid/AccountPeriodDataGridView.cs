﻿using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid
{
    public class AccountPeriodDataGridView : DcsDataGridViewBase
    {
        private readonly string[] kvNames = {
            "AccountPeriodStatus"
        };
        public AccountPeriodDataGridView()
        {
            this.KeyValueManager.Register(kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[kvNames[0]]
                    },new ColumnItem {
                        Name = "Year",
                        Title= FinancialUIStrings.DataGridView_ColumnItem_Title_AccountPeriod_Year
                    },new ColumnItem {
                        Name = "Month",
                         Title= FinancialUIStrings.DataGridView_ColumnItem_Title_AccountPeriod_Month
                    },new ColumnItem {
                        Name = "CreatorName"
                    },new ColumnItem {
                        Name = "CreateTime"
                    },new ColumnItem {
                        Name = "ModifierName"
                    },new ColumnItem {
                        Name = "ModifyTime"
                    }
                };
            }
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Multiple;
        }

        protected override string OnRequestQueryName()
        {
            return "GetAccountPeriodQuerys";

        }


        protected override System.Type EntityType
        {
            get
            {
                return typeof(AccountPeriod);
            }
        }
    }
}