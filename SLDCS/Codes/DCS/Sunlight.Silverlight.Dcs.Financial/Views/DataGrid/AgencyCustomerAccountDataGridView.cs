﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;
using FilterOperator = Sunlight.Silverlight.Core.Model.FilterOperator;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class AgencyCustomerAccountDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
          "AccountPayment_BusinessType"
        };

        public AgencyCustomerAccountDataGridView() {
            this.KeyValueManager.Register(kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "DealerCode",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_AgencyCustomerAccount_DealerCode
                    }, new ColumnItem {
                        Name = "DealerName",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_AgencyCustomerAccount_DealerName
                    }, new ColumnItem {
                        Name = "AccountGroupName",
                         Title = FinancialUIStrings.DataGridView_ColumnItem_Title_AccountGroup_Name
                    }, new ColumnItem {
                        Name = "AgencyCode",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_AgencyCustomerAccount_AgencyCode
                    }, new ColumnItem {
                        Name = "AgencyName",
                        Title =  FinancialUIStrings.DataGridView_ColumnItem_Title_AgencyCustomerAccount_AgencyName
                    }, new ColumnItem {
                        Name = "ChangeAmount",
                         Title = FinancialUIStrings.DataGridView_ColumnItem_Title_ChangeAmount ,
                         TextAlignment=TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ProcessDate",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_AgencyCustomerAccount_ProcessDate
                    }, new KeyValuesColumnItem {
                        Name = "BusinessType",
                        KeyValueItems = this.KeyValueManager[kvNames[0]],
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_BusinessType
                    }, new ColumnItem {
                        Name = "SourceCode",
                         Title = FinancialUIStrings.DataGridView_ColumnItem_Title_SourceCode
                    }, new ColumnItem {
                        Name = "Summary",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_Summary
                    }, new ColumnItem {
                        Name = "BeforeChangeAmount",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_AgencyCustomerAccount_BeforeChangeAmount,
                         TextAlignment=TextAlignment.Right                
                    }, new ColumnItem {
                        Name = "AfterChangeAmount",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_AgencyCustomerAccount_AfterChangeAmount,
                         TextAlignment=TextAlignment.Right
                    }, new ColumnItem {
                        Name = "Debit",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_AgencyCustomerAccount_Debit,
                         TextAlignment=TextAlignment.Right
                    },new ColumnItem {
                        Name = "Credit",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_AgencyCustomerAccount_Credit,
                        TextAlignment=TextAlignment.Right
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            ((GridViewDataColumn)this.GridView.Columns["ChangeAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["BeforeChangeAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["AfterChangeAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["Debit"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["Credit"]).DataFormatString = "c2";
            this.GridView.SelectionUnit = Telerik.Windows.Controls.GridView.GridViewSelectionUnit.Cell;
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualAgencyCustomerAccount);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询服务站在代理库账户明细";
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                foreach(var filterItem in compositeFilterItem.Filters.Where(filter => filter.MemberName == "BusinessType")) {
                    newCompositeFilterItem.Filters.Add(filterItem);
                }
                foreach(var filter in compositeFilterItem.Filters) {
                    var timeFileItem = filter as CompositeFilterItem;
                    if(timeFileItem != null) {
                        var time = new CompositeFilterItem();
                        time.Operator = FilterOperator.IsLessThan;
                        time.Filters.Add(new FilterItem {
                            MemberName = "ProcessDate",
                            MemberType = typeof(DateTime),
                            Operator = FilterOperator.IsGreaterThanOrEqualTo,
                            Value = timeFileItem.Filters.ElementAt(0).Value as DateTime?
                        });
                        time.Filters.Add(new FilterItem {
                            MemberName = "ProcessDate",
                            MemberType = typeof(DateTime),
                            Operator = FilterOperator.IsLessThan,
                            Value = timeFileItem.Filters.ElementAt(1).Value as DateTime?
                        });
                        newCompositeFilterItem.Filters.Add(time);
                    }
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "dealerCode":
                        return filters.Filters.Single(item => item.MemberName == "DealerCode").Value;
                    case "dealerName":
                        return filters.Filters.Single(item => item.MemberName == "DealerName").Value;
                    case "agencyCode":
                        return filters.Filters.Single(item => item.MemberName == "AgencyCode").Value;
                    case "agencyName":
                        return filters.Filters.Single(item => item.MemberName == "AgencyName").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
    }
}

