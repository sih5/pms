﻿
using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class CompaniesAndAgenciesDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                      new ColumnItem {
                        Name = "Code",
                        Title =  FinancialUIStrings.DataGridView_ColumnItem_Title_CompaniesAndAgencies_CompanyCode
                    }, new ColumnItem {
                        Name = "Name",
                      Title =  FinancialUIStrings.DataGridView_ColumnItem_Title_CompaniesAndAgencies_CompanyName
                    }
                };
            }
        }
        protected override string OnRequestQueryName() {
            return "GetCompaniesAndAgenciesByLoginCompany";
        }

        protected override Type EntityType {

            get {
                return typeof(Company);
            }
        }
    }
}
