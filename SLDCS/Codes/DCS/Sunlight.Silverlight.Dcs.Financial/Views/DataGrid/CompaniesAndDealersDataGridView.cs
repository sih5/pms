﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System.Linq;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class CompaniesAndDealersDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                      new ColumnItem {
                        Name = "MarketingDepartmentCode",
                        Title =  FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfoDetail_MarketingDepartmentCode
                    }, new ColumnItem {
                        Name = "MarketingDepartmentName",
                      Title =  FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfoDetail_MarketingDepartmentName
                    }, new ColumnItem {
                        Name = "CompanyCode",
                      Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccount_AgencyCode
                    }, new ColumnItem {
                        Name = "CompanyName",
                      Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccount_AgencyName
                    }, new ColumnItem {
                        Name = "DealerCode",
                      Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccount_DealerCode
                    }, new ColumnItem {
                        Name = "DealerName",
                      Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccount_DealerName
                    }, new ColumnItem {
                        Name = "AccountGroupCode",
                      Title =  FinancialUIStrings.DataGridView_ColumnItem_Title_AccountGroup_Code
                    }, new ColumnItem {
                        Name = "AccountGroupName",
                      Title =  FinancialUIStrings.DataGridView_ColumnItem_Title_AccountGroup_Name
                    }, new ColumnItem {
                        Name = "SIHCredit",
                      Title =  FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_SIHCredit
                    }
                };
            }
        }
        protected override string OnRequestQueryName() {
            return "GetCompaniesAndDealers";
        }

        protected override Type EntityType {

            get {
                return typeof(VirtualSIHCreditInfoDetail);
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "code":
                        return filters.Filters.First(item => item.MemberName == "Code").Value;
                    case "name":
                        return filters.Filters.First(item => item.MemberName == "Name").Value;
                    case "marketingDepartmentId":
                        return filters.Filters.First(item => item.MemberName == "MarketingDepartmentId").Value;
                    default:
                        return null;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                foreach(var item in compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).Where(filter => filter.MemberName != "Type" && filter.MemberName != "ApproveTime"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = Telerik.Windows.Controls.GridView.GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["SIHCredit"]).DataFormatString = "c2";
        }
    }
}
