﻿
using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class CompaniesForBottomStockDataGridView : DcsDataGridViewBase
    {
        private readonly string[] kvNames = {
             "Company_Type"
        };
        public CompaniesForBottomStockDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                      new ColumnItem {
                        Name = "Code",
                        Title =  FinancialUIStrings.DataGridView_ColumnItem_Title_Companies_Code
                    }, new ColumnItem {
                        Name = "Name",
                        Title =  FinancialUIStrings.DataGridView_ColumnItem_Title_Companies_Name
                    }, new KeyValuesColumnItem {
                        Name = "Type",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title =  FinancialUIStrings.DataGridView_ColumnItem_Title_Companies_Type
                    }
                };
            }
        }
        protected override string OnRequestQueryName() {
            return "GetCompaniesAndAgenciesByLoginCompany";
        }

        protected override Type EntityType {

            get {
                return typeof(Company);
            }
        }
    }
}
