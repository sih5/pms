﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;
using System.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class CredenceApplicationDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
           "CredenceApplication_Status", "Credit_Type"
        };
        public CredenceApplicationDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_Status,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                    },new ColumnItem {
                        IsSortable = false,
                        Name = "IsUplodFile",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_IsUplodFiles
                    },new ColumnItem {
                        Name = "CompanyCode",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Code
                    },new ColumnItem{
                        Name= "BusinessCode",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_BusinessCode
                    },
                    new KeyValuesColumnItem {
                        Name = "CreditType",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_CreditType,
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                    },                       
                    new ColumnItem {
                        Name = "CompanyName",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Name
                    },new ColumnItem {
                        Name = "GuarantorCompanyName",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_GuarantorCompanyName
                    },new ColumnItem {
                        Name = "AccountGroupName",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_Common_AccountGroup
                    },new ColumnItem {
                        Name = "CredenceLimit",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_AccountGroup,
                        TextAlignment=TextAlignment.Right
                    },new ColumnItem {
                        Name = "ValidationDate",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_ValidationDate
                    },new ColumnItem {
                        Name = "ExpireDate",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_ExpireDate
                    },new ColumnItem {
                        Name = "CreatorName"
                    },new ColumnItem {                       
                        Name = "CreateTime"
                    },new ColumnItem{ 
                        Name = "InitialApproverName", 
                        Title = FinancialUIStrings.DataGridView_Column_InitApprover 
                    },new ColumnItem{ 
                        Name = "InitialApproveTime", 
                        Title = FinancialUIStrings.DataGridView_Column_InitApproveTime 
                    },new ColumnItem{ 
                        Name = "CheckerName", 
                        Title = FinancialUIStrings.DataGridView_ColumnItem_CheckerName 
                    },new ColumnItem{ 
                        Name = "CheckTime", 
                        Title = FinancialUIStrings.DataGridView_ColumnItem_CheckerTime 
                    },new ColumnItem{
                        Name="ApproverName",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_ApproverName
                    } ,new ColumnItem{
                        Name="ApproveTime",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_ApproveTime
                    },new ColumnItem{ 
                        Name = "UpperCheckerName", 
                        Title = FinancialUIStrings.DataGridView_ColumnItem_UpperCheckerName
                    },new ColumnItem{ 
                        Name = "UpperCheckTime", 
                        Title = FinancialUIStrings.DataGridView_ColumnItem_UpperCheckTime 
                    },new ColumnItem{ 
                        Name = "UpperApproverName", 
                        Title = FinancialUIStrings.DataGridView_ColumnItem_UpperApproverName
                    },new ColumnItem{ 
                        Name = "UpperApproveTime", 
                        Title = FinancialUIStrings.DataGridView_ColumnItem_UpperApproveTime
                    },new ColumnItem {
                        Name = "ModifierName"
                    },new ColumnItem {
                        Name = "ModifyTime"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVirtualCredenceApplications";
        }

        protected override System.Type EntityType {
            get {
                return typeof(VirtualCredenceApplication);
            }
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return null;
            var composites = compositeFilterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
            switch(parameterName) {
                case "companyName":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "CompanyName").Value;
                case "companyCode":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "CompanyCode").Value;
                case "status":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "Status").Value;
                case "beginCreateTime":
                    var beginCreateTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                    return beginCreateTime == null ? null : beginCreateTime.Filters.First(r => r.MemberName == "CreateTime").Value;
                case "endCreateTime":
                    var endCreateTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                    return endCreateTime == null ? null : endCreateTime.Filters.Last(r => r.MemberName == "CreateTime").Value;
                case "beginApproveTime":
                    var beginApproveTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "ApproveTime");
                    return beginApproveTime == null ? null : beginApproveTime.Filters.First(r => r.MemberName == "ApproveTime").Value;
                case "endApproveTime":
                    var endApproveTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "ApproveTime");
                    return endApproveTime == null ? null : endApproveTime.Filters.Last(r => r.MemberName == "ApproveTime").Value;
                case "businessCode":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "BusinessCode").Value;
                case "creditType":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "CreditType").Value;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var newCompositeFilterItem = new CompositeFilterItem();
            return newCompositeFilterItem.ToFilterDescriptor();
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["CredenceLimit"]).DataFormatString = "c2";
        }
    }
}