﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class CredenceApplicationForImportDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name="Code",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_Code
                    }, new ColumnItem {
                            Name = "CustomerCompany.Code",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_Company_Code
                        },new ColumnItem {
                            Name = "CustomerCompany.Name",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_Company_Name
                        },new ColumnItem {
                            Name = "AccountGroup.Name",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_Common_AccountGroup
                        },new ColumnItem {
                            Name = "CredenceLimit"
                        },new ColumnItem {
                            Name = "CreditType",
                            Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_CreditType
                        },new ColumnItem {
                            Name = "ExpireDate"
                        }, new ColumnItem{
                          Name = "Remark"
                       }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(CredenceApplication);
            }
        }
    }
}
