﻿
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class CrossSalesOrderCompanyDataGridView  : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "MasterData_Status","Company_Type"
        };

        public CrossSalesOrderCompanyDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Code",
                        Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Code
                    }, new ColumnItem {
                        Name = "Name",
                        Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Name
                    },new KeyValuesColumnItem{
                        Name = "Type",
                        Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Type,
                        KeyValueItems=this.KeyValueManager[kvNames[1]]
                    },new ColumnItem {
                        Name = "ContactPerson",
                        Title="联系人"
                    }, new ColumnItem {
                        Name = "ContactPhone",
                        Title="联系人电话"
                    },  new ColumnItem {
                        Name = "CreatorName",
                        Title="创建人"
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title="创建时间"
                    }, new ColumnItem {
                        Name = "ModifierName",
                        Title="修改人"
                    }, new ColumnItem {
                        Name = "ModifyTime",
                        Title="修改时间"
                    }
                };
            }
        }

        protected override System.Type EntityType {
            get {
                return typeof(Company);
            }
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
        protected override string OnRequestQueryName() {
            return "GetCompanies";
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}