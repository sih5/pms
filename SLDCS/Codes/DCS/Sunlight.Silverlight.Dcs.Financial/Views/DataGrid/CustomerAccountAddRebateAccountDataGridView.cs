﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web.Entities;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class CustomerAccountAddRebateAccountDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "MasterData_Status","Company_Type"
        };

        public CustomerAccountAddRebateAccountDataGridView() {
            this.KeyValueManager.Register(kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[kvNames[0]]
                    },new ColumnItem {
                        Name = "CompanyCode",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Code
                    }, new ColumnItem {
                        Name = "BusinessCode",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_BusinessCode
                    }, new ColumnItem {
                        Name = "CompanyName",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Name
                    }, new KeyValuesColumnItem{
                        Name = "CompanyType",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccountHistory_Type,
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Name = "AccountGroupCode",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_AccountGroup_Code
                    }, new ColumnItem {
                        Name = "AccountGroupName",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_AccountGroup_Name
                    }, new ColumnItem {
                        Name = "AccountBalance",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_AccountBalance,
                        TextAlignment=TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ShippedProductValue",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_ShippedProductValue,
                        TextAlignment=TextAlignment.Right
                    }, new ColumnItem {
                        Name = "PendingAmount",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_PendingAmount,
                        TextAlignment=TextAlignment.Right
                    }, new ColumnItem {
                        Name = "CustomerCredenceAmount",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_CustomerCredenceAmountC,
                        TextAlignment=TextAlignment.Right                
                    }, new ColumnItem {
                        Name = "TempCreditTotalFee",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_TempCreditTotalFee                
                    }, new ColumnItem {
                        Name = "SIHCredit",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_SIHCredit                
                    }, new ColumnItem {
                        Name = "RebateAccount",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_RebateAccount,
                        TextAlignment=TextAlignment.Right
                    }, new ColumnItem {
                        Name = "UnApproveAmount",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_UnApproveAmount,
                         TextAlignment=TextAlignment.Right
                    },new ColumnItem {
                        Name = "LockBalance",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_LockBalance,
                        TextAlignment=TextAlignment.Right
                    }, new ColumnItem {
                        Name = "Balance",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_Balance,
                        TextAlignment=TextAlignment.Right
                    }, new ColumnItem {
                        Name = "UsableBalance",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_UsableBalance,
                        TextAlignment=TextAlignment.Right
                    },  new ColumnItem {
                        Name = "CreatorName",
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            ((GridViewDataColumn)this.GridView.Columns["AccountBalance"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["ShippedProductValue"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["PendingAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["CustomerCredenceAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["RebateAccount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["UnApproveAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["LockBalance"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["Balance"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["UsableBalance"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["TempCreditTotalFee"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SIHCredit"]).DataFormatString = "c2";
            this.GridView.SelectionUnit = Telerik.Windows.Controls.GridView.GridViewSelectionUnit.Cell;
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualCustomerAccount);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVirtualCustomerAccountsWithCompanyAndAccountGroup";
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "businessCode":
                        var businessCode = filters.Filters.SingleOrDefault(item => item.MemberName == "BusinessCode");
                        return businessCode == null ? null : businessCode.Value;
                    case "accountGroupId":
                        var accountGroupId = filters.Filters.SingleOrDefault(item => item.MemberName == "AccountGroupId");
                        return accountGroupId == null ? null : accountGroupId.Value;
                    case "isZeroForAccountBalance":
                        var isZeroForAccountBalance = filters.Filters.SingleOrDefault(item => item.MemberName == "IsZeroForAccountBalance");
                        return isZeroForAccountBalance == null ? null : isZeroForAccountBalance.Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "BusinessCode" && filter.MemberName != "AccountGroupId" && filter.MemberName != "IsZeroForAccountBalance"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        //protected override object OnRequestQueryParameter(string queryName, string parameterName) {
        //    switch(parameterName) {
        //        case "accountGroupId":
        //            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
        //            if(compositeFilterItem != null) {
        //                var filterItem = compositeFilterItem.Filters.SingleOrDefault(e => e.MemberName == "AccountGroupId");
        //                var accountGroupId = filterItem == null ? null : filterItem.Value;
        //                if(filterItem != null)
        //                    compositeFilterItem.Filters.Remove(filterItem);
        //                return accountGroupId;
        //            }
        //            return this.FilterItem.MemberName == "AccountGroupId" ? this.FilterItem.Value : null;
        //    }
        //    return base.OnRequestQueryParameter(queryName, parameterName);
        //}
    }
}

