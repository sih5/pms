﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web.Entities;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class CustomerAccountDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "MasterData_Status","Company_Type"
        };

        public CustomerAccountDataGridView() {
            this.KeyValueManager.Register(kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[kvNames[0]]
                    },new ColumnItem {
                        Name = "CompanyCode",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Code
                    }, new ColumnItem {
                        Name = "CompanyName",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Name
                    }, new KeyValuesColumnItem{
                        Name = "CompanyType",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccountHistory_Type,
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Name = "AccountGroupCode",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_AccountGroup_Code
                    }, new ColumnItem {
                        Name = "AccountGroupName",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_AccountGroup_Name
                    }, new ColumnItem {
                        Name = "AccountBalance",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_AccountBalance  
                    }, new ColumnItem {
                        Name = "ShippedProductValue",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_ShippedProductValue
                    }, new ColumnItem {
                        Name = "PendingAmount",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_PendingAmount
                    }, new ColumnItem {
                        Name = "CustomerCredenceAmount",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_CustomerCredenceAmountC                  
                    }, new ColumnItem {
                        Name = "TempCreditTotalFee",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_TempCreditTotalFee                
                    }, new ColumnItem {
                        Name = "SIHCredit",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_SIHCredit                
                    }, new ColumnItem {
                        Name = "RebateAccount",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_RebateAccount
                    },new ColumnItem {
                        Name = "UnApproveAmount",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_UnApproveAmount
                    },new ColumnItem {
                        Name = "LockBalance",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_LockBalance
                    }, new ColumnItem {
                        Name = "Balance",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_Balance
                    }, new ColumnItem {
                        Name = "UsableBalance",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_UsableBalance
                    },  new ColumnItem {
                        Name = "CreatorName",
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Single;
            this.GridView.SelectionUnit = Telerik.Windows.Controls.GridView.GridViewSelectionUnit.Cell;
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualCustomerAccount);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetCustomerAccountsWithCustomerCompanyAndAccountGroupNoParam";
        }

    }
}
