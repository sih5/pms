﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {

    public class CustomerAccountHisDetailDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "AccountPayment_BusinessType","Serial_Type"
        };

        public CustomerAccountHisDetailDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                      new ColumnItem {
                        Name = "AccountGroupName",
                        Title =  FinancialUIStrings.DataGridView_ColumnItem_Title_PaymentBill_AccountGroupName
                    },new ColumnItem {
                        Name = "InvoiceDate",
                        Title = FinancialUIStrings.DataEditPanel_Text_CustomerTransferBill_InvoiceDate
                    }, new ColumnItem {
                        Name = "CompanyCode",
                        Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Code
                    }, new ColumnItem {
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_BusinessCode,
                        Name = "BusinessCode"
                    }, new ColumnItem {
                        Name = "CompanyName",
                        Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Name
                    },new KeyValuesColumnItem {
                        Name = "BusinessType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_BusinessType
                    },new KeyValuesColumnItem {
                        Name = "SerialType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccountHisDetail_SerialType
                    },new ColumnItem {
                        Name = "ProcessDate",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_ProcessDate
                    },  new ColumnItem {
                        Name = "SourceCode",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_SourceCode
                    }, new ColumnItem {
                        Name = "BeforeChangeAmount",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_AgencyCustomerAccount_BeforeChangeAmount
                    }, new ColumnItem {
                        Name = "Debit",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_AgencyCustomerAccount_Debit
                    }, new ColumnItem {
                        Name = "Credit",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_AgencyCustomerAccount_Credit
                    }, new ColumnItem {
                        Name = "AfterChangeAmount",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_AgencyCustomerAccount_AfterChangeAmount
                    }, new ColumnItem {
                        Name = "Summary",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccountHisDetail_Summary
                    }, new ColumnItem {
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccountHisDetail_CreatorName,
                        Name = "CreatorName"
                    }
                };
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return null;
            var composites = compositeFilterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
            switch(parameterName) {
                case "salesCompanyId":
                    return BaseApp.Current.CurrentUserData.EnterpriseId;
                case "companyCode":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "CompanyCode").Value;
                case "companyName":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "CompanyName").Value;
                case "sourcecode":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "SourceCode").Value;
                case "businessType":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "BusinessType").Value;
                case "accountGroupId":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "AccountGroupId").Value;              
                case "businessCode":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "BusinessCode").Value;              
                case "beginProcessDate":
                    var createTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "Process");
                    return createTime == null ? null : createTime.Filters.First(r => r.MemberName == "Process").Value;
                case "endProcessDate":
                    var createTime1 = composites.FirstOrDefault(p => p.Filters.First().MemberName == "Process");
                    return createTime1 == null ? null : createTime1.Filters.Last(item => item.MemberName == "Process").Value;
                case "beginInvoiceDate":
                    var expectedPlaceDate = composites.FirstOrDefault(p => p.Filters.First().MemberName == "InvoiceDate");
                    return expectedPlaceDate == null ? null : expectedPlaceDate.Filters.First(r => r.MemberName == "InvoiceDate").Value;
                case "endInvoiceDate":
                    var expectedPlaceDate1 = composites.FirstOrDefault(p => p.Filters.First().MemberName == "InvoiceDate");
                    return expectedPlaceDate1 == null ? null : expectedPlaceDate1.Filters.Last(item => item.MemberName == "InvoiceDate").Value;
                case "serialType":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "SerialType").Value;
            }
            return OnRequestQueryParameter(queryName, parameterName);
        }


        protected override Type EntityType {

            get {
                return typeof(VirtualCustomerAccountHisDetails);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetCustomerAccountHisDetailsWithCompanyVirtual";
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var newCompositeFilterItem = new CompositeFilterItem();
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["BeforeChangeAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["Debit"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["Credit"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["AfterChangeAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["Debit"]).TextAlignment = System.Windows.TextAlignment.Right;
            ((GridViewDataColumn)this.GridView.Columns["Credit"]).TextAlignment = System.Windows.TextAlignment.Right;
            ((GridViewDataColumn)this.GridView.Columns["BeforeChangeAmount"]).TextAlignment = System.Windows.TextAlignment.Right;
            ((GridViewDataColumn)this.GridView.Columns["AfterChangeAmount"]).TextAlignment = System.Windows.TextAlignment.Right;
        }
    }

}
