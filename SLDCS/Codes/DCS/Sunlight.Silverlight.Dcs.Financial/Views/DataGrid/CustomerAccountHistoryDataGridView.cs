﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class CustomerAccountHistoryDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Company_Type"
        };

        public CustomerAccountHistoryDataGridView() {
            this.KeyValueManager.Register(kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Code
                    }, new ColumnItem {
                        Name = "Name",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Name
                    }, new KeyValuesColumnItem{
                        Name = "Type",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccountHistory_Type,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "AccountGroupCode",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_AccountGroup_Code
                    }, new ColumnItem {
                        Name = "AccountGroupName",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_AccountGroup_Name
                    }, new ColumnItem {
                        Name = "AccountBalance",
                         Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_AccountBalance,
                         TextAlignment=TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ShippedProductValue",
                         Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_ShippedProductValue,
                         TextAlignment=TextAlignment.Right
                    }, new ColumnItem {
                        Name = "PendingAmount",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_PendingAmount ,
                         TextAlignment=TextAlignment.Right
                    }, new ColumnItem {
                        Name = "CustomerCredenceAmount",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_CustomerCredenceAmountC,
                         TextAlignment=TextAlignment.Right                
                    }, new ColumnItem {
                        Name = "FlBalance",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_RebateAccount,
                         TextAlignment=TextAlignment.Right
                    }, new ColumnItem {
                        Name = "WshBalance",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_UnApproveAmount,
                         TextAlignment=TextAlignment.Right
                    },new ColumnItem {
                        Name = "SdBalance",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_LockBalance,
                        TextAlignment=TextAlignment.Right
                    }, new ColumnItem {
                        Name = "YeBalance",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_Balance,
                        TextAlignment=TextAlignment.Right
                    }, new ColumnItem {
                        Name = "KyBalance",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_UsableBalance,
                        TextAlignment=TextAlignment.Right
                    }, new ColumnItem {
                        Name = "TheDate",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccountHistory_TheDate
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            ((GridViewDataColumn)this.GridView.Columns["AccountBalance"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["ShippedProductValue"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["PendingAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["CustomerCredenceAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["FlBalance"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["WshBalance"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SdBalance"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["YeBalance"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["KyBalance"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["TheDate"]).DataFormatString = "d";
            this.GridView.SelectionUnit = Telerik.Windows.Controls.GridView.GridViewSelectionUnit.Cell;
        }

        protected override Type EntityType {
            get {
                return typeof(CustomerAccountHistory);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetCustomerAccountHistoriesWithAccountGroup";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filterItem = this.FilterItem as CompositeFilterItem;
            if(filterItem == null)
                return null;
            var dataArray = ClientVar.GetDateTimeArray();
            switch(parameterName) {
                case "theDate":
                    var dateTimeFilter = filterItem.Filters.SingleOrDefault(e => e.MemberName == "TheDate");
                    if(dateTimeFilter == null || dateTimeFilter.Value == null)
                        return null;
                    return Convert.ToDateTime(dataArray[int.Parse(dateTimeFilter.Value.ToString())].Value);
                default:
                    return null;
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                var param = new[] { "TheDate" };
                foreach(var item in compositeFilterItem.Filters.Where(e => !param.Contains(e.MemberName))) {
                    newCompositeFilterItem.Filters.Add(item);
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }


    }
}
