﻿
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class CustomerInformationBySparepartDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "MasterData_Status","Company_Type"
        };

        public CustomerInformationBySparepartDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "CustomerCompany.Code",
                        Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Code
                    }, new ColumnItem {
                        Name = "CustomerCompany.Name",
                        Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Name
                    },new KeyValuesColumnItem{
                        Name = "CustomerCompany.Type",
                        Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Type,
                        KeyValueItems=this.KeyValueManager[kvNames[1]]
                    },new ColumnItem {
                        Name = "ContactPerson"
                    }, new ColumnItem {
                        Name = "ContactPhone"
                    },  new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }
                };
            }
        }

        protected override System.Type EntityType {
            get {
                return typeof(CustomerInformation);
            }
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
        protected override string OnRequestQueryName() {
            return "GetCustomerInformationWithCustomerCompany";
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
