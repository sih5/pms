﻿
using System.Collections.Generic;
using System.Linq;
using Telerik.Windows.Data;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class CustomerInformationBySparepartForTransferDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "MasterData_Status","Company_Type"
        };

        public CustomerInformationBySparepartForTransferDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_Status,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "CompanyCode",
                        Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Code
                    }, new ColumnItem {
                        Name = "CompanyName",
                        Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Name
                    },new KeyValuesColumnItem{
                        Name = "CompanyType",
                        Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Type,
                        KeyValueItems=this.KeyValueManager[kvNames[1]]
                    },new ColumnItem {
                        Name = "ContactPerson",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerInformation_ContactPerson,
                    }, new ColumnItem {
                        Name = "ContactPhone",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerInformation_ContactPhone
                    },  new ColumnItem {
                        Name = "CreatorName",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccountHisDetail_CreatorName
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_PlannedPriceApp_CreateTime
                    }, new ColumnItem {
                        Name = "ModifierName",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerInformation_ModifierName
                    }, new ColumnItem {
                        Name = "ModifyTime",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerInformation_ModifyTime
                    }
                };
            }
        }

        protected override System.Type EntityType {
            get {
                return typeof(VirtualCustomerInformationForTransfer);
            }
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
        protected override string OnRequestQueryName() {
            return "GetCustomerInformationWithCustomerCompanyForTransfer";
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

         protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                switch (parameterName)
                {
                    case "companyCode":
                        return filters.Filters.Single(item => item.MemberName == "CompanyCode").Value;
                    case "companyName":
                        return filters.Filters.Single(item => item.MemberName == "CompanyName").Value;
                    case "companyType":
                        return filters.Filters.Single(item => item.MemberName == "CompanyType").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
    }
}
