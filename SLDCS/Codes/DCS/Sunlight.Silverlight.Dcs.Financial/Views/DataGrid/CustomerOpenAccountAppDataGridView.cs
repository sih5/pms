﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class CustomerOpenAccountAppDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "WorkflowOfSimpleApproval_Status"
        };

        public CustomerOpenAccountAppDataGridView() {
            this.KeyValueManager.Register(kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[kvNames[0]],
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_Status
                    },new ColumnItem {
                        Name = "Code",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerOpenAccountApp_Code
                    }, new ColumnItem {
                        Name = "AccountGroupName",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_AccountGroup
                    }, new ColumnItem {
                        Name = "CompanyCode",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerOpenAccountApp_Code
                    },new ColumnItem {
                        Name = "BusinessCode",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CredenceApplication_BusinessCode
                    }, new ColumnItem {
                        Name = "CompanyName",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerOpenAccountApp_Name
                    }, new ColumnItem {
                        Name = "AccountInitialDeposit",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerOpenAccountApp_AccountInitialDeposit
                    }, new ColumnItem {
                        Name = "Operator",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerOpenAccountApp_Operator
                    }, new ColumnItem {
                        Name = "Remark",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_Remark
                    },  new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    },new ColumnItem {
                        Name = "AbandonerName",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerOpenAccountApp_AbandonerName
                    },new ColumnItem {
                        Name = "AbandonTime",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerOpenAccountApp_AbandonTime
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Single;
            this.GridView.SelectionUnit = Telerik.Windows.Controls.GridView.GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["AccountInitialDeposit"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["AccountInitialDeposit"]).TextAlignment = System.Windows.TextAlignment.Right;
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return null;
            switch(parameterName) {
                case "companyName":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "CompanyName").Value;
                case "companyCode":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "CompanyCode").Value;
                case "status":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "Status").Value;
                case "beginCreateTime":
                case "endCreateTime":
                    var createTime = compositeFilterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    if(createTime == null)
                        return null;
                    if(parameterName == "beginCreateTime") {
                        return createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                    } else {
                        return createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    }
                case "businessCode":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "BusinessCode").Value;
            }
            return OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var newCompositeFilterItem = new CompositeFilterItem();
            return newCompositeFilterItem.ToFilterDescriptor();
        }
        protected override Type EntityType {
            get {
                return typeof(VirtualCustomerOpenAccountApp);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVirtualCustomerOpenAccountAppsWithCustomerCompany";
        }

    }
}
