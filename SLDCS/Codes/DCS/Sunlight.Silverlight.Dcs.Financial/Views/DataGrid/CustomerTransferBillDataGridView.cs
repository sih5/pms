﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class CustomerTransferBillDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "WorkflowOfSimpleApproval_Status","CustomerTransferBill_BusinessType"
        };

        public CustomerTransferBillDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_Status
                    }, new ColumnItem {
                        Title = FinancialUIStrings.DataEditPanel_Text_PaymentBill_InvoiceDate,
                        Name = "InvoiceDate"
                    },new ColumnItem {
                        Name = "Message",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerTransferBill_Message
                    },new ColumnItem {
                        Name = "Code",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerTransferBill_Code
                    }, new ColumnItem {
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_OutboundAccountGroup_Name,
                        Name = "OutboundAccountGroup.Name",
                    }, new ColumnItem {
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_OutboundCustomerCompany_Code,
                        Name = "OutboundCustomerCompany.Code",
                    },new ColumnItem {
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_OutboundCustomerCompany_Name,
                        Name = "OutboundCustomerCompany.Name",
                    }, new ColumnItem {
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_InboundAccountGroup_Name,
                        Name = "InboundAccountGroup.Name",
                    },new ColumnItem {
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_InboundCustomerCompany_Code,
                        Name = "InboundCustomerCompany.Code",
                    }, new ColumnItem {
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_InboundCustomerCompany_Name,
                        Name = "InboundCustomerCompany.Name",
                    }, //new ColumnItem {
                        //Name = "Amount"
                    //}, 
                    new AggregateColumnItem {
                        Name = "Amount",
                        AggregateType = AggregateType.Sum,
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_Amount
                    },
                    new ColumnItem {
                        Name = "Summary",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_Abstract
                    }, new ColumnItem {
                        Name = "CredentialDocument",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerTransferBill_CredentialDocument
                    }, new ColumnItem {
                        Name = "Operator",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_Operator
                    }, new KeyValuesColumnItem {
                        Name = "BusinessType",
                        Title= FinancialUIStrings.DataGridView_ColumnItem_Title_BusinessType,
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem {
                        Title= FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_Remark,
                        Name = "Remark"
                    }, new ColumnItem {
                        Title= FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccountHisDetail_CreatorName,
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Title= FinancialUIStrings.QueryPanel_QueryItem_Title_PlannedPriceApp_CreateTime,
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Title= FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerInformation_ModifierName,
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Title= FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerInformation_ModifyTime,
                        Name = "ModifyTime"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(CustomerTransferBill);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetCustomerTransferBillWithDetails";
        }
    }
}
