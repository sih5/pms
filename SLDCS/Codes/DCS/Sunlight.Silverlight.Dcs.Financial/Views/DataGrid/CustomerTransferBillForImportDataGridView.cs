﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class CustomerTransferBillForImportDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name="BusinessType",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_BusinessType
                    },new ColumnItem {
                        Name = "InvoiceDate",
                        Title = FinancialUIStrings.DataEditPanel_Text_CustomerTransferBill_InvoiceDate
                    }, new ColumnItem {
                        Name = "OutboundAccountGroup.Name",
                        Title = FinancialUIStrings.DataEditPanel_Text_CustomerTransferBill_OutboundAccountGroupName
                    },new ColumnItem {
                        Name = "OutboundCustomerCompany.Code",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_OutboundCustomerCompany_Code
                    },new ColumnItem {
                        Name = "OutboundCustomerCompany.Name",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_OutboundCustomerCompany_Name
                    },new ColumnItem {
                        Name = "InboundAccountGroup.Name",
                        Title = FinancialUIStrings.DataEditPanel_Text_CustomerTransferBill_InboundAccountGroupName
                    },new ColumnItem {
                        Name = "InboundCustomerCompany.Code",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_InboundCustomerCompany_Code
                    },new ColumnItem {
                        Name = "InboundCustomerCompany.Name",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_InboundCustomerCompany_Name
                    },new ColumnItem {
                        Name = "Amount",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_Amount
                    },new ColumnItem {
                        Name = "Summary",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_Abstract
                    },new ColumnItem {
                        Name = "Operator",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_Operator
                    }, new ColumnItem{
                        Name = "Remark"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(CustomerTransferBill);
            }
        }
    }
}