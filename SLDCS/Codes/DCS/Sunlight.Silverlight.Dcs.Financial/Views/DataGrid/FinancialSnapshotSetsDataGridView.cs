﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class FinancialSnapshotSetsDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Area_Category",  "Company_Type"
        };

        public FinancialSnapshotSetsDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new ColumnItem {
                        Name = "Warehouse.Name",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_Warehouse_Name
                    },new ColumnItem {
                        Name = "Company.Code",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsHistoryStock_CompanyCode
                    },new ColumnItem {
                        Name = "Company.Name",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsHistoryStock_CompanyName
                    },new ColumnItem {
                        Name = "WarehouseArea.Code",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsHistoryStock_WarehouseAreaCode
                    },new KeyValuesColumnItem {
                        Name = "WarehouseAreaCategory.Category",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsHistoryStock_WarehouseAreaCategoryCategory
                    },new ColumnItem {
                        Name = "SparePart.Code",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsHistoryStock_SparePartCode
                    },new ColumnItem {
                        Name = "SparePart.Name",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsHistoryStock_SparePartName
                    },new ColumnItem {
                        Name = "Quantity",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsHistoryStock_Quantity
                    },new ColumnItem{
                        Name="PlannedPrice",
                        Title= FinancialUIStrings.DataGridView_ColumnItem_Title_PartsHistoryStock_PlannedPrice
                    },new ColumnItem{
                        Name="PlannedPriceAmount",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsHistoryStock_PlannedPriceAmount
                    },new ColumnItem{
                        Name="SalePrice",
                        Title= FinancialUIStrings.DataGridView_ColumnItem_Title_PartsHistoryStock_SalePrice
                    },new ColumnItem{
                        Name="SaleAmount",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsHistoryStock_SaleAmount
                    },new ColumnItem{
                        Name="CreateTime",
                        Title= FinancialUIStrings.QueryPanel_QueryItem_Title_PlannedPriceApp_CreateTime
                    },new ColumnItem{
                        Name="ModifierName",
                        Title= FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerInformation_ModifierName
                    },new ColumnItem{
                        Name="ModifyTime",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerInformation_ModifyTime
                    }, new ColumnItem {
                        Name = "Branch.Name",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsHistoryStock_BranchName
                    },new ColumnItem {
                        Name = "PartsSalesCategory.Name",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategoryName
                    }
                };
            }
        }
        protected override Type EntityType {
            get {
                return typeof(PartsHistoryStock);
            }
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filterItem = this.FilterItem as CompositeFilterItem;
            if(filterItem == null)
                return null;
            switch(parameterName) {
                case "greaterThanZero":
                    var salesRegionNameFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "GreaterThanZero");
                    return salesRegionNameFilterItem == null ? null : salesRegionNameFilterItem.Value;
                default:
                    return null;
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                var param = new[] { "GreaterThanZero" };
                foreach(var item in compositeFilterItem.Filters.Where(e => !param.Contains(e.MemberName))) {
                    newCompositeFilterItem.Filters.Add(item);
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override string OnRequestQueryName() {
            return "GetPartsHistoryStocksWithSpartsParts";
        }

    }
}
