﻿using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid
{
    public class FundMonthlySettleBillDataGridView : DcsDataGridViewBase
    {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };
        public FundMonthlySettleBillDataGridView()
        {
            this.KeyValueManager.Register(kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[kvNames[0]]
                    },new ColumnItem {
                        Name = "MonthlySettleYear",
                        Title= FinancialUIStrings.QueryPanel_QueryItem_Title_AccountPeriod_MonthlySettleYear
                    },new ColumnItem {
                        Name = "MonthlySettleMonth",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_AccountPeriod_MonthlySettleMonth
                    },new ColumnItem {
                        Name = "CreatorName"
                    },new ColumnItem {
                        Name = "CreateTime"
                    },new ColumnItem {
                        Name = "ModifierName"
                    },new ColumnItem {
                        Name = "ModifyTime"
                    }
                };
            }
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
        }

        protected override string OnRequestQueryName()
        {
            return "GetFundMonthlySettleBillQuerys";
        }


        protected override System.Type EntityType
        {
            get
            {
                return typeof(FundMonthlySettleBill);
            }
        }
    }
}