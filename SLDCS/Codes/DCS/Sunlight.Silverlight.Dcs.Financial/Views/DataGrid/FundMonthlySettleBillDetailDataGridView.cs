﻿using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid
{
    public class FundMonthlySettleBillDetailDataGridView : DcsDataGridViewBase
    {
        public FundMonthlySettleBillDetailDataGridView() {
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                        Name = "CustomerCompanyCode",
                        Title= FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerOpenAccountApp_Code
                    },new ColumnItem {
                        Name = "CustomerCompanyName",
                         Title= FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerOpenAccountApp_Name
                    },new ColumnItem {
                        Name = "AccountGroupName",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_AccountGroupName
                    },new ColumnItem {
                        Name = "InitialAmount",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_FundMonthlySettleBillDetail_InitialAmount
                    },new ColumnItem {
                        Name = "ChangeAmount",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_ChangeAmount
                    },new ColumnItem {
                        Name = "FinalAmount",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_FundMonthlySettleBillDetail_FinalAmount
                    },new ColumnItem {
                        Name = "MonthlySettleYear",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_AccountPeriod_MonthlySettleYear
                    },new ColumnItem {
                        Name = "MonthlySettleMonth",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_AccountPeriod_MonthlySettleMonth
                    }
                };
            }
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Multiple;
        }

        protected override string OnRequestQueryName()
        {
            return "GetFundMonthlySettleBillDetails";
        }


        protected override System.Type EntityType
        {
            get
            {
                return typeof(FundMonthlySettleBillDetail);
            }
        }
    }
}