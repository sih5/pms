﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class HistoryStockSumSetsDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Company_Type"
        };

        public HistoryStockSumSetsDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "CompanyCode",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsHistoryStock_CompanyCode
                    },new ColumnItem {
                        Name = "CompanyName",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsHistoryStock_CompanyName
                    },new ColumnItem {
                        Name = "WarehouseCode",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsHistoryStockBill_WarehouseCode
                    },new ColumnItem {
                        Name = "WarehouseName",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsHistoryStockBill_WarehouseName
                    },new ColumnItem{
                        Name="PlannedPriceAmount",
                        Title=FinancialUIStrings.DataGridView_ColumnItem_Title_PartsHistoryStockBill_PlannedPriceAmount
                    },new ColumnItem{
                        Name="CreateTime",
                        Title=FinancialUIStrings.QueryPanel_QueryItem_Title_PlannedPriceApp_CreateTime
                    }, new ColumnItem {
                        Name = "BranchName",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsHistoryStock_BranchName
                    },new ColumnItem {
                        Name = "PartsSalesCategoryName",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategoryName
                    }
                };
            }
        }
        protected override Type EntityType {
            get {
                return typeof(PartsHistoryStockBill);
            }
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filterItem = this.FilterItem as CompositeFilterItem;
            if(filterItem == null)
                return null;
            switch(parameterName) {
                case "greaterThanZero":
                    var salesRegionNameFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "GreaterThanZero");
                    return salesRegionNameFilterItem == null ? null : salesRegionNameFilterItem.Value;
                default:
                    return null;
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                var param = new[] { "GreaterThanZero" };
                foreach(var item in compositeFilterItem.Filters.Where(e => !param.Contains(e.MemberName))) {
                    newCompositeFilterItem.Filters.Add(item);
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override string OnRequestQueryName() {
            return "HistoryStockSumSets";
        }

    }
}
