using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class MarketingDepartmentSihDataGridView : DcsDataGridViewBase {
        protected readonly string[] KvNames = {
            "MarketingDepartment_Type", "BaseData_Status"
        };

        public MarketingDepartmentSihDataGridView() {
            this.KeyValueManager.Register(this.KvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.KvNames[1]]
                    }, new ColumnItem {
                        Name = "Code",
                        IsSortDescending = false,
                        Title = "分销中心编号"
                    }, new ColumnItem {
                        Name = "Name",
                        Title = "分销中心名称"
                    }, new ColumnItem{
                        Name = "PartsSalesCategory.Name"
                    }, new KeyValuesColumnItem {
                        Name = "BusinessType",
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]]
                    },new ColumnItem {
                        Name = "Phone"
                    }, new ColumnItem {
                        Name = "Fax"
                    }, new ColumnItem {
                        Name = "PostCode"
                    }, new ColumnItem {
                        Name = "ProvinceName",
                        Title = "省"
                    }, new ColumnItem {
                        Name = "CityName",
                        Title = "市"
                    }, new ColumnItem {
                        Name = "CountyName",
                        Title = "县"
                    },new ColumnItem{
                       Name="CreatorName"
                    },new ColumnItem{
                       Name="CreateTime"
                    },new ColumnItem{
                       Name="ModifierName"
                    },new ColumnItem{
                       Name="ModifyTime"
                    }, new ColumnItem {
                        Name = "Branch.Name"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetMarketingDepartmentsForCreditInfo";
        }

        protected override Type EntityType {
            get {
                return typeof(MarketingDepartment);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
        }
    }
}
