﻿
using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class PartsPlannedPriceDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePart.Code"
                    },new ColumnItem {
                        Name = "SparePart.Name"
                    },new ColumnItem {
                        Name = "PartsSalesCategoryName",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PlannedPriceApp_PartsSalesCategory
                    },new ColumnItem {
                        Name = "PlannedPrice"
                    },new ColumnItem {
                        Name = "CreatorName"
                    },new ColumnItem {
                        Name = "CreateTime"
                    },new ColumnItem {
                        Name = "ModifierName"
                    },new ColumnItem {
                        Name = "ModifyTime"
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override string OnRequestQueryName() {
            return "查询配件计划价";
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPlannedPrice);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
