﻿
using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using System.Linq;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class PartsPlannedPriceYXDataGridView : DcsDataGridViewBase {
        public PartsPlannedPriceYXDataGridView() {
            this.DataContextChanged += PartsPlannedPriceYXDataGridView_DataContextChanged;
        }

        void PartsPlannedPriceYXDataGridView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var plannedPriceApp = this.DataContext as PlannedPriceApp;
            if(plannedPriceApp == null)
                return;
            foreach(var plannedPriceAppDetail in plannedPriceApp.PlannedPriceAppDetails) {
                plannedPriceAppDetail.PropertyChanged -= PlannedPriceAppDetail_PropertyChanged;
                plannedPriceAppDetail.PropertyChanged += PlannedPriceAppDetail_PropertyChanged;
            }
        }
        private void PlannedPriceAppDetail_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            var plannedPriceAppDetail = sender as PlannedPriceAppDetail;
            if(plannedPriceAppDetail == null)
                return;
            switch(e.PropertyName) {
                case "RequestedPrice":
                    plannedPriceAppDetail.RequestedPrice = decimal.Round(plannedPriceAppDetail.RequestedPrice, 2);
                    break;
            }
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsPlannedPrice_Code
                    },new ColumnItem {
                        Name = "Name",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsHistoryStock_SparePartName
                    },new ColumnItem {
                        Name = "PlannedPrice",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsPlannedPrice_PlannedPrice
                    },new ColumnItem {
                        Name = "CreatorName"
                    },new ColumnItem {
                        Name = "CreateTime"
                    },new ColumnItem {
                        Name = "ModifierName"
                    },new ColumnItem {
                        Name = "ModifyTime"
                    },new ColumnItem {
                        Name = "PartsSalesCategoryName",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PlannedPriceApp_PartsSalesCategory
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override string OnRequestQueryName() {
            return "查询配件计划价格品牌";
        }

        protected override Type EntityType {
            get {
                return typeof(GetAllPartsPlannedPrice);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return null;
            switch(parameterName) {
                case "startTime":
                case "endTime":
                    var createTime = compositeFilterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    if(createTime == null)
                        return null;
                    if(parameterName == "startTime") {
                        return createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                    } else {
                        return createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    }
            }
            return null;
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                var param = new[] { "CreateTime" };
                foreach(var item in compositeFilterItem.Filters.Where(e => !param.Contains(e.MemberName))) {
                    newCompositeFilterItem.Filters.Add(item);
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }
}
