﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class PartsRebateApplicationDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
           "WorkflowOfSimpleApproval_Status","PartsRebateDirection"
        };
        public PartsRebateApplicationDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                      new ColumnItem {
                            Name = "Code",
                            Title= FinancialUIStrings.DataEditPanel_Text_PlannedPriceApp_Code
                        },new ColumnItem {
                            Name = "CustomerCompanyCode",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Code
                        },new ColumnItem {
                            Name = "BusinessCode",
                            Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_BusinessCode
                        },new ColumnItem {
                            Name = "CustomerCompanyName",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Name
                        },new ColumnItem {
                            Name = "PartsRebateTypeName",
                            Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsRebateApplication_PartsRebateTypeName
                        },new KeyValuesColumnItem {
                            Name = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_Status
                        },new KeyValuesColumnItem {
                            Name = "RebateDirection",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            Title = FinancialUIStrings.DataEditPanel_Text_PartsRebateApplication_RebateDirection
                        },new ColumnItem {
                            Name = "AccountGroupName",
                            Title=FinancialUIStrings.DataGridView_ColumnItem_Title_AccountGroup
                        },new ColumnItem {
                            Name = "Amount",
                            TextAlignment=TextAlignment.Right,
                            Title = FinancialUIStrings.DataGridView_ColumnItem_Title_Amount
                        },new ColumnItem {
                            Name = "Motive",
                            Title = FinancialUIStrings.DataEditPanel_Text_PartsRebateApplication_Motive
                        },new ColumnItem {
                            Name = "ApprovalComment",
                            Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsRebateApplication_ApprovalComment
                        },new ColumnItem {
                            Name = "Remark",
                            Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_Remark
                        },new ColumnItem {
                            Name = "CreatorName",
                            Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccountHisDetail_CreatorName
                        },new ColumnItem {                       
                            Name = "CreateTime",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_PlannedPriceApp_CreateTime
                        },new ColumnItem {
                            Name = "ModifierName",
                            Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerInformation_ModifierName
                        },new ColumnItem {
                            Name = "ModifyTime",
                            Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerInformation_ModifyTime
                        },new ColumnItem {
                            Name = "AbandonerName",
                            Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerOpenAccountApp_AbandonerName
                        },new ColumnItem {
                            Name = "AbandonTime",
                            Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerOpenAccountApp_AbandonTime
                        },new ColumnItem {
                            Name = "ApproverName",
                            Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_ApproverName
                        },new ColumnItem {
                            Name = "ApproveTime",
                            Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_ApproveTime
                        },new ColumnItem {
                            Name = "BranchName",
                            Title = FinancialUIStrings.QueryPanel_QueryItem_Title_Branch_Name
                        },new ColumnItem {
                            Name = "PartsSalesCategoryName",
                            Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsRebateApplication_PartsSalesCategoryName
                        }
                        
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.DataPager.PageSize = 200;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.Columns["CustomerCompanyCode"].ShowColumnWhenGrouped = false;
            ((GridViewDataColumn)this.GridView.Columns["Amount"]).DataFormatString = "c2";
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return null;
            switch(parameterName) {
                case "code":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "Code").Value;
                case "customerCompanyName":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "CustomerCompanyName").Value;
                case "customerCompanyCode":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "CustomerCompanyCode").Value;
                case "accountGroupName":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "AccountGroupName").Value;
                case "partsSalesCategoryId":
                    return 221;
                case "status":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "Status").Value;
                case "partsRebateTypeName":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "PartsRebateTypeName").Value;
                case "creatorName":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "CreatorName").Value;
                case "beginCreateTime":
                case "endCreateTime":
                    var createTime = compositeFilterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    if(createTime == null)
                        return null;
                    if(parameterName == "beginCreateTime") {
                        return createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                    } else {
                        return createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    }
                case "businessCode":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "BusinessCode").Value;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var newCompositeFilterItem = new CompositeFilterItem();
            return newCompositeFilterItem.ToFilterDescriptor();
        }
        protected override string OnRequestQueryName() {
            return "GetVirtualPartsRebateApplicationWithDetails";
        }

        protected override System.Type EntityType {
            get {
                return typeof(VirtualPartsRebateApplication);
            }
        }
    }
}
