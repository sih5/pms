﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class PartsRebateApplicationForImportDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = { "PartsRebateDirection" };

        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategories = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsRebateTypes = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvAccountGroups = new ObservableCollection<KeyValuePair>();

        public PartsRebateApplicationForImportDataGridView() {
            this.KeyValueManager.Register(kvNames);
            var domainContext = this.DomainContext as DcsDomainContext ?? new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Entities != null) {
                    this.kvPartsSalesCategories.Clear();
                    foreach(var partsSalesCategory in loadOp.Entities) {
                        this.kvPartsSalesCategories.Add(new KeyValuePair {
                            Key = partsSalesCategory.Id,
                            Value = partsSalesCategory.Name
                        });
                    }
                }
            }, null);
            domainContext.Load(domainContext.GetPartsRebateTypesQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Entities != null) {
                    this.kvPartsRebateTypes.Clear();
                    foreach(var partsRebateType in loadOp.Entities) {
                        this.kvPartsRebateTypes.Add(new KeyValuePair {
                            Key = partsRebateType.Id,
                            Value = partsRebateType.Name
                        });
                    }
                }
            }, null);
            domainContext.Load(domainContext.GetAccountGroupsQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Entities != null) {
                    this.kvAccountGroups.Clear();
                    foreach(var accountGroup in loadOp.Entities) {
                        this.kvAccountGroups.Add(new KeyValuePair {
                            Key = accountGroup.Id,
                            Value = accountGroup.Name
                        });
                    }
                }
            }, null);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                      new ColumnItem {
                            Name = "CustomerCompanyCode"
                        },new ColumnItem {
                            Name = "CustomerCompanyName"
                        },new KeyValuesColumnItem {
                            Name = "PartsRebateTypeId",
                            Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsRebateApplication_PartsRebateType,
                            KeyValueItems = this.kvPartsRebateTypes
                        },new KeyValuesColumnItem {
                            Name = "AccountGroupId",
                            Title=FinancialUIStrings.DataGridView_ColumnItem_Title_AccountGroup,
                            KeyValueItems = this.kvAccountGroups
                        },new KeyValuesColumnItem {
                            Name = "RebateDirection",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        },new ColumnItem {
                            Name = "Amount"
                        },new ColumnItem {
                            Name = "Motive",
                            Title = FinancialUIStrings.DataEditPanel_Text_PartsRebateApplication_Motive
                        },new ColumnItem {
                            Name = "Remark"
                        },new KeyValuesColumnItem  {
                            Name = "PartsSalesCategoryId",
                            Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategoryName,
                            KeyValueItems = this.kvPartsSalesCategories
                        }
                        
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
        }

        protected override System.Type EntityType {
            get {
                return typeof(PartsRebateApplication);
            }
        }
        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsRebateApplications");
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }
    }
}

