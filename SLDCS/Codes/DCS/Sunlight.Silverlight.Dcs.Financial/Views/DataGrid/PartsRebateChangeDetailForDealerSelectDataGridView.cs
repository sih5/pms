﻿
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class PartsRebateChangeDetailForDealerSelectDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsRebateChangeDetail_SourceType"
        };

        public PartsRebateChangeDetailForDealerSelectDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new ColumnItem {
                        Name = "BranchName",
                        Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_Branch_Name
                     }, new ColumnItem {
                        Name = "CompanyCode",
                        Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Code
                    }, new ColumnItem {
                        Name = "CompanyName",
                        Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Name
                    }, new ColumnItem {
                        Name = "SourceCode",
                        Title = Utils.GetEntityLocalizedName(typeof(PartsRebateChangeDetail),"SourceCode")
                    }, new KeyValuesColumnItem {
                        Name = "SourceType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = Utils.GetEntityLocalizedName(typeof(PartsRebateChangeDetail),"SourceType")
                    }, new ColumnItem {
                        Name = "AccountGroupName",
                         Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_AccountGroup
                    }, new ColumnItem {
                        Name = "Amount",
                        TextAlignment=TextAlignment.Right,
                        Title = Utils.GetEntityLocalizedName(typeof(PartsRebateChangeDetail),"Amount")
                    }, new ColumnItem {
                        Name = "PartsRebateType",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsRebateChangeDetail_PartsRebateType
                    }, new ColumnItem {
                        Name = "Motive",
                        Title = FinancialUIStrings.DataEditPanel_Text_PartsRebateApplication_Motive
                    }, new ColumnItem {
                        Name = "Remark",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_Remark
                    }, new ColumnItem {
                        Name = "CreatorName",
                        Title = Utils.GetEntityLocalizedName(typeof(PartsRebateChangeDetail),"CreatorName")
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title = Utils.GetEntityLocalizedName(typeof(PartsRebateChangeDetail),"CreateTime")
                    }
                };
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["Amount"]).DataFormatString = "c2";
        }
        protected override System.Type EntityType {

            get {
                return typeof(VirtualPartsRebateChangeDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVirtualPartsRebateChangeDetailForDealerWithDetails";
        }
    }
}