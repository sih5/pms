﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class PartsRebateChangeDetailForSelectDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsRebateChangeDetail_SourceType"
        };

        public PartsRebateChangeDetailForSelectDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new ColumnItem {
                        Name = "CompanyCode",
                        Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Code
                    }, new ColumnItem {
                        Name = "BusinessCode",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_BusinessCode
                    }, new ColumnItem {
                        Name = "CompanyName",
                        Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerCompany_Name
                    }, new ColumnItem {
                        Name = "SourceCode",
                        Title= FinancialUIStrings.QueryPanel_QueryItem_Title_PartsRebateChangeDetail_SourceCode
                    }, new KeyValuesColumnItem {
                        Name = "SourceType",
                        Title= FinancialUIStrings.QueryPanel_QueryItem_Title_PartsRebateChangeDetail_SourceType,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "AccountGroupName",
                         Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_AccountGroup
                    }, new ColumnItem {
                        Name = "Amount",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_Amount,
                        TextAlignment=TextAlignment.Right
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "BranchName",
                        Title =  FinancialUIStrings.QueryPanel_QueryItem_Title_Branch_Name
                     }
                };
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["Amount"]).DataFormatString = "c2";
        }


        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return null;
            switch(parameterName) {
                case "companyName":
                    var customerCode = compositeFilterItem.Filters.SingleOrDefault(item => item.MemberName == "CompanyName");
                    return customerCode == null ? null : customerCode.Value;
                case "companyCode":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "CompanyCode").Value;
                case "branchId":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "BranchId").Value;
                case "accountGroupId":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "AccountGroupId").Value;
                case "sourceType":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "SourceType").Value;
                case "sourceCode":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "SourceCode").Value;
                case "beginCreateTime":
                case "endCreateTime":
                    var createTime = compositeFilterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    if(createTime == null)
                        return null;
                    if(parameterName == "beginCreateTime") {
                        return createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                    } else {
                        return createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    }
                //case "accountGroupName":
                //    return compositeFilterItem.Filters.First(r => r.MemberName == "AccountGroupName").Value;
                case "businessCode":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "BusinessCode").Value;
            }
            return OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var newCompositeFilterItem = new CompositeFilterItem();
            return newCompositeFilterItem.ToFilterDescriptor();
        }
        protected override System.Type EntityType {

            get {
                return typeof(VirtualPartsRebateChangeDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVirtualPartsRebateChangeDetailWithDetails";
        }
    }
}
