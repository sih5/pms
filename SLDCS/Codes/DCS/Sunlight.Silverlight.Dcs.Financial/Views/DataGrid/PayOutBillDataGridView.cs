﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class PayOutBillDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[]{
         "WorkflowOfSimpleApproval_Status","PayOutBill_PaymentMethod"};
        public PayOutBillDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new KeyValuesColumnItem{
                        Name="Status",
                         KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem{
                        Name="Code",
                        Title=FinancialUIStrings.DataGridView_ColumnItem_Title_Code
                    },new ColumnItem{
                        Name="Company.Code",
                        Title=FinancialUIStrings.DataGridView_ColumnItem_Title_Supplier_Code
                    },new ColumnItem{
                        Name="Company.Name",
                        Title=FinancialUIStrings.DataGridView_ColumnItem_Title_Supplier_Name
                    },new ColumnItem{
                        Name="Amount",
                        Title=FinancialUIStrings.DataGridView_ColumnItem_Title_Amount
                    },new KeyValuesColumnItem{
                        Name="PaymentMethod",
                        KeyValueItems=this.KeyValueManager[this.kvNames[1]],
                        Title=FinancialUIStrings.DataGridView_ColumnItem_Title_PaymentMethod
                    },new ColumnItem{
                        Name="Summary",
                        Title=FinancialUIStrings. DataGridView_ColumnItem_Title_Summary    
                    },new ColumnItem{
                        Name="BankAccount.BankAccountNumber",
                        Title=FinancialUIStrings.QueryPanel_QueryItem_Title_PayOutBill_BankAccountNumber
                    },new ColumnItem{
                        Name="CreatorName"
                    },new ColumnItem{
                        Name="CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }

                };
            }
        }
        protected override string OnRequestQueryName() {
            return "GetPayOutBillWithCompany";
        }

        protected override Type EntityType {
            get {
                return typeof(PayOutBill);

            }
        }
        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

    }
}
