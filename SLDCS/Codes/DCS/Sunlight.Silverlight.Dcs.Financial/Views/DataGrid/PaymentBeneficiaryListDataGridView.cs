﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class PaymentBeneficiaryListDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "WorkflowOfSimpleApproval_Status"
        };

        public PaymentBeneficiaryListDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Code",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PaymentBeneficiaryList_Code
                    },new ColumnItem {
                        Name = "Company.Code",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_Company_Code
                    }, new ColumnItem {
                        Name = "Company.Name",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_Company_Name
                    },new ColumnItem {
                        Name = "Amount"
                    }, new ColumnItem {
                        Name = "Summary"
                    },new ColumnItem {
                        Name = "PaymentBillCode"
                    }, new ColumnItem {
                        Name = "BillNumber"
                    }, new ColumnItem {
                        Name = "PaymentBill.BankAccount.BankAccountNumber",
                        Title=FinancialUIStrings.DataGridView_ColumnItem_Title_BankAccount_BankAccountNumber
                    }, new ColumnItem {
                        Name = "Operator"
                    },new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    } 
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PaymentBeneficiaryList);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPaymentBeneficiaryListsWithDetails";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ModifyTime"]).DataFormatString = "d";
        }
    }
}
