﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class PaymentBeneficiaryListForEditDataGridView : DcsDataGridViewBase {
        private readonly ObservableCollection<KeyValuePair> kvAccountGroup = new ObservableCollection<KeyValuePair>();
        public PaymentBeneficiaryListForEditDataGridView() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = this.DomainContext as DcsDomainContext;
            if(dcsDomainContext == null)
                return;
            dcsDomainContext.Load(dcsDomainContext.GetAccountGroupsQuery().Where(v => v.SalesCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && v.Status != (int)DcsPaymentBillStatus.作废), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var accountGroup in loadOp.Entities) {
                    this.kvAccountGroup.Add(new KeyValuePair {
                        Key = accountGroup.Id,
                        Value = accountGroup.Name,
                        UserObject = accountGroup
                    });
                }
            }, null);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "AccountGroupId",
                        Title =  FinancialUIStrings.DataGridView_ColumnItem_Title_PaymentBeneficiaryList_AccountGroup,
                        KeyValueItems = this.kvAccountGroup
                    }, new ColumnItem {
                        Name = "Amount"
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.CanUserInsertRows = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PaymentBeneficiaryLists");
        }

        protected override Type EntityType {
            get {
                return typeof(PaymentBeneficiaryList);
            }
        }
    }
}
