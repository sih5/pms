﻿
using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class PaymentBeneficiaryListForViewDataGridView : DcsDataGridViewBase {
        public PaymentBeneficiaryListForViewDataGridView() {
            this.DataContextChanged += this.PaymentBeneficiaryListForViewDataGridView_DataContextChanged;
        }

        private void PaymentBeneficiaryListForViewDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var paymentBill = e.NewValue as VirtualPaymentBill;
            if(paymentBill == null || paymentBill.Id == default(int))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "PaymentBillId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo
                };
            this.FilterItem.Value = paymentBill.Id;
            this.ExecuteQueryDelayed();
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "AccountGroup.Name",
                        Title = FinancialUIStrings.DataGridView_Title_PaymentBeneficiaryList_AccountGroup
                    },new ColumnItem {
                        Name = "Amount"
                    },new ColumnItem {
                        Name = "CreateTime"
                    }
                };
            }
        }
        protected override Type EntityType {
            get {
                return typeof(PaymentBeneficiaryList);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPaymentBeneficiaryListsWithAccountGroups";
        }
    }
}
