﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class PaymentBillDataGridView : DcsDataGridViewBase {

        public PaymentBillDataGridView() {
            this.KeyValueManager.Register(kvNames);
        }

        private readonly string[] kvNames = {
            "PaymentBill_Status","PaymentBill_PaymentType"
        };

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_Status
                    }, new ColumnItem {
                        Name = "InvoiceDate",
                        Title= FinancialUIStrings.DataEditPanel_Text_CustomerTransferBill_InvoiceDate
                    },new ColumnItem {
                        Name  = "Message",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerTransferBill_Message
                    },new ColumnItem {
                        Name  = "Code",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PaymentBillCode
                    },new ColumnItem {
                        Name = "AccountGroupName",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PaymentBill_AccountGroupName
                    }, new ColumnItem {
                        Name = "CustomerCompanyName",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PaymentBill_Name
                    }, new ColumnItem {
                        Name = "CustomerCompanyCode",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PaymentBill_Code
                    }, new ColumnItem {
                        Name = "BusinessCode",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_BusinessCode
                    }, 
                    new AggregateColumnItem {
                        Name = "Amount",
                        AggregateType = AggregateType.Sum,
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerOpenAccountApp_AccountInitialDeposit
                    },
                   new KeyValuesColumnItem {
                        Name = "PaymentMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title = FinancialUIStrings.DataEditPanel_Text_PaymentBill_PaymentMethod
                    },new ColumnItem {
                        Name = "Summary",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_Abstract
                    }, new ColumnItem {
                        Name = "BankAccountNumber",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_PaymentBill_BankAccountNumber
                    }, new ColumnItem {
                        Name = "Operator",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_Operator
                    }, new ColumnItem {
                        Name = "Drawer",
                        Title= FinancialUIStrings.DataGridView_ColumnItem_Title_PaymentBill_Drawer
                    }, new ColumnItem {
                        Name = "IssueDate",
                        Title= FinancialUIStrings.DataGridView_ColumnItem_Title_PaymentBill_IssueDate
                    }, new ColumnItem {
                        Name = "DueDate",
                        Title= FinancialUIStrings.DataGridView_ColumnItem_Title_PaymentBill_DueDate
                    }, new ColumnItem {
                        Name = "PayBank",
                        Title= FinancialUIStrings.DataGridView_ColumnItem_Title_PaymentBill_PayBank
                    }, new ColumnItem {
                        Name = "MoneyOrderNum",
                        Title= FinancialUIStrings.DataGridView_ColumnItem_Title_PaymentBill_MoneyOrderNum
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }
                };
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PaymentBeneficiaryLis"
                    }
                };
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return null;
            var composites = compositeFilterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
            switch(parameterName) {
                case "customerCompanyName":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "CustomerCompanyName").Value;
                case "customerCompanyCode":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "CustomerCompanyCode").Value;
                case "paymentMethod":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "PaymentMethod").Value;
                case "status":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "Status").Value;
                case "creatorName":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "CreatorName").Value;
                case "beginCreateTime":
                    var beginCreateTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                    return beginCreateTime == null ? null : beginCreateTime.Filters.First(r => r.MemberName == "CreateTime").Value;
                case "endCreateTime":
                    var endCreateTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                    return endCreateTime == null ? null : endCreateTime.Filters.Last(r => r.MemberName == "CreateTime").Value;
                case "businessCode":
                    return compositeFilterItem.Filters.First(r => r.MemberName == "BusinessCode").Value;
                case "beginInvoiceDate":
                    var beginInvoiceDate = composites.FirstOrDefault(p => p.Filters.First().MemberName == "InvoiceDate");
                    return beginInvoiceDate == null ? null : beginInvoiceDate.Filters.First(r => r.MemberName == "InvoiceDate").Value;
                case "endInvoiceDate":
                    var endInvoiceDate = composites.FirstOrDefault(p => p.Filters.First().MemberName == "InvoiceDate");
                    return endInvoiceDate == null ? null : endInvoiceDate.Filters.Last(r => r.MemberName == "InvoiceDate").Value;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var newCompositeFilterItem = new CompositeFilterItem();
            return newCompositeFilterItem.ToFilterDescriptor();
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["Amount"]).TextAlignment = System.Windows.TextAlignment.Right;
            ((GridViewDataColumn)this.GridView.Columns["Amount"]).DataFormatString = "c2";
        }

        protected override string OnRequestQueryName() {
            return "GetVirtualPaymentBillWithDetails";
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPaymentBill);
            }
        }
    }
}
