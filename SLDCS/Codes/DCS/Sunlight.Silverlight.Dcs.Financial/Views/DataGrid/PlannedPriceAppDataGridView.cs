﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class PlannedPriceAppDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PlannedPriceApp_Status","RecordStatus"
        };

        public PlannedPriceAppDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "Code",
                        Title = FinancialUIStrings.DataEditPanel_Text_PlannedPriceApp_Code
                    }, new ColumnItem {
                        Name = "AmountBeforeChange"
                    }, new ColumnItem {
                        Name = "AmountDifference"
                    }, new ColumnItem {
                        Name = "AmountAfterChange"
                    },new ColumnItem {
                        Name = "PlannedExecutionTime",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PlannedPriceApp_PlannedExecutionTime
                    },new ColumnItem {
                        Name = "ActualExecutionTime",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PlannedPriceApp_ActualExecutionTime
                    },new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    },new ColumnItem {
                        Name = "PartsSalesCategoryName",
                        Title = FinancialUIStrings.DataEditPanel_Text_PlannedPriceApp_PartsSalesCategory
                    }
                };
            }
        }
        protected override string OnRequestQueryName() {
            return "GetPlannedPriceApps";
        }

        protected override Type EntityType {
            get {
                return typeof(PlannedPriceApp);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
        }
    }
}
