﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Financial.Resources;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class PlannedPriceAppDetailDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                    }, new ColumnItem {
                        Name = "SparePartName"
                    }, new ColumnItem {
                        Name = "RequestedPrice"
                    },new ColumnItem {
                        Name = "PriceBeforeChange"
                    },new ColumnItem{
                        Name="Quantity"
                    },new ColumnItem {
                        Name = "AmountDifference"
                    },new ColumnItem {
                        Name = "PriceFluctuationRatio",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PlannedPriceAppDetail_PriceFluctuationRatio
                    }
                };
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PlannedPriceAppDetails");
        }

        protected override Type EntityType {
            get {
                return typeof(PlannedPriceAppDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.CanUserDeleteRows = false;
            this.GridView.CanUserInsertRows = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["PriceFluctuationRatio"]).DataFormatString = "p2";
        }
    }
}
