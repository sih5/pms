﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class PlannedPriceAppDetailForEditDataGridView : DcsDataGridViewBase {
        private QueryWindowBase sparePartDropDownQueryWindow;
        private DcsMultiPopupsQueryWindowBase sparePartMulitQueryWindow;
        private RadWindow selectRadWindow;
        private DcsDomainContext domainContext;

        public PlannedPriceAppDetailForEditDataGridView() {
            this.DataContextChanged += PlannedPriceAppDetailForEditDataGridView_DataContextChanged;
        }

        private void PlannedPriceAppDetailForEditDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var plannedPriceApp = this.DataContext as PlannedPriceApp;
            if(plannedPriceApp == null)
                return;
            foreach(var plannedPriceAppDetail in plannedPriceApp.PlannedPriceAppDetails) {
                plannedPriceAppDetail.PropertyChanged -= PlannedPriceAppDetail_PropertyChanged;
                plannedPriceAppDetail.PropertyChanged += PlannedPriceAppDetail_PropertyChanged;
            }
        }

        private void PlannedPriceAppDetail_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            var plannedPriceAppDetail = sender as PlannedPriceAppDetail;
            if(plannedPriceAppDetail == null)
                return;
            switch(e.PropertyName) {
                case "RequestedPrice":
                    plannedPriceAppDetail.RequestedPrice = decimal.Round(plannedPriceAppDetail.RequestedPrice, 2);
                    break;
            }
        }

        private DcsDomainContext DomainContexts {
            get {
                return this.domainContext ?? (this.domainContext = new DcsDomainContext());
            }
        }

        private QueryWindowBase SparePartDropDownQueryWindow {
            get {
                if(this.sparePartDropDownQueryWindow == null) {
                    this.sparePartDropDownQueryWindow = DI.GetQueryWindow("SparePartDropDown");
                    this.sparePartDropDownQueryWindow.SelectionDecided += this.SparePartDropDownQueryWindow_SelectionDecided;
                }
                return this.sparePartDropDownQueryWindow;
            }
        }

        public DcsMultiPopupsQueryWindowBase SparePartQueryWindow {
            get {
                if(this.sparePartMulitQueryWindow == null) {
                    this.sparePartMulitQueryWindow = DI.GetQueryWindow("SparePartMultiSelect") as DcsMultiPopupsQueryWindowBase;
                    this.sparePartMulitQueryWindow.SelectionDecided += SparePartMulitQueryWindow_SelectionDecided;
                }
                return this.sparePartMulitQueryWindow;
            }
        }

        public RadWindow SelectRadWindow {
            get {
                if(selectRadWindow == null) {
                    selectRadWindow = new RadWindow();
                    selectRadWindow.Header = FinancialUIStrings.QueryPanel_Title_SparePart;
                    selectRadWindow.Content = this.SparePartQueryWindow;
                    selectRadWindow.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                }
                return this.selectRadWindow;
            }
        }

        private void SparePartDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var sparePart = queryWindow.SelectedEntities.Cast<SparePart>().FirstOrDefault();
            if(sparePart == null)
                return;
            var plannedPriceApp = this.DataContext as PlannedPriceApp;
            if(plannedPriceApp == null)
                return;
            var plannedPriceAppDetail = queryWindow.DataContext as PlannedPriceAppDetail;
            if(plannedPriceAppDetail == null)
                return;
            if(plannedPriceApp.PlannedPriceAppDetails.Any(r => sparePart.Code == r.SparePartCode)) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_SparePart_CodeSame);
                return;
            }
            plannedPriceAppDetail.SparePartId = sparePart.Id;
            plannedPriceAppDetail.SparePartCode = sparePart.Code;
            plannedPriceAppDetail.SparePartName = sparePart.Name;
            this.DomainContexts.Load(DomainContexts.获取配件变动前价格Query(plannedPriceApp.PartsSalesCategoryId, new int[] { sparePart.Id }), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled) {
                        loadOp.MarkErrorAsHandled();
                    }
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var tempEntities = loadOp.Entities.SingleOrDefault(r => r.SparePartId == plannedPriceAppDetail.SparePartId);
                plannedPriceAppDetail.PriceBeforeChange = tempEntities == null ? 0 : tempEntities.PlannedPrice;
                plannedPriceAppDetail.Quantity = tempEntities == null ? 0 : tempEntities.TotalQuantity;
                plannedPriceAppDetail.AmountDifference = (plannedPriceAppDetail.RequestedPrice - plannedPriceAppDetail.PriceBeforeChange) * plannedPriceAppDetail.Quantity;
                if(plannedPriceAppDetail.PriceBeforeChange != 0)
                    plannedPriceAppDetail.PriceFluctuationRatio = Math.Round((double)((plannedPriceAppDetail.RequestedPrice - plannedPriceAppDetail.PriceBeforeChange) / plannedPriceAppDetail.PriceBeforeChange), 4);
                else
                    plannedPriceAppDetail.PriceFluctuationRatio = null;
                plannedPriceApp.AmountBeforeChange = plannedPriceApp.PlannedPriceAppDetails.Sum(r => r.Quantity * r.PriceBeforeChange);
                plannedPriceApp.AmountAfterChange = plannedPriceApp.PlannedPriceAppDetails.Sum(r => r.Quantity * r.RequestedPrice);
                plannedPriceApp.AmountDifference = plannedPriceApp.AmountAfterChange - plannedPriceApp.AmountBeforeChange;
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }, null);
        }

        private void SparePartMulitQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var plannedPriceApp = this.DataContext as PlannedPriceApp;
            if(plannedPriceApp == null)
                return;
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var sparePart = queryWindow.SelectedEntities.Cast<SparePart>();
            if(sparePart == null)
                return;
            if(plannedPriceApp.PlannedPriceAppDetails.Any(r => sparePart.Any(item => item.Code == r.SparePartCode))) {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_SparePart_CodeSame);
                return;
            }
            foreach(var spare in sparePart) {
                var plannedPriceAppDetail = new PlannedPriceAppDetail {
                    PlannedPriceAppId = plannedPriceApp.Id,
                    SparePartId = spare.Id,
                    SparePartCode = spare.Code,
                    SparePartName = spare.Name,
                };
                plannedPriceApp.PlannedPriceAppDetails.Add(plannedPriceAppDetail);
                plannedPriceAppDetail.PropertyChanged += PlannedPriceAppDetail_PropertyChanged;
            }
            this.DomainContexts.Load(DomainContexts.获取配件变动前价格Query(plannedPriceApp.PartsSalesCategoryId, sparePart.Select(r => r.Id).ToArray()), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled) {
                        loadOp.MarkErrorAsHandled();
                    }
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var tempEntities = loadOp.Entities;
                var newSparePart = sparePart.Select(r => r.Id).ToArray();
                var newDetail = plannedPriceApp.PlannedPriceAppDetails.Where(t => newSparePart.Contains(t.SparePartId));
                foreach(var detail in newDetail) {
                    var item = tempEntities.SingleOrDefault(r => r.SparePartId == detail.SparePartId);
                    detail.PriceBeforeChange = item == null ? 0 : item.PlannedPrice;
                    detail.Quantity = item == null ? 0 : item.TotalQuantity;
                    detail.AmountDifference = (detail.RequestedPrice - detail.PriceBeforeChange) * detail.Quantity;
                    if(detail.PriceBeforeChange != 0)
                        detail.PriceFluctuationRatio = Math.Round((double)((detail.RequestedPrice - detail.PriceBeforeChange) / detail.PriceBeforeChange), 4);
                    else
                        detail.PriceFluctuationRatio = null;
                }
                plannedPriceApp.AmountBeforeChange = plannedPriceApp.PlannedPriceAppDetails.Sum(r => r.Quantity * r.PriceBeforeChange);
                plannedPriceApp.AmountAfterChange = plannedPriceApp.PlannedPriceAppDetails.Sum(r => r.Quantity * r.RequestedPrice);
                plannedPriceApp.AmountDifference = plannedPriceApp.AmountAfterChange - plannedPriceApp.AmountBeforeChange;
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }, null);
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var t = this.DataContext as PlannedPriceApp;
            e.Cancel = true;
            if(t != null && t.PartsSalesCategoryId != default(int)) {
                this.SelectRadWindow.ShowDialog();
            } else {
                UIHelper.ShowNotification(FinancialUIStrings.DataEditView_Validation_SparePart_ChooseTheBrand);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new DropDownTextBoxColumnItem {
                        Name = "SparePartCode",
                        DropDownContent = this.SparePartDropDownQueryWindow,
                        IsEditable = false
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "RequestedPrice"
                    }, new ColumnItem {
                        Name = "PriceBeforeChange",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "AmountDifference",
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "PriceFluctuationRatio",
                        IsReadOnly=true,
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PlannedPriceAppDetail_PriceFluctuationRatio
                    }
                };
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PlannedPriceAppDetails");
        }

        protected override Type EntityType {
            get {
                return typeof(PlannedPriceAppDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.CanUserInsertRows = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            this.GridView.RowStyleSelector = this.GridViewStyleSelector();
            ((GridViewDataColumn)this.GridView.Columns["PriceFluctuationRatio"]).DataFormatString = "p2";
        }

        public GridViewStyleSelector GridViewStyleSelector() {
            return new GridViewStyleSelector {
                ExecuteFunc = (item, container) => {
                    if(item is PlannedPriceAppDetail) {
                        var plannedPriceAppDetail = item as PlannedPriceAppDetail;
                        var plannedPriceApp = this.DataContext as PlannedPriceApp;
                        plannedPriceApp.PlannedPriceAppDetails.Add(plannedPriceAppDetail);
                        List<string> codesN = new List<string>();
                        List<string> codesC = new List<string>();
                        foreach(var entity in plannedPriceApp.PlannedPriceAppDetails) {
                            if(!codesN.Contains(entity.SparePartCode)) {
                                codesN.Add(entity.SparePartCode);
                            } else {
                                codesC.Add(entity.SparePartCode);
                            }
                        }
                        for(int i = 0;i < codesC.Count;i++) {
                            if(plannedPriceAppDetail.SparePartCode == codesC[i]) {
                                return this.Resources["ErrorDataBackground"] as Style;
                            }
                        }
                        if(plannedPriceAppDetail.RequestedPrice == 0) {
                            return this.Resources["ErrorDataBackground"] as Style;
                        }
                    }
                    return null;
                }
            };
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var plannedPriceApp = this.DataContext as PlannedPriceApp;
            if(plannedPriceApp == null)
                return;
            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "RequestedPrice":
                    var detail = e.Cell.ParentRow.DataContext as PlannedPriceAppDetail;
                    if(detail == null)
                        return;
                    detail.AmountDifference = (detail.RequestedPrice - detail.PriceBeforeChange) * detail.Quantity;
                    if(detail.PriceBeforeChange.HasValue && detail.PriceBeforeChange != 0)
                        detail.PriceFluctuationRatio = Math.Round((double)((detail.RequestedPrice - detail.PriceBeforeChange) / detail.PriceBeforeChange), 4);
                    else
                        detail.PriceFluctuationRatio = null;
                    plannedPriceApp.AmountAfterChange = plannedPriceApp.PlannedPriceAppDetails.Sum(r => r.Quantity * r.RequestedPrice);
                    plannedPriceApp.AmountDifference = plannedPriceApp.AmountAfterChange - plannedPriceApp.AmountBeforeChange;
                    break;
            }
        }

    }
}
