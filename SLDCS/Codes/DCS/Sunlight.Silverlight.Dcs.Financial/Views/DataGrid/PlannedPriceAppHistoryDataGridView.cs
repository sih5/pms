﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class PlannedPriceAppHistoryDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "MasterData_Status","Company_Type"
        };

        public PlannedPriceAppHistoryDataGridView() {
            this.KeyValueManager.Register(kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "PlannedPriceApp.Code",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PlannedPriceApp_Code
                    }, new ColumnItem {
                        Name = "SparePartCode",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsHistoryStock_SparePartCode
                    }, new ColumnItem{
                        Name = "SparePartName",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsHistoryStock_SparePartName
                    }, new ColumnItem {
                        Name = "PlannedPriceApp.PlannedExecutionTime",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PlannedExecutionTime
                    }, new ColumnItem {
                        Name = "PlannedPriceApp.ActualExecutionTime",
                         Title = FinancialUIStrings.DataGridView_ColumnItem_Title_ActualExecutionTime
                    }, new ColumnItem {
                        Name = "RequestedPrice",
                         Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsPlannedPrice_PlannedPrice 
                    }, new ColumnItem {
                        Name = "PriceBeforeChange",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PlannedPriceAppDetail_PriceBeforeChange
                    }, new ColumnItem {
                        Name = "Quantity",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsHistoryStock_Quantity                   
                    }, new ColumnItem {
                        Name = "AmountDifference",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PlannedPriceAppDetail_AmountDifference
                    },new ColumnItem {
                        Name = "PlannedPriceApp.CreatorName",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccountHisDetail_CreatorName
                    },new ColumnItem {
                        Name = "PlannedPriceApp.CreateTime",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_PlannedPriceApp_CreateTime
                    }, new ColumnItem {
                        Name = "PlannedPriceApp.ModifierName",
                         Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerInformation_ModifierName
                    }, new ColumnItem {
                        Name = "PlannedPriceApp.ModifyTime",
                         Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerInformation_ModifyTime
                    }, new ColumnItem {
                        Name = "PlannedPriceApp.PartsSalesCategory.Name",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategoryName
                    }
                };
            }
        }

        //protected override object OnRequestQueryParameter(string queryName, string parameterName) {
        //    var compositeFilterItem = this.FilterItem as CompositeFilterItem;
        //    if(compositeFilterItem == null)
        //        return null;
        //    var composites = compositeFilterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
        //    switch(parameterName) {
        //        case "sparePartName":
        //            return compositeFilterItem.Filters.First(r => r.MemberName == "SparePartName").Value;
        //        case "sparePartCode":
        //            return compositeFilterItem.Filters.First(r => r.MemberName == "SparePartCode").Value;
        //        case "partsSalesCategoryId":
        //            return compositeFilterItem.Filters.First(r => r.MemberName == "PartsSalesCategoryId").Value;
        //        case "beginCreateTime":
        //            var beginCreateTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
        //            return beginCreateTime == null ? null : beginCreateTime.Filters.First(r => r.MemberName == "CreateTime").Value;
        //        case "endCreateTime":
        //            var endCreateTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
        //            return endCreateTime == null ? null : endCreateTime.Filters.Last(r => r.MemberName == "CreateTime").Value;
        //    }
        //    return base.OnRequestQueryParameter(queryName, parameterName);
        //}

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override string OnRequestQueryName() {
            return "查询计划价变更履历";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Single;
            this.GridView.SelectionUnit = Telerik.Windows.Controls.GridView.GridViewSelectionUnit.Cell;
        }

        protected override Type EntityType {
            get {
                return typeof(PlannedPriceAppDetail);
            }
        }

    }
}
