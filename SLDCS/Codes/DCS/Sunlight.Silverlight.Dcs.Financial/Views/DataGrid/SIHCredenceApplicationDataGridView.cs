﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;
using System.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Core;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class SIHCredenceApplicationDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
           "SIHCreditInfoStatus"
        };
        public SIHCredenceApplicationDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_PaymentBeneficiaryList_Code
                    },new KeyValuesColumnItem {
                        Name = "Status",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_Status,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                    },new ColumnItem {
                        Name = "ATotalFee",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfo_ATotalFee
                    },new ColumnItem {
                        Name = "BTotalFee",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfo_BTotalFee
                    },new ColumnItem {
                        Name = "ChangeAmount",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfo_ChangeAmount
                    },new ColumnItem {
                        Name = "IsAttach",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_IsUplodFiles
                    },new ColumnItem {
                        Name = "CreatorName",
                    },new ColumnItem {
                        Name = "CreateTime",
                    },new ColumnItem {
                        Name = "ModifierName",
                    },new ColumnItem {
                        Name = "ModifyTime",
                    },new ColumnItem {
                        Name = "InitialApproverName",
                        Title = FinancialUIStrings.DataGridView_Column_InitApprover
                    },new ColumnItem {
                        Name = "InitialApproveTime",
                        Title = FinancialUIStrings.DataGridView_Column_InitApproveTime
                    },new ColumnItem {
                        Name = "CheckerName",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_CheckerName
                    },new ColumnItem {
                        Name = "CheckTime",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_CheckerTime
                    },new ColumnItem {
                        Name = "ApproverName",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_ApproverName
                    },new ColumnItem {
                        Name = "ApproveTime",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_ApproveTime
                    },new ColumnItem {
                        Name = "UpperApproverName",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_UpperApproverName
                    },new ColumnItem {
                        Name = "UpperCheckTime",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_UpperApproveTime
                    },new ColumnItem {
                        Name = "RejecterName",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfo_RejecterName
                    },new ColumnItem {
                        Name = "RejectTime",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfo_RejectTime
                    },new ColumnItem {
                        Name = "AbandonerName",
                    },new ColumnItem{
                        Name= "AbandonTime",
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetSIHCreditInfos";
        }

        protected override System.Type EntityType {
            get {
                return typeof(SIHCreditInfo);
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                foreach(var item in compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).Where(filter => filter.MemberName != "Type" && filter.MemberName != "ApproveTime"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch(parameterName) {
                    case "code":
                        return filters.Filters.Single(item => item.MemberName == "Code").Value;
                    case "status":
                        return filters.Filters.Single(item => item.MemberName == "Status").Value;
                    case "companyCode":
                        return filters.Filters.Single(item => item.MemberName == "CompanyCode").Value;
                    case "companyName":
                        return filters.Filters.Single(item => item.MemberName == "CompanyName").Value;
                    case "dealerCode":
                        return filters.Filters.Single(item => item.MemberName == "DealerCode").Value;
                    case "dealerName":
                        return filters.Filters.Single(item => item.MemberName == "DealerName").Value;
                    case "beginCreateTime":
                        var beginCreateTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return beginCreateTime == null ? null : beginCreateTime.Filters.First(r => r.MemberName == "CreateTime").Value;
                    case "endCreateTime":
                        var endCreateTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return endCreateTime == null ? null : endCreateTime.Filters.Last(r => r.MemberName == "CreateTime").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["ATotalFee"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["BTotalFee"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["ChangeAmount"]).DataFormatString = "c2";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] { 
                        "SIHCredenceApplicationDetail"
                    }
                };
            }
        }
    }
}