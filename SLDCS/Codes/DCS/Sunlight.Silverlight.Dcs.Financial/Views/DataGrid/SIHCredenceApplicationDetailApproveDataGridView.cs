﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Financial.Views.DataEdit;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class SIHCredenceApplicationDetailApproveDataGridView : DcsDataGridViewBase {
         private readonly string[] kvNames = new[] {
           "SIHCreditInfoDetailStatus"
        };
         public SIHCredenceApplicationDetailApproveDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "CompanyCode",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccount_AgencyCode,
                        IsReadOnly=true
                    },new ColumnItem{
                        Name= "CompanyName",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccount_AgencyName,
                        IsReadOnly=true
                    },new ColumnItem{
                        Name= "DealerCode",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccount_DealerCode,
                        IsReadOnly=true
                    },new ColumnItem{
                        Name= "DealerName",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccount_DealerName,
                        IsReadOnly=true
                    },new ColumnItem{
                        Name= "AccountGroupCode",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_AccountGroup_Code,
                        IsReadOnly=true
                    },new ColumnItem{
                        Name= "AccountGroupName",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_AccountGroup_Name,
                        IsReadOnly=true
                    },new ColumnItem{
                        Name= "SIHCredit",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_SIHCredit,
                        IsReadOnly=true
                    },
                    new ColumnItem{
                        Name= "ASIHCredit",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfoDetail_ASIHCredit,
                        IsReadOnly=true
                    },
                    new ColumnItem{
                        Name= "BSIHCredit",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfoDetail_BSIHCredit,
                        IsReadOnly=true
                    },
                    new ColumnItem{
                        Name= "ChangeAmount",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfo_ChangeAmount,
                        IsReadOnly=true
                    },
                    new ColumnItem{
                        Name= "ValidationTime",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfoDetail_ValidationTime
                    },
                    new ColumnItem{
                        Name= "ExpireTime",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfoDetail_ExpireTime
                    },new KeyValuesColumnItem {
                        Name = "Status",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_Status,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                    }
                };
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("SIHCreditInfoDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["SIHCredit"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["ChangeAmount"]).DataFormatString = "c2";
        }

        protected override Type EntityType {
            get {
                return typeof(SIHCreditInfoDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }
    }
}
