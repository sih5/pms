﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;
using System.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using System.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class SIHCredenceApplicationDetailDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
           "SIHCreditInfoDetailStatus"
        };
        public SIHCredenceApplicationDetailDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "MarketingDepartmentCode",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfoDetail_MarketingDepartmentCode,
                    },new ColumnItem {
                        Name = "MarketingDepartmentName",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfoDetail_MarketingDepartmentName,
                    },new ColumnItem {
                        Name = "CompanyCode",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccount_AgencyCode
                    },new ColumnItem{
                        Name= "CompanyName",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccount_AgencyName,
                        IsReadOnly=true
                    },new ColumnItem{
                        Name= "DealerName",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccount_DealerName,
                        IsReadOnly=true
                    },new ColumnItem{
                        Name= "DealerCode",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccount_DealerCode
                    },new ColumnItem{
                        Name= "AccountGroupCode",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_AccountGroup_Code
                    },new ColumnItem{
                        Name= "AccountGroupName",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_AccountGroup_Name,
                        IsReadOnly=true
                    },new ColumnItem{
                        Name= "ASIHCredit",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfoDetail_ASIHCredit
                    },
                    new ColumnItem{
                        Name= "BSIHCredit",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfoDetail_BSIHCredit
                    },
                    new ColumnItem{
                        Name= "ChangeAmount",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfo_ChangeAmount
                    },
                    new ColumnItem{
                        Name= "ValidationTime",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfoDetail_ValidationTime
                    },
                    new ColumnItem{
                        Name= "ExpireTime",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfoDetail_ExpireTime
                    },new KeyValuesColumnItem {
                        Name = "Status",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CredenceApplication_Status,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                    }
                };
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("SIHCreditInfoDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override Type EntityType {
            get {
                return typeof(SIHCreditInfoDetail);
            }
        }
    }
}
