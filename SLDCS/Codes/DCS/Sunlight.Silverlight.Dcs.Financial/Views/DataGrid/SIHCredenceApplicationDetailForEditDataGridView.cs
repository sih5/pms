﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Financial.Views.DataEdit;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class SIHCredenceApplicationDetailForEditDataGridView : DcsDataGridViewBase {
        private DcsMultiPopupsQueryWindowBase dealerDropDownQueryWindow;
        private DcsMultiPopupsQueryWindowBase DealerDownQueryWindow {
            get {
                if(this.dealerDropDownQueryWindow == null) {
                    this.dealerDropDownQueryWindow = DI.GetQueryWindow("CompaniesAndDealer") as DcsMultiPopupsQueryWindowBase;
                    this.dealerDropDownQueryWindow.SelectionDecided += this.DealerQueryWindow_SelectionDecided;
                    this.dealerDropDownQueryWindow.Loaded += this.DealerQueryWindow_Loaded;
                }
                return this.dealerDropDownQueryWindow;
            }
        }
        private RadWindow radQueryWindow;

        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = this.DealerDownQueryWindow,
                    Header = "查询服务站",
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen
                });
            }
        }

        private void DealerQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var details = queryWindow.SelectedEntities.Cast<VirtualSIHCreditInfoDetail>().ToArray();
            if(details == null)
                return;
            var sihCreditInfo = this.DataContext as SIHCreditInfo;
            if(sihCreditInfo == null)
                return;
            foreach(var detail in details) {
                if(!sihCreditInfo.SIHCreditInfoDetails.Any(o => o.DealerId == detail.DealerId && o.AccountGroupId == detail.AccountGroupId)) {
                     var newSIHCreditInfoDetail= new SIHCreditInfoDetail {
                        MarketingDepartmentId = detail.MarketingDepartmentId,
                        MarketingDepartmentCode = detail.MarketingDepartmentCode,
                        MarketingDepartmentName = detail.MarketingDepartmentName,
                        CompanyId = detail.CompanyId,
                        CompanyCode = detail.CompanyCode,
                        CompanyName = detail.CompanyName,
                        DealerId = detail.DealerId,
                        DealerCode = detail.DealerCode,
                        DealerName = detail.DealerName,
                        AccountGroupId = detail.AccountGroupId,
                        AccountGroupCode = detail.AccountGroupCode,
                        AccountGroupName = detail.AccountGroupName,
                        SIHCredit = detail.SIHCredit ?? 0,
                        BSIHCredit = detail.SIHCredit ?? 0,
                        ASIHCredit = 0,
                        ThisSIHCredit = 0,
                        ChangeAmount = 0 - (detail.SIHCredit ?? 0),
                        SIHCreditZB = detail.SIHCreditZB ?? 0,
                        ValidationTime = DateTime.Now.Date,
                       // ExpireTime =detail.ExpireTime,
                        ValidationTimeCheck = detail.ValidationTime,
                        ExpireTimeCheck = detail.ExpireTime
                    };
                     if(sihCreditInfo.Type == (int)DCSSIHCreditInfoType.配件中心) {
                         newSIHCreditInfoDetail.ExpireTime = DateTime.ParseExact((DateTime.Now.Year + "1231").ToString(), "yyyyMMdd", null);
                    }else {
                         newSIHCreditInfoDetail.ExpireTime=detail.ExpireTime;
                     }
                    sihCreditInfo.SIHCreditInfoDetails.Add(newSIHCreditInfoDetail);
                }
            }
            this.CalcTotalAmount();
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void DealerQueryWindow_Loaded(object sender, RoutedEventArgs routedEventArgs) {
            var dealer = sender as DcsMultiPopupsQueryWindowBase;
            if(dealer == null)
                return;
            var sihCreditInfo = this.DataContext as SIHCreditInfo;
            if(sihCreditInfo == null)
                return;

            dealer.ExchangeData(null, "SetAdditionalFilterItem", new FilterItem("MarketingDepartmentId", typeof(int), FilterOperator.IsEqualTo, sihCreditInfo.MarketingDepartmentId));
            dealer.ExchangeData(null, "ExecuteQuery", null);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "CompanyCode",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccount_AgencyCode,
                        IsReadOnly=true
                    },new ColumnItem {
                        Name = "CompanyName",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccount_AgencyName,
                        IsReadOnly=true
                    },new ColumnItem{
                        Name= "DealerCode",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccount_DealerCode,
                        IsReadOnly=true
                    },new ColumnItem{
                        Name= "DealerName",
                        Title = FinancialUIStrings.QueryPanel_QueryItem_Title_CustomerAccount_DealerName,
                        IsReadOnly=true
                    },new ColumnItem{
                        Name= "AccountGroupCode",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_AccountGroup_Code,
                        IsReadOnly=true
                    },new ColumnItem{
                        Name= "AccountGroupName",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_AccountGroup_Name,
                        IsReadOnly=true
                    },new ColumnItem{
                        Name= "SIHCredit",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_CustomerAccount_SIHCredit,
                        IsReadOnly=true
                    },new ColumnItem{
                        Name= "SIHCreditZB",
                        Title = "SIH授信(服务商)(总部)",
                        IsReadOnly=true
                    },
                    new ColumnItem{
                        Name= "ThisSIHCredit",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfoDetail_ThisSIHCredit
                    },
                    new ColumnItem{
                        Name= "ChangeAmount",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfo_ChangeAmount,
                        IsReadOnly=true
                    },
                    new ColumnItem{
                        Name= "ValidationTime",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfoDetail_ValidationTime
                    },
                    new ColumnItem{
                        Name= "ExpireTime",
                        Title = FinancialUIStrings.DataGridView_ColumnItem_Title_SIHCreditInfoDetail_ExpireTime
                    }
                };
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("SIHCreditInfoDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.AddingNewDataItem -= GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
            this.GridView.CellEditEnded += this.GridView_CellEditEnded;
            this.GridView.CellValidating += GridView_CellValidating;
            this.GridView.Deleted += GridView_Deleted;
            ((GridViewDataColumn)this.GridView.Columns["SIHCredit"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["ChangeAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["ThisSIHCredit"]).DataFormatString = "c2";
        }

        protected override Type EntityType {
            get {
                return typeof(SIHCreditInfoDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }
        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            var sihCreditInfo = this.DataContext as SIHCreditInfo;
            if(sihCreditInfo == null)
                return;
            if(!sihCreditInfo.MarketingDepartmentId.HasValue) {
                UIHelper.ShowNotification("请先选择分销中心");
                e.Cancel = true;
                return;
            }
            this.RadQueryWindow.ShowDialog();
        }

        private void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            this.CalcTotalAmount();
        }

        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
            var sihCreditInfoDetail = e.Cell.DataContext as SIHCreditInfoDetail;
            if(sihCreditInfoDetail == null)
                return;

            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "ThisSIHCredit":
                    if(!(e.NewValue is decimal) || (decimal)e.NewValue < 0) {
                        e.IsValid = false;
                        e.ErrorMessage = "本次SIH授信额度必须为大于等于0的数字";
                    }
                    break;
            }
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var detail = e.Cell.DataContext as SIHCreditInfoDetail;
            if(detail == null)
                return;
            switch(e.Cell.Column.UniqueName) {
                case "ThisSIHCredit":
                    detail.ASIHCredit = detail.ThisSIHCredit;
                    detail.ChangeAmount = detail.SIHCredit - detail.ThisSIHCredit;
                    this.CalcTotalAmount();
                    break;
            }
        }

        private void CalcTotalAmount() {
            var sihCreditInfo = this.DataContext as SIHCreditInfo;
            if(sihCreditInfo == null)
                return;
            sihCreditInfo.ATotalFee = sihCreditInfo.SIHCreditInfoDetails.Sum(entity => entity.ASIHCredit);
            sihCreditInfo.BTotalFee = sihCreditInfo.SIHCreditInfoDetails.Sum(entity => entity.BSIHCredit);
            sihCreditInfo.ChangeAmount = sihCreditInfo.SIHCreditInfoDetails.Sum(entity => entity.ChangeAmount);
        }
    }
}
