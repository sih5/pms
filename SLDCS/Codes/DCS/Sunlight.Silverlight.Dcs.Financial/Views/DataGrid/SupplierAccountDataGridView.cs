﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class SupplierAccountDataGridView : DcsDataGridViewBase {
        private readonly string[] kvnames = {
           "MasterData_Status" 
        };
        public SupplierAccountDataGridView() {
            this.KeyValueManager.Register(kvnames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[]{
                new KeyValuesColumnItem{
                    Name = "Status",
                    KeyValueItems = this.KeyValueManager[kvnames[0]]
                }, new ColumnItem{
                    Name="Company.Code",
                    Title = FinancialUIStrings.DataGridView_ColumnItem_Title_Supplier_Code
                }, new ColumnItem{
                    Name="Company.SupplierCode",
                    Title= FinancialUIStrings.DataGridView_ColumnItem_Title_Supplier_SupplierCode
                },new ColumnItem{
                    Name="Company.Name",
                    Title=FinancialUIStrings.DataGridView_ColumnItem_Title_Supplier_Name
                },new ColumnItem{
                    Name="DueAmount",
                    Title=FinancialUIStrings.DataGridView_ColumnItem_Title_DueAmount
                },new ColumnItem {
                    Name = "CreatorName"
                },new ColumnItem {
                    Name = "CreateTime"
                },new ColumnItem {
                    Name = "ModifierName"
                },new ColumnItem {
                    Name = "ModifyTime"
                },new ColumnItem{
                    Name="PartsSalesCategory.Name",
                    Title=FinancialUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategory
                }};
            }
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.DataPager.PageSize = 300;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Single;
            this.GridView.SelectionUnit = Telerik.Windows.Controls.GridView.GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ModifyTime"]).DataFormatString = "d";
        }
        protected override Type EntityType {
            get {
                return typeof(SupplierAccount);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetSupplierAccountsWithCompany";
        }
        //private void ss() {

        //    var domaincontext =new DcsDomainContext();
        //    ;
        //}
    }
}
