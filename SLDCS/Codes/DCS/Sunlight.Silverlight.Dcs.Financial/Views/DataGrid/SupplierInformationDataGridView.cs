﻿
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class SupplierInformationDataGridView :DcsDataGridViewBase{
     private readonly string[] kvNames = {
            "BaseData_Status"
        };
     public SupplierInformationDataGridView() {
         this.KeyValueManager.Register(this.kvNames);
     }
     protected override System.Type EntityType {
         get {
             return typeof(BranchSupplierRelation);
         }
     }
     protected override IEnumerable<ColumnItem> ColumnItems {
         get {
             return new[] {
                      new KeyValuesColumnItem {
                            Name = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                      },new ColumnItem {
                          Name="Company.Name",
                          Title=FinancialUIStrings.QueryPanel_QueryItem_Title_SupplierCompany_Name
                      },new ColumnItem{
                          Name="Company.Code",
                          Title=FinancialUIStrings.QueryPanel_QueryItem_Title_SupplierCompany_Code
                      },new ColumnItem{
                          Name="Company.ContactPerson"      
                      },new ColumnItem{
                          Name="Company.ContactPhone"
                      } ,new ColumnItem{
                        Name="CreatorName"
                      },new ColumnItem{
                        Name="CreateTime"
                      }, new ColumnItem {
                        Name = "ModifierName"
                      }, new ColumnItem {
                        Name = "ModifyTime"
                      },new ColumnItem{
                          Name = "PartsSalesCategory.Name"
                      }

                  };
             }
      }
     protected override string OnRequestQueryName() {
         return "GetBranchSupplierRelationWithPartsSalesCategoryCompany";
     }
    }
}
