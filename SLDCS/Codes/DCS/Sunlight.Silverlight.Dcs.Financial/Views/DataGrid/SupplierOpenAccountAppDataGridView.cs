﻿
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class SupplierOpenAccountAppDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "WorkflowOfSimpleApproval_Status"
        };
        public SupplierOpenAccountAppDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override System.Type EntityType {
            get {
                return typeof(SupplierOpenAccountApp);
            }
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                          Name = "Status",
                          KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                      }, new ColumnItem {
                          Name="Code"
                      },new ColumnItem{
                          Name="Company.Code",
                          Title=FinancialUIStrings.QueryPanel_QueryItem_Title_SupplierCompany_Code
                      },new ColumnItem{
                          Name = "Company.Name",
                          Title=FinancialUIStrings.QueryPanel_QueryItem_Title_SupplierCompany_Name
                      },new ColumnItem{
                          Name="AccountInitialDeposit"
                      } ,new ColumnItem{
                          Name="Operator"
                      } ,new ColumnItem{
                          Name="Remark"
                      } ,new ColumnItem{
                        Name="CreatorName"
                      },new ColumnItem{
                        Name="CreateTime"
                      }, new ColumnItem {
                        Name = "ModifierName"
                      }, new ColumnItem {
                        Name = "ModifyTime"
                      },new ColumnItem {
                        Name = "AbandonerName"
                      }, new ColumnItem {
                        Name = "AbandonTime"
                      },new ColumnItem{
                          Name="PartsSalesCategory.Name"      
                      }

                  };
            }
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName() {
            return "GetSupplierOpenAccountAppFilterCompanyId";
        }
    }
}
