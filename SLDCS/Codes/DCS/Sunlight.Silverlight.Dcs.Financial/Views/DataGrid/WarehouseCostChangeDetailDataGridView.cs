﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Financial.Views.DataGrid {
    public class WarehouseCostChangeDetailDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode"
                    }, new ColumnItem {
                        Name = "SparePartName"
                    }, new ColumnItem {
                        Name = "PriceBeforeChange"
                    }, new ColumnItem {
                        Name = "PriceAfterChange"
                    }, new ColumnItem {
                        Name = "Quantity"
                    }, new ColumnItem {
                        Name = "AmountDifference"
                    }
                };
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("WarehouseCostChangeDetails");
        }

        protected override Type EntityType {
            get {
                return typeof(WarehouseCostChangeDetail);
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.CanUserDeleteRows = false;
            this.GridView.CanUserInsertRows = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override string OnRequestQueryName() {
            return "GetWarehouseCostChangeDetails";
        }

        private void WarehouseCostChangeDetailDataGridView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var warehouseCostChangeBill = this.DataContext as WarehouseCostChangeBill;
            if(warehouseCostChangeBill == null)
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "WarehouseCostChangeBillId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = warehouseCostChangeBill.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        public WarehouseCostChangeDetailDataGridView() {
            this.DataContextChanged -= WarehouseCostChangeDetailDataGridView_DataContextChanged;
            this.DataContextChanged += WarehouseCostChangeDetailDataGridView_DataContextChanged;
        }
    }
}
