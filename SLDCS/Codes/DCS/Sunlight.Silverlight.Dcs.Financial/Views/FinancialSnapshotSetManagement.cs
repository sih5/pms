﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Financial.Views {
    [PageMeta("PartsFinancialAndControls", "StockAccounting", "FinancialSnapshotSet", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON,"FinancialSnapshotSet"
    })]
    public class FinancialSnapshotSetManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        public FinancialSnapshotSetManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = FinancialUIStrings.DataManagementView_Title_FinancialSnapshotSet;
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("FinancialSnapshotSet"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("FinancialSnapshotSet");
                    dataEditView.EditSubmitted += dataEditView_EditSubmitted;
                    dataEditView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void dataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void dataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "FinancialSnapshotSet"
                };
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<FinancialSnapshotSet>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsSnapshoStatus.新建;
                case "HistoricalDataBase":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities1 = this.DataGridView.SelectedEntities.Cast<FinancialSnapshotSet>().ToArray();
                    if(entities1.Length == 1)
                        return true;
                    return false;
                case "HistoryStockSum":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities2 = this.DataGridView.SelectedEntities.Cast<FinancialSnapshotSet>().ToArray();
                    if(entities2.Length == 1)
                        return true;
                    return false;
                default:
                    return false;
            }
        }
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var financialSnapshotSet = this.DataEditView.CreateObjectToEdit<FinancialSnapshotSet>();
                    financialSnapshotSet.Status = (int)DcsSnapshoStatus.新建;
                    financialSnapshotSet.BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    financialSnapshotSet.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    financialSnapshotSet.SnapshotTime = DateTime.Now.Date;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(FinancialUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<FinancialSnapshotSet>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废财务快照)
                                entity.作废财务快照();
                            this.ExecuteSerivcesMethod(FinancialUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "HistoricalDataBase":
                    this.WindowSettlementCost.ShowDialog();
                    break;
                case "HistoryStockSum":
                    this.WindowSettlement.ShowDialog();
                    break;
            }
        }

        private RadWindow windowSettlementCost;

        private RadWindow WindowSettlementCost {
            get {
                return this.windowSettlementCost ?? (this.windowSettlementCost = new RadWindow {
                    Content = FinancialSnapshotSetQueryWindow,
                    Header = FinancialUIStrings.DataManagementView_RadWindow_FinancialSnapshotSetQuery,
                    WindowStartupLocation = WindowStartupLocation.CenterScreen,
                });
            }
        }

        private RadWindow windowSettlement;

        private RadWindow WindowSettlement {
            get {
                return this.windowSettlement ?? (this.windowSettlement = new RadWindow {
                    Content = HistoryStockSumSetQueryWindow,
                    Header = FinancialUIStrings.DataManagementView_RadWindow_HistoryStockSumSetQuery,
                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                });
            }
        }

        private DcsExportQueryWindowBase financialSnapshotSetQueryWindow;

        private DcsExportQueryWindowBase historyStockSumSetQueryWindow;

        private DcsExportQueryWindowBase FinancialSnapshotSetQueryWindow {
            get {
                if(this.financialSnapshotSetQueryWindow == null) {
                    this.financialSnapshotSetQueryWindow = DI.GetQueryWindow("FinancialSnapshotSet") as DcsExportQueryWindowBase;
                    this.financialSnapshotSetQueryWindow.Loaded += financialSnapshotSetQueryWindow_Loaded;
                }
                return this.financialSnapshotSetQueryWindow;
            }
        }
          private DcsExportQueryWindowBase HistoryStockSumSetQueryWindow {
            get {
                if(this.historyStockSumSetQueryWindow == null) {
                    this.historyStockSumSetQueryWindow = DI.GetQueryWindow("HistoryStockSumSet") as DcsExportQueryWindowBase;
                    this.historyStockSumSetQueryWindow.Loaded += historyStockSumSetQueryWindow_Loaded;
                }
                return this.historyStockSumSetQueryWindow;
            }
        }

        private void financialSnapshotSetQueryWindow_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var financialSnapshotSet = this.DataGridView.SelectedEntities.First() as FinancialSnapshotSet;
            if(financialSnapshotSet == null)
                return;
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "SnapshoTime", financialSnapshotSet.SnapshotTime
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "SnapshoTime", false
            });
            var compositeFilterItem = new CompositeFilterItem();
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }
        private void historyStockSumSetQueryWindow_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var financialSnapshotSet = this.DataGridView.SelectedEntities.First() as FinancialSnapshotSet;
            if(financialSnapshotSet == null)
                return;
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "SnapshoTime", financialSnapshotSet.SnapshotTime
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "SnapshoTime", false
            });
            var compositeFilterItem = new CompositeFilterItem();
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }
        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "BranchId",
                Operator = FilterOperator.IsEqualTo,
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
