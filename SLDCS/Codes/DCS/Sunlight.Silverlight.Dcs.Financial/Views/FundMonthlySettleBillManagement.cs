﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using System.Collections.ObjectModel;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Financial.Views {
    [PageMeta("PartsFinancialAndControls", "StockAccounting", "FundMonthlySettleBill", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_ABANDON,"FundMonthlySettleBill"
    })]
    public class FundMonthlySettleBillManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;

        private readonly string[] kvNames = {
                   "AccountPeriodYear","AccountPeriodMonth"
        };
        private ObservableCollection<KeyValuePair> kvYear;
        private ObservableCollection<KeyValuePair> KvYear
        {
            get
            {
                return this.kvYear ?? (this.kvYear = new ObservableCollection<KeyValuePair>());
            }
        }
        private ObservableCollection<KeyValuePair> kvMonth;
        private ObservableCollection<KeyValuePair> KvMonth
        {
            get
            {
                return this.kvMonth ?? (this.kvMonth = new ObservableCollection<KeyValuePair>());
            }
        }

        private KeyValueManager keyValueManager;
        private KeyValueManager KeyValueManager
        {
            get
            {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        public FundMonthlySettleBillManagement() {
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData(() =>
            {
                foreach (var item in this.KeyValueManager[kvNames[0]])
                    KvYear.Add(item);
                foreach (var item in this.KeyValueManager[kvNames[1]])
                    KvMonth.Add(item);
            });
            this.Initializer.Register(this.Initialize);
            this.Title = FinancialUIStrings.DataManagementView_Title_undMonthlySettleBill;
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("FundMonthlySettleBill"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("FundMonthlySettleBill");
                    dataEditView.EditSubmitted += dataEditView_EditSubmitted;
                    dataEditView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void dataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void dataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "FundMonthlySettleBill"
                };
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<FundMonthlySettleBill>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsBaseDataStatus.有效;
                case "Detail":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities2 = this.DataGridView.SelectedEntities.Cast<FundMonthlySettleBill>().ToArray();
                    if(entities2.Length == 1 && entities2[0].Status == (int)DcsBaseDataStatus.有效)
                        return true;
                    return false;
                default:
                    return false;
            }
        }
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var fundMonthlySettleBill = this.DataEditView.CreateObjectToEdit<FundMonthlySettleBill>();
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(FinancialUIStrings.DataManagementView_Confirm_Abandon, () => {
                        ShellViewModel.Current.IsBusy = true;
                        var entity = this.DataGridView.SelectedEntities.Cast<FundMonthlySettleBill>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废资金月结)
                                entity.作废资金月结();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                ShellViewModel.Current.IsBusy = false;
                                UIHelper.ShowNotification(FinancialUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "Detail":
                    this.WindowFundMonthlySettleBillDetail.ShowDialog();
                    break;
            }
        }

        private RadWindow windowFundMonthlySettleBillDetail;

        private RadWindow WindowFundMonthlySettleBillDetail {
            get {
                return this.windowFundMonthlySettleBillDetail ?? (this.windowFundMonthlySettleBillDetail = new RadWindow {
                    Content = FundMonthlySettleBillDetailQueryWindow,
                    Header = FinancialUIStrings.DataManagementView_RadWindow_FundMonthlySettleBillDetailQuery,
                    WindowStartupLocation = WindowStartupLocation.CenterScreen,
                });
            }
        }

        private DcsExportQueryWindowBase fundMonthlySettleBillDetailQueryWindow;

        private DcsExportQueryWindowBase FundMonthlySettleBillDetailQueryWindow {
            get {
                if(this.fundMonthlySettleBillDetailQueryWindow == null) {
                    this.fundMonthlySettleBillDetailQueryWindow = DI.GetQueryWindow("FundMonthlySettleBillDetail") as DcsExportQueryWindowBase;
                    this.fundMonthlySettleBillDetailQueryWindow.Loaded += fundMonthlySettleBillDetailQueryWindow_Loaded;
                }
                return this.fundMonthlySettleBillDetailQueryWindow;
            }
        }

        private void fundMonthlySettleBillDetailQueryWindow_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var fundMonthlySettleBill = this.DataGridView.SelectedEntities.First() as FundMonthlySettleBill;
            if(fundMonthlySettleBill == null)
                return;
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", new FilterItem("FundMonthlySettleBillId", typeof(int), FilterOperator.IsEqualTo, fundMonthlySettleBill.Id));
            queryWindow.ExchangeData(null, "ExecuteQuery", null);
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            var newFilterItem = new CompositeFilterItem();
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return;
            int? YearValue;
            int? MonthValue;
            if (compositeFilterItem != null)
            {
                foreach (var item in compositeFilterItem.Filters)
                {
                    if (item.MemberName == "MonthlySettleYear")
                    {
                        YearValue = item.Value as int?;
                        if (YearValue.HasValue && YearValue != -1)
                        {
                            string YearName = KvYear.FirstOrDefault(r => r.Key == YearValue.Value).Value;
                            newFilterItem.Filters.Add(new FilterItem("MonthlySettleYear", typeof(string), FilterOperator.IsEqualTo, YearName));
                        }
                        continue;
                    }
                    if (item.MemberName == "MonthlySettleMonth")
                    {
                        MonthValue = item.Value as int?;
                        if (MonthValue.HasValue && MonthValue != -1)
                        {
                            string MonthName = KvMonth.FirstOrDefault(r => r.Key == MonthValue.Value).Value;
                            newFilterItem.Filters.Add(new FilterItem("MonthlySettleMonth", typeof(string), FilterOperator.IsEqualTo, MonthName));
                        }
                        continue;
                    }
                    newFilterItem.Filters.Add(item);
                }
            }
            this.SwitchViewTo(DATA_GRID_VIEW);
            ClientVar.ConvertTime(compositeFilterItem);           
            this.DataGridView.FilterItem = newFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
