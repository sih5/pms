﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Financial.Views {
    [PageMeta("PartsFinancialAndControls", "StockAccounting", "PartsPlannedPrice", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class PartsPlannedPriceQuery : DcsDataManagementViewBase {

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private DataGridViewBase dataGridView;

        public DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsPlannedPrice"));
            }
        }
        public PartsPlannedPriceQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = FinancialUIStrings.DataManagementView_Title_PartsPlannedPrice;
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return true;
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var code = filterItem.Filters.Single(r => r.MemberName == "SparePart.Code").Value as string;
                    var name = filterItem.Filters.Single(r => r.MemberName == "SparePart.Name").Value as string;
                    var partsSalesCategoryId = filterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                    var ownerCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    if(createTime != null) {
                        createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                        createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.ExportPartsPlannedPrice(code, name, partsSalesCategoryId, createTimeBegin, createTimeEnd, ownerCompanyId);
                    break;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var newCompositeFilter = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                newCompositeFilter = filterItem as CompositeFilterItem;
                ClientVar.ConvertTime(newCompositeFilter);
            } else
                newCompositeFilter.Filters.Add(filterItem);
            newCompositeFilter.Filters.Add(new FilterItem {
                MemberName = "OwnerCompanyId",
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId,
                Operator = FilterOperator.IsEqualTo
            });
            this.DataGridView.FilterItem = newCompositeFilter;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsPlannedPrice"
                };
            }
        }



        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }

        private void ExportPartsPlannedPrice(string code, string name, int? partsSalesCategoryid, DateTime? createTimeBegin, DateTime? createTimeEnd, int ownerCompanyId) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsPlannedPriceAsync(code, name, partsSalesCategoryid, createTimeBegin, createTimeEnd, ownerCompanyId);
            this.excelServiceClient.ExportPartsPlannedPriceCompleted -= excelServiceClient_ExportPartsPlannedPriceCompleted;
            this.excelServiceClient.ExportPartsPlannedPriceCompleted += excelServiceClient_ExportPartsPlannedPriceCompleted;
        }

        private void excelServiceClient_ExportPartsPlannedPriceCompleted(object sender, ExportPartsPlannedPriceCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
    }
}
