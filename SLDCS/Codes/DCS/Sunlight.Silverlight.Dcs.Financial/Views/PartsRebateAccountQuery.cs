﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Financial.Views {
    [PageMeta("PartsFinancialAndControls", "PartsRebateManage", "PartsRebateAccount", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
     })]
    public class PartsRebateAccountQuery : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsRebateAccountForSelect"));
            }
        }
        public PartsRebateAccountQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = FinancialUIStrings.DataManagementView_Title_PartsRebateAccountQuery;
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);

        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsRebateAccountForSelect"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem ?? new CompositeFilterItem();
            var branchFilterItem = compositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "BranchId");
            if(branchFilterItem == null)
                compositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "BranchId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });
            else
                branchFilterItem.Value = BaseApp.Current.CurrentUserData.EnterpriseId;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var branchId = filterItem.Filters.Single(p => p.MemberName == "BranchId").Value as int?;
                    var accountGroupId = filterItem.Filters.Single(p => p.MemberName == "AccountGroupId").Value as int?;
                    var customerCompanyCode = filterItem.Filters.Single(p => p.MemberName == "CompanyCode").Value as string;
                    var customerCompanyName = filterItem.Filters.Single(p => p.MemberName == "CompanyName").Value as string;
                    var createTime = filterItem.Filters.SingleOrDefault(p => p.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    if(createTime != null) {
                        createTimeBegin = createTime.Filters.First(p => p.MemberName == "CreateTime").Value as DateTime?;
                        createTimeEnd = createTime.Filters.Last(p => p.MemberName == "CreateTime").Value as DateTime?;
                    }
                    var businessCode = filterItem.Filters.Single(p => p.MemberName == "BusinessCode").Value as string;
                    ShellViewModel.Current.IsBusy = true;
                    this.ExportPartsRebateAccount(branchId, customerCompanyCode, customerCompanyName, createTimeBegin, createTimeEnd, accountGroupId, businessCode);
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        private void ExportPartsRebateAccount(int? branchId, string customerCompanyCode, string customerCompanyName, DateTime? createTimeBegin, DateTime? createTimeEnd, int? accountGroupId, string businessCode) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsRebateAccountAsync(branchId, customerCompanyCode, customerCompanyName, createTimeBegin, createTimeEnd, accountGroupId, businessCode);
            this.excelServiceClient.ExportPartsRebateAccountCompleted -= excelServiceClient_ExportPartsRebateAccountCompleted;
            this.excelServiceClient.ExportPartsRebateAccountCompleted += excelServiceClient_ExportPartsRebateAccountCompleted;
        }
        private void excelServiceClient_ExportPartsRebateAccountCompleted(object sender, ExportPartsRebateAccountCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
    }
}
