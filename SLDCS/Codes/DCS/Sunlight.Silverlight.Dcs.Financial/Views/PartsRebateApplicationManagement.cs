﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Financial.Views {
    [PageMeta("PartsFinancialAndControls", "PartsRebateManage", "PartsRebateApplication", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_APPROVE_BATCHAPPROVAL_IMPORT_EXPORT
    })]
    public class PartsRebateApplicationManagement : DcsDataManagementViewBase {
        private const string DATA_APPROVE_EDIT_VIEW = "_DataEditViewApprove_";
        private const string IMPORT_DATA_EDIT_VIEW = "_IMPORT_DATA_EDIT_VIEW_";
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        private DataEditViewBase importDataEditView;
        private DataEditViewBase dataEditViewApprove;
        private DataEditViewBase DataEditViewApprove {
            get {
                if(this.dataEditViewApprove == null) {
                    this.dataEditViewApprove = DI.GetDataEditView("PartsRebateApplicationForApprove");
                    dataEditViewApprove.EditSubmitted += dataEditView_EditSubmitted;
                    dataEditViewApprove.EditCancelled += dataEditView_EditCancelled;
                } return this.dataEditViewApprove;
            }
        }
        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsRebateApplication");
                    dataEditView.EditSubmitted += dataEditView_EditSubmitted;
                    dataEditView.EditCancelled += dataEditView_EditCancelled;
                } return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.importDataEditView=null;
            this.dataEditViewApprove=null;
        }
        private void dataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void dataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        public PartsRebateApplicationManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = FinancialUIStrings.DataManagementView_Title_PartsRebateApplication;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsRebateApplication"));
            }
        }

        private DataEditViewBase ImportDataEditView {
            get {
                if(this.importDataEditView == null) {
                    this.importDataEditView = DI.GetDataEditView("PartsRebateApplicationForImport");
                    this.importDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.importDataEditView;
            }
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_APPROVE_EDIT_VIEW, () => this.DataEditViewApprove);
            this.RegisterView(IMPORT_DATA_EDIT_VIEW, () => this.ImportDataEditView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsRebateApplication"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "BranchId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                     if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitie1s = this.DataGridView.SelectedEntities.Cast<VirtualPartsRebateApplication>().ToArray();
                    if(entitie1s.Length != 1)
                        return false;
                    return entitie1s[0].Status == (int)DcsWorkflowOfSimpleApprovalStatus.新建;
                case CommonActionKeys.APPROVE:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<VirtualPartsRebateApplication>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsWorkflowOfSimpleApprovalStatus.新建;
                case CommonActionKeys.BATCHAPPROVAL:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitie = this.DataGridView.SelectedEntities.Cast<VirtualPartsRebateApplication>().ToArray();
                    if(!entitie.Any())
                        return false;
                    return entitie.All(e => e.Status == (int)DcsWorkflowOfSimpleApprovalStatus.新建);
                case CommonActionKeys.IMPORT:
                    return true;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var partsRebateApplication = this.DataEditView.CreateObjectToEdit<PartsRebateApplication>();
                    partsRebateApplication.Status = (int)DcsWorkflowOfSimpleApprovalStatus.新建;
                    partsRebateApplication.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    partsRebateApplication.BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(FinancialUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var id = this.DataGridView.SelectedEntities.First().GetIdentity();
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(domainContext == null)
                            return;
                        domainContext.作废配件返利申请单单据((int)id, loadOp => {
                            if(loadOp.HasError) {
                                if(!loadOp.IsErrorHandled)
                                    loadOp.MarkErrorAsHandled();
                                DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                                return;
                            }
                            this.CheckActionsCanExecute();
                            this.DataGridView.ExecuteQueryDelayed();
                        }, null);
                    });
                    break;
                case CommonActionKeys.APPROVE:
                    this.DataEditViewApprove.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_APPROVE_EDIT_VIEW);
                    break;
                case CommonActionKeys.BATCHAPPROVAL:
                    DcsUtils.Confirm(FinancialUIStrings.DataManagementView_Confirm_PartsRebateApplication_BatchApproval, () => {
                        ShellViewModel.Current.IsBusy = true;
                        try {
                            var ids = this.DataGridView.SelectedEntities.Cast<VirtualPartsRebateApplication>().Select(r => r.Id).ToArray();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext != null) {
                                domainContext.批量审核配件返利申请(ids, null, invokeOp => {
                                    if(invokeOp.HasError)
                                        return;
                                    UIHelper.ShowNotification(FinancialUIStrings.DataManagementView_Notification_BatchApprovalSuccess);
                                    ShellViewModel.Current.IsBusy = false;
                                    this.DataGridView.ExecuteQueryDelayed();
                                }, null);
                            }
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    }, () => {
                    }, FinancialUIStrings.DataManagementView_Confirm_Yes, FinancialUIStrings.DataManagementView_Confirm_No);
                    
                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(IMPORT_DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EXPORT:
                    //如果选中数据,导出参数为配件返利申请单ID 
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<VirtualPartsRebateApplication>().Select(r => r.Id).ToArray();
                        this.dcsDomainContext.ExportPartsRebateApplication(ids, BaseApp.Current.CurrentUserData.EnterpriseId, null, null, null, null, null, null, null, null, null, null,null, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                            }
                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                        }, null);
                    }
                        //否则 导出参数为
                    else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var status = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Status") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var customerCompanyCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "CustomerCompanyCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "CustomerCompanyCode").Value as string;
                        var creatorName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "CreatorName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "CreatorName").Value as string;
                        var customerCompanyName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "CustomerCompanyName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "CustomerCompanyName").Value as string;
                        var partsSalesCategoryId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSalesCategoryId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                        var accountGroupName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "AccountGroup.Name") == null ? null : filterItem.Filters.Single(r => r.MemberName == "AccountGroup.Name").Value as string;
                        var partsRebateTypeName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsRebateType.Name") == null ? null : filterItem.Filters.Single(r => r.MemberName == "PartsRebateType.Name").Value as string;
                        var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeStart = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeStart = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        var businessCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "BusinessCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "BusinessCode").Value as string;
                        this.dcsDomainContext.ExportPartsRebateApplication(new int[] { }, BaseApp.Current.CurrentUserData.EnterpriseId, partsSalesCategoryId, code, customerCompanyCode, customerCompanyName, partsRebateTypeName, accountGroupName, createTimeStart, createTimeEnd, status, creatorName, businessCode, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                            }
                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                        }, null);

                    }
                    break;
            }
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }

        #region 取消批量审核时审核意见的填写
        //private FrameworkElement forBatchApproveWindow;
        //private FrameworkElement ForBatchApproveWindow {
        //    get {
        //        return this.forBatchApproveWindow ?? (this.forBatchApproveWindow = DI.GetDataEditPanel("PsRebateAppForBatchApprove"));
        //    }
        //}

        //private RadWindow batchApproveWindow;
        //private RadWindow BatchApproveWindow {
        //    get {
        //        if(this.batchApproveWindow == null) {
        //            this.batchApproveWindow = new RadWindow {
        //                Content = this.ForBatchApproveWindow,
        //                Header = "请输入审核意见",
        //                WindowState = WindowState.Normal,
        //                WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
        //                AllowDrop = false,
        //                ResizeMode = ResizeMode.NoResize,
        //                CanClose = false
        //            };
        //            this.batchApproveWindow.Closed += this.BatchApproveWindow_Closed;
        //        }
        //        return this.batchApproveWindow;
        //    }
        //}
        //private void BatchApproveWindow_Closed(object sender, WindowClosedEventArgs e) {
        //    var radWindow = sender as RadWindow;
        //    if(radWindow == null || radWindow.Content == null)
        //        return;
        //    var dataEditPanel = radWindow.Content as PsRebateAppForBatchApproveDataEditPanel;
        //    if(dataEditPanel == null)
        //        return;
        //    var approvalComment = dataEditPanel.BatchApprovalComment;
        //    if(!string.IsNullOrWhiteSpace(approvalComment) && radWindow.DialogResult != null && (bool)radWindow.DialogResult) {
        //        try {
        //            var ids = this.DataGridView.SelectedEntities.Cast<PartsRebateApplication>().Select(r => r.Id).ToArray();
        //            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
        //            if(domainContext != null) {
        //                domainContext.批量审核配件返利申请(ids, approvalComment, invokeOp => {
        //                    if(invokeOp.HasError)
        //                        return;
        //                    UIHelper.ShowNotification("批量审批通过完成");
        //                    this.DataGridView.ExecuteQueryDelayed();
        //                }, null);
        //            }
        //        } catch(Exception ex) {
        //            UIHelper.ShowAlertMessage(ex.Message);
        //        }
        //    }
        //}
        #endregion
    }
}
