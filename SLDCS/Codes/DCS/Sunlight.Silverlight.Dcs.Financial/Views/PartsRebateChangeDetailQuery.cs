﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Financial.Views {
    [PageMeta("PartsFinancialAndControls", "PartsRebateManage", "PartsRebateChangeDetailQuery", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class PartsRebateChangeDetailQuery : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsRebateChangeDetailForSelect"));
            }
        }
        public PartsRebateChangeDetailQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = FinancialUIStrings.DataManagementView_Title_PartsRebateChangeDetailQuery;
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);

        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsRebateChangeDetailForSelect"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var companyName = filterItem.Filters.Single(r => r.MemberName == "CompanyName").Value as string;
                    var companyCode = filterItem.Filters.Single(r => r.MemberName == "CompanyCode").Value as string;
                    //var accountGroupName = filterItem.Filters.Single(r => r.MemberName == "AccountGroupName").Value as string;
                    var branchId = filterItem.Filters.Single(e => e.MemberName == "BranchId").Value as int?;
                    var accountGroupId = filterItem.Filters.Single(e => e.MemberName == "AccountGroupId").Value as int?;
                    var sourceCode = filterItem.Filters.Single(r => r.MemberName == "SourceCode").Value as string;
                    var sourcetype = filterItem.Filters.Single(r => r.MemberName == "SourceType").Value as int?;
                    var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    if(createTime != null) {
                        createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                        createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    }
                    int[] Ids = new int[] { };
                    if(this.DataGridView.SelectedEntities != null) {
                        var partsRebateChangeDetail = this.DataGridView.SelectedEntities.Cast<VirtualPartsRebateChangeDetail>();
                        Ids = partsRebateChangeDetail.Select(r => r.Id).ToArray();
                    }
                    var businessCode = filterItem.Filters.Single(r => r.MemberName == "BusinessCode").Value as string;
                    ShellViewModel.Current.IsBusy = true;
                    //this.ExportVirtualPartsRebateChangeDetail(Ids, branchId, accountGroupId, companyName, companyCode, sourceCode, sourcetype, accountGroupName, createTimeBegin, createTimeEnd,businessCode);
                    this.ExportVirtualPartsRebateChangeDetail(Ids, branchId, accountGroupId, companyName, companyCode, sourceCode, sourcetype, createTimeBegin, createTimeEnd, businessCode);
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        //private void ExportVirtualPartsRebateChangeDetail(int[] ids, int? branchId, int? accountGroupId, string companyName, string companyCode, string sourceCode, int? sourceType, string accountGroupName, DateTime? createTimeBegin, System.DateTime? createTimeEnd, string businessCode) {
        private void ExportVirtualPartsRebateChangeDetail(int[] ids, int? branchId, int? accountGroupId, string companyName, string companyCode, string sourceCode, int? sourceType, DateTime? createTimeBegin, System.DateTime? createTimeEnd, string businessCode) {
            ShellViewModel.Current.IsBusy = true;
            //this.excelServiceClient.ExportVirtualPartsRebateChangeDetailAsync(ids, branchId, accountGroupId, companyName, companyCode, sourceCode, sourceType, accountGroupName, createTimeBegin, createTimeEnd, businessCode);
            this.excelServiceClient.ExportVirtualPartsRebateChangeDetailAsync(ids, branchId, accountGroupId, companyName, companyCode, sourceCode, sourceType, createTimeBegin, createTimeEnd, businessCode);
            this.excelServiceClient.ExportVirtualPartsRebateChangeDetailCompleted -= excelServiceClient_ExportVirtualPartsRebateChangeDetailCompleted;
            this.excelServiceClient.ExportVirtualPartsRebateChangeDetailCompleted += excelServiceClient_ExportVirtualPartsRebateChangeDetailCompleted;

        }

         void excelServiceClient_ExportVirtualPartsRebateChangeDetailCompleted(object sender, ExportVirtualPartsRebateChangeDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem ?? new CompositeFilterItem();
            var branchFilterItem = compositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "BranchId");
            if(branchFilterItem == null)
                compositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "BranchId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });
            else
                branchFilterItem.Value = BaseApp.Current.CurrentUserData.EnterpriseId;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }


    }
}
