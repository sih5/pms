﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Financial.Views {
    [PageMeta("PartsFinancialAndControls", "PartFunctions", "PaymentBill", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_APPROVE_EXPORT_BATCHAPPROVAL,"PaymentBill"
    })]
    public class PaymentBillManagement : DcsDataManagementViewBase {
        private const string DATA_BENEFICIARY_VIEW = "_DataApproveView_";
        public PaymentBillManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = FinancialUIStrings.DataManagementView_Title_PaymentBill;
        }

        private DataEditViewBase dataEditView;
        private DataEditViewBase dataBeneficiaryView;

        private DataEditViewBase DataBeneficiaryView {
            get {
                if(this.dataBeneficiaryView == null) {
                    this.dataBeneficiaryView = DI.GetDataEditView("PaymentBeneficiaryList");
                    this.dataBeneficiaryView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataBeneficiaryView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataBeneficiaryView;
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PaymentBill");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataBeneficiaryView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_BENEFICIARY_VIEW, () => this.DataBeneficiaryView);
        }

        private DataGridViewBase dataGridView;

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PaymentBill"));
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PaymentBill"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var paymentBill = this.DataEditView.CreateObjectToEdit<PaymentBill>();
                    paymentBill.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    paymentBill.SalesCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.APPROVE:
                    DcsUtils.Confirm(FinancialUIStrings.DataManagementView_Confirm_Approve, () => {
                        var id = this.DataGridView.SelectedEntities.First().GetIdentity();
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(domainContext == null)
                            return;
                        domainContext.审核来款单据((int)id,DateTime.Now, loadOp => {
                            if(loadOp.HasError) {
                                if(!loadOp.IsErrorHandled)
                                    loadOp.MarkErrorAsHandled();
                                DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                                return;
                            }
                            this.CheckActionsCanExecute();
                            this.DataGridView.ExecuteQueryDelayed();
                        }, null);
                    });
                    break;
                case CommonActionKeys.BATCHAPPROVAL:
                    DcsUtils.Confirm(FinancialUIStrings.DataManagementView_Confirm_BatchApproval, () => {
                         ShellViewModel.Current.IsBusy = true;
                        var ids = this.DataGridView.SelectedEntities.Cast<VirtualPaymentBill>().Select(e => e.Id).ToArray();
                        var id = this.DataGridView.SelectedEntities.First().GetIdentity();
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(domainContext == null)
                            return;
                        domainContext.批量审核来款单据(ids, loadOp => {
                            if(loadOp.HasError) {
                                if(!loadOp.IsErrorHandled)
                                    loadOp.MarkErrorAsHandled();
                                ShellViewModel.Current.IsBusy = false;
                                DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                                return;
                            }
                            this.CheckActionsCanExecute();
                            ShellViewModel.Current.IsBusy = false;
                            UIHelper.ShowNotification(FinancialUIStrings.DataManagementView_Notification_BatchApprovalSuccess);
                            this.DataGridView.ExecuteQueryDelayed();
                        }, null);
                    });
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(FinancialUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var id = this.DataGridView.SelectedEntities.First().GetIdentity();
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(domainContext == null)
                            return;
                        domainContext.作废来款单据((int)id, loadOp => {
                            if(loadOp.HasError) {
                                if(!loadOp.IsErrorHandled)
                                    loadOp.MarkErrorAsHandled();
                                DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                                return;
                            }
                            this.CheckActionsCanExecute();
                            this.DataGridView.ExecuteQueryDelayed();
                        }, null);
                        UIHelper.ShowNotification(FinancialUIStrings.DataManagementView_Notification_AbandonSuccess);
                        this.CheckActionsCanExecute();
                    });
                    break;
                case "Beneficiary":
                    this.DataBeneficiaryView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_BENEFICIARY_VIEW);
                    break;
                case CommonActionKeys.EXPORT:
                    ShellViewModel.Current.IsBusy = true;
                    //if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                    //    //var ids = this.DataGridView.SelectedEntities.Cast<PaymentBill>().Select(r => r.Id).ToArray();
                    //    //if(CommonActionKeys.MERGEEXPORT.Equals(uniqueId)) {
                    //    //    this.PaymentBillExport(ids, null, null, null, null, null, null, null, (int)filterItem.Filters.Single(e => e.MemberName == "SalesCompanyId").Value);
                    //    //}
                    //    //if(CommonActionKeys.EXPORT.Equals(uniqueId)) {
                    //    //    //this.Export(ids, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                    //    //}
                    //} else {
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var customerCompanyName = filterItem.Filters.Single(r => r.MemberName == "CustomerCompanyName").Value as string;
                    var customerCompanyCode = filterItem.Filters.Single(e => e.MemberName == "CustomerCompanyCode").Value as string;
                    var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                    var paymentMethod = filterItem.Filters.Single(e => e.MemberName == "PaymentMethod").Value as int?;
                    var creatorName = filterItem.Filters.Single(e => e.MemberName == "CreatorName").Value as string;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    DateTime? invoiceDateBegin = null;
                    DateTime? invoiceDateEnd = null;
                    foreach (var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)))
                     {
                         var dateTime = filter as CompositeFilterItem;
                         if (dateTime != null)
                            {
                                if (dateTime.Filters.First().MemberName == "CreateTime")
                                {
                                    createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                                if (dateTime.Filters.First().MemberName == "InvoiceDate")
                                {
                                    invoiceDateBegin = dateTime.Filters.First(r => r.MemberName == "InvoiceDate").Value as DateTime?;
                                    invoiceDateEnd = dateTime.Filters.Last(r => r.MemberName == "InvoiceDate").Value as DateTime?;
                                }
                            }
                        }
                    var businessCode = filterItem.Filters.Single(r => r.MemberName == "BusinessCode").Value as string;
                    PaymentBillExport(null, customerCompanyName, customerCompanyCode, paymentMethod, status, creatorName, createTimeBegin, createTimeEnd, businessCode, (int)filterItem.Filters.Single(e => e.MemberName == "SalesCompanyId").Value, invoiceDateBegin, invoiceDateEnd);
                    //}
                    break;
            }
        }
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private void PaymentBillExport(int[] ids, string customerCompanyName, string customerCompanyCode, int? paymentMethod, int? status, string creatorName, DateTime? createTimeBegin, DateTime? createTimeEnd, string businessCode, int salesCompanyId, DateTime? invoiceDateBegin, DateTime? invoiceDateEnd)
        {
            this.excelServiceClient.ExportPaymentBillAsync(null, customerCompanyName, customerCompanyCode, paymentMethod, status, creatorName, createTimeBegin, createTimeEnd, salesCompanyId, businessCode, invoiceDateBegin, invoiceDateEnd);
            this.excelServiceClient.ExportPaymentBillCompleted += excelServiceClient_ExportPaymentBillCompleted;
            this.excelServiceClient.ExportPaymentBillCompleted += excelServiceClient_ExportPaymentBillCompleted;
        }

        private void excelServiceClient_ExportPaymentBillCompleted(object sender, ExportPaymentBillCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.APPROVE:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<VirtualPaymentBill>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsPaymentBillStatus.新建;
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitie = this.DataGridView.SelectedEntities.Cast<VirtualPaymentBill>().ToArray();
                    if(entitie.Length != 1)
                        return false;
                    return entitie[0].Status == (int)DcsPaymentBillStatus.新建 || entitie[0].Status == (int)DcsPaymentBillStatus.已审核;
                case "Beneficiary":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities1 = this.DataGridView.SelectedEntities.Cast<VirtualPaymentBill>().ToArray();

                    if(entities1.Length != 1)
                        return false;
                    return entities1[0].Status == (int)DcsPaymentBillStatus.已审核;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.BATCHAPPROVAL:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count()==0)
                        return false;
                    var virtualPaymentBills = this.DataGridView.SelectedEntities.Cast<VirtualPaymentBill>().ToArray();
                    int paymentMethod=0;

                    for(int i = 0; i < virtualPaymentBills.Count(); i++) {
                        if(virtualPaymentBills[i].Status != (int)DcsPaymentBillStatus.新建) {
                            return false;
                        }
                        if(null == virtualPaymentBills[i].PaymentMethod) {
                            virtualPaymentBills[i].PaymentMethod = 0;
                        }
                        if(i == 0) {
                            paymentMethod = virtualPaymentBills[i].PaymentMethod.Value;
                        } else {
                            if(paymentMethod != virtualPaymentBills[i].PaymentMethod.Value) {
                                return false;
                            }
                        }
                    }
                    return true;
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var newCompostiteFilterItem = new CompositeFilterItem();
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                ClientVar.ConvertTime(compositeFilterItem);
                foreach(var item in compositeFilterItem.Filters) {
                    newCompostiteFilterItem.Filters.Add(item);
                }
            }
            newCompostiteFilterItem.Filters.Add(new FilterItem {
                MemberType = typeof(int),
                MemberName = "SalesCompanyId",
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            this.DataGridView.FilterItem = newCompostiteFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
