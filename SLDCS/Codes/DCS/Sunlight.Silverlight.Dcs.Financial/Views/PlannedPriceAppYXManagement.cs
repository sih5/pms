﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
namespace Sunlight.Silverlight.Dcs.Financial.Views {
    [PageMeta("PartsFinancialAndControls", "StockAccounting", "PlannedPriceAppYX", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_APPROVE_BATCHAPPROVAL_DETAIL_EXPORT_MERGEEXPORT,"PlannedPriceApp"
    })]
    public class PlannedPriceAppYXManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataDetailView;
        private DataEditViewBase dataWarehouseDetailView;
        private const string DATA_DETAIL_VIEW = "_dataDetailView_";
        private const string DATA_WAREHOUSEDETAIL_VIEW = "_DataWarehouseDetailView_";
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public PlannedPriceAppYXManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = FinancialUIStrings.DataManagementView_Title_PlannedPriceAppYX;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PlannedPriceApp"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PlannedPriceAppYX");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataDetailView {
            get {
                if(this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("PlannedPriceAppDetail");
                    this.dataDetailView.EditSubmitted += this.dataEditView_EditSubmitted;
                    this.dataDetailView.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.dataDetailView;
            }
        }

        private DataEditViewBase DataWarehouseDetailView {
            get {
                if(this.dataWarehouseDetailView == null) {
                    this.dataWarehouseDetailView = DI.GetDataEditView("PlannedPriceAppWarehouseDetail");
                    this.dataWarehouseDetailView.EditSubmitted += this.dataEditView_EditSubmitted;
                    this.dataWarehouseDetailView.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.dataWarehouseDetailView;
            }
        }

        private void dataEditView_EditSubmitted(object sender, EventArgs e) {
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void dataEditView_EditCancelled(object sender, EventArgs e) {
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataDetailView);
            this.RegisterView(DATA_WAREHOUSEDETAIL_VIEW, () => this.DataWarehouseDetailView);
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataDetailView = null;
            this.dataWarehouseDetailView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var composite = filterItem as CompositeFilterItem;
            if(composite == null)
                return;
            composite.Filters.Add(new FilterItem {
                MemberName = "OwnerCompanyId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            ClientVar.ConvertTime(composite);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PlannedPriceApp"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var plannedPriceApp = this.DataEditView.CreateObjectToEdit<PlannedPriceApp>();
                    plannedPriceApp.PlannedExecutionTime = DateTime.Now;
                    plannedPriceApp.Status = (int)DcsPlannedPriceAppStatus.新增;
                    plannedPriceApp.RecordStatus = (int)DcsRecordStatus.未计账;
                    plannedPriceApp.OwnerCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    plannedPriceApp.OwnerCompanyCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                    plannedPriceApp.OwnerCompanyName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    plannedPriceApp.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(FinancialUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PlannedPriceApp>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废计划价申请单)
                                entity.作废计划价申请单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(FinancialUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.APPROVE:
                    DcsUtils.Confirm(FinancialUIStrings.DataManagementView_Confirm_Approve, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PlannedPriceApp>().SingleOrDefault();
                        if(entity == null)
                            return;
                        var context = DataGridView.DomainContext as DcsDomainContext;
                        if(context == null) {
                            return;
                        }
                        try {
                            context.审核计划价申请单(entity.Id, t => {
                                if(t.HasError) {
                                    if(!t.IsErrorHandled)
                                        t.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(t);
                                    context.RejectChanges();
                                    return;
                                }
                                this.DataGridView.ExecuteQueryDelayed();
                                //this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);

                        }
                    });
                    break;
                case CommonActionKeys.BATCHAPPROVAL:
                    DcsUtils.Confirm(FinancialUIStrings.DataManagementView_Confirm_BatchApproval, () => {
                        var ids = this.dataGridView.SelectedEntities.Cast<PlannedPriceApp>().Select(r => r.Id).ToArray();
                        var context = DataGridView.DomainContext as DcsDomainContext;
                        if(context == null) {
                            return;
                        }
                        try {
                            context.批量审核计划价申请单(ids, t => {
                                if(t.HasError) {
                                    if(!t.IsErrorHandled)
                                        t.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(t);
                                    context.RejectChanges();
                                    return;
                                }
                                this.DataGridView.ExecuteQueryDelayed();
                                //this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                    var partsSalesCategoryId = filterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                    var status = filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                    var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    if(createTime != null) {
                        createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                        createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    }
                    var entities = this.DataGridView.SelectedEntities;
                    if(uniqueId.Equals(CommonActionKeys.MERGEEXPORT)) {
                        int? id = null;
                        if(entities != null && entities.Any())
                            id = entities.Cast<PlannedPriceApp>().First().Id;
                        ShellViewModel.Current.IsBusy = true;
                        this.ExportPlanndPriceAppWithDetail(id, code, partsSalesCategoryId, status, createTimeBegin, createTimeEnd);
                    }
                    if(uniqueId.Equals(CommonActionKeys.EXPORT)) {
                        var ids = new int[] { };
                        if(entities != null && entities.Any())
                            ids = entities.Cast<PlannedPriceApp>().Select(r => r.Id).ToArray();
                        ShellViewModel.Current.IsBusy = true;
                        this.ExportPlanndPriceApp(ids, code, partsSalesCategoryId, status, createTimeBegin, createTimeEnd);
                    }
                    break;
                case CommonActionKeys.DETAIL:
                    this.DataDetailView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_DETAIL_VIEW);
                    break;
                case "WarehouseDetail":
                    this.DataWarehouseDetailView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_WAREHOUSEDETAIL_VIEW);
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.APPROVE:
                case CommonActionKeys.DETAIL:
                case "WarehouseDetail":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entity = this.DataGridView.SelectedEntities.Cast<PlannedPriceApp>().ToArray();
                    if(entity.Length != 1)
                        return false;
                    if(uniqueId == CommonActionKeys.APPROVE || uniqueId == CommonActionKeys.EDIT || uniqueId == CommonActionKeys.ABANDON)
                        return entity[0].Status == (int)DcsPlannedPriceAppStatus.新增;
                    if(uniqueId == "WarehouseDetail")
                        return entity[0].Status == (int)DcsPlannedPriceAppStatus.已执行;
                    return true;
                case CommonActionKeys.BATCHAPPROVAL:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() == 0)
                        return false;
                    var entity1 = this.DataGridView.SelectedEntities.Cast<PlannedPriceApp>().ToArray();
                    if(entity1.Any(r => r.Status != (int)DcsPlannedPriceAppStatus.新增))
                        return false;
                    return true;
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }

        //合并导出主清单
        private void ExportPlanndPriceAppWithDetail(int? id, string code, int? partsSalesCategoryId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPlanndPriceAppWithDetailAsync(id, BaseApp.Current.CurrentUserData.EnterpriseId, code, partsSalesCategoryId, status, createTimeBegin, createTimeEnd);
            this.excelServiceClient.ExportPlanndPriceAppWithDetailCompleted -= excelServiceClient_ExportPlanndPriceAppWithDetailCompleted;
            this.excelServiceClient.ExportPlanndPriceAppWithDetailCompleted += excelServiceClient_ExportPlanndPriceAppWithDetailCompleted;
        }

        private void excelServiceClient_ExportPlanndPriceAppWithDetailCompleted(object sender, ExportPlanndPriceAppWithDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
        //导出主单
        private void ExportPlanndPriceApp(int[] id, string code, int? partsSalesCategoryId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPlanndPriceAppAsync(id, BaseApp.Current.CurrentUserData.EnterpriseId, code, partsSalesCategoryId, status, createTimeBegin, createTimeEnd);
            this.excelServiceClient.ExportPlanndPriceAppCompleted -= excelServiceClient_ExportPlanndPriceAppCompleted;
            this.excelServiceClient.ExportPlanndPriceAppCompleted += excelServiceClient_ExportPlanndPriceAppCompleted;
        }

        private void excelServiceClient_ExportPlanndPriceAppCompleted(object sender, ExportPlanndPriceAppCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }


    }
}