﻿

namespace Sunlight.Silverlight.Dcs.Financial.Views.QueryWindow {
    public class CompaniesAndAgenciesQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "CompaniesAndAgencies";
            }
        }

        public override string QueryPanelKey {
            get {
                return "CompaniesAndAgencies";
            }
        }
    }
}
