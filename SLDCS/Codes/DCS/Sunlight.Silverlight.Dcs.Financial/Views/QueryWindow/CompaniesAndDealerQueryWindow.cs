﻿

namespace Sunlight.Silverlight.Dcs.Financial.Views.QueryWindow {
    public class CompaniesAndDealerQueryWindow : DcsMultiPopupsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "CompaniesAndDealers";
            }
        }

        public override string QueryPanelKey {
            get {
                return "CompaniesAndDealer";
            }
        }
    }
}
