﻿

namespace Sunlight.Silverlight.Dcs.Financial.Views.QueryWindow {
    public class CompaniesForBottomStockQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "CompaniesForBottomStock";
            }
        }

        public override string QueryPanelKey {
            get {
                return "CompaniesForBottomStock";
            }
        }
    }
}
