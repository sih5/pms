﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Views.QueryWindow {
    public class CrossSalesOrderCompanyQueryWindow : DcsQueryWindowBase {

        public CrossSalesOrderCompanyQueryWindow() {
            var filterItem = new CompositeFilterItem();
            filterItem.Filters.Add(new FilterItem("Status", typeof(int), FilterOperator.IsNotEqualTo, (int)DcsMasterDataStatus.作废));
            filterItem.Filters.Add(new FilterItem("Type", typeof(int), FilterOperator.IsEqualTo, (int)DcsCompanyType.代理库));
            filterItem.Filters.Add(new FilterItem("Id", typeof(int), FilterOperator.IsNotEqualTo, BaseApp.Current.CurrentUserData.EnterpriseId));
            this.SetDefaultFilterItem(filterItem);
        }

        public override string DataGridViewKey {
            get {
                return "CrossSalesOrderCompany";
            }
        }

        public override string QueryPanelKey {
            get {
                return "CrossSalesOrderCompany";
            }
        }
    }
}
