﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;

namespace Sunlight.Silverlight.Dcs.Financial.Views.QueryWindow {
    /// <summary>
    /// 选择配件客户
    /// </summary>
    public class CustomerInformationBySparepartDropDownQueryWindow : DcsDropDownQueryWindowBase {

        public CustomerInformationBySparepartDropDownQueryWindow() {
            this.SetDefaultFilterItem(new FilterItem("CompanyId", typeof(int), FilterOperator.IsEqualTo, BaseApp.Current.CurrentUserData.EnterpriseId));
        }
        public override string Title {
            get {
                return FinancialUIStrings.QueryPanel_Title_CustomerInformationBySparepart;
            }
        }

        public override string DataGridViewKey {
            get {
                return "CustomerInformationBySparepart";
            }
        }

        public override string QueryPanelKey {
            get {
                return "CustomerInformationBySparepart";
            }
        }
    }
}
