﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Views.QueryWindow {
    /// <summary>
    /// 选择配件客户
    /// </summary>
    public class CustomerInformationBySparepartForTransferQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "CustomerInformationBySparepartForTransfer";
            }
        }

        public override string QueryPanelKey {
            get {
                return "CustomerInformationBySparepartForTransfer";
            }
        }
    }
}
