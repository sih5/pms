﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Views.QueryWindow {
    /// <summary>
    /// 选择配件客户
    /// </summary>
    public class CustomerInformationBySparepartQueryWindow : DcsQueryWindowBase {

        public CustomerInformationBySparepartQueryWindow() {
            var filterItem = new CompositeFilterItem();
            filterItem.Filters.Add(new FilterItem("SalesCompanyId", typeof(int), FilterOperator.IsEqualTo, BaseApp.Current.CurrentUserData.EnterpriseId));
            filterItem.Filters.Add(new FilterItem("Status", typeof(int), FilterOperator.IsNotEqualTo, (int)DcsMasterDataStatus.作废));
            this.SetDefaultFilterItem(filterItem);
        }

        public override string DataGridViewKey {
            get {
                return "CustomerInformationBySparepart";
            }
        }

        public override string QueryPanelKey {
            get {
                return "CustomerInformationBySparepart";
            }
        }
    }
}
