﻿using Sunlight.Silverlight.Dcs.Financial.Resources;

namespace Sunlight.Silverlight.Dcs.Financial.Views.QueryWindow {
    /// <summary>
    /// 查询存在退货入库的配件客户									
    /// 必须传入参数 账户组ID
    /// 参数名 AccountGroupId
    /// </summary>
    public class CustomerInformationForPartsSalesReturnDropDownQueryWindow : DcsDropDownQueryWindowBase {
        public override string Title {
            get {
                return FinancialUIStrings.QueryPanel_Title_CustomerInformationForPartsSalesReturn;
            }
        }

        public override string DataGridViewKey {
            get {
                return "CustomerInformationForPartsSalesReturn";
            }
        }

        public override string QueryPanelKey {
            get {
                return "CustomerInformationForPartsSalesReturn";
            }
        }
    }
}
