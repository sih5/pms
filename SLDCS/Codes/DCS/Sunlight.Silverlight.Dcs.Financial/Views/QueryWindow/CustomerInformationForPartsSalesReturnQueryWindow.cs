﻿
namespace Sunlight.Silverlight.Dcs.Financial.Views.QueryWindow {

    /// <summary>
    /// 查询存在退货入库的配件客户									
    /// 必须传入参数 账户组ID
    /// 参数名 AccountGroupId
    /// </summary>
    public class CustomerInformationForPartsSalesReturnQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "CustomerInformationForPartsSalesReturn";
            }
        }

        public override string QueryPanelKey {
            get {
                return "CustomerInformationForPartsSalesReturn";
            }
        }
    }
}
