﻿using System;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Financial.Views.QueryWindow {
    public class FinancialSnapshotSetQueryWindow : DcsExportQueryWindowBase {
        public FinancialSnapshotSetQueryWindow() {
            this.Export -= FinancialSnapshotSetQueryWindow_Export;
            this.Export += FinancialSnapshotSetQueryWindow_Export;
        }

        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();

        private void FinancialSnapshotSetQueryWindow_Export(object sender, EventArgs e) {
            ShellViewModel.Current.IsBusy = true;
            this.Export -= FinancialSnapshotSetQueryWindow_Export;
            if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                var ids = this.DataGridView.SelectedEntities.Cast<PartsHistoryStock>().Select(r => r.Id).ToArray();
                this.dcsDomainContext.导出历史库存(ids, null, null, null, null, null, null, null, loadOp => {
                    this.Export += FinancialSnapshotSetQueryWindow_Export;
                    if(loadOp.HasError)
                        return;
                    if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                        UIHelper.ShowNotification(loadOp.Value);
                        ShellViewModel.Current.IsBusy = false;
                    }

                    if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                        HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                        ShellViewModel.Current.IsBusy = false;
                    }
                }, null);
            } else {
                var compositeFilterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                if(compositeFilterItem != null) {
                    var warehouseId = compositeFilterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                    var partCode = compositeFilterItem.Filters.Single(r => r.MemberName == "SparePart.Code").Value as string;
                    var partName = compositeFilterItem.Filters.Single(r => r.MemberName == "SparePart.Name").Value as string;
                    var snapshoTime = compositeFilterItem.Filters.Single(r => r.MemberName == "SnapshoTime").Value as DateTime?;
                    var greaterThanZero = compositeFilterItem.Filters.Single(r => r.MemberName == "GreaterThanZero").Value as bool?;
                    var partsSalesCategoryId = compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                    var storageCompanyType = compositeFilterItem.Filters.Single(r => r.MemberName == "StorageCompanyType").Value as int?;
                    this.dcsDomainContext.导出历史库存(new int[] { }, warehouseId, partCode, partName, snapshoTime, partsSalesCategoryId, storageCompanyType, greaterThanZero, loadOp => {
                        this.Export += FinancialSnapshotSetQueryWindow_Export;
                        if(loadOp.HasError)
                            return;
                        if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                }
            }
        }

        public override string DataGridViewKey {
            get {
                return "FinancialSnapshotSets";
            }
        }

        public override string QueryPanelKey {
            get {
                return "FinancialSnapshotSets";
            }
        }
    }
}
