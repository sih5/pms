﻿using System;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Financial.Views.QueryWindow {
    public class FundMonthlySettleBillDetailQueryWindow : DcsExportQueryWindowBase {
        public FundMonthlySettleBillDetailQueryWindow() {
            this.Export -= FundMonthlySettleBillDetailQueryWindow_Export;
            this.Export += FundMonthlySettleBillDetailQueryWindow_Export;
        }

        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();

        private void FundMonthlySettleBillDetailQueryWindow_Export(object sender, EventArgs e) {
            ShellViewModel.Current.IsBusy = true;
            this.Export -= FundMonthlySettleBillDetailQueryWindow_Export;
            if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                var ids = this.DataGridView.SelectedEntities.Cast<FundMonthlySettleBillDetail>().Select(r => r.Id).ToArray();
                this.dcsDomainContext.导出资金月结明细(ids, null, null, null, loadOp => {
                    this.Export += FundMonthlySettleBillDetailQueryWindow_Export;
                    if(loadOp.HasError)
                        return;
                    if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                        UIHelper.ShowNotification(loadOp.Value);
                        ShellViewModel.Current.IsBusy = false;
                    }

                    if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                        HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                        ShellViewModel.Current.IsBusy = false;
                    }
                }, null);
            } else {
                var compositeFilterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                if(compositeFilterItem != null) {
                    var fundMonthlySettleBillId = compositeFilterItem.Filters.Single(r => r.MemberName == "FundMonthlySettleBillId").Value as int?;
                    var customerCompanyCode = compositeFilterItem.Filters.Single(r => r.MemberName == "CustomerCompanyCode").Value as string;
                    var customerCompanyName = compositeFilterItem.Filters.Single(r => r.MemberName == "CustomerCompanyName").Value as string;
                    this.dcsDomainContext.导出资金月结明细(new int[] { }, fundMonthlySettleBillId, customerCompanyCode, customerCompanyName, loadOp => {
                        this.Export += FundMonthlySettleBillDetailQueryWindow_Export;
                        if(loadOp.HasError)
                            return;
                        if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                }
            }
        }

        public override string DataGridViewKey {
            get {
                return "FundMonthlySettleBillDetail";
            }
        }

        public override string QueryPanelKey {
            get {
                return "FundMonthlySettleBillDetail";
            }
        }
    }
}
