﻿using System;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Financial.Views.QueryWindow {
    public class HistoryStockSumSetQueryWindow : DcsExportQueryWindowBase {
        public HistoryStockSumSetQueryWindow() {
            this.Export -= HistoryStockSumSetQueryWindow_Export;
            this.Export += HistoryStockSumSetQueryWindow_Export;
        }

        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();

        private void HistoryStockSumSetQueryWindow_Export(object sender, EventArgs e) {
            if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                var ids = this.DataGridView.SelectedEntities.Cast<PartsHistoryStockBill>().Select(r => r.Id).ToArray();
                var compositeFilterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                var greaterThanZero1 = compositeFilterItem.Filters.Single(r => r.MemberName == "GreaterThanZero").Value as bool?;
                this.dcsDomainContext.导出配件历史库存汇总信息(ids, null, null, null, null, greaterThanZero1, loadOp => {
                    if(loadOp.HasError)
                        return;
                    if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                        UIHelper.ShowNotification(loadOp.Value);
                        ShellViewModel.Current.IsBusy = false;
                    }

                    if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                        HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                        ShellViewModel.Current.IsBusy = false;
                    }
                }, null);
            } else {
                var compositeFilterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                if(compositeFilterItem != null) {
                  //  var warehouseId = compositeFilterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                    var snapshoTime = compositeFilterItem.Filters.Single(r => r.MemberName == "SnapshoTime").Value as DateTime?;
                    var greaterThanZero = compositeFilterItem.Filters.Single(r => r.MemberName == "GreaterThanZero").Value as bool?;
                    var partsSalesCategoryId = compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                    var storageCompanyType = compositeFilterItem.Filters.Single(r => r.MemberName == "StorageCompanyType").Value as int?;
                    this.dcsDomainContext.导出配件历史库存汇总信息(new int[] { }, null, storageCompanyType, snapshoTime, partsSalesCategoryId, greaterThanZero, loadOp => {
                        if(loadOp.HasError)
                            return;
                        if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                }
            }
        }

        public override string DataGridViewKey {
            get {
                return "HistoryStockSumSets";
            }
        }

        public override string QueryPanelKey {
            get {
                return "HistoryStockSumSets";
            }
        }
    }
}
