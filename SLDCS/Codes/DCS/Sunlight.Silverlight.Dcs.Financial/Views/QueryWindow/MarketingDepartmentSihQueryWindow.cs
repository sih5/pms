﻿using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Financial.Views.QueryWindow {
    /// <summary>
    /// 营销公司市场部选择
    /// </summary>
    public class MarketingDepartmentSihQueryWindow : DcsQueryWindowBase {
        public MarketingDepartmentSihQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsBaseDataStatus.有效);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string DataGridViewKey {
            get {
                return "MarketingDepartmentSih";
            }
        }

        public override string QueryPanelKey {
            get {
                return "MarketingDepartmentSih";
            }
        }
    }
}
