﻿using Sunlight.Silverlight.Core.Model;
namespace Sunlight.Silverlight.Dcs.Financial.Views.QueryWindow {
    public class SupplierInformationQueryWindow:DcsQueryWindowBase {
        public SupplierInformationQueryWindow() {
            this.SetDefaultFilterItem(new FilterItem("BranchId", typeof(int), FilterOperator.IsEqualTo, BaseApp.Current.CurrentUserData.EnterpriseId));
        }
        public override string DataGridViewKey {
            get {
                return "SupplierInformation";
            }
        }

        public override string QueryPanelKey {
            get {
                return "SupplierInformation";
            }
        }

    }
}
