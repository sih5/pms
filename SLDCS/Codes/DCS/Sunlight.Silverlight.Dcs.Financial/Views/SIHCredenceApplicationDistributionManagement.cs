﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Financial.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Financial.Views {
    [PageMeta("PartsFinancialAndControls", "PartFunctions", "SIHCredenceApplicationDistribution", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_SUBMIT_ABANDON_AUDIT_APPROVE
    })]
    public class SIHCredenceApplicationDistributionManagement : DcsDataManagementViewBase {
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataDetailView;
        private DataEditViewBase dataApproveView;
        private DataEditViewBase dataEditViewImport;
        private const string DATA_EDIT_VIEW_IMPORT = "_DataEditViewImport_";
        private const string DATA_APPROVE_VIEW = "_dataApproveView_";
        private const string DATA_FIRAPPROVE_VIEW = "_dataFirApproveView_";
        private const string DATA_DETAIL_VIEW = "_DataDetailView_";
        private DcsDomainContext dcsDomainContext = new DcsDomainContext();

        public SIHCredenceApplicationDistributionManagement() {
            this.Title = FinancialUIStrings.DataManagementView_Title_SIHCredenceApplicationManagement;
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.dataDetailView);
            this.RegisterView(DATA_EDIT_VIEW_IMPORT, () => this.DataEditViewImport);
            this.RegisterView(DATA_APPROVE_VIEW, () => this.DataApproveView);
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("SIHCredenceApplication");
                    dataEditView.EditSubmitted += dataEditView_EditSubmitted;
                    dataEditView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataEditViewImport {
            get {
                if(this.dataEditViewImport == null) {
                    this.dataEditViewImport = DI.GetDataEditView("SIHCredenceApplicationForImport");
                    this.dataEditViewImport.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.dataEditViewImport;
            }
        }
        private DataEditViewBase DataApproveView {
            get {
                if(this.dataApproveView == null) {
                    this.dataApproveView = DI.GetDataEditView("SIHCredenceApplicationApprove");
                    this.dataApproveView.EditSubmitted += dataEditView_EditSubmitted;
                    this.dataApproveView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataApproveView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataApproveView = null;
            this.dataEditViewImport = null;
        }
        private void dataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void dataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("SIHCredenceApplicationDistribution");
                    this.dataGridView.RowDoubleClick += DataGridView_RowDoubleClick;
                }
                return this.dataGridView;
            }
        }

        private DataEditViewBase DataEditViewDetail {
            get {
                if(this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("SIHCredenceApplicationDetail");
                    this.dataDetailView.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.dataDetailView;
            }
        }

        private void DataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                this.DataEditViewDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                this.SwitchViewTo(DATA_DETAIL_VIEW);
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                case CommonActionKeys.IMPORT:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitiesIn = this.DataGridView.SelectedEntities.Cast<SIHCreditInfo>().ToArray();
                    if(entitiesIn.Length != 1)
                        return false;
                    return entitiesIn[0].Status == (int)DCSSIHCreditInfoStatus.新增;
                case CommonActionKeys.SUBMIT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitiesSub = this.DataGridView.SelectedEntities.Cast<SIHCreditInfo>().ToArray();
                    if(entitiesSub.Length == 0)
                        return false;
                    return !entitiesSub.Any(o => o.Status != (int)DCSSIHCreditInfoStatus.新增);
                case CommonActionKeys.AUDIT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitiesAud = this.DataGridView.SelectedEntities.Cast<SIHCreditInfo>().ToArray();
                    return !entitiesAud.Any(o => o.Status != (int)DCSSIHCreditInfoStatus.提交) && entitiesAud.Length > 0;
                case CommonActionKeys.APPROVE:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitiesApp = this.DataGridView.SelectedEntities.Cast<SIHCreditInfo>().ToArray();
                    return !entitiesApp.Any(o => o.Status != (int)DCSSIHCreditInfoStatus.审核通过) && entitiesApp.Length > 0;
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var sihCreditInfo = this.DataEditView.CreateObjectToEdit<SIHCreditInfo>();
                    sihCreditInfo.Status = (int)DCSSIHCreditInfoStatus.新增;
                    sihCreditInfo.Type = (int)DCSSIHCreditInfoType.分销中心;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.AUDIT:
                case CommonActionKeys.APPROVE:
                    var conApprove = this.DataGridView.SelectedEntities.Cast<SIHCreditInfo>().ToArray();
                    if(conApprove.Length == 1) {
                        this.DataApproveView.SetObjectToEditById(conApprove.First().GetIdentity());
                        this.SwitchViewTo(DATA_APPROVE_VIEW);
                    } else {
                        var message = conApprove.First().Status == (int)DCSSIHCreditInfoStatus.提交 ? "审核" :
                            conApprove.First().Status == (int)DCSSIHCreditInfoStatus.审核通过 ? "审批" : "";
                        DcsUtils.Confirm(message, () => {
                            dcsDomainContext.批量审核SIH授信服务商申请单(conApprove.Select(r => r.Id).ToArray(), conApprove.First().Status, true, loadOpv => {
                                if(loadOpv.HasError) {
                                    if(!loadOpv.IsErrorHandled)
                                        loadOpv.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOpv);
                                    dcsDomainContext.RejectChanges();
                                    ShellViewModel.Current.IsBusy = false;
                                    return;
                                }
                                UIHelper.ShowNotification("所选数据" + message + "成功");
                                dcsDomainContext.RejectChanges();
                                if(DataGridView != null && DataGridView.FilterItem != null) {
                                    DataGridView.ExecuteQueryDelayed();
                                }
                                CheckActionsCanExecute();
                                ShellViewModel.Current.IsBusy = false;
                            }, null);
                        }, () => {
                            dcsDomainContext.批量审核SIH授信服务商申请单(conApprove.Select(r => r.Id).ToArray(), conApprove.First().Status, false, loadOpv => {
                                if(loadOpv.HasError) {
                                    if(!loadOpv.IsErrorHandled)
                                        loadOpv.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOpv);
                                    dcsDomainContext.RejectChanges();
                                    ShellViewModel.Current.IsBusy = false;
                                    return;
                                }
                                UIHelper.ShowNotification("所选数据驳回成功");
                                dcsDomainContext.RejectChanges();
                                if(DataGridView != null && DataGridView.FilterItem != null) {
                                    DataGridView.ExecuteQueryDelayed();
                                }
                                CheckActionsCanExecute();
                                ShellViewModel.Current.IsBusy = false;
                            }, null);
                        }, message + "通过", "驳回");
                    }
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(FinancialUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<SIHCreditInfo>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废SIH授信服务商申请单)
                                entity.作废SIH授信服务商申请单();
                            this.ExecuteSerivcesMethod(FinancialUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.SUBMIT:
                    var con = this.DataGridView.SelectedEntities.Cast<SIHCreditInfo>().ToArray();
                    DcsUtils.Confirm(FinancialUIStrings.DataManagementView_Confirm_Submit, () => {
                        if(dcsDomainContext == null)
                            return;
                        try {
                            dcsDomainContext.批量提交SIH授信服务商申请单(con.Select(r => r.Id).ToArray(), loadOpv => {
                                if(loadOpv.HasError) {
                                    if(!loadOpv.IsErrorHandled)
                                        loadOpv.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOpv);
                                    dcsDomainContext.RejectChanges();
                                    ShellViewModel.Current.IsBusy = false;
                                    return;
                                }
                                UIHelper.ShowNotification("所选数据提交成功");
                                dcsDomainContext.RejectChanges();
                                if(DataGridView != null && DataGridView.FilterItem != null) {
                                    DataGridView.ExecuteQueryDelayed();
                                }
                                CheckActionsCanExecute();
                                ShellViewModel.Current.IsBusy = false;
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
            }
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                "SIHCredenceApplication"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
            this.ResetEditView();
        }
    }
}
