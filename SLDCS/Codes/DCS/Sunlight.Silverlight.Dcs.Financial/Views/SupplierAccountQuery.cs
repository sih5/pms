﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Financial.Views {
    [PageMeta("PartsFinancialAndControls", "ToPay", "SupplierAccount", ActionPanelKeys = new[] { CommonActionKeys.EXPORT })]
    public class SupplierAccountQuery : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;

        public DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("SupplierAccount"));
            }
        }

        public SupplierAccountQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = FinancialUIStrings.DataManagementView_Title_SupplierAccount;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var companyName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Company.Name") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Company.Name").Value as string;
                    var companyCode = filterItem.Filters.SingleOrDefault(e => e.MemberName == "Company.Code") == null ? null : filterItem.Filters.Single(e => e.MemberName == "Company.Code").Value as string;
                    var partsSalesCategoryId = filterItem.Filters.SingleOrDefault(e => e.MemberName == "PartsSalesCategoryId") == null ? null : filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                    var status = filterItem.Filters.SingleOrDefault(e => e.MemberName == "Status") == null ? null : filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                    var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    if(createTime != null) {
                        createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                        createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.ExportSupplierAccount(BaseApp.Current.CurrentUserData.EnterpriseId, companyName, companyCode, partsSalesCategoryId, status, createTimeBegin, createTimeEnd);
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }


        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            CompositeFilterItem compositeFilter;
            if(!(filterItem is CompositeFilterItem)) {
                compositeFilter = new CompositeFilterItem();
                compositeFilter.Filters.Add(filterItem);
            } else
                compositeFilter = filterItem as CompositeFilterItem;
            ClientVar.ConvertTime(compositeFilter);
            compositeFilter.Filters.Add(new FilterItem {
                MemberName = "BuyerCompanyId",
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId,
                Operator = FilterOperator.IsEqualTo
            });
            this.DataGridView.FilterItem = compositeFilter;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "SupplierAccount"
                };
            }
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private void ExportSupplierAccount(int buyerCompanyId, string companyName, string companyCode, int? partsSalesCategoryId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportSupplierAccountAsync(null, buyerCompanyId, companyName, companyCode, partsSalesCategoryId, status, createTimeBegin, createTimeEnd);
            this.excelServiceClient.ExportSupplierAccountCompleted -= ExcelServiceClientOnExportSupplierAccountCompleted;
            this.excelServiceClient.ExportSupplierAccountCompleted += ExcelServiceClientOnExportSupplierAccountCompleted;

        }

        private void ExcelServiceClientOnExportSupplierAccountCompleted(object sender, ExportSupplierAccountCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
    }
}
