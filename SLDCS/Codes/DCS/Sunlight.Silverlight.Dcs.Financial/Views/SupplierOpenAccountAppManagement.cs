﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Financial.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Financial.Views {
    [PageMeta("PartsFinancialAndControls", "ToPay", "SupplierOpenAccountApp", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_APPROVE
    })]
    public class SupplierOpenAccountAppManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;

        public SupplierOpenAccountAppManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = FinancialUIStrings.DataManagementView_Title_SupplierOpenAccountApp;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("SupplierOpenAccountApp"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("SupplierOpenAccountApp");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);

            CompositeFilterItem compositeFilter;

            if(!(filterItem is CompositeFilterItem)) {
                compositeFilter = new CompositeFilterItem();
                compositeFilter.Filters.Add(filterItem);
            } else
                compositeFilter = filterItem as CompositeFilterItem;
            ClientVar.ConvertTime(compositeFilter);
            this.DataGridView.FilterItem = compositeFilter;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "SupplierOpenAccountApp"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var supplierOpenAccountApp = this.DataEditView.CreateObjectToEdit<SupplierOpenAccountApp>();
                    supplierOpenAccountApp.Status = (int)DcsMasterDataStatus.有效;
                    supplierOpenAccountApp.BuyerCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.APPROVE:
                    DcsUtils.Confirm(FinancialUIStrings.DataManagementView_Confirm_Approve, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<SupplierOpenAccountApp>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can审核供应商开户申请单)
                                entity.审核供应商开户申请单();
                            this.ExecuteSerivcesMethod(FinancialUIStrings.DataManagementView_Notification_ApproveSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(FinancialUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<SupplierOpenAccountApp>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废供应商开户申请单)
                                entity.作废供应商开户申请单();
                            this.ExecuteSerivcesMethod(FinancialUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
            }
        }


        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.APPROVE:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<SupplierOpenAccountApp>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsWorkflowOfSimpleApprovalStatus.新建;
                default:
                    return false;
            }
        }
    }
}
