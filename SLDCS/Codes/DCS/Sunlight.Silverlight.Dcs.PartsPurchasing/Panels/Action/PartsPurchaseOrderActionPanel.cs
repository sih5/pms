﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Action {
    public class PartsPurchaseOrderActionPanel : DcsActionPanelBase {
        public PartsPurchaseOrderActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "PartsPurchaseOrder",
                Title = PartsPurchasingUIStrings.Action_Title_PartsPurchaseOrder,
                ActionItems = new[] {
                    new ActionItem {
                        Title =PartsPurchasingUIStrings.Action_Title_PendingConfirmation,
                        UniqueId = "PendingApproval",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/PartsPurchasing/PendingApproval.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title =PartsPurchasingUIStrings.Action_Title_ChannelChange,
                        UniqueId = "ChannelChange",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/PartsPurchasing/ChannelChange.png"),
                        CanExecute = false
                    },new ActionItem{
                       Title=PartsPurchasingUIStrings.Action_Title_ReplaceShip,
                       UniqueId="ReplaceShip",
                       ImageUri =Utils.MakeServerUri("Client/Dcs/Images/Operations/shipping.png"),
                       CanExecute=false 
                    },new ActionItem{
                       Title= PartsPurchasingUIStrings.Action_Title_Resend,
                       UniqueId="Rsend",
                       ImageUri =Utils.MakeServerUri("Client/Dcs/Images/Menu/Service/UsedPartsReturnOrder.png"),
                       CanExecute=false 
                    },
                    new ActionItem{
                       Title= PartsPurchasingUIStrings.Action_Title_OrderReplaceShipAll,
                       UniqueId="ReplaceShipAll",
                       ImageUri =Utils.MakeServerUri("Client/Dcs/Images/Operations/shipping.png"),
                       CanExecute=false 
                    },
                }
            };
        }
    }
}
