﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Action {
    public class PartsPurchaseOrderSupplierActionPanel : DcsActionPanelBase {
        public PartsPurchaseOrderSupplierActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = PartsPurchasingUIStrings.Action_Title_PartsPurchaseOrder,
                ActionItems = new[]{
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_Confirm,
                        UniqueId=CommonActionKeys.CONFIRM,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/PartsPurchasing/PendingApproval.png"),
                        CanExecute = false
                    },new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_Terminate,
                        UniqueId=CommonActionKeys.TERMINATE,
                        ImageUri=Utils.MakeServerUri("Client/Dcs/Images/Operations/Terminate.png"),
                        CanExecute=false
                    },new ActionItem{
                       Title=PartsPurchasingUIStrings.Action_Title_Ship,
                       UniqueId="ReplaceShip",
                        ImageUri =Utils.MakeServerUri("Client/Dcs/Images/Operations/shipping.png"),
                       CanExecute=false 
                    },new ActionItem{
                       Title=PartsPurchasingUIStrings.Action_Title_ReplaceShipAll,
                       UniqueId="ReplaceShipAll",
                        ImageUri =Utils.MakeServerUri("Client/Dcs/Images/Operations/shipping.png"),
                       CanExecute=false 
                    },new ActionItem{
                        Title= PartsPurchasingUIStrings.Action_Title_CommonPrint,
                        UniqueId=CommonActionKeys.PRINT,
                        ImageUri=Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute=false
                    },new ActionItem{
                        Title= PartsPurchasingUIStrings.Action_Title_MerageExport,
                        UniqueId=CommonActionKeys.MERGEEXPORT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/MergeExport.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
