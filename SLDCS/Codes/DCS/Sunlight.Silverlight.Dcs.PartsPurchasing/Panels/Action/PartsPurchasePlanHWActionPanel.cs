﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Action {
    public class PartsPurchasePlanHWActionPanel:DcsActionPanelBase {
        public PartsPurchasePlanHWActionPanel() {
            this.ActionItemGroup=new ActionItemGroup{
                UniqueId="PartsPurchasePlanHW",
                Title=PartsPurchasingUIStrings.Action_Title_PartsPurchasePlanHW,
                ActionItems = new[]{
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_DataCheck,
                        UniqueId ="DataCheck",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/CheckStock.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_Analyze,
                        UniqueId ="Analyze",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Edit.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_Termination1,
                        UniqueId ="Terminate",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Terminate.png"),
                        CanExecute=false
                    }
                }
            };
        }
    }
}
