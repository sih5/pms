﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Action {
    public class PartsPurchasePlanOrderActionPanel : DcsActionPanelBase {
        public PartsPurchasePlanOrderActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "PartsPurchasePlanOrder",
                Title = PartsPurchasingUIStrings.ActionPanel_Title_PartsPurchasePlanOrder,
                ActionItems = new[]{
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_Add,
                        UniqueId ="Add",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Add.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_Edit,
                        UniqueId ="Edit",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Edit.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_Submit,
                        UniqueId ="Submit",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Submit.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title= PartsPurchasingUIStrings.Action_Title_FirstApprove,
                        UniqueId ="FirstApprove",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/InitialApprove.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title= PartsPurchasingUIStrings.Action_Title_InitialApprove,
                        UniqueId ="InitialApprove",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/InitialApprove.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title=  PartsPurchasingUIStrings.Action_Title_FinalApprove,
                        UniqueId ="FinalApprove",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/FinalApprove.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_Abandon,
                        UniqueId ="Abandon",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Abandon.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_MerageExport,
                        UniqueId ="Export",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/ExportAll.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_CommonPrint,
                        UniqueId ="Print",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute=false
                    }
                }
            };
        }
    }
}
