﻿
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Action {
    public class PartsPurchaseRtnSettleBillActionPanel : DcsActionPanelBase {
        public PartsPurchaseRtnSettleBillActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "PartsPurchaseRtnSettleBill",
                Title = PartsPurchasingUIStrings.ActionPanel_Title_PartsPurchaseRtnSettleBill,
                ActionItems = new[] {
                    new ActionItem {
                        Title =PartsPurchasingUIStrings.Action_Title_PartsPurchaseRtnSettleBillInvoiceRegister,
                        UniqueId = "InvoiceRegister",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/PartsStocking/TransferKanBan.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title =PartsPurchasingUIStrings.Action_Title_PartsPurchaseRtnSettleBillReversesettle,
                        UniqueId = "AntiSettlement",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/PartsStocking/TransferKanBan.png"),
                        CanExecute = false
                    },new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_SettllementCostQuery,
                        UniqueId="SettlementCostSearch",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Query.png"),
                        CanExecute=false
                    },new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_MerageExport,
                        UniqueId=CommonActionKeys.MERGEEXPORT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/MergeExport.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
