﻿
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Action {
    public class PartsPurchaseSettleBillForSupplierActionPanel : DcsActionPanelBase {

        public PartsPurchaseSettleBillForSupplierActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                Title = PartsPurchasingUIStrings.ActionPanel_Title_PartsPurchaseSettleBill,
                UniqueId = "PartsPurchaseSettleBillForSupplier",
                ActionItems = new[] {
                    new ActionItem {
                        Title = PartsPurchasingUIStrings.Action_Title_PartsPurchaseSettleBillInvoiceRegister,
                        UniqueId = "InvoiceRegister",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/InvoiceRegister.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = PartsPurchasingUIStrings.Action_Title_PartsPurchaseSettleBillInvoiceEdit,
                        UniqueId = "InvoiceEdit",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/InvoiceEdit.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = PartsPurchasingUIStrings.Action_Title_Print,
                        UniqueId = "PrintForGC",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
