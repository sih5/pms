﻿
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Action {
    public class SupplierPreApprovedLoanActionPanel : DcsActionPanelBase {
        public SupplierPreApprovedLoanActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                Title = PartsPurchasingUIStrings.ActionPanel_Title_SupplierPreApprovedLoan,
                UniqueId = "SupplierPreApprovedLoan",
                ActionItems = new[] {
                    new ActionItem {
                        UniqueId="PrintAll",
                        Title=PartsPurchasingUIStrings.Action_Title_PrintAll,
                        CanExecute = false,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                    },new ActionItem {
                        UniqueId = "ZeroPrint",
                        Title = PartsPurchasingUIStrings.ActionPanel_Title_ZeroPrint,
                        CanExecute = false,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                    },new ActionItem {
                        UniqueId = "NonZeroPrint",
                        Title = PartsPurchasingUIStrings.ActionPanel_Title_NonZeroPrint,
                        CanExecute = false,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                    },new ActionItem {
                        UniqueId = "Print",
                        CanExecute = false,
                        Title = PartsPurchasingUIStrings.Action_Title_CommonPrint,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                    },new ActionItem {
                        UniqueId = "ExportDetail",
                        CanExecute = false,
                        Title = PartsPurchasingUIStrings.ActionPanel_Title_ExportDetail,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Export.png"),
                    }
                }
            };
        }

    }
}
