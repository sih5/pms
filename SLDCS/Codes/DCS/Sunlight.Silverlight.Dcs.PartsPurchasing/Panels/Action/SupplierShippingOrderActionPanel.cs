﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Action {
    public class SupplierShippingOrderActionPanel : DcsActionPanelBase {
        public SupplierShippingOrderActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "SupplierShippingOrder",
                Title = PartsPurchasingUIStrings.Action_Title_SupplierShippingOrder,
                ActionItems = new[] {
                    new ActionItem {
                        Title =PartsPurchasingUIStrings.Action_Title_PendingConfirmationSupplier,
                        UniqueId = "PendingConfirmationSupplier",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/PartsStocking/PendingApproval.png"),
                        CanExecute = false
                    },  new ActionItem {
                        Title =PartsPurchasingUIStrings.Action_Title_A4Print,
                        UniqueId = "A4Print",
                       ImageUri=Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
