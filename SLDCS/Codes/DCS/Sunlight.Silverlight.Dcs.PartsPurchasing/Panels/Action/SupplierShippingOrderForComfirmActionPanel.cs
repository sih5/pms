﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Action {
    public class SupplierShippingOrderForComfirmActionPanel : DcsActionPanelBase {
        public SupplierShippingOrderForComfirmActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = PartsPurchasingUIStrings.ActionPanel_Title_PartsPurReturnOrderForBranch,
                ActionItems = new[] {
                    new ActionItem {
                        Title = "代到货确认",
                        UniqueId = "DConFirm",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Confirm.png")
                    },
                    new ActionItem {
                        Title = PartsPurchasingUIStrings.Action_Title_InitialApprove,
                        UniqueId = CommonActionKeys.APPROVE,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Approve.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        Title = PartsPurchasingUIStrings.Action_Title_MerageExport,
                        UniqueId = CommonActionKeys.MERGEEXPORT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/MergeExport.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        Title = "修改物流",
                        UniqueId = CommonActionKeys.EDIT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Edit.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
