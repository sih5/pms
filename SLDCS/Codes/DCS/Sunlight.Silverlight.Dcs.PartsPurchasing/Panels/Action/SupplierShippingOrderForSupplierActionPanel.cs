﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Action {
    public class SupplierShippingOrderForSupplierActionPanel : DcsActionPanelBase {
        public SupplierShippingOrderForSupplierActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = PartsPurchasingUIStrings.ActionPanel_Title_PartsPurReturnOrderForBranch,
                ActionItems = new[] {
                    new ActionItem {
                        Title = PartsPurchasingUIStrings.Action_Title_Add,
                        UniqueId = CommonActionKeys.ADD,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Add.png")
                    },
                    new ActionItem {
                        Title = PartsPurchasingUIStrings.Action_Title_Edit,
                        UniqueId = CommonActionKeys.EDIT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Edit.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        Title = PartsPurchasingUIStrings.Action_Title_Abandon,
                        UniqueId = CommonActionKeys.ABANDON,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Abandon.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        Title = PartsPurchasingUIStrings.Action_Title_Export,
                        UniqueId = CommonActionKeys.EXPORT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Export.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        Title = PartsPurchasingUIStrings.Action_Title_Import,
                        UniqueId = CommonActionKeys.IMPORT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        Title = PartsPurchasingUIStrings.Action_Title_CommonPrint,
                        UniqueId = CommonActionKeys.PRINT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        Title = PartsPurchasingUIStrings.Action_Title_PrintNoPrice,
                        UniqueId = "PrintNoPrice",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                   },
                    //new ActionItem {
                    //    Title = "标签打印",
                    //    UniqueId = "PrintLabel",
                    //    ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                    //    CanExecute = false
                    //},
                    new ActionItem {
                        Title = PartsPurchasingUIStrings.Action_Title_MerageExport,
                        UniqueId = CommonActionKeys.MERGEEXPORT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/MergeExport.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title =PartsPurchasingUIStrings.Action_Title_BigLablePrint,
                        UniqueId = "ChPrint",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title =PartsPurchasingUIStrings.Action_Title_SmallLablePrint,
                        UniqueId = "PcPrint",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
