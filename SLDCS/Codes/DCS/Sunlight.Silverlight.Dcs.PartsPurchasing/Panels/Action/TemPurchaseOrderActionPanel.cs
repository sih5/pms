﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Action {
    public class TemPurchaseOrderActionPanel : DcsActionPanelBase {
        public TemPurchaseOrderActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "TemPurchaseOrder",
                Title = PartsPurchasingUIStrings.ActionPanel_Title_PartsPurReturnOrderForBranch,
                ActionItems = new[]{                   
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_Submit,
                        UniqueId ="Submit",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Submit.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_PendingConfirmation,
                        UniqueId ="PendingConfirmation",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/ApproveEdit.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_Terminate,
                        UniqueId ="Terminate",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Terminate.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_Force,
                        UniqueId ="Force",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/View.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_ReplaceShip,
                        UniqueId ="ReplaceShip",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/shipping.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                       Title= PartsPurchasingUIStrings.Action_Title_OrderReplaceShipAll,
                       UniqueId="ReplaceShipAll",
                       ImageUri =Utils.MakeServerUri("Client/Dcs/Images/Operations/shipping.png"),
                       CanExecute=false 
                    },
                    new ActionItem{
                       Title= PartsPurchasingUIStrings.Action_Title_AppRoveSubmit,
                       UniqueId="AppRoveSubmit",
                       ImageUri =Utils.MakeServerUri("Client/Dcs/Images/Operations/PickConfirm.png"),
                       CanExecute=false 
                    },
                    new ActionItem{
                       Title= PartsPurchasingUIStrings.Action_Title_InitialApprove,
                       UniqueId="InitialApprove",
                       ImageUri =Utils.MakeServerUri("Client/Dcs/Images/Operations/InitialApprove.png"),
                       CanExecute=false 
                    },
                    new ActionItem{
                       Title= PartsPurchasingUIStrings.Action_Title_FinalApprove,
                       UniqueId="FinalApprove",
                       ImageUri =Utils.MakeServerUri("Client/Dcs/Images/Operations/FinalApprove.png"),
                       CanExecute=false 
                    },
                    new ActionItem{
                       Title= PartsPurchasingUIStrings.Action_Title_CommonPrint,
                       UniqueId="Print",
                       ImageUri =Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                       CanExecute=false 
                    },
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_MerageExport,
                        UniqueId ="Export",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/ExportAll.png"),
                        CanExecute=false
                    }
                }
            };
        }
    }
}