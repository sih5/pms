﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Action {
    public class TemPurchaseOrderSuplierActionPanel: DcsActionPanelBase {
        public TemPurchaseOrderSuplierActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "TemPurchaseOrderSuplier",
                Title = PartsPurchasingUIStrings.ActionPanel_Title_PartsPurReturnOrderForBranch,
                ActionItems = new[]{                   
                  
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_Confirm,
                        UniqueId ="PendingConfirmation",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/ApproveEdit.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_Ship,
                        UniqueId ="ReplaceShip",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/shipping.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                       Title= PartsPurchasingUIStrings.Action_Title_ReplaceShipAll,
                       UniqueId="ReplaceShipAll",
                       ImageUri =Utils.MakeServerUri("Client/Dcs/Images/Operations/shipping.png"),
                       CanExecute=false 
                    },
                    new ActionItem{
                       Title= PartsPurchasingUIStrings.Action_Title_CommonPrint,
                       UniqueId="Print",
                       ImageUri =Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                       CanExecute=false 
                    },
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_MerageExport,
                        UniqueId ="Export",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/ExportAll.png"),
                        CanExecute=false
                    }
                }
            };
        }
    }
}