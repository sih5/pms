﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Action {
    public class TemPurchasePlanOrderActionPanel : DcsActionPanelBase {
        public TemPurchasePlanOrderActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "TemPurchasePlanOrder",
                Title = PartsPurchasingUIStrings.Action_Title_TmpPartsPurchasePlanOrder,
                ActionItems = new[]{
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_TmpPartsPurchasePlanOrderReplace,
                        UniqueId ="AddByDeputy",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/AddByDeputy.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_InitialApprove,
                        UniqueId ="Approve",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Approve.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_Confirm,
                        UniqueId ="Confirm",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Confirm.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_Check,
                        UniqueId ="Check",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/FinalApprove.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_MerageExport,
                        UniqueId ="Export",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/ExportAll.png"),
                        CanExecute=false
                    }
                }
            };
        }
    }
}
