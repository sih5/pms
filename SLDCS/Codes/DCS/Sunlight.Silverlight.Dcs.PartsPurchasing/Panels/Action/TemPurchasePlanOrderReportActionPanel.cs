﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Action {
    public class TemPurchasePlanOrderReportActionPanel: DcsActionPanelBase {
        public TemPurchasePlanOrderReportActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "TemPurchasePlanOrderReport",
                Title = PartsPurchasingUIStrings.Action_Title_TmpPartsPurchasePlanOrder,
                ActionItems = new[]{
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_Add,
                        UniqueId ="Add",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Add.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_Edit,
                        UniqueId ="Edit",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Edit.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_Submit,
                        UniqueId ="Submit",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Submit.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_Abandon,
                        UniqueId ="Abandon",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Abandon.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_MerageExport,
                        UniqueId ="Export",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/ExportAll.png"),
                        CanExecute=false
                    }
                }
            };
        }
    }
}
