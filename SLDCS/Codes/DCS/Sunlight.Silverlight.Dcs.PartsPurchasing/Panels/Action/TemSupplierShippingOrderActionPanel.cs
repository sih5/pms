﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Action {
    public class TemSupplierShippingOrderActionPanel: DcsActionPanelBase {
        public TemSupplierShippingOrderActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "TemSupplierShippingOrder",
                Title = PartsPurchasingUIStrings.ActionPanel_Title_PartsPurReturnOrderForBranch,
                ActionItems = new[]{                   
                  
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_ReplaceConfirm,
                        UniqueId =CommonActionKeys.CONFIRM,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Confirm.png"),
                        CanExecute=false
                    }, new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_Edit,
                        UniqueId =CommonActionKeys.EDIT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Edit.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_Terminate,
                        UniqueId =CommonActionKeys.TERMINATE,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Terminate.png"),
                        CanExecute=false
                    },
                    //new ActionItem{
                    //    Title=PartsPurchasingUIStrings.Action_Title_Force,
                    //    UniqueId ="Force",
                    //    ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/View.png"),
                    //    CanExecute=false
                    //},
                    new ActionItem{
                       Title= PartsPurchasingUIStrings.Action_Title_CommonPrint,
                       UniqueId=CommonActionKeys.PRINT,
                       ImageUri =Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                       CanExecute=false 
                    },
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_MerageExport,
                        UniqueId =CommonActionKeys.EXPORT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/ExportAll.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_A4Print,
                        UniqueId ="A4Print",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute=false
                    }
                }
            };
        }
    }
}