﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Action
{
    public class TemSupplierShippingOrderConFirmActionPanel: DcsActionPanelBase {
        public TemSupplierShippingOrderConFirmActionPanel()
        {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "TemSupplierShippingOrderConfirm",
                Title = PartsPurchasingUIStrings.ActionPanel_Title_PartsPurReturnOrderForBranch,
                ActionItems = new[]{                   
                  
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_Confirm,
                        UniqueId =CommonActionKeys.CONFIRM,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Confirm.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title=PartsPurchasingUIStrings.Action_Title_MerageExport,
                        UniqueId =CommonActionKeys.EXPORT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/ExportAll.png"),
                        CanExecute=false
                    },
                }
            };
        }
    }
}