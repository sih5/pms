﻿using System.Collections.ObjectModel;
using Sunlight.Silverlight.Dcs.Web;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.DataEdit {
    public partial class DepartmentInformationDataEditPanel {
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();

        public ObservableCollection<KeyValuePair> KvBranches {
            get {
                return this.kvBranches;
            }
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetBranchesQuery().Where(entity => entity.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var branch in loadOp.Entities)
                    this.kvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name
                    });
            }, null);
        }

        public DepartmentInformationDataEditPanel() {
            InitializeComponent();
            this.Initializer.Register(this.Initialize);
        }
    }
}
