﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;


namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.DataEdit {
    public partial class InternalAllocationBillDetailDataEditPanel{
        private ObservableCollection<KeyValuePair> kvWarehouses;
        private ObservableCollection<KeyValuePair> kvDepartments;
        private readonly string[] kvNames = {
            "InternalAllocationBill_Type"
        };
        public InternalAllocationBillDetailDataEditPanel() {
            InitializeComponent();
            this.Loaded += InternalAllocationBillForApproveDataEditPanel_Loaded;
            this.KeyValueManager.LoadData();
            this.KeyValueManager.Register(this.kvNames);
        }

        private void InternalAllocationBillForApproveDataEditPanel_Loaded(object sender, RoutedEventArgs e) {
            this.CreateUI();
        }

        public object KvTypes
        {
            get
            {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
        public ObservableCollection<KeyValuePair> KvWarehouses {
            get {
                return this.kvWarehouses ?? (this.kvWarehouses = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvDepartments {
            get {
                return this.kvDepartments ?? (this.kvDepartments = new ObservableCollection<KeyValuePair>());
            }
        }

        private void CreateUI() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetWarehousesOrderByNameQuery().Where(e => e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvWarehouses.Clear();
                foreach(var warehouse in loadOp.Entities)
                    this.KvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name,
                    });
                domainContext.Load(domainContext.GetDepartmentInformationsQuery().Where(e => e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效), loadOption => {
                    if(loadOp.HasError)
                        return;
                    this.KvDepartments.Clear();
                    foreach(var departInfo in loadOption.Entities)
                        this.KvDepartments.Add(new KeyValuePair {
                            Key = departInfo.Id,
                            Value = departInfo.Name,
                        });
                }, null);
            }, null);
        }
    }
}