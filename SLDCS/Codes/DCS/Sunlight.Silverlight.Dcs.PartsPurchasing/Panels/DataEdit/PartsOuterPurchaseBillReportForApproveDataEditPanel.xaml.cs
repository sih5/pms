﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Windows;
using System.Linq;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using System.Collections.ObjectModel;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.View;
using System.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.DataEdit {
    public partial class PartsOuterPurchaseBillReportForApproveDataEditPanel {

        private ObservableCollection<KeyValuePair> kvIsPasseds;
        public ObservableCollection<KeyValuePair> KvIsPasseds
        {
            get
            {
                return this.kvIsPasseds ?? (this.kvIsPasseds = new ObservableCollection<KeyValuePair>());
            }
        }

        public PartsOuterPurchaseBillReportForApproveDataEditPanel()
        {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI()
        {
            this.kvIsPasseds.Add(new KeyValuePair
            {
                Key = 1,
                Value = PartsPurchasingUIStrings.DataEditPanel_Title_ApprovePassed
            });
            this.kvIsPasseds.Add(new KeyValuePair
            {
                Key = 0,
                Value = PartsPurchasingUIStrings.DataEditPanel_Title_Reject
            });
            
        }

    }
}
