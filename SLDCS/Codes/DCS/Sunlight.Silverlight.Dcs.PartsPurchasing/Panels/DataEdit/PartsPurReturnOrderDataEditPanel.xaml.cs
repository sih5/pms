﻿
namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.DataEdit {
    public partial class PartsPurReturnOrderDataEditPanel {
        private readonly string[] kvNames = {
            "PartsPurReturnOrder_ReturnReason"
        };

        public PartsPurReturnOrderDataEditPanel() {
            this.KeyValueManager.Register(this.kvNames);
            InitializeComponent();
        }
    }
}
