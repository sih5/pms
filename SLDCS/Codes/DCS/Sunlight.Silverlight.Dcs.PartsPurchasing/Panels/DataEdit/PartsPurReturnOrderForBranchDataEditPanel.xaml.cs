﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.DataEdit {
    public partial class PartsPurReturnOrderForBranchDataEditPanel {
        private readonly string[] kvNames = {
            "PartsPurReturnOrder_ReturnReason"
        };
        private readonly DcsDomainContext domainContext = new DcsDomainContext();
        private ObservableCollection<KeyValuePair> kvWarehouses;
        private ObservableCollection<KeyValuePair> kvPartsSalesCategoryName;
        public PartsPurReturnOrderForBranchDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += PartsPurReturnOrderForBranchDataEditPanel_DataContextChanged;
            this.Loaded += PartsPurReturnOrderForBranchDataEditPanel_Loaded;

        }

        private void PartsPurReturnOrderForBranchDataEditPanel_Loaded(object sender, RoutedEventArgs e)
        {
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(er => er.Status == (int)DcsBaseDataStatus.有效 && er.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if (loadOp.HasError)
                    return;
                this.kvPartsSalesCategoryName.Clear();
                foreach (var partsSalesCategory in loadOp.Entities)
                    this.kvPartsSalesCategoryName.Add(new KeyValuePair
                    {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
                this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            }, null);
        }

        private void PartsPurReturnOrderForBranchDataEditPanel_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsPurReturnOrder = this.DataContext as PartsPurReturnOrder;
            if(partsPurReturnOrder == null)
                return;
            partsPurReturnOrder.PropertyChanged -= partsPurReturnOrder_PropertyChanged;
            partsPurReturnOrder.PropertyChanged += partsPurReturnOrder_PropertyChanged;
        }

        private void partsPurReturnOrder_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            var partsPurReturnOrder = this.DataContext as PartsPurReturnOrder;
            if(partsPurReturnOrder == null)
                return;

            switch(e.PropertyName) {

                case "WarehouseId":
                    var selectedWarehouse = this.KvWarehouses.First(v => v.Key == partsPurReturnOrder.WarehouseId).UserObject as Warehouse;
                    if(selectedWarehouse == null)
                        return;
                    partsPurReturnOrder.WarehouseName = selectedWarehouse.Name;
                    partsPurReturnOrder.WarehouseAddress = selectedWarehouse.Address;
                    break;
                case "ReturnReason":
                    partsPurReturnOrder.ValidationErrors.Clear();
                    break;
            }
        }


        private void queryWindowPartsPurchaseOrder_Loaded(object sender, RoutedEventArgs e) {
            var partsPurReturnOrder = this.DataContext as PartsPurReturnOrder;
            var queryWindowPartsPurchaseOrder = sender as DcsMultiSelectQueryWindowBase;
            if(queryWindowPartsPurchaseOrder == null)
                return;

            if(partsPurReturnOrder == null)
                return;
            if(partsPurReturnOrder.PartsSupplierId == default(int)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditPanel_Text_PartsPurReturnOrder_PartsSupplierIsNull);

                var parent = queryWindowPartsPurchaseOrder.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
                return;
            }
            if(partsPurReturnOrder.BranchId == default(int)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditPanel_Text_PartsPurReturnOrder_BranchIsNull);

                var parent = queryWindowPartsPurchaseOrder.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
                return;
            }
            queryWindowPartsPurchaseOrder.ExchangeData(null, "SetQueryItemValue", new object[] { "Common", "BranchName", partsPurReturnOrder.BranchName });
            queryWindowPartsPurchaseOrder.ExchangeData(null, "SetQueryItemValue", new object[] { "Common", "PartsSupplierName", partsPurReturnOrder.PartsSupplierName });
            queryWindowPartsPurchaseOrder.ExchangeData(null, "SetQueryItemValue", new object[] { "Common", "PartsSalesCategoryId", partsPurReturnOrder.PartsSalesCategoryId });
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsSupplierId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsPurReturnOrder.PartsSupplierId
            });
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "BranchId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsPurReturnOrder.BranchId
            });

            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "Status",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = (int)DcsPartsPurchaseOrderStatus.发运完毕
            });

            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "Status",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = (int)DcsPartsPurchaseOrderStatus.部分发运
            });

            queryWindowPartsPurchaseOrder.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
            queryWindowPartsPurchaseOrder.ExchangeData(null, "ExecuteQuery", null);

        }
        private void CreateUI() {
            //domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    foreach(var partsSalesCategory in loadOp.Entities)
            //        this.kvPartsSalesCategoryName.Add(new KeyValuePair {
            //            Key = partsSalesCategory.Id,
            //            Value = partsSalesCategory.Name
            //        });
            //    this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            //}, null);
            var queryWindowPartsSupplier = DI.GetQueryWindow("PartsSupplier");
            queryWindowPartsSupplier.SelectionDecided += queryWindowPartsSupplier_SelectionDecided;
            this.ptbPartsSupplierCode.PopupContent = queryWindowPartsSupplier;

            var queryWindowPartsPurchaseOrder = DI.GetQueryWindow("PartsInboundCheckBillDetailForPartsPurchaseOrder") as DcsMultiSelectQueryWindowBase;
            queryWindowPartsPurchaseOrder.Loaded += queryWindowPartsPurchaseOrder_Loaded;
            queryWindowPartsPurchaseOrder.SelectionDecided += queryWindowPartsPurchaseOrder_SelectionDecided;
            this.ptbPartsPurchaseOrderCode.PopupContent = queryWindowPartsPurchaseOrder;
        }

        private void PartsSalesCategory_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var partsPurReturnOrder = this.DataContext as PartsPurReturnOrder;
            if(partsPurReturnOrder == null)
                return;
            domainContext.Load(this.domainContext.GetWarehousesByPartsSalesCategoryIdAndOwnerCompanyIdQuery(partsPurReturnOrder.PartsSalesCategoryId, BaseApp.Current.CurrentUserData.EnterpriseId).Where(r => r.Type != (int)DcsWarehouseType.虚拟库), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                this.KvWarehouses.Clear();
                foreach(var warehouse in loadOp.Entities)
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name,
                        UserObject = warehouse
                    });
            }, null);
            partsPurReturnOrder.WarehouseAddress = null;
        }

        private void queryWindowPartsPurchaseOrder_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiSelectQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            
            var partsInboundCheckBillDetails = queryWindow.SelectedEntities.Cast<PartsInboundCheckBillDetail>();
            if(partsInboundCheckBillDetails == null)
                return;

            var partsPurReturnOrder = this.DataContext as PartsPurReturnOrder;
            if(partsPurReturnOrder == null)
                return;
            if (partsInboundCheckBillDetails.Any()) {
                    if (partsInboundCheckBillDetails.First().PartsInboundCheckBill != null)
                        partsPurReturnOrder.PartsPurchaseOrderId = partsInboundCheckBillDetails.First().PartsInboundCheckBill.OriginalRequirementBillId;
                    partsPurReturnOrder.PartsPurchaseOrderCode = partsInboundCheckBillDetails.First().OriginalRequirementBillCode;
                }
            //查询可退货数量
            var partIds = partsInboundCheckBillDetails.Select(r => r.SparePartId).Distinct();
            this.domainContext.Load(this.domainContext.查询已退货数量Query(partsPurReturnOrder.PartsPurchaseOrderCode, partIds.ToArray(),partsPurReturnOrder.Id,partsPurReturnOrder.PartsSupplierId), LoadBehavior.RefreshCurrent, loadOp => {
                if (loadOp.HasError) {
                    if (!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    return;
                }
                var datas = loadOp.Entities.ToList();
                if (datas.Any()) {
                    foreach (var data in datas) {
                        var details = partsInboundCheckBillDetails.Where(r => r.SparePartId == data.SparePartId);
                        //var allInspectedQuantity = details.Sum(r => r.InspectedQuantity);
                        foreach (var detail in details) {
                            var allInspectedQuantity = detail.AllInspectedQuantity;
                            //可退货量
                            detail.ReturnableQuantity = allInspectedQuantity - data.ReturnedQuantity;
                        }
                    }
                }

                foreach (var detail in partsPurReturnOrder.PartsPurReturnOrderDetails.ToArray()) {
                    partsPurReturnOrder.PartsPurReturnOrderDetails.Remove(detail);
                }
                foreach (var partsInboundCheckBillDetail in partsInboundCheckBillDetails) {
                    //同一配件数量合并
                    var partsPurReturnOrderDetail = partsPurReturnOrder.PartsPurReturnOrderDetails.FirstOrDefault(r => r.SparePartId == partsInboundCheckBillDetail.SparePartId);

                    if (partsPurReturnOrderDetail == null) {
                        partsPurReturnOrderDetail = new PartsPurReturnOrderDetail();
                        var maxSerialNumber = partsPurReturnOrder.PartsPurReturnOrderDetails.Count > 0 ? partsPurReturnOrder.PartsPurReturnOrderDetails.Max(entity => entity.SerialNumber) + 1 : 1;
                        partsPurReturnOrderDetail.SerialNumber = maxSerialNumber;
                        partsPurReturnOrderDetail.SparePartId = partsInboundCheckBillDetail.SparePartId;
                        partsPurReturnOrderDetail.SparePartName = partsInboundCheckBillDetail.SparePartName;
                        partsPurReturnOrderDetail.SparePartCode = partsInboundCheckBillDetail.SparePartCode;
                        partsPurReturnOrderDetail.InspectedQuantity = partsInboundCheckBillDetail.InspectedQuantity;
                        partsPurReturnOrderDetail.SupplierPartCode = datas.FirstOrDefault(r => r.SparePartId == partsPurReturnOrderDetail.SparePartId) == null ? null : datas.FirstOrDefault(r => r.SparePartId == partsPurReturnOrderDetail.SparePartId).SupplierPartCode;
                        if (partsInboundCheckBillDetail.ReturnableQuantity == null) {
                            partsPurReturnOrderDetail.Quantity = partsInboundCheckBillDetail.InspectedQuantity;
                        } else {
                            partsPurReturnOrderDetail.Quantity = (int)partsInboundCheckBillDetail.ReturnableQuantity;
                        }
                        partsPurReturnOrderDetail.UnitPrice = partsInboundCheckBillDetail.SettlementPrice;
                        partsPurReturnOrderDetail.CheckUnitPrice = partsInboundCheckBillDetail.SettlementPrice;
                        partsPurReturnOrderDetail.MeasureUnit = partsInboundCheckBillDetail.SparePart.MeasureUnit;
                        partsPurReturnOrder.PartsPurReturnOrderDetails.Add(partsPurReturnOrderDetail);
                    } else {
                        partsPurReturnOrderDetail.InspectedQuantity += partsInboundCheckBillDetail.InspectedQuantity;
                    }
                }
                foreach (var detail in partsPurReturnOrder.PartsPurReturnOrderDetails) {
                    //如果可退货量 > 检验量，显示为检验量
                    if (detail.Quantity > detail.InspectedQuantity)
                        detail.Quantity = (int)detail.InspectedQuantity;
                }
                
                partsPurReturnOrder.TotalAmount = partsPurReturnOrder.PartsPurReturnOrderDetails.Sum(entity => entity.Quantity * entity.UnitPrice);

                var parent = queryWindow.ParentOfType<RadWindow>();
                if (parent != null)
                    parent.Close();
            }, null);
            
        }

        private void queryWindowPartsSupplier_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var partsSupplier = queryWindow.SelectedEntities.Cast<PartsSupplier>().FirstOrDefault();
            if(partsSupplier == null)
                return;

            var partsPurReturnOrder = this.DataContext as PartsPurReturnOrder;
            if(partsPurReturnOrder == null)
                return;

            partsPurReturnOrder.PartsSupplierId = partsSupplier.Id;
            partsPurReturnOrder.PartsSupplierCode = partsSupplier.Code;
            partsPurReturnOrder.PartsSupplierName = partsSupplier.Name;

            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }


        public ObservableCollection<KeyValuePair> KvWarehouses {
            get {
                return this.kvWarehouses ?? (this.kvWarehouses = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvPartsSalesCategoryName {
            get {
                return this.kvPartsSalesCategoryName ?? (this.kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>());
            }
        }

        public object PartsPurReturnOrderReturnReason {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }


    }
}
