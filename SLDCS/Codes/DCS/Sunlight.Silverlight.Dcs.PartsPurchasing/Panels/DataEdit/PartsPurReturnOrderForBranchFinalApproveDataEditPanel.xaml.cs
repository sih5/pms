﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.DataEdit {
    public partial class PartsPurReturnOrderForBranchFinalApproveDataEditPanel {
        private readonly string[] kvNames = {
            "PartsPurReturnOrder_ReturnReason"
        };
        //private ObservableCollection<KeyValuePair> kvIsPasseds;
        //public ObservableCollection<KeyValuePair> KvIsPasseds
        //{
        //    get
        //    {
        //        return this.kvIsPasseds ?? (this.kvIsPasseds = new ObservableCollection<KeyValuePair>());
        //    }
        //}
        private readonly DcsDomainContext domainContext = new DcsDomainContext();
        private ObservableCollection<KeyValuePair> kvWarehouses;
        public PartsPurReturnOrderForBranchFinalApproveDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += PartsPurReturnOrderForBranchFinalApproveDataEditPanel_DataContextChanged;

        }

        private void PartsPurReturnOrderForBranchFinalApproveDataEditPanel_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var partsPurReturnOrder = this.DataContext as PartsPurReturnOrder;
            if (partsPurReturnOrder == null)
                return;
            partsPurReturnOrder.PropertyChanged -= partsPurReturnOrder_PropertyChanged;
            partsPurReturnOrder.PropertyChanged += partsPurReturnOrder_PropertyChanged;
        }

        private void partsPurReturnOrder_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            var partsPurReturnOrder = this.DataContext as PartsPurReturnOrder;
            if(partsPurReturnOrder == null)
                return;

            switch(e.PropertyName) {

                case "WarehouseId":
                    var selectedWarehouse = this.KvWarehouses.First(v => v.Key == partsPurReturnOrder.WarehouseId).UserObject as Warehouse;
                    if(selectedWarehouse == null)
                        return;
                    partsPurReturnOrder.WarehouseName = selectedWarehouse.Name;
                    partsPurReturnOrder.WarehouseAddress = selectedWarehouse.Address;
                    break;
                case "ReturnReason":
                    partsPurReturnOrder.ValidationErrors.Clear();
                    break;
            }
        }

        private void CreateUI() {
            //设置RadioButton 选择项
        //    this.kvIsPasseds.Add(new KeyValuePair
        //    {
        //        Key = 66,
        //        Value = "审核通过"
        //    });
        //    this.kvIsPasseds.Add(new KeyValuePair
        //    {
        //        Key = 77,
        //        Value = "驳回"
        //    });
        }

        public ObservableCollection<KeyValuePair> KvWarehouses {
            get {
                return this.kvWarehouses ?? (this.kvWarehouses = new ObservableCollection<KeyValuePair>());
            }
        }

    }
}
