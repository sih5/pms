﻿
using System.Collections.ObjectModel;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.DataEdit {
    public partial class PartsPurchaseOrderDetailForApproveDataEditPanel {
        private readonly string[] kvNames = {
            "PartsShipping_Method"
        };

        private ObservableCollection<KeyValuePair> partsShippingMethods;
        public ObservableCollection<KeyValuePair> PartsShippingMethods {
            get {
                return this.partsShippingMethods ?? (this.partsShippingMethods = new ObservableCollection<KeyValuePair>());
            }
        }
        public PartsPurchaseOrderDetailForApproveDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData(() => {
                foreach(var keyValuePair in this.KeyValueManager[this.kvNames[0]]) {
                    this.PartsShippingMethods.Add(keyValuePair);
                }
            });
        }
    }
}