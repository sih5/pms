﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Windows;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.DataEdit {
    public partial class PartsPurchaseOuterAbandonCommentDataEditPanel {
        public PartsPurchaseOuterAbandonCommentDataEditPanel() {
            InitializeComponent();
        }
        private void Cancel_Click(object sender, RoutedEventArgs e) {
            var parent = this.ParentOfType<RadWindow>();
            if(parent != null) {
                parent.DialogResult = false;
                parent.Close();
            }
        }

        private void Submit_Click(object sender, RoutedEventArgs e) {
            var partsOuterPurchaseChange = this.DataContext as PartsOuterPurchaseChange;
            if(partsOuterPurchaseChange == null) {
                return;
            }
            partsOuterPurchaseChange.ValidationErrors.Clear();
            if(string.IsNullOrWhiteSpace(partsOuterPurchaseChange.AbandonComment))
                partsOuterPurchaseChange.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsOuterPurchaseChange_AbandonCommentNull, new[] {
                    "AbandonComment"
                }));
            if(partsOuterPurchaseChange.HasValidationErrors)
                return;
            ((IEditableObject)partsOuterPurchaseChange).EndEdit();
            var parent = this.ParentOfType<RadWindow>();
            if(parent != null) {
                parent.DialogResult = true;
                parent.Close();
            }
        }
    }
}
