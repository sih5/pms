﻿using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit {
    public partial class PartsPurchasePlanDataEditPanel {
        public PartsPurchasePlanDataEditPanel() {
            InitializeComponent();
            this.Loaded += PartsPurchasePlanDataEditPanel_Loaded;
            this.RadComboBoxPartsSalesCategory.SelectionChanged -= RadComboBoxPartsSalesCategory_SelectionChanged;
            this.RadComboBoxPartsSalesCategory.SelectionChanged += RadComboBoxPartsSalesCategory_SelectionChanged;

        }

        void RadComboBoxPartsSalesCategory_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if(e.AddedItems.Count > 0) {
                var partsSalesPriceChange = this.DataContext as PartsSalesPriceChange;
                if(partsSalesPriceChange == null)
                    return;
                var selectedItem = e.AddedItems.Cast<PartsSalesCategory>().First();
                partsSalesPriceChange.PartsSalesCategoryName = selectedItem.Name;
                partsSalesPriceChange.PartsSalesCategoryCode = selectedItem.Code;

                var domainContext = new DcsDomainContext();
                domainContext.Load(domainContext.GetPartsPurchasePricingByPartIdsQuery(partsSalesPriceChange.PartsSalesPriceChangeDetails.Select(r => r.SparePartId).ToArray()).Where(r=>r.PartsSalesCategoryId==selectedItem.Id), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    if(loadOp.Entities == null)
                        return;
                    foreach(var partsSalesPriceChangeDetail in partsSalesPriceChange.PartsSalesPriceChangeDetails) {
                        var partsPurchasePricing = loadOp.Entities.FirstOrDefault(r => r.PartId == partsSalesPriceChangeDetail.SparePartId);
                        if(partsPurchasePricing != null) {
                            partsSalesPriceChangeDetail.PurchasePrice = partsPurchasePricing.PurchasePrice;
                            partsSalesPriceChangeDetail.IsUpsideDown = partsSalesPriceChangeDetail.SalesPrice >= partsSalesPriceChangeDetail.PurchasePrice ? 0 : 1;
                        }
                    }
                }, null);
            }
        }

        void PartsPurchasePlanDataEditPanel_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(r => r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, LoadOperation => {
                if(LoadOperation.HasError)
                    return;
                this.RadComboBoxPartsSalesCategory.ItemsSource = LoadOperation.Entities;
            }, null);
        }

        private void DcsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void DcsComboBox_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
