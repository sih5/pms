﻿
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Windows;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.DataEdit {
    public partial class PartsPurchasePlanHWTerminateDataEditPanel {
        public PartsPurchasePlanHWTerminateDataEditPanel() {
            InitializeComponent();
            this.DataContextChanged += PartsPurchasePlanHWTerminateDataEditPanel_DataContextChanged;
        }

        void PartsPurchasePlanHWTerminateDataEditPanel_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsPurchasePlan_HW = this.DataContext as PartsPurchasePlan_HW;
            if (partsPurchasePlan_HW == null) return;
            partsPurchasePlan_HW.StopReason = string.Empty;
        }

        private void Submit_Click(object sender, RoutedEventArgs e) {
            var partsPurchasePlan_HW = this.DataContext as PartsPurchasePlan_HW;
            if (partsPurchasePlan_HW == null) return;
            partsPurchasePlan_HW.ValidationErrors.Clear();
            if (string.IsNullOrWhiteSpace(partsPurchasePlan_HW.StopReason))
                partsPurchasePlan_HW.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditPanel_PartsPurchase_validation1, new[]{
                    "StopReason"
                }));
            if (partsPurchasePlan_HW.HasValidationErrors)
                return;
            ((IEditableObject)partsPurchasePlan_HW).EndEdit();
            var parent = this.ParentOfType<RadWindow>();
            if (parent != null) {
                parent.DialogResult = true;
                parent.Close();
            }
        }

        private void Cancel_Click(object sender, RoutedEventArgs e) {
            var parent = this.ParentOfType<RadWindow>();
            if (parent != null) {
                parent.DialogResult = false;
                parent.Close();
            }
        }
    }
}
