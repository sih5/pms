﻿
namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.DataEdit {
    public partial class SupplierShippingOrderForBranchEditDataEditPanel  {
        private readonly string[] kvNames = {
            "PartsShipping_Method"
        };
        public object KvShippingMethods {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
        public SupplierShippingOrderForBranchEditDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
