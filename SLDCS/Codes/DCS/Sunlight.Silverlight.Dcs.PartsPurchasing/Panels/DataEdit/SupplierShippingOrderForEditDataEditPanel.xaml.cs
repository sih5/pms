﻿
namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.DataEdit {
    public partial class SupplierShippingOrderForEditDataEditPanel {
        private readonly string[] kvNames = {
            "PartsShipping_Method"
        };
        public SupplierShippingOrderForEditDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
