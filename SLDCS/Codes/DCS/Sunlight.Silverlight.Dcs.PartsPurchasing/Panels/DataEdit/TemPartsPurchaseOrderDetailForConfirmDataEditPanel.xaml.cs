﻿using System.Collections.ObjectModel;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.DataEdit {
    public partial class TemPartsPurchaseOrderDetailForConfirmDataEditPanel {
        private readonly string[] kvNames = {
            "TemPurchasePlanOrderShippingMethod","TemPurchasePlanOrderPlanType"
        };
        private ObservableCollection<KeyValuePair> partsShippingMethods;
        public ObservableCollection<KeyValuePair> PartsShippingMethods {
            get {
                return this.partsShippingMethods ?? (this.partsShippingMethods = new ObservableCollection<KeyValuePair>());
            }
        }
        private ObservableCollection<KeyValuePair> planTypes;
        public ObservableCollection<KeyValuePair> PlanTypes {
            get {
                return this.planTypes ?? (this.planTypes = new ObservableCollection<KeyValuePair>());
            }
        }
        public TemPartsPurchaseOrderDetailForConfirmDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData(() => {
                foreach(var keyValuePair in this.KeyValueManager[this.kvNames[0]]) {
                    this.PartsShippingMethods.Add(keyValuePair);
                }
                foreach(var keyValuePair in this.KeyValueManager[this.kvNames[1]]) {
                    this.PlanTypes.Add(keyValuePair);
                }
            });
        }
    }
}