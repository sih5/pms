﻿using System;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Detail {
    public class InternalAcquisitionBillDetailKanBanDetailPanel : InternalAcquisitionBillDetailKanBanDataGridView, IDetailPanel {
        public Uri Icon {
            get {
                return null;
            }
        }

        public string Title {
            get {
                return PartsPurchasingUIStrings.DetailPanel_Title_InternalAcquisitionBillDetailKanBan;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DomainDataSource.PageSize = 10;
            this.GridView.ShowGroupPanel = false;
            //GridView.Loaded += GridView_DataLoaded;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
        }

        protected override string OnRequestQueryName() {
            return "查询清单入库数量";
        }

        private void GridView_DataLoaded(object sender, EventArgs e) {
            var serialNumber = 1;
            foreach(var detail in this.GridView.Items.Cast<SparePartWithQuantity>())
                detail.SerialNumber = serialNumber++;
        }

        public InternalAcquisitionBillDetailKanBanDetailPanel() {
            this.DataContextChanged += this.InternalAcquisitionBillDetailKanBanDetailPanel_DataContextChanged;
        }

        private int internalAcquisitionBillId;
        private void InternalAcquisitionBillDetailKanBanDetailPanel_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var internalAcquisitionBill = e.NewValue as InternalAcquisitionBill;
            if(internalAcquisitionBill == null || internalAcquisitionBill.Id == default(int))
                return;
            internalAcquisitionBillId = internalAcquisitionBill.Id;
            this.ExecuteQueryDelayed();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            switch(parameterName) {
                case "id":
                    return internalAcquisitionBillId;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

    }
}
