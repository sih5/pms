﻿using System;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Detail {
    public class InternalAllocationDetailDetailPanel : InternalAllocationDetailDataGridView ,IDetailPanel{
        public Uri Icon {
            get {
                return null;
            }
        }

        public string Title {
            get {
                return Utils.GetEntityLocalizedName(typeof(InternalAllocationBill), "InternalAllocationDetails");
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
    }
}
