﻿
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Detail {
    public partial class InvoiceInformationDetailPanel {
        private readonly string[] kvNames = new[] {
            "InvoiceInformation_InvoicePurpose","InvoiceInformation_Type","InvoiceInformation_Status","InvoiceInformation_SourceType"
        };
        public InvoiceInformationDetailPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }

        public override string Title {
            get {
                return PartsPurchasingUIStrings.DetailPanel_Title_InvoiceInformation;
            }
        }
    }
}
