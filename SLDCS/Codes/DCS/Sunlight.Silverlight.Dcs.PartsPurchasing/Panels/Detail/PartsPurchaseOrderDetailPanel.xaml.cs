﻿
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Detail {
    public partial class PartsPurchaseOrderDetailPanel {
        private readonly string[] kvNames = {
            "PartsPurchaseOrder_OrderType", "PartsPurchaseOrder_Status", "PartsShipping_Method"
        };

        public PartsPurchaseOrderDetailPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }

        public override string Title {
            get {
                return PartsPurchasingUIStrings.DetailPanel_Title_Common;
            }
        }
    }
}
