﻿using System;
using System.Windows;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Detail {
    public class PartsPurchasePlanDetail_HWDetailPanel : PartsPurchasePlanDetail_HWDataGridView, IDetailPanel {
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DomainDataSource.PageSize = 10;
            this.GridView.ShowGroupPanel = false;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
        }

        new string Title {
            get {
                return PartsPurchasingUIStrings.DetailPanel_Title_PartsPurchasePlanDetail_HW;
            }
        }
    }
}
