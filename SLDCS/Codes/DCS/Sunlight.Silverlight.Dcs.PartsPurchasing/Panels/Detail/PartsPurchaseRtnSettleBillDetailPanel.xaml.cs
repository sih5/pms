﻿using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Detail {
    public partial class PartsPurchaseRtnSettleBillDetailPanel {
        public PartsPurchaseRtnSettleBillDetailPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }
        private readonly string[] kvNames = {
            "PartsPurchaseRtnSettleBill_SettlementPath","PartsPurchaseSettle_Status","PartsPurchaseRtnSettleBill_InvoicePath"
        };

        public override string Title {
            get {
                return PartsPurchasingUIStrings.DetailPanel_Title_Common;
            }
        }
    }
}
