﻿using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Detail {
    public class PartsPurchaseRtnSettleBillForInvoiceDetailPanel : PartsPurchaseRtnSettleBillForInvoiceDataGridView, IDetailPanel {
        public string Title {
            get {
                return PartsPurchasingUIStrings.DetailPanel_Title_PartsPurchaseRtnSettleBill;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        public Uri Icon {
            get {
                return null;
            }
        }
    }
}
