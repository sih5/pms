﻿using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Detail {
    public partial class PartsPurchaseSettleBillDetailPanel {
        public PartsPurchaseSettleBillDetailPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }
        private readonly string[] kvNames = {
            "PartsPurchaseSettleBill_SettlementPath","PartsPurchaseSettle_Status"
        };

        public override string Title {
            get {
                return PartsPurchasingUIStrings.DetailPanel_Title_Common;
            }
        }
    }
}
