﻿using System.Windows.Controls;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Detail {
    public partial class PartsPurchaseSettleBillForSupplierDetailPanel {
        public PartsPurchaseSettleBillForSupplierDetailPanel() {
            InitializeComponent();
        }

        public override string Title {
            get {
                return PartsPurchasingUIStrings.DetailPanel_Title_Common;
            }
        }
    }
}
