﻿using System;
using System.Windows;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Detail {
    public class PartsPurchaseSettleDetailDetailPanel : PartsPurchaseSettleDetailDataGridView, IDetailPanel {
        public Uri Icon {
            get {
                return null;
            }
        }

        public string Title {
            get {
                return PartsPurchasingUIStrings.DetailPanel_Title_PartsPurchaseSettleDetailForSupplier;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DomainDataSource.PageSize = 10;
            this.GridView.ShowGroupPanel = false;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
        }
    }
}
