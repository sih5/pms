﻿
using System;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Detail {
    public class PartsRequisitionSettleRefDetailPanel : PartsRequisitionSettleRefDetailDataGridView, IDetailPanel {
        public Uri Icon {
            get {
                return null;
            }
        }

        public string Title {
            get {
                return Utils.GetEntityLocalizedName(typeof(PartsRequisitionSettleRef), PartsPurchasingUIStrings.DetailPanel_Title_PartsRequisitionSettleRef);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
    }
}
