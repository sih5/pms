﻿
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Detail {
    public class SupplierPreAppLoanDetailDetailPanel : SupplierPreAppLoanDetailDataGridView, IDetailPanel {

        public System.Uri Icon {
            get {
                return null;
            }
        }

        public string Title {
            get {
                return PartsPurchasingUIStrings.DetailPanel_Title_SupplierPreAppLoanDetail;
            }
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
    }
}
