﻿using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Detail {
    public partial class SupplierShippingOrderDetailPanel {
        private readonly string[] kvNames = {
            "PartsShipping_Method", "SupplierShippingOrder_Status"
        };

        public SupplierShippingOrderDetailPanel() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }

        public override string Title {
            get {
                return PartsPurchasingUIStrings.DetailPanel_Title_Common;
            }
        }
    }
}
