﻿using System;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Detail {
    public class SupplierSupplierShippingOrderForPartsPurchaseOrderDetailPanel : SupplierSupplierShippingOrderForSupplierDataGridView, IDetailPanel {
        public Uri Icon {
            get {
                return null;
            }
        }

        public string Title {
            get {
                return PartsPurchasingUIStrings.DetailPanel_Title_SupplierShippingOrder;
            }
        }
    }
}
