﻿using System;
using System.Windows;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Detail {
    public class SupplierTraceCodeDetailDetailPanel : SupplierTraceCodeDetailDataGridView, IDetailPanel {

        public Uri Icon {
            get {
                return null;
            }
        }

        public string Title {
            get {
                return "供应商零件永久性标识条码修正清单";
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DomainDataSource.PageSize = 10;
            this.GridView.ShowGroupPanel = false;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
        }
    }
}
