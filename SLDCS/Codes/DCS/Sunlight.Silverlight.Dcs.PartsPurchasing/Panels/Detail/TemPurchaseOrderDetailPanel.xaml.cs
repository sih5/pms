﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Detail {
    public partial class TemPurchaseOrderDetailPanel{
        private readonly string[] kvNames = {
            "TemPurchasePlanOrderShippingMethod", "TemPurchasePlanOrderPlanType","TemPurchasePlanOrderCustomerType"
        };
        public TemPurchaseOrderDetailPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }
        public override string Title {
            get {
                return PartsPurchasingUIStrings.DetailPanel_Title_Common;
            }
        }
    }
}
