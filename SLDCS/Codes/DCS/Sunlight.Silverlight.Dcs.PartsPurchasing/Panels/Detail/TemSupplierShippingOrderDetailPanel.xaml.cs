﻿using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Detail {
    public partial class TemSupplierShippingOrderDetailPanel  {
        private readonly string[] kvNames = {
            "TemSupplierShippingOrderStatus", "TemPurchasePlanOrderShippingMethod"
        };

        public TemSupplierShippingOrderDetailPanel() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }

        public override string Title {
            get {
                return PartsPurchasingUIStrings.DetailPanel_Title_Common;
            }
        }
    }
}
