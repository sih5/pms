﻿
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class BranchSupplierRelationForFaultyPartsQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategories = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = { 
            "BaseData_Status"
        };

        public BranchSupplierRelationForFaultyPartsQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategor in loadOp.Entities)
                    this.kvPartsSalesCategories.Add(new KeyValuePair {
                        Key = partsSalesCategor.Id,
                        Value = partsSalesCategor.Name
                    });
            }, null);
            this.QueryItemGroups = new[] { 
                new QueryItemGroup{
                    UniqueId = "Common",
                    Title = PartsPurchasingUIStrings.QueryPanel_Title_BranchSupplierRelationForFaulty,
                    EntityType = typeof(BranchSupplierRelation),
                    QueryItems = new []{
                        new QueryItem {
                            ColumnName = "BusinessCode",
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_BranchSupplierRelation_PartsSupplierCode
                        }, new QueryItem{
                            ColumnName = "BusinessName",     
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_BranchSupplierRelation_PartsSupplierName
                        },new KeyValuesQueryItem{
                            ColumnName = "PartsSalesCategoryId",
                            KeyValueItems = this.kvPartsSalesCategories,
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name,
                            IsEnabled = false
                        }, new KeyValuesQueryItem{
                            ColumnName = "PartsSupplier.Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsBaseDataStatus.有效,
                            IsEnabled = false,
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_BranchSupplierRelation_Status,
                        }
                    }
                }
            };
        }
    }
}
