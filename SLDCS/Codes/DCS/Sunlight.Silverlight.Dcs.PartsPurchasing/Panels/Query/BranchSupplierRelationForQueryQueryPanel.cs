﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class BranchSupplierRelationForQueryQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = { 
            "BaseData_Status"
        };

        public BranchSupplierRelationForQueryQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] { 
                new QueryItemGroup{
                    UniqueId = "Common",
                    Title = PartsPurchasingUIStrings.QueryPanel_Title_BranchSupplierRelation,
                    EntityType = typeof(BranchSupplierRelation),
                    QueryItems = new QueryItem[]{
                        new CustomQueryItem{
                            Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsBranch_BranchName,
                            ColumnName = "Branch.Name",
                            IsEnabled = false,
                            DataType = typeof(string)
                        }, new KeyValuesQueryItem{
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsBaseDataStatus.有效,
                            IsEnabled = false
                        }, new CustomQueryItem{
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_InvoiceInformation_InvoiceReceiveCompanyCode,
                            ColumnName = "PartsSupplier.Code",
                            DataType = typeof(string)
                        }, new CustomQueryItem{
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_InvoiceInformation_InvoiceReceiveCompanyName,
                            ColumnName = "PartsSupplier.Name",
                            DataType = typeof(string)
                        }
                    }
                }
            };
        }
    }
}
