﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class DepartmentInformationQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = { 
            "BaseData_Status" 
        };

        private void Initialize() {
            this.QueryItemGroups = new[]{
                new QueryItemGroup{
                    UniqueId = "Common",
                    EntityType = typeof(DepartmentInformation),
                    Title = PartsPurchasingUIStrings.QueryPanel_Title_DepartmentInformation,
                    QueryItems = new[]{
                        new QueryItem{
                            ColumnName = "BranchName",
                        }, new QueryItem{
                            ColumnName = "Code",
                        }, new QueryItem{
                            ColumnName = "Name",
                        }, new KeyValuesQueryItem{
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new DateTimeRangeQueryItem{
                            ColumnName = "CreateTime",
                            DefaultValue = new[]{
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                            }
                        }
                    }
                }
            };
        }

        public DepartmentInformationQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
    }
}
