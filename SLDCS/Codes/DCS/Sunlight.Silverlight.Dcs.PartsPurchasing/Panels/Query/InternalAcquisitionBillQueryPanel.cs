﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class InternalAcquisitionBillQueryPanel : DcsQueryPanelBase {
        private ObservableCollection<KeyValuePair> kvWarehouses;
        private readonly string[] kvNames = {
            "WorkflowOfSimpleApproval_Status","PurchaseInStatus"
        };

        public ObservableCollection<KeyValuePair> KvWarehouses {
            get {
                return this.kvWarehouses ?? (this.kvWarehouses = new ObservableCollection<KeyValuePair>());
            }
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetWarehousesOrderByNameQuery().Where(w => w.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && w.Status == (int)DcsBaseDataStatus.有效 && (w.Type == (int)DcsWarehouseType.分库 || w.Type == (int)DcsWarehouseType.总库)), LoadBehavior.RefreshCurrent, loadOP => {
                if(loadOP.HasError) {
                    if(!loadOP.IsErrorHandled)
                        loadOP.MarkErrorAsHandled();
                    DcsUtils.ShowLoadError(loadOP);
                    return;
                }
                foreach(var entity in loadOP.Entities) {
                    this.KvWarehouses.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name
                    });
                }
                this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = PartsPurchasingUIStrings.QueryPanel_Title_InternalAcquisitionBill,
                        EntityType = typeof(InternalAcquisitionBill),
                        QueryItems = new[] {
                             new QueryItem {
                                ColumnName = "Code",
                            },new QueryItem {
                                ColumnName = "DepartmentName",
                                Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_DepartmentName
                            },new KeyValuesQueryItem {
                                ColumnName = "WarehouseId",
                                Title = PartsPurchasingUIStrings.DataEditPanel_Text_InternalAcquisitionBill_WarehouseName,
                                KeyValueItems = this.KvWarehouses
                            }, new KeyValuesQueryItem {
                                ColumnName = "Status",
                                DefaultValue = (int)DcsWorkflowOfSimpleApprovalStatus.已审核,
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                            }, new KeyValuesQueryItem {
                                ColumnName = "InStatus",
                                KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                            },new QueryItem {
                                ColumnName = "CreatorName"
                            }, new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                DefaultValue = new[] {
                                    DateTime.Now.Date, DateTime.Now.Date
                                }
                            }
                        }
                    }
                };
            }, null);


        }

        public InternalAcquisitionBillQueryPanel() {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
        }

    }
}
