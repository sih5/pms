﻿
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class InternalAllocationBillForQueryQueryPanel : DcsQueryPanelBase {
        private ObservableCollection<KeyValuePair> kvWarehouses;
        private readonly string[] kvNames = { 
            "WorkflowOfSimpleApproval_Status"
        };

        public InternalAllocationBillForQueryQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private ObservableCollection<KeyValuePair> KvWarehouses {
            get {
                return this.kvWarehouses ?? (this.kvWarehouses = new ObservableCollection<KeyValuePair>());
            }
        }
        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetWarehousesOrderByNameQuery().Where(w => w.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && w.Status == (int)DcsBaseDataStatus.有效 && (w.Type == (int)DcsWarehouseType.分库 || w.Type == (int)DcsWarehouseType.总库)), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;

                foreach(var entity in loadOp.Entities) {
                    this.KvWarehouses.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name,
                        UserObject = entity
                    });
                }
                this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        EntityType = typeof(InternalAllocationBill),
                        Title = PartsPurchasingUIStrings.QueryPanel_Title_InternalAllocationBill,
                        QueryItems = new[] {
                            new KeyValuesQueryItem{
                                ColumnName = "WarehouseId",
                                Title = PartsPurchasingUIStrings.QueryPanel_Title_BranchSupplierRelation_Warehouse,
                                KeyValueItems=this.KvWarehouses
                            }, new QueryItem{
                                ColumnName = "Code"
                            },new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime"
                            },new KeyValuesQueryItem{
                                ColumnName = "Status",
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                                DefaultValue = (int)DcsWorkflowOfSimpleApprovalStatus.已审核
                            }
                         }
                    }
                };
            }, null);
        }

    }
}
