﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class InternalAllocationBillQueryPanel : DcsQueryPanelBase {
        private ObservableCollection<KeyValuePair> kvWarehouses;
        private readonly string[] kvNames = new[] {
            "InternalAllocationBill_Status","InternalAllocationBill_Type"
        };

        public InternalAllocationBillQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private ObservableCollection<KeyValuePair> KvWarehouses {
            get {
                return this.kvWarehouses ?? (this.kvWarehouses = new ObservableCollection<KeyValuePair>());
            }
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetWarehousesOrderByNameQuery().Where(w => w.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && w.Status == (int)DcsBaseDataStatus.有效 && (w.Type == (int)DcsWarehouseType.分库 || w.Type == (int)DcsWarehouseType.总库)), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;

                foreach(var entity in loadOp.Entities) {
                    this.KvWarehouses.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name,
                        UserObject = entity
                    });
                }
                this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        EntityType = typeof(InternalAllocationBill),
                        Title = PartsPurchasingUIStrings.QueryPanel_Title_InternalAllocationBill,
                        QueryItems = new[] {
                            new KeyValuesQueryItem {
                                Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_InternalAllocationBill_WarehouseName,
                                ColumnName = "WarehouseId",
                                KeyValueItems = this.KvWarehouses
                            }, new QueryItem {
                                ColumnName = "Code"
                            }, new QueryItem {
                                ColumnName = "DepartmentName",
                                 Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_OutDepartmentName
                            }, new KeyValuesQueryItem {
                                ColumnName = "Status",
                                IsExact = false,
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                            },new QueryItem {
                                ColumnName = "CreatorName"
                            }, new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                DefaultValue = new[] {
                               new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now
                                }
                            }, new KeyValuesQueryItem {
                                ColumnName = "Type",
                                Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Type,
                                KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                            }
                        }
                    }
                };
            }, null);
        }
    }
}
