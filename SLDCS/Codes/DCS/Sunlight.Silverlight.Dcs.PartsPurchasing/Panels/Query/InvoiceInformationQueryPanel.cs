﻿
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class InvoiceInformationQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = new[] {
            "InvoiceInformation_InvoicePurpose","InvoiceInformation_Type","InvoiceInformation_Status"
        };

        public InvoiceInformationQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var branch in loadOp.Entities)
                    this.kvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name
                    });
            }, null);


            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(VirtualInvoiceInformation),
                    Title = PartsPurchasingUIStrings.QueryPanel_Title_InvoiceInformation,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "InvoiceNumber",
                            IsExact=false,
                            Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "InvoiceNumber")
                        },new KeyValuesQueryItem {
                            Title = Utils.GetEntityLocalizedName(typeof(Branch), "Name"),
                            ColumnName = "InvoiceReceiveCompanyId",
                            KeyValueItems = this.kvBranches,
                             IsExact=false
                        },new QueryItem {
                            ColumnName = "InvoiceCompanyCode",
                             IsExact=false,
                             Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_InvoiceInformation_InvoiceReceiveCompanyCode
                        },new QueryItem {
                            ColumnName = "InvoiceCompanyName",
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_InvoiceInformation_InvoiceReceiveCompanyName
                        },new KeyValuesQueryItem {
                          ColumnName = "InvoicePurpose",
                          KeyValueItems = this.KeyValueManager[kvNames[0]],
                            Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "InvoicePurpose")
                        },new KeyValuesQueryItem {
                           ColumnName = "Type",
                           KeyValueItems = this.KeyValueManager[kvNames[1]],
                            Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "Type")
                        },new QueryItem {
                            ColumnName = "SourceCode",
                            Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "SourceCode")
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[kvNames[2]],
                            DefaultValue=(int)DcsInvoiceInformationStatus.已开票,
                            Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "Status")
                        },new DateTimeRangeQueryItem {
                          ColumnName = "CreateTime",
                            DefaultValue =new []{
                                new DateTime(DateTime.Now.Year,DateTime.Now.Month,1), DateTime.Now },
                            Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "CreateTime")
                        },new DateTimeRangeQueryItem {
                          ColumnName = "ApproveTime",
                            DefaultValue =new []{
                                new DateTime(DateTime.Now.Year,DateTime.Now.Month,1), DateTime.Now },
                            Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "ApproveTime")
                        },new QueryItem {
                            ColumnName = PartsPurchasingUIStrings.QueryPanel_QueryItem_BussinessCode,
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_BussinessCode
                        }
                    }
                }
            };
        }

    }
}
