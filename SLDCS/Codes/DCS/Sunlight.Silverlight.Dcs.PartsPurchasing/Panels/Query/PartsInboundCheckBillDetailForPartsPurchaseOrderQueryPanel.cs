﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class PartsInboundCheckBillDetailForPartsPurchaseOrderQueryPanel : DcsQueryPanelBase {
        private ObservableCollection<KeyValuePair> kvPartsSalesCategorys;

        public ObservableCollection<KeyValuePair> KvPartsSalesCategorys {
            get {
                if(this.kvPartsSalesCategorys == null)
                    this.kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();
                return kvPartsSalesCategorys;
            }
        }
        public PartsInboundCheckBillDetailForPartsPurchaseOrderQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.kvPartsSalesCategorys.Clear();
                foreach(var partsSalesCategories in loadOp.Entities)
                    this.KvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = partsSalesCategories.Id,
                        Value = partsSalesCategories.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsPurchasingUIStrings.QueryPanel_Title_PartsInboundCheckBill,
                    EntityType = typeof(PartsPurchaseOrder),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "BranchName",
                            IsEnabled = false
                        }, new QueryItem {
                            ColumnName = "PartsSupplierName",
                            IsEnabled = false
                        },new KeyValuesQueryItem {
                            ColumnName = "PartsSalesCategoryId",
                            KeyValueItems = this.KvPartsSalesCategorys,
                            IsEnabled = false,
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_SupplierShippingOrder_PartsSalesCategoryName
                        }
                        , new CustomQueryItem {
                            ColumnName = "SparePartCode",
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_SparePartCode,
                            DataType = typeof(string)
                        }
                        , new CustomQueryItem {
                            ColumnName = "SparePartName",
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_SparePartName,
                            DataType = typeof(string)
                        }
                        , new QueryItem {
                            ColumnName = "WarehouseName"
                        }, new QueryItem {
                            ColumnName = "Code"
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                            },
                        }, new CustomQueryItem {
                            ColumnName = "IfDirectProvision",
                            DataType = typeof(bool),
                            DefaultValue = false
                        }
                    }
                }
            };
        }

    }
}