﻿
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;


namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class PartsOuterPurchaseBillQueryPanel : DcsQueryPanelBase {
        //private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategories = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvMarketDepartments = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
            "PartsOuterPurchaseChange_Status"
        };
        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            //dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    foreach(var partsSalesCategory in loadOp.Entities)
            //        this.kvPartsSalesCategories.Add(new KeyValuePair {
            //            Key = partsSalesCategory.Id,
            //            Value = partsSalesCategory.Name
            //        });
            //}, null);
            if(BaseApp.Current.CurrentUserData.EnterpriseCategoryId != (int)DcsCompanyType.代理库) {
                dcsDomainContext.Load(dcsDomainContext.GetMarketingDepartmentsQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError)
                        return;
                    foreach(var marketDepartment in loadOp.Entities)
                        this.kvMarketDepartments.Add(new KeyValuePair {
                            Key = marketDepartment.Id,
                            Value = marketDepartment.Name
                        });
                }, null);
            }
            if(BaseApp.Current.CurrentUserData.EnterpriseCategoryId != (int)DcsCompanyType.代理库) {
                this.QueryItemGroups = new[]{
                new QueryItemGroup{
                    UniqueId = "Common",
                    EntityType = typeof(PartsOuterPurchaseChange),
                    Title = PartsPurchasingUIStrings.QueryPanel_Title_PartsOuterPurchaseBill,
                   
                    QueryItems = new[]{
                        new QueryItem{
                            ColumnName = "CustomerCompanyNace"
                        },
                        new KeyValuesQueryItem
                         {
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_MarketingDepartment,
                             ColumnName = "MarketDepartmentId",
                            KeyValueItems = this.kvMarketDepartments
                         }
                        ,new QueryItem{
                            ColumnName = "Code",
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_Order_Code
                        },new KeyValuesQueryItem{
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsPartsOuterPurchaseChangeStatus.提交
                        }, new DateTimeRangeQueryItem{
                            ColumnName = "CreateTime",
                            DefaultValue = new[]{
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                            }
                        }
                    }
                }
            };

            } else {
                this.QueryItemGroups = new[]{
                new QueryItemGroup{
                    UniqueId = "Common",
                    EntityType = typeof(PartsOuterPurchaseChange),
                    Title = PartsPurchasingUIStrings.QueryPanel_Title_PartsOuterPurchaseBill,
                   
                    QueryItems = new[]{
                        new QueryItem{
                            ColumnName = "CustomerCompanyNace"
                        }
                        ,new QueryItem{
                            ColumnName = "Code",
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_Order_Code
                        },new KeyValuesQueryItem{
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsPartsOuterPurchaseChangeStatus.提交
                        }, new DateTimeRangeQueryItem{
                            ColumnName = "CreateTime",
                            DefaultValue = new[]{
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                            }
                        }
                    }
                }
            };
            }


        }
        public PartsOuterPurchaseBillQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
    }

}
