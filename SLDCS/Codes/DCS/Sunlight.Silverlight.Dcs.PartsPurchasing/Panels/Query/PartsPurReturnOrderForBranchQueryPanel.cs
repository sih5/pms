﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class PartsPurReturnOrderForBranchQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvWarehouse = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = {
            "PartsPurReturnOrder_Status", "PartsPurReturnOrder_ReturnReason","ReturnOutStatus"
        };

        public PartsPurReturnOrderForBranchQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetWarehousesOrderByNameQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效).OrderBy(r => r.Name), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehouse in loadOp.Entities)
                    this.kvWarehouse.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
                this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = PartsPurchasingUIStrings.QueryPanel_Title_PartsPurReturnOrder,
                        EntityType = typeof(PartsPurReturnOrder),
                        QueryItems = new[] {
                            new QueryItem {
                                ColumnName = "PartsSupplierCode"
                            }, new QueryItem {
                                ColumnName = "PartsSupplierName"
                            }, new QueryItem {
                                ColumnName = "Code",
                                Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurReturnOrder_Code
                            }, new QueryItem {
                                ColumnName = "PartsPurchaseOrderCode",
                                Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_TitlePartsPurReturnOrder_Code
                            }, new KeyValuesQueryItem {
                                Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurReturnOrder_WarehouseId,
                                ColumnName = "WarehouseId",
                                KeyValueItems = this.kvWarehouse,
                            }, new KeyValuesQueryItem {
                                ColumnName = "Status",
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                                DefaultValue = (int)DcsPartsPurReturnOrderStatus.新建
                            }, new KeyValuesQueryItem {
                                ColumnName = "ReturnReason",
                                KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                                DefaultValue = (int)DcsPartsPurReturnOrderReturnReason.质量件退货
                            }, new KeyValuesQueryItem {
                                ColumnName = "OutStatus",
                                KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                            }, new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                DefaultValue = new[] {
                                    new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now
                                }
                            }
                        }
                    }
                };
            }, null);
        }
    }
}
