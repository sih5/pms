﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class PartsPurReturnOrderQueryPanel : DcsQueryPanelBase {
        //private ObservableCollection<KeyValuePair> kvBranchs = new ObservableCollection<KeyValuePair>();
        private ObservableCollection<KeyValuePair> kvWarehouse = new ObservableCollection<KeyValuePair>();
        //private ObservableCollection<KeyValuePair> kvPartsSalesCategorys;

        //public ObservableCollection<KeyValuePair> KvPartsSalesCategorys {
        //    get {
        //        if(this.kvPartsSalesCategorys == null)
        //            this.kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();
        //        return kvPartsSalesCategorys;
        //    }
        //}

        private readonly string[] kvNames = {
            "PartsPurReturnOrder_ReturnReason", "WorkflowOfSimpleApproval_Status"
        };
        private readonly PartsPurReturnOrderQueryPanelViewModelViewModel viewModel = new PartsPurReturnOrderQueryPanelViewModelViewModel();
        public PartsPurReturnOrderQueryPanel() {
            this.Initializer.Register(this.InitialData);
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.viewModel.Initialize);
        }

        private void InitialData() {
            var dcsDomainContext = new DcsDomainContext();
            //dcsDomainContext.Load(dcsDomainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    foreach(var branchs in loadOp.Entities)
            //        this.kvBranchs.Add(new KeyValuePair {
            //            Key = branchs.Id,
            //            Value = branchs.Name
            //        });
            //}, null);
            dcsDomainContext.Load(dcsDomainContext.GetWarehousesOrderByNameQuery().Where(e => e.StorageCompanyType == (int)DcsCompanyType.分公司 && e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehouse in loadOp.Entities)
                    this.kvWarehouse.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
            }, null);
            //dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    this.kvPartsSalesCategorys.Clear();
            //    foreach(var partsSalesCategories in loadOp.Entities)
            //        this.KvPartsSalesCategorys.Add(new KeyValuePair {
            //            Key = partsSalesCategories.Id,
            //            Value = partsSalesCategories.Name
            //        });
            //}, null);

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(PartsPurReturnOrder),
                    Title = PartsPurchasingUIStrings.QueryPanel_Title_PartsPurReturnOrder,
                    QueryItems = new[] {
                        new ComboQueryItem {
                            ColumnName = "BranchId",
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_BranchName,
                            SelectedValuePath="Id",
                            DisplayMemberPath="Name",
                            ItemsSource=this.viewModel.Branchs,
                            SelectedItemBinding = new Binding("SelectedBranch") {
                                    Source = this.viewModel,
                                    Mode = BindingMode.TwoWay
                                }
                        }, new ComboQueryItem {
                            ColumnName = "PartsSalesCategoryId",
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurReturnOrder_PartsSalesCategoryName,
                             SelectedValuePath="Id",
                            DisplayMemberPath="Name",
                            ItemsSource=this.viewModel.PartsSalesCategories,
                               SelectedItemBinding = new Binding("SelectedPartsSalesCategory") {
                                    Source = this.viewModel,
                                    Mode = BindingMode.TwoWay
                                }
                        }, new KeyValuesQueryItem {
                            ColumnName = "WarehouseId",
                            KeyValueItems = this.kvWarehouse,
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurReturnOrder_WarehouseName
                        }, new QueryItem {
                            ColumnName = "Code",
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurReturnOrder_Code
                        }, new KeyValuesQueryItem {
                            ColumnName = "ReturnReason",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurReturnOrder_ReturnReason,
                            DefaultValue = (int)DcsPartsPurReturnOrderReturnReason.质量件退货
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            DefaultValue = (int)DcsWorkflowOfSimpleApprovalStatus.新建
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 00, 00, 00), new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59)
                            }
                          }
                    }
                }
            };
        }
    }
    public class PartsPurReturnOrderQueryPanelViewModelViewModel : ViewModelBase {
        private ObservableCollection<Branch> branchs;
        public ObservableCollection<Branch> Branchs {
            get {
                return this.branchs ?? (this.branchs = new ObservableCollection<Branch>());
            }
        }

        private ObservableCollection<PartsSalesCategory> allPartsSalesCategories;
        public ObservableCollection<PartsSalesCategory> AllPartsSalesCategories {
            get {
                return this.allPartsSalesCategories ?? (this.allPartsSalesCategories = new ObservableCollection<PartsSalesCategory>());
            }
        }

        //private ObservableCollection<PartsPurchaseOrderType> allPartsPurchaseOrderTypes;
        //public ObservableCollection<PartsPurchaseOrderType> AllPartsPurchaseOrderTypes {
        //    get {
        //        return this.allPartsPurchaseOrderTypes ?? (this.allPartsPurchaseOrderTypes = new ObservableCollection<PartsPurchaseOrderType>());
        //    }
        //}

        //根据所选择的分公司过滤品牌
        private Branch selectedBranch;
        public Branch SelectedBranch {
            get {
                return this.selectedBranch;
            }
            set {
                if(this.selectedBranch == value)
                    return;
                this.selectedBranch = value;
                this.NotifyOfPropertyChange("Branch");
                this.PartsSalesCategories.Refresh();
            }
        }
        private PagedCollectionView partsSalesCategories;
        public PagedCollectionView PartsSalesCategories {
            get {
                if(this.partsSalesCategories == null) {
                    this.partsSalesCategories = new PagedCollectionView(this.AllPartsSalesCategories);
                    this.partsSalesCategories.Filter = o => ((PartsSalesCategory)o).BranchId == (this.SelectedBranch != null ? this.SelectedBranch.Id : int.MinValue);
                }
                return this.partsSalesCategories;
            }
        }

        //根据所选择的品牌过滤采购订单类型
        //private PartsSalesCategory selectedPartsSalesCategory;
        //public PartsSalesCategory SelectedPartsSalesCategory {
        //    get {
        //        return this.selectedPartsSalesCategory;
        //    }
        //    set {
        //        if(this.selectedPartsSalesCategory == value)
        //            return;
        //        this.selectedPartsSalesCategory = value;
        //        this.NotifyOfPropertyChange("PartsSalesCategory");
        //        this.PartsPurchaseOrderTypes.Refresh();
        //    }
        //}
        //private PagedCollectionView partsPurchaseOrderTypes;
        //public PagedCollectionView PartsPurchaseOrderTypes {
        //    get {
        //        if(this.partsPurchaseOrderTypes == null) {
        //            this.partsPurchaseOrderTypes = new PagedCollectionView(this.AllPartsPurchaseOrderTypes);
        //            this.partsPurchaseOrderTypes.Filter = o => ((PartsPurchaseOrderType)o).PartsSalesCategoryId == (this.SelectedPartsSalesCategory != null ? this.SelectedPartsSalesCategory.Id : int.MinValue);
        //        }
        //        return this.partsPurchaseOrderTypes;
        //    }
        //}

        public override void Validate() {
            //
        }

        public void Initialize() {
            var domainContext = new DcsDomainContext();
            //查询所有的分公司
            domainContext.Load(domainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.branchs.Clear();
                foreach(var branch in loadOp.Entities)
                    this.branchs.Add(branch);
            }, null);
            //查询所有的品牌
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp1 => {
                if(loadOp1.HasError)
                    return;
                foreach(var entity in loadOp1.Entities) {
                    this.allPartsSalesCategories.Add(entity);
                }
            }, null);
        }
    }
}