﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class PartsPurchaseOrderDetailForSelectQueryPanel : DcsQueryPanelBase {
        public PartsPurchaseOrderDetailForSelectQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(PartsPurchaseOrderDetail),
                    Title = PartsPurchasingUIStrings.QueryPanel_Title_PartsPurchaseOrderDetail,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "SparePartCode"
                        }, new QueryItem {
                            ColumnName = "SparePartName"
                        }, new QueryItem {
                            ColumnName = "SupplierPartCode"
                        }
                    }
                }
            };
        }
    }
}
