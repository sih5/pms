﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class PartsPurchaseOrderForSelectForEditQueryPanel : DcsQueryPanelBase {
        public PartsPurchaseOrderForSelectForEditQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(PartsPurchaseOrder),
                    Title = PartsPurchasingUIStrings.QueryPanel_Title_PartsPurchaseOrder,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "BranchName",
                            IsEnabled=false
                        }, new QueryItem {
                            ColumnName = "Code"
                        }, new QueryItem {
                            ColumnName = "WarehouseName"
                        }, new QueryItem {
                            ColumnName = "IfDirectProvision",
                            DefaultValue = false,
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrder_IfDirectProvision
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date.AddDays(1)
                            }
                        }
                    }
                }
            };
        }
    }
}
