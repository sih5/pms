﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class PartsPurchaseOrderForShippingQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "PartsPurchaseOrder_Status", "PartsPurchaseOrder_OrderType","PurchaseInStatus"
        };
        private readonly ObservableCollection<KeyValuePair> kvWarehouse = new ObservableCollection<KeyValuePair>();
        private readonly PartsPurchaseOrderForShippingViewModel viewModel = new PartsPurchaseOrderForShippingViewModel();

        public PartsPurchaseOrderForShippingQueryPanel() {
            this.Initializer.Register(this.InitialData);
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.viewModel.Initialize);
        }

        private void InitialData() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetWarehousesOrderByNameQuery().Where(e => e.StorageCompanyType == (int)DcsCompanyType.分公司 && e.Status == (int)DcsBaseDataStatus.有效 && (e.Type == (int)DcsWarehouseType.分库 || e.Type == (int)DcsWarehouseType.总库)), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehouse in loadOp.Entities)
                    this.kvWarehouse.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name,
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(PartsPurchaseOrder),
                    Title = PartsPurchasingUIStrings.QueryPanel_Title_PartsPurchaseOrder,
                    QueryItems = new[] {
                      
                        new KeyValuesQueryItem {
                            ColumnName = "WarehouseId",
                            KeyValueItems = this.kvWarehouse,
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrder_WarehouseName
                        },
                        new QueryItem {
                            ColumnName = "Code"
                        },new ComboQueryItem {
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrder_PartsPurchaseOrderTypeId,
                            ColumnName = "PartsPurchaseOrderTypeId",
                            ItemsSource=this.viewModel.PartsPurchaseOrderTypes,
                            SelectedValuePath="Id",
                            DisplayMemberPath="Name"
                        },new KeyValuesQueryItem {
                            ColumnName = "InStatus",
                            KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsPartsPurchaseOrderStatus.确认完毕
                        }, new CustomQueryItem {
                            ColumnName = "IfDirectProvision",
                            DataType = typeof(bool),
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrder_IfDirectProvision
                        },
                        new QueryItem {
                            ColumnName = "OriginalRequirementBillCode",
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_OriginalRequirementBillCode
                        }
                        , new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date.AddDays(1)
                            }
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "RequestedDeliveryTime"
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "ApproveTime",
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_ApproveTime
                        }
                      
                    }
                }
            };
        }

    }
    public class PartsPurchaseOrderForShippingViewModel : ViewModelBase {
      

        private ObservableCollection<PartsPurchaseOrderType> allPartsPurchaseOrderTypes;
        public ObservableCollection<PartsPurchaseOrderType> AllPartsPurchaseOrderTypes {
            get {
                return this.allPartsPurchaseOrderTypes ?? (this.allPartsPurchaseOrderTypes = new ObservableCollection<PartsPurchaseOrderType>());
            }
        }

        
        private PagedCollectionView partsPurchaseOrderTypes;
        public PagedCollectionView PartsPurchaseOrderTypes {
            get {
                if(this.partsPurchaseOrderTypes == null) {
                    this.partsPurchaseOrderTypes = new PagedCollectionView(this.AllPartsPurchaseOrderTypes);
                }
                return this.partsPurchaseOrderTypes;
            }
        }

        public override void Validate() {
            //
        }

        public void Initialize() {
            var domainContext = new DcsDomainContext();
            
            domainContext.Load(domainContext.GetPartsPurchaseOrderTypesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp2 => {
                if(loadOp2.HasError)
                    return;
                foreach(var partsPurchaseOrderType in loadOp2.Entities) {
                    this.allPartsPurchaseOrderTypes.Add(partsPurchaseOrderType);
                }
            }, null);

        }
    }
}