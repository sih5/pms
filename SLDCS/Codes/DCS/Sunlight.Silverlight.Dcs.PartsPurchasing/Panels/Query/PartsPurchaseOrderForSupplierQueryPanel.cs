﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class PartsPurchaseOrderForSupplierQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "PartsPurchaseOrder_Status", "PartsPurchaseOrder_OrderType","PurchaseInStatus"
        };
        private readonly ObservableCollection<KeyValuePair> kvWarehouse = new ObservableCollection<KeyValuePair>();
        private readonly PartsPurchaseOrderForSupplierViewModel viewModel = new PartsPurchaseOrderForSupplierViewModel();

        public PartsPurchaseOrderForSupplierQueryPanel() {
            this.Initializer.Register(this.InitialData);
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.viewModel.Initialize);
        }

        private void InitialData() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetWarehousesOrderByNameQuery().Where(e => e.StorageCompanyType == (int)DcsCompanyType.分公司 && e.Status == (int)DcsBaseDataStatus.有效 && (e.Type == (int)DcsWarehouseType.分库 || e.Type == (int)DcsWarehouseType.总库)), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehouse in loadOp.Entities)
                    this.kvWarehouse.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name,
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(PartsPurchaseOrder),
                    Title = PartsPurchasingUIStrings.QueryPanel_Title_PartsPurchaseOrder,
                    QueryItems = new[] {
                       //new ComboQueryItem {
                       //     ColumnName = "BranchId",
                       //     Title = "分公司",
                       //     SelectedValuePath="Id",
                       //     DisplayMemberPath="Name",
                       //     ItemsSource=this.viewModel.Branchs,
                       //     SelectedItemBinding = new Binding("SelectedBranch") {
                       //             Source = this.viewModel,
                       //             Mode = BindingMode.TwoWay
                       //         }
                       // }, new ComboQueryItem {
                       //     ColumnName = "PartsSalesCategoryId",
                       //     Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurReturnOrder_PartsSalesCategoryName,
                       //      SelectedValuePath="Id",
                       //     DisplayMemberPath="Name",
                       //     ItemsSource=this.viewModel.PartsSalesCategories,
                       //        SelectedItemBinding = new Binding("SelectedPartsSalesCategory") {
                       //             Source = this.viewModel,
                       //             Mode = BindingMode.TwoWay
                       //         }
                       // },
                        new KeyValuesQueryItem {
                            ColumnName = "WarehouseId",
                            KeyValueItems = this.kvWarehouse,
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrder_WarehouseName
                        },
                        //new QueryItem {
                        //    ColumnName = "PartsSupplierCode"
                        //}, new QueryItem {
                        //    ColumnName = "PartsSupplierName"
                        //},
                        new QueryItem {
                            ColumnName = "Code"
                        },new ComboQueryItem {
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrder_PartsPurchaseOrderTypeId,
                            ColumnName = "PartsPurchaseOrderTypeId",
                            ItemsSource=this.viewModel.PartsPurchaseOrderTypes,
                            SelectedValuePath="Id",
                            DisplayMemberPath="Name"
                        },new KeyValuesQueryItem {
                            ColumnName = "InStatus",
                            KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsPartsPurchaseOrderStatus.提交
                        }, new CustomQueryItem {
                            ColumnName = "IfDirectProvision",
                            DataType = typeof(bool),
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrder_IfDirectProvision
                        },
                        new QueryItem {
                            ColumnName = "OriginalRequirementBillCode",
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_OriginalRequirementBillCode
                        }
                        , new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date.AddDays(1)
                            }
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "RequestedDeliveryTime"
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "ApproveTime",
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_ApproveTime
                        }
                        //,new QueryItem {
                        //   ColumnName = "CPPartsPurchaseOrderCode",
                        //   Title =  "原始采购订单编号"
                        //}, new QueryItem {
                        //   ColumnName = "CPPartsInboundCheckCode",
                        //   Title =  "原始入库单编号"
                        //}
                    }
                }
            };
        }

    }
    public class PartsPurchaseOrderForSupplierViewModel : ViewModelBase {
        //private ObservableCollection<Branch> branchs;
        //public ObservableCollection<Branch> Branchs {
        //    get {
        //        return this.branchs ?? (this.branchs = new ObservableCollection<Branch>());
        //    }
        //}

        //private ObservableCollection<PartsSalesCategory> allPartsSalesCategories;
        //public ObservableCollection<PartsSalesCategory> AllPartsSalesCategories {
        //    get {
        //        return this.allPartsSalesCategories ?? (this.allPartsSalesCategories = new ObservableCollection<PartsSalesCategory>());
        //    }
        //}

        private ObservableCollection<PartsPurchaseOrderType> allPartsPurchaseOrderTypes;
        public ObservableCollection<PartsPurchaseOrderType> AllPartsPurchaseOrderTypes {
            get {
                return this.allPartsPurchaseOrderTypes ?? (this.allPartsPurchaseOrderTypes = new ObservableCollection<PartsPurchaseOrderType>());
            }
        }

        ////根据所选择的分公司过滤品牌
        //private Branch selectedBranch;
        //public Branch SelectedBranch {
        //    get {
        //        return this.selectedBranch;
        //    }
        //    set {
        //        if(this.selectedBranch == value)
        //            return;
        //        this.selectedBranch = value;
        //        this.NotifyOfPropertyChange("Branch");
        //        this.PartsSalesCategories.Refresh();
        //    }
        //}
        //private PagedCollectionView partsSalesCategories;
        //public PagedCollectionView PartsSalesCategories {
        //    get {
        //        if(this.partsSalesCategories == null) {
        //            this.partsSalesCategories = new PagedCollectionView(this.AllPartsSalesCategories);
        //            this.partsSalesCategories.Filter = o => ((PartsSalesCategory)o).BranchId == (this.SelectedBranch != null ? this.SelectedBranch.Id : int.MinValue);
        //        }
        //        return this.partsSalesCategories;
        //    }
        //}

        ////根据所选择的品牌过滤采购订单类型
        //private PartsSalesCategory selectedPartsSalesCategory;
        //public PartsSalesCategory SelectedPartsSalesCategory {
        //    get {
        //        return this.selectedPartsSalesCategory;
        //    }
        //    set {
        //        if(this.selectedPartsSalesCategory == value)
        //            return;
        //        this.selectedPartsSalesCategory = value;
        //        this.NotifyOfPropertyChange("PartsSalesCategory");
        //        this.PartsPurchaseOrderTypes.Refresh();
        //    }
        //}
        private PagedCollectionView partsPurchaseOrderTypes;
        public PagedCollectionView PartsPurchaseOrderTypes {
            get {
                if(this.partsPurchaseOrderTypes == null) {
                    this.partsPurchaseOrderTypes = new PagedCollectionView(this.AllPartsPurchaseOrderTypes);
                    //this.partsPurchaseOrderTypes.Filter = o => ((PartsPurchaseOrderType)o).PartsSalesCategoryId == (this.SelectedPartsSalesCategory != null ? this.SelectedPartsSalesCategory.Id : int.MinValue);
                }
                return this.partsPurchaseOrderTypes;
            }
        }

        public override void Validate() {
            //
        }

        public void Initialize() {
            var domainContext = new DcsDomainContext();
            ////查询所有的分公司
            //domainContext.Load(domainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    this.branchs.Clear();
            //    foreach(var branch in loadOp.Entities)
            //        this.branchs.Add(branch);
            //}, null);
            ////查询所有的品牌
            //domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp1 => {
            //    if(loadOp1.HasError)
            //        return;
            //    foreach(var entity in loadOp1.Entities) {
            //        this.allPartsSalesCategories.Add(entity);
            //    }
            //}, null);
            //查询所有的采购订单类型
            domainContext.Load(domainContext.GetPartsPurchaseOrderTypesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp2 => {
                if(loadOp2.HasError)
                    return;
                foreach(var partsPurchaseOrderType in loadOp2.Entities) {
                    this.allPartsPurchaseOrderTypes.Add(partsPurchaseOrderType);
                }
            }, null);

        }
    }
}