﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class PartsPurchaseOrderQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "PartsPurchaseOrder_OrderType","PartsPurchaseOrder_Status","PurchaseInStatus"
        };
        private readonly PartsPurchaseOrderQueryPanelViewModel viewModel = new PartsPurchaseOrderQueryPanelViewModel();
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvIoStatus = new ObservableCollection<KeyValuePair>();

        public PartsPurchaseOrderQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
            this.Initializer.Register(this.viewModel.Initialize);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetWarehousesOrderByNameQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehouse in loadOp.Entities)
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name,
                    });
            }, null);
            this.kvIoStatus.Add(new KeyValuePair
            {
                Key = 2,
                Value = PartsPurchasingUIStrings.QueryPanel_QueryItem_IoStatus_Success
            });
            this.kvIoStatus.Add(new KeyValuePair
            {
                Key = 0,
                Value = PartsPurchasingUIStrings.QueryPanel_QueryItem_IoStatus_WaitTransfer
            });
            this.kvIoStatus.Add(new KeyValuePair
            {
                Key = 1,
                Value = PartsPurchasingUIStrings.QueryPanel_QueryItem_IoStatus_Fail
            });
            this.kvIoStatus.Add(new KeyValuePair {
                Key = 3,
                Value = PartsPurchasingUIStrings.QueryPanel_QueryItem_IoStatus_IsPassing
            });
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(VirtualPartsPurchaseOrder),
                    Title = PartsPurchasingUIStrings.QueryPanel_Title_PartsPurchaseOrder,
                    QueryItems = new[] {
                        //new ComboQueryItem {
                        //    ColumnName = "PartsSalesCategoryId",
                        // //   KeyValueItems = this.KvPartsSalesCategorys,
                        //    Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurReturnOrder_PartsSalesCategoryName,
                        //     SelectedValuePath="Id",
                        //    DisplayMemberPath="Name",
                        //    ItemsSource=this.viewModel.KvPartsSalesCategorys,
                        //       SelectedItemBinding = new Binding("SelectedPartsSalesCategory") {
                        //            Source = this.viewModel,
                        //            Mode = BindingMode.TwoWay
                        //        }
                        //},
                        new KeyValuesQueryItem {
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrder_WarehouseId,
                            ColumnName = "WarehouseId",
                            KeyValueItems = this.kvWarehouses
                        },
                        //new QueryItem {
                        //    ColumnName = "PartsSupplierCode",
                        //  Title =  Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "PartsSupplierCode"),
                        //}, 
                        new QueryItem {
                            ColumnName = "PartsSupplierName",
                            Title =  Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "PartsSupplierName"),
                        },new QueryItem {
                            ColumnName = "Code",
                            Title =  Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "Code"),
                        }, 
                        new QueryItem {
                            ColumnName = "OriginalRequirementBillCode",
                            Title =  Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "OriginalRequirementBillCode"),
                        }, new ComboQueryItem {
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrder_PartsPurchaseOrderTypeId,
                            ColumnName = "PartsPurchaseOrderTypeId",
                            ItemsSource=this.viewModel.PartsPurchaseOrderTypes,
                            SelectedValuePath="Id",
                            DisplayMemberPath="Name"
                        },new KeyValuesQueryItem {
                            ColumnName = "InStatus",
                            KeyValueItems = this.KeyValueManager[this.kvNames[2]],
                            Title =  Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "InStatus"),
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            DefaultValue = (int)DcsPartsPurchaseOrderStatus.新增,
                            Title =  Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "Status"),
                        },
                        //new QueryItem {
                        //    ColumnName = "ERPSourceOrderCode",
                        //    Title =  "平台单号"
                        //},
                        new QueryItem {
                            ColumnName = "CreatorName",
                            Title =  Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "CreatorName"),
                        },
                        //new QueryItem {
                        //    ColumnName = "HWPurOrderCode",
                        //    Title =  "采购计划单号"
                        //},
                        new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new [] {
                                DateTime.Now.Date,DateTime.Now.Date
                            },
                            Title =  Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "CreateTime"),
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "RequestedDeliveryTime",
                            Title =  Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "RequestedDeliveryTime"),
                        }, new CustomQueryItem {
                            Title =  PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrder_IfDirectProvision,
                            ColumnName = "IfDirectProvision",
                            DataType = typeof(bool)
                        }
                        , new KeyValuesQueryItem {
                            ColumnName = "IoStatus",
                            KeyValueItems = this.kvIoStatus,
                            Title =  PartsPurchasingUIStrings.QueryPanel_QueryItem_IoStatus,
                        },
                        new QueryItem {
                            ColumnName = "SparePartCode",
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_SparePartCode2
                        }
                        //, new CustomQueryItem {
                        //    ColumnName = "IfInnerDirectProvision",
                        //    Title =  Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "IfInnerDirectProvision"),
                        //    DataType = typeof(bool)
                        //},
                        //new QueryItem {
                        //    ColumnName = "SAPPurchasePlanCode",
                        //    Title =  "SAP采购计划单号"
                        //},
                        //new QueryItem {
                        //    ColumnName = "CPPartsPurchaseOrderCode",
                        //    Title =  "原始采购订单编号"
                        //}, new QueryItem {
                        //    ColumnName = "CPPartsInboundCheckCode",
                        //    Title =  "原始入库单编号"
                        //}
                    }
                }
            };
        }
    }

    public class PartsPurchaseOrderQueryPanelViewModel : ViewModelBase {

        public ObservableCollection<PartsSalesCategory> kvPartsSalesCategorys;
        private PartsSalesCategory partsSalesCategory;
        private PagedCollectionView partsPurchaseOrderTypes;

        public readonly ObservableCollection<PartsPurchaseOrderType> allPartsPurchaseOrderTypes = new ObservableCollection<PartsPurchaseOrderType>();
        public ObservableCollection<PartsSalesCategory> KvPartsSalesCategorys {
            get {
                if(this.kvPartsSalesCategorys == null)
                    this.kvPartsSalesCategorys = new ObservableCollection<PartsSalesCategory>();
                return kvPartsSalesCategorys;
            }
        }
        public PartsSalesCategory SelectedPartsSalesCategory {
            get {
                return this.partsSalesCategory;
            }
            set {
                if(this.partsSalesCategory == value)
                    return;
                this.partsSalesCategory = value;
                this.NotifyOfPropertyChange("PartsSalesCategory");
                this.PartsPurchaseOrderTypes.Refresh();
            }
        }
        public PagedCollectionView PartsPurchaseOrderTypes {
            get {
                if(this.partsPurchaseOrderTypes == null) {
                    this.partsPurchaseOrderTypes = new PagedCollectionView(this.allPartsPurchaseOrderTypes);
                    this.partsPurchaseOrderTypes.Filter = o => ((PartsPurchaseOrderType)o).PartsSalesCategoryId == 221;
                }
                return this.partsPurchaseOrderTypes;
            }
        }

        public override void Validate() {
            //
        }

        public void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsPurchaseOrderTypesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsPurchaseOrderTypes in loadOp.Entities)
                    this.allPartsPurchaseOrderTypes.Add(partsPurchaseOrderTypes);
            }, null);
            //domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(e => e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    this.kvPartsSalesCategorys.Clear();
            //    foreach(var partsSalesCategories in loadOp.Entities)
            //        this.KvPartsSalesCategorys.Add(partsSalesCategories);
            //}, null);

            
        }
    }

}
