﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class PartsPurchasePlanHWQueryPanel : DcsQueryPanelBase {

        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames ={
                                               "PurchasePlanType","PurchasePlanStatus"
                                          };

        public PartsPurchasePlanHWQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(Initialize);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if (loadOp.HasError)
                    return;
                foreach (var partsSalesCategory in loadOp.Entities)
                    this.kvPartsSalesCategoryName.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
            }, null);

            this.QueryItemGroups = new[]{
                new QueryItemGroup{
                    UniqueId="Common",
                    EntityType=typeof(PartsPurchasePlan_HW),
                    Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW,
                    QueryItems=new[]{
                        new QueryItem{
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_Code,
                            ColumnName="Code"
                        },new KeyValuesQueryItem{
                            ColumnName="PartsSalesCategoryId",
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_PartsSalesCategoryName,
                            KeyValueItems=this.kvPartsSalesCategoryName
                        }, new KeyValuesQueryItem {
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_PurchasePlanType,
                            ColumnName = "PurchasePlanType",
                            KeyValueItems=this.KeyValueManager[this.kvNames[0]]
                        },new KeyValuesQueryItem {
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_Status,
                            ColumnName = "Status",
                            KeyValueItems=this.KeyValueManager[this.kvNames[1]]
                        },new QueryItem{
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_SAPPurchasePlanCode,
                            ColumnName="SAPPurchasePlanCode"
                        },new DateTimeRangeQueryItem {
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_RequestedDeliveryTime,
                            ColumnName="RequestedDeliveryTime"
                        },new DateTimeRangeQueryItem{
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_CreateTime,
                            ColumnName="CreateTime",
                            DefaultValue=new[]{
                                DateTime.Now.Date.AddMonths(-1),DateTime.Now.Date
                            }
                        },new DateTimeRangeQueryItem {
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_ModifyTime,
                            ColumnName="ModifyTime"
                        },new DateTimeRangeQueryItem {
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_StopTime,
                            ColumnName="StopTime"
                        }
                    }
                }
            };

        }
    }
}