﻿
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Core.ViewModel;
using System.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class PartsPurchasePlanOrderQueryPanel : DcsQueryPanelBase {
        public PartsPurchasePlanOrderQueryPanel() {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private readonly string[] kvNames = {
           "PurchasePlanOrderStatus","PartsPurchaseOrder_OrderType"
        };
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategories = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvTypes = new ObservableCollection<KeyValuePair>();
        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsPurchaseOrderTypesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                    return;
                foreach (var type in loadOp.Entities)
                    this.kvTypes.Add(new KeyValuePair
                    {
                        Key = type.Id,
                        Value = type.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsPurchasingUIStrings.QueryPanel_Title_PartsPurchasePlan,
                    EntityType = typeof(PartsPurchasePlan),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_Code
                        },
                        new QueryItem {
                            ColumnName = "WarehouseName",
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_WarehouseName
                        },
                        new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_Status
                        },
                        new KeyValuesQueryItem {
                            ColumnName = "PartsPlanTypeId",
                            //KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            KeyValueItems = this.kvTypes,
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_Type
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_CreateTime
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CloseTime",
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_CloseTime
                        }, new CustomQueryItem {
                            ColumnName = "SparePartCode",
                            DataType = typeof(string),
                            Title= PartsPurchasingUIStrings.QueryPanel_QueryItem_SparePartCode
                        }, new QueryItem {
                            ColumnName = "CreatorName",
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_CreatorName
                        },
                    }
                }
            };
        }

    }
}
