﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class PartsPurchaseRtnSettleBillCostSearchQueryPanel : DcsQueryPanelBase {
        public PartsPurchaseRtnSettleBillCostSearchQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_SettleCost,
                    EntityType = typeof(VirtualPartsPurchaseRtnSettleBill),
                    QueryItems = new[] {
                        new QueryItem { 
                            ColumnName = "Code",
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_SettleCode
                        }
                    }
                }
            };
        }
    }
}
