﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class PartsPurchaseRtnSettleBillQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "PartsPurchaseSettleBill_SettlementPath","PartsPurchaseSettle_Status"
        };

        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();

        public PartsPurchaseRtnSettleBillQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
           
            domainContext.Load(domainContext.GetWarehousesOrderByNameQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehouse in loadOp.Entities)
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name,
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(VirtualPartsPurchaseRtnSettleBillWithBusinessCode),
                    Title =PartsPurchasingUIStrings.QueryPanel_Title_PartsPurchaseRtnSettleBill,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title = Utils.GetEntityLocalizedName(typeof(PartsPurchaseRtnSettleBill), "Code")
                        }, new KeyValuesQueryItem {
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseRtnSettleBill_WarehouseId,
                            ColumnName = "WarehouseId",
                            KeyValueItems = this.kvWarehouses
                        }, new QueryItem {
                            ColumnName = "PartsSupplierCode",
                            Title = Utils.GetEntityLocalizedName(typeof(PartsPurchaseRtnSettleBill), "PartsSupplierCode")
                        }, new QueryItem {
                            ColumnName = "PartsSupplierName",
                            Title = Utils.GetEntityLocalizedName(typeof(PartsPurchaseRtnSettleBill), "PartsSupplierName")
                        }, new KeyValuesQueryItem {
                            ColumnName = "SettlementPath",
                             KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                             DefaultValue = (int)DcsPartsPurchaseSettleBillSettlementPath.正常结算,
                            Title = Utils.GetEntityLocalizedName(typeof(PartsPurchaseRtnSettleBill), "SettlementPath")
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            DefaultValue = (int)DcsPartsPurchaseSettleStatus.新建,
                            Title = Utils.GetEntityLocalizedName(typeof(PartsPurchaseRtnSettleBill), "Status")
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new [] {
                                new DateTime(DateTime.Now.Year,DateTime.Now.Month,1),DateTime.Now.Date
                            },
                            Title = Utils.GetEntityLocalizedName(typeof(PartsPurchaseRtnSettleBill), "CreateTime")
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "InvoiceDate",                            
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseRtnSettleBill_InvoiceDate
                        }, new QueryItem {
                            ColumnName = "CreatorName",
                            Title = Utils.GetEntityLocalizedName(typeof(PartsPurchaseRtnSettleBill), "CreatorName")
                        }
                    }
                }
            };
        }
    }
}
