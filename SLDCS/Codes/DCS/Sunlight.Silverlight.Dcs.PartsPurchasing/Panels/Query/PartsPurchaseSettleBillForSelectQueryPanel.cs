﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class PartsPurchaseSettleBillForSelectQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "PartsPurchaseSettleBill_SettlementPath","PartsPurchaseSettle_Status"
        };
        public PartsPurchaseSettleBillForSelectQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(PartsPurchaseSettleBill),
                    Title =PartsPurchasingUIStrings.QueryPanel_Title_PartsPurchaseSettleBill,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseSettleBill_Code
                        }, 
                        //new KeyValuesQueryItem {
                        //    Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseSettleBill_WarehouseId,
                        //    ColumnName = "WarehouseId",
                        //    KeyValueItems = this.kvWarehouses
                        //}, 
                        new QueryItem {
                            ColumnName = "PartsSupplierCode"
                        }, new QueryItem {
                            ColumnName = "PartsSupplierName"
                        }, new KeyValuesQueryItem {
                            ColumnName = "SettlementPath",
                             KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                             DefaultValue = (int)DcsPartsPurchaseSettleBillSettlementPath.正常结算
                        }, 
                        //new KeyValuesQueryItem {
                        //    Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseSettleBill_PartsSalesCategoryName,
                        //    ColumnName = "PartsSalesCategoryId",
                        //    KeyValueItems = this.kvCategoryName
                        //},
                        new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            DefaultValue = (int)DcsPartsPurchaseSettleStatus.新建   
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new [] {
                            new DateTime(DateTime.Now.Year,DateTime.Now.Month,1),DateTime.Now.Date}
                        }
                    }
                }
            };
        }
    }
}
