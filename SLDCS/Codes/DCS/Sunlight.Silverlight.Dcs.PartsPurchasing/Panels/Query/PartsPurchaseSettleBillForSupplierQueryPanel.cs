﻿
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class PartsPurchaseSettleBillForSupplierQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "PartsPurchaseSettleBill_SettlementPath","PartsPurchaseSettle_Status"
        };

        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvCategoryNames = new ObservableCollection<KeyValuePair>();

        public PartsPurchaseSettleBillForSupplierQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetWarehousesOrderByNameQuery().Where(e => e.StorageCompanyType == (int)DcsCompanyType.分公司 && e.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehouse in loadOp.Entities)
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name,
                    });

            }, null);

            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {

                if(loadOp.HasError)
                    return;
                foreach(var categoryName in loadOp.Entities)
                    this.kvCategoryNames.Add(new KeyValuePair {
                        Key = categoryName.Id,
                        Value = categoryName.Name,
                    });
            }, null);


            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(PartsPurchaseSettleBill),
                    Title =PartsPurchasingUIStrings.QueryPanel_Title_PartsPurchaseSettleBill,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseSettleBill_Code
                        }, new KeyValuesQueryItem {
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseSettleBill_WarehouseId,
                            ColumnName = "WarehouseId",
                            KeyValueItems = this.kvWarehouses
                        }, new QueryItem {
                            ColumnName = "BranchName",
                              }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                           // DefaultValue = (int)DcsPartsPurchaseSettleStatus.新建
                       
                        },new KeyValuesQueryItem{
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseSettleBill_PartsSalesCategoryName,
                            ColumnName="PartsSalesCategoryId",
                            KeyValueItems=this.kvCategoryNames,

                         }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new [] {
                                new DateTime(DateTime.Now.Year,DateTime.Now.Month,1),DateTime.Now.Date
                            }
                        
                       }, new KeyValuesQueryItem {
                            ColumnName = "SettlementPath",
                             KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                             DefaultValue = (int)DcsPartsPurchaseSettleBillSettlementPath.正常结算,
                       }
                    }
                }
            };
        }
    }
}
