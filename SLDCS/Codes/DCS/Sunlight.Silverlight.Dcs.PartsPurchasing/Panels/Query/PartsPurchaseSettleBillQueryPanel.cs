﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class PartsPurchaseSettleBillQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "PartsPurchaseSettleBill_SettlementPath","PartsPurchaseSettle_Status"
        };

        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        public PartsPurchaseSettleBillQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetWarehousesOrderByNameQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehouse in loadOp.Entities)
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name,
                    });
            }, null);
          

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(VirtualPartsPurchaseSettleBillWithSumPlannedPrice),
                    Title =PartsPurchasingUIStrings.QueryPanel_Title_PartsPurchaseSettleBill,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseSettleBill_Code
                        }, new KeyValuesQueryItem {
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseSettleBill_WarehouseId,
                            ColumnName = "WarehouseId",
                            KeyValueItems = this.kvWarehouses
                        }, new QueryItem {
                            ColumnName = "PartsSupplierCode",
                              Title=Utils.GetEntityLocalizedName(typeof(PartsPurchaseSettleBill),"PartsSupplierCode")
                        }, new QueryItem {
                            ColumnName = "PartsSupplierName",
                              Title=Utils.GetEntityLocalizedName(typeof(PartsPurchaseSettleBill),"PartsSupplierName")
                        }, new KeyValuesQueryItem {
                            ColumnName = "SettlementPath",
                             KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                             DefaultValue = (int)DcsPartsPurchaseSettleBillSettlementPath.正常结算,
                              Title=Utils.GetEntityLocalizedName(typeof(PartsPurchaseSettleBill),"SettlementPath")
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            DefaultValue = (int)DcsPartsPurchaseSettleStatus.新建 ,
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Status
                        }, new QueryItem {
                            ColumnName = "CreatorName",
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_CreatorName
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                             Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_CreateTime,
                            DefaultValue = new [] {
                            new DateTime(DateTime.Now.Year,DateTime.Now.Month,1),DateTime.Now.Date}
                        },new DateTimeRangeQueryItem {
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseRtnSettleBill_InvoiceApproveTime,
                            ColumnName = "InvoiceApproveTime"
                           
                        },new DateTimeRangeQueryItem {
                            Title= PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseRtnSettleBill_InvoiceDate,
                            ColumnName = "InvoiceDate"
                        }
                    }
                }
            };
        }
    }
}
