﻿
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class PartsRequisitionSettleBillQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategories = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvDepartment = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = { 
            "PartsRequisitionSettleBill_Status" ,"InternalAllocationBill_Type"
        };

        public PartsRequisitionSettleBillQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategor in loadOp.Entities)
                    this.kvPartsSalesCategories.Add(new KeyValuePair {
                        Key = partsSalesCategor.Id,
                        Value = partsSalesCategor.Name
                    });
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetDepartmentInformationsQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var branch in loadOp.Entities)
                    this.kvDepartment.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name
                    });
            }, null);
            this.QueryItemGroups = new[]{
                new QueryItemGroup{
                    UniqueId = "Common",
                    EntityType = typeof(PartsRequisitionSettleBillExport),
                    Title = PartsPurchasingUIStrings.QueryPanel_Title_PartsRequisitionSettleBill,
                    QueryItems = new[]{
                        new KeyValuesQueryItem{
                            ColumnName = "DepartmentId",
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsRequisitionSettleBill_Department,
                            KeyValueItems = this.kvDepartment,
                        }, new KeyValuesQueryItem{
                            ColumnName = "Type",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Type                  
                        }, new DateTimeRangeQueryItem{
                            ColumnName = "CreateTime",
                            DefaultValue = new[]{new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date }
                        }, new DateTimeRangeQueryItem{
                            ColumnName = "InvoiceDate",
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseRtnSettleBill_InvoiceDate 
                        }, new QueryItem{
                            ColumnName = "Code",
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsRequisitionSettleBill_Code,
                        }, new KeyValuesQueryItem{
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue=(int)DcsPartsRequisitionSettleBillStatus.已审批
                        }
                        
                    }
                }
            };
        }
    }
}
