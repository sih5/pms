﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class PartsSupplierByPartIdsQueryPanel : DcsQueryPanelBase {

        public PartsSupplierByPartIdsQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = "供应商查询",
                    EntityType = typeof(PartsSupplier),
                    QueryItems = new QueryItem[]{
                        new CustomQueryItem {
                            ColumnName = "Code",
                             DataType = typeof(string),
                            Title = "供应商编号"
                        }, new CustomQueryItem {
                            ColumnName = "Name",
                            Title = "供应商名称",
                            DataType = typeof(string)
                        }
                    }
                }
            };
        }
    }
}