﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class PartsSupplierRelationForIntelligentOrderQueryPanel : DcsQueryPanelBase {

        public PartsSupplierRelationForIntelligentOrderQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] { 
                new QueryItemGroup{
                    UniqueId = "Common",
                    Title = PartsPurchasingUIStrings.QueryPanel_Title_PartsSupplierRelation,
                    EntityType = typeof(PartsSupplierRelation),
                    QueryItems = new QueryItem[]{
                        new CustomQueryItem{
                            Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsBranch_BranchName,
                            ColumnName = "PartsSalesCategory.Name",
                            IsEnabled = false,
                            DataType = typeof(string)
                        },  new CustomQueryItem{
                            Title = Utils.GetEntityLocalizedName(typeof(SparePart),"Code"),
                            ColumnName = "SparePart.Code",
                            IsEnabled = false,
                            DataType = typeof(string)
                        }, new QueryItem{
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_InvoiceInformation_InvoiceReceiveCompanyCode,
                            ColumnName = "SupplierPartCode",
                        }, new QueryItem{
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_InvoiceInformation_InvoiceReceiveCompanyName,
                            ColumnName = "SupplierPartName"
                        }
                    }
                }
            };
        }
    }
}
