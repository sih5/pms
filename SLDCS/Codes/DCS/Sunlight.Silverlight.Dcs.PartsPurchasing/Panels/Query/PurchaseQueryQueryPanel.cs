﻿using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class PurchaseQueryQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategory = new ObservableCollection<KeyValuePair>();

        public PurchaseQueryQueryPanel() {
            Initializer.Register(Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetBranchesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOpKey => {
                if(loadOpKey.HasError) {
                    if(!loadOpKey.IsErrorHandled)
                        loadOpKey.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOpKey);
                }
                this.kvBranches.Clear();
                foreach(var branch in loadOpKey.Entities)
                    this.kvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Code
                    });
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效 && v.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp2 => {
                if(loadOp2.HasError)
                    return;
                this.kvPartsSalesCategory.Clear();
                foreach(var warehouse in loadOp2.Entities) {
                    this.kvPartsSalesCategory.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
                }
            }, null);

            dcsDomainContext.Load(dcsDomainContext.GetCompaniesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var company = loadOp.Entities.SingleOrDefault();
                QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = PartsPurchasingUIStrings.QueryPanel_Title_PurchaseQuery,
                        EntityType = typeof(VirtualPurchaseQuery),
                        QueryItems = new QueryItem[] {
                             new KeyValuesQueryItem {
                                Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_BranchName,
                                ColumnName = "BranchId",
                                KeyValueItems = kvBranches,
                                DefaultValue = company != null && company.Type == (int)DcsCompanyType.分公司 ? BaseApp.Current.CurrentUserData.EnterpriseId : 0,
                                IsEnabled = BaseApp.Current.CurrentUserData.UserCode == "ReportAdmin"
                            },new KeyValuesQueryItem {
                                Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseSettleBill_PartsSalesCategoryName,
                                ColumnName = "PartsSalesCategoryId",
                                KeyValueItems = this.kvPartsSalesCategory
                            },                     
                            new QueryItem {
                                Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_SparePartCode2,
                                ColumnName = "PartCode",
                            },
                            new QueryItem {
                                Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_SparePartName,
                                ColumnName = "PartName"
                            },new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_CreateTime
                          }
                        }
                    }
                };
            }, null);
        }
    }
}
