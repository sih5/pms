﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class SettlementCostQueryQueryPanel : DcsQueryPanelBase {
        public SettlementCostQueryQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_SettleCost,
                    EntityType = typeof(VirtualPartsPurchaseSettleBill),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title =PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_SettleCode
                        }
                    }
                }
            };
        }
    }
}
