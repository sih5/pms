﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class SparePartForPartsPurchasePlanQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        public SparePartForPartsPurchasePlanQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(VirtualSparePart),
                    Title = PartsPurchasingUIStrings.QueryPanel_Title_SparePartByPartsPurchaseOrder,
                    QueryItems = new[] {
                        new CustomQueryItem {
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsBranch_BranchName,
                            ColumnName = "BranchName",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsBranch_PartsSalesCategoryName,
                            ColumnName = "PartsSalesCategoryName",
                            DataType = typeof(string)
                        }, new QueryItem {
                            ColumnName = "Code",
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_SparePartCode2
                        }, new QueryItem {
                            ColumnName = "Name",
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_SparePartName
                        }, new QueryItem {
                            ColumnName = "PartsSupplierCode",
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_SupplierCode
                        }, new QueryItem {
                            ColumnName = "PartsSupplierName",
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_SupplierName
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Status,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsBaseDataStatus.有效
                        }, new QueryItem {
                            ColumnName = "ReferenceCode",
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_ReferenceCode
                        }
                    }
                }
            };
        }
    }
}
