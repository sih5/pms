﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class SupplierPartsTagPrinttingQueryPanel : DcsQueryPanelBase {

        private readonly string[] kvNames = {
            "MasterData_Status","SparePart_MeasureUnit"
        };

        public SupplierPartsTagPrinttingQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(SparePart),
                    Title = PartsPurchasingUIStrings.QueryPanel_Title_SupplierPartsTagPrintting,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_SparePartCode2
                        }, new QueryItem {
                            ColumnName = "Name"
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsMasterDataStatus.有效
                        },new KeyValuesQueryItem{
                            ColumnName="MeasureUnit",
                            KeyValueItems=this.KeyValueManager[this.kvNames[1]]
                        },new QueryItem{
                            ColumnName="Specification"
                        },new QueryItem{
                            ColumnName="ReferenceCode",
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_ReferenceCode
                        }
                    }
                }
            };
        }
    }
}
