﻿
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class SupplierPlanArrearQueryPanel : DcsQueryPanelBase {
        public SupplierPlanArrearQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private ObservableCollection<KeyValuePair> kvPartsSalesCategorie;

        private ObservableCollection<KeyValuePair> KvPartsSalesCategorie {
            get {
                return this.kvPartsSalesCategorie ?? (this.kvPartsSalesCategorie = new ObservableCollection<KeyValuePair>());
            }
        }

        private DcsDomainContext domainContext;

        private DcsDomainContext DomaomContext {
            get {
                return this.domainContext ?? (this.domainContext = new DcsDomainContext());
            }
        }

        private void Initialize() {
            this.DomaomContext.Load(this.DomaomContext.GetPartsSalesCategoriesQuery().Where(r => r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Entities != null && loadOp.Entities.Any())
                    foreach(var entity in loadOp.Entities) {
                        this.kvPartsSalesCategorie.Add(new KeyValuePair {
                            Key = entity.Id,
                            Value = entity.Name,
                            UserObject = entity
                        });
                    }
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(SupplierPlanArrear),
                    Title = PartsPurchasingUIStrings.QueryPanel_Title_SupplierPlanArrear,
                    QueryItems = new [] {
                        new KeyValuesQueryItem {
                            ColumnName = "PartsSalesCategoryId",
                            Title =PartsPurchasingUIStrings.QueryPanel_QueryItem_SupplierPlanArrear_PartsSalesCategory,
                            KeyValueItems =this.KvPartsSalesCategorie
                        },new QueryItem {
                            ColumnName = "SupplierCode"
                        },new QueryItem {
                            ColumnName = "SupplierCompanyCode"
                        },new QueryItem {
                            ColumnName = "SupplierCompanyName"
                        }
                    }
                }
            };
        }
    }
}
