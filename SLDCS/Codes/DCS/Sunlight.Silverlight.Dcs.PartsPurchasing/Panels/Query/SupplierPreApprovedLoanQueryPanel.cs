﻿

using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class SupplierPreApprovedLoanQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();
        public SupplierPreApprovedLoanQueryPanel() {
            this.Initializer.Register(this.Initializ);
        }

        private void Initializ() {
            var domcontext = new DcsDomainContext();
            domcontext.Load(domcontext.GetPartsSalesCategoriesQuery().Where(r => r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.kvPartsSalesCategorys.Clear();
                if(loadOp.Entities == null || !loadOp.Entities.Any())
                    return;
                foreach(var entity in loadOp.Entities) {
                    kvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name
                    });
                }

                this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    EntityType = typeof(SupplierPreApprovedLoan),
                    Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_SupplierPreApprovedLoan,
                    UniqueId = "Common",
                    QueryItems = new[] {
                        new KeyValuesQueryItem {
                            ColumnName="PartsSalesCategoryId",
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_SupplierPreApprovedLoan_PartsSalesCategory,
                            KeyValueItems=this.kvPartsSalesCategorys
                        },new QueryItem {
                          ColumnName = "PreApprovedCode"
                      },new DateTimeRangeQueryItem {
                          ColumnName = "CreateTime"
                      }
                    }
                }
            };
            }, null);

        }
    }
}
