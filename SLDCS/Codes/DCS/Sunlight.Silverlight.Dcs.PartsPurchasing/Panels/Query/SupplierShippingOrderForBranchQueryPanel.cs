﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Web.Entities;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class SupplierShippingOrderForBranchQueryPanel : DcsQueryPanelBase {
        private ObservableCollection<KeyValuePair> kvWarehouses;

        private readonly string[] kvNames = {
            "SupplierShippingOrder_Status","IsOrNot"
        };

        public SupplierShippingOrderForBranchQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        public ObservableCollection<KeyValuePair> KvWarehouses {
            get {
                return this.kvWarehouses ?? (kvWarehouses = new ObservableCollection<KeyValuePair>());
            }
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            //domainContext.Load(domainContext.GetWarehousesQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效 && (e.Type == (int)DcsWarehouseType.总库 || e.Type == (int)DcsWarehouseType.分库)).OrderBy(t => t.Name), LoadBehavior.RefreshCurrent, loadOp => {
            dcsDomainContext.Load(dcsDomainContext.GetWarehousesbyPersonnelIdQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效).OrderBy(t => t.Name), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var item in loadOp.Entities) {
                    this.KvWarehouses.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.Name
                    });
                }
            }, null);         

            this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        EntityType = typeof(VirtualSupplierShippingOrder),
                        UniqueId = "Common",
                        Title = PartsPurchasingUIStrings.QueryPanel_Title_SupplierShippingOrderForBranch,
                        QueryItems = new[] {
                            new CustomQueryItem {
                                ColumnName = "PartsSupplierCode",
                                Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_BranchSupplierRelation_PartsSupplierCode,
                                 DataType = typeof(string)
                            }
                            , new CustomQueryItem {
                                ColumnName = "PartsSupplierName",
                                 Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_BranchSupplierRelation_PartsSupplierName,
                                 DataType = typeof(string)
                            }
                            ,new KeyValuesQueryItem {
                                ColumnName = "Status",
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                                DefaultValue = (int)DcsSupplierShippingOrderStatus.收货确认,
                                Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Status
                            },new KeyValuesQueryItem {
                                ColumnName = "ReceivingWarehouseId",
                                KeyValueItems = this.KvWarehouses,
                                Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_SupplierShippingOrder_ReceivingWarehouseId
                            },new QueryItem {
                                ColumnName = "Code",
                                Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsShippingOrder_Code
                            },new QueryItem {
                                ColumnName = "ERPSourceOrderCode",
                                Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsShippingOrder_ERPSourceOrderCode
                            },new QueryItem {
                                ColumnName = "PartsPurchaseOrderCode"  ,
                                Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsShippingOrder_PartsPurchaseOrderCode
                            },  new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                DefaultValue = new [] {
                                    new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                                },
                                Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_CreateTime
                            },new QueryItem {
                                ColumnName = "IfDirectProvision",
                                DefaultValue=true,
                                Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_IfDirectProvision
                            },new QueryItem {
                                ColumnName = "DirectProvisionFinished",
                                Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_DirectProvisionFinished
                            }
                        }
                    }
                };
        }
    }
}
