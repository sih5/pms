﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class SupplierShippingOrderForComfirmQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "SupplierShippingOrderArriveMethod","SupplierShippingOrderComfirmStatus","SupplierShippingOrderModifyStatus"
        };

        public SupplierShippingOrderForComfirmQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
                this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        EntityType = typeof(SupplierShippingOrder),
                        UniqueId = "Common",
                        Title = "供应商到货确认查询",
                        QueryItems = new[] {
                            new QueryItem {
                                ColumnName = "PartsSupplierCode"
                            }, new QueryItem {
                                ColumnName = "PartsSupplierName"
                            }, new QueryItem {
                                ColumnName = "Code"
                            }, new CustomQueryItem {
                                ColumnName = "PartsPurchaseOrderCode",
                                DataType = typeof(string),
                                Title = "采购订单"
                            }, new CustomQueryItem {
                                ColumnName = "IfDirectProvision",
                                DataType = typeof(Boolean),
                                Title = "是否直供"
                            }, new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                DefaultValue = new[] {
                                    new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                                }
                            }, new KeyValuesQueryItem {
                                ColumnName = "ArriveMethod",
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                                Title="到货确认方式"
                            },new KeyValuesQueryItem {
                                ColumnName = "ComfirmStatus",
                                KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                                Title="确认状态"
                            },new KeyValuesQueryItem {
                                ColumnName = "ModifyStatus",
                                KeyValueItems = this.KeyValueManager[this.kvNames[2]],
                                Title="修改状态"
                            }
                        }
                    }
                };
        }
    }
}
