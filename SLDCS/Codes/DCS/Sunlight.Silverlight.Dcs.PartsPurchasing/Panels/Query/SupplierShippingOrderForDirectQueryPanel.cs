﻿
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class SupplierShippingOrderForDirectQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
            "SupplierShippingOrder_Status"
        };

        public SupplierShippingOrderForDirectQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 ), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategory in loadOp.Entities)
                    this.kvPartsSalesCategoryName.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var branch in loadOp.Entities)
                    this.kvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name
                    });
                this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        EntityType = typeof(SupplierShippingOrder),
                        UniqueId = "Common",
                        Title = PartsPurchasingUIStrings.QueryPanel_Title_SupplierShippingOrderForDirect,
                        QueryItems = new[] {
                            new QueryItem {
                                ColumnName = "PartsSupplierCode"
                            }, new QueryItem {
                                ColumnName = "PartsSupplierName"
                            }, new KeyValuesQueryItem {
                                ColumnName = "BranchId",
                                KeyValueItems = this.kvBranches,
                                Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_SupplierShippingOrder_BranchId
                            }, new QueryItem {
                                ColumnName = "Code"
                            },new KeyValuesQueryItem{
                                ColumnName="PartsSalesCategoryId",
                                KeyValueItems=this.kvPartsSalesCategoryName,
                                Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name
                            }, new CustomQueryItem {
                                ColumnName = "OriginalRequirementBillCode",
                                DataType = typeof(string),
                                Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrder_Code
                            }, new KeyValuesQueryItem {
                                ColumnName = "Status",
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                                DefaultValue = (int)DcsSupplierShippingOrderStatus.新建
                            }, new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                DefaultValue = new[] {
                                    new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                                }
                            }
                        }
                    }
                };
            }, null);
        }
    }
}
