﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class SupplierShippingOrderQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "SupplierShippingOrder_Status"
        };
        //private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvWarehouse = new ObservableCollection<KeyValuePair>();
        //private ObservableCollection<KeyValuePair> kvPartsSalesCategorys;

        //public ObservableCollection<KeyValuePair> KvPartsSalesCategorys {
        //    get {
        //        if(this.kvPartsSalesCategorys == null)
        //            this.kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();
        //        return kvPartsSalesCategorys;
        //    }
        //}

        public SupplierShippingOrderQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            //dcsDomainContext.Load(dcsDomainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    foreach(var branch in loadOp.Entities)
            //        this.kvBranches.Add(new KeyValuePair {
            //            Key = branch.Id,
            //            Value = branch.Name
            //        });
            //}, null);
            dcsDomainContext.Load(dcsDomainContext.GetWarehousesOrderByNameQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehouse in loadOp.Entities)
                    this.kvWarehouse.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
            }, null);
            //dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => /*e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId &&*/ e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    this.kvPartsSalesCategorys.Clear();
            //    foreach(var partsSalesCategories in loadOp.Entities)
            //        this.KvPartsSalesCategorys.Add(new KeyValuePair {
            //            Key = partsSalesCategories.Id,
            //            Value = partsSalesCategories.Name
            //        });
            //}, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsPurchasingUIStrings.QueryPanel_Title_SupplierShippingOrder,
                    EntityType = typeof(SupplierShippingOrder),
                    QueryItems = new[] {
                        //new KeyValuesQueryItem {
                        //    Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_Branch_Name,
                        //    ColumnName = "BranchId",
                        //    DefaultValue = BaseApp.Current.CurrentUserData.EnterpriseId,
                        //    KeyValueItems = this.kvBranches,
                        //},
                        new QueryItem {
                            ColumnName = "Code"
                        },
                        //new KeyValuesQueryItem {
                        //    ColumnName = "PartsSalesCategoryId",
                        //    KeyValueItems = this.KvPartsSalesCategorys,
                        //    Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_SupplierShippingOrder_PartsSalesCategoryName
                        //},
                        new KeyValuesQueryItem {
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_SupplierShippingOrder_ReceivingWarehouseId,
                            ColumnName = "ReceivingWarehouseId",
                            KeyValueItems = this.kvWarehouse,
                        }, new QueryItem {
                            ColumnName = "PartsPurchaseOrderCode"
                        },
                        //new QueryItem {
                        //    ColumnName = "ERPSourceOrderCode",
                        //    Title = "平台单号"
                        //},
                        new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsSupplierShippingOrderStatus.新建
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue =new []{
                                new DateTime(DateTime.Now.Year,DateTime.Now.Month,1), DateTime.Now }
                        }
                    }
                }
            };
        }
    }
}
