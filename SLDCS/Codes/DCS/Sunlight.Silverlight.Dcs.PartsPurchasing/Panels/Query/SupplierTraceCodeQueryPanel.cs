﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class SupplierTraceCodeQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "SupplierTraceCodeStatus"
        };
        public SupplierTraceCodeQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
          }
        private void Initialize() {

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(SupplierTraceCode),
                    Title = "供应商零件永久性标识条码修正查询",
                     QueryItems = new QueryItem[]{
                       new CustomQueryItem {
                            ColumnName = "Code",
                            Title =  "修正单号",
                            DataType = typeof(string)
                        },new KeyValuesQueryItem {
                            Title="状态",
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        }, new CustomQueryItem {
                            ColumnName = "PartsSupplierCode",
                            DataType = typeof(string),
                            Title = "供应商编号"
                        },new CustomQueryItem {
                            ColumnName = "PartsSupplierName",
                             DataType = typeof(string),
                            Title ="供应商名称"
                        },new CustomQueryItem {
                            ColumnName = "SparePartCode",
                             DataType = typeof(string),
                            Title =  "配件编号"
                        }, new CustomQueryItem {
                            Title="采购订单编号",
                            ColumnName = "PartsPurchaseOrderCode",
                             DataType = typeof(string),
                        },new CustomQueryItem {
                            ColumnName = "OldTraceCode",
                             DataType = typeof(string),
                            Title =  "原追溯码"
                        }, new CustomQueryItem {
                            ColumnName = "NewTraceCode",  
                             DataType = typeof(string),
                            Title =  "新追溯码"
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new [] {
                                DateTime.Now.Date.AddMonths(-1),DateTime.Now.Date
                            },
                            Title =  Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "CreateTime"),
                        }, new CustomQueryItem {
                            ColumnName = "ShippingCode",
                             DataType = typeof(string),
                            Title =  "发运单号"
                        }
                       
                    }
                }
            };
        }
    }
}
