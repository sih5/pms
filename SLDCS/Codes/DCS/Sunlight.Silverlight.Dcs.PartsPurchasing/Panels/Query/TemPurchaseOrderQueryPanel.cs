﻿
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Core.ViewModel;
using System.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class TemPurchaseOrderQueryPanel : DcsQueryPanelBase {
        public TemPurchaseOrderQueryPanel() {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private readonly string[] kvNames = {
           "TemPurchasePlanOrderCustomerType","TemPurchaseOrderStatus","TemPurchaseOrderApproveStatus","TemPurchaseOrderReceiveStatus"
        };
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsPurchasingUIStrings.QueryPanel_Title_PartsPurchaseOrde,
                    EntityType = typeof(TemPurchaseOrder),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title=PartsPurchasingUIStrings.DataGridView_TemPurchaseOrder_ColumnItem_Code
                        }, new QueryItem {
                            ColumnName = "TemPurchasePlanOrderCode",
                            Title=PartsPurchasingUIStrings.DataGridView_TemPurchaseOrder_TemPlanCode
                        }, new QueryItem {
                            ColumnName = "ReceCompanyCode",
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_emPurchasePlanOrder_ReceCompanyCode
                        }, new QueryItem {
                            ColumnName = "ReceCompanyName",
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_emPurchasePlanOrder_ReceCompanyName
                        }, new CustomQueryItem {
                            ColumnName = "SuplierName",
                            DataType = typeof(string),
                            Title= PartsPurchasingUIStrings.QueryPanel_QueryItem_SupplierName
                        }, new CustomQueryItem {
                            ColumnName = "SparePartCode",
                            DataType = typeof(string),
                            Title= PartsPurchasingUIStrings.QueryPanel_QueryItem_SparePartCode
                        }, new KeyValuesQueryItem {
                            ColumnName = "CustomerType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_CustomerType
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_Status
                        }, new KeyValuesQueryItem {
                            ColumnName = "ApproveStatus",
                            KeyValueItems = this.KeyValueManager[this.kvNames[2]],
                            Title=PartsPurchasingUIStrings.DataGridView_TemPurchaseOrder_ColumnItem_ApproveStatus
                        }, new KeyValuesQueryItem {
                            ColumnName = "ReceiveStatus",
                            KeyValueItems = this.KeyValueManager[this.kvNames[3]],
                            Title=PartsPurchasingUIStrings.DataGridView_TemPurchaseOrder_ReceiveStatus
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_CreateTime
                        }, new CustomQueryItem {
                            ColumnName = "IsSaleCode",
                            DataType = typeof(bool),
                            Title= PartsPurchasingUIStrings.QueryPanel_QueryItem_IsSaleCode
                        }, new CustomQueryItem {
                            ColumnName = "IsPurchaseOrder",
                            DataType = typeof(bool),
                            Title= PartsPurchasingUIStrings.QueryPanel_QueryItem_IsPurchaseOrder
                        }
                    }
                }
    };
        }
    }
}