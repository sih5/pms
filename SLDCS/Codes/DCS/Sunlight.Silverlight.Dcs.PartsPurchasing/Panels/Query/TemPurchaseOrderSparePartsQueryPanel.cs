﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Core.ViewModel;
using System.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class TemPurchaseOrderSparePartsQueryPanel : DcsQueryPanelBase {
        public TemPurchaseOrderSparePartsQueryPanel()
        {
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsPurchasingUIStrings.Button_Text_Common_Search,
                    EntityType = typeof(TemPurchaseOrderSpareParts),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title=PartsPurchasingUIStrings.DataGridView_TemPurchaseOrder_ColumnItem_Code
                        }, new CustomQueryItem {
                            ColumnName = "SparePartCode",
                            DataType = typeof(string),
                            Title= PartsPurchasingUIStrings.QueryPanel_QueryItem_SparePartCode
                        }, new CustomQueryItem {
                            ColumnName = "SuplierCode",
                            DataType = typeof(string),
                            Title= PartsPurchasingUIStrings.QueryPanel_QueryItem_SupplierCode
                        }, new CustomQueryItem {
                            ColumnName = "SuplierName",
                            DataType = typeof(string),
                            Title= PartsPurchasingUIStrings.QueryPanel_QueryItem_SupplierName
                        }, new CustomQueryItem {
                            ColumnName = "IsSales",
                            DataType = typeof(bool),
                            Title= "有无销售价"
                        }, new CustomQueryItem {
                            ColumnName = "IsPurchase",
                            DataType = typeof(bool),
                            Title= "有无采购价"
                        }
                    }
                }
    };
        }
    }
}