﻿
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Core.ViewModel;
using System.Windows.Data;


namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class TemPurchaseOrderSuplierQueryPanel : DcsQueryPanelBase {
        public TemPurchaseOrderSuplierQueryPanel() {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private readonly string[] kvNames = {
           "TemPurchasePlanOrderPlanType","TemPurchaseOrderStatus"
        };
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsPurchasingUIStrings.QueryPanel_Title_PartsPurchaseOrde,
                    EntityType = typeof(TemPurchaseOrder),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title=PartsPurchasingUIStrings.DataGridView_TemPurchaseOrder_ColumnItem_Code
                        }, new QueryItem {
                            ColumnName = "TemPurchasePlanOrderCode",
                            Title="原始需求单号"
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_Status
                        }, new KeyValuesQueryItem {
                            ColumnName = "OrderType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title=PartsPurchasingUIStrings.DataEditView_ImportTemplate_OrderType
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_CreateTime
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "ConfirmeTime",
                            Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_InternalAllocationBill_ApproveTime
                        }
                    }
                }
    };
        }
    }
}