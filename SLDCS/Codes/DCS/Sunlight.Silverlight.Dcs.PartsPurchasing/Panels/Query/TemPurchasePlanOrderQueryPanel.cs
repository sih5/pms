﻿
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Core.ViewModel;
using System.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class TemPurchasePlanOrderQueryPanel : DcsQueryPanelBase {
        public TemPurchasePlanOrderQueryPanel() {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private readonly string[] kvNames = {
           "TemPurchasePlanOrderStatus","TemPurchasePlanOrderCustomerType"
        };
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsPurchasingUIStrings.QueryPanel_Title_PartsPurchasePlan,
                    EntityType = typeof(TemPurchasePlanOrder),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_Code
                        }, new CustomQueryItem {
                            ColumnName = "SparePartCode",
                            DataType = typeof(string),
                            Title= PartsPurchasingUIStrings.QueryPanel_QueryItem_SparePartCode
                        }, new KeyValuesQueryItem {
                            ColumnName = "CustomerType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_CustomerType
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_Status
                        }, new QueryItem {
                            ColumnName = "ReceCompanyCode",
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_emPurchasePlanOrder_ReceCompanyCode
                        }, new QueryItem {
                            ColumnName = "ReceCompanyName",
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_emPurchasePlanOrder_ReceCompanyName
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_CreateTime
                        }
                    }
                }
    };
        }
    }
}