﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.Query {
    public class TemSupplierShippingOrderSupplierQueryPanel: DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "TemSupplierShippingOrderStatus"
        };
        private readonly ObservableCollection<KeyValuePair> kvWarehouse = new ObservableCollection<KeyValuePair>();

        public TemSupplierShippingOrderSupplierQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {       
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsPurchasingUIStrings.QueryPanel_Title_SupplierShippingOrder,
                    EntityType = typeof(TemSupplierShippingOrder),
                    QueryItems = new[] {                      
                        new QueryItem {
                            ColumnName = "Code",
                            Title="发运单号"
                        }, new QueryItem {
                            ColumnName = "TemPurchaseOrderCode",
                            Title="临时订单号"
                        }, new QueryItem {
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_SupplierShippingOrder_ReceivingWarehouseId,
                            ColumnName = "ReceivingWarehouseName",
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DCSTemSupplierShippingOrderStatus.新建
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue =new []{
                                new DateTime(DateTime.Now.Year,DateTime.Now.Month,1), DateTime.Now }
                        }
                    }
                }
            };
        }
    }
}
