﻿using System.Linq;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing {
    public class UIStrings : ModelBase {
        public void Refresh(params string[] propertyNames) {
            this.NotifyOfPropertiesChange(propertyNames.Length == 0 ? this.GetType().GetProperties().Select(p => p.Name).ToArray() : propertyNames);
        }   

            public string DataEditPanel_Text_Common_Span_To {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Text_Common_Span_To;
            }
        }

        public string DataEditPanel_Text_Common_Unit_Currency {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Text_Common_Unit_Currency;
            }
        }

        public string DataEditPanel_Text_Common_Unit_Km {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Text_Common_Unit_Km;
            }
        }

        public string DataEditPanel_Text_Common_Unit_Month {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Text_Common_Unit_Month;
            }
        }

        public string DataEditPanel_Text_Common_BranchName {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Text_Common_BranchName;
            }
        }

        public string DetailPanel_Text_Common_BranchName {
            get {
                return Resources.PartsPurchasingUIStrings.DetailPanel_Text_Common_BranchName;
            }
        }

        public string QueryPanel_Title_PartsPurchaseOrder {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_Title_PartsPurchaseOrder;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_SupplierPartCode {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_SupplierPartCode;
            }
        }

        public string QueryPanel_Title_SparePartByPartsPurchaseOrder {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_Title_SparePartByPartsPurchaseOrder;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsBranch_BranchName {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsBranch_BranchName;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsBranch_BranchName {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsBranch_BranchName;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsSupplierRelation_SupplierPartCode {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsSupplierRelation_SupplierPartCode;
            }
        }

        public string DataEditPanel_Text_PartsPurchaseOrder_IfDirectProvision {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Text_PartsPurchaseOrder_IfDirectProvision;
            }
        }

        public string QueryPanel_Title_PartsSupplier {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_Title_PartsSupplier;
            }
        }

        public string QueryPanel_Title_PartsPurchaseOrde {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_Title_PartsPurchaseOrde;
            }
        }

        public string DataEditView_Validation_DepartmentInformatioin_NameIsNotNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_DepartmentInformatioin_NameIsNotNull;
            }
        }

        public string DataEditPanel_Text_InternalAllocationBill_DepartmentName {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Text_InternalAllocationBill_DepartmentName;
            }
        }

        public string DataEditPanel_Text_InternalAllocationBill_WarehouseName {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Text_InternalAllocationBill_WarehouseName;
            }
        }

        public string DataEditPanel_Text_InternalAcquisitionBill_DepartmentName {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Text_InternalAcquisitionBill_DepartmentName;
            }
        }

        public string DataEditPanel_Text_InternalAcquisitionBill_WarehouseName {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Text_InternalAcquisitionBill_WarehouseName;
            }
        }

        public string DataEditPanel_Text_SupplierShippingOrder_DirectProvisionFinished {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Text_SupplierShippingOrder_DirectProvisionFinished;
            }
        }

        public string DetailPanel_Text_SupplierShippingOrder_DirectProvisionFinished {
            get {
                return Resources.PartsPurchasingUIStrings.DetailPanel_Text_SupplierShippingOrder_DirectProvisionFinished;
            }
        }

        public string DetailPanel_Text_SupplierShippingOrder_IfDirectProvision {
            get {
                return Resources.PartsPurchasingUIStrings.DetailPanel_Text_SupplierShippingOrder_IfDirectProvision;
            }
        }

        public string DetailPanel_Text_SupplierShippingOrder_ReceivingCompanyName {
            get {
                return Resources.PartsPurchasingUIStrings.DetailPanel_Text_SupplierShippingOrder_ReceivingCompanyName;
            }
        }

        public string DetailPanel_Text_PartsPurchaseSettleBill_Code {
            get {
                return Resources.PartsPurchasingUIStrings.DetailPanel_Text_PartsPurchaseSettleBill_Code;
            }
        }

        public string ActionPanel_Title_PartsPurchaseRtnSettleBill {
            get {
                return Resources.PartsPurchasingUIStrings.ActionPanel_Title_PartsPurchaseRtnSettleBill;
            }
        }

        public string Action_Title_PartsPurchaseRtnSettleBillInvoiceRegister {
            get {
                return Resources.PartsPurchasingUIStrings.Action_Title_PartsPurchaseRtnSettleBillInvoiceRegister;
            }
        }

        public string Action_Title_PartsPurchaseRtnSettleBillReversesettle {
            get {
                return Resources.PartsPurchasingUIStrings.Action_Title_PartsPurchaseRtnSettleBillReversesettle;
            }
        }

        public string DataEditPanel_GroupTitle_PartsPurchaseSettleBill {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_GroupTitle_PartsPurchaseSettleBill;
            }
        }

        public string DataEditPanel_GroupTitle_PartsPurchaseSettleDetail {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_GroupTitle_PartsPurchaseSettleDetail;
            }
        }

        public string DataEditPanel_GroupTitle_OutboundAndInboundBill {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_GroupTitle_OutboundAndInboundBill;
            }
        }

        public string DataEditPanel_GroupTitle_PartsPurchaseRtnSettleBill {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_GroupTitle_PartsPurchaseRtnSettleBill;
            }
        }

        public string DataEditPanel_GroupTitle_PartsPurchaseRtnSettleDetail {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_GroupTitle_PartsPurchaseRtnSettleDetail;
            }
        }

        public string DataEditPanel_Text_PartsPurchaseSettleBill_CutoffTime {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Text_PartsPurchaseSettleBill_CutoffTime;
            }
        }

        public string DataEditPanel_Text_PartsPurchaseRtnSettleBill_CutoffTime {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Text_PartsPurchaseRtnSettleBill_CutoffTime;
            }
        }

        public string DetailPanel_Text_PartsPurchaseRtnSettleBill_Code {
            get {
                return Resources.PartsPurchasingUIStrings.DetailPanel_Text_PartsPurchaseRtnSettleBill_Code;
            }
        }

        public string Button_Text_Common_Cancle {
            get {
                return Resources.PartsPurchasingUIStrings.Button_Text_Common_Cancle;
            }
        }

        public string Button_Text_Common_Search {
            get {
                return Resources.PartsPurchasingUIStrings.Button_Text_Common_Search;
            }
        }

        public string Button_Text_Common_Sure {
            get {
                return Resources.PartsPurchasingUIStrings.Button_Text_Common_Sure;
            }
        }

        public string Button_Text_Common_Selected {
            get {
                return Resources.PartsPurchasingUIStrings.Button_Text_Common_Selected;
            }
        }

        public string DataEditView_Text_PartsRequisitionSettleBill_CutoffTime {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Text_PartsRequisitionSettleBill_CutoffTime;
            }
        }

        public string DataEditView_Text_PartsRequisitionSettleBill_Department {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Text_PartsRequisitionSettleBill_Department;
            }
        }

        public string DataEditView_Title_PartsRequisitionSettleBill_BranchName {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsRequisitionSettleBill_BranchName;
            }
        }

        public string DataEditView_Title_PartsRequisitionSettleBill_Code {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsRequisitionSettleBill_Code;
            }
        }

        public string DetailPanel_Text_PartsPurchaseOrder_PartsSalesCategoryName {
            get {
                return Resources.PartsPurchasingUIStrings.DetailPanel_Text_PartsPurchaseOrder_PartsSalesCategoryName;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrder_PartsSalesCategoryName {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_PartsSalesCategoryName;
            }
        }

        public string DataEditView_Title_PendingApproval_PartsPurchaseOrder {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PendingApproval_PartsPurchaseOrder;
            }
        }

        public string QueryPanel_QueryItem_Title_Order_Code {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_Order_Code;
            }
        }

        public string DetailPanel_Title_PartsOuterPurchaselist {
            get {
                return Resources.PartsPurchasingUIStrings.DetailPanel_Title_PartsOuterPurchaselist;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsSalesCategory_Name {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name;
            }
        }

        public string DataEditView_Title_PartsOuterPurchaseChange_PartsOuterPurchaselist {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsOuterPurchaseChange_PartsOuterPurchaselist;
            }
        }

        public string DataEditView_Validation_PartsSalesCategory_NameIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_PartsSalesCategory_NameIsNull;
            }
        }

        public string DataEditPanel_Text_Button_Cancel {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Text_Button_Cancel;
            }
        }

        public string DataEditPanel_Text_Button_Submit {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Text_Button_Submit;
            }
        }

        public string DataEditView_Title_Add_PartsPurchaseOuter_AbandonComment {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_Add_PartsPurchaseOuter_AbandonComment;
            }
        }

        public string DataEditView_Validation_PartsOuterPurchaseChange_AbandonCommentNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_PartsOuterPurchaseChange_AbandonCommentNull;
            }
        }

        public string DataEditView_Validation_PartsPurchaseOrderType_CodeIsNotNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrderType_CodeIsNotNull;
            }
        }

        public string DataEditView_Validation_PartsPurchaseOrderType_NameIsNotNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrderType_NameIsNotNull;
            }
        }

        public string DataEditView_Validation_PartsPurchaseOrderType_PartsSalesCategoryIdIsNotNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrderType_PartsSalesCategoryIdIsNotNull;
            }
        }

        public string DataManagementView_Title_PartsPurchaseOrderType {
            get {
                return Resources.PartsPurchasingUIStrings.DataManagementView_Title_PartsPurchaseOrderType;
            }
        }

        public string DetailPanel_Text_PartsPurchaseOrder_IfDirectProvision {
            get {
                return Resources.PartsPurchasingUIStrings.DetailPanel_Text_PartsPurchaseOrder_IfDirectProvision;
            }
        }

        public string DetailPanel_Title_PartsPurchaseOrderKanBanDetail {
            get {
                return Resources.PartsPurchasingUIStrings.DetailPanel_Title_PartsPurchaseOrderKanBanDetail;
            }
        }

        public string QueryPanel_Title_BranchSupplierRelations {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_Title_BranchSupplierRelations;
            }
        }

        public string DataGridView_ColumnItem_SparePart_Title_PartsSalePrice {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_SparePart_Title_PartsSalePrice;
            }
        }

        public string DataEditPanel_Text_SupplierShippingOrder_PartsSalesCategoryName {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Text_SupplierShippingOrder_PartsSalesCategoryName;
            }
        }

        public string DetailPanel_Text_SupplierShippingOrder_PartsSalesCategoryName {
            get {
                return Resources.PartsPurchasingUIStrings.DetailPanel_Text_SupplierShippingOrder_PartsSalesCategoryName;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurReturnOrder_PartsSalesCategoryName {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurReturnOrder_PartsSalesCategoryName;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsSalesOrder_PartsSalesCategoryName {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrder_PartsSalesCategoryName;
            }
        }

        public string DataEditView_Title_PartsPurReturnOrder_PartsSalesCategoryName {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurReturnOrder_PartsSalesCategoryName;
            }
        }

        public string DataEdit_Text_PartsPurReturnOrder_PartsSalesCategoryName {
            get {
                return Resources.PartsPurchasingUIStrings.DataEdit_Text_PartsPurReturnOrder_PartsSalesCategoryName;
            }
        }

        public string QueryPanel_Title_PartsPurchaseOrderType {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_Title_PartsPurchaseOrderType;
            }
        }

        public string QueryPanel_KeyValuesQueryItem_Title_PartsPurchaseOrder_PartsSalesCategory {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_KeyValuesQueryItem_Title_PartsPurchaseOrder_PartsSalesCategory;
            }
        }

        public string DataGridView_Validation_InternalAcquisitionDetail_UnitPriceIsNotLessZero {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_Validation_InternalAcquisitionDetail_UnitPriceIsNotLessZero;
            }
        }

        public string DataEditView_Validation_InternalAcquisitionDetailIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_InternalAcquisitionDetailIsNull;
            }
        }

        public string DataEditView_Validation_PartsRequisitionSettleBill_CutoffTimeIsNotNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_PartsRequisitionSettleBill_CutoffTimeIsNotNull;
            }
        }

        public string DataEditView_Validation_PartsRequisitionSettleBill_DepartmentIdIsNotNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_PartsRequisitionSettleBill_DepartmentIdIsNotNull;
            }
        }

        public string DataEditView_Validation_PartsRequisitionSettleBill_PartsSalesCategoryIdIsNotNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_PartsRequisitionSettleBill_PartsSalesCategoryIdIsNotNull;
            }
        }

        public string Button_Text_Common_Export {
            get {
                return Resources.PartsPurchasingUIStrings.Button_Text_Common_Export;
            }
        }

        public string Button_Text_Common_Import {
            get {
                return Resources.PartsPurchasingUIStrings.Button_Text_Common_Import;
            }
        }

        public string PartsPurchaseOrder_TotalAmount {
            get {
                return Resources.PartsPurchasingUIStrings.PartsPurchaseOrder_TotalAmount;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrderImport {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrderImport;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsPurchaseRtnSettleBill_CategoryName {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseRtnSettleBill_CategoryName;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchaseRtnSettleBill_PartsSalesCategoryName {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseRtnSettleBill_PartsSalesCategoryName;
            }
        }

        public string DataEditView_Title_PartsPurchaseRtnSettleBill_TotalSettlementAmount {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseRtnSettleBill_TotalSettlementAmount;
            }
        }

        public string DetailPanel_Text_OriginalRequirementBillCode {
            get {
                return Resources.PartsPurchasingUIStrings.DetailPanel_Text_OriginalRequirementBillCode;
            }
        }

        public string DataEditView_Text_SupplierShippingOrder_OriginalRequirementBillCode {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Text_SupplierShippingOrder_OriginalRequirementBillCode;
            }
        }

        public string DataGridView_Validation_InternalAcquisitionBill_WarehouseIdIsRequired {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_Validation_InternalAcquisitionBill_WarehouseIdIsRequired;
            }
        }

        public string DataEditView_Validation_PartsPurchaseSettleBill_TaxRateIsNotNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseSettleBill_TaxRateIsNotNull;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrder_TotalAmount {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_TotalAmount;
            }
        }

        public string DataManagementView_PrintWindow_Title_PartsPurchaseRtnSettleBill {
            get {
                return Resources.PartsPurchasingUIStrings.DataManagementView_PrintWindow_Title_PartsPurchaseRtnSettleBill;
            }
        }

        public string DataGridView_ColumnItem_Title_OutboundAndInboundBill_WarehouseName {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_WarehouseName;
            }
        }

        public string DataManagementView_PrintWindow_Title_PartsRequisitionSettleBill {
            get {
                return Resources.PartsPurchasingUIStrings.DataManagementView_PrintWindow_Title_PartsRequisitionSettleBill;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_Specification {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_Specification;
            }
        }

        public string DataEditView_Validation_DetailCountMoreThan200 {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_DetailCountMoreThan200;
            }
        }

        public string DataEditView_PartsOuterPurchaseChange_Validation_OutLength {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_PartsOuterPurchaseChange_Validation_OutLength;
            }
        }

        public string DataEditView_Validation_DetailCountMoreThan400 {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_DetailCountMoreThan400;
            }
        }

        public string DataEditView_Error_SparePart_OrderAmountMoreThanDirectOrderAmount {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Error_SparePart_OrderAmountMoreThanDirectOrderAmount;
            }
        }

        public string DataEditView_Title_InvoiceInformation {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_InvoiceInformation;
            }
        }

        public string DataEditView_Title_InvoiceRegister_PartsPurchaseSettleBillAll {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_InvoiceRegister_PartsPurchaseSettleBillAll;
            }
        }

        public string DataEditView_Validation_InvoiceInformation_TaxRateMustBetweenZeroAndOne {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_TaxRateMustBetweenZeroAndOne;
            }
        }

        public string DataEditView_Title_InvoiceApprove_PartsPurchaseSettleBillAll {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_InvoiceApprove_PartsPurchaseSettleBillAll;
            }
        }

        public string DataEditView_Title_InvoiceEdit_PartsPurchaseSettleBillAll {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_InvoiceEdit_PartsPurchaseSettleBillAll;
            }
        }

        public string DataEditView_Validation_InvoiceInformation_SumInvoiceAmountMoreThanTotalSettlementAmount {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_SumInvoiceAmountMoreThanTotalSettlementAmount;
            }
        }

        public string DataEditView_Text_SupplierPlanArrear_PartsSalesCategory {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Text_SupplierPlanArrear_PartsSalesCategory;
            }
        }

        public string DataEditView_PopupTextBox_Title_SupplierCompany {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_PopupTextBox_Title_SupplierCompany;
            }
        }

        public string DataEditView_Text_SupplierPreApprovedLoan_PartsSalesCategory {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Text_SupplierPreApprovedLoan_PartsSalesCategory;
            }
        }

        public string DataEditView_Text_SupplierPreApprovedLoan_ActualDebt {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Text_SupplierPreApprovedLoan_ActualDebt;
            }
        }

        public string DataEditView_Text_SupplierPreApprovedLoan_PlanArrear {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Text_SupplierPreApprovedLoan_PlanArrear;
            }
        }

        public string DetailPanel_Title_InvoiceInformation_ApproverName {
            get {
                return Resources.PartsPurchasingUIStrings.DetailPanel_Title_InvoiceInformation_ApproverName;
            }
        }

        public string DetailPanel_Title_InvoiceInformation_ApproveTime {
            get {
                return Resources.PartsPurchasingUIStrings.DetailPanel_Title_InvoiceInformation_ApproveTime;
            }
        }

        public string DataEditView_Validation_InvoiceInformation_InvoiceCodeMoreThan10 {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceCodeMoreThan10;
            }
        }

        public string DataEditView_Validation_InvoiceInformation_InvoiceNumberLengthMoreThan20 {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceNumberLengthMoreThan20;
            }
        }

        public string DataEditView_Validation_PartsPurchaseSettleDetailIsNone {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseSettleDetailIsNone;
            }
        }

        public string DataEditView_Validation_PartsPurReturnOrderDetail_UnitPriceLessThanZero {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_PartsPurReturnOrderDetail_UnitPriceLessThanZero;
            }
        }

        public string QueryPanel_Title_PartsSupplierRelation {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_Title_PartsSupplierRelation;
            }
        }

        public string DataEditView_Validation_PartConPurchasePlan_ActualOrderWarehouseIdIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_PartConPurchasePlan_ActualOrderWarehouseIdIsNull;
            }
        }

        public string DataEditView_Validation_PartConPurchasePlan_ActualPurchaseIsNullOrLessThanZero {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_PartConPurchasePlan_ActualPurchaseIsNullOrLessThanZero;
            }
        }

        public string DataEditView_Validation_PartConPurchasePlan_ActualSupplierCodeIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_PartConPurchasePlan_ActualSupplierCodeIsNull;
            }
        }

        public string DataEditView_Validation_PartConPurchasePlan_AdvicePurchaseIsNullOrLessThanZero {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_PartConPurchasePlan_AdvicePurchaseIsNullOrLessThanZero;
            }
        }

        public string Action_Title_PartsPurchasePlanHW {
            get {
                return Resources.PartsPurchasingUIStrings.Action_Title_PartsPurchasePlanHW;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_BussinessCode {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_BussinessCode;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_Code {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_Code;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_CreateTime {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_CreateTime;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_ModifyTime {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_ModifyTime;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_PartsSalesCategoryName {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_PartsSalesCategoryName;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_PartsSupplierCode {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_PartsSupplierCode;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_PartsSupplierName {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_PartsSupplierName;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_PurchasePlanType {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_PurchasePlanType;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_RequestedDeliveryTime {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_RequestedDeliveryTime;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_Status {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_Status;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_StopTime {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_StopTime;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_WarehouseId {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_WarehouseId;
            }
        }

        public string Action_Title_Analyze {
            get {
                return Resources.PartsPurchasingUIStrings.Action_Title_Analyze;
            }
        }

        public string Action_Title_Termination {
            get {
                return Resources.PartsPurchasingUIStrings.Action_Title_Termination;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchasePlan_HW {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW;
            }
        }

        public string DataManagementView_Title_PartsPurchasePlan_HW {
            get {
                return Resources.PartsPurchasingUIStrings.DataManagementView_Title_PartsPurchasePlan_HW;
            }
        }

        public string Action_Title_DataCheck {
            get {
                return Resources.PartsPurchasingUIStrings.Action_Title_DataCheck;
            }
        }

        public string DetailPanel_Title_PartsPurchasePlanDetail_HW {
            get {
                return Resources.PartsPurchasingUIStrings.DetailPanel_Title_PartsPurchasePlanDetail_HW;
            }
        }

        public string DataManagementView_PrintWindow_Title_SupplierShippingOrderNoPrice {
            get {
                return Resources.PartsPurchasingUIStrings.DataManagementView_PrintWindow_Title_SupplierShippingOrderNoPrice;
            }
        }

        public string Action_Title_Termination1 {
            get {
                return Resources.PartsPurchasingUIStrings.Action_Title_Termination1;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_SAPPurchasePlanCode {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_SAPPurchasePlanCode;
            }
        }

        public string DataEditPanel_Title_ApproveComment {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Title_ApproveComment;
            }
        }

        public string DataEditPanel_Title_ApprovePassed {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Title_ApprovePassed;
            }
        }

        public string DataEditPanel_Title_File {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Title_File;
            }
        }

        public string DataEditPanel_Title_FileButton_Delete {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Title_FileButton_Delete;
            }
        }

        public string DataEditPanel_Title_FileButton_Upload {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Title_FileButton_Upload;
            }
        }

        public string DataEditPanel_Title_FinalApproveComment {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Title_FinalApproveComment;
            }
        }

        public string DataEditPanel_Title_InitialApproveComment {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Title_InitialApproveComment;
            }
        }

        public string DataEditPanel_Title_InternalAllocationBill_Type {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Title_InternalAllocationBill_Type;
            }
        }

        public string DataEditPanel_Title_OutInboundDetail {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Title_OutInboundDetail;
            }
        }

        public string DataEditPanel_Title_PartABCDetail {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Title_PartABCDetail;
            }
        }

        public string DataEditPanel_Title_PartsPurReturnOrder_Code {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Title_PartsPurReturnOrder_Code;
            }
        }

        public string DataEditPanel_Title_Reject {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Title_Reject;
            }
        }

        public string DataEditPanel_Title_RequisitionDepartment {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Title_RequisitionDepartment;
            }
        }

        public string DataEditPanel_Title_SupplierShippingOrder_ContactPerson {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Title_SupplierShippingOrder_ContactPerson;
            }
        }

        public string DataEditPanel_Title_SupplierShippingOrder_ContactPhone {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Title_SupplierShippingOrder_ContactPhone;
            }
        }

        public string DataEditPanel_Title_SupplierShippingOrder_PlanDeliveryTime {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Title_SupplierShippingOrder_PlanDeliveryTime;
            }
        }

        public string DataEditPanel_Title_TerminateReason {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_Title_TerminateReason;
            }
        }

        public string DetailPanel_Title_VolumeAll {
            get {
                return Resources.PartsPurchasingUIStrings.DetailPanel_Title_VolumeAll;
            }
        }

        public string DetailPanel_Title_WeightAll {
            get {
                return Resources.PartsPurchasingUIStrings.DetailPanel_Title_WeightAll;
            }
        }

        public string DataEditView_NotificationInternalAllocationDetailsMoreThanFourHundred {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_NotificationInternalAllocationDetailsMoreThanFourHundred;
            }
        }

        public string DataEditView_Notification_CleanInternalAllocationBillDetail {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Notification_CleanInternalAllocationBillDetail;
            }
        }

        public string DataEditView_Notification_DepartmentIdIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Notification_DepartmentIdIsNull;
            }
        }

        public string DataEditView_Notification_OperateSuccess {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Notification_OperateSuccess;
            }
        }

        public string DataEditView_Notification_RejectSuccess {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Notification_RejectSuccess;
            }
        }

        public string DataEditView_Notification_TypeIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Notification_TypeIsNull;
            }
        }

        public string DataEditView_Text_InvoiceInformation_Code {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Text_InvoiceInformation_Code;
            }
        }

        public string DataEditView_Text_InvoiceInformation_SupplierCode {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Text_InvoiceInformation_SupplierCode;
            }
        }

        public string DataEditView_Text_InvoiceInformation_SupplierName {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Text_InvoiceInformation_SupplierName;
            }
        }

        public string DataEditView_Text_InvoiceInformation_TotalSettlementAmount {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Text_InvoiceInformation_TotalSettlementAmount;
            }
        }

        public string DataEditView_Title_ChooseInternalAllocationBill {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_ChooseInternalAllocationBill;
            }
        }

        public string DataEditView_Title_Department {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_Department;
            }
        }

        public string DataEditView_Title_InitialInternalAllocationBill {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_InitialInternalAllocationBill;
            }
        }

        public string DataEditView_Title_InternalAllocationBill {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_InternalAllocationBill;
            }
        }

        public string QueryPanel_QueryItem_DirectProvisionFinished {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_DirectProvisionFinished;
            }
        }

        public string QueryPanel_QueryItem_IfDirectProvision {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_IfDirectProvision;
            }
        }

        public string QueryPanel_QueryItem_IoStatus {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_IoStatus;
            }
        }

        public string QueryPanel_QueryItem_IoStatus_Fail {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_IoStatus_Fail;
            }
        }

        public string QueryPanel_QueryItem_IoStatus_Success {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_IoStatus_Success;
            }
        }

        public string QueryPanel_QueryItem_IoStatus_WaitTransfer {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_IoStatus_WaitTransfer;
            }
        }

        public string QueryPanel_QueryItem_PartsShippingOrder_PartsPurchaseOrderCode {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsShippingOrder_PartsPurchaseOrderCode;
            }
        }

        public string QueryPanel_QueryItem_TitlePartsPurReturnOrder_Code {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_TitlePartsPurReturnOrder_Code;
            }
        }

        public string DataEditView_Title_ApproveComment {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_ApproveComment;
            }
        }

        public string DataEditView_Title_InitialApproveComment {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_InitialApproveComment;
            }
        }

        public string DataEditView_Title_PartsOuterPurchaseBillDetails {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsOuterPurchaseBillDetails;
            }
        }

        public string DataEditView_Title_PlanSource {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PlanSource;
            }
        }

        public string DataEditView_Title_Reject {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_Reject;
            }
        }

        public string DataEditView_Title_RejectPartsOuterPurchaseChange {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_RejectPartsOuterPurchaseChange;
            }
        }

        public string DataEditView_Title_RejectReason {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_RejectReason;
            }
        }

        public string DataEditView_Validation_ApproveCommentIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_ApproveCommentIsNull;
            }
        }

        public string DataEditView_Validation_ClearDetailsOnCategoryChanged {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_ClearDetailsOnCategoryChanged;
            }
        }

        public string DataEditView_Validation_MustUploadFiles {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_MustUploadFiles;
            }
        }

        public string DataEditView_Validation_ReceivingWarehouseIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_ReceivingWarehouseIsNull;
            }
        }

        public string DataEditView_Validation_RejectReasonIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_RejectReasonIsNull;
            }
        }

        public string DataEditView_Title_SAPPurchasePlanCode {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_SAPPurchasePlanCode;
            }
        }

        public string DataEditView_Title_SpareInterchangeInfoDetails {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_SpareInterchangeInfoDetails;
            }
        }

        public string DataEditView_Title_WayChange {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_WayChange;
            }
        }

        public string DataEditView_Validation_MaxOrderAmount {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_MaxOrderAmount;
            }
        }

        public string DataEditView_Validation_NoPackingProp {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_NoPackingProp;
            }
        }

        public string DataEditView_Validation_OrderAmountNotSatisfy {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_OrderAmountNotSatisfy;
            }
        }

        public string DataEditView_Validation_OrderDetailsNoPlanPriceOrNoPurchasePrice {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_OrderDetailsNoPlanPriceOrNoPurchasePrice;
            }
        }

        public string DataEditView_Validation_PlanSourceIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_PlanSourceIsNull;
            }
        }

        public string DataEditView_Validation_PlanSourcePartsSupplierCode {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_PlanSourcePartsSupplierCode;
            }
        }

        public string DataEditView_Validation_RequestedDeliveryTimeMustMoreThanNow {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_RequestedDeliveryTimeMustMoreThanNow;
            }
        }

        public string DataEditView_ImportTemplate_DetailRemark {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_ImportTemplate_DetailRemark;
            }
        }

        public string DataEditView_ImportTemplate_MainRemark {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_ImportTemplate_MainRemark;
            }
        }

        public string DataEditView_ImportTemplate_OrderAllSign {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_ImportTemplate_OrderAllSign;
            }
        }

        public string DataEditView_ImportTemplate_OrderAmount {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_ImportTemplate_OrderAmount;
            }
        }

        public string DataEditView_ImportTemplate_OrderType {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_ImportTemplate_OrderType;
            }
        }

        public string DataEditView_ImportTemplate_PartsSalesCategory {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_ImportTemplate_PartsSalesCategory;
            }
        }

        public string DataEditView_ImportTemplate_PlanAmount {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_ImportTemplate_PlanAmount;
            }
        }

        public string DataEditView_ImportTemplate_PlanSource {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_ImportTemplate_PlanSource;
            }
        }

        public string DataEditView_ImportTemplate_RequestedDeliveryTime {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_ImportTemplate_RequestedDeliveryTime;
            }
        }

        public string DataEditView_ImportTemplate_ShippingMethod {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_ImportTemplate_ShippingMethod;
            }
        }

        public string DataEditView_ImportTemplate_SparePartCode {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_ImportTemplate_SparePartCode;
            }
        }

        public string DataEditView_ImportTemplate_SparePartName {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_ImportTemplate_SparePartName;
            }
        }

        public string DataEditView_ImportTemplate_SupplierCode {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_ImportTemplate_SupplierCode;
            }
        }

        public string DataEditView_ImportTemplate_SupplierName {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_ImportTemplate_SupplierName;
            }
        }

        public string DataEditView_ImportTemplate_WarehouseName {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_ImportTemplate_WarehouseName;
            }
        }

        public string DataEditView_Notification_ClearDataOnUpdateBranch {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Notification_ClearDataOnUpdateBranch;
            }
        }

        public string DataEditView_Notification_ConfirmAmountError {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Notification_ConfirmAmountError;
            }
        }

        public string DataEditView_Text_ActualDebt {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Text_ActualDebt;
            }
        }

        public string DataEditView_Text_AmountAndPrintNumMustGreaterThanZero {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Text_AmountAndPrintNumMustGreaterThanZero;
            }
        }

        public string DataEditView_Text_BatchAdd {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Text_BatchAdd;
            }
        }

        public string DataEditView_Text_BigLablePrint {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Text_BigLablePrint;
            }
        }

        public string DataEditView_Text_EachOnePrint {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Text_EachOnePrint;
            }
        }

        public string DataEditView_Text_NumbersMustGreaterThanZero {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Text_NumbersMustGreaterThanZero;
            }
        }

        public string DataEditView_Text_PartDetailsInfo {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Text_PartDetailsInfo;
            }
        }

        public string DataEditView_Text_PartLablePrint {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Text_PartLablePrint;
            }
        }

        public string DataEditView_Text_PartLablePrintWithStandard {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Text_PartLablePrintWithStandard;
            }
        }

        public string DataEditView_Text_PlanArrear {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Text_PlanArrear;
            }
        }

        public string DataEditView_Text_PrintByNumber {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Text_PrintByNumber;
            }
        }

        public string DataEditView_Text_PrintLables {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Text_PrintLables;
            }
        }

        public string DataEditView_Text_PrintNumbers {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Text_PrintNumbers;
            }
        }

        public string DataEditView_Text_SettleType {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Text_SettleType;
            }
        }

        public string DataEditView_Text_ShippingDetailsInfo {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Text_ShippingDetailsInfo;
            }
        }

        public string DataEditView_Text_SmallLablePrint {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Text_SmallLablePrint;
            }
        }

        public string DataEditView_Text_SparePartCode {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Text_SparePartCode;
            }
        }

        public string DataEditView_Text_SparePartName {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Text_SparePartName;
            }
        }

        public string DataEditView_Text_SupplierShippingDetailsInfo {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Text_SupplierShippingDetailsInfo;
            }
        }

        public string DataEditView_Title_AbandonPartsPurchaseOrder {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_AbandonPartsPurchaseOrder;
            }
        }

        public string DataEditView_Title_AbandonPurchasePlan {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_AbandonPurchasePlan;
            }
        }

        public string DataEditView_Title_AbandonReason {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_AbandonReason;
            }
        }

        public string DataEditView_Title_AddToShippingDetails {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_AddToShippingDetails;
            }
        }

        public string DataEditView_Title_AlreadyReturnFee {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_AlreadyReturnFee;
            }
        }

        public string DataEditView_Title_ApprovePartsPurReturnOrder {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_ApprovePartsPurReturnOrder;
            }
        }

        public string DataEditView_Title_Cancel {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_Cancel;
            }
        }

        public string DataEditView_Title_Clear {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_Clear;
            }
        }

        public string DataEditView_Title_Confirm {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_Confirm;
            }
        }

        public string DataEditView_Title_DeleteShippingDetails {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_DeleteShippingDetails;
            }
        }

        public string DataEditView_Title_DetailCountError {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_DetailCountError;
            }
        }

        public string DataEditView_Title_ExportDetails {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_ExportDetails;
            }
        }

        public string DataEditView_Title_ExportPartDetails {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_ExportPartDetails;
            }
        }

        public string DataEditView_Title_FinalApproveMemo {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_FinalApproveMemo;
            }
        }

        public string DataEditView_Title_Import {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_Import;
            }
        }

        public string DataEditView_Title_ImportPartsPurchaseOrder {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_ImportPartsPurchaseOrder;
            }
        }

        public string DataEditView_Title_InitialApproveMemo {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_InitialApproveMemo;
            }
        }

        public string DataEditView_Title_InitialApproveMemoIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_InitialApproveMemoIsNull;
            }
        }

        public string DataEditView_Title_InvoiceCode {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_InvoiceCode;
            }
        }

        public string DataEditView_Title_NoPurchasePriceExport {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_NoPurchasePriceExport;
            }
        }

        public string DataEditView_Title_OrderType {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_OrderType;
            }
        }

        public string DataEditView_Title_OverseasPartsPurchasePlan {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_OverseasPartsPurchasePlan;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrderDecompose {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrderDecompose;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrderDetails {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrderDetails;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrderForImport {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrderForImport;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrderPlanDetails {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrderPlanDetails;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrderPlanInfo {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrderPlanInfo;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrderPlan_Code {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrderPlan_Code;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrderPlan_CreateTime {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrderPlan_CreateTime;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrderPlan_IsTransferSAP {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrderPlan_IsTransferSAP;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrderPlan_SAPPurchasePlanCode {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrderPlan_SAPPurchasePlanCode;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrderQuery {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrderQuery;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrderType {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrderType;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrder_Add {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_Add;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrder_Code {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_Code;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrder_Create {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_Create;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrder_CreateTime {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_CreateTime;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrder_Creator {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_Creator;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrder_DeliveryBillNumber {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_DeliveryBillNumber;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrder_Driver {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_Driver;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrder_ExpectDeliveryTime {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_ExpectDeliveryTime;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrder_IfDirectProvision {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_IfDirectProvision;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrder_LogisticCompany {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_LogisticCompany;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrder_MainShippingInformation {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_MainShippingInformation;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrder_OrderType {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_OrderType;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrder_Phone {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_Phone;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrder_Query {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_Query;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrder_ReceivingWarehouse {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_ReceivingWarehouse;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrder_Remark {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_Remark;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrder_RequestedDeliveryTime {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_RequestedDeliveryTime;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrder_Shipping {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_Shipping;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrder_ShippingMethod {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_ShippingMethod;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrder_SparePartName {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_SparePartName;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrder_SupplierPartCode {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_SupplierPartCode;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrder_To {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_To;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrder_VehicleLicensePlate {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_VehicleLicensePlate;
            }
        }

        public string DataEditView_Title_PartsPurchaseRtnSettleBill_Code {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseRtnSettleBill_Code;
            }
        }

        public string DataEditView_Title_PartsPurchaseRtnSettleBill_InvoiceDate {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseRtnSettleBill_InvoiceDate;
            }
        }

        public string DataEditView_Title_PartsPurchaseRtnSettleDetails {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseRtnSettleDetails;
            }
        }

        public string DataEditView_Title_PartsTransferOrder_Add {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsTransferOrder_Add;
            }
        }

        public string DataEditView_Title_PartsTransferOrder_Create {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsTransferOrder_Create;
            }
        }

        public string DataEditView_Title_PlanAmountMustMoreThanZero {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PlanAmountMustMoreThanZero;
            }
        }

        public string DataEditView_Title_PlanDeliveryTime {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PlanDeliveryTime;
            }
        }

        public string DataEditView_Title_PurchaseOrderPlanDetailsIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PurchaseOrderPlanDetailsIsNull;
            }
        }

        public string DataEditView_Title_Query {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_Query;
            }
        }

        public string DataEditView_Title_ReturnFee {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_ReturnFee;
            }
        }

        public string DataEditView_Title_ShippingAll {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_ShippingAll;
            }
        }

        public string DataEditView_Title_ShippingDetails {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_ShippingDetails;
            }
        }

        public string DataEditView_Title_SupplierShipping {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_SupplierShipping;
            }
        }

        public string DataEditView_Title_Template {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_Template;
            }
        }

        public string DataEditView_Title_TerminatePartsPurchaseOrder {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_TerminatePartsPurchaseOrder;
            }
        }

        public string DataEditView_Title_TerminateReason {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_TerminateReason;
            }
        }

        public string DataEditView_Title_Try {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_Try;
            }
        }

        public string DataEditView_Title_Warehouse {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_Warehouse;
            }
        }

        public string DataEditView_Validation_AbandonReasonIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_AbandonReasonIsNull;
            }
        }

        public string DataEditView_Validation_AmountError {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_AmountError;
            }
        }

        public string DataEditView_Validation_CantEditReturnPrice {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_CantEditReturnPrice;
            }
        }

        public string DataEditView_Validation_CategoryIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_CategoryIsNull;
            }
        }

        public string DataEditView_Validation_ChooseApproveResult {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_ChooseApproveResult;
            }
        }

        public string DataEditView_Validation_ConfireAmountIsAllZero {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_ConfireAmountIsAllZero;
            }
        }

        public string DataEditView_Validation_CreatePartsTransferOrderError1 {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_CreatePartsTransferOrderError1;
            }
        }

        public string DataEditView_Validation_CreatePartsTransferOrderError2 {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_CreatePartsTransferOrderError2;
            }
        }

        public string DataEditView_Validation_CreatePartsTransferOrderError3 {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_CreatePartsTransferOrderError3;
            }
        }

        public string DataEditView_Validation_CreatePartsTransferOrderError4 {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_CreatePartsTransferOrderError4;
            }
        }

        public string DataEditView_Validation_DetailsAmountError {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_DetailsAmountError;
            }
        }

        public string DataEditView_Validation_DetailsIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_DetailsIsNull;
            }
        }

        public string DataEditView_Validation_DetailsLessThanZero {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_DetailsLessThanZero;
            }
        }

        public string DataEditView_Validation_ExpectDeliveryTimeError {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_ExpectDeliveryTimeError;
            }
        }

        public string DataEditView_Validation_ExpectDeliveryTimeIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_ExpectDeliveryTimeIsNull;
            }
        }

        public string DataEditView_Validation_InvoiceCodeIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_InvoiceCodeIsNull;
            }
        }

        public string DataEditView_Validation_InvoiceDateIsNotNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_InvoiceDateIsNotNull;
            }
        }

        public string DataEditView_Validation_InvoiceDateIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_InvoiceDateIsNull;
            }
        }

        public string DataEditView_Validation_InvoiceFeeGreaterThan {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_InvoiceFeeGreaterThan;
            }
        }

        public string DataEditView_Validation_InvoiceNumberIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_InvoiceNumberIsNull;
            }
        }

        public string DataEditView_Validation_InvoiceTaxIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_InvoiceTaxIsNull;
            }
        }

        public string DataEditView_Validation_OutboundAndInboundBillsIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_OutboundAndInboundBillsIsNull;
            }
        }

        public string DataEditView_Validation_PartABCIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_PartABCIsNull;
            }
        }

        public string DataEditView_Validation_PartsSalesCategoryIdIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_PartsSalesCategoryIdIsNull;
            }
        }

        public string DataEditView_Validation_PartsSupplierCodeIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_PartsSupplierCodeIsNull;
            }
        }

        public string DataEditView_Validation_PlanAmountMustMoreThanZero {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_PlanAmountMustMoreThanZero;
            }
        }

        public string DataEditView_Validation_PlanTypeIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_PlanTypeIsNull;
            }
        }

        public string DataEditView_Validation_PleaseWriteApproveResult {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_PleaseWriteApproveResult;
            }
        }

        public string DataEditView_Validation_PurchaseOrderCodeIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_PurchaseOrderCodeIsNull;
            }
        }

        public string DataEditView_Validation_SettlementPriceIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_SettlementPriceIsNull;
            }
        }

        public string DataEditView_Validation_SettlementPriceIsZero {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_SettlementPriceIsZero;
            }
        }

        public string DataEditView_Validation_ShippingDetailsIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_ShippingDetailsIsNull;
            }
        }

        public string DataEditView_Validation_ShortSupReasonIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_ShortSupReasonIsNull;
            }
        }

        public string DataEditView_Validation_TaxRateIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_TaxRateIsNull;
            }
        }

        public string DataEditView_Validation_TerminateReasonIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_TerminateReasonIsNull;
            }
        }

        public string DataEditView_Validation_ThisShippingAmountGreaterThan {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_ThisShippingAmountGreaterThan;
            }
        }

        public string DataEditView_Validation_TotalSettlementAmount {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_TotalSettlementAmount;
            }
        }

        public string DataEditView_Validation_TotalSettlementAmountError {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_TotalSettlementAmountError;
            }
        }

        public string DataEditView_Validation_WarehouseIdIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_WarehouseIdIsNull;
            }
        }

        public string DataEditView_Validation_WarehouseIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_WarehouseIsNull;
            }
        }

        public string DataEditView_Validation_WhetherToContinue {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_WhetherToContinue;
            }
        }

        public string DataGridView_Column_Amount {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_Column_Amount;
            }
        }

        public string DataGridView_Column_InboundStatus {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_Column_InboundStatus;
            }
        }

        public string DataGridView_Column_IsOrderable {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_Column_IsOrderable;
            }
        }

        public string DataGridView_Column_PartsBranchIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_Column_PartsBranchIsNull;
            }
        }

        public string DataGridView_Column_PlanPriceIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_Column_PlanPriceIsNull;
            }
        }

        public string DataGridView_Column_PlanPriceTooMany {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_Column_PlanPriceTooMany;
            }
        }

        public string DataGridView_Column_ReferenceCodeQuery {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_Column_ReferenceCodeQuery;
            }
        }

        public string DataGridView_Column_WholeSalePirceIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_Column_WholeSalePirceIsNull;
            }
        }

        public string DataGridView_Column_WholeSalePirceTooMany {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_Column_WholeSalePirceTooMany;
            }
        }

        public string DataEditView_ImportTemplate_MeasureUnit {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_ImportTemplate_MeasureUnit;
            }
        }

        public string DataGridView_ColumnItem_ABCStrategy {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_ABCStrategy;
            }
        }

        public string DataGridView_ColumnItem_BJCDCStock {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_BJCDCStock;
            }
        }

        public string DataGridView_ColumnItem_BranchQuantity {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_BranchQuantity;
            }
        }

        public string DataGridView_ColumnItem_CanAnalyzeQty {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_CanAnalyzeQty;
            }
        }

        public string DataGridView_ColumnItem_ChangedSparePartCode {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_ChangedSparePartCode;
            }
        }

        public string DataGridView_ColumnItem_EndAmount {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_EndAmount;
            }
        }

        public string DataGridView_ColumnItem_FaultReason {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_FaultReason;
            }
        }

        public string DataGridView_ColumnItem_GDCDCStock {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_GDCDCStock;
            }
        }

        public string DataGridView_ColumnItem_GPMSPartCode {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_GPMSPartCode;
            }
        }

        public string DataGridView_ColumnItem_GPMSRowNum {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_GPMSRowNum;
            }
        }

        public string DataGridView_ColumnItem_InsteadType {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_InsteadType;
            }
        }

        public string DataGridView_ColumnItem_InterfaceMessage {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_InterfaceMessage;
            }
        }

        public string DataGridView_ColumnItem_MinPackingAmount {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_MinPackingAmount;
            }
        }

        public string DataGridView_ColumnItem_Modifier {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_Modifier;
            }
        }

        public string DataGridView_ColumnItem_Number {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_Number;
            }
        }

        public string DataGridView_ColumnItem_OrderStatus {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_OrderStatus;
            }
        }

        public string DataGridView_ColumnItem_OriginalSparePartCode {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_OriginalSparePartCode;
            }
        }

        public string DataGridView_ColumnItem_OsaPurchaseOrderCode {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_OsaPurchaseOrderCode;
            }
        }

        public string DataGridView_ColumnItem_OverseasSuplierCode {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_OverseasSuplierCode;
            }
        }

        public string DataGridView_ColumnItem_PartsPurchasePlanQty {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_PartsPurchasePlanQty;
            }
        }

        public string DataGridView_ColumnItem_PartsSupplierCode {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_PartsSupplierCode;
            }
        }

        public string DataGridView_ColumnItem_PartsSupplierName {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_PartsSupplierName;
            }
        }

        public string DataGridView_ColumnItem_PirceType {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_PirceType;
            }
        }

        public string DataGridView_ColumnItem_Remark {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_Remark;
            }
        }

        public string DataGridView_ColumnItem_RequestedDeliveryTime {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_RequestedDeliveryTime;
            }
        }

        public string DataGridView_ColumnItem_SDCDCStock {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_SDCDCStock;
            }
        }

        public string DataGridView_ColumnItem_ShippingAmount {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_ShippingAmount;
            }
        }

        public string DataGridView_ColumnItem_ShippingDate {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_ShippingDate;
            }
        }

        public string DataGridView_ColumnItem_ShortSupReason {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_ShortSupReason;
            }
        }

        public string DataGridView_ColumnItem_StoporName {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_StoporName;
            }
        }

        public string DataGridView_ColumnItem_StopTime {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_StopTime;
            }
        }

        public string DataGridView_ColumnItem_SupplierBusinessCode {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_SupplierBusinessCode;
            }
        }

        public string DataGridView_ColumnItem_SupplierCode {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_SupplierCode;
            }
        }

        public string DataGridView_ColumnItem_Title_OriginalInBoundBill {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OriginalInBoundBill;
            }
        }

        public string DataGridView_ColumnItem_Title_OriginalInBoundBill_Code {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OriginalInBoundBill_Code;
            }
        }

        public string DataGridView_ColumnItem_Title_OriginalPurchaseOrder {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OriginalPurchaseOrder;
            }
        }

        public string DataGridView_ColumnItem_Title_OriginalPurchaseOrder_Code {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OriginalPurchaseOrder_Code;
            }
        }

        public string DataGridView_ColumnItem_Title_SettlementPrice {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SettlementPrice;
            }
        }

        public string DataGridView_ColumnItem_TradePrice {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_TradePrice;
            }
        }

        public string DataGridView_ColumnItem_UnitPrice {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_UnitPrice;
            }
        }

        public string DataGridView_ColumnItem_UnShippingAmount {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_UnShippingAmount;
            }
        }

        public string DataGridView_ColumnItem_UntFulfilledQty {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_UntFulfilledQty;
            }
        }

        public string DataGridView_ColumnItem_WarehouseCode {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_WarehouseCode;
            }
        }

        public string DataGridView_ColumnItem_WarehouseName {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_WarehouseName;
            }
        }

        public string DataGridView_Title_InOutboundDetailQuery {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_Title_InOutboundDetailQuery;
            }
        }

        public string DataGridView_Title_PartABC {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_Title_PartABC;
            }
        }

        public string DataGridView_ColumnItem_AbandonerName {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_AbandonerName;
            }
        }

        public string DataGridView_ColumnItem_AbandonTime {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_AbandonTime;
            }
        }

        public string DataGridView_ColumnItem_Approver {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_Approver;
            }
        }

        public string DataGridView_ColumnItem_ApproveTime {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_ApproveTime;
            }
        }

        public string DataGridView_ColumnItem_BachMunber {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_BachMunber;
            }
        }

        public string DataGridView_ColumnItem_BillBusinessType {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_BillBusinessType;
            }
        }

        public string DataGridView_ColumnItem_BillCode {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_BillCode;
            }
        }

        public string DataGridView_ColumnItem_BranchCode {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_BranchCode;
            }
        }

        public string DataGridView_ColumnItem_CostAmount {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_CostAmount;
            }
        }

        public string DataGridView_ColumnItem_CostAmountDifference {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_CostAmountDifference;
            }
        }

        public string DataGridView_ColumnItem_CostPrice {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_CostPrice;
            }
        }

        public string DataGridView_ColumnItem_FeeSum {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_FeeSum;
            }
        }

        public string DataGridView_ColumnItem_FinalApproveMemo {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_FinalApproveMemo;
            }
        }

        public string DataGridView_ColumnItem_FinalApprover {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_FinalApprover;
            }
        }

        public string DataGridView_ColumnItem_FinalApproveTime {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_FinalApproveTime;
            }
        }

        public string DataGridView_ColumnItem_FirPrintNumber {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_FirPrintNumber;
            }
        }

        public string DataGridView_ColumnItem_InitialApproveMemo {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_InitialApproveMemo;
            }
        }

        public string DataGridView_ColumnItem_InitialApprover {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_InitialApprover;
            }
        }

        public string DataGridView_ColumnItem_InitialApproveTime {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_InitialApproveTime;
            }
        }

        public string DataGridView_ColumnItem_InvoiceAmountDifference {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_InvoiceAmountDifference;
            }
        }

        public string DataGridView_ColumnItem_InvoiceTotalAmount {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_InvoiceTotalAmount;
            }
        }

        public string DataGridView_ColumnItem_IsUplodFile {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_IsUplodFile;
            }
        }

        public string DataGridView_ColumnItem_MaterialCostVariance {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_MaterialCostVariance;
            }
        }

        public string DataGridView_ColumnItem_OffsettedSettlementBillCode {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_OffsettedSettlementBillCode;
            }
        }

        public string DataGridView_ColumnItem_OnwayQty {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_OnwayQty;
            }
        }

        public string DataGridView_ColumnItem_OutboundFulfillment {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_OutboundFulfillment;
            }
        }

        public string DataGridView_ColumnItem_PackingCoefficient {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_PackingCoefficient;
            }
        }

        public string DataGridView_ColumnItem_PlanAmountSum {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_PlanAmountSum;
            }
        }

        public string DataGridView_ColumnItem_PrintNumber {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_PrintNumber;
            }
        }

        public string DataGridView_ColumnItem_QuantityToSettle {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_QuantityToSettle;
            }
        }

        public string DataGridView_ColumnItem_SAPPurReturnOrderCode {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_SAPPurReturnOrderCode;
            }
        }

        public string DataGridView_ColumnItem_SecPrintNumber {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_SecPrintNumber;
            }
        }

        public string DataGridView_ColumnItem_SettlementPath {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_SettlementPath;
            }
        }

        public string DataGridView_ColumnItem_Stock {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_Stock;
            }
        }

        public string DataGridView_ColumnItem_SumPlannedPrice {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_SumPlannedPrice;
            }
        }

        public string DataGridView_ColumnItem_SupplierShippingOrder_Code {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_SupplierShippingOrder_Code;
            }
        }

        public string DataGridView_ColumnItem_Tax {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_Tax;
            }
        }

        public string DataGridView_ColumnItem_TaxRate {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_TaxRate;
            }
        }

        public string DataGridView_ColumnItem_ThirdPrintNumber {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_ThirdPrintNumber;
            }
        }

        public string DataGridView_ColumnItem_TotalAmount {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_TotalAmount;
            }
        }

        public string DataGridView_ColumnItem_UsableStock {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_UsableStock;
            }
        }

        public string DataGridView_ColumnItem_WMZQty {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_WMZQty;
            }
        }

        public string DataGridView_Notification_PackingCoefficientError {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_Notification_PackingCoefficientError;
            }
        }

        public string DataGridView_Notification_RetuanQuantityError {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_Notification_RetuanQuantityError;
            }
        }

        public string DataGridView_Title_PartsPurReturnOrderWarehouseStockQuery {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_Title_PartsPurReturnOrderWarehouseStockQuery;
            }
        }

        public string DataGridView_ColumnItem_ArrivalDate {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_ArrivalDate;
            }
        }

        public string DataGridView_ColumnItem_CADCode {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_CADCode;
            }
        }

        public string DataGridView_ColumnItem_Feature {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_Feature;
            }
        }

        public string DataGridView_ColumnItem_LogisticArrivalDate {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_LogisticArrivalDate;
            }
        }

        public string DataGridView_ColumnItem_OutQty {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_OutQty;
            }
        }

        public string DataGridView_ColumnItem_PartsAttribution {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_PartsAttribution;
            }
        }

        public string DataGridView_ColumnItem_ReceivingAddress {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_ReceivingAddress;
            }
        }

        public string DataGridView_ColumnItem_ShelfLife {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_ShelfLife;
            }
        }

        public string DataGridView_ColumnItem_ShippingDate2 {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_ShippingDate2;
            }
        }

        public string DataGridView_ColumnItem_ShippingOrder_Code {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_ShippingOrder_Code;
            }
        }

        public string DataGridView_ColumnItem_ShippingTotalAmount {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_ShippingTotalAmount;
            }
        }

        public string DataGridView_ColumnItem_SpareOrderRemark {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_SpareOrderRemark;
            }
        }

        public string DataGridView_ColumnItem_Volume {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_Volume;
            }
        }

        public string DataGridView_ColumnItem_WaitShippingAmount {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_WaitShippingAmount;
            }
        }

        public string DataGridView_ColumnItem_Weight {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_Weight;
            }
        }

        public string DataManagementView_Notification_SureConfirm {
            get {
                return Resources.PartsPurchasingUIStrings.DataManagementView_Notification_SureConfirm;
            }
        }

        public string DataManagementView_Text_AddExportJobFail {
            get {
                return Resources.PartsPurchasingUIStrings.DataManagementView_Text_AddExportJobFail;
            }
        }

        public string DataManagementView_Text_AddExportJobSuccess {
            get {
                return Resources.PartsPurchasingUIStrings.DataManagementView_Text_AddExportJobSuccess;
            }
        }

        public string DataManagementView_Text_DataResendSuccess {
            get {
                return Resources.PartsPurchasingUIStrings.DataManagementView_Text_DataResendSuccess;
            }
        }

        public string DataManagementView_Text_DecomposeAll {
            get {
                return Resources.PartsPurchasingUIStrings.DataManagementView_Text_DecomposeAll;
            }
        }

        public string DataManagementView_Text_DecomposeFinish {
            get {
                return Resources.PartsPurchasingUIStrings.DataManagementView_Text_DecomposeFinish;
            }
        }

        public string DataManagementView_Text_MergeInvoiceError {
            get {
                return Resources.PartsPurchasingUIStrings.DataManagementView_Text_MergeInvoiceError;
            }
        }

        public string DataManagementView_Text_PartInformation {
            get {
                return Resources.PartsPurchasingUIStrings.DataManagementView_Text_PartInformation;
            }
        }

        public string DataManagementView_Text_PurchaseOrder {
            get {
                return Resources.PartsPurchasingUIStrings.DataManagementView_Text_PurchaseOrder;
            }
        }

        public string DataManagementView_Text_PurchaseOrderPrint {
            get {
                return Resources.PartsPurchasingUIStrings.DataManagementView_Text_PurchaseOrderPrint;
            }
        }

        public string DataManagementView_Text_StatusChanged {
            get {
                return Resources.PartsPurchasingUIStrings.DataManagementView_Text_StatusChanged;
            }
        }

        public string DataManagementView_Text_SupplierShippingPrint {
            get {
                return Resources.PartsPurchasingUIStrings.DataManagementView_Text_SupplierShippingPrint;
            }
        }

        public string DataManagementView_Text_SureResendData {
            get {
                return Resources.PartsPurchasingUIStrings.DataManagementView_Text_SureResendData;
            }
        }

        public string DataManagementView_Text_TerminateReasonInput {
            get {
                return Resources.PartsPurchasingUIStrings.DataManagementView_Text_TerminateReasonInput;
            }
        }

        public string DataManagementView_Text_TerminateSuccess {
            get {
                return Resources.PartsPurchasingUIStrings.DataManagementView_Text_TerminateSuccess;
            }
        }

        public string DataManagementView_Title_PartsPurchasePlanOrder {
            get {
                return Resources.PartsPurchasingUIStrings.DataManagementView_Title_PartsPurchasePlanOrder;
            }
        }

        public string DataManagementView_Title_PartsRequisitionSettleBillForIn {
            get {
                return Resources.PartsPurchasingUIStrings.DataManagementView_Title_PartsRequisitionSettleBillForIn;
            }
        }

        public string DataManagementView_Title_PartsRequisitionSettleBillForOut {
            get {
                return Resources.PartsPurchasingUIStrings.DataManagementView_Title_PartsRequisitionSettleBillForOut;
            }
        }

        public string DataManagementView_Title_PurchaseQuery {
            get {
                return Resources.PartsPurchasingUIStrings.DataManagementView_Title_PurchaseQuery;
            }
        }

        public string DataManagementView_Title_SettlementCostQuery {
            get {
                return Resources.PartsPurchasingUIStrings.DataManagementView_Title_SettlementCostQuery;
            }
        }

        public string QueryWindow_Title_PartsSupplierQuery {
            get {
                return Resources.PartsPurchasingUIStrings.QueryWindow_Title_PartsSupplierQuery;
            }
        }

        public string DataGridView_ColumnItem_SupplierShippingDetail_Number {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_SupplierShippingDetail_Number;
            }
        }

        public string DataGridView_ColumnItem_SupplierShippingDetail_PlanAmount {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_SupplierShippingDetail_PlanAmount;
            }
        }

        public string DataGridView_ColumnItem_SupplierShippingDetail_Remark {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_SupplierShippingDetail_Remark;
            }
        }

        public string DataGridView_ColumnItem_SupplierShippingDetail_SpareOrderRemark {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_SupplierShippingDetail_SpareOrderRemark;
            }
        }

        public string DataGridView_ColumnItem_SupplierShippingDetail_Volume {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_SupplierShippingDetail_Volume;
            }
        }

        public string DataGridView_ColumnItem_SupplierShippingDetail_WaitShippingAmount {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_SupplierShippingDetail_WaitShippingAmount;
            }
        }

        public string DataGridView_ColumnItem_SupplierShippingDetail_Weight {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_SupplierShippingDetail_Weight;
            }
        }

        public string Action_Title_OrderReplaceShipAll {
            get {
                return Resources.PartsPurchasingUIStrings.Action_Title_OrderReplaceShipAll;
            }
        }

        public string DataEditView_Validation_ThisShippingAmountValidation {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_ThisShippingAmountValidation;
            }
        }

        public string DataEditView_Validation_PurchasePlanTypeIsNull {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Validation_PurchasePlanTypeIsNull;
            }
        }

        public string DataManagementView_Title_PartsPurchaseOrderForShipping {
            get {
                return Resources.PartsPurchasingUIStrings.DataManagementView_Title_PartsPurchaseOrderForShipping;
            }
        }

        public string DataGridView_ColumnItem_Title_Checker {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_Checker;
            }
        }

        public string DataGridView_ColumnItem_Title_CheckMemo {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_CheckMemo;
            }
        }

        public string DataGridView_ColumnItem_Title_CheckTime {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_CheckTime;
            }
        }

        public string DataEditPanel_PartsPurchase_validation1 {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditPanel_PartsPurchase_validation1;
            }
        }

        public string UploadFile_Validation_Error1 {
            get {
                return Resources.PartsPurchasingUIStrings.UploadFile_Validation_Error1;
            }
        }

        public string UploadFile_Validation_Error2 {
            get {
                return Resources.PartsPurchasingUIStrings.UploadFile_Validation_Error2;
            }
        }

        public string Upload_Success {
            get {
                return Resources.PartsPurchasingUIStrings.Upload_Success;
            }
        }

        public string Upload_Tips {
            get {
                return Resources.PartsPurchasingUIStrings.Upload_Tips;
            }
        }

        public string Action_Title_A4Print {
            get {
                return Resources.PartsPurchasingUIStrings.Action_Title_A4Print;
            }
        }

        public string Action_Title_FirstApprove {
            get {
                return Resources.PartsPurchasingUIStrings.Action_Title_FirstApprove;
            }
        }

        public string DataEditView_PartsPurchaseOrder_Validation_PartsSupplier {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_PartsPurchaseOrder_Validation_PartsSupplier;
            }
        }

        public string DataEditVIew_PartsPurchaseRtnSettleDetail_Discount {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditVIew_PartsPurchaseRtnSettleDetail_Discount;
            }
        }

        public string DataEdit_Title_PartsOuterPurchaseBill_ReceivingWarehouseId {
            get {
                return Resources.PartsPurchasingUIStrings.DataEdit_Title_PartsOuterPurchaseBill_ReceivingWarehouseId;
            }
        }

        public string QueryPanel_QueryItem_IoStatus_IsPassing {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_IoStatus_IsPassing;
            }
        }

        public string QueryPanel_QueryItem_PartsPurchasePlan_CloseTime2 {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_CloseTime2;
            }
        }

        public string Upload_File_Export_Model {
            get {
                return Resources.PartsPurchasingUIStrings.Upload_File_Export_Model;
            }
        }

        public string Upload_File_Export_Model_NoPricce {
            get {
                return Resources.PartsPurchasingUIStrings.Upload_File_Export_Model_NoPricce;
            }
        }

        public string Upload_File_Tips {
            get {
                return Resources.PartsPurchasingUIStrings.Upload_File_Tips;
            }
        }

        public string DataEditView_Title_PartsPurchaseOrderPlan_IsPack {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrderPlan_IsPack;
            }
        }

        public string Action_Title_TmpPartsPurchasePlanOrder {
            get {
                return Resources.PartsPurchasingUIStrings.Action_Title_TmpPartsPurchasePlanOrder;
            }
        }

        public string DataManagementView_Title_TmpPartsPurchasePlanOrder {
            get {
                return Resources.PartsPurchasingUIStrings.DataManagementView_Title_TmpPartsPurchasePlanOrder;
            }
        }

        public string DataEditView_Title_Rejector {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_Rejector;
            }
        }

        public string DataEditView_Title_TemPurchasePlanOrder_FreightType {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_TemPurchasePlanOrder_FreightType;
            }
        }

        public string DataEditView_Title_TemPurchasePlanOrder_IsReplace {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_TemPurchasePlanOrder_IsReplace;
            }
        }

        public string DataGridView_ColumnItem_CheckTime {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_CheckTime;
            }
        }

        public string DataGridView_ColumnItem_CodeSource {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_CodeSource;
            }
        }

        public string DataGridView_ColumnItem_RejectTime {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_RejectTime;
            }
        }

        public string DataGridView_ColumnItem_Title_CheckerName {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_CheckerName;
            }
        }

        public string DataGridView_SupplierShippingOrder_OrderCompanyCode {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_SupplierShippingOrder_OrderCompanyCode;
            }
        }

        public string DataGridView_SupplierShippingOrder_OrderCompanyName {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_SupplierShippingOrder_OrderCompanyName;
            }
        }

        public string DataGridView_TemPurchasePlanOrder_IsTurnSale {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_TemPurchasePlanOrder_IsTurnSale;
            }
        }

        public string DataGridView_TemPurchasePlanOrder_PlanType {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_TemPurchasePlanOrder_PlanType;
            }
        }

        public string QueryPanel_QueryItem_PartsPurchasePlan_SubmitterName {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_SubmitterName;
            }
        }

        public string QueryPanel_QueryItem_PartsPurchasePlan_SubmitTime {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_SubmitTime;
            }
        }

        public string QueryPanel_QueryItem_Title_emPurchasePlanOrder_ReceCompanyCode {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_emPurchasePlanOrder_ReceCompanyCode;
            }
        }

        public string QueryPanel_QueryItem_Title_emPurchasePlanOrder_ReceCompanyName {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_emPurchasePlanOrder_ReceCompanyName;
            }
        }

        public string DetailPanel_Title_TemPartsPurchasePlanDetail {
            get {
                return Resources.PartsPurchasingUIStrings.DetailPanel_Title_TemPartsPurchasePlanDetail;
            }
        }

        public string String {
            get {
                return Resources.PartsPurchasingUIStrings.String;
            }
        }

        public string String1 {
            get {
                return Resources.PartsPurchasingUIStrings.String1;
            }
        }

        public string String10 {
            get {
                return Resources.PartsPurchasingUIStrings.String10;
            }
        }

        public string String11 {
            get {
                return Resources.PartsPurchasingUIStrings.String11;
            }
        }

        public string String12 {
            get {
                return Resources.PartsPurchasingUIStrings.String12;
            }
        }

        public string String2 {
            get {
                return Resources.PartsPurchasingUIStrings.String2;
            }
        }

        public string String3 {
            get {
                return Resources.PartsPurchasingUIStrings.String3;
            }
        }

        public string String4 {
            get {
                return Resources.PartsPurchasingUIStrings.String4;
            }
        }

        public string String5 {
            get {
                return Resources.PartsPurchasingUIStrings.String5;
            }
        }

        public string String6 {
            get {
                return Resources.PartsPurchasingUIStrings.String6;
            }
        }

        public string String7 {
            get {
                return Resources.PartsPurchasingUIStrings.String7;
            }
        }

        public string String8 {
            get {
                return Resources.PartsPurchasingUIStrings.String8;
            }
        }

        public string String9 {
            get {
                return Resources.PartsPurchasingUIStrings.String9;
            }
        }

        public string DataEditView_Title_TmpPartsPurchaseOrderPlanInfo {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_TmpPartsPurchaseOrderPlanInfo;
            }
        }

        public string DataEditView_Title_TmpPartsPurchaseOrderPlanInfoDetail {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_TmpPartsPurchaseOrderPlanInfoDetail;
            }
        }

        public string QueryPanel_QueryItem_PartsPurchasePlan_CustomerType {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_CustomerType;
            }
        }

        public string DataManagementView_Title_TmpPartsPurchasePlanOrderManage {
            get {
                return Resources.PartsPurchasingUIStrings.DataManagementView_Title_TmpPartsPurchasePlanOrderManage;
            }
        }

        public string Action_Title_Check {
            get {
                return Resources.PartsPurchasingUIStrings.Action_Title_Check;
            }
        }

        public string Action_Title_TmpPartsPurchasePlanOrderReplace {
            get {
                return Resources.PartsPurchasingUIStrings.Action_Title_TmpPartsPurchasePlanOrderReplace;
            }
        }

        public string DataEditView_Title_InitialAp {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_InitialAp;
            }
        }

        public string TemPurchasePlanOrders_Validation3 {
            get {
                return Resources.PartsPurchasingUIStrings.TemPurchasePlanOrders_Validation3;
            }
        }

        public string DataGridView_TemPurchaseOrder_ColumnItem_ApproveStatus {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_TemPurchaseOrder_ColumnItem_ApproveStatus;
            }
        }

        public string DataGridView_TemPurchaseOrder_ColumnItem_Code {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_TemPurchaseOrder_ColumnItem_Code;
            }
        }

        public string DataGridView_ColumnItem_TemPurchaseOrder_StoporName {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_TemPurchaseOrder_StoporName;
            }
        }

        public string DataGridView_ColumnItem_TemPurchaseOrder_StopTime {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_TemPurchaseOrder_StopTime;
            }
        }

        public string DataGridView_ColumnItem_Title_TemPurchaseOrder_Shipper {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_TemPurchaseOrder_Shipper;
            }
        }

        public string DataGridView_ColumnItem_Title_TemPurchaseOrder_ShippTime {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_TemPurchaseOrder_ShippTime;
            }
        }

        public string DataGridView_TemPurchaseOrder_ReceiveStatus {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_TemPurchaseOrder_ReceiveStatus;
            }
        }

        public string DataGridView_TemPurchaseOrder_TemPlanCode {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_TemPurchaseOrder_TemPlanCode;
            }
        }

        public string DataGridView_TitlePartsPurReturnOrder_ColumnItem_Buyer {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_TitlePartsPurReturnOrder_ColumnItem_Buyer;
            }
        }

        public string DataGridView_TemSupplierShippingOrder_Title_Code {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_TemSupplierShippingOrder_Title_Code;
            }
        }

        public string Action_Title_AppRoveSubmit {
            get {
                return Resources.PartsPurchasingUIStrings.Action_Title_AppRoveSubmit;
            }
        }

        public string Action_Title_Force {
            get {
                return Resources.PartsPurchasingUIStrings.Action_Title_Force;
            }
        }

        public string QueryPanel_QueryItem_IsPurchaseOrder {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_IsPurchaseOrder;
            }
        }

        public string QueryPanel_QueryItem_IsSaleCode {
            get {
                return Resources.PartsPurchasingUIStrings.QueryPanel_QueryItem_IsSaleCode;
            }
        }

        public string DataManagementView_Title_PartsPurchaseOrderTem {
            get {
                return Resources.PartsPurchasingUIStrings.DataManagementView_Title_PartsPurchaseOrderTem;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_PendingConfirmedAmount {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_PendingConfirmedAmount;
            }
        }

        public string DataEditView_Title_ForcedReason {
            get {
                return Resources.PartsPurchasingUIStrings.DataEditView_Title_ForcedReason;
            }
        }

        public string DataGridView_ColumnItem_Title_AdvancedAuditName {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_AdvancedAuditName;
            }
        }

        public string DataGridView_ColumnItem_Title_AdvancedAuditTime {
            get {
                return Resources.PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_AdvancedAuditTime;
            }
        }

}
}
