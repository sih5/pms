﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.Custom {
    public partial class PartsInboundCheckBillDetailForPartsPurchaseOrderView : IBaseView {
        private DataGridViewBase partsPurchaseOrderForPartsInboundCheckBillsSelectDataGridView;
        private DataGridViewBase partsInboundCheckBillDetailForSelectDataGridView;
        private readonly DcsDomainContext domainContext = new DcsDomainContext();

        private DataGridViewBase PartsPurchaseOrderForPartsInboundCheckBillsSelectDataGridView {
            get {
                if(this.partsPurchaseOrderForPartsInboundCheckBillsSelectDataGridView == null) {
                    this.partsPurchaseOrderForPartsInboundCheckBillsSelectDataGridView = DI.GetDataGridView("PartsPurchaseOrderForPartsInboundCheckBillsSelect");
                    this.partsPurchaseOrderForPartsInboundCheckBillsSelectDataGridView.SelectionChanged += this.PartsPurchaseOrderForPartsInboundCheckBillsSelectDataGridView_SelectionChanged;
                    this.partsPurchaseOrderForPartsInboundCheckBillsSelectDataGridView.DomainContext = domainContext;
                }
                return this.partsPurchaseOrderForPartsInboundCheckBillsSelectDataGridView;
            }
        }

        private DataGridViewBase PartsInboundCheckBillDetailForSelectDataGridView {
            get {
                if(this.partsInboundCheckBillDetailForSelectDataGridView == null) {
                    this.partsInboundCheckBillDetailForSelectDataGridView = DI.GetDataGridView("PartsInboundCheckBillDetailForSelect");
                    this.partsInboundCheckBillDetailForSelectDataGridView.DomainContext = domainContext;
                    this.partsInboundCheckBillDetailForSelectDataGridView.SetValue(DataContextProperty, this);
                }
                return this.partsInboundCheckBillDetailForSelectDataGridView;
            }
        }

        private void PartsPurchaseOrderForPartsInboundCheckBillsSelectDataGridView_SelectionChanged(object sender, System.EventArgs e) {
            var partsPurchaseOrder = this.PartsPurchaseOrderForPartsInboundCheckBillsSelectDataGridView.SelectedEntities.Cast<PartsPurchaseOrder>().FirstOrDefault();
            this.PartsInboundCheckBillDetailForSelectDataGridView.DataContext = partsPurchaseOrder;
        }

        public PartsInboundCheckBillDetailForPartsPurchaseOrderView() {
            InitializeComponent();
            this.Initialize();
        }

        private void Initialize() {
            this.PartsPurchaseOrderForPartsInboundCheckBillsSelectDataGridView.SetValue(MinHeightProperty, 80.0);
            this.LayoutRoot.Children.Add(this.PartsPurchaseOrderForPartsInboundCheckBillsSelectDataGridView);
            this.PartsInboundCheckBillDetailForSelectDataGridView.SetValue(MinHeightProperty, 80.0);
            this.PartsInboundCheckBillDetailForSelectDataGridView.SetValue(Grid.RowProperty, 1);
            this.LayoutRoot.Children.Add(this.PartsInboundCheckBillDetailForSelectDataGridView);
        }

        public void ExecuteQuery(FilterItem filterItem) {
            this.PartsPurchaseOrderForPartsInboundCheckBillsSelectDataGridView.FilterItem = filterItem;
            this.PartsPurchaseOrderForPartsInboundCheckBillsSelectDataGridView.ExecuteQuery();
        }

        public object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            return null;
        }

        public IEnumerable<Entity> SelectedEntities {
            get {
                return this.PartsInboundCheckBillDetailForSelectDataGridView.SelectedEntities;
            }
        }
    }
}
