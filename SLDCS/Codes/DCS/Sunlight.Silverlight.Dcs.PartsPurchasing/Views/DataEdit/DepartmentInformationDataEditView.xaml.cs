﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class DepartmentInformationDataEditView {
        private void CreateUI() {
            this.LayoutRoot.Children.Add(DI.GetDataEditPanel("DepartmentInformation"));
            this.isConent = true;
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetDepartmentInformationsQuery().Where(entity => entity.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_DepartmentInformation;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void OnEditSubmitting() {
            var departmentInformation = this.DataContext as DepartmentInformation;
            if(departmentInformation == null)
                return;
            //必填字段验证
            departmentInformation.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(departmentInformation.BranchName))
                departmentInformation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_DepartmentInformatioin_BranchNameIsNotNull, new[]{
                    "BranchName"
                }));
            if(string.IsNullOrEmpty(departmentInformation.Code))
                departmentInformation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_DepartmentInformatioin_CodeIsNotNull, new[]{
                    "Code"
                }));
            if(string.IsNullOrEmpty(departmentInformation.Name))
                departmentInformation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_DepartmentInformatioin_NameIsNotNull, new[]{
                    "Name"
                }));
            if(departmentInformation.HasValidationErrors) {
                UIHelper.ShowNotification(string.Join(Environment.NewLine, departmentInformation.ValidationErrors));
                return;
            }
            ((IEditableObject)departmentInformation).EndEdit();
            base.OnEditSubmitting();
        }

        public DepartmentInformationDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
    }
}
