﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class InternalAcquisitionBillDataEditView {
        private DataGridViewBase internalAcquisitionDetailForEditDataGridView;
        private readonly ObservableCollection<KeyValuePair> kvWarehouseNames = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvDepartmentCodes = new ObservableCollection<KeyValuePair>();
        private ObservableCollection<KeyValuePair> kvPartsPurchaseOrderType;
        private readonly CompositeFilterItem compositeFilterItem = new CompositeFilterItem();
        private QueryWindowBase dropDownueryWindow;
        private string strFileName;
        private ICommand exportFileCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出内部领用清单模板.xlsx";
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private QueryWindowBase DropDownQueryWindow {
            get {
                if(this.dropDownueryWindow == null) {
                    this.dropDownueryWindow = DI.GetQueryWindow("InternalAllocationBill");
                    this.dropDownueryWindow.SelectionDecided += this.DropDownueryWindow_SelectionDecided;
                }
                return this.dropDownueryWindow;
            }
        }

        private void DropDownueryWindow_SelectionDecided(object sender, EventArgs e) {
            var myQueryWindow = sender as QueryWindowBase;
            if(myQueryWindow == null || myQueryWindow.SelectedEntities == null)
                return;

            var selectedInternalAllocationBill = myQueryWindow.SelectedEntities.Cast<InternalAllocationBill>().FirstOrDefault();
            if(selectedInternalAllocationBill == null)
                return;

            var internalAcquisitionBill = this.DataContext as InternalAcquisitionBill;
            if(internalAcquisitionBill == null)
                return;
            if(internalAcquisitionBill.InternalAcquisitionDetails == null)
                return;
            this.DomainContext.Load(this.DomainContext.GetInternalAllocationDetailsQuery().Where(r => r.InternalAllocationBillId == selectedInternalAllocationBill.Id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    if(!loadOp.IsErrorHandled) {
                        loadOp.MarkErrorAsHandled();
                        return;
                    }
                var entities = loadOp.Entities;
                if(entities == null)
                    return;
                foreach(var entity in entities) {
                    internalAcquisitionBill.InternalAcquisitionDetails.Add(new InternalAcquisitionDetail {
                        SparePartId = entity.SparePartId,
                        SparePartCode = entity.SparePartCode,
                        SparePartName = entity.SparePartName,
                        MeasureUnit = entity.MeasureUnit,
                        UnitPrice = entity.UnitPrice,
                        Quantity = entity.Quantity,
                        Remark = entity.Remark,
                        SalesPrice = entity.SalesPrice
                    });
                }
            }, null);

            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private RadWindow queryWindow;

        private RadWindow QueryWindow {
            get {
                return this.queryWindow ?? (this.queryWindow = new RadWindow {
                    CanMove = true,
                    CanClose = true,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                    Header = PartsPurchasingUIStrings.DataEditView_Title_InternalAllocationBill,
                    Content = this.DropDownQueryWindow
                });
            }
        }

        private RadUpload uploader;
        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        var internalAcquisitionBill = this.DataContext as InternalAcquisitionBill;
                        if(internalAcquisitionBill == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.ImportInternalAcquisitionDetailAsync(internalAcquisitionBill.WarehouseId, e.HandlerData.CustomData["Path"].ToString());
                        this.excelServiceClient.ImportInternalAcquisitionDetailCompleted -= excelServiceClient_ImportInternalAcquisitionDetailCompleted;
                        this.excelServiceClient.ImportInternalAcquisitionDetailCompleted += excelServiceClient_ImportInternalAcquisitionDetailCompleted;
                    };
                }
                return this.uploader;
            }
        }

        private void excelServiceClient_ImportInternalAcquisitionDetailCompleted(object sender, ImportInternalAcquisitionDetailCompletedEventArgs e) {
            var internalAcquisitionBill = this.DataContext as InternalAcquisitionBill;
            if(internalAcquisitionBill == null)
                return;
            foreach(var detail in internalAcquisitionBill.InternalAcquisitionDetails) {
                internalAcquisitionBill.InternalAcquisitionDetails.Remove(detail);
            }
            if(e.rightData != null && e.rightData.Length > 0) {
                int serialNum = 1;
                foreach(var data in e.rightData) {
                    internalAcquisitionBill.InternalAcquisitionDetails.Add(new InternalAcquisitionDetail {
                        SerialNumber = serialNum++,
                        InternalAcquisitionBill = internalAcquisitionBill,
                        SparePartId = data.SparePartId,
                        SparePartCode = data.SparePartCode,
                        SparePartName = data.SparePartName,
                        UnitPrice = data.UnitPrice,
                        MeasureUnit = data.MeasureUnit,
                        Quantity = data.Quantity,
                        SalesPrice = data.SalesPrice,
                        Remark = data.Remark,
                        IsOrderable = data.IsOrderable
                    });
                }
                internalAcquisitionBill.TotalAmount = internalAcquisitionBill.InternalAcquisitionDetails.Sum(r => r.Quantity * r.UnitPrice);
            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowAlertMessage(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
        }

        public InternalAcquisitionBillDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void PartsPurchaseOrder_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            var internalAcquisitionBill = this.DataContext as InternalAcquisitionBill;
            if(internalAcquisitionBill == null)
                return;
            this.DomainContext.Load(this.DomainContext.GetPartsPurchaseOrderTypesByWarehouseIdQuery(internalAcquisitionBill.WarehouseId), loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowLoadError(loadOp);
                    return;
                }
                this.KvPartsPurchaseOrderTypes.Clear();
                foreach(var partsPurchaseOrderType in loadOp.Entities)
                    this.KvPartsPurchaseOrderTypes.Add(new KeyValuePair {
                        Key = partsPurchaseOrderType.Id,
                        Value = partsPurchaseOrderType.Name,
                    });
            }, null);
        }

        /// <summary>
        /// 文件上传成功后，调用该方法进行上传后的处理。不支持异步调用
        /// <param>上传文件名 <name>fileName</name> </param>
        /// </summary>
        protected Action<string> UploadFileSuccessedProcessing;

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //取当前登录用户的HashCode来标记上传文件的唯一性
                //this.UploadFileSuccessedProcessing.Invoke(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }

        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage(PartsPurchasingUIStrings.Upload_File_Tips);
                    e.Handled = true;
                }
            }
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }

        public ObservableCollection<KeyValuePair> KvWarehouseNames {
            get {
                return this.kvWarehouseNames;
            }
        }

        public ObservableCollection<KeyValuePair> KvDepartmentCodes {
            get {
                return this.kvDepartmentCodes;
            }
        }

        public ObservableCollection<KeyValuePair> KvPartsPurchaseOrderTypes {
            get {
                return this.kvPartsPurchaseOrderType ?? (this.kvPartsPurchaseOrderType = new ObservableCollection<KeyValuePair>());
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_InternalAcquisitionBill;
            }
        }

        private DataGridViewBase InternalAcquisitionDetailForEditDataGridView {
            get {
                if(this.internalAcquisitionDetailForEditDataGridView == null) {
                    this.internalAcquisitionDetailForEditDataGridView = DI.GetDataGridView("InternalAcquisitionDetailForEdit");
                    this.internalAcquisitionDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.internalAcquisitionDetailForEditDataGridView;
            }
        }

        private FilterItem filterItem;

        private FilterItem FilterItem {
            get {
                return this.filterItem ?? (this.filterItem = new FilterItem {
                    MemberName = "DepartmentId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo
                });
            }
        }

        private void CreateUI() {

            this.DomainContext.Load(this.DomainContext.GetWarehousesOrderByNameQuery().Where(item => item.Status == (int)DcsBaseDataStatus.有效 && item.Type != (int)DcsWarehouseType.虚拟库 && item.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.kvWarehouseNames.Clear();
                foreach(var warehouses in loadOp.Entities)
                    this.kvWarehouseNames.Add(new KeyValuePair {
                        Key = warehouses.Id,
                        Value = warehouses.Name,
                    });
            }, null);
            this.DomainContext.Load(this.DomainContext.GetDepartmentInformationsQuery().Where(item => item.Status == (int)DcsBaseDataStatus.有效 && item.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.kvDepartmentCodes.Clear();
                if(loadOp.Entities.Any(r => r.Name == "生产与物流部"))
                    foreach(var departmentInformation in loadOp.Entities.Where(r => r.Name == "生产与物流部"))
                        this.kvDepartmentCodes.Add(new KeyValuePair {
                            Key = departmentInformation.Id,
                            Value = departmentInformation.Name,
                        });
                foreach(var departmentInformation in loadOp.Entities.Where(r => r.Name != "生产与物流部"))
                    this.kvDepartmentCodes.Add(new KeyValuePair {
                        Key = departmentInformation.Id,
                        Value = departmentInformation.Name,
                    });
            }, null);
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsPurchasingUIStrings.DataEditView_Title_ChooseInternalAllocationBill,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                Command = new DelegateCommand(() => {
                    var internalAcquisitionBill = this.DataContext as InternalAcquisitionBill;
                    if(internalAcquisitionBill == null)
                        return;
                    if(internalAcquisitionBill.DepartmentId == default(int)) {
                        UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Notification_DepartmentIdIsNull);
                        return;
                    }
                    compositeFilterItem.Filters.Clear();
                    this.FilterItem.Value = internalAcquisitionBill.DepartmentId;
                    compositeFilterItem.Filters.Add(this.FilterItem);
                    this.DropDownQueryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
                    if(internalAcquisitionBill.DepartmentId != default(int) && internalAcquisitionBill.InternalAcquisitionDetails.Any()) {
                        DcsUtils.Confirm(PartsPurchasingUIStrings.DataEditView_Notification_CleanInternalAllocationBillDetail, () => {
                            foreach(var detail in internalAcquisitionBill.InternalAcquisitionDetails) {
                                internalAcquisitionBill.InternalAcquisitionDetails.Remove(detail);
                            }
                            this.QueryWindow.ShowDialog();

                        });
                    }
                    if(internalAcquisitionBill.DepartmentId != default(int) && !internalAcquisitionBill.InternalAcquisitionDetails.Any()) {
                        this.QueryWindow.ShowDialog();
                    }
                })
            });
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(InternalAcquisitionBill), "InternalAcquisitionDetails"), null, () => this.InternalAcquisitionDetailForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = "导入",
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                Command = new DelegateCommand(this.ShowFileDialog)
            });
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = "导出模板",
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                Command = this.ExportTemplateCommand
            });
            this.Root.Children.Add(detailDataEditView);
        }
        private void ShowFileDialog() {
            this.Uploader.ShowFileDialog();
        }
        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = "配件编号",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "配件名称",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn{
                                                Name = "零部件图号",
                                                IsRequired = false
                                            },
                                            new ImportTemplateColumn {
                                                Name = "数量",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "计量单位"
                                            },
                                            new ImportTemplateColumn {
                                                Name = "备注"
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetInternalAcquisitionBillWithInternalAcquisitionDetailsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();

                if(entity != null) {
                    this.SetObjectToEdit(entity);
                    entity.TotalAmount = entity.InternalAcquisitionDetails.Sum(r => r.Quantity * r.UnitPrice);

                    var internalAcquisitionBill = this.DataContext as InternalAcquisitionBill;
                    if(internalAcquisitionBill == null || internalAcquisitionBill.InternalAcquisitionDetails.Count <= 0)
                        return;
                    var dcsDomainContext = this.DomainContext as DcsDomainContext;
                    dcsDomainContext.Load(dcsDomainContext.GetSparePartByIdsQuery(internalAcquisitionBill.InternalAcquisitionDetails.Select(r => r.SparePartId).ToArray()), LoadBehavior.RefreshCurrent, loadOpr => {
                        if(loadOpr.HasError) {
                            if(!loadOpr.IsErrorHandled)
                                loadOpr.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOpr);
                            return;
                        }
                        var spareParts = loadOpr.Entities;
                        foreach(var internalAcquisitionDetail in internalAcquisitionBill.InternalAcquisitionDetails) {
                            var sparePart = spareParts.FirstOrDefault(r => r.Id == internalAcquisitionDetail.SparePartId);
                            if(sparePart != null) {
                                internalAcquisitionDetail.ReferenceCodeQuery = sparePart.ReferenceCode;
                            }
                        }
                    }, null);

                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            var internalAcquisitionBill = this.DataContext as InternalAcquisitionBill;
            if(internalAcquisitionBill == null || !this.InternalAcquisitionDetailForEditDataGridView.CommitEdit())
                return;
            internalAcquisitionBill.ValidationErrors.Clear();
            foreach(var internalAcquisitionDetail in internalAcquisitionBill.InternalAcquisitionDetails) {
                internalAcquisitionDetail.ValidationErrors.Clear();
            }
            if(!internalAcquisitionBill.InternalAcquisitionDetails.Any()) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_InternalAcquisitionDetailIsNull);
                return;
            }

            if(internalAcquisitionBill.InternalAcquisitionDetails.Count > 400) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_DetailCountMoreThan400);
                return;
            }

            if(internalAcquisitionBill.InternalAcquisitionDetails.Any(r => r.Quantity <= 0)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataGridView_Validation_InternalAcquisitionDetail_QuantityIsNotLessZero);
                return;
            }
            if(internalAcquisitionBill.InternalAcquisitionDetails.Any(r => r.UnitPrice <= 0)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataGridView_Validation_InternalAcquisitionDetail_UnitPriceIsNotLessZero);
                return;
            }

            if(internalAcquisitionBill.ValidationErrors.Any())
                return;
            ((IEditableObject)internalAcquisitionBill).EndEdit();
            if(internalAcquisitionBill.Can保存校验内部领入单)
                internalAcquisitionBill.保存校验内部领入单();
            base.OnEditSubmitting();
        }


    }
}
