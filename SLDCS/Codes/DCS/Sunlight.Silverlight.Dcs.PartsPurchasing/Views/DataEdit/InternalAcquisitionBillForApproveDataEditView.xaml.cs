﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class InternalAcquisitionBillForApproveDataEditView {
        private DataGridViewBase internalAcquisitionDetailForEditDataGridView;
        private readonly ObservableCollection<KeyValuePair> kvWarehouseNames = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvDepartmentCodes = new ObservableCollection<KeyValuePair>();
        public InternalAcquisitionBillForApproveDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }


        public ObservableCollection<KeyValuePair> KvWarehouseNames {
            get {
                return this.kvWarehouseNames;
            }
        }

        public ObservableCollection<KeyValuePair> KvDepartmentCodes {
            get {
                return this.kvDepartmentCodes;
            }
        }

        protected override string Title {
            get {
                return PartsPurchasingUIStrings.DataEditView_Title_Approve_InternalAcquisitionBill;
            }
        }

        private DataGridViewBase InternalAcquisitionDetailForEditDataGridView {
            get {
                if(this.internalAcquisitionDetailForEditDataGridView == null) {
                    this.internalAcquisitionDetailForEditDataGridView = DI.GetDataGridView("InternalAcquisitionDetailForApp");
                    this.internalAcquisitionDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.internalAcquisitionDetailForEditDataGridView;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetInternalAcquisitionBillWithInternalAcquisitionDetailsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                    entity.TotalAmount = entity.InternalAcquisitionDetails.Sum(r => r.Quantity * r.UnitPrice);

                    var internalAcquisitionBill = this.DataContext as InternalAcquisitionBill;
                    if(internalAcquisitionBill == null || internalAcquisitionBill.InternalAcquisitionDetails.Count <= 0)
                        return;
                    var dcsDomainContext = this.DomainContext as DcsDomainContext;
                    dcsDomainContext.Load(dcsDomainContext.GetSparePartByIdsQuery(internalAcquisitionBill.InternalAcquisitionDetails.Select(r => r.SparePartId).ToArray()), LoadBehavior.RefreshCurrent, loadOpr => {
                        if(loadOpr.HasError) {
                            if(!loadOpr.IsErrorHandled)
                                loadOpr.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOpr);
                            return;
                        }
                        var spareParts = loadOpr.Entities;
                        foreach(var internalAcquisitionDetail in internalAcquisitionBill.InternalAcquisitionDetails) {
                            var sparePart = spareParts.FirstOrDefault(r => r.Id == internalAcquisitionDetail.SparePartId);
                            if(sparePart != null) {
                                internalAcquisitionDetail.ReferenceCodeQuery = sparePart.ReferenceCode;
                            }
                        }
                    }, null);

                }
            }, null);
        }

        private void CreateUI() {
            InternalAcquisitionDetailForEditDataGridView.IsEnabled = true;
            this.DomainContext.Load(this.DomainContext.GetWarehousesOrderByNameQuery().Where(item => item.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.kvWarehouseNames.Clear();
                foreach(var warehouses in loadOp.Entities)
                    this.kvWarehouseNames.Add(new KeyValuePair {
                        Key = warehouses.Id,
                        Value = warehouses.Name,
                    });
            }, null);
            this.DomainContext.Load(this.DomainContext.GetDepartmentInformationsQuery().Where(item => item.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.kvDepartmentCodes.Clear();
                foreach(var departmentInformation in loadOp.Entities)
                    this.kvDepartmentCodes.Add(new KeyValuePair {
                        Key = departmentInformation.Id,
                        Value = departmentInformation.Name,
                    });
            }, null);
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(InternalAcquisitionBill), "InternalAcquisitionDetails"), null, () => this.InternalAcquisitionDetailForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);
        }

        protected override void Reset() {
            var internalAcquisitionBill = this.DataContext as InternalAcquisitionBill;
            if(internalAcquisitionBill != null && this.DomainContext.InternalAcquisitionBills.Contains(internalAcquisitionBill))
                this.DomainContext.InternalAcquisitionBills.Detach(internalAcquisitionBill);
        }

        protected override void OnEditSubmitting() {
            var internalAcquisitionBill = this.DataContext as InternalAcquisitionBill;
            if(internalAcquisitionBill == null || !this.InternalAcquisitionDetailForEditDataGridView.CommitEdit())
                return;
            if(internalAcquisitionBill.InternalAcquisitionDetails.Count > 400) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_DetailCountMoreThan400);
                return;
            }
            internalAcquisitionBill.ValidationErrors.Clear();
            foreach(var internalAcquisitionDetail in internalAcquisitionBill.InternalAcquisitionDetails) {
                internalAcquisitionDetail.ValidationErrors.Clear();
            }
            if(internalAcquisitionBill.InternalAcquisitionDetails.Any(r => r.Quantity <= 0)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataGridView_Validation_InternalAcquisitionDetail_QuantityIsNotLessZero);
                return;
            }

            if(internalAcquisitionBill.ValidationErrors.Any())
                return;
            try {
                ((IEditableObject)internalAcquisitionBill).EndEdit();
                if(internalAcquisitionBill.Can审批内部领入单)
                    internalAcquisitionBill.审批内部领入单();
            } catch(Exception ex) {
                UIHelper.ShowAlertMessage(ex.Message);
            }
            base.OnEditSubmitting();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }
    }
}
