﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class InternalAllocationBillDataEditView {
        private DataGridViewBase internalAllocationDetailForEditDataGridView;
        private int warehouseId; private string strFileName;
        private ICommand exportFileCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出内部领出清单模板.xlsx";
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public InternalAllocationBillDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += InternalAllocationBillDataEditView_DataContextChanged;
        }

        private RadUpload uploader;
        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        var internalAllocationBill = this.DataContext as InternalAllocationBill;
                        if(internalAllocationBill == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.ImportInternalAllocationDetailAsync(internalAllocationBill.WarehouseId, e.HandlerData.CustomData["Path"].ToString());
                        this.excelServiceClient.ImportInternalAllocationDetailCompleted -= excelServiceClient_ImportInternalAllocationDetailCompleted;
                        this.excelServiceClient.ImportInternalAllocationDetailCompleted += excelServiceClient_ImportInternalAllocationDetailCompleted;
                    };
                }
                return this.uploader;
            }
        }

        private void excelServiceClient_ImportInternalAllocationDetailCompleted(object sender, ImportInternalAllocationDetailCompletedEventArgs e) {
            var internalAllocationBill = this.DataContext as InternalAllocationBill;
            if(internalAllocationBill == null)
                return;
            foreach(var detail in internalAllocationBill.InternalAllocationDetails) {
                internalAllocationBill.InternalAllocationDetails.Remove(detail);
            }
            if(e.rightData != null && e.rightData.Length > 0) {
                int serialNum = 1;
                foreach(var data in e.rightData) {
                    internalAllocationBill.InternalAllocationDetails.Add(new InternalAllocationDetail {
                        SerialNumber = serialNum++,
                        InternalAllocationBill = internalAllocationBill,
                        SparePartId = data.SparePartId,
                        SparePartCode = data.SparePartCode,
                        SparePartName = data.SparePartName,
                        UnitPrice = data.UnitPrice,
                        MeasureUnit = data.MeasureUnit,
                        Quantity = data.Quantity,
                        SalesPrice = data.SalesPrice
                    });
                }
                internalAllocationBill.TotalAmount = internalAllocationBill.InternalAllocationDetails.Sum(r => r.Quantity * r.UnitPrice);
            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowAlertMessage(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
        }

        /// <summary>
        /// 文件上传成功后，调用该方法进行上传后的处理。不支持异步调用
        /// <param>上传文件名 <name>fileName</name> </param>
        /// </summary>
        protected Action<string> UploadFileSuccessedProcessing;

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //取当前登录用户的HashCode来标记上传文件的唯一性
                //this.UploadFileSuccessedProcessing.Invoke(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }

        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage(PartsPurchasingUIStrings.Upload_File_Tips);
                    e.Handled = true;
                }
            }
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }


        private void InternalAllocationBillDataEditView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var internalAllocationBill = this.DataContext as InternalAllocationBill;
            if(internalAllocationBill == null)
                return;
            internalAllocationBill.PropertyChanged -= internalAllocationBill_PropertyChanged;
            internalAllocationBill.PropertyChanged += internalAllocationBill_PropertyChanged;
        }

        private void internalAllocationBill_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var internalAllocationBill = this.DataContext as InternalAllocationBill;
            if(internalAllocationBill == null)
                return;
            switch(e.PropertyName) {
                case "WarehouseId":
                    if(internalAllocationBill.WarehouseId == warehouseId)
                        return;
                    if(internalAllocationBill.InternalAllocationDetails.Any())
                        DcsUtils.Confirm(PartsPurchasingUIStrings.DataEditPanel_Confirm_InternalAllocationBill_InternalAllocationDetails, () => {
                            //清空清单
                            this.InternalAllocationDetailForEditDataGridView.CommitEdit();
                            foreach(var item in internalAllocationBill.InternalAllocationDetails)
                                internalAllocationBill.InternalAllocationDetails.Remove(item);
                            warehouseId = internalAllocationBill.WarehouseId;
                        }, () => {
                            //回滚
                            internalAllocationBill.WarehouseId = warehouseId;
                        });
                    else
                        warehouseId = internalAllocationBill.WarehouseId;

                    break;
            }

        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_InternalAllocationBill;
            }
        }

        private DataGridViewBase InternalAllocationDetailForEditDataGridView {
            get {
                if(this.internalAllocationDetailForEditDataGridView == null) {
                    this.internalAllocationDetailForEditDataGridView = DI.GetDataGridView("InternalAllocationDetailForEdit");
                    this.internalAllocationDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.internalAllocationDetailForEditDataGridView;
            }
        }

        protected virtual void CreateUI() {
            var dataEditPanel = DI.GetDataEditPanel("InternalAllocationBill");
            this.Root.Children.Add(dataEditPanel);

            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailEditView = new DcsDetailDataEditView();
            detailEditView.Register(Utils.GetEntityLocalizedName(typeof(InternalAllocationBill), "InternalAllocationDetails"), null, this.InternalAllocationDetailForEditDataGridView);
            detailEditView.RegisterButton(new ButtonItem {
                Title = PartsPurchasingUIStrings.ActionPanel_Title_Import,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                Command = new DelegateCommand(() => ShowFileDialog())
            });
            detailEditView.RegisterButton(new ButtonItem {
                Title = PartsPurchasingUIStrings.Upload_File_Export_Model,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                Command = this.ExportTemplateCommand
            });
            detailEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailEditView);

            //附件
            this.DataEditPanels.SetValue(Grid.RowProperty, 5);
            this.DataEditPanels.SetValue(Grid.ColumnProperty, 0);
            this.DataEditPanels.Margin = new Thickness(0, 250, 0, 0);
            this.DataEditPanels.HorizontalAlignment = HorizontalAlignment.Left;
            this.DataEditPanels.VerticalAlignment = VerticalAlignment.Top;
            this.DataEditPanels.SetValue(Grid.ColumnSpanProperty, 2);
            this.Root.Children.Add(DataEditPanels);
        }

        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private void ShowFileDialog() {
            this.Uploader.ShowFileDialog();
        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name =PartsPurchasingUIStrings.DataEditView_Text_SparePartCode,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsPurchasingUIStrings.DataEditView_Text_SparePartName,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name =PartsPurchasingUIStrings.DataGridView_Column_Amount,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_MeasureUnit,
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_Remark,
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }

        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetInternalAllocationBillWithInternalAllocationDetailsQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();

                if(entity != null) {
                    warehouseId = entity.WarehouseId;
                    this.SetObjectToEdit(entity);
                    entity.TotalAmount = entity.InternalAllocationDetails.Sum(r => r.Quantity * r.UnitPrice);
                    DataEditPanels.FilePath = entity.Path;
                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            var internalAllocationBill = this.DataContext as InternalAllocationBill;
            if(internalAllocationBill == null || !this.InternalAllocationDetailForEditDataGridView.CommitEdit())
                return;
            if(internalAllocationBill.InternalAllocationDetails.Any(r => r.Quantity <= 0)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataGridView_Validation_InternalAllocationDetail_QuantityIsNotLessZero);
                return;
            }
            if(internalAllocationBill.InternalAllocationDetails.Count > 400) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_NotificationInternalAllocationDetailsMoreThanFourHundred);
                return;
            }
            if(null==internalAllocationBill.Type){
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Notification_TypeIsNull);
                return;
            }
            internalAllocationBill.Path = DataEditPanels.FilePath;
            ((IEditableObject)internalAllocationBill).EndEdit();
            if(internalAllocationBill.EntityState == EntityState.New) {
                if(internalAllocationBill.Can生成内部领出单)
                    internalAllocationBill.生成内部领出单();
            } else {
                if(internalAllocationBill.Can修改内部领出单)
                    internalAllocationBill.修改内部领出单();
            }
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private FileUploadForPartsOuterPurchaseDataEditPanel productDataEditPanels;
        public FileUploadForPartsOuterPurchaseDataEditPanel DataEditPanels {
            get {
                return this.productDataEditPanels ?? (this.productDataEditPanels = (FileUploadForPartsOuterPurchaseDataEditPanel)DI.GetDataEditPanel("FileUploadForPartsOuterPurchase"));
            }
        }
    }
}
