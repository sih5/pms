﻿
using System;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.Command;
using System.ComponentModel.DataAnnotations;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.DataEdit;
using System.Windows;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class InternalAllocationBillDetailDataEditView {
        private DataGridViewBase internalAllocationDetailForApproveDataGridView;
        public InternalAllocationBillDetailDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return "内部领出单详情";
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetInternalAllocationBillWithInternalAllocationDetailsQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                    entity.TotalAmount = entity.InternalAllocationDetails.Sum(r => r.Quantity * r.UnitPrice);
                    DataEditPanels.FilePath = entity.Path;
                }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private DataGridViewBase InternalAllocationDetailForApproveDataGridView {
            get {
                if(this.internalAllocationDetailForApproveDataGridView == null) {
                    this.internalAllocationDetailForApproveDataGridView = DI.GetDataGridView("InternalAllocationDetailForApprove");
                    this.internalAllocationDetailForApproveDataGridView.DomainContext = this.DomainContext;

                }
                return this.internalAllocationDetailForApproveDataGridView;
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("InternalAllocationBillDetail"));

            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailEditView = new DcsDetailDataEditView();
            detailEditView.Register(Utils.GetEntityLocalizedName(typeof(InternalAllocationBill), "InternalAllocationDetails"), null, this.InternalAllocationDetailForApproveDataGridView);
            detailEditView.UnregisterButton(detailEditView.InsertButton);
            detailEditView.UnregisterButton(detailEditView.DeleteButton);
            detailEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailEditView);
            //附件
            this.DataEditPanels.SetValue(Grid.RowProperty, 5);
            this.DataEditPanels.SetValue(Grid.ColumnProperty, 0);
            this.DataEditPanels.Margin = new Thickness(0, 250, 0, 0);
            this.DataEditPanels.HorizontalAlignment = HorizontalAlignment.Left;
            this.DataEditPanels.VerticalAlignment = VerticalAlignment.Top;
            this.DataEditPanels.SetValue(Grid.ColumnSpanProperty, 2);
            this.DataEditPanels.isHiddenButtons = true;
            this.Root.Children.Add(DataEditPanels);
        }

        private FileUploadForPartsOuterPurchaseDataEditPanel productDataEditPanels;
        public FileUploadForPartsOuterPurchaseDataEditPanel DataEditPanels {
            get {
                return this.productDataEditPanels ?? (this.productDataEditPanels = (FileUploadForPartsOuterPurchaseDataEditPanel)DI.GetDataEditPanel("FileUploadForPartsOuterPurchase"));
            }
        }
       
    }
}