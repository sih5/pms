﻿
using System;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.Command;
using System.ComponentModel.DataAnnotations;
using System.Windows;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class InternalAllocationBillForApproveDataEditView {
        private DataGridViewBase internalAllocationDetailForApproveDataGridView;
        private ButtonItem rejectBtn;
        public InternalAllocationBillForApproveDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return PartsPurchasingUIStrings.DataEditView_Title_Approve_InternalAllocationBill;
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetInternalAllocationBillWithInternalAllocationDetailsQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                    entity.TotalAmount = entity.InternalAllocationDetails.Sum(r => r.Quantity * r.UnitPrice);
                    DataEditPanels.FilePath = entity.Path;
                }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private DataGridViewBase InternalAllocationDetailForApproveDataGridView {
            get {
                if(this.internalAllocationDetailForApproveDataGridView == null) {
                    this.internalAllocationDetailForApproveDataGridView = DI.GetDataGridView("InternalAllocationDetailForApprove");
                    this.internalAllocationDetailForApproveDataGridView.DomainContext = this.DomainContext;

                }
                return this.internalAllocationDetailForApproveDataGridView;
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("InternalAllocationBillForApprove"));

            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailEditView = new DcsDetailDataEditView();
            detailEditView.Register(Utils.GetEntityLocalizedName(typeof(InternalAllocationBill), "InternalAllocationDetails"), null, this.InternalAllocationDetailForApproveDataGridView);
            detailEditView.UnregisterButton(detailEditView.InsertButton);
            detailEditView.UnregisterButton(detailEditView.DeleteButton);
            detailEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailEditView);
            this.rejectBtn = new ButtonItem
            {
                Command = new DelegateCommand(this.RejecrCurrentData),
                Title = PartsPurchasingUIStrings.DataEditView_Title_Reject,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative)
            };
            this.RegisterButton(rejectBtn);

            //附件
            this.DataEditPanels.SetValue(Grid.RowProperty, 5);
            this.DataEditPanels.SetValue(Grid.ColumnProperty, 0);
            this.DataEditPanels.Margin = new Thickness(0, 250, 0, 0);
            this.DataEditPanels.HorizontalAlignment = HorizontalAlignment.Left;
            this.DataEditPanels.VerticalAlignment = VerticalAlignment.Top;
            this.DataEditPanels.SetValue(Grid.ColumnSpanProperty, 2);
            this.DataEditPanels.isHiddenButtons = true;
            this.Root.Children.Add(DataEditPanels);
        }

        protected override void Reset() {
            var internalAllocationBill = this.DataContext as InternalAllocationBill;
            if(internalAllocationBill != null && this.DomainContext.InternalAllocationBills.Contains(internalAllocationBill))
                this.DomainContext.InternalAllocationBills.Detach(internalAllocationBill);
        }

        protected override void OnEditSubmitting() {
            var internalAllocationBill = this.DataContext as InternalAllocationBill;
            if(internalAllocationBill == null)
                return;
            if(internalAllocationBill.InternalAllocationDetails.Count > 400) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_NotificationInternalAllocationDetailsMoreThanFourHundred);
                return;
            }
            try {
                ((IEditableObject)internalAllocationBill).EndEdit();
                if(internalAllocationBill.Can审批内部领出单)
                    internalAllocationBill.审批内部领出单();
                ExecuteSerivcesMethod(PartsPurchasingUIStrings.DataEditView_Notification_OperateSuccess);
            } catch(Exception ex) {
                UIHelper.ShowAlertMessage(ex.Message);
            }
        }
        //驳回
        private void RejecrCurrentData()
        {
            var internalAllocationBill = this.DataContext as InternalAllocationBill;
            if (internalAllocationBill == null)
                return;
            ((IEditableObject)internalAllocationBill).EndEdit();
            try
            {
                if (internalAllocationBill.Can驳回内部领出单)
                    internalAllocationBill.驳回内部领出单();
                ExecuteSerivcesMethod(PartsPurchasingUIStrings.DataEditView_Notification_RejectSuccess);
            }
            catch (ValidationException ex)
            {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
        }
        private void ExecuteSerivcesMethod(string notifyMessage) {
            DomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    DomainContext.RejectChanges();
                    return;
                }
                this.NotifyEditSubmitted();
                this.OnCustomEditSubmitted();
                UIHelper.ShowNotification(notifyMessage);
            }, null);
        }
        public new event EventHandler EditSubmitted;
        private void NotifyEditSubmitted() {
            var handler = this.EditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }
        public void OnCustomEditSubmitted() {
            this.DataContext = null;
        }
        protected override bool OnRequestCanSubmit() {
            return true;
        }
        private FileUploadForPartsOuterPurchaseDataEditPanel productDataEditPanels;
        public FileUploadForPartsOuterPurchaseDataEditPanel DataEditPanels {
            get {
                return this.productDataEditPanels ?? (this.productDataEditPanels = (FileUploadForPartsOuterPurchaseDataEditPanel)DI.GetDataEditPanel("FileUploadForPartsOuterPurchase"));
            }
        }
    }
}