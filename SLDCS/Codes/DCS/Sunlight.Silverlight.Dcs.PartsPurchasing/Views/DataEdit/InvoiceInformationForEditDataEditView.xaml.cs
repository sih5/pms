﻿
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class InvoiceInformationForEditDataEditView {
        public InvoiceInformationForEditDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
        private DataGridViewBase invoiceInformationForSettleDataGridView;
        private ObservableCollection<InvoiceInformation> invoiceInformations = new ObservableCollection<InvoiceInformation>();

        public DataGridViewBase InvoiceInformationForSettleDataGridView {
            get {
                if(this.invoiceInformationForSettleDataGridView == null) {
                    this.invoiceInformationForSettleDataGridView = DI.GetDataGridView("InvoiceInformationForSettle");
                    this.invoiceInformationForSettleDataGridView.DomainContext = this.DomainContext;
                    this.invoiceInformationForSettleDataGridView.DataContext = this;
                }
                return this.invoiceInformationForSettleDataGridView;
            }
        }
        public ObservableCollection<InvoiceInformation> InvoiceInformations {
            get {
                if(this.invoiceInformations == null) {
                    this.invoiceInformations = new ObservableCollection<InvoiceInformation>();
                }
                return invoiceInformations;
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsPurchaseSettleBillsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
                //查询发票信息
                this.DomainContext.Load(this.DomainContext.GetInvoiceInformationsQuery().Where(e => e.SourceId == id && e.SourceType == (int)DcsInvoiceInformationSourceType.配件采购结算单), LoadBehavior.RefreshCurrent, load => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    int serialNumber = 1;
                    foreach(var data in load.Entities.ToArray()) {
                        data.SerialNumber = serialNumber;
                        this.InvoiceInformations.Add(data);
                        serialNumber++;
                    }
                }, null);
            }, null);
        }

        protected override void OnEditSubmitting() {
            if(!this.InvoiceInformationForSettleDataGridView.CommitEdit())
                return;

            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            if(partsPurchaseSettleBill == null)
                return;

            if(!this.InvoiceInformations.Any()) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseSettleBill_InvoiceInformationsIsNull);
                return;
            }
            foreach(var invoiceInformation in InvoiceInformations) {
                if(string.IsNullOrEmpty(invoiceInformation.InvoiceCode))
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceCodeIsNull, new[] {
                    "InvoiceCode"
                }));
                if(invoiceInformation.InvoiceCode.Length > 10)
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceCodeMoreThan10, new[] {
                    "InvoiceCode"
                }));
                if(string.IsNullOrEmpty(invoiceInformation.InvoiceNumber))
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceNumberIsNull, new[] {
                    "InvoiceNumber"
                }));
                if(invoiceInformation.InvoiceNumber.Length > 20)
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceNumberLengthMoreThan20, new[] {
                    "InvoiceNumber"
                }));
                if(!invoiceInformation.InvoiceAmount.HasValue) {
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceAmountIsNull, new[] {
                    "InvoiceAmount"
                }));
                } else if(invoiceInformation.InvoiceAmount <= 0) {
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceAmountMustGreaterZero, new[] {
                    "InvoiceAmount"
                }));
                }
                if(!invoiceInformation.TaxRate.HasValue)
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_TaxRateIsNull, new[] {
                    "TaxRate"
                }));
                if(!invoiceInformation.InvoiceTax.HasValue)
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceTaxIsNull, new[] {
                    "InvoiceTax"
                }));
                if(!invoiceInformation.InvoiceDate.HasValue)
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceDateIsNull, new[] {
                    "InvoiceDate"
                }));
             
            }
          
            if(InvoiceInformations.GroupBy(entity => entity.InvoiceNumber).Any(array => array.Count() > 1)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceCodeRepeat);
                return;
            }
            if(InvoiceInformations.Any(e => e.HasValidationErrors))
                return;
           
            ((IEditableObject)partsPurchaseSettleBill).EndEdit();
            foreach(var item in this.DomainContext.InvoiceInformations.Where(e => e.EntityState == EntityState.New).ToList()) {
                if(!InvoiceInformations.Contains(item))
                    this.DomainContext.InvoiceInformations.Remove(item);
            }
            foreach(var item in this.DomainContext.InvoiceInformations) {
                item.InvoiceAmount = Math.Round(item.InvoiceAmount.Value, 2);
                item.InvoiceTax = Math.Round(item.InvoiceTax.Value, 2);
            }

            //发票修改新增多条发票时设定发票的虚拟id。
            var virtualId = 1;
            foreach(var item in this.InvoiceInformations.Where(r => r.Id == default(int))) {
                if(!this.DomainContext.InvoiceInformations.Contains(item))
                    this.DomainContext.InvoiceInformations.Add(item);
                item.Id = virtualId;
                if(item.Can新增采购发票)
                    item.新增采购发票(partsPurchaseSettleBill.Id);
                virtualId++;
            }
            base.OnEditSubmitting();
        }


        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override string Title {
            get {
                return PartsPurchasingUIStrings.DataEditView_Title_InvoiceEdit_InvoiceInformation;
            }
        }

        protected override void Reset() {
            this.InvoiceInformations.Clear();
        }

        private void CreateUI() {
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsPurchasingUIStrings.DataGridView_Title_InvoiceRegister_PartsPurchaseSettleBill, null, () => this.InvoiceInformationForSettleDataGridView);//
            detailDataEditView.SetValue(Grid.RowProperty, 1);
            detailDataEditView.SetValue(Grid.ColumnSpanProperty, 8);
            this.Root.Children.Add(detailDataEditView);
        }
    }
}
