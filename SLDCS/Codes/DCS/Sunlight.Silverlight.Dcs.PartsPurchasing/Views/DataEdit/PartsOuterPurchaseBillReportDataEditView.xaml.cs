﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class PartsOuterPurchaseBillReportDataEditView {
        private DataGridViewBase partsOuterPurchaseChangeDetailForEditDataGridView;
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        public readonly ObservableCollection<KeyValuePair> KvReceivingWarehouse = new ObservableCollection<KeyValuePair>();
        private KeyValueManager keyValueManager;
        private Company company;
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategories = new ObservableCollection<KeyValuePair>();
        private FileUploadForPartsOuterPurchaseReportDataEditPanel productDataEditPanels;

        public FileUploadForPartsOuterPurchaseReportDataEditPanel DataEditPanels
        {
            get
            {
                return this.productDataEditPanels ?? (this.productDataEditPanels = (FileUploadForPartsOuterPurchaseReportDataEditPanel)DI.GetDataEditPanel("FileUploadForPartsOuterPurchaseReport"));
            }
        }

        public PartsOuterPurchaseBillReportDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
            this.Loaded += PartsOuterPurchaseBillReportDataEditView_Loaded;
            this.DataContextChanged += PartsOuterPurchaseBillReportDataEditView_DataContextChanged;
        }

        private void PartsOuterPurchaseBillReportDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsOuterPurchaseChange = this.DataContext as PartsOuterPurchaseChange;
            if(partsOuterPurchaseChange == null)
                return;
            partsOuterPurchaseChange.PropertyChanged += PartsOuterPurchaseChangePropertyChanged;
            partsOuterPurchaseChange.PartsOuterPurchaselists.EntityRemoved -= PartsOuterPurchaselists_EntityRemoved;
            partsOuterPurchaseChange.PartsOuterPurchaselists.EntityRemoved += PartsOuterPurchaselists_EntityRemoved;
        }

        private void PartsOuterPurchaselists_EntityRemoved(object sender, EntityCollectionChangedEventArgs<PartsOuterPurchaselist> e) {
            var partsOuterPurchaseChange = this.DataContext as PartsOuterPurchaseChange;
            if(partsOuterPurchaseChange == null)
                return;
            partsOuterPurchaseChange.Amount = partsOuterPurchaseChange.PartsOuterPurchaselists.Sum(entity => entity.OuterPurchasePrice * entity.Quantity);
        }

        private readonly string[] kvNames = {
            "PartsOuterPurchase_OuterPurchaseComment"
        };

        public DataGridViewBase PartsOuterPurchaseChangeDetailForEditDataGridView {
            get {
                if(this.partsOuterPurchaseChangeDetailForEditDataGridView == null) {
                    this.partsOuterPurchaseChangeDetailForEditDataGridView = DI.GetDataGridView("PartsOuterPurchaseChangeDetailForEdit");
                    this.partsOuterPurchaseChangeDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsOuterPurchaseChangeDetailForEditDataGridView;
            }
        }


        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsOuterPurchaseChange;
            }
        }

        private void PartsOuterPurchaseBillReportDataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.DomainContext.Load(this.DomainContext.GetBranchesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvBranches.Clear();
                foreach(var branch in loadOp.Entities)
                    this.KvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name,
                        UserObject = branch
                    });
            }, null);
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvPartsSalesCategories.Clear();
                foreach(var item in loadOp.Entities)
                    this.KvPartsSalesCategories.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.Name,
                        UserObject = item
                    });
                this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            }, null);
            var partsOuterPurchaseChange = this.DataContext as PartsOuterPurchaseChange;
            if(partsOuterPurchaseChange == null)
                return;
            //注释无用代码
            //this.DomainContext.Load(this.DomainContext.GetSubChannelRelationsQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && partsOuterPurchaseChange.CustomerCompanyId == r.SubChanelId), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    var subChannelRelation = loadOp.Entities.SingleOrDefault();
            //}, null);
            this.comboBoxBranches.SelectionChanged += comboBoxBranches_SelectionChanged;
        }

        private void comboBoxBranches_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            var partsOuterPurchaseChange = this.DataContext as PartsOuterPurchaseChange;
            if(partsOuterPurchaseChange == null)
                return;
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.BranchId == partsOuterPurchaseChange.BranchId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvPartsSalesCategories.Clear();
                foreach(var item in loadOp.Entities)
                    this.KvPartsSalesCategories.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.Name,
                        UserObject = item
                    });
                this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            }, null);
        }

        private int originalPartsSalesCategoryId = 0;

        private void PartsOuterPurchaseChangePropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsOuterPurchaseChange = this.DataContext as PartsOuterPurchaseChange;
            if(partsOuterPurchaseChange == null)
                return;
            switch(e.PropertyName) {
                case "BranchId":
                    //清单存在数据 清空
                    if(partsOuterPurchaseChange.PartsOuterPurchaselists.Any())
                        foreach(var detail in partsOuterPurchaseChange.PartsOuterPurchaselists.ToArray()) {
                            partsOuterPurchaseChange.PartsOuterPurchaselists.Remove(detail);
                        }
                    break;
                case "PartsSalesCategoryrId":
                    if(partsOuterPurchaseChange.PartsOuterPurchaselists.Any()) {
                        if(partsOuterPurchaseChange.PartsSalesCategoryrId != originalPartsSalesCategoryId)
                            DcsUtils.Confirm(PartsPurchasingUIStrings.DataEditView_Validation_ClearDetailsOnCategoryChanged, () => {
                                foreach(var detail in partsOuterPurchaseChange.PartsOuterPurchaselists.ToArray()) {
                                    partsOuterPurchaseChange.PartsOuterPurchaselists.Remove(detail);
                                }
                                originalPartsSalesCategoryId = partsOuterPurchaseChange.PartsSalesCategoryrId;
                                GetReceivingWarehouseSource(partsOuterPurchaseChange.PartsSalesCategoryrId, null);
                            }, () => {
                                partsOuterPurchaseChange.PartsSalesCategoryrId = originalPartsSalesCategoryId;
                            });
                    } else {
                        originalPartsSalesCategoryId = partsOuterPurchaseChange.PartsSalesCategoryrId;
                        GetReceivingWarehouseSource(partsOuterPurchaseChange.PartsSalesCategoryrId, null);
                    }
                    break;
            }
        }

        private void GetReceivingWarehouseSource(int partsSalesCategoryrId, Action callBack) {
            if(company.Type != (int)DcsCompanyType.代理库 && company.Type != (int)DcsCompanyType.服务站兼代理库)
                return;
            this.DomainContext.Load(this.DomainContext.GetWarehousesByPartsSalesCategoryIdAndOwnerCompanyIdQuery(partsSalesCategoryrId, BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.cbReceivingWarehouse.ItemsSource = loadOp.Entities;
                if(callBack != null)
                    callBack();
            }, null);
        }
        protected override bool OnRequestCanSubmit()
        {
            this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            return true;
        }

        public ObservableCollection<KeyValuePair> KvBranches {
            get {
                return this.kvBranches;
            }
        }

        public ObservableCollection<KeyValuePair> KvPartsSalesCategories {
            get {
                return this.kvPartsSalesCategories;
            }
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void CreateUI() {
            this.KeyValueManager.LoadData();
             //附件
            this.DataEditPanels.SetValue(Grid.RowProperty, 5);
            this.DataEditPanels.SetValue(Grid.ColumnProperty, 0);
            this.DataEditPanels.HorizontalAlignment = HorizontalAlignment.Right;
            this.DataEditPanels.VerticalAlignment = VerticalAlignment.Bottom;
            this.DataEditPanels.SetValue(Grid.ColumnSpanProperty, 4);
            this.LeftRoot.Children.Add(DataEditPanels);

            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsPurchasingUIStrings.DataEditView_Title_PartsOuterPurchaseChange_PartsOuterPurchaselist, null, () => this.PartsOuterPurchaseChangeDetailForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);
            this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(r => r.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                company = loadOp.Entities.SingleOrDefault();
                if(company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库)
                    this.receivingWarehouseGrid.Visibility = Visibility.Visible;
                //只在登录企业为服务站或服务站兼代理库时收货仓库控件可见。根据 销售组织.配件销售类型id=当前选中品牌.id,and 销售组织.隶属企业id=当前登陆企业id 关联销售组织与仓库关系 根据配件仓库Id获取仓库
            }, null);
        }

        //
        //TODO:导入暂不实现
        private void ImportDataInternal() {

        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsOuterPurchaseChangeWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    DataEditPanels.FilePath = entity.Path;
                    if(company.Type != (int)DcsCompanyType.代理库 && company.Type != (int)DcsCompanyType.服务站兼代理库)
                        this.SetObjectToEdit(entity);
                    else
                        GetReceivingWarehouseSource(entity.PartsSalesCategoryrId, () => this.SetObjectToEdit(entity));

            }, null);
        }

        protected override void OnEditSubmitting() {
            var partsOuterPurchaseChange = this.DataContext as PartsOuterPurchaseChange;
            if(partsOuterPurchaseChange == null || !this.PartsOuterPurchaseChangeDetailForEditDataGridView.CommitEdit())
                return;
            partsOuterPurchaseChange.ValidationErrors.Clear();
            foreach(var item in partsOuterPurchaseChange.PartsOuterPurchaselists) {
                item.ValidationErrors.Clear();
            }
            //附件
            partsOuterPurchaseChange.Path = DataEditPanels.FilePath;

            if(!string.IsNullOrEmpty(partsOuterPurchaseChange.Remark) && partsOuterPurchaseChange.Remark.Length > 100) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_PartsOuterPurchaseChange_Validation_OutLength);
                return;
            }
            if(!partsOuterPurchaseChange.PartsOuterPurchaselists.Any()) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_PartsOuterPurchaselistsIsNull);
                return;
            }
            foreach(var relation in partsOuterPurchaseChange.PartsOuterPurchaselists.Where(e => e.Quantity <= 0)) {
                UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Validation_PartsOuterPurchaseChange_Quantity, relation.PartsCode));
                return;
            }
            foreach(var relation in partsOuterPurchaseChange.PartsOuterPurchaselists.Where(e => e.OuterPurchasePrice <= 0)) {
                UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Validation_PartsOuterPurchaseChange_OuterPurchasePrice, relation.PartsCode));
                return;
            }
            //如果总金额大于10000，必须上传附件
            if (partsOuterPurchaseChange.Amount > 10000m && string.IsNullOrEmpty(partsOuterPurchaseChange.Path) ) { 
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_MustUploadFiles);
                return;
            }
            if (company.Type == (int)DcsCompanyType.服务站兼代理库 && (partsOuterPurchaseChange.ReceivingWarehouseId == null || partsOuterPurchaseChange.ReceivingWarehouseId == default(int))) { 
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_ReceivingWarehouseIsNull);
                return;
            }
            ((IEditableObject)partsOuterPurchaseChange).EndEdit();
            base.OnEditSubmitting();
        }

        public object KvOuterPurchaseComment {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
    }
}
