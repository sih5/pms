﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class PartsOuterPurchaseBillReportDetailDataEditView {
        private DataGridViewBase partsOuterPurchaseChangeDetailForEditDataGridView;
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        public readonly ObservableCollection<KeyValuePair> KvReceivingWarehouse = new ObservableCollection<KeyValuePair>();
        private KeyValueManager keyValueManager;
        private Company company;
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategories = new ObservableCollection<KeyValuePair>();
        private FileUploadForPartsOuterPurchaseReportDataEditPanel productDataEditPanels;

        public FileUploadForPartsOuterPurchaseReportDataEditPanel DataEditPanels
        {
            get
            {
                return this.productDataEditPanels ?? (this.productDataEditPanels = (FileUploadForPartsOuterPurchaseReportDataEditPanel)DI.GetDataEditPanel("FileUploadForPartsOuterPurchaseReport"));
            }
        }

        public PartsOuterPurchaseBillReportDetailDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
        }

        private readonly string[] kvNames = {
            "PartsOuterPurchase_OuterPurchaseComment"
        };

        public DataGridViewBase PartsOuterPurchaseChangeDetailForEditDataGridView {
            get {
                if(this.partsOuterPurchaseChangeDetailForEditDataGridView == null) {
                    this.partsOuterPurchaseChangeDetailForEditDataGridView = DI.GetDataGridView("PartsOuterPurchaseChangeDetailForEdit");
                    this.partsOuterPurchaseChangeDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsOuterPurchaseChangeDetailForEditDataGridView;
            }
        }


        protected override string Title {
            get {
                return PartsPurchasingUIStrings.DataEditView_Title_PartsOuterPurchaseBillDetails;
            }
        }

        private void PartsOuterPurchaseBillReportDataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.DomainContext.Load(this.DomainContext.GetBranchesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvBranches.Clear();
                foreach(var branch in loadOp.Entities)
                    this.KvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name,
                        UserObject = branch
                    });
            }, null);
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvPartsSalesCategories.Clear();
                foreach(var item in loadOp.Entities)
                    this.KvPartsSalesCategories.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.Name,
                        UserObject = item
                    });
            }, null);
            var partsOuterPurchaseChange = this.DataContext as PartsOuterPurchaseChange;
            if(partsOuterPurchaseChange == null)
                return;

        }


        public ObservableCollection<KeyValuePair> KvBranches {
            get {
                return this.kvBranches;
            }
        }

        public ObservableCollection<KeyValuePair> KvPartsSalesCategories {
            get {
                return this.kvPartsSalesCategories;
            }
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void CreateUI() {
            this.KeyValueManager.LoadData();
             //附件
            this.DataEditPanels.SetValue(Grid.RowProperty, 5);
            this.DataEditPanels.SetValue(Grid.ColumnProperty, 0);
            this.DataEditPanels.HorizontalAlignment = HorizontalAlignment.Right;
            this.DataEditPanels.VerticalAlignment = VerticalAlignment.Bottom;
            this.DataEditPanels.SetValue(Grid.ColumnSpanProperty, 4);
            this.DataEditPanels.isHiddenButtons = true;
            this.LeftRoot.Children.Add(DataEditPanels);

            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsPurchasingUIStrings.DataEditView_Title_PartsOuterPurchaseChange_PartsOuterPurchaselist, null, () => this.PartsOuterPurchaseChangeDetailForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.IsEnabled = false;
            this.Root.Children.Add(detailDataEditView);
            this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(r => r.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                company = loadOp.Entities.SingleOrDefault();
                if(company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库)
                    this.receivingWarehouseGrid.Visibility = Visibility.Visible;
            }, null);
        }


        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsOuterPurchaseChangeWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    DataEditPanels.FilePath = entity.Path;
                    this.SetObjectToEdit(entity);

            }, null);
        }

        public object KvOuterPurchaseComment {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
    }
}
