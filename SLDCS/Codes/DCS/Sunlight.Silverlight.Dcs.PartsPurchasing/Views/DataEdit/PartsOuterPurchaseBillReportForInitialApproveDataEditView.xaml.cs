﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class PartsOuterPurchaseBillReportForInitialApproveDataEditView {
        private DataGridViewBase partsOuterPurchaselistForApproveDataGridView;
        private KeyValueManager keyValueManager;
        private Company currentCompany;
        private FileUploadForPartsOuterPurchaseDataEditPanel productDataEditPanels;

        protected FileUploadForPartsOuterPurchaseDataEditPanel DataEditPanels
        {
            get
            {
                return this.productDataEditPanels ?? (this.productDataEditPanels = (FileUploadForPartsOuterPurchaseDataEditPanel)DI.GetDataEditPanel("FileUploadForPartsOuterPurchase"));
            }
        }
        public PartsOuterPurchaseBillReportForInitialApproveDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += PartsOuterPurchaseBillReportForInitialApproveDataEditView_DataContextChanged;
        }

        private void PartsOuterPurchaseBillReportForInitialApproveDataEditView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetCompaniesQuery().Where(entity => entity.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                currentCompany = loadOp.Entities.FirstOrDefault();
            }, null);
        }
        private readonly string[] kvNames = {
            "PartsOuterPurchase_OuterPurchaseComment"
        };


        protected override string Title {
            get {
                return PartsPurchasingUIStrings.DataEditView_Title_InitialApproveComment;
            }

        }
        public DataGridViewBase PartsOuterPurchaselistForApproveDataGridView {
            get {
                if(this.partsOuterPurchaselistForApproveDataGridView == null) {
                    this.partsOuterPurchaselistForApproveDataGridView = DI.GetDataGridView("PartsOuterPurchaselistForApprove");
                    this.partsOuterPurchaselistForApproveDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsOuterPurchaselistForApproveDataGridView;
            }
        }
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private void CreateUI() {
            this.KeyValueManager.LoadData();
            //附件
            this.DataEditPanels.SetValue(Grid.RowProperty, 7);
            this.DataEditPanels.SetValue(Grid.ColumnProperty, 0);
            this.DataEditPanels.isHiddenButtons = true;
            this.DataEditPanels.HorizontalAlignment = HorizontalAlignment.Right;
            this.DataEditPanels.VerticalAlignment = VerticalAlignment.Bottom;
            this.DataEditPanels.SetValue(Grid.ColumnSpanProperty, 3);

            this.Root.Children.Add(DataEditPanels);
            //this.VerticalLineRoot.Children.Add(this.CreateVerticalLine(3));
            //var dataEditPanel = DI.GetDataEditPanel("PartsOuterPurchaseBillReportForApprove");
            //this.LayoutRoot.Children.Add(dataEditPanel);
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsPurchasingUIStrings.DataEditView_Title_PartsOuterPurchaseChange_PartsOuterPurchaselist, null, () => this.PartsOuterPurchaselistForApproveDataGridView);
            detailDataEditView.SetValue(Grid.RowProperty, 1);
            detailDataEditView.SetValue(Grid.ColumnProperty, 0);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            this.LayoutRoot.Children.Add(detailDataEditView);

            this.RegisterButton(new ButtonItem { 
                Command = new DelegateCommand(this.RejectCurrentData), 
                Title = PartsPurchasingUIStrings.DataEditView_Title_Reject, 
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative)
            }, true);
        }

        private void RejectCurrentData() {
            var partsOuterPurchaseChange = this.DataContext as PartsOuterPurchaseChange;
            if (partsOuterPurchaseChange == null)
                return;
            partsOuterPurchaseChange.ApprovalId = 0;
            if (string.IsNullOrEmpty(partsOuterPurchaseChange.ApproveComment)) { 
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_ApproveCommentIsNull);
                return;
            }
            ((IEditableObject)partsOuterPurchaseChange).EndEdit();
            try {
                if(partsOuterPurchaseChange.Status == (int)DcsPartsOuterPurchaseChangeStatus.确认通过 && partsOuterPurchaseChange.Can初审配件外采申请单)
                    partsOuterPurchaseChange.初审配件外采申请单();
                if(partsOuterPurchaseChange.Status == (int)DcsPartsOuterPurchaseChangeStatus.提交 && partsOuterPurchaseChange.Can确认配件外采申请单)
                    partsOuterPurchaseChange.确认配件外采申请单();
            } catch (ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsOuterPurchaseChangeWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    entity.ApprovalId = 1;
                    entity.ApproveComment = null;
                    DataEditPanels.FilePath = entity.Path;
                    foreach(var detail in entity.PartsOuterPurchaselists) {
                        this.DomainContext.Load(this.DomainContext.查询经销商配件库存Query().Where(r => r.DealerId == entity.CustomerCompanyId &&
                            r.SalesCategoryId == entity.PartsSalesCategoryrId && r.SparePartId == detail.PartsId && r.SubDealerId == -1), LoadBehavior.RefreshCurrent, loadOp1 => {
                                if(loadOp1.HasError) {
                                    if(!loadOp1.IsErrorHandled)
                                        loadOp1.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOp1);
                                    return;
                                }
                                var dealerPartsStock = loadOp1.Entities.FirstOrDefault();
                                if(dealerPartsStock != null)
                                    detail.BranchQuantity = dealerPartsStock.Quantity;
                                else
                                    detail.BranchQuantity = default(int);
                            }, null);
                    }
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
        protected override void OnEditSubmitting() {
            var partsOuterPurchaseChange = this.DataContext as PartsOuterPurchaseChange;
            if(partsOuterPurchaseChange == null)
                return;
            partsOuterPurchaseChange.ValidationErrors.Clear();
            partsOuterPurchaseChange.ApprovalId = 1;
            if(partsOuterPurchaseChange.ApprovalId == 0 && string.IsNullOrEmpty(partsOuterPurchaseChange.ApproveComment))
                partsOuterPurchaseChange.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsOuterPurchaseChange_ApproveCommentIsNull, new[] {
                    "ApproveComment"
                }));
            ((IEditableObject)partsOuterPurchaseChange).EndEdit();
            if(partsOuterPurchaseChange.HasValidationErrors)
                return;
             try {
                 if(partsOuterPurchaseChange.Status == (int)DcsPartsOuterPurchaseChangeStatus.确认通过 && partsOuterPurchaseChange.Can初审配件外采申请单)
                     partsOuterPurchaseChange.初审配件外采申请单();
                 if(partsOuterPurchaseChange.Status == (int)DcsPartsOuterPurchaseChangeStatus.提交 && partsOuterPurchaseChange.Can确认配件外采申请单)
                     partsOuterPurchaseChange.确认配件外采申请单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            
            base.OnEditSubmitting();
        }

        public object KvOuterPurchaseComment {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
    }
}
