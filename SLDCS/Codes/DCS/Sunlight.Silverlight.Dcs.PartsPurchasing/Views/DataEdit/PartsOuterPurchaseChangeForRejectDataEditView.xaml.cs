﻿
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class PartsOuterPurchaseChangeForRejectDataEditView {
        public PartsOuterPurchaseChangeForRejectDataEditView() {
            InitializeComponent();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsOuterPurchaseChangeByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            var partsOuterPurchaseChange = this.DataContext as PartsOuterPurchaseChange;
            if(partsOuterPurchaseChange == null)
                return;
            partsOuterPurchaseChange.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(partsOuterPurchaseChange.RejectComment))
                partsOuterPurchaseChange.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_RejectReasonIsNull, new[] {
                    "RejectComment"
                }));

            if(partsOuterPurchaseChange.HasValidationErrors)
                return;

            try {
                if(partsOuterPurchaseChange.Can驳回配件外采申请单)
                    partsOuterPurchaseChange.驳回配件外采申请单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        protected override string Title {
            get {
                return PartsPurchasingUIStrings.DataEditView_Title_RejectPartsOuterPurchaseChange;
            }
        }
    }
}
