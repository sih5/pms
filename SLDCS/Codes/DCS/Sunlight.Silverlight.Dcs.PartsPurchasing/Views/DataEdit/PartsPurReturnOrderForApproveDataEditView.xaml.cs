﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class PartsPurReturnOrderForApproveDataEditView {
        private DataGridViewBase partsPurReturnOrderDetailDataGridView;

        private DataGridViewBase PartsPurReturnOrderDetailDataGridView {
            get {
                if(this.partsPurReturnOrderDetailDataGridView == null) {
                    this.partsPurReturnOrderDetailDataGridView = DI.GetDataGridView("SupplierPartsPurReturnOrderDetailForEdit");
                    this.partsPurReturnOrderDetailDataGridView.DomainContext = this.DomainContext;
                    this.partsPurReturnOrderDetailDataGridView.IsEnabled = false;
                }
                return this.partsPurReturnOrderDetailDataGridView;
            }
        }

        public PartsPurReturnOrderForApproveDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsPurReturnOrderWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();

                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("PartsPurReturnOrder"));
            var tabControl = new RadTabControl();
            tabControl.Items.Add(new RadTabItem {
                Content = this.PartsPurReturnOrderDetailDataGridView,
                Header = PartsPurchasingUIStrings.DataGridView_Title_PartsPurReturnOrder
            });
            tabControl.SetValue(Grid.ColumnProperty, 1);
            this.Root.Children.Add(tabControl);
        }

        protected override string Title {
            get {
                return PartsPurchasingUIStrings.DataEditView_Title_ApprovePartsPurReturnOrder;//PartsPurReturnOrderForApprove
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.PartsPurReturnOrderDetailDataGridView.CommitEdit())
                return;
            var partsPurReturnOrder = this.DataContext as PartsPurReturnOrder;
            if(partsPurReturnOrder == null)
                return;
            //if(partsPurReturnOrder.PartsPurReturnOrderDetails.Count > 200) {
            //    UIHelper.ShowNotification("清单条目数量不允许超过200条");
            //    return;
            //}
            ((IEditableObject)partsPurReturnOrder).EndEdit();
            try {
                if(partsPurReturnOrder.Can审批配件采购退货单)
                    partsPurReturnOrder.审批配件采购退货单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
