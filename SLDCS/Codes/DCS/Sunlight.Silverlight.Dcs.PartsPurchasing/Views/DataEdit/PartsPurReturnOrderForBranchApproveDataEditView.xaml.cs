﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit
{
    public partial class PartsPurReturnOrderForBranchApproveDataEditView
    {
        private DataGridViewBase partsPurReturnOrderDetailForShowDataGridView;
        protected override string BusinessName
        {
            get
            {
                return DcsUIStrings.BusinessName_PartsPurReturnOrder;
            }
        }
        private DataGridViewBase PartsPurReturnOrderDetailForShowDataGridView
        {
            get
            {
                if (this.partsPurReturnOrderDetailForShowDataGridView == null)
                {
                    this.partsPurReturnOrderDetailForShowDataGridView = DI.GetDataGridView("PartsPurReturnOrderDetailForShow");
                    this.partsPurReturnOrderDetailForShowDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsPurReturnOrderDetailForShowDataGridView;
            }
        }
        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public PartsPurReturnOrderForBranchApproveDataEditView()
        {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += PartsPurReturnOrderForBranchFinalApproveDataEditView_DataContextChanged;
        }

        private void PartsPurReturnOrderForBranchFinalApproveDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var partsPurReturnOrder = this.DataContext as PartsPurReturnOrder;
            if (partsPurReturnOrder == null)
                return;
            partsPurReturnOrder.PropertyChanged -= partsPurReturnOrder_PropertyChanged;
            partsPurReturnOrder.PropertyChanged += partsPurReturnOrder_PropertyChanged;
        }

        void partsPurReturnOrder_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var partsPurReturnOrder = this.DataContext as PartsPurReturnOrder;
            if (partsPurReturnOrder == null)
                return;
            switch (e.PropertyName)
            {
                case "ReturnReason":
                    foreach (var detail in partsPurReturnOrder.PartsPurReturnOrderDetails.ToArray())
                    {
                        partsPurReturnOrder.PartsPurReturnOrderDetails.Remove(detail);
                    }
                    partsPurReturnOrder.PartsPurchaseOrderId = default(int);
                    partsPurReturnOrder.PartsPurchaseOrderCode = string.Empty;
                    break;
            }
        }

        private void CreateUI()
        {
            this.LayoutRoot.Children.Add(DI.GetDataEditPanel("PartsPurReturnOrderForBranchApprove"));
            this.LayoutRoot.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsPurReturnOrder), "PartsPurReturnOrderDetails"), null, this.PartsPurReturnOrderDetailForShowDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.LayoutRoot.Children.Add(detailDataEditView);
            this.RegisterButton(new ButtonItem {
                Command = new DelegateCommand(this.RejectCurrentData),
                Title = PartsPurchasingUIStrings.DataEditView_Title_Reject,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative)
            },true);
        }

        private void RejectCurrentData() {
            var partsPurReturnOrder = this.DataContext as PartsPurReturnOrder;
            if (partsPurReturnOrder == null)
                return;
            if (string.IsNullOrEmpty(partsPurReturnOrder.ApprovertComment)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Title_InitialApproveMemoIsNull);
                return;
            }
            ((IEditableObject)partsPurReturnOrder).EndEdit();
            try {
                if (partsPurReturnOrder.Can驳回配件采购退货单)
                    partsPurReturnOrder.驳回配件采购退货单();
                ExecuteSerivcesMethod(PartsPurchasingUIStrings.DataEditView_Notification_RejectSuccess);  
            } catch (ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            //base.OnEditSubmitting();
        }

        private void ExecuteSerivcesMethod(string notifyMessage) { 
            DomainContext.SubmitChanges(submitOp => { 
                if (submitOp.HasError) { 
                    if (!submitOp.IsErrorHandled) 
                        submitOp.MarkErrorAsHandled(); 
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp); 
                    DomainContext.RejectChanges(); 
                    return; 
                } 
                this.NotifyEditSubmitted(); 
                this.OnCustomEditSubmitted(); 
                UIHelper.ShowNotification(notifyMessage); 
            }, null); 
        }  
        public void OnCustomEditSubmitted() {  
            this.DataContext = null;  
        }  
  
        public new event EventHandler EditSubmitted;  
        private void NotifyEditSubmitted() {  
            var handler = this.EditSubmitted;  
            if (handler != null)  
                handler(this, EventArgs.Empty);  
        } 
        protected override void OnEditSubmitting()
        {
            if (!this.partsPurReturnOrderDetailForShowDataGridView.CommitEdit())
                return;
            var partsPurReturnOrder = this.DataContext as PartsPurReturnOrder;
            if (partsPurReturnOrder == null)
                return;
            partsPurReturnOrder.ValidationErrors.Clear();
            if (!partsPurReturnOrder.PartsPurReturnOrderDetails.Any())
            {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataGridView_Text_PartsPurReturnOrder_PartsPurReturnOrderDetailsPartsPurReturnOrderDetailsIsNull);
                return;
            }
            partsPurReturnOrder.Status = 66;
            if ((!partsPurReturnOrder.Status.Equals(66)) && (!partsPurReturnOrder.Status.Equals(77)))
            {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_ChooseApproveResult);
                return;
            }
            if (partsPurReturnOrder.HasValidationErrors)
                return;
            if (partsPurReturnOrder.PartsPurReturnOrderDetails.Any(detail => detail.HasValidationErrors))
            {
                var tmps = new List<PartsPurReturnOrderDetail>();
                foreach (var item in partsPurReturnOrder.PartsPurReturnOrderDetails)
                {
                    tmps.Add(item);
                    partsPurReturnOrder.PartsPurReturnOrderDetails.Remove(item);
                }
                tmps = tmps.OrderByDescending(i => i.HasValidationErrors).ToList();
                tmps.ForEach(item => partsPurReturnOrder.PartsPurReturnOrderDetails.Add(item));
                return;
            }

            ((IEditableObject)partsPurReturnOrder).EndEdit();
            //校验采购退货清单数是否大于分公司策略的最大开票行数
            this.DomainContext.Load(this.DomainContext.GetBranchstrategiesQuery().Where(r => r.BranchId == partsPurReturnOrder.BranchId), LoadBehavior.RefreshCurrent, loadOpS =>
            {
                if (loadOpS.HasError)
                {
                    if (loadOpS.IsErrorHandled)
                        loadOpS.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOpS);
                    return;
                }
                var branchstrategy = loadOpS.Entities.SingleOrDefault();
                if (branchstrategy != null && partsPurReturnOrder.PartsPurReturnOrderDetails.Count > branchstrategy.MaxInvoiceRow)
                {
                    UIHelper.ShowAlertMessage(string.Format(PartsPurchasingUIStrings.DataEditView_Title_DetailCountError, branchstrategy.MaxInvoiceRow));
                    return;
                }
                try
                {
                    if (partsPurReturnOrder.Can审批配件采购退货单)
                        partsPurReturnOrder.审批配件采购退货单();
                    ExecuteSerivcesMethod(PartsPurchasingUIStrings.DataEditView_Notification_OperateSuccess);  
                }
                catch (ValidationException ex)
                {
                    UIHelper.ShowAlertMessage(ex.Message);
                    return;
                }
                //base.OnEditSubmitting();
            }, null);
        }

        protected override void OnEditCancelled()
        {
            base.OnEditCancelled();
        }

        protected override void OnEditSubmitted()
        {
            base.OnEditSubmitted();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }
        private void LoadEntityToEdit(int id)
        {
            this.DomainContext.Load(this.DomainContext.GetPartsPurReturnOrderWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                {
                    if (!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                //接收查询方法返回数据
                if (entity != null)
                {
                    //界面加载数据
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
        public override void SetObjectToEditById(object id)
        {
            if (this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
