﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit
{
    public partial class PartsPurReturnOrderForBranchFinalApproveDataEditView
    {
        private DataGridViewBase partsPurReturnOrderDetailForShowDataGridView;
        //private bool isEnableds;
        protected override string BusinessName
        {
            get
            {
                return DcsUIStrings.BusinessName_PartsPurReturnOrder;
            }
        }
        private DataGridViewBase PartsPurReturnOrderDetailForShowDataGridView
        {
            get
            {
                if (this.partsPurReturnOrderDetailForShowDataGridView == null)
                {
                    this.partsPurReturnOrderDetailForShowDataGridView = DI.GetDataGridView("PartsPurReturnOrderDetailForShow");
                    this.partsPurReturnOrderDetailForShowDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsPurReturnOrderDetailForShowDataGridView;
            }
        }
        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;
        //public bool IsEnableds
        //{
        //    get
        //    {
        //        return isEnableds;
        //    }
        //    set
        //    {
        //        isEnableds = value;
        //        this.OnPropertyChanged("IsEnableds");

        //    }
        //}

        //private bool isCanCheck = false;
        //private void ShowFileDialog() {
        //    var partsPurReturnOrder = this.DataContext as PartsPurReturnOrder;
        //    if(partsPurReturnOrder == null)
        //        return;
        //    if(partsPurReturnOrder.PartsSalesCategoryId == default(int)) {
        //        UIHelper.ShowNotification("请选择品牌");
        //        return;
        //    }
        //    if(partsPurReturnOrder.WarehouseId == default(int)) {
        //        UIHelper.ShowNotification("请选择仓库");
        //        return;
        //    }
        //    if(partsPurReturnOrder.PartsSupplierCode == string.Empty) {
        //        UIHelper.ShowNotification("请选择供应商");
        //        return;
        //    }
        //    if(partsPurReturnOrder.ReturnReason == (int)DcsPartsPurReturnOrderReturnReason.质量件退货)
        //        //this.Uploader.ShowFileDialog();
        //        return;
        //    //else
        //    //    return;
        //}

        //private string strFileName;
        //private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        //private RadUpload uploader;
        //private RadUpload Uploader {
        //    get {
        //        if(this.uploader == null) {
        //            this.uploader = new RadUpload();
        //            this.uploader.Filter = "Excel File|*.xlsx";
        //            this.uploader.FilterIndex = 0;
        //            this.uploader.IsAppendFilesEnabled = false;
        //            this.uploader.IsAutomaticUpload = true;
        //            this.uploader.MaxFileCount = 1;
        //            this.uploader.MaxFileSize = 30000000;
        //            this.uploader.MaxUploadSize = 10000000;
        //            this.uploader.OverwriteExistingFiles = true;
        //            this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
        //            this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
        //            this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
        //            this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
        //            this.uploader.UploadFinished += this.UpLoad_UploadFinished;
        //            this.uploader.FilesSelected -= this.Uploader_FilesSelected;
        //            this.uploader.FilesSelected += this.Uploader_FilesSelected;
        //            this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
        //            this.uploader.FileUploaded += (sender, e) => {
        //                var partsPurReturnOrder = this.DataContext as PartsPurReturnOrder;
        //                if(partsPurReturnOrder == null)
        //                    return;
        //                ShellViewModel.Current.IsBusy = true;
        //                this.excelServiceClient.ImportPartsPurReturnOrderDetailAsync(e.HandlerData.CustomData["Path"].ToString(), partsPurReturnOrder.PartsSupplierId, partsPurReturnOrder.WarehouseId, partsPurReturnOrder.BranchId, partsPurReturnOrder.PartsSalesCategoryId);
        //                this.excelServiceClient.ImportPartsPurReturnOrderDetailCompleted -= this.ExcelServiceClient_ImportPartsPurReturnOrderDetailCompleted;
        //                this.excelServiceClient.ImportPartsPurReturnOrderDetailCompleted += this.ExcelServiceClient_ImportPartsPurReturnOrderDetailCompleted;
        //            };
        //        }
        //        return this.uploader;
        //    }
        //}

        //private void ExcelServiceClient_ImportPartsPurReturnOrderDetailCompleted(object sender, ImportPartsPurReturnOrderDetailCompletedEventArgs e) {
        //    ShellViewModel.Current.IsBusy = false;
        //    var partsPurReturnOrder = this.DataContext as PartsPurReturnOrder;
        //    if(partsPurReturnOrder == null)
        //        return;
        //    foreach(var detail in partsPurReturnOrder.PartsPurReturnOrderDetails) {
        //        partsPurReturnOrder.PartsPurReturnOrderDetails.Remove(detail);
        //    }

        //    ShellViewModel.Current.IsBusy = false;
        //    if(!string.IsNullOrEmpty(e.errorMessage))
        //        UIHelper.ShowAlertMessage(e.errorMessage);
        //    if(!string.IsNullOrEmpty(e.errorDataFileName))
        //        HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
        //    var rightData = e.rightData;
        //    if(rightData != null && rightData.Length > 0) {
        //        for(int i = 0; i < rightData.Length; ) {
        //            partsPurReturnOrder.PartsPurReturnOrderDetails.Add(new PartsPurReturnOrderDetail {
        //                SparePartId = rightData[i].SparePartId,
        //                SparePartCode = rightData[i].SparePartCode,
        //                SparePartName = rightData[i].SparePartName,
        //                SupplierPartCode = rightData[i].SupplierPartCode,
        //                Quantity = rightData[i].Quantity,
        //                MeasureUnit = rightData[i].MeasureUnit,
        //                BatchNumber = rightData[i].BatchNumber,
        //                Remark = rightData[i].Remark,
        //                UnitPrice = rightData[i].UnitPrice,
        //                CheckUnitPrice = rightData[i].UnitPrice,
        //                SerialNumber = ++i

        //            });
        //        }
        //        partsPurReturnOrder.TotalAmount = partsPurReturnOrder.PartsPurReturnOrderDetails.Sum(r => r.UnitPrice * r.Quantity);

        //    }
        //}

        //private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
        //    if(e.SelectedFiles.Any()) {
        //        this.strFileName = e.SelectedFiles[0].Name;
        //        try {
        //            //尝试读取，出错说明文件已打开
        //            using(e.SelectedFiles[0].File.OpenRead()) {
        //            }
        //        } catch(Exception) {
        //            UIHelper.ShowAlertMessage("请关闭选择的文件后再进行导入");
        //            e.Handled = true;
        //        }
        //    }
        //}


        //private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
        //    //取当前登录用户的HashCode来标记上传文件的唯一性
        //    e.UploadData.FileName = strFileName;
        //    ShellViewModel.Current.IsBusy = true;
        //}

        ///// <summary>
        ///// 文件上传成功后，调用该方法进行上传后的处理。不支持异步调用
        ///// <param>上传文件名 <name>fileName</name> </param>
        ///// </summary>
        //protected Action<string> UploadFileSuccessedProcessing;

        //private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
        //    var upload = sender as RadUpload;
        //    if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
        //        return;
        //    upload.CancelUpload();
        //    if(this.UploadFileSuccessedProcessing == null)
        //        return;
        //    Action importAction = () => {
        //        UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
        //        //取当前登录用户的HashCode来标记上传文件的唯一性
        //        //this.UploadFileSuccessedProcessing.Invoke(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
        //        //暂不考虑并发
        //        this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
        //    };
        //    importAction.Invoke();
        //}


        public PartsPurReturnOrderForBranchFinalApproveDataEditView()
        {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += PartsPurReturnOrderForBranchFinalApproveDataEditView_DataContextChanged;
        }

        private void PartsPurReturnOrderForBranchFinalApproveDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var partsPurReturnOrder = this.DataContext as PartsPurReturnOrder;
            if (partsPurReturnOrder == null)
                return;
            //foreach (var detail in partsPurReturnOrder.PartsPurReturnOrderDetails)
            //{
            //    detail.PropertyChanged += Entity_PropertyChanged;
            //}
            partsPurReturnOrder.PropertyChanged -= partsPurReturnOrder_PropertyChanged;
            partsPurReturnOrder.PropertyChanged += partsPurReturnOrder_PropertyChanged;
            //partsPurReturnOrder.PartsPurReturnOrderDetails.EntityAdded += PartsPurReturnOrderDetails_EntityAdded;
            //partsPurReturnOrder.PartsPurReturnOrderDetails.EntityRemoved += PartsPurReturnOrderDetails_EntityRemoved;
        }

        void partsPurReturnOrder_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var partsPurReturnOrder = this.DataContext as PartsPurReturnOrder;
            if (partsPurReturnOrder == null)
                return;
            switch (e.PropertyName)
            {
                case "ReturnReason":
                    //this.IsEnableds = partsPurReturnOrder.ReturnReason == (int)DcsPartsPurReturnOrderReturnReason.质量件退货;
                    //if (!isCanCheck)
                    //{
                    //    isCanCheck = true;
                    //    return;
                    //}
                    foreach (var detail in partsPurReturnOrder.PartsPurReturnOrderDetails.ToArray())
                    {
                        partsPurReturnOrder.PartsPurReturnOrderDetails.Remove(detail);
                    }
                    partsPurReturnOrder.PartsPurchaseOrderId = default(int);
                    partsPurReturnOrder.PartsPurchaseOrderCode = string.Empty;
                    break;
            }
        }

        //private void PartsPurReturnOrderDetails_EntityRemoved(object sender, EntityCollectionChangedEventArgs<PartsPurReturnOrderDetail> e)
        //{
        //    var partsPurReturnOrder = this.DataContext as PartsPurReturnOrder;
        //    if (partsPurReturnOrder == null)
        //        return;
        //    partsPurReturnOrder.TotalAmount = partsPurReturnOrder.PartsPurReturnOrderDetails.Sum(entity => entity.Quantity * entity.UnitPrice);
        //}

        //private void PartsPurReturnOrderDetails_EntityAdded(object sender, EntityCollectionChangedEventArgs<PartsPurReturnOrderDetail> e)
        //{
        //    e.Entity.PropertyChanged += Entity_PropertyChanged;
        //}

        //private void Entity_PropertyChanged(object sender, PropertyChangedEventArgs e)
        //{
        //    var partsPurReturnOrder = this.DataContext as PartsPurReturnOrder;
        //    if (partsPurReturnOrder == null)
        //        return;
        //    switch (e.PropertyName)
        //    {
        //        case "Quantity":
        //            partsPurReturnOrder.TotalAmount = partsPurReturnOrder.PartsPurReturnOrderDetails.Sum(entity => entity.Quantity * entity.UnitPrice);
        //            break;
        //        case "UnitPrice":
        //            partsPurReturnOrder.TotalAmount = partsPurReturnOrder.PartsPurReturnOrderDetails.Sum(entity => entity.Quantity * entity.UnitPrice);
        //            break;
        //    }
        //}

        private void CreateUI()
        {
            this.LayoutRoot.Children.Add(DI.GetDataEditPanel("PartsPurReturnOrderForBranchFinalApprove"));
            this.LayoutRoot.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsPurReturnOrder), "PartsPurReturnOrderDetails"), null, this.PartsPurReturnOrderDetailForShowDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.LayoutRoot.Children.Add(detailDataEditView);

            this.RegisterButton(new ButtonItem {
                Command = new DelegateCommand(this.RejectCurrentData),
                Title = PartsPurchasingUIStrings.DataEditPanel_Title_Reject,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative)
            },true);
        }

        private void RejectCurrentData() {
            var partsPurReturnOrder = this.DataContext as PartsPurReturnOrder;
            if (partsPurReturnOrder == null)
                return;
            if (string.IsNullOrEmpty(partsPurReturnOrder.InitialApproverComment)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Title_InitialApproveMemoIsNull);
                return;
            }
            ((IEditableObject)partsPurReturnOrder).EndEdit();
            try {
                if (partsPurReturnOrder.Can驳回配件采购退货单)
                    partsPurReturnOrder.驳回配件采购退货单();
                ExecuteSerivcesMethod(PartsPurchasingUIStrings.DataEditView_Notification_RejectSuccess); 
            } catch (ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            DomainContext.SubmitChanges(submitOp => {
                if (submitOp.HasError) {
                    if (!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    DomainContext.RejectChanges();
                    return;
                }
                this.NotifyEditSubmitted();
                this.OnCustomEditSubmitted();
                UIHelper.ShowNotification(notifyMessage);
            }, null);
        } 
        public void OnCustomEditSubmitted() { 
            this.DataContext = null; 
        }
 
        public new event EventHandler EditSubmitted; 
        private void NotifyEditSubmitted() { 
            var handler = this.EditSubmitted; 
            if (handler != null) 
                handler(this, EventArgs.Empty); 
        } 

        //private ICommand exportFileCommand;
        //public ICommand ExportTemplateCommand {
        //    get {
        //        if(this.exportFileCommand == null)
        //            this.InitializeCommand();
        //        return this.exportFileCommand;
        //    }
        //}
        //private DataGridViewBase partsPurReturnOrderDetailForExportDataGridView;
        //private DataGridViewBase PartsPurReturnOrderDetailForExportDataGridView {
        //    get {
        //        if(this.partsPurReturnOrderDetailForExportDataGridView == null) {
        //            this.partsPurReturnOrderDetailForExportDataGridView = DI.GetDataGridView("PartsPurReturnOrderDetailForExport");
        //            this.partsPurReturnOrderDetailForExportDataGridView.DomainContext = this.DomainContext;
        //        }
        //        return this.partsPurReturnOrderDetailForExportDataGridView;
        //    }
        //}
        //private const string EXPORT_DATA_FILE_NAME = "导出配件采购退货清单模板.xlsx";
        //private void InitializeCommand() {
        //    this.exportFileCommand = new DelegateCommand(() => {
        //        var columnItemsValues = new List<ImportTemplateColumn> {
        //                                    new ImportTemplateColumn {
        //                                        Name = "配件编号",
        //                                        IsRequired = true
        //                                    },
        //                                    new ImportTemplateColumn {
        //                                        Name = "配件名称",
        //                                        IsRequired = true
        //                                    },
        //                                    new ImportTemplateColumn {
        //                                        Name = "数量",
        //                                        IsRequired = true
        //                                    },
        //                                    new ImportTemplateColumn {
        //                                        Name = "备注",

        //                                    }
        //                                };
        //        this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
        //            if(loadOp.HasError) {
        //                if(!loadOp.IsErrorHandled)
        //                    loadOp.MarkErrorAsHandled();
        //                DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
        //            }
        //            if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
        //                this.ExportFile(loadOp.Value);
        //        }, null);
        //    });
        //}
        //protected void ExportFile(string errorFileName = null) {
        //    if(string.IsNullOrWhiteSpace(errorFileName)) {
        //        UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
        //        return;
        //    }
        //    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        //}
        protected override void OnEditSubmitting()
        {
            if (!this.partsPurReturnOrderDetailForShowDataGridView.CommitEdit())
                return;
            var partsPurReturnOrder = this.DataContext as PartsPurReturnOrder;
            if (partsPurReturnOrder == null)
                return;
            partsPurReturnOrder.ValidationErrors.Clear();
            if (!partsPurReturnOrder.PartsPurReturnOrderDetails.Any())
            {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataGridView_Text_PartsPurReturnOrder_PartsPurReturnOrderDetailsPartsPurReturnOrderDetailsIsNull);
                return;
            }
            #region MyRegion
            //if(string.IsNullOrWhiteSpace(partsPurReturnOrder.PartsSupplierCode))
            //    partsPurReturnOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurReturnOrder_PartsSupplierCodeIsNull, new[] {
            //        "PartsSupplierCode"
            //    }));

            //if(string.IsNullOrWhiteSpace(partsPurReturnOrder.PartsSupplierName))
            //    partsPurReturnOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurReturnOrder_PartsSupplierNameIsNull, new[] {
            //        "PartsSupplierCode"
            //    }));

            //if(partsPurReturnOrder.PartsSupplierId == default(int))
            //    partsPurReturnOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurReturnOrder_PartsSupplierCodeIsNull, new[] {
            //        "PartsSupplierCode"
            //    }));

            //if(partsPurReturnOrder.WarehouseId == default(int))
            //    partsPurReturnOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurReturnOrder_WarehouseNameIsNull, new[] {
            //        "WarehouseName"
            //    }));

            //if(string.IsNullOrWhiteSpace(partsPurReturnOrder.WarehouseName))
            //    partsPurReturnOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurReturnOrder_WarehouseNameIsNull, new[] {
            //        "WarehouseName"
            //    }));

            //if(partsPurReturnOrder.ReturnReason == (int)DcsPartsPurReturnOrderReturnReason.质量件退货) {

            //    if(partsPurReturnOrder.PartsPurchaseOrderId == default(int))
            //        partsPurReturnOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurReturnOrder_PartsPurchaseOrderCodeIsNull, new[] {
            //        "PartsPurchaseOrderCode"
            //    }));

            //    if(string.IsNullOrWhiteSpace(partsPurReturnOrder.PartsPurchaseOrderCode))
            //        partsPurReturnOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurReturnOrder_PartsPurchaseOrderCodeIsNull, new[] {
            //        "PartsPurchaseOrderCode"
            //    }));
            //}

            //if(partsPurReturnOrder.PartsPurReturnOrderDetails.GroupBy(entity => new {
            //    entity.SparePartCode,
            //    entity.SparePartId
            //}).Any(array => array.Count() > 1)) {
            //    UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurReturnOrder_SparePartCodeCanNotRepeat);
            //    return;
            //}

            //foreach(var item in partsPurReturnOrder.PartsPurReturnOrderDetails) {
            //    if(item.Quantity <= 0)
            //        item.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurReturnOrderDetail_QuantityLessThanZero, new[] {
            //            "Quantity"
            //        }));
            //    if(item.UnitPrice <= 0)
            //        item.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurReturnOrderDetail_UnitPriceLessThanZero, new[] {
            //            "UnitPrice"
            //        }));
            //} 
            #endregion

            //partsPurReturnOrder.InitialApproverComment =this.InitialApproverComment.Text;
            partsPurReturnOrder.Status = 66;
            if ((!partsPurReturnOrder.Status.Equals(66)) && (!partsPurReturnOrder.Status.Equals(77)))
            {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_ChooseApproveResult);
                return;
            }
            if (partsPurReturnOrder.HasValidationErrors)
                return;
            if (partsPurReturnOrder.PartsPurReturnOrderDetails.Any(detail => detail.HasValidationErrors))
            {
                var tmps = new List<PartsPurReturnOrderDetail>();
                foreach (var item in partsPurReturnOrder.PartsPurReturnOrderDetails)
                {
                    tmps.Add(item);
                    partsPurReturnOrder.PartsPurReturnOrderDetails.Remove(item);
                }
                tmps = tmps.OrderByDescending(i => i.HasValidationErrors).ToList();
                tmps.ForEach(item => partsPurReturnOrder.PartsPurReturnOrderDetails.Add(item));
                return;
            }

            ((IEditableObject)partsPurReturnOrder).EndEdit();
            //校验采购退货清单数是否大于分公司策略的最大开票行数
            this.DomainContext.Load(this.DomainContext.GetBranchstrategiesQuery().Where(r => r.BranchId == partsPurReturnOrder.BranchId), LoadBehavior.RefreshCurrent, loadOpS =>
            {
                if (loadOpS.HasError)
                {
                    if (loadOpS.IsErrorHandled)
                        loadOpS.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOpS);
                    return;
                }
                var branchstrategy = loadOpS.Entities.SingleOrDefault();
                if (branchstrategy != null && partsPurReturnOrder.PartsPurReturnOrderDetails.Count > branchstrategy.MaxInvoiceRow)
                {
                    UIHelper.ShowAlertMessage(string.Format(PartsPurchasingUIStrings.DataEditView_Title_DetailCountError, branchstrategy.MaxInvoiceRow));
                    return;
                }
                ////校验：分公司策略.采购退货是否修改价格=是，校验新增，修改时不允许修改退货价格
                //if(branchstrategy != null && branchstrategy.IsPurchaseReturn.HasValue && !(bool)branchstrategy.IsPurchaseReturn) {
                //    if(partsPurReturnOrder.PartsPurReturnOrderDetails.Any(r => r.UnitPrice != r.CheckUnitPrice)) {
                //        UIHelper.ShowNotification("根据分公司策略，不允许修改退货价格");
                //        return;
                //    }
                //}
                try
                {
                    if (partsPurReturnOrder.Can初审配件采购退货单)
                        partsPurReturnOrder.初审配件采购退货单();
                    ExecuteSerivcesMethod(PartsPurchasingUIStrings.DataEditView_Notification_OperateSuccess);  
                }
                catch (ValidationException ex)
                {
                    UIHelper.ShowAlertMessage(ex.Message);
                    return;
                }
                //base.OnEditSubmitting();
            }, null);
        }

        protected override void OnEditCancelled()
        {
            base.OnEditCancelled();
            //isCanCheck = false;
        }

        protected override void OnEditSubmitted()
        {
            base.OnEditSubmitted();
            //isCanCheck = false;
        }
        protected override bool OnRequestCanSubmit() {
            return true;
        }
        private void LoadEntityToEdit(int id)
        {
            this.DomainContext.Load(this.DomainContext.GetPartsPurReturnOrderWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                {
                    if (!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                //接收查询方法返回数据
                if (entity != null)
                {
                    //界面加载数据
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
        //protected override void Reset()
        //{
        //    var partsPurReturnOrder = this.DataContext as PartsPurReturnOrder;
        //    if (this.DomainContext.PartsPurReturnOrders.Contains(partsPurReturnOrder))
        //        this.DomainContext.PartsPurReturnOrders.Detach(partsPurReturnOrder);
        //}

        public override void SetObjectToEditById(object id)
        {
            if (this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
