﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;


namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class PartsPurchaseOrderChannelChangeDataEditView {
        private DataGridViewBase partsPurchaseOrderDetailForChannelChangeDataGridView;
        private DataGridViewBase partsPurchaseOrderDetailReplacePartDataGridView;
        private ObservableCollection<KeyValuePair> kvWarehouses;
        private KeyValueManager keyValueManager;
        private ObservableCollection<KeyValuePair> partsPurchaseOrderOrderTypes;
        private ObservableCollection<KeyValuePair> kvPartsPurchaseOrderType;
        private ObservableCollection<KeyValuePair> partsShippingMethods;
        private ObservableCollection<PartsInsteadInfo> partsInsteadInfos;
        private PartsPurchaseOrder NewPartsPurchaseOrder {
            get;
            set;
        }
        private readonly string[] kvNames = {
            "PartsPurchaseOrder_OrderType","PartsShipping_Method"
        };

        private DataGridViewBase PartsPurchaseOrderDetailForChannelChangeDataGridView {
            get {
                if(this.partsPurchaseOrderDetailForChannelChangeDataGridView == null) {
                    this.partsPurchaseOrderDetailForChannelChangeDataGridView = DI.GetDataGridView("PartsPurchaseOrderDetailForChannelChange");
                    this.partsPurchaseOrderDetailForChannelChangeDataGridView.DomainContext = this.DomainContext;
                    this.partsPurchaseOrderDetailForChannelChangeDataGridView.SelectionChanged += partsPurchaseOrderDetailForChannelChangeDataGridView_SelectionChanged;
                }
                return this.partsPurchaseOrderDetailForChannelChangeDataGridView;
            }
        }

        private void partsPurchaseOrderDetailForChannelChangeDataGridView_SelectionChanged(object sender, EventArgs e) {
            var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
            if(partsPurchaseOrder == null)
                return;
            var partsPurchaseOrderDetail = this.PartsPurchaseOrderDetailForChannelChangeDataGridView.SelectedEntities.FirstOrDefault() as PartsPurchaseOrderDetail;
            if(partsPurchaseOrderDetail == null)
                return;
            this.DomainContext.Load(this.DomainContext.查询采购替互换件Query(partsPurchaseOrderDetail.OriginalSparePartId, partsPurchaseOrder.PartsSalesCategoryId, partsPurchaseOrder.PartsSupplierId), loadOp => {
                if(loadOp.HasError)
                    return;
                PartsInsteadInfos.Clear();
                foreach(var item in loadOp.Entities)
                    this.PartsInsteadInfos.Add(item);
            }, null);
        }

        private DataGridViewBase PartsPurchaseOrderDetailReplacePartDataGridView {
            get {
                if(this.partsPurchaseOrderDetailReplacePartDataGridView == null) {
                    this.partsPurchaseOrderDetailReplacePartDataGridView = DI.GetDataGridView("PartsPurchaseOrderDetailReplacePart");
                    this.partsPurchaseOrderDetailReplacePartDataGridView.DomainContext = this.DomainContext;
                    this.partsPurchaseOrderDetailReplacePartDataGridView.DataContext = this;
                    this.partsPurchaseOrderDetailReplacePartDataGridView.RowDoubleClick += partsPurchaseOrderDetailReplacePartDataGridView_RowDoubleClick;
                }
                return this.partsPurchaseOrderDetailReplacePartDataGridView;
            }
        }

        private void partsPurchaseOrderDetailReplacePartDataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            var item = this.partsPurchaseOrderDetailReplacePartDataGridView.SelectedEntities.FirstOrDefault() as PartsInsteadInfo;
            if(item != null) {
                var partsPurchaseOrderDetail = this.PartsPurchaseOrderDetailForChannelChangeDataGridView.SelectedEntities.FirstOrDefault() as PartsPurchaseOrderDetail;
                if(partsPurchaseOrderDetail == null)
                    return;
                partsPurchaseOrderDetail.SparePartId = item.Id;
                partsPurchaseOrderDetail.SparePartCode = item.SparePartCode;
                partsPurchaseOrderDetail.SparePartName = item.SparePartName;
                partsPurchaseOrderDetail.UnitPrice = item.PurchasePrice;
                partsPurchaseOrderDetail.ChangedSparePartCode = item.SparePartCode;
            }
            var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
            if(partsPurchaseOrder == null)
                return;
            partsPurchaseOrder.TotalAmount = partsPurchaseOrder.PartsPurchaseOrderDetails.Sum(entity => entity.OrderAmount * entity.UnitPrice);
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsPurchaseOrder;
            }
        }

        protected override string Title {
            get {
                return PartsPurchasingUIStrings.DataEditView_Title_WayChange;
            }
        }

        public ObservableCollection<KeyValuePair> KvWarehouses {
            get {
                return this.kvWarehouses ?? (this.kvWarehouses = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvPartsPurchaseOrderTypes {
            get {
                return this.kvPartsPurchaseOrderType ?? (this.kvPartsPurchaseOrderType = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<PartsInsteadInfo> PartsInsteadInfos {
            get {
                return this.partsInsteadInfos ?? (this.partsInsteadInfos = new ObservableCollection<PartsInsteadInfo>());
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        private void LoadEntityToEdit(int id) {
            foreach(var item in this.DomainContext.PartsPurchaseOrders.ToArray()) {
                this.DomainContext.PartsPurchaseOrders.Detach(item);
            }
            this.DomainContext.Load(this.DomainContext.GetPartsPurchaseOrdersWithDetailsByIdForChannelChangeQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.DomainContext.Load(this.DomainContext.GetPartsPurchaseOrderTypesQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效 && v.PartsSalesCategoryId == entity.PartsSalesCategoryId), loadOp1 => {
                        if(loadOp1.HasError)
                            return;
                        foreach(var partsPurchaseOrderType in loadOp1.Entities)
                            this.KvPartsPurchaseOrderTypes.Add(new KeyValuePair {
                                Key = partsPurchaseOrderType.Id,
                                Value = partsPurchaseOrderType.Name,
                            });
                    }, null);
                    //var partsPurchaseOrder = this.CreateNewPartsPurchaseOrder(entity);
                    //this.DataContext = partsPurchaseOrder;
                    //this.DomainContext.PartsPurchaseOrders.Add(partsPurchaseOrder);
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        //private PartsPurchaseOrder CreateNewPartsPurchaseOrder(PartsPurchaseOrder partsPurchaseOrder) {
        //    NewPartsPurchaseOrder = new PartsPurchaseOrder();
        //    NewPartsPurchaseOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
        //    NewPartsPurchaseOrder.BranchId = partsPurchaseOrder.BranchId;
        //    NewPartsPurchaseOrder.BranchCode = partsPurchaseOrder.BranchCode;
        //    NewPartsPurchaseOrder.BranchName = partsPurchaseOrder.BranchName;
        //    NewPartsPurchaseOrder.WarehouseId = partsPurchaseOrder.WarehouseId;
        //    NewPartsPurchaseOrder.WarehouseName = partsPurchaseOrder.WarehouseName;
        //    NewPartsPurchaseOrder.OriginalRequirementBillId = partsPurchaseOrder.Id;
        //    NewPartsPurchaseOrder.OriginalRequirementBillCode = partsPurchaseOrder.Code;
        //    NewPartsPurchaseOrder.OriginalRequirementBillType = partsPurchaseOrder.OriginalRequirementBillType;
        //    NewPartsPurchaseOrder.PartsPurchaseOrderTypeId = partsPurchaseOrder.PartsPurchaseOrderTypeId;
        //    NewPartsPurchaseOrder.PartsSalesCategoryId = partsPurchaseOrder.PartsSalesCategoryId;
        //    NewPartsPurchaseOrder.PartsSalesCategoryName = partsPurchaseOrder.PartsSalesCategoryName;
        //    NewPartsPurchaseOrder.ShippingMethod = partsPurchaseOrder.ShippingMethod;
        //    NewPartsPurchaseOrder.IfDirectProvision = partsPurchaseOrder.IfDirectProvision;
        //    NewPartsPurchaseOrder.PartsSupplierId = partsPurchaseOrder.PartsSupplierId;
        //    NewPartsPurchaseOrder.PartsSupplierCode = partsPurchaseOrder.PartsSupplierCode;
        //    NewPartsPurchaseOrder.PartsSupplierName = partsPurchaseOrder.PartsSupplierName;
        //    NewPartsPurchaseOrder.TotalAmount = partsPurchaseOrder.TotalAmount;
        //    NewPartsPurchaseOrder.ReceivingCompanyId = partsPurchaseOrder.ReceivingCompanyId;
        //    NewPartsPurchaseOrder.ReceivingCompanyName = partsPurchaseOrder.ReceivingCompanyName;
        //    NewPartsPurchaseOrder.ReceivingAddress = partsPurchaseOrder.ReceivingAddress;
        //    NewPartsPurchaseOrder.Remark = partsPurchaseOrder.Remark;
        //    NewPartsPurchaseOrder.RequestedDeliveryTime = DateTime.Now.Date;
        //    NewPartsPurchaseOrder.Status = (int)DcsPartsPurchaseOrderStatus.新增;
        //    NewPartsPurchaseOrder.InStatus = (int)DcsPurchaseInStatus.未入库;
        //    int serialNum = 1;
        //    foreach(var item in partsPurchaseOrder.PartsPurchaseOrderDetails) {
        //        PartsPurchaseOrderDetail detail = new PartsPurchaseOrderDetail();
        //        detail.SerialNumber = serialNum++;
        //        detail.OriginalSparePartId = item.SparePartId;
        //        detail.OriginalSparePartCode = item.SparePartCode;
        //        detail.SparePartName = item.SparePartName;
        //        detail.SparePartId = item.SparePartId;
        //        detail.SparePartCode = item.SparePartCode;
        //        detail.UnitPrice = item.UnitPrice;
        //        detail.SupplierPartCode = item.SupplierPartCode;
        //        detail.OrderAmount = item.OrderAmount - item.ConfirmedAmount;
        //        detail.Remark = item.Remark;
        //        detail.MeasureUnit = item.MeasureUnit;
        //        detail.ConfirmedAmount = 0;
        //        detail.ShippingAmount = item.ShippingAmount;
        //        detail.ConfirmationRemark = item.ConfirmationRemark;
        //        detail.PromisedDeliveryTime = item.PromisedDeliveryTime;
        //        NewPartsPurchaseOrder.PartsPurchaseOrderDetails.Add(detail);
        //    }

        //    return NewPartsPurchaseOrder;
        //}

        public ObservableCollection<KeyValuePair> PartsPurchaseOrderOrderTypes {
            get {
                return this.partsPurchaseOrderOrderTypes ?? (partsPurchaseOrderOrderTypes = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> PartsShippingMethods {
            get {
                return this.partsShippingMethods ?? (this.partsShippingMethods = new ObservableCollection<KeyValuePair>());
            }
        }

        private void CreateUI() {
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "PartsPurchaseOrderDetails"), null, this.PartsPurchaseOrderDetailForChannelChangeDataGridView);
            detailDataEditView.SetValue(Grid.RowProperty, 2);
            this.DomainContext.Load(this.DomainContext.GetWarehousesOrderByNameQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效 && (e.Type == (int)DcsWarehouseType.分库 || e.Type == (int)DcsWarehouseType.总库)), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehouse in loadOp.Entities)
                    this.KvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name,
                    });
            }, null);

            var queryWindow = DI.GetQueryWindow("PartsSupplier");
            queryWindow.SelectionDecided += this.QueryWindow_SelectionDecided;
            this.ptbSupplierCode.PopupContent = queryWindow;
            this.Grid.Children.Add(detailDataEditView);

            var detailDataEditView1 = new DcsDetailDataEditView();
            detailDataEditView1.Register(PartsPurchasingUIStrings.DataEditView_Title_SpareInterchangeInfoDetails, null, this.PartsPurchaseOrderDetailReplacePartDataGridView);
            detailDataEditView1.SetValue(Grid.RowProperty, 2);
            detailDataEditView1.SetValue(Grid.ColumnProperty, 2);
            this.Grid.Children.Add(detailDataEditView1);

            this.Grid.Children.Add(this.CreateVerticalLine(1, 2, 0, 0));

            KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData(() => {
                foreach(var keyValuePair in this.KeyValueManager[this.kvNames[0]]) {
                    this.PartsPurchaseOrderOrderTypes.Add(keyValuePair);
                }
                foreach(var keyValuePair in this.KeyValueManager[this.kvNames[1]]) {
                    this.PartsShippingMethods.Add(keyValuePair);
                }
            });
        }

        private void queryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null)
                return;
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "BranchName", BaseApp.Current.CurrentUserData.EnterpriseName
            });
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", new FilterItem {
                MemberName = "BranchId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
        }

        private void QueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var partsSupplier = queryWindow.SelectedEntities.Cast<PartsSupplier>().FirstOrDefault();
            if(partsSupplier == null)
                return;

            var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
            if(partsPurchaseOrder == null)
                return;

            partsPurchaseOrder.PartsSupplierId = partsSupplier.Id;
            partsPurchaseOrder.PartsSupplierCode = partsSupplier.Code;
            partsPurchaseOrder.PartsSupplierName = partsSupplier.Name;

            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
            //var queryWindow = sender as QueryWindowBase;
            //if(queryWindow == null || queryWindow.SelectedEntities == null)
            //    return;

            //var branchSupplierRelation = queryWindow.SelectedEntities.Cast<BranchSupplierRelation>().FirstOrDefault();
            //if(branchSupplierRelation == null || !this.PartsPurchaseOrderDetailForChannelChangeDataGridView.CommitEdit()) {
            //    UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Error_PartsPurchaseOrder_DetailsIsEditingOrHasError);
            //    var parent = queryWindow.ParentOfType<RadWindow>();
            //    if(parent != null)
            //        parent.Close();
            //    return;
            //}
            //var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
            //if(partsPurchaseOrder == null)
            //    return;
            //try {
            //    if(partsPurchaseOrder.PartsSupplierId == branchSupplierRelation.Id)
            //        return;
            //    partsPurchaseOrder.PartsSupplierId = branchSupplierRelation.SupplierId;
            //    partsPurchaseOrder.PartsSupplierCode = branchSupplierRelation.PartsSupplier.Code;
            //    partsPurchaseOrder.PartsSupplierName = branchSupplierRelation.PartsSupplier.Name;

            //    //TODO: 删除时抛出异常 
            //    var deleteDetals = partsPurchaseOrder.PartsPurchaseOrderDetails.ToArray();
            //    ((IEditableObject)partsPurchaseOrder).EndEdit();
            //    foreach(var detail in deleteDetals) {
            //        ((IEditableObject)detail).EndEdit();
            //        detail.ValidationErrors.Clear();
            //        partsPurchaseOrder.PartsPurchaseOrderDetails.Remove(detail);
            //    }
            //} finally {
            //    var parent = queryWindow.ParentOfType<RadWindow>();
            //    if(parent != null)
            //        parent.Close();
            //}
        }

        private void PartsPurchaseOrderDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
            if(partsPurchaseOrder == null)
                return;
            partsPurchaseOrder.PartsPurchaseOrderDetails.EntityRemoved -= PartsPurchaseOrderDetails_EntityRemoved;
            partsPurchaseOrder.PartsPurchaseOrderDetails.EntityRemoved += PartsPurchaseOrderDetails_EntityRemoved;

            partsPurchaseOrder.PropertyChanged -= partsPurchaseOrder_PropertyChanged;
            partsPurchaseOrder.PropertyChanged += partsPurchaseOrder_PropertyChanged;
        }

        private void partsPurchaseOrder_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
            if(partsPurchaseOrder == null)
                return;
            switch(e.PropertyName) {
                case "PartsSupplierId":
                    PartsInsteadInfos.Clear();
                    if(this.PartsPurchaseOrderDetailForChannelChangeDataGridView.SelectedEntities == null)
                        return;
                    var partsPurchaseOrderDetail = this.PartsPurchaseOrderDetailForChannelChangeDataGridView.SelectedEntities.FirstOrDefault() as PartsPurchaseOrderDetail;
                    if(partsPurchaseOrderDetail == null)
                        return;
                    this.DomainContext.Load(this.DomainContext.查询采购替互换件Query(partsPurchaseOrderDetail.OriginalSparePartId, partsPurchaseOrder.PartsSalesCategoryId, partsPurchaseOrder.PartsSupplierId), loadOp => {
                        if(loadOp.HasError)
                            return;
                        PartsInsteadInfos.Clear();
                        foreach(var item in loadOp.Entities)
                            this.PartsInsteadInfos.Add(item);
                    }, null);
                    break;
            }
        }

        private void PartsPurchaseOrderDetails_EntityRemoved(object sender, EntityCollectionChangedEventArgs<PartsPurchaseOrderDetail> e) {
            this.CalcTotalAmount();
        }

        private void CalcTotalAmount() {
            var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
            if(partsPurchaseOrder == null)
                return;
            int serialNum = 0;
            foreach(var item in partsPurchaseOrder.PartsPurchaseOrderDetails)
                item.SerialNumber = serialNum++;
            partsPurchaseOrder.TotalAmount = partsPurchaseOrder.PartsPurchaseOrderDetails.Sum(entity => entity.OrderAmount * entity.UnitPrice);
        }

        protected override void OnEditSubmitting() {
            if(!this.PartsPurchaseOrderDetailForChannelChangeDataGridView.CommitEdit())
                return;

            var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
            if(partsPurchaseOrder == null)
                return;

            partsPurchaseOrder.ValidationErrors.Clear();
            foreach(var relation in partsPurchaseOrder.PartsPurchaseOrderDetails)
                relation.ValidationErrors.Clear();

            if(string.IsNullOrEmpty(partsPurchaseOrder.WarehouseName))
                partsPurchaseOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrder_WarehouseNameIsNull, new[] {
                    "WarehouseName"
                }));

            if(partsPurchaseOrder.PartsPurchaseOrderTypeId <= 0)
                partsPurchaseOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrder_OrderTypeIsNull, new[] {
                    "PartsPurchaseOrderTypeId"
                }));

            if(partsPurchaseOrder.ShippingMethod <= 0)
                partsPurchaseOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrder_ShippingMethodIsNull, new[] {
                    "ShippingMethod"
                }));

            if(string.IsNullOrEmpty(partsPurchaseOrder.PartsSupplierCode))
                partsPurchaseOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrder_PartsSupplierCodeIsNull, new[] {
                    "PartsSupplierCode"
                }));

            if(!partsPurchaseOrder.PartsPurchaseOrderDetails.Any()) {
                UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrderDetail_DetailIsNotNull));
                return;
            }

            if(partsPurchaseOrder.RequestedDeliveryTime < DateTime.Now) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_RequestedDeliveryTimeMustMoreThanNow);
                return;
            }

            //foreach(var relation in partsPurchaseOrder.PartsPurchaseOrderDetails) {
            //    if(string.IsNullOrEmpty(relation.SparePartCode))
            //        relation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrderDetail_SparePartCodeIsNull, new[] {
            //        "SparePartCode"
            //    }));

            //    if(string.IsNullOrEmpty(relation.SparePartName))
            //        relation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrderDetail_SparePartNameIsNull, new[] {
            //        "SparePartName"
            //    }));
            //}
            if(partsPurchaseOrder.HasValidationErrors || partsPurchaseOrder.PartsPurchaseOrderDetails.Any(relation => relation.HasValidationErrors))
                return;
            //foreach(var item in partsPurchaseOrder.PartsPurchaseOrderDetails.ToArray()) {
            //    if(string.IsNullOrEmpty(item.ChangedSparePartCode)) {
            //        partsPurchaseOrder.PartsPurchaseOrderDetails.Remove(item);
            //    }
            //}
            foreach(var relation in partsPurchaseOrder.PartsPurchaseOrderDetails.Where(e => e.OrderAmount <= 0)) {
                UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Error_SparePart_OrderAmountIsZero, relation.SparePartCode));
                return;
            }
            ((IEditableObject)partsPurchaseOrder).EndEdit();
            try {
                if(partsPurchaseOrder.Can渠道变更)
                    partsPurchaseOrder.渠道变更();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        public PartsPurchaseOrderChannelChangeDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += PartsPurchaseOrderDataEditView_DataContextChanged;
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
