﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class PartsPurchaseOrderDataEditView {
        private DataGridViewBase partsPurchaseOrderDetailForEditDataGridView;
        private ObservableCollection<Warehouse> kvWarehouses;
        private ObservableCollection<KeyValuePair> kvPartsSalesCategories;
        private KeyValueManager keyValueManager;
        private ObservableCollection<KeyValuePair> partsPurchaseOrderOrderTypes;
        private ObservableCollection<KeyValuePair> kvPartsPurchaseOrderType;
        private ObservableCollection<KeyValuePair> partsShippingMethods;

        private readonly string[] kvNames = {
            "PartsPurchaseOrder_OrderType","PartsShipping_Method"
        };

        private DataGridViewBase PartsPurchaseOrderDetailForEditDataGridView {
            get {
                if(this.partsPurchaseOrderDetailForEditDataGridView == null) {
                    this.partsPurchaseOrderDetailForEditDataGridView = DI.GetDataGridView("PartsPurchaseOrderDetailForEdit");
                    this.partsPurchaseOrderDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsPurchaseOrderDetailForEditDataGridView;
            }
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsPurchaseOrder;
            }
        }

        public ObservableCollection<Warehouse> KvWarehouses {
            get {
                return this.kvWarehouses ?? (this.kvWarehouses = new ObservableCollection<Warehouse>());
            }
        }

        public ObservableCollection<KeyValuePair> KvPartsPurchaseOrderTypes {
            get {
                return this.kvPartsPurchaseOrderType ?? (this.kvPartsPurchaseOrderType = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvPartsSalesCategories {
            get {
                return this.kvPartsSalesCategories ?? (this.kvPartsSalesCategories = new ObservableCollection<KeyValuePair>());
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsPurchaseOrdersWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.DomainContext.Load(this.DomainContext.GetWarehousesByPartsSalesCategoryIdAndOwnerCompanyIdQuery(entity.PartsSalesCategoryId, BaseApp.Current.CurrentUserData.EnterpriseId).Where(r => r.Type != (int)DcsWarehouseType.虚拟库), LoadBehavior.RefreshCurrent, loadOp1 => {
                        if(loadOp1.HasError) {
                            loadOp.MarkErrorAsHandled();
                            return;
                        }
                        this.KvWarehouses.Clear();
                        if(loadOp.Entities != null) {
                            foreach(var item in loadOp1.Entities)
                                this.KvWarehouses.Add(item);
                        }
                    }, null);
                    this.SetObjectToEdit(entity);
                }
            }, null);


        }

        public ObservableCollection<KeyValuePair> PartsPurchaseOrderOrderTypes {
            get {
                return this.partsPurchaseOrderOrderTypes ?? (partsPurchaseOrderOrderTypes = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> PartsShippingMethods {
            get {
                return this.partsShippingMethods ?? (this.partsShippingMethods = new ObservableCollection<KeyValuePair>());
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "PartsPurchaseOrderDetails"), null, this.PartsPurchaseOrderDetailForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            //this.DomainContext.Load(this.DomainContext.GetWarehousesOrderByNameQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效 && (e.Type == (int)DcsWarehouseType.分库 || e.Type == (int)DcsWarehouseType.总库)), loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    this.KvWarehouses.Clear();
            //    foreach(var warehouse in loadOp.Entities)
            //        this.KvWarehouses.Add(new KeyValuePair {
            //            Key = warehouse.Id,
            //            Value = warehouse.Name,
            //            UserObject = warehouse
            //        });
            //}, null);
            this.DomainContext.Load(this.DomainContext.GetPersonSalesCenterLinksQuery().Where(r => r.PersonId == BaseApp.Current.CurrentUserData.UserId && r.Status == (int)DcsBaseDataStatus.有效), loadOp1 => {
                if(loadOp1.HasError)
                    return;
                var partsSaleScategoryIds = loadOp1.Entities.Select(r => r.PartsSalesCategoryId).ToArray();
                this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(e => e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                    if(loadOp.HasError)
                        return;
                    this.KvPartsSalesCategories.Clear();
                    foreach(var partsSalesCategory in loadOp.Entities)
                        if(partsSaleScategoryIds.Contains(partsSalesCategory.Id)) {
                            this.KvPartsSalesCategories.Add(new KeyValuePair {
                                Key = partsSalesCategory.Id,
                                Value = partsSalesCategory.Name,
                                UserObject = partsSalesCategory
                            });
                        }
                }, null);
            }, null);

            var queryWindow = DI.GetQueryWindow("BranchSupplierRelation");
            queryWindow.SelectionDecided += this.QueryWindow_SelectionDecided;
            queryWindow.Loaded += queryWindow_Loaded;
            this.ptbSupplierCode.PopupContent = queryWindow;


            this.Root.Children.Add(detailDataEditView);

            KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData(() => {
                foreach(var keyValuePair in this.KeyValueManager[this.kvNames[0]]) {
                    this.PartsPurchaseOrderOrderTypes.Add(keyValuePair);
                }
                foreach(var keyValuePair in this.KeyValueManager[this.kvNames[1]]) {
                    this.PartsShippingMethods.Add(keyValuePair);
                }
            });
        }

        private void queryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
            if(queryWindow == null || partsPurchaseOrder == null)
                return;
            if(partsPurchaseOrder.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Error_PartsPurchaseOrder_PleaseSelectPartsSalesCategoryId);
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
                return;
            }
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "Branch.Name", BaseApp.Current.CurrentUserData.EnterpriseName
            });
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem("PartsSalesCategoryId", typeof(int), FilterOperator.IsEqualTo, partsPurchaseOrder.PartsSalesCategoryId));
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "BranchId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }

        private void QueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var branchSupplierRelation = queryWindow.SelectedEntities.Cast<BranchSupplierRelation>().FirstOrDefault();
            if(branchSupplierRelation == null || !this.PartsPurchaseOrderDetailForEditDataGridView.CommitEdit()) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Error_PartsPurchaseOrder_DetailsIsEditingOrHasError);
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
                return;
            }
            var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
            if(partsPurchaseOrder == null)
                return;
            try {
                if(partsPurchaseOrder.PartsSupplierId == branchSupplierRelation.Id)
                    return;
                partsPurchaseOrder.PartsSupplierId = branchSupplierRelation.SupplierId;
                partsPurchaseOrder.PartsSupplierCode = branchSupplierRelation.PartsSupplier.Code;
                partsPurchaseOrder.PartsSupplierName = branchSupplierRelation.PartsSupplier.Name;

                //TODO: 删除时抛出异常 
                var deleteDetals = partsPurchaseOrder.PartsPurchaseOrderDetails.ToArray();
                ((IEditableObject)partsPurchaseOrder).EndEdit();
                foreach(var detail in deleteDetals) {
                    ((IEditableObject)detail).EndEdit();
                    detail.ValidationErrors.Clear();
                    partsPurchaseOrder.PartsPurchaseOrderDetails.Remove(detail);
                }
            } finally {
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }
        }

        private void PartsPurchaseOrderDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
            if(partsPurchaseOrder == null)
                return;
            partsPurchaseOrder.PropertyChanged -= this.partsPurchaseOrder_PropertyChanged;
            partsPurchaseOrder.PropertyChanged += this.partsPurchaseOrder_PropertyChanged;
            partsPurchaseOrder.PartsPurchaseOrderDetails.EntityRemoved -= PartsPurchaseOrderDetails_EntityRemoved;
            partsPurchaseOrder.PartsPurchaseOrderDetails.EntityRemoved += PartsPurchaseOrderDetails_EntityRemoved;
        }

        private void partsPurchaseOrder_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
            if(partsPurchaseOrder == null)
                return;
            if(this.DomainContext == null) {
                this.DomainContext = new DcsDomainContext();
            }
            switch(e.PropertyName) {
                case "PartsSalesCategoryId":
                    this.DomainContext.Load(this.DomainContext.GetWarehousesByPartsSalesCategoryIdAndOwnerCompanyIdQuery(partsPurchaseOrder.PartsSalesCategoryId, BaseApp.Current.CurrentUserData.EnterpriseId).Where(r => r.Type != (int)DcsWarehouseType.虚拟库), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            loadOp.MarkErrorAsHandled();
                            return;
                        }
                        this.KvWarehouses.Clear();
                        if(loadOp.Entities != null) {
                            foreach(var item in loadOp.Entities)
                                this.KvWarehouses.Add(item);
                        }
                    }, null);
                    if(partsPurchaseOrder.PartsPurchaseOrderDetails.Any()) {
                        foreach(var detail in partsPurchaseOrder.PartsPurchaseOrderDetails.ToArray())
                            partsPurchaseOrder.PartsPurchaseOrderDetails.Remove(detail);
                    }
                    partsPurchaseOrder.WarehouseId = default(int);
                    partsPurchaseOrder.PartsPurchaseOrderTypeId = default(int);
                    break;
                default:
                    break;
            }
        }

        private void PartsPurchaseOrderDetails_EntityRemoved(object sender, EntityCollectionChangedEventArgs<PartsPurchaseOrderDetail> e) {
            this.CalcTotalAmount();
        }

        private void CalcTotalAmount() {
            var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
            if(partsPurchaseOrder == null)
                return;
            partsPurchaseOrder.TotalAmount = partsPurchaseOrder.PartsPurchaseOrderDetails.Sum(entity => entity.OrderAmount * entity.UnitPrice);
        }

        protected override void OnEditSubmitting() {
            if(!this.PartsPurchaseOrderDetailForEditDataGridView.CommitEdit())
                return;

            var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
            if(partsPurchaseOrder == null)
                return;

            partsPurchaseOrder.ValidationErrors.Clear();
            foreach(var relation in partsPurchaseOrder.PartsPurchaseOrderDetails)
                relation.ValidationErrors.Clear();

            if(string.IsNullOrEmpty(partsPurchaseOrder.WarehouseName))
                partsPurchaseOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrder_WarehouseNameIsNull, new[] {
                    "WarehouseName"
                }));

            if(partsPurchaseOrder.PartsPurchaseOrderTypeId <= 0)
                partsPurchaseOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrder_OrderTypeIsNull, new[] {
                    "PartsPurchaseOrderTypeId"
                }));

            if(string.IsNullOrEmpty(partsPurchaseOrder.PlanSource))
                partsPurchaseOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PlanSourceIsNull, new[] {
                    "PlanSource"
                }));

            if(string.IsNullOrEmpty(partsPurchaseOrder.PartsSupplierCode))
                partsPurchaseOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrder_PartsSupplierCodeIsNull, new[] {
                    "PartsSupplierCode"
                }));

            if(partsPurchaseOrder.RequestedDeliveryTime.Date < DateTime.Now.Date)
                partsPurchaseOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrder_RequestedDeliveryTimeIsEarly, new[] {
                    "RequestedDeliveryTime"
                }));
            //if(partsPurchaseOrder.PartsSalesCategory != null && partsPurchaseOrder.PartsSalesCategory.IsOverseas == true) {
            //    if(string.IsNullOrEmpty(partsPurchaseOrder.SAPPurchasePlanCode)) {
            //        UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrder_SAPPurchasePlanCodeIsNull);
            //        return;
            //    }
            //}
            if(!partsPurchaseOrder.PartsPurchaseOrderDetails.Any()) {
                UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrderDetail_DetailIsNotNull));
                return;
            }

            foreach(var relation in partsPurchaseOrder.PartsPurchaseOrderDetails) {
                if(string.IsNullOrEmpty(relation.SparePartCode))
                    relation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrderDetail_SparePartCodeIsNull, new[] {
                        "SparePartCode"
                    }));

                if(string.IsNullOrEmpty(relation.SparePartName))
                    relation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrderDetail_SparePartNameIsNull, new[] {
                        "SparePartName"
                    }));
            }

            foreach (var relation in partsPurchaseOrder.PartsPurchaseOrderDetails.Where(e => e.OrderAmount <= 0)) {
                UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Error_SparePart_OrderAmountIsZero, relation.SparePartCode));
                return;
            }

            foreach (var relation in partsPurchaseOrder.PartsPurchaseOrderDetails.Where(e => e.OrderAmount > e.MaxOrderAmount)) {
                UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Validation_MaxOrderAmount, relation.SparePartCode, relation.MaxOrderAmount));
                return;
            }

            //合法性校验：如果采购订单.是否直供=是，且采购订单清单.直供量<>0,如果订货量>直供量 提示：配件编号xxx订货量必须小于直供量
            if(partsPurchaseOrder.IfDirectProvision) {
                foreach(var relation in partsPurchaseOrder.PartsPurchaseOrderDetails.Where(e => e.DirectOrderAmount != null && e.OrderAmount > e.DirectOrderAmount)) {
                    UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Error_SparePart_OrderAmountMoreThanDirectOrderAmount, relation.SparePartCode));
                    return;
                }
            }


            var sparepartIds = partsPurchaseOrder.PartsPurchaseOrderDetails.Select(r => r.SparePartId).ToArray();
            this.DomainContext.Load(DomainContext.GetSparePartsByIdsQuery(sparepartIds), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled) {
                        loadOp.MarkErrorAsHandled();
                        return;
                    }
                }
                if(loadOp.Entities != null && loadOp.Entities.Any()) {
                    var spareParts = loadOp.Entities.ToArray();
                    if(spareParts.Any(r => r.MInPackingAmount == null || r.MInPackingAmount == 0)) {
                        UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_NoPackingProp);
                        return;
                    }
                    foreach(var partsPurchaseOrderDetail in partsPurchaseOrder.PartsPurchaseOrderDetails) {
                        if(spareParts.Any(r => r.Id == partsPurchaseOrderDetail.SparePartId && (partsPurchaseOrderDetail.OrderAmount % r.MInPackingAmount.Value) != 0)) {
                            var partsValidation = spareParts.First(r => r.Id == partsPurchaseOrderDetail.SparePartId && (partsPurchaseOrderDetail.OrderAmount % r.MInPackingAmount.Value) != 0);
                            UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Validation_OrderAmountNotSatisfy, partsValidation.Code, partsValidation.MInPackingAmount));
                            return;
                        }
                    }
                }

                if(partsPurchaseOrder.HasValidationErrors || partsPurchaseOrder.PartsPurchaseOrderDetails.Any(relation => relation.HasValidationErrors))
                    return;

                if(partsPurchaseOrder.PartsPurchaseOrderDetails.Any(r => r.UnitPrice == 0)) {
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataEditView_Validation_OrderDetailsNoPlanPriceOrNoPurchasePrice, () => {
                        ((IEditableObject)partsPurchaseOrder).EndEdit();
                        try {
                            if(this.EditState == DataEditState.New) {
                                if(partsPurchaseOrder.Can生成配件采购订单)
                                    partsPurchaseOrder.生成配件采购订单();
                            } else {
                                if(partsPurchaseOrder.Can修改配件采购订单)
                                    partsPurchaseOrder.修改配件采购订单();
                            }
                        } catch(ValidationException ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                            return;
                        }
                        base.OnEditSubmitting();
                    });
                } else {
                    ((IEditableObject)partsPurchaseOrder).EndEdit();
                    try {
                        if(this.EditState == DataEditState.New) {
                            if(partsPurchaseOrder.Can生成配件采购订单)
                                partsPurchaseOrder.生成配件采购订单();
                        } else {
                            if(partsPurchaseOrder.Can修改配件采购订单)
                                partsPurchaseOrder.修改配件采购订单();
                        }
                    } catch(ValidationException ex) {
                        UIHelper.ShowAlertMessage(ex.Message);
                        return;
                    }
                    base.OnEditSubmitting();
                }
            }, null);
        }

        public PartsPurchaseOrderDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += PartsPurchaseOrderDataEditView_DataContextChanged;
            this.KeyValueManager.LoadData();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public int AllowPartsPurchaseOrderSelectionChange = 0;
        private void PartsPurchaseOrder_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
            if(partsPurchaseOrder == null)
                return;
            this.DomainContext.Load(this.DomainContext.GetPartsPurchaseOrderTypesQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效 && v.PartsSalesCategoryId == partsPurchaseOrder.PartsSalesCategoryId), loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Entities.Count() > 0)
                    this.KvPartsPurchaseOrderTypes.Clear();
                foreach(var partsPurchaseOrderType in loadOp.Entities)
                    this.KvPartsPurchaseOrderTypes.Add(new KeyValuePair {
                        Key = partsPurchaseOrderType.Id,
                        Value = partsPurchaseOrderType.Name,
                    });
                if(AllowPartsPurchaseOrderSelectionChange != 0 && partsPurchaseOrder.PartsSalesCategoryId != AllowPartsPurchaseOrderSelectionChange) {
                    this.ptbSupplierCode.Text = null;
                    partsPurchaseOrder.PartsSupplierCode = null;
                    partsPurchaseOrder.PartsSupplierName = null;
                }
                AllowPartsPurchaseOrderSelectionChange = partsPurchaseOrder.PartsSalesCategoryId;
            }, null);
        }

        protected override void Reset() {
            var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
            if(this.DomainContext.PartsPurchaseOrders.Contains(partsPurchaseOrder))
                this.DomainContext.PartsPurchaseOrders.Detach(partsPurchaseOrder);
        }
    }
}