﻿using System;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class PartsPurchaseOrderDetailDataEditView{
       private DataGridViewBase partsPurchaseOrderDetailForApproveDataGridView;

        private DataGridViewBase PartsPurchaseOrderDetailForApproveDataGridView {
            get {
                if(partsPurchaseOrderDetailForApproveDataGridView == null) {
                    this.partsPurchaseOrderDetailForApproveDataGridView = DI.GetDataGridView("PartsPurchaseOrderForDetail");
                    this.partsPurchaseOrderDetailForApproveDataGridView.DomainContext = this.DomainContext;
                }
                return partsPurchaseOrderDetailForApproveDataGridView;
            }
        }

        public PartsPurchaseOrderDetailDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("PartsPurchaseOrderForDetail"));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrderDetail), PartsPurchasingUIStrings.DetailPanel_Title_PartsPurchaseOrderDetails), null, () => this.PartsPurchaseOrderDetailForApproveDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 1);
            this.Root.Children.Add(detailDataEditView);
        }

        protected override string Title {
            get {
                return "采购订单详情";
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsPurchaseOrdersWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;

                var PartsSalesCategoryId = entity.PartsSalesCategoryId;
                var PartsSupplierId = entity.PartsSupplierId;
                var SparePartIds = entity.PartsPurchaseOrderDetails.Select(r => r.SparePartId).ToArray();
                this.DomainContext.Load(this.DomainContext.GetPartsSupplierRelationBySpareIdAndSupplierIdQuery(PartsSalesCategoryId, PartsSupplierId, SparePartIds), LoadBehavior.RefreshCurrent, loadOp1 => {
                    if(loadOp1.HasError) {
                        if(!loadOp1.IsErrorHandled)
                            loadOp1.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp1);
                        return;
                    }
                    if(loadOp1.Entities == null) {
                        foreach(var detail in entity.PartsPurchaseOrderDetails) {
                            detail.PendingConfirmedAmount = detail.OrderAmount - detail.ConfirmedAmount;
                            detail.ConfirmedAmount = detail.PendingConfirmedAmount;
                            detail.ShippingDate = DateTime.Now.Date;
                        }
                    } else {
                        foreach(var detail in entity.PartsPurchaseOrderDetails) {
                            detail.PendingConfirmedAmount = detail.OrderAmount - detail.ConfirmedAmount;
                            detail.ConfirmedAmount = detail.PendingConfirmedAmount;
                            detail.ShippingDate = DateTime.Now.Date;
                            foreach(var partsSupplierRelation in loadOp1.Entities) {
                                if(partsSupplierRelation.PartId == detail.SparePartId) {
                                    detail.ShippingDate = DateTime.Now.Date.AddDays(partsSupplierRelation.OrderCycle ?? 0);
                                }

                            }
                        }
                    }

                }, null);              
                //查询联系人联系电话 直供的就是销售订单联系人联系方式。非直供的就是仓库的联系人，联系方式。
                this.DomainContext.Load(this.DomainContext.getVirtualContactPersonQuery(id), LoadBehavior.RefreshCurrent, link => {
                    if(link.HasError) {
                        if(!link.IsErrorHandled)
                            link.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(link);
                        return;
                    }
                    if(link.Entities != null) {
                        var person = link.Entities.FirstOrDefault();
                        if(person != null) {
                            entity.ContactPerson = person.ContactPerson;
                            entity.ContactPhone = person.ContactPhone;
                        }

                    }

                }, null);
                this.SetObjectToEdit(entity);
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }      
    }
}