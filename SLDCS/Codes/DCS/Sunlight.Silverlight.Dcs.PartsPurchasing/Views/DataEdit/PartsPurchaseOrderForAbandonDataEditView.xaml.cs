﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class PartsPurchaseOrderForAbandonDataEditView {
        public PartsPurchaseOrderForAbandonDataEditView() {
            InitializeComponent();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsPurchaseOrderByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
            if(partsPurchaseOrder == null)
                return;
            partsPurchaseOrder.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(partsPurchaseOrder.AbandonOrStopReason))
                partsPurchaseOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_AbandonReasonIsNull, new[] {
                    "AbandonOrStopReason"
                }));

            try {
                if(partsPurchaseOrder.Can作废配件采购订单)
                    partsPurchaseOrder.作废配件采购订单();
            } catch(Exception ex) {
                UIHelper.ShowAlertMessage(ex.Message);
            }
            base.OnEditSubmitting();
        }

        protected override string Title {
            get {
                return PartsPurchasingUIStrings.DataEditView_Title_AbandonPartsPurchaseOrder;
            }
        }
    }
}
