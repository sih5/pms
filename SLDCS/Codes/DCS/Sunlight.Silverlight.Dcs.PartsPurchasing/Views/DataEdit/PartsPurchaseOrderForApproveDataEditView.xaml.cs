﻿using System;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class PartsPurchaseOrderForApproveDataEditView {
        private DataGridViewBase partsPurchaseOrderDetailForApprove;

        private DataGridViewBase PartsPurchaseOrderDetailForApprove {
            get {
                if(this.partsPurchaseOrderDetailForApprove == null) {
                    this.partsPurchaseOrderDetailForApprove = DI.GetDataGridView("PartsPurchaseOrderDetailForApprove");
                    this.partsPurchaseOrderDetailForApprove.DomainContext = this.DomainContext;
                }
                return this.partsPurchaseOrderDetailForApprove;
            }
        }

        public PartsPurchaseOrderForApproveDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("PartsPurchaseOrderDetailForApprove"));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrderDetail), PartsPurchasingUIStrings.DetailPanel_Title_PartsPurchaseOrderDetails), null, () => this.PartsPurchaseOrderDetailForApprove);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.SetValue(Grid.ColumnProperty, 1);
            this.Root.Children.Add(detailDataEditView);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsPurchaseOrdersWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                var PartsSalesCategoryId = entity.PartsSalesCategoryId;
                var PartsSupplierId = entity.PartsSupplierId;
                var SparePartIds = entity.PartsPurchaseOrderDetails.Select(r => r.SparePartId).ToArray();
                this.DomainContext.Load(this.DomainContext.GetPartsSupplierRelationBySpareIdAndSupplierIdQuery(PartsSalesCategoryId, PartsSupplierId, SparePartIds), LoadBehavior.RefreshCurrent, loadOp1 => {
                    if(loadOp1.HasError) {
                        if(!loadOp1.IsErrorHandled)
                            loadOp1.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp1);
                        return;
                    }
                    if(loadOp1.Entities == null) {
                        foreach(var detail in entity.PartsPurchaseOrderDetails) {
                            detail.PendingConfirmedAmount = detail.OrderAmount - detail.ConfirmedAmount;
                            //detail.PromisedDeliveryTime = DateTime.Now.Date;
                            detail.ConfirmedAmount = detail.PendingConfirmedAmount;
                            detail.ShippingDate = DateTime.Now.Date;
                        }
                    } else {
                        foreach(var detail in entity.PartsPurchaseOrderDetails) {
                            detail.PendingConfirmedAmount = detail.OrderAmount - detail.ConfirmedAmount;
                            //detail.PromisedDeliveryTime = DateTime.Now.Date;
                            detail.ConfirmedAmount = detail.PendingConfirmedAmount;
                            detail.ShippingDate = DateTime.Now.Date;
                            foreach(var partsSupplierRelation in loadOp1.Entities) {
                                if(partsSupplierRelation.PartId == detail.SparePartId) {
                                    detail.ShippingDate = DateTime.Now.Date.AddDays(partsSupplierRelation.OrderCycle ?? 0);
                                }

                            }
                        }
                    }

                }, null);
                //查询联系人联系电话 直供的就是销售订单联系人联系方式。非直供的就是仓库的联系人，联系方式。
                this.DomainContext.Load(this.DomainContext.getVirtualContactPersonQuery(id), LoadBehavior.RefreshCurrent, link =>
                {
                    if (link.HasError)
                    {
                        if (!link.IsErrorHandled)
                            link.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(link);
                        return;
                    } 
                    if (link.Entities != null)
                    {
                        var person = link.Entities.FirstOrDefault();
                        if(person!=null) {
                            entity.ContactPerson = person.ContactPerson;
                            entity.ContactPhone = person.ContactPhone;
                        }
                      
                    }

                }, null);
                this.SetObjectToEdit(entity);
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override string Title {
            get {
                return PartsPurchasingUIStrings.DataGridView_Title_Approve_PartsPurchaseOrder;
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {
            if(!this.PartsPurchaseOrderDetailForApprove.CommitEdit())
                return;
            var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
            if(partsPurchaseOrder == null)
                return;
            partsPurchaseOrder.ValidationErrors.Clear();
            foreach(var item in partsPurchaseOrder.PartsPurchaseOrderDetails)
                item.ValidationErrors.Clear();
            if(!partsPurchaseOrder.PartsPurchaseOrderDetails.Any()) {
                UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrderDetail_DetailIsNotNull));
                return;
            }
            foreach(var relation in partsPurchaseOrder.PartsPurchaseOrderDetails.Where(e => e.ConfirmedAmount < 0 || e.ConfirmedAmount > e.PendingConfirmedAmount)) {
                UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrderDetail_ConfirmedAmountLessThanOrderAmount, relation.SparePartCode));
                return;
            }
            //确认量不等于计划量，未填写短供原因，不可保存
            foreach(var relation in partsPurchaseOrder.PartsPurchaseOrderDetails.Where(e => !e.ConfirmedAmount.Equals(e.PendingConfirmedAmount) && (!e.ShortSupReason.HasValue || e.ShortSupReason == default(int)))) {
                UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Validation_ShortSupReasonIsNull, relation.SparePartCode));
                return;
            }
            //if(partsPurchaseOrder.PartsPurchaseOrderDetails.Sum(i => i.ConfirmedAmount) == 0) {
            //    UIHelper.ShowNotification("确认量不能全部为0");
            //    return;
            //}
            int num = 0;
            //SIH_ISS20180408004
            if(num > 0) {
                DcsUtils.Confirm(PartsPurchasingUIStrings.DataEditView_Validation_WhetherToContinue, () => {
                    ((IEditableObject)partsPurchaseOrder).EndEdit();
                    foreach(var entity in partsPurchaseOrder.PartsPurchaseOrderDetails)
                        ((IEditableObject)entity).EndEdit();
                    try {
                        if(partsPurchaseOrder.Can供应商确认配件采购订单)
                            partsPurchaseOrder.供应商确认配件采购订单();
                    } catch(Exception ex) {
                        UIHelper.ShowAlertMessage(ex.Message);
                    }
                    base.OnEditSubmitting();
                });
            } else {
                ((IEditableObject)partsPurchaseOrder).EndEdit();
                foreach(var entity in partsPurchaseOrder.PartsPurchaseOrderDetails)
                    ((IEditableObject)entity).EndEdit();
                try {
                    if(partsPurchaseOrder.Can供应商确认配件采购订单)
                        partsPurchaseOrder.供应商确认配件采购订单();
                } catch(Exception ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                }
                base.OnEditSubmitting();
            }
        }
    }
}
