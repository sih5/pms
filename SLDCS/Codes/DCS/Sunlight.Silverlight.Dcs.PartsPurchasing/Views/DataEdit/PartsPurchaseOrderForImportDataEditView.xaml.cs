﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class PartsPurchaseOrderForImportDataEditView {
        private DataGridViewBase partsPurchaseOrderForImportDataGridView;
        private const string EXPORT_DATA_FILE_NAME = "导出配件采购订单管理模板.xlsx";
        private ICommand exportFileCommand;
        private ICommand importCommand;
        private ICommand deleteCommand;
        public PartsPurchaseOrderForImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private ObservableCollection<PartsPurchaseOrderDetailExtend> partsPurchaseOrderDetailExtends;

        public ObservableCollection<PartsPurchaseOrderDetailExtend> PartsPurchaseOrderDetailExtends {
            get {
                if(this.partsPurchaseOrderDetailExtends == null) {
                    this.partsPurchaseOrderDetailExtends = new ObservableCollection<PartsPurchaseOrderDetailExtend>();
                } return partsPurchaseOrderDetailExtends;
            }
        }
        protected override string Title {
            get {
                return PartsPurchasingUIStrings.DataEditView_Title_ImportPartsPurchaseOrder;
            }
        }

        protected DataGridViewBase ImportDataGridView {
            get {
                if(this.partsPurchaseOrderForImportDataGridView == null) {
                    this.partsPurchaseOrderForImportDataGridView = DI.GetDataGridView("PartsPurchaseOrderForImport");
                    this.partsPurchaseOrderForImportDataGridView.DomainContext = this.DomainContext;
                    this.partsPurchaseOrderForImportDataGridView.DataContext = this;
                }
                return this.partsPurchaseOrderForImportDataGridView;
            }
        }

        private void CreateUI() {
            var dataEdit = new DcsDetailDataEditView();
            dataEdit.Register(PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrderForImport, null, () => this.ImportDataGridView);
            dataEdit.UnregisterButton(dataEdit.InsertButton);
            dataEdit.SetValue(Grid.RowProperty, 1);
            this.LayoutRoot.Children.Add(dataEdit);
        }

        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }
        public ICommand DeleteCommand {
            get {
                if(deleteCommand == null)
                    this.InitializeCommand();
                return this.deleteCommand;
            }
        }
        public ICommand ImportCommand {
            get {
                if(importCommand == null)
                    this.InitializeCommand();
                this.importCommand = new DelegateCommand(() => this.Uploader.ShowFileDialog());
                return this.importCommand;
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columns = new List<ImportTemplateColumn> {
                                new ImportTemplateColumn {
                                    Name = PartsPurchasingUIStrings.DataEditView_ImportTemplate_PartsSalesCategory,
                                    IsRequired = true
                                },
                                new ImportTemplateColumn {
                                    Name = PartsPurchasingUIStrings.DataEditView_ImportTemplate_WarehouseName,
                                    IsRequired = true
                                },
                                new ImportTemplateColumn {
                                    Name = PartsPurchasingUIStrings.DataEditView_ImportTemplate_OrderType,
                                    IsRequired = true
                                },
                                new ImportTemplateColumn {
                                    Name = PartsPurchasingUIStrings.DataEditView_ImportTemplate_RequestedDeliveryTime,
                                    IsRequired = true,
                                },
                                new ImportTemplateColumn {
                                    Name = PartsPurchasingUIStrings.DataEditView_ImportTemplate_ShippingMethod
                                },
                                 new ImportTemplateColumn {
                                    Name = PartsPurchasingUIStrings.DataEditView_ImportTemplate_PlanSource,
                                    IsRequired = true
                                },
                                //  new ImportTemplateColumn {
                                //    Name = "SAP采购计划单号"
                                //},
                                new ImportTemplateColumn {
                                    Name = PartsPurchasingUIStrings.DataEditView_ImportTemplate_MainRemark
                                },
                                new ImportTemplateColumn {
                                    Name = PartsPurchasingUIStrings.DataEditView_ImportTemplate_SparePartCode,
                                    IsRequired = true
                                },
                                new ImportTemplateColumn {
                                    Name = PartsPurchasingUIStrings.DataEditView_ImportTemplate_SparePartName
                                },
                                new ImportTemplateColumn {
                                    Name = PartsPurchasingUIStrings.DataEditView_ImportTemplate_SupplierCode,
                                    IsRequired = true
                                },
                                new ImportTemplateColumn {
                                    Name = PartsPurchasingUIStrings.DataEditView_ImportTemplate_SupplierName
                                },
                                new ImportTemplateColumn {
                                    Name = PartsPurchasingUIStrings.DataEditView_ImportTemplate_OrderAmount,
                                    IsRequired = true
                                },
                                //new ImportTemplateColumn {
                                //    Name = "PO单号"
                                //},
                                new ImportTemplateColumn {
                                    Name = PartsPurchasingUIStrings.DataEditView_ImportTemplate_DetailRemark
                                },                              
                                new ImportTemplateColumn {
                                    Name = PartsPurchasingUIStrings.DataEditView_ImportTemplate_OrderAllSign
                                }
                            };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columns, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                    return;
                }, null);
            });

            this.deleteCommand = new Core.Command.DelegateCommand(() => this.ImportDataGridView.SelectedEntities.Reverse());
        }

        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }

        protected override void OnEditSubmitting() {
            if(!this.ImportDataGridView.CommitEdit())
                return;

            var partsPurchaseOrders = new List<PartsPurchaseOrder>();
            foreach(var item in this.PartsPurchaseOrderDetailExtends.GroupBy(e => new {
                e.RequestedDeliveryTime,
                e.PartsSalesCategoryId,
                e.WarehouseId,
                e.PartsPurchaseOrderTypeId,
                e.ShippingMethod,
                e.PartsSupplierId,
                e.OrderCollectMark
            })) {
                var partsPurchaseOrderDetailExtend = item.FirstOrDefault();
                if(partsPurchaseOrderDetailExtend == null)
                    return;
                var partsPurchaseOrderDetails = new List<PartsPurchaseOrderDetail>();
                for(int i = 0; i <= item.Count() - 1; i++) {
                    var partsPurchaseOrderDetail = item.ToList()[i];
                    partsPurchaseOrderDetails.Add(new PartsPurchaseOrderDetail() {
                        SerialNumber = i + 1,
                        SparePartId = partsPurchaseOrderDetail.SparePartId,
                        SparePartCode = partsPurchaseOrderDetail.SparePartCode,
                        SparePartName = partsPurchaseOrderDetail.SparePartName,
                        MeasureUnit = partsPurchaseOrderDetail.MeasureUnit,
                        PackingSpecification = partsPurchaseOrderDetail.PackingSpecification,
                        OrderAmount = partsPurchaseOrderDetail.OrderAmount,
                        UnitPrice = partsPurchaseOrderDetail.UnitPrice,
                        Remark = partsPurchaseOrderDetail.DetailRemark
                        //,POCode = partsPurchaseOrderDetail.POCode
                    });
                }
                var partsPurchaseOrder = new PartsPurchaseOrder() {
                    PartsSalesCategoryId = partsPurchaseOrderDetailExtend.PartsSalesCategoryId,
                    PartsSalesCategoryName = partsPurchaseOrderDetailExtend.PartsSalesCategoryName,
                    PartsPurchaseOrderTypeId = partsPurchaseOrderDetailExtend.PartsPurchaseOrderTypeId,
                    ShippingMethod = partsPurchaseOrderDetailExtend.ShippingMethod,
                    PlanSource = partsPurchaseOrderDetailExtend.PlanSource,
                    GPMSPurOrderCode = partsPurchaseOrderDetailExtend.GPMSPurOrderCode,
                    //SAPPurchasePlanCode = partsPurchaseOrderDetailExtend.SAPPurchasePlanCode,
                    RequestedDeliveryTime = partsPurchaseOrderDetailExtend.RequestedDeliveryTime,
                    WarehouseId = partsPurchaseOrderDetailExtend.WarehouseId,
                    WarehouseName = partsPurchaseOrderDetailExtend.WarehouseName,
                    PartsSupplierId = partsPurchaseOrderDetailExtend.PartsSupplierId,
                    PartsSupplierCode = partsPurchaseOrderDetailExtend.PartsSupplierCode,
                    InStatus = (int)DcsPurchaseInStatus.未入库,
                    PartsSupplierName = partsPurchaseOrderDetailExtend.PartsSupplierName,
                    BranchId = BaseApp.Current.CurrentUserData.EnterpriseId,
                    BranchCode = BaseApp.Current.CurrentUserData.EnterpriseCode,
                    BranchName = BaseApp.Current.CurrentUserData.EnterpriseName,
                    ReceivingCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId,
                    ReceivingCompanyName = BaseApp.Current.CurrentUserData.EnterpriseName,
                    Status = (int)DcsPartsPurchaseOrderStatus.新增,
                    IfDirectProvision = false,
                    Code = GlobalVar.ASSIGNED_BY_SERVER,
                    TotalAmount = partsPurchaseOrderDetails.Sum(entity => entity.OrderAmount * entity.UnitPrice),
                    Remark = item.Any(r => r.OrderRemark != null) ? item.First(r => r.OrderRemark != null).OrderRemark : null
                };
                foreach(var partsPurchaseOrderDetail in partsPurchaseOrderDetails)
                    partsPurchaseOrder.PartsPurchaseOrderDetails.Add(partsPurchaseOrderDetail);
                partsPurchaseOrders.Add(partsPurchaseOrder);
            }

            foreach(var partsPurchaseOrder in partsPurchaseOrders) {
                if(partsPurchaseOrder == null)
                    return;

                partsPurchaseOrder.ValidationErrors.Clear();
                foreach(var relation in partsPurchaseOrder.PartsPurchaseOrderDetails)
                    relation.ValidationErrors.Clear();

                if(string.IsNullOrEmpty(partsPurchaseOrder.WarehouseName))
                    partsPurchaseOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrder_WarehouseNameIsNull, new[] {
                        "WarehouseName"
                    }));

                if(partsPurchaseOrder.PartsPurchaseOrderTypeId <= 0)
                    partsPurchaseOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrder_OrderTypeIsNull, new[] {
                        "PartsPurchaseOrderTypeId"
                    }));
                //if(partsPurchaseOrder.PartsSalesCategory != null && partsPurchaseOrder.PartsSalesCategory.IsOverseas == true) {
                //    if(string.IsNullOrEmpty(partsPurchaseOrder.SAPPurchasePlanCode)) {
                //        partsPurchaseOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrder_SAPPurchasePlanCodeIsNull, new[] {
                //            "SAPPurchasePlanCode"
                //        }));
                //    }
                //}
                if(string.IsNullOrEmpty(partsPurchaseOrder.PartsSupplierCode))
                    partsPurchaseOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrder_PartsSupplierCodeIsNull, new[] {
                        "PartsSupplierCode"
                    }));

                if(!partsPurchaseOrder.PartsPurchaseOrderDetails.Any()) {
                    UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrderDetail_DetailIsNotNull));
                    return;
                }

                foreach(var relation in partsPurchaseOrder.PartsPurchaseOrderDetails) {
                    if(string.IsNullOrEmpty(relation.SparePartCode))
                        relation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrderDetail_SparePartCodeIsNull, new[] {
                            "SparePartCode"
                        }));

                    //if(string.IsNullOrEmpty(relation.SparePartName))
                    //    relation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrderDetail_SparePartNameIsNull, new[] {
                    //        "SparePartName"
                    //    }));
                }
                if(partsPurchaseOrder.HasValidationErrors || partsPurchaseOrder.PartsPurchaseOrderDetails.Any(relation => relation.HasValidationErrors))
                    return;

                foreach(var relation in partsPurchaseOrder.PartsPurchaseOrderDetails.Where(e => e.OrderAmount <= 0)) {
                    UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Error_SparePart_OrderAmountIsZero, relation.SparePartCode));
                    return;
                }
                if(!this.DomainContext.PartsPurchaseOrders.Contains(partsPurchaseOrder))
                    this.DomainContext.PartsPurchaseOrders.Add(partsPurchaseOrder);
                ((IEditableObject)partsPurchaseOrder).EndEdit();
                try {
                    if(this.EditState == DataEditState.New) {
                        if(partsPurchaseOrder.Can生成配件采购订单)
                            partsPurchaseOrder.生成配件采购订单();
                    } else {
                        if(partsPurchaseOrder.Can修改配件采购订单)
                            partsPurchaseOrder.修改配件采购订单();
                    }
                } catch(ValidationException ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                    return;
                }
            }
            if(this.PartsPurchaseOrderDetailExtends.Any())
                this.PartsPurchaseOrderDetailExtends.Clear();
            base.OnEditSubmitting();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitted() {
            if(this.PartsPurchaseOrderDetailExtends.Any())
                this.PartsPurchaseOrderDetailExtends.Clear();
        }

        protected override void OnEditCancelled() {
            if(this.PartsPurchaseOrderDetailExtends.Any())
                this.PartsPurchaseOrderDetailExtends.Clear();
        }

        private string strFileName;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private RadUpload uploader;
        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.ImportPartsPurchaseOrderDetailAsync(e.HandlerData.CustomData["Path"].ToString(), BaseApp.Current.CurrentUserData.EnterpriseId);
                        this.excelServiceClient.ImportPartsPurchaseOrderDetailCompleted -= this.ExcelServiceClient_ImportPartsPurchaseOrderDetailCompleted;
                        this.excelServiceClient.ImportPartsPurchaseOrderDetailCompleted += this.ExcelServiceClient_ImportPartsPurchaseOrderDetailCompleted;
                    };
                }
                return this.uploader;
            }
        }

        private void ExcelServiceClient_ImportPartsPurchaseOrderDetailCompleted(object sender, ImportPartsPurchaseOrderDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowAlertMessage(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
            if(this.PartsPurchaseOrderDetailExtends.Any())
                this.PartsPurchaseOrderDetailExtends.Clear();
            foreach(var detail in e.rightData) {
                PartsPurchaseOrderDetailExtends.Add(detail);
            }
        }


        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage(PartsPurchasingUIStrings.Upload_File_Tips);
                    e.Handled = true;
                }
            }
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }

        /// <summary>
        /// 文件上传成功后，调用该方法进行上传后的处理。不支持异步调用
        /// <param>上传文件名 <name>fileName</name> </param>
        /// </summary>
        protected Action<string> UploadFileSuccessedProcessing;

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //取当前登录用户的HashCode来标记上传文件的唯一性
                //this.UploadFileSuccessedProcessing.Invoke(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }
    }
}