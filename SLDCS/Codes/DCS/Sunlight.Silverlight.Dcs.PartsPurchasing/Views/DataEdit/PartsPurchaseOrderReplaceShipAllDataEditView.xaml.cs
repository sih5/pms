﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using System.Windows;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using Telerik.Windows.Data;
using System.Windows.Data;
using Telerik.Windows.Controls;
using System.Windows.Input;
using System.Collections.Generic;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class PartsPurchaseOrderReplaceShipAllDataEditView {
        private DcsDetailDataEditView partsPurchaseOrderDataEditView;
        public event EventHandler CustomEditSubmitted;
        //private ObservableCollection<PartsPurchaseOrderDetail> lartsPurchaseOrderDetails = new ObservableItemCollection<PartsPurchaseOrderDetail>();
        //private ObservableCollection<PartsPurchaseOrderDetail> PartsPurchaseOrderDetails = new ObservableCollection<PartsPurchaseOrderDetail>();

        #region 查询字段属性
        private string partsPurchaseOrderCode, sparePartCode, supplierPartCode, sparePartName, creatorName;
        private DateTime? tCreateTimeForm, tCreateTimeTo;
        private int? partsPurchaseOrderTypeId,orderWarehouseId;

        private string driver, deliveryBillNumber, phone, logisticCompany, vehicleLicensePlate, remark, partsSalesCategoryName, partsPurchaseOrderTypeName, shippingMethodName,orderWarehouseName;
        private DateTime? shippingRequestedDeliveryTime, planDeliveryTime;
        private int? partsSalesCategoryId, shippingMethod;
        private bool ifDirectProvision;
        #endregion

        private KeyValueManager keyValueManager;
        protected KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        #region 发运方式
        private readonly string[] kvNames = {
            "PartsShipping_Method"
        };

        private ObservableCollection<KeyValuePair> partsShippingMethods;
        public ObservableCollection<KeyValuePair> PartsShippingMethods {
            get {
                return this.partsShippingMethods ?? (this.partsShippingMethods = new ObservableCollection<KeyValuePair>());
            }
        }
        #endregion

        //品牌
        private ObservableCollection<KeyValuePair> kvPartsSalesCategories;
        public ObservableCollection<KeyValuePair> KvPartsSalesCategories {
            get {
                return this.kvPartsSalesCategories ?? (this.kvPartsSalesCategories = new ObservableCollection<KeyValuePair>());
            }
        }
        public int AllowPartsPurchaseOrderSelectionChange = 0;

        //订单类型
        private ObservableCollection<KeyValuePair> kvPartsPurchaseOrderType;
        public ObservableCollection<KeyValuePair> KvPartsPurchaseOrderTypes {
            get {
                return this.kvPartsPurchaseOrderType ?? (this.kvPartsPurchaseOrderType = new ObservableCollection<KeyValuePair>());
            }
        }

        //订货仓库
        private ObservableCollection<KeyValuePair> kvOrderWarehouses;
        public ObservableCollection<KeyValuePair> KvOrderWarehouses {
            get {
                return this.kvOrderWarehouses ?? (this.kvOrderWarehouses = new ObservableCollection<KeyValuePair>());
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public PartsPurchaseOrderReplaceShipAllDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += PartsPurchaseOrderReplaceShipAllDataEditView_DataContextChanged;
        }

        #region 界面事件
        private void PartsPurchaseOrderReplaceShipAllDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            //var dataGrid = sender as DcsDataGridViewBase;
            //if (dataGrid == null || dataGrid.SelectedEntities == null || !dataGrid.SelectedEntities.Any())
            //{
            //    this.radButtondShipping.IsEnabled = false;
            //    return;
            //}
            //this.radButtondShipping.IsEnabled = this.lartsPurchaseOrderDetails != null;

            var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
            if(partsPurchaseOrder == null)
                return;
            if(this.DomainContext == null)
                this.Initializer.Register(() => {
                    if(partsPurchaseOrder.Id == default(int) && this.gdShow.Visibility == Visibility.Collapsed)
                        this.HideSaveButton();
                    else
                        this.ShowSaveButton();
                });
            else {
                if(partsPurchaseOrder.Id == default(int) && this.gdShow.Visibility == Visibility.Collapsed)
                    this.HideSaveButton();
                else
                    this.ShowSaveButton();
            }
            this.gdSearch.Visibility = Visibility.Visible;
            this.gdShow.Visibility = Visibility.Collapsed;
            partsPurchaseOrder.PropertyChanged -= partsPurchaseOrder_PropertyChanged;
            partsPurchaseOrder.PropertyChanged += partsPurchaseOrder_PropertyChanged;
        }
        void partsPurchaseOrder_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
            if(partsPurchaseOrder == null)
                return;
        }
        //确定方法
        private void RadButtondShipping_Click(object sender, RoutedEventArgs e) {
            this.OnEditSubmitting();
        }
        //查询方法 ----已经正常
        private void RadButtonSearch_Click(object sender, RoutedEventArgs e) {
            var entityQuery = DomainContext.供应商人员查询待汇总发运采购订单信息Query(BaseApp.Current.CurrentUserData.EnterpriseId);
            //采购订单编号
            if(!string.IsNullOrEmpty(this.PartsPurchaseOrderCode)) {
                entityQuery = entityQuery.Where(ex => ex.PartsPurchaseOrderCode.Contains(this.PartsPurchaseOrderCode));
                this.partsPurchaseOrderCode = this.PartsPurchaseOrderCode;
            } else {
                this.partsPurchaseOrderCode = "";
            }
            //计划类型
            if(this.PartsPurchaseOrderTypeId.HasValue) {
                entityQuery = entityQuery.Where(ex => ex.PartsPurchaseOrderTypeId == this.PartsPurchaseOrderTypeId);
                this.partsPurchaseOrderTypeId = this.PartsPurchaseOrderTypeId;
            } else {
                this.partsPurchaseOrderTypeId = null;
            }
            //订货仓库
            if(this.orderWarehouseId.HasValue) {
                entityQuery = entityQuery.Where(ex => ex.WarehouseId == this.OrderWarehouseId);
                this.orderWarehouseId = this.OrderWarehouseId;
            } else {
                this.orderWarehouseId = null;
            }
            ////配件编号
            //if (!string.IsNullOrEmpty(this.SparePartCode))
            //{
            //    entityQuery = entityQuery.Where(ex => ex.SparePartCode.Contains(this.SparePartCode));
            //    this.sparePartCode = this.SparePartCode;
            //}
            //else
            //{
            //    this.sparePartCode = "";
            //}
            //供应商图号
            if(!string.IsNullOrEmpty(this.SupplierPartCode)) {
                entityQuery = entityQuery.Where(ex => ex.SupplierPartCode.Contains(this.SupplierPartCode));
                this.supplierPartCode = this.SupplierPartCode;
            } else {
                this.supplierPartCode = "";
            }
            //配件名称
            if(!string.IsNullOrEmpty(this.SparePartName)) {
                entityQuery = entityQuery.Where(ex => ex.SparePartName.Contains(this.SparePartName));
                this.sparePartName = this.SparePartName;
            } else {
                this.sparePartName = "";
            }  
            //创建人
            if(!string.IsNullOrEmpty(this.CreatorName)) {
                entityQuery = entityQuery.Where(ex => ex.CreatorName == this.CreatorName);
                this.creatorName = this.CreatorName;
            } else {
                this.creatorName = "";
            }
            //创建时间
            if(this.CreateTimeForm.HasValue) {
                this.CreateTimeForm = new DateTime(this.CreateTimeForm.Value.Year, this.CreateTimeForm.Value.Month, this.CreateTimeForm.Value.Day, 0, 00, 00);
                entityQuery = entityQuery.Where(ex => ex.CreateTime >= CreateTimeForm);
                this.tCreateTimeForm = this.CreateTimeForm;
            } else {
                this.tCreateTimeForm = null;
            }
            if(this.CreateTimeTo.HasValue) {
                this.CreateTimeTo = new DateTime(this.CreateTimeTo.Value.Year, this.CreateTimeTo.Value.Month, this.CreateTimeTo.Value.Day, 23, 59, 59);
                entityQuery = entityQuery.Where(ex => ex.CreateTime <= CreateTimeTo);
                this.tCreateTimeTo = this.CreateTimeTo;
            } else {
                this.tCreateTimeTo = null;
            }
            this.ifDirectProvision = this.txtIfDirectProvision.IsChecked.HasValue ? this.txtIfDirectProvision.IsChecked.Value : false;
            entityQuery = entityQuery.Where(ex => ex.IfDirectProvision == this.IfDirectProvision);
            this.ifDirectProvision = this.IfDirectProvision;

            this.tmppartsPurchaseOrderDetails.Clear();
            //this.lartsPurchaseOrderDetails.Clear();
            this.DomainContext.Load(entityQuery, LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowLoadError(loadOp);
                    return;
                }
                if(loadOp.Entities == null || !loadOp.Entities.Any())
                    return;
                //加载清单
                foreach(var OrderDetail in loadOp.Entities) {
                    var partsPurchaseOrderDetail = new PartsPurchaseOrderDetail();
                    partsPurchaseOrderDetail.PartsPurchaseOrderCode = OrderDetail.PartsPurchaseOrderCode;
                    partsPurchaseOrderDetail.SparePartId = OrderDetail.SparePartId;
                    partsPurchaseOrderDetail.SparePartCode = OrderDetail.SparePartCode;
                    partsPurchaseOrderDetail.SupplierPartCode = OrderDetail.SupplierPartCode;
                    partsPurchaseOrderDetail.SparePartName = OrderDetail.SparePartName;
                    partsPurchaseOrderDetail.MeasureUnit = OrderDetail.MeasureUnit;
                    partsPurchaseOrderDetail.OrderAmount = OrderDetail.OrderAmount;
                    partsPurchaseOrderDetail.ShippingAmount = OrderDetail.ShippingAmount;
                    partsPurchaseOrderDetail.UnShippingAmount = OrderDetail.OrderAmount - OrderDetail.ShippingAmount;
                    partsPurchaseOrderDetail.IfDirectProvision = OrderDetail.IfDirectProvision;//是否直供
                    partsPurchaseOrderDetail.MinPackingAmount = OrderDetail.MInPackingAmount;
                    partsPurchaseOrderDetail.PartsPurchaseOrderTypeName = OrderDetail.PartsPurchaseOrderTypeName;
                    partsPurchaseOrderDetail.TraceProperty = OrderDetail.TraceProperty;
                    //判断发运清单中是否存在数据，若存在 更新配件信息的本次发运数量
                    var tmppartsPurchaseOrderDetails1 = this.lartsPurchaseOrderDetails.Where(v => v.SparePartId == OrderDetail.SparePartId && v.PartsPurchaseOrderCode == OrderDetail.PartsPurchaseOrderCode);
                    if(tmppartsPurchaseOrderDetails1.Any())
                        foreach(var item in tmppartsPurchaseOrderDetails1) {
                            //item.UnShippingAmount = 0;
                            partsPurchaseOrderDetail.UnShippingAmount = OrderDetail.OrderAmount - OrderDetail.ShippingAmount - item.UnShippingAmount;
                            item.ShippingAmount = OrderDetail.ShippingAmount;
                            //item.ShippingAmount -= partsPurchaseOrderDetail.UnShippingAmount;
                        }
                    //若采购清单中的数量为0 则不将信息添加到采购清单中
                    if(Convert.ToInt32(partsPurchaseOrderDetail.UnShippingAmount.ToString()) > 0) {
                        //数据添加完成后再做汇总
                        this.tmppartsPurchaseOrderDetails.Add(partsPurchaseOrderDetail);
                    }
                }
                if(lartsPurchaseOrderDetails.Any(r => r.UnShippingAmount == 0)) {
                    foreach(var detail in this.lartsPurchaseOrderDetails.Where(r => r.UnShippingAmount == 0).ToArray()) {
                        this.lartsPurchaseOrderDetails.Remove(detail);
                    }
                }
            }, null);
        }
        #endregion

        //采购计划清单窗体
        private DataGridViewBase partsPurchaseOrderReplaceShipAllDetailDataGridView;
        private DataGridViewBase PartsPurchaseOrderReplaceShipAllDetailDataGridView {
            get {
                if(this.partsPurchaseOrderReplaceShipAllDetailDataGridView == null) {
                    this.partsPurchaseOrderReplaceShipAllDetailDataGridView = DI.GetDataGridView("PartsPurchaseOrderReplaceShipAllDetail");
                    this.partsPurchaseOrderReplaceShipAllDetailDataGridView.SelectionChanged -= partsPurchaseOrderReplaceShipAllDetailDataGridView_SelectionChanged;
                    this.partsPurchaseOrderReplaceShipAllDetailDataGridView.SelectionChanged += partsPurchaseOrderReplaceShipAllDetailDataGridView_SelectionChanged;
                    this.partsPurchaseOrderReplaceShipAllDetailDataGridView.DataContext = this;
                    this.partsPurchaseOrderReplaceShipAllDetailDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsPurchaseOrderReplaceShipAllDetailDataGridView;
            }
        }
        //采购计划勾选数据
        private string[] PartsPurchaseOrderCodeAdds = new string[] { };
        private int[] SparePartIdAdds = new int[] { };
        private void partsPurchaseOrderReplaceShipAllDetailDataGridView_SelectionChanged(object sender, EventArgs e) {
            if(partsPurchaseOrderReplaceShipAllDetailDataGridView.SelectedEntities != null && !partsPurchaseOrderReplaceShipAllDetailDataGridView.SelectedEntities.Any())
                return;
            if(this.partsPurchaseOrderReplaceShipAllDetailDataGridView.SelectedEntities != null && this.partsPurchaseOrderReplaceShipAllDetailDataGridView.SelectedEntities.Any()) {
                PartsPurchaseOrderCodeAdds = this.partsPurchaseOrderReplaceShipAllDetailDataGridView.SelectedEntities.Cast<PartsPurchaseOrderDetail>().Select(p => p.PartsPurchaseOrderCode).Distinct().ToArray();
                SparePartIdAdds = this.partsPurchaseOrderReplaceShipAllDetailDataGridView.SelectedEntities.Cast<PartsPurchaseOrderDetail>().Select(p => p.SparePartId).Distinct().ToArray();
            }
        }

        //发运清单窗体
        private DataGridViewBase partsPurchaseOrderReplaceShipDataGridView;
        private DataGridViewBase PartsPurchaseOrderReplaceShipDataGridView {
            get {
                if(this.partsPurchaseOrderReplaceShipDataGridView == null) {
                    this.partsPurchaseOrderReplaceShipDataGridView = DI.GetDataGridView("PartsPurchaseOrderReplaceShippingOrderDetail");
                    this.partsPurchaseOrderReplaceShipDataGridView.SelectionChanged -= partsPurchaseOrderReplaceShipDataGridView_SelectionChanged;
                    this.partsPurchaseOrderReplaceShipDataGridView.SelectionChanged += partsPurchaseOrderReplaceShipDataGridView_SelectionChanged;
                    this.partsPurchaseOrderReplaceShipDataGridView.DataContext = this;
                    this.partsPurchaseOrderReplaceShipDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsPurchaseOrderReplaceShipDataGridView;
            }
        }

        //发运清单勾选数据
        private string[] PartsPurchaseOrderCodeDeletes = new string[] { };
        private int[] SparePartIdDeletes = new int[] { };
        private void partsPurchaseOrderReplaceShipDataGridView_SelectionChanged(object sender, EventArgs e) {
            if(partsPurchaseOrderReplaceShipDataGridView.SelectedEntities != null && !partsPurchaseOrderReplaceShipDataGridView.SelectedEntities.Any())
                return;
            if(this.partsPurchaseOrderReplaceShipDataGridView.SelectedEntities != null && this.partsPurchaseOrderReplaceShipDataGridView.SelectedEntities.Any()) {
                PartsPurchaseOrderCodeDeletes = this.partsPurchaseOrderReplaceShipDataGridView.SelectedEntities.Cast<PartsPurchaseOrderDetail>().Select(p => p.PartsPurchaseOrderCode).Distinct().ToArray();
                SparePartIdDeletes = this.partsPurchaseOrderReplaceShipDataGridView.SelectedEntities.Cast<PartsPurchaseOrderDetail>().Select(p => p.SparePartId).Distinct().ToArray();
            }
        }

        //删除清单事件
        //1.发运清单删除后 直接回到采购清单中，无需判断数量及库存，默认全部发运
        private void PartsPurchaseOrderDetails_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e) {
            //var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
            //if (partsPurchaseOrder == null)
            //    return;

            if(e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove) {
                var currentRemovePartsPurchaseOrderDetails = e.OldItems.Cast<PartsPurchaseOrderDetail>();
                foreach(var currentRemoveEntity in currentRemovePartsPurchaseOrderDetails) {
                    if(!this.tmppartsPurchaseOrderDetails.Any(r => r.SparePartId == currentRemoveEntity.SparePartId && r.PartsPurchaseOrderCode == currentRemoveEntity.PartsPurchaseOrderCode)) {
                        this.tmppartsPurchaseOrderDetails.Add(lartsPurchaseOrderDetails.First(r => r.SparePartId == currentRemoveEntity.SparePartId && r.PartsPurchaseOrderCode == currentRemoveEntity.PartsPurchaseOrderCode));
                    } else {
                        var tmppartsPurchaseOrderDetails1 = this.tmppartsPurchaseOrderDetails.Where(v => v.SparePartId == currentRemoveEntity.SparePartId && v.PartsPurchaseOrderCode == currentRemoveEntity.PartsPurchaseOrderCode);
                        if(tmppartsPurchaseOrderDetails1.Any())
                            foreach(var partsPurchaseOrderDetail in tmppartsPurchaseOrderDetails1) {
                                partsPurchaseOrderDetail.UnShippingAmount += currentRemoveEntity.UnShippingAmount;
                                partsPurchaseOrderDetail.ShippingAmount -= currentRemoveEntity.UnShippingAmount;
                            }
                    }

                }
            }
        }

        //添加处理清单
        private void ProcessInternal() {
            this.PartsPurchaseOrderReplaceShipAllDetailDataGridView.CommitEdit();
            if(tmppartsPurchaseOrderDetails == null || tmppartsPurchaseOrderDetails.Count == 0)
                return;
            //this.lartsPurchaseOrderDetails.Clear();
            int diff = 0;
            //var tmppartsPurchaseOrderDetails1 = this.tmppartsPurchaseOrderDetails.Where(r => SparePartIdAdds.Contains(r.SparePartId) && PartsPurchaseOrderCodeAdds.Contains(r.PartsPurchaseOrderCode));
            var tmppartsPurchaseOrderDetails2 = this.partsPurchaseOrderReplaceShipAllDetailDataGridView.SelectedEntities.Cast<PartsPurchaseOrderDetail>();
            var partspurChaseCode = tmppartsPurchaseOrderDetails2.Select(r => r.PartsPurchaseOrderCode).ToArray();
            foreach(var selectedEntity in tmppartsPurchaseOrderDetails2) {
                //selectedEntity.UnShippingAmount = int.Parse(selectedEntity.OrderAmount.ToString()) - int.Parse(selectedEntity.ShippingAmount.ToString());
                if(selectedEntity.UnShippingAmount > 0) {
                    //紧急订单，特急订单一次只能选择一个单据 
                    if(lartsPurchaseOrderDetails.Count() > 0) {
                        var type = lartsPurchaseOrderDetails.First().PartsPurchaseOrderTypeName;
                        var code = lartsPurchaseOrderDetails.First().PartsPurchaseOrderCode;
                        if((type != "正常订单" && (type != selectedEntity.PartsPurchaseOrderTypeName || code != selectedEntity.PartsPurchaseOrderCode)) || (type == "正常订单" && selectedEntity.PartsPurchaseOrderTypeName != "正常订单")) {
                            UIHelper.ShowNotification("特急订单与紧急订单一次只能发运一个单据");
                            return;
                        }
                    }
                  
                    diff = 0;
                    diff = int.Parse(selectedEntity.OrderAmount.ToString()) - int.Parse(selectedEntity.ShippingAmount.ToString());
                    if(selectedEntity.UnShippingAmount > diff) {
                        UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Validation_ThisShippingAmountGreaterThan, selectedEntity.PartsPurchaseOrderCode, selectedEntity.SparePartCode));
                        return;
                    }
                    if (selectedEntity.PartsPurchaseOrderTypeName == "正常订单") { 
                        //本次发运数量必须是最小包装数量的整数倍
                        if (selectedEntity.UnShippingAmount % selectedEntity.MinPackingAmount != 0) {
                            UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Validation_ThisShippingAmountValidation, selectedEntity.PartsPurchaseOrderCode, selectedEntity.SparePartCode));
                            return;
                        }
                    }
                    var partsPurchaseOrderDetail = new PartsPurchaseOrderDetail {
                        PartsPurchaseOrderCode = selectedEntity.PartsPurchaseOrderCode,
                        SparePartId = selectedEntity.SparePartId,
                        SupplierPartCode = selectedEntity.SupplierPartCode,
                        SparePartCode = selectedEntity.SparePartCode,
                        SparePartName = selectedEntity.SparePartName,
                        OrderAmount = selectedEntity.OrderAmount,
                        ShippingAmount = selectedEntity.ShippingAmount,
                        MeasureUnit = selectedEntity.MeasureUnit,
                        UnShippingAmount = selectedEntity.UnShippingAmount,//本次发运量
                        IfDirectProvision = selectedEntity.IfDirectProvision,//是否直供
                        MinPackingAmount = selectedEntity.MinPackingAmount,
                        TraceProperty = selectedEntity.TraceProperty,
                        PartsPurchaseOrderTypeName = selectedEntity.PartsPurchaseOrderTypeName
                    };
                    if(!this.lartsPurchaseOrderDetails.Any(r => r.SparePartId == selectedEntity.SparePartId && r.PartsPurchaseOrderCode == selectedEntity.PartsPurchaseOrderCode)) {
                        this.lartsPurchaseOrderDetails.Add(partsPurchaseOrderDetail);
                        selectedEntity.UnShippingAmount = selectedEntity.OrderAmount - (int)selectedEntity.ShippingAmount - partsPurchaseOrderDetail.UnShippingAmount;
                    } else {
                        var lartsPurchaseOrderDetails1 = this.lartsPurchaseOrderDetails.Where(v => v.SparePartId == selectedEntity.SparePartId && v.PartsPurchaseOrderCode == selectedEntity.PartsPurchaseOrderCode);
                        if(lartsPurchaseOrderDetails1.Any())
                            foreach(var item in lartsPurchaseOrderDetails1) {
                                item.UnShippingAmount += partsPurchaseOrderDetail.UnShippingAmount;
                                if (item.UnShippingAmount> item.OrderAmount - (int)item.ShippingAmount)
                                {
                                    item.UnShippingAmount = item.OrderAmount - (int)item.ShippingAmount;
                                    selectedEntity.UnShippingAmount = 0;
                                }
                                else
                                    selectedEntity.UnShippingAmount = selectedEntity.OrderAmount - (int)selectedEntity.ShippingAmount - item.UnShippingAmount;

                            }
                    }
                    //this.lartsPurchaseOrderDetails.Add(partsPurchaseOrderDetail);
                    this.radButtondShipping.IsEnabled = true;
                }
            }
            if(tmppartsPurchaseOrderDetails.Any(r => r.UnShippingAmount == 0)) {
                foreach(var detail in this.tmppartsPurchaseOrderDetails.Where(r => r.UnShippingAmount == 0).ToArray()) {
                    this.tmppartsPurchaseOrderDetails.Remove(detail);
                }
            }
        }

        //删除发运清单
        private void ProcessDeletenal() {
            this.PartsPurchaseOrderReplaceShipDataGridView.CommitEdit();
            if(lartsPurchaseOrderDetails == null)
                return;
            //this.tmppartsPurchaseOrderDetails.Clear();
            int diff = 0;
            var lartsPurchaseOrderDetails1 = this.lartsPurchaseOrderDetails.Where(r => SparePartIdDeletes.Contains(r.SparePartId) && PartsPurchaseOrderCodeDeletes.Contains(r.PartsPurchaseOrderCode));
            foreach(var selectedEntity in lartsPurchaseOrderDetails1) {
                if(selectedEntity.UnShippingAmount > 0) {
                    diff = 0;
                    int count = 0;//用于判断是否删除发运清单
                    diff = int.Parse(selectedEntity.OrderAmount.ToString()) - int.Parse(selectedEntity.ShippingAmount.ToString());
                    //selectedEntity.UnShippingAmount = int.Parse(selectedEntity.OrderAmount.ToString()) - int.Parse(selectedEntity.ShippingAmount.ToString());
                    if(selectedEntity.UnShippingAmount > diff) {
                        selectedEntity.UnShippingAmount = diff;
                    }
                    var partsPurchaseOrderDetail = new PartsPurchaseOrderDetail {
                        PartsPurchaseOrderCode = selectedEntity.PartsPurchaseOrderCode,
                        SparePartId = selectedEntity.SparePartId,
                        SupplierPartCode = selectedEntity.SupplierPartCode,
                        SparePartCode = selectedEntity.SparePartCode,
                        SparePartName = selectedEntity.SparePartName,
                        OrderAmount = selectedEntity.OrderAmount,
                        ShippingAmount = selectedEntity.ShippingAmount,
                        MeasureUnit = selectedEntity.MeasureUnit,
                        UnShippingAmount = selectedEntity.UnShippingAmount,//本次发运量
                        IfDirectProvision = selectedEntity.IfDirectProvision,//是否直供
                        MinPackingAmount = selectedEntity.MinPackingAmount
                    };
                    count = partsPurchaseOrderDetail.UnShippingAmount;

                    if(!this.tmppartsPurchaseOrderDetails.Any(r => r.SparePartId == selectedEntity.SparePartId && r.PartsPurchaseOrderCode == selectedEntity.PartsPurchaseOrderCode)) {
                        this.tmppartsPurchaseOrderDetails.Add(partsPurchaseOrderDetail);
                    } else {
                        var tmppartsPurchaseOrderDetails1 = this.tmppartsPurchaseOrderDetails.Where(v => v.SparePartId == selectedEntity.SparePartId && v.PartsPurchaseOrderCode == selectedEntity.PartsPurchaseOrderCode);
                        if(tmppartsPurchaseOrderDetails1.Any())
                            foreach(var item in tmppartsPurchaseOrderDetails1) {
                                item.UnShippingAmount += partsPurchaseOrderDetail.UnShippingAmount;
                                //item.ShippingAmount -= partsPurchaseOrderDetail.UnShippingAmount;
                            }
                    }

                }
                selectedEntity.UnShippingAmount = 0;
            }
            if(lartsPurchaseOrderDetails.Any(r => r.UnShippingAmount == 0)) {
                foreach(var detail in this.lartsPurchaseOrderDetails.Where(r => r.UnShippingAmount == 0).ToArray()) {
                    this.lartsPurchaseOrderDetails.Remove(detail);
                }
                if(string.IsNullOrEmpty(this.lartsPurchaseOrderDetails.ToString())) {
                    this.radButtondShipping.IsEnabled = false;
                }
            }
        }

        //创建清单----已经正常
        private void CreatePartsPurchaseOrdersDetailGird(Grid grid, DcsDetailDataEditView detailView, DataGridViewBase dataGridView, bool defaultVisibility) {
            detailView.UnregisterButton(detailView.InsertButton);
            detailView.UnregisterButton(detailView.DeleteButton);
            detailView.Register(PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrderDetails, null, () => dataGridView);
            detailView.RegisterButton(new ButtonItem {
                Title = PartsPurchasingUIStrings.DataEditView_Title_AddToShippingDetails,
                Command = new DelegateCommand(this.ProcessInternal)
            }, true);

            dataGridView.SetValue(MinHeightProperty, Convert.ToDouble(170));
            dataGridView.SetValue(MaxHeightProperty, Convert.ToDouble(250));
            dataGridView.SetValue(MinWidthProperty, Convert.ToDouble(800));
            dataGridView.SetValue(MaxWidthProperty, Convert.ToDouble(1000));
            detailView.SetValue(Grid.RowProperty, 3);
            detailView.SetValue(Grid.VerticalAlignmentProperty, VerticalAlignment.Top);
            if(defaultVisibility) {
                detailView.SetValue(VisibilityProperty, Visibility.Collapsed);
            }
            grid.Children.Add(detailView);
        }

        private RadTabControl radTabControl;
        private RadTabControl RadTabControl {
            get {
                return this.radTabControl ?? (this.radTabControl = new RadTabControl());
            }
        }
        //创建UI-------已经正常
        private void CreateUI() {
            #region //创建行
            var tabItemsEditGrid = new Grid();
            tabItemsEditGrid.RowDefinitions.Add(new RowDefinition {
                Height = GridLength.Auto
            });
            tabItemsEditGrid.RowDefinitions.Add(new RowDefinition {
                Height = GridLength.Auto
            });
            tabItemsEditGrid.RowDefinitions.Add(new RowDefinition {
                Height = GridLength.Auto
            });
            tabItemsEditGrid.RowDefinitions.Add(new RowDefinition {
                Height = GridLength.Auto
            });
            tabItemsEditGrid.RowDefinitions.Add(new RowDefinition {
                Height = GridLength.Auto,
                MaxHeight = 200
            });
            tabItemsEditGrid.ColumnDefinitions.Add(new ColumnDefinition {
                Width = GridLength.Auto
            });
            tabItemsEditGrid.ColumnDefinitions.Add(new ColumnDefinition {
                Width = new GridLength(10)
            });
            tabItemsEditGrid.ColumnDefinitions.Add(new ColumnDefinition {
                Width = GridLength.Auto
            });
            #endregion
            //订单类型
            this.DomainContext.Load(this.DomainContext.GetPartsPurchaseOrderTypesQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效 /*&& v.PartsSalesCategoryId == partsPurchasePlan.PartsSalesCategoryId && v.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId*/), loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Entities.Count() > 0)
                    this.KvPartsPurchaseOrderTypes.Clear();
                foreach(var partsPurchaseOrderType in loadOp.Entities) {
                    this.KvPartsPurchaseOrderTypes.Add(new KeyValuePair {
                        Key = partsPurchaseOrderType.Id,
                        Value = partsPurchaseOrderType.Name
                    });
                }
                //if (!string.IsNullOrEmpty(this.PartsSalesCategoryId.ToString()))
                //{
                //    AllowPartsPurchaseOrderSelectionChange = int.Parse(this.PartsSalesCategoryId.ToString());
                //}
            }, null);
            //订货仓库
            this.DomainContext.Load(this.DomainContext.GetWarehousesQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效 && v.StorageCompanyType == (int)DcsCompanyType.分公司), loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Entities.Count() > 0)
                    this.KvOrderWarehouses.Clear();
                foreach(var warehouse in loadOp.Entities) {
                    this.KvOrderWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
                }
            }, null);

            //发运方式
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData(() => {
                foreach(var keyValuePair in this.KeyValueManager[this.kvNames[0]]) {
                    this.PartsShippingMethods.Add(keyValuePair);
                }
                this.txtShippingMethod.SelectedIndex = 0;//发运方式
            });
            //品牌
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Entities.Count() > 0)
                    this.KvPartsSalesCategories.Clear();
                foreach(var partsSalesCategory in loadOp.Entities) {
                    this.KvPartsSalesCategories.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
                }
                this.txtPartsSalesCategoryId.SelectedIndex = 0;//默认品牌
            }, null);
            this.radButtonSearch.Click += this.RadButtonSearch_Click;
            this.radButtondShipping.Click += this.RadButtondShipping_Click;

            this.textPartsPurchaseOrderCode.KeyDown += this.cusomerEdit_KeyDown;
            this.txtSupplierPartCode.KeyDown += this.cusomerEdit_KeyDown;
            this.txtCreatorName.KeyDown += this.cusomerEdit_KeyDown;
            this.txtSparePartName.KeyDown += this.cusomerEdit_KeyDown;
            //this.radButtondShipping.IsEnabled = false;
            //this.CreateTimeForm = DateTime.Now.Date.AddDays(1 - DateTime.Now.Day);
            //this.CreateTimeTo = DateTime.Now.Date;

            //配件清单界面
            this.gdShow.Children.Add(this.CreateVerticalLine(2));
            partsPurchaseOrderDataEditView = new DcsDetailDataEditView();
            CreatePartsPurchaseOrdersDetailGird(tabItemsEditGrid, partsPurchaseOrderDataEditView, this.PartsPurchaseOrderReplaceShipAllDetailDataGridView, false);

            var partsPurchaseOrderReplaceShipAllDataGridView = new DcsDetailDataEditView();
            partsPurchaseOrderReplaceShipAllDataGridView.UnregisterButton(partsPurchaseOrderReplaceShipAllDataGridView.InsertButton);
            partsPurchaseOrderReplaceShipAllDataGridView.UnregisterButton(partsPurchaseOrderReplaceShipAllDataGridView.DeleteButton);
            partsPurchaseOrderReplaceShipAllDataGridView.Register(PartsPurchasingUIStrings.DataEditView_Title_ShippingDetails, null, () => this.PartsPurchaseOrderReplaceShipDataGridView);
            partsPurchaseOrderReplaceShipAllDataGridView.RegisterButton(new ButtonItem {
                Title = PartsPurchasingUIStrings.DataEditView_Title_DeleteShippingDetails,
                Command = new DelegateCommand(this.ProcessDeletenal)
            }, true);
            partsPurchaseOrderReplaceShipAllDataGridView.SetValue(MinHeightProperty, Convert.ToDouble(170));
            partsPurchaseOrderReplaceShipAllDataGridView.SetValue(MaxHeightProperty, Convert.ToDouble(250));
            partsPurchaseOrderReplaceShipAllDataGridView.SetValue(MinWidthProperty, Convert.ToDouble(800));
            partsPurchaseOrderReplaceShipAllDataGridView.SetValue(MaxWidthProperty, Convert.ToDouble(1000));
            partsPurchaseOrderReplaceShipAllDataGridView.SetValue(Grid.RowProperty, 4);
            partsPurchaseOrderReplaceShipAllDataGridView.SetValue(Grid.ColumnSpanProperty, 3);
            tabItemsEditGrid.Children.Add(partsPurchaseOrderReplaceShipAllDataGridView);
            this.RadTabControl.Items.Add(new RadTabItem {
                Header = PartsPurchasingUIStrings.DataEditView_Title_ShippingAll,
                Content = tabItemsEditGrid
            });
            this.gdShow.Children.Add(this.RadTabControl);
            this.HideSaveButton();//隐藏确认按钮
        }
        //protected override bool OnRequestCanSubmit()
        //{
        //    return false;
        //}
        private void LoadEntityToEdit(int id) {
            this.ShowSaveButton();
            this.DomainContext.Load(this.DomainContext.GetPartsPurchaseOrdersWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.SetObjectToEdit(entity);
                this.gdSearch.Visibility = Visibility.Collapsed;
                this.gdShow.Visibility = Visibility.Collapsed;
                //this.gdShow.Visibility = Visibility.Visible;
            }, null);
        }

        protected override void OnEditSubmitting() {
            //1.判断必填项
            //2.获取所有采购订单编号
            //3.根据采购订单编号 查询所有采购订单具体信息
            //4.结合采购订单信息 填充供应商发运单结构
            //5.多个发运单 分别调用“生成供应商发运单”方法

            #region  //1.判断必填项
            if(this.lartsPurchaseOrderDetails == null || this.lartsPurchaseOrderDetails.Count == 0) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_ShippingDetailsIsNull);
                return;
            } else if(!this.ifDirectProvision ) {
                var detail = this.lartsPurchaseOrderDetails;
                var alldetail = new List<string>();
                foreach(var item in detail) {
                    if(item.TraceProperty.HasValue) {
                        if(string.IsNullOrEmpty(item.TraceCode)) {
                            UIHelper.ShowNotification("请填写" + item.SparePartCode + "的追溯码，并以分号隔开");
                            return;
                        }
                        var traceCode = item.TraceCode.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Distinct().ToArray(); 
                        if(traceCode.Length != item.UnShippingAmount && item.TraceProperty == (int)DCSTraceProperty.精确追溯) {
                            UIHelper.ShowNotification("追溯码数量不等于发运量");
                            return;
                        } else {
                            foreach(var code in traceCode) {
                                alldetail.Add(code);
                            }
                        }
                        if(item.TraceProperty == (int)DCSTraceProperty.批次追溯) {
                            item.TraceCode = traceCode[0];
                        }
                        if(traceCode.Any(t => t.Length != 17&&item.TraceProperty == (int)DCSTraceProperty.精确追溯)) {
                            UIHelper.ShowNotification("精确追溯的追溯码的长度必须是17");
                            return;
                        }
                        if(traceCode.Any(t => t.Length != 8 && item.TraceProperty == (int)DCSTraceProperty.批次追溯)) {
                            UIHelper.ShowNotification("批次追溯的追溯的追溯码的长度必须是8位");
                            return;
                        }
                    }
                }
                if(alldetail.ToArray().Length != alldetail.Distinct().ToArray().Length) {
                    UIHelper.ShowNotification("不同的配件存在相同的追溯码");
                    return;
                }
            }

            if(this.PartsSalesCategoryId == null) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_CategoryIsNull);
                return;
            }


            //if(this.ShippingMethod == null) {
            //    UIHelper.ShowNotification("发运方式不能为空");
            //    return;
            //}

            if(this.PlanDeliveryTime == null) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_ExpectDeliveryTimeIsNull);
                return;
            } else if(this.PlanDeliveryTime < DateTime.Now) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_ExpectDeliveryTimeError);
                return;
            }
            #endregion
            #region 作废----转移到服务端
            ////2.获取所有采购订单编号、配件ID
            //var partsPurchaseOrderCodes = lartsPurchaseOrderDetails.Select(r => r.PartsPurchaseOrderCode).ToArray();
            //var SparePartIds = lartsPurchaseOrderDetails.Select(r => r.SparePartId).ToArray();
            ////3.根据采购订单编号 查询所有采购订单具体信息
            //var domaincontext = this.DomainContext;
            //domaincontext.Load(domaincontext.供应商根据采购订单查询待汇总发运清单信息Query(partsPurchaseOrderCodes), loadOp =>
            //{
            //    if (loadOp.HasError)
            //        return;
            //    foreach (var partsPurchaseOrder in loadOp.Entities)
            //    {
            //        var supplierShippingOrder = new SupplierShippingOrder();
            //        //    return;
            //        ////查询并添加清单
            //        //Action addNewDetail = () =>
            //        //{
            //        supplierShippingOrder.Code = "<尚未生成>";
            //        supplierShippingOrder.BranchId = partsPurchaseOrder.BranchId;
            //        supplierShippingOrder.BranchCode = partsPurchaseOrder.BranchCode;
            //        supplierShippingOrder.BranchName = partsPurchaseOrder.BranchName;
            //        supplierShippingOrder.ReceivingWarehouseId = partsPurchaseOrder.WarehouseId;
            //        supplierShippingOrder.ReceivingWarehouseName = partsPurchaseOrder.WarehouseName;
            //        supplierShippingOrder.PartsPurchaseOrderId = partsPurchaseOrder.Id;
            //        supplierShippingOrder.PartsPurchaseOrderCode = partsPurchaseOrder.Code;
            //        supplierShippingOrder.GPMSPurOrderCode = partsPurchaseOrder.GPMSPurOrderCode;
            //        supplierShippingOrder.IfDirectProvision = partsPurchaseOrder.IfDirectProvision;
            //        supplierShippingOrder.PartsSupplierId = BaseApp.Current.CurrentUserData.EnterpriseId;
            //        supplierShippingOrder.PartsSupplierCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
            //        supplierShippingOrder.PartsSupplierName = BaseApp.Current.CurrentUserData.EnterpriseName;
            //        supplierShippingOrder.PartsSalesCategoryId = partsPurchaseOrder.PartsSalesCategoryId;
            //        supplierShippingOrder.PartsSalesCategoryName = partsPurchaseOrder.PartsSalesCategoryName;
            //        supplierShippingOrder.ReceivingAddress = partsPurchaseOrder.ReceivingAddress;
            //        supplierShippingOrder.PlanSource = partsPurchaseOrder.PlanSource;
            //        supplierShippingOrder.DirectRecWarehouseId = partsPurchaseOrder.DirectRecWarehouseId;
            //        supplierShippingOrder.DirectRecWarehouseName = partsPurchaseOrder.DirectRecWarehouseName;
            //        supplierShippingOrder.SAPPurchasePlanCode = partsPurchaseOrder.PurOrderCode;//SAP采购计划单号
            //        supplierShippingOrder.OriginalRequirementBillId = partsPurchaseOrder.OriginalRequirementBillId == default(int) ? partsPurchaseOrder.Id : partsPurchaseOrder.OriginalRequirementBillId.Value;
            //        supplierShippingOrder.RequestedDeliveryTime = this.ShippingRequestedDeliveryTime ?? partsPurchaseOrder.RequestedDeliveryTime;//要求到达时间
            //        supplierShippingOrder.ShippingMethod = this.shippingMethod ?? partsPurchaseOrder.ShippingMethod ?? 0;//发运方式
            //        supplierShippingOrder.Driver = this.Driver;//司机
            //        supplierShippingOrder.DeliveryBillNumber = this.DeliveryBillNumber;//送货单号
            //        supplierShippingOrder.Phone = this.Phone;//电话
            //        supplierShippingOrder.LogisticCompany = this.LogisticCompany;//物流公司
            //        supplierShippingOrder.VehicleLicensePlate = this.VehicleLicensePlate;//车牌号
            //        supplierShippingOrder.PlanDeliveryTime = this.PlanDeliveryTime ?? DateTime.Now;//预计到达时间
            //        supplierShippingOrder.Status = (int)DcsSupplierShippingOrderStatus.新建;

            //        if (partsPurchaseOrder.OriginalRequirementBillType == default(int))
            //        {
            //            supplierShippingOrder.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件采购订单;
            //        }
            //        else
            //        {
            //            supplierShippingOrder.OriginalRequirementBillType = partsPurchaseOrder.OriginalRequirementBillType.Value;
            //        }

            //        supplierShippingOrder.OriginalRequirementBillCode = partsPurchaseOrder.OriginalRequirementBillCode ?? partsPurchaseOrder.Code;
            //        //收货单位Id与名称
            //        if (supplierShippingOrder.IfDirectProvision)
            //        {
            //            supplierShippingOrder.ReceivingCompanyId = partsPurchaseOrder.ReceivingCompanyId;
            //            supplierShippingOrder.ReceivingCompanyName = partsPurchaseOrder.ReceivingCompanyName;
            //        }
            //        else
            //        {
            //            supplierShippingOrder.ReceivingCompanyId = partsPurchaseOrder.BranchId;
            //            supplierShippingOrder.ReceivingCompanyName = partsPurchaseOrder.BranchName;
            //        }
            //        var lartsPurchaseOrderDetailsForSup = lartsPurchaseOrderDetails.Where(r => r.PartsPurchaseOrderCode == supplierShippingOrder.PartsPurchaseOrderCode).ToArray();

            //        #region 作废
            //        //if (supplierShippingOrder.SupplierShippingDetails.Any())
            //        //{
            //        //    var supplierShippingDetails = supplierShippingOrder.SupplierShippingDetails.ToArray();
            //        //    var partsLogisticBatchBillDetails = supplierShippingOrder.PartsLogisticBatchBillDetails.ToArray();
            //        //    //this.PartsLogisticBatchItemDetailForEditDataGridView.CommitEdit();
            //        //    //this.SupplierShippingDetailForEditDataGridView.CommitEdit();
            //        //    foreach (var entity in supplierShippingDetails)
            //        //    {
            //        //        var batchItemDetails = partsLogisticBatchBillDetails.Where(ex => ex.PartsLogisticBatch != null).Select(ex => ex.PartsLogisticBatch).SelectMany(ex => ex.PartsLogisticBatchItemDetails).Where(ex => ex.SparePartId == entity.SparePartId);
            //        //        foreach (var batchItemDetail in batchItemDetails)
            //        //        {
            //        //            // 获取 配件物流批次 主单，删除对应配件发运清单的物流批次配件清单数据
            //        //            var logisticBatch = domaincontext.PartsLogisticBatches.SingleOrDefault(ex => ex.Id == batchItemDetail.PartsLogisticBatchId);
            //        //            if (logisticBatch != null && logisticBatch.PartsLogisticBatchItemDetails.Contains(batchItemDetail))
            //        //                logisticBatch.PartsLogisticBatchItemDetails.Remove(batchItemDetail);
            //        //        }
            //        //        supplierShippingOrder.SupplierShippingDetails.Remove(entity);
            //        //    }
            //        //}
            //        //domaincontext.Load(domaincontext.GetPartsPurchaseOrderDetailsQuery().Where(entity => entity.PartsPurchaseOrderId == partsPurchaseOrder.Id && ((entity.ShippingAmount.HasValue && entity.ShippingAmount.Value < entity.ConfirmedAmount) || (0 < entity.ConfirmedAmount))), LoadBehavior.RefreshCurrent, loadOp1 =>
            //        //{
            //        //    if (loadOp1.HasError)
            //        //    {
            //        //        if (!loadOp1.IsErrorHandled)
            //        //            loadOp1.MarkErrorAsHandled();
            //        //        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
            //        //        return;
            //        //    }
            //        //    if (supplierShippingOrder.SupplierShippingDetails.Any() && loadOp1.Entities.Any())
            //        //    {
            //        //        //this.SupplierShippingDetailForEditDataGridView.CommitEdit();
            //        //        foreach (var entity in supplierShippingOrder.SupplierShippingDetails)
            //        //            supplierShippingOrder.SupplierShippingDetails.Remove(entity);
            //        //    }
            //        //    var sparepartIds = loadOp1.Entities.Select(entity => entity.SparePartId).ToArray();
            //        //    domaincontext.Load(domaincontext.GetSparePartsWithPartsBranchByBranchIdAndPartIdsQuery(supplierShippingOrder.BranchId, sparepartIds), LoadBehavior.RefreshCurrent, loadOp2 =>
            //        //    {
            //        //        if (loadOp2.HasError)
            //        //        {
            //        //            if (!loadOp2.IsErrorHandled)
            //        //                loadOp2.MarkErrorAsHandled();
            //        //            DcsUtils.ShowDomainServiceOperationWindow(loadOp2);
            //        //            return;
            //        //        }
            //        //        foreach (var entity in loadOp1.Entities)
            //        //        {
            //        //            var shippingDetail = new SupplierShippingDetail
            //        //            {
            //        //                SerialNumber = supplierShippingOrder.SupplierShippingDetails.Any() ? supplierShippingOrder.SupplierShippingDetails.Max(detail => detail.SerialNumber) + 1 : 1,
            //        //                SparePartId = entity.SparePartId,
            //        //                SparePartCode = entity.SparePartCode,
            //        //                SparePartName = entity.SparePartName,
            //        //                SupplierPartCode = entity.SupplierPartCode,
            //        //                MeasureUnit = entity.MeasureUnit,
            //        //                POCode = entity.POCode,
            //        //                UnitPrice = entity.UnitPrice,
            //        //                ConfirmedAmount = 0//确认量默认0
            //        //            };
            //        //            //弹出窗体，选择配件返回界面时，计算待运量=配件采购清单.确认量-配件采购清单.发运量
            //        //            shippingDetail.PendingQuantity = entity.ConfirmedAmount - (entity.ShippingAmount.HasValue ? entity.ShippingAmount.Value : default(int));
            //        //            //if (shippingDetail.PendingQuantity == 0)//设计要求待运量为0 不需要显示
            //        //            //    continue;
            //        //            //然后再发运量 = 待运量
            //        //            shippingDetail.Quantity = shippingDetail.PendingQuantity;
            //        //            supplierShippingOrder.SupplierShippingDetails.Add(shippingDetail);
            //        //        }
            //        //    }, null);
            //        //}, null);
            //        //// };
            //        //if (supplierShippingOrder.HasValidationErrors || supplierShippingOrder.SupplierShippingDetails.Any(detail => detail.HasValidationErrors))
            //        //    return;
            //        //if (supplierShippingOrder.SupplierShippingDetails == null)
            //        //{
            //        //    UIHelper.ShowNotification("供应商发运单清单信息不能为空");
            //        //    return;
            //        //} 
            //        #endregion
            //        ((IEditableObject)supplierShippingOrder).EndEdit();
            //        try
            //        {
            //            //5.多个发运单 分别调用“生成供应商发运单”方法
            //            domaincontext.汇总生成供应商发运单(supplierShippingOrder, SparePartIds, lartsPurchaseOrderDetailsForSup, InvokeOp =>
            //            {
            //                if (InvokeOp.HasError)
            //                {
            //                    InvokeOp.MarkErrorAsHandled();
            //                    DcsUtils.ShowDomainServiceOperationWindow(InvokeOp);
            //                    return;
            //                }
            //                //base.OnEditSubmitting();
            //            }, null);
            //        }
            //        catch (ValidationException ex)
            //        {
            //            UIHelper.ShowNotification(ex.Message);
            //            return;
            //        }
            //    }

            //}, null);
            //base.OnEditSubmitting();
            ////UIHelper.ShowNotification("所有采购订单汇总发运完成！"); 
            #endregion
            try {
                //5.多个发运单 分别调用“生成供应商发运单”方法
                this.DomainContext.汇总生成供应商发运单(lartsPurchaseOrderDetails.ToArray(), BaseApp.Current.CurrentUserData.EnterpriseId, BaseApp.Current.CurrentUserData.EnterpriseCode, BaseApp.Current.CurrentUserData.EnterpriseName, this.ShippingRequestedDeliveryTime, this.shippingMethod, this.Driver, this.DeliveryBillNumber, this.Phone, this.LogisticCompany, this.VehicleLicensePlate, this.PlanDeliveryTime, this.ShippingRemark, InvokeOp => {
                    if(InvokeOp.HasError) {
                        InvokeOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(InvokeOp);
                        return;
                    }
                    DomainContext.RejectChanges();
                    //this.NotifyEditSubmitted();
                    base.OnEditSubmitting();
                }, null);
            } catch(ValidationException ex) {
                UIHelper.ShowNotification(ex.Message);
                return;
            }
        }
        private void NotifyEditSubmitted() {
            var handler = this.CustomEditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        protected override void OnEditCancelled() {
            this.gdShow.Visibility = Visibility.Visible;
            this.gdSearch.Visibility = Visibility.Visible;
            //采购查询信息
            this.textPartsPurchaseOrderCode.Text = "";
            this.txtSupplierPartCode.Text = "";
            this.txtSparePartName.Text = "";
            this.txtCreatorName.Text = "";
            this.txtPartsPurchaseOrderTypeId.Text = "";
            this.txtCreateTimeForm.DateTimeText = "";
            this.txtCreateTimeTo.DateTimeText = "";

            //发运主单信息
            this.txtShippingMethod.Text = "";
            this.txtPartsSalesCategoryId.Text = "";
            this.txtShippingRequestedDeliveryTime.DateTimeText = "";
            this.txtPhone.Text = "";
            this.txtDeliveryBillNumber.Text = "";
            this.txtDriver.Text = "";
            this.txtLogisticCompany.Text = "";
            this.txtShippingRemark.Text = "";
            this.txtPlanDeliveryTime.DateTimeText = "";
            this.txtVehicleLicensePlate.Text = "";
            //采购清单、发运清单容器清空
            this.LartsPurchaseOrderDetails.Clear();
            this.TmppartsPurchaseOrderDetails.Clear();
            base.OnEditCancelled();
        }

        protected override void OnEditSubmitted() {
            this.gdShow.Visibility = Visibility.Visible;
            this.gdSearch.Visibility = Visibility.Visible;
            //采购查询信息
            this.textPartsPurchaseOrderCode.Text = "";
            this.txtSupplierPartCode.Text = "";
            this.txtSparePartName.Text = "";
            this.txtCreatorName.Text = "";
            this.txtPartsPurchaseOrderTypeId.Text = "";
            this.txtCreateTimeForm.DateTimeText = "";
            this.txtCreateTimeTo.DateTimeText = "";

            //发运主单信息
            this.txtShippingMethod.Text = "";
            this.txtPartsSalesCategoryId.Text = "";
            this.txtShippingRequestedDeliveryTime.DateTimeText = "";
            this.txtPhone.Text = "";
            this.txtDeliveryBillNumber.Text = "";
            this.txtDriver.Text = "";
            this.txtLogisticCompany.Text = "";
            this.txtShippingRemark.Text = "";
            this.txtPlanDeliveryTime.DateTimeText = "";
            this.txtVehicleLicensePlate.Text = "";
            //采购清单、发运清单容器清空
            this.LartsPurchaseOrderDetails.Clear();
            this.TmppartsPurchaseOrderDetails.Clear();
            base.OnEditSubmitted();
        }
        //标题
        protected override string Title {
            get {
                return PartsPurchasingUIStrings.DataEditView_Title_SupplierShipping;
            }
        }

        #region 查询界面 字段设置 -----已经正常
        //1.采购订单号
        public string PartsPurchaseOrderCode {
            get {
                return this.partsPurchaseOrderCode;
            }
            set {
                this.partsPurchaseOrderCode = value;
                this.OnPropertyChanged("PartsPurchaseOrderCode");
            }
        }
        //2.计划类型ID
        public int? PartsPurchaseOrderTypeId {
            get {
                return this.partsPurchaseOrderTypeId;
            }
            set {
                this.partsPurchaseOrderTypeId = value;
                this.OnPropertyChanged("PartsPurchaseOrderTypeId");
            }
        }
        public string PartsPurchaseOrderTypeName {
            get {
                return this.partsPurchaseOrderTypeName;
            }
            set {
                this.partsPurchaseOrderTypeName = value;
                this.OnPropertyChanged("PartsPurchaseOrderTypeName");
            }
        }
        //3.供应商图号
        public string SupplierPartCode {
            get {
                return this.supplierPartCode;
            }
            set {
                this.supplierPartCode = value;
                this.OnPropertyChanged("SupplierPartCode");
            }
        }
        //3.配件编号
        public string SparePartCode {
            get {
                return this.sparePartCode;
            }
            set {
                this.sparePartCode = value;
                this.OnPropertyChanged("SparePartCode");
            }
        }
        //4.配件名称
        public string SparePartName {
            get {
                return this.sparePartName;
            }
            set {
                this.sparePartName = value;
                this.OnPropertyChanged("SparePartName");
            }
        }
        //5.创建人
        public string CreatorName {
            get {
                return this.creatorName;
            }
            set {
                this.creatorName = value;
                this.OnPropertyChanged("CreatorName");
            }
        }
        //6.创建时间
        public DateTime? CreateTimeForm {
            get {
                return this.tCreateTimeForm;
            }
            set {
                this.tCreateTimeForm = value;
                this.OnPropertyChanged("CreateTimeForm");
            }
        }
        public DateTime? CreateTimeTo {
            get {
                return this.tCreateTimeTo;
            }
            set {
                this.tCreateTimeTo = value;
                this.OnPropertyChanged("CreateTimeTo");
            }
        }

        //1.品牌
        public int? PartsSalesCategoryId {
            get {
                return this.partsSalesCategoryId;
            }
            set {
                this.partsSalesCategoryId = value;
                this.OnPropertyChanged("PartsSalesCategoryId");
            }
        }
        public string PartsSalesCategoryName {
            get {
                return this.partsSalesCategoryName;
            }
            set {
                this.partsSalesCategoryName = value;
                this.OnPropertyChanged("PartsSalesCategoryName");
            }
        }

        //2.发运方式
        public int? ShippingMethod {
            get {
                return this.shippingMethod;
            }
            set {
                this.shippingMethod = value;
                this.OnPropertyChanged("ShippingMethod");
            }
        }
        public string ShippingMethodName {
            get {
                return this.shippingMethodName;
            }
            set {
                this.shippingMethodName = value;
                this.OnPropertyChanged("ShippingMethodName");
            }
        }

        //是否直供
        public bool IfDirectProvision {
            get {
                return this.ifDirectProvision;
            }
            set {
                this.ifDirectProvision = value;
                this.OnPropertyChanged("IfDirectProvision");
            }
        }

        //.订货仓库Id
        public int? OrderWarehouseId {
            get {
                return this.orderWarehouseId;
            }
            set {
                this.orderWarehouseId = value;
                this.OnPropertyChanged("OrderWarehouseId");
            }
        }

        //.订货仓库Name
        public string OrderWarehouseName {
            get {
                return this.orderWarehouseName;
            }
            set {
                this.orderWarehouseName = value;
                this.OnPropertyChanged("OrderWarehouseName");
            }
        }

        //3.司机
        public string Driver {
            get {
                return this.driver;
            }
            set {
                this.driver = value;
                this.OnPropertyChanged("Driver");
            }
        }
        //4.送货单号
        public string DeliveryBillNumber {
            get {
                return this.deliveryBillNumber;
            }
            set {
                this.deliveryBillNumber = value;
                this.OnPropertyChanged("DeliveryBillNumber");
            }
        }
        //5.电话
        public string Phone {
            get {
                return this.phone;
            }
            set {
                this.phone = value;
                this.OnPropertyChanged("Phone");
            }
        }
        //6.物流公司
        public string LogisticCompany {
            get {
                return this.logisticCompany;
            }
            set {
                this.logisticCompany = value;
                this.OnPropertyChanged("LogisticCompany");
            }
        }
        //7.车牌号
        public string VehicleLicensePlate {
            get {
                return this.vehicleLicensePlate;
            }
            set {
                this.vehicleLicensePlate = value;
                this.OnPropertyChanged("VehicleLicensePlate");
            }
        }
        //8.备注
        public string ShippingRemark {
            get {
                return this.remark;
            }
            set {
                this.remark = value;
                this.OnPropertyChanged("ShippingRemark");
            }
        }
        //9.要求到货时间
        public DateTime? ShippingRequestedDeliveryTime {
            get {
                return this.shippingRequestedDeliveryTime;
            }
            set {
                this.shippingRequestedDeliveryTime = value;
                this.OnPropertyChanged("ShippingRequestedDeliveryTime");
            }
        }
        //10.预计到货时间
        public DateTime? PlanDeliveryTime {
            get {
                return this.planDeliveryTime;
            }
            set {
                this.planDeliveryTime = value;
                this.OnPropertyChanged("PlanDeliveryTime");
            }
        }

        //采购清单结构
        private ObservableCollection<PartsPurchaseOrderDetail> tmppartsPurchaseOrderDetails;
        public ObservableCollection<PartsPurchaseOrderDetail> TmppartsPurchaseOrderDetails {
            get {
                return this.tmppartsPurchaseOrderDetails ?? (this.tmppartsPurchaseOrderDetails = new ObservableCollection<PartsPurchaseOrderDetail>());
            }
            set {
                this.tmppartsPurchaseOrderDetails = value;
                this.OnPropertyChanged("PartsPurchaseOrderDetails");
            }
        }
        //发运清单结构
        private ObservableCollection<PartsPurchaseOrderDetail> lartsPurchaseOrderDetails;
        public ObservableCollection<PartsPurchaseOrderDetail> LartsPurchaseOrderDetails {
            get {
                return this.lartsPurchaseOrderDetails ?? (this.lartsPurchaseOrderDetails = new ObservableCollection<PartsPurchaseOrderDetail>());
            }
            set {
                this.lartsPurchaseOrderDetails = value;
                this.OnPropertyChanged("PartsPurchaseOrderDetails");
            }
        }
        #endregion

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void cusomerEdit_KeyDown(object sender, KeyEventArgs e) {
            if(e.Key == Key.Enter) {
                this.sparePartCode = this.txtSupplierPartCode.Text;
                this.sparePartName = this.txtSparePartName.Text;
                this.partsPurchaseOrderCode = this.textPartsPurchaseOrderCode.Text;
                this.creatorName = this.txtCreatorName.Text;
                this.supplierPartCode = this.txtSupplierPartCode.Text;
                if(!string.IsNullOrEmpty(this.txtPartsPurchaseOrderTypeId.Text)) {
                    this.partsPurchaseOrderTypeId = int.Parse(this.txtPartsPurchaseOrderTypeId.SelectedValue.ToString());
                }
                if(!string.IsNullOrEmpty(this.txtCreateTimeForm.DateTimeText)) {
                    this.tCreateTimeForm = DateTime.Parse(this.txtCreateTimeForm.DateTimeText);
                }
                if(!string.IsNullOrEmpty(this.txtCreateTimeTo.DateTimeText)) {
                    this.tCreateTimeTo = DateTime.Parse(this.txtCreateTimeTo.DateTimeText);
                }
                this.RadButtonSearch_Click(sender, e);
            }
        }

        private void AllWindow_Loaded(object sender, RoutedEventArgs e) {
            this.txtPartsSalesCategoryId.SelectedIndex = 0;//默认品牌
            this.txtShippingMethod.SelectedIndex = 0;//发运方式
            this.DomainContext.Load(this.DomainContext.GetSupplierRelationUsrinfoEnterpriseQuery(), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var branchSupplierRelation in loadOp.Entities) {
                    if(!this.PlanDeliveryTime.HasValue) {
                        this.txtPlanDeliveryTime.SelectedDate = DateTime.Now.Date.AddDays(branchSupplierRelation.ArrivalCycle ?? 0);
                    }
                }
            }, null);
        }
    }
}
