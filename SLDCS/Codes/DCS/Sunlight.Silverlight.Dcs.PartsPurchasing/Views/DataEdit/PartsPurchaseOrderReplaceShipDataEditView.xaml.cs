﻿﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class PartsPurchaseOrderReplaceShipDataEditView {
        private DataGridViewBase supplierShippingDetailForEditDataGridView;
        private KeyValueManager keyValueManager;
        private readonly ObservableCollection<Branch> kvBranches = new ObservableCollection<Branch>();

        private readonly string[] kvNames = {
            "PartsShipping_Method"
        };

        public PartsPurchaseOrderReplaceShipDataEditView() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.LoadData();
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private DataGridViewBase SupplierShippingDetailForEditDataGridView {
            get {
                if(this.supplierShippingDetailForEditDataGridView == null) {
                    this.supplierShippingDetailForEditDataGridView = DI.GetDataGridView("SupplierShippingDetailForEdit");
                    this.supplierShippingDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.supplierShippingDetailForEditDataGridView;
            }
        }

        protected virtual void CreateUI() {
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(SupplierShippingOrder), "SupplierShippingDetails"), null, () => this.SupplierShippingDetailForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);
            cbShippingMethod.ItemsSource = KvShippingMethods;
            this.cbShippingMethod.SelectedItem = 0;//发运方式默认0
        }

        ////加载收货仓库
        //private void LoadWarehouse(int branchId) {
        //    if(branchId == default(int))
        //        return;
        //    this.DomainContext.Load(this.DomainContext.GetWarehousesOrderByNameQuery().Where(entity => entity.Status == (int)DcsBaseDataStatus.有效 && entity.BranchId == branchId), LoadBehavior.KeepCurrent, loadOp => {
        //        if(loadOp.HasError)
        //            return;
        //        cbReceivingWarehouse.ItemsSource = loadOp.Entities;
        //    }, null);
        //}

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsPurchaseOrdersWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.SetObjectToEdit(entity);
            }, null);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_SupplierShippingOrder;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.SupplierShippingDetailForEditDataGridView.CommitEdit())
                return;
            var supplierShippingOrder = this.DataContext as SupplierShippingOrder;
            if(supplierShippingOrder == null)
                return;
            supplierShippingOrder.ValidationErrors.Clear();
            foreach(var item in supplierShippingOrder.SupplierShippingDetails) {
                item.ValidationErrors.Clear();
            }

            if(!supplierShippingOrder.SupplierShippingDetails.Any()) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_SupplierShippingOrder_SupplierShippingOrderDetailIsNull);
                return;
            } else if(!supplierShippingOrder.IfDirectProvision) {
                var detail = supplierShippingOrder.SupplierShippingDetails;
                var alldetail = new List<string>();
                foreach(var item in detail) {
                    if(item.TraceProperty.HasValue) {
                        if(string.IsNullOrEmpty(item.TraceCode)) {
                            UIHelper.ShowNotification("请填写" + item.SparePartCode + "的追溯码，并以分号隔开");
                            return;
                        }
                        var traceCode = item.TraceCode.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Distinct().ToArray(); 
                        if(traceCode.Length != item.Quantity && item.TraceProperty == (int)DCSTraceProperty.精确追溯) {
                            UIHelper.ShowNotification("追溯码数量不等于发运量");
                            return;
                        } else {
                            foreach(var code in traceCode) {
                                alldetail.Add(code);
                            }
                        }
                        if(traceCode.Any(t => t.Length != 17 && item.TraceProperty == (int)DCSTraceProperty.精确追溯)) {
                            UIHelper.ShowNotification("精确追溯的追溯码的长度必须是17");
                            return;
                        }
                        if(traceCode.Any(t => t.Length != 8 && item.TraceProperty == (int)DCSTraceProperty.批次追溯)) {
                            UIHelper.ShowNotification("批次追溯的追溯的追溯码的长度必须是8位");
                            return;
                        }
                    }
                }
                if(alldetail.ToArray().Length != alldetail.Distinct().ToArray().Length) {
                    UIHelper.ShowNotification("不同的配件存在相同的追溯码");
                    return;
                }
            }
            if(supplierShippingOrder.BranchId == 0)
                supplierShippingOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_SupplierShippingOrder_BranchIdIsNull, new[] {
                    "BranchId"
                }));

            if(string.IsNullOrWhiteSpace(supplierShippingOrder.PartsPurchaseOrderCode))
                supplierShippingOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_LaborHourUnitPriceGrade_PartsPurchaseOrderCodeIsNull, new[] {
                    "PartsPurchaseOrderCode"
                }));

            if(supplierShippingOrder.ReceivingWarehouseId == default(int))
                supplierShippingOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_SupplierShippingOrder_ReceivingWarehouseIdIsNull, new[] {
                    "ReceivingWarehouseId"
                }));

            if(supplierShippingOrder.ShippingDate < DateTime.Today)
                supplierShippingOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_SupplierShippingOrder_ShippingDateGreaterThanToday, new[] {
                    "ShippingDate"
                }));
            if(supplierShippingOrder.IsMust && string.IsNullOrEmpty(supplierShippingOrder.LogisticCompany)) {
                supplierShippingOrder.ValidationErrors.Add(new ValidationResult("物流公司不允许为空", new[] {
                    "LogisticCompany"
                }));
            }
            if(supplierShippingOrder.IsMust && string.IsNullOrEmpty(supplierShippingOrder.DeliveryBillNumber)) {
                supplierShippingOrder.ValidationErrors.Add(new ValidationResult("送货单号不允许为空", new[] {
                    "DeliveryBillNumber"
                }));
            }
            if(supplierShippingOrder.SupplierShippingDetails.Any(e => e.Quantity <= 0)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_SupplierShippingDetail_QuantityLessThanZero);
                return;
            }
            foreach(var items in supplierShippingOrder.SupplierShippingDetails.Where(e => e.PendingQuantity < e.Quantity)) {
                UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Validation_SupplierShippingDetail_QuantityIsInvalidation, items.SparePartCode));
                return;
            }
            if(supplierShippingOrder.SupplierShippingDetails.Any(supplierShippingDetail => supplierShippingOrder.SupplierShippingDetails.Count(item => item.SparePartId == supplierShippingDetail.SparePartId) > 1)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataGridView_Validation_SupplierShippingDetail_SparePartCodeCanNotRepeat);
                return;
            }
            if(supplierShippingOrder.IfDirectProvision ) {

            }
            if(supplierShippingOrder.HasValidationErrors || supplierShippingOrder.SupplierShippingDetails.Any(detail => detail.HasValidationErrors))
                return;
            ((IEditableObject)supplierShippingOrder).EndEdit();
            if(supplierShippingOrder.Can生成供应商发运单)
                supplierShippingOrder.生成供应商发运单();
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public ObservableCollection<Branch> KvBranches {
            get {
                return this.kvBranches;
            }
        }

        public ObservableCollection<KeyValuePair> KvShippingMethods {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
    }
}
