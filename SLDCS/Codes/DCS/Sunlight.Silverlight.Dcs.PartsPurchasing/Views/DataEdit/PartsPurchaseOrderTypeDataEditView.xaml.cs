﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class PartsPurchaseOrderTypeDataEditView {
        private ObservableCollection<KeyValuePair> kvPartsSalesCategoryNames = new ObservableCollection<KeyValuePair>();

        public ObservableCollection<KeyValuePair> KvPartsSalesCategoryNames {
            get {
                return this.kvPartsSalesCategoryNames;
            }
        }

        public PartsPurchaseOrderTypeDataEditView() {
            InitializeComponent();
            this.Loaded += PartsPurchasePricingChangeDataEditView_Loaded;
            this.Initializer.Register(this.CreateUI);
        }
        private void CreateUI() {
            this.isConent = true;
        }
        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsPurchaseOrderType;
            }
        }

        public void PartsPurchasePricingChangeDataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesWithBranchIdQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvPartsSalesCategoryNames.Clear();
                foreach(var partsSalesCategory in loadOp.Entities) {
                    this.KvPartsSalesCategoryNames.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name,
                        UserObject = partsSalesCategory
                    });
                }
                this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            }, null);
        }

        protected override void OnEditSubmitting() {
            var partsPurchaseOrderType = this.DataContext as PartsPurchaseOrderType;
            if(partsPurchaseOrderType == null)
                return;
            partsPurchaseOrderType.ValidationErrors.Clear();
            if(string.IsNullOrWhiteSpace(partsPurchaseOrderType.Code))
                partsPurchaseOrderType.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrderType_CodeIsNotNull, new[] { "Code" }));

            if(partsPurchaseOrderType.PartsSalesCategoryId == default(int))
                partsPurchaseOrderType.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrderType_PartsSalesCategoryIdIsNotNull, new[] { "PartsSalesCategoryId" }));

            if(string.IsNullOrWhiteSpace(partsPurchaseOrderType.Name))
                partsPurchaseOrderType.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrderType_NameIsNotNull, new[] { "Name" }));

            if(partsPurchaseOrderType.HasValidationErrors)
                return;

            ((IEditableObject)partsPurchaseOrderType).EndEdit();
            base.OnEditSubmitting();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsPurchaseOrderTypesQuery().Where(entity => entity.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
