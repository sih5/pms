﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Sunlight.Silverlight.ViewModel;
using System.Windows.Browser;
using System.Collections.Generic;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class PartsPurchasePlanDetailDataEditView {
        private DataGridViewBase partsPurchasePlanDetailForEditDataGridView;
        private KeyValueManager keyValueManager;

        private DataGridViewBase PartsPurchasePlanDetailForEditDataGridView {
            get {
                if(this.partsPurchasePlanDetailForEditDataGridView == null) {
                    this.partsPurchasePlanDetailForEditDataGridView = DI.GetDataGridView("PartsPurchasePlanOrderDetailForEdit");
                    this.partsPurchasePlanDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsPurchasePlanDetailForEditDataGridView;
            }
        }

        public PartsPurchasePlanDetailDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
            //this.DataContextChanged += PartsOuterPurchaseBillReportForApproveDataEditView_DataContextChanged;
        }
        private void CreateUI() {
            //附件
            this.DataEditPanels.SetValue(Grid.RowProperty, 5);
            this.DataEditPanels.SetValue(Grid.ColumnProperty, 0);
            this.DataEditPanels.isHiddenButtons = true;
            this.DataEditPanels.Margin = new Thickness(0, 200, 0, 0);
            this.DataEditPanels.HorizontalAlignment = HorizontalAlignment.Left;
            this.DataEditPanels.VerticalAlignment = VerticalAlignment.Top;
            this.DataEditPanels.SetValue(Grid.ColumnSpanProperty, 2);
            this.Root.Children.Add(DataEditPanels);

            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsPurchasePlan), PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrderPlanDetails /*"PartsPurchasePlanDetails"*/), null, this.PartsPurchasePlanDetailForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            this.Root.Children.Add(detailDataEditView);

            KeyValueManager.Register(this.kvNames);
            this.HideSaveButton();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsPurchasePlansWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    var sparePartIds = entity.PartsPurchasePlanDetails.Select(o => o.SparePartId).Distinct().ToArray();
                    DataEditPanels.FilePath = entity.Path;
                    this.DomainContext.Load(this.DomainContext.获取当前企业的配件营销包装属性Query(entity.PartsSalesCategoryId.Value, entity.BranchId, sparePartIds), LoadBehavior.RefreshCurrent, loadOp2 => {
                        if(loadOp2.HasError) {
                            if(!loadOp2.IsErrorHandled) {
                                loadOp2.MarkErrorAsHandled();
                                return;
                            }
                        }
                        if(loadOp2.Entities != null && loadOp2.Entities.Any()) {
                            var partsBranchPackingProps = loadOp2.Entities.ToArray();
                            foreach(var data in entity.PartsPurchasePlanDetails) {
                                var partsBranchPackingProp = partsBranchPackingProps.FirstOrDefault(o => o.SparePartId == data.SparePartId);
                                data.PackingCoefficient = partsBranchPackingProp.PackingCoefficient.Value;
                            }
                        }
                    }, null);
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        private readonly string[] kvNames = {
           "PartsPurchaseOrder_OrderType"
        };

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private FileUploadForPartsOuterPurchaseDataEditPanel productDataEditPanels;

        protected FileUploadForPartsOuterPurchaseDataEditPanel DataEditPanels {
            get {
                return this.productDataEditPanels ?? (this.productDataEditPanels = (FileUploadForPartsOuterPurchaseDataEditPanel)DI.GetDataEditPanel("FileUploadForPartsOuterPurchase"));
            }
        }

        protected override string Title {
            get {
                return PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrderPlanInfo;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
