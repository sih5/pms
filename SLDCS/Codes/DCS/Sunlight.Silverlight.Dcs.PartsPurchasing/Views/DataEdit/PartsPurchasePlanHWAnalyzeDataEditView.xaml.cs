﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.PartsStocking.Views;
using System.Windows.Browser;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class PartsPurchasePlanHWAnalyzeDataEditView {
        public PartsPurchasePlanHWAnalyzeDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData();
            this.Initializer.Register(this.CreateUI);
        }

        private KeyValueManager keyValueManager;

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private string[] kvNames ={
            "PurchasePlanType"
        };

        private RadWindow radPartsTransferOrderWindow;
        private RadWindow RadPartsTransferOrderWindow {
            get {
                return this.radPartsTransferOrderWindow ?? (this.radPartsTransferOrderWindow = new RadWindow {
                    Content = this.PartsTransferOrderDataEdit,
                    Header = PartsPurchasingUIStrings.DataEditView_Title_PartsTransferOrder_Add,
                    WindowState = WindowState.Normal,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                    AllowDrop = true,
                    ResizeMode = ResizeMode.CanResize,
                    CanClose = false,
                    Width = 1300,
                    Height = 700
                });
            }
        }

        private PartsTransferOrderDataEditView partsTransferOrderDataEdit;
        private PartsTransferOrderDataEditView PartsTransferOrderDataEdit {
            get {
                if(partsTransferOrderDataEdit == null) {
                    partsTransferOrderDataEdit = (PartsTransferOrderDataEditView)DI.GetDataEditView("PartsTransferOrder");
                    partsTransferOrderDataEdit.Loaded += PartsTransferOrderDataEdit_Loaded;
                    partsTransferOrderDataEdit.EditCancelled += partsTransferOrderDataEdit_EditCancelled;
                    partsTransferOrderDataEdit.EditSubmitted += DataEdit_EditSubmitted;
                }
                return partsTransferOrderDataEdit;
            }
        }

        private void PartsTransferOrderDataEdit_Loaded(object sender, RoutedEventArgs e) {
            var entity = this.DataContext as PartsPurchasePlan_HW;
            PartsTransferOrderDataEdit.PurOrderId = entity.Id;
        }

        private RadWindow radPartsPurchaseOrderWindow;
        private RadWindow RadPartsPurchaseOrderWindow {
            get {
                return this.radPartsPurchaseOrderWindow ?? (this.radPartsPurchaseOrderWindow = new RadWindow {
                    Content = this.PartsPurchaseOrderDataEdit,
                    Header = PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_Add,
                    WindowState = WindowState.Normal,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                    AllowDrop = true,
                    ResizeMode = ResizeMode.CanResize,
                    CanClose = false,
                    Width = 1200,
                    Height = 700
                });
            }
        }

        private DcsDataEditViewBase partsPurchaseOrderDataEdit;
        private DcsDataEditViewBase PartsPurchaseOrderDataEdit {
            get {
                if(radPartsPurchaseOrderWindow == null) {
                    partsPurchaseOrderDataEdit = DI.GetDataEditView("PartsPurchaseOrder1") as DcsDataEditViewBase;
                    partsPurchaseOrderDataEdit.Loaded += partsPurchaseOrderDataEdit_Loaded;
                    partsPurchaseOrderDataEdit.EditCancelled += partsPurchaseOrderDataEdit_EditCancelled;
                    partsPurchaseOrderDataEdit.EditSubmitted += DataEdit_EditSubmitted;
                }
                return partsPurchaseOrderDataEdit;
            }

        }

        void partsPurchaseOrderDataEdit_EditCancelled(object sender, EventArgs e) {
            RadPartsPurchaseOrderWindow.Close();
        }


        void DataEdit_EditSubmitted(object sender, EventArgs e) {
            //var entity = this.DataContext as PartsPurchasePlan_HW;

            //ShellViewModel.Current.IsBusy = true;
            //var partsTransferOrder = PartsTransferOrderDataEdit.DataContext as PartsTransferOrder;
            //if(entity.Can更新采购计划状态) {
            //    entity.更新采购计划状态(true);
            //}
            //var domainContext = this.DomainContext;
            //if(domainContext == null)
            //    return;
            //domainContext.SubmitChanges(submitOp => {
            //    if(submitOp.HasError) {
            //        if(!submitOp.IsErrorHandled)
            //            submitOp.MarkErrorAsHandled();
            //        DcsUtils.ShowDomainServiceOperationWindow(submitOp);
            //        domainContext.RejectChanges();
            //        return;
            //    }
            //    ShellViewModel.Current.IsBusy = false;
            //    this.PartsPurchasePlanDetail_HWForEditDataGridView.ExecuteQueryDelayed();
            //}, null);
            if(RadPartsTransferOrderWindow.IsActiveWindow)
                RadPartsTransferOrderWindow.Close();
            else
                RadPartsPurchaseOrderWindow.Close();
            this.PartsPurchasePlanDetail_HWForEditDataGridView.ExecuteQueryDelayed();
        }

        void partsTransferOrderDataEdit_EditCancelled(object sender, EventArgs e) {
            RadPartsTransferOrderWindow.Close();
        }

        private DataGridViewBase partsPurchasePlanDetail_HWForEditDataGridView;

        public DataGridViewBase PartsPurchasePlanDetail_HWForEditDataGridView {
            get {
                if(this.partsPurchasePlanDetail_HWForEditDataGridView == null) {
                    this.partsPurchasePlanDetail_HWForEditDataGridView = DI.GetDataGridView("PartsPurchasePlanDetail_HWForEdit");
                    var _isNavigator = HtmlPage.BrowserInformation.Name.Contains("Netscape");
                    var h = _isNavigator ? (double)HtmlPage.Window.GetProperty("pageYOffset")
                    : (double)HtmlPage.Document.Body.GetProperty("scrollHeight");
                    if(h > 340)
                        this.partsPurchasePlanDetail_HWForEditDataGridView.MaxHeight = h - 340;
                    this.partsPurchasePlanDetail_HWForEditDataGridView.MinHeight = 400;
                    this.partsPurchasePlanDetail_HWForEditDataGridView.DomainContext = this.DomainContext;
                    this.partsPurchasePlanDetail_HWForEditDataGridView.Loaded += PartsPurchasePlanDetail_HWForEditDataGridView_Loaded;
                }
                return this.partsPurchasePlanDetail_HWForEditDataGridView;
            }
        }

        private void PartsPurchasePlanDetail_HWForEditDataGridView_Loaded(object sender, RoutedEventArgs e) {
            this.partsPurchasePlanDetail_HWForEditDataGridView.DataContext = null;
        }

        void partsPurchaseOrderDataEdit_Loaded(object sender, RoutedEventArgs e) {
            var entity = this.DataContext as PartsPurchasePlan_HW;
            var Details = this.PartsPurchasePlanDetail_HWForEditDataGridView.SelectedEntities.Cast<PartsPurchasePlanDetail_HWEx>();
            var _partsPurchaseOrderDataEditView = partsPurchaseOrderDataEdit as PartsPurchaseOrder1DataEditView;
            PartsPurchaseOrder partsPurchaseOrder = PartsPurchaseOrderDataEdit.CreateObjectToEdit<PartsPurchaseOrder>();
            _partsPurchaseOrderDataEditView.AllowPartsPurchaseOrderSelectionChange = 0;
            partsPurchaseOrder.PartsSalesCategoryId = entity.PartsSalesCategoryId.HasValue ? entity.PartsSalesCategoryId.Value : 0;
            partsPurchaseOrder.PartsSalesCategoryName = entity.PartsSalesCategoryName;
            partsPurchaseOrder.WarehouseId = Details.First().WarehouseId.HasValue ? Details.First().WarehouseId.Value : 0;
            partsPurchaseOrder.WarehouseName = Details.First().WarehouseName;
            partsPurchaseOrder.PlanSource = PartsPurchasingUIStrings.DataEditView_Title_OverseasPartsPurchasePlan;

            //var partsPurchasePlanDetail_HWEx = this.PartsPurchasePlanDetail_HWForEditDataGridView.SelectedEntities.Cast<PartsPurchasePlanDetail_HWEx>().First();
            //partsPurchaseOrder.PartsSupplierId = partsPurchasePlanDetail_HWEx.PartsSupplierId.HasValue ? partsPurchasePlanDetail_HWEx.PartsSupplierId.Value : 0;
            //partsPurchaseOrder.PartsSupplierCode = partsPurchasePlanDetail_HWEx.PartsSupplierCode;
            //partsPurchaseOrder.PartsSupplierName = partsPurchasePlanDetail_HWEx.PartsSupplierName;
            partsPurchaseOrder.PurOrderCode = entity.Code;

            partsPurchaseOrder.BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
            partsPurchaseOrder.BranchCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
            partsPurchaseOrder.BranchName = BaseApp.Current.CurrentUserData.EnterpriseName;
            partsPurchaseOrder.ReceivingCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
            partsPurchaseOrder.ReceivingCompanyName = BaseApp.Current.CurrentUserData.EnterpriseName;
            partsPurchaseOrder.Status = (int)DcsPartsPurchaseOrderStatus.新增;
            partsPurchaseOrder.RequestedDeliveryTime = DateTime.Now.Date;
            partsPurchaseOrder.IfDirectProvision = false;
            partsPurchaseOrder.InStatus = (int)DcsPurchaseInStatus.未入库;
            partsPurchaseOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
            partsPurchaseOrder.SAPPurchasePlanCode = entity.SAPPurchasePlanCode;
            //partsPurchaseOrder.OriginalRequirementBillCode = entity.Code;
            //partsPurchaseOrder.OriginalRequirementBillId = entity.Id;
            partsPurchaseOrder.HWPurOrderCode = entity.Code;
            partsPurchaseOrder.PurOrderId = entity.Id;

            int index = 1;

            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(i => i.Name == "海外轻卡" && i.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, _loadOp => {
                if(_loadOp.HasError) {
                    if(!_loadOp.IsErrorHandled)
                        _loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(_loadOp);
                    return;
                }
                foreach(var Detail in Details) {
                    PartsPurchaseOrderDetail item = new PartsPurchaseOrderDetail();
                    item.SerialNumber = index++;
                    item.SparePartId = Detail.PartId.HasValue ? Detail.PartId.Value : 0;
                    item.SparePartCode = Detail.PartCode;
                    item.SparePartName = Detail.PartName;
                    item.UnitPrice = Detail.UnitPrice.HasValue ? Detail.UnitPrice.Value : 0;
                    var tmpAmount = (Detail.UntFulfilledQty ?? 0) - (Detail.EndAmount ?? 0);
                    item.OrderAmount = tmpAmount;
                    item.MeasureUnit = Detail.MeasureUnit;
                    item.MaxOrderAmount = tmpAmount;
                    item.OverseasPartsFigure = Detail.GPMSPartCode;
                    item.OriginalPlanNumber = Detail.PartsPurchasePlanQty;
                    item.POCode = Detail.POCode;

                    if(Detail.SuplierCode == Detail.OverseasSuplierCode) {
                        item.PartsSupplierId = Detail.PartsSupplierId;
                        item.PartsSupplierCode = Detail.PartsSupplierCode;
                        item.PartsSupplierName = Detail.PartsSupplierName;
                        //this.DomainContext.Load(this.DomainContext.查询配件合同价Query(partsPurchaseOrder.BranchId, item.PartsSupplierId, DateTime.Now, new[] { item.SparePartId }.ToList(), partsPurchaseOrder.PartsSalesCategoryId), LoadBehavior.RefreshCurrent, loadOp => {
                        //    if(loadOp.HasError) {
                        //        if(!loadOp.IsErrorHandled)
                        //            loadOp.MarkErrorAsHandled();
                        //        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        //        return;
                        //    }
                        //    var tmp = loadOp.Entities.FirstOrDefault();
                        //    item.UnitPrice = tmp == null ? 0 : tmp.PurchasePrice;
                        //}, null);
                    } else {
                        var partsSalesCategoryHW = _loadOp.Entities.FirstOrDefault();
                        if(partsSalesCategoryHW != null)
                            this.DomainContext.Load(this.DomainContext.GetBranchSupplierRelationsQuery().Where(i => i.BusinessCode == Detail.OverseasSuplierCode && i.PartsSalesCategoryId == partsSalesCategoryHW.Id && i.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                                if(loadOp.HasError) {
                                    if(!loadOp.IsErrorHandled)
                                        loadOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                                    return;
                                }
                                var _entity = loadOp.Entities.SingleOrDefault();
                                if(_entity != null) {
                                    this.DomainContext.Load(this.DomainContext.GetPartsSuppliersQuery().Where(i => i.Id == _entity.SupplierId && i.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp1 => {
                                        if(loadOp1.HasError) {
                                            if(!loadOp1.IsErrorHandled)
                                                loadOp1.MarkErrorAsHandled();
                                            DcsUtils.ShowDomainServiceOperationWindow(loadOp1);
                                            return;
                                        }
                                        var entity1 = loadOp1.Entities.FirstOrDefault();
                                        if(entity1 != null) {
                                            item.PartsSupplierId = entity1.Id;
                                            item.PartsSupplierCode = entity1.Code;
                                            item.PartsSupplierName = entity1.Name;
                                        }
                                    }, null);
                                }
                            }, null);
                    }
                    item.RequestedDeliveryTime = Detail.RequestedDeliveryTime;
                    partsPurchaseOrder.PartsPurchaseOrderDetails.Add(item);
                }
            }, null);
            _partsPurchaseOrderDataEditView.AllowPartsPurchaseOrderSelectionChange = 0;
        }

        private void CreatePartsTransferOrder() {
            var entity = this.DataContext as PartsPurchasePlan_HW;
            if(entity.Status == (int)DcsPurchasePlanStatus.可分解 || entity.Status == (int)DcsPurchasePlanStatus.部分分解 || entity.Status == (int)DcsPurchasePlanStatus.数据异常) {
                if(this.PartsPurchasePlanDetail_HWForEditDataGridView.SelectedEntities == null || this.PartsPurchasePlanDetail_HWForEditDataGridView.SelectedEntities.Count() == 0) {
                    UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_CreatePartsTransferOrderError1);
                    return;
                }
                var Details = this.PartsPurchasePlanDetail_HWForEditDataGridView.SelectedEntities.Cast<PartsPurchasePlanDetail_HWEx>();
                if(Details.Any(i => !string.IsNullOrEmpty(i.FaultReason))) {
                    UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_CreatePartsTransferOrderError2, 5);
                    return;
                }
                if(Details.Select(r => r.WarehouseId).Distinct().Count() > 1) {
                    UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_CreatePartsTransferOrderError3);
                    return;
                }


                PartsTransferOrder partsTransferOrder = PartsTransferOrderDataEdit.CreateObjectToEdit<PartsTransferOrder>();
                partsTransferOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                partsTransferOrder.Status = (int)DcsPartsTransferOrderStatus.新建;
                partsTransferOrder.SAPPurchasePlanCode = entity.SAPPurchasePlanCode;
                partsTransferOrder.DestWarehouseId = Details.First().WarehouseId.HasValue ? Details.First().WarehouseId.Value : 0;
                partsTransferOrder.DestWarehouseName = Details.First().WarehouseName;
                partsTransferOrder.DestWarehouseCode = Details.First().WarehouseCode;
                partsTransferOrder.PurOrderCode = entity.Code;
                partsTransferOrder.SAPPurchasePlanCode = entity.SAPPurchasePlanCode;
                partsTransferOrder.OriginalBillCode = entity.Code;
                partsTransferOrder.OriginalBillId = entity.Id;

                foreach(var Detail in Details) {
                    PartsTransferOrderDetail item = new PartsTransferOrderDetail();
                    item.SparePartId = Detail.PartId.HasValue ? Detail.PartId.Value : 0;
                    item.SparePartName = Detail.PartName;
                    item.SparePartCode = Detail.PartCode;
                    item.MaxPlannedAmount = (Detail.UntFulfilledQty ?? 0) - (Detail.EndAmount ?? 0);
                    item.Price = Detail.UnitPrice ?? 0;
                    this.DomainContext.Load(this.DomainContext.调拨查询仓库库存Query(Detail.WarehouseId.HasValue ? Detail.WarehouseId.Value : default(int)).Where(r => r.SparePartId == item.SparePartId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            if(!loadOp.IsErrorHandled)
                                loadOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                            return;
                        }
                        var tmp = loadOp.Entities.Where(i => i.SparePartId == item.SparePartId).FirstOrDefault();
                        var tmpPrice = tmp == null ? 0 : tmp.PartsPlannedPrice;
                        if(tmpPrice == 0) {
                            this.DomainContext.Load(this.DomainContext.GetPartsPlannedPriceBySparePartIdQuery(new int[] { item.SparePartId }, entity.PartsSalesCategoryId ?? 0), LoadBehavior.RefreshCurrent, loadOp1 => {
                                if(loadOp1.HasError) {
                                    if(!loadOp1.IsErrorHandled)
                                        loadOp1.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOp1);
                                    return;
                                }
                                var tmp1 = loadOp1.Entities.FirstOrDefault();
                                item.PlannPrice = tmp1 == null ? 0 : tmp1.PlannedPrice;
                            }, null);
                        } else
                            item.PlannPrice = tmpPrice;
                        item.DestinWarehouseStock = tmp == null ? 0 : tmp.UsableQuantity;
                    }, null);
                    item.PartsSalesCategoryId = entity.PartsSalesCategoryId;
                    item.PlannedAmount = (Detail.UntFulfilledQty ?? 0) - (Detail.EndAmount ?? 0);
                    item.OverseasPartsFigure = Detail.GPMSPartCode;
                    item.OriginalPlanNumber = Detail.PartsPurchasePlanQty;
                    item.POCode = Detail.POCode;
                    partsTransferOrder.PartsTransferOrderDetails.Add(item);
                }
                RadPartsTransferOrderWindow.ShowDialog();
            } else {
                UIHelper.ShowAlertMessage(PartsPurchasingUIStrings.DataEditView_Validation_CreatePartsTransferOrderError4);
            }
        }

        private void CreatePartsPurchaseOrder() {
            var entity = this.DataContext as PartsPurchasePlan_HW;
            if(entity.Status == (int)DcsPurchasePlanStatus.可分解 || entity.Status == (int)DcsPurchasePlanStatus.部分分解 || entity.Status == (int)DcsPurchasePlanStatus.数据异常) {
                if(this.PartsPurchasePlanDetail_HWForEditDataGridView.SelectedEntities == null || this.PartsPurchasePlanDetail_HWForEditDataGridView.SelectedEntities.Count() == 0) {
                    UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_CreatePartsTransferOrderError1);
                    return;
                }
                var details = this.PartsPurchasePlanDetail_HWForEditDataGridView.SelectedEntities.Cast<PartsPurchasePlanDetail_HWEx>().ToArray();
                if(details.Any(i => !string.IsNullOrEmpty(i.FaultReason))) {
                    UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_CreatePartsTransferOrderError2, 5);
                    return;
                }
                if(details.Select(r => r.WarehouseId).Distinct().Count() > 1) {
                    UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_CreatePartsTransferOrderError3);
                    return;
                }
                RadPartsPurchaseOrderWindow.ShowDialog();
            } else {
                UIHelper.ShowAlertMessage(PartsPurchasingUIStrings.DataEditView_Validation_CreatePartsTransferOrderError4);
            }
        }

        private void CreateUI() {
            this.HideSaveButton();
            this.Title = "";
            this.RegisterButton(new ButtonItem {
                Command = new DelegateCommand(this.CreatePartsTransferOrder),
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/DeliverParts.png", UriKind.Relative),
                Title = PartsPurchasingUIStrings.DataEditView_Title_PartsTransferOrder_Create
            }, true);
            this.RegisterButton(new ButtonItem {
                Command = new DelegateCommand(this.CreatePartsPurchaseOrder),
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/QueryOldPMSClaimOrder.png", UriKind.Relative),
                Title = PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_Create
            }, true);
            var dcsDetailDataEditView = new DcsDetailDataEditView();
            dcsDetailDataEditView.UnregisterButton(dcsDetailDataEditView.InsertButton);
            dcsDetailDataEditView.UnregisterButton(dcsDetailDataEditView.DeleteButton);
            dcsDetailDataEditView.Register(PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrderDecompose, null, () => this.PartsPurchasePlanDetail_HWForEditDataGridView);
            dcsDetailDataEditView.SetValue(Grid.RowProperty, 3);
            LayoutRoot.Children.Add(dcsDetailDataEditView);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsPurchasePlan_HWWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                this.SetObjectToEdit(entity);
                this.PartsPurchasePlanDetail_HWForEditDataGridView.DataContext = entity;
            }, null);
        }
    }
}
