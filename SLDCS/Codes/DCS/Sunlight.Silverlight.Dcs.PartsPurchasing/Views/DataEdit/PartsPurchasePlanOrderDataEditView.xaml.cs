﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Sunlight.Silverlight.ViewModel;
using System.Windows.Browser;
using System.Collections.Generic;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class PartsPurchasePlanOrderDataEditView {
        private DataGridViewBase partsPurchasePlanDetailForEditDataGridView;
        private ObservableCollection<Warehouse> kvWarehouses;
        private ObservableCollection<KeyValuePair> kvPartsSalesCategories;
        private KeyValueManager keyValueManager;
        private ObservableCollection<KeyValuePair> kvPartsPurchaseOrderType;

        private readonly string[] kvNames = {
           "PartsPurchaseOrder_OrderType"
        };

        private DataGridViewBase PartsPurchasePlanDetailForEditDataGridView {
            get {
                if(this.partsPurchasePlanDetailForEditDataGridView == null) {
                    this.partsPurchasePlanDetailForEditDataGridView = DI.GetDataGridView("PartsPurchasePlanOrderDetailForEdit");
                    this.partsPurchasePlanDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsPurchasePlanDetailForEditDataGridView;
            }
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        //新增、修改界面 默认 标题
        protected override string BusinessName {
            get {
                return PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrderPlanInfo;
            }
        }

        public ObservableCollection<Warehouse> KvWarehouses {
            get {
                return this.kvWarehouses ?? (this.kvWarehouses = new ObservableCollection<Warehouse>());
            }
        }

        public ObservableCollection<KeyValuePair> KvPartsPurchaseOrderTypes {
            get {
                return this.kvPartsPurchaseOrderType ?? (this.kvPartsPurchaseOrderType = new ObservableCollection<KeyValuePair>());
            }
        }



        public ObservableCollection<KeyValuePair> KvPartsSalesCategories {
            get {
                return this.kvPartsSalesCategories ?? (this.kvPartsSalesCategories = new ObservableCollection<KeyValuePair>());
            }
        }

        protected override bool OnRequestCanSubmit()
        {
            this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            return true;
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsPurchasePlansWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    DataEditPanels.FilePath = entity.Path;
                    this.DomainContext.Load(this.DomainContext.GetWarehousesByPartsSalesCategoryIdAndOwnerCompanyIdQuery(int.Parse(entity.PartsSalesCategoryId.ToString()), BaseApp.Current.CurrentUserData.EnterpriseId).Where(r => r.Type != (int)DcsWarehouseType.虚拟库), LoadBehavior.RefreshCurrent, loadOp1 => {
                        if(loadOp1.HasError) {
                            loadOp.MarkErrorAsHandled();
                            return;
                        }
                        this.KvWarehouses.Clear();
                        if(loadOp.Entities != null) {
                            foreach(var item in loadOp1.Entities)
                                this.KvWarehouses.Add(item);
                        }
                    }, null);

                    var sparePartIds = entity.PartsPurchasePlanDetails.Select(o => o.SparePartId).Distinct().ToArray();
                    this.DomainContext.Load(this.DomainContext.获取当前企业的配件营销包装属性Query(entity.PartsSalesCategoryId.Value, entity.BranchId, sparePartIds), LoadBehavior.RefreshCurrent, loadOp2 => {
                        if(loadOp2.HasError) {
                            if(!loadOp2.IsErrorHandled) {
                                loadOp2.MarkErrorAsHandled();
                                return;
                            }
                        }
                        if(loadOp2.Entities != null && loadOp2.Entities.Any()) {
                            var partsBranchPackingProps = loadOp2.Entities.ToArray();
                            foreach(var data in entity.PartsPurchasePlanDetails) {
                                var partsBranchPackingProp = partsBranchPackingProps.FirstOrDefault(o => o.SparePartId == data.SparePartId);
                                data.PackingCoefficient = partsBranchPackingProp.PackingCoefficient.Value;
                            }
                        }
                    }, null);
                    if (entity.IsTransSap == null)
                        entity.IsTransSap = false;
                    this.SetObjectToEdit(entity);
                }
            }, null);


        }
        private ICommand exportFileCommand;

        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }
        //导出抬头
        private const string EXPORT_DATA_FILE_NAME = "导出配件计划采购清单模板.xlsx";

        private void InitializeCommand() {
            this.exportFileCommand = new DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                    new ImportTemplateColumn {
                        Name = PartsPurchasingUIStrings.DataEditView_ImportTemplate_SparePartCode,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = PartsPurchasingUIStrings.DataEditView_ImportTemplate_SparePartName
                    },
                    new ImportTemplateColumn {
                        Name = PartsPurchasingUIStrings.DataEditView_ImportTemplate_PlanAmount,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = PartsPurchasingUIStrings.DataEditView_ImportTemplate_MeasureUnit,
                    }
                };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }

        /// <summary>
        /// 文件上传成功后，调用该方法进行上传后的处理。不支持异步调用
        /// <param>上传文件名 <name>fileName</name> </param>
        /// </summary>
        protected Action<string> UploadFileSuccessedProcessing;

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //取当前登录用户的HashCode来标记上传文件的唯一性
                //this.UploadFileSuccessedProcessing.Invoke(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }

        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage(PartsPurchasingUIStrings.Upload_File_Tips);
                    e.Handled = true;
                }
            }
        }

        private void ExcelServiceClient_ImportPartsPurchasePlanDetailCompleted(object sender, ImportPartsPurchasePlanDetailCompletedEventArgs e) {
            var partsPurchasePlan = this.DataContext as PartsPurchasePlan;
            if(partsPurchasePlan == null)
                return;
            var detailList = partsPurchasePlan.PartsPurchasePlanDetails.ToList();
            foreach(var detail in detailList) {
                partsPurchasePlan.PartsPurchasePlanDetails.Remove(detail);
            }
            if(e.rightData != null && e.rightData.Length > 0) {
                int num = 1;
                var sparePartIds = e.rightData.Select(o => o.SparePartId).Distinct().ToArray();
                this.DomainContext.Load(this.DomainContext.获取当前企业的配件营销包装属性Query(partsPurchasePlan.PartsSalesCategoryId.Value, partsPurchasePlan.BranchId, sparePartIds), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled) {
                            loadOp.MarkErrorAsHandled();
                            return;
                        }
                    }
                    if(loadOp.Entities != null && loadOp.Entities.Any()) {
                        var partsBranchPackingProps = loadOp.Entities.ToArray();
                        foreach(var data in e.rightData) {
                            var partsBranchPackingProp = partsBranchPackingProps.FirstOrDefault(o => o.SparePartId == data.SparePartId);
                            var partsPurchasePlanDetail = new PartsPurchasePlanDetail {
                                SerialNumber = num++,
                                PurchasePlanId = partsPurchasePlan.Id,
                                SupplierPartCode = data.SupplierPartCode,
                                SparePartId = data.SparePartId,
                                SparePartCode = data.SparePartCode,
                                SparePartName = data.SparePartName,
                                PlanAmount = data.PlanAmount,
                                MeasureUnit = data.MeasureUnit,
                                Price = data.Price,
                                PackingCoefficient = partsBranchPackingProp.PackingCoefficient.Value,
                                LimitQty = data.LimitQty,
                                UsedQty = data.UsedQty,
                                ABCStrategy = Enum.GetName(typeof(DcsABCStrategyCategory), data.PartABC == null ? 0 : data.PartABC)
                            };
                            partsPurchasePlan.PartsPurchasePlanDetails.Add(partsPurchasePlanDetail);
                        }
                    }
                }, null);
            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowNotification(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));

        }

        //本地缓存，记录修改前的配件销售订单类型Id
        //private int oldPartsPurchasePlanTypeId;

        private string strFileName;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private RadUpload uploader;
        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        var partsPurchasePlan = this.DataContext as PartsPurchasePlan;
                        if(partsPurchasePlan == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.ImportPartsPurchasePlanDetailAsync(e.HandlerData.CustomData["Path"].ToString(), int.Parse(partsPurchasePlan.Id.ToString()), int.Parse(partsPurchasePlan.BranchId.ToString()), int.Parse(partsPurchasePlan.PartsSalesCategoryId.ToString()),this.PartsPurchaseOrderTypeName.Text);
                        this.excelServiceClient.ImportPartsPurchasePlanDetailCompleted -= ExcelServiceClient_ImportPartsPurchasePlanDetailCompleted;
                        this.excelServiceClient.ImportPartsPurchasePlanDetailCompleted += ExcelServiceClient_ImportPartsPurchasePlanDetailCompleted;
                    };
                }
                return this.uploader;
            }
        }

        private void ShowFileDialog() {
            var partsPurchasePlan = this.DataContext as PartsPurchasePlan;
            if(partsPurchasePlan == null)
                return;
            partsPurchasePlan.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(partsPurchasePlan.PartsSalesCategoryId.ToString())) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_CategoryIsNull);
                return;
            }
            if(string.IsNullOrEmpty(partsPurchasePlan.PartsPlanTypeId.ToString()) || string.IsNullOrEmpty(this.PartsPurchaseOrderTypeName.Text)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_PurchasePlanTypeIsNull);
                return;
            }
            this.Uploader.ShowFileDialog();
        }

        private void CreateUI() {
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsPurchasePlan), PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrderPlanDetails /*"PartsPurchasePlanDetails"*/), null, this.PartsPurchasePlanDetailForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            //导入、导出
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsPurchasingUIStrings.Action_Title_Import,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                Command = new DelegateCommand(this.ShowFileDialog)
            });
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsPurchasingUIStrings.Upload_File_Export_Model,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                Command = this.ExportTemplateCommand
            });

            this.DomainContext.Load(this.DomainContext.GetPersonSalesCenterLinksQuery().Where(r => r.PersonId == BaseApp.Current.CurrentUserData.UserId && r.Status == (int)DcsBaseDataStatus.有效), loadOp1 => {
                if(loadOp1.HasError)
                    return;
                var partsSaleScategoryIds = loadOp1.Entities.Select(r => r.PartsSalesCategoryId).ToArray();
                this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(e => e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                    if(loadOp.HasError)
                        return;
                    this.KvPartsSalesCategories.Clear();
                    foreach(var partsSalesCategory in loadOp.Entities)
                        if(partsSaleScategoryIds.Contains(partsSalesCategory.Id)) {
                            this.KvPartsSalesCategories.Add(new KeyValuePair {
                                Key = partsSalesCategory.Id,
                                Value = partsSalesCategory.Name,
                                UserObject = partsSalesCategory
                            });
                        }
                    this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
                }, null);
            }, null);
            this.Root.Children.Add(detailDataEditView);

            KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData(() => {
                //foreach (var keyValuePair in this.KeyValueManager[this.kvNames[0]])
                //{
                //    this.PartsPurchaseOrderOrderTypes.Add(keyValuePair);
                //}
                //foreach (var keyValuePair in this.KeyValueManager[this.kvNames[1]])
                //{
                //    this.PartsShippingMethods.Add(keyValuePair);
                //}
                //foreach (var keyValuePair in this.KeyValueManager[this.kvNames[0]])
                //{
                //    this.KvPartsPurchaseOrderTypes.Add(keyValuePair);
                //}
            });

            //附件
            this.DataEditPanels.SetValue(Grid.RowProperty, 5);
            this.DataEditPanels.SetValue(Grid.ColumnProperty, 0);
            this.DataEditPanels.Margin = new Thickness(0, 250, 0, 0);
            this.DataEditPanels.HorizontalAlignment = HorizontalAlignment.Left;
            this.DataEditPanels.VerticalAlignment = VerticalAlignment.Top;
            this.DataEditPanels.SetValue(Grid.ColumnSpanProperty, 2);
            this.Root.Children.Add(DataEditPanels);
        }

        private void queryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            var partsPurchasePlan = this.DataContext as PartsPurchasePlan;
            if(queryWindow == null || partsPurchasePlan == null)
                return;
            if(partsPurchasePlan.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Error_PartsPurchaseOrder_PleaseSelectPartsSalesCategoryId);
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
                return;
            }
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "BranchName", BaseApp.Current.CurrentUserData.EnterpriseName
            });
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem("PartsSalesCategoryId", typeof(int), FilterOperator.IsEqualTo, partsPurchasePlan.PartsSalesCategoryId));
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "BranchId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }

        private void QueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var branchSupplierRelation = queryWindow.SelectedEntities.Cast<BranchSupplierRelation>().FirstOrDefault();
            if(branchSupplierRelation == null || !this.PartsPurchasePlanDetailForEditDataGridView.CommitEdit()) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Error_PartsPurchaseOrder_DetailsIsEditingOrHasError);
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
                return;
            }
            var partsPurchasePlan = this.DataContext as PartsPurchasePlan;
            if(partsPurchasePlan == null)
                return;
            try {
                //TODO: 删除时抛出异常 
                var deleteDetals = partsPurchasePlan.PartsPurchasePlanDetails.ToArray();
                ((IEditableObject)partsPurchasePlan).EndEdit();
                foreach(var detail in deleteDetals) {
                    ((IEditableObject)detail).EndEdit();
                    detail.ValidationErrors.Clear();
                    partsPurchasePlan.PartsPurchasePlanDetails.Remove(detail);
                }
            } finally {
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }
        }

        private void PartsPurchaseOrderDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsPurchasePlan = this.DataContext as PartsPurchasePlan;
            if(partsPurchasePlan == null)
                return;
            partsPurchasePlan.PropertyChanged -= this.partsPurchasePlan_PropertyChanged;
            partsPurchasePlan.PropertyChanged += this.partsPurchasePlan_PropertyChanged;
            //partsPurchasePlan.PartsPurchaseOrderDetails.EntityRemoved -= PartsPurchaseOrderDetails_EntityRemoved;
            //partsPurchasePlan.PartsPurchaseOrderDetails.EntityRemoved += PartsPurchaseOrderDetails_EntityRemoved;
        }

        private void partsPurchasePlan_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsPurchasePlan = this.DataContext as PartsPurchasePlan;
            if(partsPurchasePlan == null)
                return;
            if(this.DomainContext == null) {
                this.DomainContext = new DcsDomainContext();
            }
            switch(e.PropertyName) {
                case "PartsSalesCategoryId":
                    this.DomainContext.Load(this.DomainContext.GetWarehousesByPartsSalesCategoryIdAndOwnerCompanyIdQuery((int)partsPurchasePlan.PartsSalesCategoryId, BaseApp.Current.CurrentUserData.EnterpriseId).Where(r => r.Type != (int)DcsWarehouseType.虚拟库), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            loadOp.MarkErrorAsHandled();
                            return;
                        }
                        this.KvWarehouses.Clear();
                        if(loadOp.Entities != null) {
                            foreach(var item in loadOp.Entities)
                                this.KvWarehouses.Add(item);
                        }
                    }, null);
                    if(partsPurchasePlan.PartsPurchasePlanDetails.Any()) {
                        foreach(var detail in partsPurchasePlan.PartsPurchasePlanDetails.ToArray())
                            partsPurchasePlan.PartsPurchasePlanDetails.Remove(detail);
                    }
                    partsPurchasePlan.WarehouseId = default(int);
                    partsPurchasePlan.PartsPlanTypeId = default(int);
                    break;
                default:
                    break;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.PartsPurchasePlanDetailForEditDataGridView.CommitEdit())
                return;

            var partsPurchasePlan = this.DataContext as PartsPurchasePlan;
            if(partsPurchasePlan == null)
                return;

            partsPurchasePlan.ValidationErrors.Clear();
            partsPurchasePlan.Path = DataEditPanels.FilePath;
            //if(string.IsNullOrWhiteSpace(sparePart.Path)) {
            //    UIHelper.ShowNotification("请上传附件");
            //    return;
            //}
            foreach(var relation in partsPurchasePlan.PartsPurchasePlanDetails)
                relation.ValidationErrors.Clear();

            if(string.IsNullOrEmpty(partsPurchasePlan.WarehouseName))
                partsPurchasePlan.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_WarehouseIsNull, new[] {
                    "WarehouseName"
                }));

            if((partsPurchasePlan.PartsPlanTypeId <= 0) || (partsPurchasePlan.PartsPlanTypeId == null))
                partsPurchasePlan.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PlanTypeIsNull, new[] {
                    "PartsPlanTypeId"
                }));
            partsPurchasePlan.BranchCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
            partsPurchasePlan.BranchName = BaseApp.Current.CurrentUserData.EnterpriseName;
            foreach(var w in KvWarehouses) {
                if(w.Name == (this.Cbwarehouse.Text))
                    partsPurchasePlan.WarehouseCode = w.Code;
            }
            partsPurchasePlan.Memo = this.CbMemo.Text;

            if(string.IsNullOrEmpty(partsPurchasePlan.Code)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_PurchaseOrderCodeIsNull);
                return;
            }

            if(!partsPurchasePlan.PartsPurchasePlanDetails.Any()) {
                UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrderDetail_DetailIsNotNull));
                return;
            }

            foreach(var relation in partsPurchasePlan.PartsPurchasePlanDetails) {
                if(string.IsNullOrEmpty(relation.SparePartCode))
                    relation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrderDetail_SparePartCodeIsNull, new[] {
                        "SparePartCode"
                    }));

                if(string.IsNullOrEmpty(relation.SparePartName))
                    relation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrderDetail_SparePartNameIsNull, new[] {
                        "SparePartName"
                    }));
            }

            foreach(var relation in partsPurchasePlan.PartsPurchasePlanDetails.Where(e => e.PlanAmount <= 0)) {
                UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Validation_PlanAmountMustMoreThanZero, relation.SparePartCode));
                return;
            }
            if(partsPurchasePlan.HasValidationErrors || partsPurchasePlan.PartsPurchasePlanDetails.Any(relation => relation.HasValidationErrors))
                return;

            var partsPurchasePlanDetail = partsPurchasePlan.PartsPurchasePlanDetails.FirstOrDefault(r => (r.LimitQty ?? 0) - (r.UsedQty ?? 0) < r.PlanAmount && r.LimitQty.HasValue && r.LimitQty.Value > 0);
            if(partsPurchasePlanDetail != null) {
                UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Validation_AmountError, partsPurchasePlanDetail.SparePartCode, partsPurchasePlanDetail.LimitQty, partsPurchasePlanDetail.UsedQty));
                return;
            }

            if(partsPurchasePlan.PartsPurchasePlanDetails.Any(r => r.Price == 0)) {
                //DcsUtils.Confirm("订单清单中存在没有计划价或采购价的配件，是否继续提交？", () =>
                //{
                ((IEditableObject)partsPurchasePlan).EndEdit();
                try {
                    if(this.EditState == DataEditState.New) {
                        if(partsPurchasePlan.Can生成配件采购计划订单)
                            partsPurchasePlan.生成配件采购计划订单();
                    } else {
                        if(partsPurchasePlan.Can修改配件采购计划订单)
                            partsPurchasePlan.修改配件采购计划订单();
                    }
                } catch(ValidationException ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                    return;
                }
                base.OnEditSubmitting();
                //});
            } else {
                ((IEditableObject)partsPurchasePlan).EndEdit();
                try {
                    if(this.EditState == DataEditState.New) {
                        if(partsPurchasePlan.Can生成配件采购计划订单)
                            partsPurchasePlan.生成配件采购计划订单();
                    } else {
                        if(partsPurchasePlan.Can修改配件采购计划订单)
                            partsPurchasePlan.修改配件采购计划订单();
                    }
                } catch(ValidationException ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                    return;
                }
                base.OnEditSubmitting();
            }

            //var sparepartIds = partsPurchasePlan.PartsPurchasePlanDetails.Select(r => r.SparePartId).ToArray();
            //this.DomainContext.Load(DomainContext.GetSparePartsByIdsQuery(sparepartIds), LoadBehavior.RefreshCurrent, loadOp => {
            //    if (loadOp.HasError)
            //    {
            //        if (!loadOp.IsErrorHandled)
            //        {
            //            loadOp.MarkErrorAsHandled();
            //            return;
            //        }
            //    }
            //    if (loadOp.Entities != null && loadOp.Entities.Any())
            //    {
            //        var spareParts = loadOp.Entities.ToArray();
            //        if (spareParts.Any(r => r.MInPackingAmount == null || r.MInPackingAmount == 0))
            //        {
            //            UIHelper.ShowNotification("配件清单存在未维护最小包装数量的的配件,请先维护");
            //            return;
            //        }
            //        foreach (var partsPurchaseOrderDetail in partsPurchasePlan.PartsPurchasePlanDetails)
            //        {
            //            if (spareParts.Any(r => r.Id == partsPurchaseOrderDetail.SparePartId && (partsPurchaseOrderDetail.PlanAmount % r.MInPackingAmount.Value) != 0))
            //            {
            //                var partsValidation = spareParts.First(r => r.Id == partsPurchaseOrderDetail.SparePartId && (partsPurchaseOrderDetail.PlanAmount % r.MInPackingAmount.Value) != 0);
            //                UIHelper.ShowNotification(string.Format("{0}配件订货数量必须为最小包装数量({1})的整数倍", partsValidation.Code, partsValidation.MInPackingAmount));
            //                return;
            //            }
            //        }
            //    }

            //}, null);
        }

        public PartsPurchasePlanOrderDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += PartsPurchaseOrderDataEditView_DataContextChanged;
            this.KeyValueManager.LoadData();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public int AllowPartsPurchaseOrderSelectionChange = 0;
        private void PartsPurchaseOrder_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            var partsPurchasePlan = this.DataContext as PartsPurchasePlan;
            if(partsPurchasePlan == null)
                return;
            this.DomainContext.Load(this.DomainContext.GetPartsPurchaseOrderTypesQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效 && v.PartsSalesCategoryId == partsPurchasePlan.PartsSalesCategoryId && v.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Entities.Count() > 0)
                    this.KvPartsPurchaseOrderTypes.Clear();
                foreach(var partsPurchaseOrderType in loadOp.Entities) {
                    if (partsPurchaseOrderType.Name != "中心库采购") {
                        this.KvPartsPurchaseOrderTypes.Add(new KeyValuePair {
                            Key = partsPurchaseOrderType.Id,
                            Value = partsPurchaseOrderType.Name
                        });
                    }
                }
                //if (AllowPartsPurchaseOrderSelectionChange != 0 && partsPurchasePlan.PartsSalesCategoryId != AllowPartsPurchaseOrderSelectionChange)
                //{
                //    this.ptbSupplierCode.Text = null;
                //    partsPurchasePlan.PartsSupplierCode = null;
                //    partsPurchasePlan.PartsSupplierName = null;
                //}
                if(!string.IsNullOrEmpty(partsPurchasePlan.PartsSalesCategoryId.ToString())) {
                    AllowPartsPurchaseOrderSelectionChange = int.Parse(partsPurchasePlan.PartsSalesCategoryId.ToString());
                }
            }, null);
        }

        protected override void Reset() {
            var partsPurchasePlan = this.DataContext as PartsPurchasePlan;
            if(this.DomainContext.PartsPurchasePlans.Contains(partsPurchasePlan))
                this.DomainContext.PartsPurchasePlans.Detach(partsPurchasePlan);
        }

        private FileUploadForPartsOuterPurchaseDataEditPanel productDataEditPanels;

        public FileUploadForPartsOuterPurchaseDataEditPanel DataEditPanels {
            get {
                return this.productDataEditPanels ?? (this.productDataEditPanels = (FileUploadForPartsOuterPurchaseDataEditPanel)DI.GetDataEditPanel("FileUploadForPartsOuterPurchase"));
            }
        }
    }
}