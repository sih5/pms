﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsPurchasePlanOrderForAbandod {
        public PartsPurchasePlanOrderForAbandod() {
            InitializeComponent();
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsPurchasePlansByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
        protected override void OnEditSubmitting() {
            var partsPurchasePlan = this.DataContext as PartsPurchasePlan;
            if(partsPurchasePlan == null)
                return;
            partsPurchasePlan.ValidationErrors.Clear();
            if (string.IsNullOrEmpty(partsPurchasePlan.AbandonComment))
                partsPurchasePlan.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_AbandonReasonIsNull, new[] {
                    "AbandonComment"
                }));
            if(partsPurchasePlan.HasValidationErrors)
                return;
            try {
                if(partsPurchasePlan.Can作废配件采购计划订单)
                    partsPurchasePlan.作废配件采购计划订单();
            } catch(Exception ex) {
                UIHelper.ShowAlertMessage(ex.Message);
            }
            base.OnEditSubmitting();

        }


        protected override string Title {
            get {
                return PartsPurchasingUIStrings.DataEditView_Title_AbandonPurchasePlan;
            }
        }

    }
}
