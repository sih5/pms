﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand; 

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class PartsPurchasePlanOrderForFirstApproveDataEditView {
        private DataGridViewBase partsPurchasePlanOrderForApproveDataEditView;

        //private ObservableCollection<KeyValuePair> kvIsPasseds;
        // private DcsDomainContext dcsDomainContext = new DcsDomainContext();
        //public ObservableCollection<KeyValuePair> KvIsPasseds {
        //    get {
        //        return this.kvIsPasseds ?? (this.kvIsPasseds = new ObservableCollection<KeyValuePair>());
        //    }
        //}

        private DataGridViewBase PartsPurchaseOrderDetailForEditDataGridView {
            get {
                if(this.partsPurchasePlanOrderForApproveDataEditView == null) {
                    this.partsPurchasePlanOrderForApproveDataEditView = DI.GetDataGridView("PartsPurchasePlanOrderDetailForShow");
                    this.partsPurchasePlanOrderForApproveDataEditView.DomainContext = this.DomainContext;
                }
                return this.partsPurchasePlanOrderForApproveDataEditView;
            }
        }
      
        protected override string Title {
            get {              
                    return "初审采购计划单";
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsPurchasePlansWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                //接收查询方法返回数据
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    DataEditPanels.FilePath = entity.Path;
                    //界面加载数据
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        private void CreateUI() {
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsPurchasePlan), PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrderPlanDetails /*"PartsPurchasePlanDetails"*/), null, this.PartsPurchaseOrderDetailForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            //去掉 添加、删除 按钮
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            //设置RadioButton 选择项
            //this.kvIsPasseds.Add(new KeyValuePair {
            //    Key = 66,
            //    Value = "审核通过"
            //});
            //this.kvIsPasseds.Add(new KeyValuePair {
            //    Key = 77,
            //    Value = "驳回"
            //});

            this.Root.Children.Add(detailDataEditView);

            //附件
            this.DataEditPanels.SetValue(Grid.RowProperty, 5);
            this.DataEditPanels.SetValue(Grid.ColumnProperty, 0);
            this.DataEditPanels.Margin = new Thickness(0, 220, 0, 0);
            this.DataEditPanels.isHiddenButtons = true;
            this.DataEditPanels.HorizontalAlignment = HorizontalAlignment.Left;
            this.DataEditPanels.VerticalAlignment = VerticalAlignment.Top;
            this.DataEditPanels.SetValue(Grid.ColumnSpanProperty, 2);
            this.Root.Children.Add(DataEditPanels);

            this.RegisterButton(new ButtonItem {
                Command = new DelegateCommand(this.RejectCurrentData),
                Title = PartsPurchasingUIStrings.DataEditPanel_Title_Reject,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative)
            },true);
        }

        private void RejectCurrentData() {
            var partsPurchasePlan = this.DataContext as PartsPurchasePlan;
            if (partsPurchasePlan == null)
                return;
            partsPurchasePlan.ApproveMemo = this.CbApproveMemo.Text;
            partsPurchasePlan.Memo = this.CbRemark.Text;
            if (string.IsNullOrEmpty(partsPurchasePlan.ApproveMemo)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Title_InitialApproveMemoIsNull);
                return;
            }
            ((IEditableObject)partsPurchasePlan).EndEdit();
            try {
                if (partsPurchasePlan.Can驳回配件采购计划订单)
                    partsPurchasePlan.驳回配件采购计划订单();
                ExecuteSerivcesMethod(PartsPurchasingUIStrings.DataEditView_Notification_RejectSuccess);
            } catch (ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
        }
        private void ExecuteSerivcesMethod(string notifyMessage) {
            DomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    DomainContext.RejectChanges();
                    return;
                }
                this.NotifyEditSubmitted();
                this.OnCustomEditSubmitted();
                UIHelper.ShowNotification(notifyMessage);
            }, null);
        }
        public void OnCustomEditSubmitted() {
            this.DataContext = null;
        }
        public new event EventHandler EditSubmitted;
        private void NotifyEditSubmitted() {
            var handler = this.EditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }
        private void PartsPurchaseOrderDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsPurchasePlan = this.DataContext as PartsPurchasePlan;
            if(partsPurchasePlan == null)
                return;
            partsPurchasePlan.PropertyChanged -= this.partsPurchaseOrder_PropertyChanged;
            partsPurchasePlan.PropertyChanged += this.partsPurchaseOrder_PropertyChanged;
            //partsPurchasePlan.PartsPurchaseOrderDetails.EntityRemoved -= PartsPurchaseOrderDetails_EntityRemoved;
            //partsPurchasePlan.PartsPurchaseOrderDetails.EntityRemoved += PartsPurchaseOrderDetails_EntityRemoved;
        }

        private void partsPurchaseOrder_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsPurchasePlan = this.DataContext as PartsPurchasePlan;
            if(partsPurchasePlan == null)
                return;
            if(this.DomainContext == null) {
                this.DomainContext = new DcsDomainContext();
            }
            switch(e.PropertyName) {
                case "PartsSalesCategoryId":
                    if(partsPurchasePlan.PartsPurchasePlanDetails.Any()) {
                        foreach(var detail in partsPurchasePlan.PartsPurchasePlanDetails.ToArray())
                            partsPurchasePlan.PartsPurchasePlanDetails.Remove(detail);
                    }
                    partsPurchasePlan.WarehouseId = default(int);
                    partsPurchasePlan.PartsPlanTypeId = default(int);
                    break;
                default:
                    break;
            }
        }
        protected override void OnEditSubmitting() {
            if(!this.PartsPurchaseOrderDetailForEditDataGridView.CommitEdit())
                return;

            var partsPurchasePlan = this.DataContext as PartsPurchasePlan;
            if(partsPurchasePlan == null)
                return;

            #region 数据校验
            partsPurchasePlan.ValidationErrors.Clear();
            foreach(var relation in partsPurchasePlan.PartsPurchasePlanDetails)
                relation.ValidationErrors.Clear();

            if(string.IsNullOrEmpty(partsPurchasePlan.WarehouseName))
                partsPurchasePlan.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_WarehouseIsNull, new[] {
                    "WarehouseName"
                }));

            if(partsPurchasePlan.PartsPlanTypeId <= 0)
                partsPurchasePlan.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PlanTypeIsNull, new[] {
                    "PartsPlanTypeId"
                }));

            if(string.IsNullOrEmpty(partsPurchasePlan.Code)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_PurchaseOrderCodeIsNull);
                return;
            }

            if(!partsPurchasePlan.PartsPurchasePlanDetails.Any()) {
                UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Title_PurchaseOrderPlanDetailsIsNull));
                return;
            }

            foreach(var relation in partsPurchasePlan.PartsPurchasePlanDetails) {
                if(string.IsNullOrEmpty(relation.SparePartCode))
                    relation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrderDetail_SparePartCodeIsNull, new[] {
                        "SparePartCode"
                    }));

                if(string.IsNullOrEmpty(relation.SparePartName))
                    relation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrderDetail_SparePartNameIsNull, new[] {
                        "SparePartName"
                    }));
            }

            foreach(var relation in partsPurchasePlan.PartsPurchasePlanDetails.Where(e => e.PlanAmount <= 0)) {
                UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Title_PlanAmountMustMoreThanZero, relation.SparePartCode));
                return;
            }
            #endregion


            if(partsPurchasePlan.HasValidationErrors || partsPurchasePlan.PartsPurchasePlanDetails.Any(relation => relation.HasValidationErrors))
                return;

            partsPurchasePlan.ApproveMemo = this.CbApproveMemo.Text;
            partsPurchasePlan.Memo = this.CbRemark.Text;
            partsPurchasePlan.Status = 66;
            if((!partsPurchasePlan.Status.Equals(66)) && (!partsPurchasePlan.Status.Equals(77))) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_ChooseApproveResult);
                return;
            }

            //调用服务端方法
            try {
                if(partsPurchasePlan.Can初审配件采购计划订单)
                    partsPurchasePlan.初审配件采购计划订单();
                ExecuteSerivcesMethod(PartsPurchasingUIStrings.DataEditView_Notification_OperateSuccess);
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            ((IEditableObject)partsPurchasePlan).EndEdit();//结束编辑
        }

        public PartsPurchasePlanOrderForFirstApproveDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += PartsPurchaseOrderDataEditView_DataContextChanged;
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void Reset() {
            var partsPurchasePlan = this.DataContext as PartsPurchasePlan;
            if(this.DomainContext.PartsPurchasePlans.Contains(partsPurchasePlan))
                this.DomainContext.PartsPurchasePlans.Detach(partsPurchasePlan);
        }

        private FileUploadForPartsOuterPurchaseDataEditPanel productDataEditPanels;

        protected FileUploadForPartsOuterPurchaseDataEditPanel DataEditPanels {
            get {
                return this.productDataEditPanels ?? (this.productDataEditPanels = (FileUploadForPartsOuterPurchaseDataEditPanel)DI.GetDataEditPanel("FileUploadForPartsOuterPurchase"));
            }
        }
    }
}