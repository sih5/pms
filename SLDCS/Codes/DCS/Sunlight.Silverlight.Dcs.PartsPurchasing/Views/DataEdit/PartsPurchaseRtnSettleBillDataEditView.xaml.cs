﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Controls;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.MaskedInput;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class PartsPurchaseRtnSettleBillDataEditView : INotifyPropertyChanged {
        private readonly string[] kvNames = {
            "PartsPurchaseRtnSettleBill_InvoicePath","PartsSalesSettlement_RebateMethod"
        };

        private bool searchCanUse, sureCanUse;
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        private KeyValueManager keyValueManager;
        private ObservableCollection<OutboundAndInboundBill> outboundAndInboundBills = new ObservableCollection<OutboundAndInboundBill>();
        private ObservableCollection<PartsPurchaseRtnSettleDetail> partsPurchaseRtnSettleDetails = new ObservableCollection<PartsPurchaseRtnSettleDetail>();
        private DataGridViewBase partsPurchaseRtnSettleRefForEditDataGridView;
        private DataGridViewBase partsPurchaseRtnSettleDetailForEditDataGridView;
        private DateTime cutoffTime = DateTime.Now.Date;
        private decimal? rebateAmount;
        private int? rebateMethod;
        private ButtonItem saveBtn, returnBtn;
        private ICommand searchCommand, sureCommand;
        private RadMaskedNumericInput txtSpreadsheet;
        private ObservableCollection<KeyValuePair> rebateMethods;

        private DateTime? invoiceDate;
        public ObservableCollection<KeyValuePair> RebateMethods {
            get {
                return this.rebateMethods ?? (rebateMethods = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<OutboundAndInboundBill> OutboundAndInboundBills {
            get {
                return outboundAndInboundBills ?? (this.outboundAndInboundBills = new ObservableCollection<OutboundAndInboundBill>());
            }
        }

        public ObservableCollection<PartsPurchaseRtnSettleDetail> PartsPurchaseRtnSettleDetails {
            get {
                return partsPurchaseRtnSettleDetails ?? (this.partsPurchaseRtnSettleDetails = new ObservableCollection<PartsPurchaseRtnSettleDetail>());
            }
            set {
                partsPurchaseRtnSettleDetails = value;
            }
        }

        public ICommand DataCancleCommand {
            get {
                return this.CancelCommand;
            }
        }

        public ICommand SearchCommand {
            get {
                return this.searchCommand ?? (this.searchCommand = new Core.Command.DelegateCommand(this.Search));
            }
        }

        public ICommand SureCommand {
            get {
                return this.sureCommand ?? (this.sureCommand = new Core.Command.DelegateCommand(this.Sure));
            }
        }

        public bool SearchCanUse {
            get {
                return this.searchCanUse;
            }
            set {
                this.searchCanUse = value;
                this.OnPropertyChanged("SearchCanUse");
            }
        }

        public bool SureCanUse {
            get {
                return this.sureCanUse;
            }
            set {
                this.sureCanUse = value;
                this.OnPropertyChanged("SureCanUse");
            }
        }

        public DateTime CutoffTime {
            get {
                return this.cutoffTime == null ? this.cutoffTime : new DateTime(this.cutoffTime.Year, this.cutoffTime.Month, this.cutoffTime.Day, 23, 59, 59);
            }
            set {
                this.cutoffTime = value;
                this.OnPropertyChanged("CutoffTime");
            }
        }

        public decimal? RebateAmount {
            get {
                return this.rebateAmount;
            }
            set {
                this.rebateAmount = value;
                this.OnPropertyChanged("RebateAmount");
            }
        }

        public int? RebateMethod {
            get {
                return this.rebateMethod;
            }
            set {
                this.rebateMethod = value;
                this.OnPropertyChanged("RebateMethod");
            }
        }

        public KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        public ObservableCollection<KeyValuePair> KvInvoicePath {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public ObservableCollection<KeyValuePair> KvWarehouses {
            get {
                return this.kvWarehouses;
            }
        }

        protected override bool OnRequestCanSubmit()
        {
            this.DcsComboBoxPartsSalesCategoryName.SelectedIndex = 0;//默认品牌
            return true;
        }

        private DataGridViewBase PartsPurchaseRtnSettleRefForEditDataGridView {
            get {
                if(this.partsPurchaseRtnSettleRefForEditDataGridView == null) {
                    this.partsPurchaseRtnSettleRefForEditDataGridView = DI.GetDataGridView("PartsPurchaseRtnSettleRefForEdit");
                    this.partsPurchaseRtnSettleRefForEditDataGridView.DomainContext = this.DomainContext;
                    this.partsPurchaseRtnSettleRefForEditDataGridView.DataContext = this;
                }
                return this.partsPurchaseRtnSettleRefForEditDataGridView;
            }
        }

        private DataGridViewBase PartsPurchaseRtnRtnSettleDetailForEditDataGridView {
            get {
                if(this.partsPurchaseRtnSettleDetailForEditDataGridView == null) {
                    this.partsPurchaseRtnSettleDetailForEditDataGridView = DI.GetDataGridView("PartsPurchaseRtnSettleDetailForEdit");
                    this.partsPurchaseRtnSettleDetailForEditDataGridView.DomainContext = this.DomainContext;
                    this.partsPurchaseRtnSettleDetailForEditDataGridView.DataContext = this;
                }
                return this.partsPurchaseRtnSettleDetailForEditDataGridView;
            }
        }

        //控制查询按钮启用
        private void PartsPurchaseRtnSettleBill_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsPurchaseRtnSettleBill = this.DataContext as PartsPurchaseRtnSettleBill;
            if(partsPurchaseRtnSettleBill == null)
                return;
            switch(e.PropertyName) {
                case "PartsSupplierId":
                case "WarehouseId":
                case "CutoffTime":
                case "PartsSalesCategoryId":
                    if(partsPurchaseRtnSettleBill != null && (partsPurchaseRtnSettleBill.PartsSalesCategoryId != null && partsPurchaseRtnSettleBill.PartsSupplierCode != "" && partsPurchaseRtnSettleBill.WarehouseId != 0 && !string.IsNullOrEmpty(this.CutoffTime.ToShortDateString()))) {
                        SearchCanUse = true;
                    } else {
                        SearchCanUse = false;
                    }
                    var singleOrDefault = this.kvWarehouses.SingleOrDefault(v => v.Key == partsPurchaseRtnSettleBill.WarehouseId);
                    if(singleOrDefault != null) {
                        var warehouse = singleOrDefault.UserObject as Warehouse;
                        if(warehouse != null) {
                            partsPurchaseRtnSettleBill.WarehouseId = warehouse.Id;
                            partsPurchaseRtnSettleBill.WarehouseCode = warehouse.Code;
                            partsPurchaseRtnSettleBill.WarehouseName = warehouse.Name;
                        }
                    }
                    //清单存在数据 清空
                    if(OutboundAndInboundBills.Any())
                        this.OutboundAndInboundBills.Clear();
                    break;
                case "TotalSettlementAmount":
                case "TaxRate":
                    if(partsPurchaseRtnSettleBill.TaxRate < 0 || partsPurchaseRtnSettleBill.TaxRate > 1)
                        partsPurchaseRtnSettleBill.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_partsPurchaseRtnSettleBill_TaxRateBetZeroOne, new[] {
                            "TaxRate"
                        }));
                    partsPurchaseRtnSettleBill.Tax = Math.Round(((partsPurchaseRtnSettleBill.TotalSettlementAmount * (decimal)partsPurchaseRtnSettleBill.TaxRate)), 2);
                    break;
            }
        }

        private void PartsPurchaseRtnSettleBillDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            if(!(this.DataContext is PartsPurchaseRtnSettleBill))
                return;
            var partsPurchaseRtnSettleBill = this.DataContext as PartsPurchaseRtnSettleBill;
            partsPurchaseRtnSettleBill.PropertyChanged -= this.PartsPurchaseRtnSettleBill_PropertyChanged;
            partsPurchaseRtnSettleBill.PropertyChanged += this.PartsPurchaseRtnSettleBill_PropertyChanged;
        }

        private DcsComboBox Combbox {
            get;
            set;
        }
        private void CreateUI() {
            this.HideSaveButton();
            this.HideCancelButton();
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsPurchasingUIStrings.DataGridView_Title_OutboundAndInboundBill, null, this.PartsPurchaseRtnSettleRefForEditDataGridView);
            detailDataEditView.SetValue(Grid.RowProperty, 4);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsPurchasingUIStrings.DataEditView_Title_ExportPartDetails,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/export.png", UriKind.Relative),
                Command = new Core.Command.DelegateCommand(this.ExportNoGoldenTaxClassify)
            });
            this.OutboundAndInboundBillData.Children.Add(detailDataEditView);
            var detailDataEditView2 = new DcsDetailDataEditView();
            detailDataEditView2.Register(PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseRtnSettleDetails, null, this.PartsPurchaseRtnRtnSettleDetailForEditDataGridView);
            detailDataEditView2.SetValue(Grid.RowProperty, 3);
            detailDataEditView2.UnregisterButton(detailDataEditView2.InsertButton);
            detailDataEditView2.UnregisterButton(detailDataEditView2.DeleteButton);
            detailDataEditView2.RegisterButton(new ButtonItem {
                Title = PartsPurchasingUIStrings.Action_Title_MergerLists,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/merge.png", UriKind.Relative),
                Command = new DelegateCommand(this.MergerDetail, delegate {
                    return true;
                })
            });
            detailDataEditView2.RegisterButton(new ButtonItem {
                Title = PartsPurchasingUIStrings.DataEditView_Title_ExportDetails,
                Icon = Utils.MakeServerUri("Client/Dcs/Images/Operations/ExportDetail.png"),
                Command = new DelegateCommand(this.ExportPartsPurchaseRtnRtnSettleDetail, delegate {
                    return true;
                })
            });
            var testCalc = new Grid();
            testCalc.ColumnDefinitions.Add(new ColumnDefinition {
                Width = GridLength.Auto
            });
            testCalc.ColumnDefinitions.Add(new ColumnDefinition {
                Width = new GridLength(150)
            });
            testCalc.ColumnDefinitions.Add(new ColumnDefinition {
                Width = new GridLength(35)
            });
            testCalc.RowDefinitions.Add(new RowDefinition {
                Height = new GridLength(20)
            });
            var textBlock = new TextBlock {
                Text = PartsPurchasingUIStrings.DataEditView_Title_ReturnFee
            };
            this.Combbox = new DcsComboBox {
                Height = 22,
                Width = 150,
                ItemsSource = RebateMethods,
                CanKeyboardNavigationSelectItems = true,
                ClearSelectionButtonVisibility = Visibility.Visible,
                ClearSelectionButtonContent = PartsPurchasingUIStrings.DataEditView_Title_Clear,
                IsEditable = false,
                IsMouseWheelEnabled = true,
                DisplayMemberPath = "Value",
                SelectedValuePath = "Key",
                SelectedValue = (int)DcsPartsSalesSettlementRebateMethod.直接折扣
            };
            this.Combbox.SelectionChanged += combbox_SelectionChanged;
            this.Combbox.SetValue(Grid.RowProperty, 2);
            this.Combbox.SetValue(Grid.ColumnProperty, 1);
            this.gdPartsPurchaseRtnSettleBill.Children.Add(this.Combbox);
            textBlock.SetValue(Grid.ColumnProperty, 0);
            testCalc.Children.Add(textBlock);
            txtSpreadsheet = new RadMaskedNumericInput();
            txtSpreadsheet.MinWidth = 130;
            txtSpreadsheet.Mask = "";
            txtSpreadsheet.FormatString = "n2";
            txtSpreadsheet.IsClearButtonVisible = false;
            txtSpreadsheet.SpinMode = SpinMode.Position;
            txtSpreadsheet.SelectionOnFocus = SelectionOnFocus.SelectAll;
            txtSpreadsheet.VerticalAlignment = VerticalAlignment.Center;
            txtSpreadsheet.HorizontalContentAlignment = HorizontalAlignment.Right;
            txtSpreadsheet.Margin = new Thickness(10, -2, 10, 0);
            txtSpreadsheet.SetValue(Grid.ColumnProperty, 1);
            txtSpreadsheet.KeyDown += txtSpreadsheet_KeyDown;
            testCalc.Children.Add(txtSpreadsheet);
            var radButton = new RadButton {
                Content = PartsPurchasingUIStrings.DataEditView_Title_Try,
                Command = new Core.Command.DelegateCommand(this.Spreadsheet)
            };
            radButton.SetValue(Grid.ColumnProperty, 2);
            testCalc.Children.Add(radButton);
            testCalc.HorizontalAlignment = HorizontalAlignment.Center;
            testCalc.VerticalAlignment = VerticalAlignment.Top;
            testCalc.SetValue(Grid.RowProperty, 1);
            testCalc.SetValue(Grid.ColumnProperty, 0);
            testCalc.Margin = new Thickness(0, 5, 0, 0);
            this.PartsPurchaseSettleDetailData.Children.Add(detailDataEditView2);
            this.PartsPurchaseSettleDetailData.Children.Add(testCalc);
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoryByPersonSalesCenterLinkQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.DcsComboBoxPartsSalesCategoryName.ItemsSource = loadOp.Entities;
                this.DcsComboBoxPartsSalesCategoryName.SelectedIndex = 0;//默认品牌
            }, null);
            this.saveBtn = new ButtonItem {
                Title = PartsPurchasingUIStrings.Action_Title_Save,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/save.png", UriKind.Relative),
                Command = new DelegateCommand(this.SaveCurrentData, delegate {
                    return true;
                })
            };
            this.returnBtn = new ButtonItem {
                Title = PartsPurchasingUIStrings.Action_Title_Return,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/return.png", UriKind.Relative),
                Command = new DelegateCommand(this.ReturnCurrentData, delegate {
                    return true;
                })
            };
        }


        private void ExportNoGoldenTaxClassify() {
            var partsPurchaseRtnSettleBill = this.DataContext as PartsPurchaseRtnSettleBill;
            if(partsPurchaseRtnSettleBill == null)
                return;
            //this.OutboundAndInboundBills.Clear();
            //foreach(var partsPurchaseRtnSettleRef in partsPurchaseRtnSettleBill.PartsPurchaseRtnSettleRefs) {
            //    if(partsPurchaseRtnSettleBill.PartsPurchaseRtnSettleRefs.Contains(partsPurchaseRtnSettleRef))
            //        partsPurchaseRtnSettleBill.PartsPurchaseRtnSettleRefs.Remove(partsPurchaseRtnSettleRef);
            //}
            int[] outboundTypes = {
                (int)DcsPartsOutboundType.采购退货
            };
            this.DomainContext.ExportPurchaseSettlementOutboundAndInboundBillDetailForWithGoldenTaxClassifyCode(BaseApp.Current.CurrentUserData.EnterpriseId, partsPurchaseRtnSettleBill.PartsSupplierId, partsPurchaseRtnSettleBill.WarehouseId, null, partsPurchaseRtnSettleBill.PartsSalesCategoryId, outboundTypes, this.CutoffTime, loadOp1 => {
                if(loadOp1.HasError)
                    return;
                if(loadOp1.Value == null || string.IsNullOrEmpty(loadOp1.Value)) {
                    UIHelper.ShowNotification(loadOp1.Value);
                }
                if(loadOp1.Value != null && !string.IsNullOrEmpty(loadOp1.Value)) {
                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp1.Value));
                }
            }, null);
        }
        //返利
        private void combbox_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            var partsPurchaseRtnSettleBill = this.DataContext as PartsPurchaseRtnSettleBill;
            if(partsPurchaseRtnSettleBill == null)
                return;
            RebateMethod = (int?)this.Combbox.SelectedValue;
            var partsPurchaseRtnSettleDetail = this.PartsPurchaseRtnSettleDetails.FirstOrDefault(ex => ex.SparePartCode == "折扣商品");
            //不返利时候  删除折扣商品数据 清除返利金额
            if(partsPurchaseRtnSettleDetail != null)
                this.PartsPurchaseRtnSettleDetails.Remove(partsPurchaseRtnSettleDetail);
            RebateAmount = null;
            txtSpreadsheet.Value = null;
            partsPurchaseRtnSettleBill.TotalSettlementAmount = this.PartsPurchaseRtnSettleDetails.Sum(entity => entity.SettlementAmount);
        }

        private void txtSpreadsheet_KeyDown(object sender, KeyEventArgs e) {
            if(e.Key == Key.Enter)
                this.Spreadsheet();
        }

        // 试算
        private void Spreadsheet() {
            var partsPurchaseRtnSettleBill = this.DataContext as PartsPurchaseRtnSettleBill;
            if(partsPurchaseRtnSettleBill == null)
                return;
            // 试算的值
            var spreadsheet = this.txtSpreadsheet.Value ?? 0;
            //if(spreadsheet == null)
            //    return;
            if(RebateMethod == (int)DcsPartsSalesSettlementRebateMethod.直接折扣) {
                RebateAmount = Convert.ToDecimal(spreadsheet);
            } else {
                return;
            }
            //设计未注明折扣金额是否可大于结算金额
            if(RebateAmount > partsPurchaseRtnSettleBill.TotalSettlementAmount)
                return;
            var partsPurchaseRtnSettleDetail = this.PartsPurchaseRtnSettleDetails.FirstOrDefault(ex => ex.SparePartCode == "折扣商品");
            //当返利摊销为0时 删除折扣商品数据
            if(RebateAmount == 0 || RebateAmount == null) {
                if(partsPurchaseRtnSettleDetail != null)
                    PartsPurchaseRtnSettleDetails.Remove(partsPurchaseRtnSettleDetail);
                //计算总金额（减去返利金额）
                partsPurchaseRtnSettleBill.TotalSettlementAmount = this.PartsPurchaseRtnSettleDetails.Sum(entity => entity.SettlementAmount);
                return;
            }
            //已经存在返利商品。 不需要继续新增。 只需要修改里面的金额即可
            if(partsPurchaseRtnSettleDetail != null) {
                partsPurchaseRtnSettleDetail.SettlementPrice = RebateAmount.Value * -1;
                partsPurchaseRtnSettleDetail.SettlementAmount = RebateAmount.Value * -1;
                //计算总金额（减去返利金额）
                partsPurchaseRtnSettleBill.TotalSettlementAmount = this.PartsPurchaseRtnSettleDetails.Sum(entity => entity.SettlementAmount);
                return;
            }
            //增加返利摊销清单
            this.PartsPurchaseRtnSettleDetails.Add(new PartsPurchaseRtnSettleDetail {
                SerialNumber = PartsPurchaseRtnSettleDetails.Max(x => x.SerialNumber) + 1,
                SparePartCode = "折扣商品",
                SparePartName = "折扣商品",
                SettlementPrice = RebateAmount.Value * -1,
                QuantityToSettle = 1,
                SettlementAmount = RebateAmount.Value * -1
            });
            //计算总金额（减去返利金额）
            partsPurchaseRtnSettleBill.TotalSettlementAmount = this.PartsPurchaseRtnSettleDetails.Sum(entity => entity.SettlementAmount);
        }

        private void ExportPartsPurchaseRtnRtnSettleDetail(object obj) {
            ShellViewModel.Current.IsBusy = true;
            var partsPurchaseRtnSettleBill = this.DataContext as PartsPurchaseRtnSettleBill;
            if(partsPurchaseRtnSettleBill == null)
                return;

            foreach(var partsPurchaseRtnSettleDetail in partsPurchaseRtnSettleBill.PartsPurchaseRtnSettleDetails) {
                if(partsPurchaseRtnSettleBill.PartsPurchaseRtnSettleDetails.Contains(partsPurchaseRtnSettleDetail))
                    partsPurchaseRtnSettleBill.PartsPurchaseRtnSettleDetails.Remove(partsPurchaseRtnSettleDetail);
            }
            int[] BillIds = this.OutboundAndInboundBills.Select(item => item.BillId).ToArray();
            int[] inbound = {
            };
            //this.DomainContext.Load(this.DomainContext.GetBranchstrategiesQuery().Where(r => r.BranchId == partsPurchaseRtnSettleBill.BranchId), LoadBehavior.RefreshCurrent, loadBranch => {
            //    if(loadBranch.HasError) {
            //        loadBranch.MarkErrorAsHandled();
            //        return;
            //    }
            //if(loadBranch.Entities == null)
            //return;
            //var branchStrategies = loadBranch.Entities.First();
            //if(branchStrategies.PartsPurchasePricingStrategy == (int)DcsBranchstrategyPartsPurchasePricingStrategy.先定价) {
            if(BillIds.Length == 0)
                return;
            (this.PartsPurchaseRtnRtnSettleDetailForEditDataGridView.DomainContext as DcsDomainContext).ExportPartsSalesSettlementBill(inbound, BillIds.ToArray(), null, null, "配件采购退货结算单清单", loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                    UIHelper.ShowNotification(loadOp.Value);
                }
                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                ShellViewModel.Current.IsBusy = false;
            }, null);
            //} else {
            //    if(BillIds.Length == 0)
            //        return;
            //    (this.PartsPurchaseRtnRtnSettleDetailForEditDataGridView.DomainContext as DcsDomainContext).ExportPartsSalesSettlementBill(inbound, BillIds.ToArray(), partsPurchaseRtnSettleBill.PartsSalesCategoryId.Value, partsPurchaseRtnSettleBill.PartsSupplierId, "配件采购退货结算单清单", loadOp => {
            //        if(loadOp.HasError)
            //            return;
            //        if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
            //            UIHelper.ShowNotification(loadOp.Value);
            //        }
            //        if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
            //            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
            //        ShellViewModel.Current.IsBusy = false;
            //    }, null);
            //}
            //}, null);
        }

        private void Initialize() {
            this.KeyValueManager.LoadData(() => {
                foreach(var textBox in this.ChildrenOfType<TextBox>()) {
                    var expression = textBox.GetBindingExpression(TextBox.TextProperty);
                    if(expression == null)
                        continue;
                    var binding = expression.ParentBinding;
                    if(!(binding.Converter is KeyValueItemConverter))
                        continue;
                    textBox.SetBinding(TextBox.TextProperty, binding);
                }
            });
            this.KeyValueManager.LoadData(() => {
                if(BaseApp.Current.CurrentUserData.EnterpriseCode == "1101") {
                        KvInvoicePath.Remove(this.keyValueManager[this.kvNames[0]].FirstOrDefault(r => r.Key == (int)DcsPartsPurchaseRtnSettleBillInvoicePath.反开销售发票));
                }
                foreach(var item in this.keyValueManager[this.kvNames[1]].Where(r => r.Key == (int)DcsPartsSalesSettlementRebateMethod.直接折扣))
                    RebateMethods.Add(item);
            });
            var queryWindowPartsSupplier = DI.GetQueryWindow("PartsSupplierForPartsPurchaseRtnSettleBill");
            queryWindowPartsSupplier.SelectionDecided += this.QueryWindowPartsSupplier_SelectionDecided;
            this.popupTextBoxPartsSupplierCode.PopupContent = queryWindowPartsSupplier;
            this.DomainContext.Load(this.DomainContext.GetWarehousesOrderByNameQuery().Where(entity => entity.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if(loadOp.HasError || loadOp.Entities == null)
                    return;
                foreach(var warehouse in loadOp.Entities)
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name,
                        UserObject = warehouse
                    });
            }, null);
            this.Second.Visibility = Visibility.Collapsed;
            this.First.Visibility = Visibility.Visible;
            //this.RadRadioButtondCancle.Command=new DelegateCommand(this.Cancle)
        }

        //查询按钮事件
        private void Search() {
            var partsPurchaseRtnSettleBill = this.DataContext as PartsPurchaseRtnSettleBill;
            if(partsPurchaseRtnSettleBill == null)
                return;
            //this.OutboundAndInboundBills.Clear();
            //foreach(var partsPurchaseRtnSettleRef in partsPurchaseRtnSettleBill.PartsPurchaseRtnSettleRefs) {
            //    if(partsPurchaseRtnSettleBill.PartsPurchaseRtnSettleRefs.Contains(partsPurchaseRtnSettleRef))
            //        partsPurchaseRtnSettleBill.PartsPurchaseRtnSettleRefs.Remove(partsPurchaseRtnSettleRef);
            //}
            int[] outboundTypes = {
                (int)DcsPartsOutboundType.采购退货
            };
            this.DomainContext.Load(this.DomainContext.查询待采购退货结算出库明细Query(BaseApp.Current.CurrentUserData.EnterpriseId, partsPurchaseRtnSettleBill.PartsSupplierId, partsPurchaseRtnSettleBill.WarehouseId, null, partsPurchaseRtnSettleBill.PartsSalesCategoryId, outboundTypes, this.CutoffTime), loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Entities == null)
                    return;
                var OutboundAndInbounds = this.OutboundAndInboundBills.Where(e => e.EntityType == "Modify").ToList();
                this.OutboundAndInboundBills.Clear();
                foreach(var inbound in OutboundAndInbounds) {
                    inbound.SerialNumber = this.OutboundAndInboundBills.Count + 1;
                    this.OutboundAndInboundBills.Add(inbound);
                }
                partsPurchaseRtnSettleBill.TotalSettlementAmount = partsPurchaseRtnSettleBill.PartsPurchaseRtnSettleRefs.Sum(v => v.SettlementAmount);
                foreach(var outboundAndInboundBill in loadOp.Entities) {
                    this.DomainContext.OutboundAndInboundBills.Detach(outboundAndInboundBill);
                    outboundAndInboundBill.SerialNumber = OutboundAndInboundBills.Count + 1;
                    this.OutboundAndInboundBills.Add(outboundAndInboundBill);
                }
                this.SureCanUse = (this.OutboundAndInboundBills.Count > 0);
            }, null);
        }

        //确定按钮事件
        private decimal? originalRebateAmount;
        private void Sure() {
            
            var partsPurchaseRtnSettleBill = this.DataContext as PartsPurchaseRtnSettleBill;
            if(partsPurchaseRtnSettleBill == null)
                return;
            foreach(var partsPurchaseRtnSettleDetail in partsPurchaseRtnSettleBill.PartsPurchaseRtnSettleDetails) {
                if(partsPurchaseRtnSettleBill.PartsPurchaseRtnSettleDetails.Contains(partsPurchaseRtnSettleDetail))
                    partsPurchaseRtnSettleBill.PartsPurchaseRtnSettleDetails.Remove(partsPurchaseRtnSettleDetail);
            }
            int[] BillIds = this.OutboundAndInboundBills.Select(item => item.BillId).ToArray();
            int[] inbound = {
            };
            if(originalRebateAmount != null) {
                this.Combbox.SelectedValue = (int)DcsPartsSalesSettlementRebateMethod.直接折扣;
                RebateAmount = originalRebateAmount;
            }
            //this.DomainContext.Load(this.DomainContext.GetBranchstrategiesQuery().Where(r => r.BranchId == partsPurchaseRtnSettleBill.BranchId), LoadBehavior.RefreshCurrent, loadBranch => {
            //    if(loadBranch.HasError) {
            //        loadBranch.MarkErrorAsHandled();
            //        return;
            //    }
            //    if(loadBranch.Entities == null)
            //        return;
            //var branchStrategies = loadBranch.Entities.First();
            //if(branchStrategies.PartsPurchasePricingStrategy == (int)DcsBranchstrategyPartsPurchasePricingStrategy.先定价)
            var currentUserData = BaseApp.Current.CurrentUserData;
             DomainContext.Load(DomainContext.GetBranchesQuery().Where(r => r.Id == currentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp1 => {
                if(loadOp1.HasError)
                    if(!loadOp1.IsErrorHandled) {
                        loadOp1.MarkErrorAsHandled();
                        return;
                    }
                var branch = loadOp1.Entities.FirstOrDefault();
            this.DomainContext.Load(this.DomainContext.查询待结算出入库明细清单Query(inbound, BillIds.ToArray(), null), loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Entities == null)
                    return;
                int temp = 1;
                if(branch != null) {
                    if(branch.Code == "1101" || branch.Code == "2450" || branch.Code == "2290") {
                        bool flag = loadOp.Entities.Any(r => string.IsNullOrEmpty(r.GoldenTaxClassifyCode));
                        if(flag) {
                            UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_PartABCIsNull);
                            return;
                        }
                    }
                }
                this.First.Visibility = Visibility.Collapsed;
                this.Second.Visibility = Visibility.Visible;
                this.RegisterButton(returnBtn, true);
                this.RegisterButton(saveBtn, true);
                PartsPurchaseRtnSettleDetails.Clear();
                foreach(var item in loadOp.Entities) {
                    this.PartsPurchaseRtnSettleDetails.Add(new PartsPurchaseRtnSettleDetail {
                        SerialNumber = temp++,
                        SparePartId = item.SparePartId,
                        SparePartCode = item.SparePartCode,
                        SparePartName = item.SparePartName,
                        QuantityToSettle = item.Quantity,
                        SettlementPrice = item.SettlementPrice,
                        SettlementAmount = item.SettlementAmount,
                    });
                }
                //todo:折扣商品如何增加增加返利列，根据清单是否存在折扣商品
                if(RebateAmount != null && RebateAmount != 0) {
                    this.PartsPurchaseRtnSettleDetails.Add(new PartsPurchaseRtnSettleDetail {
                        SerialNumber = PartsPurchaseRtnSettleDetails.Max(x => x.SerialNumber) + 1,
                        SparePartCode = "折扣商品",
                        SparePartName = "折扣商品",
                        SettlementPrice = RebateAmount.Value * -1,
                        QuantityToSettle = 1,
                        SettlementAmount = RebateAmount.Value * -1
                    });
                }
                //计算总金额（减去折扣商品金额）
                partsPurchaseRtnSettleBill.TotalSettlementAmount = this.PartsPurchaseRtnSettleDetails.Sum(entity => entity.SettlementAmount);
            }, null);
             }, null);
             int temps = 1;
             foreach(var item in this.OutboundAndInboundBills) {
                 if(partsPurchaseRtnSettleBill.PartsPurchaseRtnSettleRefs.All(r => r.SourceCode != item.BillCode)) {
                     partsPurchaseRtnSettleBill.PartsPurchaseRtnSettleRefs.Add(new PartsPurchaseRtnSettleRef {
                         SerialNumber = temps++,
                         SourceId = item.BillId,
                         SourceCode = item.BillCode,
                         SettlementAmount = item.SettlementAmount,
                         SourceBillCreateTime = item.BillCreateTime,
                         WarehouseId = item.WarehouseId,
                         WarehouseName = item.WarehouseName
                     });
                 }
             }
            //else
            //    this.DomainContext.Load(this.DomainContext.查询待结算出入库明细清单后定价Query(inbound, BillIds, partsPurchaseRtnSettleBill.PartsSalesCategoryId.Value, partsPurchaseRtnSettleBill.PartsSupplierId), LoadBehavior.RefreshCurrent, loadOp => {
            //        if(loadOp.HasError) {
            //            loadOp.MarkErrorAsHandled();
            //            return;
            //        }
            //        if(loadOp.Entities == null)
            //            return;
            //        int temp = 1;
            //        PartsPurchaseRtnSettleDetails.Clear();
            //        foreach(var item in loadOp.Entities) {
            //            this.PartsPurchaseRtnSettleDetails.Add(new PartsPurchaseRtnSettleDetail {
            //                SerialNumber = temp++,
            //                SparePartId = item.SparePartId,
            //                SparePartCode = item.SparePartCode,
            //                SparePartName = item.SparePartName,
            //                QuantityToSettle = item.Quantity,
            //                SettlementPrice = item.SettlementPrice,
            //                SettlementAmount = item.SettlementAmount,
            //            });
            //        }
            //        //todo:折扣商品如何增加增加返利列
            //        if(RebateAmount != null && RebateAmount != 0) {
            //            this.PartsPurchaseRtnSettleDetails.Add(new PartsPurchaseRtnSettleDetail {
            //                SerialNumber = PartsPurchaseRtnSettleDetails.Max(x => x.SerialNumber) + 1,
            //                SparePartCode = "折扣商品",
            //                SparePartName = "折扣商品",
            //                SettlementPrice = RebateAmount.Value * -1,
            //                QuantityToSettle = 1,
            //                SettlementAmount = RebateAmount.Value * -1
            //            });
            //        }
            //        //计算总金额（减去折扣商品金额）
            //        partsPurchaseRtnSettleBill.TotalSettlementAmount = this.PartsPurchaseRtnSettleDetails.Sum(entity => entity.SettlementAmount);
            //    }, null);
            //}, null);
        }
        public DateTime? InvoiceDate
        {
            get
            {
                return this.invoiceDate;
            }
            set
            {
                this.invoiceDate = value;
                this.OnPropertyChanged("InvoiceDate");
            }
        }
        //保存提交事件
        private void SaveCurrentData(object obj) {
            if(!this.PartsPurchaseRtnSettleRefForEditDataGridView.CommitEdit() || !this.PartsPurchaseRtnRtnSettleDetailForEditDataGridView.CommitEdit())
                return;
            var partsPurchaseRtnSettleBill = this.DataContext as PartsPurchaseRtnSettleBill;
            if(partsPurchaseRtnSettleBill == null)
                return;

            if (this.InvoiceDate.HasValue)
            {
                this.InvoiceDate = new DateTime(this.InvoiceDate.Value.Year, this.InvoiceDate.Value.Month, this.InvoiceDate.Value.Day, 00, 00, 00);
                partsPurchaseRtnSettleBill.InvoiceDate = this.InvoiceDate.Value;
            } else if(!partsPurchaseRtnSettleBill.InvoiceDate.HasValue)
            {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceDateIsNull);
                return;
            }
            if(partsPurchaseRtnSettleBill.TotalSettlementAmount < 0) {
                UIHelper.ShowAlertMessage(PartsPurchasingUIStrings.DataEditView_Validation_partsPurchaseRtnSettleBill_TotalSettlementAmountLessThanZero);
                return;
            }

            if(partsPurchaseRtnSettleBill.InvoicePath == default(int)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_partsPurchaseRtnSettleBill_IsBullInvoicePath);
                return;
            }
            foreach(var item in PartsPurchaseRtnSettleDetails) {
                partsPurchaseRtnSettleBill.PartsPurchaseRtnSettleDetails.Add(item);
            }
            //int temp = 1;
            //foreach(var item in OutboundAndInboundBills) {
            //    if(partsPurchaseRtnSettleBill.PartsPurchaseRtnSettleRefs.All(r => r.SourceCode != item.BillCode)) {
            //        partsPurchaseRtnSettleBill.PartsPurchaseRtnSettleRefs.Add(new PartsPurchaseRtnSettleRef {
            //            SerialNumber = temp++,
            //            SourceId = item.BillId,
            //            SourceCode = item.BillCode,
            //            SettlementAmount = item.SettlementAmount,
            //            SourceBillCreateTime = item.BillCreateTime,
            //            WarehouseId = item.WarehouseId,
            //            WarehouseName = item.WarehouseName
            //        });
            //    }
            //}
            partsPurchaseRtnSettleBill.TotalSettlementAmount = this.PartsPurchaseRtnSettleDetails.Sum(entity => entity.SettlementAmount);
            ((IEditableObject)partsPurchaseRtnSettleBill).EndEdit();
            //校验采购退货结算单的总金额是否大于分公司策略的最大开票金额,清单数是否大于分公司策略的最大开票行数
            this.DomainContext.Load(this.DomainContext.GetBranchstrategiesQuery().Where(r => r.BranchId == partsPurchaseRtnSettleBill.BranchId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var branchstrategy = loadOp.Entities.SingleOrDefault();
                if(branchstrategy != null && partsPurchaseRtnSettleBill.TotalSettlementAmount > branchstrategy.MaxInvoiceAmount) {
                    UIHelper.ShowAlertMessage(PartsPurchasingUIStrings.DataEditView_Validation_TotalSettlementAmountError);
                    return;
                }
                if(partsPurchaseRtnSettleBill.InvoicePath == (int)DcsPartsPurchaseRtnSettleBillInvoicePath.反开销售发票) {
                    if(branchstrategy != null && partsPurchaseRtnSettleBill.PartsPurchaseRtnSettleDetails.Count > branchstrategy.MaxInvoiceRow) {
                        DcsUtils.Confirm(string.Format(PartsPurchasingUIStrings.DataEditView_Validation_DetailsAmountError, branchstrategy.MaxInvoiceRow), () => this.SubmitData(partsPurchaseRtnSettleBill));
                    } else {
                        this.SubmitData(partsPurchaseRtnSettleBill);
                    }
                } else {
                    this.SubmitData(partsPurchaseRtnSettleBill);
                }
            }, null);
        }

        private void SubmitData(PartsPurchaseRtnSettleBill partsPurchaseRtnSettleBill) {
            try {
                if(this.EditState == DataEditState.New) {
                    if(partsPurchaseRtnSettleBill.Can生成配件采购退货结算单)
                        partsPurchaseRtnSettleBill.生成配件采购退货结算单();
                } else {
                    if(partsPurchaseRtnSettleBill.Can修改配件采购退货结算单)
                        partsPurchaseRtnSettleBill.修改配件采购退货结算单();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        //返回
        private void ReturnCurrentData(object obj) {
            var partsPurchaseRtnSettleBill = this.DataContext as PartsPurchaseRtnSettleBill;
            if(partsPurchaseRtnSettleBill == null)
                return;
            partsPurchaseRtnSettleBill.TotalSettlementAmount = OutboundAndInboundBills.Sum(item => item.SettlementAmount);
            this.UnregisterButton(saveBtn);
            this.UnregisterButton(returnBtn);
            this.Second.Visibility = Visibility.Collapsed;
            this.First.Visibility = Visibility.Visible;
        }

        //清单合并事件
        private void MergerDetail(object obj) {
            var partsPurchaseRtnSettleBill = this.DataContext as PartsPurchaseRtnSettleBill;
            if(partsPurchaseRtnSettleBill == null)
                return;
            var mergeDetail = from detail in PartsPurchaseRtnSettleDetails
                              group detail by new {
                                  detail.SparePartId,
                                  detail.SettlementPrice
                              } into g
                              select new {
                                  g.Key.SparePartId,
                                  g.Key.SettlementPrice
                              };
            var mergeCollection = new ObservableCollection<PartsPurchaseRtnSettleDetail>();
            var serialNumber = 1;
            foreach(var detailItem in mergeDetail) {
                var item = detailItem;
                var details = this.PartsPurchaseRtnSettleDetails.Where(e => e.SparePartId == item.SparePartId && e.SettlementPrice == item.SettlementPrice);
                var purchaseSettleDetails = details as PartsPurchaseRtnSettleDetail[] ?? details.ToArray();
                if(purchaseSettleDetails.Any())
                    mergeCollection.Add(new PartsPurchaseRtnSettleDetail {
                        SerialNumber = serialNumber++,
                        SparePartId = purchaseSettleDetails.First().SparePartId,
                        SparePartCode = purchaseSettleDetails.First().SparePartCode,
                        SparePartName = purchaseSettleDetails.First().SparePartName,
                        SettlementPrice = purchaseSettleDetails.First().SettlementPrice,
                        QuantityToSettle = purchaseSettleDetails.Sum(e => e.QuantityToSettle),
                        SettlementAmount = purchaseSettleDetails.First().SettlementPrice * purchaseSettleDetails.Sum(e => e.QuantityToSettle),
                    });
            }
            partsPurchaseRtnSettleBill.TotalSettlementAmount = mergeCollection.Sum(e => e.SettlementAmount);
            this.PartsPurchaseRtnSettleDetails = new ObservableCollection<PartsPurchaseRtnSettleDetail>(mergeCollection.OrderBy(e => e.QuantityToSettle).ThenBy(e => e.SettlementAmount));
            this.OnPropertyChanged("PartsPurchaseRtnSettleDetails");
        }

        private void QueryWindowPartsSupplier_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var partsSupplier = queryWindow.SelectedEntities.Cast<PartsSupplier>().FirstOrDefault();
            if(partsSupplier == null)
                return;
            var partsPurchaseRtnSettleBill = this.DataContext as PartsPurchaseRtnSettleBill;
            if(partsPurchaseRtnSettleBill == null)
                return;
            partsPurchaseRtnSettleBill.PartsSupplierId = partsSupplier.Id;
            partsPurchaseRtnSettleBill.PartsSupplierName = partsSupplier.Name;
            partsPurchaseRtnSettleBill.PartsSupplierCode = partsSupplier.Code;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load((EntityQuery)this.DomainContext.GetPartsPurchaseRtnSettleBillsWithDetailsAndRefsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault() as PartsPurchaseRtnSettleBill;
                if(entity != null) {
                    this.OutboundAndInboundBills.Clear();
                    this.PartsPurchaseRtnSettleDetails.Clear();
                    int SerialNumber = 1;
                    foreach(var item in entity.PartsPurchaseRtnSettleRefs) {
                        var outboundAndInboundBill = new OutboundAndInboundBill();
                        outboundAndInboundBill.SerialNumber = SerialNumber++;
                        outboundAndInboundBill.EntityType = "Modify";
                        outboundAndInboundBill.BillId = item.SourceId;
                        outboundAndInboundBill.BillCode = item.SourceCode;
                        outboundAndInboundBill.BillCreateTime = item.SourceBillCreateTime;
                        outboundAndInboundBill.SettlementAmount = item.SettlementAmount;
                        outboundAndInboundBill.WarehouseName = item.WarehouseName;
                        outboundAndInboundBill.WarehouseId = item.WarehouseId;
                        OutboundAndInboundBills.Add(outboundAndInboundBill);
                        entity.PartsPurchaseRtnSettleRefs.Remove(item);//必要代码删除后修改报错
                    }
                    //修改的时候加载折扣商品
                    if(entity.PartsPurchaseRtnSettleDetails.Any(r => r.SparePartCode == "折扣商品")) {
                        originalRebateAmount = entity.PartsPurchaseRtnSettleDetails.First(r => r.SparePartCode == "折扣商品").SettlementAmount * -1;
                    }
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsPurchaseRtnSettleBill;
            }
        }

        public override void SetObjectToEditById(object id) {
            SearchCanUse = true;
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
            this.SearchCanUse = true;
        }

        protected override void Reset() {
            this.Second.Visibility = Visibility.Collapsed;
            this.First.Visibility = Visibility.Visible;
            this.OutboundAndInboundBills.Clear();
            this.Combbox.SelectedValue = default(int);
            this.txtSpreadsheet.Value = null;
            this.originalRebateAmount = default(decimal);
            this.RebateAmount = null;
            if(saveBtn != null)
                this.UnregisterButton(saveBtn);
            if(returnBtn != null)
                this.UnregisterButton(returnBtn);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public PartsPurchaseRtnSettleBillDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.Initialize);
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += this.PartsPurchaseRtnSettleBillDataEditView_DataContextChanged;
            this.OutboundAndInboundBills.CollectionChanged += this.OutboundAndInboundBillsCollectionChanged;
        }

        private void OutboundAndInboundBillsCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e) {
            var partsPurchaseRtnSettleBill = this.DataContext as PartsPurchaseRtnSettleBill;
            if(partsPurchaseRtnSettleBill == null)
                return;
            SureCanUse = (this.OutboundAndInboundBills.Count > 0);
            partsPurchaseRtnSettleBill.TotalSettlementAmount = OutboundAndInboundBills.Sum(item => item.SettlementAmount);
        }

        private void RadDateTimePicker_SelectionChanged_1(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            //清单存在数据 清空
            if(OutboundAndInboundBills.Any())
                this.OutboundAndInboundBills.Clear();
        }
    }
}
