﻿
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class PartsPurchaseRtnSettleBillInvoiceRegisterDataEditView {
        private ObservableCollection<InvoiceInformation> invoiceInformations;
        private DataGridViewBase invoiceInformationForSettleDataGridView;

        public DataGridViewBase InvoiceInformationForSettleDataGridView {
            get {
                if(this.invoiceInformationForSettleDataGridView == null) {
                    this.invoiceInformationForSettleDataGridView = DI.GetDataGridView("InvoiceInformationForRtnSettle");
                    this.invoiceInformationForSettleDataGridView.DomainContext = this.DomainContext;
                    this.invoiceInformationForSettleDataGridView.DataContext = this;
                }
                return this.invoiceInformationForSettleDataGridView;
            }
        }

        public ObservableCollection<InvoiceInformation> InvoiceInformations {
            get {
                return this.invoiceInformations ?? (this.invoiceInformations = new ObservableCollection<InvoiceInformation>());
            }
        }

        public PartsPurchaseRtnSettleBillInvoiceRegisterDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsPurchasingUIStrings.DataGridView_Title_InvoiceInformation, null, () => this.InvoiceInformationForSettleDataGridView);
            detailDataEditView.SetValue(Grid.RowProperty, 1);
            detailDataEditView.SetValue(Grid.ColumnSpanProperty, 8);
            this.Root.Children.Add(detailDataEditView);
        }

        protected override void OnEditSubmitting() {
            if(!this.InvoiceInformationForSettleDataGridView.CommitEdit())
                return;

            var partsPurchaseRtnSettleBill = this.DataContext as PartsPurchaseRtnSettleBill;
            if(partsPurchaseRtnSettleBill == null)
                return;

            if(!this.InvoiceInformations.Any()) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseSettleBill_InvoiceInformationsIsNull);
                return;
            }
            var invoiceAmountTotal = this.InvoiceInformations.Sum(v => v.InvoiceAmount);
            if(InvoiceInformations.GroupBy(entity => entity.InvoiceNumber).Any(array => array.Count() > 1)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceCodeRepeat);
                return;
            }
            foreach(var invoiceInformation in InvoiceInformations) {
                invoiceInformation.ValidationErrors.Clear();
                if(string.IsNullOrEmpty(invoiceInformation.InvoiceCode))
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceCodeIsNull, new[] {
                    "InvoiceCode"
                }));
                if(string.IsNullOrEmpty(invoiceInformation.InvoiceNumber))
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceNumberIsNull, new[] {
                    "InvoiceNumber"
                }));
                if(!invoiceInformation.InvoiceAmount.HasValue) {
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceAmountIsNull, new[] {
                    "InvoiceAmount"
                }));
                } else if(invoiceInformation.InvoiceAmount <= 0) {
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceAmountMustGreaterZero, new[] {
                    "InvoiceAmount"
                }));
                }
                if(!invoiceInformation.TaxRate.HasValue)
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_TaxRateIsNull, new[] {
                    "TaxRate"
                }));
                if(!invoiceInformation.InvoiceTax.HasValue)
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceTaxIsNull, new[] {
                    "InvoiceTax"
                }));
                if(!invoiceInformation.InvoiceDate.HasValue)
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceDateIsNull, new[] {
                    "InvoiceDate"
                }));
                if(!invoiceInformation.HasValidationErrors)
                    ((IEditableObject)invoiceInformation).EndEdit();
                //((IEditableObject)invoiceInformation).EndEdit();
                if(!this.DomainContext.InvoiceInformations.Contains(invoiceInformation))
                    this.DomainContext.InvoiceInformations.Add(invoiceInformation);
                //this.DomainContext.InvoiceInformations.Add(invoiceInformation);
            }

            //if(InvoiceInformations.GroupBy(entity => entity.InvoiceNumber).Any(array => array.Count() > 1)) {
            //    UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceCodeRepeat);
            //    return;
            //}
            if(InvoiceInformations.Any(e => e.HasValidationErrors))
                return;
            ((IEditableObject)partsPurchaseRtnSettleBill).EndEdit();
            try {
                if(partsPurchaseRtnSettleBill.Can配件采购退货结算单发票登记)
                    if(invoiceAmountTotal != null)
                        partsPurchaseRtnSettleBill.配件采购退货结算单发票登记(invoiceAmountTotal.Value);
            } catch(ValidationException ex) {
                this.DomainContext.InvoiceInformations.Clear();
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsPurchaseRtnSettleBillsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditCancelled() {
            base.OnEditCancelled();
            this.InvoiceInformations.Clear();
        }

        protected override void OnEditSubmitted() {
            base.OnEditSubmitted();
            this.InvoiceInformations.Clear();
        }

        protected override string Title {
            get {
                return PartsPurchasingUIStrings.DataEditView_Title_InvoiceRegister_PartsPurchaseRtnSettleBill;
            }
        }

        protected override void Reset() {
            var invoiceInformation = this.DataContext as InvoiceInformation;
            if(invoiceInformation != null && this.DomainContext.InvoiceInformations.Contains(invoiceInformation))
                this.DomainContext.InvoiceInformations.Detach(invoiceInformation);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
