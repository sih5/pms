﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Web.Entities;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class PartsPurchaseSettleBillDataEditView : INotifyPropertyChanged {
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        private ObservableCollection<PartsPurchaseSettleDetail> partsPurchaseSettleDetails;
        private ObservableCollection<OutboundAndInboundBill> outboundAndInboundBills;
        private DataGridViewBase outboundAndInboundBillForEditDataGridView;
        private bool searchCanUse, sureCanUse;
        private DataGridViewBase partsPurchaseSettleDetailForEditDataGridView;
        private DateTime cutoffTime = DateTime.Now.Date;
        private ButtonItem saveBtn, returnBtn;
        private ICommand searchCommand, sureCommand;
        private DateTime? invoiceDate;

        private ICommand exportFileCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出待结算出入库清单模板.xlsx";
        private string strFileName;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private RadUpload uploader;
        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
                        if(partsPurchaseSettleBill == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.ImpOutboundAndInboundBillAsync(e.HandlerData.CustomData["Path"].ToString(), partsPurchaseSettleBill.PartsSalesCategoryId.Value, partsPurchaseSettleBill.PartsSupplierId);
                        this.excelServiceClient.ImpOutboundAndInboundBillCompleted -= ExcelServiceClient_ImpOutboundAndInboundBillCompleted;
                        this.excelServiceClient.ImpOutboundAndInboundBillCompleted += ExcelServiceClient_ImpOutboundAndInboundBillCompleted;
                    };
                }
                return this.uploader;
            }
        }

        /// <summary>
        /// 文件上传成功后，调用该方法进行上传后的处理。不支持异步调用
        /// <param>上传文件名 <name>fileName</name> </param>
        /// </summary>
        protected Action<string> UploadFileSuccessedProcessing;

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //取当前登录用户的HashCode来标记上传文件的唯一性
                //this.UploadFileSuccessedProcessing.Invoke(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }

        private void ExcelServiceClient_ImpOutboundAndInboundBillCompleted(object sender, ImpOutboundAndInboundBillCompletedEventArgs e) {
            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            if(partsPurchaseSettleBill == null)
                return;
            this.outboundAndInboundBills.Clear();
            if (e.rightData != null) {
                for (var i = 0; i < e.rightData.Length; i++) {
                    var data = e.rightData[i];
                    outboundAndInboundBills.Add(new OutboundAndInboundBill {
                        SerialNumber = i + 1,
                        BillId = data.Id,
                        BillType = data.Type,
                        BillCode = data.Code,
                        BillBusinessType = data.InboundType,
                        BillCreateTime = data.CreateTime,
                        SettlementAmount = data.Type == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单 ? -data.SettlementAmount : data.SettlementAmount,
                        CostAmount = data.CostAmount,
                        CompanyId = data.StorageCompanyId,
                        CounterpartCompanyId = data.CounterpartCompanyId,
                        WarehouseId = data.WarehouseId,
                        WarehouseName = data.WarehouseName,
                        ReturnReason = data.ReturnReason,
                        OriginalRequirementBillCode = data.OriginalRequirementBillCode
                    });
                }
            }
            SureCanUse = (this.OutboundAndInboundBills.Count > 0);
            partsPurchaseSettleBill.TotalCostAmount = this.OutboundAndInboundBills.Sum(item => item.CostAmount);
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowAlertMessage(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
        }

        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage(PartsPurchasingUIStrings.Upload_File_Tips);
                    e.Handled = true;
                }
            }
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }

        public ObservableCollection<OutboundAndInboundBill> OutboundAndInboundBills {
            get {
                return this.outboundAndInboundBills ?? (this.outboundAndInboundBills = new ObservableCollection<OutboundAndInboundBill>());

            }
        }

        private void OutboundAndInboundBillsCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e) {
            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            if(partsPurchaseSettleBill == null)
                return;
            SureCanUse = (this.OutboundAndInboundBills.Count > 0);
            partsPurchaseSettleBill.TotalSettlementAmount = OutboundAndInboundBills.Sum(item => item.SettlementAmount);
        }

        public ICommand DataCancleCommand {
            get {
                return this.CancelCommand;
            }
        }

        public ICommand SearchCommand {
            get {
                return this.searchCommand ?? (this.searchCommand = new Core.Command.DelegateCommand(this.Search));
            }
        }

        public ICommand SureCommand {
            get {
                return this.sureCommand ?? (this.sureCommand = new Core.Command.DelegateCommand(this.Sure));
            }
        }

        public ObservableCollection<PartsPurchaseSettleDetail> PartsPurchaseSettleDetails {
            get {
                return partsPurchaseSettleDetails ?? (this.partsPurchaseSettleDetails = new ObservableCollection<PartsPurchaseSettleDetail>());
            }
            set {
                partsPurchaseSettleDetails = value;
            }
        }

        public bool SearchCanUse {
            get {
                return this.searchCanUse;
            }
            set {
                this.searchCanUse = value;
                this.OnPropertyChanged("SearchCanUse");
            }
        }

        public bool SureCanUse {
            get {
                return this.sureCanUse;
            }
            set {
                this.sureCanUse = value;
                this.OnPropertyChanged("SureCanUse");
            }
        }
        //截止时间
        public DateTime CutoffTime {
            get {
                return this.cutoffTime;
            }
            set {
                this.cutoffTime = value;
                this.OnPropertyChanged("CutoffTime");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public PartsPurchaseSettleBillDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += this.PartsPurchaseSettleBillDataEditViewDataContextChanged;
            this.OutboundAndInboundBills.CollectionChanged += this.OutboundAndInboundBillsCollectionChanged;
        }

        private void PartsPurchaseSettleBillDataEditViewDataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            if(!(this.DataContext is PartsPurchaseSettleBill))
                return;
            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            partsPurchaseSettleBill.PropertyChanged -= this.PartsPurchaseSettleBillPropertyChanged;
            partsPurchaseSettleBill.PropertyChanged += this.PartsPurchaseSettleBillPropertyChanged;
        }
        //控制查询按钮启用
        private void PartsPurchaseSettleBillPropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            switch(e.PropertyName) {
                case "PartsSupplierCode":
                case "WarehouseId":
                case "CutoffTime":
                case "PartsSalesCategoryId":
                    if(partsPurchaseSettleBill != null && (partsPurchaseSettleBill.PartsSalesCategoryId != null && partsPurchaseSettleBill.PartsSupplierCode != "" && partsPurchaseSettleBill.WarehouseId != 0 && this.CutoffTime.ToShortDateString() != "")) {
                        SearchCanUse = true;
                    } else {
                        SearchCanUse = false;
                    }
                    //清单存在数据 清空
                    if(OutboundAndInboundBills.Any())
                        this.OutboundAndInboundBills.Clear();
                    break;
                case "TotalSettlementAmount":
                case "TaxRate":
                    if(partsPurchaseSettleBill != null && (partsPurchaseSettleBill.TaxRate < 0 || partsPurchaseSettleBill.TaxRate > 1)) {
                        partsPurchaseSettleBill.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseSettleBill_TaxRateBetZeroOne, new[] {
                            "TaxRate"
                        }));
                    } else if(partsPurchaseSettleBill != null)
                        partsPurchaseSettleBill.Tax = (partsPurchaseSettleBill.TotalSettlementAmount) * (decimal)partsPurchaseSettleBill.TaxRate;
                    break;

            }
        }
        //带结算出入库明细
        private DataGridViewBase OutboundAndInboundBillForEditDataGridView {
            get {
                if(this.outboundAndInboundBillForEditDataGridView == null) {
                    this.outboundAndInboundBillForEditDataGridView = DI.GetDataGridView("OutboundAndInboundBillForEdit");
                    this.outboundAndInboundBillForEditDataGridView.DomainContext = this.DomainContext;
                    this.outboundAndInboundBillForEditDataGridView.DataContext = this;
                }
                return this.outboundAndInboundBillForEditDataGridView;
            }
        }

        private DataGridViewBase PartsPurchaseSettleDetailForEditDataGridView {
            get {
                if(this.partsPurchaseSettleDetailForEditDataGridView == null) {
                    this.partsPurchaseSettleDetailForEditDataGridView = DI.GetDataGridView("PartsPurchaseSettleDetailForEdit");
                    this.partsPurchaseSettleDetailForEditDataGridView.DomainContext = this.DomainContext;
                    this.partsPurchaseSettleDetailForEditDataGridView.DataContext = this;
                }
                return this.partsPurchaseSettleDetailForEditDataGridView;
            }
        }

         private void ShowFileDialog() {
            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            if(partsPurchaseSettleBill == null)
                return;
            if(string.IsNullOrWhiteSpace(partsPurchaseSettleBill.PartsSupplierCode) || partsPurchaseSettleBill.PartsSupplierId == default(int)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrder_PartsSupplierCodeIsNull);
                return;
            }
            this.Uploader.ShowFileDialog();
        }

        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_BillCode,
                                                IsRequired = true
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }

        private void CreateUI() {
            //this.HideCancelButton();
            this.HideSaveButton();
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsPurchasingUIStrings.DataGridView_Title_OutboundAndInboundBill, null, this.OutboundAndInboundBillForEditDataGridView);//Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "PartsPurchaseOrderDetails")
            detailDataEditView.SetValue(Grid.RowProperty, 4);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsPurchasingUIStrings.Action_Title_Import,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                Command = new Core.Command.DelegateCommand(this.ShowFileDialog)
            });
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsPurchasingUIStrings.Upload_File_Export_Model,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                Command = this.ExportTemplateCommand
            });
            this.OutboundAndInboundBillData.Children.Add(detailDataEditView);
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsPurchasingUIStrings.Upload_File_Export_Model_NoPricce,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                Command = new Core.Command.DelegateCommand(this.ExportLiaison)
            });
            //detailDataEditView.RegisterButton(new ButtonItem {
            //    Title = "配件无分类编码清单导出",
            //    Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/export.png", UriKind.Relative),
            //    Command = new Core.Command.DelegateCommand(this.ExportNoGoldenTaxClassify)
            //});
            var detailDataEditView2 = new DcsDetailDataEditView();
            detailDataEditView2.Register(Utils.GetEntityLocalizedName(typeof(PartsPurchaseSettleBill), "PartsPurchaseSettleDetails"), null, this.PartsPurchaseSettleDetailForEditDataGridView);
            detailDataEditView2.SetValue(Grid.RowProperty, 3);
            detailDataEditView2.UnregisterButton(detailDataEditView2.InsertButton);
            detailDataEditView2.UnregisterButton(detailDataEditView2.DeleteButton);
            detailDataEditView2.RegisterButton(new ButtonItem {
                Title = PartsPurchasingUIStrings.Action_Title_Merge,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/merge.png", UriKind.Relative),
                Command = new Core.Command.DelegateCommand(this.MergerDetail)
            });
            detailDataEditView2.RegisterButton(new ButtonItem {
                Title = PartsPurchasingUIStrings.Action_Title_Export,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/export.png", UriKind.Relative),
                Command = new Core.Command.DelegateCommand(this.ExportPartsPurchaseSettleDetail)
            });
            this.PartsPurchaseSettleDetailData.Children.Add(detailDataEditView2);
            var queryWindowPartsSupplier = DI.GetQueryWindow("PartsSupplierForPartsPurchaseSettleBill");
            queryWindowPartsSupplier.SelectionDecided += this.QueryWindowPartsSupplierSelectionDecided;
            this.popupTextBoxPartsSupplierCode.PopupContent = queryWindowPartsSupplier;
            this.DomainContext.Load(this.DomainContext.GetWarehousesOrderByNameQuery().Where(entity => entity.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Entities == null)
                    return;
                foreach(var warehouse in loadOp.Entities)
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name,
                        UserObject = warehouse
                    });
            }, null);
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(e => e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                if(loadOp.HasError)
                    return;
                this.DcsComboBoxPartsSalesCategoryName.ItemsSource = loadOp.Entities;
                this.DcsComboBoxPartsSalesCategoryName.SelectedIndex = 0;//默认品牌
            }, null);
            this.Second.Visibility = Visibility.Collapsed;
            this.First.Visibility = Visibility.Visible;
            this.saveBtn = new ButtonItem {
                Command = new Core.Command.DelegateCommand(this.SaveCurrentData),
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/save.png", UriKind.Relative),
                Title = PartsPurchasingUIStrings.Action_Title_Save
            };
            this.returnBtn = new ButtonItem {
                Command = new Core.Command.DelegateCommand(this.ReturnCurrentData),
                Title = PartsPurchasingUIStrings.Action_Title_Return,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/return.png", UriKind.Relative)
            };
        }

        private void ExportLiaison() {
            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            if(partsPurchaseSettleBill == null)
                return;
            if(this.OutboundAndInboundBills == null || this.outboundAndInboundBills.Count() == default(int)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_OutboundAndInboundBillsIsNull);
                return;
            }
            var inboundIds = new List<int>();
            var outboundIds = new List<int>();
            foreach(var item in OutboundAndInboundBills) {
                if(item.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件入库检验单)
                    inboundIds.Add(item.BillId);
                if(item.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单)
                    outboundIds.Add(item.BillId);
            }
            this.DomainContext.Load(this.DomainContext.GetSalesCenterstrategiesQuery().Where(r => r.PartsSalesCategoryId == partsPurchaseSettleBill.PartsSalesCategoryId), LoadBehavior.RefreshCurrent, loadBranch => {
                if(loadBranch.HasError) {
                    loadBranch.MarkErrorAsHandled();
                    return;
                }
                if(loadBranch.Entities == null)
                    return;
                var branchStrategies = loadBranch.Entities.First();
                if(branchStrategies.PartsPurchasePricingStrategy == (int)DcsBranchstrategyPartsPurchasePricingStrategy.先定价)
                    this.DomainContext.Load(this.DomainContext.查询待结算出入库明细清单Query(inboundIds.ToArray(), outboundIds.ToArray(), null), loadOp => {
                        if(loadOp.HasError)
                            return;
                        if(loadOp.Entities == null)
                            return;
                        if(!loadOp.Entities.Any(e => e.SettlementPrice == 0)) {
                            UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_SettlementPriceIsNull);
                            return;
                        }
                        this.DomainContext.ExportPurchaseSettlementOutboundAndInboundBillDetailForWith(partsPurchaseSettleBill.PartsSalesCategoryId.Value, partsPurchaseSettleBill.PartsSupplierId, branchStrategies.PartsPurchasePricingStrategy, inboundIds.ToArray(), outboundIds.ToArray(), loadOp1 => {
                            if(loadOp1.HasError)
                                return;
                            if(loadOp1.Value == null || string.IsNullOrEmpty(loadOp1.Value)) {
                                UIHelper.ShowNotification(loadOp1.Value);
                            }
                            if(loadOp1.Value != null && !string.IsNullOrEmpty(loadOp1.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp1.Value));
                            }
                        }, null);
                    }, null);
                else
                    this.DomainContext.Load(this.DomainContext.查询待结算出入库明细清单后定价Query(inboundIds.ToArray(), outboundIds.ToArray(), partsPurchaseSettleBill.PartsSalesCategoryId.Value, partsPurchaseSettleBill.PartsSupplierId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            loadOp.MarkErrorAsHandled();
                            return;
                        }
                        if(loadOp.Entities == null)
                            return;
                        if(!loadOp.Entities.Any(e => e.SettlementPrice == 0)) {
                            UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_SettlementPriceIsNull);
                            return;
                        }
                        this.DomainContext.ExportPurchaseSettlementOutboundAndInboundBillDetailForWith(partsPurchaseSettleBill.PartsSalesCategoryId.Value, partsPurchaseSettleBill.PartsSupplierId, branchStrategies.PartsPurchasePricingStrategy, inboundIds.ToArray(), outboundIds.ToArray(), loadOp1 => {
                            if(loadOp1.HasError)
                                return;
                            if(loadOp1.Value == null || string.IsNullOrEmpty(loadOp1.Value)) {
                                UIHelper.ShowNotification(loadOp1.Value);
                            }
                            if(loadOp1.Value != null && !string.IsNullOrEmpty(loadOp1.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp1.Value));
                            }
                        }, null);
                    }, null);
            }, null);
        }

        //private void ExportNoGoldenTaxClassify() {
        //    var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
        //    if(partsPurchaseSettleBill == null)
        //        return;
        //    if(this.OutboundAndInboundBills == null || this.outboundAndInboundBills.Count() == default(int)) {
        //        UIHelper.ShowNotification("待结算出入库单清单不能为空！");
        //        return;
        //    }
        //    var inboundIds = new List<int>();
        //    var outboundIds = new List<int>();
        //    foreach(var item in OutboundAndInboundBills) {
        //        if(item.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件入库检验单)
        //            inboundIds.Add(item.BillId);
        //        if(item.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单)
        //            outboundIds.Add(item.BillId);
        //    }
        //    this.DomainContext.Load(this.DomainContext.GetSalesCenterstrategiesQuery().Where(r => r.PartsSalesCategoryId == partsPurchaseSettleBill.PartsSalesCategoryId), LoadBehavior.RefreshCurrent, loadBranch => {
        //        if(loadBranch.HasError) {
        //            loadBranch.MarkErrorAsHandled();
        //            return;
        //        }
        //        if(loadBranch.Entities == null)
        //            return;
        //        var branchStrategies = loadBranch.Entities.First();
        //        if(branchStrategies.PartsPurchasePricingStrategy == (int)DcsBranchstrategyPartsPurchasePricingStrategy.先定价)
        //            this.DomainContext.Load(this.DomainContext.查询待结算出入库明细清单Query(inboundIds.ToArray(), outboundIds.ToArray(), null), loadOp => {
        //                if(loadOp.HasError)
        //                    return;
        //                if(loadOp.Entities == null)
        //                    return;
        //                this.DomainContext.ExportPurchaseSettlementOutboundAndInboundBillDetailForWithGoldenTaxClassifyCode(partsPurchaseSettleBill.PartsSalesCategoryId.Value, partsPurchaseSettleBill.PartsSupplierId, branchStrategies.PartsPurchasePricingStrategy, inboundIds.ToArray(), outboundIds.ToArray(), loadOp1 => {
        //                    if(loadOp1.HasError)
        //                        return;
        //                    if(loadOp1.Value == null || string.IsNullOrEmpty(loadOp1.Value)) {
        //                        UIHelper.ShowNotification(loadOp1.Value);
        //                    }
        //                    if(loadOp1.Value != null && !string.IsNullOrEmpty(loadOp1.Value)) {
        //                        HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp1.Value));
        //                    }
        //                }, null);
        //            }, null);
        //        else
        //            this.DomainContext.Load(this.DomainContext.查询待结算出入库明细清单后定价Query(inboundIds.ToArray(), outboundIds.ToArray(), partsPurchaseSettleBill.PartsSalesCategoryId.Value, partsPurchaseSettleBill.PartsSupplierId), LoadBehavior.RefreshCurrent, loadOp => {
        //                if(loadOp.HasError) {
        //                    loadOp.MarkErrorAsHandled();
        //                    return;
        //                }
        //                if(loadOp.Entities == null)
        //                    return;
        //                this.DomainContext.ExportPurchaseSettlementOutboundAndInboundBillDetailForWithGoldenTaxClassifyCode(partsPurchaseSettleBill.PartsSalesCategoryId.Value, partsPurchaseSettleBill.PartsSupplierId, branchStrategies.PartsPurchasePricingStrategy, inboundIds.ToArray(), outboundIds.ToArray(), loadOp1 => {
        //                    if(loadOp1.HasError)
        //                        return;
        //                    if(loadOp1.Value == null || string.IsNullOrEmpty(loadOp1.Value)) {
        //                        UIHelper.ShowNotification(loadOp1.Value);
        //                    }
        //                    if(loadOp1.Value != null && !string.IsNullOrEmpty(loadOp1.Value)) {
        //                        HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp1.Value));
        //                    }
        //                }, null);
        //            }, null);
        //    }, null);
        //}

        private void ExportPartsPurchaseSettleDetail() {
            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            if(partsPurchaseSettleBill == null)
                return;
            var inboundIds = new List<int>();
            var outboundIds = new List<int>();
            foreach(var item in OutboundAndInboundBills) {
                if(item.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件入库检验单)
                    inboundIds.Add(item.BillId);
                if(item.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单)
                    outboundIds.Add(item.BillId);
            }
            this.DomainContext.Load(this.DomainContext.GetSalesCenterstrategiesQuery().Where(r => r.PartsSalesCategoryId == partsPurchaseSettleBill.PartsSalesCategoryId), LoadBehavior.RefreshCurrent, loadBranch => {
                if(loadBranch.HasError) {
                    loadBranch.MarkErrorAsHandled();
                    return;
                }
                if(loadBranch.Entities == null)
                    return;
                var branchStrategies = loadBranch.Entities.First();
                if(branchStrategies.PartsPurchasePricingStrategy == (int)DcsBranchstrategyPartsPurchasePricingStrategy.先定价)
                    this.DomainContext.ExportPartsSalesSettlementBill(inboundIds.ToArray(), outboundIds.ToArray(), null, null, PartsPurchasingUIStrings.DataEditPanel_GroupTitle_PartsPurchaseSettleDetail, loadOp => {
                        if(loadOp.HasError)
                            return;
                        if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                            UIHelper.ShowNotification(loadOp.Value);
                        }
                        if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                    }, null);
                else
                    this.DomainContext.ExportPartsSalesSettlementBill(inboundIds.ToArray(), outboundIds.ToArray(), partsPurchaseSettleBill.PartsSalesCategoryId, partsPurchaseSettleBill.PartsSupplierId, PartsPurchasingUIStrings.DataEditPanel_GroupTitle_PartsPurchaseSettleDetail, loadOp1 => {
                        if(loadOp1.HasError)
                            return;
                        if(loadOp1.Value == null || string.IsNullOrEmpty(loadOp1.Value)) {
                            UIHelper.ShowNotification(loadOp1.Value);
                        }
                        if(loadOp1.Value != null && !string.IsNullOrEmpty(loadOp1.Value))
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp1.Value));
                    }, null);
            }, null);
        }
        //确定按钮事件
        private void Sure() {
            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            if(partsPurchaseSettleBill == null)
                return;

            //////
            foreach(var partsPurchaseSettleDetail in partsPurchaseSettleBill.PartsPurchaseSettleDetails) {
                if(partsPurchaseSettleBill.PartsPurchaseSettleDetails.Contains(partsPurchaseSettleDetail))
                    partsPurchaseSettleBill.PartsPurchaseSettleDetails.Remove(partsPurchaseSettleDetail);
            }

            if(partsPurchaseSettleBill.TaxRate == 0) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseSettleBill_TaxRateIsNotNull);
                return;
            }

            var inboundIds = new List<int>();
            var outboundIds = new List<int>();
            foreach(var item in OutboundAndInboundBills) {
                if(item.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件入库检验单)
                    inboundIds.Add(item.BillId);
                if(item.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单)
                    outboundIds.Add(item.BillId);
            }

            this.DomainContext.Load(this.DomainContext.GetSalesCenterstrategiesQuery().Where(r => r.PartsSalesCategoryId == partsPurchaseSettleBill.PartsSalesCategoryId), LoadBehavior.RefreshCurrent, loadBranch => {
                if(loadBranch.HasError) {
                    loadBranch.MarkErrorAsHandled();
                    return;
                }
                if(loadBranch.Entities == null)
                    return;
                var branchStrategies = loadBranch.Entities.First();
                if(branchStrategies.PartsPurchasePricingStrategy == (int)DcsBranchstrategyPartsPurchasePricingStrategy.先定价 || (partsPurchaseSettleBill.PartsSalesCategoryName == "随车行" && partsPurchaseSettleBill.PartsSupplierCode == "FT001078"))
                    this.DomainContext.Load(this.DomainContext.查询待结算出入库明细清单Query(inboundIds.ToArray(), outboundIds.ToArray(), null), loadOp => {
                        if(loadOp.HasError)
                            return;
                        if(loadOp.Entities == null)
                            return;

                        this.First.Visibility = Visibility.Collapsed;
                        this.Second.Visibility = Visibility.Visible;
                        this.RegisterButton(saveBtn, true);
                        this.RegisterButton(returnBtn, true);

                        int temp = 1;
                        this.PartsPurchaseSettleDetails.Clear();
                        var totalCostAmout = loadOp.Entities.Sum(r => r.CostPrice * (r.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单 ? -r.Quantity : r.Quantity));
                        foreach(var item in loadOp.Entities) {
                            this.PartsPurchaseSettleDetails.Add(new PartsPurchaseSettleDetail {
                                SerialNumber = temp++,
                                SparePartId = item.SparePartId,
                                SparePartCode = item.SparePartCode,
                                SparePartName = item.SparePartName,
                                QuantityToSettle = item.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单 ? -item.Quantity : item.Quantity,
                                BillType = item.BillType,
                                SettlementPrice = item.SettlementPrice,
                                SettlementAmount = item.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单 ? -item.SettlementAmount : item.SettlementAmount
                            });
                        }
                        partsPurchaseSettleBill.TotalSettlementAmount = this.PartsPurchaseSettleDetails.Sum(r => r.SettlementAmount);
                        partsPurchaseSettleBill.TotalCostAmount = totalCostAmout;
                        //this.PartsPurchaseSettleDetails = new ObservableCollection<PartsPurchaseSettleDetail>(PartsPurchaseSettleDetails.OrderBy(e => e.QuantityToSettle).ThenBy(e => e.SettlementAmount));
                        //this.OnPropertyChanged("PartsPurchaseSettleDetails");
                    }, null);
                else
                    this.DomainContext.Load(this.DomainContext.查询待结算出入库明细清单后定价Query(inboundIds.ToArray(), outboundIds.ToArray(), partsPurchaseSettleBill.PartsSalesCategoryId.Value, partsPurchaseSettleBill.PartsSupplierId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            loadOp.MarkErrorAsHandled();
                            return;
                        }
                        if(loadOp.Entities == null)
                            return;

                        this.First.Visibility = Visibility.Collapsed;
                        this.Second.Visibility = Visibility.Visible;
                        this.RegisterButton(saveBtn, true);
                        this.RegisterButton(returnBtn, true);
                        int temp = 1;
                        this.PartsPurchaseSettleDetails.Clear();
                        var sumTotalCostAmout = loadOp.Entities.Sum(r => r.CostPrice * (r.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单 ? -r.Quantity : r.Quantity));
                        foreach(var item in loadOp.Entities) {

                            this.PartsPurchaseSettleDetails.Add(new PartsPurchaseSettleDetail {
                                SerialNumber = temp++,
                                SparePartId = item.SparePartId,
                                SparePartCode = item.SparePartCode,
                                SparePartName = item.SparePartName,
                                BillType = item.BillType,
                                QuantityToSettle = item.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单 ? -item.Quantity : item.Quantity,
                                SettlementPrice = item.SettlementPrice,
                                SettlementAmount = item.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单 ? -item.SettlementAmount : item.SettlementAmount
                            });
                        }
                        partsPurchaseSettleBill.TotalSettlementAmount = this.PartsPurchaseSettleDetails.Sum(r => r.SettlementAmount);
                        partsPurchaseSettleBill.TotalCostAmount = sumTotalCostAmout;
                        //this.PartsPurchaseSettleDetails = new ObservableCollection<PartsPurchaseSettleDetail>(PartsPurchaseSettleDetails.OrderBy(e => e.QuantityToSettle).ThenBy(e => e.SettlementAmount));
                        //this.OnPropertyChanged("PartsPurchaseSettleDetails");
                        if(this.PartsPurchaseSettleDetails.Any(e => e.SettlementPrice == 0))
                            UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_SettlementPriceIsZero);
                    }, null);
            }, null);
            //this.生成运单清单和积分清单(inboundIds, outboundIds, partsPurchaseSettleBill);
        }

        //public void 生成运单清单和积分清单(List<int> inboundIds, List<int> outboundIds, PartsPurchaseSettleBill partsPurchaseSettleBill) {
        //    //判断虚拟积分配件和虚拟运费配件
        //    this.DomainContext.Load(this.DomainContext.GetSparePartsByCodeQuery(new string[] { "XNJF", "XNYF" }), LoadBehavior.RefreshCurrent, loadOp => {
        //        if(loadOp.HasError)
        //            return;
        //        if(loadOp.Entities.Count() != 2) {
        //            UIHelper.ShowNotification("虚拟积分配件或虚拟运费配件不存在,请确认");
        //            return;
        //        } else {
        //            this.DomainContext.Load(this.DomainContext.GetBonusPointsOrderByPartsInboundBillIdsQuery(inboundIds.ToArray()), LoadBehavior.RefreshCurrent, loadOp1 => {
        //                if(loadOp1.HasError)
        //                    return;
        //                this.DomainContext.Load(this.DomainContext.GetBonusPointsOrderByPartsOutboundBillIdsQuery(outboundIds.ToArray()), LoadBehavior.RefreshCurrent, loadOp2 => {
        //                    if(loadOp2.HasError)
        //                        return;
        //                    if(loadOp1.Entities.Any() || loadOp2.Entities.Any()) {
        //                        var partsPurchaseSettleDetail = new PartsPurchaseSettleDetail();
        //                        var XNJFSparePart = loadOp.Entities.FirstOrDefault(r => r.Code == "XNJF");
        //                        if(XNJFSparePart == null) {
        //                            UIHelper.ShowNotification("虚拟积分配件不存在,请确认");
        //                            return;
        //                        } else {
        //                            partsPurchaseSettleDetail.SparePartId = XNJFSparePart.Id;
        //                            partsPurchaseSettleDetail.SparePartCode = XNJFSparePart.Code;
        //                            partsPurchaseSettleDetail.SparePartName = XNJFSparePart.Name;
        //                            partsPurchaseSettleDetail.SerialNumber = this.PartsPurchaseSettleDetails.Count() + 1;
        //                            partsPurchaseSettleDetail.SettlementPrice = loadOp1.Entities.Sum(r => r.BonusPointsAmount ?? 0) + loadOp2.Entities.Sum(r => r.BonusPointsAmount ?? 0);
        //                            partsPurchaseSettleDetail.SettlementAmount = loadOp1.Entities.Sum(r => r.BonusPointsAmount ?? 0) + loadOp2.Entities.Sum(r => r.BonusPointsAmount ?? 0);
        //                            partsPurchaseSettleDetail.QuantityToSettle = 1;
        //                            this.PartsPurchaseSettleDetails.Add(partsPurchaseSettleDetail);
        //                            partsPurchaseSettleBill.TotalSettlementAmount = this.PartsPurchaseSettleDetails.Sum(r => r.SettlementAmount);
        //                        }
        //                    }
        //                }, null);
        //            }, null);

        //            this.DomainContext.Load(this.DomainContext.GetEcommerceFreightDisposalByPartsInboundBillIdsQuery(inboundIds.ToArray(), outboundIds.ToArray()), LoadBehavior.RefreshCurrent, loadOp1 => {
        //                if(loadOp1.HasError)
        //                    return;

        //                this.DomainContext.Load(this.DomainContext.GetReturnFreightDisposalsByPartsOutboundBillIdssQuery(inboundIds.ToArray(), outboundIds.ToArray()), LoadBehavior.RefreshCurrent, loadOp2 => {
        //                    if(loadOp2.HasError)
        //                        return;
        //                    if(loadOp1.Entities.Any() || loadOp2.Entities.Any()) {
        //                        var partsPurchaseSettleDetail = new PartsPurchaseSettleDetail();
        //                        var XNJFSparePart = loadOp.Entities.FirstOrDefault(r => r.Code == "XNYF");
        //                        if(XNJFSparePart == null) {
        //                            UIHelper.ShowNotification("虚拟运费配件不存在,请确认");
        //                            return;
        //                        } else {
        //                            partsPurchaseSettleDetail.SparePartId = XNJFSparePart.Id;
        //                            partsPurchaseSettleDetail.SparePartCode = XNJFSparePart.Code;
        //                            partsPurchaseSettleDetail.SparePartName = XNJFSparePart.Name;
        //                            partsPurchaseSettleDetail.SerialNumber = this.PartsPurchaseSettleDetails.Count() + 1;
        //                            partsPurchaseSettleDetail.SettlementPrice = loadOp1.Entities.Sum(r => r.FreightCharges ?? 0) + loadOp2.Entities.Sum(r => r.FreightCharges ?? 0);
        //                            partsPurchaseSettleDetail.SettlementAmount = loadOp1.Entities.Sum(r => r.FreightCharges ?? 0) + loadOp2.Entities.Sum(r => r.FreightCharges ?? 0);
        //                            partsPurchaseSettleDetail.QuantityToSettle = 1;
        //                            this.PartsPurchaseSettleDetails.Add(partsPurchaseSettleDetail);
        //                            partsPurchaseSettleBill.TotalSettlementAmount = this.PartsPurchaseSettleDetails.Sum(r => r.SettlementAmount);
        //                        }
        //                    }
        //                }, null);
        //            }, null);
        //        }
        //    }, null);
        //}

        //保存事件
        private void SaveCurrentData() {
            if(!this.OutboundAndInboundBillForEditDataGridView.CommitEdit() || !this.PartsPurchaseSettleDetailForEditDataGridView.CommitEdit())
                return;
            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            if(partsPurchaseSettleBill == null)
                return;
            if(PartsPurchaseSettleDetails == null || !PartsPurchaseSettleDetails.Any()) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseSettleDetailIsNone);
                return;
            }
            if(this.PartsPurchaseSettleDetails.Any(e => e.SettlementPrice == 0)) {
                if(!(partsPurchaseSettleBill.PartsSalesCategoryName == "随车行" && partsPurchaseSettleBill.PartsSupplierCode == "FT001078")) {
                    UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_SettlementPriceIsZero);
                    return;
                }
            }
            if(this.PartsPurchaseSettleDetails.Any(e => e.QuantityToSettle < 0)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_DetailsLessThanZero);
                return;
            }
            partsPurchaseSettleBill.ValidationErrors.Clear();
            if(partsPurchaseSettleBill.TotalSettlementAmount < 0)
                partsPurchaseSettleBill.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseSettleBill_TotalSettlementAmountGreaterZero, new[] {
                    "TotalSettlementAmount"
                }));
            //校验采购结算单的总金额是否大于分公司策略的最大开票金额
            //(issue4728)取消校验
            //this.DomainContext.Load(this.DomainContext.GetBranchstrategiesQuery().Where(r => r.BranchId == partsPurchaseSettleBill.BranchId), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    var branchstrategy = loadOp.Entities.SingleOrDefault();
            //    if(branchstrategy != null && partsPurchaseSettleBill.TotalSettlementAmount > branchstrategy.MaxInvoiceAmount) {
            //        UIHelper.ShowAlertMessage("结算总金额不能超过最大开票金额");
            //        return;
            //    }
            //}, null);
            if(partsPurchaseSettleBill.HasValidationErrors)
                return;
            foreach(var item in PartsPurchaseSettleDetails) {
                partsPurchaseSettleBill.PartsPurchaseSettleDetails.Add(item);
            }
            //校验取消（issue5192）
            //if(partsPurchaseSettleBill.PartsPurchaseSettleDetails.Count() > 200) {
            //    UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_DetailCountMoreThan200);
            //    return;
            //}
            if (partsPurchaseSettleBill.PartsPurchaseSettleRefs.Count()>0) {
                foreach (var item in partsPurchaseSettleBill.PartsPurchaseSettleRefs)
                {
                    partsPurchaseSettleBill.PartsPurchaseSettleRefs.Remove(item);
              }
            }
            foreach(var item in OutboundAndInboundBills) {
                partsPurchaseSettleBill.PartsPurchaseSettleRefs.Add(new PartsPurchaseSettleRef {
                    SerialNumber = item.SerialNumber,
                    SourceId = item.BillId,
                    SourceCode = item.BillCode,
                    SourceType = (int)(DcsPartsPurchaseSettleRefSourceType)Enum.Parse(typeof(DcsPartsPurchaseSettleRefSourceType), Enum.GetName(typeof(DcsPartsLogisticBatchBillDetailBillType), item.BillType), true),//TODO 键值对需要转换 不能直接赋值
                    SettlementAmount = item.SettlementAmount,
                    SourceBillCreateTime = item.BillCreateTime,
                    WarehouseId = item.WarehouseId,
                    WarehouseName = item.WarehouseName
                });
            }
            if (this.InvoiceDate.HasValue)
            {
                this.InvoiceDate = new DateTime(this.InvoiceDate.Value.Year, this.InvoiceDate.Value.Month, this.InvoiceDate.Value.Day, 00, 00, 00);
                partsPurchaseSettleBill.InvoiceDate = this.InvoiceDate.Value;
            } else if(!partsPurchaseSettleBill.InvoiceDate.HasValue)
            {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceDateIsNull);
                return;
            }
            partsPurchaseSettleBill.TotalSettlementAmount = this.PartsPurchaseSettleDetails.Sum(r => r.SettlementAmount);
            ((IEditableObject)partsPurchaseSettleBill).EndEdit();
            try {
                if(this.EditState == DataEditState.New) {
                    if(partsPurchaseSettleBill.Can生成配件采购结算单)
                        partsPurchaseSettleBill.生成配件采购结算单();
                } else {
                    if(partsPurchaseSettleBill.Can修改配件采购结算单)
                        partsPurchaseSettleBill.修改配件采购结算单();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        //返回
        private void ReturnCurrentData() {
            this.UnregisterButton(returnBtn);
            this.UnregisterButton(saveBtn);
            this.Second.Visibility = Visibility.Collapsed;
            this.First.Visibility = Visibility.Visible;
        }

        public DateTime? InvoiceDate
        {
            get
            {
                return this.invoiceDate;
            }
            set
            {
                this.invoiceDate = value;
                this.OnPropertyChanged("InvoiceDate");
            }
        }
        //清单合并事件
        private void MergerDetail() {
            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            if(partsPurchaseSettleBill == null)
                return;
            var mergeDetail = from detail in PartsPurchaseSettleDetails
                              group detail by new {
                                  detail.SparePartId,
                                  detail.SettlementPrice,
                                  detail.BillType
                              } into g
                              select new {
                                  g.Key.SparePartId,
                                  g.Key.SettlementPrice,
                                  g.Key.BillType
                              };
            var mergeCollection = new ObservableCollection<PartsPurchaseSettleDetail>();
            var serialNumber = 1;
            foreach(var detailItem in mergeDetail) {
                var item = detailItem;
                var details = this.PartsPurchaseSettleDetails.Where(e => e.SparePartId == item.SparePartId && e.SettlementPrice == item.SettlementPrice && item.BillType == e.BillType);
                var purchaseSettleDetails = details as PartsPurchaseSettleDetail[] ?? details.ToArray();
                if(purchaseSettleDetails.Any())
                    mergeCollection.Add(new PartsPurchaseSettleDetail {
                        SerialNumber = serialNumber++,
                        SparePartId = purchaseSettleDetails.First().SparePartId,
                        SparePartCode = purchaseSettleDetails.First().SparePartCode,
                        SparePartName = purchaseSettleDetails.First().SparePartName,
                        SettlementPrice = purchaseSettleDetails.First().SettlementPrice,
                        QuantityToSettle = purchaseSettleDetails.Sum(e => e.QuantityToSettle),
                        SettlementAmount = purchaseSettleDetails.Sum(e => e.QuantityToSettle) * purchaseSettleDetails.First().SettlementPrice
                    });
            }
            partsPurchaseSettleBill.TotalSettlementAmount = mergeCollection.Sum(e => e.SettlementAmount);
            this.PartsPurchaseSettleDetails = new ObservableCollection<PartsPurchaseSettleDetail>(mergeCollection.OrderBy(e => e.SerialNumber).ThenBy(e => e.SettlementAmount));
            this.OnPropertyChanged("PartsPurchaseSettleDetails");

        }

        //查询按钮事件
        private void Search() {
            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            if(partsPurchaseSettleBill == null)
                return;
            int[] inboundTypes = { (int)DcsPartsInboundType.配件采购 };
            int[] outboundTypes = { (int)DcsPartsOutboundType.采购退货 };
            var invoiceNumber = this.InvoiceNumber.Text;
            this.DomainContext.Load(this.DomainContext.查询待采购结算出入库明细过滤原始入库单编号Query(BaseApp.Current.CurrentUserData.EnterpriseId, partsPurchaseSettleBill.PartsSupplierId, partsPurchaseSettleBill.WarehouseId, null, partsPurchaseSettleBill.PartsSalesCategoryId, inboundTypes, outboundTypes, CutoffTime = new DateTime(CutoffTime.Year, CutoffTime.Month, CutoffTime.Day, 23, 59, 59), invoiceNumber), loadOp => {//this.CutoffTime
                if(loadOp.HasError || loadOp.Entities == null)
                    return;
                var OutboundAndInbound = this.OutboundAndInboundBills.Where(e => e.EntityType == "ReadOnly").ToList();
                this.OutboundAndInboundBills.Clear();
                foreach(var inbound in OutboundAndInbound) {
                    inbound.SerialNumber = this.OutboundAndInboundBills.Count + 1;
                    this.OutboundAndInboundBills.Add(inbound);
                }
                foreach(var item in loadOp.Entities) {
                    if(this.DomainContext.OutboundAndInboundBills.Contains(item))
                        this.DomainContext.OutboundAndInboundBills.Detach(item);
                    item.SerialNumber = this.OutboundAndInboundBills.Count + 1;
                    item.EntityType = "Add";//标记数据状态为新增的
                    if(item.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单)
                        item.SettlementAmount = -item.SettlementAmount;
                    this.OutboundAndInboundBills.Add(item);
                }
                SureCanUse = (this.OutboundAndInboundBills.Count > 0);
                partsPurchaseSettleBill.TotalCostAmount = this.OutboundAndInboundBills.Sum(item => item.CostAmount);
            }, null);
        }

        private void QueryWindowPartsSupplierSelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            this.OutboundAndInboundBills.Clear();
            var partsSupplier = queryWindow.SelectedEntities.Cast<VirtualPartsSupplier>().FirstOrDefault();
            if(partsSupplier == null)
                return;
            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            if(partsPurchaseSettleBill == null)
                return;
            partsPurchaseSettleBill.PartsSupplierId = partsSupplier.Id;
            partsPurchaseSettleBill.PartsSupplierName = partsSupplier.Name;
            partsPurchaseSettleBill.PartsSupplierCode = partsSupplier.Code;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }
        protected override bool OnRequestCanSubmit()
        {
            this.DcsComboBoxPartsSalesCategoryName.SelectedIndex = 0;//默认品牌
            return true;
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsPurchaseSettleBill;
            }
        }

        public ObservableCollection<KeyValuePair> KvWarehouses {
            get {
                return this.kvWarehouses;
            }
        }

        protected override void OnEditCancelled() {
            this.Second.Visibility = Visibility.Collapsed;
            this.First.Visibility = Visibility.Visible;
            this.OutboundAndInboundBills.Clear();
            PartsPurchaseSettleDetails.Clear();

            this.UnregisterButton(returnBtn);
            this.UnregisterButton(saveBtn);
            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            if(partsPurchaseSettleBill == null)
                return;
            if(this.DomainContext.PartsPurchaseSettleBills.Contains(partsPurchaseSettleBill))
                this.DomainContext.PartsPurchaseSettleBills.Detach(partsPurchaseSettleBill);
            base.OnEditCancelled();
        }

        protected override void OnEditSubmitted() {
            this.Second.Visibility = Visibility.Collapsed;
            this.First.Visibility = Visibility.Visible;
            this.OutboundAndInboundBills.Clear();
            PartsPurchaseSettleDetails.Clear();
            this.UnregisterButton(returnBtn);
            this.UnregisterButton(saveBtn);
            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            if(partsPurchaseSettleBill == null)
                return;
            if(this.DomainContext.PartsPurchaseSettleBills.Contains(partsPurchaseSettleBill))
                this.DomainContext.PartsPurchaseSettleBills.Detach(partsPurchaseSettleBill);
            base.OnEditSubmitted();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsPurchaseSettleBillsWithDetailsAndRefsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.OutboundAndInboundBills.Clear();
                    this.PartsPurchaseSettleDetails.Clear();
                    foreach(var item in entity.PartsPurchaseSettleRefs) {
                        this.OutboundAndInboundBills.Add(new OutboundAndInboundBill {
                            SerialNumber = item.SerialNumber,
                            BillId = item.SourceId,
                            EntityType = "ReadOnly",
                            BillCode = item.SourceCode,
                            BillType = (int)(DcsPartsLogisticBatchBillDetailBillType)Enum.Parse(typeof(DcsPartsLogisticBatchBillDetailBillType), Enum.GetName(typeof(DcsPartsPurchaseSettleRefSourceType), item.SourceType), true),//TODO 两个键值对不一致 需转换
                            SettlementAmount = item.SettlementAmount,
                            BillCreateTime = item.SourceBillCreateTime,
                            WarehouseName = item.WarehouseName,
                            WarehouseId = item.WarehouseId,
                            ReturnReason = item.ReturnReason
                        });
                        entity.PartsPurchaseSettleRefs.Remove(item);
                    }
                    //foreach(var item in entity.PartsPurchaseSettleDetails) {
                    //    this.PartsPurchaseSettleDetails.Add(item);
                    //    entity.PartsPurchaseSettleDetails.Remove(item);
                    //}
                    this.SureCanUse = true;
                    this.SearchCanUse = true;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        private void WarehouseDcsComboBoxSelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            if(e.AddedItems == null || e.AddedItems.Count != 1)
                return;
            this.OutboundAndInboundBills.Clear();
            var temp = sender as RadComboBox;
            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            if(temp != null && partsPurchaseSettleBill != null) {
                var warehouse = temp.SelectedItem as KeyValuePair;
                if(warehouse != null)
                    partsPurchaseSettleBill.WarehouseCode = ((Warehouse)warehouse.UserObject).Code;
            }
        }
    }
}
