﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class PartsPurchaseSettleBillForSupplierDataEditView : INotifyPropertyChanged {
        private bool searchCanUse, sureCanUse;
        private ButtonItem saveBtn, returnBtn;
        private ICommand searchCommand, sureCommand;
        private DateTime cutoffTime = DateTime.Now.Date;
        public event PropertyChangedEventHandler PropertyChanged;
        private DataGridViewBase partsPurchaseSettleDetailForSupplierForEditDataGridView;
        private DataGridViewBase outboundAndInboundBillForSupplierForEditDataGridView;
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        private ObservableCollection<KeyValuePair> kvPartsSalesCategories = new ObservableCollection<KeyValuePair>();
        private ObservableCollection<OutboundAndInboundBill> outboundAndInboundBills = new ObservableCollection<OutboundAndInboundBill>();
        private ObservableCollection<PartsPurchaseSettleDetail> partsPurchaseSettleDetails = new ObservableCollection<PartsPurchaseSettleDetail>();

        public PartsPurchaseSettleBillForSupplierDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += this.PartsPurchaseSettleBillDataEditViewDataContextChanged;
            this.OutboundAndInboundBills.CollectionChanged += this.OutboundAndInboundBillsCollectionChanged;

        }

        private DataGridViewBase OutboundAndInboundBillForSupplierForEditDataGridView {
            get {
                if(this.outboundAndInboundBillForSupplierForEditDataGridView == null) {
                    this.outboundAndInboundBillForSupplierForEditDataGridView = DI.GetDataGridView("OutboundAndInboundBillForSupplierEdit");
                    this.outboundAndInboundBillForSupplierForEditDataGridView.DomainContext = this.DomainContext;
                    this.outboundAndInboundBillForSupplierForEditDataGridView.DataContext = this;
                }
                return this.outboundAndInboundBillForSupplierForEditDataGridView;
            }
        }

        private DataGridViewBase PartsPurchaseSettleDetailForSupplierForEditDataGridView {
            get {
                if(this.partsPurchaseSettleDetailForSupplierForEditDataGridView == null) {
                    this.partsPurchaseSettleDetailForSupplierForEditDataGridView = DI.GetDataGridView("PartsPurchaseSettleDetailForSupplierForEdit");
                    this.partsPurchaseSettleDetailForSupplierForEditDataGridView.DomainContext = this.DomainContext;
                    this.partsPurchaseSettleDetailForSupplierForEditDataGridView.DataContext = this;
                }
                return this.partsPurchaseSettleDetailForSupplierForEditDataGridView;
            }
        }

        public ICommand DataCancleCommand {
            get {
                return this.CancelCommand;
            }
        }

        public ICommand SearchCommand {
            get {
                return this.searchCommand ?? (this.searchCommand = new Core.Command.DelegateCommand(this.Search));
            }
        }

        public ICommand SureCommand {
            get {
                return this.sureCommand ?? (this.sureCommand = new Core.Command.DelegateCommand(this.Sure));
            }
        }

        public bool SearchCanUse {
            get {
                return this.searchCanUse;
            }
            set {
                this.searchCanUse = value;
                this.OnPropertyChanged("SearchCanUse");
            }
        }

        public bool SureCanUse {
            get {
                return this.sureCanUse;
            }
            set {
                this.sureCanUse = value;
                this.OnPropertyChanged("SureCanUse");
            }
        }
        //截止时间
        public DateTime CutoffTime {
            get {
                return this.cutoffTime;// == null ? this.cutoffTime : new DateTime(this.cutoffTime.Year, this.cutoffTime.Month, this.cutoffTime.Day, 23, 59, 59);
            }
            set {
                this.cutoffTime = value;
                this.OnPropertyChanged("CutoffTime");
            }
        }

        public ObservableCollection<KeyValuePair> KvWarehouses {
            get {
                return this.kvWarehouses;
            }
        }

        public ObservableCollection<OutboundAndInboundBill> OutboundAndInboundBills {
            get {
                if(this.outboundAndInboundBills == null) {
                    this.outboundAndInboundBills = new ObservableCollection<OutboundAndInboundBill>();
                }
                return outboundAndInboundBills;
            }
        }

        public ObservableCollection<PartsPurchaseSettleDetail> PartsPurchaseSettleDetails {
            get {
                return partsPurchaseSettleDetails ?? (this.partsPurchaseSettleDetails = new ObservableCollection<PartsPurchaseSettleDetail>());
            }
            set {
                partsPurchaseSettleDetails = value;
            }
        }

        private void OutboundAndInboundBillsCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e) {
            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            if(partsPurchaseSettleBill == null)
                return;
            SureCanUse = (this.OutboundAndInboundBills.Count > 0);
            partsPurchaseSettleBill.TotalSettlementAmount = OutboundAndInboundBills.Sum(item => item.SettlementAmount);
        }

        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }


        private void PartsPurchaseSettleBillDataEditViewDataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            if(!(this.DataContext is PartsPurchaseSettleBill))
                return;
            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            partsPurchaseSettleBill.PropertyChanged -= this.PartsPurchaseSettleBillPropertyChanged;
            partsPurchaseSettleBill.PropertyChanged += this.PartsPurchaseSettleBillPropertyChanged;
        }
        //控制查询按钮启用
        private void PartsPurchaseSettleBillPropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            switch(e.PropertyName) {
                case "PartsSupplierCode":
                case "PartsSalesCategoryId":
                    //partsPurchaseSettleBill.WarehouseId = 0;
                    this.SearchCanUse = false;
                    this.CutoffTime = DateTime.Now.Date;
                    if(partsPurchaseSettleBill != null && (partsPurchaseSettleBill.PartsSalesCategoryId != null && !string.IsNullOrWhiteSpace(partsPurchaseSettleBill.PartsSupplierCode))) {
                        SearchCanUse = true;
                        //Search();
                    } else {
                        SearchCanUse = false;
                    }
                    //清单存在数据 清空
                    if(OutboundAndInboundBills.Any())
                        this.OutboundAndInboundBills.Clear();
                    break;
                case "CutoffTime":
                    this.SearchCanUse = partsPurchaseSettleBill != null && (partsPurchaseSettleBill.PartsSalesCategoryId != null && !string.IsNullOrWhiteSpace(partsPurchaseSettleBill.PartsSupplierCode) && partsPurchaseSettleBill.WarehouseId != 0);
                    break;
                case "WarehouseId":
                    this.CutoffTime = DateTime.Now.Date;
                    break;
                case "TotalSettlementAmount":
                case "TaxRate":
                    if(partsPurchaseSettleBill != null && (partsPurchaseSettleBill.TaxRate < 0 || partsPurchaseSettleBill.TaxRate > 1)) {
                        partsPurchaseSettleBill.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseSettleBill_TaxRateBetZeroOne, new[] {
                            "TaxRate"
                        }));
                    } else if(partsPurchaseSettleBill != null)
                        partsPurchaseSettleBill.Tax = (partsPurchaseSettleBill.TotalSettlementAmount / (decimal)(1 + partsPurchaseSettleBill.TaxRate)) * (decimal)partsPurchaseSettleBill.TaxRate;
                    break;
            }
        }

        private void CreateUI() {
            //this.HideCancelButton();
            this.HideSaveButton();
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsPurchasingUIStrings.DataGridView_Title_OutboundAndInboundBill, null, this.OutboundAndInboundBillForSupplierForEditDataGridView);//Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "PartsPurchaseOrderDetails")
            detailDataEditView.SetValue(Grid.RowProperty, 4);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            this.OutboundAndInboundBillData.Children.Add(detailDataEditView);
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsPurchasingUIStrings.DataEditView_Title_NoPurchasePriceExport,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/merge.png", UriKind.Relative),
                Command = new Core.Command.DelegateCommand(this.ExportLiaison)
            });
            var detailDataEditView2 = new DcsDetailDataEditView();
            detailDataEditView2.Register(Utils.GetEntityLocalizedName(typeof(PartsPurchaseSettleBill), "PartsPurchaseSettleDetails"), null, this.PartsPurchaseSettleDetailForSupplierForEditDataGridView);
            detailDataEditView2.SetValue(Grid.RowProperty, 3);
            detailDataEditView2.UnregisterButton(detailDataEditView2.InsertButton);
            detailDataEditView2.UnregisterButton(detailDataEditView2.DeleteButton);
            detailDataEditView2.RegisterButton(new ButtonItem {
                Title = PartsPurchasingUIStrings.Action_Title_Merge,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/merge.png", UriKind.Relative),
                Command = new Core.Command.DelegateCommand(this.MergerDetail)
            });
            this.PartsPurchaseSettleDetailData.Children.Add(detailDataEditView2);
            popupTextBoxPartsSupplierCode.IsEnabled = false;
            this.DomainContext.Load(this.DomainContext.GetWarehousesOrderByNameQuery().Where(entity => entity.StorageCompanyType == 1), loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Entities == null)
                    return;
                foreach(var warehouse in loadOp.Entities)
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name,
                        UserObject = warehouse
                    });
            }, null);
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(entity => entity.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategories in loadOp.Entities)
                    this.kvPartsSalesCategories.Add(new KeyValuePair {
                        Key = partsSalesCategories.Id,
                        Value = partsSalesCategories.Name,
                        UserObject = partsSalesCategories
                    });
            }, null);
            this.Second.Visibility = Visibility.Collapsed;
            this.First.Visibility = Visibility.Visible;
            this.saveBtn = new ButtonItem {
                Command = new Core.Command.DelegateCommand(this.SaveCurrentData),
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/confirm1.png", UriKind.Relative),
                Title = PartsPurchasingUIStrings.Action_Title_Save
            };
            this.returnBtn = new ButtonItem {
                Command = new Core.Command.DelegateCommand(this.ReturnCurrentData),
                Title = PartsPurchasingUIStrings.Action_Title_Return,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/return.png", UriKind.Relative)
            };
        }
        //确定按钮事件
        private void Sure() {
            this.First.Visibility = Visibility.Collapsed;
            this.Second.Visibility = Visibility.Visible;
            this.RegisterButton(returnBtn, true);
            this.RegisterButton(saveBtn, true);
            var inboundIds = new List<int>();
            var outboundIds = new List<int>();
            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            if(partsPurchaseSettleBill == null)
                return;
            foreach(var item in OutboundAndInboundBills) {
                if(item.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件入库检验单)
                    inboundIds.Add(item.BillId);
                if(item.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单)
                    outboundIds.Add(item.BillId);
            }
            this.DomainContext.Load(this.DomainContext.GetSalesCenterstrategiesQuery().Where(r => r.PartsSalesCategoryId == partsPurchaseSettleBill.PartsSalesCategoryId), LoadBehavior.RefreshCurrent, loadBranch => {
                if(loadBranch.HasError) {
                    loadBranch.MarkErrorAsHandled();
                    return;
                }
                if(loadBranch.Entities == null)
                    return;
                var branchStrategies = loadBranch.Entities.First();
                if(branchStrategies.PartsPurchasePricingStrategy == (int)DcsBranchstrategyPartsPurchasePricingStrategy.先定价)
                    this.DomainContext.Load(this.DomainContext.查询待结算出入库明细清单Query(inboundIds.ToArray(), outboundIds.ToArray(), null), loadOp => {
                        if(loadOp.HasError)
                            return;
                        if(loadOp.Entities == null)
                            return;
                        int temp = 1;
                        this.PartsPurchaseSettleDetails.Clear();

                        foreach(var item in loadOp.Entities) {
                            this.PartsPurchaseSettleDetails.Add(new PartsPurchaseSettleDetail {
                                SerialNumber = temp++,
                                SparePartId = item.SparePartId,
                                SparePartCode = item.SparePartCode,
                                SparePartName = item.SparePartName,
                                QuantityToSettle = (item.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单) ? -item.Quantity : item.Quantity,
                                SettlementPrice = item.SettlementPrice,
                                SettlementAmount = item.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单 ? -item.SettlementAmount : item.SettlementAmount
                            });
                        }
                        partsPurchaseSettleBill.TotalSettlementAmount = this.PartsPurchaseSettleDetails.Sum(r => r.SettlementAmount);
                    }, null);
                else
                    this.DomainContext.Load(this.DomainContext.查询待结算出入库明细清单后定价Query(inboundIds.ToArray(), outboundIds.ToArray(), partsPurchaseSettleBill.PartsSalesCategoryId.Value, partsPurchaseSettleBill.PartsSupplierId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            loadOp.MarkErrorAsHandled();
                            return;
                        }
                        if(loadOp.Entities == null)
                            return;
                        int temp = 1;
                        this.PartsPurchaseSettleDetails.Clear();

                        foreach(var item in loadOp.Entities) {
                            this.PartsPurchaseSettleDetails.Add(new PartsPurchaseSettleDetail {
                                SerialNumber = temp++,
                                SparePartId = item.SparePartId,
                                SparePartCode = item.SparePartCode,
                                SparePartName = item.SparePartName,
                                QuantityToSettle = (item.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单) ? -item.Quantity : item.Quantity,
                                SettlementPrice = item.SettlementPrice,
                                SettlementAmount = item.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单 ? -item.SettlementAmount : item.SettlementAmount
                            });
                        }
                        partsPurchaseSettleBill.TotalSettlementAmount = this.PartsPurchaseSettleDetails.Sum(r => r.SettlementAmount);
                        if(this.PartsPurchaseSettleDetails.Any(e => e.SettlementPrice == 0))
                            UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_SettlementPriceIsZero);
                    }, null);
            }, null);
        }
        //保存事件
        private void SaveCurrentData() {
            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            if(partsPurchaseSettleBill == null)
                return;

            if(PartsPurchaseSettleDetails == null || !PartsPurchaseSettleDetails.Any()) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseSettleDetailIsNone);
                return;
            }
            if(this.PartsPurchaseSettleDetails.Any(e => e.SettlementPrice == 0)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_SettlementPriceIsZero);
                return;
            }
            if(this.PartsPurchaseSettleDetails.Any(e => e.QuantityToSettle < 0)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_DetailsLessThanZero);
                return;
            }
            partsPurchaseSettleBill.ValidationErrors.Clear();
            if(partsPurchaseSettleBill.TotalSettlementAmount < 0)
                partsPurchaseSettleBill.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseSettleBill_TotalSettlementAmountGreaterZero, new[] {
                    "TotalSettlementAmount"
                }));
            if(partsPurchaseSettleBill.HasValidationErrors)
                return;
            foreach(var item in PartsPurchaseSettleDetails) {
                partsPurchaseSettleBill.PartsPurchaseSettleDetails.Add(item);
            }
            foreach(var item in OutboundAndInboundBills) {
                partsPurchaseSettleBill.PartsPurchaseSettleRefs.Add(new PartsPurchaseSettleRef {
                    SerialNumber = item.SerialNumber,
                    SourceId = item.BillId,
                    SourceCode = item.BillCode,
                    SourceType = (int)(DcsPartsPurchaseSettleRefSourceType)Enum.Parse(typeof(DcsPartsPurchaseSettleRefSourceType), Enum.GetName(typeof(DcsPartsLogisticBatchBillDetailBillType), item.BillType), true),//TODO 键值对需要转换 不能直接赋值
                    SettlementAmount = item.SettlementAmount,
                    SourceBillCreateTime = item.BillCreateTime,
                    WarehouseId = item.WarehouseId,
                    WarehouseName = item.WarehouseName
                });
            }
            if (partsPurchaseSettleBill.PartsSalesCategoryName== "红岩汽车总部" || partsPurchaseSettleBill.PartsSalesCategoryName == "红岩汽车总部保外") {
                partsPurchaseSettleBill.IsUnifiedSettle = true;
            }
            partsPurchaseSettleBill.TotalSettlementAmount = this.PartsPurchaseSettleDetails.Sum(r => r.SettlementAmount);
            ((IEditableObject)partsPurchaseSettleBill).EndEdit();
            try {
                if(this.EditState == DataEditState.New) {
                    if(partsPurchaseSettleBill.Can生成配件采购结算单)
                        partsPurchaseSettleBill.生成配件采购结算单();
                } else {
                    if(partsPurchaseSettleBill.Can修改配件采购结算单)
                        partsPurchaseSettleBill.修改配件采购结算单();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        //返回
        private void ReturnCurrentData() {
            this.UnregisterButton(saveBtn);
            this.UnregisterButton(returnBtn);
            this.Second.Visibility = Visibility.Collapsed;
            this.First.Visibility = Visibility.Visible;
        }

        //清单合并事件
        private void MergerDetail() {
            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            if(partsPurchaseSettleBill == null)
                return;
            var mergeDetail = from detail in PartsPurchaseSettleDetails
                              group detail by new {
                                  detail.SparePartId,
                                  detail.SettlementPrice
                              } into g
                              select new {
                                  g.Key.SparePartId,
                                  g.Key.SettlementPrice
                              };
            var mergeCollection = new ObservableCollection<PartsPurchaseSettleDetail>();
            var serialNumber = 1;
            foreach(var detailItem in mergeDetail) {
                var item = detailItem;
                var details = this.PartsPurchaseSettleDetails.Where(e => e.SparePartId == item.SparePartId && e.SettlementPrice == item.SettlementPrice);
                var purchaseSettleDetails = details as PartsPurchaseSettleDetail[] ?? details.ToArray();
                if(purchaseSettleDetails.Any())
                    mergeCollection.Add(new PartsPurchaseSettleDetail {
                        SerialNumber = serialNumber++,
                        SparePartId = purchaseSettleDetails.First().SparePartId,
                        SparePartCode = purchaseSettleDetails.First().SparePartCode,
                        SparePartName = purchaseSettleDetails.First().SparePartName,
                        SettlementPrice = purchaseSettleDetails.First().SettlementPrice,
                        QuantityToSettle = purchaseSettleDetails.Sum(e => e.QuantityToSettle),
                        SettlementAmount = purchaseSettleDetails.First().SettlementPrice * purchaseSettleDetails.Sum(e => e.QuantityToSettle),
                    });
            }
            partsPurchaseSettleBill.TotalSettlementAmount = this.PartsPurchaseSettleDetails.Sum(r => r.SettlementAmount);
            this.PartsPurchaseSettleDetails = new ObservableCollection<PartsPurchaseSettleDetail>(mergeCollection.OrderBy(e => e.QuantityToSettle).ThenBy(e => e.SettlementAmount));
            this.OnPropertyChanged("PartsPurchaseSettleDetails");

        }
        //点击查询按钮
        private void Search() {
            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            if(partsPurchaseSettleBill == null)
                return;
            int[] inboundTypes = { (int)DcsPartsInboundType.配件采购 };
            int[] outboundTypes = { (int)DcsPartsOutboundType.采购退货 };
            if(partsPurchaseSettleBill.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseSettleBilln_PartsSalesCategoryIsNull);
                return;
            }
            var singleOrDefault = this.kvPartsSalesCategories.SingleOrDefault(e => (e.Key == partsPurchaseSettleBill.PartsSalesCategoryId));
            if(singleOrDefault != null) {
                var entity = singleOrDefault.UserObject as PartsSalesCategory;
                if(entity != null) {
                    partsPurchaseSettleBill.BranchCode = entity.BranchCode;
                    partsPurchaseSettleBill.BranchId = entity.BranchId;
                    partsPurchaseSettleBill.BranchName = entity.BranchName;
                    this.DomainContext.Load(this.DomainContext.查询待采购结算出入库明细Query(entity.BranchId, partsPurchaseSettleBill.PartsSupplierId, partsPurchaseSettleBill.WarehouseId.HasValue ? (partsPurchaseSettleBill.WarehouseId.Value == default(int) ? (int?)null : partsPurchaseSettleBill.WarehouseId.Value)
                    : null, null, partsPurchaseSettleBill.PartsSalesCategoryId, inboundTypes, outboundTypes, CutoffTime = new DateTime(CutoffTime.Year, CutoffTime.Month, CutoffTime.Day, 23, 59, 59)), loadOp => {//this.CutoffTime
                        if(loadOp.HasError || loadOp.Entities == null)
                            return;
                        int temp = 1;
                        this.OutboundAndInboundBills.Clear();
                        partsPurchaseSettleBill.TotalCostAmount = loadOp.Entities.Sum(item => item.CostAmount);
                        foreach(var item in loadOp.Entities) {
                            this.DomainContext.OutboundAndInboundBills.Detach(item);
                            item.SerialNumber = temp++;
                            if(item.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单)
                                item.SettlementAmount = -item.SettlementAmount;
                            this.OutboundAndInboundBills.Add(item);
                        }
                        this.SureCanUse = (this.OutboundAndInboundBills.Count > 0);
                    }, null);
                }
            }
        }

        private void QueryWindowPartsSupplierSelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var partsSupplier = queryWindow.SelectedEntities.Cast<PartsSupplier>().FirstOrDefault();
            if(partsSupplier == null)
                return;
            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            if(partsPurchaseSettleBill == null)
                return;
            partsPurchaseSettleBill.PartsSupplierId = partsSupplier.Id;
            partsPurchaseSettleBill.PartsSupplierName = partsSupplier.Name;
            partsPurchaseSettleBill.PartsSupplierCode = partsSupplier.Code;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsPurchaseSettleBill;
            }
        }

        //无采购价导出
        private void ExportLiaison() {
            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            if(partsPurchaseSettleBill == null)
                return;
            if(this.OutboundAndInboundBills == null || this.outboundAndInboundBills.Count() == default(int)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_OutboundAndInboundBillsIsNull);
                return;
            }
            int[] inboundTypes = { (int)DcsPartsInboundType.配件采购 };
            int[] outboundTypes = { (int)DcsPartsOutboundType.采购退货 };

            this.DomainContext.Load(this.DomainContext.查询带采购价的待采购结算出入库明细Query(partsPurchaseSettleBill.BranchId, partsPurchaseSettleBill.PartsSupplierId, partsPurchaseSettleBill.WarehouseId, null, partsPurchaseSettleBill.PartsSalesCategoryId, inboundTypes, outboundTypes, CutoffTime = new DateTime(CutoffTime.Year, CutoffTime.Month, CutoffTime.Day, 23, 59, 59)), loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Entities == null || !loadOp.Entities.Any()) {
                    UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_SettlementPriceIsNull);
                    return;
                }
                this.DomainContext.ExportPurchaseSettlementOutboundAndInboundBillDetail(partsPurchaseSettleBill.BranchId, partsPurchaseSettleBill.PartsSupplierId, partsPurchaseSettleBill.WarehouseId, null, partsPurchaseSettleBill.PartsSalesCategoryId, inboundTypes, outboundTypes, CutoffTime = new DateTime(CutoffTime.Year, CutoffTime.Month, CutoffTime.Day, 23, 59, 59), loadOp1 => {
                    if(loadOp1.HasError)
                        return;
                    if(loadOp1.Value == null || string.IsNullOrEmpty(loadOp1.Value)) {
                        UIHelper.ShowNotification(loadOp1.Value);
                    }
                    if(loadOp1.Value != null && !string.IsNullOrEmpty(loadOp1.Value)) {
                        HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp1.Value));
                    }
                }, null);
            }, null);
        }

        //这里不用Reset()  会有问题。 
        protected override void OnEditCancelled() {
            this.Second.Visibility = Visibility.Collapsed;
            this.First.Visibility = Visibility.Visible;
            this.OutboundAndInboundBills.Clear();
            PartsPurchaseSettleDetails.Clear();
            this.UnregisterButton(saveBtn);
            this.UnregisterButton(returnBtn);

            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            if(partsPurchaseSettleBill == null)
                return;
            if(this.DomainContext.PartsPurchaseSettleBills.Contains(partsPurchaseSettleBill))
                this.DomainContext.PartsPurchaseSettleBills.Detach(partsPurchaseSettleBill);

            base.OnEditCancelled();
        }

        protected override void OnEditSubmitted() {
            this.Second.Visibility = Visibility.Collapsed;
            this.First.Visibility = Visibility.Visible;
            this.OutboundAndInboundBills.Clear();
            PartsPurchaseSettleDetails.Clear();
            this.UnregisterButton(saveBtn);
            this.UnregisterButton(returnBtn);

            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            if(partsPurchaseSettleBill == null)
                return;
            if(this.DomainContext.PartsPurchaseSettleBills.Contains(partsPurchaseSettleBill))
                this.DomainContext.PartsPurchaseSettleBills.Detach(partsPurchaseSettleBill);

            base.OnEditSubmitted();
        }

        public override void SetObjectToEditById(object id) {
            SearchCanUse = true;
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsPurchaseSettleBillsWithDetailsAndRefsByIdSupplyQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.OutboundAndInboundBills.Clear();
                    this.PartsPurchaseSettleDetails.Clear();
                    foreach(var item in entity.PartsPurchaseSettleRefs) {
                        this.OutboundAndInboundBills.Add(new OutboundAndInboundBill {
                            SerialNumber = item.SerialNumber,
                            BillId = item.SourceId,
                            BillCode = item.SourceCode,
                            BillType = (int)(DcsPartsLogisticBatchBillDetailBillType)Enum.Parse(typeof(DcsPartsLogisticBatchBillDetailBillType), Enum.GetName(typeof(DcsPartsPurchaseSettleRefSourceType), item.SourceType), true),//TODO 两个键值对不一致 需转换
                            SettlementAmount = item.SettlementAmount,
                            BillCreateTime = item.SourceBillCreateTime,
                            WarehouseId = item.WarehouseId,
                            WarehouseName = item.WarehouseName,
                            ReturnReason = item.ReturnReason,
                            CPPartsPurchaseOrderCode = item.CPPartsPurchaseOrderCode,
                            CPPartsInboundCheckCode = item.CPPartsInboundCheckCode
                        });
                        entity.PartsPurchaseSettleRefs.Remove(item);
                    }
                    foreach(var item in entity.PartsPurchaseSettleDetails) {
                        this.PartsPurchaseSettleDetails.Add(item);
                        entity.PartsPurchaseSettleDetails.Remove(item);
                    }
                    this.SureCanUse = true;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        private void WarehouseDcsComboBoxSelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            if(e.AddedItems == null || e.AddedItems.Count != 1)
                return;
            var temp = sender as RadComboBox;
            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            if(temp != null && partsPurchaseSettleBill != null) {
                var warehouse = temp.SelectedItem as KeyValuePair;
                if(warehouse != null)
                    partsPurchaseSettleBill.WarehouseCode = ((Warehouse)warehouse.UserObject).Code;
            }
        }

        public ObservableCollection<KeyValuePair> KvPartsSalesCategories {
            get {
                return this.kvPartsSalesCategories;
            }
        }
    }
}
