﻿
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using System;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class PartsPurchaseSettleBillInvoiceApproveDataEditView {
        //private ObservableCollection<KeyValuePair> kvIsPasseds;
        //public ObservableCollection<KeyValuePair> KvIsPasseds {
        //    get {
        //        return this.kvIsPasseds ?? (this.kvIsPasseds = new ObservableCollection<KeyValuePair>());
        //    }
        //}

        private DataGridViewBase invoiceInformationForSettleDataGridView;
        public DataGridViewBase InvoiceInformationForSettleDataGridView {
            get {
                if(this.invoiceInformationForSettleDataGridView == null) {
                    this.invoiceInformationForSettleDataGridView = DI.GetDataGridView("InvoiceInformationForApprove");
                    this.invoiceInformationForSettleDataGridView.DomainContext = this.DomainContext;
                    this.invoiceInformationForSettleDataGridView.DataContext = this;
                }
                return this.invoiceInformationForSettleDataGridView;
            }
        }

        private ObservableCollection<InvoiceInformation> invoiceInformations = new ObservableCollection<InvoiceInformation>();
        public ObservableCollection<InvoiceInformation> InvoiceInformations {
            get {
                if(this.invoiceInformations == null) {
                    this.invoiceInformations = new ObservableCollection<InvoiceInformation>();
                }
                return invoiceInformations;
            }
        }

        public PartsPurchaseSettleBillInvoiceApproveDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += PartsPurchaseSettleBillInvoiceApproveDataEditView_DataContextChanged;
        }

        void PartsPurchaseSettleBillInvoiceApproveDataEditView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            if(partsPurchaseSettleBill == null)
                return;
        }

        private void CreateUI() {
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsPurchasingUIStrings.DataGridView_Title_InvoiceRegister_PartsPurchaseSettleBill, null, () => this.InvoiceInformationForSettleDataGridView);//
            detailDataEditView.SetValue(Grid.RowProperty, 1);
            detailDataEditView.SetValue(Grid.ColumnSpanProperty, 2);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            this.LayoutRoot.Children.Add(detailDataEditView);
            //this.kvIsPasseds.Add(new KeyValuePair {
            //    Key = 0,
            //    Value = "通过"
            //});
            //this.kvIsPasseds.Add(new KeyValuePair {
            //    Key = 1,
            //    Value = "驳回"
            //});
            this.RegisterButton(new ButtonItem {
                Command = new DelegateCommand(this.RejectCurrentData),
                Title = PartsPurchasingUIStrings.DataEditPanel_Title_Reject,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative)
            },true);
        }

        private void RejectCurrentData() {
            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            if(partsPurchaseSettleBill == null)
                return;
            ((IEditableObject)partsPurchaseSettleBill).EndEdit();
            try {
                if (partsPurchaseSettleBill.Can审核采购发票)
                {
                    var sum = this.InvoiceInformations.Sum(e => e.InvoiceAmount);
                    if(sum != null) {
                        var sumInvoiceTax = this.InvoiceInformations.Sum(e => e.InvoiceTax);
                        if(sumInvoiceTax != null)
                            partsPurchaseSettleBill.审核采购发票(false);
                    }
                }
            } catch(ValidationException ex) {
                this.DomainContext.InvoiceInformations.Clear();
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        protected override string Title {
            get {
                return PartsPurchasingUIStrings.Action_Title_PartsPurchaseSettleBillInvoiceApprove;
            }
        }

        protected override void OnEditSubmitting() {
            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            if(partsPurchaseSettleBill == null)
                return;
            partsPurchaseSettleBill.IsPassed = 0;
            if(partsPurchaseSettleBill.IsPassed == null || partsPurchaseSettleBill.IsPassed <0) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_ChooseApproveResult);
                return;
            }
            ((IEditableObject)partsPurchaseSettleBill).EndEdit();
            try {
                if (partsPurchaseSettleBill.Can审核采购发票)
                {
                    var sum = this.InvoiceInformations.Sum(e => e.InvoiceAmount);
                    if(sum != null) {
                        var sumInvoiceTax = this.InvoiceInformations.Sum(e => e.InvoiceTax);
                        if(sumInvoiceTax != null)
                            partsPurchaseSettleBill.审核采购发票(partsPurchaseSettleBill.IsPassed == 0);
                    }
                }
            } catch(ValidationException ex) {
                this.DomainContext.InvoiceInformations.Clear();
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsPurchaseSettleBillsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    //采购结算单审核结果赋值为空
                    entity.IsPassed = 0;
                    //查询发票信息
                    this.DomainContext.Load(this.DomainContext.GetInvoiceInformationsQuery().Where(e => e.SourceId == id && e.SourceType == (int)DcsInvoiceInformationSourceType.配件采购结算单), LoadBehavior.RefreshCurrent, load => {
                        if(loadOp.HasError) {
                            if(!loadOp.IsErrorHandled)
                                loadOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                            return;
                        }
                        int serialNumber = 1;
                        this.InvoiceInformations.Clear();
                        foreach(var data in load.Entities.ToArray()) {
                            data.SerialNumber = serialNumber;
                            this.InvoiceInformations.Add(data);
                            serialNumber++;
                        }
                    }, null);
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void OnEditCancelled() {
            if(this.InvoiceInformations.Any())
                this.InvoiceInformations.Clear();
            base.OnEditCancelled();
        }

        protected override void OnEditSubmitted() {
            base.OnEditSubmitted();
            this.InvoiceInformations.Clear();
        }

        protected override void Reset() {
            var invoiceInformation = this.DataContext as InvoiceInformation;
            if(invoiceInformation != null && this.DomainContext.InvoiceInformations.Contains(invoiceInformation))
                this.DomainContext.InvoiceInformations.Detach(invoiceInformation);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

    }
}
