﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class PartsPurchaseSettleBillInvoiceRegisterDataEditView {
        private ObservableCollection<InvoiceInformation> invoiceInformations = new ObservableCollection<InvoiceInformation>();
        private DataGridViewBase invoiceInformationForSettleDataGridView;

        public DataGridViewBase InvoiceInformationForSettleDataGridView {
            get {
                if(this.invoiceInformationForSettleDataGridView == null) {
                    this.invoiceInformationForSettleDataGridView = DI.GetDataGridView("InvoiceInformationRegisterForSettle");
                    this.invoiceInformationForSettleDataGridView.DomainContext = this.DomainContext;
                    this.invoiceInformationForSettleDataGridView.DataContext = this;
                }
                return this.invoiceInformationForSettleDataGridView;
            }
        }

        public ObservableCollection<InvoiceInformation> InvoiceInformations {
            get {
                if(this.invoiceInformations == null) {
                    this.invoiceInformations = new ObservableCollection<InvoiceInformation>();
                }
                return invoiceInformations;
            }
        }

        public PartsPurchaseSettleBillInvoiceRegisterDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsPurchasingUIStrings.DataGridView_Title_InvoiceRegister_PartsPurchaseSettleBill, null, () => this.InvoiceInformationForSettleDataGridView);//
            detailDataEditView.SetValue(Grid.RowProperty, 1);
            detailDataEditView.SetValue(Grid.ColumnSpanProperty, 8);
            this.Root.Children.Add(detailDataEditView);
        }

        protected override void OnEditSubmitting() {
            if(!this.InvoiceInformationForSettleDataGridView.CommitEdit())
                return;

            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            if(partsPurchaseSettleBill == null)
                return;

            if(!this.InvoiceInformations.Any()) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseSettleBill_InvoiceInformationsIsNull);
                return;
            }
            foreach(var invoiceInformation in InvoiceInformations) {
                if(string.IsNullOrEmpty(invoiceInformation.InvoiceCode)) {
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceCodeIsNull, new[] {
                        "InvoiceCode"
                    }));
                } else {
                    if(invoiceInformation.InvoiceCode.Length > 10)
                        invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceCodeMoreThan10, new[] {
                       "InvoiceCode"
                    }));
                }
                if(string.IsNullOrEmpty(invoiceInformation.InvoiceNumber)) {
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceNumberIsNull, new[] {
                        "InvoiceNumber"
                    }));
                } else {
                    if(invoiceInformation.InvoiceNumber.Length > 20)
                        invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceNumberLengthMoreThan20, new[] {
                        "InvoiceNumber"
                    }));
                }
                if(!invoiceInformation.InvoiceAmount.HasValue) {
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceAmountIsNull, new[] {
                    "InvoiceAmount"
                }));
                } else if(invoiceInformation.InvoiceAmount <= 0) {
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceAmountMustGreaterZero, new[] {
                    "InvoiceAmount"
                }));
                }
                if(!invoiceInformation.TaxRate.HasValue)
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_TaxRateIsNull, new[] {
                    "TaxRate"
                }));
                if(!invoiceInformation.InvoiceTax.HasValue)
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceTaxIsNull, new[] {
                    "InvoiceTax"
                }));
                if(!invoiceInformation.InvoiceDate.HasValue)
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceDateIsNull, new[] {
                    "InvoiceDate"
                }));

            }

            if(InvoiceInformations.GroupBy(entity => entity.InvoiceNumber).Any(array => array.Count() > 1)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceCodeRepeat);
                return;
            }

            if(InvoiceInformations.Any(e => e.HasValidationErrors))
                return;


            ((IEditableObject)partsPurchaseSettleBill).EndEdit();
            try {
                foreach(var item in InvoiceInformations) {
                    item.PartsSalesCategoryId = partsPurchaseSettleBill.PartsSalesCategoryId;
                }
                this.DomainContext.Load(this.DomainContext.GetBranchstrategiesQuery().Where(r => r.BranchId == partsPurchaseSettleBill.BranchId), LoadBehavior.RefreshCurrent, loadOpS => {
                    if(loadOpS.HasError) {
                        if(loadOpS.IsErrorHandled)
                            loadOpS.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOpS);
                        return;
                    }
                    var branchstrategy = loadOpS.Entities.SingleOrDefault();
                    //校验：分公司策略.发票金额是否允许大于采购结算单金额
                    if(branchstrategy != null && branchstrategy.IsPurchaseAmount.HasValue && (bool)branchstrategy.IsPurchaseAmount) {
                        if(InvoiceInformations.Sum(e => e.InvoiceAmount) > partsPurchaseSettleBill.TotalSettlementAmount) {
                            UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_SumInvoiceAmountMoreThanTotalSettlementAmount);
                            return;
                        }
                    }
                    this.DomainContext.配件采购结算单发票登记(partsPurchaseSettleBill, InvoiceInformations.ToArray(), invokeOp => {
                        if(invokeOp.HasError) {
                            if(!invokeOp.IsErrorHandled)
                                invokeOp.MarkErrorAsHandled();
                            var error = invokeOp.ValidationErrors.FirstOrDefault();
                            if(error != null) {
                                UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage, 5);
                            } else {
                                DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                            }
                            DomainContext.RejectChanges();
                            return;
                        }
                        this.NotifyEditSubmitted();
                        this.OnCustomEditSubmitted();
                    }, null);
                }, null);
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
        }

        public void OnCustomEditSubmitted() {
            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            if(partsPurchaseSettleBill != null && this.DomainContext.PartsPurchaseSettleBills.Contains(partsPurchaseSettleBill)) {
                this.DomainContext.PartsPurchaseSettleBills.Detach(partsPurchaseSettleBill);
            }
            if(this.InvoiceInformations.Any()) {
                foreach(var item in this.InvoiceInformations.Where(item => this.DomainContext.InvoiceInformations.Contains(item))) {
                    this.DomainContext.InvoiceInformations.Detach(item);
                }
                this.InvoiceInformations.Clear();
            }
        }

        public new event EventHandler EditSubmitted;
        private void NotifyEditSubmitted() {
            var handler = this.EditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsPurchaseSettleBillsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.DomainContext.Load(this.DomainContext.GetBranchstrategiesQuery().Where(r => r.BranchId == entity.BranchId && r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp1 => {
                        if(loadOp1.HasError) {
                            if(!loadOp1.IsErrorHandled) {
                                loadOp1.MarkErrorAsHandled();
                                DcsUtils.ShowLoadError(loadOp1);
                                return;
                            }
                        }
                        var branchstrategy = loadOp1.Entities.FirstOrDefault();
                        if(branchstrategy != null)
                            entity.MutInvoiceStrategy = branchstrategy.MutInvoiceStrategy;
                        this.SetObjectToEdit(entity);
                    }, null);
                }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override string Title {
            get {
                return PartsPurchasingUIStrings.DataEditView_Title_InvoiceRegister_PartsPurchaseSettleBill;
            }
        }

        protected override void Reset() {
            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            if(partsPurchaseSettleBill != null && this.DomainContext.PartsPurchaseSettleBills.Contains(partsPurchaseSettleBill))
                this.DomainContext.PartsPurchaseSettleBills.Detach(partsPurchaseSettleBill);
            if(this.InvoiceInformations.Any()) {
                foreach(var item in this.InvoiceInformations.Where(item => this.DomainContext.InvoiceInformations.Contains(item))) {
                    this.DomainContext.InvoiceInformations.Detach(item);
                }
                this.InvoiceInformations.Clear();
            }
        }

    }
}