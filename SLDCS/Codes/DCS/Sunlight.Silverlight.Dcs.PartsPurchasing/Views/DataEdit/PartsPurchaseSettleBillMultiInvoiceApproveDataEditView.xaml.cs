﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using System;


namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class PartsPurchaseSettleBillMultiInvoiceApproveDataEditView : INotifyPropertyChanged {

        public PartsPurchaseSettleBillMultiInvoiceApproveDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private ObservableCollection<PartsPurchaseSettleBill> partsPurchaseSettleBills;
        public ObservableCollection<PartsPurchaseSettleBill> PartsPurchaseSettleBills {
            get {
                if(this.partsPurchaseSettleBills == null) {
                    this.partsPurchaseSettleBills = new ObservableCollection<PartsPurchaseSettleBill>();
                }
                return partsPurchaseSettleBills;
            }
        }

        //private ObservableCollection<KeyValuePair> kvIsPasseds;
        //public ObservableCollection<KeyValuePair> KvIsPasseds {
        //    get {
        //        return this.kvIsPasseds ?? (this.kvIsPasseds = new ObservableCollection<KeyValuePair>());
        //    }
        //}

        private DataGridViewBase invoiceInformationForPartsPurchaseSettleBillAllDataGridView;
        public DataGridViewBase InvoiceInformationForPartsPurchaseSettleBillAllDataGridView {
            get {
                if(this.invoiceInformationForPartsPurchaseSettleBillAllDataGridView == null) {
                    this.invoiceInformationForPartsPurchaseSettleBillAllDataGridView = DI.GetDataGridView("InvoiceInformationForPartsPurchaseSettleBillAll");
                    this.invoiceInformationForPartsPurchaseSettleBillAllDataGridView.DomainContext = this.DomainContext;
                    this.invoiceInformationForPartsPurchaseSettleBillAllDataGridView.DataContext = this;
                }
                return this.invoiceInformationForPartsPurchaseSettleBillAllDataGridView;
            }
        }

        private int isPassed;
        public int IsPassed {
            get {
                return this.isPassed;
            }
            set {
                this.isPassed = value;
                this.OnPropertyChanged(("IsPassed"));
            }
        }

        private void CreateUI() {
            //this.kvIsPasseds.Add(new KeyValuePair {
            //    Key = 0,
            //    Value = "驳回"
            //});
            //this.kvIsPasseds.Add(new KeyValuePair {
            //    Key = 1,
            //    Value = "通过"
            //});
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.Register(PartsPurchasingUIStrings.DataEditView_Title_InvoiceInformation, null, () => this.InvoiceInformationForPartsPurchaseSettleBillAllDataGridView);
            this.Root.Children.Add(detailDataEditView);

            this.RegisterButton(new ButtonItem {
                Command = new DelegateCommand(this.RejectCurrentData),
                Title = PartsPurchasingUIStrings.DataEditPanel_Title_Reject,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative)
            },true);
        }

        private void RejectCurrentData() {
            var invoiceInformation = this.DataContext as InvoiceInformation;
            if(invoiceInformation == null)
                return;
            ((IEditableObject)invoiceInformation).EndEdit();
            this.DomainContext.Load(this.DomainContext.GetPartsPurchaseSettleBillByIdQuery(selectPartsPurchaseSettleBillId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var partsPurchaseSettleBill = loadOp.Entities.SingleOrDefault();
                if(partsPurchaseSettleBill != null) {
                    try {
                        if(partsPurchaseSettleBill.Can审核多条采购结算单发票) {
                            partsPurchaseSettleBill.审核多条采购结算单发票(false, invoiceInformation.Id);
                        }
                    } catch(ValidationException ex) {
                        UIHelper.ShowAlertMessage(ex.Message);
                        return;
                    }
                }
                base.OnEditSubmitting();
            }, null);
        }

        protected override string Title {
            get {
                return PartsPurchasingUIStrings.DataEditView_Title_InvoiceApprove_PartsPurchaseSettleBillAll;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private int selectPartsPurchaseSettleBillId;
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.根据采购结算单获取发票与采购结算单信息Query(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                selectPartsPurchaseSettleBillId = id;
                this.IsPassed = 1;
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                    var sequenceNumber = 1;
                    PartsPurchaseSettleBills.Clear();
                    if(entity.PurchaseSettleInvoiceRels.Any()) {
                        var items = entity.PurchaseSettleInvoiceRels.Select(r => r.PartsPurchaseSettleBill).ToArray();
                        foreach(var item in items) {
                            item.SerialNumber = sequenceNumber++;
                            PartsPurchaseSettleBills.Add(item);
                        }
                    }
                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            var invoiceInformation = this.DataContext as InvoiceInformation;
            if(invoiceInformation == null)
                return;
            ((IEditableObject)invoiceInformation).EndEdit();
            IsPassed = 1;
            if(IsPassed == -1) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_PleaseWriteApproveResult);
                return;
            }
            this.DomainContext.Load(this.DomainContext.GetPartsPurchaseSettleBillByIdQuery(selectPartsPurchaseSettleBillId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var partsPurchaseSettleBill = loadOp.Entities.SingleOrDefault();
                if(partsPurchaseSettleBill != null) {
                    try {
                        if(partsPurchaseSettleBill.Can审核多条采购结算单发票) {
                            partsPurchaseSettleBill.审核多条采购结算单发票(this.IsPassed == 1, invoiceInformation.Id);
                        }
                    } catch(ValidationException ex) {
                        UIHelper.ShowAlertMessage(ex.Message);
                        return;
                    }
                }
                base.OnEditSubmitting();
            }, null);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void Reset() {
            var invoiceInformation = this.DataContext as InvoiceInformation;
            if(invoiceInformation != null) {
                if(invoiceInformation.PurchaseSettleInvoiceRels != null) {
                    foreach(var item in invoiceInformation.PurchaseSettleInvoiceRels.Where(item => this.DomainContext.PurchaseSettleInvoiceRels.Contains(item))) {
                        this.DomainContext.PurchaseSettleInvoiceRels.Detach(item);
                    }
                }
                if(this.DomainContext.InvoiceInformations.Contains(invoiceInformation))
                    this.DomainContext.InvoiceInformations.Detach(invoiceInformation);
            }
            foreach(var item in this.PartsPurchaseSettleBills.Where(item => this.DomainContext.PartsPurchaseSettleBills.Contains(item))) {
                this.DomainContext.PartsPurchaseSettleBills.Detach(item);
            }
            this.PartsPurchaseSettleBills.Clear();

        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
