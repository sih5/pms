﻿
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class PartsPurchaseSettleBillMultiInvoiceEditDataEditView {
        public int SupplierId, PartsSalesCategoryId;
        public string PartsSupplierCode, PartsSupplierName;
        public new event EventHandler EditSubmitted;
        public PartsPurchaseSettleBillMultiInvoiceEditDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += PartsPurchaseSettleBillMultiInvoiceRegisterDataEditView_DataContextChanged;
        }

        private void PartsPurchaseSettleBillMultiInvoiceRegisterDataEditView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var invoiceInformation = this.DataContext as InvoiceInformation;
            if(invoiceInformation == null)
                return;
            invoiceInformation.PropertyChanged += invoiceInformation_PropertyChanged;
        }

        private ObservableCollection<PartsPurchaseSettleBill> partsPurchaseSettleBills;
        public ObservableCollection<PartsPurchaseSettleBill> PartsPurchaseSettleBills {
            get {
                if(this.partsPurchaseSettleBills == null) {
                    this.partsPurchaseSettleBills = new ObservableCollection<PartsPurchaseSettleBill>();
                }
                return partsPurchaseSettleBills;
            }
        }

        private DataGridViewBase invoiceInformationForPartsPurchaseSettleBillAllDataGridView;
        public DataGridViewBase InvoiceInformationForPartsPurchaseSettleBillAllDataGridView {
            get {
                if(this.invoiceInformationForPartsPurchaseSettleBillAllDataGridView == null) {
                    this.invoiceInformationForPartsPurchaseSettleBillAllDataGridView = DI.GetDataGridView("InvoiceInformationForPartsPurchaseSettleBillAll");
                    this.invoiceInformationForPartsPurchaseSettleBillAllDataGridView.DomainContext = this.DomainContext;
                    this.invoiceInformationForPartsPurchaseSettleBillAllDataGridView.DataContext = this;
                }
                return this.invoiceInformationForPartsPurchaseSettleBillAllDataGridView;
            }
        }

        private void CreateUI() {
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsPurchasingUIStrings.DataEditView_Title_InvoiceInformation, null, () => this.InvoiceInformationForPartsPurchaseSettleBillAllDataGridView);
            this.Root.Children.Add(detailDataEditView);
        }

        protected override string Title {
            get {
                return PartsPurchasingUIStrings.DataEditView_Title_InvoiceEdit_PartsPurchaseSettleBillAll;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.根据采购结算单获取发票与采购结算单信息Query(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                    var sequenceNumber = 1;
                    PartsPurchaseSettleBills.Clear();
                    if(entity.PurchaseSettleInvoiceRels.Any()) {
                        //用于修改时添加采购结算单时给弹出窗传参数
                        var paramEntity = entity.PurchaseSettleInvoiceRels.Last().PartsPurchaseSettleBill;
                        SupplierId = paramEntity.PartsSupplierId;
                        PartsSalesCategoryId = (int)paramEntity.PartsSalesCategoryId;
                        PartsSupplierCode = paramEntity.PartsSupplierCode;
                        PartsSupplierName = paramEntity.PartsSupplierName;
                        var items = entity.PurchaseSettleInvoiceRels.Select(r => r.PartsPurchaseSettleBill).ToArray();
                        foreach(var item in items) {
                            item.SerialNumber = sequenceNumber++;
                            PartsPurchaseSettleBills.Add(item);
                        }
                    }
                }
            }, null);
        }

        private void invoiceInformation_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var invoiceInformation = this.DataContext as InvoiceInformation;
            if(invoiceInformation == null)
                return;
            switch(e.PropertyName) {
                case "TaxRate":
                case "InvoiceAmount":
                    if(invoiceInformation.TaxRate > 1 || invoiceInformation.TaxRate < 0) {
                        invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_TaxRateMustBetweenZeroAndOne, new[] {
                            "TaxRate"
                        }));
                        invoiceInformation.InvoiceTax = default(decimal);
                        return;
                    }
                    if(invoiceInformation.TaxRate != null && invoiceInformation.InvoiceAmount != null)
                        invoiceInformation.InvoiceTax = Math.Round((decimal)((invoiceInformation.InvoiceAmount / (decimal)(1 + invoiceInformation.TaxRate)) * (decimal)invoiceInformation.TaxRate), 2);
                    break;
            }
        }

        protected override void OnEditSubmitting() {
            var invoiceInformation = this.DataContext as InvoiceInformation;
            if(invoiceInformation == null)
                return;
            if(!this.PartsPurchaseSettleBills.Any()) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_DetailsIsNull);
                return;
            }
            if(string.IsNullOrEmpty(invoiceInformation.InvoiceCode)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceCodeIsNull);
                return;
            }
            if(invoiceInformation.InvoiceCode.Length > 10) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceCodeMoreThan10);
                return;
            }
            if(string.IsNullOrEmpty(invoiceInformation.InvoiceNumber)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceNumberIsNull);
                return;
            }
            if(invoiceInformation.InvoiceNumber.Length > 20) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceNumberLengthMoreThan20);
                return;
            }
            if(invoiceInformation.InvoiceTax == default(int) || invoiceInformation.InvoiceTax == null) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceTaxIsNull);
                return;
            }
            if(invoiceInformation.TaxRate == null) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_TaxRateIsNull);
                return;
            }
            if(invoiceInformation.InvoiceDate == null) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceDateIsNotNull);
                return;
            }
            //取消校验--0106
            //var partsPurchaseSettleBillTotalAmount = this.PartsPurchaseSettleBills.Sum(r => r.TotalSettlementAmount);
            //if(invoiceInformation.InvoiceAmount == null || invoiceInformation.InvoiceAmount > partsPurchaseSettleBillTotalAmount) {
            //    UIHelper.ShowNotification("发票金额不可大于采购结算单汇总金额");
            //    return;
            //}
            var partsPurchaseSettleBillIds = this.PartsPurchaseSettleBills.Select(r => r.Id).ToArray();
            ((IEditableObject)invoiceInformation).EndEdit();
            try {
                this.DomainContext.修改采购发票信息及采购结算单(partsPurchaseSettleBillIds, invoiceInformation, invokeOp => {
                    if(invokeOp.HasError) {
                        if(!invokeOp.IsErrorHandled)
                            invokeOp.MarkErrorAsHandled();
                        var error = invokeOp.ValidationErrors.FirstOrDefault();
                        if(error != null) {
                            UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage, 5);
                            DomainContext.RejectChanges();
                            return;
                        }
                        DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                        this.DomainContext.RejectChanges();
                        return;
                    }
                    this.NotifyEditSubmitted();
                    this.OnCustomEditSubmitted();
                }, null);
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }

        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void Reset() {
            var invoiceInformation = this.DataContext as InvoiceInformation;
            if(invoiceInformation != null) {
                if(invoiceInformation.PurchaseSettleInvoiceRels != null) {
                    foreach(var item in invoiceInformation.PurchaseSettleInvoiceRels.Where(item => this.DomainContext.PurchaseSettleInvoiceRels.Contains(item))) {
                        this.DomainContext.PurchaseSettleInvoiceRels.Detach(item);
                    }
                }
                if(this.DomainContext.InvoiceInformations.Contains(invoiceInformation))
                    this.DomainContext.InvoiceInformations.Detach(invoiceInformation);
            }
            foreach(var item in this.PartsPurchaseSettleBills.Where(item => this.DomainContext.PartsPurchaseSettleBills.Contains(item))) {
                this.DomainContext.PartsPurchaseSettleBills.Detach(item);
            }
            this.PartsPurchaseSettleBills.Clear();
        }

        private void NotifyEditSubmitted() {
            var handler = this.EditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        public void OnCustomEditSubmitted() {
            var invoiceInformation = this.DataContext as InvoiceInformation;
            if(invoiceInformation != null) {
                if(invoiceInformation.PurchaseSettleInvoiceRels != null) {
                    foreach(var item in invoiceInformation.PurchaseSettleInvoiceRels.Where(item => this.DomainContext.PurchaseSettleInvoiceRels.Contains(item))) {
                        this.DomainContext.PurchaseSettleInvoiceRels.Detach(item);
                    }
                }
                if(this.DomainContext.InvoiceInformations.Contains(invoiceInformation))
                    this.DomainContext.InvoiceInformations.Detach(invoiceInformation);
            }
            foreach(var item in this.PartsPurchaseSettleBills.Where(item => this.DomainContext.PartsPurchaseSettleBills.Contains(item))) {
                this.DomainContext.PartsPurchaseSettleBills.Detach(item);
            }
            this.PartsPurchaseSettleBills.Clear();
        }



    }
}
