﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class PartsPurchaseSettleBillMultiInvoiceRegisterDataEditView {

        public new event EventHandler EditSubmitted;

        public PartsPurchaseSettleBillMultiInvoiceRegisterDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += PartsPurchaseSettleBillMultiInvoiceRegisterDataEditView_DataContextChanged;
        }

        private void PartsPurchaseSettleBillMultiInvoiceRegisterDataEditView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var invoiceInformation = this.DataContext as InvoiceInformation;
            if(invoiceInformation == null)
                return;
            invoiceInformation.PropertyChanged += invoiceInformation_PropertyChanged;
        }

        private ObservableCollection<PartsPurchaseSettleBill> partsPurchaseSettleBills;
        public ObservableCollection<PartsPurchaseSettleBill> PartsPurchaseSettleBills {
            get {
                if(this.partsPurchaseSettleBills == null) {
                    this.partsPurchaseSettleBills = new ObservableCollection<PartsPurchaseSettleBill>();
                }
                return partsPurchaseSettleBills;
            }
        }

        private DataGridViewBase invoiceInformationForPartsPurchaseSettleBillAllDataGridView;
        public DataGridViewBase InvoiceInformationForPartsPurchaseSettleBillAllDataGridView {
            get {
                if(this.invoiceInformationForPartsPurchaseSettleBillAllDataGridView == null) {
                    this.invoiceInformationForPartsPurchaseSettleBillAllDataGridView = DI.GetDataGridView("InvoiceInformationForPartsPurchaseSettleBillAllAdd");
                    this.invoiceInformationForPartsPurchaseSettleBillAllDataGridView.DomainContext = this.DomainContext;
                    this.invoiceInformationForPartsPurchaseSettleBillAllDataGridView.DataContext = this;
                }
                return this.invoiceInformationForPartsPurchaseSettleBillAllDataGridView;
            }
        }

        private void CreateUI() {
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsPurchasingUIStrings.DataEditView_Title_InvoiceInformation, null, () => this.InvoiceInformationForPartsPurchaseSettleBillAllDataGridView);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            this.Root.Children.Add(detailDataEditView);
        }

        protected override string Title {
            get {
                return PartsPurchasingUIStrings.DataEditView_Title_InvoiceRegister_PartsPurchaseSettleBillAll;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit(id));
            else
                this.LoadEntityToEdit(id);
        }

        private void LoadEntityToEdit(object id) {
            var arrayOfIds = id as int[];
            this.DomainContext.Load(this.DomainContext.GetPartsPurchaseSettleBillByIdsQuery(arrayOfIds), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities;
                var sequenceNumber = 1;
                PartsPurchaseSettleBills.Clear();
                if(entity != null) {
                    foreach(var item in entity) {
                        item.SerialNumber = sequenceNumber++;
                        PartsPurchaseSettleBills.Add(item);
                    }
                    var partsPurchaseSettleBill = loadOp.Entities.First();
                    var invoiceInformation = this.CreateObjectToEdit<InvoiceInformation>();
                    invoiceInformation.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    invoiceInformation.InvoiceAmount = PartsPurchaseSettleBills.Sum(r => r.TotalSettlementAmount);
                    invoiceInformation.InvoicePurpose = (int)DcsInvoiceInformationInvoicePurpose.配件采购;
                    invoiceInformation.InvoiceCompanyId = partsPurchaseSettleBill.PartsSupplierId;
                    invoiceInformation.InvoiceCompanyCode = partsPurchaseSettleBill.PartsSupplierCode;
                    invoiceInformation.InvoiceCompanyName = partsPurchaseSettleBill.PartsSupplierName;
                    invoiceInformation.InvoiceReceiveCompanyId = partsPurchaseSettleBill.BranchId;
                    invoiceInformation.InvoiceReceiveCompanyCode = partsPurchaseSettleBill.BranchCode;
                    invoiceInformation.InvoiceReceiveCompanyName = partsPurchaseSettleBill.BranchName;
                    invoiceInformation.Type = (int)DcsInvoiceInformationType.增值税发票;
                    //源单据信息需要赋值(由于字段必填，设计要求赋0)
                    invoiceInformation.SourceId = 0;
                    invoiceInformation.SourceType = 0;
                    invoiceInformation.SourceCode = "无";
                    invoiceInformation.Status = (int)DcsInvoiceInformationStatus.已开票;
                    invoiceInformation.OwnerCompanyId = partsPurchaseSettleBill.BranchId;
                    invoiceInformation.PropertyChanged += invoiceInformation_PropertyChanged;
                }
            }, null);
        }

        private void invoiceInformation_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var invoiceInformation = this.DataContext as InvoiceInformation;
            if(invoiceInformation == null)
                return;
            switch(e.PropertyName) {
                case "TaxRate":
                case "InvoiceAmount":
                    if(invoiceInformation.TaxRate > 1 || invoiceInformation.TaxRate < 0) {
                        invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_TaxRateMustBetweenZeroAndOne, new[] {
                            "TaxRate"
                        }));
                        invoiceInformation.InvoiceTax = default(decimal);
                        return;
                    }
                    if(invoiceInformation.TaxRate != null && invoiceInformation.InvoiceAmount != null)
                        invoiceInformation.InvoiceTax = Math.Round((decimal)((invoiceInformation.InvoiceAmount / (decimal)(1 + invoiceInformation.TaxRate)) * (decimal)invoiceInformation.TaxRate), 2);
                    break;
            }
        }

        protected override void OnEditSubmitting() {
            var invoiceInformation = this.DataContext as InvoiceInformation;
            if(invoiceInformation == null)
                return;
            if(!this.PartsPurchaseSettleBills.Any()) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_DetailsIsNull);
                return;
            }
            if(string.IsNullOrEmpty(invoiceInformation.InvoiceCode)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceCodeIsNull);
                return;
            }
            if(invoiceInformation.InvoiceCode.Length > 10) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceCodeMoreThan10);
                return;
            }
            if(string.IsNullOrEmpty(invoiceInformation.InvoiceNumber)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceNumberIsNull);
                return;
            }
            if(invoiceInformation.InvoiceNumber.Length > 20) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceNumberLengthMoreThan20);
                return;
            }
            if(invoiceInformation.InvoiceTax == default(int) || invoiceInformation.InvoiceTax == null) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceTaxIsNull);
                return;
            }
            if(invoiceInformation.TaxRate == null) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_TaxRateIsNull);
                return;
            }
            if(invoiceInformation.InvoiceDate == null) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceDateIsNotNull);
                return;
            }
            var partsPurchaseSettleBillIds = this.PartsPurchaseSettleBills.Select(r => r.Id).ToArray();
            invoiceInformation.PartsSalesCategoryId = this.PartsPurchaseSettleBills.First().PartsSalesCategoryId;
            ((IEditableObject)invoiceInformation).EndEdit();
            try {
                this.DomainContext.Load(this.DomainContext.GetBranchstrategiesQuery().Where(r => r.BranchId == PartsPurchaseSettleBills.First().BranchId), LoadBehavior.RefreshCurrent, loadOpS => {
                    if(loadOpS.HasError) {
                        if(loadOpS.IsErrorHandled)
                            loadOpS.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOpS);
                        return;
                    }
                    var branchstrategy = loadOpS.Entities.SingleOrDefault();
                    //校验：分公司策略.发票金额是否允许大于采购结算单金额
                    if(branchstrategy != null && branchstrategy.IsPurchaseAmount.HasValue && (bool)branchstrategy.IsPurchaseAmount) {
                        var partsPurchaseSettleBillTotalAmount = this.PartsPurchaseSettleBills.Sum(r => r.TotalSettlementAmount);
                        if(invoiceInformation.InvoiceAmount == null || invoiceInformation.InvoiceAmount > partsPurchaseSettleBillTotalAmount) {
                            UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceFeeGreaterThan);
                            return;
                        }
                    }
                    this.DomainContext.多条配件采购结算单发票登记(invoiceInformation, partsPurchaseSettleBillIds, invokeOp => {
                        if(invokeOp.HasError) {
                            if(!invokeOp.IsErrorHandled)
                                invokeOp.MarkErrorAsHandled();
                            var error = invokeOp.ValidationErrors.FirstOrDefault();
                            if(error != null) {
                                UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage, 5);
                                DomainContext.RejectChanges();
                                return;
                            }
                            DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                            DomainContext.RejectChanges();
                            return;
                        }
                        this.NotifyEditSubmitted();
                        this.OnCustomEditSubmitted();
                    }, null);
                }, null);

            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void Reset() {
            var invoiceInformation = this.DataContext as InvoiceInformation;
            if(invoiceInformation != null && this.DomainContext.InvoiceInformations.Contains(invoiceInformation))
                this.DomainContext.InvoiceInformations.Detach(invoiceInformation);
            foreach(var item in this.PartsPurchaseSettleBills.Where(item => this.DomainContext.PartsPurchaseSettleBills.Contains(item))) {
                this.DomainContext.PartsPurchaseSettleBills.Detach(item);
            }
            this.PartsPurchaseSettleBills.Clear();
        }

        private void NotifyEditSubmitted() {
            var handler = this.EditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        public void OnCustomEditSubmitted() {
            var invoiceInformation = this.DataContext as InvoiceInformation;
            if(invoiceInformation != null) {
                if(invoiceInformation.PurchaseSettleInvoiceRels != null) {
                    foreach(var item in invoiceInformation.PurchaseSettleInvoiceRels.Where(item => this.DomainContext.PurchaseSettleInvoiceRels.Contains(item))) {
                        this.DomainContext.PurchaseSettleInvoiceRels.Detach(item);
                    }
                }
                if(this.DomainContext.InvoiceInformations.Contains(invoiceInformation))
                    this.DomainContext.InvoiceInformations.Detach(invoiceInformation);
            }
            foreach(var item in this.PartsPurchaseSettleBills.Where(item => this.DomainContext.PartsPurchaseSettleBills.Contains(item))) {
                this.DomainContext.PartsPurchaseSettleBills.Detach(item);
            }
            this.PartsPurchaseSettleBills.Clear();
        }


    }
}
