﻿
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class PartsRequisitionSettleBillDataEditView : INotifyPropertyChanged {
        private DateTime? invoiceDate;
        private readonly string[] kvNames = {
            "RequisitionSettleType","InternalAllocationBill_Type"
        };
       
        public object KvTypes {
            get {
                return this.KeyValueManager[this.kvNames[1]];
            }
        }
        private KeyValueManager keyValueManager;
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private ObservableCollection<PartsRequisitionSettleRef> partsRequisitionSettleRef;

        private ObservableCollection<PartsRequisitionSettleRef> PartsRequisitionSettleRef {
            get {
                return this.partsRequisitionSettleRef ?? (this.partsRequisitionSettleRef = new ObservableCollection<PartsRequisitionSettleRef>());
            }
        }

        public PartsRequisitionSettleBillDataEditView() {
            InitializeComponent();
            this.Receipt.Visibility = Visibility.Collapsed;
            this.Initializer.Register(this.CreateUI);
            this.Loaded += PartsRequisitionSettleBillDataEditView_Loaded;
            this.DataContextChanged += this.PartsRequisitionSettleBillDataEditView_DataContextChanged;
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData();
        }

        void PartsRequisitionSettleBillDataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.Root.Visibility = Visibility.Visible;
            this.Receipt.Visibility = Visibility.Collapsed;
            this.UnregisterButton(returnBtn);
            this.UnregisterButton(saveBtn);
            this.ShowCancelButton();
            var partsRequisitionSettleBill = this.DataContext as PartsRequisitionSettleBill;
            if(partsRequisitionSettleBill == null)
                return;
            this.SureCanUse = partsRequisitionSettleBill.PartsRequisitionSettleRefs.Any();
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private bool sureCanUse;
        private ButtonItem saveBtn, returnBtn;
        private DateTime? cutoffTime = DateTime.Now.Date;
        private ICommand searchCommand, sureCommand;
        private DataGridViewBase partsRequisitionSettleRefForEditDataGridView;
        private DataGridViewBase partsRequisitionSettleDetailForEditDataGridView;

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsRequisitionSettleBill;
            }
        }
        private void PartsRequisitionSettleBillDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsRequisitionSettleBill = this.DataContext as PartsRequisitionSettleBill;
            if(partsRequisitionSettleBill == null)
                return;
            this.SureCanUse = partsRequisitionSettleBill.PartsRequisitionSettleRefs.Any();
            partsRequisitionSettleBill.PartsRequisitionSettleRefs.EntityRemoved += PartsRequisitionSettleRefs_EntityRemoved;
        }

        void PartsRequisitionSettleRefs_EntityRemoved(object sender, EntityCollectionChangedEventArgs<PartsRequisitionSettleRef> e) {
            var partsRequisitionSettleBill = this.DataContext as PartsRequisitionSettleBill;
            if(partsRequisitionSettleBill == null)
                return;
            this.SureCanUse = partsRequisitionSettleBill.PartsRequisitionSettleRefs.Any();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsRequisitionSettleBillsWithDetailsAndRefsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                foreach(var itme in entity.PartsRequisitionSettleRefs)
                    this.PartsRequisitionSettleRef.Add(itme);
                this.SetObjectToEdit(entity);
            }, null);
        }
        public ICommand SearchCommand {
            get {
                return this.searchCommand ?? (this.searchCommand = new Core.Command.DelegateCommand(this.Search));
            }
        }

        public ICommand SureCommand {
            get {
                return this.sureCommand ?? (this.sureCommand = new Core.Command.DelegateCommand(this.Sure));
            }
        }

        //确定按钮事件
        private void Sure() {
            var partsRequisitionSettleBill = this.DataContext as PartsRequisitionSettleBill;
            if(partsRequisitionSettleBill == null)
                return;
            this.Root.Visibility = Visibility.Collapsed;
            this.Receipt.Visibility = Visibility.Visible;
            this.RegisterButton(saveBtn, true);
            this.RegisterButton(returnBtn, true);
            this.HideCancelButton();
            var inboundIds = partsRequisitionSettleBill.PartsRequisitionSettleRefs.Where(r => r.SourceType == (int)DcsPartsRequisitionSettleRefSourceType.配件入库检验单).Select(item => item.SourceId).ToArray();
            var outboundIds = partsRequisitionSettleBill.PartsRequisitionSettleRefs.Where(r => r.SourceType == (int)DcsPartsRequisitionSettleRefSourceType.配件出库单).Select(item => item.SourceId).ToArray();
            this.DomainContext.Load(this.DomainContext.查询待结算出入库明细清单Query(inboundIds, outboundIds, null), loadOp => {
                if(loadOp.HasError || loadOp.Entities == null)
                    return;
                foreach(var item in partsRequisitionSettleBill.PartsRequisitionSettleDetails) {
                    partsRequisitionSettleBill.PartsRequisitionSettleDetails.Remove(item);
                }
                var serialNumber = 1;
                foreach(var item in loadOp.Entities) {
                    var partsRequisitionSettleDetai = partsRequisitionSettleBill.PartsRequisitionSettleDetails.FirstOrDefault(r => item != null && r.SparePartId == item.SparePartId && r.SettlementPrice == item.SettlementPrice);
                    if(partsRequisitionSettleDetai != null) {
                        partsRequisitionSettleDetai.Quantity = (partsRequisitionSettleDetai.Quantity + (item.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单 ? item.Quantity : item.Quantity * -1));
                        partsRequisitionSettleDetai.SettlementAmount = partsRequisitionSettleDetai.SettlementAmount + (item.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单 ? item.SettlementAmount : item.SettlementAmount * -1);
                        continue;
                    }
                    var partsRequisitionSettleDetail = new PartsRequisitionSettleDetail();
                    partsRequisitionSettleBill.PartsRequisitionSettleDetails.Add(partsRequisitionSettleDetail);
                    partsRequisitionSettleDetail.SerialNumber = serialNumber++;
                    partsRequisitionSettleDetail.SparePartId = item.SparePartId;
                    partsRequisitionSettleDetail.SparePartName = item.SparePartName;
                    partsRequisitionSettleDetail.SparePartCode = item.SparePartCode;
                    partsRequisitionSettleDetail.SettlementAmount = item.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单 ? item.SettlementAmount : item.SettlementAmount * -1;
                    partsRequisitionSettleDetail.SettlementPrice = item.SettlementPrice;
                    partsRequisitionSettleDetail.Quantity = item.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单 ? item.Quantity : item.Quantity * -1;
                }
                partsRequisitionSettleBill.TotalSettlementAmount = partsRequisitionSettleBill.PartsRequisitionSettleDetails.Sum(item => item.SettlementAmount);
            }, null);
        }

        //查询事件
        private void Search() {
            var partsRequisitionSettleBill = this.DataContext as PartsRequisitionSettleBill;
            if(partsRequisitionSettleBill == null)
                return;
            if(partsRequisitionSettleBill.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_PartsRequisitionSettleBill_PartsSalesCategoryIdIsNotNull);
                return;
            }

            if(partsRequisitionSettleBill.DepartmentId == default(int)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_PartsRequisitionSettleBill_DepartmentIdIsNotNull);
                return;
            }

            if(CutoffTime == null) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_PartsRequisitionSettleBill_CutoffTimeIsNotNull);
                return;
            }
            if(null==partsRequisitionSettleBill.Type){
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Notification_TypeIsNull);
                return;
            }
            this.DomainContext.Load(this.DomainContext.查询待结算出入库明细Query(BaseApp.Current.CurrentUserData.EnterpriseId, partsRequisitionSettleBill.DepartmentId, null, null, partsRequisitionSettleBill.PartsSalesCategoryId, new[] {
                (int)DcsPartsInboundType.内部领入
            }, new[] {
                (int)DcsPartsOutboundType.内部领出
            }, this.CutoffTime, false, partsRequisitionSettleBill.Type), loadOp => {
                if(loadOp.HasError || loadOp.Entities == null)
                    return;
                foreach(var item in partsRequisitionSettleBill.PartsRequisitionSettleRefs) {
                    partsRequisitionSettleBill.PartsRequisitionSettleRefs.Remove(item);
                }
                foreach(var item in this.PartsRequisitionSettleRef)
                    partsRequisitionSettleBill.PartsRequisitionSettleRefs.Add(item);

                foreach(var item in loadOp.Entities) {
                    var partsRequisitionSettleRef = new PartsRequisitionSettleRef();
                    partsRequisitionSettleRef.PartsRequisitionSettleBillId = partsRequisitionSettleBill.Id;
                    partsRequisitionSettleRef.SourceId = item.BillId;
                    partsRequisitionSettleRef.SourceCode = item.BillCode;
                    partsRequisitionSettleRef.SettlementAmount = item.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单 ? item.SettlementAmount : item.SettlementAmount * -1;
                    partsRequisitionSettleRef.SourceType = item.BillType;
                    partsRequisitionSettleBill.PartsRequisitionSettleRefs.Add(partsRequisitionSettleRef);
                }
                partsRequisitionSettleBill.TotalSettlementAmount = partsRequisitionSettleBill.PartsRequisitionSettleRefs.Sum(item => item.SettlementAmount);
                this.SureCanUse = partsRequisitionSettleBill.PartsRequisitionSettleRefs.Any();
            }, null);
        }

        //截止时间
        public DateTime? CutoffTime {
            get {
                return this.cutoffTime == null ? this.cutoffTime : new DateTime(this.cutoffTime.Value.Year, this.cutoffTime.Value.Month, this.cutoffTime.Value.Day, 23, 59, 59);
            }
            set {
                this.cutoffTime = value;
                this.OnPropertyChanged("CutoffTime");
            }
        }

        public bool SureCanUse {
            get {
                return this.sureCanUse;
            }
            set {
                this.sureCanUse = value;
                this.OnPropertyChanged("SureCanUse");
            }
        }
        public DateTime? InvoiceDate
        {
            get
            {
                return this.invoiceDate;
            }
            set
            {
                this.invoiceDate = value;
                this.OnPropertyChanged("InvoiceDate");
            }
        }
        private DataGridViewBase PartsRequisitionSettleRefForEditDataGridView {
            get {
                if(this.partsRequisitionSettleRefForEditDataGridView == null) {
                    this.partsRequisitionSettleRefForEditDataGridView = DI.GetDataGridView("PartsRequisitionSettleRefForEdit");
                    this.partsRequisitionSettleRefForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsRequisitionSettleRefForEditDataGridView;
            }
        }


        private DataGridViewBase PartsRequisitionSettleDetailForEditDataGridView {
            get {
                if(this.partsRequisitionSettleDetailForEditDataGridView == null) {
                    this.partsRequisitionSettleDetailForEditDataGridView = DI.GetDataGridView("PartsRequisitionSettleDetailForEdit");
                    this.partsRequisitionSettleDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsRequisitionSettleDetailForEditDataGridView;
            }
        }

        private ObservableCollection<KeyValuePair> kvDepartmentInformations;

        public ObservableCollection<KeyValuePair> KvDepartmentInformations {
            get {
                return this.kvDepartmentInformations ?? (this.kvDepartmentInformations = new ObservableCollection<KeyValuePair>());
            }
        }

        private ObservableCollection<KeyValuePair> kvPartsSalesCategories;

        public ObservableCollection<KeyValuePair> KvPartsSalesCategories {
            get {
                return this.kvPartsSalesCategories ?? (this.kvPartsSalesCategories = new ObservableCollection<KeyValuePair>());
            }
        }

        private void CreateUI() {
            this.HideSaveButton();
            var detailEditView = new DcsDetailDataEditView();
            detailEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsSalesRtnSettlementRef), PartsPurchasingUIStrings.DataEditView_GroupTitle_PartsRequisitionSettleRef), null, this.PartsRequisitionSettleRefForEditDataGridView);
            detailEditView.SetValue(Grid.RowProperty, 1);
            detailEditView.UnregisterButton(detailEditView.InsertButton);
            this.Root.Children.Add(detailEditView);

            var detailEditView2 = new DcsDetailDataEditView();
            detailEditView2.Register(Utils.GetEntityLocalizedName(typeof(PartsSalesRtnSettlementDetail), PartsPurchasingUIStrings.DataEditView_GroupTitle_PartsRequisitionSettleDetail), null, this.PartsRequisitionSettleDetailForEditDataGridView);
            detailEditView2.SetValue(Grid.RowProperty, 2);
            detailEditView2.SetValue(Grid.ColumnSpanProperty, 6);
            detailEditView2.UnregisterButton(detailEditView2.InsertButton);
            detailEditView2.UnregisterButton(detailEditView2.DeleteButton);
            this.Receipt.Children.Add(detailEditView2);
            this.saveBtn = new ButtonItem {
                Command = new Core.Command.DelegateCommand(this.SaveCurrentData),
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/save.png", UriKind.Relative),
                Title = PartsPurchasingUIStrings.Action_Title_Save
            };
            this.returnBtn = new ButtonItem {
                Command = new Core.Command.DelegateCommand(this.ReturnCurrentData),
                Title = PartsPurchasingUIStrings.Action_Title_Return,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/return.png", UriKind.Relative)
            };

            this.DomainContext.Load(this.DomainContext.GetDepartmentInformationsQuery().Where(v => v.Status == (int)DcsMasterDataStatus.有效 && v.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvDepartmentInformations.Clear();
                foreach(var department in loadOp.Entities) {
                    this.KvDepartmentInformations.Add(new KeyValuePair {
                        Key = department.Id,
                        Value = department.Name,
                        UserObject = department
                    });
                }
            }, null);
            this.DomainContext.Load(DomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategor in loadOp.Entities)
                    this.kvPartsSalesCategories.Add(new KeyValuePair {
                        Key = partsSalesCategor.Id,
                        Value = partsSalesCategor.Name
                    });
                this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            }, null);
        }

        //保存事件
        private void SaveCurrentData() {
            if(!this.PartsRequisitionSettleDetailForEditDataGridView.CommitEdit())
                return;
            if(!this.PartsRequisitionSettleRefForEditDataGridView.CommitEdit())
                return;
            var partsRequisitionSettleBill = this.DataContext as PartsRequisitionSettleBill;
            if(partsRequisitionSettleBill == null)
                return;
            if(partsRequisitionSettleBill.PartsRequisitionSettleDetails.Count > 400) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_DetailCountMoreThan400);
                return;
            }
            if (this.InvoiceDate.HasValue)
            {
                this.InvoiceDate = new DateTime(this.InvoiceDate.Value.Year, this.InvoiceDate.Value.Month, this.InvoiceDate.Value.Day, 00, 00, 00);
                partsRequisitionSettleBill.InvoiceDate = this.InvoiceDate.Value;
            } else if(!partsRequisitionSettleBill.InvoiceDate.HasValue)
            {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_InvoiceDateIsNull);
                return;
            }

            partsRequisitionSettleBill.ValidationErrors.Clear();
            if(partsRequisitionSettleBill.TotalSettlementAmount < 0) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_TotalSettlementAmount);
                return;
            }
            foreach(var item in partsRequisitionSettleBill.PartsRequisitionSettleRefs) {
                item.ValidationErrors.Clear();
                if(!item.HasValidationErrors) {
                    ((IEditableObject)item).EndEdit();
                    if(!partsRequisitionSettleBill.PartsRequisitionSettleRefs.Contains(item))
                        partsRequisitionSettleBill.PartsRequisitionSettleRefs.Add(item);
                }
            }
            if(partsRequisitionSettleBill.HasValidationErrors || partsRequisitionSettleBill.PartsRequisitionSettleDetails.Any(e => e.HasValidationErrors) || partsRequisitionSettleBill.PartsRequisitionSettleRefs.Any(e => e.HasValidationErrors))
                return;
            if(this.EditState == DataEditState.New)
                partsRequisitionSettleBill.Code = GlobalVar.ASSIGNED_BY_SERVER;
            ((IEditableObject)partsRequisitionSettleBill).EndEdit();
            try {
                if(this.EditState == DataEditState.New) {
                    if(partsRequisitionSettleBill.Can生成配件领用结算单)
                        partsRequisitionSettleBill.生成配件领用结算单();
                } else {
                    if(partsRequisitionSettleBill.Can修改配件领用结算单)
                        partsRequisitionSettleBill.修改配件领用结算单();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();

        }

        //返回事件
        private void ReturnCurrentData() {
            var partsRequisitionSettleBill = this.DataContext as PartsRequisitionSettleBill;
            if(partsRequisitionSettleBill == null)
                return;
            this.UnregisterButton(returnBtn);
            this.UnregisterButton(saveBtn);
            this.ShowCancelButton();
            this.Root.Visibility = Visibility.Visible;
            this.Receipt.Visibility = Visibility.Collapsed;
            foreach(var item in partsRequisitionSettleBill.PartsRequisitionSettleDetails) {
                partsRequisitionSettleBill.PartsRequisitionSettleDetails.Remove(item);
            }
        }

        private void OnPropertyChanged(string property) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(property));
        }


        private void DcsComboBox_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            if(e.AddedItems == null || e.AddedItems.Count != 1)
                return;
            var temp = sender as RadComboBox;
            var partsRequisitionSettleBill = this.DataContext as PartsRequisitionSettleBill;
            if(temp != null && partsRequisitionSettleBill != null) {
                var departmentInformation = temp.SelectedItem as KeyValuePair;
                if(departmentInformation != null)
                    partsRequisitionSettleBill.DepartmentCode = ((DepartmentInformation)departmentInformation.UserObject).Code;
            }
        }

        protected override void Reset() {
            this.PartsRequisitionSettleRef.Clear();
        }
        protected override bool OnRequestCanSubmit()
        {
            this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            return true;
        }
    }
}
