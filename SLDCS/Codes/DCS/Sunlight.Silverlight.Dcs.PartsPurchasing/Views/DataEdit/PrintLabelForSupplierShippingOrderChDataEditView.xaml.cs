﻿using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Web.Entities;
using Sunlight.Silverlight.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit
{
    public partial class PrintLabelForSupplierShippingOrderChDataEditView 
    {
        public PrintLabelForSupplierShippingOrderChDataEditView()
        {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
        private void Print()
        {
            this.printLabelForPartsInboundCheckBillDataGridView.CommitEdit();
            ObservableCollection<VirtualSupplierShippingDetail> print = new ObservableCollection<VirtualSupplierShippingDetail>();
            if (this.PrintLabelForPartsInboundCheckBillDataGridView.SelectedEntities != null && this.PrintLabelForPartsInboundCheckBillDataGridView.SelectedEntities.Cast<VirtualSupplierShippingDetail>().ToArray().Count() > 0)
            {
                foreach (var item in this.PrintLabelForPartsInboundCheckBillDataGridView.SelectedEntities.Cast<VirtualSupplierShippingDetail>().ToArray())
                {
                    print.Add(item);
                }
            }
            else
            {
                print = PartsInboundPlanDetails;
            }
            var partsQuantity = print.Select(detail => detail.Id).ToList();
            var billId = print.Select(detail => detail.SupplierShippingOrderId).Distinct().ToList();
            var details = "";
            var billIds = "";
            for (var i = 0; i < partsQuantity.Count(); i++)
            {
                details += partsQuantity[i] + ",";
            }
            for (var i = 0; i < billId.Count(); i++)
            {
                billIds += billId[i] + ",";
            }

            SunlightPrinter.ShowPrinter(PartsPurchasingUIStrings.DataEditView_Text_BigLablePrint, "ReportSupplierShippingOrderLabelForCh", null, true, new Tuple<string, string>("PararPartsTags", details.ToString()), new Tuple<string, string>("SupplierShippingOrderId", billIds.ToString()));

        }
        private void CreateUI()
        {
            var dcsDetailGridView = new DcsDetailDataEditView();
            dcsDetailGridView.Register(PartsPurchasingUIStrings.DataEditView_Text_ShippingDetailsInfo, null, () => this.PrintLabelForPartsInboundCheckBillDataGridView);
            dcsDetailGridView.SetValue(Grid.RowProperty, 1);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.InsertButton);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.DeleteButton);
            this.LayoutRoot.Children.Add(dcsDetailGridView);
            this.Title = PartsPurchasingUIStrings.DataEditView_Text_PartDetailsInfo;
            this.RegisterButton(new ButtonItem
            {
                Title = PartsPurchasingUIStrings.DataEditView_Text_BigLablePrint,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/print.png", UriKind.Relative),
                Command = new DelegateCommand(this.Print)
            }, true);
            this.HideSaveButton();
            this.HideCancelButton();
        }
        public override void SetObjectToEditById(object id)
        {
            if (this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int[])id));
            else
                this.LoadEntityToEdit(((int[])id));
        }

        private void LoadEntityToEdit(int[] ids)
        {
            this.PartsInboundPlanDetails.Clear();
            this.DomainContext.Load(this.DomainContext.GetupplierShippingDetailForPrintQuery(ids), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                {
                    if (!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                if (!loadOp.Entities.Any())
                    return;
                foreach (var partsInboundPlanDetail in loadOp.Entities)
                {
                    this.PartsInboundPlanDetails.Add(partsInboundPlanDetail);
                }
            }, null);
        }
        private DataGridViewBase printLabelForPartsInboundCheckBillDataGridView;

        private DataGridViewBase PrintLabelForPartsInboundCheckBillDataGridView
        {
            get
            {
                if (this.printLabelForPartsInboundCheckBillDataGridView == null)
                {
                    this.printLabelForPartsInboundCheckBillDataGridView = DI.GetDataGridView("PrintLabelForSupplierShippingOrderForQuery");
                    this.printLabelForPartsInboundCheckBillDataGridView.DomainContext = this.DomainContext;
                    this.printLabelForPartsInboundCheckBillDataGridView.DataContext = this;
                }
                return this.printLabelForPartsInboundCheckBillDataGridView;
            }
        }
        private ObservableCollection<VirtualSupplierShippingDetail> partsInboundPlanDetails;

        public ObservableCollection<VirtualSupplierShippingDetail> PartsInboundPlanDetails
        {
            get
            {
                return partsInboundPlanDetails ?? (this.partsInboundPlanDetails = new ObservableCollection<VirtualSupplierShippingDetail>());
            }
        }
    }
}
