﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class PrintLabelForSupplierShippingOrderDataEditView {
        public PrintLabelForSupplierShippingOrderDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void Print() {
            this.PrintLabelForSupplierShippingOrderDataGridView.CommitEdit();
            BasePrintWindow printWindow1 = new SupplierShippingOrderPrintLabelPrintWindow {
                Header = PartsPurchasingUIStrings.DataEditView_Text_PartLablePrint,
                SupplierShippingDetails = SupplierShippingDetails,
            };
            printWindow1.ShowDialog();
        }

        private void PrintForOnePage() {
            this.PrintLabelForSupplierShippingOrderDataGridView.CommitEdit();
            BasePrintWindow printWindow2 = new SupplierShippingOrderPrintLabelForOnePrintWindow {
                Header = PartsPurchasingUIStrings.DataEditView_Text_PartLablePrint,
                SupplierShippingDetails = SupplierShippingDetails,
            };
            printWindow2.ShowDialog();
        }

        private void CreateUI() {
            var dcsDetailGridView = new DcsDetailDataEditView();
            dcsDetailGridView.Register(PartsPurchasingUIStrings.DataEditView_Text_SupplierShippingDetailsInfo, null, () => this.PrintLabelForSupplierShippingOrderDataGridView);
            dcsDetailGridView.SetValue(Grid.RowProperty, 0);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.InsertButton);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.DeleteButton);
            this.LayoutRoot.Children.Add(dcsDetailGridView);
            this.Title = PartsPurchasingUIStrings.DataEditView_Text_PartDetailsInfo;
            this.RegisterButton(new ButtonItem {
                Title = PartsPurchasingUIStrings.DataEditView_Text_EachOnePrint,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/print.png", UriKind.Relative),
                Command = new DelegateCommand(this.PrintForOnePage)
            }, true);
            this.RegisterButton(new ButtonItem {
                Title = PartsPurchasingUIStrings.DataEditView_Text_PrintByNumber,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/print.png", UriKind.Relative),
                Command = new DelegateCommand(this.Print)
            }, true);
            this.HideSaveButton();
            this.HideCancelButton();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int[])id));
            else
                this.LoadEntityToEdit((int[])id);
        }

        private void LoadEntityToEdit(int[] id) {
            this.SupplierShippingDetails.Clear();
            this.DomainContext.Load(this.DomainContext.GetSupplierShippingDetailByIdQuery(id), loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                if(!loadOp.Entities.Any())
                    return;
                foreach(var supplierShippingDetail in loadOp.Entities) {
                    supplierShippingDetail.PrintNumber = supplierShippingDetail.Quantity;
                    this.SupplierShippingDetails.Add(supplierShippingDetail);
                }
            }, null);
        }

        private DataGridViewBase printLabelForSupplierShippingOrderDataGridView;

        private DataGridViewBase PrintLabelForSupplierShippingOrderDataGridView {
            get {
                if(this.printLabelForSupplierShippingOrderDataGridView == null) {
                    this.printLabelForSupplierShippingOrderDataGridView = DI.GetDataGridView("PrintLabelForSupplierShippingOrder");
                    this.printLabelForSupplierShippingOrderDataGridView.DomainContext = this.DomainContext;
                    this.printLabelForSupplierShippingOrderDataGridView.DataContext = this;
                }
                return this.printLabelForSupplierShippingOrderDataGridView;
            }
        }

        private ObservableCollection<SupplierShippingDetail> supplierShippingDetails;

        public ObservableCollection<SupplierShippingDetail> SupplierShippingDetails {
            get {
                return supplierShippingDetails ?? (this.supplierShippingDetails = new ObservableCollection<SupplierShippingDetail>());
            }
        }
    }
}
