﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class StandardPartsTagPrinttingDataEditView : INotifyPropertyChanged {
        private int printNums, printNumber;
        public StandardPartsTagPrinttingDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.Loaded += StandardPartsTagPrinttingDataEditView_Loaded;
        }

        private void CreateUI() {
            this.Title = PartsPurchasingUIStrings.DataEditView_Text_PartDetailsInfo;
            this.RegisterButton(new ButtonItem {
                Title = PartsPurchasingUIStrings.DataEditView_Text_PrintLables,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/print.png", UriKind.Relative),
                Command = new DelegateCommand(this.Print)
            }, true);
            this.HideSaveButton();
            this.HideCancelButton();
        }

        private void Print() {
            if (PrintNumber == 0) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Text_NumbersMustGreaterThanZero);
                return;
            }
            SpareParts.First().PrintNumber = PrintNumber;
            SpareParts.First().PrintNums = PrintNums;
            BasePrintWindow printWindow1 = new StandardPartsTagPrinttingPrintWindow {
                Header = PartsPurchasingUIStrings.DataEditView_Text_PartLablePrintWithStandard,
                SpareParts = SpareParts
            };
            printWindow1.ShowDialog();
        }

        public override void SetObjectToEditById(object id) {
            if (this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int[])id));
            else
                this.LoadEntityToEdit((int[])id);
        }

        private void LoadEntityToEdit(int[] id) {
            this.DomainContext.Load(this.DomainContext.GetSparePartsByIdsQuery(id), loadOp => {
                if (loadOp.HasError) {
                    if (!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                if (!loadOp.Entities.Any())
                    return;
                foreach (var sparePart in loadOp.Entities) {
                    this.txtCode.Text = sparePart.Code;
                    this.txtName.Text = sparePart.Name;
                    this.txtNum.Text = sparePart.MInPackingAmount.HasValue ? sparePart.MInPackingAmount.ToString() : "1";
                    this.PrintNums = sparePart.MInPackingAmount ?? 1;
                    this.SpareParts.Add(sparePart);
                }
            }, null);
        }
        private void StandardPartsTagPrinttingDataEditView_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            txtPrintNumber.Value = null;
            txtNum.Text = string.Empty;
            this.DataContext = null;
            this.SpareParts = null;
        }
        private ObservableCollection<SparePart> spareParts;
        public ObservableCollection<SparePart> SpareParts {
            get {
                return spareParts ?? (this.spareParts = new ObservableCollection<SparePart>());
            }
            set {
                this.spareParts = value;
            }
        }

        public int PrintNums {
            get {
                return this.printNums;
            }
            set {
                this.printNums = value;
                this.OnPropertyChanged("PrintNums");
            }
        }

        public int PrintNumber {
            get {
                return this.printNumber;
            }
            set {
                this.printNumber = value;
                this.OnPropertyChanged("PrintNumber");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
