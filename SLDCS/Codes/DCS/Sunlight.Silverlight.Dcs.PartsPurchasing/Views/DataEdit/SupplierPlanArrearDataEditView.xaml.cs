﻿
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class SupplierPlanArrearDataEditView {
        private ObservableCollection<PartsSalesCategory> kvPartsSalesCategory;

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_SupplierPlanArrear;
            }
        }
        public ObservableCollection<PartsSalesCategory> KvPartsSalesCategory {
            get {
                return this.kvPartsSalesCategory ?? (this.kvPartsSalesCategory = new ObservableCollection<PartsSalesCategory>());
            }
        }

        public SupplierPlanArrearDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreatrUI);
            this.DataContextChanged += SupplierPlanArrearDataEditView_DataContextChanged;
        }

        private void SupplierPlanArrearDataEditView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var supplierPlanArrear = this.DataContext as SupplierPlanArrear;
            if(supplierPlanArrear == null)
                return;
            supplierPlanArrear.PropertyChanged -= supplierPlanArrear_PropertyChanged;
            supplierPlanArrear.PropertyChanged += supplierPlanArrear_PropertyChanged;
        }

        private void supplierPlanArrear_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            var supplierPlanArrear = this.DataContext as SupplierPlanArrear;
            if(supplierPlanArrear == null)
                return;
            switch(e.PropertyName) {
                case "PartsSalesCategoryId":
                    supplierPlanArrear.SupplierId = 0;
                    supplierPlanArrear.SupplierCompanyCode = string.Empty;
                    supplierPlanArrear.SupplierCompanyName = string.Empty;
                    supplierPlanArrear.SupplierCode = string.Empty;
                    supplierPlanArrear.PlannedAmountOwed = 0;
                    break;
            }
        }

        private void CreatrUI() {
            var queryWidow = DI.GetQueryWindow("PartsSupplier");
            queryWidow.SelectionDecided += queryWidow_SelectionDecided;
            this.popuuTextBox_PartsSalesCategory.PopupContent = queryWidow;
            this.DomainContext.Load(DomainContext.GetPartsSalesCategoriesQuery().Where(r => r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Entities == null || !loadOp.Entities.Any())
                    return;
                this.KvPartsSalesCategory.Clear();
                foreach(var entity in loadOp.Entities) {
                    this.KvPartsSalesCategory.Add(entity);
                }
            }, null);
        }

        private void queryWidow_SelectionDecided(object sender, System.EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;
            if(queryWindow.SelectedEntities == null || queryWindow.SelectedEntities.Count() != 1)
                return;
            var supplierPlanArrear = this.DataContext as SupplierPlanArrear;
            if(supplierPlanArrear == null)
                return;
            var partsSupplier = queryWindow.SelectedEntities.Cast<PartsSupplier>().First();
            supplierPlanArrear.SupplierId = partsSupplier.Id;
            supplierPlanArrear.SupplierCompanyCode = partsSupplier.Code;
            supplierPlanArrear.SupplierCompanyName = partsSupplier.Name;
            supplierPlanArrear.SupplierCode = partsSupplier.Company.SupplierCode;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(DomainContext.GetSupplierPlanArrearsQuery().Where(r => r.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                }
                var supplierPlanArrear = loadOp.Entities.SingleOrDefault();
                if(supplierPlanArrear == null)
                    return;
                this.SetObjectToEdit(supplierPlanArrear);
            }, null);
        }

        protected override void OnEditSubmitting() {
            var supplierPlanArrear = this.DataContext as SupplierPlanArrear;
            if(supplierPlanArrear == null)
                return;
            if(supplierPlanArrear.EntityState == EntityState.New) {
                if(supplierPlanArrear.Can新增供应商计划欠款)
                    supplierPlanArrear.新增供应商计划欠款();
            } else {
                if(supplierPlanArrear.Can修改供应商计划欠款)
                    supplierPlanArrear.修改供应商计划欠款();
            }
            base.OnEditSubmitting();
        }

    }
}
