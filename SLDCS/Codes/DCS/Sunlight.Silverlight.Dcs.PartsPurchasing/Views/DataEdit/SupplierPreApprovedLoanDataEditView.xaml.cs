﻿
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class SupplierPreApprovedLoanDataEditView {
        private int partsSalesCategoryId = 0;
        private ObservableCollection<PartsSalesCategory> kvPartsSalesCategory;
        public ObservableCollection<PartsSalesCategory> KvPartsSalesCategory {
            get {
                return this.kvPartsSalesCategory ?? (this.kvPartsSalesCategory = new ObservableCollection<PartsSalesCategory>());
            }
        }
        public SupplierPreApprovedLoanDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += SupplierPreApprovedLoanDataEditView_DataContextChanged;
        }


        private void SupplierPreApprovedLoanDataEditView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var supplierPreApprovedLoan = this.DataContext as SupplierPreApprovedLoan;
            if(supplierPreApprovedLoan == null)
                return;
            supplierPreApprovedLoan.PropertyChanged -= supplierPreApprovedLoan_PropertyChanged;
            supplierPreApprovedLoan.PropertyChanged += supplierPreApprovedLoan_PropertyChanged;
        }

        private void supplierPreApprovedLoan_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            var supplierPreApprovedLoan = this.DataContext as SupplierPreApprovedLoan;
            if(supplierPreApprovedLoan == null)
                return;
            switch(e.PropertyName) {
                case "PartsSalesCategoryId":
                    if(supplierPreApprovedLoan.SupplierPreAppLoanDetails.Any() && supplierPreApprovedLoan.PartsSalesCategoryId != this.partsSalesCategoryId)
                        DcsUtils.Confirm(PartsPurchasingUIStrings.DataEditView_Validation_SupplierPreApprovedLoan_DetailClear, () => {
                            var supplierPreAppLoanDetails = supplierPreApprovedLoan.SupplierPreAppLoanDetails.ToArray();
                            foreach(var supplierPreAppLoanDetail in supplierPreAppLoanDetails)
                                supplierPreApprovedLoan.SupplierPreAppLoanDetails.Remove(supplierPreAppLoanDetail);
                            supplierPreApprovedLoan.PlanArrear = default(int);
                            supplierPreApprovedLoan.ActualDebt = default(int);
                            supplierPreApprovedLoan.ApprovalAmount = default(int);
                            supplierPreApprovedLoan.ActualPaidAmount = default(int);
                            supplierPreApprovedLoan.ExceedAmount = default(int);
                            this.partsSalesCategoryId = supplierPreApprovedLoan.PartsSalesCategoryId;
                        }, () => supplierPreApprovedLoan.PartsSalesCategoryId = this.partsSalesCategoryId);
                    else {
                        this.partsSalesCategoryId = supplierPreApprovedLoan.PartsSalesCategoryId;
                    }
                    break;
            }
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetSupplierPreApprovedLoanIncludeDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                var serialNumber = 1;
                foreach(var supplierPreAppLoanDetail in entity.SupplierPreAppLoanDetails) {
                    supplierPreAppLoanDetail.SerialNumber = serialNumber;
                    serialNumber++;
                }
                this.SetObjectToEdit(entity);
            }, null);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_SupplierPreApprovedLoan;
            }
        }

        private DataGridViewBase dataGridView;

        private DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("SupplierPreAppLoanDetailForEdit");
                    this.dataGridView.DomainContext = this.DomainContext;
                }
                return this.dataGridView;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.DataGridView.CommitEdit())
                return;
            var supplierPreApprovedLoan = this.DataContext as SupplierPreApprovedLoan;
            if(supplierPreApprovedLoan == null)
                return;
            if(supplierPreApprovedLoan.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Error_SupplierPreApprovedLoanPartsSalesCategoryIsNull);
                return;
            }
            if(supplierPreApprovedLoan.EntityState == EntityState.New) {
                if(supplierPreApprovedLoan.Can新增供应商货款预批单)
                    supplierPreApprovedLoan.新增供应商货款预批单();
            } else {
                if(supplierPreApprovedLoan.Can修改供应商货款预批单)
                    supplierPreApprovedLoan.修改供应商货款预批单();
            }
            base.OnEditSubmitting();
        }

        private void CreateUI() {
            var detailView = new DcsDetailDataEditView();
            detailView.Register(PartsPurchasingUIStrings.DetailPanel_Title_SupplierPreAppLoanDetail, null, this.DataGridView);
            detailView.SetValue(Grid.ColumnProperty, 2);
            detailView.SetValue(Grid.RowSpanProperty, 7);

            detailView.UnregisterButton(detailView.DeleteButton);
            detailView.UnregisterButton(detailView.InsertButton);
            detailView.RegisterButton(new ButtonItem {
                Title = PartsPurchasingUIStrings.DataEditView_Text_BatchAdd,
                Command = new DelegateCommand(this.AddAll)
            }, true);
            this.LayoutRoot.Children.Add(detailView);

            this.DomainContext.Load(DomainContext.GetPartsSalesCategoriesQuery().Where(r => r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;

                if(loadOp.Entities == null || !loadOp.Entities.Any())
                    return;
                this.KvPartsSalesCategory.Clear();
                foreach(var entity in loadOp.Entities)
                    this.KvPartsSalesCategory.Add(entity);
            }, null);
        }

        private void AddAll() {
            var supplierPreApprovedLoan = this.DataContext as SupplierPreApprovedLoan;
            if(supplierPreApprovedLoan == null)
                return;
            if(supplierPreApprovedLoan.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditPanel_Text_SupplierPreApprovedLoan_PartsSalesCategoryIsNull);
                return;
            } this.DomainContext.Load(DomainContext.GetSupplierPreAppLoanDetailByPartsSalesCategoryIdQuery(supplierPreApprovedLoan.PartsSalesCategoryId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                }
                var supplierPreAppLoanDetails = supplierPreApprovedLoan.SupplierPreAppLoanDetails.ToArray();
                foreach(var supplierPreAppLoanDetail in supplierPreAppLoanDetails)
                    supplierPreApprovedLoan.SupplierPreAppLoanDetails.Remove(supplierPreAppLoanDetail);
                if(loadOp.Entities == null || !loadOp.Entities.Any())
                    return;
                var serialNumber = 1;
                foreach(var entity in loadOp.Entities) {
                    supplierPreApprovedLoan.SupplierPreAppLoanDetails.Add(new SupplierPreAppLoanDetail {
                        SerialNumber = serialNumber,
                        SupplierCode = entity.SupplierCode,
                        SupplierId = entity.SupplierId,
                        SupplierCompanyCode = entity.SupplierCompanyCode,
                        SupplierCompanyName = entity.SupplierCompanyName,
                        PlannedAmountOwed = entity.PlannedAmountOwed,
                        ActualDebt = entity.ActualDebt,
                        BankName = entity.BankName,
                        BankCode = entity.BankCode,
                        //根据获取信息系统自动计算：超出金额=（实际欠款金额-计划欠款金额）/10000,计算结果截取保留两位小数，结果<=0时，取0）
                        ExceedAmount = ((entity.ActualDebt - entity.PlannedAmountOwed) / 10000) <= 0 ? 0 : Math.Round(((entity.ActualDebt - entity.PlannedAmountOwed) / 10000), 2)
                    });
                    serialNumber++;
                }
                supplierPreApprovedLoan.PlanArrear = supplierPreApprovedLoan.SupplierPreAppLoanDetails.Sum(r => r.PlannedAmountOwed);
                supplierPreApprovedLoan.ActualDebt = supplierPreApprovedLoan.SupplierPreAppLoanDetails.Sum(r => r.ActualDebt);
                supplierPreApprovedLoan.ApprovalAmount = supplierPreApprovedLoan.SupplierPreAppLoanDetails.Sum(r => r.ApprovalAmount ?? 0);
                supplierPreApprovedLoan.ActualPaidAmount = supplierPreApprovedLoan.SupplierPreAppLoanDetails.Sum(r => r.ActualPaidAmount ?? 0);
                supplierPreApprovedLoan.ExceedAmount = supplierPreApprovedLoan.SupplierPreAppLoanDetails.Sum(r => r.ExceedAmount);
            }, null);
        }
        protected override void Reset() {
            this.partsSalesCategoryId = 0;
        }
    }
}
