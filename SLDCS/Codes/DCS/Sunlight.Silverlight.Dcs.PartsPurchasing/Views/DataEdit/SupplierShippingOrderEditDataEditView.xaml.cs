﻿﻿using System;
﻿using System.Collections.ObjectModel;
﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
﻿using Sunlight.Silverlight.Core.Model;
﻿using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
﻿using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class SupplierShippingOrderEditDataEditView {
        private DataGridViewBase supplierShippingDetailForEditDataGridView;
        private DataGridViewBase partsLogisticBatchItemDetailForEditDataGridView;
        private QueryWindowBase partsPurchaseOrderQueryWindow;
        private KeyValueManager keyValueManager;
        private int customBranchId;
        private readonly ObservableCollection<Branch> kvBranches = new ObservableCollection<Branch>();

        private readonly string[] kvNames = {
            "PartsShipping_Method"
        };
        public SupplierShippingOrderEditDataEditView() {
            this.InitializeComponent();
            this.DataContextChanged += this.SupplierShippingOrderDataEditView_DataContextChanged;
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.LoadData();
        }

        protected void SupplierShippingOrderDataEditView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var supplierShippingOrder = this.DataContext as SupplierShippingOrder;
            if(supplierShippingOrder == null)
                return;
            this.LoadWarehouse(supplierShippingOrder.BranchId);
            supplierShippingOrder.PropertyChanged -= this.SupplierShippingOrder_PropertyChanged;
            supplierShippingOrder.PropertyChanged += this.SupplierShippingOrder_PropertyChanged;
        }

        //分公司事件对应 触发收货仓库下拉
        private void SupplierShippingOrder_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            switch(e.PropertyName) {
                case "BranchId":
                    var supplierShippingOrder = this.DataContext as SupplierShippingOrder;
                    if(supplierShippingOrder == null)
                        return;
                    if(this.customBranchId != 0) {
                        DcsUtils.Confirm(PartsPurchasingUIStrings.DataEditView_Notification_ClearDataOnUpdateBranch, () => {
                            //清空数据
                            this.ResetSupplierShippingOrderWhithDetails(supplierShippingOrder);
                            this.customBranchId = supplierShippingOrder.BranchId;
                        }, () => {
                            supplierShippingOrder.BranchId = this.customBranchId;
                        });
                    }
                    this.ptbPartsPurchaseOrder.IsEnabled = true;
                    var selectedBranch = this.KvBranches.SingleOrDefault(branch => branch.Id == supplierShippingOrder.BranchId);
                    if(selectedBranch == null)
                        return;
                    supplierShippingOrder.BranchCode = selectedBranch.Code;
                    this.customBranchId = supplierShippingOrder.BranchId;
                    this.LoadWarehouse(supplierShippingOrder.BranchId);
                    break;
            }
        }

        private void ResetSupplierShippingOrderWhithDetails(SupplierShippingOrder supplierShippingOrder) {
            supplierShippingOrder.PartsSalesCategoryId = 0;
            supplierShippingOrder.PartsSalesCategoryName = null;
            supplierShippingOrder.PartsPurchaseOrderCode = null;
            supplierShippingOrder.PartsPurchaseOrderId = 0;
            supplierShippingOrder.ReceivingWarehouseName = null;
            supplierShippingOrder.ReceivingWarehouseId = 0;
            supplierShippingOrder.LogisticCompany = null;
            supplierShippingOrder.Driver = null;
            supplierShippingOrder.Phone = null;
            supplierShippingOrder.VehicleLicensePlate = null;
            supplierShippingOrder.DeliveryBillNumber = null;
            supplierShippingOrder.ShippingDate = null;
            supplierShippingOrder.Remark = null;
            supplierShippingOrder.ShippingMethod = 0;
            supplierShippingOrder.ReceivingAddress = null;
            supplierShippingOrder.ReceivingCompanyName = null;
            supplierShippingOrder.ReceivingCompanyId = 0;
            supplierShippingOrder.IfDirectProvision = false;
            this.PartsLogisticBatchItemDetailForEditDataGridView.CommitEdit();
            this.SupplierShippingDetailForEditDataGridView.CommitEdit();
            foreach(var item in supplierShippingOrder.PartsLogisticBatchBillDetails) {
                supplierShippingOrder.PartsLogisticBatchBillDetails.Remove(item);
            }
            foreach(var item in supplierShippingOrder.SupplierShippingDetails) {
                supplierShippingOrder.SupplierShippingDetails.Remove(item);
            }
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private QueryWindowBase PartsPurchaseOrderQueryWindow {
            get {
                if(this.partsPurchaseOrderQueryWindow == null) {
                    this.partsPurchaseOrderQueryWindow = DI.GetQueryWindow("PartsPurchaseOrder");
                    this.partsPurchaseOrderQueryWindow.Loaded += QueryWindowOrder_Loaded;
                    this.partsPurchaseOrderQueryWindow.SelectionDecided += this.QueryWindowOrder_SelectionDecided;
                }
                return this.partsPurchaseOrderQueryWindow;
            }
        }

        private DataGridViewBase SupplierShippingDetailForEditDataGridView {
            get {
                if(this.supplierShippingDetailForEditDataGridView == null) {
                    this.supplierShippingDetailForEditDataGridView = DI.GetDataGridView("SupplierShippingDetailEdit");
                    this.supplierShippingDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.supplierShippingDetailForEditDataGridView;
            }
        }

        private DataGridViewBase PartsLogisticBatchItemDetailForEditDataGridView {
            get {
                if(this.partsLogisticBatchItemDetailForEditDataGridView == null) {
                    this.partsLogisticBatchItemDetailForEditDataGridView = DI.GetDataGridView("PartsLogisticBatchItemDetailForSupplierShippingDetailEdit");
                    this.partsLogisticBatchItemDetailForEditDataGridView.DomainContext = this.DomainContext;
                    this.partsLogisticBatchItemDetailForEditDataGridView.DataContext = this.SupplierShippingDetailForEditDataGridView;
                }
                return this.partsLogisticBatchItemDetailForEditDataGridView;
            }
        }

        protected virtual void CreateUI() {
            this.ptbPartsPurchaseOrder.PopupContent = this.PartsPurchaseOrderQueryWindow;
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(SupplierShippingOrder), "SupplierShippingDetails"), null, () => this.SupplierShippingDetailForEditDataGridView);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);
            this.KvBranches.Clear();
            this.DomainContext.Load(this.DomainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.KeepCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var branch in loadOp.Entities)
                    this.KvBranches.Add(branch);
            }, null);
            cbBranch.ItemsSource = KvBranches;
            cbShippingMethod.ItemsSource = KvShippingMethods;
        }

        //加载收货仓库
        private void LoadWarehouse(int branchId) {
            if(branchId == default(int))
                return;
            this.DomainContext.Load(this.DomainContext.GetWarehousesOrderByNameQuery().Where(entity => entity.Status == (int)DcsBaseDataStatus.有效 && entity.BranchId == branchId), LoadBehavior.KeepCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                cbReceivingWarehouse.ItemsSource = loadOp.Entities;
            }, null);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetSupplierShippingOrderWithDetailsAndBatchInfoByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                //修改界面，待运量=配件采购清单.确认量-配件采购清单.发运量+供应商发运清单.发运量（本次发运单）
                foreach(var detail in entity.SupplierShippingDetails) {
                    if(entity.PartsPurchaseOrder == null || !entity.PartsPurchaseOrder.PartsPurchaseOrderDetails.Any())
                        continue;
                    var orderDetail = entity.PartsPurchaseOrder.PartsPurchaseOrderDetails.SingleOrDefault(e => e.SparePartId == detail.SparePartId);
                    if(orderDetail != null)
                        detail.PendingQuantity = orderDetail.ConfirmedAmount - (orderDetail.ShippingAmount.HasValue ? orderDetail.ShippingAmount.Value : default(int)) + detail.Quantity;
                }
                this.SetObjectToEdit(entity);
            }, null);
        }

        //添加默认查询条件
        protected void QueryWindowOrder_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            var supplierShippingOrder = this.DataContext as SupplierShippingOrder;
            if(queryWindow == null || supplierShippingOrder == null)
                return;

            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "BranchName", supplierShippingOrder.BranchName
            });
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "WarehouseName", supplierShippingOrder.ReceivingWarehouseName
            });
            var statusFilterComposite = new CompositeFilterItem();
            statusFilterComposite.LogicalOperator = LogicalOperator.Or;
            statusFilterComposite.Filters.Add(new FilterItem {
                MemberName = "Status",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = (int)DcsPartsPurchaseOrderStatus.部分确认
            });
            statusFilterComposite.Filters.Add(new FilterItem {
                MemberName = "Status",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = (int)DcsPartsPurchaseOrderStatus.确认完毕
            });
            statusFilterComposite.Filters.Add(new FilterItem {
                MemberName = "Status",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = (int)DcsPartsPurchaseOrderStatus.部分发运
            });
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", statusFilterComposite);
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common","BranchName",false
            });
        }

        protected void QueryWindowOrder_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var partsPurchaseOrder = queryWindow.SelectedEntities.Cast<PartsPurchaseOrder>().FirstOrDefault();
            if(partsPurchaseOrder == null)
                return;
            var supplierShippingOrder = this.DataContext as SupplierShippingOrder;
            if(supplierShippingOrder == null)
                return;
            //查询并添加清单
            Action addNewDetail = () => {
                supplierShippingOrder.BranchId = partsPurchaseOrder.BranchId;
                supplierShippingOrder.ReceivingWarehouseId = partsPurchaseOrder.WarehouseId;
                supplierShippingOrder.ReceivingWarehouseName = partsPurchaseOrder.WarehouseName;
                supplierShippingOrder.PartsPurchaseOrderId = partsPurchaseOrder.Id;
                supplierShippingOrder.PartsPurchaseOrderCode = partsPurchaseOrder.Code;
                supplierShippingOrder.GPMSPurOrderCode = partsPurchaseOrder.GPMSPurOrderCode;
                supplierShippingOrder.IfDirectProvision = partsPurchaseOrder.IfDirectProvision;
                supplierShippingOrder.PartsSupplierId = BaseApp.Current.CurrentUserData.EnterpriseId;
                supplierShippingOrder.PartsSupplierCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                supplierShippingOrder.PartsSupplierName = BaseApp.Current.CurrentUserData.EnterpriseName;
                supplierShippingOrder.PartsSalesCategoryId = partsPurchaseOrder.PartsSalesCategoryId;
                supplierShippingOrder.PartsSalesCategoryName = partsPurchaseOrder.PartsSalesCategoryName;
                supplierShippingOrder.ReceivingAddress = partsPurchaseOrder.ReceivingAddress;
                supplierShippingOrder.RequestedDeliveryTime = partsPurchaseOrder.RequestedDeliveryTime;
                supplierShippingOrder.PlanSource = partsPurchaseOrder.PlanSource;
                supplierShippingOrder.DirectRecWarehouseId = partsPurchaseOrder.DirectRecWarehouseId;
                supplierShippingOrder.DirectRecWarehouseName = partsPurchaseOrder.DirectRecWarehouseName;
                supplierShippingOrder.SAPPurchasePlanCode = partsPurchaseOrder.PurOrderCode;//SAP采购计划单号
                supplierShippingOrder.OriginalRequirementBillId = partsPurchaseOrder.Id;
                supplierShippingOrder.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件采购订单;
                supplierShippingOrder.OriginalRequirementBillCode = partsPurchaseOrder.Code;
                //收货单位Id与名称
                if(supplierShippingOrder.IfDirectProvision) {
                    supplierShippingOrder.ReceivingCompanyId = partsPurchaseOrder.ReceivingCompanyId;
                    supplierShippingOrder.ReceivingCompanyName = partsPurchaseOrder.ReceivingCompanyName;
                } else {
                    supplierShippingOrder.ReceivingCompanyId = partsPurchaseOrder.BranchId;
                    supplierShippingOrder.ReceivingCompanyName = partsPurchaseOrder.BranchName;
                }
                if(supplierShippingOrder.SupplierShippingDetails.Any()) {
                    var supplierShippingDetails = supplierShippingOrder.SupplierShippingDetails.ToArray();
                    var partsLogisticBatchBillDetails = supplierShippingOrder.PartsLogisticBatchBillDetails.ToArray();
                    this.PartsLogisticBatchItemDetailForEditDataGridView.CommitEdit();
                    this.SupplierShippingDetailForEditDataGridView.CommitEdit();
                    foreach(var entity in supplierShippingDetails) {
                        var batchItemDetails = partsLogisticBatchBillDetails.Where(ex => ex.PartsLogisticBatch != null).Select(ex => ex.PartsLogisticBatch).SelectMany(ex => ex.PartsLogisticBatchItemDetails).Where(ex => ex.SparePartId == entity.SparePartId);
                        foreach(var batchItemDetail in batchItemDetails) {
                            // 获取 配件物流批次 主单，删除对应配件发运清单的物流批次配件清单数据
                            var logisticBatch = this.DomainContext.PartsLogisticBatches.SingleOrDefault(ex => ex.Id == batchItemDetail.PartsLogisticBatchId);
                            if(logisticBatch != null && logisticBatch.PartsLogisticBatchItemDetails.Contains(batchItemDetail))
                                logisticBatch.PartsLogisticBatchItemDetails.Remove(batchItemDetail);
                        }
                        supplierShippingOrder.SupplierShippingDetails.Remove(entity);
                    }
                }
                this.DomainContext.Load(this.DomainContext.GetPartsPurchaseOrderDetailsQuery().Where(entity => entity.PartsPurchaseOrderId == partsPurchaseOrder.Id && ((entity.ShippingAmount.HasValue && entity.ShippingAmount.Value < entity.ConfirmedAmount) || (0 < entity.ConfirmedAmount))), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    if(supplierShippingOrder.SupplierShippingDetails.Any() && loadOp.Entities.Any()) {
                        this.SupplierShippingDetailForEditDataGridView.CommitEdit();
                        foreach(var entity in supplierShippingOrder.SupplierShippingDetails)
                            supplierShippingOrder.SupplierShippingDetails.Remove(entity);
                    }
                    var sparepartIds = loadOp.Entities.Select(entity => entity.SparePartId).ToArray();
                    this.DomainContext.Load(this.DomainContext.GetSparePartsWithPartsBranchByBranchIdAndPartIdsQuery(supplierShippingOrder.BranchId, sparepartIds), LoadBehavior.RefreshCurrent, loadOp2 => {
                        if(loadOp2.HasError) {
                            if(!loadOp2.IsErrorHandled)
                                loadOp2.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp2);
                            return;
                        }
                        foreach(var entity in loadOp.Entities) {
                            var shippingDetail = new SupplierShippingDetail {
                                SerialNumber = supplierShippingOrder.SupplierShippingDetails.Any() ? supplierShippingOrder.SupplierShippingDetails.Max(detail => detail.SerialNumber) + 1 : 1,
                                SparePartId = entity.SparePartId,
                                SparePartCode = entity.SparePartCode,
                                SparePartName = entity.SparePartName,
                                SupplierPartCode = entity.SupplierPartCode,
                                MeasureUnit = entity.MeasureUnit,
                                POCode = entity.POCode,
                                UnitPrice = entity.UnitPrice,
                                ConfirmedAmount = 0//确认量默认0
                            };
                            //弹出窗体，选择配件返回界面时，计算待运量=配件采购清单.确认量-配件采购清单.发运量
                            shippingDetail.PendingQuantity = entity.ConfirmedAmount - (entity.ShippingAmount.HasValue ? entity.ShippingAmount.Value : default(int));
                            if(shippingDetail.PendingQuantity == 0)//设计要求待运量为0 不需要显示
                                continue;
                            //然后再发运量 = 待运量
                            shippingDetail.Quantity = shippingDetail.PendingQuantity;
                            supplierShippingOrder.SupplierShippingDetails.Add(shippingDetail);
                        }
                    }, null);
                }, null);
            };

            //更换主单配件采购订单时
            if(supplierShippingOrder.PartsPurchaseOrderId == default(int)) {
                addNewDetail.Invoke();
            } else if(supplierShippingOrder.PartsPurchaseOrderId != partsPurchaseOrder.Id)
                DcsUtils.Confirm(PartsPurchasingUIStrings.DataEditPanel_Confirm_SupplierShippingOrder_ClearSupplierShippingOrderInfo, addNewDetail.Invoke);
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_SupplierShippingOrder;
            }
        }

        protected override void Reset() {
            this.customBranchId = 0;
            this.ptbPartsPurchaseOrder.IsEnabled = false;
            this.DataContext = null;
        }

        protected override void OnEditSubmitting() {
            if(!this.SupplierShippingDetailForEditDataGridView.CommitEdit())
                return;
            if(!this.PartsLogisticBatchItemDetailForEditDataGridView.CommitEdit())
                return;
            var supplierShippingOrder = this.DataContext as SupplierShippingOrder;
            if(supplierShippingOrder == null)
                return;
            supplierShippingOrder.ValidationErrors.Clear();
            foreach(var item in supplierShippingOrder.SupplierShippingDetails) {
                item.ValidationErrors.Clear();
            }

            if(!supplierShippingOrder.SupplierShippingDetails.Any()) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_SupplierShippingOrder_SupplierShippingOrderDetailIsNull);
                return;
            }

            if(supplierShippingOrder.BranchId == 0)
                supplierShippingOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_SupplierShippingOrder_BranchIdIsNull, new[] {
                    "BranchId"
                }));
            if(supplierShippingOrder.ShippingMethod == 0)
                supplierShippingOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_SupplierShippingOrder_ShippingMethodIsNull, new[] {
                    "ShippingMethod"
                }));

            if(string.IsNullOrWhiteSpace(supplierShippingOrder.PartsPurchaseOrderCode))
                supplierShippingOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_LaborHourUnitPriceGrade_PartsPurchaseOrderCodeIsNull, new[] {
                    "PartsPurchaseOrderCode"
                }));

            if(supplierShippingOrder.ReceivingWarehouseId == default(int))
                supplierShippingOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_SupplierShippingOrder_ReceivingWarehouseIdIsNull, new[] {
                    "ReceivingWarehouseId"
                }));

            if(supplierShippingOrder.ShippingDate < DateTime.Today)
                supplierShippingOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_SupplierShippingOrder_ShippingDateGreaterThanToday, new[] {
                    "ShippingDate"
                }));
            if(supplierShippingOrder.SupplierShippingDetails.Any(e => e.Quantity <= 0)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_SupplierShippingDetail_QuantityLessThanZero);
                return;
            }

            foreach(var items in supplierShippingOrder.SupplierShippingDetails.Where(e => e.PendingQuantity < e.Quantity)) {
                UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Validation_SupplierShippingDetail_QuantityIsInvalidation, items.SparePartCode));
                return;
            }

            if((from supplierShippingDetail in supplierShippingOrder.SupplierShippingDetails
                let detail = supplierShippingDetail
                select supplierShippingDetail).Any(supplierShippingDetail => supplierShippingOrder.SupplierShippingDetails.Count(item => item.SparePartId == supplierShippingDetail.SparePartId) > 1)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataGridView_Validation_SupplierShippingDetail_SparePartCodeCanNotRepeat);
                return;
            }
            //if(supplierShippingOrder.SupplierShippingDetails.Count > 200) {
            //    UIHelper.ShowNotification("清单条目数量不允许超过200条");
            //    return;
            //}
            //foreach(var supplierShippingDetail in supplierShippingOrder.SupplierShippingDetails) {
            //    var detail = supplierShippingDetail;
            //    if(supplierShippingDetail.SupplierShippingOrder.PartsLogisticBatchBillDetails.First().PartsLogisticBatch.PartsLogisticBatchItemDetails.Any(e => e.SparePartId == detail.SparePartId))
            //        supplierShippingDetail.Quantity = supplierShippingDetail.SupplierShippingOrder.PartsLogisticBatchBillDetails.First().PartsLogisticBatch.PartsLogisticBatchItemDetails.Where(e => e.SparePartId == supplierShippingDetail.SparePartId).Sum(partsLogisticBatchItemDetail => partsLogisticBatchItemDetail.OutboundAmount);
            //    if(supplierShippingOrder.SupplierShippingDetails.Count(item => item.SparePartId == supplierShippingDetail.SparePartId) > 1) {
            //        UIHelper.ShowNotification(PartsPurchasingUIStrings.DataGridView_Validation_SupplierShippingDetail_SparePartCodeCanNotRepeat);
            //        return;
            //    }
            //    if(supplierShippingDetail.Quantity <= 0)
            //        supplierShippingDetail.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_SupplierShippingDetail_QuantityLessThanZero, new[] {
            //            "Quantity"
            //        }));
            //    if(supplierShippingDetail.PendingQuantity < supplierShippingDetail.Quantity)
            //        supplierShippingDetail.ValidationErrors.Add(new ValidationResult(string.Format(PartsPurchasingUIStrings.DataEditView_Validation_SupplierShippingDetail_QuantityIsInvalidation, supplierShippingDetail.SparePartCode), new[] {
            //            "Quantity"
            //        }));
            //}

            if(supplierShippingOrder.HasValidationErrors || supplierShippingOrder.SupplierShippingDetails.Any(detail => detail.HasValidationErrors))
                return;
            ((IEditableObject)supplierShippingOrder).EndEdit();
            //foreach(var supplierShippingDetail in supplierShippingOrder.SupplierShippingDetails)
            //    ((IEditableObject)supplierShippingDetail).EndEdit();
            //try {
            if(this.EditState == DataEditState.New) {
                if(supplierShippingOrder.Can生成供应商发运单)
                    supplierShippingOrder.生成供应商发运单();
            } else {
                if(supplierShippingOrder.Can修改供应商发运单)
                    supplierShippingOrder.修改供应商发运单();
            }
            //} catch(ValidationException ex) {
            //    UIHelper.ShowAlertMessage(ex.Message);
            //    return;
            //}
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public ObservableCollection<Branch> KvBranches {
            get {
                return this.kvBranches;
            }
        }

        public ObservableCollection<KeyValuePair> KvShippingMethods {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
    }
}
