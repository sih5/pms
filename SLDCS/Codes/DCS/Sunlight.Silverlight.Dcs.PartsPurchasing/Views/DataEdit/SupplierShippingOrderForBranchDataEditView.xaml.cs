﻿using System;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class SupplierShippingOrderForBranchDataEditView {
        private DataGridViewBase supplierShippingDetailForConfirmDataGridView;

        private DataGridViewBase SupplierShippingDetailForConfirmDataGridView {
            get {
                if (this.supplierShippingDetailForConfirmDataGridView == null) {
                    this.supplierShippingDetailForConfirmDataGridView = DI.GetDataGridView("SupplierShippingDetailForConfirm");
                    this.supplierShippingDetailForConfirmDataGridView.DomainContext = this.DomainContext;
                }
                return this.supplierShippingDetailForConfirmDataGridView;
            }
        }
        public SupplierShippingOrderForBranchDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("SupplierShippingOrderForBranch"));
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var tabControl = new RadTabControl();
            tabControl.Items.Add(new RadTabItem {
                Content = this.SupplierShippingDetailForConfirmDataGridView,
                Header = Utils.GetEntityLocalizedName(typeof(SupplierShippingOrder), "SupplierShippingDetails")
            });
            tabControl.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(tabControl);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetSupplierShippingOrderWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if (loadOp.HasError) {
                    if (!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if (entity == null)
                    return;
                entity.ArrivalDate = DateTime.Now;
                foreach (var suppliershippingdetail in entity.SupplierShippingDetails) {
                    // suppliershippingdetail.Quantity = suppliershippingdetail.Quantity - (suppliershippingdetail.ConfirmedAmount.HasValue ? suppliershippingdetail.ConfirmedAmount.Value : 0);
                    suppliershippingdetail.ConfirmedAmount = suppliershippingdetail.Quantity;
                }
                //查询联系人联系电话 直供的就是销售订单联系人联系方式。非直供的就是仓库的联系人，联系方式。
                this.DomainContext.Load(this.DomainContext.getVirtualContactPersonShipQuery(id), LoadBehavior.RefreshCurrent, link =>
                {
                    if (link.HasError)
                    {
                        if (!link.IsErrorHandled)
                            link.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(link);
                        return;
                    }
                    if (link.Entities.Count()>0 )
                    {
                        var person = link.Entities.FirstOrDefault();
                        entity.ContactPerson = person.ContactPerson;
                        entity.ContactPhone = person.ContactPhone;
                    }

                }, null);
                this.SetObjectToEdit(entity);
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if (this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override string Title {
            get {
                return PartsPurchasingUIStrings.DataEditView_Title_Confirm_SupplierShippingOrderForBranch;
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {
            if (!this.supplierShippingDetailForConfirmDataGridView.CommitEdit())
                return;
            var supplierShippingOrder = this.DataContext as SupplierShippingOrder;
            if (supplierShippingOrder == null)
                return;
            if (supplierShippingOrder.ArrivalDate < supplierShippingOrder.ShippingDate) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_SupplierShippingDetail_ArrivalDateIsShippingDate);
                return;
            }
            foreach (var item in supplierShippingOrder.SupplierShippingDetails.Where(r => r.ConfirmedAmount > r.Quantity)) {
                UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Validation_SupplierShippingDetail_ConfirmedAmountLessThanAmount, item.SparePartCode));
                return;
            }
            if (supplierShippingOrder.SupplierShippingDetails.Any(r => r.ConfirmedAmount < 0) || !supplierShippingOrder.SupplierShippingDetails.Any(r => r.ConfirmedAmount > 0)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Notification_ConfirmAmountError);
                return;
            }
            supplierShippingOrder.ValidationErrors.Clear();
            ((IEditableObject)supplierShippingOrder).EndEdit();
            if (supplierShippingOrder.IfDirectProvision) {
                if (supplierShippingOrder.Can完成供应商直供发运单)
                    supplierShippingOrder.完成供应商直供发运单();
            } else {
                if (supplierShippingOrder.Can确认供应商发运单)
                    supplierShippingOrder.确认供应商发运单();
            }
            base.OnEditSubmitting();
        }
    }
}
