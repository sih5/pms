﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class SupplierShippingOrderForComfirmApproveDataEditView {
        private DataGridViewBase partsPurchasePlanOrderForApproveDataEditView;     

        private DataGridViewBase PartsPurchaseOrderDetailForEditDataGridView {
            get {
                if(this.partsPurchasePlanOrderForApproveDataEditView == null) {
                    this.partsPurchasePlanOrderForApproveDataEditView = DI.GetDataGridView("SupplierShippingOrderForComfirmForShow");
                    this.partsPurchasePlanOrderForApproveDataEditView.DomainContext = this.DomainContext;
                }
                return this.partsPurchasePlanOrderForApproveDataEditView;
            }
        }

        protected override string Title {
            get {
                return "审核供应商发运单";
            }
        }
        protected override bool OnRequestCanSubmit() {
            return true;
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetSupplierShippingOrderWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                //接收查询方法返回数据
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    DataEditPanels.FilePath = entity.Path;
                    //界面加载数据
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        private void CreateUI() {
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsPurchasePlan), "供应商发运清单" /*"PartsPurchasePlanDetails"*/), null, this.PartsPurchaseOrderDetailForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            //去掉 添加、删除 按钮
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
           

            this.Root.Children.Add(detailDataEditView);

            //附件
            this.DataEditPanels.SetValue(Grid.RowProperty, 5);
            this.DataEditPanels.SetValue(Grid.ColumnProperty, 0);
            this.DataEditPanels.Margin = new Thickness(0, 250, 0, 0);
            this.DataEditPanels.isHiddenButtons = true;
            this.DataEditPanels.HorizontalAlignment = HorizontalAlignment.Left;
            this.DataEditPanels.VerticalAlignment = VerticalAlignment.Top;
            this.DataEditPanels.SetValue(Grid.ColumnSpanProperty, 2);
            this.Root.Children.Add(DataEditPanels);

            this.RegisterButton(new ButtonItem {
                Command = new DelegateCommand(this.RejectCurrentData),
                Title = PartsPurchasingUIStrings.DataEditPanel_Title_Reject,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative)
            }, true);
        }

        private void RejectCurrentData() {
            var supplierShippingOrder = this.DataContext as SupplierShippingOrder;
            if(supplierShippingOrder == null)
                return;
            if( string.IsNullOrEmpty(supplierShippingOrder.RejectReason)) {
                UIHelper.ShowNotification("请填写审核意见");
                return;
            }
            ((IEditableObject)supplierShippingOrder).EndEdit();
            try {
                if(supplierShippingOrder.Can驳回供应商直供发运单)
                    supplierShippingOrder.驳回供应商直供发运单();
                ExecuteSerivcesMethod("驳回成功");
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
        }
        private void ExecuteSerivcesMethod(string notifyMessage) {
            DomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    DomainContext.RejectChanges();
                    return;
                }
                this.NotifyEditSubmitted();
                this.OnCustomEditSubmitted();
                UIHelper.ShowNotification(notifyMessage);
            }, null);
        }
        public void OnCustomEditSubmitted() {
            this.DataContext = null;
        }
        public new event EventHandler EditSubmitted;
        private void NotifyEditSubmitted() {
            var handler = this.EditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }
              
        protected override void OnEditSubmitting() {
            var supplierShippingOrder = this.DataContext as SupplierShippingOrder;
            if(supplierShippingOrder == null)
                return;

          //  #region 数据校验
            supplierShippingOrder.ValidationErrors.Clear();


            //调用服务端方法
            try {
                if(supplierShippingOrder.Can审核供应商直供发运单)
                    supplierShippingOrder.审核供应商直供发运单();
                ExecuteSerivcesMethod("操作成功");
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            ((IEditableObject)supplierShippingOrder).EndEdit();//结束编辑
        }

        public SupplierShippingOrderForComfirmApproveDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void Reset() {
            var partsPurchasePlan = this.DataContext as SupplierShippingOrder;
            if(this.DomainContext.SupplierShippingOrders.Contains(partsPurchasePlan))
                this.DomainContext.SupplierShippingOrders.Detach(partsPurchasePlan);
        }

        private FileUploadForPartsOuterPurchaseDataEditPanel productDataEditPanels;

        protected FileUploadForPartsOuterPurchaseDataEditPanel DataEditPanels {
            get {
                return this.productDataEditPanels ?? (this.productDataEditPanels = (FileUploadForPartsOuterPurchaseDataEditPanel)DI.GetDataEditPanel("FileUploadForPartsOuterPurchase"));
            }
        }
    }
}