﻿using System;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class SupplierShippingOrderForDirectDataEditView {
        private DataGridViewBase supplierShippingDetailForConfirmDataGridView;

        private DataGridViewBase SupplierShippingDetailForConfirmDataGridView {
            get {
                if(this.supplierShippingDetailForConfirmDataGridView == null) {
                    this.supplierShippingDetailForConfirmDataGridView = DI.GetDataGridView("SupplierShippingDetailForConfirm");
                    this.supplierShippingDetailForConfirmDataGridView.DomainContext = this.DomainContext;
                }
                return this.supplierShippingDetailForConfirmDataGridView;
            }
        }
        public SupplierShippingOrderForDirectDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("SupplierShippingOrderForEdit"));
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var tabControl = new RadTabControl();
            tabControl.Items.Add(new RadTabItem {
                Content = this.SupplierShippingDetailForConfirmDataGridView,
                Header = Utils.GetEntityLocalizedName(typeof(SupplierShippingOrder), "SupplierShippingDetails")
            });
            tabControl.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(tabControl);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetSupplierShippingOrderWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                foreach(var detail in entity.SupplierShippingDetails.ToArray()) {
                    detail.ConfirmedAmount = detail.Quantity;
                }
                entity.ArrivalDate = DateTime.Now;
                this.SetObjectToEdit(entity);
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override string Title {
            get {
                return PartsPurchasingUIStrings.DataEditView_Title_Confirm_SupplierShippingOrderForDirect;
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {
            if(!this.supplierShippingDetailForConfirmDataGridView.CommitEdit())
                return;
            var supplierShippingOrder = this.DataContext as SupplierShippingOrder;
            if(supplierShippingOrder == null)
                return;
            supplierShippingOrder.ValidationErrors.Clear();
            ((IEditableObject)supplierShippingOrder).EndEdit();

            if(supplierShippingOrder.Can确认供应商直供发运单)
                supplierShippingOrder.确认供应商直供发运单();

            base.OnEditSubmitting();
        }
    }
}