﻿using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public class SupplierShippingOrderForEditDataEditView : SupplierShippingOrderDataEditView {
        private QueryWindowBase partsPurchaseOrderQueryWindow;

        private DataGridViewBase partsLogisticBatchItemDetailForEditDataGridView;

        private DataGridViewBase supplierShippingDetailForEditDataGridView;

        private QueryWindowBase PartsPurchaseOrderQueryWindow {
            get {
                if(this.partsPurchaseOrderQueryWindow == null) {
                    this.partsPurchaseOrderQueryWindow = DI.GetQueryWindow("PartsPurchaseOrderForEdit");
                    this.partsPurchaseOrderQueryWindow.Loaded += QueryWindowOrder_Loaded;
                    this.partsPurchaseOrderQueryWindow.SelectionDecided += this.QueryWindowOrder_SelectionDecided;
                }
                return this.partsPurchaseOrderQueryWindow;
            }
        }

        private DataGridViewBase SupplierShippingDetailForEditDataGridView {
            get {
                if(this.supplierShippingDetailForEditDataGridView == null) {
                    this.supplierShippingDetailForEditDataGridView = DI.GetDataGridView("SupplierShippingDetailForEdit");
                    this.supplierShippingDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.supplierShippingDetailForEditDataGridView;
            }
        }

        private DataGridViewBase PartsLogisticBatchItemDetailForEditDataGridView {
            get {
                if(this.partsLogisticBatchItemDetailForEditDataGridView == null) {
                    this.partsLogisticBatchItemDetailForEditDataGridView = DI.GetDataGridView("PartsLogisticBatchItemDetailForSupplierShippingDetailEdit");
                    this.partsLogisticBatchItemDetailForEditDataGridView.DomainContext = this.DomainContext;
                    this.partsLogisticBatchItemDetailForEditDataGridView.DataContext = this.SupplierShippingDetailForEditDataGridView;
                }
                return this.partsLogisticBatchItemDetailForEditDataGridView;
            }
        }

        protected override void CreateUI() {
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(SupplierShippingOrder), "SupplierShippingDetails"), null, () => this.SupplierShippingDetailForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);
            var verticalLine = this.CreateVerticalLine(1);
            verticalLine.SetValue(Grid.ColumnProperty, 3);
            this.Root.Children.Add(verticalLine);
            var batchDetailDataEditView = new DcsDetailDataEditView();
            batchDetailDataEditView.Register(PartsPurchasingUIStrings.DataGridView_Title_SupplierShippingOrder_PartsLogisticBatchItemDetail, null, () => this.PartsLogisticBatchItemDetailForEditDataGridView);
            batchDetailDataEditView.SetValue(Grid.ColumnProperty, 4);
            this.Root.Children.Add(batchDetailDataEditView);
            this.KvBranches.Clear();
            this.DomainContext.Load(this.DomainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.KeepCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var branch in loadOp.Entities)
                    this.KvBranches.Add(branch);
            }, null);
            this.ptbPartsPurchaseOrder.PopupContent = this.PartsPurchaseOrderQueryWindow;


        }
    }
}
