﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.ViewModel;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.DataEdit;
using System.Collections.Generic;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class SupplierTraceCodeDataEditView : INotifyPropertyChanged {
       public event PropertyChangedEventHandler PropertyChanged;
        private string packingCode, sparePartCode, oldTraceCode;
        private DataGridViewBase pickingTaskDataGridView;

        private ObservableCollection<SupplierTraceCodeDetail> packingTasks = new ObservableCollection<SupplierTraceCodeDetail>();

        public SupplierTraceCodeDataEditView()
        {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        private void CreateUI()
        {

            this.radButtonSearch.Click += this.RadButtonSearch_Click;            
            this.gdShow.Children.Add(PickingTaskDataGridView);
            //附件
            this.DataEditPanels.SetValue(Grid.RowProperty, 5);
            this.DataEditPanels.SetValue(Grid.ColumnProperty, 0);
            this.DataEditPanels.Margin = new Thickness(0, 20, 0, 0);
            //  this.DataEditPanels.isHiddenButtons = true;
            this.DataEditPanels.HorizontalAlignment = HorizontalAlignment.Left;
            this.DataEditPanels.VerticalAlignment = VerticalAlignment.Top;
            this.DataEditPanels.SetValue(Grid.ColumnSpanProperty, 2);
            this.fileShow.Children.Add(DataEditPanels);
            DataEditPanels.FilePath =null;
        }

        private void RadButtonSearch_Click(object sender, RoutedEventArgs e)
        {
            this.PackingTasks.Clear();
            this.DomainContext.Load(this.DomainContext.getSupplierTraceCodeForAddQuery(packingCode, sparePartCode, oldTraceCode)
            , LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                {
                    if (!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }

                foreach(var item in loadOp.Entities)
                {
                    var supplierTraceCodeDetail = new SupplierTraceCodeDetail {
                        SparePartId = item.SparePartId,
                        SparePartCode = item.SparePartCode,
                        SparePartName = item.SparePartName,
                        PartsSupplierId = item.PartsSupplierId,
                        PartsSupplierCode = item.PartsSupplierCode,
                        PartsSupplierName = item.PartsSupplierName,
                        PartsPurchaseOrderId = item.PartsPurchaseOrderId,
                        PartsPurchaseOrderCode = item.PartsPurchaseOrderCode,
                        ShippingId = item.ShippingId,
                        ShippingCode = item.ShippingCode,
                        TraceProperty = item.TraceProperty,
                        OldTraceCode = item.OldTraceCode,
                        NewTraceCode = item.NewTraceCode,
                        PackingCode = item.PackingCode
                    };
                    this.packingTasks.Add(supplierTraceCodeDetail);
                }
            }, null);
        }

        private DataGridViewBase PickingTaskDataGridView
        {
            get
            {
                if (this.pickingTaskDataGridView == null)
                {
                    this.pickingTaskDataGridView = DI.GetDataGridView("SupplierTraceCodeForAdd");
                    this.pickingTaskDataGridView.DomainContext = this.DomainContext;
                    this.pickingTaskDataGridView.DataContext = this;
                }
                return this.pickingTaskDataGridView;
            }

        }


        public ObservableCollection<SupplierTraceCodeDetail> PackingTasks
        {
            get
            {
                return this.packingTasks ?? (this.packingTasks = new ObservableCollection<SupplierTraceCodeDetail>());
            }
            set
            {
                this.packingTasks = value;
                this.OnPropertyChanged("PackingTasks");
            }
        }

        protected override void OnEditSubmitting()
        {
            if (!this.PickingTaskDataGridView.CommitEdit())
                return;
            if (this.PickingTaskDataGridView.SelectedEntities == null || this.PickingTaskDataGridView.SelectedEntities.Count()==0)
            {
                UIHelper.ShowNotification("请选择包装单");
                return;
            }
            var supplierTraceCode = this.DataContext as SupplierTraceCode;
            if(supplierTraceCode == null)
                return;
            supplierTraceCode.ValidationErrors.Clear();
            var details = supplierTraceCode.SupplierTraceCodeDetails;
            foreach(var detail in details){
                supplierTraceCode.SupplierTraceCodeDetails.Remove(detail);
            }
            var supplierTraceCodeForAdds = this.PickingTaskDataGridView.SelectedEntities.Cast<SupplierTraceCodeDetail>().ToArray();
            var alldetail = new List<string>();
            foreach(var item in supplierTraceCodeForAdds) {
                if(string.IsNullOrEmpty(item.NewTraceCode) || item.NewTraceCode.Equals(item.OldTraceCode)) {
                    UIHelper.ShowNotification("请填写"+item.OldTraceCode+"新追溯码");
                    return;
                }
                alldetail.Add(item.NewTraceCode);
                var supplierTraceCodeDetail=new SupplierTraceCodeDetail{
                    SparePartId = item.SparePartId,
                    SparePartCode = item.SparePartCode,
                    SparePartName = item.SparePartName,
                    PartsSupplierId = item.PartsSupplierId,
                    PartsSupplierCode = item.PartsSupplierCode,
                    PartsSupplierName = item.PartsSupplierName,
                    PartsPurchaseOrderId = item.PartsPurchaseOrderId,
                    PartsPurchaseOrderCode = item.PartsPurchaseOrderCode,
                    ShippingId = item.ShippingId,
                    ShippingCode= item.ShippingCode,
                    TraceProperty = item.TraceProperty,
                    OldTraceCode = item.OldTraceCode,
                    NewTraceCode = item.NewTraceCode,
                    PackingCode = item.PackingCode
                };
                supplierTraceCode.SupplierTraceCodeDetails.Add(supplierTraceCodeDetail);
           }
            if(alldetail.ToArray().Length != alldetail.Distinct().ToArray().Length) {
                UIHelper.ShowNotification("新追溯码不允许相同");
                return;
            }
            supplierTraceCode.Path = DataEditPanels.FilePath;
            
            ((IEditableObject)supplierTraceCode).EndEdit();
            supplierTraceCode.ValidationErrors.Clear();
            if (this.DomainContext.IsBusy)
                return;           
            ShellViewModel.Current.IsBusy = true;          
            try {
                if(supplierTraceCode.Can新增修正单)
                    supplierTraceCode.新增修正单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            ShellViewModel.Current.IsBusy = false;
            base.OnEditSubmitting();
          
        }
        public string PackingCode
        {
            get
            {
                return this.packingCode;
            }
            set
            {
                this.packingCode = value;
                this.OnPropertyChanged("PackingCode");
            }
        }
        public string SparePartCode
        {
            get
            {
                return this.sparePartCode;
            }
            set
            {
                this.sparePartCode = value;
                this.OnPropertyChanged("SparePartCode");
            }
        }

        public string OldTraceCode
        {
            get
            {
                return this.oldTraceCode;
            }
            set
            {
                this.oldTraceCode = value;
                this.OnPropertyChanged("OldTraceCode");
            }
        }

        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }


        protected override string BusinessName {
            get {
                return "新增供应商零件永久性标识条码修正";
            }
        }
        private FileUploadForPartsOuterPurchaseDataEditPanel productDataEditPanels;

        protected FileUploadForPartsOuterPurchaseDataEditPanel DataEditPanels {
            get {
                return this.productDataEditPanels ?? (this.productDataEditPanels = (FileUploadForPartsOuterPurchaseDataEditPanel)DI.GetDataEditPanel("FileUploadForPartsOuterPurchase"));
            }
        }

    }
}
