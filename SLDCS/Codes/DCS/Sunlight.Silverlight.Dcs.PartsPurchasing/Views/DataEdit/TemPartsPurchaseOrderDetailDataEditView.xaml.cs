﻿﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Sunlight.Silverlight.ViewModel;
using System.Windows.Browser;
using System.Collections.Generic;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class TemPartsPurchaseOrderDetailDataEditView {
        private DataGridViewBase partsPurchaseOrderDetailForApproveDataGridView;

        private DataGridViewBase PartsPurchaseOrderDetailForApproveDataGridView {
            get {
                if(partsPurchaseOrderDetailForApproveDataGridView == null) {
                    this.partsPurchaseOrderDetailForApproveDataGridView = DI.GetDataGridView("TemPurchaseOrderDetail");
                    this.partsPurchaseOrderDetailForApproveDataGridView.DomainContext = this.DomainContext;
                }
                return partsPurchaseOrderDetailForApproveDataGridView;
            }
        }
        private KeyValueManager keyValueManager;
        protected KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        #region 发运方式
        private readonly string[] kvNames = {
            "TemPurchasePlanOrderShippingMethod"
        };

        private ObservableCollection<KeyValuePair> partsShippingMethods;
        public ObservableCollection<KeyValuePair> PartsShippingMethods {
            get {
                return this.partsShippingMethods ?? (this.partsShippingMethods = new ObservableCollection<KeyValuePair>());
            }
        }
        #endregion
        public TemPartsPurchaseOrderDetailDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrderDetail), PartsPurchasingUIStrings.DetailPanel_Title_PartsPurchaseOrderDetails), null, () => this.PartsPurchaseOrderDetailForApproveDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 1);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            this.Root.Children.Add(detailDataEditView);
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData(() => {
                foreach(var keyValuePair in this.KeyValueManager[this.kvNames[0]]) {
                    this.PartsShippingMethods.Add(keyValuePair);
                }
                this.cbShippingMethod.ItemsSource = PartsShippingMethods;
            });
        }
      
        protected override string Title {
            get {
                return "临时采购订单详情";
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetTemPurchaseOrdersWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;                                   
                this.SetObjectToEdit(entity);
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void Reset() {
            var partsPurchaseOrder = this.DataContext as TemPurchaseOrder;
            if(partsPurchaseOrder != null && this.DomainContext.TemPurchaseOrders.Contains(partsPurchaseOrder))
                this.DomainContext.TemPurchaseOrders.Detach(partsPurchaseOrder);
        }        
    }
}