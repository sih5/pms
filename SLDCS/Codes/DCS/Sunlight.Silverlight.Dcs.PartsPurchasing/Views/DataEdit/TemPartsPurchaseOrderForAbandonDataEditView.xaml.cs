﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class TemPartsPurchaseOrderForAbandonDataEditView  {
        public TemPartsPurchaseOrderForAbandonDataEditView() {
            InitializeComponent();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetTemPurchaseOrdersQuery().Where(t => t.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            var partsPurchaseOrder = this.DataContext as TemPurchaseOrder;
            if(partsPurchaseOrder == null)
                return;
            partsPurchaseOrder.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(partsPurchaseOrder.ForcedReason))
                partsPurchaseOrder.ValidationErrors.Add(new ValidationResult("强制完成原因不能为空", new[] {
                    "ForcedReason"
                }));

            try {
                if(partsPurchaseOrder.Can强制完成临时采购订单)
                    partsPurchaseOrder.强制完成临时采购订单();
            } catch(Exception ex) {
                UIHelper.ShowAlertMessage(ex.Message);
            }
            base.OnEditSubmitting();
        }

        protected override string Title {
            get {
                return "强制完成临时采购单";
            }
        }
    }
}
