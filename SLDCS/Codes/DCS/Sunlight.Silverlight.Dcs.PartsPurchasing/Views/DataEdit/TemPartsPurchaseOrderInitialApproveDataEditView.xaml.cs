﻿﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Sunlight.Silverlight.ViewModel;
using System.Windows.Browser;
using System.Collections.Generic;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class TemPartsPurchaseOrderInitialApproveDataEditView  {
        private DataGridViewBase partsPurchaseOrderDetailForApproveDataGridView;
        private KeyValueManager keyValueManager;
        private ObservableCollection<KeyValuePair> kvShippingMethods;
        private readonly string[] kvNames = {
            "TemPurchasePlanOrderShippingMethod"
        };
        private DataGridViewBase PartsPurchaseOrderDetailForApproveDataGridView {
            get {
                if(partsPurchaseOrderDetailForApproveDataGridView == null) {
                    this.partsPurchaseOrderDetailForApproveDataGridView = DI.GetDataGridView("TemPurchaseOrderDetail");
                    this.partsPurchaseOrderDetailForApproveDataGridView.DomainContext = this.DomainContext;
                }
                return partsPurchaseOrderDetailForApproveDataGridView;
            }
        }

        public TemPartsPurchaseOrderInitialApproveDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvNames);
        }
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private void CreateUI() {
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrderDetail), PartsPurchasingUIStrings.DetailPanel_Title_PartsPurchaseOrderDetails), null, () => this.PartsPurchaseOrderDetailForApproveDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 1);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            this.Root.Children.Add(detailDataEditView);

            this.RegisterButton(new ButtonItem {
                Command = new DelegateCommand(this.RejectCurrentData),
                Title = PartsPurchasingUIStrings.DataEditPanel_Title_Reject,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative)
            }, true);
            KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData(() => {
                foreach(var keyValuePair in this.KeyValueManager[this.kvNames[0]]) {
                    this.KvShippingMethods.Add(keyValuePair);
                }
            });
            cbShippingMethod.ItemsSource = KvShippingMethods;
        }
        private void RejectCurrentData() {
            var temPurchasePlanOrder = this.DataContext as TemPurchaseOrder;
            if(temPurchasePlanOrder == null)
                return;
            temPurchasePlanOrder.RejectReason = this.RejectReason.Text;
            if(string.IsNullOrEmpty(temPurchasePlanOrder.RejectReason)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_RejectReasonIsNull);
                return;
            }
            ((IEditableObject)temPurchasePlanOrder).EndEdit();
            try {
                if(temPurchasePlanOrder.Can驳回临时采购订单)
                    temPurchasePlanOrder.驳回临时采购订单();
                ExecuteSerivcesMethod(PartsPurchasingUIStrings.DataEditView_Notification_RejectSuccess);
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
        }
        protected override string Title {
            get {
                return "审核";
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetTemPurchaseOrdersWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;                                    
                this.SetObjectToEdit(entity);
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void Reset() {
            var partsPurchaseOrder = this.DataContext as TemPurchaseOrder;
            if(partsPurchaseOrder != null && this.DomainContext.TemPurchaseOrders.Contains(partsPurchaseOrder))
                this.DomainContext.TemPurchaseOrders.Detach(partsPurchaseOrder);
        }

        protected override void OnEditSubmitting() {
            if(!this.PartsPurchaseOrderDetailForApproveDataGridView.CommitEdit())
                return;
            var partsPurchaseOrder = this.DataContext as TemPurchaseOrder;
            if(partsPurchaseOrder == null)
                return;
            partsPurchaseOrder.ValidationErrors.Clear();
            foreach(var item in partsPurchaseOrder.TemPurchaseOrderDetails)
                item.ValidationErrors.Clear();                       
            //调用服务端方法
            try {
                if(partsPurchaseOrder.Can审核临时采购订单)
                    partsPurchaseOrder.审核临时采购订单();
                ExecuteSerivcesMethod(PartsPurchasingUIStrings.DataEditView_Notification_OperateSuccess);
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            ((IEditableObject)partsPurchaseOrder).EndEdit();//结束编辑
        }
        private void ExecuteSerivcesMethod(string notifyMessage) {
            DomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    DomainContext.RejectChanges();
                    return;
                }
                this.NotifyEditSubmitted();
                this.OnCustomEditSubmitted();
                UIHelper.ShowNotification(notifyMessage);
            }, null);
        }
        public void OnCustomEditSubmitted() {
            this.DataContext = null;
        }
        public new event EventHandler EditSubmitted;
        private void NotifyEditSubmitted() {
            var handler = this.EditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }
        public ObservableCollection<KeyValuePair> KvShippingMethods {
            get {
                return this.kvShippingMethods ?? (this.kvShippingMethods = new ObservableCollection<KeyValuePair>());
            }
        }
        protected override bool OnRequestCanSubmit() {
            return true;
        }
    }
}