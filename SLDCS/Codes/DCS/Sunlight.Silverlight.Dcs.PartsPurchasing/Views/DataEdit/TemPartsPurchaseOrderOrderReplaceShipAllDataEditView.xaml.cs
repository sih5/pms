﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using System.Windows;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using Telerik.Windows.Data;
using System.Windows.Data;
using Telerik.Windows.Controls;
using System.Windows.Input;
using System.Collections.Generic;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class TemPartsPurchaseOrderOrderReplaceShipAllDataEditView {
        private DcsDetailDataEditView partsPurchaseOrderDataEditView;
        public event EventHandler CustomEditSubmitted;

        #region 查询字段属性
        private string code, supplierPartCode, sparePartName, partsSupplierCode, orderCompanyCode, orderCompanyName;
        private DateTime? tCreateTimeForm, tCreateTimeTo;
        private int? partsPurchaseOrderTypeId;

        private string driver, deliveryBillNumber, phone, logisticCompany, vehicleLicensePlate, remark, partsSalesCategoryName, shippingMethodName, orderWarehouseName;
        private DateTime? shippingRequestedDeliveryTime, planDeliveryTime;
        private int? partsSalesCategoryId, shippingMethod;
        #endregion

        private KeyValueManager keyValueManager;
        protected KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        #region 发运方式
        private readonly string[] kvNames = {
            "TemPurchasePlanOrderShippingMethod","TemPurchasePlanOrderPlanType"
        };

        private ObservableCollection<KeyValuePair> partsShippingMethods;
        public ObservableCollection<KeyValuePair> PartsShippingMethods {
            get {
                return this.partsShippingMethods ?? (this.partsShippingMethods = new ObservableCollection<KeyValuePair>());
            }
        }
        #endregion


        public int AllowPartsPurchaseOrderSelectionChange = 0;

        //订单类型
        private ObservableCollection<KeyValuePair> kvPartsPurchaseOrderType;
        public ObservableCollection<KeyValuePair> KvPartsPurchaseOrderTypes {
            get {
                return this.kvPartsPurchaseOrderType ?? (this.kvPartsPurchaseOrderType = new ObservableCollection<KeyValuePair>());
            }
        }



        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public TemPartsPurchaseOrderOrderReplaceShipAllDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += PartsPurchaseOrderOrderReplaceShipAllDataEditView_DataContextChanged;
        }

        #region 界面事件
        private void PartsPurchaseOrderOrderReplaceShipAllDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {

            var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
            if(partsPurchaseOrder == null)
                return;
            if(this.DomainContext == null)
                this.Initializer.Register(() => {
                    if(partsPurchaseOrder.Id == default(int) && this.gdShow.Visibility == Visibility.Collapsed)
                        this.HideSaveButton();
                    else
                        this.ShowSaveButton();
                });
            else {
                if(partsPurchaseOrder.Id == default(int) && this.gdShow.Visibility == Visibility.Collapsed)
                    this.HideSaveButton();
                else
                    this.ShowSaveButton();
            }
            this.gdSearch.Visibility = Visibility.Visible;
            this.gdShow.Visibility = Visibility.Collapsed;
            partsPurchaseOrder.PropertyChanged -= partsPurchaseOrder_PropertyChanged;
            partsPurchaseOrder.PropertyChanged += partsPurchaseOrder_PropertyChanged;
        }
        void partsPurchaseOrder_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
            if(partsPurchaseOrder == null)
                return;
        }
        //确定方法
        private void RadButtondShipping_Click(object sender, RoutedEventArgs e) {
            this.OnEditSubmitting();
        }
        //查询方法 ----已经正常
        private void RadButtonSearch_Click(object sender, RoutedEventArgs e) {
            if(string.IsNullOrEmpty(this.PartsSupplierCode)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_PartsPurchaseOrder_Validation_PartsSupplier);
                return;
            }
            this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(o => o.Code == PartsSupplierCode), LoadBehavior.RefreshCurrent, loadOp1 => {
                if(loadOp1.HasError) {
                    if(!loadOp1.IsErrorHandled)
                        loadOp1.MarkErrorAsHandled();
                    DcsUtils.ShowLoadError(loadOp1);
                    return;
                }
                if(loadOp1.Entities == null || !loadOp1.Entities.Any())
                    return;

                var entityQuery = DomainContext.临时订单汇总发运查询Query(loadOp1.Entities.First().Id);
                //采购订单编号
                if(!string.IsNullOrEmpty(this.Code)) {
                    entityQuery = entityQuery.Where(ex => ex.TemPurchaseOrderCode.Contains(this.Code));
                    this.code = this.Code;
                } else {
                    this.code = "";
                }
                //计划类型
                if(this.PartsPurchaseOrderTypeId.HasValue) {
                    entityQuery = entityQuery.Where(ex => ex.OrderType == this.PartsPurchaseOrderTypeId);
                    this.partsPurchaseOrderTypeId = this.PartsPurchaseOrderTypeId;
                } else {
                    this.partsPurchaseOrderTypeId = null;
                }

                //供应商图号
                if(!string.IsNullOrEmpty(this.SupplierPartCode)) {
                    entityQuery = entityQuery.Where(ex => ex.SupplierPartCode.Contains(this.SupplierPartCode));
                    this.supplierPartCode = this.SupplierPartCode;
                } else {
                    this.supplierPartCode = "";
                }
                //订货企业名称
                if(!string.IsNullOrEmpty(this.OrderCompanyCode)) {
                    entityQuery = entityQuery.Where(ex => ex.OrderCompanyCode.Contains(this.OrderCompanyCode));
                    this.orderCompanyCode = this.OrderCompanyCode;
                } else {
                    this.orderCompanyCode = "";
                }
                //订货企业编号
                if(!string.IsNullOrEmpty(this.OrderCompanyCode)) {
                    entityQuery = entityQuery.Where(ex => ex.OrderCompanyCode.Contains(this.OrderCompanyCode));
                    this.orderCompanyCode = this.OrderCompanyCode;
                } else {
                    this.orderCompanyCode = "";
                }
                //配件名称
                if(!string.IsNullOrEmpty(this.SparePartName)) {
                    entityQuery = entityQuery.Where(ex => ex.SparePartName.Contains(this.SparePartName));
                    this.sparePartName = this.SparePartName;
                } else {
                    this.sparePartName = "";
                }

                //创建时间
                if(this.CreateTimeForm.HasValue) {
                    this.CreateTimeForm = new DateTime(this.CreateTimeForm.Value.Year, this.CreateTimeForm.Value.Month, this.CreateTimeForm.Value.Day, 0, 00, 00);
                    entityQuery = entityQuery.Where(ex => ex.CreateTime >= CreateTimeForm);
                    this.tCreateTimeForm = this.CreateTimeForm;
                } else {
                    this.tCreateTimeForm = null;
                }
                if(this.CreateTimeTo.HasValue) {
                    this.CreateTimeTo = new DateTime(this.CreateTimeTo.Value.Year, this.CreateTimeTo.Value.Month, this.CreateTimeTo.Value.Day, 23, 59, 59);
                    entityQuery = entityQuery.Where(ex => ex.CreateTime <= CreateTimeTo);
                    this.tCreateTimeTo = this.CreateTimeTo;
                } else {
                    this.tCreateTimeTo = null;
                }

                this.tmppartsPurchaseOrderDetails.Clear();
                this.DomainContext.Load(entityQuery, LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowLoadError(loadOp);
                        return;
                    }
                    if(loadOp.Entities == null || !loadOp.Entities.Any())
                        return;
                    //加载清单
                    foreach(var OrderDetail in loadOp.Entities) {
                        var partsPurchaseOrderDetail = new TemPurchaseOrderDetail();
                        partsPurchaseOrderDetail.TemPurchaseOrderCode = OrderDetail.TemPurchaseOrderCode;
                        partsPurchaseOrderDetail.TemPurchaseOrderId = OrderDetail.TemPurchaseOrderId;
                        partsPurchaseOrderDetail.TemPurchaseOrderId = OrderDetail.TemPurchaseOrderId;
                        partsPurchaseOrderDetail.SparePartId = OrderDetail.SparePartId;
                        partsPurchaseOrderDetail.SparePartCode = OrderDetail.SparePartCode;
                        partsPurchaseOrderDetail.SupplierPartCode = OrderDetail.SupplierPartCode;
                        partsPurchaseOrderDetail.SparePartName = OrderDetail.SparePartName;
                        partsPurchaseOrderDetail.MeasureUnit = OrderDetail.MeasureUnit;
                        partsPurchaseOrderDetail.PlanAmount = OrderDetail.PlanAmount;
                        partsPurchaseOrderDetail.ShippingAmount = OrderDetail.ShippingAmount;
                        partsPurchaseOrderDetail.UnShippingAmount = OrderDetail.PlanAmount - OrderDetail.ShippingAmount;
                        //判断发运清单中是否存在数据，若存在 更新配件信息的本次发运数量
                        var tmppartsPurchaseOrderDetails1 = this.lartsPurchaseOrderDetails.Where(v => v.SparePartId == OrderDetail.SparePartId && v.TemPurchaseOrderCode == OrderDetail.TemPurchaseOrderCode);
                        if(tmppartsPurchaseOrderDetails1.Any())
                            foreach(var item in tmppartsPurchaseOrderDetails1) {
                                partsPurchaseOrderDetail.UnShippingAmount = OrderDetail.PlanAmount - OrderDetail.ShippingAmount - item.UnShippingAmount;
                                item.ShippingAmount = OrderDetail.ShippingAmount;
                            }
                        //若采购清单中的数量为0 则不将信息添加到采购清单中
                        if(Convert.ToInt32(partsPurchaseOrderDetail.UnShippingAmount.ToString()) > 0) {
                            //数据添加完成后再做汇总
                            this.tmppartsPurchaseOrderDetails.Add(partsPurchaseOrderDetail);
                        }
                    }
                    if(lartsPurchaseOrderDetails.Any(r => r.UnShippingAmount == 0)) {
                        foreach(var detail in this.lartsPurchaseOrderDetails.Where(r => r.UnShippingAmount == 0).ToArray()) {
                            this.lartsPurchaseOrderDetails.Remove(detail);
                        }
                    }
                }, null);
            }, null);
        }
        #endregion

        //采购计划清单窗体
        private DataGridViewBase partsPurchaseOrderReplaceShipAllDetailDataGridView;
        private DataGridViewBase PartsPurchaseOrderReplaceShipAllDetailDataGridView {
            get {
                if(this.partsPurchaseOrderReplaceShipAllDetailDataGridView == null) {
                    this.partsPurchaseOrderReplaceShipAllDetailDataGridView = DI.GetDataGridView("TemPartsPurchaseOrderOrderReplaceShipAllDetail");
                    this.partsPurchaseOrderReplaceShipAllDetailDataGridView.SelectionChanged -= partsPurchaseOrderReplaceShipAllDetailDataGridView_SelectionChanged;
                    this.partsPurchaseOrderReplaceShipAllDetailDataGridView.SelectionChanged += partsPurchaseOrderReplaceShipAllDetailDataGridView_SelectionChanged;
                    this.partsPurchaseOrderReplaceShipAllDetailDataGridView.DataContext = this;
                    this.partsPurchaseOrderReplaceShipAllDetailDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsPurchaseOrderReplaceShipAllDetailDataGridView;
            }
        }
        //采购计划勾选数据
        private string[] PartsPurchaseOrderCodeAdds = new string[] { };
        private int[] SparePartIdAdds = new int[] { };
        private void partsPurchaseOrderReplaceShipAllDetailDataGridView_SelectionChanged(object sender, EventArgs e) {
            if(partsPurchaseOrderReplaceShipAllDetailDataGridView.SelectedEntities != null && !partsPurchaseOrderReplaceShipAllDetailDataGridView.SelectedEntities.Any())
                return;
            if(this.partsPurchaseOrderReplaceShipAllDetailDataGridView.SelectedEntities != null && this.partsPurchaseOrderReplaceShipAllDetailDataGridView.SelectedEntities.Any()) {
                PartsPurchaseOrderCodeAdds = this.partsPurchaseOrderReplaceShipAllDetailDataGridView.SelectedEntities.Cast<TemPurchaseOrderDetail>().Select(p => p.TemPurchaseOrderCode).Distinct().ToArray();
                SparePartIdAdds = this.partsPurchaseOrderReplaceShipAllDetailDataGridView.SelectedEntities.Cast<TemPurchaseOrderDetail>().Select(p => p.SparePartId.Value).Distinct().ToArray();
            }
        }

        //发运清单窗体
        private DataGridViewBase partsPurchaseOrderReplaceShipDataGridView;
        private DataGridViewBase PartsPurchaseOrderReplaceShipDataGridView {
            get {
                if(this.partsPurchaseOrderReplaceShipDataGridView == null) {
                    this.partsPurchaseOrderReplaceShipDataGridView = DI.GetDataGridView("TemPartsPurchaseOrderReplaceShippingOrderDetail");
                    this.partsPurchaseOrderReplaceShipDataGridView.SelectionChanged -= partsPurchaseOrderReplaceShipDataGridView_SelectionChanged;
                    this.partsPurchaseOrderReplaceShipDataGridView.SelectionChanged += partsPurchaseOrderReplaceShipDataGridView_SelectionChanged;
                    this.partsPurchaseOrderReplaceShipDataGridView.DataContext = this;
                    this.partsPurchaseOrderReplaceShipDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsPurchaseOrderReplaceShipDataGridView;
            }
        }

        //发运清单勾选数据
        private string[] PartsPurchaseOrderCodeDeletes = new string[] { };
        private int[] SparePartIdDeletes = new int[] { };
        private void partsPurchaseOrderReplaceShipDataGridView_SelectionChanged(object sender, EventArgs e) {
            if(partsPurchaseOrderReplaceShipDataGridView.SelectedEntities != null && !partsPurchaseOrderReplaceShipDataGridView.SelectedEntities.Any())
                return;
            if(this.partsPurchaseOrderReplaceShipDataGridView.SelectedEntities != null && this.partsPurchaseOrderReplaceShipDataGridView.SelectedEntities.Any()) {
                PartsPurchaseOrderCodeDeletes = this.partsPurchaseOrderReplaceShipDataGridView.SelectedEntities.Cast<TemPurchaseOrderDetail>().Select(p => p.TemPurchaseOrderCode).Distinct().ToArray();
                SparePartIdDeletes = this.partsPurchaseOrderReplaceShipDataGridView.SelectedEntities.Cast<TemPurchaseOrderDetail>().Select(p => p.SparePartId.Value).Distinct().ToArray();
            }
        }

        //删除清单事件
        //1.发运清单删除后 直接回到采购清单中，无需判断数量及库存，默认全部发运
        private void PartsPurchaseOrderDetails_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e) {

            if(e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove) {
                var currentRemovePartsPurchaseOrderDetails = e.OldItems.Cast<TemPurchaseOrderDetail>();
                foreach(var currentRemoveEntity in currentRemovePartsPurchaseOrderDetails) {
                    if(!this.tmppartsPurchaseOrderDetails.Any(r => r.SparePartId == currentRemoveEntity.SparePartId && r.TemPurchaseOrderCode == currentRemoveEntity.TemPurchaseOrderCode)) {
                        this.tmppartsPurchaseOrderDetails.Add(lartsPurchaseOrderDetails.First(r => r.SparePartId == currentRemoveEntity.SparePartId && r.TemPurchaseOrderCode == currentRemoveEntity.TemPurchaseOrderCode));
                    } else {
                        var tmppartsPurchaseOrderDetails1 = this.tmppartsPurchaseOrderDetails.Where(v => v.SparePartId == currentRemoveEntity.SparePartId && v.TemPurchaseOrderCode == currentRemoveEntity.TemPurchaseOrderCode);
                        if(tmppartsPurchaseOrderDetails1.Any())
                            foreach(var partsPurchaseOrderDetail in tmppartsPurchaseOrderDetails1) {
                                partsPurchaseOrderDetail.UnShippingAmount += currentRemoveEntity.UnShippingAmount;
                                partsPurchaseOrderDetail.ShippingAmount -= currentRemoveEntity.UnShippingAmount;
                            }
                    }

                }
            }
        }

        //添加处理清单
        private void ProcessInternal() {
            this.PartsPurchaseOrderReplaceShipAllDetailDataGridView.CommitEdit();
            if(tmppartsPurchaseOrderDetails == null || tmppartsPurchaseOrderDetails.Count == 0)
                return;
            int diff = 0;
            var tmppartsPurchaseOrderDetails2 = this.partsPurchaseOrderReplaceShipAllDetailDataGridView.SelectedEntities.Cast<TemPurchaseOrderDetail>();
            foreach(var selectedEntity in tmppartsPurchaseOrderDetails2) {
                if(selectedEntity.UnShippingAmount > 0) {
                    diff = 0;
                    diff = int.Parse(selectedEntity.PlanAmount.ToString()) - int.Parse(selectedEntity.ShippingAmount.ToString());
                    if(selectedEntity.UnShippingAmount > diff) {
                        UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Validation_ThisShippingAmountGreaterThan, selectedEntity.TemPurchaseOrderCode, selectedEntity.SparePartCode));
                        return;
                    }
                    var partsPurchaseOrderDetail = new TemPurchaseOrderDetail {
                        TemPurchaseOrderCode = selectedEntity.TemPurchaseOrderCode,
                        TemPurchaseOrderId = selectedEntity.TemPurchaseOrderId,
                        SparePartId = selectedEntity.SparePartId,
                        SupplierPartCode = selectedEntity.SupplierPartCode,
                        SparePartCode = selectedEntity.SparePartCode,
                        SparePartName = selectedEntity.SparePartName,
                        PlanAmount = selectedEntity.PlanAmount,
                        ShippingAmount = selectedEntity.ShippingAmount,
                        MeasureUnit = selectedEntity.MeasureUnit,
                        UnShippingAmount = selectedEntity.UnShippingAmount,//本次发运量                      
                    };
                    if(!this.lartsPurchaseOrderDetails.Any(r => r.SparePartId == selectedEntity.SparePartId && r.TemPurchaseOrderCode == selectedEntity.TemPurchaseOrderCode)) {
                        this.lartsPurchaseOrderDetails.Add(partsPurchaseOrderDetail);
                        selectedEntity.UnShippingAmount = selectedEntity.PlanAmount - (int)selectedEntity.ShippingAmount - partsPurchaseOrderDetail.UnShippingAmount;
                    } else {
                        var lartsPurchaseOrderDetails1 = this.lartsPurchaseOrderDetails.Where(v => v.SparePartId == selectedEntity.SparePartId && v.TemPurchaseOrderCode == selectedEntity.TemPurchaseOrderCode);
                        if(lartsPurchaseOrderDetails1.Any())
                            foreach(var item in lartsPurchaseOrderDetails1) {
                                item.UnShippingAmount += partsPurchaseOrderDetail.UnShippingAmount;
                                if(item.UnShippingAmount > item.PlanAmount - (int)item.ShippingAmount) {
                                    item.UnShippingAmount = item.PlanAmount - (int)item.ShippingAmount;
                                    selectedEntity.UnShippingAmount = 0;
                                } else
                                    selectedEntity.UnShippingAmount = selectedEntity.PlanAmount - (int)selectedEntity.ShippingAmount - item.UnShippingAmount;

                            }
                    }
                    this.radButtondShipping.IsEnabled = true;
                }
            }
            if(tmppartsPurchaseOrderDetails.Any(r => r.UnShippingAmount == 0)) {
                foreach(var detail in this.tmppartsPurchaseOrderDetails.Where(r => r.UnShippingAmount == 0).ToArray()) {
                    this.tmppartsPurchaseOrderDetails.Remove(detail);
                }
            }
        }

        //删除发运清单
        private void ProcessDeletenal() {
            this.PartsPurchaseOrderReplaceShipDataGridView.CommitEdit();
            if(lartsPurchaseOrderDetails == null)
                return;
            int diff = 0;
            var lartsPurchaseOrderDetails1 = this.lartsPurchaseOrderDetails.Where(r => SparePartIdDeletes.Contains(r.SparePartId.Value) && PartsPurchaseOrderCodeDeletes.Contains(r.TemPurchaseOrderCode));
            foreach(var selectedEntity in lartsPurchaseOrderDetails1) {
                if(selectedEntity.UnShippingAmount > 0) {
                    diff = 0;
                    int count = 0;//用于判断是否删除发运清单
                    diff = int.Parse(selectedEntity.PlanAmount.ToString()) - int.Parse(selectedEntity.ShippingAmount.ToString());
                    if(selectedEntity.UnShippingAmount > diff) {
                        selectedEntity.UnShippingAmount = diff;
                    }
                    var partsPurchaseOrderDetail = new TemPurchaseOrderDetail {
                        TemPurchaseOrderCode = selectedEntity.TemPurchaseOrderCode,
                        TemPurchaseOrderId = selectedEntity.TemPurchaseOrderId,
                        SparePartId = selectedEntity.SparePartId,
                        SupplierPartCode = selectedEntity.SupplierPartCode,
                        SparePartCode = selectedEntity.SparePartCode,
                        SparePartName = selectedEntity.SparePartName,
                        PlanAmount = selectedEntity.PlanAmount,
                        ShippingAmount = selectedEntity.ShippingAmount,
                        MeasureUnit = selectedEntity.MeasureUnit,
                        UnShippingAmount = selectedEntity.UnShippingAmount,//本次发运量

                    };
                    count = partsPurchaseOrderDetail.UnShippingAmount;

                    if(!this.tmppartsPurchaseOrderDetails.Any(r => r.SparePartId == selectedEntity.SparePartId && r.TemPurchaseOrderCode == selectedEntity.TemPurchaseOrderCode)) {
                        this.tmppartsPurchaseOrderDetails.Add(partsPurchaseOrderDetail);
                    } else {
                        var tmppartsPurchaseOrderDetails1 = this.tmppartsPurchaseOrderDetails.Where(v => v.SparePartId == selectedEntity.SparePartId && v.TemPurchaseOrderCode == selectedEntity.TemPurchaseOrderCode);
                        if(tmppartsPurchaseOrderDetails1.Any())
                            foreach(var item in tmppartsPurchaseOrderDetails1) {
                                item.UnShippingAmount += partsPurchaseOrderDetail.UnShippingAmount;
                            }
                    }

                }
                selectedEntity.UnShippingAmount = 0;
            }
            if(lartsPurchaseOrderDetails.Any(r => r.UnShippingAmount == 0)) {
                foreach(var detail in this.lartsPurchaseOrderDetails.Where(r => r.UnShippingAmount == 0).ToArray()) {
                    this.lartsPurchaseOrderDetails.Remove(detail);
                }
                if(string.IsNullOrEmpty(this.lartsPurchaseOrderDetails.ToString())) {
                    this.radButtondShipping.IsEnabled = false;
                }
            }
        }

        //创建清单----已经正常
        private void CreatePartsPurchaseOrdersDetailGird(Grid grid, DcsDetailDataEditView detailView, DataGridViewBase dataGridView, bool defaultVisibility) {
            detailView.UnregisterButton(detailView.InsertButton);
            detailView.UnregisterButton(detailView.DeleteButton);
            detailView.Register(PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrderDetails, null, () => dataGridView);
            detailView.RegisterButton(new ButtonItem {
                Title = PartsPurchasingUIStrings.DataEditView_Title_AddToShippingDetails,
                Command = new DelegateCommand(this.ProcessInternal)
            }, true);

            dataGridView.SetValue(MinHeightProperty, Convert.ToDouble(170));
            dataGridView.SetValue(MaxHeightProperty, Convert.ToDouble(250));
            dataGridView.SetValue(MinWidthProperty, Convert.ToDouble(800));
            dataGridView.SetValue(MaxWidthProperty, Convert.ToDouble(1000));
            detailView.SetValue(Grid.RowProperty, 3);
            detailView.SetValue(Grid.VerticalAlignmentProperty, VerticalAlignment.Top);
            if(defaultVisibility) {
                detailView.SetValue(VisibilityProperty, Visibility.Collapsed);
            }
            grid.Children.Add(detailView);
        }

        private RadTabControl radTabControl;
        private RadTabControl RadTabControl {
            get {
                return this.radTabControl ?? (this.radTabControl = new RadTabControl());
            }
        }
        //创建UI-------已经正常
        private void CreateUI() {
            #region //创建行
            var tabItemsEditGrid = new Grid();
            tabItemsEditGrid.RowDefinitions.Add(new RowDefinition {
                Height = GridLength.Auto
            });
            tabItemsEditGrid.RowDefinitions.Add(new RowDefinition {
                Height = GridLength.Auto
            });
            tabItemsEditGrid.RowDefinitions.Add(new RowDefinition {
                Height = GridLength.Auto
            });
            tabItemsEditGrid.RowDefinitions.Add(new RowDefinition {
                Height = GridLength.Auto
            });
            tabItemsEditGrid.RowDefinitions.Add(new RowDefinition {
                Height = GridLength.Auto,
                MaxHeight = 200
            });
            tabItemsEditGrid.ColumnDefinitions.Add(new ColumnDefinition {
                Width = GridLength.Auto
            });
            tabItemsEditGrid.ColumnDefinitions.Add(new ColumnDefinition {
                Width = new GridLength(10)
            });
            tabItemsEditGrid.ColumnDefinitions.Add(new ColumnDefinition {
                Width = GridLength.Auto
            });
            #endregion

            //发运方式
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData(() => {
                foreach(var keyValuePair in this.KeyValueManager[this.kvNames[0]]) {
                    this.PartsShippingMethods.Add(keyValuePair);
                }
                foreach(var keyValuePair in this.KeyValueManager[this.kvNames[1]]) {
                    this.KvPartsPurchaseOrderTypes.Add(keyValuePair);
                }
                this.txtShippingMethod.SelectedIndex = 0;//发运方式
            });

            this.radButtonSearch.Click += this.RadButtonSearch_Click;
            this.radButtondShipping.Click += this.RadButtondShipping_Click;

            this.textPartsPurchaseOrderCode.KeyDown += this.cusomerEdit_KeyDown;
            this.txtSupplierPartCode.KeyDown += this.cusomerEdit_KeyDown;
            this.txtSparePartName.KeyDown += this.cusomerEdit_KeyDown;
            //this.radButtondShipping.IsEnabled = false;
            //this.CreateTimeForm = DateTime.Now.Date.AddDays(1 - DateTime.Now.Day);
            //this.CreateTimeTo = DateTime.Now.Date;

            //配件清单界面
            this.gdShow.Children.Add(this.CreateVerticalLine(2));
            partsPurchaseOrderDataEditView = new DcsDetailDataEditView();
            CreatePartsPurchaseOrdersDetailGird(tabItemsEditGrid, partsPurchaseOrderDataEditView, this.PartsPurchaseOrderReplaceShipAllDetailDataGridView, false);

            var partsPurchaseOrderReplaceShipAllDataGridView = new DcsDetailDataEditView();
            partsPurchaseOrderReplaceShipAllDataGridView.UnregisterButton(partsPurchaseOrderReplaceShipAllDataGridView.InsertButton);
            partsPurchaseOrderReplaceShipAllDataGridView.UnregisterButton(partsPurchaseOrderReplaceShipAllDataGridView.DeleteButton);
            partsPurchaseOrderReplaceShipAllDataGridView.Register(PartsPurchasingUIStrings.DataEditView_Title_ShippingDetails, null, () => this.PartsPurchaseOrderReplaceShipDataGridView);
            partsPurchaseOrderReplaceShipAllDataGridView.RegisterButton(new ButtonItem {
                Title = PartsPurchasingUIStrings.DataEditView_Title_DeleteShippingDetails,
                Command = new DelegateCommand(this.ProcessDeletenal)
            }, true);
            partsPurchaseOrderReplaceShipAllDataGridView.SetValue(MinHeightProperty, Convert.ToDouble(170));
            partsPurchaseOrderReplaceShipAllDataGridView.SetValue(MaxHeightProperty, Convert.ToDouble(250));
            partsPurchaseOrderReplaceShipAllDataGridView.SetValue(MinWidthProperty, Convert.ToDouble(800));
            partsPurchaseOrderReplaceShipAllDataGridView.SetValue(MaxWidthProperty, Convert.ToDouble(1000));
            partsPurchaseOrderReplaceShipAllDataGridView.SetValue(Grid.RowProperty, 4);
            partsPurchaseOrderReplaceShipAllDataGridView.SetValue(Grid.ColumnSpanProperty, 3);
            tabItemsEditGrid.Children.Add(partsPurchaseOrderReplaceShipAllDataGridView);
            this.RadTabControl.Items.Add(new RadTabItem {
                Header = PartsPurchasingUIStrings.DataEditView_Title_ShippingAll,
                Content = tabItemsEditGrid
            });
            this.gdShow.Children.Add(this.RadTabControl);
            this.HideSaveButton();//隐藏确认按钮
        }
        //protected override bool OnRequestCanSubmit()
        //{
        //    return false;
        //}
        private void LoadEntityToEdit(int id) {
            this.ShowSaveButton();
            this.DomainContext.Load(this.DomainContext.GetPartsPurchaseOrdersWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.SetObjectToEdit(entity);
                this.gdSearch.Visibility = Visibility.Collapsed;
                this.gdShow.Visibility = Visibility.Collapsed;
            }, null);
        }

        protected override void OnEditSubmitting() {

            #region  //1.判断必填项
            if(this.lartsPurchaseOrderDetails == null || this.lartsPurchaseOrderDetails.Count == 0) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_ShippingDetailsIsNull);
                return;
            }

            if(this.PlanDeliveryTime == null) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_ExpectDeliveryTimeIsNull);
                return;
            } else if(this.PlanDeliveryTime < DateTime.Now) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_ExpectDeliveryTimeError);
                return;
            }
            #endregion
            try {
                //  5.多个发运单 分别调用“生成供应商发运单”方法
                this.DomainContext.汇总生成供应商临时发运单(lartsPurchaseOrderDetails.ToArray(), this.shippingMethod, this.Driver, this.DeliveryBillNumber, this.Phone, this.LogisticCompany, this.VehicleLicensePlate, this.PlanDeliveryTime, this.ShippingRemark, InvokeOp => {
                    if(InvokeOp.HasError) {
                        InvokeOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(InvokeOp);
                        return;
                    }
                    DomainContext.RejectChanges();
                    //this.NotifyEditSubmitted();
                    base.OnEditSubmitting();
                }, null);
            } catch(ValidationException ex) {
                UIHelper.ShowNotification(ex.Message);
                return;
            }
        }
        private void NotifyEditSubmitted() {
            var handler = this.CustomEditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        protected override void OnEditCancelled() {
            this.gdShow.Visibility = Visibility.Visible;
            this.gdSearch.Visibility = Visibility.Visible;
            //采购查询信息
            this.textPartsPurchaseOrderCode.Text = "";
            this.txtSupplierPartCode.Text = "";
            this.txtSparePartName.Text = "";
            this.txtPartsPurchaseOrderTypeId.Text = "";
            this.txtCreateTimeForm.DateTimeText = "";
            this.txtCreateTimeTo.DateTimeText = "";
            this.txtOrderCompanyCode.Text = "";
            this.txtOrderCompanyName.Text = "";

            //发运主单信息
            this.txtShippingMethod.Text = "";
            this.txtPhone.Text = "";
            this.txtDeliveryBillNumber.Text = "";
            this.txtDriver.Text = "";
            this.txtLogisticCompany.Text = "";
            this.txtShippingRemark.Text = "";
            this.txtPlanDeliveryTime.DateTimeText = "";
            this.txtVehicleLicensePlate.Text = "";
            //采购清单、发运清单容器清空
            this.LartsPurchaseOrderDetails.Clear();
            this.TmppartsPurchaseOrderDetails.Clear();
            base.OnEditCancelled();
        }

        protected override void OnEditSubmitted() {
            this.gdShow.Visibility = Visibility.Visible;
            this.gdSearch.Visibility = Visibility.Visible;
            //采购查询信息
            this.textPartsPurchaseOrderCode.Text = "";
            this.txtSupplierPartCode.Text = "";
            this.txtSparePartName.Text = "";
            this.txtPartsPurchaseOrderTypeId.Text = "";
            this.txtCreateTimeForm.DateTimeText = "";
            this.txtCreateTimeTo.DateTimeText = "";

            //发运主单信息
            this.txtShippingMethod.Text = "";
            this.txtPhone.Text = "";
            this.txtDeliveryBillNumber.Text = "";
            this.txtDriver.Text = "";
            this.txtLogisticCompany.Text = "";
            this.txtShippingRemark.Text = "";
            this.txtPlanDeliveryTime.DateTimeText = "";
            this.txtVehicleLicensePlate.Text = "";
            //采购清单、发运清单容器清空
            this.LartsPurchaseOrderDetails.Clear();
            this.TmppartsPurchaseOrderDetails.Clear();
            base.OnEditSubmitted();
        }
        //标题
        protected override string Title {
            get {
                return PartsPurchasingUIStrings.DataEditView_Title_SupplierShipping;
            }
        }

        #region 查询界面 字段设置 -----已经正常
        //1.采购订单号
        public string Code {
            get {
                return this.code;
            }
            set {
                this.code = value;
                this.OnPropertyChanged("Code");
            }
        }
        //2.计划类型ID
        public int? PartsPurchaseOrderTypeId {
            get {
                return this.partsPurchaseOrderTypeId;
            }
            set {
                this.partsPurchaseOrderTypeId = value;
                this.OnPropertyChanged("PartsPurchaseOrderTypeId");
            }
        }
      
        //3.供应商图号
        public string PartsSupplierCode {
            get {
                return this.partsSupplierCode;
            }
            set {
                this.partsSupplierCode = value;
                this.OnPropertyChanged("PartsSupplierCode");
            }
        }
        //3.供应商图号
        public string SupplierPartCode {
            get {
                return this.supplierPartCode;
            }
            set {
                this.supplierPartCode = value;
                this.OnPropertyChanged("SupplierPartCode");
            }
        }

        //4.配件名称
        public string SparePartName {
            get {
                return this.sparePartName;
            }
            set {
                this.sparePartName = value;
                this.OnPropertyChanged("SparePartName");
            }
        }

        //6.创建时间
        public DateTime? CreateTimeForm {
            get {
                return this.tCreateTimeForm;
            }
            set {
                this.tCreateTimeForm = value;
                this.OnPropertyChanged("CreateTimeForm");
            }
        }
        public DateTime? CreateTimeTo {
            get {
                return this.tCreateTimeTo;
            }
            set {
                this.tCreateTimeTo = value;
                this.OnPropertyChanged("CreateTimeTo");
            }
        }

        //1.品牌
        public int? PartsSalesCategoryId {
            get {
                return this.partsSalesCategoryId;
            }
            set {
                this.partsSalesCategoryId = value;
                this.OnPropertyChanged("PartsSalesCategoryId");
            }
        }
        public string PartsSalesCategoryName {
            get {
                return this.partsSalesCategoryName;
            }
            set {
                this.partsSalesCategoryName = value;
                this.OnPropertyChanged("PartsSalesCategoryName");
            }
        }

        //2.发运方式
        public int? ShippingMethod {
            get {
                return this.shippingMethod;
            }
            set {
                this.shippingMethod = value;
                this.OnPropertyChanged("ShippingMethod");
            }
        }
        public string ShippingMethodName {
            get {
                return this.shippingMethodName;
            }
            set {
                this.shippingMethodName = value;
                this.OnPropertyChanged("ShippingMethodName");
            }
        }


        //.订货仓库Name
        public string OrderWarehouseName {
            get {
                return this.orderWarehouseName;
            }
            set {
                this.orderWarehouseName = value;
                this.OnPropertyChanged("OrderWarehouseName");
            }
        }

        //3.司机
        public string Driver {
            get {
                return this.driver;
            }
            set {
                this.driver = value;
                this.OnPropertyChanged("Driver");
            }
        }
        //4.送货单号
        public string DeliveryBillNumber {
            get {
                return this.deliveryBillNumber;
            }
            set {
                this.deliveryBillNumber = value;
                this.OnPropertyChanged("DeliveryBillNumber");
            }
        }
        //5.电话
        public string Phone {
            get {
                return this.phone;
            }
            set {
                this.phone = value;
                this.OnPropertyChanged("Phone");
            }
        }
        //6.物流公司
        public string LogisticCompany {
            get {
                return this.logisticCompany;
            }
            set {
                this.logisticCompany = value;
                this.OnPropertyChanged("LogisticCompany");
            }
        }
        //7.车牌号
        public string VehicleLicensePlate {
            get {
                return this.vehicleLicensePlate;
            }
            set {
                this.vehicleLicensePlate = value;
                this.OnPropertyChanged("VehicleLicensePlate");
            }
        }
        //8.备注
        public string ShippingRemark {
            get {
                return this.remark;
            }
            set {
                this.remark = value;
                this.OnPropertyChanged("ShippingRemark");
            }
        }
        //9.要求到货时间
        public DateTime? ShippingRequestedDeliveryTime {
            get {
                return this.shippingRequestedDeliveryTime;
            }
            set {
                this.shippingRequestedDeliveryTime = value;
                this.OnPropertyChanged("ShippingRequestedDeliveryTime");
            }
        }
        //10.预计到货时间
        public DateTime? PlanDeliveryTime {
            get {
                return this.planDeliveryTime;
            }
            set {
                this.planDeliveryTime = value;
                this.OnPropertyChanged("PlanDeliveryTime");
            }
        }
        //订货单位编号
        public string OrderCompanyCode {
            get {
                return this.orderCompanyCode;
            }
            set {
                this.orderCompanyCode = value;
                this.OnPropertyChanged("OrderCompanyCode");
            }
        }  //订货单位名称
        public string OrderCompanyName {
            get {
                return this.orderCompanyName;
            }
            set {
                this.orderCompanyName = value;
                this.OnPropertyChanged("OrderCompanyName");
            }
        }
        //采购清单结构
        private ObservableCollection<TemPurchaseOrderDetail> tmppartsPurchaseOrderDetails;
        public ObservableCollection<TemPurchaseOrderDetail> TmppartsPurchaseOrderDetails {
            get {
                return this.tmppartsPurchaseOrderDetails ?? (this.tmppartsPurchaseOrderDetails = new ObservableCollection<TemPurchaseOrderDetail>());
            }
            set {
                this.tmppartsPurchaseOrderDetails = value;
                this.OnPropertyChanged("TemPurchaseOrderDetails");
            }
        }
        //发运清单结构
        private ObservableCollection<TemPurchaseOrderDetail> lartsPurchaseOrderDetails;
        public ObservableCollection<TemPurchaseOrderDetail> LartsPurchaseOrderDetails {
            get {
                return this.lartsPurchaseOrderDetails ?? (this.lartsPurchaseOrderDetails = new ObservableCollection<TemPurchaseOrderDetail>());
            }
            set {
                this.lartsPurchaseOrderDetails = value;
                this.OnPropertyChanged("TemPurchaseOrderDetails");
            }
        }
        #endregion

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void cusomerEdit_KeyDown(object sender, KeyEventArgs e) {
            if(e.Key == Key.Enter) {
                this.sparePartName = this.txtSparePartName.Text;
                this.code = this.textPartsPurchaseOrderCode.Text;
                this.supplierPartCode = this.txtSupplierPartCode.Text;
                this.orderCompanyName = this.txtOrderCompanyName.Text;
                this.orderCompanyCode = this.txtOrderCompanyCode.Text;
                if(!string.IsNullOrEmpty(this.txtPartsPurchaseOrderTypeId.Text)) {
                    this.partsPurchaseOrderTypeId = int.Parse(this.txtPartsPurchaseOrderTypeId.SelectedValue.ToString());
                }
                if(!string.IsNullOrEmpty(this.txtCreateTimeForm.DateTimeText)) {
                    this.tCreateTimeForm = DateTime.Parse(this.txtCreateTimeForm.DateTimeText);
                }
                if(!string.IsNullOrEmpty(this.txtCreateTimeTo.DateTimeText)) {
                    this.tCreateTimeTo = DateTime.Parse(this.txtCreateTimeTo.DateTimeText);
                }
                this.RadButtonSearch_Click(sender, e);
            }
        }

        private void AllWindow_Loaded(object sender, RoutedEventArgs e) {
            this.txtShippingMethod.SelectedIndex = 0;//发运方式
            this.DomainContext.Load(this.DomainContext.GetSupplierRelationUsrinfoEnterpriseQuery(), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var branchSupplierRelation in loadOp.Entities) {
                    if(!this.PlanDeliveryTime.HasValue) {
                        this.txtPlanDeliveryTime.SelectedDate = DateTime.Now.Date.AddDays(branchSupplierRelation.ArrivalCycle ?? 0);
                    }
                }
            }, null);
        }
    }
}
