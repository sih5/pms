﻿﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;


namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class TemPartsPurchaseOrderReplaceShipDataEditView{
        private DataGridViewBase supplierShippingDetailForEditDataGridView;
        private KeyValueManager keyValueManager;
        private readonly ObservableCollection<Branch> kvBranches = new ObservableCollection<Branch>();

        private readonly string[] kvNames = {
            "PartsShipping_Method"
        };

        public TemPartsPurchaseOrderReplaceShipDataEditView() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.LoadData();
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private DataGridViewBase SupplierShippingDetailForEditDataGridView {
            get {
                if(this.supplierShippingDetailForEditDataGridView == null) {
                    this.supplierShippingDetailForEditDataGridView = DI.GetDataGridView("TemSupplierShippingDetailForEdit");
                    this.supplierShippingDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.supplierShippingDetailForEditDataGridView;
            }
        }

        protected virtual void CreateUI() {
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.Register("发运清单", null, () => this.SupplierShippingDetailForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);
            cbShippingMethod.ItemsSource = KvShippingMethods;
            this.cbShippingMethod.SelectedItem = 0;//发运方式默认0
        }     
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsPurchaseOrdersWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.SetObjectToEdit(entity);
            }, null);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_SupplierShippingOrder;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.SupplierShippingDetailForEditDataGridView.CommitEdit())
                return;
            var supplierShippingOrder = this.DataContext as TemSupplierShippingOrder;
            if(supplierShippingOrder == null)
                return;
            supplierShippingOrder.ValidationErrors.Clear();
            foreach(var item in supplierShippingOrder.TemShippingOrderDetails) {
                item.ValidationErrors.Clear();
            }

            if(!supplierShippingOrder.TemShippingOrderDetails.Any()) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_SupplierShippingOrder_SupplierShippingOrderDetailIsNull);
                return;
            }        
          
            if(supplierShippingOrder.ShippingDate < DateTime.Today)
                supplierShippingOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_SupplierShippingOrder_ShippingDateGreaterThanToday, new[] {
                    "ShippingDate"
                }));
            if(supplierShippingOrder.TemShippingOrderDetails.Any(e => e.Quantity <= 0)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_SupplierShippingDetail_QuantityLessThanZero);
                return;
            }
            foreach(var items in supplierShippingOrder.TemShippingOrderDetails.Where(e => e.PendingQuantity < e.Quantity)) {
                UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Validation_SupplierShippingDetail_QuantityIsInvalidation, items.SparePartCode));
                return;
            }
            if(supplierShippingOrder.TemShippingOrderDetails.Any(supplierShippingDetail => supplierShippingOrder.TemShippingOrderDetails.Count(item => item.SparePartId == supplierShippingDetail.SparePartId) > 1)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataGridView_Validation_SupplierShippingDetail_SparePartCodeCanNotRepeat);
                return;
            }
            if(supplierShippingOrder.HasValidationErrors || supplierShippingOrder.TemShippingOrderDetails.Any(detail => detail.HasValidationErrors))
                return;
            ((IEditableObject)supplierShippingOrder).EndEdit();
            if(supplierShippingOrder.Can生成临时供应商发运单)
                supplierShippingOrder.生成临时供应商发运单();
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public ObservableCollection<Branch> KvBranches {
            get {
                return this.kvBranches;
            }
        }

        public ObservableCollection<KeyValuePair> KvShippingMethods {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

    }
}