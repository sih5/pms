﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class TemPurchaseOrderConfirmDataEditView  {
        private DataGridViewBase partsPurchaseOrderDetailForApproveDataGridView;

        private DataGridViewBase PartsPurchaseOrderDetailForApproveDataGridView {
            get {
                if(partsPurchaseOrderDetailForApproveDataGridView == null) {
                    this.partsPurchaseOrderDetailForApproveDataGridView = DI.GetDataGridView("TemPartsPurchaseOrderDetailForConfirm");
                    this.partsPurchaseOrderDetailForApproveDataGridView.DomainContext = this.DomainContext;
                }
                return partsPurchaseOrderDetailForApproveDataGridView;
            }
        }

        public TemPurchaseOrderConfirmDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("TemPartsPurchaseOrderDetailForConfirm"));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrderDetail), PartsPurchasingUIStrings.DetailPanel_Title_PartsPurchaseOrderDetails), null, () => this.PartsPurchaseOrderDetailForApproveDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 1);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            this.Root.Children.Add(detailDataEditView);
        }

        protected override string Title {
            get {
                return "代确认界面";
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetTemPurchaseOrdersWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;           
                foreach(var detail in entity.TemPurchaseOrderDetails) {
                    detail.PendingConfirmedAmount = detail.PlanAmount;
                    detail.ConfirmedAmount = detail.PendingConfirmedAmount;
                    
                }            
                this.SetObjectToEdit(entity);
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void Reset() {
            var partsPurchaseOrder = this.DataContext as TemPurchaseOrder;
            if(partsPurchaseOrder != null && this.DomainContext.TemPurchaseOrders.Contains(partsPurchaseOrder))
                this.DomainContext.TemPurchaseOrders.Detach(partsPurchaseOrder);
        }

        protected override void OnEditSubmitting() {
            if(!this.PartsPurchaseOrderDetailForApproveDataGridView.CommitEdit())
                return;
            var partsPurchaseOrder = this.DataContext as TemPurchaseOrder;
            if(partsPurchaseOrder == null)
                return;
            partsPurchaseOrder.ValidationErrors.Clear();
            foreach(var item in partsPurchaseOrder.TemPurchaseOrderDetails)
                item.ValidationErrors.Clear();
            if(!partsPurchaseOrder.TemPurchaseOrderDetails.Any()) {
                UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrderDetail_DetailIsNotNull));
                return;
            }
            foreach(var relation in partsPurchaseOrder.TemPurchaseOrderDetails.Where(e => e.ConfirmedAmount < 0 || e.ConfirmedAmount > e.PendingConfirmedAmount)) {
                UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrderDetail_ConfirmedAmountLessThanOrderAmount, relation.SparePartCode));
                return;
            }
            //确认量不等于计划量，未填写短供原因，不可保存
            foreach(var relation in partsPurchaseOrder.TemPurchaseOrderDetails.Where(e => !e.ConfirmedAmount.Equals(e.PendingConfirmedAmount) && (!e.ShortSupReason.HasValue || e.ShortSupReason == default(int)))) {
                UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Validation_ShortSupReasonIsNull, relation.SparePartCode));
                return;
            }           
            var sparepartIds = partsPurchaseOrder.TemPurchaseOrderDetails.Select(r => r.SparePartId).ToArray();

            //调用服务端方法
            try {
                if(partsPurchaseOrder.Can确认临时采购订单)
                    partsPurchaseOrder.确认临时采购订单();
                ExecuteSerivcesMethod(PartsPurchasingUIStrings.DataEditView_Notification_OperateSuccess);
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            ((IEditableObject)partsPurchaseOrder).EndEdit();//结束编辑
        }
        private void ExecuteSerivcesMethod(string notifyMessage) {
            DomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
             //       DomainContext.RejectChanges();
                    return;
                }
                this.NotifyEditSubmitted();
                this.OnCustomEditSubmitted();
                UIHelper.ShowNotification(notifyMessage);
            }, null);
        }
        public void OnCustomEditSubmitted() {
            this.DataContext = null;
        }
        public new event EventHandler EditSubmitted;
        private void NotifyEditSubmitted() {
            var handler = this.EditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }
    }
}