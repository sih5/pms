﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Sunlight.Silverlight.ViewModel;
using System.Windows.Browser;
using System.Collections.Generic;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class TemPurchasePlanOrderConfirmDataEditView  {
       private DataGridViewBase partsPurchasePlanDetailForEditDataGridView;
        private ObservableCollection<KeyValuePair> kvPlanTypes;
        private KeyValueManager keyValueManager;
        private ObservableCollection<KeyValuePair> kvShippingMethods;
        private ObservableCollection<KeyValuePair> kvFreightTypes;

        private readonly string[] kvNames = {
           "TemPurchasePlanOrderPlanType","TemPurchasePlanOrderShippingMethod","TemPurchasePlanOrderFreightType"
        };

        private DataGridViewBase PartsPurchasePlanDetailForEditDataGridView {
            get {
                if(this.partsPurchasePlanDetailForEditDataGridView == null) {
                    this.partsPurchasePlanDetailForEditDataGridView = DI.GetDataGridView("TemPurchasePlanOrderDetailforDetail");
                    this.partsPurchasePlanDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsPurchasePlanDetailForEditDataGridView;
            }
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        //新增、修改界面 默认 标题
        protected override string BusinessName {
            get {
                return PartsPurchasingUIStrings.Action_Title_Confirm;
            }
        }       

        public ObservableCollection<KeyValuePair> KvShippingMethods {
            get {
                return this.kvShippingMethods ?? (this.kvShippingMethods = new ObservableCollection<KeyValuePair>());
            }
        }



        public ObservableCollection<KeyValuePair> KvPlanTypes {
            get {
                return this.kvPlanTypes ?? (this.kvPlanTypes = new ObservableCollection<KeyValuePair>());
            }
        }
        public ObservableCollection<KeyValuePair> KvFreightTypes {
            get {
                return this.kvFreightTypes ?? (this.kvFreightTypes = new ObservableCollection<KeyValuePair>());
            }
        }
        protected override bool OnRequestCanSubmit() {
            return true;
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetTemPurchasePlansWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    DataEditPanels.FilePath = entity.Path;
                    if(entity.CustomerType == (int)DCSTemPurchasePlanOrderCustomerType.中心库) {
                          this.blockReturnWarehouse.Visibility = Visibility.Visible;
                        this.Cbwarehouse.Visibility = Visibility.Visible;
                    }
                    this.SetObjectToEdit(entity);
                }
            }, null);


        }
        public TemPurchasePlanOrderConfirmDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += PartsPurchaseOrderDataEditView_DataContextChanged;
            this.KeyValueManager.LoadData();
        }
        private void PartsPurchaseOrderDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var temPurchasePlanOrder = this.DataContext as TemPurchasePlanOrder;
            if(temPurchasePlanOrder == null)
                return;
            temPurchasePlanOrder.PropertyChanged -= this.partsPurchasePlan_PropertyChanged;
            temPurchasePlanOrder.PropertyChanged += this.partsPurchasePlan_PropertyChanged;

        }
        private void partsPurchasePlan_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var temPurchasePlanOrder = this.DataContext as TemPurchasePlanOrder;
            if(temPurchasePlanOrder == null || temPurchasePlanOrder.ReceiveAddress != null)
                return;
            if(temPurchasePlanOrder.CustomerType == (int)DCSTemPurchasePlanOrderCustomerType.中心库) {
                this.blockReturnWarehouse.Visibility = Visibility.Visible;
                this.Cbwarehouse.Visibility = Visibility.Visible;
            }
        }
        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }

        /// <summary>
        /// 文件上传成功后，调用该方法进行上传后的处理。不支持异步调用
        /// <param>上传文件名 <name>fileName</name> </param>
        /// </summary>
        protected Action<string> UploadFileSuccessedProcessing;

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }

        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage(PartsPurchasingUIStrings.Upload_File_Tips);
                    e.Handled = true;
                }
            }
        }

        private void ExcelServiceClient_Import临时采购订单确认导入供应商Completed(object sender, Import临时采购订单确认导入供应商CompletedEventArgs e) {
            var temPurchasePlanOrder = this.DataContext as TemPurchasePlanOrder;
            if(temPurchasePlanOrder == null)
                return;
            var detailList = temPurchasePlanOrder.TemPurchasePlanOrderDetails.ToList();          
            if(e.rightData != null && e.rightData.Length > 0) {
                foreach(var data in detailList) {
                    var tem = e.rightData.Where(t => t.SparePartCode == data.SparePartCode).FirstOrDefault();
                    if(tem!=null) {
                        data.SparePartId = tem.SparePartId;
                        data.SparePartCode = tem.SparePartCode;
                        data.SparePartName = tem.SparePartName;
                        data.ReferenceCode = tem.ReferenceCode;
                        data.SuplierId = tem.SuplierId;
                        data.SuplierCode = tem.SuplierCode;
                        data.SuplierName = tem.SuplierName;
                        data.SupplierPartCode = tem.SupplierPartCode;
                        if(data.SparePartCode.StartsWith("W") || data.SparePartCode.StartsWith("w")) {
                            data.SupplierPartCode = tem.ReferenceCode;
                        } else {
                            data.SupplierPartCode = tem.SparePartCode;
                        }
                    }                   
                }
            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowNotification(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));

        }

        //本地缓存，记录修改前的配件销售订单类型Id
        //private int oldPartsPurchasePlanTypeId;

        private string strFileName;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private RadUpload uploader;
        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        var temPurchasePlanOrder = this.DataContext as TemPurchasePlanOrder;
                        if(temPurchasePlanOrder == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.Import临时采购订单确认导入供应商Async(e.HandlerData.CustomData["Path"].ToString());
                        this.excelServiceClient.Import临时采购订单确认导入供应商Completed -= ExcelServiceClient_Import临时采购订单确认导入供应商Completed;
                        this.excelServiceClient.Import临时采购订单确认导入供应商Completed += ExcelServiceClient_Import临时采购订单确认导入供应商Completed;
                    };
                }
                return this.uploader;
            }
        }

        private void ShowFileDialog() {
            var temPurchasePlanOrder = this.DataContext as TemPurchasePlanOrder;
            if(temPurchasePlanOrder == null)
                return;
            temPurchasePlanOrder.ValidationErrors.Clear();
            this.Uploader.ShowFileDialog();
        }

        private void CreateUI() {
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(TemPurchasePlanOrder), PartsPurchasingUIStrings.DetailPanel_Title_TemPartsPurchasePlanDetail /*"PartsPurchasePlanDetails"*/), null, this.PartsPurchasePlanDetailForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);                      
            this.Root.Children.Add(detailDataEditView);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData(() => {
                foreach(var keyValuePair in this.KeyValueManager[this.kvNames[0]]) {
                    this.KvPlanTypes.Add(keyValuePair);
                }
                foreach(var keyValuePair in this.KeyValueManager[this.kvNames[2]]) {
                    this.KvFreightTypes.Add(keyValuePair);
                }
                foreach(var keyValuePair in this.KeyValueManager[this.kvNames[1]]) {
                    this.KvShippingMethods.Add(keyValuePair);
                }
            });
            comPlanType.ItemsSource = KvPlanTypes;
            CbShippingMethod.ItemsSource = KvShippingMethods;
            comFreightType.ItemsSource = KvFreightTypes;
            //导入、导出
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsPurchasingUIStrings.Action_Title_Import,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                Command = new DelegateCommand(this.ShowFileDialog)
            });
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsPurchasingUIStrings.Upload_File_Export_Model,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
              //  Command = this.ExportTemplateCommand
                Command = new DelegateCommand(this.ExportDetails)
            });
            //附件
            this.DataEditPanels.SetValue(Grid.RowProperty, 5);
            this.DataEditPanels.SetValue(Grid.ColumnProperty, 0);
            this.DataEditPanels.Margin = new Thickness(0, 420, 0, 0);
            this.DataEditPanels.HorizontalAlignment = HorizontalAlignment.Left;
            this.DataEditPanels.VerticalAlignment = VerticalAlignment.Top;
            this.DataEditPanels.SetValue(Grid.ColumnSpanProperty, 2);
            this.DataEditPanels.isHiddenButtons = true;
            this.Root.Children.Add(DataEditPanels);
            this.RegisterButton(new ButtonItem {
                Command = new DelegateCommand(this.RejectCurrentData),
                Title = PartsPurchasingUIStrings.DataEditPanel_Title_Reject,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative)
            }, true);
        }
      //  private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private void ExportDetails() {
            var temPurchasePlanOrder = this.DataContext as TemPurchasePlanOrder;
            if(temPurchasePlanOrder == null)
                return;
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.导出临时采购计划确认时的清单Async(temPurchasePlanOrder.Id);
            this.excelServiceClient.导出临时采购计划确认时的清单Completed -= excelServiceClient_导出临时采购计划确认时的清单Completed;
            this.excelServiceClient.导出临时采购计划确认时的清单Completed += excelServiceClient_导出临时采购计划确认时的清单Completed;
        }

        private void excelServiceClient_导出临时采购计划确认时的清单Completed(object sender, 导出临时采购计划确认时的清单CompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.filename));
        }
        private ICommand exportFileCommand;

        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }
        //导出抬头
        private const string EXPORT_DATA_FILE_NAME = "导出临时配件计划采购清单供应商模板.xlsx";
        private void InitializeCommand() {
            this.exportFileCommand = new DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                    new ImportTemplateColumn {
                        Name = PartsPurchasingUIStrings.DataEditView_Text_SparePartCode,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = PartsPurchasingUIStrings.DataEditView_ImportTemplate_SparePartName
                    },
                    new ImportTemplateColumn {
                        Name = PartsPurchasingUIStrings.DataEditView_ImportTemplate_SupplierCode,
                         IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = PartsPurchasingUIStrings.DataEditView_ImportTemplate_SupplierName,
                    }
                };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private void RejectCurrentData() {
            var temPurchasePlanOrder = this.DataContext as TemPurchasePlanOrder;
            if(temPurchasePlanOrder == null)
                return;
            temPurchasePlanOrder.RejectReason = this.RejectReason.Text;
            if(string.IsNullOrEmpty(temPurchasePlanOrder.RejectReason)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_RejectReasonIsNull);
                return;
            }
            ((IEditableObject)temPurchasePlanOrder).EndEdit();
            try {
                if(temPurchasePlanOrder.Can驳回临时采购计划单)
                    temPurchasePlanOrder.驳回临时采购计划单();
                ExecuteSerivcesMethod(PartsPurchasingUIStrings.DataEditView_Notification_RejectSuccess);
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
        }
        protected override void OnEditSubmitting() {
            var temPurchasePlanOrder = this.DataContext as TemPurchasePlanOrder;
            if(temPurchasePlanOrder == null)
                return;
            foreach(var item in temPurchasePlanOrder.TemPurchasePlanOrderDetails){
                if(item.SuplierId==null) {
                    UIHelper.ShowNotification(item.SparePartCode+PartsPurchasingUIStrings.DataEditView_Validation_PartConPurchasePlan_ActualSupplierCodeIsNull);
                    return;
                }
                ((IEditableObject)item).EndEdit();//结束编辑
            }
            temPurchasePlanOrder.ValidationErrors.Clear();
            temPurchasePlanOrder.RejectReason = null;

            //调用服务端方法
            try {
                if(temPurchasePlanOrder.Can确认临时采购计划单)
                    temPurchasePlanOrder.确认临时采购计划单();
                ExecuteSerivcesMethod(PartsPurchasingUIStrings.DataEditView_Notification_OperateSuccess);
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            ((IEditableObject)temPurchasePlanOrder).EndEdit();//结束编辑
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            DomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    DomainContext.RejectChanges();
                    return;
                }
                this.NotifyEditSubmitted();
                this.OnCustomEditSubmitted();
                UIHelper.ShowNotification(notifyMessage);
            }, null);
        }
        public void OnCustomEditSubmitted() {
            this.DataContext = null;
        }
        public new event EventHandler EditSubmitted;
        private void NotifyEditSubmitted() {
            var handler = this.EditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }
        private FileUploadForPartsOuterPurchaseDataEditPanel productDataEditPanels;

        public FileUploadForPartsOuterPurchaseDataEditPanel DataEditPanels {
            get {
                return this.productDataEditPanels ?? (this.productDataEditPanels = (FileUploadForPartsOuterPurchaseDataEditPanel)DI.GetDataEditPanel("FileUploadForPartsOuterPurchase"));
            }
        }
    }
}