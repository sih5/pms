﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Sunlight.Silverlight.ViewModel;
using System.Windows.Browser;
using System.Collections.Generic;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class TemPurchasePlanOrderDataEditView {
        private DataGridViewBase partsPurchasePlanDetailForEditDataGridView;
        private ObservableCollection<KeyValuePair> kvPlanTypes;
        private KeyValueManager keyValueManager;
        private ObservableCollection<KeyValuePair> kvShippingMethods;
        private ObservableCollection<KeyValuePair> kvFreightTypes;

        private readonly string[] kvNames = {
           "TemPurchasePlanOrderPlanType","TemPurchasePlanOrderShippingMethod","TemPurchasePlanOrderFreightType"
        };

        private DataGridViewBase PartsPurchasePlanDetailForEditDataGridView {
            get {
                if(this.partsPurchasePlanDetailForEditDataGridView == null) {
                    this.partsPurchasePlanDetailForEditDataGridView = DI.GetDataGridView("TemPurchasePlanOrderDetailForEdit");
                    this.partsPurchasePlanDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsPurchasePlanDetailForEditDataGridView;
            }
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        //新增、修改界面 默认 标题
        protected override string BusinessName {
            get {
                return PartsPurchasingUIStrings.DataEditView_Title_TmpPartsPurchaseOrderPlanInfo;
            }
        }

        public ObservableCollection<KeyValuePair> KvShippingMethods {
            get {
                return this.kvShippingMethods ?? (this.kvShippingMethods = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvPlanTypes {
            get {
                return this.kvPlanTypes ?? (this.kvPlanTypes = new ObservableCollection<KeyValuePair>());
            }
        }
        public ObservableCollection<KeyValuePair> KvFreightTypes {
            get {
                return this.kvFreightTypes ?? (this.kvFreightTypes = new ObservableCollection<KeyValuePair>());
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetTemPurchasePlansWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    DataEditPanels.FilePath = entity.Path;
                    this.SetObjectToEdit(entity);
                }
            }, null);


        }
        private ICommand exportFileCommand;

        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }
        //导出抬头
        private const string EXPORT_DATA_FILE_NAME = "导出临时配件计划采购清单模板.xlsx";

        private void InitializeCommand() {
            this.exportFileCommand = new DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                    new ImportTemplateColumn {
                        Name = PartsPurchasingUIStrings.DataEditView_Text_SparePartCode,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = PartsPurchasingUIStrings.DataEditView_ImportTemplate_SparePartName
                    },
                    new ImportTemplateColumn {
                        Name = PartsPurchasingUIStrings.DataEditView_ImportTemplate_PlanAmount,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = PartsPurchasingUIStrings.DataEditView_ImportTemplate_MeasureUnit,
                    },
                    new ImportTemplateColumn {
                        Name = PartsPurchasingUIStrings.QueryPanel_QueryItem_ReferenceCode,
                    }
                };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }

        /// <summary>
        /// 文件上传成功后，调用该方法进行上传后的处理。不支持异步调用
        /// <param>上传文件名 <name>fileName</name> </param>
        /// </summary>
        protected Action<string> UploadFileSuccessedProcessing;

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }

        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage(PartsPurchasingUIStrings.Upload_File_Tips);
                    e.Handled = true;
                }
            }
        }

        private void ExcelServiceClient_ImportTemPurchasePlanOrderDetailCompleted(object sender, ImportTemPurchasePlanOrderDetailCompletedEventArgs e) {
            var temPurchasePlanOrder = this.DataContext as TemPurchasePlanOrder;
            if(temPurchasePlanOrder == null)
                return;
            var detailList = temPurchasePlanOrder.TemPurchasePlanOrderDetails.ToList();
            foreach(var detail in detailList) {
                temPurchasePlanOrder.TemPurchasePlanOrderDetails.Remove(detail);
            }
            if(e.rightData != null && e.rightData.Length > 0) {
                var sparePartIds = e.rightData.Select(o => o.SparePartId).Distinct().ToArray();
                foreach(var data in e.rightData) {
                    var temPurchasePlanOrderDetail = new TemPurchasePlanOrderDetail {
                        SparePartId = data.SparePartId,
                        SparePartCode = data.SparePartCode,
                        SparePartName = data.SparePartName,
                        PlanAmount = data.PlanAmount,
                        MeasureUnit = data.MeasureUnit,
                        ReferenceCode = data.ReferenceCode,
                    };
                    if(temPurchasePlanOrderDetail.SparePartId.HasValue) {
                        temPurchasePlanOrderDetail.CodeSource = (int)DCSTemPurchasePlanOrderDetailCodeSource.SPM;
                    } else temPurchasePlanOrderDetail.CodeSource = (int)DCSTemPurchasePlanOrderDetailCodeSource.手工录入;

                    if(data.SparePartCode.StartsWith("W") || data.SparePartCode.StartsWith("w")) {
                        temPurchasePlanOrderDetail.SupplierPartCode = data.ReferenceCode;
                    } else {
                        temPurchasePlanOrderDetail.SupplierPartCode = data.SparePartCode;
                    }
                    temPurchasePlanOrder.TemPurchasePlanOrderDetails.Add(temPurchasePlanOrderDetail);
                }
            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowNotification(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));

        }

        //本地缓存，记录修改前的配件销售订单类型Id
        //private int oldPartsPurchasePlanTypeId;

        private string strFileName;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private RadUpload uploader;
        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        var temPurchasePlanOrder = this.DataContext as TemPurchasePlanOrder;
                        if(temPurchasePlanOrder == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.ImportTemPurchasePlanOrderDetailAsync(e.HandlerData.CustomData["Path"].ToString());
                        this.excelServiceClient.ImportTemPurchasePlanOrderDetailCompleted -= ExcelServiceClient_ImportTemPurchasePlanOrderDetailCompleted;
                        this.excelServiceClient.ImportTemPurchasePlanOrderDetailCompleted += ExcelServiceClient_ImportTemPurchasePlanOrderDetailCompleted;
                    };
                }
                return this.uploader;
            }
        }

        private void ShowFileDialog() {
            var temPurchasePlanOrder = this.DataContext as TemPurchasePlanOrder;
            if(temPurchasePlanOrder == null)
                return;
            temPurchasePlanOrder.ValidationErrors.Clear();
            this.Uploader.ShowFileDialog();
        }

        private void CreateUI() {
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(TemPurchasePlanOrder), PartsPurchasingUIStrings.DetailPanel_Title_TemPartsPurchasePlanDetail /*"PartsPurchasePlanDetails"*/), null, this.PartsPurchasePlanDetailForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            //导入、导出
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsPurchasingUIStrings.Action_Title_Import,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                Command = new DelegateCommand(this.ShowFileDialog)
            });
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsPurchasingUIStrings.Upload_File_Export_Model,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                Command = this.ExportTemplateCommand
            });


            this.Root.Children.Add(detailDataEditView);

            KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData(() => {
                foreach(var keyValuePair in this.KeyValueManager[this.kvNames[0]]) {
                    this.KvPlanTypes.Add(keyValuePair);
                }
                foreach(var keyValuePair in this.KeyValueManager[this.kvNames[2]]) {
                    this.KvFreightTypes.Add(keyValuePair);
                }
                foreach(var keyValuePair in this.KeyValueManager[this.kvNames[1]]) {
                    this.KvShippingMethods.Add(keyValuePair);
                }
            });
            comPlanType.ItemsSource = KvPlanTypes;
            CbShippingMethod.ItemsSource = KvShippingMethods;
            comFreightType.ItemsSource = KvFreightTypes;
            //附件
            this.DataEditPanels.SetValue(Grid.RowProperty, 5);
            this.DataEditPanels.SetValue(Grid.ColumnProperty, 0);
            this.DataEditPanels.Margin = new Thickness(0, 330, 0, 0);
            this.DataEditPanels.HorizontalAlignment = HorizontalAlignment.Left;
            this.DataEditPanels.VerticalAlignment = VerticalAlignment.Top;
            this.DataEditPanels.SetValue(Grid.ColumnSpanProperty, 2);
            this.Root.Children.Add(DataEditPanels);
            //收货单位
            var customerInformationBySparepart = DI.GetQueryWindow("CustomerInformationBySparepart");
            customerInformationBySparepart.SelectionDecided += this.QueryWindowForCustomerInformationBySparepart_SelectionDecided;
            customerInformationBySparepart.Loaded += customerInformationBySparepart_Loaded;
            this.popupTextBoxSubmitCompanyName.PopupContent = customerInformationBySparepart;
        }

        private void PartsPurchaseOrderDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var temPurchasePlanOrder = this.DataContext as TemPurchasePlanOrder;
            if(temPurchasePlanOrder == null)
                return;
            temPurchasePlanOrder.PropertyChanged -= this.partsPurchasePlan_PropertyChanged;
            temPurchasePlanOrder.PropertyChanged += this.partsPurchasePlan_PropertyChanged;

        }
        private void partsPurchasePlan_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var temPurchasePlanOrder = this.DataContext as TemPurchasePlanOrder;
            if(temPurchasePlanOrder == null || temPurchasePlanOrder.ReceCompanyId != null)
                return;
            if(this.DomainContext == null) {
                this.DomainContext = new DcsDomainContext();
            }

            this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(r => r.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                if(loadOp.Entities != null) {
                    var company = loadOp.Entities.First();
                    if(company.Type == (int)DcsCompanyType.分公司) {
                        this.ckIsTurnSale.IsEnabled = true;
                    } else {
                        this.ckIsTurnSale.IsEnabled = false;
                        temPurchasePlanOrder.IsTurnSale = true;
                    }

                }
            }, null);

        }
        private void QueryWindowForCustomerInformationBySparepart_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;

            if(queryWindow.SelectedEntities == null || !queryWindow.SelectedEntities.Any() || queryWindow.SelectedEntities.Cast<CustomerInformation>() == null)
                return;
            var temPurchasePlanOrder = this.DataContext as TemPurchasePlanOrder;
            if(temPurchasePlanOrder == null)
                return;
            var customerInformation = queryWindow.SelectedEntities.Cast<CustomerInformation>().FirstOrDefault();
            if(customerInformation == null)
                return;
            if(this.DomainContext == null) {
                this.DomainContext = new DcsDomainContext();
            }
            this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(r => r.Id == customerInformation.CustomerCompanyId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                if(loadOp.Entities != null) {
                    var company = loadOp.Entities.First();
                    temPurchasePlanOrder.ReceCompanyId = company.Id;
                    temPurchasePlanOrder.ReceCompanyCode = company.Code;
                    temPurchasePlanOrder.ReceCompanyName = company.Name;
                    temPurchasePlanOrder.Status = (int)DCSTemPurchasePlanOrderStatus.提交;
                    if(company.Type == (int)DcsCompanyType.代理库) {
                        //收货仓库
                        this.DomainContext.Load(this.DomainContext.GetWarehousesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.Type == (int)DcsWarehouseType.总库 && r.StorageCompanyId == company.Id), LoadBehavior.RefreshCurrent, loadOp1 => {
                            if(loadOp1.HasError) {
                                loadOp1.MarkErrorAsHandled();
                                return;
                            }
                            if(loadOp1.Entities != null) {
                                temPurchasePlanOrder.WarehouseId = loadOp1.Entities.First().Id;
                                temPurchasePlanOrder.WarehouseName = loadOp1.Entities.First().Name;
                                temPurchasePlanOrder.OrderCompanyId = company.Id;
                                temPurchasePlanOrder.OrderCompanyCode = company.Code;
                                temPurchasePlanOrder.OrderCompanyName = company.Name;
                                temPurchasePlanOrder.Status = (int)DCSTemPurchasePlanOrderStatus.审核通过;
                                temPurchasePlanOrder.CustomerType = (int)DCSTemPurchasePlanOrderCustomerType.中心库;
                            }
                        }, null);
                        this.blockReturnWarehouse.Visibility = Visibility.Visible;
                        this.Cbwarehouse.Visibility = Visibility.Visible;
                    } else if(company.Type == (int)DcsCompanyType.出口客户 || company.Type == (int)DcsCompanyType.集团企业) {
                        temPurchasePlanOrder.Status = (int)DCSTemPurchasePlanOrderStatus.审核通过;
                        temPurchasePlanOrder.OrderCompanyId = company.Id;
                        temPurchasePlanOrder.OrderCompanyCode = company.Code;
                        temPurchasePlanOrder.OrderCompanyName = company.Name;
                        temPurchasePlanOrder.IsTurnSale = false;
                        if(company.Type == (int)DcsCompanyType.出口客户) {
                            temPurchasePlanOrder.CustomerType = (int)DCSTemPurchasePlanOrderCustomerType.出口客户;
                        } else temPurchasePlanOrder.CustomerType = (int)DCSTemPurchasePlanOrderCustomerType.集团企业;
                    }
                    //联系人联系地址
                    this.DomainContext.Load(this.DomainContext.GetCompanyAddressesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.CompanyId == company.Id), LoadBehavior.RefreshCurrent, loadOp1 => {
                        if(loadOp1.HasError) {
                            loadOp1.MarkErrorAsHandled();
                            return;
                        }
                        if(loadOp1.Entities != null) {
                            temPurchasePlanOrder.ReceiveAddress = loadOp1.Entities.First().DetailAddress;
                            temPurchasePlanOrder.Linker = loadOp1.Entities.First().ContactPerson;
                            temPurchasePlanOrder.LinkPhone = loadOp1.Entities.First().ContactPhone;
                        }
                    }, null);

                }
            }, null);
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }
        protected void customerInformationBySparepart_Loaded(object sender, RoutedEventArgs e) {
            var queryWindowCustomerInformationBySparepart = sender as QueryWindowBase;
            if(queryWindowCustomerInformationBySparepart == null)
                return;
            var temPurchasePlanOrder = this.DataContext as TemPurchasePlanOrder;
            if(temPurchasePlanOrder == null)
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem("SalesCompanyId", typeof(int), FilterOperator.IsEqualTo, BaseApp.Current.CurrentUserData.EnterpriseId));
            queryWindowCustomerInformationBySparepart.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }
        protected override void OnEditSubmitting() {
            if(!this.PartsPurchasePlanDetailForEditDataGridView.CommitEdit())
                return;

            var temPurchasePlanOrder = this.DataContext as TemPurchasePlanOrder;
            if(temPurchasePlanOrder == null)
                return;

            temPurchasePlanOrder.ValidationErrors.Clear();
            temPurchasePlanOrder.Path = DataEditPanels.FilePath;
            foreach(var relation in temPurchasePlanOrder.TemPurchasePlanOrderDetails)
                relation.ValidationErrors.Clear();



            if((temPurchasePlanOrder.PlanType <= 0))
                temPurchasePlanOrder.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PlanTypeIsNull, new[] {
                    "PlanType"
                }));

            temPurchasePlanOrder.Memo = this.CbMemo.Text;

            if(!temPurchasePlanOrder.TemPurchasePlanOrderDetails.Any()) {
                UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrderDetail_DetailIsNotNull));
                return;
            }

            foreach(var relation in temPurchasePlanOrder.TemPurchasePlanOrderDetails) {
                if(string.IsNullOrEmpty(relation.SparePartCode))
                    relation.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrderDetail_SparePartCodeIsNull, new[] {
                        "SparePartCode"
                    }));       
            }

            foreach(var relation in temPurchasePlanOrder.TemPurchasePlanOrderDetails.Where(e => e.PlanAmount <= 0)) {
                UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Validation_PlanAmountMustMoreThanZero, relation.SparePartCode));
                return;
            }
            if(temPurchasePlanOrder.HasValidationErrors || temPurchasePlanOrder.TemPurchasePlanOrderDetails.Any(relation => relation.HasValidationErrors))
                return;
            temPurchasePlanOrder.IsReplace = true;
            ((IEditableObject)temPurchasePlanOrder).EndEdit();
                try {
                    if(this.EditState == DataEditState.New) {
                        if(temPurchasePlanOrder.Can生成临时采购计划单)
                            temPurchasePlanOrder.生成临时采购计划单();
                    } else {
                        if(temPurchasePlanOrder.Can修改临时采购计划单)
                            temPurchasePlanOrder.修改临时采购计划单();
                    }
                } catch(ValidationException ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                    return;
                }
                base.OnEditSubmitting();
                //});                  
        }

        public TemPurchasePlanOrderDataEditView() {
            InitializeComponent();
            this.KeyValueManager.LoadData();
            this.Initializer.Register(this.CreateUI);           
        }
       
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        protected override void Reset() {
            var temPurchasePlanOrder = this.DataContext as TemPurchasePlanOrder;
            if(this.DomainContext.TemPurchasePlanOrders.Contains(temPurchasePlanOrder))
                this.DomainContext.TemPurchasePlanOrders.Detach(temPurchasePlanOrder);
        }

        private FileUploadForPartsOuterPurchaseDataEditPanel productDataEditPanels;

        public FileUploadForPartsOuterPurchaseDataEditPanel DataEditPanels {
            get {
                return this.productDataEditPanels ?? (this.productDataEditPanels = (FileUploadForPartsOuterPurchaseDataEditPanel)DI.GetDataEditPanel("FileUploadForPartsOuterPurchase"));
            }
        }
    }
}
