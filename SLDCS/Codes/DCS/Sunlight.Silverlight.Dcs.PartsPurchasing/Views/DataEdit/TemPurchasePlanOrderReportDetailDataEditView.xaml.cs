﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Sunlight.Silverlight.ViewModel;
using System.Windows.Browser;
using System.Collections.Generic;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class TemPurchasePlanOrderReportDetailDataEditView  {
        private DataGridViewBase partsPurchasePlanDetailForEditDataGridView;
        private ObservableCollection<KeyValuePair> kvPlanTypes;
        private KeyValueManager keyValueManager;
        private ObservableCollection<KeyValuePair> kvShippingMethods;
        private ObservableCollection<KeyValuePair> kvFreightTypes;

        private readonly string[] kvNames = {
           "TemPurchasePlanOrderPlanType","TemPurchasePlanOrderShippingMethod","TemPurchasePlanOrderFreightType"
        };

        private DataGridViewBase PartsPurchasePlanDetailForEditDataGridView {
            get {
                if(this.partsPurchasePlanDetailForEditDataGridView == null) {
                    this.partsPurchasePlanDetailForEditDataGridView = DI.GetDataGridView("TemPurchasePlanOrderDetailforDetail");
                    this.partsPurchasePlanDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsPurchasePlanDetailForEditDataGridView;
            }
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        //新增、修改界面 默认 标题
        protected override string BusinessName {
            get {
                return PartsPurchasingUIStrings.DataEditView_Title_TmpPartsPurchaseOrderPlanInfoDetail;
            }
        }       

        public ObservableCollection<KeyValuePair> KvShippingMethods {
            get {
                return this.kvShippingMethods ?? (this.kvShippingMethods = new ObservableCollection<KeyValuePair>());
            }
        }



        public ObservableCollection<KeyValuePair> KvPlanTypes {
            get {
                return this.kvPlanTypes ?? (this.kvPlanTypes = new ObservableCollection<KeyValuePair>());
            }
        }
        public ObservableCollection<KeyValuePair> KvFreightTypes {
            get {
                return this.kvFreightTypes ?? (this.kvFreightTypes = new ObservableCollection<KeyValuePair>());
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetTemPurchasePlansWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    DataEditPanels.FilePath = entity.Path;
                    if(entity.CustomerType == (int)DCSTemPurchasePlanOrderCustomerType.中心库) {
                          this.blockReturnWarehouse.Visibility = Visibility.Visible;
                        this.Cbwarehouse.Visibility = Visibility.Visible;
                    }
                    this.SetObjectToEdit(entity);
                }
            }, null);


        }
        public TemPurchasePlanOrderReportDetailDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += PartsPurchaseOrderDataEditView_DataContextChanged;
            this.KeyValueManager.LoadData();
        }
        private void PartsPurchaseOrderDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var temPurchasePlanOrder = this.DataContext as TemPurchasePlanOrder;
            if(temPurchasePlanOrder == null)
                return;
            temPurchasePlanOrder.PropertyChanged -= this.partsPurchasePlan_PropertyChanged;
            temPurchasePlanOrder.PropertyChanged += this.partsPurchasePlan_PropertyChanged;

        }
        private void partsPurchasePlan_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var temPurchasePlanOrder = this.DataContext as TemPurchasePlanOrder;
            if(temPurchasePlanOrder == null || temPurchasePlanOrder.ReceiveAddress != null)
                return;
            if(temPurchasePlanOrder.CustomerType == (int)DCSTemPurchasePlanOrderCustomerType.中心库) {
                this.blockReturnWarehouse.Visibility = Visibility.Visible;
                this.Cbwarehouse.Visibility = Visibility.Visible;
            }
        }
        private void CreateUI() {
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(TemPurchasePlanOrder), PartsPurchasingUIStrings.DetailPanel_Title_TemPartsPurchasePlanDetail /*"PartsPurchasePlanDetails"*/), null, this.PartsPurchasePlanDetailForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);                      
            this.Root.Children.Add(detailDataEditView);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData(() => {
                foreach(var keyValuePair in this.KeyValueManager[this.kvNames[0]]) {
                    this.KvPlanTypes.Add(keyValuePair);
                }
                foreach(var keyValuePair in this.KeyValueManager[this.kvNames[2]]) {
                    this.KvFreightTypes.Add(keyValuePair);
                }
                foreach(var keyValuePair in this.KeyValueManager[this.kvNames[1]]) {
                    this.KvShippingMethods.Add(keyValuePair);
                }
            });
            comPlanType.ItemsSource = KvPlanTypes;
            CbShippingMethod.ItemsSource = KvShippingMethods;
            comFreightType.ItemsSource = KvFreightTypes;
            //附件
            this.DataEditPanels.SetValue(Grid.RowProperty, 5);
            this.DataEditPanels.SetValue(Grid.ColumnProperty, 0);
            this.DataEditPanels.Margin = new Thickness(0, 420, 0, 0);
            this.DataEditPanels.HorizontalAlignment = HorizontalAlignment.Left;
            this.DataEditPanels.VerticalAlignment = VerticalAlignment.Top;
            this.DataEditPanels.SetValue(Grid.ColumnSpanProperty, 2);
            this.DataEditPanels.isHiddenButtons = true;
            this.Root.Children.Add(DataEditPanels);
        }          
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private FileUploadForPartsOuterPurchaseDataEditPanel productDataEditPanels;

        public FileUploadForPartsOuterPurchaseDataEditPanel DataEditPanels {
            get {
                return this.productDataEditPanels ?? (this.productDataEditPanels = (FileUploadForPartsOuterPurchaseDataEditPanel)DI.GetDataEditPanel("FileUploadForPartsOuterPurchase"));
            }
        }
    }
}