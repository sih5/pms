﻿﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class TemSupplierShippingOrderConfirmDataEditView{
        private DataGridViewBase supplierShippingDetailForEditDataGridView;
        private KeyValueManager keyValueManager;

        private readonly string[] kvNames = {
            "TemPurchasePlanOrderShippingMethod"
        };
        public TemSupplierShippingOrderConfirmDataEditView() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.LoadData();
        }     
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private DataGridViewBase SupplierShippingDetailForEditDataGridView {
            get {
                if(this.supplierShippingDetailForEditDataGridView == null) {
                    this.supplierShippingDetailForEditDataGridView = DI.GetDataGridView("TemShippingOrderDetailConfirm");
                    this.supplierShippingDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.supplierShippingDetailForEditDataGridView;
            }
        }


        protected virtual void CreateUI() {
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(SupplierShippingOrder), "SupplierShippingDetails"), null, () => this.SupplierShippingDetailForEditDataGridView);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);
            cbShippingMethod.ItemsSource = KvShippingMethods;

            //附件
            this.DataEditPanels.SetValue(Grid.RowProperty, 5);
            this.DataEditPanels.SetValue(Grid.ColumnProperty, 0);
            this.DataEditPanels.Margin = new Thickness(0, 570, 0, 0);
            this.DataEditPanels.HorizontalAlignment = HorizontalAlignment.Left;
            this.DataEditPanels.VerticalAlignment = VerticalAlignment.Top;
            this.DataEditPanels.SetValue(Grid.ColumnSpanProperty, 2);
            this.Root.Children.Add(DataEditPanels);
        }
        private FileUploadForPartsOuterPurchaseDataEditPanel productDataEditPanels;

        public FileUploadForPartsOuterPurchaseDataEditPanel DataEditPanels {
            get {
                return this.productDataEditPanels ?? (this.productDataEditPanels = (FileUploadForPartsOuterPurchaseDataEditPanel)DI.GetDataEditPanel("FileUploadForPartsOuterPurchase"));
            }
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetTemSupplierShippingOrderWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                DataEditPanels.FilePath = entity.Path;
                foreach(var item in entity.TemShippingOrderDetails ){
                    item.UnConfirmedAmount = item.Quantity - item.ConfirmedAmount ?? 0;
                }
                this.SetObjectToEdit(entity);
            }, null);
        }           

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_SupplierShippingOrder;
            }
        }

        protected override void Reset() {
            this.ptbPartsPurchaseOrder.IsEnabled = false;
            this.DataContext = null;
        }

        protected override void OnEditSubmitting() {
            if(!this.SupplierShippingDetailForEditDataGridView.CommitEdit())
                return;

            var supplierShippingOrder = this.DataContext as TemSupplierShippingOrder;
            if(supplierShippingOrder == null)
                return;
            supplierShippingOrder.ValidationErrors.Clear();
            foreach(var item in supplierShippingOrder.TemShippingOrderDetails) {
                if(item.ConfirmedAmount.HasValue && item.ConfirmedAmount < 0) {
                    UIHelper.ShowNotification(item.SparePartCode + "确认数量不能小于0");
                    return;
                }
                if(item.ConfirmedAmount == null) {
                    item.ConfirmedAmount = 0;
                }
                if(item.Quantity < item.ConfirmedAmount + item.UnConfirmedAmount) {
                    UIHelper.ShowNotification(item.SparePartCode + "确认数量不能大于发运数量");
                    return;
                }
                item.ValidationErrors.Clear();
            }
            if(string.IsNullOrEmpty(DataEditPanels.FilePath) && BaseApp.Current.CurrentUserData.EnterpriseId != supplierShippingOrder.OrderCompanyId) {
                UIHelper.ShowNotification("确认前请先上传附件");
                return;
            }
            supplierShippingOrder.Path = DataEditPanels.FilePath;
            ((IEditableObject)supplierShippingOrder).EndEdit();
            if(supplierShippingOrder.Can收货确认临时供应商发运单)
                    supplierShippingOrder.收货确认临时供应商发运单();                    
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }   
        public ObservableCollection<KeyValuePair> KvShippingMethods {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
    }
}