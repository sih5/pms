﻿﻿using System;
﻿using System.Collections.ObjectModel;
﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
﻿using Sunlight.Silverlight.Core.Model;
﻿using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
﻿using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit {
    public partial class TemSupplierShippingOrderEditDataEditView {
        private DataGridViewBase supplierShippingDetailForEditDataGridView;
        private KeyValueManager keyValueManager;
        private readonly ObservableCollection<Branch> kvBranches = new ObservableCollection<Branch>();

        private readonly string[] kvNames = {
            "TemPurchasePlanOrderShippingMethod"
        };
        public TemSupplierShippingOrderEditDataEditView() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.LoadData();
        }     
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private DataGridViewBase SupplierShippingDetailForEditDataGridView {
            get {
                if(this.supplierShippingDetailForEditDataGridView == null) {
                    this.supplierShippingDetailForEditDataGridView = DI.GetDataGridView("TemShippingOrderDetail");
                    this.supplierShippingDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.supplierShippingDetailForEditDataGridView;
            }
        }


        protected virtual void CreateUI() {
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(SupplierShippingOrder), "SupplierShippingDetails"), null, () => this.SupplierShippingDetailForEditDataGridView);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);
            this.KvBranches.Clear();
            this.DomainContext.Load(this.DomainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.KeepCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var branch in loadOp.Entities)
                    this.KvBranches.Add(branch);
            }, null);
            cbShippingMethod.ItemsSource = KvShippingMethods;
        }     
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetTemSupplierShippingOrderWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.SetObjectToEdit(entity);
            }, null);
        }           

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_SupplierShippingOrder;
            }
        }

        protected override void Reset() {
            this.ptbPartsPurchaseOrder.IsEnabled = false;
            this.DataContext = null;
        }

        protected override void OnEditSubmitting() {
            if(!this.SupplierShippingDetailForEditDataGridView.CommitEdit())
                return;

            var supplierShippingOrder = this.DataContext as TemSupplierShippingOrder;
            if(supplierShippingOrder == null)
                return;
            supplierShippingOrder.ValidationErrors.Clear();
            foreach(var item in supplierShippingOrder.TemShippingOrderDetails) {
                item.ValidationErrors.Clear();
            }           
            ((IEditableObject)supplierShippingOrder).EndEdit();
                if(supplierShippingOrder.Can修改临时供应商发运单)
                    supplierShippingOrder.修改临时供应商发运单();                    
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public ObservableCollection<Branch> KvBranches {
            get {
                return this.kvBranches;
            }
        }

        public ObservableCollection<KeyValuePair> KvShippingMethods {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
    }
}