﻿
using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class BranchSupplierRelationForFaultyPartsDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = { 
            "BaseData_Status"
        };

        public BranchSupplierRelationForFaultyPartsDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem{
                        Name = "PartsSupplier.Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem{
                        Name = "BusinessCode",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_PartsSupplierCode
                    }, new ColumnItem{
                        Name = "BusinessName",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_PartsSupplierName
                    },  new ColumnItem{
                        Name = "PartsSupplier.Remark"
                    }, new ColumnItem{
                        Name = "PartsSalesCategory.Name",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_BranchSupplierRelation_Name
                    }
                };
            }
        }
        protected override Type EntityType {
            get {
                return typeof(BranchSupplierRelation);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetBranchSupplierRelationWithPartsSalesCategory";
        }
    }
}
