﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class BranchSupplierRelationForQueryDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = { 
            "BaseData_Status"
        };

        public BranchSupplierRelationForQueryDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new KeyValuesColumnItem{
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },  new ColumnItem{
                        Name = "PartsSupplier.Code"
                    }, new ColumnItem{
                        Name = "PartsSupplier.Name"
                    }, new ColumnItem{
                        Name = "PurchasingCycle"
                    },new ColumnItem{
                        Name = "Remark"
                    }, new ColumnItem{
                        Name = "CreatorName"
                    }, new ColumnItem{
                        Name = "CreateTime"
                    } ,new ColumnItem{
                        Name = "Branch.Name"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(BranchSupplierRelation);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetBranchSupplierRelationWithPartsSupplier";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filterItem = this.FilterItem as CompositeFilterItem;
            if(filterItem != null) {
                var compositeFilterItem = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                if(compositeFilterItem != null) {
                    switch(parameterName) {
                        case "partsSalesCategoryId":
                            var branchSupplierRelationFilterItem = compositeFilterItem.Filters.SingleOrDefault(e => e.MemberName == "PartsSalesCategoryId");
                            return branchSupplierRelationFilterItem == null ? null : branchSupplierRelationFilterItem.Value;
                    }
                }
            }
            return null;
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;

        }
    }
}
