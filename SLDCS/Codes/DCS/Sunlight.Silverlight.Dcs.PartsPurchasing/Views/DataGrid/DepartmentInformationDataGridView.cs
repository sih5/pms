﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class DepartmentInformationDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = { 
            "BaseData_Status" 
        };

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new ColumnItem[] { 
                    new KeyValuesColumnItem{
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem{
                        Name = "Code"
                    }, new ColumnItem{
                        Name = "Name"
                    }, new ColumnItem{
                        Name = "Responsible"
                    }, new ColumnItem{
                        Name = "ContactPhone"
                    }, new ColumnItem{
                        Name = "Remark"
                    }, new ColumnItem{
                        Name = "CreatorName"
                    }, new ColumnItem{
                        Name = "CreateTime"
                    }, new ColumnItem{
                        Name = "ModifierName"
                    }, new ColumnItem{
                        Name = "ModifyTime"
                    }, new ColumnItem{
                        Name = "BranchName"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DepartmentInformation);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetDepartmentInformations";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
        }

        public DepartmentInformationDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
