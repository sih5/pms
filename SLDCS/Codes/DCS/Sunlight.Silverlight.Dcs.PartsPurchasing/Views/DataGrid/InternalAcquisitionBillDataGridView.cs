﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class InternalAcquisitionBillDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "WorkflowOfSimpleApproval_Status","PurchaseInStatus"
        };

        public InternalAcquisitionBillDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "WarehouseName",
                        Title = PartsPurchasingUIStrings.DataEditPanel_Text_InternalAcquisitionBill_WarehouseName
                    }, new ColumnItem {
                        Name = "DepartmentName",
                        Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_DepartmentName
                    }, //new ColumnItem {
                        //Name = "TotalAmount",
                        //TextAlignment=TextAlignment.Right
                    //}, 
                    new AggregateColumnItem {
                        Name = "TotalAmount",
                        AggregateType = AggregateType.Sum ,
                        TextAlignment=TextAlignment.Right
                    },
                    new ColumnItem {
                        Name = "Reason"
                    }, new ColumnItem {
                        Name = "Operator"
                    }, new ColumnItem {
                        Name = "RequestedDeliveryTime"
                    }, new ColumnItem {
                        Name = "PartsPurchaseOrderType.Name"
                    },  new KeyValuesColumnItem {
                        Name = "InStatus",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title = PartsPurchasingUIStrings.DataGridView_Column_InboundStatus
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "ApproverName"
                    }, new ColumnItem {
                        Name = "ApproveTime"
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime"
                    }, new ColumnItem {
                        Name = "BranchName"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetInternalAcquisitionBillsForSort";
        }

        protected override Type EntityType {
            get {
                return typeof(InternalAcquisitionBill);
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "InternalAcquisitionBillDetail","InternalAcquisitionBillDetailKanBan"
                    }
                };
            }
        }

    }
}
