﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class InternalAcquisitionBillDetailDataGridView : DcsDataGridViewBase {
        public InternalAcquisitionBillDetailDataGridView() {
            this.DataContextChanged += this.InternalAcquisitionBillDetailDetailDataGridView_DataContextChanged;
        }

        private void InternalAcquisitionBillDetailDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var internalAcquisitionBill = e.NewValue as InternalAcquisitionBill;
            if(internalAcquisitionBill == null || internalAcquisitionBill.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "InternalAcquisitionBillId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = internalAcquisitionBill.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override Type EntityType {
            get {
                return typeof(InternalAcquisitionDetail);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                    }, new ColumnItem {
                        Name = "SparePartCode",
                    }, new ColumnItem {
                        Name = "SparePartName"
                    }, new ColumnItem {
                        Name="ReferenceCodeQuery",
                        Title = PartsPurchasingUIStrings.DataGridView_Column_ReferenceCodeQuery,
                        IsReadOnly=true          
                    },new ColumnItem {
                        Name = "Quantity",
                        TextAlignment=TextAlignment.Right,
                    }, new ColumnItem {
                        Name = "UnitPrice",
                        TextAlignment=TextAlignment.Right,
                    }, new ColumnItem {
                        Name = "SalesPrice",
                        TextAlignment=TextAlignment.Right,
                    }, new ColumnItem {
                        Name = "MeasureUnit",
                    }, new ColumnItem {
                        Name = "Remark",
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetInternalAcquisitionDetails";
        }

        protected override void OnDataLoaded() {
            var internalAcquisitionDetails = this.Entities.Cast<InternalAcquisitionDetail>().ToArray();
            if(internalAcquisitionDetails == null) {
                return;
            }
            var dcsDomainContext = this.DomainContext as DcsDomainContext;

            dcsDomainContext.Load(dcsDomainContext.GetSparePartByIdsQuery(internalAcquisitionDetails.Select(r => r.SparePartId).ToArray()), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var spareParts = loadOp.Entities;
                foreach(var internalAcquisitionDetail in internalAcquisitionDetails) {
                    var sparePart = spareParts.FirstOrDefault(r => r.Id == internalAcquisitionDetail.SparePartId);
                    if(sparePart != null) {
                        internalAcquisitionDetail.ReferenceCodeQuery = sparePart.ReferenceCode;
                    }
                }
            }, null);


        }

    }
}
