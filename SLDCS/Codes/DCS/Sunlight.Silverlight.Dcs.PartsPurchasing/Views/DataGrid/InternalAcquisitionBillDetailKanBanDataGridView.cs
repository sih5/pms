﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class InternalAcquisitionBillDetailKanBanDataGridView : DcsDataGridViewBase {

        protected override Type EntityType {
            get {
                return typeof(SparePartWithQuantity);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber"
                    }, new ColumnItem {
                        Name = "SparePartCode"
                    }, new ColumnItem {
                        Name = "SparePartName"
                    }, new ColumnItem {
                        Name = "Quantity",
                        TextAlignment=TextAlignment.Right
                    }, new ColumnItem {
                        Name = "InspectedQuantity",
                        TextAlignment=TextAlignment.Right
                    }
                };
            }
        }

    }
}
