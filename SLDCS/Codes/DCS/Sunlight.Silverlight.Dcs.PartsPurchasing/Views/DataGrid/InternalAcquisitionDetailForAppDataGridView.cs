﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class InternalAcquisitionDetailForAppDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = false
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name="ReferenceCodeQuery",
                        Title = PartsPurchasingUIStrings.DataGridView_Column_ReferenceCodeQuery,
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "Quantity",
                        TextAlignment=TextAlignment.Right
                    }, new ColumnItem {
                        Name = "UnitPrice",
                        IsReadOnly = true,
                        TextAlignment=TextAlignment.Right
                    }, new ColumnItem {
                        Name = "SalesPrice",
                        IsReadOnly = true,
                        TextAlignment=TextAlignment.Right
                    }, new ColumnItem {
                        Name = "MeasureUnit",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(InternalAcquisitionDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("InternalAcquisitionDetails");
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}