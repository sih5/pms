﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class InternalAcquisitionDetailForEditDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
          "IsOrNot"
        };
        private int[] PartsBranch_IsOrderable = new[] {
        (int)DcsIsOrNot.否
        };

        public InternalAcquisitionDetailForEditDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        #region 弹窗

        private DcsMultiPopupsQueryWindowBase sparePartMultiSelectQueryWindow;
        private RadWindow radQueryWindow;
        private int PartsSalesCategoryId;
        private DcsMultiPopupsQueryWindowBase SparePartMultiSelectQueryWindow {
            get {
                if(this.sparePartMultiSelectQueryWindow == null) {
                    this.sparePartMultiSelectQueryWindow = DI.GetQueryWindow("SparePartMultiSelect") as DcsMultiPopupsQueryWindowBase;

                    this.sparePartMultiSelectQueryWindow.SelectionDecided += sparePartMultiSelectQueryWindow_SelectionDecided;
                }
                return this.sparePartMultiSelectQueryWindow;
            }
        }

        private void sparePartMultiSelectQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            var domainContext = this.DomainContext as DcsDomainContext;
            var internalAcquisition = this.DataContext as InternalAcquisitionBill;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            if(queryWindow == null || domainContext == null || internalAcquisition == null)
                return;

            var sparePart = queryWindow.SelectedEntities.Cast<SparePart>();
            if(sparePart == null)
                return;

            if(internalAcquisition.InternalAcquisitionDetails.Any(r => sparePart.Any(ex => ex.Code == r.SparePartCode))) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_InternalAcquisitionDetail_SparePartCodeIsAlreadyHave);
                return;
            }

            foreach(var spare in sparePart) {
                var serialNumber = internalAcquisition.InternalAcquisitionDetails.Count();
                serialNumber++;


                domainContext.Load(domainContext.GetPartsBranchesQuery().Where(entity => entity.PartId == spare.Id), LoadBehavior.RefreshCurrent, loadOpPartsBranch => {
                    if(loadOpPartsBranch.HasError) {
                        loadOpPartsBranch.MarkErrorAsHandled();
                        return;
                    }
                    var PartsBranchs = loadOpPartsBranch.Entities.ToArray();
                    var partsbranch = PartsBranchs.FirstOrDefault();

                    if(partsbranch == null) {
                        UIHelper.ShowNotification(spare.Code + PartsPurchasingUIStrings.DataGridView_Column_PartsBranchIsNull);
                        return;
                    }

                    var internalAcquisitionDetail = new InternalAcquisitionDetail {
                        InternalAcquisitionBill = internalAcquisition,
                        SerialNumber = serialNumber,
                        SparePartId = spare.Id,
                        SparePartCode = spare.Code,
                        SparePartName = spare.Name,
                        Quantity = default(int),
                        UnitPrice = spare.PartsSalePrice,
                        MeasureUnit = spare.MeasureUnit,
                        IsOrderable = partsbranch.IsOrderable ? (int)DcsIsOrNot.是 : (int)DcsIsOrNot.否,
                        ReferenceCodeQuery = spare.ReferenceCode
                    };


                    domainContext.Load(domainContext.GetPartsPlannedPricesQuery().Where(entity => entity.PartsSalesCategoryId == PartsSalesCategoryId && entity.OwnerCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && entity.SparePartId == spare.Id), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            loadOp.MarkErrorAsHandled();
                            return;
                        }
                        var plannedPrices = loadOp.Entities.ToArray();
                        if(!plannedPrices.Any()) {
                            UIHelper.ShowNotification(internalAcquisitionDetail.SparePartCode + PartsPurchasingUIStrings.DataGridView_Column_PlanPriceIsNull);
                            return;
                        } else if(plannedPrices.Length > 1) {
                            UIHelper.ShowNotification(internalAcquisitionDetail.SparePartCode + PartsPurchasingUIStrings.DataGridView_Column_PlanPriceTooMany);
                            return;
                        } else {
                            internalAcquisitionDetail.UnitPrice = plannedPrices.First().PlannedPrice;
                        }
                        domainContext.Load(domainContext.GetPartsSalesPricesQuery().Where(entity => entity.PartsSalesCategoryId == PartsSalesCategoryId && entity.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && entity.SparePartId == spare.Id && entity.PriceType == (int)DcsPartsSalesPricePriceType.基准销售价), LoadBehavior.RefreshCurrent, loadOpSalesPrice => {
                            if(loadOpSalesPrice.HasError) {
                                loadOpSalesPrice.MarkErrorAsHandled();
                                return;
                            }
                            var salesPrice = loadOpSalesPrice.Entities.ToArray();
                            if(!salesPrice.Any()) {
                                UIHelper.ShowNotification(internalAcquisitionDetail.SparePartCode + PartsPurchasingUIStrings.DataGridView_Column_WholeSalePirceIsNull);
                                return;
                            } else if(salesPrice.Length > 1) {
                                UIHelper.ShowNotification(internalAcquisitionDetail.SparePartCode + PartsPurchasingUIStrings.DataGridView_Column_WholeSalePirceTooMany);
                                return;
                            } else {
                                internalAcquisitionDetail.SalesPrice = salesPrice.First().SalesPrice;
                                internalAcquisition.InternalAcquisitionDetails.Add(internalAcquisitionDetail);
                            }
                        }, null);
                    }, null);
                }, null);
            }
        }

        #endregion

        #region GridView操作

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var internalAcquisitionBill = this.DataContext as InternalAcquisitionBill;
            if(internalAcquisitionBill == null)
                return;
            e.Cancel = true;
            if(internalAcquisitionBill.WarehouseId == default(int)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataGridView_Validation_InternalAcquisitionBill_WarehouseIdIsRequired);
                return;
            }
            var dcsDomainContext = this.DomainContext as DcsDomainContext;
            if(dcsDomainContext == null)
                return;
            dcsDomainContext.Load(dcsDomainContext.GetSalesUnitByWarehouseIdQuery(internalAcquisitionBill.WarehouseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                var salesUnit = loadOp.Entities.SingleOrDefault();
                if(salesUnit != null)
                    PartsSalesCategoryId = salesUnit.PartsSalesCategoryId;
            }, null);
            if(this.radQueryWindow == null) {
                this.radQueryWindow = new RadWindow {
                    Content = this.SparePartMultiSelectQueryWindow,
                    Header = PartsPurchasingUIStrings.QueryPanel_Title_SparePart,
                    WindowStartupLocation = WindowStartupLocation.CenterOwner
                };
            }
            this.radQueryWindow.ShowDialog();
        }

        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
            var internalAcquisitionDetail = e.Cell.DataContext as InternalAcquisitionDetail;
            if(internalAcquisitionDetail == null)
                return;

            switch(e.Cell.Column.UniqueName) {
                case "Quantity":
                    var quantityError = internalAcquisitionDetail.ValidationErrors.FirstOrDefault(r => r.MemberNames.Contains("Quantity"));
                    if(quantityError != null)
                        internalAcquisitionDetail.ValidationErrors.Remove(quantityError);
                    if(!(e.NewValue is int) || (int)e.NewValue <= 0) {
                        e.IsValid = false;
                        e.ErrorMessage = string.Format(PartsPurchasingUIStrings.DataGridView_Validation_InternalAcquisitionDetail_QuantityIsMustGreatThanZero, internalAcquisitionDetail.SparePartCode);
                    }
                    break;
            }
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var internalAcquisitionBill = this.DataContext as InternalAcquisitionBill;
            if(internalAcquisitionBill == null)
                return;
            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "Quantity":
                case "SparePartCode":
                    internalAcquisitionBill.TotalAmount = internalAcquisitionBill.InternalAcquisitionDetails.Sum(r => r.Quantity * r.UnitPrice);
                    break;
            }
        }

        private void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var internalAcquisitionBill = this.DataContext as InternalAcquisitionBill;
            if(internalAcquisitionBill == null)
                return;

            internalAcquisitionBill.TotalAmount = internalAcquisitionBill.InternalAcquisitionDetails.Sum(r => r.Quantity * r.UnitPrice);
            var searial = 0;
            foreach(var detail in internalAcquisitionBill.InternalAcquisitionDetails)
                detail.SerialNumber = ++searial;
        }

        #endregion

        protected override Type EntityType {
            get {
                return typeof(InternalAcquisitionDetail);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name="ReferenceCodeQuery",
                        Title = PartsPurchasingUIStrings.DataGridView_Column_ReferenceCodeQuery,
                        IsReadOnly=true
                    }, new KeyValuesColumnItem {
                        Name = "IsOrderable",
                        IsReadOnly = true,
                        Title = PartsPurchasingUIStrings.DataGridView_Column_IsOrderable,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Quantity",
                        TextAlignment=TextAlignment.Right
                    }, new ColumnItem {
                        Name = "UnitPrice",
                        IsReadOnly = true,
                        TextAlignment=TextAlignment.Right
                    }, new ColumnItem {
                        Name = "SalesPrice",
                        IsReadOnly = true,
                        TextAlignment=TextAlignment.Right
                    },new ColumnItem {
                        Name = "MeasureUnit",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("InternalAcquisitionDetails");
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            this.GridView.CellValidating += GridView_CellValidating;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
            this.GridView.Deleted += GridView_Deleted;
            this.GridView.RowStyleSelector = this.GridViewStyleSelector();
        }

        private GridViewStyleSelector GridViewStyleSelector() {
            return new GridViewStyleSelector {
                ExecuteFunc = (item, container) => {
                    if(item is InternalAcquisitionDetail) {
                        var internalacquisitiondetail = item as InternalAcquisitionDetail;
                        if(PartsBranch_IsOrderable.Contains(internalacquisitiondetail.IsOrderable))
                            return this.Resources["lightRedDataBackground"] as Style;
                    }
                    return null;
                }
            };
        }
    }
}
