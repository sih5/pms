﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class InternalAllocationBillDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "InternalAllocationBill_Status","InternalAllocationBill_Type"
        };

        public InternalAllocationBillDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Title = "是否有附件",
                        Name = "IsUplodFile"
                    }, new ColumnItem {
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_InternalAllocationBill_WarehouseName,
                        Name = "WarehouseName"
                    }, new ColumnItem {
                        Name = "DepartmentName",
                        Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_OutDepartmentName
                    }, new ColumnItem {
                        Name = "TotalAmount"
                    }, new ColumnItem {
                        Name = "Reason"
                    }, new ColumnItem {
                        Name = "Operator"
                    }, new KeyValuesColumnItem {
                        Name = "Type",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PurchaseSettleInvoiceResource_Type,
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Name = "SourceCode",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PurchaseSettleInvoiceResource_SourceCode,
                    },new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    },new ColumnItem {
                        Name = "ModifierName"
                    },new ColumnItem {
                        Name = "ModifyTime"
                    },new ColumnItem {
                        Title = "审核人",
                        Name = "CheckerName"
                    },new ColumnItem {
                        Title = "审核时间",
                        Name = "CheckTime"
                    },new ColumnItem {
                        Title = "审批人",
                        Name = "ApproverName"
                    },new ColumnItem {
                        Title = "审批时间",
                        Name = "ApproveTime"
                    },new ColumnItem {
                        Name = "AbandonerName"
                    },new ColumnItem {
                        Name = "AbandonTime"
                    }, new ColumnItem {
                        Name = "BranchName"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
        }

        protected override Type EntityType {
            get {
                return typeof(InternalAllocationBill);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetInternalAllocationBills";
        }

        protected override Core.IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "InternalAllocationDetail"
                    }
                };
            }
        }
    }
}
