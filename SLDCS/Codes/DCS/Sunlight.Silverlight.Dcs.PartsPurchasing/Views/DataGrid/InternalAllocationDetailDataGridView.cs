﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class InternalAllocationDetailDataGridView : DcsDataGridViewBase {
        public InternalAllocationDetailDataGridView() {
            this.DataContextChanged += this.InternalAllocationDetailDataGridView_DataContextChanged;
        }

        protected virtual void InternalAllocationDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var internalAllocationBill = e.NewValue as InternalAllocationBill;
            if(internalAllocationBill == null || internalAllocationBill.Id == default(int))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "InternalAllocationBillId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                };
            this.FilterItem.Value = internalAllocationBill.Id;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                       Name = "SerialNumber"
                    },new ColumnItem {
                       Name = "SparePartCode"
                    },new ColumnItem {
                       Name = "SparePartName"
                    },new ColumnItem {
                       Name = "Quantity"
                    },new ColumnItem {
                       Name = "UnitPrice"
                    }
                    //,new ColumnItem {
                    //   Name = "SalesPrice"
                    //}
                    ,new ColumnItem {
                       Name = "MeasureUnit",
                    },new ColumnItem {
                       Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(InternalAllocationDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetInternalAllocationDetails";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
