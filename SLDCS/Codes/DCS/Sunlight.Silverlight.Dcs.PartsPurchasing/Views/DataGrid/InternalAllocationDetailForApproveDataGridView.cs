﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class InternalAllocationDetailForApproveDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                       Name = "SerialNumber"
                    },new ColumnItem {
                       Name = "SparePartCode"
                    },new ColumnItem {
                       Name = "SparePartName"
                    },new ColumnItem {
                       Name = "Quantity" 
                    },new ColumnItem {
                       Name = "UnitPrice"
                    }
                    //,new ColumnItem {
                    //   Name = "SalesPrice"
                    //}
                    ,new ColumnItem {
                       Name = "MeasureUnit",
                    },new ColumnItem {
                       Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(InternalAllocationDetail);
            }
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("InternalAllocationDetails");
        }
    }
}
