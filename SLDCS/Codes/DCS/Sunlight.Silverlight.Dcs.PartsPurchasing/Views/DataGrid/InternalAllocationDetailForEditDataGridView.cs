﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class InternalAllocationDetailForEditDataGridView : DcsDataGridViewBase {
        private DcsMultiPopupsQueryWindowBase warehousePartsStockQueryWindow;
        private RadWindow radWindow;
        private DcsMultiPopupsQueryWindowBase WarehousePartsStockQueryWindow {
            get {
                if(this.warehousePartsStockQueryWindow == null) {
                    this.warehousePartsStockQueryWindow = DI.GetQueryWindow("WarehousePartsStockDetails") as DcsMultiPopupsQueryWindowBase;
                    this.warehousePartsStockQueryWindow.Loaded += this.warehousePartsStockQueryWindow_Loaded;
                    this.warehousePartsStockQueryWindow.SelectionDecided += warehousePartsStockQueryWindow_SelectionDecided;
                }
                return warehousePartsStockQueryWindow;
            }
        }
        private void warehousePartsStockQueryWindow_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var internalAllocationBill = this.DataContext as InternalAllocationBill;
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(internalAllocationBill == null || queryWindow == null)
                return;
            //获取 根据所选仓库Id 获取 仓库与销售组织关系。 在获取 配件销售类型Id  传入弹出框。获取唯一的价格属性
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "WarehouseId", internalAllocationBill.WarehouseId
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "WarehouseId", false
            });
        }

        private RadWindow SelectedWindow {
            get {
                if(radWindow == null) {
                    radWindow = new RadWindow();
                    radWindow.Header = PartsPurchasingUIStrings.QueryPanel_Title_WarehousePartsStockDetails;
                    radWindow.Content = this.WarehousePartsStockQueryWindow;
                    radWindow.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                }
                return radWindow;
            }
        }

        private void warehousePartsStockQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var internalAllocationBill = this.DataContext as InternalAllocationBill;
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            var domainContext = this.DomainContext as DcsDomainContext;
            if(internalAllocationBill == null || queryWindow == null || domainContext == null)
                return;

            if(queryWindow.SelectedEntities == null || !queryWindow.SelectedEntities.Any() || queryWindow.SelectedEntities.Cast<WarehousePartsStock>() == null)
                return;

            var warehousePartsStocks = queryWindow.SelectedEntities.Cast<WarehousePartsStock>();
            if(warehousePartsStocks == null)
                return;
            if(internalAllocationBill.InternalAllocationDetails.Any(r => warehousePartsStocks.Any(ex => ex.SparePartId == r.SparePartId))) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_InternalAllocationDetail_SparePartCodeIsAlreadyHave);
                return;
            }
            foreach(var warehousePartsStock in warehousePartsStocks) {
                var serialNumber = internalAllocationBill.InternalAllocationDetails.Count();
                serialNumber++;

                var internalAllocationDetail = new InternalAllocationDetail {
                    InternalAllocationBill = internalAllocationBill,
                    SerialNumber = serialNumber,
                    SparePartId = warehousePartsStock.SparePartId,
                    SparePartCode = warehousePartsStock.SparePartCode,
                    SparePartName = warehousePartsStock.SparePartName,
                    Quantity = default(int),
                    UnitPrice = warehousePartsStock.CostPrice,
                    SalesPrice = warehousePartsStock.PartsSalesPrice,
                    MeasureUnit = warehousePartsStock.MeasureUnit,
                };
                internalAllocationBill.InternalAllocationDetails.Add(internalAllocationDetail);
            }
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartCode",
                        //DropDownContent = this.WarehousePartsStockDropQueryWindow,
                        //IsEditable = false
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Quantity"
                    }, new ColumnItem {
                        Name = "UnitPrice",
                        IsReadOnly = true
                    }
                    //,  new ColumnItem {
                    //    Name = "SalesPrice",
                    //    Title="服务站价",
                    //    IsReadOnly = true
                    //}
                    , new ColumnItem {
                        Name = "MeasureUnit",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(InternalAllocationDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("InternalAllocationDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            this.GridView.CellValidating += GridView_CellValidating;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
            this.GridView.Deleted += GridView_Deleted;
        }

        private void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var internalAllocationBill = this.DataContext as InternalAllocationBill;
            if(internalAllocationBill == null)
                return;

            internalAllocationBill.TotalAmount = internalAllocationBill.InternalAllocationDetails.Sum(r => r.Quantity * r.UnitPrice);
            var searial = 0;
            foreach(var detail in internalAllocationBill.InternalAllocationDetails) {
                searial += 1;
                detail.SerialNumber = searial;
            }
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var internalAllocationBill = this.DataContext as InternalAllocationBill;
            if(internalAllocationBill == null)
                return;

            e.Cancel = true;
            if(internalAllocationBill.WarehouseId == default(int)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataGridView_Validation_InternalAllocationBill_WarehouseIdIsRequired);
                return;
            }
            var dcsDomainContext = this.DomainContext as DcsDomainContext;
            if(dcsDomainContext == null)
                return;

            this.SelectedWindow.ShowDialog();

        }

        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
            var internalAllocationDetail = e.Cell.DataContext as InternalAllocationDetail;
            if(internalAllocationDetail == null)
                return;

            switch(e.Cell.Column.UniqueName) {
                case "Quantity":
                    var quantityError = internalAllocationDetail.ValidationErrors.FirstOrDefault(r => r.MemberNames.Contains("Quantity"));
                    if(quantityError != null)
                        internalAllocationDetail.ValidationErrors.Remove(quantityError);
                    if(!(e.NewValue is int) || (int)e.NewValue <= 0) {
                        e.IsValid = false;
                        e.ErrorMessage = string.Format(PartsPurchasingUIStrings.DataGridView_Validation_InternalAllocationDetail_QuantityIsMustGreatThanZero, internalAllocationDetail.SparePartCode);
                    }
                    break;
            }
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var internalAllocationBill = this.DataContext as InternalAllocationBill;
            if(internalAllocationBill == null)
                return;

            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "Quantity":
                case "SparePartCode":
                    internalAllocationBill.TotalAmount = internalAllocationBill.InternalAllocationDetails.Sum(r => r.Quantity * r.UnitPrice);
                    break;
            }
        }
    }
}
