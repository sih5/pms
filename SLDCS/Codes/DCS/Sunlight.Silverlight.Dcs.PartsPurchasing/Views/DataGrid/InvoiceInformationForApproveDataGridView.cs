﻿using System;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class InvoiceInformationForApproveDataGridView : DcsDataGridViewBase {

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly=true
                    },new ColumnItem {
                        Name = "InvoiceCode",
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "InvoiceNumber",
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "InvoiceAmount",
                        IsReadOnly=true,
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_InvoiceInformation_InvoiceAmount
                    }, new ColumnItem {
                        Name = "TaxRate",
                        IsReadOnly =true
                    }, new ColumnItem {
                        TextAlignment = TextAlignment.Right,
                        Name = "InvoiceTax",
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "InvoiceDate",
                        IsReadOnly=true
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            ((GridViewDataColumn)this.GridView.Columns["InvoiceTax"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["InvoiceDate"]).DataFormatString = "d";
            base.OnControlsCreated();
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("InvoiceInformations");
        }

        protected override Type EntityType {
            get {
                return typeof(InvoiceInformation);
            }
        }
    }
}
