﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class InvoiceInformationForPartsPurchaseSettleBillAllAddDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "Code",
                        Title = PartsPurchasingUIStrings.DataEditView_Text_InvoiceInformation_Code,
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "TotalSettlementAmount",
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "TaxRate",
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "Tax",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurchaseSettleBill);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsPurchaseSettleBills");
        }

        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.Deleting -= GridView_Deleting;
            this.GridView.Deleting += GridView_Deleting;
            ((GridViewDataColumn)this.GridView.Columns["Tax"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["TotalSettlementAmount"]).DataFormatString = "c2";
            base.OnControlsCreated();
        }

        private void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            var serialNumber = 1;
            foreach(var information in this.GridView.Items.Cast<PartsPurchaseSettleBill>().Where(entity => !e.Items.Contains(entity)))
                information.SerialNumber = serialNumber++;
            var partsPurchaseSettleBillMultiInvoiceRegisterDataEditView = this.DataContext as PartsPurchaseSettleBillMultiInvoiceRegisterDataEditView;
            if(partsPurchaseSettleBillMultiInvoiceRegisterDataEditView == null)
                return;
            var invoiceInformation = partsPurchaseSettleBillMultiInvoiceRegisterDataEditView.DataContext as InvoiceInformation;
            if(invoiceInformation == null)
                return;
            var partsPurchaseSettleBill = this.GridView.SelectedItem as PartsPurchaseSettleBill;
            partsPurchaseSettleBillMultiInvoiceRegisterDataEditView.PartsPurchaseSettleBills.Remove(partsPurchaseSettleBill);
            invoiceInformation.InvoiceAmount = partsPurchaseSettleBillMultiInvoiceRegisterDataEditView.PartsPurchaseSettleBills.Sum(r => r.TotalSettlementAmount);
            if(invoiceInformation.TaxRate != null)
                invoiceInformation.InvoiceTax = Math.Round((decimal)((invoiceInformation.InvoiceAmount / (decimal)(1 + invoiceInformation.TaxRate)) * (decimal)invoiceInformation.TaxRate), 2);
        }

    }
}
