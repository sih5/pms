﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class InvoiceInformationForPartsPurchaseSettleBillAllDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "Code",
                        Title = PartsPurchasingUIStrings.DataEditView_Text_InvoiceInformation_Code,
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "TotalSettlementAmount",
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "TaxRate",
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "Tax",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurchaseSettleBill);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsPurchaseSettleBills");
        }

        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.Deleting -= GridView_Deleting;
            this.GridView.Deleting += GridView_Deleting;
            this.GridView.AddingNewDataItem -= this.GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            ((GridViewDataColumn)this.GridView.Columns["Tax"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["TotalSettlementAmount"]).DataFormatString = "c2";
            base.OnControlsCreated();
        }

        private void GridView_AddingNewDataItem(object sender, Telerik.Windows.Controls.GridView.GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            RadQueryWindow.ShowDialog();

        }

        private void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            var serialNumber = 1;
            foreach(var information in this.GridView.Items.Cast<PartsPurchaseSettleBill>().Where(entity => !e.Items.Contains(entity)))
                information.SerialNumber = serialNumber++;
            var partsPurchaseSettleBillMultiInvoiceEditDataEditView = this.DataContext as PartsPurchaseSettleBillMultiInvoiceEditDataEditView;
            if(partsPurchaseSettleBillMultiInvoiceEditDataEditView == null)
                return;
            var invoiceInformation = partsPurchaseSettleBillMultiInvoiceEditDataEditView.DataContext as InvoiceInformation;
            if(invoiceInformation == null)
                return;
            var partsPurchaseSettleBill = this.GridView.SelectedItem as PartsPurchaseSettleBill;
            partsPurchaseSettleBillMultiInvoiceEditDataEditView.PartsPurchaseSettleBills.Remove(partsPurchaseSettleBill);
            invoiceInformation.InvoiceAmount = partsPurchaseSettleBillMultiInvoiceEditDataEditView.PartsPurchaseSettleBills.Sum(r => r.TotalSettlementAmount);
            invoiceInformation.InvoiceTax = Math.Round((decimal)((invoiceInformation.InvoiceAmount / (decimal)(1 + invoiceInformation.TaxRate)) * (decimal)invoiceInformation.TaxRate), 2);
        }

        private DcsMultiPopupsQueryWindowBase partsPurchaseSettleBillForSelectQueryWindow;
        private DcsMultiPopupsQueryWindowBase PartsPurchaseSettleBillForSelectQueryWindow {
            get {
                partsPurchaseSettleBillForSelectQueryWindow = DI.GetQueryWindow("PartsPurchaseSettleBillForSelectMulti") as DcsMultiPopupsQueryWindowBase;
                if(partsPurchaseSettleBillForSelectQueryWindow != null) {
                    partsPurchaseSettleBillForSelectQueryWindow.SelectionDecided += partsPurchaseSettleBillForSelectQueryWindow_SelectionDecided;
                    partsPurchaseSettleBillForSelectQueryWindow.Loaded += partsPurchaseSettleBillForSelectQueryWindow_Loaded;
                }
                return this.partsPurchaseSettleBillForSelectQueryWindow;
            }
        }

        private void partsPurchaseSettleBillForSelectQueryWindow_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;
            var dataContext = this.DataContext as PartsPurchaseSettleBillMultiInvoiceEditDataEditView;
            if(dataContext == null)
                return;
            //var query = this.PartsPurchaseSettleBillForSelectQueryWindow as PartsPurchaseSettleBillForSelectMultiQueryWindow;
            //if(query != null) {
            //    query.SetQueryPanelParameters(dataContext.BranchId);
            //}
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem("PartsSalesCategoryId", typeof(int), FilterOperator.IsEqualTo, dataContext.PartsSalesCategoryId));
            compositeFilterItem.Filters.Add(new FilterItem("PartsSupplierId", typeof(int), FilterOperator.IsEqualTo, dataContext.SupplierId));
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "PartsSupplierCode", dataContext.PartsSupplierCode
            });
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "PartsSupplierName", dataContext.PartsSupplierName
            });
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "Status", (int)DcsPartsPurchaseSettleStatus.已审批
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "PartsSupplierCode", false
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "PartsSupplierName", false
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "Status", false
            });
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }

        private void partsPurchaseSettleBillForSelectQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var domainContext = new DcsDomainContext();
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var partsPurchaseSettleBillMultiInvoiceEditDataEditView = this.DataContext as PartsPurchaseSettleBillMultiInvoiceEditDataEditView;
            if(partsPurchaseSettleBillMultiInvoiceEditDataEditView == null)
                return;
            var invoiceInformation = partsPurchaseSettleBillMultiInvoiceEditDataEditView.DataContext as InvoiceInformation;
            if(invoiceInformation == null)
                return;
            var partsPurchaseSettleBills = queryWindow.SelectedEntities.Cast<VirtualPartsPurchaseSettleBillWithSumPlannedPrice>();
            if(partsPurchaseSettleBills == null)
                return;
            if(partsPurchaseSettleBillMultiInvoiceEditDataEditView.PartsPurchaseSettleBills.Any(r => partsPurchaseSettleBills.Any(ex => ex.Code == r.Code))) {
                UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Error_InvoiceInformation_DoublePartsPurchaseSettleBill));
                return;
            }
            var ids = partsPurchaseSettleBills.Select(r => r.Id).ToArray();
            domainContext.Load(domainContext.GetPartsPurchaseSettleBillByIdsQuery(ids), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entitys = loadOp.Entities.ToArray();
                if(entitys != null) {
                    foreach(var item in entitys) {
                        item.SerialNumber = this.GridView.Items.Count > 0 ? this.GridView.Items.Cast<PartsPurchaseSettleBill>().Max(entity => entity.SerialNumber) + 1 : 1;
                        partsPurchaseSettleBillMultiInvoiceEditDataEditView.PartsPurchaseSettleBills.Add(item);
                    }
                    invoiceInformation.InvoiceAmount = partsPurchaseSettleBillMultiInvoiceEditDataEditView.PartsPurchaseSettleBills.Sum(r => r.TotalSettlementAmount);
                    invoiceInformation.InvoiceTax = Math.Round((decimal)((invoiceInformation.InvoiceAmount / (decimal)(1 + invoiceInformation.TaxRate)) * (decimal)invoiceInformation.TaxRate), 2);
                }
            }, null);
        }

        private RadWindow radQueryWindow;
        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = PartsPurchaseSettleBillForSelectQueryWindow,
                    Header = PartsPurchasingUIStrings.QueryPanel_Title_PartsPurchaseSettleBillForSelect,
                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                });
            }
        }
    }
}

