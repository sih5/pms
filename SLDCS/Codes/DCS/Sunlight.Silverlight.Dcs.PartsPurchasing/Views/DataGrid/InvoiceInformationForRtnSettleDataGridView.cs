﻿﻿using System;
﻿using System.Linq;
﻿using System.Windows;
﻿using System.Windows.Data;
﻿using Sunlight.Silverlight.Core.Model;
﻿using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
﻿using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class InvoiceInformationForRtnSettleDataGridView : DcsDataGridViewBase {
        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly=true
                    },new ColumnItem {
                        Name = "InvoiceCode"
                    }, new ColumnItem {
                        Name = "InvoiceNumber"
                    }, new ColumnItem {
                        Name = "InvoiceAmount",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_InvoiceInformation_InvoiceAmount
                    }, new ColumnItem {
                        Name = "TaxRate",
                        IsReadOnly =true
                    }, new ColumnItem {
                        TextAlignment = TextAlignment.Right,
                        MaskType = Core.Model.MaskType.Numeric,
                        FormatString = "c2",
                        Name = "InvoiceTax"
                    }, new ColumnItem {
                        Name = "InvoiceDate"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(InvoiceInformation);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.AddingNewDataItem -= GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            this.GridView.CellEditEnded -= GridView_CellEditEnded;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            this.GridView.Deleting -= this.GridView_Deleting;
            this.GridView.Deleting += this.GridView_Deleting;
        }

        void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var invoiceInformation = e.Cell.DataContext as InvoiceInformation;
            if(invoiceInformation == null)
                return;

            switch(e.Cell.Column.UniqueName) {
                case "InvoiceAmount":
                    if(invoiceInformation.TaxRate != null && (1 + invoiceInformation.TaxRate) != 0) {
                        var tmpInvoiceTax = (invoiceInformation.InvoiceAmount / (decimal)(1 + invoiceInformation.TaxRate)) * (decimal)invoiceInformation.TaxRate;
                        invoiceInformation.InvoiceTax = decimal.Parse(Convert.ToDouble(tmpInvoiceTax).ToString("f2"));
                    }
                    break;
            }
        }

        private void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            var serialNumber = 1;
            foreach(var information in this.GridView.Items.Cast<InvoiceInformation>())
                information.SerialNumber = serialNumber++;
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var frameworkElement = this.DataContext as FrameworkElement;
            if(frameworkElement == null)
                return;
            var partsPurchaseRtnSettleBill = frameworkElement.DataContext as PartsPurchaseRtnSettleBill;
            if(partsPurchaseRtnSettleBill == null)
                return;

            var invoiceInformation = new InvoiceInformation();
            invoiceInformation.Code = GlobalVar.ASSIGNED_BY_SERVER;
            invoiceInformation.SerialNumber = this.GridView.Items.Count > 0 ? this.GridView.Items.Cast<InvoiceInformation>().Max(entity => entity.SerialNumber) + 1 : 1;
            invoiceInformation.Status = (int)DcsInvoiceInformationStatus.已开票;
            invoiceInformation.InvoiceDate = DateTime.Now;
            invoiceInformation.TaxRate = partsPurchaseRtnSettleBill.TaxRate;
            invoiceInformation.InvoicePurpose = (int)DcsInvoiceInformationInvoicePurpose.配件采购;
            if(partsPurchaseRtnSettleBill.InvoicePath == (int)DcsPartsPurchaseRtnSettleBillInvoicePath.开红字发票) {
                invoiceInformation.InvoiceCompanyId = partsPurchaseRtnSettleBill.PartsSupplierId;
                invoiceInformation.InvoiceCompanyCode = partsPurchaseRtnSettleBill.PartsSupplierCode;
                invoiceInformation.InvoiceCompanyName = partsPurchaseRtnSettleBill.PartsSupplierName;
                invoiceInformation.InvoiceReceiveCompanyId = partsPurchaseRtnSettleBill.BranchId;
                invoiceInformation.InvoiceReceiveCompanyCode = partsPurchaseRtnSettleBill.BranchCode;
                invoiceInformation.InvoiceReceiveCompanyName = partsPurchaseRtnSettleBill.BranchName;
            }
            if(partsPurchaseRtnSettleBill.InvoicePath == (int)DcsPartsPurchaseRtnSettleBillInvoicePath.反开销售发票) {
                invoiceInformation.InvoiceCompanyId = partsPurchaseRtnSettleBill.BranchId;
                invoiceInformation.InvoiceCompanyCode = partsPurchaseRtnSettleBill.BranchCode;
                invoiceInformation.InvoiceCompanyName = partsPurchaseRtnSettleBill.BranchName;
                invoiceInformation.InvoiceReceiveCompanyId = partsPurchaseRtnSettleBill.PartsSupplierId;
                invoiceInformation.InvoiceReceiveCompanyCode = partsPurchaseRtnSettleBill.PartsSupplierCode;
                invoiceInformation.InvoiceReceiveCompanyName = partsPurchaseRtnSettleBill.PartsSupplierName;
            }
            invoiceInformation.Type = (int)DcsInvoiceInformationType.增值税发票;
            invoiceInformation.SourceId = partsPurchaseRtnSettleBill.Id;
            invoiceInformation.SourceCode = partsPurchaseRtnSettleBill.Code;
            invoiceInformation.SourceType = (int)DcsInvoiceInformationSourceType.配件采购退货结算单;
            invoiceInformation.OwnerCompanyId = partsPurchaseRtnSettleBill.BranchId;
            e.NewObject = invoiceInformation;
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("InvoiceInformations");
        }
    }
}
