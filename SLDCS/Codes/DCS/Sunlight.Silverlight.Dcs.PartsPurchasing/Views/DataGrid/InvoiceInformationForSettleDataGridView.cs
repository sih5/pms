﻿﻿using System;
 ﻿using System.Linq;
using System.Windows;
﻿using System.Windows.Data;
﻿using Sunlight.Silverlight.Core.Model;
﻿using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class InvoiceInformationForSettleDataGridView : DcsDataGridViewBase {
        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly=true
                    },new ColumnItem {
                        Name = "InvoiceCode"
                    }, new ColumnItem {
                        Name = "InvoiceNumber"
                    }, new ColumnItem {
                        Name = "InvoiceAmount",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_InvoiceInformation_InvoiceAmount,
                        TextAlignment = TextAlignment.Right,
                         FormatString="c2"
                    }, new ColumnItem {
                        Name = "TaxRate",
                        IsReadOnly =true
                    }, new ColumnItem {
                        Name = "InvoiceTax",
                        TextAlignment = TextAlignment.Right,
                        FormatString="c2"
                    }, new ColumnItem {
                        Name = "InvoiceDate"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(InvoiceInformation);
            }
        }

        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            this.GridView.Deleting += this.GridView_Deleting;
            ((GridViewDataColumn)this.GridView.Columns["InvoiceDate"]).DataFormatString = "d";
            base.OnControlsCreated();
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var invoiceInformation = e.Cell.DataContext as InvoiceInformation;
            if(invoiceInformation == null)
                return;

            switch(e.Cell.Column.UniqueName) {
                case "InvoiceAmount":
                    if(invoiceInformation.TaxRate != null && invoiceInformation.InvoiceAmount != null)
                        invoiceInformation.InvoiceTax = Math.Round((decimal)((invoiceInformation.InvoiceAmount / (decimal)(1 + invoiceInformation.TaxRate)) * (decimal)invoiceInformation.TaxRate), 2);
                    break;
            }
        }

        private void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            var ss = this.DataContext as FrameworkElement;
            if(ss != null) {
                var partsPurchaseSettleBill = ss.DataContext as PartsPurchaseSettleBill;
                if(partsPurchaseSettleBill == null)
                    return;
                var serialNumber = 1;
                foreach(var information in this.GridView.Items.Cast<InvoiceInformation>())
                    information.SerialNumber = serialNumber++;
                foreach(var invoiceInformation in e.Items.Cast<InvoiceInformation>())
                    if(invoiceInformation.Id != default(int)) {
                        if(invoiceInformation.Can删除采购发票)
                            invoiceInformation.删除采购发票(partsPurchaseSettleBill.Id);
                    }
            }
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var frameworkElement = this.DataContext as FrameworkElement;
            if(frameworkElement != null) {
                var partsPurchaseSettleBill = frameworkElement.DataContext as PartsPurchaseSettleBill;
                if(partsPurchaseSettleBill == null)
                    return;

                e.NewObject = new InvoiceInformation {
                    Code = GlobalVar.ASSIGNED_BY_SERVER,
                    SerialNumber = this.GridView.Items.Count > 0 ? this.GridView.Items.Cast<InvoiceInformation>().Max(entity => entity.SerialNumber) + 1 : 1,
                    TaxRate = partsPurchaseSettleBill.TaxRate,
                    InvoicePurpose = (int)DcsInvoiceInformationInvoicePurpose.配件采购,
                    InvoiceCompanyId = partsPurchaseSettleBill.PartsSupplierId,
                    InvoiceCompanyCode = partsPurchaseSettleBill.PartsSupplierCode,
                    InvoiceCompanyName = partsPurchaseSettleBill.PartsSupplierName,
                    InvoiceReceiveCompanyId = partsPurchaseSettleBill.BranchId,
                    InvoiceReceiveCompanyCode = partsPurchaseSettleBill.BranchCode,
                    InvoiceReceiveCompanyName = partsPurchaseSettleBill.BranchName,
                    Type = (int)DcsInvoiceInformationType.增值税发票,
                    SourceId = partsPurchaseSettleBill.Id,
                    SourceType = (int)DcsInvoiceInformationSourceType.配件采购结算单,
                    SourceCode = partsPurchaseSettleBill.Code,
                    Status = (int)DcsInvoiceInformationStatus.已开票,
                    OwnerCompanyId = partsPurchaseSettleBill.BranchId,
                };
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("InvoiceInformations");
        }
    }
}
