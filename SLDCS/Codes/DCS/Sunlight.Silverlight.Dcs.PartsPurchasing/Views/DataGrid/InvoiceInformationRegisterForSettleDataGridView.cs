﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class InvoiceInformationRegisterForSettleDataGridView : DcsDataGridViewBase {
        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            //this.GridView.CellValidating += GridView_CellValidating;
            this.GridView.Deleting += this.GridView_Deleting;
            ((GridViewDataColumn)this.GridView.Columns["InvoiceDate"]).DataFormatString = "d";
            base.OnControlsCreated();
        }

        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
            var invoiceInformation = e.Cell.DataContext as InvoiceInformation;
            if(invoiceInformation == null)
                return;

            switch(e.Cell.Column.UniqueName) {
                case "InvoiceAmount":
                    int temp = invoiceInformation.InvoiceAmount.ToString().Substring(invoiceInformation.InvoiceAmount.ToString().IndexOf(".") + 1, invoiceInformation.InvoiceAmount.ToString().Length - invoiceInformation.InvoiceAmount.ToString().IndexOf(".") - 1).Length;
                    if(temp > 2) {
                        invoiceInformation.InvoiceAmount = Convert.ToDecimal(invoiceInformation.InvoiceAmount.ToString().Substring(0, invoiceInformation.InvoiceAmount.ToString().IndexOf(".") + 3));
                    } else {
                        invoiceInformation.InvoiceAmount = invoiceInformation.InvoiceAmount;
                    }
                    break;
            }
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var invoiceInformation = e.Cell.DataContext as InvoiceInformation;
            if(invoiceInformation == null)
                return;

            switch(e.Cell.Column.UniqueName) {
                case "InvoiceAmount":
                    if(invoiceInformation.TaxRate != null && invoiceInformation.InvoiceAmount != null)
                        invoiceInformation.InvoiceTax = Math.Round((decimal)((invoiceInformation.InvoiceAmount / (decimal)(1 + invoiceInformation.TaxRate)) * (decimal)invoiceInformation.TaxRate), 2);
                    break;
            }
        }

        private void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            var serialNumber = 1;
            foreach(var information in ((DataEdit.PartsPurchaseSettleBillInvoiceRegisterDataEditView)(this.DataContext)).InvoiceInformations)
                information.SerialNumber = serialNumber++;
            foreach(var invoiceInformation in ((DataEdit.PartsPurchaseSettleBillInvoiceRegisterDataEditView)(this.DataContext)).InvoiceInformations)
                if(invoiceInformation.Id != default(int))
                    if(invoiceInformation.Can删除发票信息)
                        invoiceInformation.删除发票信息();
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var frameworkElement = this.DataContext as FrameworkElement;
            
            if(frameworkElement != null) {
                var partsPurchaseSettleBill = frameworkElement.DataContext as PartsPurchaseSettleBill;
                if(partsPurchaseSettleBill == null)
                    return;
                e.NewObject = new InvoiceInformation {
                    Code = GlobalVar.ASSIGNED_BY_SERVER,
                    //SerialNumber = this.GridView.Items.Count > 0 ? this.GridView.Items.Cast<InvoiceInformation>().Max(entity => entity.SerialNumber) + 1 : 1,
                    SerialNumber = ((DataEdit.PartsPurchaseSettleBillInvoiceRegisterDataEditView)(this.DataContext)).InvoiceInformations.Count>0?((DataEdit.PartsPurchaseSettleBillInvoiceRegisterDataEditView)(this.DataContext)).InvoiceInformations.Count+1:1,
                    TaxRate = partsPurchaseSettleBill.TaxRate,
                    InvoicePurpose = (int)DcsInvoiceInformationInvoicePurpose.配件采购,
                    InvoiceCompanyId = partsPurchaseSettleBill.PartsSupplierId,
                    InvoiceCompanyCode = partsPurchaseSettleBill.PartsSupplierCode,
                    InvoiceCompanyName = partsPurchaseSettleBill.PartsSupplierName,
                    InvoiceReceiveCompanyId = partsPurchaseSettleBill.BranchId,
                    InvoiceReceiveCompanyCode = partsPurchaseSettleBill.BranchCode,
                    InvoiceReceiveCompanyName = partsPurchaseSettleBill.BranchName,
                    Type = (int)DcsInvoiceInformationType.增值税发票,
                    SourceId = partsPurchaseSettleBill.Id,
                    SourceType = (int)DcsInvoiceInformationSourceType.配件采购结算单,
                    SourceCode = partsPurchaseSettleBill.Code,
                    Status = (int)DcsInvoiceInformationStatus.已开票,
                    OwnerCompanyId = partsPurchaseSettleBill.BranchId,
                };
                if(partsPurchaseSettleBill.MutInvoiceStrategy.HasValue && !(bool)partsPurchaseSettleBill.MutInvoiceStrategy && this.GridView.Items.Count == 1)
                    e.Cancel = true;
                else
                    e.Cancel = false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("InvoiceInformations");
        }

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly=true
                    },new ColumnItem {
                        Name = "InvoiceCode"
                    }, new ColumnItem {
                        Name = "InvoiceNumber"
                    }, new AggregateColumnItem {
                        Name = "InvoiceAmount",
                        AggregateType=AggregateType.Sum,
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_InvoiceInformation_InvoiceAmount,
                        TextAlignment = TextAlignment.Right,
                        FormatString="c2"
                    }, new ColumnItem {
                        Name = "TaxRate",
                        IsReadOnly =true
                    }, new ColumnItem {
                        Name = "InvoiceTax",
                        TextAlignment = TextAlignment.Right,
                        FormatString="c2"
                    }, new ColumnItem {
                        Name = "InvoiceDate"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(InvoiceInformation);
            }
        }

    }
}