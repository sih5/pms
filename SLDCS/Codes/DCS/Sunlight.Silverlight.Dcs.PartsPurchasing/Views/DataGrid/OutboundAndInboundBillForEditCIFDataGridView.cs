﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class OutboundAndInboundBillForEditCIFDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsLogisticBatchBillDetail_BillType"
        };

        public OutboundAndInboundBillForEditCIFDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "BillCode",
                        IsReadOnly = true,
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_BillCode,
                    }, new KeyValuesColumnItem {
                        Name = "BillType",
                        IsReadOnly = true,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_BillType,
                    }, new ColumnItem {
                        Name = "SettlementAmount",
                        IsReadOnly = true,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_SettlementAmount,
                    }, new ColumnItem {
                        Name = "WarehouseName",
                        IsReadOnly = true,
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_WarehouseName,
                    }, new ColumnItem {
                        Name = "BillCreateTime",
                        IsReadOnly = true,
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_BillCreateTime,
                    }, new ColumnItem {
                        Name = "ReturnReason",
                        IsReadOnly = true,
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_ReturnReason
                    }, new ColumnItem {
                        Name = "CPPartsInboundCheckCode",
                        IsReadOnly = true,
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OriginalInBoundBill
                    }, new ColumnItem {
                        Name = "CPPartsPurchaseOrderCode",
                        IsReadOnly = true,
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OriginalPurchaseOrder
                    }

                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(OutboundAndInboundBill);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.CanUserInsertRows = false;
            this.DataPager.PageSize = 50;
            ((GridViewDataColumn)this.GridView.Columns["BillCreateTime"]).DataFormatString = "d";
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.Deleted += GridView_Deleted;
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "OutboundAndInboundDetail"
                    }
                };
            }
        }

        private void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var dataEditView = this.DataContext as PartsPurchaseSettleBillDataEditView;
            if(dataEditView == null)
                return;
            var number = 1;
            foreach(var data in dataEditView.OutboundAndInboundBills)
                data.SerialNumber = number++;
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("OutboundAndInboundBills");
        }
    }
}
