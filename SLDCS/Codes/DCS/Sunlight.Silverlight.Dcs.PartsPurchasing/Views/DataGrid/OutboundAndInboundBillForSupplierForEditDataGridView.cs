﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class OutboundAndInboundBillForSupplierForEditDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsLogisticBatchBillDetail_BillType"
        };

        public OutboundAndInboundBillForSupplierForEditDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "BillCode",
                        IsReadOnly = true,
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_BillCode,
                    }, new KeyValuesColumnItem {
                        Name = "BillType",
                        IsReadOnly = true,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_BillType,
                    }, new ColumnItem {
                        Name = "SettlementAmount",
                        IsReadOnly = true,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_SettlementAmount,
                    }, new ColumnItem {
                        Name = "BillCreateTime",
                        IsReadOnly = true,
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_BillCreateTime,
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(OutboundAndInboundBill);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.CanUserInsertRows = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Multiple;
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("OutboundAndInboundBills");
        }
    }
}
