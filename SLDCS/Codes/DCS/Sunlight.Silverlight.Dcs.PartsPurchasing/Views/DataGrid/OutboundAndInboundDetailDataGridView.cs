﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class OutboundAndInboundDetailDataGridView : DcsDataGridViewBase {
        int inboundId, outboundId, inboundType, outboundType;
        public OutboundAndInboundDetailDataGridView() {
            this.DataContextChanged += OutboundAndInboundDetailDataGridView_DataContextChanged;
        }

        private void OutboundAndInboundDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var outboundAndInboundBill = e.NewValue as OutboundAndInboundBill;
            if(outboundAndInboundBill == null)
                return;
            if(outboundAndInboundBill.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件入库检验单) {
                inboundId = outboundAndInboundBill.BillId;
                inboundType = outboundAndInboundBill.BillType;
                outboundId = default(int);
                outboundType = default(int);
            }
            if(outboundAndInboundBill.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件出库单) {
                outboundId = outboundAndInboundBill.BillId;
                outboundType = outboundAndInboundBill.BillType;
                inboundId = default(int);
                inboundType = default(int);
            }
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        Title= PartsPurchasingUIStrings.DataEditView_Text_SparePartCode,
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "SparePartName",
                        Title= PartsPurchasingUIStrings.DataEditView_Text_SparePartName,
                        IsReadOnly = true,
                    },new ColumnItem {
                        Name = "MeasureUnit",
                        Title= PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_MeasureUnit,
                        IsReadOnly = true,
                    }, new ColumnItem {
                        Name = "SettlementPrice",
                        IsReadOnly = true,
                        Title= PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SettlementPrice,
                        TextAlignment = TextAlignment.Right,
                    }, new ColumnItem {
                        Name = "Quantity",
                        Title= PartsPurchasingUIStrings.DataGridView_Column_Amount,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Remark",
                        IsReadOnly = true,
                        Title= PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_Remark
                    },
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            switch(parameterName) {
                case "inboundId":
                    return inboundId;
                case "outboundId":
                    return outboundId;
                case "inboundType":
                    return inboundType;
                case "outboundType":
                    return outboundType;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override string OnRequestQueryName() {
            return PartsPurchasingUIStrings.DataGridView_Title_InOutboundDetailQuery;
        }

        protected override Type EntityType {
            get {
                return typeof(OutboundAndInboundDetail);
            }
        }
    }
}
