﻿using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartABCDetailDetailDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = { 
            "ABCStrategy_Category" 
        };

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new ColumnItem[] {
                  new KeyValuesColumnItem{
                        Name = "PartABC",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = PartsPurchasingUIStrings.DataGridView_Title_PartABC
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartABCDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartABCDetails";
        }

        private void PartsPurchaseRtnSettleRefDataGridView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            //var productLifeCycleCategory = e.NewValue as ProductLifeCycleCategory;
            //if(productLifeCycleCategory == null || productLifeCycleCategory.Id == default(int))
            //    return;
            //var compositeFilterItem = new CompositeFilterItem();
            //compositeFilterItem.Filters.Add(new FilterItem {
            //    MemberName = "ProductLifeCycleCategoryId",
            //    MemberType = typeof(int),
            //    Operator = FilterOperator.IsEqualTo,
            //    Value = productLifeCycleCategory.Id
            //});
            //this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        public PartABCDetailDetailDataGridView() {
            this.DataContextChanged += PartsPurchaseRtnSettleRefDataGridView_DataContextChanged;
            this.KeyValueManager.Register(this.kvNames);

        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
        }
    }
}
