﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System.ServiceModel.DomainServices.Client;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsInboundCheckBillDetailForSelectDataGridView : DcsDataGridViewBase {

        public PartsInboundCheckBillDetailForSelectDataGridView() {
            this.DataContextChanged += PartsInboundCheckBillDetailForSelectDataGridView_DataContextChanged;
        }

        private void PartsInboundCheckBillDetailForSelectDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            this.ExecuteQuery();
        }

        /// <summary>
        /// 根据配件采购业务下， 分公司配件采购退货管理 节点设计需求。选择 配件检验入库单 时，同时带回 配件采购订单编号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridView_DataLoaded(object sender, EventArgs e) {
            if(this.GridView.Items.Count > 0) {
                var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
                if(partsPurchaseOrder == null)
                    return;
                var entitys = this.GridView.Items.Cast<PartsInboundCheckBillDetail>();
                foreach (var entity in entitys) { 
                    entity.OriginalRequirementBillCode = partsPurchaseOrder.Code;
                }
                 foreach (var entity in entitys) {
                     entity.AllInspectedQuantity = entitys.Where(r => r.SparePartId == entity.SparePartId).Sum(d => d.InspectedQuantity);
                }
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "PartsInboundCheckBill.Code",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBillDetail_PartsInboundCheckBill_Code
                    }, new ColumnItem {
                        Name = "PartsInboundCheckBill.WarehouseName",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBillDetail_PartsInboundCheckBill_WarehouseName
                    }, new ColumnItem {
                        Name = "SparePartCode"
                    }, new ColumnItem {
                        Name = "SparePartName"
                    }, new ColumnItem {
                        Name = "WarehouseAreaCode"
                    }, new ColumnItem {
                        Name = "InspectedQuantity"
                    }, new ColumnItem {
                        Name = "SettlementPrice"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.DataLoaded += this.GridView_DataLoaded;
        }

        protected override Type EntityType {
            get {
                return typeof(PartsInboundCheckBillDetail);
            }
        }
        //protected override void OnDataLoaded() {
        //    var partsInboundCheckBillDetails = this.Entities.Cast<PartsInboundCheckBillDetail>().ToArray();
        //    if(partsInboundCheckBillDetails == null) {
        //        return;
        //    }
        //    var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
        //    var partIds = partsInboundCheckBillDetails.Select(r => r.SparePartId).Distinct();
        //    if (partIds.Any() && partsPurchaseOrder != null) { 
        //        var dcsDomainContext = this.DomainContext as DcsDomainContext;
        //        dcsDomainContext.Load(dcsDomainContext.查询已退货数量Query(partsPurchaseOrder.Code,partIds.ToArray()), LoadBehavior.RefreshCurrent, loadOp => {
        //            if (loadOp.HasError) {
        //                if (!loadOp.IsErrorHandled)
        //                    loadOp.MarkErrorAsHandled();
        //                return;
        //            }
        //            var datas = loadOp.Entities.ToList();
        //            if (datas == null || !datas.Any())
        //                return;
        //            foreach (var data in datas) {
        //                var details = partsInboundCheckBillDetails.Where(r => r.SparePartId == data.SparePartId);
        //                var allInspectedQuantity = details.Sum(r => r.InspectedQuantity);
        //                foreach(var detail in details){
        //                    //可退货量
        //                    detail.ReturnableQuantity = allInspectedQuantity - data.ReturnedQuantity;
        //                }
        //            }
        //        }, null);
            
        //    }

        //}

        protected override string OnRequestQueryName() {
            return "按原始需求单据查询入库明细";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            switch(parameterName) {
                case "originalRequirementBillId":
                    var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
                    return partsPurchaseOrder != null ? partsPurchaseOrder.Id : default(int);
                case "originalRequirementBillType":
                    return (int)DcsOriginalRequirementBillType.配件采购订单;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
    }
}