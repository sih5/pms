﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using FilterOperator = Telerik.Windows.Data.FilterOperator;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsLogisticBatchItemDetailForSupplierShippingDetailEditDataGridView : DcsDataGridViewBase {
        private static readonly DependencyProperty SelectedItemProperty;
        private static readonly DependencyProperty MasterObjectProperty;

        static PartsLogisticBatchItemDetailForSupplierShippingDetailEditDataGridView() {
            SelectedItemProperty = DependencyProperty.Register("SelectedItem", typeof(object), typeof(PartsLogisticBatchItemDetailForSupplierShippingDetailEditDataGridView),
                new PropertyMetadata(null, OnSelectedItemPropertyChanged));
            MasterObjectProperty = DependencyProperty.Register("MasterObject", typeof(object), typeof(PartsLogisticBatchItemDetailForSupplierShippingDetailEditDataGridView),
                new PropertyMetadata(null, OnMasterObjectPropertyPropertyChanged));
        }

        private static void OnSelectedItemPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var dataGridView = d as PartsLogisticBatchItemDetailForSupplierShippingDetailEditDataGridView;
            if(dataGridView != null)
                dataGridView.OnSelectedItemPropertyChanged(e);
        }

        public PartsLogisticBatchItemDetailForSupplierShippingDetailEditDataGridView() {
            this.DataContextChanged += this.PartsLogisticBatchItemDetailForSupplierShippingDetailEditDataGridView_DataContextChanged;
        }

        private void OnSelectedItemPropertyChanged(DependencyPropertyChangedEventArgs e) {
            if(this.GridView == null)
                return;
            if(e.NewValue == null)
                return;
            this.GridView.FilterDescriptors.Clear();
            var shippingDetail = e.NewValue as SupplierShippingDetail;
            if(shippingDetail == null) {
                this.GridView.FilterDescriptors.Add(new FilterDescriptor("SparePartId", FilterOperator.IsEqualTo, -1));
                return;
            }
            this.SelectedItem = shippingDetail;
            this.GridView.FilterDescriptors.Add(new FilterDescriptor("SparePartId", FilterOperator.IsEqualTo, shippingDetail.SparePartId));
        }

        private static void OnMasterObjectPropertyPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var dataGridView = d as PartsLogisticBatchItemDetailForSupplierShippingDetailEditDataGridView;
            if(dataGridView != null)
                dataGridView.OnMasterObjectPropertyChanged(e);
        }

        private void OnMasterObjectPropertyChanged(DependencyPropertyChangedEventArgs e) {
            if(e.NewValue == null)
                return;
            var shippingOrder = e.NewValue as SupplierShippingOrder;
            if(shippingOrder == null)
                return;
            this.MasterObject = shippingOrder;
        }

        private object SelectedItem {
            get {
                return this.GetValue(SelectedItemProperty);
            }
            set {
                SetValue(SelectedItemProperty, value);
            }
        }

        private object MasterObject {
            get {
                return this.GetValue(MasterObjectProperty);
            }
            set {
                SetValue(MasterObjectProperty, value);
            }
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            switch(e.Cell.Column.UniqueName) {
                case "OutboundAmount":
                    var shippingDetail = this.SelectedItem as SupplierShippingDetail;
                    if(shippingDetail == null)
                        return;
                    var quantity = this.GridView.Items.OfType<PartsLogisticBatchItemDetail>().Sum(partsLogisticBatchItemDetail => partsLogisticBatchItemDetail.OutboundAmount);
                    shippingDetail.Quantity = quantity;
                    break;
            }
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            if(this.SelectedItem == null || this.MasterObject == null || !(this.SelectedItem is SupplierShippingDetail) || !(this.MasterObject is SupplierShippingOrder)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataGridView_Validation_PartsLogisticBatchItemDetail_SupplierShippingDetailIsNull);
                e.Cancel = true;
                return;
            }
            var shippingDetail = this.SelectedItem as SupplierShippingDetail;
            var shippingOrder = this.MasterObject as SupplierShippingOrder;
            //if(shippingDetail.SparePart == null || shippingDetail.SparePart.PartsBranch == null || !shippingDetail.SparePart.PartsBranch.IsManagedByBatch.GetValueOrDefault() || !shippingDetail.SparePart.PartsBranch.IsManagedBySerial.GetValueOrDefault()) {
            //    UIHelper.ShowNotification(PartsPurchasingUIStrings.DataGridView_Validation_PartsLogisticBatchItemDetail_CanNotAddBatchInfoIsNull);
            //    e.Cancel = true;
            //    return;
            //}
            e.NewObject = new PartsLogisticBatchItemDetail {
                SparePartId = shippingDetail.SparePartId,
                SparePartCode = shippingDetail.SparePartCode,
                SparePartName = shippingDetail.SparePartName,
                CounterpartCompanyId = shippingOrder.BranchId,
                ReceivingCompanyId = shippingOrder.ReceivingCompanyId.HasValue ? shippingOrder.ReceivingCompanyId.Value : default(int),
                ShippingCompanyId = shippingOrder.PartsSupplierId,
                InboundAmount = 0
            };
        }

        private void PartsLogisticBatchItemDetailForSupplierShippingDetailEditDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var dataGridViewBase = this.DataContext as DcsDataGridViewBase;
            if(dataGridViewBase == null)
                return;
            this.SetBinding(SelectedItemProperty, new Binding("SelectedItem") {
                Mode = BindingMode.TwoWay
            });
            this.SetBinding(MasterObjectProperty, new Binding("MasterObject"));
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.IsReadOnly = false;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.CanUserInsertRows = true;
            this.GridView.IsFilteringAllowed = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.CellEditEnded += this.GridView_CellEditEnded;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "BatchNumber",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsLogisticBatchItemDetail_BatchNumber
                    }, new ColumnItem {
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsLogisticBatchItemDetail_OutboundAmount,
                        Name = "OutboundAmount",
                        TextAlignment = TextAlignment.Right,
                        MaskType = Core.Model.MaskType.Numeric,
                        FormatString = "d"
                    }
                };
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsLogisticBatchItemDetails");
        }

        protected override Type EntityType {
            get {
                return typeof(PartsLogisticBatchItemDetail);
            }
        }
    }
}
