﻿
using System;
using System.Linq;
using System.Windows.Controls;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsOuterPurchaseBillDataGridView : DcsDataGridViewBase {
        protected override System.Collections.Generic.IEnumerable<Core.Model.ColumnItem> ColumnItems {
            get {
                return getColumnItem();
            }
        }
        private ColumnItem[] getColumnItem() {
            if(BaseApp.Current.CurrentUserData.EnterpriseCategoryId != (int)DcsCompanyType.代理库) {
                return new ColumnItem[] { 
                    new KeyValuesColumnItem{
                        Name = "Status",
                         KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem {
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_IsUplodFile,
                        Name = "IsUplodFile"
                    }, new ColumnItem{
                        Name = "Code"
                    }, new ColumnItem{
                        Name = "CustomerCompanyCode"
                    }, new ColumnItem{
                        Name = "CustomerCompanyNace"
                    }
                    , new ColumnItem{
                        Name = "MarketDepartmentName",
                        Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_MarketingDepartment
                    }
                    , new ColumnItem{
                        Name = "Amount"
                    }
                    , new KeyValuesColumnItem{
                        Name = "OuterPurchaseComment",
                        KeyValueItems=this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem{
                        Name = "CreatorName"
                    }, new ColumnItem{
                        Name = "CreateTime"
                    }, new ColumnItem{
                        Name = "ModifierName"
                    }, new ColumnItem{
                        Name = "ModifyTime"
                    }, new ColumnItem{
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_InternalAllocationBill_ApproverName,
                        Name = "ConfirmName"
                    }, new ColumnItem{
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_InternalAllocationBill_ApproveTime,
                        Name = "CONFIRMTIME"
                    }, new ColumnItem{
                        Name = "InitialApproverName"
                    }, new ColumnItem{
                        Name = "InitialApproveTime"
                    }, new ColumnItem{
                        Name = "CheckerName",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_Checker
                    }, new ColumnItem{
                        Name = "CheckTime",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_CheckTime
                    }, new ColumnItem{
                        Name = "ApproverName"
                    }, new ColumnItem{
                        Name = "ApproveTime"
                    }, new ColumnItem{
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_AdvancedAuditName,
                        Name = "AdvancedAuditName"
                    }, new ColumnItem{
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_AdvancedAuditTime,
                        Name = "AdvancedAuditTime"
                    }, new ColumnItem{
                        Title ="高级审批人",
                        Name = "SeniorApproveName"
                    }, new ColumnItem{
                        Title ="高级审批时间",
                        Name = "SeniorApproveTime"
                    }, new ColumnItem{
                        Name = "AbandonerName"
                    }, new ColumnItem{
                        Name = "AbandonTime"
                    }, new ColumnItem{
                        Name = "RejecterName"
                    }, new ColumnItem{
                        Name = "RejectTime"
                    },new ColumnItem{
                        Name = "Remark"
                    }, new ColumnItem{
                        Name = "BranchName"
                    }, new ColumnItem{
                        Name = "PartsSalesCategory.Name"
                    }, new ColumnItem{
                        Name = "ApproveComment"
                    }, new ColumnItem{
                        Name = "AbandonComment"
                    },new ColumnItem{
                        Name = "RejectComment"
                    }
                };
            } else {
                return new ColumnItem[] { 
                    new KeyValuesColumnItem{
                        Name = "Status",
                         KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem {
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_IsUplodFile,
                        Name = "IsUplodFile"
                    }, new ColumnItem{
                        Name = "Code"
                    }, new ColumnItem{
                        Name = "CustomerCompanyCode"
                    }, new ColumnItem{
                        Name = "CustomerCompanyNace"
                    }
                    
                    , new ColumnItem{
                        Name = "Amount"
                    }
                    , new KeyValuesColumnItem{
                        Name = "OuterPurchaseComment",
                        KeyValueItems=this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem{
                        Name = "CreatorName"
                    }, new ColumnItem{
                        Name = "CreateTime"
                    }, new ColumnItem{
                        Name = "ModifierName"
                    }, new ColumnItem{
                        Name = "ModifyTime"
                    }, new ColumnItem{
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_InternalAllocationBill_ApproverName,
                        Name = "ConfirmName"
                    }, new ColumnItem{
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_InternalAllocationBill_ApproveTime,
                        Name = "CONFIRMTIME"
                    }, new ColumnItem{
                        Name = "InitialApproverName"
                    }, new ColumnItem{
                        Name = "InitialApproveTime"
                    }, new ColumnItem{
                        Name = "CheckerName",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_Checker
                    }, new ColumnItem{
                        Name = "CheckTime",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_CheckTime
                    }, new ColumnItem{
                        Name = "ApproverName"
                    }, new ColumnItem{
                        Name = "ApproveTime"
                    }, new ColumnItem{
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_AdvancedAuditName,
                        Name = "AdvancedAuditName"
                    }, new ColumnItem{
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_AdvancedAuditTime,
                        Name = "AdvancedAuditTime"
                    }, new ColumnItem{
                        Title ="高级审批人",
                        Name = "SeniorApproveName"
                    }, new ColumnItem{
                        Title ="高级审批时间",
                        Name = "SeniorApproveTime"
                    }, new ColumnItem{
                        Name = "AbandonerName"
                    }, new ColumnItem{
                        Name = "AbandonTime"
                    }, new ColumnItem{
                        Name = "RejecterName"
                    }, new ColumnItem{
                        Name = "RejectTime"
                    },new ColumnItem{
                        Name = "Remark"
                    }, new ColumnItem{
                        Name = "BranchName"
                    }, new ColumnItem{
                        Name = "PartsSalesCategory.Name"
                    }, new ColumnItem{
                        Name = "ApproveComment"
                    }, new ColumnItem{
                        Name = "AbandonComment"
                    },new ColumnItem{
                        Name = "RejectComment"
                    }
                };
            }
        }
        private readonly string[] kvNames = { 
            "PartsOuterPurchase_OuterPurchaseComment","PartsOuterPurchaseChange_Status","OriginalRequirementBill_Type"
        };
        protected override Type EntityType {
            get {
                return typeof(PartsOuterPurchaseChange);
            }
        }
        protected override string OnRequestQueryName() {
            return "GetPartsOuterPurchaseChangeWithPartsSalesCategoryandRelationByMarketDepartmentId";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filterItem = this.FilterItem as CompositeFilterItem;
            if(filterItem == null || BaseApp.Current.CurrentUserData.EnterpriseCategoryId == (int)DcsCompanyType.代理库)
                return null;
            switch(parameterName) {
                case "marketDepartmentId":

                    var partsSalesCategoryFilterItem = (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.SingleOrDefault(e => e.MemberName == "MarketDepartmentId");
                    return partsSalesCategoryFilterItem == null ? null : partsSalesCategoryFilterItem.Value;

                default:
                    return null;
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem != null) {
                var param = new[] { "MarketDepartmentId" };
                foreach(var item in (compositeFilterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.Where(e => !param.Contains(e.MemberName))) {
                    newCompositeFilterItem.Filters.Add(item);
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }



        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.DataLoaded += GridView_DataLoaded;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        private void GridView_DataLoaded(object sender, EventArgs e) {
            if(this.GridView.Items.Count > 0 && BaseApp.Current.CurrentUserData.EnterpriseCategoryId != (int)DcsCompanyType.代理库) {
                var partsOuterPurchaseChanges = this.GridView.Items.Cast<PartsOuterPurchaseChange>();
                this.DomainContext.Load((this.DomainContext as DcsDomainContext).GetDealerServiceInfoByDealerIdsQuery(partsOuterPurchaseChanges.Select(r => r.CustomerCompanyId).ToArray()), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError)
                        return;
                    foreach(var partsOuterPurchaseChange in partsOuterPurchaseChanges) {
                        var dealerServiceInfo = loadOp.Entities.FirstOrDefault(r => r.DealerId == partsOuterPurchaseChange.CustomerCompanyId && r.PartsSalesCategoryId == partsOuterPurchaseChange.PartsSalesCategoryrId && r.BranchId == partsOuterPurchaseChange.BranchId);
                        if(dealerServiceInfo != null)
                            partsOuterPurchaseChange.MarketDepartmentName = dealerServiceInfo.MarketingDepartment.Name;
                    }
                }, null);

            }
        }

        public PartsOuterPurchaseBillDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override Core.IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsOuterPurchaseChange"
                    }
                };
            }
        }
    }
}
