﻿using System;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsOuterPurchaseBillReportDataGridView : DcsDataGridViewBase {

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                   new KeyValuesColumnItem{
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem{
                        Name = "Code",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsOuterPurchaseChange_Code
                    },  new ColumnItem{
                        Name = "Amount"
                    }                 
                    , new KeyValuesColumnItem{
                        Name = "OuterPurchaseComment",
                        KeyValueItems=this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem{
                        Name = "ApproveComment"
                    }, new ColumnItem{
                        Name = "AbandonComment"
                    },new ColumnItem{
                        Name = "RejectComment"
                    }, new ColumnItem{
                        Name = "CreatorName"
                    }, new ColumnItem{
                        Name = "CreateTime"
                    }, new ColumnItem{
                        Name = "ModifierName"
                    }, new ColumnItem{
                        Name = "ModifyTime"
                    }, new ColumnItem{
                        Name = "ApproverName"
                    }, new ColumnItem{
                        Name = "ApproveTime"
                    }, new ColumnItem{
                        Name = "AbandonerName"
                    }, new ColumnItem{
                        Name = "AbandonTime"
                    }, new ColumnItem{
                        Name = "RejecterName"
                    }, new ColumnItem{
                        Name = "RejectTime"
                    }, new ColumnItem{
                        Name = "Branch.Name"
                    }, new ColumnItem{
                        Name = "PartsSalesCategory.Name"
                    }
                };
            }
        }
        private readonly string[] kvNames = { 
            "PartsOuterPurchase_OuterPurchaseComment" ,"PartsOuterPurchaseChange_Status" 
        };
        protected override Type EntityType {
            get {
                return typeof(PartsOuterPurchaseChange);
            }
        }
        protected override string OnRequestQueryName() {
            return "GetPartsOuterPurchaseChangeWithPartsSalesCategory";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
        }
        public PartsOuterPurchaseBillReportDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override Core.IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsOuterPurchaseChange"
                    }
                };
            }
        }
    }
}
