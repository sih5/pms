﻿using System;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsOuterPurchaseChangeDetailDataGridView : DcsDataGridViewBase {

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name = "SerialNumber"
                    }, new ColumnItem{
                        Name = "PartsCode"
                    }, new ColumnItem{
                        Name = "PartsName"
                    }, new ColumnItem{
                        Name = "OuterPurchasePrice"
                    }, new ColumnItem{
                        Name = "TradePrice",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_TradePrice
                    }, new ColumnItem{
                        Name = "Quantity"
                    }, new ColumnItem{
                        Name = "Supplier"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsOuterPurchaselist);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            ((GridViewDataColumn)this.GridView.Columns["TradePrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OuterPurchasePrice"]).DataFormatString = "c2";
        }

        protected override string OnRequestQueryName() {
            return "GetPartsOuterPurchaselists";
        }


        private void PartsOuterPurchaseChangeDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsOuterPurchaseChange = e.NewValue as PartsOuterPurchaseChange;
            if(partsOuterPurchaseChange == null || partsOuterPurchaseChange.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsOuterPurchaseChangeId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsOuterPurchaseChange.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        public PartsOuterPurchaseChangeDetailDataGridView() {
            this.DataContextChanged += PartsOuterPurchaseChangeDetailDataGridView_DataContextChanged;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }
    }
}
