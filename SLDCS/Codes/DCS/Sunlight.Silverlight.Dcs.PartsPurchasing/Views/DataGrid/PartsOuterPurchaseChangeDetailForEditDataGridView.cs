﻿
using System;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsOuterPurchaseChangeDetailForEditDataGridView : DcsDataGridViewBase {
        private DcsDropDownQueryWindowBase sparePartForOuterPurchaseDropDownQueryWindow;
        private DcsMultiPopupsQueryWindowBase SparePartForOuterPurchaseQueryWindow {
            get {
                if(this.sparePartForOuterPurchaseQueryWindow == null) {
                    this.sparePartForOuterPurchaseQueryWindow = DI.GetQueryWindow("SparePartForOuterPurchaseMulit") as DcsMultiPopupsQueryWindowBase;
                    this.sparePartForOuterPurchaseQueryWindow.SelectionDecided += sparePartForOuterPurchaseQueryWindow_SelectionDecided;
                    this.sparePartForOuterPurchaseQueryWindow.Loaded += sparePartForOuterPurchaseQueryWindow_Loaded;
                }
                return this.sparePartForOuterPurchaseQueryWindow;
            }
        }


        private DcsMultiPopupsQueryWindowBase sparePartForOuterPurchaseQueryWindow;
        private DcsDropDownQueryWindowBase SparePartForOuterPurchaseDropDownQueryWindow {
            get {
                if(this.sparePartForOuterPurchaseDropDownQueryWindow == null) {
                    this.sparePartForOuterPurchaseDropDownQueryWindow = DI.GetQueryWindow("SparePartForOuterPurchaseDropDown") as DcsDropDownQueryWindowBase;
                    this.sparePartForOuterPurchaseDropDownQueryWindow.SelectionDecided += this.SparePartForOuterPurchaseDropDownQueryWindow_SelectionDecided;
                    this.sparePartForOuterPurchaseDropDownQueryWindow.Loaded += this.SparePartForOuterPurchaseDropDownQueryWindow_Loaded;
                }
                return this.sparePartForOuterPurchaseDropDownQueryWindow;
            }
        }


        private RadWindow radQueryWindow;
        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = SparePartForOuterPurchaseQueryWindow,
                    Header = PartsPurchasingUIStrings.QueryPanel_Title_SparePartForOuterPurchase,
                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                });
            }
        }

        //下拉弹出框
        private void sparePartForOuterPurchaseQueryWindow_Loaded(object sender, RoutedEventArgs routedEventArgs) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            var partsOuterPurchaseChange = this.DataContext as PartsOuterPurchaseChange;
            if(queryWindow == null || partsOuterPurchaseChange == null)
                return;
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", new object[] {
                new FilterItem("PartsSalesCategoryrId", typeof(int), FilterOperator.IsEqualTo, partsOuterPurchaseChange.PartsSalesCategoryrId)
            });

        }
        private void sparePartForOuterPurchaseQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var sparePart = queryWindow.SelectedEntities.Cast<SparePartOuterPurchase>();
            if(sparePart == null)
                return;
            var partsOuterPurchaseChange = this.DataContext as PartsOuterPurchaseChange;
            if(partsOuterPurchaseChange == null)
                return;
            if(partsOuterPurchaseChange.PartsOuterPurchaselists.Any(r => sparePart.Any(ex => ex.Code == r.PartsCode))) {
                UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Error_SparePart_DoubleSparePartID));
                return;
            }
            foreach(var item in sparePart) {
                var partsOuterPurchaselist = new PartsOuterPurchaselist {
                    SerialNumber = this.GridView.Items.Count > 0 ? this.GridView.Items.Cast<PartsOuterPurchaselist>().Max(entity => entity.SerialNumber) + 1 : 1,
                    PartsId = item.PartId,
                    PartsCode = item.Code,
                    PartsName = item.Name,
                    TradePrice = item.PartsSalesPrice != null ? (decimal)item.PartsSalesPrice : 0,
                    Quantity = default(int)
                };
                partsOuterPurchaseChange.PartsOuterPurchaselists.Add(partsOuterPurchaselist);
            }
        }

        //多选弹出框
        private void SparePartForOuterPurchaseDropDownQueryWindow_Loaded(object sender, RoutedEventArgs routedEventArgs) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            var partsOuterPurchaseChange = this.DataContext as PartsOuterPurchaseChange;
            if(queryWindow == null || partsOuterPurchaseChange == null)
                return;
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", new object[] {
                new FilterItem("PartsSalesCategoryrId", typeof(int), FilterOperator.IsEqualTo, partsOuterPurchaseChange.PartsSalesCategoryrId)
            });

        }
        private void SparePartForOuterPurchaseDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var sparePart = queryWindow.SelectedEntities.Cast<SparePartOuterPurchase>().FirstOrDefault();
            if(sparePart == null)
                return;
            var partsOuterPurchaselist = queryWindow.DataContext as PartsOuterPurchaselist;
            if(partsOuterPurchaselist == null)
                return;
            var partsOuterPurchaseChange = this.DataContext as PartsOuterPurchaseChange;
            if(partsOuterPurchaseChange == null)
                return;
            if(partsOuterPurchaseChange.PartsOuterPurchaselists.Any(r => r.PartsCode == sparePart.Code)) {
                UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Error_SparePart_DoubleSparePartID, sparePart.Code));
                return;
            }
            partsOuterPurchaselist.PartsId = sparePart.PartId;
            partsOuterPurchaselist.PartsCode = sparePart.Code;
            partsOuterPurchaselist.PartsName = sparePart.Name;
            partsOuterPurchaselist.TradePrice = sparePart.PartsSalesPrice != null ? (decimal)sparePart.PartsSalesPrice : 0;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }


        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new DropDownTextBoxColumnItem{
                        Name = "PartsCode",
                        DropDownContent = this.SparePartForOuterPurchaseDropDownQueryWindow,
                        IsEditable = false
                    }, new ColumnItem{
                        Name = "PartsName",
                        IsReadOnly = true,
                    }, new ColumnItem{
                        Name = "OuterPurchasePrice",
                        IsReadOnly = false
                    }, new ColumnItem{
                        Name = "TradePrice",
                        IsReadOnly = true,
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_TradePrice
                    }, new ColumnItem{
                        Name = "Quantity"
                    }, new ColumnItem{
                        Name = "Supplier"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsOuterPurchaselist);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsOuterPurchaselists");
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.Deleted -= GridView_Deleted;
            this.GridView.Deleted += GridView_Deleted;
            this.GridView.CellEditEnded -= GridView_CellEditEnded;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            this.GridView.AddingNewDataItem -= this.GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            ((GridViewDataColumn)this.GridView.Columns["TradePrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OuterPurchasePrice"]).DataFormatString = "c2";

        }
        void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var partsOuterPurchaseChange = this.DataContext as PartsOuterPurchaseChange;
            if(partsOuterPurchaseChange == null)
                return;
            partsOuterPurchaseChange.Amount = partsOuterPurchaseChange.PartsOuterPurchaselists.Sum(ex => ex.OuterPurchasePrice * ex.Quantity);
        }

        void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var partsOuterPurchaseChange = this.DataContext as PartsOuterPurchaseChange;
            if(partsOuterPurchaseChange == null)
                return;
            var serialNumber = 1;
            foreach(var partsOuterPurchaselist in partsOuterPurchaseChange.PartsOuterPurchaselists.Where(entity => !e.Items.Contains(entity))) {
                partsOuterPurchaselist.SerialNumber = serialNumber;
                serialNumber++;
            }
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            var partsOuterPurchaseChange = this.DataContext as PartsOuterPurchaseChange;
            if(partsOuterPurchaseChange == null)
                return;
            if(partsOuterPurchaseChange.BranchId == default(int)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_DepartmentInformatioin_BranchNameIsNotNull);
                return;
            }
            if(partsOuterPurchaseChange.OuterPurchaseComment == default(int)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_DepartmentInformatioin_OuterPurchaseComment);
                return;
            }
            if(partsOuterPurchaseChange.PartsSalesCategoryrId == default(int)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_PartsSalesCategory_NameIsNull);
                return;
            }
            RadQueryWindow.ShowDialog();
            //var maxSerialNumber = this.GridView.Items.Count > 0 ? this.GridView.Items.Cast<PartsOuterPurchaselist>().Max(entity => entity.SerialNumber) + 1 : 1;
            //e.NewObject = new PartsOuterPurchaselist {
            //    SerialNumber = maxSerialNumber,
            //    Quantity = 0
            //};
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }
    }
}
