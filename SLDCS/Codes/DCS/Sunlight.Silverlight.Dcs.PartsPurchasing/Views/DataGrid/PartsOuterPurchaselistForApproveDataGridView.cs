﻿
using System;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsOuterPurchaselistForApproveDataGridView : DcsDataGridViewBase {

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name = "SerialNumber"
                    }, new ColumnItem{
                        Name = "PartsCode"
                    }, new ColumnItem{
                        Name = "PartsName"
                    }, new ColumnItem{
                        Name = "OuterPurchasePrice"
                    }, new ColumnItem{
                        Name = "TradePrice",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_TradePrice
                    }, new ColumnItem{
                        Name = "Quantity"
                    },new ColumnItem{
                       Name = "BranchQuantity",
                       Title= PartsPurchasingUIStrings.DataGridView_ColumnItem_BranchQuantity
                    }, new ColumnItem{
                        Name = "Supplier"
                    }
                };
            }
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }


        protected override Type EntityType {
            get {
                return typeof(PartsOuterPurchaselist);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            ((GridViewDataColumn)this.GridView.Columns["TradePrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OuterPurchasePrice"]).DataFormatString = "c2";
        }
        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsOuterPurchaselists");
        }

    }
}
