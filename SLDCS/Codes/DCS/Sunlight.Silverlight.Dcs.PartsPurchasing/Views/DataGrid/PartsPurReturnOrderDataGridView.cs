﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurReturnOrderDataGridView : DcsDataGridViewBase {
        protected readonly string[] kvNames = {
            "PartsPurReturnOrder_ReturnReason", "WorkflowOfSimpleApproval_Status"
        };

        public PartsPurReturnOrderDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurReturnOrder);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                       Name = "Status",
                       KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem {
                       Name = "Code",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurReturnOrder_Code
                    }, new ColumnItem {
                       Name = "PartsPurchaseOrderCode"
                    }, new ColumnItem {
                       Name = "PartsSupplierCode",
                       IsDefaultGroup = true
                    }, new ColumnItem {
                       Name = "PartsSupplierName"
                    }, new ColumnItem {
                       Name = "WarehouseName"
                    }, new ColumnItem {
                       Name = "WarehouseAddress"
                    }, new ColumnItem {
                       Name = "InvoiceRequirement"
                    }, new ColumnItem {
                       Name = "TotalAmount"
                    }, new KeyValuesColumnItem {
                       Name = "ReturnReason",
                       KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                       Name = "Remark"
                    }, new ColumnItem {
                       Name = "CreatorName"
                    }, new ColumnItem {
                       Name = "CreateTime"
                    } , new ColumnItem {
                       Name = "ApproverName"
                    }, new ColumnItem {
                       Name = "ApproveTime"
                    }  , new ColumnItem {
                       Name = "ModifierName"
                    }, new ColumnItem {
                       Name = "ModifyTime"
                    }, new ColumnItem {
                       Name = "AbandonerName"
                    }, new ColumnItem {
                       Name = "AbandonTime"
                    } , new ColumnItem {
                       Name = "BranchName",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurReturnOrder_BranchName,
                       IsDefaultGroup = true
                    }, new ColumnItem{
                        Name = "PartsSalesCategoryName",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurReturnOrder_PartsSalesCategoryName
                    }                 
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsPurReturnOrderWithDetails";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "SupplierPartsPurReturnOrderDetail"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.Columns["BranchName"].ShowColumnWhenGrouped = false;
            this.GridView.Columns["PartsSupplierCode"].ShowColumnWhenGrouped = false;
            ((GridViewDataColumn)this.GridView.Columns["TotalAmount"]).DataFormatString = "c2";
        }
    }
}
