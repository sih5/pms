﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurReturnOrderDetailDataGridView : DcsDataGridViewBase {
        public PartsPurReturnOrderDetailDataGridView() {
            this.DataContextChanged += this.PartsPurReturnOrderForDetailDataGridView_DataContextChanged;
        }

        private void PartsPurReturnOrderForDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsPurReturnOrder = e.NewValue as PartsPurReturnOrder;
            if(partsPurReturnOrder == null || partsPurReturnOrder.Id == default(int))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "PartsPurReturnOrderId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                };
            this.FilterItem.Value = partsPurReturnOrder.Id;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                       Name = "SerialNumber"
                    },new ColumnItem {
                       Name = "SparePartCode"
                    },new ColumnItem {
                       Name = "SparePartName"
                    },new ColumnItem {
                       Name = "SupplierPartCode",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierPartCode
                    },new ColumnItem {
                       Name = "MeasureUnit"
                    },new ColumnItem {
                       Name = "Quantity"
                    },new ColumnItem {
                       Name = "UnitPrice",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurReturnOrderDetail_UnitPrice
                    },new ColumnItem {
                       Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurReturnOrderDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsPurReturnOrderDetails";
        }
    }
}
