﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using ValidationResult = System.ComponentModel.DataAnnotations.ValidationResult;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurReturnOrderDetailForEditDataGridView : DcsDataGridViewBase {
        private bool isCanUserInsertOrDelete = true;

        private DcsMultiPopupsQueryWindowBase warehousePartsStockDropDownQueryWindow;
        private RadWindow radQueryWindow;
        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = this.WarehousePartsStockDropDownQueryWindow,
                    Header = PartsPurchasingUIStrings.DataGridView_Title_PartsPurReturnOrderWarehouseStockQuery,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen
                });
            }
        }
        private DcsMultiPopupsQueryWindowBase WarehousePartsStockDropDownQueryWindow {
            get {
                if(this.warehousePartsStockDropDownQueryWindow == null) {
                    this.warehousePartsStockDropDownQueryWindow = DI.GetQueryWindow("PurchaseWarehousePartsStock") as DcsMultiPopupsQueryWindowBase;
                    this.warehousePartsStockDropDownQueryWindow.Loaded += warehousePartsStockDropDownQueryWindow_Loaded;
                    this.warehousePartsStockDropDownQueryWindow.SelectionDecided += warehousePartsStockDropDownQueryWindow_SelectionDecided;
                }
                return this.warehousePartsStockDropDownQueryWindow;
            }
        }

        private void warehousePartsStockDropDownQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var partsPurReturnOrder = this.DataContext as PartsPurReturnOrder;
            if(partsPurReturnOrder == null || partsPurReturnOrder.WarehouseName == null)
                return;
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null)
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem("WarehouseId", typeof(int), FilterOperator.IsEqualTo, partsPurReturnOrder.WarehouseId));
            compositeFilterItem.Filters.Add(new FilterItem("PartsSalesCategoryId", typeof(int), FilterOperator.IsEqualTo, partsPurReturnOrder.PartsSalesCategoryId));
            compositeFilterItem.Filters.Add(new FilterItem("PartsSupplierId", typeof(int), FilterOperator.IsEqualTo, partsPurReturnOrder.PartsSupplierId));
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);

            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "PartsPurReturnOrder.WarehouseName", partsPurReturnOrder.WarehouseName
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "PartsPurReturnOrder.WarehouseName", false
            });
        }

        private void warehousePartsStockDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var warehousePartsStock = queryWindow.SelectedEntities.Cast<WarehousePartsStock>();
            if(warehousePartsStock == null)
                return;
            var partsPurReturnOrder = this.DataContext as PartsPurReturnOrder;
            if(partsPurReturnOrder == null)
                return;
            if(partsPurReturnOrder.BranchId == default(int)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataGridView_Text_PartsPurReturnOrder_BranchIsNull);
                return;
            }
            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;

            foreach(var item in warehousePartsStock) {
                var partsPurReturnOrderDetails = new PartsPurReturnOrderDetail {
                    SerialNumber = this.GridView.Items.Count > 0 ? this.GridView.Items.Cast<PartsPurReturnOrderDetail>().Max(entity => entity.SerialNumber) + 1 : 1,
                    SparePartId = item.SparePartId,
                    SparePartCode = item.SparePartCode,
                    SparePartName = item.SparePartName,
                    MeasureUnit = item.MeasureUnit,
                    Quantity = default(int),
                    UnitPrice = item.PartsPurchasePricing,
                    CheckUnitPrice = item.PartsPurchasePricing
                };
                partsPurReturnOrder.PartsPurReturnOrderDetails.Add(partsPurReturnOrderDetails);
                if(partsPurReturnOrder.PartsPurReturnOrderDetails.GroupBy(entity => new {
                    entity.SparePartId,
                    entity.SupplierPartCode
                }).Any(array => array.Count() > 1)) {
                    UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataGridView_Validation_PartsPurReturnOrderDetail_SparePartIsAlreadyExists, item.SparePartCode));
                    partsPurReturnOrder.PartsPurReturnOrderDetails.Remove(partsPurReturnOrderDetails);
                    return;
                }
            }
        }


        //消除bug配件销售类型Id暂时给1，设计修改后根据设计调整
        //domainContext.Load(domainContext.GetPurcha (BaseApp.Current.CurrentUserData.EnterpriseId, partsPurReturnOrder.PartsSupplierId, DateTime.Now, new[] { warehousePartsStock.SparePartId }, 1), LoadBehavior.RefreshCurrent, loadOp => {
        //    if(loadOp.HasError) {
        //        if(!loadOp.IsErrorHandled)
        //            loadOp.MarkErrorAsHandled();
        //        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
        //        return;
        //    }
        //    var sparePart = loadOp.Entities.SingleOrDefault();
        //    if(sparePart == null)
        //        return;
        //    if(sparePart.PartsPurchasePricingDetail != null)
        //        partsPurReturnOrderDetail.UnitPrice = sparePart.PartsPurchasePricingDetail.Price;
        //    partsPurReturnOrderDetail.SparePartId = warehousePartsStock.SparePartId;
        //    partsPurReturnOrderDetail.SparePartName = warehousePartsStock.SparePartName;
        //    partsPurReturnOrderDetail.SparePartCode = warehousePartsStock.SparePartCode;
        //}, null);



        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SupplierPartCode",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierPartCode,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Quantity"
                    }, new ColumnItem {
                        Name = "UnitPrice",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurReturnOrderDetail_UnitPrice,
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "MeasureUnit",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurReturnOrderDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsPurReturnOrderDetails");
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.IsReadOnly = false;
            //this.GridView.CanUserInsertRows = true;
            //this.GridView.CanUserDeleteRows = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.AddingNewDataItem -= GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
            this.GridView.Deleting -= GridView_Deleting;
            this.GridView.Deleting += GridView_Deleting;
            this.GridView.BeginningEdit -= GridView_BeginningEdit;
            this.GridView.BeginningEdit += GridView_BeginningEdit;
            this.GridView.CellEditEnded -= GridView_CellEditEnded;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var partsPurReturnOrder = this.DataContext as PartsPurReturnOrder;
            if(partsPurReturnOrder == null)
                return;
            var partsPurReturnOrderDetail = e.Cell.DataContext as PartsPurReturnOrderDetail;
            if(partsPurReturnOrderDetail == null)
                return;
            if(partsPurReturnOrder.ReturnReason == (int)DcsPartsPurReturnOrderReturnReason.质量件退货) {
                switch(e.Cell.Column.UniqueName) {
                    case "Quantity":
                        if(partsPurReturnOrderDetail.Quantity > partsPurReturnOrderDetail.InspectedQuantity) {
                            partsPurReturnOrderDetail.ValidationErrors.Add(new ValidationResult(PartsPurchasingUIStrings.DataGridView_Notification_RetuanQuantityError, new[] {
                                "Quantity"
                            }));
                        } else
                            partsPurReturnOrderDetail.ValidationErrors.Clear();
                        break;
                    default:
                        break;
                }
            }
        }

        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            var partsPurReturnOrder = this.DataContext as PartsPurReturnOrder;
            if(partsPurReturnOrder == null)
                return;
            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            var partsPurReturnOrderDetail = e.Row.DataContext as PartsPurReturnOrderDetail;
            if(partsPurReturnOrderDetail == null)
                return;

            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "SparePartCode":
                    e.Cancel = partsPurReturnOrder.ReturnReason == (int)DcsPartsPurReturnOrderReturnReason.质量件退货;
                    break;
            }

        }


        private void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            var partsPurReturnOrder = this.DataContext as PartsPurReturnOrder;
            if(partsPurReturnOrder == null)
                return;
            var serialNumber = 1;
            foreach(var partsPurReturnOrderDetail in partsPurReturnOrder.PartsPurReturnOrderDetails.Where(entity => !e.Items.Contains(entity))) {
                partsPurReturnOrderDetail.SerialNumber = serialNumber;
                serialNumber++;
            }
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            if(!this.isCanUserInsertOrDelete)
                e.Cancel = true;

            var partsPurReturnOrder = this.DataContext as PartsPurReturnOrder;
            if(partsPurReturnOrder == null)
                return;
            if(partsPurReturnOrder.ReturnReason == (int)DcsPartsPurReturnOrderReturnReason.质量件退货)
                return;
            if(partsPurReturnOrder.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_PartsSalesCategory_NameIsNull);
                e.Cancel = true;
                return;
            }
            if(partsPurReturnOrder.WarehouseId == default(int)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrder_WarehouseNameIsNull);
                e.Cancel = true;
                return;
            }
            if(partsPurReturnOrder.PartsSupplierId == default(int)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataGridView_Text_PartsPurReturnOrder_PartsSupplierIsNull);
                e.Cancel = true;
                return;
            }
            e.Cancel = true;
            RadQueryWindow.ShowDialog();
        }

        private void PartsPurReturnOrderDetailForEditDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsPurReturnOrder = this.DataContext as PartsPurReturnOrder;
            if(partsPurReturnOrder == null)
                return;
            this.isCanUserInsertOrDelete = partsPurReturnOrder.ReturnReason != (int)DcsPartsPurReturnOrderReturnReason.质量件退货;
            partsPurReturnOrder.PropertyChanged += partsPurReturnOrder_PropertyChanged;
        }

        private void partsPurReturnOrder_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            var partsPurReturnOrder = this.DataContext as PartsPurReturnOrder;
            if(partsPurReturnOrder == null)
                return;
            switch(e.PropertyName) {
                case "ReturnReason":
                    this.GridView.CancelEdit();
                    this.isCanUserInsertOrDelete = partsPurReturnOrder.ReturnReason != (int)DcsPartsPurReturnOrderReturnReason.质量件退货;
                    var details = partsPurReturnOrder.PartsPurReturnOrderDetails.ToArray();
                    foreach(var item in details) {
                        partsPurReturnOrder.PartsPurReturnOrderDetails.Remove(item);
                    }
                    break;
            }
        }

        public PartsPurReturnOrderDetailForEditDataGridView() {
            this.DataContextChanged += PartsPurReturnOrderDetailForEditDataGridView_DataContextChanged;
        }
    }
}
