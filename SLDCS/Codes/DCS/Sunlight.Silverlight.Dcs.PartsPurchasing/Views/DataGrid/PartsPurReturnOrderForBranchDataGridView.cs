﻿
using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurReturnOrderForBranchDataGridView : DcsDataGridViewBase {
        protected readonly string[] kvNames = {
            "PartsPurReturnOrder_ReturnReason", "PartsPurReturnOrder_Status","ReturnOutStatus"
        };

        public PartsPurReturnOrderForBranchDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurReturnOrder);
            }
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                       Name = "Status",
                       KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem {
                       Name = "Code",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurReturnOrder_Code
                    }, new ColumnItem {
                       Name = "PartsPurchaseOrderCode"
                    }, new ColumnItem {
                       Name = "PartsSupplierCode"
                    }, new ColumnItem {
                       Name = "PartsSupplierName"
                    }, new ColumnItem {
                       Name = "WarehouseName"
                    }, new ColumnItem {
                       Name = "WarehouseAddress"
                    }, new ColumnItem {
                       Name = "InvoiceRequirement"
                    }, new ColumnItem {
                       Name = "TotalAmount"
                    }, new KeyValuesColumnItem {
                       Name = "ReturnReason",
                       KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },  new KeyValuesColumnItem {
                        Name = "OutStatus",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    }, new ColumnItem {
                       Name = "SAPPurReturnOrderCode",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_SAPPurReturnOrderCode
                    } , new ColumnItem {
                       Name = "Remark"
                    }, new ColumnItem {
                       Name = "CreatorName"
                    }, new ColumnItem {
                       Name = "CreateTime"
                    }, new ColumnItem {
                       Name = "InitialApproverName",
                       Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_InitialApprover
                    }, new ColumnItem {
                       Name = "InitialApproveTime",
                       Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_InitialApproveTime
                    }, new ColumnItem {
                       Name = "InitialApproverComment",
                       Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_InitialApproveMemo
                    }, new ColumnItem {
                       Name = "ApproverName",
                       Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_FinalApprover
                    }, new ColumnItem {
                       Name = "ApproveTime",
                       Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_FinalApproveTime
                    }, new ColumnItem {
                       Name = "ApprovertComment",
                       Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_FinalApproveMemo
                    }, new ColumnItem {
                       Name = "ModifierName"
                    }, new ColumnItem {
                       Name = "ModifyTime"
                    }, new ColumnItem {
                       Name = "AbandonerName"
                    }, new ColumnItem {
                       Name = "AbandonTime"
                    }, new ColumnItem {
                       Name = "BranchName",
                       Title = PartsPurchasingUIStrings.DetailPanel_Text_Common_BranchName
                    }                   
                };
            }
        }
        protected override string OnRequestQueryName() {
            return "GetPartsPurReturnOrderWithDetails";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsPurReturnOrderDetail","PartsPurReturnOrderKanBanDetail"
                    }
                };
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["TotalAmount"]).DataFormatString = "c2";
        }
    }
}
