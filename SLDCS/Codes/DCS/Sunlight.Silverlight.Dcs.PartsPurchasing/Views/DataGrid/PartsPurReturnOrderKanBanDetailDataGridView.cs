﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Controls;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurReturnOrderKanBanDetailDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        Title=Utils.GetEntityLocalizedName(typeof(SparePart),"Code")
                    },new ColumnItem {
                        Name = "SparePartName",
                        Title=Utils.GetEntityLocalizedName(typeof(SparePart),"Name")
                    },new ColumnItem {
                        Name = "Quantity",
                        Title =PartsPurchasingUIStrings.DataGridView_Column_Amount
                    },new ColumnItem {
                        Name = "OutboundFulfillment",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_OutboundFulfillment
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPartsPurReturnOrderDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVirtualPartsPurReturnOrderDetail";
        }

        private void PartsPurReturnOrderKanBanDetailDataGridView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var partsPurReturnOrder = e.NewValue as PartsPurReturnOrder;
            if(partsPurReturnOrder == null || partsPurReturnOrder.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsPurReturnOrderId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsPurReturnOrder.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        public PartsPurReturnOrderKanBanDetailDataGridView() {
            this.DataContextChanged += this.PartsPurReturnOrderKanBanDetailDataGridView_DataContextChanged;
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.Columns.Insert(0, new GridViewSerialNumberColumn {
                Header = PartsPurchasingUIStrings.DataGridView_ColumnItem_Number
            });
        }
    }
}
