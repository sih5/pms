﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurchaseOrderDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsPurchaseOrder_OrderType","PartsPurchaseOrder_Status","PartsShipping_Method","PurchaseInStatus"
        };

        public PartsPurchaseOrderDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurchaseOrder);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "WarehouseName"
                    }, new ColumnItem {
                        Name = "PartsSupplierCode"
                    }, new ColumnItem {
                        Name = "PartsSupplierName"
                    }, new KeyValuesColumnItem {
                        Name = "OrderType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "ReceivingAddress"
                    }, new ColumnItem {
                        Name = "IfDirectProvision"
                    }, new ColumnItem {
                        Name = "ReceivingCompanyName"
                    }, new ColumnItem {
                        Name = "TotalAmount"
                    }, new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    },new KeyValuesColumnItem {
                        Name = "InStatus",
                           KeyValueItems = this.KeyValueManager[this.kvNames[3]]
                    }, new ColumnItem {
                        Name = "RequestedDeliveryTime"
                    }, new ColumnItem {
                        Name = "ConfirmationRemark"
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }  , new ColumnItem {
                        Name = "BranchName"
                    }                  
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsPurchaseOrders";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsPurchaseOrder","PartsPurchaseOrderDetail","SupplierShippingOrder"
                    }
                };
            }
        }
    }
}
