﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System;
using System.Collections.Generic;
using System.Windows;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurchaseOrderDetailDataGridView : DcsDataGridViewBase {
        protected readonly string[] KvNames = {
            "PurchasePriceType","PartsPurchaseOrderDetail_ShortSupReason"
        };
        public PartsPurchaseOrderDetailDataGridView() {
            this.DataContextChanged += this.PartsPurchaseOrderDetailDataGridView_DataContextChanged;
            this.KeyValueManager.Register(this.KvNames);
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
        }

        private void PartsPurchaseOrderDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsPurchaseOrder = e.NewValue as VirtualPartsPurchaseOrder;
            if(partsPurchaseOrder == null || partsPurchaseOrder.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsPurchaseOrderId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsPurchaseOrder.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override string OnRequestQueryName() {
            return "GetPartsPurchaseOrderDetailsWithSpareParts";
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber"
                    },new ColumnItem {
                        Name = "SparePartCode"
                    },new ColumnItem {
                        Name = "SparePartName"
                    },new ColumnItem {
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_SupplierPartCode,
                        Name = "SupplierPartCode"
                    },new ColumnItem {
                        Name = "MeasureUnit"
                    },new ColumnItem {
                        Name = "Specification",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_Specification
                    },new ColumnItem {
                        Name = "OrderAmount"
                    },new ColumnItem {
                        Name = "ConfirmedAmount"
                    }, new ColumnItem {
                        Name = "ShippingAmount"
                    },new ColumnItem {
                        Name = "UnitPrice"
                    },new KeyValuesColumnItem {
                        Name = "PriceType",
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]],
                        Title= PartsPurchasingUIStrings.DataGridView_ColumnItem_PirceType
                    },new ColumnItem {
                        Name = "PromisedDeliveryTime"
                    },new KeyValuesColumnItem {
                        Name = "ShortSupReason",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_ShortSupReason,
                        KeyValueItems = this.KeyValueManager[this.KvNames[1]]
                    }
                    ,new ColumnItem {
                        Name = "ABCStrategy",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_ABCStrategy
                    }
                    ,new ColumnItem {
                        Name = "Remark"
                    },new ColumnItem {
                        Name = "ConfirmationRemark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurchaseOrderDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
    }
}
