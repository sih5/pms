﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurchaseOrderDetailForApproveDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsPurchaseOrderDetail_ShortSupReason"
        };

        public PartsPurchaseOrderDetailForApproveDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_SupplierPartCode,
                        Name = "SupplierPartCode",
                        IsReadOnly = true
                    },
                    //new ColumnItem {
                    //    Name = "SparePartCode",
                    //    IsReadOnly = true
                    //},
                    new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "OrderAmount",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "PendingConfirmedAmount",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ConfirmedAmount"
                    }, new KeyValuesColumnItem {
                        Name = "ShortSupReason",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_ShortSupReason,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "ShippingDate",
                        Title= PartsPurchasingUIStrings.DataGridView_ColumnItem_ShippingDate
                    }
                    , new ColumnItem {
                        Name = "PromisedDeliveryTime",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ConfirmationRemark"
                    }, new ColumnItem {
                        Name = "UnitPrice",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "MeasureUnit",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePart.Specification",
                        IsReadOnly = true,
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_Specification
                    },
                    //new ColumnItem {
                    //    Name = "POCode",
                    //    Title = "PO单号",
                    //    IsReadOnly = true
                    //},
                    new ColumnItem {
                        Name = "Remark",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurchaseOrderDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.IsReadOnly = false;
            this.GridView.CanUserDeleteRows = false;
            this.GridView.CanUserInsertRows = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["PromisedDeliveryTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ShippingDate"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["UnitPrice"]).DataFormatString = "c2";
            this.GridView.BeginningEdit += GridView_BeginningEdit;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            //审批时主单总金额无需重新计算(FTDCS_ISS20141209002)
            //this.GridView.CellEditEnded += this.GridView_CellEditEnded;
        }
       private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e)
        {
            var detail = e.Row.DataContext as PartsPurchaseOrderDetail;
            if (detail == null)
                return;
           //当确认量= 计划量，短供原因不能编辑。
            switch (e.Cell.DataColumn.DataMemberBinding.Path.Path)
            {
                case "ShortSupReason":
                    if (detail.ConfirmedAmount >= detail.OrderAmount) { 
                        e.Cancel = true;
                    }
                    break;
            }
        }
       private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
           if (!e.Cell.Column.UniqueName.Equals("ConfirmedAmount"))
               return;
           var detail = e.Cell.DataContext as PartsPurchaseOrderDetail;
           if (detail == null)
               return;
           if (detail.ConfirmedAmount >= detail.OrderAmount) {
               detail.ShortSupReason = null;
           }
       }
        //private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
        //    switch(e.Cell.Column.UniqueName) {
        //        case "ConfirmedAmount":
        //            var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
        //            //var partsPurchaseOrderDetail = e.Cell.DataContext as PartsPurchaseOrderDetail;
        //            if(partsPurchaseOrder == null)
        //                return;
        //            partsPurchaseOrder.TotalAmount = partsPurchaseOrder.PartsPurchaseOrderDetails.Sum(entity => entity.ConfirmedAmount * entity.UnitPrice);
        //            //if(partsPurchaseOrderDetail != null)
        //            //    partsPurchaseOrderDetail.PendingConfirmedAmount = partsPurchaseOrderDetail.OrderAmount - partsPurchaseOrderDetail.ConfirmedAmount;
        //            break;

        //    }
        //}


        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsPurchaseOrderDetails");
        }

    }
}
