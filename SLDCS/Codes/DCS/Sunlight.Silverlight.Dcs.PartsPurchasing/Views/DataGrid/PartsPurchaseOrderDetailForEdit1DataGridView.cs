﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurchaseOrderDetailForEdit1DataGridView : DcsDataGridViewBase {
        private DcsMultiPopupsQueryWindowBase sparePartForPartsPurchaseOrderDropDownQueryWindow;
        private RadWindow radQueryWindow;
        private QueryWindowBase partsSupplierForPartsPurchaseOrderDropDownQueryWindow;

        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = this.SparePartForPartsPurchaseOrderDropDownQueryWindow,
                    Header = PartsPurchasingUIStrings.QueryPanel_Title_SparePartForPartsPurchaseOrder,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen
            });
            }
        }

        private QueryWindowBase PartsSupplierForPartsPurchaseOrderDropDownQueryWindow {
            get {
                if(this.partsSupplierForPartsPurchaseOrderDropDownQueryWindow == null) {
                    this.partsSupplierForPartsPurchaseOrderDropDownQueryWindow = DI.GetQueryWindow("BranchSupplierRelationDropDown");
                    this.partsSupplierForPartsPurchaseOrderDropDownQueryWindow.SelectionDecided += this.QueryWindow_SelectionDecided;
                    this.partsSupplierForPartsPurchaseOrderDropDownQueryWindow.Loaded += queryWindow_Loaded;
                }
                return this.partsSupplierForPartsPurchaseOrderDropDownQueryWindow;
            }
        }

        private void QueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var branchSupplierRelation = queryWindow.SelectedEntities.Cast<BranchSupplierRelation>().FirstOrDefault();
            if(branchSupplierRelation == null) {
                return;
            }
            
            var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
            if(partsPurchaseOrder == null)
                return;
            //try {
            var partsPurchaseOrderDetail = GridView.SelectedItem as PartsPurchaseOrderDetail;
            partsPurchaseOrderDetail.PartsSupplierId = branchSupplierRelation.PartsSupplier.Id;
            partsPurchaseOrderDetail.PartsSupplierCode = branchSupplierRelation.PartsSupplier.Code;
            partsPurchaseOrderDetail.PartsSupplierName = branchSupplierRelation.PartsSupplier.Name;

                //    if(partsPurchaseOrder.PartsSupplierId == branchSupplierRelation.Id)
                //        return;
                //    partsPurchaseOrder.PartsSupplierId = branchSupplierRelation.SupplierId;
                //    partsPurchaseOrder.PartsSupplierCode = branchSupplierRelation.PartsSupplier.Code;
                //    partsPurchaseOrder.PartsSupplierName = branchSupplierRelation.PartsSupplier.Name;

                //    //TODO: 删除时抛出异常 
                //    var deleteDetals = partsPurchaseOrder.PartsPurchaseOrderDetails.ToArray();
                //    ((IEditableObject)partsPurchaseOrder).EndEdit();
                //    foreach(var detail in deleteDetals) {
                //        ((IEditableObject)detail).EndEdit();
                //        detail.ValidationErrors.Clear();
                //        partsPurchaseOrder.PartsPurchaseOrderDetails.Remove(detail);
            //}
            //finally {
            //    var parent = queryWindow.ParentOfType<RadWindow>();
            //    if(parent != null)
            //        parent.Close();
            //}
        }

        private void queryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
            if(queryWindow == null || partsPurchaseOrder == null)
                return;
            if(partsPurchaseOrder.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Error_PartsPurchaseOrder_PleaseSelectPartsSalesCategoryId);
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
                return;
            }
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "Branch.Name", BaseApp.Current.CurrentUserData.EnterpriseName
            });
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem("PartsSalesCategoryId", typeof(int), FilterOperator.IsEqualTo, partsPurchaseOrder.PartsSalesCategoryId));
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "BranchId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }


        private DcsMultiPopupsQueryWindowBase SparePartForPartsPurchaseOrderDropDownQueryWindow {
            get {
                if(this.sparePartForPartsPurchaseOrderDropDownQueryWindow == null) {
                    this.sparePartForPartsPurchaseOrderDropDownQueryWindow = DI.GetQueryWindow("SparePartForPartsPurchaseOrderMulti") as DcsMultiPopupsQueryWindowBase;
                    this.sparePartForPartsPurchaseOrderDropDownQueryWindow.SelectionDecided += this.SparePartForPartsPurchaseOrderDropDownQueryWindow_SelectionDecided;
                    this.sparePartForPartsPurchaseOrderDropDownQueryWindow.Loaded += this.SparePartForPartsPurchaseOrderDropDownQueryWindow_Loaded;
                }
                return this.sparePartForPartsPurchaseOrderDropDownQueryWindow;
            }
        }

        private void SparePartForPartsPurchaseOrderDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var spareParts = queryWindow.SelectedEntities.Cast<VirtualSparePart>();
            if(spareParts == null)
                return;
            var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
            if(partsPurchaseOrder == null)
                return;
            if(partsPurchaseOrder.PartsPurchaseOrderDetails.Any(r => spareParts.Any(ex => ex.Code == r.SparePartCode))) {
                UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Error_SparePart_DoubleSparePartID));
                return;
            }
            foreach(var sparePart in spareParts) {
                var partsPurchaseOrderDetail = new PartsPurchaseOrderDetail();
                partsPurchaseOrderDetail.ConfirmedAmount = 0;
                partsPurchaseOrderDetail.SerialNumber = this.GridView.Items.Count > 0 ? this.GridView.Items.Cast<PartsPurchaseOrderDetail>().Max(entity => entity.SerialNumber) + 1 : 1;
                partsPurchaseOrderDetail.SparePartId = sparePart.Id;
                partsPurchaseOrderDetail.SparePartCode = sparePart.Code;
                partsPurchaseOrderDetail.SparePartName = sparePart.Name;
                partsPurchaseOrderDetail.MeasureUnit = sparePart.MeasureUnit;
                partsPurchaseOrderDetail.PackingAmount = sparePart.PackingAmount;
                partsPurchaseOrderDetail.PackingSpecification = sparePart.PackingSpecification;
                partsPurchaseOrderDetail.Specification = sparePart.Specification;
                partsPurchaseOrderDetail.SupplierPartCode = sparePart.SupplierPartCode;
                partsPurchaseOrderDetail.UnitPrice = sparePart.PurchasePrice;
                partsPurchaseOrder.PartsPurchaseOrderDetails.Add(partsPurchaseOrderDetail);
            }
            this.GridView.IsReadOnly = false;
        }

        private void SparePartForPartsPurchaseOrderDropDownQueryWindow_Loaded(object sender, RoutedEventArgs routedEventArgs) {
            var queryWindowSparePartForPartsPurchaseOrder = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindowSparePartForPartsPurchaseOrder == null)
                return;
            var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
            if(partsPurchaseOrder == null)
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem("BranchId", typeof(int), FilterOperator.IsEqualTo, BaseApp.Current.CurrentUserData.EnterpriseId));
            compositeFilterItem.Filters.Add(new FilterItem("PartsSupplierId", typeof(int), FilterOperator.IsEqualTo, partsPurchaseOrder.PartsSupplierId));
            compositeFilterItem.Filters.Add(new FilterItem("Time", typeof(DateTime), FilterOperator.IsEqualTo, DateTime.Now));
            compositeFilterItem.Filters.Add(new FilterItem("SparePartIds", typeof(int[]), FilterOperator.IsEqualTo, new int[] { }));
            compositeFilterItem.Filters.Add(new FilterItem("PartsSaleCategoryId", typeof(int), FilterOperator.IsEqualTo, partsPurchaseOrder.PartsSalesCategoryId));

            queryWindowSparePartForPartsPurchaseOrder.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);

            queryWindowSparePartForPartsPurchaseOrder.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "PartsBranchName", partsPurchaseOrder.BranchName
            });
            queryWindowSparePartForPartsPurchaseOrder.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "PartsBranchName", false
            });
            queryWindowSparePartForPartsPurchaseOrder.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common","PartsSalesCategoryName",partsPurchaseOrder.PartsSalesCategoryName
            });
            queryWindowSparePartForPartsPurchaseOrder.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "PartsSalesCategoryName", false
            });
            queryWindowSparePartForPartsPurchaseOrder.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common","PartsSupplierCode",partsPurchaseOrder.PartsSupplierCode
            });
            queryWindowSparePartForPartsPurchaseOrder.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "PartsSupplierCode", false
            });
            queryWindowSparePartForPartsPurchaseOrder.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common","PartsSupplierName",partsPurchaseOrder.PartsSupplierName
            });
            queryWindowSparePartForPartsPurchaseOrder.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "PartsSupplierName", false
            });
            queryWindowSparePartForPartsPurchaseOrder.ExchangeData(null, "RefreshQueryResult", null);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new DropDownTextBoxColumnItem {
                        Name = "PartsSupplierCode",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_PartsSupplierCode,
                        DropDownContent = this.PartsSupplierForPartsPurchaseOrderDropDownQueryWindow,
                        IsEditable=false
                    }, new ColumnItem {
                        Name = "PartsSupplierName",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_PartsSupplierName,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title =PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingDetail_SupplierPartCode,
                        Name = "SupplierPartCode",
                        IsReadOnly = true
                    },new ColumnItem{
                        Name="Specification",
                        IsReadOnly=false,
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_Specification
                    }, new ColumnItem {
                        Name = "OrderAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                    }, new ColumnItem {
                        Name = "UnitPrice",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "MeasureUnit",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "RequestedDeliveryTime",
                        Title = Web.Resources.EntityStrings.PartsPurchaseOrder_RequestedDeliveryTime
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurchaseOrderDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.CellEditEnded += this.GridView_CellEditEnded;
            this.GridView.BeginningEdit += GridView_BeginningEdit;
            this.GridView.ValidatesOnDataErrors = GridViewValidationMode.InViewMode;
            this.GridView.CellValidating += GridView_CellValidating;
            ((GridViewDataColumn)this.GridView.Columns["UnitPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["RequestedDeliveryTime"]).DataFormatString = "d";
        }

        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
             if(partsPurchaseOrder == null)
                   return;
             if(e.Cell.DataColumn.DataMemberBinding.Path.Path == "OrderAmount")
                 if(partsPurchaseOrder.IfDirectProvision)
                     e.Cancel = true;
        }

        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
            var partsPurchaseOrderDetail = e.Cell.DataContext as PartsPurchaseOrderDetail;
            if(partsPurchaseOrderDetail == null)
                return;

            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "OrderAmount":
                    if(!(e.NewValue is int) || (int)e.NewValue <= 0) {
                        e.IsValid = false;
                        e.ErrorMessage = PartsPurchasingUIStrings.DataEditView_Validation_PartsInboundCheckBillDetail_InspectedQuantity_IsOrderAmount;
                    }
                    break;
            }
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            switch(e.Cell.Column.UniqueName) {
                case "OrderAmount":
                case "UnitPrice":
                    this.CalcTotalAmount();
                    break;
            }
        }

        private void CalcTotalAmount() {
            var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
            if(partsPurchaseOrder == null)
                return;
            partsPurchaseOrder.TotalAmount = partsPurchaseOrder.PartsPurchaseOrderDetails.Sum(entity => entity.OrderAmount * entity.UnitPrice);
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsPurchaseOrderDetails");
        }
    }
}