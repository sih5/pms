﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurchaseOrderDetailForSelectDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "SparePart_MeasureUnit"
        };
        public PartsPurchaseOrderDetailForSelectDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber"
                    }, new ColumnItem {
                        Name = "SparePartCode"
                    }, new ColumnItem {
                        Name = "SparePartName"
                    }, new ColumnItem {
                        Name = "SupplierPartCode"
                    }, new ColumnItem {
                        Name = "OrderAmount"
                    }, new ColumnItem {
                        Name = "ConfirmedAmount"
                    }, new KeyValuesColumnItem {
                        Name = "MeasureUnit",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "PackingAmount"
                    }, new ColumnItem {
                        Name = "PackingSpecification"
                    }, new ColumnItem {
                        Name = "PromisedDeliveryTime"
                    }, new ColumnItem {
                        Name = "ConfirmationRemark"
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "PartsPurchaseOrder.CreatorName"
                    }, new ColumnItem {
                        Name = "PartsPurchaseOrder.CreateTime"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurchaseOrderDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsPurchaseOrderDetailsWithOrder";
        }
    }
}
