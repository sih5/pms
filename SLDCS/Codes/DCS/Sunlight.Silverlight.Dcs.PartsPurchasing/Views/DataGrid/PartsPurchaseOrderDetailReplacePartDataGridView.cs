﻿
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurchaseOrderDetailReplacePartDataGridView : DcsDataGridViewBase {

        protected readonly string[] kvNames = {
            "PartsReplacement_ReplacementType"
        };

        public PartsPurchaseOrderDetailReplacePartDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override System.Collections.Generic.IEnumerable<Core.Model.ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true,
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_SparePartCode
                    },new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true,
                        Title= PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_SparePartName
                    },new KeyValuesColumnItem {
                        Name = "InsteadType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        IsReadOnly=true,
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_InsteadType
                    },new ColumnItem{
                        Name="PurchasePrice",
                        IsReadOnly=true,
                        Title= PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_UnitPrice
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.CanUserInsertRows = false;
            this.GridView.CanUserDeleteRows = false;
        }

        protected override System.Type EntityType {
            get {
                return typeof(PartsInsteadInfo);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsInsteadInfos");
        }
    }
}
