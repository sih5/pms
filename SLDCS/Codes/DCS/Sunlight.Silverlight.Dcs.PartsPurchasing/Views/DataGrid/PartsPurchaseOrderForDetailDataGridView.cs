﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurchaseOrderForDetailDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsPurchaseOrderDetail_ShortSupReason"
        };

        public PartsPurchaseOrderForDetailDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_SupplierPartCode,
                        Name = "SupplierPartCode",
                        IsReadOnly = true
                    },                  
                    new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "OrderAmount",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "PendingConfirmedAmount",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ConfirmedAmount",
                        IsReadOnly = true
                    }, new KeyValuesColumnItem {
                        Name = "ShortSupReason",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_ShortSupReason,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ShippingDate",
                        Title= PartsPurchasingUIStrings.DataGridView_ColumnItem_ShippingDate,
                        IsReadOnly = true
                    }
                    , new ColumnItem {
                        Name = "PromisedDeliveryTime",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ConfirmationRemark",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UnitPrice",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "MeasureUnit",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePart.Specification",
                        IsReadOnly = true,
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_Specification
                    },                    
                    new ColumnItem {
                        Name = "Remark",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurchaseOrderDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.IsReadOnly = false;
            this.GridView.CanUserDeleteRows = false;
            this.GridView.CanUserInsertRows = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["PromisedDeliveryTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ShippingDate"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["UnitPrice"]).DataFormatString = "c2";           
        }           
        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsPurchaseOrderDetails");
        }

    }
}
