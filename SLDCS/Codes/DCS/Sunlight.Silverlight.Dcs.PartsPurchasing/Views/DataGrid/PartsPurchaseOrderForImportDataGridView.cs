﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurchaseOrderForImportDataGridView : DcsDataGridViewBase {

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.CanUserInsertRows = false;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.IsReadOnly = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.Deleted += GridView_Deleted;
        }

        private void GridView_Deleted(object sender, Telerik.Windows.Controls.GridViewDeletedEventArgs e) {
            var dataEditView = this.DataContext as PartsPurchaseOrderForImportDataEditView;
            if(dataEditView == null)
                return;
            if(dataEditView.PartsPurchaseOrderDetailExtends.Any())
                foreach(var item in e.Items.Cast<PartsPurchaseOrderDetailExtend>().Where(item => dataEditView.PartsPurchaseOrderDetailExtends.Contains(item)))
                    dataEditView.PartsPurchaseOrderDetailExtends.Remove(item);
        }

        private readonly string[] kvNames = {
            "PartsShipping_Method"
        };

        public PartsPurchaseOrderForImportDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new ColumnItem {
                        Name = "PartsPurchaseOrderTypeNameStr",
                        IsReadOnly=true,
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_PartsPurchaseOrderTypeNameStr
                    }, new ColumnItem {
                        Name = "WarehouseName",
                        IsReadOnly=true,
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_WarehouseName
                    }, new ColumnItem {
                        Name = "RequestedDeliveryTime",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_RequestedDeliveryTime
                    }, new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        IsReadOnly=true,
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_ShippingMethod
                    },new ColumnItem {
                        Name = "PlanSource",
                        IsReadOnly=true,
                        Title=PartsPurchasingUIStrings.DataEditView_ImportTemplate_PlanSource
                    }
                    //,new ColumnItem {
                    //    Name = "SAPPurchasePlanCode",
                    //    Title="SAP采购计划单号"
                    //}
                    ,new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly=true,
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_SparePartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly=true,
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_SparePartName
                    }, new ColumnItem {
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_PartsSupplierCode,
                        Name = "PartsSupplierCode"
                    },new ColumnItem {
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_PartsSupplierName,
                        Name = "PartsSupplierName",
                        IsReadOnly=true
                    },new ColumnItem {
                        Name = "OrderAmount",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_OrderAmount
                    },new ColumnItem {
                        Name = "UnitPrice",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_UnitPrice
                    },new ColumnItem {
                        Name = "MeasureUnit",
                        IsReadOnly=true,
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_MeasureUnit
                    },new ColumnItem{
                        Name="Specification",
                         IsReadOnly=true,
                         Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_Specification
                    },new ColumnItem {
                        Name = "PartsSalesCategoryName",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_PartsSalesCategoryName,
                        IsReadOnly=true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurchaseOrderDetailExtend);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsPurchaseOrderDetailExtends");
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
    }
}

