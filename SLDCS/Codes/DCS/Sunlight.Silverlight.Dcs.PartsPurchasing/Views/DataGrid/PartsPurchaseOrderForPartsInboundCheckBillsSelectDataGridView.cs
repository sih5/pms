﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurchaseOrderForPartsInboundCheckBillsSelectDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsShipping_Method", "PartsPurchaseOrder_Status"
        };

        public PartsPurchaseOrderForPartsInboundCheckBillsSelectDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "WarehouseName"
                    }, new ColumnItem {
                        Name = "PartsPurchaseOrderType.Name",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_PartsPurchaseOrder_OrderType
                    }, new ColumnItem {
                        Name = "ReceivingAddress"
                    }, new ColumnItem {
                        Name = "IfDirectProvision"
                    }, new ColumnItem {
                        Name = "ReceivingCompanyName"
                    }, new ColumnItem {
                        Name = "TotalAmount"
                    }, new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "BranchName"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurchaseOrder);
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                foreach (var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "SparePartCode" && filter.MemberName != "SparePartName")) { 
                    newCompositeFilterItem.Filters.Add(item);
                }
                //var createTimeFilters = compositeFilterItem.Filters.Where(filter => filter is CompositeFilterItem && (filter as CompositeFilterItem).Filters.All(item => item.MemberName == "CreateTime")).SingleOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                //if (createTimeFilters != null) {
                //    var dateTime = createTimeFilters.Filters.ElementAt(1).Value as DateTime?;
                //    if (dateTime.HasValue)
                //        createTimeFilters.Filters.ElementAt(1).Value = new DateTime(dateTime.Value.Year, dateTime.Value.Month, dateTime.Value.Day, 23, 59, 59);
                //newCompositeFilterItem.Filters.Add(compositeFilterItem);
                //}
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override string OnRequestQueryName() {
            return "GetPartsPurchaseOrdersWithOrderTypeStatus";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                switch (parameterName)
                {
                    case "sparePartCode":
                        return filters.Filters.Single(item => item.MemberName == "SparePartCode").Value;
                    case "sparePartName":
                        return filters.Filters.Single(item => item.MemberName == "SparePartName").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
    }
}