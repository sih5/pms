﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurchaseOrderForSelectDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsPurchaseOrder_OrderType", "PartsShipping_Method", "PartsPurchaseOrder_Status"
        };

        public PartsPurchaseOrderForSelectDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    },new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "WarehouseName"
                    }, new ColumnItem {
                        Name = "PartsPurchaseOrderType.Name"
                    }, new ColumnItem {
                        Name = "ReceivingAddress"
                    }, new ColumnItem {
                        Name = "IfDirectProvision"
                    }, new ColumnItem {
                        Name = "ReceivingCompanyName"
                    }, new ColumnItem {
                        Name = "TotalAmount"
                    }, new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },  new ColumnItem {
                        Name = "RequestedDeliveryTime"
                    }, new ColumnItem {
                        Name = "ConfirmationRemark"
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "BranchName"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurchaseOrder);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsPurchaseOrderInfos";
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
        }
    }
}
