﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Media;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurchaseOrderForShippingDataGridView : DcsDataGridViewBase {

        protected readonly string[] KvNames = {
            "PartsShipping_Method","PartsPurchaseOrder_Status","PartsPurchaseOrder_OrderType","PurchaseInStatus"
        };

        public PartsPurchaseOrderForShippingDataGridView() {
            this.KeyValueManager.Register(this.KvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.KvNames[1]]
                    }, new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "WarehouseName"
                    }, new ColumnItem {
                        Name = "PartsSupplierCode"
                    }, new ColumnItem {
                        Name = "PartsSupplierName"
                    }, new ColumnItem {
                        Name = "PartsPurchaseOrderType.Name",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrder_PartsPurchaseOrderType
                    },new ColumnItem {
                       Name = "ReceivingAddress" 
                    },new ColumnItem {
                        Name = "IfDirectProvision"
                    },new ColumnItem {
                        Name = "OriginalRequirementBillCode"
                    },new ColumnItem {
                        Name = "ReceivingCompanyName"
                    }, new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]]
                    },new KeyValuesColumnItem {
                        Name = "InStatus",
                           KeyValueItems = this.KeyValueManager[this.KvNames[3]]
                    },new ColumnItem {
                        Name = "PlanSource"
                    },new ColumnItem {
                        Name = "AbandonOrStopReason"
                    }, new ColumnItem {
                        Name = "ConfirmationRemark"
                    }, new ColumnItem {
                        Name = "Remark"
                    },new ColumnItem {
                        Name = "CreatorName"
                    },new ColumnItem {
                        Name = "CreateTime"
                    },new ColumnItem {
                        Name = "ApproverName",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_InternalAllocationBill_ApproverName
                    },new ColumnItem {
                        Name = "ApproveTime",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_InternalAllocationBill_ApproveTime
                    }, new ColumnItem {
                        Name = "BranchName"
                    }, new ColumnItem {
                        Name = "PartsSalesCategoryName",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrder_PartsSalesCategoryName
                    }
                };
            }
        }

        protected override Core.IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsPurchaseOrder", "PartsPurchaseOrderDetailForShiping", "SupplierSupplierShippingOrderForPartsPurchaseOrder", "SupplierSPartsPurchaseOrderKanBanDetail"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurchaseOrder);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.Columns["PartsSupplierCode"].ShowColumnWhenGrouped = false;
            this.GridView.RowLoaded += GridView_RowLoaded;
        }

        private void GridView_RowLoaded(object sender, RowLoadedEventArgs e) {
            var partsPurchaseOrder = e.Row.DataContext as VirtualPartsPurchaseOrder;
            if(partsPurchaseOrder == null)
                return;
            GridViewRow gridViewRow = this.GridView.GetRowForItem(partsPurchaseOrder);
            if((partsPurchaseOrder.CreateTime != null && partsPurchaseOrder.CreateTime.Value.AddDays(15) < DateTime.Now) && partsPurchaseOrder.Status == (int)DcsPartsPurchaseOrderStatus.提交) {
                if(gridViewRow != null) {
                    gridViewRow.Background = new SolidColorBrush(Color.FromArgb(255, 255, 255, 0));
                }
            } else if(partsPurchaseOrder.Status == (int)DcsPartsPurchaseOrderStatus.部分发运 || partsPurchaseOrder.Status == (int)DcsPartsPurchaseOrderStatus.发运完毕) {               
                gridViewRow.Background = new SolidColorBrush(Colors.White);
            } else {
                if(gridViewRow != null)
                    gridViewRow.Background = new SolidColorBrush(Colors.White);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsPurchaseOrdersWithOrderTypeForSupplier";
        }
    }
}
