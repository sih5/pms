﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurchaseOrderKanBanDetailDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode"
                    }, new ColumnItem {
                        Name = "SparePartName"
                    }, new ColumnItem {
                        Name = "ConfirmedAmount",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_ConfirmedAmount
                    }, new ColumnItem {
                        Name = "ShippingQuantity",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingDetail_Quantitys
                    },new ColumnItem {
                        Name = "InspectedQuantity",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBillDetail_InspectedQuantity
                    },new ColumnItem {
                        Name = "ReceiptAmount",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBillDetail_ReceiptAmount
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurchaseOrderDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询采购订单看板";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var partsPurchaseOrder = this.DataContext as VirtualPartsPurchaseOrder;
            if(partsPurchaseOrder == null)
                return null;

            switch(parameterName) {
                case "partsPurchaseOrderId":
                    return partsPurchaseOrder.Id;
                case "originalRequirementBillId":
                    return partsPurchaseOrder.OriginalRequirementBillId;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        private void PartsPurchaseOrderKanBanDetailDataGridView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            this.ExecuteQueryDelayed();
        }

        public PartsPurchaseOrderKanBanDetailDataGridView() {
            this.DataContextChanged += this.PartsPurchaseOrderKanBanDetailDataGridView_DataContextChanged;
        }
    }
}
