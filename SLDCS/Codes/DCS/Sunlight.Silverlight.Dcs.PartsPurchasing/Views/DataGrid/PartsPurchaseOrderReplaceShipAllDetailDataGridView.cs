﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid
{
    public class PartsPurchaseOrderReplaceShipAllDetailDataGridView : DcsDataGridViewBase {
         private readonly string[] kvNames = {
            "TraceProperty"
        };
         public PartsPurchaseOrderReplaceShipAllDetailDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    
                    new ColumnItem {
                        Name = "PartsPurchaseOrderCode",
                        Title = PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_Code,
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "IfDirectProvision",
                        Title = PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_IfDirectProvision,
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "PartsPurchaseOrderTypeName",
                        Title = "订单类型",
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "SupplierPartCode",
                        Title = PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_SupplierPartCode,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_SparePartName,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "MeasureUnit",
                        Title = PartsPurchasingUIStrings.DataEditView_ImportTemplate_MeasureUnit,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "MinPackingAmount",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_MinPackingAmount,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "OrderAmount",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingDetail_ConfirmedAmount,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_ShippingAmount,
                        Name = "ShippingAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_UnShippingAmount,
                        Name = "UnShippingAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    } , new KeyValuesColumnItem {
                        Title = "追溯属性",
                        Name = "TraceProperty",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        
                    } 
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurchaseOrderDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("TmppartsPurchaseOrderDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            ((GridViewDataColumn)this.GridView.Columns["OrderAmount"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ShippingAmount"]).DataFormatString = "d";
            this.GridView.RowStyleSelector = this.GridViewStyleSelector();
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            //this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            //this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.BeginningEdit += GridView_BeginningEdit;
            this.DataPager.PageSize = 15;
        }

        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            var dataEdit = this.DataContext as PartsPurchaseOrderReplaceShipAllDataEditView;
            var partsPurchaseOrder = dataEdit.DataContext as PartsPurchaseOrder;
            if (partsPurchaseOrder == null)
                return;
            if (e.Cell.Column.UniqueName.Equals("UnShippingAmount"))
            {
                var partsPurchaseOrderDetail = e.Row.DataContext as PartsPurchaseOrderDetail;
                if (partsPurchaseOrderDetail == null)
                {
                    e.Cancel = true;
                    return;
                }
            }
        }

        private GridViewStyleSelector GridViewStyleSelector() {
            return new GridViewStyleSelector {
                ExecuteFunc = (item, container) => {
                    if (item is PartsPurchaseOrderDetail)
                    {
                        var partsPurchaseOrderDetail = item as PartsPurchaseOrderDetail;
                        if (partsPurchaseOrderDetail.UnShippingAmount > partsPurchaseOrderDetail.OrderAmount - partsPurchaseOrderDetail.ShippingAmount)
                        {
                            return this.Resources["BlueDataBackground"] as Style;
                        }
                        else
                        {
                            return this.Resources["WhiteDataBackground"] as Style;
                        }

                    }
                    return null;
                }
            };
        }

        private void GridView_RowLoaded(object sender, RowLoadedEventArgs e) {
            var partsPurchaseOrderDetail = e.Row.DataContext as PartsPurchaseOrderDetail;
            if (partsPurchaseOrderDetail == null)
                return;
            GridViewRow girdViewRow = this.GridView.GetRowForItem(partsPurchaseOrderDetail);
            if(girdViewRow == null)
                return;
            switch (partsPurchaseOrderDetail.UnShippingAmount > partsPurchaseOrderDetail.OrderAmount-partsPurchaseOrderDetail.ShippingAmount)
            {
                case false:
                    girdViewRow.Background = new SolidColorBrush(Colors.White);
                    break;
                case true:
                    girdViewRow.Background = new SolidColorBrush(Color.FromArgb(255, 199, 237, 204));
                    break;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }
    }
}
