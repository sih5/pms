﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurchaseOrderTypeDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };
        public PartsPurchaseOrderTypeDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "Name"
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new KeyValuesColumnItem {
                        Name = "ModifyTime",
                    }, new KeyValuesColumnItem {
                        Name = "AbandonerName",
                    }, new ColumnItem {
                        Name = "AbandonTime"
                    }, new ColumnItem {
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrder_PartsSalesCategoryName,
                        Name = "PartsSalesCategory.Name"
                    }
                };
            }
        }
        protected override string OnRequestQueryName() {
            return "GetPartsPurchaseOrderTypesWithPartsSalesCategory";
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurchaseOrderType);
            }
        }
    }
}
