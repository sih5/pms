﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Media;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System.Collections.ObjectModel;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurchaseOrderWithDetailsDataGridView : DcsDataGridViewBase {

        protected readonly string[] KvNames = {
            "PartsShipping_Method","PartsPurchaseOrder_Status","PartsPurchaseOrder_OrderType","PurchaseInStatus"
        };
        private readonly ObservableCollection<KeyValuePair> kvIoStatus = new ObservableCollection<KeyValuePair>();
        public PartsPurchaseOrderWithDetailsDataGridView() {
            this.KeyValueManager.Register(this.KvNames);

             this.kvIoStatus.Add(new KeyValuePair
            {
                Key = 2,
                Value = PartsPurchasingUIStrings.QueryPanel_QueryItem_IoStatus_Success
            });
            this.kvIoStatus.Add(new KeyValuePair
            {
                Key = 0,
                Value = PartsPurchasingUIStrings.QueryPanel_QueryItem_IoStatus_WaitTransfer
            });
            this.kvIoStatus.Add(new KeyValuePair
            {
                Key = 1,
                Value = PartsPurchasingUIStrings.QueryPanel_QueryItem_IoStatus_Fail
            });
            this.kvIoStatus.Add(new KeyValuePair {
                Key = 3,
                Value = PartsPurchasingUIStrings.QueryPanel_QueryItem_IoStatus_IsPassing
            });
        }

        protected override IEnumerable<ColumnItem> ColumnItems {

            get {
                return new[] {
                        new KeyValuesColumnItem {
                            Name = "Status",
                            KeyValueItems = this.KeyValueManager[this.KvNames[1]],
                            Title =  PartsPurchasingUIStrings.DataGridView_ColumnItem_OrderStatus
                        }, new ColumnItem {
                            Name = "Code",
                            Title =  Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "Code")
                        }, new ColumnItem {
                            Name = "WarehouseName",
                            Title =  Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "WarehouseName")
                        }
                        , new ColumnItem {
                            Name = "PartsSupplierCode",
                            //IsDefaultGroup = true,
                            Title =  Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "PartsSupplierCode")
                        }, new ColumnItem {
                            Name = "PartsSupplierName",
                            Title =  Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "PartsSupplierName")
                        },new ColumnItem {
                            Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_SupplierBusinessCode,
                            Name = "BusinessCode"
                        }, new ColumnItem {
                            Name = "PartsPurchaseOrderTypeName",
                            Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrder_PartsPurchaseOrderType
                        },new ColumnItem {
                           Name = "ReceivingAddress" ,
                            Title =  Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "ReceivingAddress")
                        },new ColumnItem {
                            Name = "IfDirectProvision",
                            Title =  Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "IfDirectProvision")
                        },new ColumnItem {
                        Name = "IsPack",
                        Title=PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrderPlan_IsPack
                        },new ColumnItem {
                            Name = "OriginalRequirementBillCode",
                            Title =  Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "OriginalRequirementBillCode")
                        },new ColumnItem {
                            Name = "ReceivingCompanyName",
                            Title =  Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "ReceivingCompanyName")
                        },new ColumnItem {
                            Name = "TotalAmount",
                            Title =  Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "TotalAmount")
                        }, new KeyValuesColumnItem {
                            Name = "ShippingMethod",
                            KeyValueItems = this.KeyValueManager[this.KvNames[0]],
                            Title =  Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "ShippingMethod")
                        },new KeyValuesColumnItem {
                            Name = "InStatus",
                            KeyValueItems = this.KeyValueManager[this.KvNames[3]],
                            Title =  Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "InStatus")
                        },new ColumnItem {
                            Name = "PlanSource",
                            Title =  Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "PlanSource")
                        },new ColumnItem {
                            Name = "AbandonOrStopReason",
                            Title =  Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "AbandonOrStopReason")
                        }, new ColumnItem {
                            Name = "ConfirmationRemark",
                            Title =  Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "ConfirmationRemark")
                        }, new ColumnItem {
                            Name = "Remark",
                            Title =  Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "Remark")
                        }, new ColumnItem {
                            Name = "IsTransSap",
                            Title = PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrderPlan_IsTransferSAP
                        }, new KeyValuesColumnItem {
                            Name = "IoStatus",
                            KeyValueItems = this.kvIoStatus,
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_IoStatus
                        },new ColumnItem {
                            Name = "CreatorName",
                            Title =  Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "CreatorName")
                        },new ColumnItem {
                            Name = "CreateTime",
                            Title =  Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "CreateTime")
                        },new ColumnItem {
                            Name = "SubmitterName",
                            Title =  Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "SubmitterName")
                        },new ColumnItem {
                            Name = "SubmitTime",
                            Title =  Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "SubmitTime")
                        }, new ColumnItem {
                            Name = "BranchName",
                            Title =  Utils.GetEntityLocalizedName(typeof(PartsPurchaseOrder), "BranchName")
                        }, new ColumnItem {
                            Name = "PartsSalesCategoryName",
                            Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrder_PartsSalesCategoryName
                        }, new ColumnItem {
                            Name = "Message",
                            Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_InterfaceMessage
                        }
                    };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPartsPurchaseOrder);
            }
        }

        protected override Core.IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsPurchaseOrder", "PartsPurchaseOrderDetail", "SupplierShippingOrderForPartsPurchaseOrder", "PartsPurchaseOrderKanBanDetail"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.Columns["PartsSupplierCode"].ShowColumnWhenGrouped = false;
            ((GridViewDataColumn)this.GridView.Columns["TotalAmount"]).DataFormatString = "c2";
            this.GridView.RowLoaded += GridView_RowLoaded;
        }

        private void GridView_RowLoaded(object sender, RowLoadedEventArgs e) {
            var partsPurchaseOrder = e.Row.DataContext as VirtualPartsPurchaseOrder;
            if(partsPurchaseOrder == null)
                return;
            GridViewRow gridViewRow = this.GridView.GetRowForItem(partsPurchaseOrder);
            if((partsPurchaseOrder.CreateTime != null && partsPurchaseOrder.CreateTime.Value.AddDays(15) < DateTime.Now) && partsPurchaseOrder.Status == (int)DcsPartsPurchaseOrderStatus.提交) {
                if(gridViewRow != null) {
                    gridViewRow.Background = new SolidColorBrush(Color.FromArgb(255, 255, 255, 0));
                }
            } else if(partsPurchaseOrder.Status == (int)DcsPartsPurchaseOrderStatus.部分发运 || partsPurchaseOrder.Status == (int)DcsPartsPurchaseOrderStatus.发运完毕) {
                //var domainContext = new DcsDomainContext();
                //domainContext.Load(domainContext.GetSupplierShippingOrdersQuery().Where(r => r.PartsPurchaseOrderId == partsPurchaseOrder.Id && r.Status == (int)DcsSupplierShippingOrderStatus.新建), LoadBehavior.RefreshCurrent, loadOP => {
                //    if(loadOP.HasError) {
                //        if(!loadOP.IsErrorHandled) {
                //            loadOP.MarkErrorAsHandled();
                //            DcsUtils.ShowLoadError(loadOP);
                //            return;
                //        }
                //    }
                //    var entity = loadOP.Entities.FirstOrDefault();
                //    if(gridViewRow != null) {
                //        if(entity != null && partsPurchaseOrder.RequestedDeliveryTime.AddDays(7) < DateTime.Now) {
                //            gridViewRow.Background = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                //        } else {
                //            gridViewRow.Background = new SolidColorBrush(Colors.White);
                //        }
                //    }
                //}, null);
            } else {
                if(gridViewRow != null)
                    gridViewRow.Background = new SolidColorBrush(Colors.White);
            }

        }

        protected override string OnRequestQueryName() {
            return "根据供应商编码查询人员采购订单";
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "businessCode":
                        var businessCode = filters.Filters.SingleOrDefault(item => item.MemberName == "BranchSupplierRelation.BusinessCode");
                        return businessCode == null ? null : businessCode.Value;
                    case "ioStatus":
                        var ioStatus = filters.Filters.SingleOrDefault(item => item.MemberName == "IoStatus");
                        return ioStatus == null ? null : ioStatus.Value;
                    case "sparePartCode":
                        var sparePartCode = filters.Filters.SingleOrDefault(item => item.MemberName == "SparePartCode");
                        return sparePartCode == null ? null : sparePartCode.Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "BranchSupplierRelation.BusinessCode" && filter.MemberName != "IoStatus" && filter.MemberName != "SparePartCode"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }
}
