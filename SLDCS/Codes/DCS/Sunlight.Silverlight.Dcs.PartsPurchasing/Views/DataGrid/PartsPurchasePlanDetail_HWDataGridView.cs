﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Controls;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurchasePlanDetail_HWDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem{
                        Name="PartCode",
                        Title=PartsPurchasingUIStrings.DataGridView_Title_PartNotConPurchasePlan_PartCode
                    },new ColumnItem{
                        Name="PartName",
                        Title= PartsPurchasingUIStrings.DataGridView_Title_PartNotConPurchasePlan_PartName
                    },new ColumnItem{
                        Name="GPMSPartCode",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_GPMSPartCode
                    },new ColumnItem{
                        Name="PartsPurchasePlanQty",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_PartsPurchasePlanQty
                    },new ColumnItem{
                        Name="BJCDCStock",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_BJCDCStock
                    },new ColumnItem{
                        Name="SDCDCStock",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_SDCDCStock
                    },new ColumnItem{
                        Name="GDCDCStock",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_GDCDCStock
                    },new ColumnItem{
                        Name="UntFulfilledQty",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_UntFulfilledQty
                    },new ColumnItem{
                        Name="EndAmount",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_EndAmount
                    },new ColumnItem{
                        Name="CanAnalyzeQty",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_CanAnalyzeQty
                    },new ColumnItem{
                        Name="UnitPrice",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_UnitPrice
                    },new ColumnItem{
                        Name="MeasureUnit",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_MeasureUnit
                    },new ColumnItem{
                        Name="GPMSRowNum",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_GPMSRowNum
                    },new ColumnItem{
                        Name="OverseasSuplierCode",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_OverseasSuplierCode
                    },new ColumnItem{
                        Name="PartsSupplierCode",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_PartsSupplierCode
                    },new ColumnItem{
                        Name="PartsSupplierName",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_PartsSupplierName
                    },new ColumnItem{
                        Name="SuplierCode",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_SupplierCode
                    },new ColumnItem{
                        Name="RequestedDeliveryTime",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_RequestedDeliveryTime
                    },new ColumnItem{
                        Name="WarehouseCode",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_WarehouseCode
                    },new ColumnItem{
                        Name="WarehouseName",
                        Title= PartsPurchasingUIStrings.DataGridView_ColumnItem_WarehouseName
                    },new ColumnItem{
                        Name="FaultReason",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_FaultReason
                    },new ColumnItem{
                        Name="Remark",
                        Title= PartsPurchasingUIStrings.DataGridView_ColumnItem_Remark
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurchasePlanDetail_HWEx);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsPurchasePlanDetail_HWsWithPIP";
        }

        private void PartsPurchasePlanDetail_HWDataGridView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var partsPurchasePlan_HW = e.NewValue as PartsPurchasePlan_HW;
            if (partsPurchasePlan_HW == null || partsPurchasePlan_HW.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsPurchasePlanId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsPurchasePlan_HW.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        public PartsPurchasePlanDetail_HWDataGridView() {
            this.DataContextChanged += this.PartsPurchasePlanDetail_HWDataGridView_DataContextChanged;
        }


        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DomainDataSource.PageSize = 100;
            this.GridView.Columns.Insert(0, new GridViewSerialNumberColumn {
                Header = PartsPurchasingUIStrings.DataGridView_ColumnItem_Number
            });
        }

        public Uri Icon {
            get {
                return null;
            }
        }

        public string Title {
            get {
                return PartsPurchasingUIStrings.DetailPanel_Title_PartsPurchasePlanDetail_HW;
            }
        }
    }
}
