﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurchasePlanHWDataGridView : DcsDataGridViewBase {

        protected readonly string[] KvNames = { "PurchasePlanType", "PurchasePlanStatus" };

        public PartsPurchasePlanHWDataGridView() {
            this.KeyValueManager.Register(KvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[]{
                   new KeyValuesColumnItem{
                        Name="Status",
                        Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Status,
                        KeyValueItems=this.KeyValueManager[this.KvNames[1]]
                    }, new ColumnItem{
                        Name="Code",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_OsaPurchaseOrderCode
                    },new ColumnItem{
                        Name="PartsSalesCategoryName",
                        Title= PartsPurchasingUIStrings.DataGridView_ColumnItem_SupplierPlanArrear_PartsSalesCategoryName
                    },new KeyValuesColumnItem{
                        Name="PurchasePlanType",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrder_PartsPurchaseOrderType,
                        KeyValueItems=this.KeyValueManager[this.KvNames[0]]
                    },new ColumnItem{
                        Name="TotalAmount",
                        Title= PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseRtnSettleBill_TotalSettlementAmount
                    },new ColumnItem{
                        Name="SAPPurchasePlanCode",
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_SAPPurchasePlanCode
                    },new ColumnItem{
                        Name="Remark",
                        Title= PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_Remark
                    },new ColumnItem{
                        Name="CreatorName",
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_CreatorName
                    },new ColumnItem{
                        Name="CreateTime",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_BillCreateTime
                    },new ColumnItem{
                        Name="ModifierName",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Modifier
                    },new ColumnItem{
                        Name="ModifyTime",
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_ModifyTime
                    },new ColumnItem{
                        Name="StoporName",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_StoporName
                    },new ColumnItem{
                        Name="StopTime",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_StopTime
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsPurchasePlan_HW";
        }

        protected override Type EntityType {
            get { return typeof(PartsPurchasePlan_HW); }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }
        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsPurchasePlanDetail_HW"
                    }
                };
            }
        }
    }
}
