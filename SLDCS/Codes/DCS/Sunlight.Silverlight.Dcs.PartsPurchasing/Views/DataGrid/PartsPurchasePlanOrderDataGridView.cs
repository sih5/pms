﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;
using System.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;


namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsPurchasePlanOrderDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
             "PurchasePlanOrderStatus","PartsPurchaseOrder_OrderType"
        };
        public PartsPurchasePlanOrderDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override Type EntityType {
            get {
                return typeof(PartsPurchasePlan);
            }
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_Status
                    },new ColumnItem {
                        IsSortable = false,
                        Name = "IsUplodFile",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_IsUplodFile
                    }, new ColumnItem {
                        Name = "Code",
                        Title= PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_Code
                    },new ColumnItem {
                        Name = "WarehouseName",
                        Title= PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_WarehouseName
                    }, new ColumnItem {
                        Name = "PartsPurchaseOrderType.Name",
                        //KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title= PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrderType
                    }, new ColumnItem {
                        Name = "FeeSum",
                        Title= PartsPurchasingUIStrings.DataGridView_ColumnItem_FeeSum
                    },new ColumnItem {
                        Name = "PlanAmountSum",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_PlanAmountSum
                    }
                    ,new ColumnItem {
                        Name = "IsTransSap",
                        Title=PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrderPlan_IsTransferSAP
                    },new ColumnItem {
                        Name = "IsPack",
                        Title=PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrderPlan_IsPack
                    }
                    , new ColumnItem {
                        Name = "CreatorName",
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_CreatorName
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_CreateTime
                    }, new ColumnItem {
                        Name = "ModifierName",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Modifier
                    }, new ColumnItem {
                        Name = "ModifyTime",
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_ModifyTime
                    }, new ColumnItem {
                        Name = "CheckerName",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_Checker
                    },new ColumnItem {
                        Name = "CheckTime",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_CheckTime
                    },new ColumnItem {
                        Name = "CheckMemo",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_CheckMemo
                    }
                    , new ColumnItem {
                        Name = "CloserName",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Approver
                    }, new ColumnItem {
                        Name = "CloseTime",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_ApproveTime
                    },new ColumnItem {
                        Name = "CloseMemo",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_FinalApproveMemo
                    },new ColumnItem {
                        Name = "AbandonComment",
                        Title=PartsPurchasingUIStrings.DataEditView_Title_AbandonReason
                    }, new ColumnItem {
                        Name = "Memo",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Remark
                    }, new ColumnItem {
                         Name = "BranchCode",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_BranchCode
                    }, new ColumnItem {
                        Name = "BranchName",
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsBranch_BranchName
                    },new ColumnItem {
                        Name = "PartsSalesCategoryName",
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsBranch_PartsSalesCategoryName
                    }, new ColumnItem {
                        Name = "ApproverName",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_InitialApprover
                    },new ColumnItem {
                        Name = "ApproveTime",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_InitialApproveTime
                    },new ColumnItem {
                        Name = "ApproveMemo",
                        Title=PartsPurchasingUIStrings.DataEditPanel_Title_InitialApproveComment
                    }
                };
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return base.OnRequestFilterDescriptor(queryName);
            var newCompositeFilterItem = new CompositeFilterItem();
            foreach(var filter in compositeFilterItem.Filters.Where(item => item.MemberName != "SparePartCode"))
                newCompositeFilterItem.Filters.Add(filter);
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filter = this.FilterItem as CompositeFilterItem;
            if(filter == null)
                return null;
            FilterItem filterItem;
            switch(parameterName) {
                case "sparePartCode":
                    filterItem = filter.Filters.SingleOrDefault(item => item.MemberName == "SparePartCode");
                    return filterItem == null ? null : filterItem.Value;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override string OnRequestQueryName() {
            return "GetPartsPurchasePlanOrder";
        }
        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsPurchasePlanOrder"
                    }
                };
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            //((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            //((GridViewDataColumn)this.GridView.Columns["ModifyTime"]).DataFormatString = "d";
            //((GridViewDataColumn)this.GridView.Columns["ApproveTime"]).DataFormatString = "d";
            //((GridViewDataColumn)this.GridView.Columns["CloseTime"]).DataFormatString = "d";
        }
    }
}
