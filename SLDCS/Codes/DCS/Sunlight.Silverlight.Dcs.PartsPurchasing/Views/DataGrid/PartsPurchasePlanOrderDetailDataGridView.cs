﻿
using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid
{
    public class PartsPurchasePlanOrderDetailDataGridView : DcsDataGridViewBase {

        protected override Type EntityType {
            get {
                return typeof(PartsPurchasePlanDetail);
            }
        }

        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }

        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
        }

        public PartsPurchasePlanOrderDetailDataGridView()
        {

            this.DataContextChanged += PartsPurchasePlanOrderDetailDataGridView_DataContextChanged;
        }

        protected override string OnRequestQueryName()
        {
            return "GetPartsPurchasePlanDetailsWithSpareParts";
        }
        void PartsPurchasePlanOrderDetailDataGridView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var partsPurchasePlan = e.NewValue as PartsPurchasePlan;
            if (partsPurchasePlan == null || partsPurchasePlan.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem
            {
                MemberName = "PurchasePlanId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsPurchasePlan.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true,
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Number
                    }, new ColumnItem {
                        Name = "SupplierPartCode",
                        IsReadOnly = true,
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_SupplierPartCode
                    },new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true,
                        Title=PartsPurchasingUIStrings.DataEditView_Text_SparePartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true,
                        Title=PartsPurchasingUIStrings.DataEditView_Text_SparePartName
                    },
                    new ColumnItem {
                        Name = "PlanAmount",
                        Title=PartsPurchasingUIStrings.DataEditView_ImportTemplate_PlanAmount
                    }, new ColumnItem {
                        Name = "MeasureUnit",
                        IsReadOnly = true,
                        Title=PartsPurchasingUIStrings.DataEditView_ImportTemplate_MeasureUnit
                    }
                    , new ColumnItem {
                        Name = "ABCStrategy",
                        IsReadOnly = true,
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_ABCStrategy
                    }
                };
            }
        }
    }
}
