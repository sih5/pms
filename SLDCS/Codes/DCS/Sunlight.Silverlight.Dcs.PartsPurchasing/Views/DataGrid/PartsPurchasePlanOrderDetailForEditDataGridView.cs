﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurchasePlanOrderDetailForEditDataGridView : DcsDataGridViewBase {
        private DcsMultiPopupsQueryWindowBase sparePartForPartsPurchaseOrderDropDownQueryWindow;
        private RadWindow radQueryWindow;

        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = this.SparePartForPartsPurchaseOrderDropDownQueryWindow,
                    Header = PartsPurchasingUIStrings.QueryPanel_Title_SparePartForPartsPurchaseOrder,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen
                });
            }
        }

        private DcsMultiPopupsQueryWindowBase SparePartForPartsPurchaseOrderDropDownQueryWindow {
            get {
                if(this.sparePartForPartsPurchaseOrderDropDownQueryWindow == null) {
                    this.sparePartForPartsPurchaseOrderDropDownQueryWindow = DI.GetQueryWindow("SparePartForPartsPurchasePlanMulti") as DcsMultiPopupsQueryWindowBase;
                    this.sparePartForPartsPurchaseOrderDropDownQueryWindow.SelectionDecided += this.SparePartForPartsPurchaseOrderDropDownQueryWindow_SelectionDecided;
                    this.sparePartForPartsPurchaseOrderDropDownQueryWindow.Loaded += this.SparePartForPartsPurchaseOrderDropDownQueryWindow_Loaded;
                }
                return this.sparePartForPartsPurchaseOrderDropDownQueryWindow;
            }
        }

        private void SparePartForPartsPurchaseOrderDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var spareParts = queryWindow.SelectedEntities.Cast<VirtualSparePart>();
            if(spareParts == null)
                return;
            var partsPurchasePlan = this.DataContext as PartsPurchasePlan;
            if(partsPurchasePlan == null)
                return;
            if(partsPurchasePlan.PartsPurchasePlanDetails.Any(r => spareParts.Any(ex => ex.Code == r.SparePartCode))) {
                UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Error_SparePart_DoubleSparePartID));
                return;
            }
            foreach(var sparePart in spareParts) {
                var partsPurchasePlanDetail = new PartsPurchasePlanDetail();
                partsPurchasePlanDetail.SerialNumber = this.GridView.Items.Count > 0 ? this.GridView.Items.Cast<PartsPurchasePlanDetail>().Max(entity => entity.SerialNumber) + 1 : 1;
                partsPurchasePlanDetail.SparePartId = sparePart.Id;
                partsPurchasePlanDetail.SparePartCode = sparePart.Code;
                partsPurchasePlanDetail.SparePartName = sparePart.Name;
                partsPurchasePlanDetail.MeasureUnit = sparePart.MeasureUnit;
                partsPurchasePlanDetail.PackingAmount = sparePart.PackingAmount;
                partsPurchasePlanDetail.PackingSpecification = sparePart.PackingSpecification;
                partsPurchasePlanDetail.SupplierPartCode = sparePart.SupplierPartCode;
                partsPurchasePlanDetail.SuplierName = sparePart.SupplierPartName;
                partsPurchasePlanDetail.Price = sparePart.PurchasePrice;
                if(sparePart.PackingCoefficient.HasValue) {
                    partsPurchasePlanDetail.PlanAmount = sparePart.PackingCoefficient.Value;
                    partsPurchasePlanDetail.PackingCoefficient = sparePart.PackingCoefficient;
                }
                partsPurchasePlanDetail.LimitQty = sparePart.LimitQty;
                partsPurchasePlanDetail.UsedQty = sparePart.UsedQty;
                partsPurchasePlanDetail.ABCStrategy = Enum.GetName(typeof(DcsABCStrategyCategory), sparePart.PartABC == null ? 0 : sparePart.PartABC);
                partsPurchasePlan.PartsPurchasePlanDetails.Add(partsPurchasePlanDetail);
            }
            this.GridView.IsReadOnly = false;
            var dataGridView = queryWindow.ChildrenOfType<DcsDataGridViewBase>().Cast<SparePartForPartsPurchasePlanDataGridView>().ToArray();
            if(dataGridView.Any()) {
                dataGridView.First().ClearSelected();
            }

        }

        private void SparePartForPartsPurchaseOrderDropDownQueryWindow_Loaded(object sender, RoutedEventArgs routedEventArgs) {
            var queryWindowSparePartForPartsPurchaseOrder = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindowSparePartForPartsPurchaseOrder == null)
                return;
            var partsPurchasePlan = this.DataContext as PartsPurchasePlan;
            if(partsPurchasePlan == null)
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem("BranchId", typeof(int), FilterOperator.IsEqualTo, BaseApp.Current.CurrentUserData.EnterpriseId));
            compositeFilterItem.Filters.Add(new FilterItem("SparePartIds", typeof(int[]), FilterOperator.IsEqualTo, new int[] { }));
            compositeFilterItem.Filters.Add(new FilterItem("PartsSaleCategoryId", typeof(int), FilterOperator.IsEqualTo, partsPurchasePlan.PartsSalesCategoryId));

            queryWindowSparePartForPartsPurchaseOrder.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);

            queryWindowSparePartForPartsPurchaseOrder.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "BranchName",""
            });
            queryWindowSparePartForPartsPurchaseOrder.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "BranchName", false
            });
            queryWindowSparePartForPartsPurchaseOrder.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common","PartsSalesCategoryName",partsPurchasePlan.PartsSalesCategoryName
            });
            queryWindowSparePartForPartsPurchaseOrder.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "PartsSalesCategoryName", false
            });
            queryWindowSparePartForPartsPurchaseOrder.ExchangeData(null, "RefreshQueryResult", null);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true,
                        Title= PartsPurchasingUIStrings.DataGridView_ColumnItem_Number
                    },new ColumnItem {
                        Name = "SupplierPartCode",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_SupplierPartCode,
                        IsReadOnly = true,
                    },new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true,
                        Title=PartsPurchasingUIStrings.DataGridView_Title_PartNotConPurchasePlan_PartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true,
                        Title=PartsPurchasingUIStrings.DataGridView_Title_PartNotConPurchasePlan_PartName
                    },
                    new ColumnItem {
                        Name = "PackingCoefficient",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_PackingCoefficient,
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "PlanAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title=PartsPurchasingUIStrings.DataEditView_ImportTemplate_PlanAmount
                    }, new ColumnItem {
                        Name = "MeasureUnit",
                        IsReadOnly = true,
                        Title = PartsPurchasingUIStrings.DataEditView_ImportTemplate_MeasureUnit
                    }
                    , new ColumnItem {
                        Name = "ABCStrategy",
                        IsReadOnly = true,
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_ABCStrategy
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurchasePlanDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DataContextChanged += PartsPurchaseOrderDetailForEditDataGridView_DataContextChanged;
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.BeginningEdit += GridView_BeginningEdit;
            this.GridView.ValidatesOnDataErrors = GridViewValidationMode.InViewMode;
            this.GridView.Deleting += this.GridView_Deleting;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
            this.GridView.CellValidating += GridView_CellValidating;
        }

        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            var partsPurchasePlan = this.DataContext as PartsPurchasePlan;
            if(partsPurchasePlan == null)
                return;
        }

        private void PartsPurchaseOrderDetailForEditDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsPurchasePlan = this.DataContext as PartsPurchasePlan;
            if(partsPurchasePlan == null)
                return;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.CanUserInsertRows = true;
        }

        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
            switch(e.Cell.DataColumn.UniqueName) {
                case "PlanAmount":
                    if(!(e.NewValue is int) || (int)e.NewValue <= 0) {
                        e.IsValid = false;
                        e.ErrorMessage = PartsPurchasingUIStrings.DataEditView_Validation_PartsInboundCheckBillDetail_InspectedQuantity_IsOrderAmount;
                    } else {
                        var detail = e.Cell.DataContext as PartsPurchasePlanDetail;
                        if(detail == null)
                            return;
                        if((int)e.NewValue % detail.PackingCoefficient.Value != 0 && detail.PartsPurchasePlan.PartsPurchaseOrderType.Name.IndexOf("急") < 0 && detail.PartsPurchasePlan.PartsPurchaseOrderType.Name != "中心库采购") {
                            e.IsValid = false;
                            e.ErrorMessage = PartsPurchasingUIStrings.DataGridView_Notification_PackingCoefficientError;
                        }
                    }
                    break;
            }
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            var partsPurchasePlan = this.DataContext as PartsPurchasePlan;
            if(partsPurchasePlan == null)
                return;
            if(!partsPurchasePlan.PartsSalesCategoryId.HasValue) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_PartsPurchaseOrderType_PartsSalesCategoryIdIsNotNull);
                return;
            }
            this.RadQueryWindow.ShowDialog();
        }

        private void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            var partsPurchasePlan = this.DataContext as PartsPurchasePlan;
            if(partsPurchasePlan == null)
                return;
            var serialNumber = 1;
            foreach(var partsPurchaseOrderDetail in partsPurchasePlan.PartsPurchasePlanDetails.Where(entity => !e.Items.Contains(entity))) {
                partsPurchaseOrderDetail.SerialNumber = serialNumber;
                serialNumber++;
            }
        }
        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsPurchasePlanDetails");
        }
    }
}