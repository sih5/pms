﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid
{
    public class PartsPurchasePlanOrderDetailForShowDataGridView : DcsDataGridViewBase
    {
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true,
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Number,
                        IsSortDescending=false
                    },new ColumnItem {
                        Name = "SupplierPartCode",
                        IsReadOnly = true,
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_SupplierPartCode,
                        IsSortDescending=false
                    },new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true,
                        Title=PartsPurchasingUIStrings.DataGridView_Title_PartNotConPurchasePlan_PartCode,
                        IsSortDescending=false
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true,
                        Title=PartsPurchasingUIStrings.DataGridView_Title_PartNotConPurchasePlan_PartName
                    },
                    new ColumnItem {
                        Name = "PlanAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title=PartsPurchasingUIStrings.DataEditView_ImportTemplate_PlanAmount,
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "MeasureUnit",
                        IsReadOnly = true,
                        Title=PartsPurchasingUIStrings.DataEditView_ImportTemplate_MeasureUnit
                    }, new ColumnItem {
                        Name = "Price",
                        IsReadOnly = true,
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PurchasePrice
                    }
                    , new ColumnItem {
                        Name = "ABCStrategy",
                        IsReadOnly = true,
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_ABCStrategy
                    }
                };
            }
        }

        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }

        protected override bool UsePaging
        {
            get
            {
                return false;
            }
        }

        protected override Type EntityType
        {
            get
            {
                return typeof(PartsPurchasePlanDetail);
            }
        }

        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.ValidatesOnDataErrors = GridViewValidationMode.InViewMode;
        }

        protected override string OnRequestQueryName()
        {
            return "GetPartsPurchasePlanDetailsWithOrder";
        }
        protected override Binding OnRequestDataSourceBinding()
        {
            return new Binding("PartsPurchasePlanDetails");
        }
    }
}