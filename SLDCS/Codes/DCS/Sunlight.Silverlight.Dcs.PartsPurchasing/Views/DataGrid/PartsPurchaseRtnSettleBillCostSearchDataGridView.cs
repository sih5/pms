﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurchaseRtnSettleBillCostSearchDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "BillCode",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_BillCode
                    },new ColumnItem {
                        Name = "BillBusinessType",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_BillBusinessType
                    },new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsPurchasingUIStrings.DataGridView_Title_PartNotConPurchasePlan_PartCode
                    },new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsPurchasingUIStrings.DataGridView_Title_PartNotConPurchasePlan_PartName
                    },new ColumnItem {
                        Name = "Quantity",
                        Title = PartsPurchasingUIStrings.DataGridView_Column_Amount
                    },new ColumnItem {
                        Name = "SettlementPrice",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SettlementPrice
                    },new ColumnItem{
                        Name="SettlementAmount",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_SettlementAmount
                    },new ColumnItem {
                        Name = "PlanPrice",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_CostPrice
                    },new ColumnItem {
                        Name = "PlanAmount",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_CostAmount
                    },new ColumnItem{
                        Name="MaterialCostVariance",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_MaterialCostVariance
                    }
                };
            }
        }
        protected override string OnRequestQueryName() {
            return "查询虚拟配件采购退货结算成本";
        }
        protected override Type EntityType {
            get {
                return typeof(VirtualPartsPurchaseRtnSettleBill);
            }
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
        protected override bool UsePaging {
            get {
                return true;
            }
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var FilterItem = filters.Filters.FirstOrDefault(item => item.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                switch(parameterName) {
                    case "partsPurchaseRtnSettleBillId":
                        return FilterItem.Filters.Single(item => item.MemberName == "PartsPurchaseRtnSettleBillId").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var newCompositeFilterItem = new CompositeFilterItem();
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }
}
