﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurchaseRtnSettleBillDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsPurchaseSettleBill_SettlementPath","PartsPurchaseSettle_Status","PartsPurchaseRtnSettleBill_InvoicePath"
        };

        public PartsPurchaseRtnSettleBillDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPartsPurchaseRtnSettleBillWithBusinessCode);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title = Utils.GetEntityLocalizedName(typeof(PartsPurchaseRtnSettleBill), "Status")
                    }, new ColumnItem {
                        Name = "InvoiceDate",
                        Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseRtnSettleBill_InvoiceDate
                    },new ColumnItem {
                        Name = "Code",
                        Title = Utils.GetEntityLocalizedName(typeof(PartsPurchaseRtnSettleBill), "Code")
                    }, new ColumnItem {
                        Name = "WarehouseName",
                        Title = Utils.GetEntityLocalizedName(typeof(PartsPurchaseRtnSettleBill), "WarehouseName")
                    }, new ColumnItem {
                        Name = "PartsSupplierCode",
                        Title = Utils.GetEntityLocalizedName(typeof(PartsPurchaseRtnSettleBill), "PartsSupplierCode")
                    }, new ColumnItem {
                        Name = "PartsSupplierName",
                        Title = Utils.GetEntityLocalizedName(typeof(PartsPurchaseRtnSettleBill), "PartsSupplierName")
                    }, 
                    new AggregateColumnItem {
                        Name = "TotalSettlementAmount",
                        AggregateType = AggregateType.Sum,
                        TextAlignment = TextAlignment.Right,
                        Title = Utils.GetEntityLocalizedName(typeof(PartsPurchaseRtnSettleBill), "TotalSettlementAmount")
                    }, 
                    new ColumnItem {
                        Name = "TaxRate",
                        Title = Utils.GetEntityLocalizedName(typeof(PartsPurchaseRtnSettleBill), "TaxRate")
                    }, new ColumnItem {
                        Name = "Tax",
                        Title = Utils.GetEntityLocalizedName(typeof(PartsPurchaseRtnSettleBill), "Tax")
                    }, new ColumnItem {
                        Name = "InvoiceTotalAmount",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_InvoiceTotalAmount
                    }, new ColumnItem {
                        Name = "InvoiceAmountDifference",
                        Title = Utils.GetEntityLocalizedName(typeof(PartsPurchaseRtnSettleBill), "InvoiceAmountDifference")
                    }, new ColumnItem {
                        Name = "TotalAmount",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_TotalAmount
                    }, new KeyValuesColumnItem {
                        Name = "InvoicePath",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]],
                        Title = Utils.GetEntityLocalizedName(typeof(PartsPurchaseRtnSettleBill), "InvoicePath")
                    }, new KeyValuesColumnItem {
                        Name = "SettlementPath",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = Utils.GetEntityLocalizedName(typeof(PartsPurchaseRtnSettleBill), "SettlementPath")
                    }, new ColumnItem {
                        Name = "OffsettedSettlementBillCode",
                        Title = Utils.GetEntityLocalizedName(typeof(PartsPurchaseRtnSettleBill), "OffsettedSettlementBillCode")
                    }, new ColumnItem {
                        Name = "Remark",
                        Title = Utils.GetEntityLocalizedName(typeof(PartsPurchaseRtnSettleBill), "Remark")
                    }, new ColumnItem {
                        Name = "CreatorName",
                        Title = Utils.GetEntityLocalizedName(typeof(PartsPurchaseRtnSettleBill), "CreatorName")
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title = Utils.GetEntityLocalizedName(typeof(PartsPurchaseRtnSettleBill), "CreateTime")
                    }, new ColumnItem {
                        Name = "BranchName",
                        Title = Utils.GetEntityLocalizedName(typeof(PartsPurchaseRtnSettleBill), "BranchName")
                    },new ColumnItem {
                        Name = "PartsSalesCategoryName",
                         Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseRtnSettleBill_CategoryName                       
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsPurchaseRtnSettleBillsWithBusinessCode";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsPurchaseRtnSettleBill","PartsPurchaseRtnSettleDetailForSupplier","PartsPurchaseRtnSettleRef","PartsPurchaseRtnSettleBillForInvoice"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            ((GridViewDataColumn)this.GridView.Columns["Tax"]).DataFormatString = "c2";
            base.OnControlsCreated();
        }
    }
}
