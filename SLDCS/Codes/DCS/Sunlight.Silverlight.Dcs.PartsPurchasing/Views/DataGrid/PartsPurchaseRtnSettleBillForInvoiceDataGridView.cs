﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurchaseRtnSettleBillForInvoiceDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "InvoiceInformation_SourceType", "InvoiceInformation_InvoicePurpose", "InvoiceInformation_Type", "InvoiceInformation_Status"
        };

        public PartsPurchaseRtnSettleBillForInvoiceDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += PartsPurchaseRtnSettleBillForInvoiceDataGridView_DataContextChanged;
        }

        void PartsPurchaseRtnSettleBillForInvoiceDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsPurchaseRtnSettleBill = e.NewValue as VirtualPartsPurchaseRtnSettleBillWithBusinessCode;
            if(partsPurchaseRtnSettleBill == null || partsPurchaseRtnSettleBill.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "SourceId",
                MemberType = typeof(int),
                Operator = Sunlight.Silverlight.Core.Model.FilterOperator.IsEqualTo,
                Value = partsPurchaseRtnSettleBill.Id
            });
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "SourceType",
                MemberType = typeof(int),
                Operator = Sunlight.Silverlight.Core.Model.FilterOperator.IsEqualTo,
                Value = (int)DcsInvoiceInformationSourceType.配件采购退货结算单
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[kvNames[3]]
                    }, new ColumnItem {
                        Name = "InvoiceCode"
                    }, new ColumnItem {
                        Name = "InvoiceNumber"
                    }, new ColumnItem {
                        Name = "InvoiceCompanyCode"
                    }, new ColumnItem {
                        Name = "InvoiceCompanyName"
                    }, new ColumnItem {
                        Name = "InvoiceReceiveCompanyCode"
                    }, new ColumnItem {
                        Name = "InvoiceReceiveCompanyName"
                    }, new ColumnItem {
                        Name = "InvoiceAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "TaxRate",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "InvoiceTax",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "InvoiceDate",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new KeyValuesColumnItem {
                        Name = "SourceType",
                        KeyValueItems = this.KeyValueManager[kvNames[0]]
                    }, new ColumnItem {
                        Name = "SourceCode"
                    }, new KeyValuesColumnItem {
                        Name = "InvoicePurpose",
                        KeyValueItems = this.KeyValueManager[kvNames[1]]
                    }, new KeyValuesColumnItem {
                        Name = "Type",
                        KeyValueItems = this.KeyValueManager[kvNames[2]]
                    },  new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetInvoiceInformations";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            ((GridViewDataColumn)this.GridView.Columns["InvoiceAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["InvoiceTax"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["TaxRate"]).DataFormatString = "n2";
            ((GridViewDataColumn)this.GridView.Columns["InvoiceDate"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
        }

        protected override Type EntityType {
            get {
                return typeof(InvoiceInformation);
            }
        }
    }
}
