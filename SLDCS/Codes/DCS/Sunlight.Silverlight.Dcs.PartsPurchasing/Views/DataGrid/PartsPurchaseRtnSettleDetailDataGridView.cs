﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurchaseRtnSettleDetailDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SettlementPrice",
                        IsReadOnly = true,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "QuantityToSettle",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SettlementAmount",
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Remark",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurchaseRtnSettleDetail);
            }
        }

        //protected override Binding OnRequestDataSourceBinding() {
        //    return new Binding("PartsPurchaseRtnSettleDetails");
        //}

        protected override string OnRequestQueryName() {
            return "GetPartsPurchaseRtnSettleDetails";
        }

        private void PartsPurchaseRtnSettleDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsPurchaseRtnSettleBill = e.NewValue as VirtualPartsPurchaseRtnSettleBillWithBusinessCode;
            if(partsPurchaseRtnSettleBill == null || partsPurchaseRtnSettleBill.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsPurchaseRtnSettleBillId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsPurchaseRtnSettleBill.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        public PartsPurchaseRtnSettleDetailDataGridView() {
            this.DataContextChanged += PartsPurchaseRtnSettleDetailDataGridView_DataContextChanged;
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
        }
    }
}
