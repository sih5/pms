﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurchaseRtnSettleDetailForEditDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SettlementPrice",
                        TextAlignment = TextAlignment.Right,
                        //MaskType = Sunlight.Silverlight.Core.Model.MaskType.Numeric,
                    }, new ColumnItem {
                        Name = "QuantityToSettle",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SettlementAmount",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Remark",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurchaseRtnSettleDetail);
            }
        }

        protected virtual void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            if(!e.Cell.Column.UniqueName.Equals("SettlementPrice"))
                return;
            var partsPurchaseRtnSettleBill = this.DataContext as PartsPurchaseRtnSettleBill;
            if(partsPurchaseRtnSettleBill == null)
                return;
            var detail = e.Cell.DataContext as PartsPurchaseRtnSettleDetail;
            if(detail == null)
                return;
            detail.SettlementAmount = detail.QuantityToSettle * detail.SettlementPrice;
            partsPurchaseRtnSettleBill.TotalSettlementAmount = partsPurchaseRtnSettleBill.PartsPurchaseRtnSettleDetails.Sum(r => r.SettlementPrice * r.QuantityToSettle);
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.CanUserInsertRows = false;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            this.GridView.RowStyleSelector = this.GridViewStyleSelector();
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        private GridViewStyleSelector GridViewStyleSelector() {
            return new GridViewStyleSelector {
                ExecuteFunc = (item, container) => {
                    if(item is PartsPurchaseRtnSettleDetail) {
                        var partsPurchaseRtnSettleDetail = item as PartsPurchaseRtnSettleDetail;
                        if(partsPurchaseRtnSettleDetail.SettlementPrice == 0) {
                            UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_SettlementPriceIsZero);
                            return this.Resources["ZeroDataBackground"] as Style;
                        }
                    }
                    return null;
                }
            };
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsPurchaseRtnSettleDetails");
        }
    }
}
