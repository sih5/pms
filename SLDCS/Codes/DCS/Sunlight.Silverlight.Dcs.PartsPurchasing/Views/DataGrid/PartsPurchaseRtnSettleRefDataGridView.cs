﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurchaseRtnSettleRefDataGridView : DcsDataGridViewBase {
        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber"
                    }, new ColumnItem {
                        Name = "SourceCode",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseRtnSettleRef_InvoiceAmount
                    }, new ColumnItem {
                        Name = "SettlementAmount",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseRtnSettleRef_SettlementAmount
                    }, new ColumnItem {
                        Name = "WarehouseName",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_WarehouseName
                    }, new ColumnItem {
                        Name = "SourceBillCreateTime",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseRtnSettleRef_SourceBillCreateTime
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurchaseRtnSettleRef);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        //protected override Binding OnRequestDataSourceBinding() {
        //    return new Binding("PartsPurchaseRtnSettleRefs");
        //}

        protected override string OnRequestQueryName() {
            return "GetPartsPurchaseRtnSettleRefs";
        }

        private void PartsPurchaseRtnSettleRefDataGridView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var partsPurchaseRtnSettleBill = e.NewValue as VirtualPartsPurchaseRtnSettleBillWithBusinessCode;
            if(partsPurchaseRtnSettleBill == null || partsPurchaseRtnSettleBill.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsPurchaseRtnSettleBillId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsPurchaseRtnSettleBill.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        public PartsPurchaseRtnSettleRefDataGridView() {
            this.DataContextChanged += PartsPurchaseRtnSettleRefDataGridView_DataContextChanged;
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
        }
    }
}
