﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurchaseRtnSettleRefForEditDataGridView : DcsDataGridViewBase {
        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "BillCode",
                        IsReadOnly = true,
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_BillCode,
                    }, new ColumnItem {
                        Name = "SettlementAmount",
                        IsReadOnly = true,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_SettlementAmount,
                    }, new ColumnItem {
                        Name = "WarehouseName",
                        IsReadOnly = true,
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_WarehouseName
                    }, new ColumnItem {
                        Name = "BillCreateTime",
                        IsReadOnly = true,
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_BillCreateTime,
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(OutboundAndInboundBill);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.CanUserInsertRows = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            base.OnControlsCreated();
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("OutboundAndInboundBills");
        }
    }
}
