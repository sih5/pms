﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurchaseSettleBillDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsPurchaseSettleBill_SettlementPath","PartsPurchaseSettle_Status"
        };

        public PartsPurchaseSettleBillDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPartsPurchaseSettleBillWithSumPlannedPrice);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Status
                    }, new ColumnItem {
                        Name = "InvoiceDate",
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseRtnSettleBill_InvoiceDate
                    },new ColumnItem {
                        Name = "Code",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseSettleBill_Code
                    },new ColumnItem {
                        Name = "WarehouseName",
                        Title=PartsPurchasingUIStrings.DataEditView_ImportTemplate_WarehouseName
                    }, new ColumnItem {
                        Name = "PartsSupplierCode",
                        Title=PartsPurchasingUIStrings.DataEditView_Text_InvoiceInformation_SupplierCode
                    },new ColumnItem {
                        Name = "BusinessCode",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_SupplierBusinessCode
                    }, new ColumnItem {
                        Name = "PartsSupplierName",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_PartsSupplierName
                    }, //new ColumnItem {
                        //Name = "TotalSettlementAmount",
                        //Title="结算总金额"
                    //}, 
                    new AggregateColumnItem {
                        Name = "TotalSettlementAmount",
                        AggregateType = AggregateType.Sum ,
                        Title=PartsPurchasingUIStrings.DataEditView_Text_InvoiceInformation_TotalSettlementAmount
                    },
                    new ColumnItem {
                        Name = "TaxRate",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_TaxRate
                    },
                    //new ColumnItem {
                    //    Name = "Tax",
                    //    Title="税额"
                    //},
                    new ColumnItem {
                        Name = "SumPlannedPrice",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_SumPlannedPrice
                    }, 
                    new ColumnItem {
                        Name = "InvoiceTotalAmount",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_InvoiceTotalAmount
                    },new ColumnItem {
                        Name = "InvoiceAmountDifference",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_InvoiceAmountDifference
                    }, new ColumnItem {
                        Name = "CostAmountDifference",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_CostAmountDifference
                    },  new KeyValuesColumnItem {
                        Name = "SettlementPath",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_SettlementPath
                    }, new ColumnItem {
                        Name = "OffsettedSettlementBillCode",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_OffsettedSettlementBillCode
                    }, new ColumnItem {
                        Name = "Remark",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Remark
                    }, new ColumnItem {
                        Name = "CreatorName",
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_CreatorName
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_CreateTime
                    } , new ColumnItem {
                        Name = "ModifierName",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Modifier
                    }, new ColumnItem {
                        Name = "ModifyTime",
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_ModifyTime
                    } , new ColumnItem {
                        Name = "ApproverName",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Approver
                    }, new ColumnItem {
                        Name = "ApproveTime",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_ApproveTime
                    } , new ColumnItem {
                        Name = "InvoiceApproverName",
                        Title=PartsPurchasingUIStrings.DetailPanel_Title_InvoiceInformation_ApproverName
                    }, new ColumnItem {
                        Name = "InvoiceApproveTime",
                        Title=PartsPurchasingUIStrings.DetailPanel_Title_InvoiceInformation_ApproveTime
                    } , new ColumnItem {
                        Name = "AbandonerName",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_AbandonerName
                    }, new ColumnItem {
                        Name = "AbandonTime",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_AbandonTime
                    }   , new ColumnItem {
                        Name = "BranchName",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsBranch_BranchName
                    }, new ColumnItem {
                        Name = "PartsSalesCategoryName",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseSettleBill_CategoryName
                    }                  
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsPurchaseSettleBillWithPartsSalesCategoryByUnifiedSettle";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.DataPager.PageSize = 200;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsPurchaseSettleBill","PartsPurchaseSettleDetail","PartsPurchaseSettleRef"
                    }
                };
            }
        }
    }
}
