﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurchaseSettleBillForSupplierDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsPurchaseSettle_Status", "PartsPurchaseSettleBill_SettlementPath"
        };

        public PartsPurchaseSettleBillForSupplierDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Code",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseSettleBill_Code
                    }, new ColumnItem {
                        Name = "WarehouseName",
                    }, new ColumnItem {
                        Name = "PartsSupplierCode"
                    }, new ColumnItem {
                        Name = "PartsSupplierName"
                    }, new ColumnItem {
                        Name = "TotalSettlementAmount"
                    }, new ColumnItem {
                        Name = "TaxRate"
                    }, new ColumnItem {
                        Name = "Tax"
                    }, new ColumnItem {
                        Name = "InvoiceAmountDifference"
                    }, new ColumnItem {
                        Name = "CostAmountDifference"
                    }, new KeyValuesColumnItem {
                        Name = "SettlementPath",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Name = "OffsettedSettlementBillCode"
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    } , new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    } , new ColumnItem {
                        Name = "ApproverName"
                    }, new ColumnItem {
                        Name = "ApproveTime"
                    } , new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime"
                    }, new ColumnItem {
                        Name = "BranchName"
                    },new ColumnItem{
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseSettleBill_PartsSalesCategoryName,
                        Name="PartsSalesCategoryName"
                    }                
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurchaseSettleBill);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsPurchaseSettleBills";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsPurchaseSettleBillForSupplier","PartsPurchaseSettleDetailForSupplier","PartsPurchaseSettleRefForSupplier"
                    }
                };
            }
        }
    }
}
