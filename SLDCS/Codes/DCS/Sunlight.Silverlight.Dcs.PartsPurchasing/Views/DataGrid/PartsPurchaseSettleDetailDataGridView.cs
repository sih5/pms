﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurchaseSettleDetailDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber"
                    },new ColumnItem {
                        Name = "SparePartCode"
                    }, new ColumnItem {
                        Name = "SparePartName"
                    }, new ColumnItem {
                        Name = "SettlementPrice"
                    }, new ColumnItem {
                        Name = "QuantityToSettle"
                    }, new ColumnItem {
                        Name = "SettlementAmount"
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurchaseSettleDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsPurchaseSettleDetails";
        }

        public PartsPurchaseSettleDetailDataGridView() {
            this.DataContextChanged += this.PartsPurchaseSettleDetailDataGridView_DataContextChanged;
        }

        private void PartsPurchaseSettleDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsPurchaseSettleBill = e.NewValue as VirtualPartsPurchaseSettleBillWithSumPlannedPrice;
            if(partsPurchaseSettleBill == null || partsPurchaseSettleBill.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsPurchaseSettleBillId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsPurchaseSettleBill.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }


        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
        }
    }
}
