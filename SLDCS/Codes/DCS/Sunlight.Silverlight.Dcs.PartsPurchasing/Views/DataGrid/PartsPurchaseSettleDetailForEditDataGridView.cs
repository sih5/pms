﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurchaseSettleDetailForEditDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SettlementPrice",
                        IsReadOnly = true,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "QuantityToSettle",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SettlementAmount",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Remark",
                        IsReadOnly = true
                    }
                };
            }
        }

        private GridViewStyleSelector GridViewStyleSelector() {
            return new GridViewStyleSelector {
                ExecuteFunc = (item, container) => {
                    if(item is PartsPurchaseSettleDetail) {
                        var partsPurchaseSettleDetail = item as PartsPurchaseSettleDetail;
                        if(partsPurchaseSettleDetail.SettlementPrice == 0) {
                            return this.Resources["ZeroDataBackground"] as Style;
                        }
                    }
                    return null;
                }
            };
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsPurchaseSettleDetail);
            }
        }

        protected virtual void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            if(!e.Cell.Column.UniqueName.Equals("SettlementPrice"))
                return;
            var partsPurchaseSettleBill = this.DataContext as PartsPurchaseSettleBill;
            if(partsPurchaseSettleBill == null)
                return;
            var detail = e.Cell.DataContext as PartsPurchaseSettleDetail;
            if(detail == null)
                return;
            detail.SettlementAmount = detail.QuantityToSettle * detail.SettlementPrice;
            partsPurchaseSettleBill.TotalSettlementAmount = partsPurchaseSettleBill.PartsPurchaseSettleDetails.Sum(r => r.SettlementPrice * r.QuantityToSettle);
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DataPager.PageSize = 50;
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.CanUserDeleteRows = false;
            this.GridView.CanUserInsertRows = false;
            this.GridView.RowStyleSelector = this.GridViewStyleSelector();
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsPurchaseSettleDetails");
        }
    }
}
