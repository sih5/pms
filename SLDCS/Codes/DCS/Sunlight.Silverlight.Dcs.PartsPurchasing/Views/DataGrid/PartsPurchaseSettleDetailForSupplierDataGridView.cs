﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurchaseSettleDetailForSupplierDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        Title= PartsPurchasingUIStrings.DataGridView_ColumnItem_Number
                    }, new ColumnItem {
                        Name = "SupplierPartCode",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_SupplierPartCode
                    },new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_SparePartName
                    }, new ColumnItem {
                        Name = "SettlementPrice",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SettlementPrice
                    }, new ColumnItem {
                        Name = "QuantityToSettle",
                        Title =PartsPurchasingUIStrings.DataGridView_ColumnItem_QuantityToSettle
                    }, new ColumnItem {
                        Name = "SettlementAmount",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_SettlementAmount
                    }, new ColumnItem {
                        Name = "Remark",
                        Title = PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_Remark
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPartsPurchaseSettleDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetDetailsWithSupplierPartCode";
        }

        public PartsPurchaseSettleDetailForSupplierDataGridView() {
            this.DataContextChanged += this.PartsPurchaseSettleDetailDataGridView_DataContextChanged;
        }

        private void PartsPurchaseSettleDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsPurchaseSettleBill = e.NewValue as PartsPurchaseSettleBill;
            if(partsPurchaseSettleBill == null || partsPurchaseSettleBill.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsPurchaseSettleBillId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsPurchaseSettleBill.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }


        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
        }
    }
}
