﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurchaseSettleInvoiceDetailDataGridView : DcsDataGridViewBase {

        public PartsPurchaseSettleInvoiceDetailDataGridView() {
            this.DataContextChanged += PartsPurchaseSettleInvoiceDetailDataGridView_DataContextChanged;
        }

        private int type;
        private int invoiceId;
        private void PartsPurchaseSettleInvoiceDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var invoiceInformation = this.DataContext as VirtualInvoiceInformation;
            if(invoiceInformation == null)
                return;
            invoiceId = invoiceInformation.Id;
            type = invoiceInformation.SourceType;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem  {
                        Name = "SerialNumber"
                    }, new ColumnItem {
                        Name = "PartsPurchaseSettleCode",
                        Title =PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PurchaseSettleInvoiceResource_PartsPurchaseSettleCode
                    }, new ColumnItem {
                        Name = "Type",
                          Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PurchaseSettleInvoiceResource_Type
                    }
                };
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            switch(parameterName) {
                case "invoiceId":
                    return invoiceId;
                case "type":
                    return type == 3 ? "配件采购结算单" : "配件采购退货结算单";
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override Type EntityType {
            get {
                return typeof(PurchaseSettleInvoiceResource);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询采购结算发票源单据";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.DataLoaded += GridView_DataLoaded;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }

        private void GridView_DataLoaded(object sender, EventArgs e) {
            var serialNumber = 1;
            foreach(var detail in this.GridView.Items.Cast<PurchaseSettleInvoiceResource>())
                detail.SerialNumber = serialNumber++;
        }
    }
}
