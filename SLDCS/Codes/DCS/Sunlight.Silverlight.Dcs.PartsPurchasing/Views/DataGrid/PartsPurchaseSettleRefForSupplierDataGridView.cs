﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsPurchaseSettleRefForSupplierDataGridView : DcsDataGridViewBase {

        private readonly string[] kvNames = {
            "PartsPurchaseSettleRef_SourceType"
        };

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Number
                    },new ColumnItem {
                        Name = "SourceCode",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseSettleRef_SourceCode
                    }, new ColumnItem {
                        Name = "OriginalRequirementBillCode",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OriginalRequirementBillCode
                    }, new KeyValuesColumnItem {
                        Name = "SourceType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseSettleRef_SourceType
                    }, new ColumnItem {
                        Name = "WarehouseName",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_WarehouseName
                    }, new ColumnItem {
                        Name = "SourceBillCreateTime",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseSettleRef_SourceBillCreateTime
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPartsPurchaseSettleRef);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsPurchaseSettleRefWithOriginalRequirementBillCode";
        }

        public PartsPurchaseSettleRefForSupplierDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += this.PartsPurchaseSettleRefDataGridView_DataContextChanged;
        }

        private void PartsPurchaseSettleRefDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsPurchaseSettleBill = e.NewValue as PartsPurchaseSettleBill;
            if(partsPurchaseSettleBill == null || partsPurchaseSettleBill.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsPurchaseSettleBillId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsPurchaseSettleBill.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }


        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
        }
    }
}
