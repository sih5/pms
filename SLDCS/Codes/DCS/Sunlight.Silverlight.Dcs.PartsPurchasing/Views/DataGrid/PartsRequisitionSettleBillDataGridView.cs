﻿
using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsRequisitionSettleBillDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = { 
            "PartsRequisitionSettleBill_Status" ,"RequisitionSettleType","InternalAllocationBill_Type"
        };

        public PartsRequisitionSettleBillDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                   },new ColumnItem {
                        Name = "InvoiceDate",
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseRtnSettleBill_InvoiceDate
                   }, new ColumnItem {
                        Name = "Code",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsRequisitionSettleBill_Code
                   },new ColumnItem {
                        Name = "PartsSalesCategoryName",
                        Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsRequisitionSettleBill_PartsSalesCategoryName
                   },new ColumnItem {
                        Name = "DepartmentName",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsRequisitionSettleBill_Department
                   },new KeyValuesColumnItem{
                       Name="SettleType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                   },new KeyValuesColumnItem{
                       Name="Type",
                       Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Type,
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                   },//new ColumnItem {
                        //Name = "TotalSettlementAmount",
                   //},
                   new AggregateColumnItem {
                        Name = "TotalSettlementAmount",
                        AggregateType = AggregateType.Sum 
                   },
                   new ColumnItem {
                        Name = "TotalTaxAmount",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Tax
                   }
                   //,new ColumnItem {
                   //     Name = "PlannedPriceTotalAmount",
                   //     Title="计划价合计"
                   //}
                   ,new ColumnItem {
                        Name = "Remark"
                   },new ColumnItem {
                        Name = "CreatorName"
                   },new ColumnItem {
                        Name = "CreateTime"
                   },new ColumnItem {
                        Name = "ModifierName"
                   },new ColumnItem {
                        Name = "ModifyTime"
                   },new ColumnItem {
                        Name = "ApproverName"
                   },new ColumnItem {
                        Name = "ApproveTime"
                   },new ColumnItem {
                        Name = "AbandonerName"
                   },new ColumnItem {
                        Name = "AbandonTime"
                   },new ColumnItem {
                        Name = "BranchName",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsRequisitionSettleBill_Branch
                   }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "查询配件领用结算单计划价合计";
        }


        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsRequisitionSettleDetail","PartsRequisitionSettleRef"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            ((GridViewDataColumn)this.GridView.Columns["TotalSettlementAmount"]).DataFormatString = "c2";
           // ((GridViewDataColumn)this.GridView.Columns["PlannedPriceTotalAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["TotalTaxAmount"]).DataFormatString = "c2";
        }

        protected override Type EntityType {
            get {
                return typeof(PartsRequisitionSettleBillExport);
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            switch(parameterName) {
                case "settleType":
                    return (int)DcsRequisitionSettleType.领入;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
    }
}
