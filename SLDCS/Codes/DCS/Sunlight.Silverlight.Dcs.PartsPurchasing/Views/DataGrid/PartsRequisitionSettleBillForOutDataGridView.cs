﻿
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsRequisitionSettleBillForOutDataGridView : PartsRequisitionSettleBillDataGridView {
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            switch(parameterName) {
                case "settleType":
                    return (int)DcsRequisitionSettleType.领出;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
    }
}
