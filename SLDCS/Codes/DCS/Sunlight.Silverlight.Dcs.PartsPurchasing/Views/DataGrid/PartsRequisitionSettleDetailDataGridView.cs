﻿
using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsRequisitionSettleDetailDataGridView : DcsDataGridViewBase {
        public PartsRequisitionSettleDetailDataGridView() {
            this.DataContextChanged += this.PartsRequisitionSettleDetailDataGirdView_DataContextChanged;
        }
        private void PartsRequisitionSettleDetailDataGirdView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsRequisitionSettleBill = e.NewValue as PartsRequisitionSettleBillExport;
            if(partsRequisitionSettleBill == null || partsRequisitionSettleBill.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsRequisitionSettleBillId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsRequisitionSettleBill.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber"
                    },new ColumnItem {
                        Name = "SparePartCode"
                    },new ColumnItem {
                        Name = "SparePartName"
                    },new  ColumnItem {
                        Name = "SettlementPrice"
                    },new  ColumnItem {
                        Name = "Quantity"
                    }
                };
            }
        }
        protected override string OnRequestQueryName() {
            return "GetPartsRequisitionSettleDetails";
        }
        protected override Type EntityType {
            get {
                return typeof(PartsRequisitionSettleDetail);
            }
        }
    }
}
