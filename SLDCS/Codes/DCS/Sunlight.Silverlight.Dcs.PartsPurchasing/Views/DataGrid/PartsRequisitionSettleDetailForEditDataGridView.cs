﻿
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsRequisitionSettleDetailForEditDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "SettlementPrice",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "Quantity",
                        IsReadOnly = true
                    },new ColumnItem{
                        Name="SettlementAmount",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_SettlementAmount,
                        IsReadOnly=true
                    }
                };
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionMode = SelectionMode.Single;
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }
        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsRequisitionSettleDetails");
        }
        protected override System.Type EntityType {
            get {
                return typeof(PartsRequisitionSettleDetail);
            }
        }
    }
}
