﻿
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsRequisitionSettleRefDetailDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsRequisitionSettleRef_SourceType"
        };
        public PartsRequisitionSettleRefDetailDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += this.PartsRequisitionSettleRefDetailDataGirdView_DataContextChanged;

        }

        private void PartsRequisitionSettleRefDetailDataGirdView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsRequisitionSettleBill = e.NewValue as PartsRequisitionSettleBillExport;
            if(partsRequisitionSettleBill == null || partsRequisitionSettleBill.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsRequisitionSettleBillId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsRequisitionSettleBill.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SourceCode"
                    }, new KeyValuesColumnItem {
                        Name = "SourceType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                    }
                };
            }
        }
        protected override string OnRequestQueryName() {
            return "GetPartsRequisitionSettleRefs";
        }
        protected override System.Type EntityType {
            get {
                return typeof(PartsRequisitionSettleRef);
            }
        }
    }
}
