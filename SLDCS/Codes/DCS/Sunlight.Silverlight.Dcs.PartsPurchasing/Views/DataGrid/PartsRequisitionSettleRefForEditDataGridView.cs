﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsRequisitionSettleRefForEditDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsRequisitionSettleRef_SourceType"
        };

        public PartsRequisitionSettleRefForEditDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SourceCode",
                        IsReadOnly = true
                    },new KeyValuesColumnItem {
                        Name = "SourceType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "SettlementAmount",
                        IsReadOnly = true
                    }
                };
            }
        }
        private void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var partsRequisitionSettleBill = this.DataContext as PartsRequisitionSettleBill;
            if(partsRequisitionSettleBill == null)
                return;
            partsRequisitionSettleBill.TotalSettlementAmount = partsRequisitionSettleBill.PartsRequisitionSettleRefs.Sum(item => item.SettlementAmount);
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.CanUserInsertRows = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Extended;
            this.GridView.Deleted -= GridView_Deleted;
            this.GridView.Deleted += GridView_Deleted;
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }
        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsRequisitionSettleRefs");
        }
        protected override Type EntityType {
            get {
                return typeof(PartsRequisitionSettleRef);
            }
        }
    }
}
