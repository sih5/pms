﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsSupplierByPartIdsDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new ColumnItem {
                        Name = "Code",
                        Title="供应商编号"
                    }, new ColumnItem {
                        Name = "Name",
                        Title="供应商名称"
                    }
                };
            }
        }

        public PartsSupplierByPartIdsDataGridView() {
        }
        protected override Type EntityType {
            get {
                return typeof(PartsSupplier);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsSuppliers";
        }       

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.ShowGroupPanel = false;
        }
    }
}