﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PartsSupplierRelationForIntelligentOrderDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = { 
            "BaseData_Status"
        };

        public PartsSupplierRelationForIntelligentOrderDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                       new KeyValuesColumnItem{
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem{
                        Name = "SparePart.Code"
                    }, new ColumnItem{
                        Name = "PartsSupplier.Code"
                    }, new ColumnItem{
                        Name = "PartsSupplier.Name"
                    }, new ColumnItem{
                        Name = "CreatorName"
                    }, new ColumnItem{
                        Name = "CreateTime"
                    },new ColumnItem{
                        Name = "PartsSalesCategory.Name"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSupplierRelation);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询配件实际下达供应商编码";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filterItem = this.FilterItem as CompositeFilterItem;
            if(filterItem != null) {
                var compositeFilterItem = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                if(compositeFilterItem != null) {
                    switch(parameterName) {
                        case "partsSalesCategoryId":
                            var partsSalesCategoryId = compositeFilterItem.Filters.SingleOrDefault(e => e.MemberName == "PartsSalesCategoryId");
                            return partsSalesCategoryId == null ? null : partsSalesCategoryId.Value;
                        case "partId":
                            var partId = compositeFilterItem.Filters.SingleOrDefault(e => e.MemberName == "PartId");
                            return partId == null ? null : partId.Value;

                    }
                }
            }
            return null;
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;

        }
    }
}
