﻿using System;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class PrintLabelForSupplierShippingOrderDataGridView : DcsDataGridViewBase {
        protected override System.Collections.Generic.IEnumerable<Core.Model.ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SupplierShippingOrder.Code",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_SupplierShippingOrder_Code,
                        IsReadOnly = true
                    }, new ColumnItem {
                         Name = "SparePartCode",
                        Title = PartsPurchasingUIStrings.DataGridView_Title_PartNotConPurchasePlan_PartCode,
                        IsReadOnly = true
                    }, new ColumnItem {
                         Name = "SparePartName",
                        Title = PartsPurchasingUIStrings.DataGridView_Title_PartNotConPurchasePlan_PartName,
                        IsReadOnly = true
                    }, new ColumnItem {
                         Name = "Quantity",
                        Title = PartsPurchasingUIStrings.DataGridView_Column_Amount,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "PrintNumber",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_PrintNumber,
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SupplierShippingDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("SupplierShippingDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
    }
}
