﻿using System;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;
using Sunlight.Silverlight.Dcs.Web.Entities;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid
{
    public class PrintLabelForSupplierShippingOrderForQuery : DcsDataGridViewBase
    {
        protected override System.Collections.Generic.IEnumerable<Core.Model.ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                   new ColumnItem {
                         Name = "Code",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_SupplierShippingOrder_Code,
                        IsReadOnly = true
                    },new ColumnItem {
                         Name = "SparePartCode",
                        Title =PartsPurchasingUIStrings.DataGridView_Title_PartNotConPurchasePlan_PartCode,
                        IsReadOnly = true
                    }, new ColumnItem {
                         Name = "SparePartName",
                        Title = PartsPurchasingUIStrings.DataGridView_Title_PartNotConPurchasePlan_PartName,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Quantity",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_Quantity,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "FirPrintNumber",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_FirPrintNumber,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SecPrintNumber",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_SecPrintNumber,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ThidPrintNumber",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_ThirdPrintNumber,
                        IsReadOnly = true
                    } , new ColumnItem {
                        Name = "BachMunber",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_BachMunber,
                        IsReadOnly = true
                    } 
                };
            }
        }

        protected override Type EntityType
        {
            get
            {
                return typeof(VirtualSupplierShippingDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding()
        {
            return new Binding("PartsInboundPlanDetails");
        }


        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DomainDataSource.PageSize = 100;
        }

        protected override bool ShowCheckBox
        {
            get
            {
                return true;
            }
        }
    }

}


