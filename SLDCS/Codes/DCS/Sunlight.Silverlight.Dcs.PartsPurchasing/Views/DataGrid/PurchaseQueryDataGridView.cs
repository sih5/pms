﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PurchaseQueryDataGridView : DcsDataGridViewBase {
        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = true;
            base.OnControlsCreated();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {                  
                    new ColumnItem {
                        Name = "PartCode",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_SparePartCode
                    },
                    new ColumnItem {
                        Name = "PartName",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_SparePartName
                    },
                    new ColumnItem {
                        Name = "stock",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Stock
                    },
                    new ColumnItem {
                        Name = "Usablequantity",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_UsableStock
                    },
                    new ColumnItem {
                        Name = "OnwayQty",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_OnwayQty
                    },
                    new ColumnItem {
                        Name = "WMZQty",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_WMZQty
                    },
                    new ColumnItem {
                        Name = "OutQty",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_OutQty
                    },
                    new ColumnItem {
                        Name = "PartsAttribution",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_PartsAttribution
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPurchaseQuery);
            }
        }
        protected override string OnRequestQueryName() {
            return "查询配件采购需求";
        }

    }
}
