﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class SparePartForPartsPurchasePlanDataGridView : DcsDataGridViewBase {
        protected override string OnRequestQueryName() {
            return "查询配件合同价采购计划";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                foreach(var composite in filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                    var compositeInternal = composite as CompositeFilterItem;
                    if(compositeInternal == null)
                        continue;
                    switch(parameterName) {
                        case "branchId":
                            return compositeInternal.Filters.Single(item => item.MemberName == "BranchId").Value;
                        //case "partsSupplierId":
                        //    return compositeInternal.Filters.Single(item => item.MemberName == "PartsSupplierId").Value;
                        //case "time":
                        //    return compositeInternal.Filters.Single(item => item.MemberName == "Time").Value;
                        case "sparePartIds":
                            return compositeInternal.Filters.Single(item => item.MemberName == "SparePartIds").Value;
                        case "partsSaleCategoryId":
                            return compositeInternal.Filters.Single(item => item.MemberName == "PartsSaleCategoryId").Value;
                    }
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return base.OnRequestFilterDescriptor(queryName);
            var compositeCondition = new[] {
                "PartsSalesCategoryrId"
            };
            var newCompositeFilterItem = new CompositeFilterItem();
            foreach(var filter in compositeFilterItem.Filters) {
                var filterItem = filter as CompositeFilterItem;
                if(filterItem != null)
                    continue;
                if(!(filter.MemberName == "PartsBranchName" || filter.MemberName == "PartsSalesCategoryName" /*|| filter.MemberName == "PartsSupplierCode" || filter.MemberName == "PartsSupplierName"*/))
                    newCompositeFilterItem.Filters.Add(filter);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualSparePart);
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        /// <summary>
        /// 清除当前选中项
        /// </summary>
        public void ClearSelected() {
            if(this.GridView != null && this.GridView.SelectedItems != null)
                this.GridView.SelectedItems.Clear();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_SparePartCode
                    }, new ColumnItem {
                        Name = "Name",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_SparePartName
                    }, new ColumnItem {
                        Name = "ReferenceCode",
                        Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsBranch_ReferenceCode
                    }, new ColumnItem {
                        Name = "Feature",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Feature
                    },                   
                    new ColumnItem {
                        Name = "MeasureUnit",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_MeasureUnit
                    },new ColumnItem{
                        Name="Specification",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_Specification
                    }
                    //,new ColumnItem{
                    //    Name="PurchasePrice",
                    //    Title="采购价"
                    //}, new ColumnItem {
                    //    Name = "PartsSupplierCode",
                    //    Title="供应商编号"
                    //},new ColumnItem {
                    //    Name = "PartsSupplierName",
                    //    Title="供应商名称"
                    //}
                    ,new ColumnItem {
                        Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsBranch_BranchName,
                        Name = "PartsBranchName"
                    },new ColumnItem {
                        Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsBranch_PartsSalesCategoryName,
                        Name = "PartsSalesCategoryName"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            //((GridViewDataColumn)this.GridView.Columns["PurchasePrice"]).DataFormatString = "c2";
        }
    }
}
