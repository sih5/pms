﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using SelectionMode = System.Windows.Controls.SelectionMode;
namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class SparePartTepPlanDataGridView : DcsDataGridViewBase {
        protected override string OnRequestQueryName() {
            return "GetSpareParts";
        }      

        protected override Type EntityType {
            get {
                return typeof(SparePart);
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }


        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_SparePartCode
                    }, new ColumnItem {
                        Name = "Name",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_SparePartName
                    }, new ColumnItem {
                        Name = "ReferenceCode",
                        Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsBranch_ReferenceCode
                    },                   
                    new ColumnItem {
                        Name = "MeasureUnit",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_MeasureUnit
                    }                   
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Multiple;
        }
    }
}
