﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class SparePartsDataForOuterPurchaseDataGridView : DcsDataGridViewBase {
        protected override string OnRequestQueryName() {
            return "查询配件带批发价";
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                switch(parameterName) {
                    case "partsSalesCategoryId":
                        var FilterPartsSalesCategory = compositeFilterItem.Filters.SingleOrDefault(item => item.MemberName == "PartsSalesCategoryrId");
                        return FilterPartsSalesCategory == null ? null : FilterPartsSalesCategory.Value;
                }
            }
            return null;
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                var filters = compositeFilterItem.Filters;
                foreach(var item in filters) {
                    var filterItem = item as CompositeFilterItem;
                    if(filterItem != null) {
                        if(filterItem.Filters.Any(filter => filter.MemberName != "PartsSalesCategoryrId"))
                            newCompositeFilterItem.Filters.Add(item);
                    } else {
                        if(item.MemberName != "PartsSalesCategoryrId")
                            newCompositeFilterItem.Filters.Add(item);
                    }
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override Type EntityType {
            get {
                return typeof(SparePartOuterPurchase);

            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        Title = PartsPurchasingUIStrings.DataGridView_Title_PartNotConPurchasePlan_PartCode
                    }, new ColumnItem {
                        Name = "Name",
                        Title = PartsPurchasingUIStrings.DataGridView_Title_PartNotConPurchasePlan_PartName
                    }, new ColumnItem {
                        Name = "PartsSalesPrice",
                        MaskType = Core.Model.MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_SparePart_Title_PartsSalePrice
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["PartsSalesPrice"]).DataFormatString = "c2";

        }
    }
}
