﻿using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class SupplierPartsPurReturnOrderDetailForEditDataGridView : PartsPurReturnOrderDetailForEditDataGridView {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SupplierPartCode",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierPartCode,
                        IsReadOnly = true
                    },
                    //new ColumnItem {
                    //    Name = "SparePartCode",
                    //    IsReadOnly=true
                    //},
                    new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Quantity"
                    }, new ColumnItem {
                        Name = "UnitPrice",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurReturnOrderDetail_UnitPrice,
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "MeasureUnit",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }
    }
}
