﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class SupplierPartsTagPrinttingDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "MasterData_Status", "SparePart_PartType"
        };

        public SupplierPartsTagPrinttingDataGridView() {
            this.KeyValueManager.Register(kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Code",
                        Title = PartsPurchasingUIStrings.DataEditView_ImportTemplate_SparePartCode,
                        IsSortDescending = false
                    }, new ColumnItem {
                        Name = "Name"
                    },  new ColumnItem {
                        Name = "ReferenceCode",
                        Title =PartsPurchasingUIStrings.DataGridView_Column_ReferenceCodeQuery
                    },new ColumnItem {
                        Name = "CADCode",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_CADCode
                    }, new ColumnItem {
                        Name = "Specification"
                    }, new ColumnItem {
                        Name = "EnglishName"
                    }, new KeyValuesColumnItem {
                        Name = "PartType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Name = "MeasureUnit"
                    }, new ColumnItem {
                        Name = "MInPackingAmount"
                    }, new ColumnItem {
                        Name = "ShelfLife",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_ShelfLife
                    }, new ColumnItem {
                        Name = "LastSubstitute"
                    }, new ColumnItem {
                        Name = "NextSubstitute"
                    }, new ColumnItem {
                        Name = "Weight"
                    },new ColumnItem{
                        Name="Volume"
                    }, new ColumnItem {
                        Name = "Feature",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Feature
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime"
                    }
                };
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsSupplierWithRelation", "PartsReplacementForSparePart", "PartsBranchForSparePart", "CombinedPart"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetSparePartsWithPartsSupplierRelation";
        }
        protected override Type EntityType {
            get {
                return typeof(SparePart);
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

    }
}
