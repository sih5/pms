﻿
using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class SupplierPlanArrearDataGridView : DcsDataGridViewBase {
        private string[] kvName = {
             "BaseData_Status"
        };
        public SupplierPlanArrearDataGridView() {
            this.KeyValueManager.Register(this.kvName);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new ColumnItem {
                        Name = "SupplierCode"
                    },new ColumnItem {
                        Name = "SupplierCompanyCode"
                    },new ColumnItem {
                        Name = "SupplierCompanyName"
                    },new ColumnItem {
                        Name = "PlannedAmountOwed"
                    },new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvName[0]]
                    },new ColumnItem {
                        Name = "CreatorName"
                    },new ColumnItem {
                        Name = "CreateTime"
                    },new ColumnItem {
                        Name = "ModifierName"
                    },new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "PartsSalesCategory.Name",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_SupplierPlanArrear_PartsSalesCategoryName
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetSupplierPlanArrearnIncludeSupplierPlanArrear";
        }

        protected override Type EntityType {
            get {
                return typeof(SupplierPlanArrear);
            }
        }
    }
}
