﻿
using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class SupplierPreAppLoanDetailDataGridView : DcsDataGridViewBase {
        public SupplierPreAppLoanDetailDataGridView() {
            this.DataContextChanged -= SupplierPreAppLoanDetailDataGridView_DataContextChanged;
            this.DataContextChanged += SupplierPreAppLoanDetailDataGridView_DataContextChanged;
        }

        private void SupplierPreAppLoanDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var supplierPreApprovedLoan = e.NewValue as SupplierPreApprovedLoan;
            if(supplierPreApprovedLoan == null)
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "SupplierPreApprovedLoanId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = supplierPreApprovedLoan.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new ColumnItem {
                       Name = "SupplierCode"
                   },new ColumnItem {
                       Name = "SupplierCompanyCode"
                   },new ColumnItem {
                       Name = "SupplierCompanyName"
                   },new ColumnItem {
                       Name = "BankName"
                   },new ColumnItem {
                       Name = "BankCode"
                   },new ColumnItem {
                       Name = "ActualDebt",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_SupplierPreAppLoanDetail_ActualDebt
                   },new ColumnItem {
                       Name = "PlannedAmountOwed",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_SupplierPreAppLoanDetail_PlannedAmountOwed
                   },new ColumnItem {
                       Name = "ExceedAmount"
                   },new ColumnItem {
                       Name = "ApprovalAmount"
                   },new ColumnItem {
                       Name = "ActualPaidAmount"
                   },new ColumnItem {
                       Name = "IsOverstock"
                   },new ColumnItem {
                       Name = "Remark"
                   }
               };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            ((GridViewDataColumn)this.GridView.Columns["ActualDebt"]).DataFormatString = "n2";
            ((GridViewDataColumn)this.GridView.Columns["PlannedAmountOwed"]).DataFormatString = "n2";
            ((GridViewDataColumn)this.GridView.Columns["ExceedAmount"]).DataFormatString = "n2";
            ((GridViewDataColumn)this.GridView.Columns["ApprovalAmount"]).DataFormatString = "n2";
            ((GridViewDataColumn)this.GridView.Columns["ActualPaidAmount"]).DataFormatString = "n2";
        }

        protected override string OnRequestQueryName() {
            return "GetSupplierPreAppLoanDetails";
        }


        protected override Type EntityType {
            get {
                return typeof(SupplierPreAppLoanDetail);
            }
        }
    }
}
