﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class SupplierPreAppLoanDetailForEditDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "SupplierPreApprovedLoan.PreApprovedCode",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "SupplierCode",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "SupplierCompanyCode",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "SupplierCompanyName",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "BankName",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name="BankCode",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "ActualDebt",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_SupplierPreAppLoanDetail_ActualDebt,
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "PlannedAmountOwed",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_SupplierPreAppLoanDetail_PlannedAmountOwed,
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "ExceedAmount",
                        IsReadOnly = true
                    },new ColumnItem{
                        Name="ApprovalAmount"
                    },new ColumnItem{
                        Name="ActualPaidAmount"
                    },new ColumnItem {
                        Name = "IsOverstock",
                    },new ColumnItem {
                        Name = "Remark"
                    },
                };
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.IsReadOnly = false;
            ((GridViewDataColumn)this.GridView.Columns["ActualDebt"]).DataFormatString = "n2";
            ((GridViewDataColumn)this.GridView.Columns["PlannedAmountOwed"]).DataFormatString = "n2";
            ((GridViewDataColumn)this.GridView.Columns["ExceedAmount"]).DataFormatString = "n2";
            ((GridViewDataColumn)this.GridView.Columns["ApprovalAmount"]).DataFormatString = "n2";
            ((GridViewDataColumn)this.GridView.Columns["ActualPaidAmount"]).DataFormatString = "n2";
            //this.GridView.GroupRenderMode = Telerik.Windows.Controls.GridView.GroupRenderMode.Flat;
            this.GridView.SortDescriptors.Add(new ColumnSortDescriptor {
                Column = this.GridView.Columns["SupplierCode"]
            });
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            this.DataPager.PageSize = 300;
            this.GridView.BeginningEdit += GridView_BeginningEdit;
        }

        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            var supplierPreApprovedLoan = this.DataContext as SupplierPreApprovedLoan;
            if(supplierPreApprovedLoan == null)
                return;
            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "ApprovalAmount":
                case "ActualPaidAmount":
                    if(supplierPreApprovedLoan.EntityState == EntityState.New)
                        e.Cancel = true;
                    break;
            }
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var supplierPreApprovedLoan = this.DataContext as SupplierPreApprovedLoan;
            if(supplierPreApprovedLoan == null)
                return;
            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "ApprovalAmount":
                    supplierPreApprovedLoan.ApprovalAmount = supplierPreApprovedLoan.SupplierPreAppLoanDetails.Sum(r => r.ApprovalAmount ?? 0);
                    break;
                case "ActualPaidAmount":
                    supplierPreApprovedLoan.ActualPaidAmount = supplierPreApprovedLoan.SupplierPreAppLoanDetails.Sum(r => r.ActualPaidAmount ?? 0);
                    break;
            }
        }
        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("SupplierPreAppLoanDetails");
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SupplierPreAppLoanDetail);
            }
        }
    }
}
