﻿
using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class SupplierPreApprovedLoanDataGridView : DcsDataGridViewBase {
        private readonly string[] kvName = { "BaseData_Status" };

        public SupplierPreApprovedLoanDataGridView() {
            this.KeyValueManager.Register(this.kvName);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvName[0]]
                    },new ColumnItem {
                        Name = "PreApprovedCode"
                    },new ColumnItem {
                        Name = "ActualDebt",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_SupplierPreApprovedLoan_ActualDebt
                    },new ColumnItem {
                        Name = "PlanArrear",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_SupplierPreApprovedLoan_PlanArrear
                    },new ColumnItem {
                        Name = "ExceedAmount"
                    },new ColumnItem {
                        Name = "ApprovalAmount"
                    },new ColumnItem {
                        Name = "ActualPaidAmount"
                    },new ColumnItem {
                        Name = "CreatorName"
                    },new ColumnItem {
                        Name = "CreateTime"
                    },new ColumnItem {
                        Name = "ModifierName"
                    },new ColumnItem {
                        Name = "ModifyTime"
                    },new ColumnItem {
                        Name = "PartsSalesCategory.Name",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_SupplierPreApprovedLoan_PartsSalesCategory
                    }
                };
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "SupplierPreAppLoanDetail"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            this.GridView.SelectionMode = SelectionMode.Single;
            this.DataPager.PageSize = 300;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["ActualDebt"]).DataFormatString = "n2";
            ((GridViewDataColumn)this.GridView.Columns["PlanArrear"]).DataFormatString = "n2";
            ((GridViewDataColumn)this.GridView.Columns["ExceedAmount"]).DataFormatString = "n2";
            ((GridViewDataColumn)this.GridView.Columns["ApprovalAmount"]).DataFormatString = "n2";
            ((GridViewDataColumn)this.GridView.Columns["ActualPaidAmount"]).DataFormatString = "n2";
            base.OnControlsCreated();
        }

        protected override Type EntityType {
            get {
                return typeof(SupplierPreApprovedLoan);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetSupplierPreApprovedLoanIncludePartsSalesCategory";
        }
    }
}
