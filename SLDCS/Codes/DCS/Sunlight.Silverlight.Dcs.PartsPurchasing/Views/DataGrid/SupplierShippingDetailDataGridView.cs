﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using Telerik.Windows.Data;
using Sunlight.Silverlight.Dcs.Web.Entities;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class SupplierShippingDetailDataGridView : DcsDataGridViewBase {
        private int supplierShippingOrderId;
        public SupplierShippingDetailDataGridView() {
            this.DataContextChanged += this.SupplierShippingDetailForDetailDataGridView_DataContextChanged;
        }

        private void SupplierShippingDetailForDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var supplierShippingOrder = e.NewValue as SupplierShippingOrder;
            if(supplierShippingOrder == null || supplierShippingOrder.Id == default(int))
                return;
            this.supplierShippingOrderId = supplierShippingOrder.Id;
            this.ExecuteQueryDelayed();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            switch (parameterName) {
                case "supplierShippingOrderId":
                    return supplierShippingOrderId;
                default:
                    return null;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                       Name = "SerialNumber",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Number
                    },new ColumnItem {
                       Name = "SparePartCode",
                       Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_SparePartCode2
                    },new ColumnItem {
                       Name = "SparePartName",
                       Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_SparePartName
                    },new ColumnItem {
                       Name = "SupplierPartCode",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierPartCode
                    },new ColumnItem {
                       Name = "MeasureUnit",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_MeasureUnit
                    },new ColumnItem {
                       Name = "PlanAmount",
                       Title = PartsPurchasingUIStrings.DataEditView_ImportTemplate_PlanAmount
                    },new ColumnItem {
                       Name = "WaitShippingAmount",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_WaitShippingAmount
                    },new ColumnItem {
                       Name = "Quantity",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_Quantity
                    },new ColumnItem {
                       Name = "ConfirmedAmount",
                       MaskType = MaskType.Numeric,
                       TextAlignment = TextAlignment.Right,
                       Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingDetail_ConfirmedAmount
                    },new ColumnItem {
                       Name = "Remark",
                       Title = PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_Remark
                    },new ColumnItem {
                       Name = "SpareOrderRemark",
                       Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_SpareOrderRemark
                    },new ColumnItem {
                       Name = "Weight",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Weight
                    },new ColumnItem {
                       Name = "Volume",
                       Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Volume
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualSupplierShippingDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            ((GridViewDataColumn)this.GridView.Columns["ConfirmedAmount"]).DataFormatString = "d";
        }

        protected override string OnRequestQueryName() {
            return "GetSupplierShippingDetailByIdWithOthers";
        }

        protected override bool UsePaging {
            get {
                return true;
            }

        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
    }
}
