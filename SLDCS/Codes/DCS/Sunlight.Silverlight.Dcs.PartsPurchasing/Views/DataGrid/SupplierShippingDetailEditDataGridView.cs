﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class SupplierShippingDetailEditDataGridView  : DcsDataGridViewBase {

        public SupplierShippingDetailEditDataGridView() {
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingDetail_SupplierPartCode,
                        Name = "SupplierPartCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "MeasureUnit",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingDetail_Quantity,
                        Name = "Quantity",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "PendingQuantity",
                        IsReadOnly = true
                    }                 
                    , new ColumnItem {
                        Name = "Remark",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("SupplierShippingDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.IsReadOnly = false;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.CanUserInsertRows = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override Type EntityType {
            get {
                return typeof(SupplierShippingDetail);
            }
        }

        public object SelectedItem {
            get {
                return this.GridView.SelectedItem;
            }
        }
    }
}
