﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class SupplierShippingDetailForConfirmDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                       Name = "SerialNumber",
                       IsReadOnly=true
                    },new ColumnItem {
                       Name = "SparePartCode",
                       IsReadOnly=true
                    },new ColumnItem {
                       Name = "SparePartName",
                       IsReadOnly=true
                    },new ColumnItem {
                       Name = "SupplierPartCode",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierPartCode,
                       IsReadOnly=true
                    },new ColumnItem {
                       Name = "Quantity",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_Quantity,
                       IsReadOnly=true
                    },new ColumnItem {
                       Name = "ConfirmedAmount",
                       MaskType = MaskType.Numeric,
                       TextAlignment = TextAlignment.Right,
                       Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingDetail_ConfirmedAmount,
                       IsReadOnly=true 
                    },new ColumnItem {
                       Name = "MeasureUnit",
                       IsReadOnly=true
                    },new ColumnItem {
                       Name = "Remark"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            ((GridViewDataColumn)this.GridView.Columns["ConfirmedAmount"]).DataFormatString = "d";
        }

        protected override Type EntityType {
            get {
                return typeof(SupplierShippingDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("SupplierShippingDetails");
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
    }
}
