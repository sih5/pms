﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class SupplierShippingDetailForEditDataGridView : DcsDataGridViewBase {
        private DcsMultiPopupsQueryWindowBase partsPurchaseOrderDetailDropDownQueryWindow;
        private readonly string[] kvNames = new[] {
            "TraceProperty"
        };
        public SupplierShippingDetailForEditDataGridView() {
            this.DataContextChanged += this.SupplierShippingDetailForEditDataGridView_DataContextChanged;
            this.KeyValueManager.Register(kvNames);
        }

        private void SupplierShippingDetailForEditDataGridView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            // 新增和修改进入该页面时，清空之前页面编辑的缓存数据
            if(this.GridView != null && this.GridView.SelectedItem != null) {
                this.GridView.SelectedItem = null;
                this.NotifyOfPropertyChange("SelectedItem");
                this.NotifyOfPropertyChange("MasterObject");
                this.NotifyOfPropertyChange("PartsLogisticBatchItemDetails");
            }
            var order = this.DataContext as SupplierShippingOrder;
            if(order == null)
                return;
            order.PropertyChanged -= this.SupplierShippingOrder_PropertyChanged;
            order.PropertyChanged += this.SupplierShippingOrder_PropertyChanged;
        }

        private void SupplierShippingOrder_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            switch(e.PropertyName) {
                case "PartsPurchaseOrderId":
                    // 如果配件采购单ID变了，对应清单中的数据及关系需要重置
                    if(this.GridView != null && this.GridView.SelectedItem != null) {
                        this.GridView.SelectedItem = null;
                        this.NotifyOfPropertyChange("SelectedItem");
                        this.NotifyOfPropertyChange("MasterObject");
                        this.NotifyOfPropertyChange("PartsLogisticBatchItemDetails");
                    }
                    break;
            }
        }

        private DcsMultiPopupsQueryWindowBase PartsPurchaseOrderDetailDropDownQueryWindow {
            get {
                if(this.partsPurchaseOrderDetailDropDownQueryWindow == null) {
                    this.partsPurchaseOrderDetailDropDownQueryWindow = DI.GetQueryWindow("PartsPurchaseOrderDetailDropDown") as DcsMultiPopupsQueryWindowBase;
                    this.partsPurchaseOrderDetailDropDownQueryWindow.Loaded += this.PartsPurchaseOrderDetailDropDownQueryWindow_Loaded;
                    this.partsPurchaseOrderDetailDropDownQueryWindow.SelectionDecided += this.PartsPurchaseOrderDetailDropDownQueryWindow_SelectionDecided;
                }
                return this.partsPurchaseOrderDetailDropDownQueryWindow;
            }
        }

        private RadWindow radQueryWindow;

        public RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = this.PartsPurchaseOrderDetailDropDownQueryWindow,
                    Header = PartsPurchasingUIStrings.QueryPanel_Title_PartsPurchaseOrderDetail,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen
                });
            }
        }

        private void PartsPurchaseOrderDetailDropDownQueryWindow_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var supplierShippingOrder = this.DataContext as SupplierShippingOrder;
            if(supplierShippingOrder == null || supplierShippingOrder.PartsPurchaseOrderId == default(int))
                return;
            PartsPurchaseOrderDetailDropDownQueryWindow.ExchangeData(null, "SetAdditionalFilterItem", new FilterItem("PartsPurchaseOrderId", typeof(int), FilterOperator.IsEqualTo, supplierShippingOrder.PartsPurchaseOrderId));
        }

        private void PartsPurchaseOrderDetailDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var supplierShippingOrder = this.DataContext as SupplierShippingOrder;
            if(supplierShippingOrder == null)
                return;
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var partsPurchaseOrderDetail = queryWindow.SelectedEntities.Cast<PartsPurchaseOrderDetail>().ToArray();
            if(partsPurchaseOrderDetail == null)
                return;
            //var supplierShippingDetail = queryWindow.DataContext as SupplierShippingDetail;
            //if(supplierShippingDetail == null)
            //    return;
            if(supplierShippingOrder.SupplierShippingDetails.Any(r => partsPurchaseOrderDetail.Any(ex => ex.SparePartCode == r.SparePartCode))) {
                UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Error_SparePart_DoubleSparePartID));
                return;
            }

            foreach(var partsPurchaseOrderDetails in partsPurchaseOrderDetail) {
                var supplierShippingDetails = new SupplierShippingDetail {
                    SerialNumber = partsPurchaseOrderDetails.SerialNumber,
                    SparePartId = partsPurchaseOrderDetails.SparePartId,
                    SparePartCode = partsPurchaseOrderDetails.SparePartCode,
                    SparePartName = partsPurchaseOrderDetails.SparePartName,
                    SupplierPartCode = partsPurchaseOrderDetails.SupplierPartCode,
                    MeasureUnit = partsPurchaseOrderDetails.MeasureUnit,
                    UnitPrice = partsPurchaseOrderDetails.UnitPrice,
                    POCode = partsPurchaseOrderDetails.POCode,
                    Quantity = partsPurchaseOrderDetails.ConfirmedAmount - (partsPurchaseOrderDetails.ShippingAmount.HasValue ? partsPurchaseOrderDetails.ShippingAmount.Value : default(int)),
                    ConfirmedAmount = 0,
                    PendingQuantity = partsPurchaseOrderDetails.ConfirmedAmount - (partsPurchaseOrderDetails.ShippingAmount.HasValue ? partsPurchaseOrderDetails.ShippingAmount.Value : default(int))
                };
                //if(supplierShippingOrder.SupplierShippingDetails.Any(r => r.SparePartId == partsPurchaseOrderDetails.SparePartId)) {
                //    var detail = supplierShippingOrder.SupplierShippingDetails.First(r => r.SparePartId == partsPurchaseOrderDetails.SparePartId);
                //    if(detail.SerialNumber != partsPurchaseOrderDetails.SerialNumber) {
                //        UIHelper.ShowAlertMessage(string.Format(PartsPurchasingUIStrings.DataGridView_Validation_SupplierShippingDetail_SparePartCodeIsAlreadyExisit, partsPurchaseOrderDetails.SparePartCode));
                //        return;
                //    }
                //}
                supplierShippingOrder.SupplierShippingDetails.Add(supplierShippingDetails);
                DetailSort();
            }
            //var parent = queryWindow.ParentOfType<RadWindow>();
            //if(parent != null)
            //    parent.Close();

            //if(supplierShippingOrder.SupplierShippingDetails.Any(r => r.SparePartId == partsPurchaseOrderDetail.SparePartId)) {
            //    var detail = supplierShippingOrder.SupplierShippingDetails.First(r => r.SparePartId == partsPurchaseOrderDetail.SparePartId);
            //    if(detail.SerialNumber != supplierShippingDetail.SerialNumber) {
            //        UIHelper.ShowAlertMessage(string.Format(PartsPurchasingUIStrings.DataGridView_Validation_SupplierShippingDetail_SparePartCodeIsAlreadyExisit, partsPurchaseOrderDetail.SparePartCode));
            //        return;
            //    }
            //}
            //var domainContext = this.DomainContext as DcsDomainContext;
            //if(domainContext == null)
            //    return;
            ////根据选中配件及主单上的营销分公司获取配件营销信息，供后续新增批次信息时使用
            //domainContext.Load(domainContext.GetSparePartsWithPartsBranchByBranchIdAndPartIdsQuery(supplierShippingOrder.BranchId, new[] {
            //    partsPurchaseOrderDetail.SparePartId
            //}), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError) {
            //        if(!loadOp.IsErrorHandled)
            //            loadOp.MarkErrorAsHandled();
            //        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
            //        return;
            //    }
            //    if(loadOp.Entities == null || !loadOp.Entities.Any()) {
            //    UIHelper.ShowNotification(PartsPurchasingUIStrings.DataGridView_Notification_SupplierShippingDetail_SelectedPartNotExistPartsBranchInfo);
            //        return;
            //    }
            //    supplierShippingDetail.SparePartId = partsPurchaseOrderDetail.SparePartId;
            //    supplierShippingDetail.SparePartCode = partsPurchaseOrderDetail.SparePartCode;
            //    supplierShippingDetail.SparePartName = partsPurchaseOrderDetail.SparePartName;
            //    supplierShippingDetail.SupplierPartCode = partsPurchaseOrderDetail.SupplierPartCode;
            //    supplierShippingDetail.MeasureUnit = partsPurchaseOrderDetail.MeasureUnit;
            //    supplierShippingDetail.UnitPrice = partsPurchaseOrderDetail.UnitPrice;
            //    if(supplierShippingDetail.Id != default(int))
            //        //修改界面，待运量=配件采购清单.确认量-配件采购清单.发运量+供应商发运清单.发运量（本次发运单）
            //        supplierShippingDetail.PendingQuantity = partsPurchaseOrderDetail.ConfirmedAmount - (partsPurchaseOrderDetail.ShippingAmount.HasValue ? partsPurchaseOrderDetail.ShippingAmount.Value : default(int)) + supplierShippingDetail.Quantity;
            //    else
            //        //弹出窗体，选择配件返回界面时，计算待运量=配件采购清单.确认量-配件采购清单.发运量
            //        supplierShippingDetail.PendingQuantity = partsPurchaseOrderDetail.ConfirmedAmount - (partsPurchaseOrderDetail.ShippingAmount.HasValue ? partsPurchaseOrderDetail.ShippingAmount.Value : default(int));
            //    //然后再发运量 = 待运量
            //    supplierShippingDetail.Quantity = supplierShippingDetail.PendingQuantity;
            //}, null);
        }

        private void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            var supplierShippingOrder = this.DataContext as SupplierShippingOrder;
            if(supplierShippingOrder == null || e.Items == null || !e.Items.Cast<SupplierShippingDetail>().Any())
                return;
            var serialNumber = 1;
            foreach(var supplierShippingDetail in supplierShippingOrder.SupplierShippingDetails.Where(entity => !e.Items.Contains(entity)))
                supplierShippingDetail.SerialNumber = serialNumber++;
            if(supplierShippingOrder.SupplierShippingDetails.Any()) {
                var supplierShippingDetails = e.Items.Cast<SupplierShippingDetail>();
                var partsLogisticBatchBillDetails = supplierShippingOrder.PartsLogisticBatchBillDetails.ToArray();
                var domainContext = this.DomainContext as DcsDomainContext;
                if(domainContext == null)
                    return;
                foreach(var entity in supplierShippingDetails) {
                    var batchItemDetails = partsLogisticBatchBillDetails.Where(ex => ex.PartsLogisticBatch != null).Select(ex => ex.PartsLogisticBatch).SelectMany(ex => ex.PartsLogisticBatchItemDetails).Where(ex => ex.SparePartId == entity.SparePartId);
                    foreach(var batchItemDetail in batchItemDetails) {
                        // 获取 配件物流批次 主单，删除对应配件发运清单的物流批次配件清单数据
                        var logisticBatch = domainContext.PartsLogisticBatches.SingleOrDefault(ex => ex.Id == batchItemDetail.PartsLogisticBatchId);
                        if(logisticBatch != null && logisticBatch.PartsLogisticBatchItemDetails.Contains(batchItemDetail))
                            logisticBatch.PartsLogisticBatchItemDetails.Remove(batchItemDetail);
                    }
                    //if(supplierShippingOrder.SupplierShippingDetails.Contains(entity))
                    //    supplierShippingOrder.SupplierShippingDetails.Remove(entity);
                }
            }
            DetailSort();
        }

        private void GridView_SelectionChanged(object sender, SelectionChangeEventArgs e) {
            this.NotifyOfPropertyChange("SelectedItem");
            this.NotifyOfPropertyChange("MasterObject");
            this.NotifyOfPropertyChange("PartsLogisticBatchItemDetails");
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            var supplierShippingOrder = this.DataContext as SupplierShippingOrder;
            if(supplierShippingOrder == null || supplierShippingOrder.PartsPurchaseOrderId == default(int)) {
                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataGridView_Validation_SupplierShippingDetail_PartsPurchaseOrderIsNull);
                //e.Cancel = true;
                return;
            }
            var supplierShippingDetail = new SupplierShippingDetail {
                SerialNumber = supplierShippingOrder.SupplierShippingDetails.Any() ? supplierShippingOrder.SupplierShippingDetails.Max(entity => entity.SerialNumber) : 1
            };
            supplierShippingDetail.SerialNumber = supplierShippingOrder.SupplierShippingDetails.Any() ? supplierShippingOrder.SupplierShippingDetails.Max(entity => entity.SerialNumber) + 1 : 1;
            supplierShippingDetail.Quantity = supplierShippingDetail.PendingQuantity;
            supplierShippingDetail.ConfirmedAmount = 0;
            e.NewObject = supplierShippingDetail;
            RadQueryWindow.ShowDialog();
        }

        private void DetailSort() {
            var supplierShippingOrder = this.DataContext as SupplierShippingOrder;
            int SerialNumber = 1;
            foreach(var item in supplierShippingOrder.SupplierShippingDetails) {
                item.SerialNumber = SerialNumber++;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingDetail_SupplierPartCode,
                        Name = "SupplierPartCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "MeasureUnit",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingDetail_Quantity,
                        Name = "Quantity"
                    }, new ColumnItem {
                        Name = "PendingQuantity",
                        IsReadOnly = true
                    }
                   
                    , new ColumnItem {
                        Name = "Remark"
                    }, new KeyValuesColumnItem {
                        Name = "TraceProperty",
                        Title = "追溯属性",
                        IsReadOnly = true,
                         KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },
                    new ColumnItem {
                        Title="追溯码",
                        Name = "TraceCode"
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("SupplierShippingDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.IsReadOnly = false;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.CanUserInsertRows = true;
            this.GridView.Deleting += this.GridView_Deleting;
            this.GridView.SelectionChanged += this.GridView_SelectionChanged;
            this.GridView.AddingNewDataItem -= this.GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override Type EntityType {
            get {
                return typeof(SupplierShippingDetail);
            }
        }

        public object SelectedItem {
            get {
                return this.GridView.SelectedItem;
            }
        }

        public object MasterObject {
            get {
                return this.DataContext;
            }
        }

        public object PartsLogisticBatchItemDetails {
            get {
                var supplierShipping = this.DataContext as SupplierShippingOrder;
                if(supplierShipping == null)
                    return null;
                if(!supplierShipping.PartsLogisticBatchBillDetails.Any()) {
                    var partsLogisticBatchBillDetail = new PartsLogisticBatchBillDetail();
                    partsLogisticBatchBillDetail.BillType = (int)DcsPartsLogisticBatchBillDetailBillType.供应商发运单;
                    supplierShipping.PartsLogisticBatchBillDetails.Add(partsLogisticBatchBillDetail);
                }
                if(supplierShipping.PartsLogisticBatchBillDetails.First().PartsLogisticBatchId == default(int) && supplierShipping.PartsLogisticBatchBillDetails.First().PartsLogisticBatch == null) {
                    var partsLogisticBatch = new PartsLogisticBatch();
                    partsLogisticBatch.ShippingStatus = (int)DcsPartsLogisticBatchShippingStatus.供应商发运;
                    partsLogisticBatch.Status = (int)DcsPartsLogisticBatchStatus.新增;
                    supplierShipping.PartsLogisticBatchBillDetails.First().PartsLogisticBatch = partsLogisticBatch;
                }
                return supplierShipping.PartsLogisticBatchBillDetails.First().PartsLogisticBatch.PartsLogisticBatchItemDetails;
            }
        }
    }
}
