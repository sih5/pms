﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class SupplierShippingDetailForSupplierDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "SparePart_MeasureUnit"
        };

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber"
                    }, new ColumnItem {
                        Name = "SparePartCode"
                    }, new ColumnItem {
                        Name = "SparePartName"
                    }, new ColumnItem {
                        Name = "SupplierPartCode"
                    }, new KeyValuesColumnItem {
                        Name = "MeasureUnit",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Quantity"
                    }, new ColumnItem {
                       Name = "ConfirmedAmount",
                       MaskType = MaskType.Numeric,
                       TextAlignment = TextAlignment.Right,
                       Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingDetail_ConfirmedAmount
                    },new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SupplierShippingDetail);
            }
        }

        public SupplierShippingDetailForSupplierDataGridView() {
            this.DataContextChanged += this.SupplierShippingDetailForSupplierDataGridView_DataContextChanged;
            this.KeyValueManager.Register(this.kvNames);
        }

        private void SupplierShippingDetailForSupplierDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var supplierShippingOrder = e.NewValue as SupplierShippingOrder;
            if(supplierShippingOrder == null || supplierShippingOrder.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "SupplierShippingOrderId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = supplierShippingOrder.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override string OnRequestQueryName() {
            return "GetSupplierShippingDetails";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            ((GridViewDataColumn)this.GridView.Columns["ConfirmedAmount"]).DataFormatString = "d";
        }
    }
}
