﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class SupplierShippingOrderDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "SupplierShippingOrder_Status", "PartsShipping_Method"
        };

        public SupplierShippingOrderDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "ReceivingWarehouseName"
                    }, new ColumnItem {
                        Name = "Company1.Code",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_PartsSupplierCode
                    }, new ColumnItem {
                        Name = "Company1.Name",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_PartsSupplierName
                    }, new ColumnItem {
                        Name = "PartsPurchaseOrderCode"
                    },  new ColumnItem {
                        Name = "PlanSource"
                    }, new ColumnItem {
                        Name = "ReceivingAddress",
                        IsGroupable = true
                    }, new ColumnItem {
                        Name = "ReceivingCompanyName",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingOrder_ReceivingCompanyName
                    }, new ColumnItem {
                        Name = "IfDirectProvision",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingOrder_IfDirectProvision
                    }, new ColumnItem {
                        Name = "OriginalRequirementBillCode"
                    }
                    //, new ColumnItem{
                    //    Name = "TotalAmount",
                    //    Title = "发运总金额",
                    //    TextAlignment = TextAlignment.Right,
                    //}
                    , new ColumnItem {
                        Name = "DirectProvisionFinished",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingOrder_DirectProvisionFinished
                    }, new ColumnItem {
                        Name = "DeliveryBillNumber"
                    },new ColumnItem{
                        Name="RequestedDeliveryTime",
                        Title=PartsPurchasingUIStrings.DataEditView_ImportTemplate_RequestedDeliveryTime
                    },new ColumnItem{
                        Name="PlanDeliveryTime",
                        Title=PartsPurchasingUIStrings.DataEditView_Title_PlanDeliveryTime
                    }, new ColumnItem {
                        Name = "ShippingDate"
                    }, new ColumnItem {
                        Name = "ArrivalDate"
                    }, new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Name = "LogisticCompany"
                    }, new ColumnItem {
                        Name = "Driver"
                    }, new ColumnItem {
                        Name = "Phone"
                    }, new ColumnItem {
                        Name = "VehicleLicensePlate"
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "ERPSourceOrderCode",
                        Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsShippingOrder_ERPSourceOrderCode
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "BranchName"
                    }, new ColumnItem{
                        Name = "PartsSalesCategoryName",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingOrder_PartsSalesCategoryName
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetSupplierShippingOrderWithCompany";
        }

        protected override Type EntityType {
            get {
                return typeof(SupplierShippingOrder);
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "SupplierShippingOrder", "SupplierSupplierShippingDetail"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.RowLoaded += GridView_RowLoaded;
            this.GridView.Columns["BranchName"].ShowColumnWhenGrouped = false;
            this.GridView.Columns["Company1.Code"].ShowColumnWhenGrouped = false;
           // ((GridViewDataColumn)this.GridView.Columns["TotalAmount"]).DataFormatString = "c2";
        }

        private void GridView_RowLoaded(object sender, RowLoadedEventArgs e) {
            var supplierShippingOrder = e.Row.DataContext as SupplierShippingOrder;
            if(supplierShippingOrder == null)
                return;
            GridViewRow girdViewRow = this.GridView.GetRowForItem(supplierShippingOrder);
            if(supplierShippingOrder.RequestedDeliveryTime < DateTime.Now && (supplierShippingOrder.Status == (int)DcsSupplierShippingOrderStatus.新建 || supplierShippingOrder.Status == (int)DcsSupplierShippingOrderStatus.部分确认)) {
                if(girdViewRow != null) {
                    girdViewRow.Background = new SolidColorBrush(Color.FromArgb(255, 222, 150, 162));
                }
            } else {
                if(girdViewRow != null)
                    girdViewRow.Background = new SolidColorBrush(Colors.White);
            }
        }
    }
}
