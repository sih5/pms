﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Web.Entities;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class SupplierShippingOrderForComfirmDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "SupplierShippingOrder_Status","SupplierShippingOrderArriveMethod","SupplierShippingOrderComfirmStatus","SupplierShippingOrderModifyStatus"
        };

        public SupplierShippingOrderForComfirmDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        IsSortable = false, 
                        Name = "IsUplodFile",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_IsUplodFile
                    },new KeyValuesColumnItem {
                        Name = "ComfirmStatus",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]],
                         Title="确认状态"
                    },new ColumnItem {
                        Name = "Code",
                         Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_ShippingOrder_Code
                    },new ColumnItem {
                        Name = "LogisticArrivalDate",
                         Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_LogisticArrivalDate
                    },new ColumnItem {
                        Name = "ModifyArrive",
                         Title="修改承运商送达时间"
                    },  new KeyValuesColumnItem {
                        Name = "ArriveMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                         Title="到货确认方式"
                    },new ColumnItem {
                        Name = "InboundTime",
                        Title = "入库时间"
                    },new ColumnItem{
                        Name="PlanDeliveryTime",
                        Title=PartsPurchasingUIStrings.DataEditView_Title_PlanDeliveryTime
                    } ,new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                         Title="发运单状态"
                    },new ColumnItem {
                        Name = "PartsSupplierCode",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_PartsSupplierCode
                    }, new ColumnItem {
                        Name = "PartsSupplierName",
                         Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_PartsSupplierName
                    },new ColumnItem {
                        Name = "PartsPurchaseOrderCode",
                         Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_TitlePartsPurReturnOrder_Code
                    }, new ColumnItem {
                        Name = "ReceivingWarehouseName",
                         Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_SupplierShippingOrder_ReceivingWarehouseId
                    }, new ColumnItem {
                        Name = "ReceivingAddress",
                         Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_ReceivingAddress
                    }, new ColumnItem {
                        Name = "ReceivingCompanyName",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingOrder_ReceivingCompanyName
                    }, new ColumnItem {
                        Name = "IfDirectProvision",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingOrder_IfDirectProvision
                    },new ColumnItem{
                        Name="OriginalRequirementBillCode",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OriginalRequirementBillCode
                    },new ColumnItem {
                        Name = "LogisticCompany",
                         Title="物流公司"
                    },new ColumnItem {
                        Name = "DeliveryBillNumber",
                         Title=PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_DeliveryBillNumber
                    }, new ColumnItem {
                        Name = "SupplierComfirm",
                         Title="确认人"
                    },new ColumnItem {
                        Name = "SupplierComfirmDate",
                         Title="确认时间"
                    },  new ColumnItem {
                        Name = "SupplierModify",
                         Title="修改人"
                    },new ColumnItem {
                        Name = "SupplierModifyDate",
                         Title="修改时间"
                    }, new ColumnItem {
                        Name = "Approver",
                         Title="审核人"
                    },new ColumnItem {
                        Name = "ApproveDate",
                         Title="审核时间"
                    },new KeyValuesColumnItem {
                        Name = "ModifyStatus",
                        KeyValueItems = this.KeyValueManager[this.kvNames[3]],
                         Title="修改状态"
                    },new ColumnItem {
                        Name = "RejectReason",
                         Title="审核意见"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SupplierShippingOrder);
            }
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch(parameterName) {
                    case "code":
                        var code = filters.Filters.Single(item => item.MemberName == "Code");
                        return code == null ? null : code.Value;
                    case "partsPurchaseOrderCode":
                        var partsPurchaseOrderCode = filters.Filters.Single(item => item.MemberName == "PartsPurchaseOrderCode");
                        return partsPurchaseOrderCode == null ? null : partsPurchaseOrderCode.Value;
                    case "partsSupplierName":
                        var partsSupplierName = filters.Filters.Single(item => item.MemberName == "PartsSupplierName");
                        return partsSupplierName == null ? null : partsSupplierName.Value;
                    case "partsSupplierCode":
                        var partsSupplierCode = filters.Filters.Single(item => item.MemberName == "PartsSupplierCode");
                        return partsSupplierCode == null ? null : partsSupplierCode.Value;
                    case "arriveMethod":
                        var eRPSourceOrderCode = filters.Filters.Single(item => item.MemberName == "ArriveMethod");
                        return eRPSourceOrderCode == null ? null : eRPSourceOrderCode.Value;
                    case "comfirmStatus":
                        var directProvisionFinished = filters.Filters.Single(item => item.MemberName == "ComfirmStatus");
                        return directProvisionFinished == null ? null : directProvisionFinished.Value;
                    case "modifyStatus":
                        var modifyStatus = filters.Filters.Single(item => item.MemberName == "ModifyStatus");
                        return modifyStatus == null ? null : modifyStatus.Value;
                    case "ifDirectProvision":
                        var ifDirectProvision = filters.Filters.Single(item => item.MemberName == "IfDirectProvision");
                        return ifDirectProvision == null ? null : ifDirectProvision.Value;
                    case "createTimeBegin":
                        var bOutBoundTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return bOutBoundTime == null ? null : bOutBoundTime.Filters.First(item => item.MemberName == "CreateTime").Value;
                    case "createTimeEnd":
                        var eOutBoundTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return eOutBoundTime == null ? null : eOutBoundTime.Filters.Last(item => item.MemberName == "CreateTime").Value;
                }
            }
                return base.OnRequestQueryParameter(queryName, parameterName);
            }

        protected override string OnRequestQueryName() {
            return "GetSupplierShippingOrderWithDifCompany";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "SupplierShippingDetailForConFirm"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
          
        }
    }
}
