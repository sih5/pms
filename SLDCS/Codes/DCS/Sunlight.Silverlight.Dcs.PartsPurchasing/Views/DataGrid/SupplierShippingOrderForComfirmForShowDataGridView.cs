﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class SupplierShippingOrderForComfirmForShowDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new ColumnItem {
                       Name = "SparePartCode",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_SparePartCode
                    },new ColumnItem {
                       Name = "SparePartName",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_SparePartName
                    },new ColumnItem {
                       Name = "SupplierPartCode",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierPartCode
                    },new ColumnItem {
                       Name = "MeasureUnit",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_MeasureUnit
                    },new ColumnItem {
                       Name = "Quantity",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_Quantity
                    },new ColumnItem {
                       Name = "ConfirmedAmount",
                       MaskType = MaskType.Numeric,
                       TextAlignment = TextAlignment.Right,
                       Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingDetail_ConfirmedAmount
                    },new ColumnItem {
                       Name = "UnitPrice",
                       MaskType = MaskType.Numeric,
                       TextAlignment = TextAlignment.Right,
                       Title="单价"
                    },new ColumnItem {
                       Name = "SpareOrderRemark",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_SupplierShippingDetail_SpareOrderRemark
                    },new ColumnItem {
                       Name = "Remark",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_SupplierShippingDetail_Remark
                    },new ColumnItem {
                       Name = "Weight",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_SupplierShippingDetail_Weight
                    },new ColumnItem {
                       Name = "Volume",
                       Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_SupplierShippingDetail_Volume
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SupplierShippingDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.ValidatesOnDataErrors = GridViewValidationMode.InViewMode;
        }

     
        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("SupplierShippingDetails");
        }
    }
}