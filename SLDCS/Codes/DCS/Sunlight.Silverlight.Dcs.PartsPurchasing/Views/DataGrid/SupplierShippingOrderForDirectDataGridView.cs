﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class SupplierShippingOrderForDirectDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "SupplierShippingOrder_Status","PartsShipping_Method"
        };

        public SupplierShippingOrderForDirectDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            //this.DataLoaded += SupplierShippingOrderForDirectDataGridView_DataLoaded;
        }

        //private void SupplierShippingOrderForDirectDataGridView_DataLoaded(object sender, EventArgs e) {
        //    throw new NotImplementedException();
        //}

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Code",
                        ShowColumnWhenGrouped = true
                    }, new ColumnItem {
                        Name = "ReceivingWarehouseName"
                    }, new ColumnItem {
                        Name = "OriginalRequirementBillCode",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrder_Code
                    }, new ColumnItem {
                        Name = "ReceivingAddress"
                    }, new ColumnItem {
                        Name = "IfDirectProvision",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingOrder_IfDirectProvision
                    }, new ColumnItem {
                        Name = "DirectProvisionFinished",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingOrder_DirectProvisionFinished
                    }, new ColumnItem {
                        Name = "ReceivingCompanyName",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingOrder_ReceivingCompanyName
                    }, new ColumnItem {
                        Name = "DeliveryBillNumber"
                    }, new ColumnItem {
                        Name = "ShippingDate"
                    }, new ColumnItem {
                        Name = "ArrivalDate"
                    }, new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Name = "LogisticCompany"
                    }, new ColumnItem {
                        Name = "Driver"
                    }, new ColumnItem {
                        Name = "Phone"
                    }, new ColumnItem {
                        Name = "VehicleLicensePlate"
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "BranchName",
                        ShowColumnWhenGrouped = true
                    },new ColumnItem{
                        Name="PartsSalesCategoryName",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrder_PartsSalesCategoryName
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SupplierShippingOrder);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询供应商直供发运单New";
        }

        //protected override object OnRequestQueryParameter(string queryName, string parameterName) {
        //    var filters = this.FilterItem as CompositeFilterItem;
        //    if(filters != null) {
        //        switch(parameterName) {
        //            case "partsSalesOrderCode":
        //                var partsSalesOrderCode = filters.Filters.SingleOrDefault(item => item.MemberName == "PartsSalesOrder.Code");
        //                return partsSalesOrderCode == null ? null : partsSalesOrderCode.Value;
        //        }
        //    }
        //    return base.OnRequestQueryParameter(queryName, parameterName);
        //}

        //protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
        //    var compositeFilterItem = this.FilterItem as CompositeFilterItem;
        //    var newCompositeFilterItem = new CompositeFilterItem();

        //    if(compositeFilterItem == null) {
        //        newCompositeFilterItem.Filters.Add(this.FilterItem);
        //    } else {
        //        foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "PartsSalesOrder.Code"))
        //            newCompositeFilterItem.Filters.Add(item);
        //    }
        //    return newCompositeFilterItem.ToFilterDescriptor();
        //}

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "SupplierShippingOrder", "SupplierShippingDetail"
                    }
                };
            }
        }
    }
}
