﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class SupplierShippingOrderForSupplierDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsShipping_Method","SupplierShippingOrder_Status"
        };

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "PartsPurchaseOrderCode"
                    }, new ColumnItem {
                        Name = "ReceivingAddress"
                    }, new ColumnItem {
                        Name = "DeliveryBillNumber"
                    }, new ColumnItem {
                        Name = "ShippingDate"
                    }, new ColumnItem {
                        Name = "ArrivalDate"
                    }, new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "LogisticCompany"
                    }, new ColumnItem {
                        Name = "Driver"
                    }, new ColumnItem {
                        Name = "Phone"
                    }, new ColumnItem {
                        Name = "VehicleLicensePlate"
                    },  new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SupplierShippingOrder);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetSupplierShippingOrders";
        }

        public SupplierShippingOrderForSupplierDataGridView() {
            this.DataContextChanged += this.SupplierShippingOrderForSupplierDataGridView_DataContextChanged;
            this.KeyValueManager.Register(this.kvNames);
        }

        private void SupplierShippingOrderForSupplierDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsPurchaseOrder = e.NewValue as VirtualPartsPurchaseOrder;
            if(partsPurchaseOrder == null || partsPurchaseOrder.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsPurchaseOrderId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsPurchaseOrder.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override Core.IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "SupplierShippingDetailForSupplier"
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
        }
    }
}
