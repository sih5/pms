﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Web.Entities;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class SupplierShippingOrderWithDetailsForBranchDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "SupplierShippingOrder_Status","PartsShipping_Method"
        };

        public SupplierShippingOrderWithDetailsForBranchDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                         Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Status
                    },new ColumnItem {
                        Name = "Code",
                         Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_ShippingOrder_Code
                    }, new ColumnItem {
                        Name = "ReceivingWarehouseName",
                         Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_SupplierShippingOrder_ReceivingWarehouseId
                    },new ColumnItem {
                        Name = "PartsSupplierCode",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_PartsSupplierCode
                    }, new ColumnItem {
                        Name = "PartsSupplierName",
                         Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_PartsSupplierName
                    },new ColumnItem {
                        Name = "PartsPurchaseOrderCode",
                         Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_TitlePartsPurReturnOrder_Code
                    },  new ColumnItem {
                        Name = "PlanSource",
                         Title=PartsPurchasingUIStrings.DataEditView_ImportTemplate_PlanSource
                    }, new ColumnItem {
                        Name = "ReceivingAddress",
                         Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_ReceivingAddress
                    }, new ColumnItem {
                        Name = "ReceivingCompanyName",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingOrder_ReceivingCompanyName
                    }, new ColumnItem {
                        Name = "IfDirectProvision",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingOrder_IfDirectProvision
                    },new ColumnItem{
                        Name="OriginalRequirementBillCode",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OriginalRequirementBillCode
                    }, new ColumnItem{
                        Name = "TotalAmount",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_ShippingTotalAmount,
                        TextAlignment = TextAlignment.Right,
                    }, new ColumnItem {
                        Name = "DirectProvisionFinished",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingOrder_DirectProvisionFinished
                    },new ColumnItem {
                        Name = "DeliveryBillNumber",
                         Title=PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_DeliveryBillNumber
                    },new ColumnItem{
                        Name="RequestedDeliveryTime",
                        Title=PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_RequestedDeliveryTime
                    },new ColumnItem{
                        Name="PlanDeliveryTime",
                        Title=PartsPurchasingUIStrings.DataEditView_Title_PlanDeliveryTime
                    }, new ColumnItem {
                        Name = "ShippingDate",
                         Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_ShippingDate2
                    },new ColumnItem {
                        Name = "ArrivalDate",
                         Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_ArrivalDate
                    },new ColumnItem {
                        Name = "LogisticArrivalDate",
                         Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_LogisticArrivalDate
                    },  new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                         Title=PartsPurchasingUIStrings.DataEditView_ImportTemplate_ShippingMethod
                    },new ColumnItem {
                        Name = "LogisticCompany",
                         Title=PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_LogisticCompany
                    }, new ColumnItem {
                        Name = "Driver",
                         Title=PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_Driver
                    },new ColumnItem {
                        Name = "Phone",
                         Title=PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_Phone
                    }, new ColumnItem {
                        Name = "VehicleLicensePlate",
                         Title=PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_VehicleLicensePlate
                    }, new ColumnItem {
                        Name = "Remark",
                         Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Remark
                    }, new ColumnItem {
                        Name = "ERPSourceOrderCode",
                        Title =PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsShippingOrder_ERPSourceOrderCode
                    },new ColumnItem {
                        Name = "CreatorName",
                         Title=PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_Creator
                    }, new ColumnItem {
                        Name = "CreateTime",
                         Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_BillCreateTime
                    },new ColumnItem {
                        Name = "BranchName",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsBranch_BranchName
                    }, new ColumnItem{
                        Name = "PartsSalesCategoryName",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingOrder_PartsSalesCategoryName
                    },new ColumnItem {
                        Name = "ModifyArrive",
                         Title="修改承运商送达时间"
                    },new ColumnItem {
                        Name = "InboundTime",
                        Title = "入库时间"
                    }, new ColumnItem {
                        Name = "Approver",
                         Title="审核人"
                    },new ColumnItem {
                        Name = "ApproveDate",
                         Title="审核时间"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualSupplierShippingOrder);
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                return base.OnRequestFilterDescriptor(queryName);
            } else {
                var cFilterItem = compositeFilterItem.Filters.FirstOrDefault(item => item.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                if(cFilterItem == null)
                    return base.OnRequestFilterDescriptor(queryName);
                foreach(var item in cFilterItem.Filters.Where(filter => filter.MemberName != "PartsSalesCategoryId" && filter.MemberName != "ReceivingWarehouseId" && filter.MemberName != "Code" && filter.MemberName != "ERPSourceOrderCode"
                    && filter.MemberName != "PartsPurchaseOrderCode" && filter.MemberName != "DirectProvisionFinished" && filter.MemberName != "IfDirectProvision" && filter.MemberName != "Status" && filter.MemberName != "CreateTime" && filter.MemberName != "PartsSupplierName" && filter.MemberName != "PartsSupplierCode" && filter.MemberName != "BranchId"))
                    newCompositeFilterItem.Filters.Add(item);
                //foreach(var item in cFilterItem.Filters.Where(filter =>filter.MemberName != "PartsSupplierName" && filter.MemberName != "PartsSupplierCode"))
                //    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return null;
            var cFilterItem = compositeFilterItem.Filters.FirstOrDefault(item => item.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
            if(cFilterItem == null)
                return null;
            switch(parameterName) {
                case "partsSalesCategoryId":
                    var partsSalesCategoryId = cFilterItem.Filters.FirstOrDefault(item => item.MemberName == "PartsSalesCategoryId");
                    return partsSalesCategoryId == null ? null : partsSalesCategoryId.Value;
                case "receivingWarehouseId":
                    var receivingWarehouseId = cFilterItem.Filters.FirstOrDefault(item => item.MemberName == "ReceivingWarehouseId");
                    return receivingWarehouseId == null ? null : receivingWarehouseId.Value;
                case "code":
                    var code = cFilterItem.Filters.FirstOrDefault(item => item.MemberName == "Code");
                    return code == null ? null : code.Value;
                case "partsPurchaseOrderCode":
                    var partsPurchaseOrderCode = cFilterItem.Filters.FirstOrDefault(item => item.MemberName == "PartsPurchaseOrderCode");
                    return partsPurchaseOrderCode == null ? null : partsPurchaseOrderCode.Value;
                case "partsSupplierName":
                    var partsSupplierName = cFilterItem.Filters.FirstOrDefault(item => item.MemberName == "PartsSupplierName");
                    return partsSupplierName == null ? null : partsSupplierName.Value;
                case "partsSupplierCode":
                    var partsSupplierCode = cFilterItem.Filters.FirstOrDefault(item => item.MemberName == "PartsSupplierCode");
                    return partsSupplierCode == null ? null : partsSupplierCode.Value;
                case "eRPSourceOrderCode":
                    var eRPSourceOrderCode = cFilterItem.Filters.FirstOrDefault(item => item.MemberName == "ERPSourceOrderCode");
                    return eRPSourceOrderCode == null ? null : eRPSourceOrderCode.Value;
                case "directProvisionFinished":
                    var directProvisionFinished = cFilterItem.Filters.FirstOrDefault(item => item.MemberName == "DirectProvisionFinished");
                    return directProvisionFinished == null ? null : directProvisionFinished.Value;
                case "ifDirectProvision":
                    var ifDirectProvision = cFilterItem.Filters.FirstOrDefault(item => item.MemberName == "IfDirectProvision");
                    return ifDirectProvision == null ? null : ifDirectProvision.Value;
                case "status":
                    var status = cFilterItem.Filters.FirstOrDefault(item => item.MemberName == "Status");
                    return status == null ? null : status.Value;
                case "createTimeBegin":
                case "createTimeEnd":
                    object createTimeBegin = null;
                    object createTimeEnd = null;
                    var composites = cFilterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).ToArray();
                    if(composites != null) {
                        var createtime = composites.FirstOrDefault() as CompositeFilterItem;
                        if(createtime != null) {
                            if(createtime.Filters.FirstOrDefault(r => r.MemberName == "CreateTime") != null) {
                                createTimeBegin = createtime.Filters.First(r => r.MemberName == "CreateTime").Value;
                                createTimeEnd = createtime.Filters.Last(r => r.MemberName == "CreateTime").Value;
                            }
                        }
                    }
                    switch(parameterName) {
                        case "createTimeBegin":
                            return createTimeBegin;
                        case "createTimeEnd":
                            return createTimeEnd;
                        default:
                            return null;
                    }
            }
            return null;
        }

        protected override string OnRequestQueryName() {
            return "根据人员查询供应商发运单";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "SupplierShippingOrder","SupplierShippingDetailForBranch"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["TotalAmount"]).DataFormatString = "c2";
        }
    }
}
