﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class SupplierSupplierShippingDetailDataGridView : DcsDataGridViewBase {
        public SupplierSupplierShippingDetailDataGridView() {
            this.DataContextChanged += this.SupplierShippingDetailForDetailDataGridView_DataContextChanged;
        }

        private void SupplierShippingDetailForDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var supplierShippingOrder = e.NewValue as SupplierShippingOrder;
            if(supplierShippingOrder == null || supplierShippingOrder.Id == default(int))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "SupplierShippingOrderId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                };
            this.FilterItem.Value = supplierShippingOrder.Id;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                       Name = "SerialNumber"
                    },new ColumnItem {
                       Name = "SupplierPartCode",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierPartCode
                    },
                    //new ColumnItem {
                    //   Name = "SparePartCode"
                    //},
                    new ColumnItem {
                       Name = "SparePartName"
                    },new ColumnItem {
                       Name = "MeasureUnit"
                    },new ColumnItem {
                       Name = "Quantity",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_Quantity
                    },new ColumnItem {
                       Name = "Remark"
                    },new ColumnItem {
                       Name = "Weight",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Weight
                    },new ColumnItem {
                       Name = "Volume",
                       Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Volume
                    }
                };
            }
        }
        protected override Type EntityType {
            get {
                return typeof(SupplierShippingDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetSupplierShippingDetails";
        }
    }
}
