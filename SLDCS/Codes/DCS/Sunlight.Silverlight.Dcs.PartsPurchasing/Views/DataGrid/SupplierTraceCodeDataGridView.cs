﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Web.Entities;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using SelectionMode = System.Windows.Controls.SelectionMode;
namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class SupplierTraceCodeDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "SupplierTraceCodeStatus"
        };

        public SupplierTraceCodeDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "IsUplodFile",
                        Title="是否有附件"
                    },
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                         Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Status
                    },new ColumnItem {
                        Name = "Code",
                        Title="修正单号"
                    },new ColumnItem {
                        Name = "ApproveComment",
                        Title="审核意见"
                    },new ColumnItem {
                        Name = "CreatorName",
                        Title =  "创建人"
                    },new ColumnItem {
                         Name = "CreateTime",
                         Title =  "创建时间"
                    },new ColumnItem {
                        Name = "ModifierName",
                        Title =  "修改人"
                    },new ColumnItem {
                         Name = "ModifyTime",
                         Title =  "修改时间"
                    },new ColumnItem {
                         Name = "SubmitterName",
                         Title =  "提交人"
                    },new ColumnItem {
                         Name = "SubmitTime",
                         Title =  "提交时间"
                    },new ColumnItem {
                         Name = "CheckerName",
                         Title =  "审核人"
                    },new ColumnItem {
                         Name = "CheckTime",
                         Title =  "审核时间"
                    },new ColumnItem {
                         Name = "ApproverName",
                         Title =  "审批人"
                    },new ColumnItem {
                         Name = "ApproverTime",
                         Title =  "审批时间"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SupplierTraceCode);
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filterItem = this.FilterItem as CompositeFilterItem;
            if(filterItem == null)
                return null;
            switch(parameterName) {
                case "partsSupplierCode":
                    return filterItem.Filters.SingleOrDefault(item => item.MemberName == "PartsSupplierCode").Value;
                case "partsSupplierName":
                    return filterItem.Filters.SingleOrDefault(item => item.MemberName == "PartsSupplierName").Value;
                case "sparePartCode":
                    return filterItem.Filters.SingleOrDefault(item => item.MemberName == "SparePartCode").Value;
                case "partsPurchaseOrderCode":
                    return filterItem.Filters.SingleOrDefault(item => item.MemberName == "PartsPurchaseOrderCode").Value;
                case "oldTraceCode":
                    return filterItem.Filters.SingleOrDefault(item => item.MemberName == "OldTraceCode").Value;
                case "newTraceCode":
                    return filterItem.Filters.SingleOrDefault(item => item.MemberName == "NewTraceCode").Value;
                case "shippingCode":              
                    return filterItem.Filters.SingleOrDefault(item => item.MemberName == "ShippingCode").Value;
                case "status":
                    return filterItem.Filters.SingleOrDefault(item => item.MemberName == "Status").Value;
                case "code":
                    return filterItem.Filters.SingleOrDefault(item => item.MemberName == "Code").Value;
                default:
                    return null;
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                foreach(var item in compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).Where(filter => filter.MemberName != "PartsSupplierCode" && filter.MemberName != "PartsSupplierName" && filter.MemberName != "SparePartCode" && filter.MemberName != "PartsPurchaseOrderCode" && filter.MemberName != "OldTraceCode" && filter.MemberName != "NewTraceCode" && filter.MemberName != "ShippingCode"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override string OnRequestQueryName() {
            return "GetSupplierTraceCodeOrderByTime";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                       "SupplierTraceCodeDetail"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }
    }
}
