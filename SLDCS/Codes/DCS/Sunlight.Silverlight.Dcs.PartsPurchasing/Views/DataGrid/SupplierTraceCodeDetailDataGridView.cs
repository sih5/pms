﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using Telerik.Windows.Data;
using Sunlight.Silverlight.Dcs.Web.Entities;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class SupplierTraceCodeDetailDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "TraceProperty"
        };
        private int supplierTraceCodeId;
        public SupplierTraceCodeDetailDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += this.SupplierShippingDetailForDetailDataGridView_DataContextChanged;
        }

        private void SupplierShippingDetailForDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var supplierTraceCode = e.NewValue as SupplierTraceCode;
            if(supplierTraceCode == null || supplierTraceCode.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "SupplierTraceCodeId",
                MemberType = typeof(int),
                Operator = Sunlight.Silverlight.Core.Model.FilterOperator.IsEqualTo,
                Value = supplierTraceCode.Id
            });
            this.FilterItem = compositeFilterItem;
            this.supplierTraceCodeId = supplierTraceCode.Id;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new ColumnItem {
                       Name = "SparePartCode",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_SparePartCode
                    },new ColumnItem {
                       Name = "SparePartName",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_SparePartName
                    },new ColumnItem {
                       Name = "PartsSupplierCode",
                       Title = "供应商编号"
                    },new ColumnItem {
                       Name = "PartsSupplierName",
                       Title = "供应商名称"
                    },new ColumnItem {
                       Name = "PartsPurchaseOrderCode",
                       Title = "采购订单编号"
                    },new ColumnItem {
                       Name = "ShippingCode",
                       Title = "发运单编号"
                    },new KeyValuesColumnItem {
                       Name = "TraceProperty",
                       Title = "追溯属性",
                         KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                       Name = "OldTraceCode",                     
                       Title="原追溯码"
                    },new ColumnItem {
                       Name = "NewTraceCode",
                       Title = "新追溯码"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SupplierTraceCodeDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
        }

        protected override string OnRequestQueryName() {
            return "GetSupplierTraceCodeDetails";
        }

        protected override bool UsePaging {
            get {
                return true;
            }

        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
    }
}
