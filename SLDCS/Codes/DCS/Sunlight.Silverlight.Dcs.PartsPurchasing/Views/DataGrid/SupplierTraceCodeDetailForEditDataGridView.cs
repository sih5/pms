﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class SupplierTraceCodeDetailForEditDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "TraceProperty"
        };
        public SupplierTraceCodeDetailForEditDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                       Name = "SparePartCode",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_SparePartCode,
                       IsReadOnly=true
                    },new ColumnItem {
                       Name = "SparePartName",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_SparePartName,
                       IsReadOnly=true
                    },new ColumnItem {
                       Name = "PartsSupplierCode",
                       Title = "供应商编号",
                       IsReadOnly=true
                    },new ColumnItem {
                       Name = "PartsSupplierName",
                       Title = "供应商名称",
                       IsReadOnly=true
                    },new ColumnItem {
                       Name = "PartsPurchaseOrderCode",
                       Title = "采购订单编号",
                       IsReadOnly=true
                    },new ColumnItem {
                       Name = "ShippingCode",
                       Title = "发运单编号",
                       IsReadOnly=true
                    },new KeyValuesColumnItem {
                       Name = "TraceProperty",
                       Title = "追溯属性",
                         KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                       IsReadOnly=true
                    },new ColumnItem {
                       Name = "OldTraceCode",                     
                       Title="原追溯码",
                       IsReadOnly=true
                    },new ColumnItem {
                       Name = "NewTraceCode",
                       Title = "新追溯码"
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SupplierTraceCodeDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.ValidatesOnDataErrors = GridViewValidationMode.InViewMode;
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("SupplierTraceCodeDetails");
        }
    }
}