﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class SupplierTraceCodeForAddDataGridView  : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
             "TraceProperty"
        };
        public SupplierTraceCodeForAddDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }

        protected override Binding OnRequestDataSourceBinding()
        {
            return new Binding("PackingTasks");
        }


        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = "包装任务单号",
                        Name = "PackingCode",
                        IsReadOnly=true
                    }, new ColumnItem {
                        Title = "配件编号",
                        Name = "SparePartCode",
                        IsReadOnly=true
                    }, new ColumnItem {
                        Title ="配件名称",
                        Name = "SparePartName",
                        IsReadOnly=true
                    }, new KeyValuesColumnItem {
                        Title = "追溯属性",
                        Name = "TraceProperty",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        IsReadOnly=true
                    },new ColumnItem {
                        Title = "采购订单编号",
                        Name = "PartsPurchaseOrderCode",
                        IsReadOnly=true
                    }, new ColumnItem {
                        Title = "发运单号",
                        Name = "ShippingCode",
                        IsReadOnly=true
                    },new ColumnItem{
                        Name = "PartsSupplierCode",
                        Title = "供应商编号",
                        IsReadOnly=true                      
                    }, new ColumnItem {
                        Title = "供应商名称",
                        Name = "PartsSupplierName",
                        IsReadOnly=true
                    }, new ColumnItem {
                        Title = "原追溯码",
                        Name = "OldTraceCode",
                        IsReadOnly=true
                    }, new ColumnItem {
                        Title = "新追溯码",
                        Name = "NewTraceCode"
                    }
                };
            }
        }

        protected override bool UsePaging
        {
            get
            {
                return true;
            }

        }
        protected override bool ShowCheckBox
        {
            get
            {
                return true;
            }
        }

        protected override Type EntityType
        {
            get
            {
                return typeof(SupplierTraceCodeDetail);
            }
        }
    }
}