﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class TemPartsPurchaseOrderDetailForConfirmDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsPurchaseOrderDetail_ShortSupReason"
        };

        public TemPartsPurchaseOrderDetailForConfirmDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_SupplierPartCode,
                        Name = "SupplierPartCode",
                        IsReadOnly = true
                    },                   
                    new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsPurchasingUIStrings.DataEditView_Text_SparePartCode,
                        IsReadOnly = true
                    },                   
                    new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsPurchasingUIStrings.DataEditView_Text_SparePartName,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "PlanAmount",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_SupplierShippingDetail_PlanAmount,
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "PendingConfirmedAmount",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_PendingConfirmedAmount,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ConfirmedAmount",
                          Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingDetail_ConfirmedAmount,
                    }, new KeyValuesColumnItem {
                        Name = "ShortSupReason",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_ShortSupReason,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "MeasureUnit",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_MeasureUnit,
                        IsReadOnly = true
                    },                  
                    new ColumnItem {
                        Name = "Remark",
                        Title = PartsPurchasingUIStrings.DataEditView_ImportTemplate_DetailRemark,
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(TemPurchaseOrderDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.IsReadOnly = false;
            this.GridView.CanUserDeleteRows = false;
            this.GridView.CanUserInsertRows = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.BeginningEdit += GridView_BeginningEdit;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
          
        }
       private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e)
        {
            var detail = e.Row.DataContext as TemPurchaseOrderDetail;
            if (detail == null)
                return;
           //当确认量= 待确认量，短供原因不能编辑。
            switch (e.Cell.DataColumn.DataMemberBinding.Path.Path)
            {
                case "ShortSupReason":
                    if (detail.ConfirmedAmount >= detail.PendingConfirmedAmount) { 
                        e.Cancel = true;
                    }
                    break;
            }
        }
       private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
           if (!e.Cell.Column.UniqueName.Equals("ConfirmedAmount"))
               return;
           var detail = e.Cell.DataContext as PartsPurchaseOrderDetail;
           if (detail == null)
               return;
           if (detail.ConfirmedAmount >= detail.OrderAmount) {
               detail.ShortSupReason = null;
           }
       }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("TemPurchaseOrderDetails");
        }

    }
}
