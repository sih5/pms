﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class TemPartsPurchaseOrderKanBanDetailDataGridView: DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsPurchasingUIStrings.DataEditView_Text_SparePartCode,
                    }, new ColumnItem {
                        Name = "SparePartName",
                       Title=PartsPurchasingUIStrings.DataEditView_Text_SparePartName
                    }, new ColumnItem {
                        Name = "ConfirmedAmount",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_ConfirmedAmount
                    }, new ColumnItem {
                        Name = "ShippingAmount",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingDetail_Quantitys
                    },new ColumnItem {
                        Name = "ReceiptAmount",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBillDetail_ReceiptAmount
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(TemPurchaseOrderDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询临时采购订单看板";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var partsPurchaseOrder = this.DataContext as TemPurchaseOrder;
            if(partsPurchaseOrder == null)
                return null;

            switch(parameterName) {
                case "temPurchaseOrderId":
                    return partsPurchaseOrder.Id;                
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        private void PartsPurchaseOrderKanBanDetailDataGridView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            this.ExecuteQueryDelayed();
        }

        public TemPartsPurchaseOrderKanBanDetailDataGridView() {
            this.DataContextChanged += this.PartsPurchaseOrderKanBanDetailDataGridView_DataContextChanged;
        }
    }
}
