﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;
namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class TemPartsPurchaseOrderReplaceShippingOrderDetailDataGridView : DcsDataGridViewBase
    {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    
                    new ColumnItem {
                        Name = "TemPurchaseOrderCode",
                        Title = PartsPurchasingUIStrings.DataGridView_TemPurchaseOrder_ColumnItem_Code,
                        IsReadOnly = true
                    },                   
                    new ColumnItem {
                        Name = "SupplierPartCode",
                        Title = PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_SupplierPartCode,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_SparePartName,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "MeasureUnit",
                        Title = PartsPurchasingUIStrings.DataEditView_ImportTemplate_MeasureUnit,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "PlanAmount",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingDetail_ConfirmedAmount,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_ShippingAmount,
                        Name = "ShippingAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_UnShippingAmount,
                        Name = "UnShippingAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }                    
                };
            }
        }

        protected override Type EntityType
        {
            get
            {
                return typeof(TemPurchaseOrderDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding()
        {
            return new Binding("LartsPurchaseOrderDetails");
        }

        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            ((GridViewDataColumn)this.GridView.Columns["PlanAmount"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ShippingAmount"]).DataFormatString = "d";
            this.GridView.RowStyleSelector = this.GridViewStyleSelector();
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;//分页显示
            this.GridView.SelectionMode = SelectionMode.Multiple;
            //this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            //this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.BeginningEdit += GridView_BeginningEdit;
            this.DataPager.PageSize = 15;
        }

        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e)
        {
            //var dataEdit = this.DataContext as PartsPurchaseOrderReplaceShipAllDataEditView;
            //var partsPurchaseOrder = dataEdit.DataContext as PartsPurchaseOrder;
            //if (partsPurchaseOrder == null)
            //    return;
            if (e.Cell.Column.UniqueName.Equals("UnShippingAmount"))
            {
                var partsPurchaseOrderDetail = e.Row.DataContext as TemPurchaseOrderDetail;
                if (partsPurchaseOrderDetail == null)
                {
                    e.Cancel = true;
                    return;
                }
            }
        }
        private GridViewStyleSelector GridViewStyleSelector()
        {
            return new GridViewStyleSelector
            {
                ExecuteFunc = (item, container) =>
                {
                    if (item is PartsPurchaseOrderDetail)
                    {
                        var partsPurchaseOrderDetail = item as TemPurchaseOrderDetail;
                        if (partsPurchaseOrderDetail.UnShippingAmount > partsPurchaseOrderDetail.PlanAmount - partsPurchaseOrderDetail.ShippingAmount)
                        {
                            return this.Resources["BlueDataBackground"] as Style;
                        }
                        else
                        {
                            return this.Resources["WhiteDataBackground"] as Style;
                        }

                    }
                    return null;
                }
            };
        }

        private void GridView_RowLoaded(object sender, RowLoadedEventArgs e)
        {
            var partsPurchaseOrderDetail = e.Row.DataContext as TemPurchaseOrderDetail;
            if (partsPurchaseOrderDetail == null)
                return;
            GridViewRow girdViewRow = this.GridView.GetRowForItem(partsPurchaseOrderDetail);
            if (girdViewRow == null)
                return;
            switch (partsPurchaseOrderDetail.UnShippingAmount > partsPurchaseOrderDetail.PlanAmount - partsPurchaseOrderDetail.ShippingAmount)
            {
                case false:
                    girdViewRow.Background = new SolidColorBrush(Colors.White);
                    break;
                case true:
                    girdViewRow.Background = new SolidColorBrush(Color.FromArgb(255, 199, 237, 204));
                    break;
            }
        }

        protected override bool ShowCheckBox
        {
            get
            {
                return true;
            }
        }

        protected override bool UsePaging
        {
            get
            {
                return false;
            }
        }
    }
}