﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System;
using System.Collections.Generic;
using System.Windows;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class TemPurchaseOrderDetailDataGridView: DcsDataGridViewBase {
        protected readonly string[] KvNames = {
            "PartsPurchaseOrderDetail_ShortSupReason"
        };
        public TemPurchaseOrderDetailDataGridView() {
            this.DataContextChanged += this.PartsPurchaseOrderDetailDataGridView_DataContextChanged;
            this.KeyValueManager.Register(this.KvNames);
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
        }

        private void PartsPurchaseOrderDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsPurchaseOrder = e.NewValue as TemPurchaseOrder;
            if(partsPurchaseOrder == null || partsPurchaseOrder.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "TemPurchaseOrderId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsPurchaseOrder.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override string OnRequestQueryName() {
            return "GetTemPurchaseOrderDetails";
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsPurchasingUIStrings.DataEditView_Text_SparePartCode,
                    },new ColumnItem {
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_SupplierPartCode,
                        Name = "SupplierPartCode"
                    },new ColumnItem {
                        Name = "SparePartName",
                        Title=PartsPurchasingUIStrings.DataEditView_Text_SparePartName
                    },new ColumnItem {
                        Name = "MeasureUnit",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_MeasureUnit
                    },new ColumnItem {
                        Name = "PlanAmount",
                        Title=PartsPurchasingUIStrings.DataEditView_ImportTemplate_PlanAmount
                    },new ColumnItem {
                        Name = "ConfirmedAmount",
                         Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingDetail_ConfirmedAmount
                    }, new ColumnItem {
                        Name = "ShippingAmount",
                         Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingDetail_Quantity
                    },new KeyValuesColumnItem {
                        Name = "ShortSupReason",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_ShortSupReason,
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]]
                    }
                    ,new ColumnItem {
                        Name = "Remark",
                        Title=PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_Remark
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(TemPurchaseOrderDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
    }
}
