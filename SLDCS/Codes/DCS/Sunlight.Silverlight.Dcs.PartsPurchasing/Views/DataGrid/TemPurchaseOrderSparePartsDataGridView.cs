﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid
{
    public class TemPurchaseOrderSparePartsDataGridView: DcsDataGridViewBase {
       
        protected override Type EntityType {
            get {
                return typeof(TemPurchaseOrderSpareParts);
            }
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        Title=PartsPurchasingUIStrings.DataGridView_TemPurchaseOrder_ColumnItem_Code
                    },new ColumnItem {
                        IsSortable = false,
                        Name = "SparePartCode",
                        Title = PartsPurchasingUIStrings.DataGridView_Title_PartNotConPurchasePlan_PartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title= PartsPurchasingUIStrings.DataGridView_Title_PartNotConPurchasePlan_PartName
                    }, new ColumnItem {
                        Name = "SuplierCode",
                        Title= PartsPurchasingUIStrings.QueryPanel_QueryItem_SupplierCode
                    }, new ColumnItem {
                        Name = "SuplierName",
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_SupplierName
                    },new ColumnItem {
                        Name = "PurchasePrice",
                        Title= PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePricing_PurchasePrice
                    },new ColumnItem {
                        Name = "CenterPrice",
                        Title= PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_TemPurchasePlanOrder_CenterPrice
                    }, new ColumnItem {
                        Name = "SalesPrice",
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_TemPurchasePlanOrder_DelaerPrice
                    }
                };
            }
        }


        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filter = this.FilterItem as CompositeFilterItem;
            if(filter == null)
                return null;
            FilterItem filterItem;
            switch(parameterName) {
                case "sparePartCode":
                    filterItem = filter.Filters.SingleOrDefault(item => item.MemberName == "SparePartCode");
                    return filterItem == null ? null : filterItem.Value;
                case "code":
                    filterItem = filter.Filters.SingleOrDefault(item => item.MemberName == "Code");
                    return filterItem == null ? null : filterItem.Value;
                case "suplierCode":
                    filterItem = filter.Filters.SingleOrDefault(item => item.MemberName == "SuplierCode");
                    return filterItem == null ? null : filterItem.Value;
                case "suplierName":
                    filterItem = filter.Filters.SingleOrDefault(item => item.MemberName == "SuplierName");
                    return filterItem == null ? null : filterItem.Value;
                case "isPurchase":
                    filterItem = filter.Filters.SingleOrDefault(item => item.MemberName == "IsPurchase");
                    return filterItem == null ? null : filterItem.Value;
                case "isSales":
                    filterItem = filter.Filters.SingleOrDefault(item => item.MemberName == "IsSales");
                    return filterItem == null ? null : filterItem.Value;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override string OnRequestQueryName() {
            return "getTemPurchaseOrderSpareParts";
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["PurchasePrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["CenterPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SalesPrice"]).DataFormatString = "c2";
        }
    }
}