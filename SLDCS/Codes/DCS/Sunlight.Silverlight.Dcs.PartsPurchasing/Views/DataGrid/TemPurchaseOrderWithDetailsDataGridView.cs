﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Media;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System.Collections.ObjectModel;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class TemPurchaseOrderWithDetailsDataGridView : DcsDataGridViewBase {

        protected readonly string[] KvNames = {
            "TemPurchaseOrderStatus","TemPurchaseOrderApproveStatus","TemPurchasePlanOrderPlanType","TemPurchaseOrderReceiveStatus"
        };
        public TemPurchaseOrderWithDetailsDataGridView() {
            this.KeyValueManager.Register(this.KvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {

            get {
                return new[] {
                        new KeyValuesColumnItem {
                            Name = "Status",
                            KeyValueItems = this.KeyValueManager[this.KvNames[0]],
                            Title =  PartsPurchasingUIStrings.DataGridView_ColumnItem_OrderStatus
                        },new KeyValuesColumnItem {
                            Name = "ApproveStatus",
                            KeyValueItems = this.KeyValueManager[this.KvNames[1]],
                            Title =  PartsPurchasingUIStrings.DataGridView_TemPurchaseOrder_ColumnItem_ApproveStatus
                        }, new ColumnItem {
                            Name = "Code",
                            Title =  PartsPurchasingUIStrings.DataGridView_TemPurchaseOrder_ColumnItem_Code
                        }, new ColumnItem {
                            Name = "WarehouseName",
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchaseOrder_WarehouseId
                        }, new ColumnItem {
                            Name = "SuplierCode",
                            Title =  PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_PartsSupplierCode
                        }, new ColumnItem {
                            Name = "SuplierName",
                            Title = PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_PartsSupplierName
                        },new KeyValuesColumnItem {
                            Name = "OrderType",
                            KeyValueItems = this.KeyValueManager[this.KvNames[2]],
                            Title =  PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_PurchasePlanType
                        }, new ColumnItem {
                            Name = "IsTurnSale",
                            Title= PartsPurchasingUIStrings.DataGridView_TemPurchasePlanOrder_IsTurnSale
                        },new ColumnItem {
                            Name = "OrderCompanyCode",
                            Title=PartsPurchasingUIStrings.DataGridView_SupplierShippingOrder_OrderCompanyCode
                        },new ColumnItem {
                            Name = "OrderCompanyName",
                            Title=PartsPurchasingUIStrings.DataGridView_SupplierShippingOrder_OrderCompanyName
                        },new ColumnItem {
                            Name = "ReceCompanyCode",
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_emPurchasePlanOrder_ReceCompanyCode
                        },new ColumnItem {
                            Name = "ReceCompanyName",
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_emPurchasePlanOrder_ReceCompanyName
                        },new KeyValuesColumnItem {
                            Name = "ReceiveStatus",
                            KeyValueItems = this.KeyValueManager[this.KvNames[3]],
                            Title =  PartsPurchasingUIStrings.DataGridView_TemPurchaseOrder_ReceiveStatus
                        },new ColumnItem {
                            Name = "ReceiveAddress",
                            Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_ReceivingAddress
                        },new ColumnItem {
                            Name = "TemPurchasePlanOrderCode",
                            Title=PartsPurchasingUIStrings.DataGridView_TemPurchaseOrder_TemPlanCode
                        },new ColumnItem {
                            Name = "PartsSalesOrderCode",
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrder_Code
                        },new ColumnItem {
                            Name = "PartsPurchaseOrderCode",
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_TitlePartsPurReturnOrder_Code
                        },new ColumnItem {
                            Name = "Memo",
                            Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Remark
                        },new ColumnItem {
                            Name = "RejectReason",
                            Title="驳回原因"
                        },new ColumnItem {
                            Name = "Buyer",
                            Title=PartsPurchasingUIStrings.DataGridView_TitlePartsPurReturnOrder_ColumnItem_Buyer
                        }, new ColumnItem {
                            Name = "ApproverName",
                            Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_Checker
                        },new ColumnItem {
                            Name = "ApproveTime",
                            Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_CheckTime
                        },new ColumnItem {
                            Name = "CheckerName",
                            Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Approver
                        }, new ColumnItem {
                            Name = "CheckTime",
                            Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_ApproveTime
                        }, new ColumnItem {
                            Name = "CreatorName",
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_CreatorName
                        }, new ColumnItem {
                            Name = "CreateTime",
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_CreateTime
                        }, new ColumnItem {
                            Name = "ModifierName",
                            Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Modifier
                        }, new ColumnItem {
                            Name = "ModifyTime",
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_ModifyTime
                        }, new ColumnItem {
                            Name = "SubmitterName",
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_SubmitterName
                        }, new ColumnItem {
                            Name = "SubmitTime",
                            Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_SubmitTime
                        }, new ColumnItem {
                            Name = "Confirmer",
                            Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_InternalAllocationBill_ApproverName
                        },new ColumnItem {
                            Name = "ConfirmeTime",
                            Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_InternalAllocationBill_ApproveTime
                        }, new ColumnItem {
                            Name = "Shipper",
                            Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_TemPurchaseOrder_Shipper
                        },new ColumnItem {
                            Name = "ShippTime",
                            Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_TemPurchaseOrder_ShippTime
                        }, new ColumnItem {
                            Name = "Stopper",
                            Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_StoporName
                        }, new ColumnItem {
                            Name = "StopReason",
                            Title="终止原因"
                        },new ColumnItem {
                            Name = "StopTime",
                            Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_StopTime
                        }, new ColumnItem {
                            Name = "Forcedder",
                            Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_TemPurchaseOrder_StoporName
                        },new ColumnItem {
                            Name = "ForcedTime",
                            Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_TemPurchaseOrder_StopTime
                        }, new ColumnItem {
                            Name = "ForcedReason",
                            Title="强制完成原因"
                        }
                    };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(TemPurchaseOrder);
            }
        }

        protected override Core.IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "TemPurchaseOrder", "TemPurchaseOrderDetail", "TemSupplierShippingOrderForPartsPurchaseOrder", "TemPartsPurchaseOrderKanBanDetail"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return base.OnRequestFilterDescriptor(queryName);
            var newCompositeFilterItem = new CompositeFilterItem();
            foreach(var filter in compositeFilterItem.Filters.Where(item => item.MemberName != "SparePartCode"))
                newCompositeFilterItem.Filters.Add(filter);
            return newCompositeFilterItem.ToFilterDescriptor();
        }


        protected override string OnRequestQueryName() {
            return "GetTemPurchaseOrderForMan";
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "isSaleCode":
                        var businessCode = filters.Filters.SingleOrDefault(item => item.MemberName == "IsSaleCode");
                        return businessCode == null ? null : businessCode.Value;
                    case "isPurchaseOrder":
                        var ioStatus = filters.Filters.SingleOrDefault(item => item.MemberName == "IsPurchaseOrder");
                        return ioStatus == null ? null : ioStatus.Value;
                    case "sparePartCode":
                        var sparePartCode = filters.Filters.SingleOrDefault(item => item.MemberName == "SparePartCode");
                        return sparePartCode == null ? null : sparePartCode.Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
    }
}
