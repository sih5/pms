﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;


namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class TemPurchasePlanOrderDetailForEditDataGridView : DcsDataGridViewBase {
        private DcsMultiPopupsQueryWindowBase sparePartForPartsPurchaseOrderDropDownQueryWindow;
        private RadWindow radQueryWindow;
        private readonly string[] kvNames = {
             "TemPurchasePlanOrderDetailCodeSource"
        };
        public TemPurchasePlanOrderDetailForEditDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = this.SparePartForPartsPurchaseOrderDropDownQueryWindow,
                    Header = PartsPurchasingUIStrings.QueryPanel_Title_SparePartForPartsPurchaseOrder,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen
                });
            }
        }

        private DcsMultiPopupsQueryWindowBase SparePartForPartsPurchaseOrderDropDownQueryWindow {
            get {
                if(this.sparePartForPartsPurchaseOrderDropDownQueryWindow == null) {
                    this.sparePartForPartsPurchaseOrderDropDownQueryWindow = DI.GetQueryWindow("SparePartTepPlan") as DcsMultiPopupsQueryWindowBase;
                    this.sparePartForPartsPurchaseOrderDropDownQueryWindow.SelectionDecided += this.SparePartForPartsPurchaseOrderDropDownQueryWindow_SelectionDecided;
                }
                return this.sparePartForPartsPurchaseOrderDropDownQueryWindow;
            }
        }

        private void SparePartForPartsPurchaseOrderDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var spareParts = queryWindow.SelectedEntities.Cast<SparePart>();
            if(spareParts == null)
                return;
            var temPurchasePlanOrder = this.DataContext as TemPurchasePlanOrder;
            if(temPurchasePlanOrder == null)
                return;
            if(temPurchasePlanOrder.TemPurchasePlanOrderDetails.Any(r => spareParts.Any(ex => ex.Code == r.SparePartCode))) {
                UIHelper.ShowNotification(string.Format(PartsPurchasingUIStrings.DataEditView_Error_SparePart_DoubleSparePartID));
                return;
            }
            foreach(var sparePart in spareParts) {
                var temPurchasePlanOrderDetail = new TemPurchasePlanOrderDetail();
                temPurchasePlanOrderDetail.SparePartId = sparePart.Id;
                temPurchasePlanOrderDetail.SparePartCode = sparePart.Code;
                temPurchasePlanOrderDetail.SparePartName = sparePart.Name;
                temPurchasePlanOrderDetail.MeasureUnit = sparePart.MeasureUnit;
                temPurchasePlanOrderDetail.ReferenceCode = sparePart.ReferenceCode;
                temPurchasePlanOrderDetail.CodeSource = (int)DCSTemPurchasePlanOrderDetailCodeSource.SPM;
                if(sparePart.Code.StartsWith("W") || sparePart.Code.StartsWith("w")) {
                    temPurchasePlanOrderDetail.SupplierPartCode = sparePart.ReferenceCode;
                } else {
                    temPurchasePlanOrderDetail.SupplierPartCode = sparePart.Code;
                }
                temPurchasePlanOrder.TemPurchasePlanOrderDetails.Add(temPurchasePlanOrderDetail);
            }
            this.GridView.IsReadOnly = false;           
        }


        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true,
                        Title=PartsPurchasingUIStrings.DataGridView_Title_PartNotConPurchasePlan_PartCode
                    },  new ColumnItem {
                        Name = "ReferenceCode",
                        IsReadOnly = true,
                        Title= PartsPurchasingUIStrings.QueryPanel_QueryItem_ReferenceCode
                    },new ColumnItem {
                        Name = "SupplierPartCode",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_SupplierPartCode,
                        IsReadOnly = true,
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true,
                        Title=PartsPurchasingUIStrings.DataGridView_Title_PartNotConPurchasePlan_PartName
                    }, new ColumnItem {
                        Name = "MeasureUnit",
                        IsReadOnly = true,
                        Title = PartsPurchasingUIStrings.DataEditView_ImportTemplate_MeasureUnit
                    },
                    new ColumnItem {
                        Name = "PlanAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title=PartsPurchasingUIStrings.DataEditView_ImportTemplate_PlanAmount
                    }, new KeyValuesColumnItem {
                        Name = "CodeSource",
                         KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_CodeSource,
                        IsReadOnly = true,
                    }, new ColumnItem {
                        Name = "SuplierCode",
                        IsReadOnly = true,
                        Title=PartsPurchasingUIStrings.DataEditView_ImportTemplate_SupplierCode
                    }, new ColumnItem {
                        Name = "SuplierName",
                        IsReadOnly = true,
                        Title=PartsPurchasingUIStrings.DataEditView_ImportTemplate_SupplierName
                    }, new ColumnItem {
                        Name = "Memo",
                        Title=PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_Remark
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(TemPurchasePlanOrderDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DataContextChanged += PartsPurchaseOrderDetailForEditDataGridView_DataContextChanged;
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.BeginningEdit += GridView_BeginningEdit;
            this.GridView.ValidatesOnDataErrors = GridViewValidationMode.InViewMode;
            this.GridView.Deleting += this.GridView_Deleting;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
            this.GridView.CellValidating += GridView_CellValidating;
        }

        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            var temPurchasePlanOrder = this.DataContext as TemPurchasePlanOrder;
            if(temPurchasePlanOrder == null)
                return; 
        }

        private void PartsPurchaseOrderDetailForEditDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var temPurchasePlanOrder = this.DataContext as TemPurchasePlanOrder;
            if(temPurchasePlanOrder == null)
                return; 
            this.GridView.CanUserDeleteRows = true;
            this.GridView.CanUserInsertRows = true;
        }

        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
            switch(e.Cell.DataColumn.UniqueName) {
                case "PlanAmount":
                    if(!(e.NewValue is int) || (int)e.NewValue <= 0) {
                        e.IsValid = false;
                        e.ErrorMessage = PartsPurchasingUIStrings.DataEditView_Validation_PartsInboundCheckBillDetail_InspectedQuantity_IsOrderAmount;
                    } 
                    break;
            }
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            var temPurchasePlanOrder = this.DataContext as TemPurchasePlanOrder;
            if(temPurchasePlanOrder == null)
                return;         
            this.RadQueryWindow.ShowDialog();
        }

        private void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            var temPurchasePlanOrder = this.DataContext as TemPurchasePlanOrder;
            if(temPurchasePlanOrder == null)
                return;
            
        }
        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("TemPurchasePlanOrderDetails");
        }
    }
}