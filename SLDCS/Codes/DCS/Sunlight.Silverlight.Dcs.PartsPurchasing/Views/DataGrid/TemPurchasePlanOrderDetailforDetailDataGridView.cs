﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Views.QueryWindow;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class TemPurchasePlanOrderDetailforDetailDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
             "TemPurchasePlanOrderDetailCodeSource"
        };
        private QueryWindowBase partsSupplierDropDownQueryWindow;

        void partsSupplierDropDownQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as PartsSupplierByPartIdQueryWindow;
            var window = sender as QueryWindowBase;
            var queryid = window.DataContext as TemPurchasePlanOrderDetail;
            if(queryWindow == null)
                return;

            //var filterItem = new FilterItem();
            //filterItem.MemberName = "PartId";
            //filterItem.MemberType = typeof(int);
            //filterItem.Operator = FilterOperator.IsEqualTo;
            //filterItem.Value = queryid.SparePartId;
            //queryWindow.ExchangeData(null, "SetAdditionalFilterItem", filterItem);
        }

        private QueryWindowBase PartsSupplierDropDownQueryWindow {
            get {
                if(this.partsSupplierDropDownQueryWindow == null) {
                    this.partsSupplierDropDownQueryWindow = DI.GetQueryWindow("PartsSupplierByPartId");
                    this.partsSupplierDropDownQueryWindow.Loaded += partsSupplierDropDownQueryWindow_Loaded;
                    this.partsSupplierDropDownQueryWindow.SelectionDecided += this.PartsSupplierDropDownQueryWindow_SelectionDecided;
                }
                return this.partsSupplierDropDownQueryWindow;
            }
        }
        private void PartsSupplierDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var partsPurchasePricingChange = this.DataContext as TemPurchasePlanOrder;
            if(partsPurchasePricingChange == null) {
                return;
            }
            var partsSupplier = queryWindow.SelectedEntities.Cast<PartsSupplier>().FirstOrDefault();
            if(partsSupplier == null)
                return;

            var partsPurchasePricingDetail = queryWindow.DataContext as TemPurchasePlanOrderDetail;
            if(partsPurchasePricingDetail == null)
                return;
            partsPurchasePricingDetail.SuplierId = partsSupplier.Id;
            partsPurchasePricingDetail.SuplierCode = partsSupplier.Code;
            partsPurchasePricingDetail.SuplierName = partsSupplier.Name;


        }

        public TemPurchasePlanOrderDetailforDetailDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override Type EntityType {
            get {
                return typeof(TemPurchasePlanOrderDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.IsReadOnly = false;
        }


        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("TemPurchasePlanOrderDetails");
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true,
                        Title=PartsPurchasingUIStrings.DataEditView_Text_SparePartCode
                    }, new ColumnItem {
                        Name = "ReferenceCode",
                        IsReadOnly = true,
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_ReferenceCode
                    }, new ColumnItem {
                        Name = "SupplierPartCode",
                        IsReadOnly = true,
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_SupplierPartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true,
                        Title=PartsPurchasingUIStrings.DataEditView_Text_SparePartName
                    }, new ColumnItem {
                        Name = "MeasureUnit",
                        IsReadOnly = true,
                        Title=PartsPurchasingUIStrings.DataEditView_ImportTemplate_MeasureUnit
                    }, new ColumnItem {
                        Name = "PlanAmount",
                        Title=PartsPurchasingUIStrings.DataEditView_ImportTemplate_PlanAmount
                    }, new KeyValuesColumnItem {
                        Name = "CodeSource",
                         KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_CodeSource
                    }, new DropDownTextBoxColumnItem {
                        Name = "SuplierCode",
                        Title=PartsPurchasingUIStrings.DataEditView_ImportTemplate_SupplierCode,
                        DropDownContent = this.PartsSupplierDropDownQueryWindow,
                    }, new ColumnItem {
                        Name = "SuplierName",
                        Title=PartsPurchasingUIStrings.DataEditView_ImportTemplate_SupplierName,
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "Memo",
                        Title=PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_Remark,
                        IsReadOnly=true
                    }
                };
            }
        }
    }
}
