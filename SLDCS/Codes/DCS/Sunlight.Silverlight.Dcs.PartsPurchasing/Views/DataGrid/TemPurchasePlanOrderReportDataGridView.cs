﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;
using System.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class TemPurchasePlanOrderReportDataGridView  : DcsDataGridViewBase {
        private readonly string[] kvNames = {
             "TemPurchasePlanOrderStatus","TemPurchasePlanOrderPlanType","TemPurchasePlanOrderShippingMethod","TemPurchasePlanOrderFreightType"
        };
        public TemPurchasePlanOrderReportDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override Type EntityType {
            get {
                return typeof(TemPurchasePlanOrder);
            }
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_Status
                    },new ColumnItem {
                        IsSortable = false,
                        Name = "IsUplodFile",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_IsUplodFile
                    }, new ColumnItem {
                        Name = "Code",
                        Title= PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_Code
                    }, new ColumnItem {
                        Name = "IsTurnSale",
                        Title= PartsPurchasingUIStrings.DataGridView_TemPurchasePlanOrder_IsTurnSale
                    }, new KeyValuesColumnItem {
                        Name = "PlanType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title=PartsPurchasingUIStrings.DataGridView_TemPurchasePlanOrder_PlanType
                    },new ColumnItem {
                        Name = "ReceCompanyCode",
                        Title= PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_emPurchasePlanOrder_ReceCompanyCode
                    },new ColumnItem {
                        Name = "ReceCompanyName",
                        Title= PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_emPurchasePlanOrder_ReceCompanyName
                    },new ColumnItem {
                        Name = "WarehouseName",
                        Title= PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_SupplierShippingOrder_ReceivingWarehouseId
                    },new ColumnItem {
                        Name = "ReceiveAddress",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_ReceivingAddress
                    }, new ColumnItem {
                        Name = "Linker",
                        Title=PartsPurchasingUIStrings.DataEditPanel_Title_SupplierShippingOrder_ContactPerson
                    },new ColumnItem {
                        Name = "LinkPhone",
                        Title=PartsPurchasingUIStrings.DataEditPanel_Title_SupplierShippingOrder_ContactPhone
                    }
                    //,new ColumnItem {
                    //    Name = "OrderCompanyCode",
                    //    Title=PartsPurchasingUIStrings.DataGridView_SupplierShippingOrder_OrderCompanyCode
                    //},new ColumnItem {
                    //    Name = "OrderCompanyName",
                    //    Title=PartsPurchasingUIStrings.DataGridView_SupplierShippingOrder_OrderCompanyName
                    //}
                    , new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]],
                        Title=PartsPurchasingUIStrings.DataEditView_ImportTemplate_ShippingMethod
                    },new ColumnItem {
                        Name = "IsReplace",
                        Title=PartsPurchasingUIStrings.DataEditView_Title_TemPurchasePlanOrder_IsReplace
                    },new KeyValuesColumnItem {
                        Name = "FreightType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[3]],
                        Title=PartsPurchasingUIStrings.DataEditView_Title_TemPurchasePlanOrder_FreightType
                    },new ColumnItem {
                        Name = "Memo",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Remark
                    }, new ColumnItem {
                        Name = "CreatorName",
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_CreatorName
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_CreateTime
                    }, new ColumnItem {
                        Name = "ModifierName",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Modifier
                    }, new ColumnItem {
                        Name = "ModifyTime",
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_PartsPurchasePlan_HW_ModifyTime
                    }, new ColumnItem {
                        Name = "SubmitterName",
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_SubmitterName
                    }, new ColumnItem {
                        Name = "SubmitTime",
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_SubmitTime
                    }, new ColumnItem {
                        Name = "ApproverName",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_Checker
                    },new ColumnItem {
                        Name = "ApproveTime",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_CheckTime
                    },new ColumnItem {
                        Name = "CheckerName",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_CheckerName
                    }
                    , new ColumnItem {
                        Name = "CheckTime",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_CheckTime
                    }, new ColumnItem {
                        Name = "Confirmer",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_InternalAllocationBill_ApproverName
                    },new ColumnItem {
                        Name = "ConfirmeTime",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_InternalAllocationBill_ApproveTime
                    },new ColumnItem {
                        Name = "Rejector",
                        Title=PartsPurchasingUIStrings.DataEditView_Title_Rejector
                    }, new ColumnItem {
                        Name = "RejectTime",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_RejectTime
                    }
                    , new ColumnItem {
                        Name = "RejectReason",
                        Title=PartsPurchasingUIStrings.DataEditView_Title_RejectReason
                    }
                    , new ColumnItem {
                         Name = "AbandonerName",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_AbandonerName
                    }, new ColumnItem {
                        Name = "AbandonTime",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_AbandonTime
                    }
                };
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return base.OnRequestFilterDescriptor(queryName);
            var newCompositeFilterItem = new CompositeFilterItem();
            foreach(var filter in compositeFilterItem.Filters.Where(item => item.MemberName != "SparePartCode"))
                newCompositeFilterItem.Filters.Add(filter);
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filter = this.FilterItem as CompositeFilterItem;
            if(filter == null)
                return null;
            FilterItem filterItem;
            switch(parameterName) {
                case "sparePartCode":
                    filterItem = filter.Filters.SingleOrDefault(item => item.MemberName == "SparePartCode");
                    return filterItem == null ? null : filterItem.Value;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override string OnRequestQueryName() {
            return "GetTemPurchasePlanOrder";
        }
        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "TemPurchasePlanOrderDetails"
                    }
                };
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;          
        }
    }
}
