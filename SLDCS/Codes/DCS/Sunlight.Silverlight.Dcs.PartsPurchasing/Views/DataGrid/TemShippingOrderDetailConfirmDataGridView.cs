﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class TemShippingOrderDetailConfirmDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                  new ColumnItem {
                       Name = "SupplierPartCode",
                       IsReadOnly=true,
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierPartCode
                    },                  
                    new ColumnItem {
                       Name = "SparePartCode",
                        IsReadOnly=true,
                       Title=PartsPurchasingUIStrings.DataEditView_Text_SparePartCode
                    },                  
                    new ColumnItem {
                       Name = "SparePartName",
                        IsReadOnly=true,
                       Title=PartsPurchasingUIStrings.DataEditView_Text_SparePartName
                    },new ColumnItem {
                       Name = "MeasureUnit",
                        IsReadOnly=true,
                       Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_MeasureUnit
                    },new ColumnItem {
                       Name = "Quantity",
                        IsReadOnly=true,
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_Quantity
                    },new ColumnItem {
                       Name = "ConfirmedAmount",
                        IsReadOnly=true,
                       Title = "已确认量"
                    },new ColumnItem {
                       Name = "UnConfirmedAmount",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingDetail_ConfirmedAmount
                    },new ColumnItem {
                       Name = "Remark",
                        IsReadOnly=true,
                       Title=PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_Remark
                    },new ColumnItem {
                       Name = "Weight",
                        IsReadOnly=true,
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Weight
                    },new ColumnItem {
                       Name = "Volume",
                        IsReadOnly=true,
                       Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Volume
                    }
                };
            }
        }
        protected override Type EntityType {
            get {
                return typeof(TemShippingOrderDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("TemShippingOrderDetails");
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.IsReadOnly = false;
            this.GridView.CanUserDeleteRows = false;
            this.GridView.CanUserInsertRows = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
