﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class TemShippingOrderDetailDataGridView : DcsDataGridViewBase {
        public TemShippingOrderDetailDataGridView() {
            this.DataContextChanged += this.SupplierShippingDetailForDetailDataGridView_DataContextChanged;
        }

        private void SupplierShippingDetailForDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var supplierShippingOrder = e.NewValue as TemSupplierShippingOrder;
            if(supplierShippingOrder == null || supplierShippingOrder.Id == default(int))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "ShippingOrderId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                };
            this.FilterItem.Value = supplierShippingOrder.Id;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                  new ColumnItem {
                       Name = "SupplierPartCode",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierPartCode
                    },                  
                    new ColumnItem {
                       Name = "SparePartCode",
                       Title=PartsPurchasingUIStrings.DataEditView_Text_SparePartCode
                    },                  
                    new ColumnItem {
                       Name = "SparePartName",
                       Title=PartsPurchasingUIStrings.DataEditView_Text_SparePartName
                    },new ColumnItem {
                       Name = "MeasureUnit",
                       Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_MeasureUnit
                    },new ColumnItem {
                       Name = "Quantity",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_Quantity
                    },new ColumnItem {
                       Name = "ConfirmedAmount",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingDetail_ConfirmedAmount
                    },new ColumnItem {
                       Name = "Remark",
                       Title=PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_Remark
                    },new ColumnItem {
                       Name = "Weight",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Weight
                    },new ColumnItem {
                       Name = "Volume",
                       Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Volume
                    }
                };
            }
        }
        protected override Type EntityType {
            get {
                return typeof(TemShippingOrderDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetTemShippingOrderDetails";
        }
    }
}
