﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class TemSupplierShippingDetailForEditDataGridView : DcsDataGridViewBase {  
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                 new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsPurchasingUIStrings.DataEditView_Text_SparePartCode,
                    },new ColumnItem {
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_SupplierPartCode,
                        Name = "SupplierPartCode"
                    },new ColumnItem {
                        Name = "SparePartName",
                        Title=PartsPurchasingUIStrings.DataEditView_Text_SparePartName
                    },new ColumnItem {
                        Name = "MeasureUnit",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_MeasureUnit
                    }, new ColumnItem {
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingDetail_Quantity,
                        Name = "Quantity"
                    }, new ColumnItem {
                        Name = "PendingQuantity",
                        IsReadOnly = true,
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_SupplierShippingDetail_WaitShippingAmount,
                    }, new ColumnItem {
                        Name = "Remark",
                        Title=PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_Remark
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("TemShippingOrderDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.IsReadOnly = false;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.CanUserInsertRows = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override Type EntityType {
            get {
                return typeof(TemShippingOrderDetail);
            }
        }

        public object SelectedItem {
            get {
                return this.GridView.SelectedItem;
            }
        }

        public object MasterObject {
            get {
                return this.DataContext;
            }
        }

    }
}