﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class TemSupplierShippingDetailForSupplierDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "SparePart_MeasureUnit"
        };

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                      new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsPurchasingUIStrings.DataEditView_Text_SparePartCode,
                    },new ColumnItem {
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_SupplierPartCode,
                        Name = "SupplierPartCode"
                    },new ColumnItem {
                        Name = "SparePartName",
                        Title=PartsPurchasingUIStrings.DataEditView_Text_SparePartName
                    },new ColumnItem {
                        Name = "MeasureUnit",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_MeasureUnit
                    },new ColumnItem {
                        Name = "ConfirmedAmount",
                         Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingDetail_ConfirmedAmount
                    }, new ColumnItem {
                        Name = "ShippingAmount",
                         Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingDetail_Quantity
                    },new ColumnItem {
                        Name = "Weight",
                         Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_SupplierShippingDetail_Weight
                    }, new ColumnItem {
                        Name = "Volume",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Volume
                    },new ColumnItem {
                        Name = "Remark",
                       Title=PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_Remark
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(TemShippingOrderDetail);
            }
        }

        public TemSupplierShippingDetailForSupplierDataGridView() {
            this.DataContextChanged += this.SupplierShippingDetailForSupplierDataGridView_DataContextChanged;
            this.KeyValueManager.Register(this.kvNames);
        }

        private void SupplierShippingDetailForSupplierDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var supplierShippingOrder = e.NewValue as TemSupplierShippingOrder;
            if(supplierShippingOrder == null || supplierShippingOrder.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "ShippingOrderId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = supplierShippingOrder.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override string OnRequestQueryName() {
            return "GetTemShippingOrderDetails";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
        }
    }
}