﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class TemSupplierShippingOrderDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "TemSupplierShippingOrderStatus", "TemPurchasePlanOrderShippingMethod"
        };

        public TemSupplierShippingOrderDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Status
                    }, new ColumnItem {
                        Name = "Code",
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsShippingOrder_Code,
                    }, new ColumnItem {
                        Name = "ReceivingWarehouseName",
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Title_SupplierShippingOrder_ReceivingWarehouseId
                    }, new ColumnItem {
                        Name = "PartsSupplierCode",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_PartsSupplierCode
                    }, new ColumnItem {
                        Name = "PartsSupplierName",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetail_PartsSupplierName
                    }, new ColumnItem {
                        Name = "TemPurchaseOrderCode",
                          Title = PartsPurchasingUIStrings.DataGridView_TemPurchaseOrder_ColumnItem_Code
                    }, new ColumnItem {
                        Name = "ReceivingAddress",
                       Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_ReceivingAddress
                    }, new ColumnItem {
                        Name = "ReceivingCompanyName",
                        Title = PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingOrder_ReceivingCompanyName
                    }, new ColumnItem {
                        Name = "OriginalRequirementBillCode",
                         Title = "原始需求单号"
                    }, new ColumnItem {
                        Name = "DeliveryBillNumber",
                        Title=PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_DeliveryBillNumber
                    },new ColumnItem{
                        Name="PlanDeliveryTime",
                        Title=PartsPurchasingUIStrings.DataEditView_Title_PlanDeliveryTime
                    }, new ColumnItem {
                        Name = "ShippingDate",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_SupplierShippingOrder_ShippingDate
                    }, new ColumnItem {
                        Name = "ArrivalDate",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_ArrivalDate
                    }, new ColumnItem {
                        Name = "ConfirmorName",
                        Title="确认人"
                    }, new ColumnItem {
                        Name = "LogisticArrivalDate",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_LogisticArrivalDate
                    }, new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_ShippingMethod
                    }, new ColumnItem {
                        Name = "LogisticCompany",
                        Title=PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_LogisticCompany
                    }, new ColumnItem {
                        Name = "Driver",
                        Title=PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_Driver
                    }, new ColumnItem {
                        Name = "Phone",
                        Title=PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_Phone
                    }, new ColumnItem {
                        Name = "VehicleLicensePlate",
                        Title=PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_VehicleLicensePlate
                    }, new ColumnItem {
                        Name = "Remark",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Remark
                    }, new ColumnItem {
                        Name = "CreatorName",
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_CreatorName
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_PartsPurchasePlan_CreateTime
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetTemSupplierShippingOrders";
        }

        protected override Type EntityType {
            get {
                return typeof(TemSupplierShippingOrder);
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "TemSupplierShippingOrder", "TemShippingOrderDetail"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }

    }
}
