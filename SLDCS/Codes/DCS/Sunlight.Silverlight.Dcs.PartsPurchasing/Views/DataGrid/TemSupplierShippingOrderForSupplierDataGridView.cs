﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataGrid {
    public class TemSupplierShippingOrderForSupplierDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "TemSupplierShippingOrderStatus","SupplierShippingOrder_Status"
        };

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title=PartsPurchasingUIStrings.QueryPanel_QueryItem_Status
                    },new ColumnItem {
                        Name = "Code",
                         Title=PartsPurchasingUIStrings.DataGridView_TemSupplierShippingOrder_Title_Code
                    }, new ColumnItem {
                        Name = "TemPurchaseOrderCode",
                        Title=PartsPurchasingUIStrings.DataGridView_TemPurchaseOrder_ColumnItem_Code
                    }, new ColumnItem {
                        Name = "ReceivingAddress",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_ReceivingAddress
                    }, new ColumnItem {
                        Name = "DeliveryBillNumber",
                        Title=PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_DeliveryBillNumber
                    }, new ColumnItem {
                        Name = "ShippingDate",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_ShippingDate2
                    }, new ColumnItem {
                        Name = "ArrivalDate",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_ArrivalDate
                    }, new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Title_PartsPurchaseOrderDetailExtend_ShippingMethod
                    }, new ColumnItem {
                        Name = "LogisticCompany",
                        Title=PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_LogisticCompany
                    }, new ColumnItem {
                        Name = "Driver",
                        Title=PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_Driver
                    }, new ColumnItem {
                        Name = "Phone",
                        Title=PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_Phone
                    }, new ColumnItem {
                        Name = "VehicleLicensePlate",
                        Title=PartsPurchasingUIStrings.DataEditView_Title_PartsPurchaseOrder_VehicleLicensePlate
                    },  new ColumnItem {
                        Name = "Remark",
                        Title=PartsPurchasingUIStrings.DataGridView_ColumnItem_Remark
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(TemSupplierShippingOrder);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetTemSupplierShippingOrders";
        }

        public TemSupplierShippingOrderForSupplierDataGridView() {
            this.DataContextChanged += this.SupplierShippingOrderForSupplierDataGridView_DataContextChanged;
            this.KeyValueManager.Register(this.kvNames);
        }

        private void SupplierShippingOrderForSupplierDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsPurchaseOrder = e.NewValue as TemPurchaseOrder;
            if(partsPurchaseOrder == null || partsPurchaseOrder.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "TemPurchaseOrderId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsPurchaseOrder.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override Core.IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "SupplierShippingDetailForSupplier"
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
        }
    }
}
