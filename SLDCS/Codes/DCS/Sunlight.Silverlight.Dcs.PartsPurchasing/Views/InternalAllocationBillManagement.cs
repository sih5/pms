﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views {
    [PageMeta("PartsPurchasing", "InternalRecipient", "InternalAllocationBill", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_APPROVE_AUDIT_EXPORT_PRINT_MERGEEXPORT
    })]
    public class InternalAllocationBillManagement : DcsDataManagementViewBase {
        private DataEditViewBase dataEditView;
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataApproveView;
        private DataEditViewBase dataFirApproveView;
        private DataEditViewBase dataDetailView;
        private const string DATA_DETAIL_VIEW = "_DataDetailView_";
        private const string DATA_APPROVE_VIEW = "_dataApproveView_";
        private const string DATA_FIRAPPROVE_VIEW = "_dataFirApproveView_";
        public InternalAllocationBillManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsPurchasingUIStrings.DataManagementView_Title_InternalAllocationBill;
        }

        private DataGridViewBase DataGridView {
             get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("InternalAllocationBill");
                    this.dataGridView.RowDoubleClick += DataGridView_RowDoubleClick;
                }
                return this.dataGridView;
            }
        }
        private void DataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                this.DataEditViewDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                this.SwitchViewTo(DATA_DETAIL_VIEW);
            }
        }
        private DataEditViewBase DataEditViewDetail {
            get {
                if(this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("InternalAllocationBillDetail");
                    this.dataDetailView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataDetailView;
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("InternalAllocationBill");
                    this.dataEditView.EditSubmitted += dataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataApproveView {
            get {
                if(this.dataApproveView == null) {
                    this.dataApproveView = DI.GetDataEditView("InternalAllocationBillForApprove");
                    //this.dataApproveView.EditSubmitted += dataEditView_EditSubmitted;
                    //this.dataApproveView.EditCancelled += dataEditView_EditCancelled;
                    ((InternalAllocationBillForApproveDataEditView)this.dataApproveView).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataApproveView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataApproveView;
            }
        }

        private DataEditViewBase DataFirApproveView
        {
            get
            {
                if (this.dataFirApproveView == null)
                {
                    this.dataFirApproveView = DI.GetDataEditView("InternalAllocationBillForFirApprove");
                    //this.dataFirApproveView.EditSubmitted += dataEditView_EditSubmitted;
                    //this.dataFirApproveView.EditCancelled += dataEditView_EditCancelled;
                    ((InternalAllocationBillForFirApproveDataEditView)this.dataFirApproveView).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataFirApproveView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataFirApproveView;
            }
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataApproveView = null;
            this.dataFirApproveView = null;
        }
        private void dataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void dataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_APPROVE_VIEW, () => this.DataApproveView);
            this.RegisterView(DATA_FIRAPPROVE_VIEW, () => this.DataFirApproveView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataEditViewDetail);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "InternalAllocationBill"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var internalAllocationBill = this.DataEditView.CreateObjectToEdit<InternalAllocationBill>();
                    internalAllocationBill.Objid = Guid.NewGuid().ToString();
                    internalAllocationBill.BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    internalAllocationBill.BranchName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    internalAllocationBill.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    internalAllocationBill.Status = (int)DcsInternalAllocationBillStatus.新建;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<InternalAllocationBill>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废内部领出单)
                                entity.作废内部领出单();
                            this.ExecuteSerivcesMethod(PartsPurchasingUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.AUDIT:
                    this.DataFirApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_FIRAPPROVE_VIEW);
                    break;
                case CommonActionKeys.APPROVE:
                    this.DataApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_APPROVE_VIEW);
                    break;
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    //如果选中一条数据 导出参数为 内部领出单ID 
                    var domainContext = this.dataGridView.DomainContext as DcsDomainContext;
                    if(domainContext == null)
                        return;
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var id = this.DataGridView.SelectedEntities.Cast<InternalAllocationBill>().Select(r => r.Id).ToArray();
                        if(uniqueId.Equals(CommonActionKeys.EXPORT)) {
                            domainContext.ExportInternalAllocationBill(id, null, null, null, null, null, null,null, loadOp => {
                                if(loadOp.HasError)
                                    return;
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                    ShellViewModel.Current.IsBusy = false;
                                }
                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                    ShellViewModel.Current.IsBusy = false;
                                }
                            }, null);
                        } else {
                            this.ExportInternalallocationbillAndDetail(id, null, null, null, null, null, null, null,null);
                        }
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var departmentName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "DepartmentName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "DepartmentName").Value as string;
                        var warehouseId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "WarehouseId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        var type = filterItem.Filters.Single(e => e.MemberName == "Type").Value as int?;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        if(uniqueId.Equals(CommonActionKeys.EXPORT)) {
                            domainContext.ExportInternalAllocationBill(new int[] {
                            }, warehouseId, code, departmentName, status, createTimeBegin, createTimeEnd,type, loadOp => {
                                if(loadOp.HasError)
                                    return;
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                    ShellViewModel.Current.IsBusy = false;
                                }
                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                    ShellViewModel.Current.IsBusy = false;
                                }
                            }, null);
                        } else {
                            this.ExportInternalallocationbillAndDetail(new int[] { }, code, warehouseId, null, departmentName, status, createTimeBegin, createTimeEnd,type);
                        }
                    }
                    break;
                case CommonActionKeys.PRINT:
                    //TODO: 导入暂时不实现
                    break;
            }
        }

        //合并导出
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private void ExportInternalallocationbillAndDetail(int[] ids, string code, int? warehouseid, string creatorName, string departmentName, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd,int? type) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportInternalallocationbillAndDetailAsync(ids, code, warehouseid, creatorName, departmentName, status, createTimeBegin, createTimeEnd, type);
            this.excelServiceClient.ExportInternalallocationbillAndDetailCompleted -= excelServiceClient_ExportInternalallocationbillAndDetailCompleted;
            this.excelServiceClient.ExportInternalallocationbillAndDetailCompleted += excelServiceClient_ExportInternalallocationbillAndDetailCompleted;
        }

        private void excelServiceClient_ExportInternalallocationbillAndDetailCompleted(object sender, ExportInternalallocationbillAndDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }


        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var newCompositeFilter = new CompositeFilterItem();
            var compositeFilter = filterItem as CompositeFilterItem;
            if(compositeFilter == null)
                return;
            compositeFilter.Filters.Add(new FilterItem {
                MemberName = "BranchId",
                Operator = FilterOperator.IsEqualTo,
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            if(compositeFilter.Filters.Any(filter => filter.GetType() == typeof(CompositeFilterItem))) {
                var createTimeFilter = compositeFilter.Filters.Single(filter => filter.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                if(createTimeFilter == null)
                    return;
                var endCreateTimeFilter = createTimeFilter.Filters.ElementAt(1).Value as DateTime?;
                if(endCreateTimeFilter.HasValue)
                    createTimeFilter.Filters.ElementAt(1).Value = new DateTime(endCreateTimeFilter.Value.Year, endCreateTimeFilter.Value.Month, endCreateTimeFilter.Value.Day, 23, 59, 59);
            }
            newCompositeFilter.Filters.Add(compositeFilter);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;                         
                case CommonActionKeys.APPROVE:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<InternalAllocationBill>().Any(r => r.Status == (int)DcsInternalAllocationBillStatus.已初审);
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.SelectedEntities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT:
                    //TODO: 导入暂时不实现
                    return false;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.AUDIT:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<InternalAllocationBill>().Any(r => r.Status == (int)DcsInternalAllocationBillStatus.新建);
                default:
                    return false;
            }
        }
    }
}
