﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views {
    [PageMeta("PartsPurchasing", "PartsPurchaseSettle", "PartsPurchaseSettleInvoice", ActionPanelKeys = new[]{
        CommonActionKeys.EXPORT_PRINT
    })]
    public class InvoiceInformationQuery : DcsDataManagementViewBase {
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();

        private DataGridViewBase dataGridView;

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("InvoiceInformation"));
            }
        }
        public InvoiceInformationQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsPurchasingUIStrings.DataManagementView_Title_InvoiceInformation;
        }


        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                   "InvoiceInformation"
               };
            }
        }


        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            ClientVar.ConvertTime(compositeFilterItem);
            if(filterItem is CompositeFilterItem) {
                compositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "OwnerCompanyId",
                    Operator = FilterOperator.IsEqualTo,
                    MemberType = typeof(int),
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });

                var compositeFilters = new CompositeFilterItem();
                compositeFilters.LogicalOperator = LogicalOperator.Or;
                compositeFilters.Filters.Add(new FilterItem {
                    MemberName = "SourceType",
                    Operator = FilterOperator.IsEqualTo,
                    MemberType = typeof(int),
                    Value = (int)DcsInvoiceInformationSourceType.配件采购退货结算单
                });
                compositeFilters.Filters.Add(new FilterItem {
                    MemberName = "SourceType",
                    Operator = FilterOperator.IsEqualTo,
                    MemberType = typeof(int),
                    Value = (int)DcsInvoiceInformationSourceType.配件采购结算单
                });

                compositeFilterItem.Filters.Add(compositeFilters);
            }

            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<VirtualInvoiceInformation>().Select(r => r.Id).ToArray();
                        this.dcsDomainContext.ExportInvoiceInformationOfPurchase(ids, null, null, null, null, null, null, null, null, null, null, null, null, null, null, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                                ShellViewModel.Current.IsBusy = false;
                            }

                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                ShellViewModel.Current.IsBusy = false;
                            }
                        }, null);
                    } else {
                        var compositeFilterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(compositeFilterItem != null) {
                            var invoiceNumber = compositeFilterItem.Filters.Single(r => r.MemberName == "InvoiceNumber").Value as string;
                            var sourceCode = compositeFilterItem.Filters.Single(r => r.MemberName == "SourceCode").Value as string;
                            var invoiceCompanyCode = compositeFilterItem.Filters.Single(r => r.MemberName == "InvoiceCompanyCode").Value as string;
                            var invoiceCompanyName = compositeFilterItem.Filters.Single(r => r.MemberName == "InvoiceCompanyName").Value as string;
                            var invoiceReceiveCompanyId = compositeFilterItem.Filters.Single(r => r.MemberName == "InvoiceReceiveCompanyId").Value as int?;
                            var invoicePurpose = compositeFilterItem.Filters.Single(r => r.MemberName == "InvoicePurpose").Value as int?;
                            var type = compositeFilterItem.Filters.Single(r => r.MemberName == "Type").Value as int?;
                         //   var partssalesCategoryId = compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                            var status = compositeFilterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                            DateTime? createTimeStart = null;
                            DateTime? createTimeEnd = null;
                            DateTime? approveTimeStart = null;
                            DateTime? approveTimeEnd = null;
                            var dateTimeFilterItems = compositeFilterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem));
                            foreach(var dateTimeFilterItem in dateTimeFilterItems) {
                                var dateTime = dateTimeFilterItem as CompositeFilterItem;
                                if(dateTime != null) {
                                    if(dateTime.Filters.Any(r => r.MemberName == "CreateTime")) {
                                        createTimeStart = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                        createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    }
                                    if(dateTime.Filters.Any(r => r.MemberName == "ApproveTime")) {
                                        approveTimeStart = dateTime.Filters.First(r => r.MemberName == "ApproveTime").Value as DateTime?;
                                        approveTimeEnd = dateTime.Filters.Last(r => r.MemberName == "ApproveTime").Value as DateTime?;
                                    }
                                }
                            }
                            this.dcsDomainContext.ExportInvoiceInformationOfPurchase(new int[] { }, null, BaseApp.Current.CurrentUserData.EnterpriseId, invoicePurpose, invoiceNumber, sourceCode, invoiceCompanyCode, invoiceCompanyName, invoiceReceiveCompanyId, type, createTimeStart, createTimeEnd, approveTimeStart, approveTimeEnd, status, loadOp => {
                                if(loadOp.HasError)
                                    return;
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                    ShellViewModel.Current.IsBusy = false;
                                }

                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                    ShellViewModel.Current.IsBusy = false;
                                }
                            }, null);
                        }
                    }
                    break;
                case CommonActionKeys.PRINT:
                    //TODO:打印功能暂不实现
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT:
                    //TODO:打印功能暂不实现
                    return false;
                default:
                    return false;
            }
        }
    }
}
