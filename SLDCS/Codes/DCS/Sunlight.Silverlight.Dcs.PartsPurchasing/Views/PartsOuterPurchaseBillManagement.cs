﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;
using WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views {
    [PageMeta("PartsSales", "PartsOuterPurchase", "PartsOuterPurchaseBill", ActionPanelKeys = new[] {
        CommonActionKeys.INITIALAPPROVE_APPROVE_AUDIT_ABANDON_EXPORT_REJECT_MERGEEXPORT_PRINT_COMFIRM_ADVANCEDAUDIT_SENIORAPPROVE
    })]
    public class PartsOuterPurchaseBillManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataInitialApproveView;
        private DataEditViewBase dataFinalApproveView;
        private RadWindow editWindow;
        private RadWindow rejectWindow;
        private DataEditViewBase dataDetailView;
        private const string DATA_DETAIL_VIEW = "_DataDetailView_";
        private const string DATA_INITIAL_VIEW = "_dataInitialView";
        private const string DATA_FINAL_VIEW = "_dataFinalView";
        private RadWindow RejectWindow {
            get {
                if(this.rejectWindow == null) {
                    this.rejectWindow = new RadWindow();
                    this.rejectWindow.CanClose = false;
                    this.rejectWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    this.rejectWindow.Content = this.RejectDataEditView;
                    this.rejectWindow.ResizeMode = ResizeMode.NoResize;
                    this.rejectWindow.Height = 200;
                    this.rejectWindow.Width = 400;
                    this.rejectWindow.Header = "";
                }
                return this.rejectWindow;
            }
        }

        private DataEditViewBase rejectDataEditView;
        private DataEditViewBase RejectDataEditView {
            get {
                if(this.rejectDataEditView == null) {
                    this.rejectDataEditView = DI.GetDataEditView("PartsOuterPurchaseChangeForReject");
                    this.rejectDataEditView.EditCancelled += rejectDataEditView_EditCancelled;
                    this.rejectDataEditView.EditSubmitted += rejectDataEditView_EditSubmitted;
                }
                return this.rejectDataEditView;
            }
        }

        private void rejectDataEditView_EditSubmitted(object sender, EventArgs e) {
            this.RejectWindow.Close();
            this.DataGridView.ExecuteQueryDelayed();
        }

        private void rejectDataEditView_EditCancelled(object sender, EventArgs e) {
            this.RejectWindow.Close();
        }

        private FrameworkElement partsPurchaseOuterPurchaseCommentForQueryWindow;
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsOuterPurchaseBill"
                };
            }
        }
        private DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("PartsOuterPurchaseBill");
                    this.dataGridView.RowDoubleClick += DataGridView_RowDoubleClick;
                }
                return this.dataGridView;
            }
        }
        private void DataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Count() == 1) {
                this.DataEditViewDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                this.SwitchViewTo(DATA_DETAIL_VIEW);
            }
        }

        private DataEditViewBase DataEditViewDetail {
            get {
                if(this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("PartsOuterPurchaseBillReportDetail");
                    this.dataDetailView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataDetailView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataInitialApproveView = null;
            this.dataFinalApproveView = null;
            this.dataDetailView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }
        private FrameworkElement PartsPurchaseOuterPurchaseCommentDataEditPanel {
            get {
                return this.partsPurchaseOuterPurchaseCommentForQueryWindow ?? (this.partsPurchaseOuterPurchaseCommentForQueryWindow = DI.GetDataEditPanel("PartsPurchaseOuterAbandonComment"));
            }
        }

        private RadWindow EditWindow {
            get {
                if(this.editWindow == null) {
                    this.editWindow = new RadWindow {
                        Content = this.PartsPurchaseOuterPurchaseCommentDataEditPanel,
                        Header = PartsPurchasingUIStrings.DataEditView_Title_Add_PartsPurchaseOuter_AbandonComment,
                        WindowState = WindowState.Normal,
                        WindowStartupLocation = WindowStartupLocation.CenterScreen,
                        AllowDrop = false,
                        ResizeMode = ResizeMode.NoResize,
                        CanClose = false
                    };
                    this.editWindow.Closed += this.EditWindow_Closed;
                }
                return this.editWindow;
            }
        }

        private void EditWindow_Closed(object sender, WindowClosedEventArgs e) {
            var radWindow = sender as RadWindow;
            if(radWindow == null || radWindow.Content == null)
                return;
            var dataEditPanel = radWindow.Content as FrameworkElement;
            if(dataEditPanel == null || dataEditPanel.DataContext == null || !(dataEditPanel.DataContext is PartsOuterPurchaseChange))
                return;
            var partsOuterPurchaseChange = dataEditPanel.DataContext as PartsOuterPurchaseChange;
            if(!string.IsNullOrWhiteSpace(partsOuterPurchaseChange.AbandonComment) && radWindow.DialogResult != null && (bool)radWindow.DialogResult) {
                try {
                    if(partsOuterPurchaseChange.Can作废配件外采申请单)
                        partsOuterPurchaseChange.作废配件外采申请单();
                    var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                    if(domainContext == null)
                        return;
                    domainContext.SubmitChanges(submitOp => {
                        if(submitOp.HasError) {
                            if(!submitOp.IsErrorHandled)
                                submitOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                            domainContext.RejectChanges();
                            return;
                        }
                        UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Notification_AbandonSuccess);
                        this.CheckActionsCanExecute();
                    }, null);
                } catch(Exception ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                }
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ABANDON:
                    var domainContext = new DcsDomainContext();
                    var partsOuterPurchaseChange = this.DataGridView.SelectedEntities.Cast<PartsOuterPurchaseChange>().SingleOrDefault();
                    if(partsOuterPurchaseChange == null)
                        return;
                    domainContext.Load(domainContext.GetPersonSalesCenterLinksQuery().Where(r => r.PersonId == BaseApp.Current.CurrentUserData.UserId && r.PartsSalesCategoryId == partsOuterPurchaseChange.PartsSalesCategoryrId && r.CompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            if(!loadOp.IsErrorHandled)
                                loadOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                            return;
                        }
                        var entity = loadOp.Entities.SingleOrDefault();
                        if(entity == null) {
                            UIHelper.ShowNotification(PartsPurchasingUIStrings.DataGridView_Validation_PartsOuterPurchaseChange_QuantityIsAbandon);
                        } else
                            DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Confirm_Abandon, () => {
                                this.EditWindow.ShowDialog();
                                this.PartsPurchaseOuterPurchaseCommentDataEditPanel.SetValue(DataContextProperty, partsOuterPurchaseChange);
                            });
                    }, null);
                    break;
                case CommonActionKeys.AUDIT://审核
                case CommonActionKeys.APPROVE://审批
                case CommonActionKeys.ADVANCEDAUDIT://高级审核

                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.CONFIRM://确认
                case CommonActionKeys.INITIALAPPROVE://初审
                    this.DataInitialApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_INITIAL_VIEW);
                    break;


                case CommonActionKeys.SENIORAPPROVE://高级审批
                    this.DataFinalApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_FINAL_VIEW);
                    break;
                case CommonActionKeys.REJECT:
                    this.RejectDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.RejectWindow.ShowDialog();
                    break;
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsOuterPurchaseChange>().Select(r => r.Id).ToArray();
                        this.ExportPartsOuterPurchaseChangeWithDetail(ids, null, null, null, null, null, null, null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var t = filterItem.Filters.FirstOrDefault() as CompositeFilterItem;
                        var partsOuterPurchaseChangeCode = t.Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : t.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var customerCompanyName = t.Filters.SingleOrDefault(r => r.MemberName == "CustomerCompanyNace") == null ? null : t.Filters.Single(r => r.MemberName == "CustomerCompanyNace").Value as string;
                        var marketingdepartmentId = t.Filters.SingleOrDefault(r => r.MemberName == "MarketDepartmentId") == null ? null : t.Filters.Single(r => r.MemberName == "MarketDepartmentId").Value as int?;
                        var statusStr = statustr;
                        var partsSalesCategoryrsId = partsSalesCategoryrIdStr;
                        var createTime = t.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        this.ExportPartsOuterPurchaseChangeWithDetail(null, partsOuterPurchaseChangeCode, customerCompanyName, partsSalesCategoryrsId, marketingdepartmentId, statusStr, createTimeBegin, createTimeEnd);
                    }
                    break;

                case CommonActionKeys.MERGEEXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsOuterPurchaseChange>().Select(r => r.Id).ToArray();
                        this.ExportForPartsOuterPurchaseChangeWithDetail(ids, null, null, null, null, null, null, null);
                    } else {
                        var filterItemexport = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItemexport == null)
                            return;
                        var compositeFilterItem = filterItemexport.Filters.FirstOrDefault() as CompositeFilterItem;
                        var name = compositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "CustomerCompanyNace") == null ? null : compositeFilterItem.Filters.Single(r => r.MemberName == "CustomerCompanyNace").Value as string;
                        var code = compositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : compositeFilterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var marketingdepartmentId = compositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "MarketDepartmentId") == null ? null : compositeFilterItem.Filters.Single(r => r.MemberName == "MarketDepartmentId").Value as int?;
                        var statuStr = statustr;
                        var partsSalesCategoryrId = partsSalesCategoryrIdStr;
                        DateTime? beginCreateTime = null;
                        DateTime? endCreateTime = null;
                        foreach(var filter in compositeFilterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var dateTime = filter as CompositeFilterItem;
                            if(dateTime != null) {
                                if(dateTime.Filters.First().MemberName == "CreateTime") {
                                    beginCreateTime = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    endCreateTime = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }

                            }
                        }
                        ShellViewModel.Current.IsBusy = true;
                        this.ExportForPartsOuterPurchaseChangeWithDetail(null, code, name, partsSalesCategoryrId, marketingdepartmentId, statuStr, beginCreateTime, endCreateTime);
                    }
                    break;
                case CommonActionKeys.PRINT:
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<PartsOuterPurchaseChange>().FirstOrDefault();
                    if(selectedItem == null)
                        return;
                    BasePrintWindow printWindow = new PartsOuterPurchaseChangePrintWindow {
                        Header = PartsPurchasingUIStrings.DataManagementView_PrintWindow_Title_PartsOuterPurchaseChange,
                        PartsOuterPurchaseChange = selectedItem
                    };
                    printWindow.ShowDialog();
                    break;
            }
        }


        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private void ExportForPartsOuterPurchaseChangeWithDetail(int[] ids, string code, string name, int? partsSalesCategoryrId, int? marketingdepartmentId, int? statuStr, DateTime? beginCreateTime, DateTime? endCreateTime) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportForPartsOuterPurchaseChangeWithDetailAsync(ids, code, name, partsSalesCategoryrId, marketingdepartmentId, statuStr, beginCreateTime, endCreateTime);
            this.excelServiceClient.ExportForPartsOuterPurchaseChangeWithDetailCompleted -= excelServiceClient_ExportForPartsOuterPurchaseChangeWithDetailCompleted;
            this.excelServiceClient.ExportForPartsOuterPurchaseChangeWithDetailCompleted += excelServiceClient_ExportForPartsOuterPurchaseChangeWithDetailCompleted;
        }

        private void excelServiceClient_ExportForPartsOuterPurchaseChangeWithDetailCompleted(object sender, ExportForPartsOuterPurchaseChangeWithDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }



        private void ExportPartsOuterPurchaseChangeWithDetail(int[] ids, string partsOuterPurchaseChangeCode, string customerCompanyName, int? partsSalesCategoryrsId, int? marketingdepartmentId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsOuterPurchaseChangeWithDetailAsync(ids, partsOuterPurchaseChangeCode, customerCompanyName, partsSalesCategoryrsId, marketingdepartmentId, status, createTimeBegin, createTimeEnd);
            this.excelServiceClient.ExportPartsOuterPurchaseChangeWithDetailCompleted -= excelServiceClient_ExportPartsOuterPurchaseChangeWithDetailCompleted;
            this.excelServiceClient.ExportPartsOuterPurchaseChangeWithDetailCompleted += excelServiceClient_ExportPartsOuterPurchaseChangeWithDetailCompleted;
        }

        protected void excelServiceClient_ExportPartsOuterPurchaseChangeWithDetailCompleted(object sender, ExportPartsOuterPurchaseChangeWithDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
        private int? partsSalesCategoryrIdStr;
        private int? statustr;
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            CompositeFilterItem compositeFilterStr;
            if(!(filterItem is CompositeFilterItem)) {
                compositeFilterStr = new CompositeFilterItem();
                compositeFilterStr.Filters.Add(filterItem);
            } else
                compositeFilterStr = filterItem as CompositeFilterItem;
            var compositeFilterItem = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                var compositeFilter = filterItem as CompositeFilterItem;

                if(BaseApp.Current.CurrentUserData.EnterpriseCategoryId != (int)DcsCompanyType.代理库) {
                    compositeFilter.Filters.Add(new FilterItem {
                        MemberName = "BranchId",
                        Operator = FilterOperator.IsEqualTo,
                        MemberType = typeof(int),
                        Value = BaseApp.Current.CurrentUserData.EnterpriseId
                    });
                }

                var createTimeFilters = compositeFilter.Filters.Where(filter => filter is CompositeFilterItem && (filter as CompositeFilterItem).Filters.All(item => item.MemberName == "CreateTime")).SingleOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                if(createTimeFilters != null) {
                    var dateTime = createTimeFilters.Filters.ElementAt(1).Value as DateTime?;
                    if(dateTime.HasValue)
                        createTimeFilters.Filters.ElementAt(1).Value = new DateTime(dateTime.Value.Year, dateTime.Value.Month, dateTime.Value.Day, 23, 59, 59);
                }
                compositeFilterItem.Filters.Add(compositeFilter);
            } else
                compositeFilterItem.Filters.Add(filterItem);
            partsSalesCategoryrIdStr = compositeFilterStr.Filters.SingleOrDefault(e => e.MemberName == "PartsSalesCategoryrId") == null ? 221 : compositeFilterStr.Filters.Single(e => e.MemberName == "PartsSalesCategoryrId").Value as int?;
            statustr = compositeFilterStr.Filters.SingleOrDefault(e => e.MemberName == "Status") == null ? null : compositeFilterStr.Filters.Single(e => e.MemberName == "Status").Value as int?;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ABANDON://删除
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities1 = this.DataGridView.SelectedEntities.Cast<PartsOuterPurchaseChange>().ToArray();
                    if(entities1.Length != 1)
                        return false;
                    return entities1[0].Status == (int)DcsPartsOuterPurchaseChangeStatus.新建 || entities1[0].Status == (int)DcsPartsOuterPurchaseChangeStatus.提交;

                case CommonActionKeys.REJECT://驳回
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsOuterPurchaseChange>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsPartsOuterPurchaseChangeStatus.提交;
                case CommonActionKeys.AUDIT://审核
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities2 = this.DataGridView.SelectedEntities.Cast<PartsOuterPurchaseChange>().ToArray();
                    if(entities2.Length != 1)
                        return false;
                    return entities2[0].Status == (int)DcsPartsOuterPurchaseChangeStatus.初审通过;
                case CommonActionKeys.APPROVE://审批
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities3 = this.DataGridView.SelectedEntities.Cast<PartsOuterPurchaseChange>().ToArray();
                    if(entities3.Length != 1)
                        return false;
                    return entities3[0].Status == (int)DcsPartsOuterPurchaseChangeStatus.审核通过;
                case CommonActionKeys.MERGEEXPORT://合并导出
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.EXPORT://导出
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT://打印
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    return this.DataGridView.SelectedEntities.Count() == 1;
                case CommonActionKeys.CONFIRM://确认
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities4 = this.DataGridView.SelectedEntities.Cast<PartsOuterPurchaseChange>().ToArray();
                    if(entities4.Length != 1)
                        return false;
                    return entities4[0].Status == (int)DcsPartsOuterPurchaseChangeStatus.提交;
                case CommonActionKeys.INITIALAPPROVE://初审
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities7 = this.DataGridView.SelectedEntities.Cast<PartsOuterPurchaseChange>().ToArray();
                    if(entities7.Length != 1)
                        return false;
                    return entities7[0].Status == (int)DcsPartsOuterPurchaseChangeStatus.确认通过;
                case CommonActionKeys.ADVANCEDAUDIT://高级审核
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities5 = this.DataGridView.SelectedEntities.Cast<PartsOuterPurchaseChange>().ToArray();
                    if(entities5.Length != 1)
                        return false;
                    return entities5[0].Status == (int)DcsPartsOuterPurchaseChangeStatus.审批通过;
                case CommonActionKeys.SENIORAPPROVE://高级审批
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities6 = this.DataGridView.SelectedEntities.Cast<PartsOuterPurchaseChange>().ToArray();
                    if(entities6.Length != 1)
                        return false;
                    return entities6[0].Status == (int)DcsPartsOuterPurchaseChangeStatus.高级审核通过;
                default:
                    return false;
            }
        }
        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsOuterPurchaseBillReportForApprove");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataInitialApproveView {
            get {
                if(this.dataInitialApproveView == null) {
                    this.dataInitialApproveView = DI.GetDataEditView("PartsOuterPurchaseBillReportForInitialApprove");
                    this.dataInitialApproveView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataInitialApproveView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataInitialApproveView;
            }
        }

        private DataEditViewBase DataFinalApproveView {
            get {
                if(this.dataFinalApproveView == null) {
                    this.dataFinalApproveView = DI.GetDataEditView("PartsOuterPurchaseBillReportForFinalApprove");
                    this.dataFinalApproveView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataFinalApproveView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataFinalApproveView;
            }
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_INITIAL_VIEW, () => this.DataInitialApproveView);
            this.RegisterView(DATA_FINAL_VIEW, () => this.DataFinalApproveView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataEditViewDetail);
        }
        public PartsOuterPurchaseBillManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsPurchasingUIStrings.DataManagementView_Title_PartsOuterPurchaseChange;
        }
    }
}
