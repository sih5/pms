﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views {
    [PageMeta("PartsSales", "PartsOuterPurchase", "PartsOuterPurchaseBillReport", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_SUBMIT_ABANDON_APPROVE_EXPORT
    })]
    public class PartsOuterPurchaseBillReportManagement : DcsDataManagementViewBase {
        private RadWindow editWindow;
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataDetailView;
        private DataEditViewBase dataEditViewApprove;
        private const string DATA_EDIT_VIEW_APPROVE = "_DataEditViewApprove_";
        private const string DATA_DETAIL_VIEW = "_DataDetailView_";
        private FrameworkElement partsPurchaseOuterPurchaseCommentForQueryWindow;
        private DataGridViewBase DataGridView {
            get {
                if (this.dataGridView == null)
                {
                    this.dataGridView = DI.GetDataGridView("PartsOuterPurchaseBillReport");
                    this.dataGridView.RowDoubleClick += DataGridView_RowDoubleClick;
                }
                return this.dataGridView;
            }
        }
        private void DataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e)
        {
            if (this.DataGridView.SelectedEntities !=null && this.DataGridView.SelectedEntities.Any()) { 
                this.DataEditViewDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                this.SwitchViewTo(DATA_DETAIL_VIEW);
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsOuterPurchaseBillReport"
                };
            }
        }

        private FrameworkElement PartsPurchaseOuterPurchaseCommentDataEditPanel {
            get {
                return this.partsPurchaseOuterPurchaseCommentForQueryWindow ?? (this.partsPurchaseOuterPurchaseCommentForQueryWindow = DI.GetDataEditPanel("PartsPurchaseOuterAbandonComment"));
            }
        }

        private RadWindow EditWindow {
            get {
                if(this.editWindow == null) {
                    this.editWindow = new RadWindow {
                        Content = this.PartsPurchaseOuterPurchaseCommentDataEditPanel,
                        Header = PartsPurchasingUIStrings.DataEditView_Title_Add_PartsPurchaseOuter_AbandonComment,
                        WindowState = WindowState.Normal,
                        WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                        AllowDrop = false,
                        ResizeMode = ResizeMode.NoResize,
                        CanClose = false
                    };
                    this.editWindow.Closed += this.EditWindow_Closed;
                }
                return this.editWindow;

            }
        }

        private void EditWindow_Closed(object sender, WindowClosedEventArgs e) {
            var radWindow = sender as RadWindow;
            if(radWindow == null || radWindow.Content == null)
                return;
            var dataEditPanel = radWindow.Content as FrameworkElement;
            if(dataEditPanel == null || dataEditPanel.DataContext == null || !(dataEditPanel.DataContext is PartsOuterPurchaseChange))
                return;
            var partsOuterPurchaseChange = dataEditPanel.DataContext as PartsOuterPurchaseChange;
            if(!string.IsNullOrWhiteSpace(partsOuterPurchaseChange.AbandonComment) && radWindow.DialogResult != null && (bool)radWindow.DialogResult) {
                try {
                    if(partsOuterPurchaseChange.Can作废配件外采申请单)
                        partsOuterPurchaseChange.作废配件外采申请单();
                    var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                    if(domainContext == null)
                        return;
                    domainContext.SubmitChanges(submitOp => {
                        if(submitOp.HasError) {
                            if(!submitOp.IsErrorHandled)
                                submitOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                            domainContext.RejectChanges();
                            return;
                        }
                        UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Notification_AbandonSuccess);
                        this.CheckActionsCanExecute();
                    }, null);
                } catch(Exception ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                }
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsOuterPurchaseBillReport");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataEditViewApprove {
            get {
                if(this.dataEditViewApprove == null) {
                    this.dataEditViewApprove = DI.GetDataEditView("PartsOuterPurchaseBillReportForApprove");
                    this.dataEditViewApprove.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditViewApprove.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewApprove;
            }
        }

        private DataEditViewBase DataEditViewDetail
        {
            get
            {
                if (this.dataDetailView == null)
                {
                    this.dataDetailView = DI.GetDataEditView("PartsOuterPurchaseBillReportDetail");
                    this.dataDetailView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataDetailView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataDetailView = null;
            this.dataEditViewApprove = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }


        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    //第二次新增时附件框中还存在新增的文件，所以清理掉
                    var addView = this.DataEditView as PartsOuterPurchaseBillReportDataEditView;
                    addView.DataEditPanels.FilePath = null;

                    var partsOuterPurchaseBill = this.DataEditView.CreateObjectToEdit<PartsOuterPurchaseChange>();
                    partsOuterPurchaseBill.Status = (int)DcsPartsOuterPurchaseChangeStatus.新建;
                    partsOuterPurchaseBill.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    partsOuterPurchaseBill.CustomerCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    partsOuterPurchaseBill.CustomerCompanyCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                    partsOuterPurchaseBill.CustomerCompanyNace = BaseApp.Current.CurrentUserData.EnterpriseName;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.SUBMIT:
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Confirm_Submit, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsOuterPurchaseChange>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can提交配件外采申请单)
                                entity.提交配件外采申请单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Notification_SubmitSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsOuterPurchaseChange>().SingleOrDefault();
                        if(entity == null)
                            return;
                        this.EditWindow.ShowDialog();
                        this.PartsPurchaseOuterPurchaseCommentDataEditPanel.SetValue(DataContextProperty, entity);
                    });
                    break;
                case CommonActionKeys.APPROVE:
                    this.DataEditViewApprove.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_APPROVE);
                    break;
                default:
                    ((DcsDataGridViewBase)this.DataGridView).ExportData();
                    break;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                var compositeFilter = filterItem as CompositeFilterItem;
                compositeFilter.Filters.Add(new FilterItem {
                    MemberName = "CustomerCompanyId",
                    Operator = FilterOperator.IsEqualTo,
                    MemberType = typeof(int),
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });
                var createTimeFilters = compositeFilter.Filters.Where(filter => filter is CompositeFilterItem && (filter as CompositeFilterItem).Filters.All(item => item.MemberName == "CreateTime")).SingleOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                if(createTimeFilters != null) {
                    var dateTime = createTimeFilters.Filters.ElementAt(1).Value as DateTime?;
                    if(dateTime.HasValue)
                        createTimeFilters.Filters.ElementAt(1).Value = new DateTime(dateTime.Value.Year, dateTime.Value.Month, dateTime.Value.Day, 23, 59, 59);
                }
                compositeFilterItem.Filters.Add(compositeFilter);
            } else
                compositeFilterItem.Filters.Add(filterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.SUBMIT:
                case CommonActionKeys.EDIT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities1 = this.DataGridView.SelectedEntities.Cast<PartsOuterPurchaseChange>().ToArray();
                    if(entities1.Length != 1)
                        return false;
                    return entities1[0].Status == (int)DcsPartsOuterPurchaseChangeStatus.新建;
                case CommonActionKeys.APPROVE:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsOuterPurchaseChange>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsPartsOuterPurchaseChangeStatus.提交;
                default:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
            }
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_APPROVE, () => this.DataEditViewApprove);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataEditViewDetail);
            //this.RegisterView(DATA_ADD_EDIT_VIEW, () => this.DataAddEditView);

        }
        public PartsOuterPurchaseBillReportManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsPurchasingUIStrings.DataManagementView_Title_PartsOuterPurchaseBillReport;
        }

       
    }
}
