﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit;
namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views {
    [PageMeta("PartsPurchasing", "PartsPurchasing", "PartsPurReturnOrderForBranch", ActionPanelKeys = new[] {
        "PartsPurReturnOrderForBranch",CommonActionKeys.MERGEEXPORT
    })]
    public class PartsPurReturnOrderForBranchManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditFinalApproveView;
        private DataEditViewBase dataEditApproveView;
        private const string DATA_EEDITFORFINALAPPORVE_VIEW = "_DataEditForFinalApproveView_";
        private const string DATA_EEDITFORAPPORVE_VIEW = "_DataEditForApproveView_";
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public PartsPurReturnOrderForBranchManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsPurchasingUIStrings.DataManagementView_Title_PartsPurReturnOrderForBranch;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsPurReturnOrderForBranch"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsPurReturnOrderForBranch");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        //初审界面
        private DataEditViewBase DataEditFinalApproveView
        {
            get
            {
                if (this.dataEditFinalApproveView == null)
                {
                    this.dataEditFinalApproveView = DI.GetDataEditView("PartsPurReturnOrderForBranchFinalApprove");
                    ((PartsPurReturnOrderForBranchFinalApproveDataEditView)this.dataEditFinalApproveView).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditFinalApproveView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditFinalApproveView;
            }
        }

        //终审界面
        private DataEditViewBase DataEditApproveView
        {
            get
            {
                if (this.dataEditApproveView == null)
                {
                    this.dataEditApproveView = DI.GetDataEditView("PartsPurReturnOrderForBranchApprove");
                    ((PartsPurReturnOrderForBranchApproveDataEditView)this.dataEditApproveView).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditApproveView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditApproveView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditFinalApproveView = null;
            this.dataEditApproveView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsPurReturnOrderForBranch"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var partsPurReturnOrder = this.DataEditView.CreateObjectToEdit<PartsPurReturnOrder>();
                    partsPurReturnOrder.Status = (int)DcsPartsPurReturnOrderStatus.新建;
                    partsPurReturnOrder.ReturnReason = (int)DcsPartsPurReturnOrderReturnReason.质量件退货;
                    partsPurReturnOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    partsPurReturnOrder.BranchCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                    partsPurReturnOrder.BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    partsPurReturnOrder.BranchName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    partsPurReturnOrder.OutStatus = (int)DcsReturnOutStatus.未出库;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    var abanPartsPurReturnOrder = this.DataGridView.SelectedEntities.Cast<PartsPurReturnOrder>().SingleOrDefault();
                    if(abanPartsPurReturnOrder == null)
                        return;
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Confirm_Abandon, () => {
                        try {
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            if(!abanPartsPurReturnOrder.Can作废配件采购退货单)
                                return;
                            abanPartsPurReturnOrder.作废配件采购退货单();
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "FinalApprove"://初审
                    this.DataEditFinalApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EEDITFORFINALAPPORVE_VIEW);
                    #region 取消快速操作
                    //var appPartsPurReturnOrderFinal = this.DataGridView.SelectedEntities.Cast<PartsPurReturnOrder>().SingleOrDefault();
                    //if (appPartsPurReturnOrderFinal == null)
                    //    return;
                    ////if(appPartsPurReturnOrder.PartsPurReturnOrderDetails.Count > 200) {
                    ////    UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_DetailCountMoreThan200);
                    ////    return;
                    ////}
                    //DcsUtils.Confirm("确定要初审选择数据吗？", () =>
                    //{
                    //    try
                    //    {
                    //        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                    //        if (domainContext == null)
                    //            return;
                    //        if (!appPartsPurReturnOrderFinal.Can初审配件采购退货单)
                    //            return;
                    //        appPartsPurReturnOrderFinal.初审配件采购退货单();
                    //        domainContext.SubmitChanges(submitOp =>
                    //        {
                    //            if (submitOp.HasError)
                    //            {
                    //                if (!submitOp.IsErrorHandled)
                    //                    submitOp.MarkErrorAsHandled();
                    //                DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    //                domainContext.RejectChanges();
                    //                return;
                    //            }
                    //            UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Notification_ApproveSuccess);
                    //            this.CheckActionsCanExecute();
                    //        }, null);
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        UIHelper.ShowAlertMessage(ex.Message);
                    //    }
                    //});
                    //break; 
                    #endregion
                    break;

                case CommonActionKeys.INSTEAD_APPROVE://终审
                    this.DataEditApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EEDITFORAPPORVE_VIEW);
                    #region 取消快速操作
                    //var appPartsPurReturnOrder = this.DataGridView.SelectedEntities.Cast<PartsPurReturnOrder>().SingleOrDefault();
                    //if(appPartsPurReturnOrder == null)
                    //    return;
                    ////if(appPartsPurReturnOrder.PartsPurReturnOrderDetails.Count > 200) {
                    ////    UIHelper.ShowNotification(PartsPurchasingUIStrings.DataEditView_Validation_DetailCountMoreThan200);
                    ////    return;
                    ////}
                    //DcsUtils.Confirm("确定要终审选择数据吗？", () => {
                    //    try {
                    //        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                    //        if(domainContext == null)
                    //            return;
                    //        if(!appPartsPurReturnOrder.Can审批配件采购退货单)
                    //            return;
                    //        appPartsPurReturnOrder.审批配件采购退货单();
                    //        domainContext.SubmitChanges(submitOp => {
                    //            if(submitOp.HasError) {
                    //                if(!submitOp.IsErrorHandled)
                    //                    submitOp.MarkErrorAsHandled();
                    //                DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    //                domainContext.RejectChanges();
                    //                return;
                    //            }
                    //            UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Notification_ApproveSuccess);
                    //            this.CheckActionsCanExecute();
                    //        }, null);
                    //    } catch(Exception ex) {
                    //        UIHelper.ShowAlertMessage(ex.Message);
                    //    }
                    //}); 
                    #endregion
                    break;
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    try {
                        this.ExecuteExport(filterItem, uniqueId);
                    } catch(Exception) {
                        var filterItem2 = filterItem.Filters.First() as CompositeFilterItem;
                        if(filterItem2 == null)
                            return;
                        this.ExecuteExport(filterItem2, uniqueId);
                    }

                    break;
                case CommonActionKeys.PRINT:
                    //TODO: 暂不实现
                    break;
            }
        }

        private void ExecuteExport(CompositeFilterItem filterItem, string uniqueId) {
            var partsSupplierCode = filterItem.Filters.Single(r => r.MemberName == "PartsSupplierCode").Value as string;
            var partsSupplierName = filterItem.Filters.Single(r => r.MemberName == "PartsSupplierName").Value as string;
         //   var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
            var code = filterItem.Filters.Single(e => e.MemberName == "Code").Value as string;
            var partsPurchaseOrderCode = filterItem.Filters.Single(r => r.MemberName == "PartsPurchaseOrderCode").Value as string;
            var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
            var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
            var outStatus = filterItem.Filters.Single(e => e.MemberName == "OutStatus").Value as int?;
            var returnReason = filterItem.Filters.Single(e => e.MemberName == "ReturnReason").Value as int?;
            var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
            DateTime? createTimeBegin = null;
            DateTime? createTimeEnd = null;
            if(createTime != null) {
                createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
            }
            ShellViewModel.Current.IsBusy = true;
            if(uniqueId == CommonActionKeys.MERGEEXPORT) {
                if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Count() == 1)
                    this.excelServiceClient.ExportPartsPurReturnOrderWithDetailAsync(this.DataGridView.SelectedEntities.Cast<PartsPurReturnOrder>().First().Id, partsSupplierCode, partsSupplierName, warehouseId, null,null, code, returnReason, status, outStatus, createTimeBegin, createTimeEnd);
                else
                    this.excelServiceClient.ExportPartsPurReturnOrderWithDetailAsync(null, partsSupplierCode, partsSupplierName, warehouseId, null, null,code, returnReason, status, outStatus, createTimeBegin, createTimeEnd);
                this.excelServiceClient.ExportPartsPurReturnOrderWithDetailCompleted -= this.excelServiceClient_ExportPartsPurReturnOrderWithDetailCompleted;
                this.excelServiceClient.ExportPartsPurReturnOrderWithDetailCompleted += this.excelServiceClient_ExportPartsPurReturnOrderWithDetailCompleted;
            } else {
                if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any())
                    this.excelServiceClient.ExportPartsPurReturnOrderAsync(this.DataGridView.SelectedEntities.Cast<PartsPurReturnOrder>().Select(r => r.Id).ToArray(), partsSupplierCode, partsSupplierName, warehouseId, null, code, returnReason, status, outStatus, createTimeBegin, createTimeEnd);
                else
                    this.excelServiceClient.ExportPartsPurReturnOrderAsync(null, partsSupplierCode, partsSupplierName, warehouseId, null, code, returnReason, status, outStatus, createTimeBegin, createTimeEnd);
                this.excelServiceClient.ExportPartsPurReturnOrderCompleted -= this.ExcelServiceClient_ExportPartsPurReturnOrderCompleted;
                this.excelServiceClient.ExportPartsPurReturnOrderCompleted += this.ExcelServiceClient_ExportPartsPurReturnOrderCompleted;
            }

        }

        void ExcelServiceClient_ExportPartsPurReturnOrderCompleted(object sender, ExportPartsPurReturnOrderCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.filename));
        }

        void excelServiceClient_ExportPartsPurReturnOrderWithDetailCompleted(object sender, ExportPartsPurReturnOrderWithDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.filename));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case "FinalApprove"://初审
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsPurReturnOrder>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsPartsPurReturnOrderStatus.新建;
                case CommonActionKeys.INSTEAD_APPROVE://终审
                    if (this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities1 = this.DataGridView.SelectedEntities.Cast<PartsPurReturnOrder>().ToArray();
                    if (entities1.Length != 1)
                        return false;
                    return entities1[0].Status == (int)DcsPartsPurReturnOrderStatus.初审通过;
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT:
                    //TODO: 暂不实现
                    return false;
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                var compositeFilter = filterItem as CompositeFilterItem;
                compositeFilter.Filters.Add(new FilterItem {
                    MemberName = "BranchId",
                    Operator = FilterOperator.IsEqualTo,
                    MemberType = typeof(int),
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });
                var createTimeFilters = compositeFilter.Filters.Where(filter => filter is CompositeFilterItem && (filter as CompositeFilterItem).Filters.All(item => item.MemberName == "CreateTime")).SingleOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                if(createTimeFilters != null) {
                    var dateTime = createTimeFilters.Filters.ElementAt(1).Value as DateTime?;
                    if(dateTime.HasValue)
                        createTimeFilters.Filters.ElementAt(1).Value = new DateTime(dateTime.Value.Year, dateTime.Value.Month, dateTime.Value.Day);
                }
                compositeFilterItem.Filters.Add(compositeFilter);
            } else
                compositeFilterItem.Filters.Add(filterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EEDITFORFINALAPPORVE_VIEW,()=>this.DataEditFinalApproveView);
            this.RegisterView(DATA_EEDITFORAPPORVE_VIEW, () => this.DataEditApproveView);
        }
    }
}
