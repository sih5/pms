﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using System.Windows.Browser;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views
{
    [PageMeta("PartsPurchasing", "PartsPurchasing", "PartsPurReturnOrder", ActionPanelKeys = new[] {
        CommonActionKeys.APPROVE_PRINT,"MergeExport"
    })]
    public class PartsPurReturnOrderManagement : DcsDataManagementViewBase
    {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditViewApprove;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private const string DATA_EDIT_VIEW_APPROVE = "_DataEditViewApprove_";

        public PartsPurReturnOrderManagement()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsPurchasingUIStrings.DataManagementView_Title_PartsPurReturnOrder;
        }

        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW_APPROVE, () => this.DataEditViewApprove);
        }

        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsPurReturnOrder"));
            }
        }

        private DataEditViewBase DataEditViewApprove
        {
            get
            {
                if (this.dataEditViewApprove == null)
                {
                    this.dataEditViewApprove = DI.GetDataEditView("PartsPurReturnOrderForApprove");
                    this.dataEditViewApprove.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditViewApprove.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewApprove;
            }
        }
        private void ResetEditView() {
            this.dataEditViewApprove = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e)
        {
            this.ResetEditView();
            if (this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e)
        {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "PartsPurReturnOrder"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem)
        {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilter = filterItem as CompositeFilterItem;
            compositeFilter.Filters.Add(new FilterItem
            {
                MemberName = "PartsSupplierId",
                Operator = FilterOperator.IsEqualTo,
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            ClientVar.ConvertTime(compositeFilter);
            this.DataGridView.FilterItem = compositeFilter;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId)
            {
                case CommonActionKeys.APPROVE:
                    this.DataEditViewApprove.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_APPROVE);
                    break;
                case CommonActionKeys.PRINT:
                    //TODO: 打印暂时不实现
                    break;
                case "MergeExport"://合并导出
                    var filtes = this.DataGridView.FilterItem as CompositeFilterItem;
                    if (filtes == null)
                        return;
                    var Ids = new int[] { };
                    if (this.DataGridView.SelectedEntities != null)
                    {
                        Ids = this.DataGridView.SelectedEntities.Select(r => (int)r.GetIdentity()).ToArray();
                    }
                    var PartsSupplierCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                    var code = filtes.Filters.SingleOrDefault(e => e.MemberName == "Code").Value as string;
                    var BranchId = filtes.Filters.SingleOrDefault(e => e.MemberName == "BranchId").Value as int?;
                    var partsSalesCategoryId = filtes.Filters.SingleOrDefault(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                    var warehouseId = filtes.Filters.SingleOrDefault(e => e.MemberName == "WarehouseId").Value as int?;
                    var returnReason = filtes.Filters.SingleOrDefault(e => e.MemberName == "ReturnReason").Value as int?;
                    var status = filtes.Filters.SingleOrDefault(e => e.MemberName == "Status").Value as int?;
                    var createTime = filtes.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    if (createTime != null)
                    {
                        createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                        createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.ExportPartsPurReturnOrderForSupplierWithDetail(Ids, PartsSupplierCode, BranchId, partsSalesCategoryId, warehouseId, code, returnReason, status, createTimeBegin, createTimeEnd);
                    break;
            }
        }

        //合并导出方法
        private void ExportPartsPurReturnOrderForSupplierWithDetail(int[] ids, string partsSupplierCode, int? BranchId, int? partsSalesCategoryId, int? warehouseId, string Code, int? returnReason, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd)
        {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsPurReturnOrderForSupplierWithDetailAsync(true,ids, partsSupplierCode, BranchId, partsSalesCategoryId, warehouseId, Code, returnReason, status, createTimeBegin, createTimeEnd);
            this.excelServiceClient.ExportPartsPurReturnOrderForSupplierWithDetailCompleted += excelServiceClient_ExportPartsPurReturnOrderForSupplierWithDetailCompleted;
            this.excelServiceClient.ExportPartsPurReturnOrderForSupplierWithDetailCompleted += excelServiceClient_ExportPartsPurReturnOrderForSupplierWithDetailCompleted;
        }

        private void excelServiceClient_ExportPartsPurReturnOrderForSupplierWithDetailCompleted(object sender, ExportPartsPurReturnOrderForSupplierWithDetailCompletedEventArgs e)
        {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.filename));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch (uniqueId)
            {
                case CommonActionKeys.APPROVE:
                    if (this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsPurReturnOrder>().ToArray();
                    if (entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsWorkflowOfSimpleApprovalStatus.新建;
                case CommonActionKeys.PRINT:
                    //TODO: 打印暂时不实现
                    return false;
                case "MergeExport"://合并导出
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
    }
}
