﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views {
      [PageMeta("PartsPurchasing", "PartsPurchasing", "SupplierPartsPurchaseOrderForShipping", ActionPanelKeys = new[] {
      "PartsPurchaseOrderSupplier"
    })]
    public class PartsPurchaseOrderForShippingManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private const string DATA_REPLACESHIP_VIEW = "_dataReplaceShipView_";
        private const string DATA_REPLACESHIPALL_VIEW = "_dataReplaceShipAllView_";
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsPurchaseOrderForShipping"));
            }
        }

        private DataEditViewBase dataReplaceShipAllView;
        private DataEditViewBase DataReplaceShipAllView {
            get {
                if(this.dataReplaceShipAllView == null) {
                    this.dataReplaceShipAllView = DI.GetDataEditView("PartsPurchaseOrderReplaceShipAll");
                    this.dataReplaceShipAllView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataReplaceShipAllView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataReplaceShipAllView;
            }
        }
        private DataEditViewBase dataReplaceShipView;
        private DataEditViewBase DataReplaceShipView {
            get {
                if(this.dataReplaceShipView == null) {
                    this.dataReplaceShipView = DI.GetDataEditView("PartsPurchaseOrderReplaceShip");
                    this.dataReplaceShipView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataReplaceShipView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataReplaceShipView;
            }
        }

        private void ResetEditView() {
            this.dataReplaceShipAllView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        public PartsPurchaseOrderForShippingManagement() {
            this.Title = PartsPurchasingUIStrings.DataManagementView_Title_PartsPurchaseOrderForShipping;
            this.Initializer.Register(this.Initialize);
        }
        //批量供应商确认配件采购订单
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.MERGEEXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var partsPurchaseOrderIds = new int[] { };
                    if(this.DataGridView.SelectedEntities != null) {
                        partsPurchaseOrderIds = this.DataGridView.SelectedEntities.Select(r => (int)r.GetIdentity()).ToArray();
                    }
                    var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                    var partsPurchaseOrderTypeId = filterItem.Filters.Single(e => e.MemberName == "PartsPurchaseOrderTypeId").Value as int?;
                    var ifDirectProvision = filterItem.Filters.Single(e => e.MemberName == "IfDirectProvision").Value as bool?;
                    var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                    var inStatus = filterItem.Filters.Single(e => e.MemberName == "InStatus").Value as int?;
                    var compositeFilterItems = filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).ToArray();
                    DateTime? rDeliveryTimeBegin = null;
                    DateTime? rDeliveryTimeEnd = null;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    DateTime? approveTimeBegin = null;
                    DateTime? approveTimeEnd = null;
                    var originalRequirementBillCode = filterItem.Filters.Single(e => e.MemberName == "OriginalRequirementBillCode").Value as string;
                    foreach (var dateTimeFilterItem in compositeFilterItems) {
                        var dateTime = dateTimeFilterItem as CompositeFilterItem;
                        if(dateTime != null) {
                            if(dateTime.Filters.Any(r => r.MemberName == "CreateTime")) {
                                createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                            }
                            if(dateTime.Filters.Any(r => r.MemberName == "RequestedDeliveryTime")) {
                                rDeliveryTimeBegin = dateTime.Filters.First(r => r.MemberName == "RequestedDeliveryTime").Value as DateTime?;
                                rDeliveryTimeEnd = dateTime.Filters.Last(r => r.MemberName == "RequestedDeliveryTime").Value as DateTime?;
                            }
                            if (dateTime.Filters.Any(r => r.MemberName == "ApproveTime"))
                            {
                                approveTimeBegin = dateTime.Filters.First(r => r.MemberName == "ApproveTime").Value as DateTime?;
                                approveTimeEnd = dateTime.Filters.Last(r => r.MemberName == "ApproveTime").Value as DateTime?;
                            }
                        }
                    }
                    var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                    ShellViewModel.Current.IsBusy = true;
                    this.ExportPartsPurchaseOrderWithDetailForShipping(partsPurchaseOrderIds, code, null, null, null, partsPurchaseOrderTypeId, ifDirectProvision, status, createTimeBegin, createTimeEnd, null, BaseApp.Current.CurrentUserData.EnterpriseId, warehouseId, inStatus, rDeliveryTimeBegin, rDeliveryTimeEnd, approveTimeBegin, approveTimeEnd,originalRequirementBillCode/*, cpPartsPurchaseOrderCode, cpPartsInboundCheckCode*/);
                    break;
                case "ReplaceShip":
                    this.CreatePartsShipOrder();
                    break;
                case "ReplaceShipAll"://汇总发运
                    this.SwitchViewTo(DATA_REPLACESHIPALL_VIEW);
                    break;

                  
            }
        }

        private void CreatePartsShipOrder() {
            var partsPurchaseOrder = this.DataGridView.SelectedEntities.First() as PartsPurchaseOrder;
            var domainContext = new DcsDomainContext();
            if(partsPurchaseOrder != null)
                domainContext.Load(domainContext.GetPartsPurchaseOrdersWithDetailsByIdQuery(partsPurchaseOrder.Id), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    var item = loadOp.Entities.FirstOrDefault();
                    if(item == null)
                        return;
                    var supplierShippingOrder = this.DataReplaceShipView.CreateObjectToEdit<SupplierShippingOrder>();
                    supplierShippingOrder.PartsSupplierId = item.PartsSupplierId;
                    supplierShippingOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    supplierShippingOrder.PartsSupplierCode = item.PartsSupplierCode;
                    supplierShippingOrder.PartsSupplierName = item.PartsSupplierName;
                    supplierShippingOrder.PartsPurchaseOrderId = item.Id;
                    supplierShippingOrder.PartsPurchaseOrderCode = item.Code;
                    supplierShippingOrder.ReceivingWarehouseId = item.WarehouseId;
                    supplierShippingOrder.ReceivingWarehouseName = item.WarehouseName;
                    supplierShippingOrder.BranchId = item.BranchId;
                    supplierShippingOrder.BranchCode = item.BranchCode;
                    supplierShippingOrder.BranchName = item.BranchName;
                    supplierShippingOrder.DirectRecWarehouseId = item.DirectRecWarehouseId;
                    supplierShippingOrder.DirectRecWarehouseName = item.DirectRecWarehouseName;
                    supplierShippingOrder.ReceivingAddress = item.ReceivingAddress;
                    supplierShippingOrder.IfDirectProvision = item.IfDirectProvision;
                    supplierShippingOrder.PartsSalesCategoryId = item.PartsSalesCategoryId;
                    supplierShippingOrder.PartsSalesCategoryName = item.PartsSalesCategoryName;
                    supplierShippingOrder.DirectProvisionFinished = false;
                    supplierShippingOrder.RequestedDeliveryTime = item.RequestedDeliveryTime;
                    supplierShippingOrder.PlanDeliveryTime = DateTime.Now;
                    supplierShippingOrder.GPMSPurOrderCode = item.GPMSPurOrderCode;
                    supplierShippingOrder.OriginalRequirementBillId = item.Id;
                    supplierShippingOrder.OriginalRequirementBillCode = item.Code;
                    supplierShippingOrder.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件采购订单;
                    supplierShippingOrder.SAPPurchasePlanCode = item.SAPPurchasePlanCode;
                    ;
                    //收货单位Id与名称
                    if(supplierShippingOrder.IfDirectProvision) {
                        supplierShippingOrder.ReceivingCompanyId = item.ReceivingCompanyId;
                        supplierShippingOrder.ReceivingCompanyName = item.ReceivingCompanyName;
                    } else {
                        supplierShippingOrder.ReceivingCompanyId = item.BranchId;
                        supplierShippingOrder.ReceivingCompanyName = item.BranchName;
                    }
                    supplierShippingOrder.ShippingDate = DateTime.Now;
                    supplierShippingOrder.ShippingMethod = item.ShippingMethod ?? 0;
                    if (supplierShippingOrder.IfDirectProvision)
                    {
                        supplierShippingOrder.Status = (int)DcsSupplierShippingOrderStatus.新建;
                    }
                    else
                    {
                        supplierShippingOrder.Status = (int)DcsSupplierShippingOrderStatus.收货确认;
                    }
                    int serialNumber = 1;
                    foreach(var detail in item.PartsPurchaseOrderDetails) {
                        if(detail.ConfirmedAmount > 0 && detail.ConfirmedAmount != detail.ShippingAmount) {
                            var supplierShippingDetail = new SupplierShippingDetail();
                            supplierShippingDetail.SerialNumber = serialNumber++;
                            supplierShippingDetail.SparePartId = detail.SparePartId;
                            supplierShippingDetail.SparePartCode = detail.SparePartCode;
                            supplierShippingDetail.SparePartName = detail.SparePartName;
                            supplierShippingDetail.SupplierPartCode = detail.SupplierPartCode;
                            supplierShippingDetail.MeasureUnit = detail.MeasureUnit;
                            supplierShippingDetail.UnitPrice = detail.UnitPrice;
                            supplierShippingDetail.ConfirmedAmount = 0;
                            supplierShippingDetail.PendingQuantity = detail.ConfirmedAmount - (detail.ShippingAmount.HasValue ? detail.ShippingAmount.Value : 0);
                            supplierShippingDetail.Quantity = supplierShippingDetail.PendingQuantity;
                            supplierShippingDetail.POCode = detail.POCode;
                            supplierShippingDetail.TraceProperty = detail.SparePart.TraceProperty;
                            if(supplierShippingDetail.PendingQuantity > 0)
                                supplierShippingOrder.SupplierShippingDetails.Add(supplierShippingDetail);
                        }
                    }
                    this.SwitchViewTo(DATA_REPLACESHIP_VIEW);
                }, null);
        }


        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                    case "ReplaceShip":
                  if(this.DataGridView.SelectedEntities == null)
                        return false;
                  var entities = this.DataGridView.SelectedEntities.Cast<PartsPurchaseOrder>().ToArray();
                    if(entities.Length != 1)
                        return false;
                   return entities[0].Status == (int)DcsPartsPurchaseOrderStatus.确认完毕 || entities[0].Status == (int)DcsPartsPurchaseOrderStatus.部分确认 || entities[0].Status == (int)DcsPartsPurchaseOrderStatus.部分发运;
                case "ReplaceShipAll"://汇总发运
                    return true;             
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
              
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            if(filterItem is CompositeFilterItem) {
                var compositeFilter = filterItem as CompositeFilterItem;
                ClientVar.ConvertTime(compositeFilter);
                compositeFilter.Filters.Add(new FilterItem {
                    MemberName = "PartsSupplierId",
                    Operator = FilterOperator.IsEqualTo,
                    MemberType = typeof(int),
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });
                this.DataGridView.FilterItem = compositeFilter;
                this.DataGridView.ExecuteQueryDelayed();
            }
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsPurchaseOrderForShipping"
                };
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_REPLACESHIP_VIEW, () => this.DataReplaceShipView);
            this.RegisterView(DATA_REPLACESHIPALL_VIEW, () => this.DataReplaceShipAllView);
        }


        private void ExportPartsPurchaseOrderWithDetailForShipping(int[] partsPurchaseOrderIds, string code, string warehouseName, string partsSupplierCode, string partsSupplierName, int? partsPurchaseOrderTypeId, bool? ifDirectProvision, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int? branchId, int? supplierId, int? warehouseId, int? inStatus, DateTime? rDeliveryTimeBegin, DateTime? rDeliveryTimeEnd, DateTime? approveTimeBegin, DateTime? approveTimeEnd,string originalRequirementBillCode) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsPurchaseOrderWithDetailForShippingAsync(true, partsPurchaseOrderIds, code, null, warehouseName, partsSupplierCode, partsSupplierName, partsPurchaseOrderTypeId, ifDirectProvision, status, createTimeBegin, createTimeEnd, branchId, supplierId, warehouseId, inStatus, rDeliveryTimeBegin, rDeliveryTimeEnd, approveTimeBegin, approveTimeEnd, originalRequirementBillCode,null);
            this.excelServiceClient.ExportPartsPurchaseOrderWithDetailForShippingCompleted -= excelServiceClient_ExportPartsPurchaseOrderWithDetailForShippingCompleted;
            this.excelServiceClient.ExportPartsPurchaseOrderWithDetailForShippingCompleted += excelServiceClient_ExportPartsPurchaseOrderWithDetailForShippingCompleted;
        }

        private void excelServiceClient_ExportPartsPurchaseOrderWithDetailForShippingCompleted(object sender, ExportPartsPurchaseOrderWithDetailForShippingCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.filename));
        }
    }
}
