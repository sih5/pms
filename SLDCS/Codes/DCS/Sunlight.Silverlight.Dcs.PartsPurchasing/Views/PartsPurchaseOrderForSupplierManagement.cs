﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views {
    [PageMeta("PartsPurchasing", "PartsPurchasing", "SupplierPartsPurchaseOrder", ActionPanelKeys = new[] {
      "PartsPurchaseOrderSupplier"
    })]
    public class PartsPurchaseOrderForSupplierManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataDetailView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private const string DATA_REPLACESHIP_VIEW = "_dataReplaceShipView_";
        private const string DATA_REPLACESHIPALL_VIEW = "_dataReplaceShipAllView_";
        private const string DATA_DETAIL_VIEW = "_DataDetailView_";
        private DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("PartsPurchaseOrderWithDetailsForSupplier");
                    this.dataGridView.RowDoubleClick += DataGridView_RowDoubleClick;
                }
                return this.dataGridView;               
            }
        }
        private void DataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                this.DataEditViewDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                this.SwitchViewTo(DATA_DETAIL_VIEW);
            }
        }
        private DataEditViewBase DataEditViewDetail {
            get {
                if(this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("PartsPurchaseOrderDetail");
                    this.dataDetailView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataDetailView;
            }
        }
        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsPurchaseOrderForApprove");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase dataReplaceShipView;
        private DataEditViewBase DataReplaceShipView {
            get {
                if(this.dataReplaceShipView == null) {
                    this.dataReplaceShipView = DI.GetDataEditView("PartsPurchaseOrderReplaceShip");
                    this.dataReplaceShipView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataReplaceShipView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataReplaceShipView;
            }
        }

        private DataEditViewBase dataReplaceShipAllView;
        private DataEditViewBase DataReplaceShipAllView
        {
            get
            {
                if (this.dataReplaceShipAllView == null)
                {
                    this.dataReplaceShipAllView = DI.GetDataEditView("PartsPurchaseOrderReplaceShipAll");
                    this.dataReplaceShipAllView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataReplaceShipAllView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataReplaceShipAllView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        public PartsPurchaseOrderForSupplierManagement() {
            this.Title = PartsPurchasingUIStrings.DataManagementView_Title_PartsPurchaseOrderForSupplier;
            this.Initializer.Register(this.Initialize);
        }
        //批量供应商确认配件采购订单
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.CONFIRM:
                    if (this.DataGridView.SelectedEntities.Count()==1)
                    {
                        this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                        this.SwitchViewTo(DATA_EDIT_VIEW);
                    }
                    else
                    {
                        DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Notification_SureConfirm, () =>
                        {
                            try
                            {
                                var ids = DataGridView.SelectedEntities.Cast<PartsPurchaseOrder>().ToArray().Select(r => r.Id).ToArray();
                                var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                                if (domainContext != null)
                                {

                                    domainContext.批量供应商确认配件采购订单(ids, invokeOp =>
                                    {
                                        if (invokeOp.HasError)
                                            return;
                                        UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Notification_SubmitSuccess);
                                        this.DataGridView.ExecuteQueryDelayed();
                                        this.CheckActionsCanExecute();
                                    }, null);
                                }
                            }
                            catch (Exception ex)
                            {
                                UIHelper.ShowAlertMessage(ex.Message);
                            }
                        });
                    }
                   
                    break;
                case CommonActionKeys.TERMINATE:
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Confirm_Terminate, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsPurchaseOrder>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(!entity.IfDirectProvision) {
                                if(entity.Can终止配件采购订单)
                                    entity.终止配件采购订单();
                            } else {
                                if(entity.Can终止直供配件采购订单)
                                    entity.终止直供配件采购订单();
                            }
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Notification_TerminateSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.PRINT:
                    var print = this.DataGridView.SelectedEntities.Cast<PartsPurchaseOrder>().FirstOrDefault();
                    SunlightPrinter.ShowPrinter(PartsPurchasingUIStrings.DataManagementView_Text_PurchaseOrderPrint, "ReportPartsPurchaseOrder", null, true, new Tuple<string, string>("partsPurchaseOrderId", print.Id.ToString()));
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var partsPurchaseOrderIds = new int[] { };
                    if(this.DataGridView.SelectedEntities != null) {
                        partsPurchaseOrderIds = this.DataGridView.SelectedEntities.Select(r => (int)r.GetIdentity()).ToArray();
                    }

                    //var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                    var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                    //var partsSupplierCode = filterItem.Filters.Single(r => r.MemberName == "PartsSupplierCode").Value as string;
                    //var partsSupplierName = filterItem.Filters.Single(r => r.MemberName == "PartsSupplierName").Value as string;
                    var partsPurchaseOrderTypeId = filterItem.Filters.Single(e => e.MemberName == "PartsPurchaseOrderTypeId").Value as int?;
                    var ifDirectProvision = filterItem.Filters.Single(e => e.MemberName == "IfDirectProvision").Value as bool?;
                    var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                    var inStatus = filterItem.Filters.Single(e => e.MemberName == "InStatus").Value as int?;
                    var compositeFilterItems = filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).ToArray();
                    DateTime? rDeliveryTimeBegin = null;
                    DateTime? rDeliveryTimeEnd = null;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    DateTime? approveTimeBegin = null;
                    DateTime? approveTimeEnd = null;
                    //var cpPartsPurchaseOrderCode = filterItem.Filters.Single(e => e.MemberName == "CPPartsPurchaseOrderCode").Value as string;
                    //var cpPartsInboundCheckCode = filterItem.Filters.Single(e => e.MemberName == "CPPartsInboundCheckCode").Value as string;
                    var originalRequirementBillCode = filterItem.Filters.Single(e => e.MemberName == "OriginalRequirementBillCode").Value as string;
                    foreach (var dateTimeFilterItem in compositeFilterItems) {
                        var dateTime = dateTimeFilterItem as CompositeFilterItem;
                        if(dateTime != null) {
                            if(dateTime.Filters.Any(r => r.MemberName == "CreateTime")) {
                                createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                            }
                            if(dateTime.Filters.Any(r => r.MemberName == "RequestedDeliveryTime")) {
                                rDeliveryTimeBegin = dateTime.Filters.First(r => r.MemberName == "RequestedDeliveryTime").Value as DateTime?;
                                rDeliveryTimeEnd = dateTime.Filters.Last(r => r.MemberName == "RequestedDeliveryTime").Value as DateTime?;
                            }
                            if (dateTime.Filters.Any(r => r.MemberName == "ApproveTime"))
                            {
                                approveTimeBegin = dateTime.Filters.First(r => r.MemberName == "ApproveTime").Value as DateTime?;
                                approveTimeEnd = dateTime.Filters.Last(r => r.MemberName == "ApproveTime").Value as DateTime?;
                            }
                        }
                    }
                    //var branchId = filterItem.Filters.Single(e => e.MemberName == "BranchId").Value as int?;
                    var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                    ShellViewModel.Current.IsBusy = true;
                    this.ExportPartsPurchaseOrderWithDetail(partsPurchaseOrderIds/*, partsSalesCategoryId*/, code, null, null, null, partsPurchaseOrderTypeId, ifDirectProvision, status, createTimeBegin, createTimeEnd, null, BaseApp.Current.CurrentUserData.EnterpriseId, warehouseId, inStatus, rDeliveryTimeBegin, rDeliveryTimeEnd, approveTimeBegin, approveTimeEnd, originalRequirementBillCode/*, cpPartsPurchaseOrderCode, cpPartsInboundCheckCode*/);
                    break;
                case "ReplaceShip":
                    this.CreatePartsShipOrder();
                    break;
                case "ReplaceShipAll"://汇总发运
                    var partsPurchaseOrder = this.DataEditView.CreateObjectToEdit<PartsPurchaseOrder>();
                    //partsPurchaseOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    //partsPurchaseOrder.Status = (int)DcsPartsShippingOrderStatus.新建;
                    //partsPurchaseOrder.ShippingDate = DateTime.Now;
                    this.SwitchViewTo(DATA_REPLACESHIPALL_VIEW);
                    break;

                  
            }
        }

        private void CreatePartsShipOrder() {
            var partsPurchaseOrder = this.DataGridView.SelectedEntities.First() as PartsPurchaseOrder;
            var domainContext = new DcsDomainContext();
            if(partsPurchaseOrder != null)
                domainContext.Load(domainContext.GetPartsPurchaseOrdersWithDetailsByIdQuery(partsPurchaseOrder.Id), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    var item = loadOp.Entities.FirstOrDefault();
                    if(item == null)
                        return;
                    var supplierShippingOrder = this.DataReplaceShipView.CreateObjectToEdit<SupplierShippingOrder>();
                    supplierShippingOrder.PartsSupplierId = item.PartsSupplierId;
                    supplierShippingOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    supplierShippingOrder.PartsSupplierCode = item.PartsSupplierCode;
                    supplierShippingOrder.PartsSupplierName = item.PartsSupplierName;
                    supplierShippingOrder.PartsPurchaseOrderId = item.Id;
                    supplierShippingOrder.PartsPurchaseOrderCode = item.Code;
                    supplierShippingOrder.ReceivingWarehouseId = item.WarehouseId;
                    supplierShippingOrder.ReceivingWarehouseName = item.WarehouseName;
                    supplierShippingOrder.BranchId = item.BranchId;
                    supplierShippingOrder.BranchCode = item.BranchCode;
                    supplierShippingOrder.BranchName = item.BranchName;
                    supplierShippingOrder.DirectRecWarehouseId = item.DirectRecWarehouseId;
                    supplierShippingOrder.DirectRecWarehouseName = item.DirectRecWarehouseName;
                    supplierShippingOrder.ReceivingAddress = item.ReceivingAddress;
                    supplierShippingOrder.IfDirectProvision = item.IfDirectProvision;
                    supplierShippingOrder.PartsSalesCategoryId = item.PartsSalesCategoryId;
                    supplierShippingOrder.PartsSalesCategoryName = item.PartsSalesCategoryName;
                    supplierShippingOrder.DirectProvisionFinished = false;
                    supplierShippingOrder.RequestedDeliveryTime = item.RequestedDeliveryTime;
                    supplierShippingOrder.PlanDeliveryTime = DateTime.Now;
                    supplierShippingOrder.GPMSPurOrderCode = item.GPMSPurOrderCode;
                    supplierShippingOrder.OriginalRequirementBillId = item.Id;
                    supplierShippingOrder.OriginalRequirementBillCode = item.Code;
                    supplierShippingOrder.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件采购订单;
                    supplierShippingOrder.SAPPurchasePlanCode = item.SAPPurchasePlanCode;
                    ;
                    //收货单位Id与名称
                    if(supplierShippingOrder.IfDirectProvision) {
                        supplierShippingOrder.ReceivingCompanyId = item.ReceivingCompanyId;
                        supplierShippingOrder.ReceivingCompanyName = item.ReceivingCompanyName;
                    } else {
                        supplierShippingOrder.ReceivingCompanyId = item.BranchId;
                        supplierShippingOrder.ReceivingCompanyName = item.BranchName;
                    }
                    supplierShippingOrder.ShippingDate = DateTime.Now;
                    supplierShippingOrder.ShippingMethod = item.ShippingMethod ?? 0;
                    if (supplierShippingOrder.IfDirectProvision)
                    {
                        supplierShippingOrder.Status = (int)DcsSupplierShippingOrderStatus.新建;
                    }
                    else
                    {
                        supplierShippingOrder.Status = (int)DcsSupplierShippingOrderStatus.收货确认;
                    }
                    //supplierShippingOrder.ArrivalDate
                    int serialNumber = 1;
                    foreach(var detail in item.PartsPurchaseOrderDetails) {
                        if(detail.ConfirmedAmount > 0 && detail.ConfirmedAmount != detail.ShippingAmount) {
                            var supplierShippingDetail = new SupplierShippingDetail();
                            supplierShippingDetail.SerialNumber = serialNumber++;
                            supplierShippingDetail.SparePartId = detail.SparePartId;
                            supplierShippingDetail.SparePartCode = detail.SparePartCode;
                            supplierShippingDetail.SparePartName = detail.SparePartName;
                            supplierShippingDetail.SupplierPartCode = detail.SupplierPartCode;
                            supplierShippingDetail.MeasureUnit = detail.MeasureUnit;
                            supplierShippingDetail.UnitPrice = detail.UnitPrice;
                            supplierShippingDetail.ConfirmedAmount = 0;
                            supplierShippingDetail.PendingQuantity = detail.ConfirmedAmount - (detail.ShippingAmount.HasValue ? detail.ShippingAmount.Value : 0);
                            supplierShippingDetail.Quantity = supplierShippingDetail.PendingQuantity;
                            supplierShippingDetail.POCode = detail.POCode;
                            if(supplierShippingDetail.PendingQuantity > 0)
                                supplierShippingOrder.SupplierShippingDetails.Add(supplierShippingDetail);
                        }
                    }
                    this.SwitchViewTo(DATA_REPLACESHIP_VIEW);
                }, null);
        }


        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
               
                case CommonActionKeys.TERMINATE:
                case "ReplaceShip":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsPurchaseOrder>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    if(uniqueId == "ReplaceShip")
                        return entities[0].Status == (int)DcsPartsPurchaseOrderStatus.确认完毕 || entities[0].Status == (int)DcsPartsPurchaseOrderStatus.部分确认 || entities[0].Status == (int)DcsPartsPurchaseOrderStatus.部分发运;                 
                    return (entities[0].Status == (int)DcsPartsPurchaseOrderStatus.提交 || entities[0].Status == (int)DcsPartsPurchaseOrderStatus.确认完毕 || entities[0].Status == (int)DcsPartsPurchaseOrderStatus.部分发运 || entities[0].Status == (int)DcsPartsPurchaseOrderStatus.部分确认);

                case "ReplaceShipAll"://汇总发运
                    return true;
                case CommonActionKeys.PRINT:
                   if(this.DataGridView.SelectedEntities == null)
                        return false;
                   var entitiesPrint = this.DataGridView.SelectedEntities.Cast<PartsPurchaseOrder>().ToArray();
                    return (entitiesPrint.Length == 1);
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.CONFIRM:
                    if (this.DataGridView.SelectedEntities == null)
                        return false;
                    var conentities = this.DataGridView.SelectedEntities.Cast<PartsPurchaseOrder>().ToArray();
                    foreach (var item in conentities)
                    {
                        if (item.Status == (int)DcsPartsPurchaseOrderStatus.提交)
                        {
                            return true;
                        }                       
                    }
                    return false;
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            if(filterItem is CompositeFilterItem) {
                var compositeFilter = filterItem as CompositeFilterItem;
                ClientVar.ConvertTime(compositeFilter);
                compositeFilter.Filters.Add(new FilterItem {
                    MemberName = "PartsSupplierId",
                    Operator = FilterOperator.IsEqualTo,
                    MemberType = typeof(int),
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });
                this.DataGridView.FilterItem = compositeFilter;
                this.DataGridView.ExecuteQueryDelayed();
            }
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsPurchaseOrderForSupplier"
                };
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_REPLACESHIP_VIEW, () => this.DataReplaceShipView);
            this.RegisterView(DATA_REPLACESHIPALL_VIEW,()=>this.DataReplaceShipAllView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataEditViewDetail);
        }


        private void ExportPartsPurchaseOrderWithDetail(int[] partsPurchaseOrderIds/*, int? partsSalesCategoryId*/, string code, string warehouseName, string partsSupplierCode, string partsSupplierName, int? partsPurchaseOrderTypeId, bool? ifDirectProvision, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int? branchId, int? supplierId, int? warehouseId, int? inStatus, DateTime? rDeliveryTimeBegin, DateTime? rDeliveryTimeEnd, DateTime? approveTimeBegin, DateTime? approveTimeEnd, string originalRequirementBillCode/*, string cpPartsPurchaseOrderCode, string cpPartsInboundCheckCode*/) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsPurchaseOrderWithDetailAsync(true, partsPurchaseOrderIds/*, partsSalesCategoryId*/, code, null, warehouseName, partsSupplierCode, partsSupplierName, partsPurchaseOrderTypeId, ifDirectProvision, status, createTimeBegin, createTimeEnd, branchId, supplierId, warehouseId, inStatus, rDeliveryTimeBegin, rDeliveryTimeEnd, approveTimeBegin, approveTimeEnd/*, null, null*/, originalRequirementBillCode/*, cpPartsPurchaseOrderCode, cpPartsInboundCheckCode*/, null, null);
            this.excelServiceClient.ExportPartsPurchaseOrderWithDetailCompleted -= excelServiceClient_ExportPartsPurchaseOrderWithDetailCompleted;
            this.excelServiceClient.ExportPartsPurchaseOrderWithDetailCompleted += excelServiceClient_ExportPartsPurchaseOrderWithDetailCompleted;
        }

        private void excelServiceClient_ExportPartsPurchaseOrderWithDetailCompleted(object sender, ExportPartsPurchaseOrderWithDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.filename));
        }
    }
}

