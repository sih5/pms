﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views {
    [PageMeta("PartsPurchasing", "PartsPurchasing", "PartsPurchaseOrder", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_SUBMIT_TERMINATE_EXPORT_IMPORT_MERGEEXPORT, "PartsPurchaseOrder",CommonActionKeys.SCHEDULEREEXPORT
    })]
    public class PartsPurchaseOrderManagement : DcsDataManagementViewBase {
        #region 终止弹出
        private RadWindow terminateRadWindow;

        private RadWindow TerminateRadWindow {
            get {
                if(this.terminateRadWindow == null) {
                    this.terminateRadWindow = new RadWindow();
                    this.terminateRadWindow.CanClose = false;
                    this.terminateRadWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    this.terminateRadWindow.Content = this.TerminateDataEditView;
                    this.terminateRadWindow.ResizeMode = ResizeMode.NoResize;
                    this.terminateRadWindow.Height = 200;
                    this.terminateRadWindow.Width = 400;
                    this.terminateRadWindow.Header = "";
                }
                return this.terminateRadWindow;
            }
        }

        private DataEditViewBase terminateDataEditView;

        private DataEditViewBase TerminateDataEditView {
            get {
                if(this.terminateDataEditView == null) {
                    this.terminateDataEditView = DI.GetDataEditView("PartsPurchaseOrderForTerminate");
                    this.terminateDataEditView.EditCancelled += terminateDataEditView_EditCancelled;
                    this.terminateDataEditView.EditSubmitted += terminateDataEditView_EditSubmitted;
                }
                return this.terminateDataEditView;
            }
        }

        private void terminateDataEditView_EditSubmitted(object sender, EventArgs e) {
            this.TerminateRadWindow.Close();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
        }

        private void terminateDataEditView_EditCancelled(object sender, EventArgs e) {
            this.TerminateRadWindow.Close();
        }
        #endregion

        #region 作废弹出
        private RadWindow abandonRadWindow;

        private RadWindow AbandonRadWindow {
            get {
                if(this.abandonRadWindow == null) {
                    this.abandonRadWindow = new RadWindow();
                    this.abandonRadWindow.CanClose = false;
                    this.abandonRadWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    this.abandonRadWindow.Content = this.AbandonDataEditView;
                    this.abandonRadWindow.ResizeMode = ResizeMode.NoResize;
                    this.abandonRadWindow.Height = 200;
                    this.abandonRadWindow.Width = 400;
                    this.abandonRadWindow.Header = "";
                }
                return this.abandonRadWindow;
            }
        }

        private DataEditViewBase abandonDataEditView;

        private DataEditViewBase AbandonDataEditView {
            get {
                if(this.abandonDataEditView == null) {
                    this.abandonDataEditView = DI.GetDataEditView("PartsPurchaseOrderForAbandon");
                    this.abandonDataEditView.EditCancelled += abandonDataEditView_EditCancelled;
                    this.abandonDataEditView.EditSubmitted += abandonDataEditView_EditSubmitted;
                }
                return this.abandonDataEditView;
            }
        }


        private void abandonDataEditView_EditSubmitted(object sender, EventArgs e) {
            this.AbandonRadWindow.Close();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
        }

        private void abandonDataEditView_EditCancelled(object sender, EventArgs e) {
            this.AbandonRadWindow.Close();
        }
        #endregion

        private DataEditViewBase dataEditView;
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataApproveView;
        private DataEditViewBase dataEditViewImport;
        private DataEditViewBase dataChannelChangeView;
        private DataEditViewBase dataReplaceShipView;
        private DataEditViewBase dataDetailView;
        private const string DATA_CHANNELCHANGE_VIEW = "_dataChannelChangeView_";
        private const string DATA_EDIT_VIEW_IMPORT = "_DataEditViewImport_";
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private const string DATA_APPROVE_VIEW = "_dataPendingApprovalView_";
        private const string DATA_REPLACESHIP_VIEW = "_dataReplaceShipView_";
        private const string DATA_REPLACESHIPALL_VIEW = "_dataReplaceShipAllView_";
        private const string DATA_DETAIL_VIEW = "_DataDetailView_";

        public PartsPurchaseOrderManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsPurchasingUIStrings.DataManagementView_Title_PartsPurchaseOrder;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_APPROVE_VIEW, () => this.DataApproveView);
            this.RegisterView(DATA_EDIT_VIEW_IMPORT, () => this.DataEditViewImport);
            this.RegisterView(DATA_CHANNELCHANGE_VIEW, () => this.DataChannelChangeView);
            this.RegisterView(DATA_REPLACESHIP_VIEW, () => this.DataReplaceShipView);
            this.RegisterView(DATA_REPLACESHIPALL_VIEW, () => this.DataReplaceShipAllView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataEditViewDetail);
        }

        private DataGridViewBase DataGridView {
            get {             
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("PartsPurchaseOrderWithDetails");
                    this.dataGridView.RowDoubleClick += DataGridView_RowDoubleClick;
                }
                return this.dataGridView;
            }
        }
        private void DataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                this.DataEditViewDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                this.SwitchViewTo(DATA_DETAIL_VIEW);
            }
        }

        private DataEditViewBase DataChannelChangeView {
            get {
                if(this.dataChannelChangeView == null) {
                    this.dataChannelChangeView = DI.GetDataEditView("PartsPurchaseOrderChannelChange");
                    this.dataChannelChangeView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataChannelChangeView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataChannelChangeView;
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsPurchaseOrder");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataApproveView {
            get {
                if(this.dataApproveView == null) {
                    this.dataApproveView = DI.GetDataEditView("PartsPurchaseOrderPendingApproval");
                    this.dataApproveView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataApproveView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataApproveView;
            }
        }

        private DataEditViewBase DataEditViewImport {
            get {
                if(this.dataEditViewImport == null) {
                    this.dataEditViewImport = DI.GetDataEditView("PartsPurchaseOrderForImport");
                    this.dataEditViewImport.EditCancelled += this.DataEditView_EditCancelled;
                    this.dataEditViewImport.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataEditViewImport;
            }
        }

        private DataEditViewBase DataReplaceShipView {
            get {
                if(this.dataReplaceShipView == null) {
                    this.dataReplaceShipView = DI.GetDataEditView("PartsPurchaseOrderReplaceShip");
                    this.dataReplaceShipView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataReplaceShipView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataReplaceShipView;
            }
        }

        private DataEditViewBase dataReplaceShipAllView;
        private DataEditViewBase DataReplaceShipAllView {
            get {
                if(this.dataReplaceShipAllView == null) {
                    this.dataReplaceShipAllView = DI.GetDataEditView("PartsPurchaseOrderOrderReplaceShipAll");
                    this.dataReplaceShipAllView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataReplaceShipAllView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataReplaceShipAllView;
            }
        }
        private DataEditViewBase DataEditViewDetail {
            get {
                if(this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("PartsPurchaseOrderDetail");
                    this.dataDetailView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataDetailView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataApproveView = null;
            this.dataEditViewImport = null;
            this.dataChannelChangeView = null;
            this.dataReplaceShipView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsPurchaseOrder"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "BranchId",
                Operator = FilterOperator.IsEqualTo,
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var partsPurchaseOrder = this.DataEditView.CreateObjectToEdit<PartsPurchaseOrder>();
                    partsPurchaseOrder.BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    partsPurchaseOrder.BranchCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                    partsPurchaseOrder.BranchName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    partsPurchaseOrder.ReceivingCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    partsPurchaseOrder.ReceivingCompanyName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    partsPurchaseOrder.Status = (int)DcsPartsPurchaseOrderStatus.新增;
                    partsPurchaseOrder.RequestedDeliveryTime = DateTime.Now.Date;
                    partsPurchaseOrder.IfDirectProvision = false;
                    partsPurchaseOrder.InStatus = (int)DcsPurchaseInStatus.未入库;
                    partsPurchaseOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case "ReplaceShip":
                    this.CreatePartsShipOrder();
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Confirm_Abandon, () => {
                        this.AbandonDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                        this.AbandonRadWindow.ShowDialog();
                    });
                    break;
                case CommonActionKeys.SUBMIT:
                    //此处重写Invoke方法
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Confirm_Submit, () => {
                        try {
                            var entity = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseOrder>().ToArray();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            //if(entity.PartsSupplierCode == "FT010063") {
                            //    domainContext.Load(domainContext.GetBranchstrategiesQuery().Where(r => r.BranchId == entity.BranchId && r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                            //        if(loadOp.HasError) {
                            //            return;
                            //        }
                            //        var item = loadOp.Entities.FirstOrDefault();
                            //        if(item.IsIMS.HasValue && item.IsIMS == true) {
                            //            domainContext.Load(domainContext.GetPartsPurchaseOrderDetailsQuery().Where(r => r.PartsPurchaseOrderId == entity.Id), LoadBehavior.RefreshCurrent, loadOp1 => {
                            //                if(loadOp1.HasError) {
                            //                    return;
                            //                }
                            //                var dbPartsPurchaseOrderDetail = loadOp1.Entities.Select(p => p.SparePartId).ToArray();
                            //                domainContext.Load(domainContext.GetSparePartsByIdsQuery(dbPartsPurchaseOrderDetail), LoadBehavior.RefreshCurrent, loadOp2 => {
                            //                    var codes = loadOp2.Entities.Where(p => string.IsNullOrEmpty(p.ReferenceCode) || string.IsNullOrEmpty(p.IMSCompressionNumber)).Select(p => p.Code).ToArray();
                            //                    if(codes.Length > 0) {
                            //                        UIHelper.ShowNotification("配件编号为" + codes[0] + " 零部件图号或IMS压缩号为空。");
                            //                    } else {
                            //                        domainContext.提交配件采购订单(entity.Id, entity.OriginalRequirementBillId, entity.OriginalRequirementBillType, invokeOp => {
                            //                            if(invokeOp.HasError)
                            //                                return;
                            //                            UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Notification_SubmitSuccess);
                            //                            this.DataGridView.ExecuteQueryDelayed();
                            //                            this.CheckActionsCanExecute();
                            //                        }, null);
                            //                    }
                            //                }, null);
                            //            }, null);
                            //        }
                            //    }, null);
                            //} else {
                                if(domainContext != null) {
                                    
                                    domainContext.提交配件采购订单(entity.Select(r=>r.Id).Distinct().ToArray(),invokeOp => {
                                        if(invokeOp.HasError)
                                            return;
                                        UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Notification_SubmitSuccess);
                                        this.DataGridView.ExecuteQueryDelayed();
                                        this.CheckActionsCanExecute();
                                    }, null);
                                //}
                            }

                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.TERMINATE:
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Confirm_Terminate, () => {
                        this.TerminateDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                        this.TerminateRadWindow.ShowDialog();
                    });
                    break;
                case "ChannelChange":
                    this.DataChannelChangeView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_CHANNELCHANGE_VIEW);
                    break;
                case "PendingApproval":
                    this.DataApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_APPROVE_VIEW);
                    break;

                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                case CommonActionKeys.SCHEDULEREEXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    //var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                    var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                    //var businessCode = filterItem.Filters.Single(r => r.MemberName == "BusinessCode").Value as string;
                    //var hWPurOrderCode = filterItem.Filters.Single(r => r.MemberName == "HWPurOrderCode").Value as string;
                    //var eRPSourceOrderCode = filterItem.Filters.Single(r => r.MemberName == "ERPSourceOrderCode").Value as string;
                    var warehouseId = filterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                    //var partsSupplierCode = filterItem.Filters.Single(r => r.MemberName == "PartsSupplierCode").Value as string;
                    var partsSupplierName = filterItem.Filters.Single(r => r.MemberName == "PartsSupplierName").Value as string;
                    //var sapPurchasePlanCode = filterItem.Filters.Single(r => r.MemberName == "SAPPurchasePlanCode").Value as string;
                    //var cpPartsPurchaseOrderCode = filterItem.Filters.Single(r => r.MemberName == "CPPartsPurchaseOrderCode").Value as string;
                    //var cpPartsInboundCheckCode = filterItem.Filters.Single(r => r.MemberName == "CPPartsInboundCheckCode").Value as string;
                    var partsPurchaseOrderTypeId = filterItem.Filters.Single(e => e.MemberName == "PartsPurchaseOrderTypeId").Value as int?;
                    var status = filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                    var inStatus = filterItem.Filters.Single(e => e.MemberName == "InStatus").Value as int?;
                    var ifDirectProvision = filterItem.Filters.Single(r => r.MemberName == "IfDirectProvision").Value as bool?;
                    var ioStatus = filterItem.Filters.Single(r => r.MemberName == "IoStatus").Value as int?;
                    var sparepartCode = filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                    var compositeFilterItems = filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).ToArray();
                    DateTime? rDeliveryTimeBegin = null;
                    DateTime? rDeliveryTimeEnd = null;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    foreach(var dateTimeFilterItem in compositeFilterItems) {
                        var dateTime = dateTimeFilterItem as CompositeFilterItem;
                        if(dateTime != null) {
                            if(dateTime.Filters.Any(r => r.MemberName == "CreateTime")) {
                                createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                            }
                            if(dateTime.Filters.Any(r => r.MemberName == "RequestedDeliveryTime")) {
                                rDeliveryTimeBegin = dateTime.Filters.First(r => r.MemberName == "RequestedDeliveryTime").Value as DateTime?;
                                rDeliveryTimeEnd = dateTime.Filters.Last(r => r.MemberName == "RequestedDeliveryTime").Value as DateTime?;
                            }
                        }
                    }

                    if(uniqueId == CommonActionKeys.MERGEEXPORT) {
                        ShellViewModel.Current.IsBusy = true;
                        var partsPurchaseOrderIds = new int[] { };
                        if(this.DataGridView.SelectedEntities != null) {
                            var partsPurchaseOrders = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseOrder>();
                            partsPurchaseOrderIds = partsPurchaseOrders.Select(r => r.Id).ToArray();
                        }
                        this.ExportPartsPurchaseOrderWithDetail(partsPurchaseOrderIds/*, partsSalesCategoryId*/, code, null, null, null, partsSupplierName, partsPurchaseOrderTypeId, ifDirectProvision, status, createTimeBegin, createTimeEnd, BaseApp.Current.CurrentUserData.EnterpriseId, null, warehouseId, inStatus, rDeliveryTimeBegin, rDeliveryTimeEnd/*, sapPurchasePlanCode, hWPurOrderCode*/, null/*, cpPartsPurchaseOrderCode, cpPartsInboundCheckCode*/, ioStatus, sparepartCode);
                        break;
                    }
                    if(uniqueId == CommonActionKeys.EXPORT) {
                        ShellViewModel.Current.IsBusy = true;
                        var partsPurchaseOrderIds = new int[] { };
                        if(this.DataGridView.SelectedEntities != null) {
                            var partsPurchaseOrders = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseOrder>();
                            partsPurchaseOrderIds = partsPurchaseOrders.Select(r => r.Id).ToArray();
                        }
                        this.ExportPartsPurchaseOrder(partsPurchaseOrderIds/*, partsSalesCategoryId*/, code, null, null, null, partsSupplierName, partsPurchaseOrderTypeId, ifDirectProvision, status, createTimeBegin, createTimeEnd, BaseApp.Current.CurrentUserData.EnterpriseId, null, warehouseId, inStatus, rDeliveryTimeBegin, rDeliveryTimeEnd/*, sapPurchasePlanCode, hWPurOrderCode*/, null/*, cpPartsPurchaseOrderCode, cpPartsInboundCheckCode*/,ioStatus);
                        break;
                    }

                    if(uniqueId == CommonActionKeys.SCHEDULEREEXPORT) {
                        var exportParams = new Dictionary<string, string> {
                            //{ "partsSalesCategoryId", (partsSalesCategoryId ?? (object)string.Empty).ToString() },
                            { "code", (code ??  string.Empty).ToString() },
                            { "warehouseId", (warehouseId ?? (object)string.Empty).ToString() },
                            //{ "partsSupplierCode", (partsSupplierCode ?? string.Empty).ToString() },
                            { "partsSupplierName", (partsSupplierName ?? string.Empty).ToString() },
                            { "partsPurchaseOrderTypeId", (partsPurchaseOrderTypeId ?? (object)string.Empty).ToString() },
                        };
                        var dcsDomainContext = new DcsDomainContext();
                        dcsDomainContext.AddExportJobToScheduler("ScheduleExportPartsPurchaseOrderWithDetail", exportParams, PartsPurchasingUIStrings.DataManagementView_Text_PurchaseOrder, invokeOp => {
                            if(invokeOp.Value)
                                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Text_AddExportJobSuccess);
                            else
                                UIHelper.ShowAlertMessage(PartsPurchasingUIStrings.DataManagementView_Text_AddExportJobFail);
                        }, null);
                    }
                    break;
                case CommonActionKeys.PRINT:

                    var print = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseOrder>().FirstOrDefault();
                    SunlightPrinter.ShowPrinter(PartsPurchasingUIStrings.DataManagementView_Text_PurchaseOrderPrint, "ReportPartsPurchaseOrder", null, true, new Tuple<string, string>("partsPurchaseOrderId", print.Id.ToString()));
                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORT);
                    break;
                case "Rsend":
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Text_SureResendData, () => {
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if (domainContext != null) {
                            var rsend = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseOrder>().FirstOrDefault();
                            domainContext.重传配件采购订单(rsend.Id, invokeOp => {
                                if (invokeOp.HasError)
                                    return;
                                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Text_DataResendSuccess);
                                this.DataGridView.ExecuteQueryDelayed();
                                this.CheckActionsCanExecute();
                            }, null);
                        }
                    });
                    break;
                case "ReplaceShipAll"://汇总发运
                    var partsPurchaseOrderAll = this.DataEditView.CreateObjectToEdit<PartsPurchaseOrder>();
                    this.SwitchViewTo(DATA_REPLACESHIPALL_VIEW);
                    break;
            }
        }

        DateTime DtPlanDeliveryTime = DateTime.Now;

        private void CreatePartsShipOrder() {
            var partsPurchaseOrder = this.DataGridView.SelectedEntities.First() as VirtualPartsPurchaseOrder;
            var domainContext = new DcsDomainContext();
            if(partsPurchaseOrder != null)
                //赋值：计划到货时间：供应商发运单的生成日期 + 供应商物流周期 默认计算。年月日，不带时分秒
                domainContext.Load(domainContext.GetBranchSupplierRelationBySupplierIdQuery(partsPurchaseOrder.PartsSalesCategoryId, partsPurchaseOrder.PartsSupplierId, BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp1 => {
                    if(loadOp1.HasError) {
                        if(!loadOp1.IsErrorHandled)
                            loadOp1.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp1);
                        return;
                    }
                    if(loadOp1.Entities.Count()>0) {
                        DtPlanDeliveryTime = DateTime.Now.Date.AddDays(loadOp1.Entities.First().ArrivalCycle ?? 0);
                    }
                }, null);
            domainContext.Load(domainContext.GetPartsPurchaseOrdersWithDetailsByIdQuery(partsPurchaseOrder.Id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var item = loadOp.Entities.FirstOrDefault();
                if(item == null)
                    return;
                var supplierShippingOrder = this.DataReplaceShipView.CreateObjectToEdit<SupplierShippingOrder>();
                supplierShippingOrder.PartsSupplierId = item.PartsSupplierId;
                supplierShippingOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                supplierShippingOrder.PartsSupplierCode = item.PartsSupplierCode;
                supplierShippingOrder.PartsSupplierName = item.PartsSupplierName;
                supplierShippingOrder.PartsPurchaseOrderId = item.Id;
                supplierShippingOrder.PartsPurchaseOrderCode = item.Code;
                supplierShippingOrder.ReceivingWarehouseId = item.WarehouseId;
                supplierShippingOrder.ReceivingWarehouseName = item.WarehouseName;
                supplierShippingOrder.BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                supplierShippingOrder.BranchCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                supplierShippingOrder.BranchName = BaseApp.Current.CurrentUserData.EnterpriseName;
                supplierShippingOrder.ReceivingAddress = item.ReceivingAddress;
                supplierShippingOrder.IfDirectProvision = item.IfDirectProvision;
                supplierShippingOrder.PartsSalesCategoryId = item.PartsSalesCategoryId;
                supplierShippingOrder.PartsSalesCategoryName = item.PartsSalesCategoryName;
                supplierShippingOrder.DirectProvisionFinished = false;
                supplierShippingOrder.RequestedDeliveryTime = item.RequestedDeliveryTime;
                supplierShippingOrder.PlanDeliveryTime = DtPlanDeliveryTime;
                supplierShippingOrder.PlanSource = item.PlanSource;
                supplierShippingOrder.Remark = item.Remark;
                supplierShippingOrder.SAPPurchasePlanCode = item.SAPPurchasePlanCode;
                supplierShippingOrder.GPMSPurOrderCode = item.GPMSPurOrderCode;
                supplierShippingOrder.OriginalRequirementBillId = item.Id;
                supplierShippingOrder.DirectRecWarehouseId = item.DirectRecWarehouseId;
                supplierShippingOrder.DirectRecWarehouseName = item.DirectRecWarehouseName;
                supplierShippingOrder.OriginalRequirementBillCode = item.Code;
                supplierShippingOrder.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件采购订单;
                //收货单位Id与名称
                if(supplierShippingOrder.IfDirectProvision) {
                    supplierShippingOrder.ReceivingCompanyId = item.ReceivingCompanyId;
                    supplierShippingOrder.ReceivingCompanyName = item.ReceivingCompanyName;
                    supplierShippingOrder.Status = (int)DcsSupplierShippingOrderStatus.新建;
                } else {
                    supplierShippingOrder.ReceivingCompanyId = item.BranchId;
                    supplierShippingOrder.ReceivingCompanyName = item.BranchName;
                    supplierShippingOrder.Status = (int)DcsSupplierShippingOrderStatus.收货确认;
                }
                if(!partsPurchaseOrder.PartsPurchaseOrderTypeName.Equals("正常订单") && item.ShippingMethod.HasValue) {
                    supplierShippingOrder.ShippingMethod = item.ShippingMethod.Value;
                }
                supplierShippingOrder.ShippingDate = DateTime.Now;
                supplierShippingOrder.ShippingMethod = item.ShippingMethod ?? 0;
                if(partsPurchaseOrder.IfDirectProvision && (partsPurchaseOrder.PartsPurchaseOrderTypeName == "紧急订单" || partsPurchaseOrder.PartsPurchaseOrderTypeName == "特急订单")) {
                    supplierShippingOrder.IsMust = true;
                } else {
                    supplierShippingOrder.IsMust = false;
                }
                //supplierShippingOrder.ArrivalDate
                int serialNumber = 1;
                foreach(var detail in item.PartsPurchaseOrderDetails) {
                    if(detail.ConfirmedAmount > 0 && detail.ConfirmedAmount != detail.ShippingAmount) {
                        var supplierShippingDetail = new SupplierShippingDetail();
                        supplierShippingDetail.SerialNumber = serialNumber++;
                        supplierShippingDetail.SparePartId = detail.SparePartId;
                        supplierShippingDetail.SparePartCode = detail.SparePartCode;
                        supplierShippingDetail.SparePartName = detail.SparePartName;
                        supplierShippingDetail.SupplierPartCode = detail.SupplierPartCode;
                        supplierShippingDetail.MeasureUnit = detail.MeasureUnit;
                        supplierShippingDetail.UnitPrice = detail.UnitPrice;
                        supplierShippingDetail.ConfirmedAmount = 0;
                        supplierShippingDetail.PendingQuantity = detail.ConfirmedAmount - (detail.ShippingAmount.HasValue ? detail.ShippingAmount.Value : 0);
                        supplierShippingDetail.Quantity = supplierShippingDetail.PendingQuantity;
                        supplierShippingDetail.POCode = detail.POCode;
                      supplierShippingDetail.TraceProperty=  detail.SparePart.TraceProperty;
                        if(supplierShippingDetail.PendingQuantity > 0)
                            supplierShippingOrder.SupplierShippingDetails.Add(supplierShippingDetail);
                    }
                }
                this.SwitchViewTo(DATA_REPLACESHIP_VIEW);
            }, null);
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                case CommonActionKeys.IMPORT:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:               
                case CommonActionKeys.TERMINATE:
                case "PendingApproval":
                case "ReplaceShip":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseOrder>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    if((String.CompareOrdinal(uniqueId, CommonActionKeys.EDIT) == 0))
                        return (entities[0].Status == (int)DcsPartsPurchaseOrderStatus.新增 || entities[0].Status == (int)DcsPartsPurchaseOrderStatus.提交);
                    if((String.CompareOrdinal(uniqueId, CommonActionKeys.ABANDON) == 0))
                        return entities[0].Status == (int)DcsPartsPurchaseOrderStatus.新增;
                    if((String.CompareOrdinal(uniqueId, CommonActionKeys.SUBMIT) == 0))
                        return entities[0].Status == (int)DcsPartsPurchaseOrderStatus.新增;
                    if(uniqueId == "PendingApproval")
                        return entities[0].Status == (int)DcsPartsPurchaseOrderStatus.提交 || entities[0].Status == (int)DcsPartsPurchaseOrderStatus.部分确认 /*|| entities[0].Status == (int)DcsPartsPurchaseOrderStatus.部分发运*/;
                    if(uniqueId == "ReplaceShip")
                        return entities[0].Status == (int)DcsPartsPurchaseOrderStatus.确认完毕 || entities[0].Status == (int)DcsPartsPurchaseOrderStatus.部分确认 || entities[0].Status == (int)DcsPartsPurchaseOrderStatus.部分发运;
                    return (entities[0].Status == (int)DcsPartsPurchaseOrderStatus.提交 || entities[0].Status == (int)DcsPartsPurchaseOrderStatus.确认完毕 || entities[0].Status == (int)DcsPartsPurchaseOrderStatus.部分发运 || entities[0].Status == (int)DcsPartsPurchaseOrderStatus.部分确认);
                case CommonActionKeys.MERGEEXPORT:
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                //case CommonActionKeys.PRINT:
                //    //打印暂不实现
                //    return false;
                case "ChannelChange":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities1 = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseOrder>().ToArray();
                    if(entities1.Length != 1)
                        return false;
                    //如果 采购订单.供应商编号=FT010063，且 采购订单.分公司编号=2450 ，按钮 作废/代确认/代发运/渠道变更 不可使用
                    if(entities1[0].PartsSupplierCode == "FT010063" && entities1[0].BranchCode == "2450")
                        return false;
                    return (entities1[0].Status == (int)DcsPartsPurchaseOrderStatus.提交 || entities1[0].Status == (int)DcsPartsPurchaseOrderStatus.部分确认 || entities1[0].Status == (int)DcsPartsPurchaseOrderStatus.部分发运);
                case CommonActionKeys.SCHEDULEREEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitiesPrint = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseOrder>().ToArray();
                    return (entitiesPrint.Length == 1);
                case CommonActionKeys.SUBMIT:
                    if (this.DataGridView.SelectedEntities == null)
                    {
                        return false;
                    }
                    var conentities = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseOrder>().ToArray();
                    foreach (var item in conentities)
                    {
                        if (item.Status != (int)DcsPartsPurchaseOrderStatus.新增)
                        {
                            return false;
                        }                       
                    }
                    return true;
                case "Rsend":
                    if (this.DataGridView.SelectedEntities == null) {
                        return false;
                    }
                    var es = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseOrder>().ToArray();
                    if(es.Length != 1)
                        return false;
                    return es[0].IoStatus == 1 || es[0].IoStatus == null|| es[0].IoStatus==3;;
                case "ReplaceShipAll"://汇总发运
                    return true;
                default:
                    return false;
            }
        }

        private void ExportPartsPurchaseOrderWithDetail(int[] partsPurchaseOrderIds/*, int? partsSalesCategoryId*/, string code, string businessCode, string warehouseName, string partsSupplierCode, string partsSupplierName, int? partsPurchaseOrderTypeId, bool? ifDirectProvision, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int? branchId, int? supplierId, int? warehouseId, int? inStatus, DateTime? rDeliveryTimeBegin, DateTime? rDeliveryTimeEnd/*, string SAPPurchasePlanCode, string hWPurOrderCode*/, string eRPSourceOrderCode/*, string cPPartsPurchaseOrderCode, string cPPartsInboundCheckCode*/,int? ioStatus,string sparepartCode) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsPurchaseOrderWithDetailAsync(false, partsPurchaseOrderIds/*, partsSalesCategoryId*/, code, businessCode, warehouseName, partsSupplierCode, partsSupplierName, partsPurchaseOrderTypeId, ifDirectProvision, status, createTimeBegin, createTimeEnd, branchId, supplierId, warehouseId, inStatus, rDeliveryTimeBegin, rDeliveryTimeEnd, null, null, eRPSourceOrderCode/*, cPPartsPurchaseOrderCode, cPPartsInboundCheckCode*/, ioStatus, sparepartCode);
            this.excelServiceClient.ExportPartsPurchaseOrderWithDetailCompleted -= excelServiceClient_ExportPartsPurchaseOrderWithDetailCompleted;
            this.excelServiceClient.ExportPartsPurchaseOrderWithDetailCompleted += excelServiceClient_ExportPartsPurchaseOrderWithDetailCompleted;
        }

        private void ExportPartsPurchaseOrder(int[] partsPurchaseOrderIds/*, int? partsSalesCategoryId*/, string code, string businessCode, string warehouseName, string partsSupplierCode, string partsSupplierName, int? partsPurchaseOrderTypeId, bool? ifDirectProvision, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int? branchId, int? supplierId, int? warehouseId, int? inStatus, DateTime? rDeliveryTimeBegin, DateTime? rDeliveryTimeEnd/*, string SAPPurchasePlanCode, string hWPurOrderCode*/, string eRPSourceOrderCode/*, string cPPartsPurchaseOrderCode, string cPPartsInboundCheckCode*/,int? ioStatus) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsPurchaseOrderAsync(false, partsPurchaseOrderIds/*, partsSalesCategoryId*/, code, businessCode, warehouseName, partsSupplierCode, partsSupplierName, partsPurchaseOrderTypeId, ifDirectProvision, status, createTimeBegin, createTimeEnd, branchId, supplierId, warehouseId, inStatus, rDeliveryTimeBegin, rDeliveryTimeEnd/*, SAPPurchasePlanCode, hWPurOrderCode*/, eRPSourceOrderCode/*, cPPartsPurchaseOrderCode, cPPartsInboundCheckCode*/,ioStatus);
            this.excelServiceClient.ExportPartsPurchaseOrderCompleted -= excelServiceClient_ExportPartsPurchaseOrderilCompleted;
            this.excelServiceClient.ExportPartsPurchaseOrderCompleted += excelServiceClient_ExportPartsPurchaseOrderilCompleted;
        }

        private void excelServiceClient_ExportPartsPurchaseOrderWithDetailCompleted(object sender, ExportPartsPurchaseOrderWithDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.filename));
        }

        private void excelServiceClient_ExportPartsPurchaseOrderilCompleted(object sender, ExportPartsPurchaseOrderCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.filename));
        }
    }
}
