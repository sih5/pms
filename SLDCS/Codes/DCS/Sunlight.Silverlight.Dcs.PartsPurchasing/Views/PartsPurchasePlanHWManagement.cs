﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.ViewModel;
using WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views {
    [PageMeta("PartsPurchasing", "PartsPurchasing", "PartsPurchasePlanHW", ActionPanelKeys = new[] {
        "PartsPurchasePlanHW",CommonActionKeys.EXPORT_MERGEEXPORT
    })]
    public class PartsPurchasePlanHWManagement : DcsDataManagementViewBase {

        public PartsPurchasePlanHWManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsPurchasingUIStrings.DataManagementView_Title_PartsPurchasePlan_HW;
        }

        private DataEditViewBase dataEditView;
        private DataGridViewBase dataGridView;
        private RadWindow editWindow;

        private FrameworkElement terminateWindow;

        private FrameworkElement TerminateWindow {
            get {
                return this.terminateWindow ?? (this.terminateWindow = DI.GetDataEditPanel("PartsPurchasePlanHWTerminate"));
            }
        }

        private RadWindow EditWindow {
            get {
                if (this.editWindow == null) {
                    this.editWindow = new RadWindow {
                        Content = this.TerminateWindow,
                        Header = PartsPurchasingUIStrings.DataManagementView_Text_TerminateReasonInput,
                        WindowState = WindowState.Normal,
                        WindowStartupLocation = WindowStartupLocation.CenterScreen,
                        AllowDrop = false,
                        ResizeMode = ResizeMode.NoResize,
                        CanClose = false
                    };
                    this.editWindow.Closed += editWindow_Closed;
                }
                return this.editWindow;
            }
        }

        void editWindow_Closed(object sender, WindowClosedEventArgs e) {
            var radWindow = sender as RadWindow;
            if (radWindow == null || radWindow.Content == null) return;
            var dataEditPanel = radWindow.Content as FrameworkElement;
            if (dataEditPanel == null || dataEditPanel.DataContext == null || !(dataEditPanel.DataContext is PartsPurchasePlan_HW))
                return;
            var partsPurchasePlan_HW = dataEditPanel.DataContext as PartsPurchasePlan_HW;
            if (!string.IsNullOrWhiteSpace(partsPurchasePlan_HW.StopReason) && radWindow.DialogResult != null && (bool)radWindow.DialogResult) {
                try {
                    ShellViewModel.Current.IsBusy = true;
                    if (partsPurchasePlan_HW.Can申请终止采购计划)
                        partsPurchasePlan_HW.申请终止采购计划(partsPurchasePlan_HW.StopReason);
                    var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                    if (domainContext == null)
                        return;
                    domainContext.SubmitChanges(submitOp => {
                        if (submitOp.HasError) {
                            if (!submitOp.IsErrorHandled)
                                submitOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                            domainContext.RejectChanges();
                            this.CheckActionsCanExecute();
                            return;
                        }
                        ShellViewModel.Current.IsBusy = false;
                        UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Text_TerminateSuccess);
                        this.CheckActionsCanExecute();
                        this.SwitchViewTo(DATA_GRID_VIEW);
                    }, null);
                } catch (Exception ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                }
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsPurchasePlanHW"
                };
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsPurchasePlanHW"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if (this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsPurchasePlanHWAnalyze");
                    this.dataEditView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        void dataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            var partsPurchasePlan = this.DataGridView.SelectedEntities.Cast<PartsPurchasePlan_HW>().FirstOrDefault();
            if(partsPurchasePlan == null)
                return;
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            this.SwitchViewTo(DATA_GRID_VIEW);
            domainContext.Load(domainContext.GetPartsPurchasePlan_HWQuery().Where(r => r.Id == partsPurchasePlan.Id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(loadOp.IsErrorHandled) {
                        loadOp.MarkErrorAsHandled();
                        return;
                    }
                }
            }, null);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch (uniqueId) {
                case "DataCheck":
                    ShellViewModel.Current.IsBusy = true;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsPurchasePlan_HW>().ToArray().Where(e => e.Status == (int)DcsPurchasePlanStatus.新增 || e.Status == (int)DcsPurchasePlanStatus.数据异常 || e.Status == (int)DcsPurchasePlanStatus.可分解 || e.Status == (int)DcsPurchasePlanStatus.部分分解).ToList();
                    var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                    if (domainContext == null)
                        return;
                    if(entities.Count == 0)
                        return;
                    if (entities.Count == 1)
                        domainContext.Load(domainContext.GetPartsPurchasePlan_HWWithDetailsByIdQuery(entities[0].Id), LoadBehavior.RefreshCurrent, loadOp => {
                            if (loadOp.HasError) {
                                if (loadOp.IsErrorHandled) {
                                    loadOp.MarkErrorAsHandled();
                                    ShellViewModel.Current.IsBusy = false;
                                    return;
                                }
                            }
                            try {
                                var entityData = loadOp.Entities.SingleOrDefault();
                                if (entityData == null)
                                    return;
                                if (entityData.Can数据校验)
                                    entityData.数据校验();
                                domainContext.SubmitChanges(submitOp => {
                                    ShellViewModel.Current.IsBusy = false;
                                    if (submitOp.HasError) {
                                        if (!submitOp.IsErrorHandled)
                                            submitOp.MarkErrorAsHandled();
                                        DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                        domainContext.RejectChanges();
                                        return;
                                    }
                                    UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Text_DecomposeFinish);
                                    this.CheckActionsCanExecute();
                                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                                    this.SwitchViewTo(DATA_EDIT_VIEW);
                                }, null);
                            } catch (Exception ex) {
                                ShellViewModel.Current.IsBusy = false;
                                UIHelper.ShowAlertMessage(ex.Message);
                            }
                        }, null);
                    else {
                        ShellViewModel.Current.IsBusy = true;
                        int _count = entities.Count;
                        foreach (PartsPurchasePlan_HW item in entities) {
                            domainContext.Load(domainContext.GetPartsPurchasePlan_HWWithDetailsByIdQuery(item.Id), LoadBehavior.RefreshCurrent, loadOp => {
                                if (loadOp.HasError) {
                                    if (loadOp.IsErrorHandled) {
                                        loadOp.MarkErrorAsHandled();
                                        ShellViewModel.Current.IsBusy = false;
                                        return;
                                    }
                                }
                                try {
                                    _count--;
                                    var entityData = loadOp.Entities.SingleOrDefault();
                                    if (entityData == null)
                                        return;
                                    if (entityData.Can数据校验)
                                        entityData.数据校验();
                                    if (_count == 0) {
                                        domainContext.SubmitChanges(submitOp => {
                                            ShellViewModel.Current.IsBusy = false;
                                            if (submitOp.HasError) {
                                                if (!submitOp.IsErrorHandled)
                                                    submitOp.MarkErrorAsHandled();
                                                DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                                domainContext.RejectChanges();
                                                return;
                                            } else {
                                                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Text_DecomposeAll);
                                                this.CheckActionsCanExecute();
                                            }
                                        }, null);
                                    }
                                } catch (Exception ex) {
                                    ShellViewModel.Current.IsBusy = false;
                                    UIHelper.ShowAlertMessage(ex.Message);
                                }
                            }, null);
                        }
                    }
                    break;
                case "Analyze":
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case "Terminate":
                    var entity2 = this.DataGridView.SelectedEntities.Cast<PartsPurchasePlan_HW>().SingleOrDefault();
                    if (entity2 == null)
                        return;
                    var domainContext2 = this.DataGridView.DomainContext as DcsDomainContext;
                    if (domainContext2 == null)
                        return;
                    domainContext2.Load(domainContext2.GetPartsPurchasePlan_HWQuery().Where(r => r.Id == entity2.Id), LoadBehavior.RefreshCurrent, loadOp => {
                        if (loadOp.HasError) {
                            if (loadOp.IsErrorHandled) {
                                loadOp.MarkErrorAsHandled();
                                return;
                            }
                        }
                        var tmp = (this.EditWindow.Content as Sunlight.Silverlight.Dcs.PartsPurchasing.Panels.DataEdit.PartsPurchasePlanHWTerminateDataEditPanel);
                        if (tmp != null) tmp.TxtReason.Text = "";
                        this.EditWindow.ShowDialog();
                        this.TerminateWindow.SetValue(DataContextProperty, loadOp.Entities.First());
                    }, null);
                    break;
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if (filterItem == null) return;
                    var code = filterItem.Filters.Single(e => e.MemberName == "Code").Value as string;
                    var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                    //var partsSupplierCode = filterItem.Filters.Single(e => e.MemberName == "PartsSupplierCode").Value as string;
                    //var partsSupplierName = filterItem.Filters.Single(e => e.MemberName == "PartsSupplierName").Value as string;
                    //var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                    var partsPurchaseOrderTypeId = filterItem.Filters.Single(e => e.MemberName == "PurchasePlanType").Value as int?;
                    var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                    //var sussinessCode = filterItem.Filters.Single(e => e.MemberName == "BussinessCode").Value as string;
                    var SAPPurchasePlanCode = filterItem.Filters.Single(e => e.MemberName == "SAPPurchasePlanCode").Value as string;
                    DateTime? requestedDeliveryTimeBegin = null;
                    DateTime? requestedDeliveryTimeEnd = null;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    DateTime? modifyTimeBegin = null;
                    DateTime? modifyTimeEnd = null;
                    DateTime? stopTimeBegin = null;
                    DateTime? stopTimeEnd = null;
                    var compositeFilterItems = filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).ToArray();
                    foreach (var dateTimeFilterItem in compositeFilterItems) {
                        var dateTime = dateTimeFilterItem as CompositeFilterItem;
                        if (dateTime != null) {
                            if (dateTime.Filters.Any(r => r.MemberName == "RequestedDeliveryTime")) {
                                requestedDeliveryTimeBegin = dateTime.Filters.First(r => r.MemberName == "RequestedDeliveryTime").Value as DateTime?;
                                requestedDeliveryTimeEnd = dateTime.Filters.Last(r => r.MemberName == "RequestedDeliveryTime").Value as DateTime?;
                            }
                            if (dateTime.Filters.Any(r => r.MemberName == "CreateTime")) {
                                createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                            }
                            if (dateTime.Filters.Any(r => r.MemberName == "ModifyTime")) {
                                modifyTimeBegin = dateTime.Filters.First(r => r.MemberName == "ModifyTime").Value as DateTime?;
                                modifyTimeEnd = dateTime.Filters.Last(r => r.MemberName == "ModifyTime").Value as DateTime?;
                            }
                            if (dateTime.Filters.Any(r => r.MemberName == "StopTime")) {
                                stopTimeBegin = dateTime.Filters.First(r => r.MemberName == "StopTime").Value as DateTime?;
                                stopTimeEnd = dateTime.Filters.Last(r => r.MemberName == "StopTime").Value as DateTime?;
                            }
                        }
                    }
                    if(CommonActionKeys.EXPORT.Equals(uniqueId)) {
                        if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                            var ids = this.DataGridView.SelectedEntities.Cast<PartsPurchasePlan_HW>().Select(r => r.Id).ToArray();
                            this.ExportPartsPurchasePlan_HW(ids, null, null, null, null, null, null, null, null, null, null, null, null, null);
                        } else {
                            this.ExportPartsPurchasePlan_HW(null, code, partsSalesCategoryId, partsPurchaseOrderTypeId, status, SAPPurchasePlanCode, requestedDeliveryTimeBegin, requestedDeliveryTimeEnd, createTimeBegin, createTimeEnd, modifyTimeBegin, modifyTimeEnd, stopTimeBegin, stopTimeEnd);
                        }
                    } else {
                        if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                            var ids = this.DataGridView.SelectedEntities.Cast<PartsPurchasePlan_HW>().Select(r => r.Id).ToArray();
                            this.ExportPartsPurchasePlan_HWWithDetail(ids, null, null, null, null, null, null, null, null, null, null, null, null, null);
                        } else {
                            this.ExportPartsPurchasePlan_HWWithDetail(null, code, partsSalesCategoryId, partsPurchaseOrderTypeId, status, SAPPurchasePlanCode, requestedDeliveryTimeBegin, requestedDeliveryTimeEnd, createTimeBegin, createTimeEnd, modifyTimeBegin, modifyTimeEnd, stopTimeBegin, stopTimeEnd);
                        }
                    }
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId) {
                case "DataCheck":
                    if (this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsPurchasePlan_HW>().ToList();
                    if(entities.Any(e => e.Status == (int)DcsPurchasePlanStatus.申请终止 || e.Status == (int)DcsPurchasePlanStatus.终止))
                        return false;
                    if (entities.Any(e => e.Status == (int)DcsPurchasePlanStatus.新增 || e.Status == (int)DcsPurchasePlanStatus.数据异常 || e.Status == (int)DcsPurchasePlanStatus.可分解 || e.Status == (int)DcsPurchasePlanStatus.部分分解))
                        return true;
                    return false;
                case "Analyze":
                case "Terminate":
                    if (this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Cast<PartsPurchasePlan_HW>().Count() != 1)
                        return false;
                    var entity = this.DataGridView.SelectedEntities.Cast<PartsPurchasePlan_HW>().ToArray()[0];
                    if (uniqueId == "Analyze" && (entity.Status == (int)DcsPurchasePlanStatus.可分解 || entity.Status == (int)DcsPurchasePlanStatus.部分分解 || entity.Status == (int)DcsPurchasePlanStatus.数据异常))
                        return true;
                    if (uniqueId == "Terminate" && (entity.Status == (int)DcsPurchasePlanStatus.新增 || entity.Status == (int)DcsPurchasePlanStatus.可分解 || entity.Status == (int)DcsPurchasePlanStatus.部分分解 || entity.Status == (int)DcsPurchasePlanStatus.数据异常 || entity.Status == (int)DcsPurchasePlanStatus.分解完成))
                        return true;
                    return false;
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private void ExportPartsPurchasePlan_HW(int[] ids, string code, int? partsSalesCategoryId, int? partsPurchaseOrderTypeId, int? status, string SAPPurchasePlanCode, DateTime? requestedDeliveryTimeBegin, DateTime? requestedDeliveryTimeEnd, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? modifyTimeBegin, DateTime? modifyTimeEnd, DateTime? stopTimeBegin, DateTime? stopTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsPurchasePlan_HWAsync(ids, code, partsSalesCategoryId, partsPurchaseOrderTypeId, status, SAPPurchasePlanCode, requestedDeliveryTimeBegin, requestedDeliveryTimeEnd, createTimeBegin, createTimeEnd, modifyTimeBegin, modifyTimeEnd, stopTimeBegin, stopTimeEnd);
            this.excelServiceClient.ExportPartsPurchasePlan_HWCompleted -= excelServiceClient_ExportPartsPurchasePlan_HWCompleted;
            this.excelServiceClient.ExportPartsPurchasePlan_HWCompleted += excelServiceClient_ExportPartsPurchasePlan_HWCompleted;
        }

        private void ExportPartsPurchasePlan_HWWithDetail(int[] ids, string code, int? partsSalesCategoryId, int? partsPurchaseOrderTypeId, int? status, string SAPPurchasePlanCode, DateTime? requestedDeliveryTimeBegin, DateTime? requestedDeliveryTimeEnd, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? modifyTimeBegin, DateTime? modifyTimeEnd, DateTime? stopTimeBegin, DateTime? stopTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsPurchasePlan_HWWithDetailAsync(ids, code, partsSalesCategoryId, partsPurchaseOrderTypeId, status, SAPPurchasePlanCode, requestedDeliveryTimeBegin, requestedDeliveryTimeEnd, createTimeBegin, createTimeEnd, modifyTimeBegin, modifyTimeEnd, stopTimeBegin, stopTimeEnd);
            this.excelServiceClient.ExportPartsPurchasePlan_HWWithDetailCompleted -= excelServiceClient_ExportPartsPurchasePlan_HWWithDetailCompleted;
            this.excelServiceClient.ExportPartsPurchasePlan_HWWithDetailCompleted += excelServiceClient_ExportPartsPurchasePlan_HWWithDetailCompleted; ;
        }

        void excelServiceClient_ExportPartsPurchasePlan_HWCompleted(object sender, ExportPartsPurchasePlan_HWCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.filename));
        }

        void excelServiceClient_ExportPartsPurchasePlan_HWWithDetailCompleted(object sender, ExportPartsPurchasePlan_HWWithDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.filename));
        }
    }
}
