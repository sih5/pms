﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.ViewModel;
using WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views {
    [PageMeta("PartsPurchasing", "PartsPurchasing", "PartsPurchasePlanOrder", ActionPanelKeys = new[] {
        "PartsPurchasePlanOrder"
    })]
    public class PartsPurchasePlanOrderManagement : DcsDataManagementViewBase {
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public PartsPurchasePlanOrderManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsPurchasingUIStrings.DataManagementView_Title_PartsPurchasePlanOrder;
        }
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataDetailView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditForFirstApproveView;
        private DataEditViewBase dataEditForApproveView;
        private DataEditViewBase dataEditForTerminateView;
        private const string DATA_DETAIL_VIEW = "_DataDetailView_";
        private const string DATA_EEDITFORAPPORVE_VIEW = "_DataEditForApproveView_";
        private const string DATA_EEDITFORTERMINATE_VIEW = "_DataEditForTerminateView_";
        private const string DATA_EDITFORFIRSTAPPROVE_VIEW = "_DataEditForFirstApproveView_";

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsPurchasePlanOrder");
                    this.dataEditView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        //初审
        private DataEditViewBase DataEditForFirstApproveView {
            get {
                if(this.dataEditForFirstApproveView == null) {
                    this.dataEditForFirstApproveView = DI.GetDataEditView("PartsPurchasePlanOrderForFirstApprove");
                    ((PartsPurchasePlanOrderForFirstApproveDataEditView)this.dataEditForFirstApproveView).EditSubmitted += this.DataEditView_EditSubmitted; 
                    this.dataEditForFirstApproveView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataEditForFirstApproveView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataEditForFirstApproveView;
            }
        }

        private DataEditViewBase DataEditForApproveView {
            get {
                if(this.dataEditForApproveView == null) {
                    this.dataEditForApproveView = DI.GetDataEditView("PartsPurchasePlanOrderForApprove");
                    ((PartsPurchasePlanOrderForApproveDataEditView)this.dataEditForApproveView).EditSubmitted += this.DataEditView_EditSubmitted; 
                    this.dataEditForApproveView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataEditForApproveView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataEditForApproveView;
            }
        }
        private DataEditViewBase DataEditForTerminateView {
            get {
                if(this.dataEditForTerminateView == null) {
                    this.dataEditForTerminateView = DI.GetDataEditView("PartsPurchasePlanOrderForTerminate");
                    ((PartsPurchasePlanOrderForTerminateDataEditView)this.dataEditForTerminateView).EditSubmitted += this.DataEditView_EditSubmitted; 
                    this.dataEditForTerminateView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataEditForTerminateView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataEditForTerminateView;
            }
        }

        private RadWindow abandodRadWindow;

        private RadWindow AbandodRadWindow {
            get {
                if(this.abandodRadWindow == null) {
                    this.abandodRadWindow = new RadWindow();
                    this.abandodRadWindow.CanClose = false;
                    this.abandodRadWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    this.abandodRadWindow.Content = this.AbandodDataEditView;
                    this.abandodRadWindow.ResizeMode = ResizeMode.NoResize;
                    this.abandodRadWindow.Height = 200;
                    this.abandodRadWindow.Width = 400;
                    this.abandodRadWindow.Header = "";
                }
                return this.abandodRadWindow;
            }
        }
        private DataEditViewBase abandodDataEditView;

        private DataEditViewBase AbandodDataEditView {
            get {
                if(this.abandodDataEditView == null) {
                    this.abandodDataEditView = DI.GetDataEditView("PartsPurchasePlanOrderForAbandod");
                    this.abandodDataEditView.EditCancelled += AbandodDataEditView_EditCancelled;
                    this.abandodDataEditView.EditSubmitted += AbandodDataEditView_EditSubmitted;
                }
                return this.abandodDataEditView;
            }
        }

        private void AbandodDataEditView_EditCancelled(object sender, EventArgs e) {
            this.AbandodRadWindow.Close();
        }

        private void AbandodDataEditView_EditSubmitted(object sender, EventArgs e) {
            this.AbandodRadWindow.Close();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataDetailView = null;
            this.dataEditForApproveView = null;
            this.dataEditForFirstApproveView = null;
            this.dataEditForTerminateView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("PartsPurchasePlanOrder");
                    this.dataGridView.RowDoubleClick += DataGridView_RowDoubleClick;
                }
                return this.dataGridView;
            }
        }
        private void DataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                this.DataEditViewDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                this.SwitchViewTo(DATA_DETAIL_VIEW);
            }
        }

        private DataEditViewBase DataEditViewDetail {
            get {
                if(this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("PartsPurchasePlanDetail");
                    this.dataDetailView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataDetailView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataEditViewDetail);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EEDITFORAPPORVE_VIEW, () => this.DataEditForApproveView);
            this.RegisterView(DATA_EEDITFORTERMINATE_VIEW, () => this.DataEditForTerminateView);
            this.RegisterView(DATA_EDITFORFIRSTAPPROVE_VIEW, () => this.DataEditForFirstApproveView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsPurchasePlanOrder"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                //新增
                case CommonActionKeys.ADD:
                    return true;
                //修改、提交、作废
                case CommonActionKeys.EDIT:
                case CommonActionKeys.SUBMIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsPurchasePlan>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsPurchasePlanOrderStatus.新增;
                //审核
                case WorkflowActionKey.INITIALAPPROVE:
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var entitie = this.DataGridView.SelectedEntities.Cast<PartsPurchasePlan>().ToArray();
                    if(entitie.Length != 1)
                        return false;
                    return entitie[0].Status == (int)DcsPurchasePlanOrderStatus.初审通过;
                //初审
                case "FirstApprove":
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var first = this.DataGridView.SelectedEntities.Cast<PartsPurchasePlan>().ToArray();
                    if(first.Length != 1)
                        return false;
                    return first[0].Status == (int)DcsPurchasePlanOrderStatus.提交;
                //终审
                case WorkflowActionKey.FINALAPPROVE:
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var entitie1 = this.DataGridView.SelectedEntities.Cast<PartsPurchasePlan>().ToArray();
                    if(entitie1.Length != 1)
                        return false;
                    return entitie1[0].Status == (int)DcsPurchasePlanOrderStatus.审核通过;
                //合并导出
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                //打印
                case CommonActionKeys.PRINT:
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    return true;
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "BranchId",
                Operator = FilterOperator.IsEqualTo,
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                //新增
                case CommonActionKeys.ADD:
                    var partsPurchasePlan = this.DataEditView.CreateObjectToEdit<PartsPurchasePlan>();
                    partsPurchasePlan.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    partsPurchasePlan.Status = (int)DcsPurchasePlanOrderStatus.新增;
                    partsPurchasePlan.BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    partsPurchasePlan.IsTransSap = false;
                    partsPurchasePlan.IsPack = true;
                    var dataEditView = this.DataEditView as PartsPurchasePlanOrderDataEditView;
                    dataEditView.DataEditPanels.FilePath = null;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                //修改
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                //提交
                case CommonActionKeys.SUBMIT:
                    //此处重写Invoke方法
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Confirm_Submit, () => {
                        try {
                            ShellViewModel.Current.IsBusy = true;
                            var entity = this.DataGridView.SelectedEntities.Cast<PartsPurchasePlan>().SingleOrDefault();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            domainContext.提交配件采购计划订单(entity.Id, invokeOp => {
                                if(invokeOp.HasError) {
                                    if(!invokeOp.IsErrorHandled)
                                        invokeOp.MarkErrorAsHandled();
                                    UIHelper.ShowAlertMessage(invokeOp.ValidationErrors.FirstOrDefault().ErrorMessage);
                                    return;
                                }
                                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Notification_SubmitSuccess);
                                ShellViewModel.Current.IsBusy = false;
                                this.DataGridView.ExecuteQueryDelayed();
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                            ShellViewModel.Current.IsBusy = false;
                        }
                        ShellViewModel.Current.IsBusy = false;
                    });
                    break;
                //初审
                case "FirstApprove":
                    this.DataEditForFirstApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDITFORFIRSTAPPROVE_VIEW);
                    break;
                //审核
                case WorkflowActionKey.INITIALAPPROVE:
                    this.DataEditForApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EEDITFORAPPORVE_VIEW);
                    break;

                //终审
                case WorkflowActionKey.FINALAPPROVE:
                    this.DataEditForTerminateView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EEDITFORTERMINATE_VIEW);
                    break;
                //作废
                case CommonActionKeys.ABANDON:
                    this.AbandodDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.AbandodRadWindow.ShowDialog();
                    break;
                //合并导出
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsPurchasePlan>().Select(r => r.Id).ToArray();
                        this.ExportPartsPurchasePlanOrder(ids, null, null, null, null, null, null, null, null, null, null, null, null);
                    } else {
                        var filtes = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filtes == null)
                            return;
                        var code = filtes.Filters.SingleOrDefault(e => e.MemberName == "Code").Value as string;
                        var sparePartCode = filtes.Filters.SingleOrDefault(e => e.MemberName == "SparePartCode").Value as string;
                    //    var partsSalesCategoryId = filtes.Filters.SingleOrDefault(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                        var status = filtes.Filters.SingleOrDefault(e => e.MemberName == "Status").Value as int?;
                        var PartsPlanTypeId = filtes.Filters.SingleOrDefault(e => e.MemberName == "PartsPlanTypeId").Value as int?;
                        var WarehouseName = filtes.Filters.SingleOrDefault(e => e.MemberName == "WarehouseName").Value as string;
                        var createTime = filtes.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem) && ((CompositeFilterItem)r).Filters.Any(e => e.MemberName == "CreateTime")) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        var ApproveTime = filtes.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem) && ((CompositeFilterItem)r).Filters.Any(e => e.MemberName == "ApproveTime")) as CompositeFilterItem;
                        DateTime? ApproveTimeBegin = null;
                        DateTime? ApproveTimeEnd = null;
                        if(ApproveTime != null) {
                            ApproveTimeBegin = ApproveTime.Filters.First(r => r.MemberName == "ApproveTime").Value as DateTime?;
                            ApproveTimeEnd = ApproveTime.Filters.Last(r => r.MemberName == "ApproveTime").Value as DateTime?;
                        }
                        var CloseTime = filtes.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem) && ((CompositeFilterItem)r).Filters.Any(e => e.MemberName == "CloseTime")) as CompositeFilterItem;
                        DateTime? CloseTimeBegin = null;
                        DateTime? CloseTimeEnd = null;
                        if(CloseTime != null) {
                            CloseTimeBegin = CloseTime.Filters.First(r => r.MemberName == "CloseTime").Value as DateTime?;
                            CloseTimeEnd = CloseTime.Filters.Last(r => r.MemberName == "CloseTime").Value as DateTime?;
                        }
                        this.ExportPartsPurchasePlanOrder(null, code, null, status, PartsPlanTypeId, WarehouseName, createTimeBegin, createTimeEnd, ApproveTimeBegin, ApproveTimeEnd, CloseTimeBegin, CloseTimeEnd, sparePartCode);
                    }
                    break;
                //打印
                case CommonActionKeys.PRINT:
                    //后续开发
                    break;
            }
        }
        //合并导出方法
        private void ExportPartsPurchasePlanOrder(int[] ids, string code, int? PartsSalesCategoryId, int? status, int? PartsPlanTypeId, string WarehouseName, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? ApproveTimeBegin, DateTime? ApproveTimeEnd, DateTime? CloseTimeBegin, DateTime? CloseTimeEnd, string sparePartCode) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsPurchasePlanWithDetailAsync(ids, code, PartsSalesCategoryId, status, PartsPlanTypeId, WarehouseName, createTimeBegin, createTimeEnd, ApproveTimeBegin, ApproveTimeEnd, CloseTimeBegin, CloseTimeEnd, sparePartCode);
            this.excelServiceClient.ExportPartsPurchasePlanWithDetailCompleted += ExcelServiceClient_ExportPartsPurchasePlanWithDetailCompleted;
            this.excelServiceClient.ExportPartsPurchasePlanWithDetailCompleted += ExcelServiceClient_ExportPartsPurchasePlanWithDetailCompleted;
        }

        private void ExcelServiceClient_ExportPartsPurchasePlanWithDetailCompleted(object sender, ExportPartsPurchasePlanWithDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.filename));
        }
    }
}
