﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views {
    [PageMeta("PartsPurchasing", "PartsPurchaseSettle", "PartsPurchaseRtnSettleBill", ActionPanelKeys = new[] {
         CommonActionKeys.ADD_EDIT_ABANDON_APPROVE_EXPORT_IMPORT,"PartsPurchaseRtnSettleBill",CommonActionKeys.PRINT
    })]
    public class PartsPurchaseRtnSettleBillManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataInvoiceRegisterView;
        private const string DATA_INVOICEREGISTER_VIEW = "_dataInvoiceRegisterView_";
        public PartsPurchaseRtnSettleBillManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsPurchasingUIStrings.DataManagementView_Title_PartsPurchaseRtnSettleBill;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsPurchaseRtnSettleBill"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsPurchaseRtnSettleBill");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataInvoiceRegisterView {
            get {
                if(this.dataInvoiceRegisterView == null) {
                    this.dataInvoiceRegisterView = DI.GetDataEditView("PartsPurchaseRtnSettleBillInvoiceRegister");
                    this.dataInvoiceRegisterView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataInvoiceRegisterView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataInvoiceRegisterView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_INVOICEREGISTER_VIEW, () => this.DataInvoiceRegisterView);
        }
        private DcsExportQueryWindowBase partsPurchaseRtnSettleBillCostSearchQueryWindow;

        private DcsExportQueryWindowBase PartsPurchaseRtnSettleBillCostSearchQueryWindow {
            get {
                if(this.partsPurchaseRtnSettleBillCostSearchQueryWindow == null) {
                    this.partsPurchaseRtnSettleBillCostSearchQueryWindow = DI.GetQueryWindow("PartsPurchaseRtnSettleBillCostSearch") as DcsExportQueryWindowBase;
                    this.partsPurchaseRtnSettleBillCostSearchQueryWindow.Loaded += settlementCostSearchQueryWindow_Loaded;
                }
                return this.partsPurchaseRtnSettleBillCostSearchQueryWindow;
            }
        }
        private void settlementCostSearchQueryWindow_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var partsPurchaseRtnSettleBill = this.DataGridView.SelectedEntities.First() as VirtualPartsPurchaseRtnSettleBillWithBusinessCode;
            if(partsPurchaseRtnSettleBill == null)
                return;
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "Code", partsPurchaseRtnSettleBill.Code
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "Code", false
            });
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem("PartsPurchaseRtnSettleBillId", typeof(int), FilterOperator.IsEqualTo, partsPurchaseRtnSettleBill.Id));
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }

        private RadWindow windowDataBase;

        private RadWindow WindowDataBase {
            get {
                return this.windowDataBase ?? (this.windowDataBase = new RadWindow {
                    Content = this.PartsPurchaseRtnSettleBillCostSearchQueryWindow,
                    Header = PartsPurchasingUIStrings.DataManagementView_Title_SettlementCostQuery,
                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                });
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var partsPurchaseRtnSettleBill = this.DataEditView.CreateObjectToEdit<PartsPurchaseRtnSettleBill>();
                    partsPurchaseRtnSettleBill.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    partsPurchaseRtnSettleBill.Status = (int)DcsPartsPurchaseSettleStatus.新建;
                    partsPurchaseRtnSettleBill.TaxRate = 0.13;
                    partsPurchaseRtnSettleBill.BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    partsPurchaseRtnSettleBill.BranchCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                    partsPurchaseRtnSettleBill.BranchName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    partsPurchaseRtnSettleBill.SettlementPath = (int)DcsPartsPurchaseSettleBillSettlementPath.正常结算;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Confirm_Abandon, () => {

                        try {
                            var entity = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseRtnSettleBillWithBusinessCode>().SingleOrDefault();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext != null) {
                                domainContext.作废配件采购退货结算单(entity.Id, invokeOp => {
                                    if(invokeOp.HasError)
                                        return;
                                    UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Notification_AbandonSuccess);
                                    this.DataGridView.ExecuteQueryDelayed();
                                    this.CheckActionsCanExecute();
                                }, null);
                            }
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.APPROVE:
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Confirm_Approve, () => {

                        try {
                            var entity = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseRtnSettleBillWithBusinessCode>().SingleOrDefault();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext != null) {
                                domainContext.审批配件采购退货结算单(entity.Id, invokeOp => {
                                    if(invokeOp.HasError)
                                        return;
                                    UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Notification_ApproveSuccess);
                                    this.DataGridView.ExecuteQueryDelayed();
                                    this.CheckActionsCanExecute();
                                }, null);
                            }
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORT:
                    var domaincontext = this.DataGridView.DomainContext as DcsDomainContext;
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseRtnSettleBillWithBusinessCode>().Select(p => p.Id).ToArray();
                        domaincontext.导出配件采购退货结算单(ids, null, null, null, null, null, null, null, null, null, null, null, null,null,null, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                                ShellViewModel.Current.IsBusy = false;
                            }

                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                ShellViewModel.Current.IsBusy = false;
                            }
                        }, null);
                    } else {
                        var compositeFilterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(compositeFilterItem != null) {
                            var branchId = compositeFilterItem.Filters.Single(r => r.MemberName == "BranchId").Value as int?;
                            var code = compositeFilterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                            var partsSupplierCode = compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSupplierCode").Value as string;
                            var partsSupplierName = compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSupplierName").Value as string;
                            var warehouseid = compositeFilterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                            var settlementPath = compositeFilterItem.Filters.Single(r => r.MemberName == "SettlementPath").Value as int?;
                            var status = compositeFilterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        //    var partsSalesCategoryId = compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                            var creatorName = compositeFilterItem.Filters.Single(r => r.MemberName == "CreatorName").Value as string;
                          //  var businessCode = compositeFilterItem.Filters.Single(r => r.MemberName == "BusinessCode").Value as string;
                            DateTime? createTimeBegin = null;
                            DateTime? createTimeEnd = null;
                            DateTime? invoiceDateBegin = null;
                            DateTime? invoiceDateEnd = null;
                            foreach (var filter in compositeFilterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)))
                            {
                                var dateTime = filter as CompositeFilterItem;
                                if (dateTime != null)
                                {
                                    if (dateTime.Filters.First().MemberName == "CreateTime")
                                    {
                                        createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                        createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    }
                                   
                                    if (dateTime.Filters.First().MemberName == "InvoiceDate")
                                    {
                                        invoiceDateBegin = dateTime.Filters.First(r => r.MemberName == "InvoiceDate").Value as DateTime?;
                                        invoiceDateEnd = dateTime.Filters.Last(r => r.MemberName == "InvoiceDate").Value as DateTime?;
                                    }
                                }
                            }


                            domaincontext.导出配件采购退货结算单(new int[] { }, branchId, code, partsSupplierCode, partsSupplierName, warehouseid, settlementPath, status, null, createTimeBegin, createTimeEnd, creatorName, null,invoiceDateBegin,invoiceDateEnd ,loadOp => {
                                if(loadOp.HasError)
                                    return;
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                    ShellViewModel.Current.IsBusy = false;
                                }

                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                    ShellViewModel.Current.IsBusy = false;
                                }
                            }, null);
                        }
                    }
                    break;
                case CommonActionKeys.PRINT:
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseRtnSettleBillWithBusinessCode>().FirstOrDefault();
                    if(selectedItem == null)
                        return;
                    BasePrintWindow printWindow = new PartsPurchaseRtnSettleBillPrintWindow() {
                        Header = PartsPurchasingUIStrings.DataManagementView_PrintWindow_Title_PartsPurchaseRtnSettleBill,
                        PartsPurchaseRtnSettleBill = selectedItem
                    };
                    printWindow.ShowDialog();
                    break;
                case "InvoiceRegister":
                    this.DataInvoiceRegisterView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_INVOICEREGISTER_VIEW);
                    break;
                case "AntiSettlement":
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Confirm_ReverseSettle, () => {
                        try {
                            var entity = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseRtnSettleBillWithBusinessCode>().SingleOrDefault();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext != null) {
                                domainContext.反结算配件采购退货结算单(entity.Id, invokeOp => {
                                    if(invokeOp.HasError)
                                        return;
                                    UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Notification_ReverseSettle);
                                    this.DataGridView.ExecuteQueryDelayed();
                                    this.CheckActionsCanExecute();
                                }, null);
                            }
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }

                    });
                    break;
                case "SettlementCostSearch":
                    this.WindowDataBase.ShowDialog();
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var id = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseRtnSettleBillWithBusinessCode>().Select(r => r.Id).ToArray();
                        this.ExportPartsPurchaseRtnSettleBillandDetail(id, null, null, null, null, null, null, null, null, null,null,null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var partsSupplierCode = filterItem.Filters.Single(r => r.MemberName == "PartsSupplierCode").Value as string;
                        var partsSupplierName = filterItem.Filters.Single(r => r.MemberName == "PartsSupplierName").Value as string;

                        var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                       // var businessCode = filterItem.Filters.Single(r => r.MemberName == "BusinessCode").Value as string;
                    //    var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;

                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        DateTime? invoiceDateBegin = null;
                        DateTime? invoiceDateEnd = null;
                        foreach (var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)))
                        {
                            var dateTime = filter as CompositeFilterItem;
                            if (dateTime != null)
                            {
                                if (dateTime.Filters.First().MemberName == "CreateTime")
                                {
                                    createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }

                                if (dateTime.Filters.First().MemberName == "InvoiceDate")
                                {
                                    invoiceDateBegin = dateTime.Filters.First(r => r.MemberName == "InvoiceDate").Value as DateTime?;
                                    invoiceDateEnd = dateTime.Filters.Last(r => r.MemberName == "InvoiceDate").Value as DateTime?;
                                }
                            }
                        }
                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        this.ExportPartsPurchaseRtnSettleBillandDetail(null, code, partsSupplierCode, partsSupplierName, warehouseId, status, null, createTimeBegin, createTimeEnd, null, invoiceDateBegin, invoiceDateEnd);
                    }
                    break;
            }
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private void ExportPartsPurchaseRtnSettleBillandDetail(int[] partsPurchaseRtnSettleBillId, string code, string suppliercode, string suppliername, int? warehouseid, int? status, int? partsSalesCategoryId, DateTime? createTimeBegin, DateTime? createTimeEnd, string businessCode, DateTime? invoiceDateBegin, DateTime? invoiceDateEnd)
        {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsPurchaseRtnSettleBillandDetailAsync(partsPurchaseRtnSettleBillId, code, suppliercode, suppliername, warehouseid, status, partsSalesCategoryId, createTimeBegin, createTimeEnd, businessCode, invoiceDateBegin, invoiceDateEnd);
            this.excelServiceClient.ExportPartsPurchaseRtnSettleBillandDetailCompleted -= excelServiceClient_ExportPartsPurchaseRtnSettleBillandDetailCompleted;
            this.excelServiceClient.ExportPartsPurchaseRtnSettleBillandDetailCompleted += excelServiceClient_ExportPartsPurchaseRtnSettleBillandDetailCompleted;
        }

        private void excelServiceClient_ExportPartsPurchaseRtnSettleBillandDetailCompleted(object sender, ExportPartsPurchaseRtnSettleBillandDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.APPROVE:
                case "AntiSettlement":
                case "InvoiceRegister":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseRtnSettleBillWithBusinessCode>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    if(uniqueId == CommonActionKeys.EDIT)
                        return entities[0].Status == (int)DcsPartsPurchaseSettleStatus.新建;
                    if(uniqueId == CommonActionKeys.ABANDON)
                        return entities[0].Status == (int)DcsPartsPurchaseSettleStatus.新建;
                    if(uniqueId == CommonActionKeys.APPROVE)
                        return entities[0].Status == (int)DcsPartsPurchaseSettleStatus.新建;
                    if(uniqueId == "InvoiceRegister")
                        return (entities[0].Status == (int)DcsPartsPurchaseSettleStatus.已审批 && entities[0].SettlementPath != (int)DcsPartsPurchaseSettleBillSettlementPath.反冲结算);
                    if(uniqueId == "AntiSettlement")
                        return (entities[0].Status == (int)DcsPartsPurchaseSettleStatus.发票已审核 && entities[0].SettlementPath != (int)DcsPartsPurchaseSettleBillSettlementPath.反冲结算);
                    return entities[0].Status == (int)DcsPartsPurchaseSettleStatus.新建;
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT:
                case "SettlementCostSearch":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseRtnSettleBillWithBusinessCode>().ToArray();
                    return selectItems.Length == 1;
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var composite = filterItem as CompositeFilterItem;
            if(composite == null)
                return;
            composite.Filters.Add(new FilterItem {
                MemberName = "BranchId",
                Operator = FilterOperator.IsEqualTo,
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            ClientVar.ConvertTime(composite);
            this.DataGridView.FilterItem = composite;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataInvoiceRegisterView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsPurchaseRtnSettleBill"
                };
            }
        }
    }
}
