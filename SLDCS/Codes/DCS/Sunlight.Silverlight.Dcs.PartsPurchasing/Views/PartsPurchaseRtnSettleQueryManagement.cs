﻿using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views {
    [PageMeta("PartsPurchasing", "PartsPurchaseSettle", "PartsPurchaseRtnSettleQuery", ActionPanelKeys = new[] {
         CommonActionKeys.EXPORT
    })]
    public class PartsPurchaseRtnSettleQueryManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        public PartsPurchaseRtnSettleQueryManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsPurchasingUIStrings.DataManagementView_Title_PartsPurchaseRtnSettleQuery;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsPurchaseRtnSettleBill"));
            }
        }


        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    ((DcsDataGridViewBase)this.DataGridView).ExportData();
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var composite = filterItem as CompositeFilterItem;
            if(composite == null)
                return;
            if(!composite.Filters.Any(f=>f.MemberName=="PartsSupplierCode")) {
                var compositeFilterItem = new CompositeFilterItem();
                compositeFilterItem.LogicalOperator = LogicalOperator.And;
                compositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "PartsSupplierCode",
                    MemberType = typeof(string),
                    Operator = FilterOperator.IsEqualTo,
                    Value = BaseApp.Current.CurrentUserData.EnterpriseCode
                });
                composite.Filters.Add(compositeFilterItem);
            }            
            ClientVar.ConvertTime(composite);
            this.DataGridView.FilterItem = composite;
            this.DataGridView.ExecuteQueryDelayed();
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsPurchaseRtnSettleBillForSupplier"
                };
            }
        }
    }
}