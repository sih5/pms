﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views {
    [PageMeta("PartsPurchasing", "PartsPurchaseSettle", "PartsPurchaseSettleForSupplier", ActionPanelKeys = new[] {
         CommonActionKeys.ADD_EDIT_ABANDON_APPROVE_EXPORT_MERGEEXPORT_PRINT,"PartsPurchaseSettleBillForSupplier"
    })]
    public class PartsPurchaseSettleBillForSupplierManagement : DcsDataManagementViewBase {
        private DataEditViewBase dataEditView;
        private DataGridViewBase dataGridView;
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        private const string DATA_EDIT_VIEW_INVOICEREGISTER = "_DataEditViewInvoiceRegister_";
        private const string DATA_EDIT_VIEW_INVOICEEDIT = "_DataEditViewInvoiceEdit_";
        private const string DATA_EDIT_VIEW_MULTIINVOICEREGISTER = "_DataEditViewMultiInvoiceRegister_";
        private const string DATA_EDIT_VIEW_MULTIINVOICEEDIT = "_DataEditViewMultiInvoiceEdit_";

        public PartsPurchaseSettleBillForSupplierManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsPurchasingUIStrings.DataManagementView_Title_PartsPurchasingSettleBillForSupplier;
        }

        public DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsPurchaseSettleBillForSupplier");
                    this.dataEditView.EditSubmitted += dataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewInvoiceRegister = null;
            this.dataEditViewInvoiceEdit = null;
            this.dataEditViewMultiInvoiceRegister = null;
            this.dataEditViewMultiInvoiceEdit = null;
        }
        private void dataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void dataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        public DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsPurchaseSettleBillForSupplier"));
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[]{
                    "PartsPurchaseSettleBillForSupplier"
                };
            }
        }

        private DataEditViewBase dataEditViewInvoiceRegister;
        private DataEditViewBase DataEditViewInvoiceRegister {
            get {
                if(this.dataEditViewInvoiceRegister == null) {
                    this.dataEditViewInvoiceRegister = DI.GetDataEditView("PartsPurchaseSettleBillInvoiceRegister");
                    this.dataEditViewInvoiceRegister.EditCancelled += this.dataEditView_EditCancelled;
                    this.dataEditViewInvoiceRegister.EditSubmitted += this.dataEditView_EditSubmitted;
                }
                return this.dataEditViewInvoiceRegister;
            }
        }

        private DataEditViewBase dataEditViewInvoiceEdit;
        private DataEditViewBase DataEditViewInvoiceEdit {
            get {
                if(this.dataEditViewInvoiceEdit == null) {
                    this.dataEditViewInvoiceEdit = DI.GetDataEditView("InvoiceInformationForEdit");
                    this.dataEditViewInvoiceEdit.EditCancelled += this.dataEditView_EditCancelled;
                    this.dataEditViewInvoiceEdit.EditSubmitted += this.dataEditView_EditSubmitted;
                }
                return this.dataEditViewInvoiceEdit;
            }
        }

        //多条采购结算发票登记
        private DataEditViewBase dataEditViewMultiInvoiceRegister;
        private DataEditViewBase DataEditViewMultiInvoiceRegister {
            get {
                if(this.dataEditViewMultiInvoiceRegister == null) {
                    this.dataEditViewMultiInvoiceRegister = DI.GetDataEditView("PartsPurchaseSettleBillMultiInvoiceRegister");
                    this.dataEditViewMultiInvoiceRegister.EditCancelled += this.dataEditView_EditCancelled;
                    ((PartsPurchaseSettleBillMultiInvoiceRegisterDataEditView)this.dataEditViewMultiInvoiceRegister).EditSubmitted += this.dataEditView_EditSubmitted;
                }
                return this.dataEditViewMultiInvoiceRegister;
            }
        }

        //多条采购结算发票修改
        private DataEditViewBase dataEditViewMultiInvoiceEdit;
        private DataEditViewBase DataEditViewMultiInvoiceEdit {
            get {
                if(this.dataEditViewMultiInvoiceEdit == null) {
                    this.dataEditViewMultiInvoiceEdit = DI.GetDataEditView("PartsPurchaseSettleBillMultiInvoiceEdit");
                    this.dataEditViewMultiInvoiceEdit.EditCancelled += this.dataEditView_EditCancelled;
                    ((PartsPurchaseSettleBillMultiInvoiceEditDataEditView)this.dataEditViewMultiInvoiceEdit).EditSubmitted += this.dataEditView_EditSubmitted;
                }
                return this.dataEditViewMultiInvoiceEdit;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_INVOICEREGISTER, () => this.DataEditViewInvoiceRegister);
            this.RegisterView(DATA_EDIT_VIEW_INVOICEEDIT, () => this.DataEditViewInvoiceEdit);
            this.RegisterView(DATA_EDIT_VIEW_MULTIINVOICEREGISTER, () => this.DataEditViewMultiInvoiceRegister);
            this.RegisterView(DATA_EDIT_VIEW_MULTIINVOICEEDIT, () => this.DataEditViewMultiInvoiceEdit);


        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var partsPurchaseSettleBill = this.DataEditView.CreateObjectToEdit<PartsPurchaseSettleBill>();
                    partsPurchaseSettleBill.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    partsPurchaseSettleBill.CostAmountDifference = 0;
                    partsPurchaseSettleBill.TaxRate = 0.13;
                    partsPurchaseSettleBill.Status = (int)DcsPartsPurchaseSettleStatus.新建;
                    partsPurchaseSettleBill.SettlementPath = (int)DcsPartsPurchaseSettleBillSettlementPath.正常结算;
                    partsPurchaseSettleBill.PartsSupplierId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    partsPurchaseSettleBill.PartsSupplierName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    partsPurchaseSettleBill.PartsSupplierCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.APPROVE:
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Confirm_Approve, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsPurchaseSettleBill>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can审批配件采购结算单)
                                entity.审批配件采购结算单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Notification_ApproveSuccess);
                                if(this.DataGridView.FilterItem != null)
                                    this.DataGridView.ExecuteQueryDelayed();
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsPurchaseSettleBill>().SingleOrDefault();
                        if(entity == null)
                            return;
                        if(entity.Status != (int)DcsPartsPurchaseSettleStatus.新建) {
                            UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Text_StatusChanged);
                            return;
                        }
                        try {
                            if(entity.Can作废配件采购结算单)
                                entity.作废配件采购结算单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.PRINT:
                case "PrintForGC":
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<PartsPurchaseSettleBill>().FirstOrDefault();
                    if(selectedItem == null)
                        return;
                    if(uniqueId == "PrintForGC") {
                        BasePrintWindow printWindow = new PartsPurchaseSettleBillPrintForGCWindow {
                            Header = PartsPurchasingUIStrings.DataManagementView_PrintWindow_Title_PartsOutAndInDetail,
                            PartsPurchaseSettleBill = selectedItem
                        };
                        printWindow.ShowDialog();
                    } else {
                        BasePrintWindow printWindow = new PartsPurchaseSettleBillPrintWindow {
                            Header = PartsPurchasingUIStrings.DataManagementView_PrintWindow_Title_PartsPurchaseSettleBill,
                            PartsPurchaseSettleBill = selectedItem
                        };
                        printWindow.ShowDialog();
                    }
                    break;
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsPurchaseSettleBill>().Select(r => r.Id).ToArray();
                        this.dcsDomainContext.ExportPartsPurchaseSettleBill(ids, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, "导出配件采购结算单", loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                                ShellViewModel.Current.IsBusy = false;
                            }

                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                ShellViewModel.Current.IsBusy = false;
                            }
                        }, null);
                    } else {
                        var compositeFilterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(compositeFilterItem != null) {
                            var code = compositeFilterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                            var warehouseId = compositeFilterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                            var branchName = compositeFilterItem.Filters.Single(r => r.MemberName == "BranchName").Value as string;
                            var settlementPath = compositeFilterItem.Filters.Single(r => r.MemberName == "SettlementPath").Value as int?;
                            var partsSalesCategoryId = compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                            var status = compositeFilterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                            var createTime = compositeFilterItem.Filters.Select(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                            DateTime? startDateTime = null;
                            DateTime? endDateTime = null;
                            DateTime? startInvoiceApproveTime = null;
                            DateTime? endInvoiceApproveTime = null;
                            if(createTime != null) {
                                startDateTime = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                endDateTime = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                startInvoiceApproveTime = createTime.Filters.First(r => r.MemberName == "InvoiceApproveTime").Value as DateTime?;
                                endInvoiceApproveTime = createTime.Filters.Last(r => r.MemberName == "InvoiceApproveTime").Value as DateTime?;

                            }
                            this.dcsDomainContext.ExportPartsPurchaseSettleBill(new int[] { }, null, branchName, BaseApp.Current.CurrentUserData.EnterpriseId, code, warehouseId, null, null, settlementPath, partsSalesCategoryId, startDateTime, endDateTime, status, null, startInvoiceApproveTime, endInvoiceApproveTime, null, "导出配件采购结算单", loadOp => {
                                if(loadOp.HasError)
                                    return;
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                    ShellViewModel.Current.IsBusy = false;
                                }

                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                    ShellViewModel.Current.IsBusy = false;
                                }
                            }, null);
                        }
                    }
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsPurchaseSettleBill>().Select(r => r.Id).ToArray();
                        ShellViewModel.Current.IsBusy = true;
                        this.dcsDomainContext.ExportPartsPurchaseSettleBillWithDetailBySupplier(ids, null, null, null, null, null, null, null, null, null, "合并导出配件采购结算单", loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                                ShellViewModel.Current.IsBusy = false;
                            }

                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                ShellViewModel.Current.IsBusy = false;
                            }
                        }, null);
                    } else {
                        var compositeFilterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(compositeFilterItem != null) {
                            var code = compositeFilterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                            var warehouseId = compositeFilterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                            var branchName = compositeFilterItem.Filters.Single(r => r.MemberName == "BranchName").Value as string;
                            var settlementPath = compositeFilterItem.Filters.Single(r => r.MemberName == "SettlementPath").Value as int?;
                            var partsSalesCategoryId = compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                            var status = compositeFilterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                            var createTime = compositeFilterItem.Filters.FirstOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                            DateTime? startDateTime = null;
                            DateTime? endDateTime = null;
                            if(createTime != null) {
                                startDateTime = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                endDateTime = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                              
                            }
                            ShellViewModel.Current.IsBusy = true;
                            this.dcsDomainContext.ExportPartsPurchaseSettleBillWithDetailBySupplier(new int[] { },  branchName, BaseApp.Current.CurrentUserData.EnterpriseId, code, warehouseId, settlementPath, partsSalesCategoryId, startDateTime, endDateTime, status, "导出配件采购结算单", loadOp => {
                                if(loadOp.HasError)
                                    return;
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                    ShellViewModel.Current.IsBusy = false;
                                }
                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                    ShellViewModel.Current.IsBusy = false;
                                }
                            }, null);
                        }
                    }
                    break;
                case "InvoiceRegister":
                    var selectId = this.DataGridView.SelectedEntities.Cast<PartsPurchaseSettleBill>().Select(c => c.Id).ToArray();
                    //单张采购结算单
                    if(selectId.Length == 1) {
                        this.DataEditViewInvoiceRegister.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                        this.SwitchViewTo(DATA_EDIT_VIEW_INVOICEREGISTER);
                    } else {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsPurchaseSettleBill>().ToArray();
                        var check = entity.GroupBy(r => new {
                            r.PartsSupplierId,
                            r.PartsSalesCategoryId,
                            r.BranchId
                        }).Count();
                        if(check != 1) {
                            UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Text_MergeInvoiceError);
                            return;
                        }
                        this.DataEditViewMultiInvoiceRegister.SetObjectToEditById(selectId);
                        this.SwitchViewTo(DATA_EDIT_VIEW_MULTIINVOICEREGISTER);
                    }
                    break;
                case "InvoiceEdit":
                    var editPartsPurchaseSettleBill = this.DataGridView.SelectedEntities.Cast<PartsPurchaseSettleBill>().First();
                    if(editPartsPurchaseSettleBill == null)
                        return;
                    if(editPartsPurchaseSettleBill.InvoiceFlag == (int)DcsInvoiceFlag.多结算单一发票) {
                        this.DataEditViewMultiInvoiceEdit.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                        this.SwitchViewTo(DATA_EDIT_VIEW_MULTIINVOICEEDIT);
                    } else {
                        this.DataEditViewInvoiceEdit.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                        this.SwitchViewTo(DATA_EDIT_VIEW_INVOICEEDIT);
                    }
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.APPROVE:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsPurchaseSettleBill>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsPartsPurchaseSettleStatus.新建;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT:
                case "PrintForGC":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems = this.DataGridView.SelectedEntities.Cast<PartsPurchaseSettleBill>().ToArray();
                    return selectItems.Length == 1;
                case "InvoiceRegister":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitity = this.DataGridView.SelectedEntities.Cast<PartsPurchaseSettleBill>().ToArray();
                    if(entitity.Length != 1)
                        return false;
                    return (entitity[0].Status == (int)DcsPartsPurchaseSettleStatus.已审批 && entitity[0].SettlementPath != (int)DcsPartsPurchaseSettleBillSettlementPath.反冲结算);
                case "InvoiceEdit":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitie = this.DataGridView.SelectedEntities.Cast<PartsPurchaseSettleBill>().ToArray();
                    if(entitie.Length != 1)
                        return false;
                    return (entitie[0].Status == (int)DcsPartsPurchaseSettleStatus.发票驳回 || entitie[0].Status == (int)DcsPartsPurchaseSettleStatus.发票登记);
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var newCompositeFilterItem = filterItem as CompositeFilterItem;
            if(newCompositeFilterItem != null) {
                newCompositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "PartsSupplierId",
                    Operator = FilterOperator.IsEqualTo,
                    MemberType = typeof(int),
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });
                ClientVar.ConvertTime(newCompositeFilterItem);
                this.DataGridView.FilterItem = newCompositeFilterItem;
            }
            this.DataGridView.ExecuteQueryDelayed();

        }
    }
}
