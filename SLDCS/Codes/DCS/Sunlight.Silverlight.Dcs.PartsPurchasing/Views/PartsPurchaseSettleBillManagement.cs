﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views {
    [PageMeta("PartsPurchasing", "PartsPurchaseSettle", "PartsPurchaseSettleBill", ActionPanelKeys = new[] {
         CommonActionKeys.ADD_EDIT_ABANDON_APPROVE_EXPORT_MERGEEXPORT_PRINT,"PartsPurchaseSettleBill",CommonActionKeys.SETTLEMENTCOSTQUERY
    })]
    public class PartsPurchaseSettleBillManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditViewInvoiceRegister;
        private DataEditViewBase dataEditViewInvoiceEdit;
        private DataEditViewBase dataEditViewInvoiceApprove;
        private DataEditViewBase dataEditViewMultiInvoiceRegister;
        private DataEditViewBase dataEditViewMultiInvoiceEdit;
        private DataEditViewBase dataEditViewMultiInvoiceApprove;
        private const string DATA_EDIT_VIEW_INVOICEREGISTER = "_DataEditViewInvoiceRegister_";
        private const String DATA_EDIT_VIEW_INVOICEAPPROVE = "_DataEditViewInvoiceApprove_";
        private const string DATA_EDIT_VIEW_INVOICEEDIT = "_DataEditViewInvoiceEdit_";
        private const string DATA_EDIT_VIEW_MULTIINVOICEREGISTER = "_DataEditViewMultiInvoiceRegister_";
        private const string DATA_EDIT_VIEW_MULTIINVOICEEDIT = "_DataEditViewMultiInvoiceEdit_";
        private const String DATA_EDIT_VIEW_MULTIINVOICEAPPROVE = "_DataEditViewMultiInvoiceApprove_";


        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();

        public PartsPurchaseSettleBillManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsPurchasingUIStrings.DataManagementView_Title_PartsPurchaseSettleBill;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsPurchaseSettleBill"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsPurchaseSettleBill");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataEditViewInvoiceRegister {
            get {
                if(this.dataEditViewInvoiceRegister == null) {
                    this.dataEditViewInvoiceRegister = DI.GetDataEditView("PartsPurchaseSettleBillInvoiceRegister");
                    this.dataEditViewInvoiceRegister.EditCancelled += this.DataEditView_EditCancelled;
                    //this.dataEditViewInvoiceRegister.EditSubmitted += this.DataEditView_EditSubmitted;
                    var item = (PartsPurchaseSettleBillInvoiceRegisterDataEditView)this.dataEditViewInvoiceRegister;
                    item.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataEditViewInvoiceRegister;
            }
        }

        private DataEditViewBase DataEditViewInvoiceApprove {
            get {
                if(this.dataEditViewInvoiceApprove == null) {
                    this.dataEditViewInvoiceApprove = DI.GetDataEditView("PartsPurchaseSettleBillInvoiceApprove");
                    this.dataEditViewInvoiceApprove.EditCancelled += this.DataEditView_EditCancelled;
                    this.dataEditViewInvoiceApprove.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataEditViewInvoiceApprove;
            }
        }

        private DataEditViewBase DataEditViewInvoiceEdit {
            get {
                if(this.dataEditViewInvoiceEdit == null) {
                    this.dataEditViewInvoiceEdit = DI.GetDataEditView("InvoiceInformationForEdit");
                    this.dataEditViewInvoiceEdit.EditCancelled += this.DataEditView_EditCancelled;
                    this.dataEditViewInvoiceEdit.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataEditViewInvoiceEdit;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewInvoiceRegister = null;
            this.dataEditViewInvoiceEdit = null;
            this.dataEditViewInvoiceApprove = null;
            this.dataEditViewMultiInvoiceRegister = null;
            this.dataEditViewMultiInvoiceEdit = null;
            this.dataEditViewMultiInvoiceApprove = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }
        //多条采购结算发票登记
        private DataEditViewBase DataEditViewMultiInvoiceRegister {
            get {
                if(this.dataEditViewMultiInvoiceRegister == null) {
                    this.dataEditViewMultiInvoiceRegister = DI.GetDataEditView("PartsPurchaseSettleBillMultiInvoiceRegister");
                    this.dataEditViewMultiInvoiceRegister.EditCancelled += this.DataEditView_EditCancelled;
                    var item = (PartsPurchaseSettleBillMultiInvoiceRegisterDataEditView)this.dataEditViewMultiInvoiceRegister;
                    item.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataEditViewMultiInvoiceRegister;
            }
        }

        //多条采购结算发票修改
        private DataEditViewBase DataEditViewMultiInvoiceEdit {
            get {
                if(this.dataEditViewMultiInvoiceEdit == null) {
                    this.dataEditViewMultiInvoiceEdit = DI.GetDataEditView("PartsPurchaseSettleBillMultiInvoiceEdit");
                    this.dataEditViewMultiInvoiceEdit.EditCancelled += this.DataEditView_EditCancelled;
                    ((PartsPurchaseSettleBillMultiInvoiceEditDataEditView)this.dataEditViewMultiInvoiceEdit).EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataEditViewMultiInvoiceEdit;
            }
        }

        //多条采购结算发票审批
        private DataEditViewBase DataEditViewMultiInvoiceApprove {
            get {
                if(this.dataEditViewMultiInvoiceApprove == null) {
                    this.dataEditViewMultiInvoiceApprove = DI.GetDataEditView("PartsPurchaseSettleBillMultiInvoiceApprove");
                    this.dataEditViewMultiInvoiceApprove.EditCancelled += this.DataEditView_EditCancelled;
                    this.dataEditViewMultiInvoiceApprove.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataEditViewMultiInvoiceApprove;
            }
        }



        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_INVOICEREGISTER, () => this.DataEditViewInvoiceRegister);
            this.RegisterView(DATA_EDIT_VIEW_INVOICEAPPROVE, () => this.DataEditViewInvoiceApprove);
            this.RegisterView(DATA_EDIT_VIEW_INVOICEEDIT, () => this.DataEditViewInvoiceEdit);
            this.RegisterView(DATA_EDIT_VIEW_MULTIINVOICEREGISTER, () => this.DataEditViewMultiInvoiceRegister);
            this.RegisterView(DATA_EDIT_VIEW_MULTIINVOICEEDIT, () => this.DataEditViewMultiInvoiceEdit);
            this.RegisterView(DATA_EDIT_VIEW_MULTIINVOICEAPPROVE, () => this.DataEditViewMultiInvoiceApprove);


        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var partsPurchaseSettleBill = this.DataEditView.CreateObjectToEdit<PartsPurchaseSettleBill>();
                    partsPurchaseSettleBill.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    partsPurchaseSettleBill.CostAmountDifference = 0;
                    partsPurchaseSettleBill.TaxRate = 0.13;
                    partsPurchaseSettleBill.BranchCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                    partsPurchaseSettleBill.BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    partsPurchaseSettleBill.BranchName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    partsPurchaseSettleBill.Status = (int)DcsPartsPurchaseSettleStatus.新建;
                    partsPurchaseSettleBill.SettlementPath = (int)DcsPartsPurchaseSettleBillSettlementPath.正常结算;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseSettleBillWithSumPlannedPrice>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废虚拟配件采购结算单)
                                entity.作废虚拟配件采购结算单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Notification_AbandonSuccess);
                                if(this.DataGridView.FilterItem != null)
                                    this.DataGridView.ExecuteQueryDelayed();
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.APPROVE:
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Confirm_Approve, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseSettleBillWithSumPlannedPrice>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can审批配件采购结算单)
                                entity.审批配件采购结算单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Notification_ApproveSuccess);
                                if(this.DataGridView.FilterItem != null)
                                    this.DataGridView.ExecuteQueryDelayed();
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "InvoiceRegister":
                    var selectId = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseSettleBillWithSumPlannedPrice>().Select(c => c.Id).ToArray();
                    //单张采购结算单
                    if(selectId.Length == 1) {
                        this.DataEditViewInvoiceRegister.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                        this.SwitchViewTo(DATA_EDIT_VIEW_INVOICEREGISTER);
                    } else {
                        var entity = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseSettleBillWithSumPlannedPrice>().ToArray();
                        var check = entity.GroupBy(r => new {
                            r.PartsSupplierId,
                            r.PartsSalesCategoryId,
                            r.BranchId
                        }).Count();
                        if(check != 1) {
                            UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Text_MergeInvoiceError);
                            return;
                        }
                        this.DataEditViewMultiInvoiceRegister.SetObjectToEditById(selectId);
                        this.SwitchViewTo(DATA_EDIT_VIEW_MULTIINVOICEREGISTER);
                    }
                    break;
                case "InvoiceApprove":
                    var approvePartsPurchaseSettleBill = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseSettleBillWithSumPlannedPrice>().First();
                    if(approvePartsPurchaseSettleBill == null)
                        return;
                    if(approvePartsPurchaseSettleBill.InvoiceFlag == (int)DcsInvoiceFlag.多结算单一发票) {
                        this.DataEditViewMultiInvoiceApprove.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                        this.SwitchViewTo(DATA_EDIT_VIEW_MULTIINVOICEAPPROVE);
                    } else {
                        this.DataEditViewInvoiceApprove.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                        this.SwitchViewTo(DATA_EDIT_VIEW_INVOICEAPPROVE);
                    }
                    break;
                case "AntiSettlement":
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Confirm_AntiSettlement, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseSettleBillWithSumPlannedPrice>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can反结算配件采购结算单)
                                entity.反结算配件采购结算单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Notification_AntiSettlementSuccess);
                                if(this.DataGridView.FilterItem != null)
                                    this.DataGridView.ExecuteQueryDelayed();
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.PRINT:
                case "PrintForGC":
                    var partsPurchaseSettleBillIds = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseSettleBillWithSumPlannedPrice>().Select(r => r.Id).First();
                    this.dcsDomainContext.Load(dcsDomainContext.GetPartsPurchaseSettleBillsQuery().Where(ex => ex.Id == partsPurchaseSettleBillIds), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError)
                            return;
                        var entity = loadOp.Entities.SingleOrDefault();
                        if(entity == null)
                            return;
                        if(uniqueId == "PrintForGC") {
                            BasePrintWindow printWindow = new PartsPurchaseSettleBillPrintForGCWindow {
                                Header = PartsPurchasingUIStrings.DataManagementView_PrintWindow_Title_PartsOutAndInDetail,
                                PartsPurchaseSettleBill = entity
                            };
                            printWindow.ShowDialog();
                        } else {
                            BasePrintWindow printWindow = new PartsPurchaseSettleBillPrintWindow {
                                Header = PartsPurchasingUIStrings.DataManagementView_PrintWindow_Title_PartsPurchaseSettleBill,
                                PartsPurchaseSettleBill = entity
                            };
                            printWindow.ShowDialog();
                        }
                    }, null);
                    break;
                case CommonActionKeys.SETTLEMENTCOSTQUERY:
                    this.WindowDataBase.ShowDialog();
                    break;
                case CommonActionKeys.EXPORT:
                    ShellViewModel.Current.IsBusy = true;
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseSettleBillWithSumPlannedPrice>().Select(r => r.Id).ToArray();
                        this.dcsDomainContext.ExportPartsPurchaseSettleBillByUnifiedSettle(ids, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, "导出配件采购结算单",null,null, loadOp => {
                            if(loadOp.HasError) {
                                ShellViewModel.Current.IsBusy = false;
                                return;
                            }
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                                ShellViewModel.Current.IsBusy = false;
                            }

                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                ShellViewModel.Current.IsBusy = false;
                            }
                        }, null);
                    } else {
                        var compositeFilterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(compositeFilterItem != null) {
                            var code = compositeFilterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                            var warehouseId = compositeFilterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                            var partsSupplierCode = compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSupplierCode").Value as string;
                            var partsSupplierName = compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSupplierName").Value as string;
                            var settlementPath = compositeFilterItem.Filters.Single(r => r.MemberName == "SettlementPath").Value as int?;
                           // var partsSalesCategoryId = compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                            var status = compositeFilterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                            var creatorName = compositeFilterItem.Filters.Single(r => r.MemberName == "CreatorName").Value as string;
                          //  var businessCode = compositeFilterItem.Filters.Single(r => r.MemberName == "BusinessCode").Value as string;                           
                            //var createTime = compositeFilterItem.Filters.First(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                            //var invoiceApprove = compositeFilterItem.Filters.Last(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                            DateTime? startDateTime = null;
                            DateTime? endDateTime = null;
                            DateTime? startInvoiceApproveTime = null;
                            DateTime? endInvoiceApproveTime = null;
                            DateTime? startInvoiceDate = null;
                            DateTime? endInvoiceDate = null;
                            foreach (var filter in compositeFilterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)))
                            {
                                var dateTime = filter as CompositeFilterItem;
                                if (dateTime != null)
                                {
                                    if (dateTime.Filters.First().MemberName == "CreateTime")
                                    {
                                        startDateTime = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                        endDateTime = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    }
                                    if (dateTime.Filters.First().MemberName == "InvoiceApproveTime")
                                    {
                                        startInvoiceApproveTime = dateTime.Filters.First(r => r.MemberName == "InvoiceApproveTime").Value as DateTime?;
                                        endInvoiceApproveTime = dateTime.Filters.Last(r => r.MemberName == "InvoiceApproveTime").Value as DateTime?;
                                    }
                                    if (dateTime.Filters.First().MemberName == "InvoiceDate")
                                    {
                                        startInvoiceDate = dateTime.Filters.First(r => r.MemberName == "InvoiceDate").Value as DateTime?;
                                        endInvoiceDate = dateTime.Filters.Last(r => r.MemberName == "InvoiceDate").Value as DateTime?;
                                    }
                                }
                            }
                            this.dcsDomainContext.ExportPartsPurchaseSettleBillByUnifiedSettle(new int[] { }, BaseApp.Current.CurrentUserData.EnterpriseId, null, null, code, warehouseId, partsSupplierCode, partsSupplierName, settlementPath, null, startDateTime, endDateTime, status, creatorName, startInvoiceApproveTime, endInvoiceApproveTime, null, "导出配件采购结算单",startInvoiceDate,endInvoiceDate, loadOp => {
                                if(loadOp.HasError) {
                                    ShellViewModel.Current.IsBusy = false;
                                    return;
                                }
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                    ShellViewModel.Current.IsBusy = false;
                                }

                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                    ShellViewModel.Current.IsBusy = false;
                                }
                            }, null);
                        }
                    }
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    ShellViewModel.Current.IsBusy = true;
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseSettleBillWithSumPlannedPrice>().Select(r => r.Id).ToArray();
                        this.dcsDomainContext.ExportPartsPurchaseSettleBillWithDetailByUnifiedSettle(ids, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, "合并导出配件采购结算单",null,null, loadOp => {
                            if(loadOp.HasError) {
                                ShellViewModel.Current.IsBusy = false;
                                return;
                            }
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                                ShellViewModel.Current.IsBusy = false;
                            }

                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                ShellViewModel.Current.IsBusy = false;
                            }
                        }, null);
                    } else {
                        var compositeFilterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(compositeFilterItem != null) {
                            var code = compositeFilterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                            var warehouseId = compositeFilterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                            var partsSupplierCode = compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSupplierCode").Value as string;
                            var partsSupplierName = compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSupplierName").Value as string;
                            var settlementPath = compositeFilterItem.Filters.Single(r => r.MemberName == "SettlementPath").Value as int?;
                            //var partsSalesCategoryId = compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                            var status = compositeFilterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                          //  var businessCode = compositeFilterItem.Filters.Single(r => r.MemberName == "BusinessCode").Value as string;
                            var creatorName = compositeFilterItem.Filters.Single(r => r.MemberName == "CreatorName").Value as string;
                            CompositeFilterItem time = null;
                            CompositeFilterItem invoiceApprove = null;
                            if(compositeFilterItem.Filters.Any(r => r.GetType() == typeof(CompositeFilterItem))) {
                                time = compositeFilterItem.Filters.First(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                                invoiceApprove = compositeFilterItem.Filters.Last(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                            }
                            //var time = compositeFilterItem.Filters.First(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                            //var invoiceApprove = compositeFilterItem.Filters.Last(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                            DateTime? startDateTime = null;
                            DateTime? endDateTime = null;
                            DateTime? startInvoiceApproveTime = null;
                            DateTime? endInvoiceApproveTime = null;
                            DateTime? startInvoiceDate = null;
                            DateTime? endInvoiceDate = null;
                            foreach (var filter in compositeFilterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)))
                            {
                                var dateTime = filter as CompositeFilterItem;
                                if (dateTime != null)
                                {
                                    if (dateTime.Filters.First().MemberName == "CreateTime")
                                    {
                                        startDateTime = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                        endDateTime = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    }
                                    if (dateTime.Filters.First().MemberName == "InvoiceApproveTime")
                                    {
                                        startInvoiceApproveTime = dateTime.Filters.First(r => r.MemberName == "InvoiceApproveTime").Value as DateTime?;
                                        endInvoiceApproveTime = dateTime.Filters.Last(r => r.MemberName == "InvoiceApproveTime").Value as DateTime?;
                                    }
                                    if (dateTime.Filters.First().MemberName == "InvoiceDate")
                                    {
                                        startInvoiceDate = dateTime.Filters.First(r => r.MemberName == "InvoiceDate").Value as DateTime?;
                                        endInvoiceDate = dateTime.Filters.Last(r => r.MemberName == "InvoiceDate").Value as DateTime?;
                                    }
                                }
                            }
                            this.dcsDomainContext.ExportPartsPurchaseSettleBillWithDetailByUnifiedSettle(new int[] { }, BaseApp.Current.CurrentUserData.EnterpriseId, null, null, code, warehouseId, partsSupplierCode, partsSupplierName, settlementPath, null, startDateTime, endDateTime, status, creatorName, startInvoiceApproveTime, endInvoiceApproveTime, null, "合并导出配件采购结算单",startInvoiceDate,endInvoiceDate, loadOp => {
                                if(loadOp.HasError) {
                                    ShellViewModel.Current.IsBusy = false;
                                    return;
                                }
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                    ShellViewModel.Current.IsBusy = false;
                                }

                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                    ShellViewModel.Current.IsBusy = false;
                                }
                            }, null);
                        }
                    }
                    break;
                case "InvoiceEdit":
                    var editPartsPurchaseSettleBill = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseSettleBillWithSumPlannedPrice>().First();
                    if(editPartsPurchaseSettleBill == null)
                        return;
                    if(editPartsPurchaseSettleBill.InvoiceFlag == (int)DcsInvoiceFlag.多结算单一发票) {
                        this.DataEditViewMultiInvoiceEdit.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                        this.SwitchViewTo(DATA_EDIT_VIEW_MULTIINVOICEEDIT);
                    } else {
                        this.DataEditViewInvoiceEdit.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                        this.SwitchViewTo(DATA_EDIT_VIEW_INVOICEEDIT);
                    }
                    break;
                case "InvoiceSettlement":
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Confirm_AntiSettlement, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseSettleBillWithSumPlannedPrice>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can发票已审核反结算)
                                entity.发票已审核反结算();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Notification_AntiSettlementSuccess);
                                if(this.DataGridView.FilterItem != null)
                                    this.DataGridView.ExecuteQueryDelayed();
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
            }
        }

        private DcsExportQueryWindowBase settlementCostSearchQueryWindow;

        private DcsExportQueryWindowBase SettlementCostSearchQueryWindow {
            get {
                if(this.settlementCostSearchQueryWindow == null) {
                    this.settlementCostSearchQueryWindow = DI.GetQueryWindow("SettlementCostQuery") as DcsExportQueryWindowBase;
                    this.settlementCostSearchQueryWindow.Loaded += settlementCostSearchQueryWindow_Loaded;
                }
                return this.settlementCostSearchQueryWindow;
            }
        }

        private void settlementCostSearchQueryWindow_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var partsPurchaseSettleBill = this.DataGridView.SelectedEntities.First() as VirtualPartsPurchaseSettleBillWithSumPlannedPrice;
            if(partsPurchaseSettleBill == null)
                return;
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "Code", partsPurchaseSettleBill.Code
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "Code", false
            });
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem("PartsPurchaseSettleBillId", typeof(int), FilterOperator.IsEqualTo, partsPurchaseSettleBill.Id));
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }

        private RadWindow windowDataBase;

        private RadWindow WindowDataBase {
            get {
                return this.windowDataBase ?? (this.windowDataBase = new RadWindow {
                    Content = this.SettlementCostSearchQueryWindow,
                    Header = "结算成本查询",
                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                });
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.APPROVE:
                case "InvoiceEdit":
                case "InvoiceApprove":
                case "AntiSettlement":
                case "InvoiceSettlement":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseSettleBillWithSumPlannedPrice>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    if(uniqueId == "InvoiceEdit") {
                        return (entities[0].Status == (int)DcsPartsPurchaseSettleStatus.发票驳回 || entities[0].Status == (int)DcsPartsPurchaseSettleStatus.发票登记);
                    }
                    if(uniqueId == "AntiSettlement")
                        return (entities[0].Status == (int)DcsPartsPurchaseSettleStatus.发票登记 && entities[0].SettlementPath != (int)DcsPartsPurchaseSettleBillSettlementPath.反冲结算);
                    if(uniqueId == "InvoiceSettlement")
                        return (entities[0].Status == (int)DcsPartsPurchaseSettleStatus.发票已审核);
                    if(uniqueId == "InvoiceApprove")
                        return ((entities[0].Status == (int)DcsPartsPurchaseSettleStatus.发票登记 || entities[0].Status == (int)DcsPartsPurchaseSettleStatus.发票驳回) && entities[0].SettlementPath != (int)DcsPartsPurchaseSettleBillSettlementPath.反冲结算);
                    if(uniqueId == CommonActionKeys.ABANDON)
                        return entities[0].Status == (int)DcsPartsPurchaseSettleStatus.新建;//|| entities[0].Status != (int)DcsPartsPurchaseSettleStatus.已审批
                    return entities[0].Status == (int)DcsPartsPurchaseSettleStatus.新建;
                case "InvoiceRegister":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var allEntitity = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseSettleBillWithSumPlannedPrice>().ToArray();
                    return allEntitity.All(r => r.Status == (int)DcsPartsPurchaseSettleStatus.已审批 && r.SettlementPath != (int)DcsPartsPurchaseSettleBillSettlementPath.反冲结算);
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT:
                case "PrintForGC":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseSettleBillWithSumPlannedPrice>().ToArray();
                    return selectItems.Length == 1;
                case CommonActionKeys.SETTLEMENTCOSTQUERY:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItem = this.DataGridView.SelectedEntities.Cast<VirtualPartsPurchaseSettleBillWithSumPlannedPrice>().ToArray();
                    return selectItem.Length == 1;
                default:
                    return false;

            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "BranchId",
                Operator = FilterOperator.IsEqualTo,
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsPurchaseSettleBill"
                };
            }
        }
    }
}
