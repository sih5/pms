﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views {
    [PageMeta("PartsPurchasing", "InternalSettlement", "PartsRequisitionSettleBillForOut", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_APPROVE_MERGEEXPORT_PRINT})]

    public class PartsRequisitionSettleBillForOutManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        public PartsRequisitionSettleBillForOutManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsPurchasingUIStrings.DataManagementView_Title_PartsRequisitionSettleBillForOut;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsRequisitionSettleBill");
                    this.dataEditView.EditSubmitted += this.dataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void dataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void dataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsRequisitionSettleBillForOut"));
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var partsRequisitionSettleBill = this.DataEditView.CreateObjectToEdit<PartsRequisitionSettleBill>();
                    partsRequisitionSettleBill.BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    partsRequisitionSettleBill.BranchCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                    partsRequisitionSettleBill.BranchName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    partsRequisitionSettleBill.Status = (int)DcsPartsRequisitionSettleBillStatus.新建;
                    partsRequisitionSettleBill.SettleType = (int)DcsRequisitionSettleType.领出;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsRequisitionSettleBillExport>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废配件领用结算单)
                                entity.作废配件领用结算单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.APPROVE:
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Confirm_Approve, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsRequisitionSettleBillExport>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can审批配件领用结算单)
                                entity.审批配件领用结算单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Notification_ApproveSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.PRINT:
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<PartsRequisitionSettleBillExport>().FirstOrDefault();
                    if(selectedItem == null)
                        return;
                    BasePrintWindow printWindow = new PartsRequisitionSettleBillPrintWindow {
                        Header = PartsPurchasingUIStrings.DataManagementView_PrintWindow_Title_PartsRequisitionSettleBill,
                        PartsRequisitionSettleBill = selectedItem
                    };
                    printWindow.ShowDialog();
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsRequisitionSettleBillExport>().Select(r => r.Id).ToArray();
                        this.dcsDomainContext.合并导出配件领用结算单(ids, null, null, null, null, null, null, null, (int)DcsRequisitionSettleType.领出,null,null,null, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                                ShellViewModel.Current.IsBusy = false;
                            }

                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                ShellViewModel.Current.IsBusy = false;
                            }
                        }, null);
                    } else {
                        var compositeFilterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(compositeFilterItem != null) {
                            var code = compositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : compositeFilterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                            //var partsSalesCategoryId = compositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSalesCategoryId") == null ? null : compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                            var departmentId = compositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "DepartmentId") == null ? null : compositeFilterItem.Filters.Single(r => r.MemberName == "DepartmentId").Value as int?;
                            var status = compositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "Status") == null ? null : compositeFilterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                            var type = compositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "Type") == null ? null : compositeFilterItem.Filters.Single(r => r.MemberName == "Type").Value as int?;

                            DateTime? startDateTime = null;
                            DateTime? endDateTime = null;
                            DateTime? invoiceDateBegin = null;
                            DateTime? invoiceDateEnd = null;
                            foreach (var filter in compositeFilterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)))
                            {
                                var dateTime = filter as CompositeFilterItem;
                                if (dateTime != null)
                                {
                                    if (dateTime.Filters.First().MemberName == "CreateTime")
                                    {
                                        startDateTime = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                        endDateTime = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    }

                                    if (dateTime.Filters.First().MemberName == "InvoiceDate")
                                    {
                                        invoiceDateBegin = dateTime.Filters.First(r => r.MemberName == "InvoiceDate").Value as DateTime?;
                                        invoiceDateEnd = dateTime.Filters.Last(r => r.MemberName == "InvoiceDate").Value as DateTime?;
                                    }
                                }
                            }
                            this.dcsDomainContext.合并导出配件领用结算单(new int[] { }, null, null, departmentId, startDateTime, endDateTime, code, status, (int)DcsRequisitionSettleType.领出,invoiceDateBegin,invoiceDateEnd,type, loadOp => {
                                if(loadOp.HasError)
                                    return;
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                    ShellViewModel.Current.IsBusy = false;
                                }

                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                    ShellViewModel.Current.IsBusy = false;
                                }
                            }, null);
                        }
                    }
                    break;
            }
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilter = filterItem as CompositeFilterItem;
            if(compositeFilter == null)
                return;
            compositeFilter.Filters.Add(new FilterItem {
                MemberName = "BranchId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            ClientVar.ConvertTime(compositeFilter);
            this.DataGridView.FilterItem = compositeFilter;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.APPROVE:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<PartsRequisitionSettleBillExport>().Any(r => r.Status == (int)DcsPartsRequisitionSettleBillStatus.新建);
                case CommonActionKeys.PRINT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems = this.DataGridView.SelectedEntities.Cast<PartsRequisitionSettleBillExport>().ToArray();
                    return selectItems.Length == 1;
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsRequisitionSettleBill"
                };
            }
        }
    }
}
