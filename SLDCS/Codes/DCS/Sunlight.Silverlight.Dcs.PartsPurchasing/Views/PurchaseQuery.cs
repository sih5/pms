﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsPurchasing", "PartsPurchasing", "PurchaseQuery", ActionPanelKeys = new[] { CommonActionKeys.EXPORT })]
    public class PurchaseQuery : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        public PurchaseQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsPurchasingUIStrings.DataManagementView_Title_PurchaseQuery;
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PurchaseQuery"));
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PurchaseQuery"
                };
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    ShellViewModel.Current.IsBusy = true;
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var branchId = filterItem.Filters.Single(e => e.MemberName == "BranchId").Value as int?;
                    var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                    var partCode = filterItem.Filters.Single(r => r.MemberName == "PartCode").Value as string;
                    var partName = filterItem.Filters.Single(r => r.MemberName == "PartName").Value as string;
                    var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    if(createTime != null) {
                        createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                        createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    }

                    this.dcsDomainContext.ExportPurchaseQuery(branchId, partsSalesCategoryId, partCode, partName, createTimeBegin, createTimeEnd, loadOp => {
                        if(loadOp.HasError)
                            return;
                        if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }


        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
