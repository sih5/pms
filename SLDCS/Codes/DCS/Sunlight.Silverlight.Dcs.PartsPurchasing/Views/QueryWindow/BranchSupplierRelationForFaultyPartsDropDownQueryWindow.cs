﻿
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.QueryWindow {
    /// <summary>
    /// 选择祸首件供应商
    /// </summary>
    public class BranchSupplierRelationForFaultyPartsDropDownQueryWindow : DcsDropDownQueryWindowBase {
        public override string Title {
            get {
                return PartsPurchasingUIStrings.QueryPanel_Title_BranchSupplierRelationForFaulty;
            }
        }
        public override string DataGridViewKey {
            get {
                return "BranchSupplierRelationForFaultyParts";
            }
        }

        public override string QueryPanelKey {
            get {
                return "BranchSupplierRelationForFaultyParts";
            }
        }
    }
}
