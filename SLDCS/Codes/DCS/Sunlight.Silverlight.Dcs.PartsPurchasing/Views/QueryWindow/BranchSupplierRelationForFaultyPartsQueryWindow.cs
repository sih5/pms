﻿
namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.QueryWindow {
    /// <summary>
    /// 选择祸首件供应商
    /// </summary>
    public class BranchSupplierRelationForFaultyPartsQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "BranchSupplierRelationForFaultyParts";
            }
        }

        public override string QueryPanelKey {
            get {
                return "BranchSupplierRelationForFaultyParts";
            }
        }
    }
}
