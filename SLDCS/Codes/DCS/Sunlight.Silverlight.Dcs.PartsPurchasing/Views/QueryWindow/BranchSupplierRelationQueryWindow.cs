﻿namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.QueryWindow {
    /// <summary>
    /// 选择分公司与供应商（关系）
    /// </summary>
    public class BranchSupplierRelationQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "BranchSupplierRelationForQuery";
            }
        }

        public override string QueryPanelKey {
            get {
                return "BranchSupplierRelationForQuery";
            }
        }
    }
}
