﻿using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.QueryWindow {
    public class InternalAllocationBillDropDownQueryWindow : DcsDropDownQueryWindowBase {
        public override string Title {
            get {
                return PartsPurchasingUIStrings.QueryPanel_Title_InternalAllocationBill;
            }
        }

        public override string DataGridViewKey {
            get {
                return "InternalAllocationBill";
            }
        }

        public override string QueryPanelKey {
            get {
                return "InternalAllocationBillForQuery";
            }
        }
    }
}
