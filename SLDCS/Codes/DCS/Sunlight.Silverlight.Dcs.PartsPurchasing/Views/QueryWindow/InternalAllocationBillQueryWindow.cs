﻿
namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.QueryWindow {
    /// <summary>
    /// 选择领出单
    /// </summary>
    public class InternalAllocationBillQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "InternalAllocationBill";
            }
        }

        public override string QueryPanelKey {
            get {
                return "InternalAllocationBillForQuery";
            }
        }
    }
}
