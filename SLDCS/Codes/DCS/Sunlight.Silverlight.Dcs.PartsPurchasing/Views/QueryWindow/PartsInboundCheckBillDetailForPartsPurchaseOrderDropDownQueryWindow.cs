﻿using System;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.QueryWindow {
    public class PartsInboundCheckBillDetailForPartsPurchaseOrderDropDownQueryWindow : DcsDropDownQueryWindowBase {

        public override string Title {
            get {
                return PartsPurchasingUIStrings.QueryPanel_Title_PartsInboundCheckBillDetail;
            }
        }

        public override string DataGridViewKey {
            get {
                throw new NotImplementedException();
            }
        }

        public override string QueryPanelKey {
            get {
                return "PartsInboundCheckBillDetailForPartsPurchaseOrder";
            }
        }
    }
}
