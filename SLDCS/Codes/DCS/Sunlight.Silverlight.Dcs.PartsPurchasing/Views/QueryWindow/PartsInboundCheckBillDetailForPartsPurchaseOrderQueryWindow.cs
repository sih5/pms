﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Controls;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Views.Custom;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.QueryWindow {
    /// <summary>
    /// 根据采购订单选择入库检验明细
    /// </summary>
    public class PartsInboundCheckBillDetailForPartsPurchaseOrderQueryWindow : DcsMultiSelectQueryWindowBase {

        public override string QueryPanelKey {
            get {
                return "PartsInboundCheckBillDetailForPartsPurchaseOrder";
            }
        }

        public override string[] DataGridViewKeies {
            get {
                return new[] {
                    ""
                };
            }
        }

        protected override UserControlBase CustomerView {
            get {
                return this.customerView ?? (this.customerView = new PartsInboundCheckBillDetailForPartsPurchaseOrderView());
            }
        }

        public override string[] GridViewTitles {
            get {
                return new[] {
                    ""
                };
            }
        }

        protected override void QueryPanel_ExecutingQuery(object sender, FilterPanelRibbonGroup.ExecutingQueryEventArgs e) {
            if(CustomerView != null) {
                var multiSelectView = this.CustomerView as PartsInboundCheckBillDetailForPartsPurchaseOrderView;
                if(multiSelectView != null) {
                    multiSelectView.ExecuteQuery(e.Filter);
                }
            }
        }

        public override IEnumerable<Entity> SelectedEntities {
            get {
                if(customerView != null) {
                    var multiSelectView = this.CustomerView as PartsInboundCheckBillDetailForPartsPurchaseOrderView;
                    if(multiSelectView != null) {
                        return multiSelectView.SelectedEntities;
                    }
                }
                return Enumerable.Empty<Entity>();
            }
        }
    }
}
