﻿namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.QueryWindow {
    /// <summary>
    /// 选择采购订单清单
    /// </summary>
    public class PartsPurchaseOrderDetailQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "PartsPurchaseOrderDetailForSelect";
            }
        }

        public override string QueryPanelKey {
            get {
                return "PartsPurchaseOrderDetailForSelect";
            }
        }
    }
}
