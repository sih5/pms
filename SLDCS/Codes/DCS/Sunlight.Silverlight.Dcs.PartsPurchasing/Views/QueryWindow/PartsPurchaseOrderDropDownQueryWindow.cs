﻿using System;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.QueryWindow {
    /// <summary>
    /// 选择配件采购订单
    /// </summary>
    public class PartsPurchaseOrderDropDownQueryWindow : DcsDropDownQueryWindowBase {

        public PartsPurchaseOrderDropDownQueryWindow() {
            this.SetDefaultFilterItem(new FilterItem("PartsSupplierId", typeof(int), FilterOperator.IsEqualTo, BaseApp.Current.CurrentUserData.EnterpriseId));
        }

        public override string Title {
            get {
                return PartsPurchasingUIStrings.QueryPanel_Title_PartsPurchaseOrder;
            }
        }

        public override string DataGridViewKey {
            get {
                return "PartsPurchaseOrderForSelect";
            }
        }

        public override string QueryPanelKey {
            get {
                return "PartsPurchaseOrderForSelect";
            }
        }

        protected override FilterItem OnRequestFilterItem(FilterItem filterItem) {
            var compositeFilterItem = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                var compositeFilter = filterItem as CompositeFilterItem;
                var createTimeFilters = compositeFilter.Filters.Where(filter => filter is CompositeFilterItem && (filter as CompositeFilterItem).Filters.All(item => item.MemberName == "CreateTime")).SingleOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                if(createTimeFilters != null) {
                    var dateTime = createTimeFilters.Filters.ElementAt(1).Value as DateTime?;
                    if(dateTime.HasValue)
                        createTimeFilters.Filters.ElementAt(1).Value = new DateTime(dateTime.Value.Year, dateTime.Value.Month, dateTime.Value.Day, 23, 59, 59);
                }
                compositeFilterItem.Filters.Add(compositeFilter);
            } else
                compositeFilterItem.Filters.Add(filterItem);
            filterItem = compositeFilterItem;
            return base.OnRequestFilterItem(filterItem);
        }
    }
}
