﻿using System;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.QueryWindow {
    public class PartsPurchaseRtnSettleBillCostSearchQueryWindow : DcsExportQueryWindowBase {
        public PartsPurchaseRtnSettleBillCostSearchQueryWindow() {
            this.Export -= PartsPurchaseRtnSettleBillCostSearchQueryWindow_Export;
            this.Export += PartsPurchaseRtnSettleBillCostSearchQueryWindow_Export;
        }
        private void PartsPurchaseRtnSettleBillCostSearchQueryWindow_Export(object sender, EventArgs e) {
            ((DcsDataGridViewBase)this.DataGridView).ExportData();
        }
        public override string DataGridViewKey {
            get {
                return "PartsPurchaseRtnSettleBillCostSearch";
            }
        }

        public override string QueryPanelKey {
            get {
                return "PartsPurchaseRtnSettleBillCostSearch";
            }
        }
    }
}
