﻿

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.QueryWindow {
    /// <summary>
    /// 选择配件采购结算单(多选)
    /// </summary>
    public class PartsPurchaseSettleBillForSelectMultiQueryWindow : DcsMultiPopupsQueryWindowBase {
        public override string QueryPanelKey {
            get {

                return "PartsPurchaseSettleBillForSelect";
            }
        }

        public override string DataGridViewKey {
            get {
                return "PartsPurchaseSettleBillForSelect";
            }
        }
    }
}

