﻿using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.QueryWindow {
    public class PartsSupplierByPartIdQueryWindow : DcsDropDownQueryWindowBase {
        public PartsSupplierByPartIdQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string Title {
            get {
                return "供应商查询";
            }
        }

        public override string DataGridViewKey {
            get {
                return "PartsSupplierByPartIds";
            }
        }

        public override string QueryPanelKey {
            get {
                return "PartsSupplierByPartIds";
            }
        }
    }
}
