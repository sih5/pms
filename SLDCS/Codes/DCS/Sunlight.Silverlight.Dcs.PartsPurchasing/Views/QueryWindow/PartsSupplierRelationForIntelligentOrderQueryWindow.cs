﻿
namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.QueryWindow {
    /// <summary>
    /// 选择配件与供应商（关系）
    /// </summary>
    public class PartsSupplierRelationForIntelligentOrderQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "PartsSupplierRelationForIntelligentOrder";
            }
        }

        public override string QueryPanelKey {
            get {
                return "PartsSupplierRelationForIntelligentOrder";
            }
        }
    }
}
