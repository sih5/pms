﻿using System;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.QueryWindow {
    public class SettlementCostQueryQueryWindow : DcsExportQueryWindowBase {
        public SettlementCostQueryQueryWindow() {
            this.Export -= SettlementCostSearchQueryWindow_Export;
            this.Export += SettlementCostSearchQueryWindow_Export;
        }

        private void SettlementCostSearchQueryWindow_Export(object sender, EventArgs e) {
            ((DcsDataGridViewBase)this.DataGridView).ExportData();
        }

        public override string DataGridViewKey {
            get {
                return "SettlementCostQuery";
            }
        }

        public override string QueryPanelKey {
            get {
                return "SettlementCostQuery";
            }
        }
    }
}
