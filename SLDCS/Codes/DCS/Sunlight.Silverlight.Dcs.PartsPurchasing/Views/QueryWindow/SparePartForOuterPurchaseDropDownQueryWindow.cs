﻿using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.QueryWindow {
    /// <summary>
    /// 选择配件
    /// </summary>
    public class SparePartForOuterPurchaseDropDownQueryWindow : DcsDropDownQueryWindowBase {

        public override string Title {
            get {
                return PartsPurchasingUIStrings.QueryPanel_Title_SalePriceSelectPartsInfo;
            }
        }

        public override string DataGridViewKey {
            get {
                return "SparePartsDataForOuterPurchase";
            }
        }

        public override string QueryPanelKey {
            get {
                return "SparePartsDataForOuterPurchase";
            }
        }

        public SparePartForOuterPurchaseDropDownQueryWindow() {
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }
    }
}
