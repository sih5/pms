﻿
namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.QueryWindow {
    /// <summary>
    /// 选择配件(多选)
    /// </summary>
    public class SparePartForOuterPurchaseMulitQueryWindow : DcsMultiPopupsQueryWindowBase {


        public override string QueryPanelKey {
            get {

                return "SparePartsDataForOuterPurchase";
            }
        }

        public override string DataGridViewKey {
            get {
                return "SparePartsDataForOuterPurchase";
            }
        }
    }
}
