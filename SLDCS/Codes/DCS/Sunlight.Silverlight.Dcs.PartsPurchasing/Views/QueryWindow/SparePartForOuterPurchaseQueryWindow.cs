﻿
namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.QueryWindow {
    public class SparePartForOuterPurchaseQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "SparePartsDataForOuterPurchase";
            }
        }
        public override string QueryPanelKey {
            get {
                return "SparePartsDataForOuterPurchase";
            }
        }

    }
}
