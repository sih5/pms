﻿using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.QueryWindow {
    /// <summary>
    /// 采购订单选择配件
    /// </summary>
    public class SparePartForPartsPurchaseOrderDropDownQueryWindow : DcsDropDownQueryWindowBase {

        public SparePartForPartsPurchaseOrderDropDownQueryWindow() {
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string DataGridViewKey {
            get {
                return "SparePartForPartsPurchaseOrder";
            }
        }

        public override string QueryPanelKey {
            get {
                return "SparePartForPartsPurchaseOrder";
            }
        }

        public override string Title {
            get {
                return PartsPurchasingUIStrings.QueryPanel_Title_SparePartByPartsPurchaseOrder;
            }
        }
    }
}
