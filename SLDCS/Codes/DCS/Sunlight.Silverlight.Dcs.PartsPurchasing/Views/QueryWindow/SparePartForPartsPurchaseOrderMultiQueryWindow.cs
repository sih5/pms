﻿


namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.QueryWindow {
    /// <summary>
    /// 选择配件采购订单多选弹出框
    /// </summary>
    public class SparePartForPartsPurchaseOrderMultiQueryWindow : DcsMultiPopupsQueryWindowBase {
        public SparePartForPartsPurchaseOrderMultiQueryWindow() {
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string QueryPanelKey {
            get {
                return "SparePartForPartsPurchaseOrder";

            }
        }

        public override string DataGridViewKey {
            get {
                return "SparePartForPartsPurchaseOrder";
            }
        }
    }
}
