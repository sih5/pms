﻿namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.QueryWindow {
    /// <summary>
    /// 采购订单选择配件
    /// </summary>
    public class SparePartForPartsPurchaseOrderQueryWindow : DcsQueryWindowBase {
        public SparePartForPartsPurchaseOrderQueryWindow() {
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string DataGridViewKey {
            get {
                return "SparePartForPartsPurchaseOrder";
            }
        }

        public override string QueryPanelKey {
            get {
                return "SparePartForPartsPurchaseOrder";
            }
        }
    }
}
