﻿


namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.QueryWindow {
    /// <summary>
    /// 选择配件采购订单多选弹出框
    /// </summary>
    public class SparePartForPartsPurchasePlanMultiQueryWindow : DcsMultiPopupsQueryWindowBase {
        public SparePartForPartsPurchasePlanMultiQueryWindow() {
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string QueryPanelKey {
            get {
                return "SparePartForPartsPurchasePlan";

            }
        }

        public override string DataGridViewKey {
            get {
                return "SparePartForPartsPurchasePlan";
            }
        }
    }
}
