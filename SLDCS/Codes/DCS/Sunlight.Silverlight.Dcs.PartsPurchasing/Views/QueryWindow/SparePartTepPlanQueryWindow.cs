﻿using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views.QueryWindow {
    public class SparePartTepPlanQueryWindow : DcsMultiPopupsQueryWindowBase {
        public SparePartTepPlanQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string DataGridViewKey {
            get {
                return "SparePartTepPlan";
            }
        }

        public override string QueryPanelKey {
            get {
                return "SparePartForWindow";
            }
        }
    }
}