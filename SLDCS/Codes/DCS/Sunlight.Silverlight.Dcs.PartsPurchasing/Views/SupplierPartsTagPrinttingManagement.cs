﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views {
    [PageMeta("PartsPurchasing", "PartsPurchasing", "SupplierPartsTagPrintting", ActionPanelKeys = new[] {
           "SupplierShippingPrintLabel"
    })]
    public class SupplierPartsTagPrinttingManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;

        private readonly string[] kvNames = {
                   "SparePart_MeasureUnit"
        };
        private ObservableCollection<KeyValuePair> kvMeasureUnit;
        private ObservableCollection<KeyValuePair> KvMeasureUnit {
            get {
                return this.kvMeasureUnit ?? (this.kvMeasureUnit = new ObservableCollection<KeyValuePair>());
            }
        }
        private KeyValueManager keyValueManager;
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        public SupplierPartsTagPrinttingManagement() {
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData(() => {
                foreach(var item in this.KeyValueManager[kvNames[0]])
                    KvMeasureUnit.Add(item);
            });
            this.Initializer.Register(this.Initialize);
            this.Title = PartsPurchasingUIStrings.DataManagementView_Title_SupplierPartsTagPrintting;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("SupplierPartsTagPrintting"));
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "SupplierPartsTagPrintting"
                };
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            var newFilterItem = new CompositeFilterItem();
            var compositeFilterItem = filterItem as CompositeFilterItem;
            int? MeasureUnitValue;
            if(compositeFilterItem != null) {
                foreach(var item in compositeFilterItem.Filters) {
                    if(item.MemberName == "MeasureUnit") {
                        MeasureUnitValue = item.Value as int?;
                        if(MeasureUnitValue.HasValue && MeasureUnitValue != -1) {
                            string MeasureUnitName = KvMeasureUnit.FirstOrDefault(r => r.Key == MeasureUnitValue.Value).Value;
                            newFilterItem.Filters.Add(new FilterItem("MeasureUnit", typeof(string), FilterOperator.IsEqualTo, MeasureUnitName));
                        }
                        continue;
                    }
                    newFilterItem.Filters.Add(item);
                }
            }
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = newFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case "PrintLabel":
                case "UsedPrintLabel":
                case "StandardPrintLabel":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItem = this.DataGridView.SelectedEntities.Cast<SparePart>().ToArray();
                    return selectItem.Length == 1;
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            var spareParts = this.DataGridView.SelectedEntities.Select(r => (int)r.GetIdentity()).ToArray();
            switch(uniqueId) {
                case "PrintLabel":
                    this.SupplierPartsTagPrinttingDataEditView.SetObjectToEditById(spareParts);
                    LabelPrintWindow.ShowDialog();
                    break;
                case "UsedPrintLabel":
                    this.usedSupplierPartsTagPrinttingDataEditView.SetObjectToEditById(spareParts);
                    usedLabelPrintWindow.ShowDialog();
                    break;
                case "StandardPrintLabel":
                     this.StandardPartsTagPrinttingDataEditView.SetObjectToEditById(spareParts);
                     StandardPrintLabelPrintWindow.ShowDialog();
                    break;

            }
        }

        //标签打印（带标准）
        private RadWindow standardPrintLabelPrintWindow;
        private DataEditViewBase standardPartsTagPrinttingDataEditView;

        private DataEditViewBase StandardPartsTagPrinttingDataEditView {
            get {
                return this.standardPartsTagPrinttingDataEditView ?? (this.standardPartsTagPrinttingDataEditView = DI.GetDataEditView("StandardPartsTagPrintting"));
            }
        }

        private RadWindow StandardPrintLabelPrintWindow {
            get {
                return this.standardPrintLabelPrintWindow ?? (this.standardPrintLabelPrintWindow = new RadWindow {
                    Content = this.StandardPartsTagPrinttingDataEditView,
                    Header = PartsPurchasingUIStrings.DataManagementView_Text_PartInformation,
                    WindowState = WindowState.Normal,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                    AllowDrop = false,
                    ResizeMode = ResizeMode.NoResize,
                    CanClose = true
                });
            }
        }

        //配件标签打印
        private RadWindow labelPrintWindow;
        private DataEditViewBase supplierPartsTagPrinttingDataEditView;

        private DataEditViewBase SupplierPartsTagPrinttingDataEditView {
            get {
                return this.supplierPartsTagPrinttingDataEditView ?? (this.supplierPartsTagPrinttingDataEditView = DI.GetDataEditView("SupplierPartsTagPrintting"));
            }
        }
        private RadWindow LabelPrintWindow {
            get {
                return this.labelPrintWindow ?? (this.labelPrintWindow = new RadWindow {
                    Content = this.SupplierPartsTagPrinttingDataEditView,
                    Header = PartsPurchasingUIStrings.DataManagementView_Text_PartInformation,
                    WindowState = WindowState.Normal,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                    AllowDrop = false,
                    ResizeMode = ResizeMode.NoResize,
                    CanClose = true
                });
            }
        }

        //配件标签打印(旧)
        private RadWindow usedlabelPrintWindow;
        private DataEditViewBase usedsupplierPartsTagPrinttingDataEditView;

        private DataEditViewBase usedSupplierPartsTagPrinttingDataEditView {
            get {
                return this.usedsupplierPartsTagPrinttingDataEditView ?? (this.usedsupplierPartsTagPrinttingDataEditView = DI.GetDataEditView("UsedSupplierPartsTagPrintting"));
            }
        }
        private RadWindow usedLabelPrintWindow {
            get {
                return this.usedlabelPrintWindow ?? (this.usedlabelPrintWindow = new RadWindow {
                    Content = this.usedSupplierPartsTagPrinttingDataEditView,
                    Header = PartsPurchasingUIStrings.DataManagementView_Text_PartInformation,
                    WindowState = WindowState.Normal,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                    AllowDrop = false,
                    ResizeMode = ResizeMode.NoResize,
                    CanClose = true
                });
            }
        }
    }
}
