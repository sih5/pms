﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views {
    [PageMeta("PartsPurchasing", "PartsPurchasing", "SupplierPreApprovedLoan", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON,"SupplierPreApprovedLoan"
    })]
    public class SupplierPreApprovedLoanManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public SupplierPreApprovedLoanManagement() {
            this.Initializer.Register(this.Initializ);
            this.Title = PartsPurchasingUIStrings.DataManagementView_Title_SupplierPreApprovedLoan;
        }

        private void Initializ() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("SupplierPreApprovedLoan"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("SupplierPreApprovedLoan");
                    this.dataEditView.EditCancelled += dataEditView_EditCancelled;
                    this.dataEditView.EditSubmitted += dataEditView_EditSubmitted;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void dataEditView_EditSubmitted(object sender, System.EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQuery();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void dataEditView_EditCancelled(object sender, System.EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "SupplierPreApprovedLoan"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var supplierPreApprovedLoan = this.DataEditView.CreateObjectToEdit<SupplierPreApprovedLoan>();
                    supplierPreApprovedLoan.Status = (int)DcsBaseDataStatus.有效;
                    supplierPreApprovedLoan.PreApprovedCode = GlobalVar.ASSIGNED_BY_SERVER;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case "ExportDetail":
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<SupplierPreApprovedLoan>().ToArray().Select(r => r.Id).ToArray();
                        this.ExportPartsShiftOrderWithDetail(ids, null, null, null, null);
                    } else {
                        var compositeFilterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(compositeFilterItem == null)
                            return;
                        var preApprovedCode = compositeFilterItem.Filters.Single(r => r.MemberName == "PreApprovedCode").Value as string;
                        var partsSalesCategoryId = compositeFilterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                        var createTime = compositeFilterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        this.ExportPartsShiftOrderWithDetail(null, preApprovedCode, partsSalesCategoryId, createTimeBegin, createTimeEnd);
                    }
                    break;
                case "PrintAll":
                case "ZeroPrint":
                case "NonZeroPrint":
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return;
                    var selectEntity = this.DataGridView.SelectedEntities.Cast<SupplierPreApprovedLoan>().SingleOrDefault();
                    BasePrintWindow printWindow = new SupplierPreApprovedLoanPrintWindow {
                        Header = PartsPurchasingUIStrings.DataManagementView_PrintWindow_Title_SupplierPreApprovedLoan,
                        SupplierPreApprovedLoan = selectEntity,
                        PrintType = uniqueId
                    };
                    printWindow.ShowDialog();
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entityForAbandon = this.DataGridView.SelectedEntities.Cast<SupplierPreApprovedLoan>().SingleOrDefault();
                        if(entityForAbandon == null)
                            return;
                        try {
                            if(entityForAbandon.Can作废供应商货款预批单)
                                entityForAbandon.作废供应商货款预批单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError)
                                    submitOp.MarkErrorAsHandled();
                                else
                                    UIHelper.ShowAlertMessage(PartsPurchasingUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception e) {
                            UIHelper.ShowAlertMessage(e.Message);
                        }
                    });
                    break;
                case "Print":
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return;
                    var entityPrint = this.DataGridView.SelectedEntities.Cast<SupplierPreApprovedLoan>().SingleOrDefault();
                    BasePrintWindow print = new SupplierPreApprovedLoanDetailPrintWindow {
                        Header = PartsPurchasingUIStrings.DataManagementView_PrintWindow_Title_SupplierPreApprovedLoan,
                        SupplierPreApprovedLoan = entityPrint,
                    };
                    print.ShowDialog();
                    break;

            }
        }

        private void ExportPartsShiftOrderWithDetail(int[] ids, string preApprovedCode, int? partsSalesCategoryId, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportSupplierPreApprovedLoanWithDetailAsync(ids, preApprovedCode, partsSalesCategoryId, createTimeBegin, createTimeEnd);
            this.excelServiceClient.ExportSupplierPreApprovedLoanWithDetailCompleted -= excelServiceClient_ExportSupplierPreApprovedLoanWithDetailCompleted;
            this.excelServiceClient.ExportSupplierPreApprovedLoanWithDetailCompleted += excelServiceClient_ExportSupplierPreApprovedLoanWithDetailCompleted;

        }

        private void excelServiceClient_ExportSupplierPreApprovedLoanWithDetailCompleted(object sender, ExportSupplierPreApprovedLoanWithDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.filename));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<SupplierPreApprovedLoan>().First().Status == (int)DcsBaseDataStatus.有效;
                case "PrintAll":
                case "Print":
                case "ZeroPrint":
                case "NonZeroPrint":
                    return this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Count() == 1;
                case "ExportDetail":
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
            }
            return false;
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQuery();
        }
    }
}
