﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Web.Entities;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views {
    [PageMeta("PartsPurchasing", "PartsPurchasing", "SupplierShippingOrderForBranch", ActionPanelKeys = new[] {
        CommonActionKeys.CONFIRM_TERMINATE_EXPORT_PRINT_MERGEEXPORT_EDIT, "SupplierShippingOrder","SupplierShippingPrintLabel"
    })]
    public class SupplierShippingOrderForBranchManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataForBranchView;
        private DataEditViewBase dataEditViewForEdit;
        private const string DATA_INVOICEREGISTER_VIEW = "_dataInvoiceRegisterView_";
        private const string DATAEDITVIEWFOREDIT = "_dataEditViewForEdit_";
              
        public SupplierShippingOrderForBranchManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsPurchasingUIStrings.DataManagementView_Title_SupplierShippingOrderForBranch;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("SupplierShippingOrderWithDetailsForBranch"));
            }
        }
        private DataEditViewBase DataEditViewForEdit {
            get {
                if(dataEditViewForEdit == null) {
                    dataEditViewForEdit = DI.GetDataEditView("SupplierShippingOrderForBranchEdit");
                    dataEditViewForEdit.EditSubmitted += this.DataEditView_EditSubmitted;
                    dataEditViewForEdit.EditCancelled += this.DataEditView_EditCancelled;
                }
                return dataEditViewForEdit;
            }
        }
        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("SupplierShippingOrderForBranch");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase SupplierShippingOrderForBranchDataEditView {
            get {
                if(this.dataForBranchView == null) {
                    this.dataForBranchView = DI.GetDataEditView("SupplierShippingOrderPendingConfirmationSupplier");
                    this.dataForBranchView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataForBranchView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataForBranchView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_INVOICEREGISTER_VIEW, () => this.SupplierShippingOrderForBranchDataEditView);
            this.RegisterView(DATAEDITVIEWFOREDIT, () => this.DataEditViewForEdit);
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataForBranchView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "SupplierShippingOrderForBranch"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EDIT:
                    this.DataEditViewForEdit.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATAEDITVIEWFOREDIT);
                    break;
                case CommonActionKeys.CONFIRM:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.TERMINATE:
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Confirm_Terminate, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<VirtualSupplierShippingOrder>().SingleOrDefault();
                        if(entity == null)
                            return;
                       
                        try {
                            if(entity.Can终止供应商发运单)
                                entity.终止供应商发运单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Notification_TerminateSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORT:
                    ((DcsDataGridViewBase)this.DataGridView).ExportData();
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<VirtualSupplierShippingOrder>().Select(r => r.Id).ToArray();
                        this.ExecuteMergeExport(ids, null, null, null, null, null, null, null, null, null);
                    } else {
                        var fil = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(fil == null)
                            return;
                        var filterItem = fil.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        var partsSupplierCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSupplierCode") == null ? null : filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSupplierCode").Value as string;
                        var partsSupplierName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSupplierName") == null ? null : filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSupplierName").Value as string;
                     //   var partsSalesCategoryId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSalesCategoryId") == null ? null : filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                        var receivingWarehouseId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "ReceivingWarehouseId") == null ? null : filterItem.Filters.SingleOrDefault(r => r.MemberName == "ReceivingWarehouseId").Value as int?;
                        var code = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : filterItem.Filters.SingleOrDefault(r => r.MemberName == "Code").Value as string;
                        var partsPurchaseOrderCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsPurchaseOrderCode") == null ? null : filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsPurchaseOrderCode").Value as string;
                        var status = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Status") == null ? null : filterItem.Filters.SingleOrDefault(r => r.MemberName == "Status").Value as int?;
                        var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        ShellViewModel.Current.IsBusy = true;
                        this.ExecuteMergeExport(null, partsSupplierCode, partsSupplierName, null, receivingWarehouseId, code, partsPurchaseOrderCode, status, createTimeBegin, createTimeEnd);
                    }
                    break;

                case CommonActionKeys.PRINT:
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<VirtualSupplierShippingOrder>().FirstOrDefault();
                    if(selectedItem == null)
                        return;
                    if(selectedItem.IfDirectProvision) {
                        //不打印二维码
                        SunlightPrinter.ShowPrinter(PartsPurchasingUIStrings.DataManagementView_Text_SupplierShippingPrint, "SupplierShippingOrderNoBarSih", null, true, new Tuple<string, string>("supplierShippingOrderId", selectedItem.Id.ToString()), new Tuple<string, string>("UserName", BaseApp.Current.CurrentUserData.UserName));
                    } else {
                        SunlightPrinter.ShowPrinter(PartsPurchasingUIStrings.DataManagementView_Text_SupplierShippingPrint, "SupplierShippingOrderSih", null, true, new Tuple<string, string>("supplierShippingOrderId", selectedItem.Id.ToString()), new Tuple<string, string>("UserName", BaseApp.Current.CurrentUserData.UserName));
                    }

                    break;
                case "A4Print":
                      var selectedA4 = this.DataGridView.SelectedEntities.Cast<VirtualSupplierShippingOrder>().FirstOrDefault();
                      if(selectedA4 == null)
                        return;
                      SunlightPrinter.ShowPrinter(PartsPurchasingUIStrings.DataManagementView_Text_SupplierShippingPrint, "SupplierShippingOrderNoBarSihA4", null, true, new Tuple<string, string>("supplierShippingOrderId", selectedA4.Id.ToString()), new Tuple<string, string>("UserName", BaseApp.Current.CurrentUserData.UserName));
                    
                    break;
                case "PendingConfirmationSupplier":
                    this.SupplierShippingOrderForBranchDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_INVOICEREGISTER_VIEW);
                    break;
                case "PrintLabel":
                    var supplierShippingOrders = this.DataGridView.SelectedEntities.Select(r => (int)r.GetIdentity()).ToArray();
                    this.PrintLabelForSupplierShippingOrderDataEditView.SetObjectToEditById(supplierShippingOrders);
                    LabelPrintWindow.ShowDialog();
                    break;
            }
        }
        //配件标签打印
        private RadWindow labelPrintWindow;
        private DataEditViewBase printLabelForSupplierShippingOrderDataEditView;

        private DataEditViewBase PrintLabelForSupplierShippingOrderDataEditView {
            get {
                return this.printLabelForSupplierShippingOrderDataEditView ?? (this.printLabelForSupplierShippingOrderDataEditView = DI.GetDataEditView("PrintLabelForSupplierShippingOrder"));
            }
        }

        private RadWindow LabelPrintWindow {
            get {
                return this.labelPrintWindow ?? (this.labelPrintWindow = new RadWindow {
                    Content = this.PrintLabelForSupplierShippingOrderDataEditView,
                    Header = PartsPurchasingUIStrings.DataManagementView_Text_PartInformation,
                    WindowState = WindowState.Normal,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                    AllowDrop = false,
                    ResizeMode = ResizeMode.NoResize,
                    CanClose = true
                });
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EDIT:
                     if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entityEdit = this.DataGridView.SelectedEntities.Cast<VirtualSupplierShippingOrder>().ToArray();
                    if(entityEdit.Length != 1)
                        return false;
                    return entityEdit[0].Status == (int)DcsSupplierShippingOrderStatus.新建;
                case "PendingConfirmationSupplier":
                case CommonActionKeys.CONFIRM:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entity = this.DataGridView.SelectedEntities.Cast<VirtualSupplierShippingOrder>().ToArray();
                    if(entity.Length != 1)
                        return false;
                    if(entity[0].Status == (int)DcsSupplierShippingOrderStatus.终止) {
                        return false;
                    }
                    if(uniqueId == "PendingConfirmationSupplier")
                        return entity[0].Status == (int)DcsSupplierShippingOrderStatus.新建 && entity[0].IfDirectProvision && entity[0].DirectProvisionFinished == false;
                    if((entity[0].Status == (int)DcsSupplierShippingOrderStatus.新建 || entity[0].Status == (int)DcsSupplierShippingOrderStatus.部分确认) && entity[0].IfDirectProvision == false)
                        return true;
                    return (entity[0].Status == (int)DcsSupplierShippingOrderStatus.收货确认 || entity[0].Status == (int)DcsSupplierShippingOrderStatus.终止) && entity[0].IfDirectProvision && (entity[0].DirectProvisionFinished == false || entity[0].DirectProvisionFinished == null);
                case CommonActionKeys.TERMINATE:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entity1 = this.DataGridView.SelectedEntities.Cast<VirtualSupplierShippingOrder>().ToArray();
                    if(entity1.Length != 1)
                        return false;
                    return entity1[0].Status == (int)DcsSupplierShippingOrderStatus.部分确认 || entity1[0].Status == (int)DcsSupplierShippingOrderStatus.新建;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case "A4Print":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItemA4 = this.DataGridView.SelectedEntities.Cast<VirtualSupplierShippingOrder>().ToArray();
                    return selectItemA4.Length == 1 && selectItemA4.First().IfDirectProvision==true;
                case CommonActionKeys.PRINT:                   
                case "PrintLabel":               
                case "UsedPrintLabel":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItem = this.DataGridView.SelectedEntities.Cast<VirtualSupplierShippingOrder>().ToArray();
                    return selectItem.Length == 1;
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                var compositeFilter = filterItem as CompositeFilterItem;
                compositeFilter.Filters.Add(new FilterItem {
                    MemberName = "BranchId",
                    Operator = FilterOperator.IsEqualTo,
                    MemberType = typeof(int),
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });
                var createTimeFilters = compositeFilter.Filters.Where(filter => filter is CompositeFilterItem && (filter as CompositeFilterItem).Filters.All(item => item.MemberName == "CreateTime")).SingleOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                if(createTimeFilters != null) {
                    var dateTime = createTimeFilters.Filters.ElementAt(1).Value as DateTime?;
                    if(dateTime.HasValue)
                        createTimeFilters.Filters.ElementAt(1).Value = new DateTime(dateTime.Value.Year, dateTime.Value.Month, dateTime.Value.Day, 23, 59, 59);
                }
                compositeFilterItem.Filters.Add(compositeFilter);
            } else
                compositeFilterItem.Filters.Add(filterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();          
        }

        //合并导出
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private void ExecuteMergeExport(int[] ids, string partsSupplierCode, string partsSupplierName, int? partsSalesCategoryId, int? receivingWarehouseId, string code, string partsPurchaseOrderCode, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportSupplierShippingOrderWithDetailAsync(ids, BaseApp.Current.CurrentUserData.EnterpriseId, partsSupplierCode, partsSupplierName, partsSalesCategoryId, receivingWarehouseId, code, partsPurchaseOrderCode, status, createTimeBegin, createTimeEnd);
            this.excelServiceClient.ExportSupplierShippingOrderWithDetailCompleted -= this.ExcelServiceClientOnExportSupplierShippingOrderWithDetailCompleted;
            this.excelServiceClient.ExportSupplierShippingOrderWithDetailCompleted += this.ExcelServiceClientOnExportSupplierShippingOrderWithDetailCompleted;

        }

        private void ExcelServiceClientOnExportSupplierShippingOrderWithDetailCompleted(object sender, ExportSupplierShippingOrderWithDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
    }
}
