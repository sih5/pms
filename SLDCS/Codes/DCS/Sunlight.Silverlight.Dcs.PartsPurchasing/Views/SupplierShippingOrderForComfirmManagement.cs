﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
﻿using System.Windows;
﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
﻿using Sunlight.Silverlight.Dcs.Print;
﻿using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using System.Windows.Browser;
using Sunlight.Silverlight.ViewModel;
﻿using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views {
    [PageMeta("PartsPurchasing", "PartsPurchasing", "SupplierShippingOrderForComfirm", ActionPanelKeys = new[] {
           "SupplierShippingOrderForComfirm"
    })]
    public class SupplierShippingOrderForComfirmManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;

        private const string DATAEDITVIEWFORAPPROVE = "_dataEditViewForApprove_";
        private const string DATAEDITVIEWFORDETAIL = "_dataEditViewForDetail_";
        private DataEditViewBase dataEditViewForApprove;
        private const string DATAEDITVIEWFOREDIT = "_dataEditViewForEdit_";
        private DataEditViewBase dataEditViewForEdit;
        private DataEditViewBase DataEditViewForEdit {
            get {
                if(dataEditViewForEdit == null) {
                    dataEditViewForEdit = DI.GetDataEditView("SupplierShippingOrderForConfirmEdit");
                    ((SupplierShippingOrderForConfirmEditDataEditView)this.dataEditViewForEdit).EditSubmitted += this.DataEditView_EditSubmitted;
                    dataEditViewForEdit.EditCancelled += this.DataEditView_EditCancelled;
                }
                return dataEditViewForEdit;
            }
        }
        private DataEditViewBase DataEditViewForApprove {
            get {
                if(dataEditViewForApprove == null) {
                    dataEditViewForApprove = DI.GetDataEditView("SupplierShippingOrderForComfirmApprove");
                    ((SupplierShippingOrderForComfirmApproveDataEditView)this.dataEditViewForApprove).EditSubmitted += this.DataEditView_EditSubmitted;
                    dataEditViewForApprove.EditCancelled += this.DataEditView_EditCancelled;
                }
                return dataEditViewForApprove;
            }
        }
        private DataEditViewBase dataEditViewForDetail;
        private DataEditViewBase DataEditViewForDetail {
            get {
                if(dataEditViewForDetail == null) {
                    dataEditViewForDetail = DI.GetDataEditView("SupplierShippingOrderForComfirmDetail");
                    dataEditViewForDetail.EditCancelled += this.DataEditView_EditCancelled;
                }
                return dataEditViewForDetail;
            }
        }

        public SupplierShippingOrderForComfirmManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = "供应商到货确认";
        }

        private DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("SupplierShippingOrderForComfirm");
                    this.dataGridView.RowDoubleClick += DataGridView_RowDoubleClick;
                }
                return this.dataGridView;
            }
        }
        private void DataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                this.DataEditViewForDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                this.SwitchViewTo(DATAEDITVIEWFORDETAIL);
            }
        }
        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("SupplierShippingOrderForComfirm");
                    ((SupplierShippingOrderForComfirmDataEditView)this.dataEditView).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATAEDITVIEWFORAPPROVE, () => this.DataEditViewForApprove);
            this.RegisterView(DATAEDITVIEWFORDETAIL, () => this.DataEditViewForDetail);
            this.RegisterView(DATAEDITVIEWFOREDIT, () => this.DataEditViewForEdit);
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewForApprove = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "SupplierShippingOrderForComfirm"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case "DConFirm":
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.APPROVE:
                    this.DataEditViewForApprove.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATAEDITVIEWFORAPPROVE);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditViewForEdit.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATAEDITVIEWFOREDIT);
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    //如果选中一条数据 合并导出参数为 ID 
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any())
                    {
                        var ids = this.DataGridView.SelectedEntities.Cast<SupplierShippingOrder>().Select(r => r.Id).ToArray();
                        this.ExecuteMergeExport(ids, null, null, null, null, null, null, null, null,null,null);
                    }
                    else
                    {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if (filterItem == null)
                            return;
                        var arriveMethod = filterItem.Filters.Single(e => e.MemberName == "ArriveMethod").Value as int?; ;
                        var comfirmStatus = filterItem.Filters.Single(e => e.MemberName == "ComfirmStatus").Value as int?;
                        var code = filterItem.Filters.Single(e => e.MemberName == "Code").Value as string;
                        var partsSupplierName = filterItem.Filters.Single(e => e.MemberName == "PartsSupplierName").Value as string; 
                        var partsSupplierCode = filterItem.Filters.Single(e => e.MemberName == "PartsSupplierCode").Value as string; 
                        var partsPurchaseOrderCode = filterItem.Filters.Single(e => e.MemberName == "PartsPurchaseOrderCode").Value as string; 
                        var ifDirectProvision = filterItem.Filters.Single(e => e.MemberName == "IfDirectProvision").Value as bool?;
                        var modifyStatus = filterItem.Filters.Single(e => e.MemberName == "ModifyStatus").Value as int?;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var dateTime = filter as CompositeFilterItem;
                            if(dateTime != null) {
                                if(dateTime.Filters.First().MemberName == "CreateTime") {
                                    createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }

                            }
                        }
                        this.ExecuteMergeExport(null, code, partsPurchaseOrderCode, partsSupplierName, partsSupplierCode, arriveMethod, comfirmStatus, ifDirectProvision, createTimeBegin, createTimeEnd,modifyStatus);
                    }
                    break;
            }
        }
        
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EDIT:
                     if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitiesEdit = this.DataGridView.SelectedEntities.Cast<SupplierShippingOrder>().ToArray();                   
                    return entitiesEdit.Length==1;
                case "DConFirm":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<SupplierShippingOrder>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].LogisticArrivalDate==null;
                case CommonActionKeys.APPROVE:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitie = this.DataGridView.SelectedEntities.Cast<SupplierShippingOrder>().ToArray();
                    if(entitie.Length != 1)
                        return false;
                    return entitie[0].ModifyStatus == (int)DCSSupplierShippingOrderModifyStatus.已修改;
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default: return false;
            }
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private void ExecuteMergeExport(int[] ids, string code, string partsPurchaseOrderCode, string partsSupplierName, string partsSupplierCode, int? arriveMethod, int? comfirmStatus, bool? ifDirectProvision, DateTime? createTimeBegin, DateTime? createTimeEnd, int? modifyStatus)
        {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.Export供应商确认Async(ids, code, partsPurchaseOrderCode, partsSupplierName, partsSupplierCode, arriveMethod, comfirmStatus, ifDirectProvision, createTimeBegin, createTimeEnd, modifyStatus);
            this.excelServiceClient.Export供应商确认Async(ids, code, partsPurchaseOrderCode, partsSupplierName, partsSupplierCode, arriveMethod, comfirmStatus, ifDirectProvision, createTimeBegin, createTimeEnd, modifyStatus);
            this.excelServiceClient.Export供应商确认Completed -= this.ExcelServiceClient_Export供应商确认Completed;
            this.excelServiceClient.Export供应商确认Completed += this.ExcelServiceClient_Export供应商确认Completed;
        }

        private void ExcelServiceClient_Export供应商确认Completed(object sender, Export供应商确认CompletedEventArgs e)
        {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

    }
}

