﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views {
    [PageMeta("PartsPurchasing", "PartsPurchasing", "SupplierShippingOrderForDirect", ActionPanelKeys = new[] {
        CommonActionKeys.CONFIRM_EXPORT_PRINT_MERGEEXPORT
    })]
    public class SupplierShippingOrderForDirectManagement : DcsDataManagementViewBase {

        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;

        public SupplierShippingOrderForDirectManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsPurchasingUIStrings.DataManagementView_Title_SupplierShippingOrderForDirect;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("SupplierShippingOrderForDirect"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("SupplierShippingOrderForDirect");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "SupplierShippingOrderForDirect"
                };
            }
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.CONFIRM:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EXPORT:
                    ((DcsDataGridViewBase)this.DataGridView).ExportData();
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<SupplierShippingOrder>().Select(r => r.Id).ToArray();
                        this.excelServiceClient.ExportSupplierShippingOrderForDirectWithDetailAsync(ids, null, null, null, null, null, null, null, null, null);
                        this.excelServiceClient.ExportSupplierShippingOrderForDirectWithDetailCompleted -= excelServiceClient_ExportSupplierShippingOrderForDirectWithDetailCompleted;
                        this.excelServiceClient.ExportSupplierShippingOrderForDirectWithDetailCompleted += excelServiceClient_ExportSupplierShippingOrderForDirectWithDetailCompleted;
                    } else {
                        var compositeFilterItem = (this.DataGridView.FilterItem as CompositeFilterItem).Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        if(compositeFilterItem != null) {
                            var supplierCode = compositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSupplierCode") == null ? null : compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSupplierCode").Value as string;
                            var supplierName = compositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSupplierName") == null ? null : compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSupplierName").Value as string;
                            var branchId = compositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "BranchId") == null ? null : compositeFilterItem.Filters.Single(r => r.MemberName == "BranchId").Value as int?;
                            var code = compositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : compositeFilterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                            var partsSalesCategoryId = compositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSalesCategoryId") == null ? null : compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                            var salesOrderCode = compositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSalesOrder.Code") == null ? null : compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSalesOrder.Code").Value as string;
                            var status = compositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "Status") == null ? null : compositeFilterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                            var createTime = compositeFilterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                            DateTime? startDateTime = null;
                            DateTime? endDateTime = null;
                            if(createTime != null) {
                                startDateTime = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                endDateTime = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                            }
                            this.excelServiceClient.ExportSupplierShippingOrderForDirectWithDetailAsync(new int[] { }, branchId, supplierCode, supplierName, code, partsSalesCategoryId, salesOrderCode, status, startDateTime, endDateTime);
                            this.excelServiceClient.ExportSupplierShippingOrderForDirectWithDetailCompleted -= excelServiceClient_ExportSupplierShippingOrderForDirectWithDetailCompleted;
                            this.excelServiceClient.ExportSupplierShippingOrderForDirectWithDetailCompleted += excelServiceClient_ExportSupplierShippingOrderForDirectWithDetailCompleted;

                        }
                    }
                    break;
                case CommonActionKeys.PRINT:
                    //TODO: 导入暂时不实现
                    break;
            }
        }

        private void excelServiceClient_ExportSupplierShippingOrderForDirectWithDetailCompleted(object sender, ExportSupplierShippingOrderForDirectWithDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }


        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.CONFIRM:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entity = this.DataGridView.SelectedEntities.Cast<SupplierShippingOrder>().ToArray();
                    if(entity.Length != 1)
                        return false;
                    return (entity[0].Status == (int)DcsSupplierShippingOrderStatus.新建) || (entity[0].Status == (int)DcsSupplierShippingOrderStatus.部分确认);
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT:
                    //TODO: 导入暂时不实现
                    return false;
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                var compositeFilter = filterItem as CompositeFilterItem;
                var createTimeFilters = compositeFilter.Filters.Where(filter => filter is CompositeFilterItem && (filter as CompositeFilterItem).Filters.All(item => item.MemberName == "CreateTime")).SingleOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                if(createTimeFilters != null) {
                    var dateTime = createTimeFilters.Filters.ElementAt(1).Value as DateTime?;
                    if(dateTime.HasValue)
                        createTimeFilters.Filters.ElementAt(1).Value = new DateTime(dateTime.Value.Year, dateTime.Value.Month, dateTime.Value.Day, 23, 59, 59);
                }
                compositeFilterItem.Filters.Add(compositeFilter);
            } else
                compositeFilterItem.Filters.Add(filterItem);
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "ReceivingCompanyId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }
    }
}
