﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
﻿using System.Windows;
﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
﻿using Sunlight.Silverlight.Dcs.Print;
﻿using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using System.Windows.Browser;
using Sunlight.Silverlight.ViewModel;
﻿using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views {
    [PageMeta("PartsPurchasing", "PartsPurchasing", "SupplierShippingOrder", ActionPanelKeys = new[] {
           "SupplierShippingOrderForSupplier"
    })]
    public class SupplierShippingOrderManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;

        private const string DATAEDITVIEWFOREDIT = "_dataEditViewForEdit_";
        private DataEditViewBase dataEditViewForEdit;
        private DataEditViewBase DataEditViewForEdit {
            get {
                if(dataEditViewForEdit == null) {
                    dataEditViewForEdit = DI.GetDataEditView("SupplierShippingOrderEdit");
                    dataEditViewForEdit.EditSubmitted += this.DataEditView_EditSubmitted;
                    dataEditViewForEdit.EditCancelled += this.DataEditView_EditCancelled;
                }
                return dataEditViewForEdit;
            }
        }

        public SupplierShippingOrderManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsPurchasingUIStrings.DataManagementView_Title_SupplierShippingOrder;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("SupplierShippingOrder"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("SupplierShippingOrder");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATAEDITVIEWFOREDIT, () => this.DataEditViewForEdit);
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewForEdit = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                var compositeFilter = filterItem as CompositeFilterItem;
                compositeFilter.Filters.Add(new FilterItem {
                    MemberName = "PartsSupplierId",
                    Operator = FilterOperator.IsEqualTo,
                    MemberType = typeof(int),
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });
                var createTimeFilters = compositeFilter.Filters.Where(filter => filter is CompositeFilterItem && (filter as CompositeFilterItem).Filters.All(item => item.MemberName == "CreateTime")).SingleOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                if(createTimeFilters != null) {
                    var dateTime = createTimeFilters.Filters.ElementAt(1).Value as DateTime?;
                    if(dateTime.HasValue)
                        createTimeFilters.Filters.ElementAt(1).Value = new DateTime(dateTime.Value.Year, dateTime.Value.Month, dateTime.Value.Day, 23, 59, 59);
                }
                compositeFilterItem.Filters.Add(compositeFilter);
            } else
                compositeFilterItem.Filters.Add(filterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "SupplierShippingOrder"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var supplierShippingOrder = this.DataEditView.CreateObjectToEdit<SupplierShippingOrder>();
                    supplierShippingOrder.Status = (int)DcsSupplierShippingOrderStatus.新建;
                    supplierShippingOrder.PartsSupplierId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    supplierShippingOrder.PartsSupplierCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                    supplierShippingOrder.PartsSupplierName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    supplierShippingOrder.ShippingDate = DateTime.Now;
                    supplierShippingOrder.LogisticArrivalDate = DateTime.Now;
                    supplierShippingOrder.RequestedDeliveryTime = DateTime.Now;
                    supplierShippingOrder.PlanDeliveryTime = DateTime.Now;
                    supplierShippingOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    supplierShippingOrder.DirectProvisionFinished = false;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditViewForEdit.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATAEDITVIEWFOREDIT);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<SupplierShippingOrder>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废供应商发运单)
                                entity.作废供应商发运单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORT:
                    ((DcsDataGridViewBase)this.DataGridView).ExportData();
                    break;
                case CommonActionKeys.PRINT:
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<SupplierShippingOrder>().FirstOrDefault();
                    if(selectedItem == null)
                        return;
                    //BasePrintWindow printWindow = new SupplierShippingOrderPrintWindow {
                    //    Header = PartsPurchasingUIStrings.DataManagementView_PrintWindow_Title_SupplierShippingOrder,
                    //    SupplierShippingOrder = selectedItem
                    //};
                    //printWindow.ShowDialog();
                    if(selectedItem.IfDirectProvision){
                        //不打印二维码
                        SunlightPrinter.ShowPrinter(PartsPurchasingUIStrings.DataManagementView_Text_SupplierShippingPrint, "SupplierShippingOrderNoBar", null, true, new Tuple<string, string>("supplierShippingOrderId", selectedItem.Id.ToString()), new Tuple<string, string>("UserName", BaseApp.Current.CurrentUserData.UserName));
                    }
                    else
                    {
                        SunlightPrinter.ShowPrinter(PartsPurchasingUIStrings.DataManagementView_Text_SupplierShippingPrint, "SupplierShippingOrder", null, true, new Tuple<string, string>("supplierShippingOrderId", selectedItem.Id.ToString()), new Tuple<string, string>("UserName", BaseApp.Current.CurrentUserData.UserName));
                    }

                    break;
                case "PrintNoPrice":
                    var selectedItemNoPrice = this.DataGridView.SelectedEntities.Cast<SupplierShippingOrder>().FirstOrDefault();
                    if(selectedItemNoPrice == null)
                        return;
                    BasePrintWindow printWindowNoPrice = new SupplierShippingOrderPrintNoPriceWindow {
                        Header = PartsPurchasingUIStrings.DataManagementView_PrintWindow_Title_SupplierShippingOrderNoPrice,
                        SupplierShippingOrderNoPrice = selectedItemNoPrice
                    };
                    printWindowNoPrice.ShowDialog();
                    break;
                case "PrintLabel":
                    var supplierShippingOrders = this.DataGridView.SelectedEntities.Select(r => (int)r.GetIdentity()).ToArray();
                    this.PrintLabelForSupplierShippingOrderDataEditView.SetObjectToEditById(supplierShippingOrders);
                    LabelPrintWindow.ShowDialog();
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    //如果选中一条数据 合并导出参数为 ID 
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any())
                    {
                        var ids = this.DataGridView.SelectedEntities.Cast<SupplierShippingOrder>().Select(r => r.Id).ToArray();
                        this.ExecuteMergeExport(ids, /*null,*/ null, /*null,*/ null, null, /*null,*/ null, null,null);
                    }
                    else
                    {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if (filterItem == null)
                            return;
                        //var branchId = (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.SingleOrDefault(r => r.MemberName == "BranchId") == null ? null : (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.Single(r => r.MemberName == "BranchId").Value as int?;
                        var code = (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.Single(r => r.MemberName == "Code").Value as string;
                        //var partsSalesCategoryId = (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.SingleOrDefault(r => r.MemberName == "PartsSalesCategoryId") == null ? null : (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                        var receivingWarehouseId = (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.SingleOrDefault(r => r.MemberName == "ReceivingWarehouseId") == null ? null : (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.Single(r => r.MemberName == "ReceivingWarehouseId").Value as int?;
                        var partsPurchaseOrderCode = (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.SingleOrDefault(r => r.MemberName == "PartsPurchaseOrderCode") == null ? null : (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.Single(r => r.MemberName == "PartsPurchaseOrderCode").Value as string;
                        //var ERPSourceOrderCode = (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.SingleOrDefault(r => r.MemberName == "ERPSourceOrderCode") == null ? null : (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.Single(r => r.MemberName == "ERPSourceOrderCode").Value as string;
                        var status = (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.SingleOrDefault(r => r.MemberName == "Status") == null ? null : (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.Single(r => r.MemberName == "Status").Value as int?;
                        
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        foreach (var filter in (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)))
                        {
                            var dateTime = filter as CompositeFilterItem;
                            {
                                if (dateTime.Filters.First().MemberName == "CreateTime")
                                {
                                    createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                            }
                        }
                        this.ExecuteMergeExport(null, /*branchId, */code, /*partsSalesCategoryId,*/ receivingWarehouseId, partsPurchaseOrderCode, /*ERPSourceOrderCode,*/ status, createTimeBegin, createTimeEnd);
                    }
                    break;
                //箱标打印
                case "ChPrint":
                    var billCh = this.DataGridView.SelectedEntities.Cast<SupplierShippingOrder>().ToArray();
                    if (billCh.Count() == 0)
                        return;
                    this.PrintLabelForPartsInboundCheckDataEditViewCh.SetObjectToEditById(billCh.Select(r => r.Id).ToArray());
                    this.LabelPrintWindowCh.ShowDialog();
                    break;              
                //件标打印
                case "PcPrint":
                    var billPc = this.DataGridView.SelectedEntities.Cast<SupplierShippingOrder>().ToArray();
                    if (billPc.Count() == 0)
                        return;
                    this.PrintLabelForPartsInboundCheckDataEditViewCp.SetObjectToEditById(billPc.Select(r => r.Id).ToArray());
                    this.LabelPrintWindowCp.ShowDialog();
                    break;
            }
        }
        //件标打印
        private RadWindow labelPrintWindowCp;
        private DataEditViewBase printLabelForPartsInboundCheckDataEditViewCp;

        private DataEditViewBase PrintLabelForPartsInboundCheckDataEditViewCp
        {
            get
            {
                return this.printLabelForPartsInboundCheckDataEditViewCp ?? (this.printLabelForPartsInboundCheckDataEditViewCp = DI.GetDataEditView("PrintLabelForSupplierShippingOrderCp"));
            }
        }
        private RadWindow LabelPrintWindowCp
        {
            get
            {
                return this.labelPrintWindowCp ?? (this.labelPrintWindowCp = new RadWindow
                {
                    Content = this.PrintLabelForPartsInboundCheckDataEditViewCp,
                    Header = PartsPurchasingUIStrings.DataManagementView_Text_PartInformation,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                   
                });
            }
        }
        //配件标签打印
        private RadWindow labelPrintWindow;
        private DataEditViewBase printLabelForSupplierShippingOrderDataEditView;

        private DataEditViewBase PrintLabelForSupplierShippingOrderDataEditView {
            get {
                return this.printLabelForSupplierShippingOrderDataEditView ?? (this.printLabelForSupplierShippingOrderDataEditView = DI.GetDataEditView("PrintLabelForSupplierShippingOrder"));
            }
        }

        private RadWindow LabelPrintWindow {
            get {
                return this.labelPrintWindow ?? (this.labelPrintWindow = new RadWindow {
                    Content = this.PrintLabelForSupplierShippingOrderDataEditView,
                    Header = PartsPurchasingUIStrings.DataManagementView_Text_PartInformation,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                 
                });
            }
        }
        // 中文打印
        private RadWindow labelPrintWindowCh;
        private DataEditViewBase printLabelForPartsInboundCheckDataEditViewCh;

        private DataEditViewBase PrintLabelForPartsInboundCheckDataEditViewCh
        {
            get
            {
                return this.printLabelForPartsInboundCheckDataEditViewCh ?? (this.printLabelForPartsInboundCheckDataEditViewCh = DI.GetDataEditView("PrintLabelForSupplierShippingOrderCh"));
            }
        }
        private RadWindow LabelPrintWindowCh
        {
            get
            {
                return this.labelPrintWindowCh ?? (this.labelPrintWindowCh = new RadWindow
                {
                    Content = this.PrintLabelForPartsInboundCheckDataEditViewCh,
                    Header = PartsPurchasingUIStrings.DataManagementView_Text_PartInformation,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                 
                });
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<SupplierShippingOrder>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsSupplierShippingOrderStatus.新建;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT:
                case "PrintNoPrice":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems = this.DataGridView.SelectedEntities.Cast<SupplierShippingOrder>().ToArray();
                    return selectItems.Length == 1;
                case "PrintLabel":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItem = this.DataGridView.SelectedEntities.Cast<SupplierShippingOrder>().ToArray();
                    return selectItem.Length == 1;
                case "PcPrint":
                case "ChPrint":
                    if (this.DataGridView.SelectedEntities == null)
                        return false;
                    return true;
                default:
                    return false;
            }
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private void ExecuteMergeExport(int[] ids, /*int? branchId,*/ string code, /*int? partsSalesCategoryId,*/ int? receivingWarehouseId, string partsPurchaseOrderCode, /*string ERPSourceOrderCode,*/int? status, DateTime? createTimeBegin, DateTime? createTimeEnd)
        {
            ShellViewModel.Current.IsBusy = false;
            this.excelServiceClient.ExportSupplierShippingOrderForSupplierAsync(ids,/* branchId,*/ code, /*partsSalesCategoryId,*/ receivingWarehouseId, partsPurchaseOrderCode, /*ERPSourceOrderCode,*/ status, createTimeBegin, createTimeEnd);
            this.excelServiceClient.ExportSupplierShippingOrderForSupplierAsync(ids, /*branchId,*/ code, /*partsSalesCategoryId,*/ receivingWarehouseId, partsPurchaseOrderCode, /*ERPSourceOrderCode,*/ status, createTimeBegin, createTimeEnd);
            this.excelServiceClient.ExportSupplierShippingOrderForSupplierCompleted -= this.ExcelServiceClient_ExportSupplierShippingOrderForSupplierCompleted;
            this.excelServiceClient.ExportSupplierShippingOrderForSupplierCompleted += this.ExcelServiceClient_ExportSupplierShippingOrderForSupplierCompleted;
        }

        private void ExcelServiceClient_ExportSupplierShippingOrderForSupplierCompleted(object sender, ExportSupplierShippingOrderForSupplierCompletedEventArgs e)
        {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

    }
}
