﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views {
    [PageMeta("PartsPurchasing", "PartsPurchasing", "SupplierTraceCode", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_SUBMIT_AUDIT_APPROVE_EXPORT
    })]
    public class SupplierTraceCodeManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataApproveView;
        private DataEditViewBase dataForEditView;
        private DataEditViewBase dataDetailView;
        private const string DATA_DETAIL_VIEW = "_DataDetailView_";
        private const string DATA_APPROVE_VIEW = "_dataApproveView_";
        private const string DATA_FOREDIT_VIEW = "_dataForEditView_";

        public SupplierTraceCodeManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = "供应商零件永久性标识条码修正";
        }
        private DataGridViewBase DataGridView {         
              get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("SupplierTraceCode");
                    this.dataGridView.RowDoubleClick += DataGridView_RowDoubleClick;
                }
                return this.dataGridView;
            }
        }
        private void DataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                this.DataEditViewDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                this.SwitchViewTo(DATA_DETAIL_VIEW);
            }
        }
        private DataEditViewBase DataEditViewDetail {
            get {
                if(this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("SupplierTraceCodeDetail");
                    this.dataDetailView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataDetailView;
            }
        }
        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("SupplierTraceCode");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private DataEditViewBase DataForEditView {
            get {
                if(this.dataForEditView == null) {
                    this.dataForEditView = DI.GetDataEditView("SupplierTraceCodeEdit");
                    this.dataForEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataForEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataForEditView;
            }
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }
        private DataEditViewBase DataApproveView {
            get {
                if(this.dataApproveView == null) {
                    this.dataApproveView = DI.GetDataEditView("SupplierTraceCodeApprove");
                    ((SupplierTraceCodeApproveDataEditView)this.dataApproveView).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataApproveView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataApproveView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataApproveView;
            }
        }
     
        private void ResetEditView() {
            this.dataEditView = null;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_APPROVE_VIEW, () => this.DataApproveView);
            this.RegisterView(DATA_FOREDIT_VIEW, () => this.dataForEditView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataEditViewDetail);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "SupplierTraceCode"
                };
            }
        }
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var supplierTraceCode = this.DataEditView.CreateObjectToEdit<SupplierTraceCode>();
                    supplierTraceCode.Status = (int)DcsPartsPurchaseOrderStatus.新增;
                    supplierTraceCode.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataForEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_FOREDIT_VIEW);
                    break;
                case CommonActionKeys.SUBMIT:
                    //此处重写Invoke方法
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Confirm_Submit, () => {
                        try {
                            var entity = this.DataGridView.SelectedEntities.Cast<SupplierTraceCode>().ToArray();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext != null) {
                                domainContext.SubmitSupplierTraceCode(entity.Select(r => r.Id).Distinct().ToArray(), invokeOp => {
                                    if(invokeOp.HasError)
                                        return;
                                    UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Notification_SubmitSuccess);
                                    this.DataGridView.ExecuteQueryDelayed();
                                    this.CheckActionsCanExecute();
                                }, null);
                            }

                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.ABANDON:
                    //此处重写Invoke方法
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Confirm_Submit, () => {
                        try {
                            var entity = this.DataGridView.SelectedEntities.Cast<SupplierTraceCode>().ToArray();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext != null) {
                                domainContext.AbandonSupplierTraceCode(entity.Select(r => r.Id).Distinct().ToArray(), invokeOp => {
                                    if(invokeOp.HasError)
                                        return;
                                    UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Notification_SubmitSuccess);
                                    this.DataGridView.ExecuteQueryDelayed();
                                    this.CheckActionsCanExecute();
                                }, null);
                            }

                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "Audit": //审核
                    this.DataApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_APPROVE_VIEW);
                    break;
                case "Approve"://审批
                    this.DataApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_APPROVE_VIEW);
                    break;
                
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<SupplierTraceCode>().Select(r => r.Id).ToArray();
                        ShellViewModel.Current.IsBusy = true;
                        this.ExportDealerPartsTransferOrder(ids, null, null, null, null, null, null, null, null, null, null, null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var partsSupplierCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSupplierCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "PartsSupplierCode").Value as string;
                        var partsSupplierName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSupplierName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "PartsSupplierName").Value as string;
                        var partsPurchaseOrderCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsPurchaseOrderCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "PartsPurchaseOrderCode").Value as string;
                        var oldTraceCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "OldTraceCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "OldTraceCode").Value as string;
                        var status = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Status") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var sparePartCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SparePartCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                        var newTraceCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "NewTraceCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "NewTraceCode").Value as string;
                        var shippingCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "ShippingCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "ShippingCode").Value as string;

                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var dateTime = filter as CompositeFilterItem;
                            if(dateTime != null) {
                                if(dateTime.Filters.First().MemberName == "CreateTime") {
                                    createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                                
                            }
                        }

                        this.ExportDealerPartsTransferOrder(null, code, status, createTimeBegin, createTimeEnd, partsSupplierCode, partsSupplierName, sparePartCode, partsPurchaseOrderCode, oldTraceCode, newTraceCode, shippingCode);
                    }
                    break;
            }
        }
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public void ExportDealerPartsTransferOrder(int[] ids, string code, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, string partsSupplierCode, string partsSupplierName, string sparePartCode, string partsPurchaseOrderCode, string oldTraceCode, string newTraceCode, string shippingCode) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportSupplierTraceCodeAsync(ids, code, status, createTimeBegin, createTimeEnd, partsSupplierCode, partsSupplierName, sparePartCode, partsPurchaseOrderCode, oldTraceCode, newTraceCode, shippingCode);
            this.excelServiceClient.ExportSupplierTraceCodeCompleted -= ExcelServiceClient_ExportDealerPartsTransferOrderAndDetailCompleted;
            this.excelServiceClient.ExportSupplierTraceCodeCompleted += ExcelServiceClient_ExportDealerPartsTransferOrderAndDetailCompleted;
        }
        private void ExcelServiceClient_ExportDealerPartsTransferOrderAndDetailCompleted(object sender, ExportSupplierTraceCodeCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.SUBMIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<SupplierTraceCode>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DCSSupplierTraceCodeStatus.新增;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.AUDIT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems = this.DataGridView.SelectedEntities.Cast<SupplierTraceCode>().ToArray();
                    return selectItems.Length == 1 && selectItems[0].Status == (int)DCSSupplierTraceCodeStatus.提交;
                case CommonActionKeys.APPROVE:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItemsApprove = this.DataGridView.SelectedEntities.Cast<SupplierTraceCode>().ToArray();
                    return selectItemsApprove.Length == 1 && selectItemsApprove[0].Status == (int)DCSSupplierTraceCodeStatus.已审核;
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
