﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views {
    [PageMeta("PartsPurchasing", "PartsPurchasing", "TemPurchaseOrder", ActionPanelKeys = new[] {
        "TemPurchaseOrder"
    })]
    public class TemPurchaseOrderManagement : DcsDataManagementViewBase {
        #region 终止弹出
        private RadWindow terminateRadWindow;

        private RadWindow TerminateRadWindow {
            get {
                if(this.terminateRadWindow == null) {
                    this.terminateRadWindow = new RadWindow();
                    this.terminateRadWindow.CanClose = false;
                    this.terminateRadWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    this.terminateRadWindow.Content = this.TerminateDataEditView;
                    this.terminateRadWindow.ResizeMode = ResizeMode.NoResize;
                    this.terminateRadWindow.Height = 200;
                    this.terminateRadWindow.Width = 400;
                    this.terminateRadWindow.Header = "";
                }
                return this.terminateRadWindow;
            }
        }

        private DataEditViewBase terminateDataEditView;

        private DataEditViewBase TerminateDataEditView {
            get {
                if(this.terminateDataEditView == null) {
                    this.terminateDataEditView = DI.GetDataEditView("TemPartsPurchaseOrderForTerminate");
                    this.terminateDataEditView.EditCancelled += terminateDataEditView_EditCancelled;
                    this.terminateDataEditView.EditSubmitted += terminateDataEditView_EditSubmitted;
                }
                return this.terminateDataEditView;
            }
        }

        private void terminateDataEditView_EditSubmitted(object sender, EventArgs e) {
            this.TerminateRadWindow.Close();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
        }

        private void terminateDataEditView_EditCancelled(object sender, EventArgs e) {
            this.TerminateRadWindow.Close();
        }
        #endregion

        #region 强制完成弹出
        private RadWindow abandonRadWindow;

        private RadWindow AbandonRadWindow {
            get {
                if(this.abandonRadWindow == null) {
                    this.abandonRadWindow = new RadWindow();
                    this.abandonRadWindow.CanClose = false;
                    this.abandonRadWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    this.abandonRadWindow.Content = this.AbandonDataEditView;
                    this.abandonRadWindow.ResizeMode = ResizeMode.NoResize;
                    this.abandonRadWindow.Height = 200;
                    this.abandonRadWindow.Width = 400;
                    this.abandonRadWindow.Header = "";
                }
                return this.abandonRadWindow;
            }
        }

        private DataEditViewBase abandonDataEditView;

        private DataEditViewBase AbandonDataEditView {
            get {
                if(this.abandonDataEditView == null) {
                    this.abandonDataEditView = DI.GetDataEditView("TemPartsPurchaseOrderForAbandon");
                    this.abandonDataEditView.EditCancelled += abandonDataEditView_EditCancelled;
                    this.abandonDataEditView.EditSubmitted += abandonDataEditView_EditSubmitted;
                }
                return this.abandonDataEditView;
            }
        }


        private void abandonDataEditView_EditSubmitted(object sender, EventArgs e) {
            this.AbandonRadWindow.Close();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
        }

        private void abandonDataEditView_EditCancelled(object sender, EventArgs e) {
            this.AbandonRadWindow.Close();
        }
        #endregion

        private DataEditViewBase dataEditView;
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataApproveView;
        private DataEditViewBase dataChannelChangeView;
        private DataEditViewBase dataReplaceShipView;
        private DataEditViewBase dataDetailView;
        private DataEditViewBase dataConfirmView;
        private const string DATA_CHANNELCHANGE_VIEW = "_dataChannelChangeView_";
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private const string DATA_APPROVE_VIEW = "_dataPendingApprovalView_";
        private const string DATA_REPLACESHIP_VIEW = "_dataReplaceShipView_";
        private const string DATA_REPLACESHIPALL_VIEW = "_dataReplaceShipAllView_";
        private const string DATA_DETAIL_VIEW = "_DataDetailView_";
        private const string DATA_CONFIRM_VIEW = "_DataConfirmView_";

        public TemPurchaseOrderManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsPurchasingUIStrings.DataManagementView_Title_PartsPurchaseOrderTem;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_APPROVE_VIEW, () => this.DataApproveView);
            this.RegisterView(DATA_CHANNELCHANGE_VIEW, () => this.DataChannelChangeView);
            this.RegisterView(DATA_REPLACESHIP_VIEW, () => this.DataReplaceShipView);
            this.RegisterView(DATA_REPLACESHIPALL_VIEW, () => this.DataReplaceShipAllView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataEditViewDetail);
            this.RegisterView(DATA_CONFIRM_VIEW, () => this.DataConfirmView);
        }
        private DataEditViewBase DataConfirmView {
            get {
                if(this.dataConfirmView == null) {
                    this.dataConfirmView = DI.GetDataEditView("TemPurchaseOrderConfirm");
                    ((TemPurchaseOrderConfirmDataEditView)this.dataConfirmView).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataConfirmView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataConfirmView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataConfirmView;
            }
        }
        private DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("TemPurchaseOrderWithDetails");
                    this.dataGridView.RowDoubleClick += DataGridView_RowDoubleClick;
                }
                return this.dataGridView;
            }
        }
        private void DataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                this.DataEditViewDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                this.SwitchViewTo(DATA_DETAIL_VIEW);
            }
        }

        private DataEditViewBase DataChannelChangeView {
              get {
                if(this.dataChannelChangeView == null) {
                    this.dataChannelChangeView = DI.GetDataEditView("TemPartsPurchaseOrderInitialApprove");
                    ((TemPartsPurchaseOrderInitialApproveDataEditView)this.dataChannelChangeView).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataChannelChangeView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataChannelChangeView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataChannelChangeView;
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsPurchaseOrder");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataApproveView {
               get {
                if(this.dataApproveView == null) {
                    this.dataApproveView = DI.GetDataEditView("TemPartsPurchaseOrderPendingApproval");
                    ((TemPartsPurchaseOrderPendingApprovalDataEditView)this.dataApproveView).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataApproveView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataApproveView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataApproveView;
            }
        }


        private DataEditViewBase DataReplaceShipView {
            get {
                if(this.dataReplaceShipView == null) {
                    this.dataReplaceShipView = DI.GetDataEditView("TemPartsPurchaseOrderReplaceShip");
                    this.dataReplaceShipView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataReplaceShipView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataReplaceShipView;
            }
        }

        private DataEditViewBase dataReplaceShipAllView;
        private DataEditViewBase DataReplaceShipAllView {
            get {
                if(this.dataReplaceShipAllView == null) {
                    this.dataReplaceShipAllView = DI.GetDataEditView("TemPartsPurchaseOrderOrderReplaceShipAll");
                    this.dataReplaceShipAllView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataReplaceShipAllView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataReplaceShipAllView;
            }
        }
        private DataEditViewBase DataEditViewDetail {
            get {
                if(this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("TemPartsPurchaseOrderDetail");
                    this.dataDetailView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataDetailView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataApproveView = null;
            this.dataChannelChangeView = null;
            this.dataReplaceShipView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "TemPurchaseOrder"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.SUBMIT:
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Confirm_Submit, () => {
                        try {
                            var entity = this.DataGridView.SelectedEntities.Cast<TemPurchaseOrder>().ToArray();
                            var gridDomainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(gridDomainContext == null)
                                return;
                            ShellViewModel.Current.IsBusy = true;
                            gridDomainContext.提交临时采购订单(entity.First().Id, loadOpv => {
                                if(loadOpv.HasError) {
                                    if(!loadOpv.IsErrorHandled)
                                        loadOpv.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOpv);
                                    gridDomainContext.RejectChanges();
                                    ShellViewModel.Current.IsBusy = false;
                                    return;
                                }
                                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Notification_SubmitSuccess);
                                gridDomainContext.RejectChanges();
                                if(DataGridView != null && DataGridView.FilterItem != null) {
                                    DataGridView.ExecuteQueryDelayed();
                                }
                                CheckActionsCanExecute();
                                ShellViewModel.Current.IsBusy = false;
                            }, null);
                        } catch(Exception e) {
                            ShellViewModel.Current.IsBusy = false;
                            UIHelper.ShowAlertMessage(e.Message);
                        }
                    });
                    break;
                case "AppRoveSubmit"://审核提交
                    //此处重写Invoke方法
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Confirm_Submit, () => {
                        try {
                            var entity = this.DataGridView.SelectedEntities.Cast<TemPurchaseOrder>().ToArray();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext != null) {

                                domainContext.审核提交临时采购订单(entity.First().Id, invokeOp => {
                                    if(invokeOp.HasError)
                                        return;
                                    UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Notification_SubmitSuccess);
                                    this.DataGridView.ExecuteQueryDelayed();
                                    this.CheckActionsCanExecute();
                                }, null);
                            }

                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "PendingConfirmation"://代确认
                    this.DataConfirmView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_CONFIRM_VIEW);
                    break;
                case "ReplaceShip"://代发运
                    this.CreatePartsShipOrder();
                    break;           
                case "Force"://强制完成
                    DcsUtils.Confirm("确定要强制完成所选数据吗？", () => {
                        this.AbandonDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                        this.AbandonRadWindow.ShowDialog();
                    });
                    break;

                case CommonActionKeys.TERMINATE://终止
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Confirm_Terminate, () => {
                        this.TerminateDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                        this.TerminateRadWindow.ShowDialog();
                    });
                    break;
                case "InitialApprove"://审核
                    this.DataChannelChangeView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_CHANNELCHANGE_VIEW);
                    break;

                case "FinalApprove"://审批
                    this.DataApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_APPROVE_VIEW);
                    break;
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<TemPurchaseOrder>().Select(r => r.Id).ToArray();
                        this.Export临时采购订单(ids, BaseApp.Current.CurrentUserData.UserId, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var temPurchasePlanOrderCode = filterItem.Filters.Single(r => r.MemberName == "TemPurchasePlanOrderCode").Value as string;
                        var suplierName = filterItem.Filters.Single(r => r.MemberName == "SuplierName").Value as string;
                        var approveStatus = filterItem.Filters.Single(e => e.MemberName == "ApproveStatus").Value as int?;
                        var status = filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var receiveStatus = filterItem.Filters.Single(e => e.MemberName == "ReceiveStatus").Value as int?;
                        var customerType = filterItem.Filters.Single(r => r.MemberName == "CustomerType").Value as int?;
                        var sparePartCode = filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                        var receCompanyCode = filterItem.Filters.Single(r => r.MemberName == "ReceCompanyCode").Value as string;
                        var receCompanyName = filterItem.Filters.Single(r => r.MemberName == "ReceCompanyName").Value as string;
                        var isSaleCode = filterItem.Filters.Single(r => r.MemberName == "IsSaleCode").Value as bool?;
                        var isPurchaseOrder = filterItem.Filters.Single(r => r.MemberName == "IsPurchaseOrder").Value as bool?;
                        var compositeFilterItems = filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).ToArray();
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        foreach(var dateTimeFilterItem in compositeFilterItems) {
                            var dateTime = dateTimeFilterItem as CompositeFilterItem;
                            if(dateTime != null) {
                                if(dateTime.Filters.Any(r => r.MemberName == "CreateTime")) {
                                    createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                            }
                        }
                        ShellViewModel.Current.IsBusy = true;
                        var partsPurchaseOrderIds = new int[] { };
                        if(this.DataGridView.SelectedEntities != null) {
                            var partsPurchaseOrders = this.DataGridView.SelectedEntities.Cast<TemPurchaseOrder>();
                            partsPurchaseOrderIds = partsPurchaseOrders.Select(r => r.Id).ToArray();
                        }
                        this.Export临时采购订单(null, BaseApp.Current.CurrentUserData.UserId, temPurchasePlanOrderCode, suplierName, code, approveStatus, receiveStatus, status, customerType, receCompanyCode, receCompanyName, createTimeBegin, createTimeEnd, sparePartCode, isSaleCode, isPurchaseOrder);
                    }
                    break;
                case CommonActionKeys.PRINT:

                    var print = this.DataGridView.SelectedEntities.Cast<TemPurchaseOrder>().FirstOrDefault();
                    SunlightPrinter.ShowPrinter("临时订单打印", "TemReportPartsPurchaseOrder", null, true, new Tuple<string, string>("partsPurchaseOrderId", print.Id.ToString()));
                    break;
                case "ReplaceShipAll"://汇总发运
                    this.SwitchViewTo(DATA_REPLACESHIPALL_VIEW);
                    break;
            }
        }

        DateTime DtPlanDeliveryTime = DateTime.Now;

        private void CreatePartsShipOrder() {
            var partsPurchaseOrder = this.DataGridView.SelectedEntities.First() as TemPurchaseOrder;
            var domainContext = new DcsDomainContext();
            if(partsPurchaseOrder != null)
                //赋值：计划到货时间：供应商发运单的生成日期 + 供应商物流周期 默认计算。年月日，不带时分秒
                domainContext.Load(domainContext.GetBranchSupplierRelationBySupplierIdQuery(221, partsPurchaseOrder.SuplierId.Value, BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp1 => {
                    if(loadOp1.HasError) {
                        if(!loadOp1.IsErrorHandled)
                            loadOp1.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp1);
                        return;
                    }
                    if(loadOp1.Entities.Count() > 0) {
                        DtPlanDeliveryTime = DateTime.Now.Date.AddDays(loadOp1.Entities.First().ArrivalCycle ?? 0);
                    }
                }, null);
            domainContext.Load(domainContext.GetTemPurchaseOrdersWithDetailsByIdQuery(partsPurchaseOrder.Id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var item = loadOp.Entities.FirstOrDefault();
                if(item == null)
                    return;
                var supplierShippingOrder = this.DataReplaceShipView.CreateObjectToEdit<TemSupplierShippingOrder>();
                supplierShippingOrder.PartsSupplierId = item.SuplierId.Value;
                supplierShippingOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                supplierShippingOrder.PartsSupplierCode = item.SuplierCode;
                supplierShippingOrder.PartsSupplierName = item.SuplierName;
                supplierShippingOrder.TemPurchaseOrderId = item.Id;
                supplierShippingOrder.TemPurchaseOrderCode = item.Code;
                supplierShippingOrder.ReceivingWarehouseId = item.WarehouseId ?? 0; 
                supplierShippingOrder.ReceivingWarehouseName = item.WarehouseName;
                supplierShippingOrder.ReceivingAddress = item.ReceiveAddress;
                supplierShippingOrder.PlanDeliveryTime = DtPlanDeliveryTime;
                supplierShippingOrder.Remark = item.Memo;
                supplierShippingOrder.OriginalRequirementBillId = item.TemPurchasePlanOrderId;
                supplierShippingOrder.OriginalRequirementBillCode = item.TemPurchasePlanOrderCode;
                supplierShippingOrder.ReceivingCompanyId = item.ReceCompanyId;
                supplierShippingOrder.ReceivingCompanyName = item.ReceCompanyName;
                supplierShippingOrder.ReceivingCompanyCode = item.ReceCompanyCode;
                supplierShippingOrder.Status = (int)DCSTemSupplierShippingOrderStatus.新建;
                supplierShippingOrder.ShippingDate = DateTime.Now;
                supplierShippingOrder.ShippingMethod = item.ShippingMethod ?? 0;
                supplierShippingOrder.OrderCompanyId = item.OrderCompanyId;
                supplierShippingOrder.OrderCompanyCode = item.OrderCompanyCode;
                supplierShippingOrder.OrderCompanyName = item.OrderCompanyName;
                foreach(var detail in item.TemPurchaseOrderDetails) {
                    if(detail.ShippingAmount == null) {
                        detail.ShippingAmount = 0;
                    }
                    if(detail.ConfirmedAmount > 0 && detail.ConfirmedAmount != detail.ShippingAmount) {
                        var supplierShippingDetail = new TemShippingOrderDetail();
                        supplierShippingDetail.SparePartId = detail.SparePartId.Value;
                        supplierShippingDetail.SparePartCode = detail.SparePartCode;
                        supplierShippingDetail.SparePartName = detail.SparePartName;
                        supplierShippingDetail.SupplierPartCode = detail.SupplierPartCode;
                        supplierShippingDetail.MeasureUnit = detail.MeasureUnit;
                        supplierShippingDetail.ConfirmedAmount = 0;
                        supplierShippingDetail.Quantity = detail.ConfirmedAmount - detail.ShippingAmount.Value;
                        supplierShippingDetail.PendingQuantity = supplierShippingDetail.Quantity;
                        if(supplierShippingDetail.Quantity > 0)
                            supplierShippingOrder.TemShippingOrderDetails.Add(supplierShippingDetail);
                    }
                }
                this.SwitchViewTo(DATA_REPLACESHIP_VIEW);
            }, null);
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.SUBMIT:
                case CommonActionKeys.TERMINATE:
                case "AppRoveSubmit":
                case "InitialApprove":
                case "FinalApprove":
                case "ReplaceShip":
                case "PendingConfirmation":
                case "Force":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<TemPurchaseOrder>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    if(uniqueId == CommonActionKeys.SUBMIT)
                        return entities[0].Status == (int)DCSTemPurchaseOrderStatus.新增;
                    if(uniqueId == "Force")
                        return (entities[0].Status == (int)DCSTemPurchaseOrderStatus.部分发运 || (entities[0].Status == (int)DCSTemPurchaseOrderStatus.发运完毕 && entities[0].PartsSalesOrderCode == null));
                    if(uniqueId == CommonActionKeys.TERMINATE)
                        return (entities[0].Status != (int)DCSTemPurchaseOrderStatus.发运完毕 && entities[0].Status != (int)DCSTemPurchaseOrderStatus.强制完成 && entities[0].Status != (int)DCSTemPurchaseOrderStatus.终止);
                    if(uniqueId == "PendingConfirmation")
                        return entities[0].Status == (int)DCSTemPurchaseOrderStatus.提交;
                    if(uniqueId == "ReplaceShip")
                        return entities[0].Status == (int)DCSTemPurchaseOrderStatus.确认完毕 || entities[0].Status == (int)DCSTemPurchaseOrderStatus.部分发运;
                    if(uniqueId == "AppRoveSubmit")
                        return entities[0].ApproveStatus == (int)DCSTemPurchaseOrderApproveStatus.新增;
                    if(uniqueId == "InitialApprove")
                        return entities[0].ApproveStatus == (int)DCSTemPurchaseOrderApproveStatus.提交;
                    if(uniqueId == "FinalApprove")
                        return entities[0].ApproveStatus == (int)DCSTemPurchaseOrderApproveStatus.审核通过;
                    return false;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitiesPrint = this.DataGridView.SelectedEntities.Cast<TemPurchaseOrder>().ToArray();
                    return (entitiesPrint.Length == 1);
                case "ReplaceShipAll"://汇总发运
                    return true;
                default:
                    return false;
            }
        }



        private void Export临时采购订单(int[] partsPurchaseOrderIds, int personelId, string temPurchasePlanOrderCode, string suplierName, string code, int? approveStatus, int? receiveStatus, int? status, int? customerType, string receCompanyCode, string receCompanyName, DateTime? createTimeBegin, DateTime? createTimeEnd, string sparePartCode, bool? isSaleCode, bool? isPurchaseOrder) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.Export临时采购订单Async(partsPurchaseOrderIds, personelId, temPurchasePlanOrderCode, suplierName, code, approveStatus, receiveStatus, status, customerType, receCompanyCode, receCompanyName, createTimeBegin, createTimeEnd, sparePartCode, isSaleCode, isPurchaseOrder);
            this.excelServiceClient.Export临时采购订单Completed -= excelServiceClient_Export临时采购订单ilCompleted;
            this.excelServiceClient.Export临时采购订单Completed += excelServiceClient_Export临时采购订单ilCompleted;
        }

      
        private void excelServiceClient_Export临时采购订单ilCompleted(object sender, Export临时采购订单CompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.filename));
        }
    }
}
