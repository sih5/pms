﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.ViewModel;
using WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views
{
    [PageMeta("PartsPurchasing", "PartsPurchasing", "TemPurchaseOrderSpareParts", ActionPanelKeys = new[] {
       CommonActionKeys.EXPORT
    })]
    public class TemPurchaseOrderSparePartsReport: DcsDataManagementViewBase {
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public TemPurchaseOrderSparePartsReport()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = "临时订单配件资料待维护查询";
        }
        private DataGridViewBase dataGridView;      
    
        private DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("TemPurchaseOrderSpareParts");
                }
                return this.dataGridView;
            }
        }
      
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
         
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "TemPurchaseOrderSpareParts"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                //合并导出
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
           
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                //合并导出
                case CommonActionKeys.EXPORT:
                        var filtes = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filtes == null)
                            return;
                        var code = filtes.Filters.SingleOrDefault(e => e.MemberName == "Code").Value as string;
                        var isPurchase = filtes.Filters.SingleOrDefault(e => e.MemberName == "IsPurchase").Value as bool?;
                        var suplierCode = filtes.Filters.SingleOrDefault(e => e.MemberName == "SuplierCode").Value as string;
                        var suplierName = filtes.Filters.SingleOrDefault(e => e.MemberName == "SuplierName").Value as string;
                        var isSales = filtes.Filters.SingleOrDefault(e => e.MemberName == "IsSales").Value as bool?;
                        var sparePartCode = filtes.Filters.SingleOrDefault(e => e.MemberName == "SparePartCode").Value as string;
                        this.dcsDomainContext.导出临时订单配件资料待维护(code, sparePartCode, suplierCode, suplierName, isPurchase, isSales, loadOp =>
                        {
                            if (loadOp.HasError)
                            {
                                ShellViewModel.Current.IsBusy = false;
                                return;
                            }
                            if (loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value))
                            {
                                UIHelper.ShowNotification(loadOp.Value);
                                ShellViewModel.Current.IsBusy = false;
                            }

                            if (loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                            {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                ShellViewModel.Current.IsBusy = false;
                            }
                        }, null);
                    break;
            }
        }
      
    }
}

