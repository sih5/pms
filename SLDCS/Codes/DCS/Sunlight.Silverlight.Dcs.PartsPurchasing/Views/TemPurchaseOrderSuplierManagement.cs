﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views {
    [PageMeta("PartsPurchasing", "PartsPurchasing", "TemPurchaseOrderSuplier", ActionPanelKeys = new[] {
        "TemPurchaseOrderSuplier"
    })]
    public class TemPurchaseOrderSuplierManagement: DcsDataManagementViewBase {

        private DataGridViewBase dataGridView;
        private DataEditViewBase dataReplaceShipView;
        private DataEditViewBase dataDetailView;
        private DataEditViewBase dataConfirmView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private const string DATA_REPLACESHIP_VIEW = "_dataReplaceShipView_";
        private const string DATA_REPLACESHIPALL_VIEW = "_dataReplaceShipAllView_";
        private const string DATA_DETAIL_VIEW = "_DataDetailView_";
        private const string DATA_CONFIRM_VIEW = "_DataConfirmView_";

        public TemPurchaseOrderSuplierManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = "临时订单管理-供应商";
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_REPLACESHIP_VIEW, () => this.DataReplaceShipView);
            this.RegisterView(DATA_REPLACESHIPALL_VIEW, () => this.DataReplaceShipAllView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataEditViewDetail);
            this.RegisterView(DATA_CONFIRM_VIEW, () => this.DataConfirmView);
        }
        private DataEditViewBase DataConfirmView {
            get {
                if(this.dataConfirmView == null) {
                    this.dataConfirmView = DI.GetDataEditView("TemPurchaseOrderConfirm");
                    ((TemPurchaseOrderConfirmDataEditView)this.dataConfirmView).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataConfirmView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataConfirmView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataConfirmView;
            }
        }
        private DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("TemPurchaseOrderSuplierWithDetails");
                    this.dataGridView.RowDoubleClick += DataGridView_RowDoubleClick;
                }
                return this.dataGridView;
            }
        }
        private void DataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                this.DataEditViewDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                this.SwitchViewTo(DATA_DETAIL_VIEW);
            }
        }                   
        private DataEditViewBase DataReplaceShipView {
            get {
                if(this.dataReplaceShipView == null) {
                    this.dataReplaceShipView = DI.GetDataEditView("TemPartsPurchaseOrderReplaceShip");
                    this.dataReplaceShipView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataReplaceShipView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataReplaceShipView;
            }
        }

        private DataEditViewBase dataReplaceShipAllView;
        private DataEditViewBase DataReplaceShipAllView {
            get {
                if(this.dataReplaceShipAllView == null) {
                    this.dataReplaceShipAllView = DI.GetDataEditView("TemPartsPurchaseOrderShipAll");
                    this.dataReplaceShipAllView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataReplaceShipAllView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataReplaceShipAllView;
            }
        }
        private DataEditViewBase DataEditViewDetail {
            get {
                if(this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("TemPartsPurchaseOrderDetail");
                    this.dataDetailView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataDetailView;
            }
        }
        private void ResetEditView() {
            this.dataReplaceShipView = null;
            this.dataConfirmView = null;
            this.dataReplaceShipAllView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "TemPurchaseOrderSuplier"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case "PendingConfirmation"://确认
                    this.DataConfirmView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_CONFIRM_VIEW);
                    break;
                case "ReplaceShip"://代发运
                    this.CreatePartsShipOrder();
                    break;
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<TemPurchaseOrder>().Select(r => r.Id).ToArray();
                        this.Export临时采购订单供应商(ids, BaseApp.Current.CurrentUserData.EnterpriseId, null, null, null, null, null, null, null, null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var temPurchasePlanOrderCode = filterItem.Filters.Single(r => r.MemberName == "TemPurchasePlanOrderCode").Value as string;
                        var status = filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var orderType = filterItem.Filters.Single(e => e.MemberName == "OrderType").Value as int?;
                        var compositeFilterItems = filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).ToArray();
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        DateTime? bConfirmeTime = null;
                        DateTime? eConfirmeTime = null;

                        foreach(var dateTimeFilterItem in compositeFilterItems) {
                            var dateTime = dateTimeFilterItem as CompositeFilterItem;
                            if(dateTime != null) {
                                if(dateTime.Filters.Any(r => r.MemberName == "CreateTime")) {
                                    createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                                if(dateTime.Filters.Any(r => r.MemberName == "ConfirmeTime")) {
                                    bConfirmeTime = dateTime.Filters.First(r => r.MemberName == "ConfirmeTime").Value as DateTime?;
                                    eConfirmeTime = dateTime.Filters.Last(r => r.MemberName == "ConfirmeTime").Value as DateTime?;
                                }
                            }
                        }
                        ShellViewModel.Current.IsBusy = true;
                        var partsPurchaseOrderIds = new int[] { };
                        if(this.DataGridView.SelectedEntities != null) {
                            var partsPurchaseOrders = this.DataGridView.SelectedEntities.Cast<TemPurchaseOrder>();
                            partsPurchaseOrderIds = partsPurchaseOrders.Select(r => r.Id).ToArray();
                        }
                        this.Export临时采购订单供应商(null, BaseApp.Current.CurrentUserData.EnterpriseId, temPurchasePlanOrderCode, code, status, orderType, createTimeBegin, createTimeEnd, bConfirmeTime, eConfirmeTime);
                    }
                    break;
                case CommonActionKeys.PRINT:
                    var print = this.DataGridView.SelectedEntities.Cast<TemPurchaseOrder>().FirstOrDefault();
                    SunlightPrinter.ShowPrinter(PartsPurchasingUIStrings.DataManagementView_Text_PurchaseOrderPrint, "TemReportPartsPurchaseOrder", null, true, new Tuple<string, string>("partsPurchaseOrderId", print.Id.ToString()));
                    break;
                case "ReplaceShipAll"://汇总发运
                    this.SwitchViewTo(DATA_REPLACESHIPALL_VIEW);
                    break;
            }
        }

        DateTime DtPlanDeliveryTime = DateTime.Now;

        private void CreatePartsShipOrder() {
            var partsPurchaseOrder = this.DataGridView.SelectedEntities.First() as TemPurchaseOrder;
            var domainContext = new DcsDomainContext();
            if(partsPurchaseOrder != null)
                //赋值：计划到货时间：供应商发运单的生成日期 + 供应商物流周期 默认计算。年月日，不带时分秒
                domainContext.Load(domainContext.GetBranchSupplierRelationBySupplierIdQuery(221, partsPurchaseOrder.SuplierId.Value, BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp1 => {
                    if(loadOp1.HasError) {
                        if(!loadOp1.IsErrorHandled)
                            loadOp1.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp1);
                        return;
                    }
                    if(loadOp1.Entities.Count() > 0) {
                        DtPlanDeliveryTime = DateTime.Now.Date.AddDays(loadOp1.Entities.First().ArrivalCycle ?? 0);
                    }
                }, null);
            domainContext.Load(domainContext.GetTemPurchaseOrdersWithDetailsByIdQuery(partsPurchaseOrder.Id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var item = loadOp.Entities.FirstOrDefault();
                if(item == null)
                    return;
                var supplierShippingOrder = this.DataReplaceShipView.CreateObjectToEdit<TemSupplierShippingOrder>();
                supplierShippingOrder.PartsSupplierId = item.SuplierId.Value;
                supplierShippingOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                supplierShippingOrder.PartsSupplierCode = item.SuplierCode;
                supplierShippingOrder.PartsSupplierName = item.SuplierName;
                supplierShippingOrder.TemPurchaseOrderId = item.Id;
                supplierShippingOrder.TemPurchaseOrderCode = item.Code;
                supplierShippingOrder.ReceivingWarehouseId = item.WarehouseId.Value;
                supplierShippingOrder.ReceivingWarehouseName = item.WarehouseName;
                supplierShippingOrder.ReceivingAddress = item.ReceiveAddress;
                supplierShippingOrder.PlanDeliveryTime = DtPlanDeliveryTime;
                supplierShippingOrder.Remark = item.Memo;
                supplierShippingOrder.OriginalRequirementBillId = item.TemPurchasePlanOrderId;
                supplierShippingOrder.OriginalRequirementBillCode = item.TemPurchasePlanOrderCode;
                supplierShippingOrder.ReceivingCompanyId = item.ReceCompanyId;
                supplierShippingOrder.ReceivingCompanyName = item.ReceCompanyName;
                supplierShippingOrder.ReceivingCompanyCode = item.ReceCompanyCode;
                supplierShippingOrder.Status = (int)DCSTemSupplierShippingOrderStatus.新建;
                supplierShippingOrder.ShippingDate = DateTime.Now;
                supplierShippingOrder.ShippingMethod = item.ShippingMethod ?? 0;
                foreach(var detail in item.TemPurchaseOrderDetails) {
                    if(detail.ShippingAmount == null) {
                        detail.ShippingAmount = 0;
                    }
                    if(detail.ConfirmedAmount > 0 && detail.ConfirmedAmount != detail.ShippingAmount) {
                        var supplierShippingDetail = new TemShippingOrderDetail();
                        supplierShippingDetail.SparePartId = detail.SparePartId.Value;
                        supplierShippingDetail.SparePartCode = detail.SparePartCode;
                        supplierShippingDetail.SparePartName = detail.SparePartName;
                        supplierShippingDetail.SupplierPartCode = detail.SupplierPartCode;
                        supplierShippingDetail.MeasureUnit = detail.MeasureUnit;
                        supplierShippingDetail.ConfirmedAmount = 0;
                        supplierShippingDetail.Quantity = detail.ConfirmedAmount - detail.ShippingAmount.Value;
                        supplierShippingDetail.PendingQuantity = supplierShippingDetail.Quantity;
                        if(supplierShippingDetail.Quantity > 0)
                            supplierShippingOrder.TemShippingOrderDetails.Add(supplierShippingDetail);
                    }
                }
                this.SwitchViewTo(DATA_REPLACESHIP_VIEW);
            }, null);
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case "ReplaceShip":
                case "PendingConfirmation":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<TemPurchaseOrder>().ToArray();
                    if(entities.Length != 1)
                        return false;                
                    if(uniqueId == "PendingConfirmation")
                        return entities[0].Status == (int)DCSTemPurchaseOrderStatus.提交;
                    if(uniqueId == "ReplaceShip")
                        return entities[0].Status == (int)DCSTemPurchaseOrderStatus.确认完毕 || entities[0].Status == (int)DCSTemPurchaseOrderStatus.部分发运;                  
                    return false;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitiesPrint = this.DataGridView.SelectedEntities.Cast<TemPurchaseOrder>().ToArray();
                    return (entitiesPrint.Length == 1);
                case "ReplaceShipAll"://汇总发运
                    return true;
                default:
                    return false;
            }
        }



        private void Export临时采购订单供应商(int[] ids, int suplierId, string temPurchasePlanOrderCode, string code, int? status,int? orderType, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? bConfirmeTime, DateTime? eConfirmeTime){
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.Export临时采购订单供应商Async(ids, suplierId,temPurchasePlanOrderCode, code, status, orderType, createTimeBegin, createTimeEnd, bConfirmeTime, eConfirmeTime);
            this.excelServiceClient.Export临时采购订单供应商Completed -= excelServiceClient_Export临时采购订单供应商ilCompleted;
            this.excelServiceClient.Export临时采购订单供应商Completed += excelServiceClient_Export临时采购订单供应商ilCompleted;
        }

      
        private void excelServiceClient_Export临时采购订单供应商ilCompleted(object sender, Export临时采购订单供应商CompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.filename));
        }
    }
}
