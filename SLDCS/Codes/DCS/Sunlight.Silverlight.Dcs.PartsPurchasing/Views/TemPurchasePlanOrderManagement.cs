﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.ViewModel;
using WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views {
    [PageMeta("PartsPurchasing", "PartsPurchasing", "TemPurchasePlanOrder", ActionPanelKeys = new[] {
        "TemPurchasePlanOrder"
    })]
    public class TemPurchasePlanOrderManagement : DcsDataManagementViewBase {
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public TemPurchasePlanOrderManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsPurchasingUIStrings.DataManagementView_Title_TmpPartsPurchasePlanOrderManage;
        }
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataDetailView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditForFirstApproveView;
        private DataEditViewBase dataEditForCheckView;
        private DataEditViewBase dataEditForConfirmView;
        private const string DATA_DETAIL_VIEW = "_DataDetailView_";
        private const string DATA_EDITFORFIRSTAPPROVE_VIEW = "_DataEditForFirstApproveView_";
        private const string DATA_EDITFORCHECK_VIEW = "_DataEditForCheckView_";
        private const string DATA_EDITFORCONFIRM_VIEW = "_DataEditForConfirmView_";


        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("TemPurchasePlanOrder");
                    this.dataEditView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataDetailView = null;
            this.dataEditForFirstApproveView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("TemPurchasePlanOrder");
                    this.dataGridView.RowDoubleClick += DataGridView_RowDoubleClick;
                }
                return this.dataGridView;
            }
        }
        private void DataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                this.DataEditViewDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                this.SwitchViewTo(DATA_DETAIL_VIEW);
            }
        }

        private DataEditViewBase DataEditViewDetail {
            get {
                if(this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("TemPurchasePlanOrderReportDetail");
                    this.dataDetailView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataDetailView;
            }
        }
        //审核
        private DataEditViewBase DataEditForFirstApproveView {
            get {
                if(this.dataEditForFirstApproveView == null) {
                    this.dataEditForFirstApproveView = DI.GetDataEditView("TemPurchasePlanOrderFirstApprove");
                    ((TemPurchasePlanOrderFirstApproveDataEditView)this.dataEditForFirstApproveView).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditForFirstApproveView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataEditForFirstApproveView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataEditForFirstApproveView;
            }
        }
        //复审
        private DataEditViewBase DataEditForCheckView {
            get {
                if(this.dataEditForCheckView == null) {
                    this.dataEditForCheckView = DI.GetDataEditView("TemPurchasePlanOrderCheck");
                    ((TemPurchasePlanOrderCheckDataEditView)this.dataEditForCheckView).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditForCheckView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataEditForCheckView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataEditForCheckView;
            }
        }
        //确认
        private DataEditViewBase DataEditForConfirmView {
            get {
                if(this.dataEditForConfirmView == null) {
                    this.dataEditForConfirmView = DI.GetDataEditView("TemPurchasePlanOrderConfirm");
                    ((TemPurchasePlanOrderConfirmDataEditView)this.dataEditForConfirmView).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditForConfirmView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataEditForConfirmView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataEditForConfirmView;
            }
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataEditViewDetail);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDITFORFIRSTAPPROVE_VIEW, () => this.DataEditForFirstApproveView);
            this.RegisterView(DATA_EDITFORCHECK_VIEW, () => this.DataEditForCheckView);
            this.RegisterView(DATA_EDITFORCONFIRM_VIEW, () => this.DataEditForConfirmView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "TemPurchasePlanOrder"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                //新增
                case "AddByDeputy":
                    return true;     
                case CommonActionKeys.APPROVE:
                     if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                     var entities = this.DataGridView.SelectedEntities.Cast<TemPurchasePlanOrder>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DCSTemPurchasePlanOrderStatus.提交;
                case "Check":
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                     var entitiesCheck = this.DataGridView.SelectedEntities.Cast<TemPurchasePlanOrder>().ToArray();
                     if(entitiesCheck.Length != 1)
                        return false;
                     return entitiesCheck[0].Status == (int)DCSTemPurchasePlanOrderStatus.审核通过;
                case CommonActionKeys.CONFIRM:
                     if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                         return false;
                     var entitiesConfirm = this.DataGridView.SelectedEntities.Cast<TemPurchasePlanOrder>().ToArray();
                     if(entitiesConfirm.Length != 1)
                         return false;
                     return entitiesConfirm[0].Status == (int)DCSTemPurchasePlanOrderStatus.复核通过;
                //合并导出
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
           
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                //新增
                case "AddByDeputy":
                    var temPurchasePlanOrder = this.DataEditView.CreateObjectToEdit<TemPurchasePlanOrder>();
                    temPurchasePlanOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    temPurchasePlanOrder.IsTurnSale = false;
                    temPurchasePlanOrder.FreightType = (int)DCSTemPurchasePlanOrderFreightType.到付;
                    var dataEditView = this.DataEditView as TemPurchasePlanOrderDataEditView;
                    dataEditView.DataEditPanels.FilePath = null;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.APPROVE:
                    this.DataEditForFirstApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDITFORFIRSTAPPROVE_VIEW);
                    break;
                case "Check":
                    this.DataEditForCheckView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDITFORCHECK_VIEW);
                    break;
                case CommonActionKeys.CONFIRM:
                    this.DataEditForConfirmView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDITFORCONFIRM_VIEW);
                    break;
                //合并导出
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsPurchasePlan>().Select(r => r.Id).ToArray();
                        this.ExportPartsPurchasePlanOrder(ids, null, null, null, null, null, null, null, null, null);
                    } else {
                        var filtes = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filtes == null)
                            return;
                        var code = filtes.Filters.SingleOrDefault(e => e.MemberName == "Code").Value as string;
                        var sparePartCode = filtes.Filters.SingleOrDefault(e => e.MemberName == "SparePartCode").Value as string;
                        var status = filtes.Filters.SingleOrDefault(e => e.MemberName == "Status").Value as int?;
                        var receCompanyCode = filtes.Filters.SingleOrDefault(e => e.MemberName == "ReceCompanyCode").Value as string;
                        var receCompanyName = filtes.Filters.SingleOrDefault(e => e.MemberName == "ReceCompanyName").Value as string;
                        var customerType = filtes.Filters.SingleOrDefault(e => e.MemberName == "CustomerType").Value as int?;
                        var createTime = filtes.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem) && ((CompositeFilterItem)r).Filters.Any(e => e.MemberName == "CreateTime")) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        this.ExportPartsPurchasePlanOrder(null, BaseApp.Current.CurrentUserData.EnterpriseId, code, status, customerType, receCompanyCode, receCompanyName, createTimeBegin, createTimeEnd, sparePartCode);
                    }
                    break;
            }
        }
        //合并导出方法
        private void ExportPartsPurchasePlanOrder(int[] ids,int? companyId, string code, int? status,int?customerType, string receCompanyCode, string receCompanyName, DateTime? createTimeBegin, DateTime? createTimeEnd, string sparePartCode) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.Export临时采购计划Async(ids, companyId.Value, code, status, customerType,receCompanyCode, receCompanyName, createTimeBegin, createTimeEnd, sparePartCode);
            this.excelServiceClient.Export临时采购计划Completed += ExcelServiceClient_ExportExport临时采购计划Completed;
            this.excelServiceClient.Export临时采购计划Completed += ExcelServiceClient_ExportExport临时采购计划Completed;
        }

        private void ExcelServiceClient_ExportExport临时采购计划Completed(object sender, Export临时采购计划CompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.filename));
        }
    }
}
