﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.ViewModel;
using WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Views.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views {
    [PageMeta("PartsPurchasing", "PartsPurchasing", "TemPurchasePlanOrderReport", ActionPanelKeys = new[] {
        "TemPurchasePlanOrderReport"
    })]
    public class TemPurchasePlanOrderReportManagement : DcsDataManagementViewBase {
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public TemPurchasePlanOrderReportManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsPurchasingUIStrings.DataManagementView_Title_TmpPartsPurchasePlanOrder;
        }
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataDetailView;
        private DataEditViewBase dataEditView;
        private const string DATA_DETAIL_VIEW = "_DataDetailView_";

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("TemPurchasePlanOrderReport");
                    this.dataEditView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataDetailView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("TemPurchasePlanOrderReport");
                    this.dataGridView.RowDoubleClick += DataGridView_RowDoubleClick;
                }
                return this.dataGridView;
            }
        }
        private void DataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                this.DataEditViewDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                this.SwitchViewTo(DATA_DETAIL_VIEW);
            }
        }

        private DataEditViewBase DataEditViewDetail {
            get {
                if(this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("TemPurchasePlanOrderReportDetail");
                    this.dataDetailView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataDetailView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataEditViewDetail);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "TemPurchasePlanOrderReport"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                //新增
                case CommonActionKeys.ADD:
                    return true;
                //修改、提交、作废
                case CommonActionKeys.EDIT:
                case CommonActionKeys.SUBMIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<TemPurchasePlanOrder>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DCSTemPurchasePlanOrderStatus.新建;
                //合并导出
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "ReceCompanyId",
                Operator = FilterOperator.IsEqualTo,
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                //新增
                case CommonActionKeys.ADD:
                    var temPurchasePlanOrder = this.DataEditView.CreateObjectToEdit<TemPurchasePlanOrder>();
                    temPurchasePlanOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    temPurchasePlanOrder.Status = (int)DcsPurchasePlanOrderStatus.新增;
                    temPurchasePlanOrder.FreightType = (int)DCSTemPurchasePlanOrderFreightType.到付;
                    temPurchasePlanOrder.IsTurnSale = true;
                    var dataEditView = this.DataEditView as TemPurchasePlanOrderReportDataEditView;
                    dataEditView.DataEditPanels.FilePath = null;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                //修改
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                //提交
                case CommonActionKeys.SUBMIT:
                    //此处重写Invoke方法
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Confirm_Submit, () => {
                        try {
                            ShellViewModel.Current.IsBusy = true;
                            var entity = this.DataGridView.SelectedEntities.Cast<TemPurchasePlanOrder>().SingleOrDefault();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            domainContext.提交临时采购计划订单(entity.Id, invokeOp => {
                                if(invokeOp.HasError) {
                                    if(!invokeOp.IsErrorHandled)
                                        invokeOp.MarkErrorAsHandled();
                                    UIHelper.ShowAlertMessage(invokeOp.ValidationErrors.FirstOrDefault().ErrorMessage);
                                    return;
                                }
                                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Notification_SubmitSuccess);
                                ShellViewModel.Current.IsBusy = false;
                                this.DataGridView.ExecuteQueryDelayed();
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                            ShellViewModel.Current.IsBusy = false;
                        }
                        ShellViewModel.Current.IsBusy = false;
                    });
                    break;
                //作废
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsPurchasingUIStrings.DataManagementView_Confirm_Abandon, () => {
                        try {
                            ShellViewModel.Current.IsBusy = true;
                            var entitys = this.DataGridView.SelectedEntities.Cast<TemPurchasePlanOrder>().SingleOrDefault();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            domainContext.作废临时采购计划单(entitys.Id, invokeOp => {
                                if(invokeOp.HasError) {
                                    if(!invokeOp.IsErrorHandled)
                                        invokeOp.MarkErrorAsHandled();
                                    UIHelper.ShowAlertMessage(invokeOp.ValidationErrors.FirstOrDefault().ErrorMessage);
                                    return;
                                }
                                UIHelper.ShowNotification(PartsPurchasingUIStrings.DataManagementView_Notification_AbandonSuccess);
                                ShellViewModel.Current.IsBusy = false;
                                this.DataGridView.ExecuteQueryDelayed();
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                            ShellViewModel.Current.IsBusy = false;
                        }
                        ShellViewModel.Current.IsBusy = false;
                    });
                    break;
                //合并导出
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<TemPurchasePlanOrder>().Select(r => r.Id).ToArray();
                        this.ExportPartsPurchasePlanOrder(ids, null, null, null, null, null, null, null);
                    } else {
                        var filtes = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filtes == null)
                            return;
                        var code = filtes.Filters.SingleOrDefault(e => e.MemberName == "Code").Value as string;
                        var sparePartCode = filtes.Filters.SingleOrDefault(e => e.MemberName == "SparePartCode").Value as string;
                        var status = filtes.Filters.SingleOrDefault(e => e.MemberName == "Status").Value as int?;
                        var receCompanyCode = filtes.Filters.SingleOrDefault(e => e.MemberName == "ReceCompanyCode").Value as string;
                        var receCompanyName = filtes.Filters.SingleOrDefault(e => e.MemberName == "ReceCompanyName").Value as string;
                        var createTime = filtes.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem) && ((CompositeFilterItem)r).Filters.Any(e => e.MemberName == "CreateTime")) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        this.ExportPartsPurchasePlanOrder(null, code, status, receCompanyCode, receCompanyName, createTimeBegin, createTimeEnd, sparePartCode);
                    }
                    break;
            }
        }
        //合并导出方法
        private void ExportPartsPurchasePlanOrder(int[] ids, string code, int? status, string receCompanyCode, string receCompanyName, DateTime? createTimeBegin, DateTime? createTimeEnd, string sparePartCode) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportTemPurchasePlanOrderWithDetailAsync(ids, code, status, receCompanyCode, receCompanyName, createTimeBegin, createTimeEnd, sparePartCode);
            this.excelServiceClient.ExportTemPurchasePlanOrderWithDetailCompleted += ExcelServiceClient_ExportExportTemPurchasePlanOrderWithDetailCompleted;
            this.excelServiceClient.ExportTemPurchasePlanOrderWithDetailCompleted += ExcelServiceClient_ExportExportTemPurchasePlanOrderWithDetailCompleted;
        }

        private void ExcelServiceClient_ExportExportTemPurchasePlanOrderWithDetailCompleted(object sender, ExportTemPurchasePlanOrderWithDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.filename));
        }
    }
}
