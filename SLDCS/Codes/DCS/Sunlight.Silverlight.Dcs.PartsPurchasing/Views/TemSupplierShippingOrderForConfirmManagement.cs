﻿using System;
using System.Collections.Generic;
using System.Linq;
﻿using System.Windows;
﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
﻿using Sunlight.Silverlight.Dcs.Print;
﻿using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using System.Windows.Browser;
using Sunlight.Silverlight.ViewModel;
﻿using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views {
    [PageMeta("PartsPurchasing", "PartsPurchasing", "TemSupplierShippingOrderConfirm", ActionPanelKeys = new[] {
           "TemSupplierShippingOrderConFirm"
    })]
    public class TemSupplierShippingOrderForConfirmManagement  : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataConfirmtView;

        private const string DATAEDITVIEWFOREDIT = "_dataEditViewForEdit_";
        private const string DATAEDITVIEWFORCONFIRM= "_dataEditViewForConfirm_";
        private DataEditViewBase dataEditViewForEdit;
        private DataEditViewBase DataEditViewForEdit {
            get {
                if(dataEditViewForEdit == null) {
                    dataEditViewForEdit = DI.GetDataEditView("TemSupplierShippingOrderEdit");
                    dataEditViewForEdit.EditSubmitted += this.DataEditView_EditSubmitted;
                    dataEditViewForEdit.EditCancelled += this.DataEditView_EditCancelled;
                }
                return dataEditViewForEdit;
            }
        }

        public TemSupplierShippingOrderForConfirmManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = "临时订单发运确认管理";
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("TemSupplierShippingOrderConfirm"));
            }
        }

        private DataEditViewBase DataConfirmtView {
            get {
                if(this.dataConfirmtView == null) {
                    this.dataConfirmtView = DI.GetDataEditView("TemSupplierShippingOrderConfirm");
                    this.dataConfirmtView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataConfirmtView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataConfirmtView;
            }
        }
    
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATAEDITVIEWFOREDIT, () => this.DataEditViewForEdit);
            this.RegisterView(DATAEDITVIEWFORCONFIRM, () => this.DataConfirmtView);
        }
        private void ResetEditView() {
            this.dataEditViewForEdit = null;
            this.dataConfirmtView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = new CompositeFilterItem();
            if (filterItem is CompositeFilterItem)
            {
                var compositeFilter = filterItem as CompositeFilterItem;
                compositeFilter.Filters.Add(new FilterItem
                {
                    MemberName = "OrderCompanyId",
                    Operator = FilterOperator.IsEqualTo,
                    MemberType = typeof(int),
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });              
                compositeFilterItem.Filters.Add(compositeFilter);
            }
            else
                compositeFilterItem.Filters.Add(filterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
          
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "TemSupplierShippingOrderConFirm"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {                           
                case CommonActionKeys.CONFIRM:
                    this.DataConfirmtView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATAEDITVIEWFORCONFIRM);
                    break;              
                case CommonActionKeys.EXPORT:
                    //如果选中一条数据 合并导出参数为 ID  
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any())
                    {
                        var ids = this.DataGridView.SelectedEntities.Cast<TemSupplierShippingOrder>().Select(r => r.Id).ToArray();
                        this.ExecuteMergeExport(ids, BaseApp.Current.CurrentUserData.EnterpriseId, null, null, null, null, null, null);
                    }
                    else
                    {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if (filterItem == null)
                            return;
                        var filtes = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        if(filtes == null)
                            return;
                        var code = filtes.Filters.SingleOrDefault(e => e.MemberName == "Code").Value as string;
                        var originalRequirementBillCode = filtes.Filters.SingleOrDefault(e => e.MemberName == "OriginalRequirementBillCode").Value as string;
                        var temPurchaseOrderCode = filtes.Filters.SingleOrDefault(e => e.MemberName == "TemPurchaseOrderCode").Value as string;
                        var status = filtes.Filters.SingleOrDefault(e => e.MemberName == "Status").Value as int?;
                     
                        var createTime = filtes.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem) && ((CompositeFilterItem)r).Filters.Any(e => e.MemberName == "CreateTime")) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                         if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                         this.ExecuteMergeExport(null, BaseApp.Current.CurrentUserData.EnterpriseId, originalRequirementBillCode, temPurchaseOrderCode, code, status, createTimeBegin, createTimeEnd);
                    }
                    break;              
            }
        }
      
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {           

                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.CONFIRM:
                       if(this.DataGridView.SelectedEntities == null)
                        return false;
                     var fentities = this.DataGridView.SelectedEntities.Cast<TemSupplierShippingOrder>().ToArray();
                     if(fentities.Length != 1)
                        return false;
                     return (fentities[0].Status == (int)DCSTemSupplierShippingOrderStatus.新建 || fentities[0].Status == (int)DCSTemSupplierShippingOrderStatus.部分收货);                
                default:
                    return false;
            }
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private void ExecuteMergeExport(int[] ids, int? orderCompanyId, string originalRequirementBillCode, string temPurchaseOrderCode, string code, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd)
        {
            ShellViewModel.Current.IsBusy = false;
            this.excelServiceClient.Export临时采购发运单确认Async(ids, orderCompanyId, originalRequirementBillCode, temPurchaseOrderCode, code, status, createTimeBegin, createTimeEnd);
            this.excelServiceClient.Export临时采购发运单确认Completed -= this.ExcelServiceClient_Export临时采购发运单确认Completed;
            this.excelServiceClient.Export临时采购发运单确认Completed += this.ExcelServiceClient_Export临时采购发运单确认Completed;
        }

        private void ExcelServiceClient_Export临时采购发运单确认Completed(object sender, Export临时采购发运单确认CompletedEventArgs e)
        {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.filename));
        }

    }
}
