﻿using System;
using System.Collections.Generic;
using System.Linq;
﻿using System.Windows;
﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
﻿using Sunlight.Silverlight.Dcs.Print;
﻿using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using System.Windows.Browser;
using Sunlight.Silverlight.ViewModel;
﻿using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views {
     [PageMeta("PartsPurchasing", "PartsPurchasing", "TemSupplierShippingOrder", ActionPanelKeys = new[] {
           "TemSupplierShippingOrder"
    })]
    public class TemSupplierShippingOrderManagement  : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataConfirmtView;

        private const string DATAEDITVIEWFOREDIT = "_dataEditViewForEdit_";
        private const string DATAEDITVIEWFORCONFIRM= "_dataEditViewForConfirm_";
        private DataEditViewBase dataEditViewForEdit;
        private DataEditViewBase DataEditViewForEdit {
            get {
                if(dataEditViewForEdit == null) {
                    dataEditViewForEdit = DI.GetDataEditView("TemSupplierShippingOrderEdit");
                    dataEditViewForEdit.EditSubmitted += this.DataEditView_EditSubmitted;
                    dataEditViewForEdit.EditCancelled += this.DataEditView_EditCancelled;
                }
                return dataEditViewForEdit;
            }
        }

        public TemSupplierShippingOrderManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = "临时订单发运管理";
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("TemSupplierShippingOrder"));
            }
        }

        private DataEditViewBase DataConfirmtView {
            get {
                if(this.dataConfirmtView == null) {
                    this.dataConfirmtView = DI.GetDataEditView("TemSupplierShippingOrderConfirm");
                    this.dataConfirmtView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataConfirmtView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataConfirmtView;
            }
        }
    
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATAEDITVIEWFOREDIT, () => this.DataEditViewForEdit);
            this.RegisterView(DATAEDITVIEWFORCONFIRM, () => this.DataConfirmtView);
        }
        private void ResetEditView() {
            this.dataEditViewForEdit = null;
            this.dataConfirmtView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "TemSupplierShippingOrder"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
              
                case CommonActionKeys.EDIT:
                    this.DataEditViewForEdit.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATAEDITVIEWFOREDIT);
                    break;
                case CommonActionKeys.CONFIRM:
                    this.DataConfirmtView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATAEDITVIEWFORCONFIRM);
                    break;
                case  CommonActionKeys.TERMINATE:
                    //此处重写Invoke方法
                    DcsUtils.Confirm("确定要终止所选数据？", () => {
                        try {
                            var entity = this.DataGridView.SelectedEntities.Cast<TemSupplierShippingOrder>().ToArray();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext != null) {

                                domainContext.终止临时供应商发运单(entity.First().Id, invokeOp => {
                                    if(invokeOp.HasError)
                                        return;
                                    UIHelper.ShowNotification("终止成功");
                                    this.DataGridView.ExecuteQueryDelayed();
                                    this.CheckActionsCanExecute();
                                }, null);
                            }

                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;               
                case CommonActionKeys.PRINT:
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<TemSupplierShippingOrder>().FirstOrDefault();
                    if(selectedItem == null)
                        return;
                    SunlightPrinter.ShowPrinter("临时发运单打印", "TemSupplierShippingOrder", null, true, new Tuple<string, string>("TemSupplierShippingOrderId", selectedItem.Id.ToString()), new Tuple<string, string>("UserName", BaseApp.Current.CurrentUserData.UserName));

                    break;
                case "A4Print":
                    var selectedA4 = this.DataGridView.SelectedEntities.Cast<TemSupplierShippingOrder>().FirstOrDefault();
                    if(selectedA4 == null)
                        return;
                    SunlightPrinter.ShowPrinter(PartsPurchasingUIStrings.DataManagementView_Text_SupplierShippingPrint, "TemSupplierShippingOrderNoBarSihA4", null, true, new Tuple<string, string>("supplierShippingOrderId", selectedA4.Id.ToString()), new Tuple<string, string>("UserName", BaseApp.Current.CurrentUserData.UserName));

                    break;
                case CommonActionKeys.EXPORT:
                    //如果选中一条数据 合并导出参数为 ID  
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any())
                    {
                        var ids = this.DataGridView.SelectedEntities.Cast<TemSupplierShippingOrder>().Select(r => r.Id).ToArray();
                        this.ExecuteMergeExport(ids, null, null, null, null, null, null, null, null, null);
                    }
                    else
                    {
                        var filtes = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filtes == null)
                            return;
                        var code = filtes.Filters.SingleOrDefault(e => e.MemberName == "Code").Value as string;
                        var receivingWarehouseName = filtes.Filters.SingleOrDefault(e => e.MemberName == "ReceivingWarehouseName").Value as string;
                        var temPurchaseOrderCode = filtes.Filters.SingleOrDefault(e => e.MemberName == "TemPurchaseOrderCode").Value as string;
                        var status = filtes.Filters.SingleOrDefault(e => e.MemberName == "Status").Value as int?;
                        var partsSupplierName = filtes.Filters.SingleOrDefault(e => e.MemberName == "PartsSupplierName").Value as string;
                        var partsSupplierCode = filtes.Filters.SingleOrDefault(e => e.MemberName == "PartsSupplierCode").Value as string;
                        var createTime = filtes.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem) && ((CompositeFilterItem)r).Filters.Any(e => e.MemberName == "CreateTime")) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                         if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        this.ExecuteMergeExport(null, null, partsSupplierCode, temPurchaseOrderCode, partsSupplierName, code, status, receivingWarehouseName, createTimeBegin, createTimeEnd);
                    }
                    break;
              
            }
        }
      
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {           
                case CommonActionKeys.EDIT:
                     if(this.DataGridView.SelectedEntities == null)
                        return false;
                     var entities = this.DataGridView.SelectedEntities.Cast<TemSupplierShippingOrder>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return (entities[0].Status != (int)DCSTemSupplierShippingOrderStatus.作废);
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.CONFIRM:
                case CommonActionKeys.TERMINATE:
                       if(this.DataGridView.SelectedEntities == null)
                        return false;
                     var fentities = this.DataGridView.SelectedEntities.Cast<TemSupplierShippingOrder>().ToArray();
                     if(fentities.Length != 1)
                        return false;
                     return (fentities[0].Status == (int)DCSTemSupplierShippingOrderStatus.新建 || fentities[0].Status == (int)DCSTemSupplierShippingOrderStatus.部分收货); 
                case CommonActionKeys.PRINT:
                case "A4Print":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems = this.DataGridView.SelectedEntities.Cast<TemSupplierShippingOrder>().ToArray();
                    return selectItems.Length == 1;
              
                default:
                    return false;
            }
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private void ExecuteMergeExport(int[] ids, int? suplierId, string partsSupplierCode,string temPurchaseOrderCode, string partsSupplierName, string code, int? status, string receivingWarehouseName, DateTime? createTimeBegin, DateTime? createTimeEnd)
        {
            ShellViewModel.Current.IsBusy = false;
            this.excelServiceClient.Export临时采购发运单Async(ids,suplierId,  partsSupplierCode, temPurchaseOrderCode,  partsSupplierName,  code,  status,  receivingWarehouseName,  createTimeBegin,  createTimeEnd);
            this.excelServiceClient.Export临时采购发运单Completed -= this.ExcelServiceClient_Export临时采购发运单Completed;
            this.excelServiceClient.Export临时采购发运单Completed += this.ExcelServiceClient_Export临时采购发运单Completed;
        }

        private void ExcelServiceClient_Export临时采购发运单Completed(object sender, Export临时采购发运单CompletedEventArgs e)
        {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.filename));
        }

    }
}
