﻿using System;
using System.Collections.Generic;
using System.Linq;
﻿using System.Windows;
﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsPurchasing.Resources;
﻿using Sunlight.Silverlight.Dcs.Print;
﻿using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using System.Windows.Browser;
using Sunlight.Silverlight.ViewModel;
﻿using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsPurchasing.Views {
      [PageMeta("PartsPurchasing", "PartsPurchasing", "TemSupplierShippingOrderSupplier", ActionPanelKeys = new[] {
           "TemSupplierShippingOrder"
    })]
    public class TemSupplierShippingOrderSupplierManagement  : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;

        private const string DATAEDITVIEWFOREDIT = "_dataEditViewForEdit_";
        private DataEditViewBase dataEditViewForEdit;
        private DataEditViewBase DataEditViewForEdit {
            get {
                if(dataEditViewForEdit == null) {
                    dataEditViewForEdit = DI.GetDataEditView("TemSupplierShippingOrderEdit");
                    dataEditViewForEdit.EditSubmitted += this.DataEditView_EditSubmitted;
                    dataEditViewForEdit.EditCancelled += this.DataEditView_EditCancelled;
                }
                return dataEditViewForEdit;
            }
        }

        public TemSupplierShippingOrderSupplierManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = "临时订单发运管理-供应商";
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("TemSupplierShippingOrder"));
            }
        }

      
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATAEDITVIEWFOREDIT, () => this.DataEditViewForEdit);
        }
        private void ResetEditView() {
            this.dataEditViewForEdit = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                var compositeFilter = filterItem as CompositeFilterItem;
                compositeFilter.Filters.Add(new FilterItem {
                    MemberName = "PartsSupplierId",
                    Operator = FilterOperator.IsEqualTo,
                    MemberType = typeof(int),
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });               
                compositeFilterItem.Filters.Add(compositeFilter);
            } else
                compositeFilterItem.Filters.Add(filterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
          
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "TemSupplierShippingOrderSupplier"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
              
                case CommonActionKeys.EDIT:
                    this.DataEditViewForEdit.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATAEDITVIEWFOREDIT);
                    break;               
                case CommonActionKeys.PRINT:
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<TemSupplierShippingOrder>().FirstOrDefault();
                    if(selectedItem == null)
                        return;
                    SunlightPrinter.ShowPrinter(PartsPurchasingUIStrings.DataManagementView_Text_SupplierShippingPrint, "TemSupplierShippingOrder", null, true, new Tuple<string, string>("TemSupplierShippingOrderId", selectedItem.Id.ToString()), new Tuple<string, string>("UserName", BaseApp.Current.CurrentUserData.UserName));

                    break;
                case CommonActionKeys.EXPORT:
                    //如果选中一条数据 合并导出参数为 ID  
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any())
                    {
                        var ids = this.DataGridView.SelectedEntities.Cast<TemSupplierShippingOrder>().Select(r => r.Id).ToArray();
                        this.ExecuteMergeExport(ids, null, null, null, null, null, null, null, null, null);
                    }
                    else
                    {
                        var filtes = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filtes == null)
                            return;
                        var code = (filtes.Filters.FirstOrDefault() as CompositeFilterItem).Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : (filtes.Filters.FirstOrDefault() as CompositeFilterItem).Filters.Single(r => r.MemberName == "Code").Value as string;
                        var receivingWarehouseName = (filtes.Filters.FirstOrDefault() as CompositeFilterItem).Filters.SingleOrDefault(r => r.MemberName == "ReceivingWarehouseName") == null ? null : (filtes.Filters.FirstOrDefault() as CompositeFilterItem).Filters.Single(r => r.MemberName == "ReceivingWarehouseName").Value as string;
                        var temPurchaseOrderCode = (filtes.Filters.FirstOrDefault() as CompositeFilterItem).Filters.SingleOrDefault(r => r.MemberName == "TemPurchaseOrderCode") == null ? null : (filtes.Filters.FirstOrDefault() as CompositeFilterItem).Filters.Single(r => r.MemberName == "TemPurchaseOrderCode").Value as string;
                        var status = (filtes.Filters.FirstOrDefault() as CompositeFilterItem).Filters.SingleOrDefault(r => r.MemberName == "Status") == null ? null : (filtes.Filters.FirstOrDefault() as CompositeFilterItem).Filters.Single(r => r.MemberName == "Status").Value as int?;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        foreach(var filter in (filtes.Filters.FirstOrDefault() as CompositeFilterItem).Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var dateTime = filter as CompositeFilterItem;
                            {
                                if(dateTime.Filters.First().MemberName == "CreateTime") {
                                    createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                            }
                        }
                        this.ExecuteMergeExport(null, BaseApp.Current.CurrentUserData.EnterpriseId, null, temPurchaseOrderCode, null, code, status, receivingWarehouseName, createTimeBegin, createTimeEnd);
                    }
                    break;
              
            }
        }
      
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {           
                case CommonActionKeys.EDIT:
                     if(this.DataGridView.SelectedEntities == null)
                        return false;
                     var entities = this.DataGridView.SelectedEntities.Cast<TemSupplierShippingOrder>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return ( entities[0].Status != (int)DCSTemSupplierShippingOrderStatus.作废);
             
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
              
                case CommonActionKeys.PRINT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems = this.DataGridView.SelectedEntities.Cast<TemSupplierShippingOrder>().ToArray();
                    return selectItems.Length == 1;
              
                default:
                    return false;
            }
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private void ExecuteMergeExport(int[] ids, int? suplierId, string partsSupplierCode,string temPurchaseOrderCode, string partsSupplierName, string code, int? status, string receivingWarehouseName, DateTime? createTimeBegin, DateTime? createTimeEnd)
        {
            ShellViewModel.Current.IsBusy = false;
            this.excelServiceClient.Export临时采购发运单Async(ids,suplierId,  partsSupplierCode, temPurchaseOrderCode,  partsSupplierName,  code,  status,  receivingWarehouseName,  createTimeBegin,  createTimeEnd);
            this.excelServiceClient.Export临时采购发运单Completed -= this.ExcelServiceClient_Export临时采购发运单Completed;
            this.excelServiceClient.Export临时采购发运单Completed += this.ExcelServiceClient_Export临时采购发运单Completed;
        }

        private void ExcelServiceClient_Export临时采购发运单Completed(object sender, Export临时采购发运单CompletedEventArgs e)
        {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.filename));
        }

    }
}
