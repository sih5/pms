﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Action {
    public class AgencyRetailerOrderActionPanel : DcsActionPanelBase {
        public AgencyRetailerOrderActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "AgencyRetailerOrder",
                Title = PartsSalesUIStrings.ActionPanel_Title_AgencyRetailerOrder,
                ActionItems = new[] {
                    new ActionItem {
                        Title = PartsSalesUIStrings.Action_Title_Confirm,
                        UniqueId = "Confirm",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Confirm.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = PartsSalesUIStrings.Action_Title_Terminate,
                        UniqueId = "Terminate",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Terminate.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = PartsSalesUIStrings.Action_Title_Export,
                        UniqueId = "Export",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Export.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = PartsSalesUIStrings.Action_Title_MergeExport,
                        UniqueId = "MergeExport",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/MergeExport.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
