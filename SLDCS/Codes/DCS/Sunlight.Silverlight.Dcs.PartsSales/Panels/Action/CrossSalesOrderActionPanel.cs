﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Action {
    public class CrossSalesOrderActionPanel : DcsActionPanelBase {
        public CrossSalesOrderActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "CrossSalesOrder",
                Title = PartsSalesUIStrings.ActionPanel_Title_PartsSalesReturnBill,
                ActionItems = new[] {
                   new ActionItem {
                        Title = PartsSalesUIStrings.Action_Title_Add,
                        UniqueId = CommonActionKeys.ADD,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Add.png")
                    }, new ActionItem {
                        Title = PartsSalesUIStrings.Action_Title_Edit,
                        UniqueId = CommonActionKeys.EDIT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Edit.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = PartsSalesUIStrings.Action_Title_Submit,
                        UniqueId = CommonActionKeys.SUBMIT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Submit.png"),
                        CanExecute = false
                    }, new ActionItem{
                        Title = PartsSalesUIStrings.Action_Title_Abandon,
                        UniqueId = CommonActionKeys.ABANDON,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Abandon.png"),
                        CanExecute=false
                    }, new ActionItem {
                        Title = "初审",
                        UniqueId ="InitialApprove",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Approve.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = "审核",
                        UniqueId =CommonActionKeys.AUDIT,
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/FinalApprove.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title ="审批",
                        UniqueId = CommonActionKeys.APPROVE,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/AllowPick.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title ="高级审核",
                        UniqueId = "SeniorAudit",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/CheckStock.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title ="完善资料",
                        UniqueId = "Finish",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/MakeInvoice.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = PartsSalesUIStrings.Action_Title_Terminate,
                        UniqueId = CommonActionKeys.TERMINATE,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Terminate.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = PartsSalesUIStrings.Action_Title_MergeExport,
                        UniqueId = CommonActionKeys.MERGEEXPORT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/MergeExport.png"),
                        CanExecute = false
                    }

                }
            };
        }
    }
}
