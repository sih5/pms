﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Action {
    public class DealerPartsStockActionPanel : DcsActionPanelBase {
        public DealerPartsStockActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "DealerPartsStock",
                Title = PartsSalesUIStrings.DataEdit_Title_DealerPartsStock,
                ActionItems = new[] {
                    new ActionItem {
                        UniqueId = "InAndOutRecordQuery",
                        Title =PartsSalesUIStrings.DataEdit_Title_DealerPartsStockQuery,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Query.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
