﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class LogisticsQueryActionPanel : DcsActionPanelBase {
        public LogisticsQueryActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Logistics",
                Title = PartsSalesUIStrings.DataEdit_Title_Logistics,
                ActionItems = new[]{
                    new ActionItem{
                        Title =PartsSalesUIStrings.DataEdit_Title_LogisticsQuery,
                        UniqueId ="LogisticsQuery",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/shipping.png"),
                        CanExecute=false
                    }
                }
            };
        }
    }
}
