﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Action {
    public class OrderApproveWeekdayActionPanel : DcsActionPanelBase {
        public OrderApproveWeekdayActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "OrderApproveWeekday",
                Title = PartsSalesUIStrings.ActionPanel_Title_General,
                ActionItems = new[] {
                    new ActionItem {
                        Title = PartsSalesUIStrings.DataEdit_Title_ImportEdit,
                        UniqueId = "ImportEdit",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
