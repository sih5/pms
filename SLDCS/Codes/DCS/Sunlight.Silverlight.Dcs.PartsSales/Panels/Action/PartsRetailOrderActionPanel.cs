﻿
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Action {
    public class PartsRetailOrderActionPanel : DcsActionPanelBase {
        public PartsRetailOrderActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "PartsRetailOrder",
                Title = PartsSalesUIStrings.ActionPanel_Title_PartsRetailOrder,
                ActionItems = new[] {
                    new ActionItem {
                        Title = PartsSalesUIStrings.Action_Title_Invoice,
                        UniqueId = "MakeInvoice",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/MakeInvoice.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
