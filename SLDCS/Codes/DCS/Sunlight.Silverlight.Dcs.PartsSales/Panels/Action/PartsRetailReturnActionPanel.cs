﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Action {
    public class PartsRetailReturnActionPanel : DcsActionPanelBase {
        public PartsRetailReturnActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "PartsRetailReturnBill",
                Title = PartsSalesUIStrings.ActionPanel_Title_PartsRetailReturn,
                ActionItems = new[] {
                    new ActionItem {
                        Title = PartsSalesUIStrings.Action_Title_Refund,
                        UniqueId = "Refund",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Refund.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
