﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Action {
    public class PartsSalesOrderActionPanel : DcsActionPanelBase {
        public PartsSalesOrderActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "PartsSalesOrder",
                Title = PartsSalesUIStrings.ActionPanel_Title_PartsSalesOrder,
                ActionItems = new[] {
                    new ActionItem {
                        Title = PartsSalesUIStrings.Action_Title_AddByDeputy,
                        UniqueId = "AddByDeputy",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/AddByDeputy.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = PartsSalesUIStrings.Action_Title_RevokeByDeputy,
                        UniqueId = "RevokeByDeputy",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/RevokeByDeputy.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = PartsSalesUIStrings.Action_Title_RebateApprove,
                        UniqueId = "RebateApprove",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/RebateApprove.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = PartsSalesUIStrings.Action_Title_CheckStock,
                        UniqueId = "CheckStock",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/CheckStock.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = PartsSalesUIStrings.Action_Title_EditDetail,
                        UniqueId = "EditDetail",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Edit.png"),
                        CanExecute = false
                    },
                }
            };
        }
    }
}
