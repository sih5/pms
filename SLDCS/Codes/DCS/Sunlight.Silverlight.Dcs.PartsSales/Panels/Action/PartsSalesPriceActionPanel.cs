﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Action {
    public class PartsSalesPriceActionPanel : DcsActionPanelBase {
        public PartsSalesPriceActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = PartsSalesUIStrings.ActionPanel_Title_General,
                ActionItems = new[] {
                     new ActionItem {  
                        UniqueId = "Export",  
                        Title = PartsSalesUIStrings.Action_Title_Export,  
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Export.png"),  
                        CanExecute = false  
                     },
                    new ActionItem {  
                        UniqueId = "ExactExport",  
                        Title = PartsSalesUIStrings.Action_Title_ExportAccurate,  
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/MergeExport.png"),  
                        CanExecute = false  
                    }
                }
            };
        }
    }
}
