﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Action {
    public class PartsSalesReturnBillActionPanel : DcsActionPanelBase {
        public PartsSalesReturnBillActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "PartsSalesReturnBill",
                Title = PartsSalesUIStrings.ActionPanel_Title_PartsSalesReturnBill,
                ActionItems = new[] {
                    new ActionItem {
                        Title = PartsSalesUIStrings.Action_Title_AddByDeputy,
                        UniqueId = "AddByDeputy",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/AddByDeputy.png"),
                        CanExecute = false
                    },
                     new ActionItem {
                        Title = PartsSalesUIStrings.Action_Title_BatchInitialApprove,
                        UniqueId = "FirstTrial",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/CheckStock.png"),
                        CanExecute = false
                    },
                     new ActionItem {
                        Title = PartsSalesUIStrings.Action_Title_BatchFinalApprove,
                        UniqueId = "FinalTrial",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Confirm.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = PartsSalesUIStrings.DataEdit_Title_UploadFile,
                        UniqueId = CommonActionKeys.UPLOAD,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Upload.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = PartsSalesUIStrings.Action_Title_BatchApprove,
                        UniqueId ="BatchApprove",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/PickConfirm.png"),
                        CanExecute = false
                    }

                }
            };
        }
    }
}
