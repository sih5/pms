﻿
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Action {
    public class PartsSalesRtnSettlementActionPanel : DcsActionPanelBase {
        public PartsSalesRtnSettlementActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "PartsSalesRtnSettlement",
                Title = PartsSalesUIStrings.ActionPanel_Title_PartsSalesRtnSettlement,
                ActionItems = new[] {
                    new ActionItem {
                        Title = PartsSalesUIStrings.Action_Title_InvoiceRegister,
                        UniqueId = "InvoiceRegister",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/InvoiceRegister.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = PartsSalesUIStrings.Action_Title_AntiSettlement,
                        UniqueId = "AntiSettlement",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/AntiSettlement.png"),
                        CanExecute = false
                    },new ActionItem{
                        Title=PartsSalesUIStrings.Action_Title_SettlementCostSearch,
                        UniqueId="SettlementCostSearch",
                         ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Query.png"),
                        CanExecute=false
                    }
                }
            };
        }
    }
}
