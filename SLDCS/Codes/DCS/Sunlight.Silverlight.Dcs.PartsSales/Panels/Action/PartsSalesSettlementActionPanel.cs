﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Action {
    public class PartsSalesSettlementActionPanel : DcsActionPanelBase {

        public PartsSalesSettlementActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                Title = PartsSalesUIStrings.ActionPanel_Title_PartsSalesSettlement,
                UniqueId = "Common",
                ActionItems = new[] {
                    new ActionItem {
                        Title = PartsSalesUIStrings.Action_Title_PrintInvoice,
                        UniqueId = "PrintInvoice",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        Title = PartsSalesUIStrings.Action_Title_AppoveRebate,
                        UniqueId = "AppoveRebate",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/AppoveRebate.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        Title = PartsSalesUIStrings.Action_Title_PartsSalesSettlementInvoiceRegister,
                        UniqueId = "InvoiceRegister",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/InvoiceRegister.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        Title = PartsSalesUIStrings.Action_Title_PartsSalesSettlementAntiSettlement,
                        UniqueId = "AntiSettlement",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/AntiSettlement.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        UniqueId = "SettlementCost",
                        Title = PartsSalesUIStrings.Action_Title_SettlementCost,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Query.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
