﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Action {
    public class PartsSpecialTreatyPriceActionPanel : DcsActionPanelBase {
        public PartsSpecialTreatyPriceActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = PartsSalesUIStrings.ActionPanel_Title_General,
                ActionItems = new[] {
                    new ActionItem {
                        Title = PartsSalesUIStrings.Action_Title_BatchAbandon,
                        UniqueId = "BatchAbandon",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/ManualProcessing.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        Title = PartsSalesUIStrings.Action_Title_ImportAbandon,
                        UniqueId = "ImportAbandon",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}