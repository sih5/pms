﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Action {
    public class SalePartsSalesPriceChangeApplicationActionPanel : DcsActionPanelBase {
        public SalePartsSalesPriceChangeApplicationActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                Title = PartsSalesUIStrings.Action_Title_PartsSalesPriceChange,
                UniqueId = "PartsSalesPriceChange",
                ActionItems = new[] {
                    new ActionItem {
                        Title = PartsSalesUIStrings.Action_Title_BatchApprove,
                        UniqueId = "BatchInitialApprove",
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/InitialApprove.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = PartsSalesUIStrings.Action_Title_BatchFinalApproveNew,
                        UniqueId = "BatchFinalApprove",
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/FinalApprove.png"),
                        CanExecute = false
                    }
                }

            };
        }
    }
}
