﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Action {
    public class SpecialTreatyPriceChangeActionPanel : DcsActionPanelBase {
        public SpecialTreatyPriceChangeActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = PartsSalesUIStrings.ActionPanel_Title_General,
                ActionItems = new[] {
                    new ActionItem{
                        Title=PartsSalesUIStrings.Action_Title_Add,
                        UniqueId ="Add",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Add.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title=PartsSalesUIStrings.Action_Title_Edit,
                        UniqueId ="Edit",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Edit.png"),
                        CanExecute=false
                    }, new ActionItem {
                        Title = PartsSalesUIStrings.Action_Title_Submit,
                        UniqueId = CommonActionKeys.SUBMIT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Submit.png"),
                        CanExecute = false
                    },
                    new ActionItem{
                        Title= PartsSalesUIStrings.Action_Title_InitApprove,
                        UniqueId ="FirstApprove",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/InitialApprove.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title= PartsSalesUIStrings.Action_Title_BatchInitialApprove,
                        UniqueId ="InitialApprove",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/InitialApprove.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title= PartsSalesUIStrings.Action_Title_BatchFinalApprove,
                        UniqueId ="Approve",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/FinalApprove.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title= PartsSalesUIStrings.Action_Title_Abandon,
                        UniqueId ="Abandon",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Abandon.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title=PartsSalesUIStrings.Action_Title_DistributeExport,
                        UniqueId ="MergeExport",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/ExportAll.png"),
                        CanExecute=false
                    },
                    new ActionItem{
                        Title=PartsSalesUIStrings.DataEditPanel_Action_ImportNew,
                        UniqueId ="Import",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute=false
                    }
                }
            };
        }
    }
}
