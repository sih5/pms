﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Action {
    public class VehiclePartsStockLevelActionPanel : DcsActionPanelBase {
        public VehiclePartsStockLevelActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "VehiclePartsStockLevel",
                Title = PartsSalesUIStrings.Action_Title_VehiclePartsStockLevel,
                ActionItems = new[] {
                   new ActionItem {
                        Title =PartsSalesUIStrings.Action_Title_ImportEdit,
                        UniqueId = "ImportEdit",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    } , new ActionItem {
                        Title =PartsSalesUIStrings.Action_Title_Abandon,
                        UniqueId = "Invalid",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Abandon.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
