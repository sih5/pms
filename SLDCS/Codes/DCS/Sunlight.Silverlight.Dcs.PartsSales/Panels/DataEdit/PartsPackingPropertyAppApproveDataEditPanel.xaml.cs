﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit
{
    public partial class PartsPackingPropertyAppApproveDataEditPanel
    {
        private DcsDomainContext domainContext = new DcsDomainContext();
        //private ObservableCollection<KeyValuePair> kvIsPasseds;
        //public ObservableCollection<KeyValuePair> KvIsPasseds
        //{
        //    get
        //    {
        //        return this.kvIsPasseds ?? (this.kvIsPasseds = new ObservableCollection<KeyValuePair>());
        //    }
        //}

   

        public PartsPackingPropertyAppApproveDataEditPanel()
        {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
        private void CreateUI()
        {           
            //this.kvIsPasseds.Add(new KeyValuePair
            //{
            //    Key = 1,
            //    Value = "审核通过"
            //});
            //this.kvIsPasseds.Add(new KeyValuePair
            //{
            //    Key = 0,
            //    Value = "驳回"
            //});
           
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                    return;
                this.RadComboBoxPartsSalesCategory.ItemsSource = loadOp.Entities;
            }, null);
            //this.CbIsRadio.SelectedValue = 1;
        }
      
    }
}
