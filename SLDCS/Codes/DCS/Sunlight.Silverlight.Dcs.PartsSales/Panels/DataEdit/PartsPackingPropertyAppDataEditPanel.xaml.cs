﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit {
    public partial class PartsPackingPropertyAppDataEditPanel {
        private QueryWindowBase sparePartQueryWindow;
        public DcsDomainContext QueryDomainContext {
            get;
            set;
        }
        public PartsPackingPropertyAppDataEditPanel() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += this.PartsPackingPropertyAppDataEditPanel_Loaded;
        }
        private QueryWindowBase SparePartQueryWindow {
            get {
                if(this.sparePartQueryWindow == null) {
                    this.sparePartQueryWindow = DI.GetQueryWindow("SparePart");
                    this.sparePartQueryWindow.SelectionDecided += this.sparePartCodes_SelectionDecided;
                }
                return this.sparePartQueryWindow;
            }
        }

        private void CreateUI() {
            this.SparePartCodes.PopupContent = this.SparePartQueryWindow;
        }
        void PartsPackingPropertyAppDataEditPanel_Loaded(object sender, DependencyPropertyChangedEventArgs e) {
            this.QueryDomainContext.Load(this.QueryDomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if (loadOp.HasError)
                    return;
                this.RadComboBoxPartsSalesCategory.ItemsSource = loadOp.Entities;
                var partsPacking = this.DataContext as PartsPackingPropertyApp;
                if (null != partsPacking && partsPacking.PartsSalesCategoryId == null) {
                    partsPacking.PartsSalesCategoryId = loadOp.Entities.FirstOrDefault().Id;
                }
                if (null != partsPacking && partsPacking.SparePartId != default(int)) {
                    this.QueryDomainContext.Load(this.QueryDomainContext.GetPartsRetailGuidePricesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.SparePartId == partsPacking.SparePartId && r.PartsSalesCategoryId == partsPacking.PartsSalesCategoryId), LoadBehavior.RefreshCurrent, details => {
                        if (details.HasError) {
                            if (!details.IsErrorHandled)
                                details.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(details);
                            return;
                        }
                        var price = details.Entities.FirstOrDefault();
                        if (price != null) {
                            this.RetailGuidePriceBox.Text = price.RetailGuidePrice.ToString();
                        }
                    }, null);
                }
            }, null);
        }

        private void sparePartCodes_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var sparePart = queryWindow.SelectedEntities.Cast<SparePart>().FirstOrDefault();
            if(sparePart == null)
                return;
            var partsPacking = this.DataContext as PartsPackingPropertyApp;
            partsPacking.MInSalesAmount = sparePart.MInPackingAmount;
            partsPacking.OldMInPackingAmount = sparePart.MInPackingAmount;
            //根据配件查询已有的配件包装属性
            //查询清单

            this.QueryDomainContext.Load(this.QueryDomainContext.GetPartsBranchesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.PartId == sparePart.Id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Entities.ToArray().Count() == 1) {
                    var entity = loadOp.Entities.First();
                    partsPacking.MainPackingType = entity.MainPackingType;
                    this.QueryDomainContext.Load(this.QueryDomainContext.GetPartsBranchPackingPropsQuery().Where(r => r.SparePartId == sparePart.Id && r.PartsBranchId == entity.Id), LoadBehavior.RefreshCurrent, details => {
                        if(details.HasError) {
                            if(!details.IsErrorHandled)
                                details.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(details);
                            return;
                        }
                        var detailsList = details.Entities.ToArray();
                        if(detailsList.Any()) {
                            for(int i = 0; i < detailsList.Count(); i++) {
                                if((int)DcsPackingUnitType.一级包装 == detailsList[i].PackingType) {
                                    partsPacking.FirId = detailsList[i].Id;
                                    partsPacking.FirPackingType = detailsList[i].PackingType;
                                    partsPacking.FirMeasureUnit = detailsList[i].MeasureUnit;
                                    partsPacking.FirPackingCoefficient = detailsList[i].PackingCoefficient;
                                //    partsPacking.FirPackingMaterial = detailsList[i].PackingMaterial;
                                    partsPacking.FirVolume = detailsList[i].Volume;
                                    partsPacking.FirWeight = detailsList[i].Weight;
                                    partsPacking.FirLength = detailsList[i].Length;
                                    partsPacking.FirWidth = detailsList[i].Width;
                                    partsPacking.FirHeight = detailsList[i].Height;
                                //    partsPacking.FirPackingMaterialName = detailsList[i].PackingMaterialName;
                                    partsPacking.FirIsBoxStandardPrint = detailsList[i].IsBoxStandardPrint == null ? 0 : detailsList[i].IsBoxStandardPrint;
                                    partsPacking.FirIsPrintPartStandard = detailsList[i].IsPrintPartStandard == null ? 0 : detailsList[i].IsPrintPartStandard;
                                } else if((int)DcsPackingUnitType.二级包装 == detailsList[i].PackingType) {
                                    partsPacking.SecId = detailsList[i].Id;
                                    partsPacking.SecPackingType = detailsList[i].PackingType;
                                    partsPacking.SecMeasureUnit = detailsList[i].MeasureUnit;
                                    partsPacking.SecPackingCoefficient = detailsList[i].PackingCoefficient;
                                //    partsPacking.SecPackingMaterial = detailsList[i].PackingMaterial;
                                    partsPacking.SecVolume = detailsList[i].Volume;
                                    partsPacking.SecWeight = detailsList[i].Weight;
                                    partsPacking.SecLength = detailsList[i].Length;
                                    partsPacking.SecWidth = detailsList[i].Width;
                                    partsPacking.SecHeight = detailsList[i].Height;
                                  //  partsPacking.SecPackingMaterialName = detailsList[i].PackingMaterialName;
                                    partsPacking.SecIsBoxStandardPrint = detailsList[i].IsBoxStandardPrint;
                                    partsPacking.SecIsPrintPartStandard = detailsList[i].IsPrintPartStandard;
                                    partsPacking.SecIsBoxStandardPrint = detailsList[i].IsBoxStandardPrint == null ? 0 : detailsList[i].IsBoxStandardPrint;
                                    partsPacking.SecIsPrintPartStandard = detailsList[i].IsPrintPartStandard == null ? 0 : detailsList[i].IsPrintPartStandard;
                                } else if((int)DcsPackingUnitType.三级包装 == detailsList[i].PackingType) {
                                    partsPacking.ThidId = detailsList[i].Id;
                                    partsPacking.ThidPackingType = detailsList[i].PackingType;
                                    partsPacking.ThidMeasureUnit = detailsList[i].MeasureUnit;
                                    partsPacking.ThidPackingCoefficient = detailsList[i].PackingCoefficient;
                                  //  partsPacking.ThidPackingMaterial = detailsList[i].PackingMaterial;
                                    partsPacking.ThidVolume = detailsList[i].Volume;
                                    partsPacking.ThidWeight = detailsList[i].Weight;
                                    partsPacking.ThidLength = detailsList[i].Length;
                                    partsPacking.ThidWidth = detailsList[i].Width;
                                    partsPacking.ThidHeight = detailsList[i].Height;
                                //    partsPacking.ThidPackingMaterialName = detailsList[i].PackingMaterialName;
                                    partsPacking.ThidIsBoxStandardPrint = detailsList[i].IsBoxStandardPrint;
                                    partsPacking.ThidIsPrintPartStandard = detailsList[i].IsPrintPartStandard;
                                    partsPacking.ThidIsBoxStandardPrint = detailsList[i].IsBoxStandardPrint == null ? 0 : detailsList[i].IsBoxStandardPrint;
                                    partsPacking.ThidIsPrintPartStandard = detailsList[i].IsPrintPartStandard == null ? 0 : detailsList[i].IsPrintPartStandard;
                                }
                            }
                        }
                    }, null);

                    this.QueryDomainContext.Load(this.QueryDomainContext.GetPartsRetailGuidePricesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.SparePartId == sparePart.Id && r.PartsSalesCategoryId == entity.PartsSalesCategoryId), LoadBehavior.RefreshCurrent, details => {
                        if(details.HasError) {
                            if(!details.IsErrorHandled)
                                details.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(details);
                            return;
                        }
                        var price = details.Entities.FirstOrDefault();
                        if (price != null) {
                            this.RetailGuidePriceBox.Text = price.RetailGuidePrice.ToString();
                        }
                    }, null);
                }
            }, null);
            partsPacking.SparePartId = sparePart.Id;
            this.SparePartCodes.Text = sparePart.Code;
            this.SihCode.Text = sparePart.ReferenceCode;
            this.SpareName.Text = sparePart.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null) {
                parent.Close();
                this.sparePartQueryWindow = null;
            }
        }
    }
}
