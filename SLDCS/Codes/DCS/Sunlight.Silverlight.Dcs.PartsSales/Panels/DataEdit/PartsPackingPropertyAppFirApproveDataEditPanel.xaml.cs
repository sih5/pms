﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit
{
    public partial class PartsPackingPropertyAppFirApproveDataEditPanel
    {
        private readonly ObservableCollection<KeyValuePair> kvMeasureUnit = new ObservableCollection<KeyValuePair>();


        public PartsPackingPropertyAppFirApproveDataEditPanel()
        {
            InitializeComponent();
         //   this.KeyValueManager.LoadData();
            this.DataContextChanged += PartsPackingPropertyAppFirPackingTypeDataEditPanel_Loaded;

        }

      
        public object KvMeasureUnit
        {
            get
            {
                return this.kvMeasureUnit;
            }
        }
        void PartsPackingPropertyAppFirPackingTypeDataEditPanel_Loaded(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
        {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetKeyValueItemsQuery().Where(r => r.Name == "PackingUnit" && (r.Value == PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_Piece || r.Value == PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_Set || r.Value == PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_Litre)), LoadBehavior.RefreshCurrent, loadOpKey =>
            {
                if (loadOpKey.HasError)
                {
                    if (!loadOpKey.IsErrorHandled)
                        loadOpKey.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOpKey);
                }
                var partsPacking = this.DataContext as PartsPackingPropertyApp;
                string rirMeasureUnit = "";
                if (partsPacking != null)
                {
                    rirMeasureUnit = partsPacking.FirMeasureUnit;
                }
                this.kvMeasureUnit.Clear();
                foreach (var branch in loadOpKey.Entities)
                {
                    this.kvMeasureUnit.Add(new KeyValuePair
                    {
                        Key = branch.Key,
                        Value = branch.Value
                    });
                }
                this.tets2.Text = rirMeasureUnit;

            }, null);
        }
    }
}
