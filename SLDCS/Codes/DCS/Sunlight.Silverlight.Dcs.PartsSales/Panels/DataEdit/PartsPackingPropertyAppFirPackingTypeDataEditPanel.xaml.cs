﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit {
    public partial class PartsPackingPropertyAppFirPackingTypeDataEditPanel {
        private QueryWindowBase sparePartForPackQueryWindow;
        private readonly ObservableCollection<KeyValuePair> kvMeasureUnit = new ObservableCollection<KeyValuePair>();
        public DcsDomainContext QueryDomainContext {
            get;
            set;
        }

        private QueryWindowBase SparePartForPackQueryWindow {
            get {
                if(this.sparePartForPackQueryWindow == null) {
                    this.sparePartForPackQueryWindow = DI.GetQueryWindow("SparePartForPack");
                    this.sparePartForPackQueryWindow.SelectionDecided += this.FirPackingMaterial_SelectionDecided;
                }
                return this.sparePartForPackQueryWindow;
            }
        }

        public PartsPackingPropertyAppFirPackingTypeDataEditPanel() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += PartsPackingPropertyAppFirPackingTypeDataEditPanel_Loaded;
        }

        private void CreateUI() {
            this.FirPackingMaterial.PopupContent = this.SparePartForPackQueryWindow;
        }
        private void FirPackingMaterial_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var sparePart = queryWindow.SelectedEntities.Cast<SparePart>().FirstOrDefault();
            if(sparePart == null)
                return;

            this.FirPackingMaterial.Text = sparePart.Code;
            this.FirPackingMaterialName.Text = sparePart.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null) {
                parent.Close();
                this.sparePartForPackQueryWindow = null;
            }
        }

        public object KvMeasureUnit {
            get {
                return this.kvMeasureUnit;
            }
        }

        void PartsPackingPropertyAppFirPackingTypeDataEditPanel_Loaded(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            this.QueryDomainContext.Load(this.QueryDomainContext.GetKeyValueItemsQuery().Where(r => r.Name == "PackingUnit" && (r.Value == PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_Piece || r.Value == PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_Set || r.Value == PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_Litre)), LoadBehavior.RefreshCurrent, loadOpKey => {
                if(loadOpKey.HasError) {
                    if(!loadOpKey.IsErrorHandled)
                        loadOpKey.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOpKey);
                }
                var partsPacking = this.DataContext as PartsPackingPropertyApp;
                string rirMeasureUnit = "";
                if(partsPacking != null) {
                    rirMeasureUnit = partsPacking.FirMeasureUnit;
                }
                this.kvMeasureUnit.Clear();
                foreach(var branch in loadOpKey.Entities) {
                    this.kvMeasureUnit.Add(new KeyValuePair {
                        Key = branch.Key,
                        Value = branch.Value
                    });
                }
                this.tets2.Text = rirMeasureUnit;
            }, null);
        }


        private void FirIsBoxStandardPrint_Click(object sender, System.Windows.RoutedEventArgs e) {
            var partsPacking = this.DataContext as PartsPackingPropertyApp;
            if(null == partsPacking)
                return;
            if(this.FirIsBoxStandardPrint.IsChecked == true) {
                partsPacking.FirIsPrintPartStandard = 0;
            }
        }

        private void FirIsPrintPartStandard_Click(object sender, System.Windows.RoutedEventArgs e) {
            var partsPacking = this.DataContext as PartsPackingPropertyApp;
            if(null == partsPacking)
                return;
            if(this.FirIsPrintPartStandard.IsChecked == true) {
                partsPacking.FirIsBoxStandardPrint = 0;
            }
        }


    }
}
