﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit
{
    public partial class PartsPackingPropertyAppMainApproveDataEditPanel
    {

        private readonly string[] kvNames = {
            "PackingUnitType"
        };

        public PartsPackingPropertyAppMainApproveDataEditPanel()
        {
            InitializeComponent();
            this.KeyValueManager.LoadData();
            this.KeyValueManager.Register(this.kvNames);

        }

        public object KvMainPackingTypes
        {
            get
            {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
    }
}
