﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit
{
    public partial class PartsPackingPropertyAppMainPackingTypeDataEditPanel 
    {
     
        private readonly string[] kvNames = {
            "PackingUnitType"
        };
    
        public PartsPackingPropertyAppMainPackingTypeDataEditPanel()
        {
            InitializeComponent();
            this.KeyValueManager.LoadData();
            this.KeyValueManager.Register(this.kvNames);

        }    
      
        public object KvMainPackingTypes
         {
             get
             {
                 return this.KeyValueManager[this.kvNames[0]];
             }
         }

        private void MInSalesAmount_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            //var partsPacking = this.DataContext as PartsPackingPropertyApp;
            //if ((0 != partsPacking.FirPackingCoefficient && null != partsPacking.FirPackingCoefficient) && (0 != partsPacking.MInSalesAmount && null != partsPacking.MInSalesAmount))
            //{
            //    int min = Convert.ToInt32(this.MInSalesAmount.Text);
            //    int remainder = min % Convert.ToInt32(partsPacking.FirPackingCoefficient);
            //    if (0 != remainder)
            //    {
            //        UIHelper.ShowNotification("最小销售数量应为一级包装数量的整数倍");
            //        return;
            //    }
            //}
        }
    }
}
