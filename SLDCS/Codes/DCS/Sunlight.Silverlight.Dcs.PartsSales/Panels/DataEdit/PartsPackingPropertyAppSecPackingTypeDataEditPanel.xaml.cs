﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit {
    public partial class PartsPackingPropertyAppSecPackingTypeDataEditPanel {
        private QueryWindowBase sparePartForPackQueryWindow;
        private readonly ObservableCollection<KeyValuePair> kvMeasureUnit = new ObservableCollection<KeyValuePair>();
        public DcsDomainContext QueryDomainContext {
            get;
            set;
        }
        private QueryWindowBase SparePartForPackQueryWindow {
            get {
                if(this.sparePartForPackQueryWindow == null) {
                    this.sparePartForPackQueryWindow = DI.GetQueryWindow("SparePartForPack");
                    this.sparePartForPackQueryWindow.SelectionDecided += this.SecPackingMaterial_SelectionDecided;
                }
                return this.sparePartForPackQueryWindow;
            }
        }
        public PartsPackingPropertyAppSecPackingTypeDataEditPanel() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += PartsPackingPropertyAppFirPackingTypeDataEditPanel_Loaded;
        }

        private void CreateUI() {
            this.SecPackingMaterial.PopupContent = this.SparePartForPackQueryWindow;
        }
        private void SecPackingMaterial_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var sparePart = queryWindow.SelectedEntities.Cast<SparePart>().FirstOrDefault();
            if(sparePart == null)
                return;

            this.SecPackingMaterial.Text = sparePart.Code;
            this.SecPackingMaterialName.Text = sparePart.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null) {
                parent.Close();
                this.sparePartForPackQueryWindow = null;
            }
        }

        public object KvMeasureUnit {
            get {
                return this.kvMeasureUnit;
            }
        }

        private void SecPackingCoefficient_TextChanged(object sender, MouseEventArgs e) {
            var partsPacking = this.DataContext as PartsPackingPropertyApp;
            if(this.SecPackingCoefficient.Text.Equals("")) {
                partsPacking.SecPackingCoefficient = 0;
                return;
            }
            if((0 != partsPacking.FirPackingCoefficient && null != partsPacking.FirPackingCoefficient) && (0 != partsPacking.SecPackingCoefficient && null != partsPacking.SecPackingCoefficient)) {
                int thid = Convert.ToInt32(this.SecPackingCoefficient.Text);
                int remainder = thid % Convert.ToInt32(partsPacking.FirPackingCoefficient);
                if(0 != remainder) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditPanel_Validation_PartsPackingTask_Coefficient);
                    return;
                }
            }
        }
        void PartsPackingPropertyAppFirPackingTypeDataEditPanel_Loaded(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            this.QueryDomainContext.Load(this.QueryDomainContext.GetKeyValueItemsQuery().Where(r => r.Name == "PackingUnit" && (r.Value == PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_Bag || r.Value == PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_Box || r.Value == PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_Case)), LoadBehavior.RefreshCurrent, loadOpKey => {
                if(loadOpKey.HasError) {
                    if(!loadOpKey.IsErrorHandled)
                        loadOpKey.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOpKey);
                }
                var partsPacking = this.DataContext as PartsPackingPropertyApp;
                string rirMeasureUnit = "";
                if(partsPacking != null) {
                    rirMeasureUnit = partsPacking.SecMeasureUnit;
                }
                this.kvMeasureUnit.Clear();
                foreach(var branch in loadOpKey.Entities)
                    this.kvMeasureUnit.Add(new KeyValuePair {
                        Key = branch.Key,
                        Value = branch.Value
                    });
                this.SecMeasureUnit.Text = rirMeasureUnit;
            }, null);
        }

        private void SecIsBoxStandardPrint_Click(object sender, System.Windows.RoutedEventArgs e) {
            var partsPacking = this.DataContext as PartsPackingPropertyApp;
            if(null == partsPacking)
                return;
            if(this.SecIsBoxStandardPrint.IsChecked == true) {
                partsPacking.SecIsPrintPartStandard = 0;
            }
        }

        private void SecIsPrintPartStandard_Click(object sender, System.Windows.RoutedEventArgs e) {
            var partsPacking = this.DataContext as PartsPackingPropertyApp;
            if(null == partsPacking)
                return;
            if(this.SecIsPrintPartStandard.IsChecked == true) {
                partsPacking.SecIsBoxStandardPrint = 0;
            }
        }

        private void SecPackingCoefficient_TextChanged_1(object sender, System.Windows.Controls.TextChangedEventArgs e) {
            var partsPacking = this.DataContext as PartsPackingPropertyApp;
            if(this.SecPackingCoefficient.Text.Equals(""))
                partsPacking.SecPackingCoefficient = 0;
        }
    }
}