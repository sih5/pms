﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit {
    public partial class PartsPackingPropertyAppThidPackingTypeDataEditPanel {
        private readonly ObservableCollection<KeyValuePair> kvMeasureUnit = new ObservableCollection<KeyValuePair>();
        public DcsDomainContext QueryDomainContext {
            get;
            set;
        }
        private QueryWindowBase sparePartForPackQueryWindow;

        private QueryWindowBase SparePartForPackQueryWindow {
            get {
                if(this.sparePartForPackQueryWindow == null) {
                    this.sparePartForPackQueryWindow = DI.GetQueryWindow("SparePartForPack");
                    this.sparePartForPackQueryWindow.SelectionDecided += this.ThidPackingMaterial_SelectionDecided;
                }
                return this.sparePartForPackQueryWindow;
            }
        }

        public PartsPackingPropertyAppThidPackingTypeDataEditPanel() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += this.PartsPackingPropertyAppFirPackingTypeDataEditPanel_Loaded;
        }

        private void CreateUI() {
            this.ThidPackingMaterial.PopupContent = this.SparePartForPackQueryWindow;
        }
        private void ThidPackingMaterial_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var sparePart = queryWindow.SelectedEntities.Cast<SparePart>().FirstOrDefault();
            if(sparePart == null)
                return;
            this.ThidPackingMaterial.Text = sparePart.Code;
            this.ThidPackingMaterialName.Text = sparePart.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null) {
                parent.Close();
                this.sparePartForPackQueryWindow = null;
            }
        }
        public object KvMeasureUnit {
            get {
                return this.kvMeasureUnit;
            }
        }

        private void ThidPackingCoefficient_TextChanged(object sender, MouseEventArgs e) {
            var partsPacking = this.DataContext as PartsPackingPropertyApp;
            if(this.ThidPackingCoefficient.Text.Equals("")) {
                partsPacking.ThidPackingCoefficient = 0;
                return;
            }
            if((0 != partsPacking.SecPackingCoefficient && null != partsPacking.SecPackingCoefficient) && (0 != partsPacking.ThidPackingCoefficient && null != partsPacking.ThidPackingCoefficient)) {
                int thid = Convert.ToInt32(this.ThidPackingCoefficient.Text);
                int remainder = thid % Convert.ToInt32(partsPacking.SecPackingCoefficient);
                if(0 != remainder || thid == partsPacking.SecPackingCoefficient) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditPanel_Validation_PartsPackingTask_SecPackingCoefficient);
                    return;
                }
            }
            if((0 != partsPacking.FirPackingCoefficient && null != partsPacking.FirPackingCoefficient) && (0 != partsPacking.ThidPackingCoefficient && null != partsPacking.ThidPackingCoefficient)) {
                int thid = Convert.ToInt32(this.ThidPackingCoefficient.Text);
                int remainder = thid % Convert.ToInt32(partsPacking.FirPackingCoefficient);
                if(0 != remainder || thid == partsPacking.FirPackingCoefficient) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditPanel_Validation_PartsPackingTask_FirPackingCoefficient);
                    return;
                }
            }
        }
        void PartsPackingPropertyAppFirPackingTypeDataEditPanel_Loaded(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            this.QueryDomainContext.Load(this.QueryDomainContext.GetKeyValueItemsQuery().Where(r => r.Name == "PackingUnit" && r.Value == PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_Case), LoadBehavior.RefreshCurrent, loadOpKey => {
                if(loadOpKey.HasError) {
                    if(!loadOpKey.IsErrorHandled)
                        loadOpKey.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOpKey);
                }
                var partsPacking = this.DataContext as PartsPackingPropertyApp;
                string rirMeasureUnit = "";
                if(partsPacking != null) {
                    rirMeasureUnit = partsPacking.ThidMeasureUnit;
                }
                this.kvMeasureUnit.Clear();
                foreach(var branch in loadOpKey.Entities)
                    this.kvMeasureUnit.Add(new KeyValuePair {
                        Key = branch.Key,
                        Value = branch.Value
                    });
                this.ThidMeasureUnit.Text = rirMeasureUnit;
            }, null);
        }

        private void ThidIsBoxStandardPrint_Click(object sender, System.Windows.RoutedEventArgs e) {
            var partsPacking = this.DataContext as PartsPackingPropertyApp;
            if(null == partsPacking)
                return;
            if(this.ThidIsBoxStandardPrint.IsChecked == true) {
                partsPacking.ThidIsPrintPartStandard = 0;
            }
        }

        private void ThidIsPrintPartStandard_Click(object sender, System.Windows.RoutedEventArgs e) {
            var partsPacking = this.DataContext as PartsPackingPropertyApp;
            if(null == partsPacking)
                return;
            if(this.ThidIsPrintPartStandard.IsChecked == true) {
                partsPacking.ThidIsBoxStandardPrint = 0;
            }
        }

        private void ThidPackingCoefficient_TextChanged_1(object sender, System.Windows.Controls.TextChangedEventArgs e) {
            var partsPacking = this.DataContext as PartsPackingPropertyApp;
            if(null == partsPacking)
                return;
            if(this.ThidPackingCoefficient.Text.Equals(""))
                partsPacking.ThidPackingCoefficient = 0;
        }
    }
}
