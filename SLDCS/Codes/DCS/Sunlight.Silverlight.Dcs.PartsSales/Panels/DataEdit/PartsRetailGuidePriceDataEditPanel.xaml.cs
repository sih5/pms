﻿using System;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit {
    public partial class PartsRetailGuidePriceDataEditPanel {

        public PartsRetailGuidePriceDataEditPanel() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            var queryWindowSparepart = DI.GetQueryWindow("SparePart");
            queryWindowSparepart.SelectionDecided += this.QueryWindowSparepart_SelectionDecided;
            queryWindowSparepart.Loaded += this.QueryWindowSparepart_Loaded;
            this.ptbSparePartCode.PopupContent = queryWindowSparepart;
        }

        private void QueryWindowSparepart_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow != null)
                queryWindow.ExchangeData(null, "RefreshQueryResult", null);
        }

        private void QueryWindowSparepart_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var sparePart = queryWindow.SelectedEntities.Cast<SparePart>().FirstOrDefault();
            if(sparePart == null)
                return;

            var partsRetailGuidePrice = this.DataContext as PartsRetailGuidePrice;
            if(partsRetailGuidePrice == null)
                return;

            partsRetailGuidePrice.SparePartId = sparePart.Id;
            partsRetailGuidePrice.SparePartCode = sparePart.Code;
            partsRetailGuidePrice.SparePartName = sparePart.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }
    }
}
