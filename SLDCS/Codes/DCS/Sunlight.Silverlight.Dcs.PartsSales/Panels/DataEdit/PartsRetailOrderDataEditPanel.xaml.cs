﻿
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using System.Windows;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit {
    public partial class PartsRetailOrderDataEditPanel {

        private readonly string[] kvNames = {
            "PayOutBill_PaymentMethod"
        };

        private readonly ObservableCollection<Warehouse> kvWarehouses = new ObservableCollection<Warehouse>();

        public PartsRetailOrderDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += this.PartsRetailOrderDataEditPanel_DataContextChanged;
        }

        void PartsRetailOrderDataEditPanel_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var partsRetailOrder = this.DataContext as PartsRetailOrder;
            if(partsRetailOrder == null)
                return;
            partsRetailOrder.PropertyChanged -= this.PartsRetailOrder_PropertyChanged;
            partsRetailOrder.PropertyChanged += this.PartsRetailOrder_PropertyChanged;
        }

        void PartsRetailOrder_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            var partsRetailOrder = sender as PartsRetailOrder;
            if(partsRetailOrder == null)
                return;
            switch(e.PropertyName) {
                case "WarehouseId":
                    var warehouse = this.KvWarehouses.SingleOrDefault(entity => entity.Id == partsRetailOrder.WarehouseId);
                    if(warehouse == null)
                        return;
                    partsRetailOrder.WarehouseCode = warehouse.Code;
                    partsRetailOrder.WarehouseName = warehouse.Name;
                    partsRetailOrder.BranchId = warehouse.Branch.Id;
                    partsRetailOrder.BranchCode = warehouse.Branch.Code;
                    partsRetailOrder.BranchName = warehouse.Branch.Name;
                    this.ClearPartsRetailOrderDetails();
                    break;
            }
        }

        private void ClearPartsRetailOrderDetails() {
            var partsRetailOrder = this.DataContext as PartsRetailOrder;
            if(partsRetailOrder == null)
                return;
            if(partsRetailOrder.PartsRetailOrderDetails == null || !partsRetailOrder.PartsRetailOrderDetails.Any())
                return;
            foreach(var partsRetailOrderDetail in partsRetailOrder.PartsRetailOrderDetails) {
                partsRetailOrderDetail.ValidationErrors.Clear();
                partsRetailOrder.PartsRetailOrderDetails.Remove(partsRetailOrderDetail);
            }
        }

        private void CreateUI() {
            var queryWindow = DI.GetQueryWindow("RetailOrderCustomer");
            queryWindow.Loaded += queryWindow_Loaded;
            queryWindow.SelectionDecided += queryWindow_SelectionDecided;
            this.popupTextBoxReceivingCustomerName.PopupContent = queryWindow;
        }

        private void queryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            var rartsRetailOrder = this.DataContext as PartsRetailOrder;
            if (queryWindow == null || rartsRetailOrder == null)
                return;
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", new FilterItem("企业Id", typeof(int), FilterOperator.IsEqualTo, BaseApp.Current.CurrentUserData.EnterpriseId));
        }

        void queryWindow_SelectionDecided(object sender, System.EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;
            if(queryWindow.SelectedEntities == null || !queryWindow.SelectedEntities.Any() || queryWindow.SelectedEntities.Cast<PartsClaimPrice>() == null)
                return;
            var partsRetailOrder = this.DataContext as PartsRetailOrder;
            if(partsRetailOrder == null)
                return;

            var retailOrderCustomer = queryWindow.SelectedEntities.Cast<RetailOrderCustomer>().FirstOrDefault();
            if(retailOrderCustomer == null)
                return;
            partsRetailOrder.CustomerName = retailOrderCustomer.CustomerName;
            partsRetailOrder.CustomerPhone = retailOrderCustomer.CustomerPhone;
            partsRetailOrder.CustomerCellPhone = retailOrderCustomer.CustomerCellPhone;
            partsRetailOrder.CustomerAddress = retailOrderCustomer.Address;
            partsRetailOrder.IsQueryWindow = false;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetWarehousesWithBranchQuery().Where(entity => entity.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Entities == null)
                    return;
                foreach(var warehouse in loadOp.Entities) {
                    this.kvWarehouses.Add(warehouse);
                }
            }, null);
        }

        public ObservableCollection<Warehouse> KvWarehouses {
            get {
                return this.kvWarehouses;
            }
        }

        public object KvPaymentMethods {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
    }
}