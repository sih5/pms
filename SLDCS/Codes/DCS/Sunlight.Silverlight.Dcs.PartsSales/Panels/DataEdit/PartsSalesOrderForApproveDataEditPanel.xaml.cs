﻿using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit {

    public partial class PartsSalesOrderForApproveDataEditPanel {
        private readonly string[] kvNames = new[] {
          "PartsSalesOrder_SecondLevelOrderType"
        };
        private SalesUnit salesUnit = null;
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesOrderTypes = new ObservableCollection<KeyValuePair>();
        public PartsSalesOrderForApproveDataEditPanel() {
            InitializeComponent();
            KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
            this.DataContextChanged -= PartsSalesOrderForApproveDataEditPanel_DataContextChanged;
            this.DataContextChanged += PartsSalesOrderForApproveDataEditPanel_DataContextChanged;

        }
        private void PartsSalesOrderForApproveDataEditPanel_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var domainContext = new DcsDomainContext();
            salesUnit = null;
            this.kvPartsSalesOrderTypes.Clear();
            if(this.DataContext as PartsSalesOrderProcess == null)
                return;
            var entity = (this.DataContext as PartsSalesOrderProcess).PartsSalesOrder;
            domainContext.Load(domainContext.GetSalesUnitsQuery().Where(r => r.Id == entity.SalesUnitId), LoadBehavior.RefreshCurrent, LoadOperation => {
                if(LoadOperation.HasError)
                    return;
                salesUnit = LoadOperation.Entities.SingleOrDefault();
                if(salesUnit == null) return;
                domainContext.Load(domainContext.GetPartsSalesOrderTypesQuery().Where(r => r.PartsSalesCategoryId == salesUnit.PartsSalesCategoryId), LoadBehavior.RefreshCurrent, LoadOperation1 => {
                    if(LoadOperation1.HasError)
                        return;
                    foreach(var partsSalesOrderType in LoadOperation1.Entities) {
                        this.kvPartsSalesOrderTypes.Add(new KeyValuePair {
                            Key = partsSalesOrderType.Id,
                            Value = partsSalesOrderType.Name,
                            UserObject = partsSalesOrderType,
                        });
                    }
                }, null);
            }, null);

        }
        private void Initialize() {


        }
        public ObservableCollection<KeyValuePair> KvPartsSalesOrderTypes {
            get {
                return this.kvPartsSalesOrderTypes;
            }
        }
        public object KvSecondLevelOrderTypes {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

    }
}
