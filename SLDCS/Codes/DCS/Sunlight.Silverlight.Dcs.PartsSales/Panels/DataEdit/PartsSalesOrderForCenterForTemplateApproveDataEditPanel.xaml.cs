﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit {
    public partial class PartsSalesOrderForCenterForTemplateApproveDataEditPanel : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;
        private DcsDomainContext domainContext = new DcsDomainContext();
        private ObservableCollection<Warehouse> kvWarehouses;
        public ObservableCollection<Warehouse> KvWarehouses {
            get {
                return this.kvWarehouses ?? (this.kvWarehouses = new ObservableCollection<Warehouse>());
            }
            set {
                this.kvWarehouses = value;
                this.OnPropertyChanged("KvWarehouses");
            }
        }
        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public PartsSalesOrderForCenterForTemplateApproveDataEditPanel() {
            InitializeComponent();
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var customerInformationBySparepart = DI.GetQueryWindow("AgencyAffiBranch");
            customerInformationBySparepart.SelectionDecided += this.QueryWindowForCustomerInformationBySparepart_SelectionDecided;
            customerInformationBySparepart.Loaded += this.customerInformationBySparepart_Loaded;
            this.ptbCenterCompany.PopupContent = customerInformationBySparepart;
        }

        private void QueryWindowForCustomerInformationBySparepart_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var agencyAffiBranch = queryWindow.SelectedEntities.Cast<AgencyAffiBranch>().SingleOrDefault();
            if(agencyAffiBranch == null)
                return;
            var agency = agencyAffiBranch.Agency;
            if(agency == null)
                return;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            if(agencyAffiBranch.Agency == null)
                return;
            partsSalesOrder.CenterCompanyId = agencyAffiBranch.Agency.Id;
            partsSalesOrder.CenterCompanyCode = agencyAffiBranch.Agency.Code;
            partsSalesOrder.CenterCompanyName = agencyAffiBranch.Agency.Name;
            partsSalesOrder.CenterWarehouseId = default(int);
            partsSalesOrder.CenterWarehouseCode = string.Empty;
            partsSalesOrder.CenterWarehouseName = string.Empty;
            //加载中心仓库数据源
            this.domainContext.Load(this.domainContext.GetWarehousesWithStorageCompanyIdsQuery(partsSalesOrder.CenterCompanyId), LoadBehavior.RefreshCurrent, loadWarehouse => {
                if(loadWarehouse.HasError) {
                    loadWarehouse.MarkErrorAsHandled();
                    return;
                }
                this.KvWarehouses.Clear();
                if(loadWarehouse.Entities != null && loadWarehouse.Entities.Any()) {
                    foreach(var warehouse in loadWarehouse.Entities) {
                        this.KvWarehouses.Add(warehouse);
                    }
                }
            }, null);
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }
        protected void customerInformationBySparepart_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            var compositeFilterItemQuery = new CompositeFilterItem();
            compositeFilterItemQuery.Filters.Add(new FilterItem {
                MemberName = "BranchId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            compositeFilterItemQuery.Filters.Add(new FilterItem {
                MemberType = typeof(int),
                MemberName = "AgencyId",
                Operator = FilterOperator.IsNotEqualTo,
                Value = partsSalesOrder.SubmitCompanyId
            });
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItemQuery);
        }

        private void ComboBoxWarehouse_OnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            if(e.AddedItems == null || e.AddedItems.Count != 1)
                return;
            var temp = sender as RadComboBox;
            if(temp == null)
                return;
            var warehouse = temp.SelectedItem as Warehouse;
            if(warehouse == null)
                return;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            partsSalesOrder.CenterWarehouseId = warehouse.Id;
            partsSalesOrder.CenterWarehouseCode = warehouse.Code;
            partsSalesOrder.CenterWarehouseName = warehouse.Name;
        }
    }
}