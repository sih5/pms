﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit {
    public partial class PartsSalesOrderForCenterPurchaseForTemplateApproveDataEditPanel : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public PartsSalesOrderForCenterPurchaseForTemplateApproveDataEditPanel() {
            InitializeComponent();
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var customerInformationBySparepart = DI.GetQueryWindow("PartsSupplierDropDownByPartId");
            customerInformationBySparepart.SelectionDecided += this.QueryWindowForCustomerInformationBySparepart_SelectionDecided;
            customerInformationBySparepart.Loaded += this.customerInformationBySparepart_Loaded;
            this.ptbCenterCompany.PopupContent = customerInformationBySparepart;
        }

        private void QueryWindowForCustomerInformationBySparepart_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var partsSupplier = queryWindow.SelectedEntities.Cast<PartsSupplier>().SingleOrDefault();
            if(partsSupplier == null)
                return;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            partsSalesOrder.CenterSupplierId = partsSupplier.Id;
            partsSalesOrder.CenterSupplierCode = partsSupplier.Code;
            partsSalesOrder.CenterSupplierName = partsSupplier.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }
        protected void customerInformationBySparepart_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;

        }

    }
}