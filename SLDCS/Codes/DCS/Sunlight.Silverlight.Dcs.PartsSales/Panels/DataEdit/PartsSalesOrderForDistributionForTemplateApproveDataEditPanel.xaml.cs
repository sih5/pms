﻿
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit {
    public partial class PartsSalesOrderForDistributionForTemplateApproveDataEditPanel {
        public PartsSalesOrderForDistributionForTemplateApproveDataEditPanel() {
            InitializeComponent();
        }

        private ICommand addCommand;
        private DcsDomainContext domainContext;
        private ObservableCollection<PartsPurchaseOrder> partsPurchaseOrdersNew;

        private DcsDomainContext DomainContext {
            get {
                return this.domainContext ?? (this.domainContext = new DcsDomainContext());
            }
        }
        public ICommand AddCommand {
            get {
                return this.addCommand ?? (this.addCommand = new Core.Command.DelegateCommand(this.AddDate));
            }
        }

        public ObservableCollection<PartsPurchaseOrder> PartsPurchaseOrdersNew {
            get {
                return this.partsPurchaseOrdersNew ?? (this.partsPurchaseOrdersNew = new ObservableCollection<PartsPurchaseOrder>());
            }
        }

        private void AddDate() {
            var tabItem = this.Parent as RadTabItem;
            if(tabItem == null)
                return;
            var tabControl = tabItem.Parent as RadTabControl;
            if(tabControl == null)
                return;
            var lPartsSalesOrder = tabControl.DataContext as PartsSalesOrder;
            if(lPartsSalesOrder == null)
                return;
            var tempPartsDetail = lPartsSalesOrder.PartsSalesOrderDetails.Where(r => r.CurrentFulfilledQuantity < r.OrderSum);
            var parameters = tempPartsDetail.ToDictionary(aa => aa.SparePartId, aa => aa.UnfulfilledQuantity);
            try {
                this.DomainContext.销售生成采购订单(parameters, lPartsSalesOrder.DistributionOWarehouseId, lPartsSalesOrder.Id, lPartsSalesOrder.Code, lPartsSalesOrder.RequestedDeliveryTime ?? DateTime.Now, invokeOp => {
                    if(invokeOp.HasError) {
                        if(!invokeOp.IsErrorHandled)
                            invokeOp.MarkErrorAsHandled();
                        var error = invokeOp.ValidationErrors.FirstOrDefault();
                        if(error != null) {
                            UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage, 5);
                        } else {
                            DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                        }
                        this.DomainContext.RejectChanges();
                        return;
                    }
                    if(invokeOp.Value >= 1)
                        UIHelper.ShowNotification(PartsSalesUIStrings.DataEditPanel_Validation_PartsSales_CreatePurChaseOrder);
                }, null);
            } catch(Exception ex) {
                UIHelper.ShowAlertMessage(ex.Message);
            }
        }

        private void ComboBoxWarehouseForCadre_OnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            var tabItem = this.Parent as RadTabItem;
            if(tabItem == null)
                return;
            var tabControl = tabItem.Parent as RadTabControl;
            if(tabControl == null)
                return;
            var lPartsSalesOrder = tabControl.DataContext as PartsSalesOrder;
            if(lPartsSalesOrder == null)
                return;
            this.DomainContext.Load(this.DomainContext.GetWarehousesOrderByNameQuery().Where(r => r.Id == lPartsSalesOrder.DistributionIWarehouseId), LoadBehavior.RefreshCurrent, loadoP => {
                if(loadoP.HasError) {
                    loadoP.MarkErrorAsHandled();
                    return;
                }
                if(loadoP.Entities.Any()) {
                    lPartsSalesOrder.DistributionIWarehouseCode = loadoP.Entities.First().Code;
                    lPartsSalesOrder.DistributionIWarehouseName = loadoP.Entities.First().Name;
                }
            }, null);
        }
    }
}
