﻿

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit {
    public partial class PartsSalesOrderForInsideSupplyingTurnForTemplateApproveDataEditPanel {

        private readonly string[] kvNames = {
            "InternalSupplier"
        };

        public PartsSalesOrderForInsideSupplyingTurnForTemplateApproveDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
             this.KeyValueManager.LoadData();
        }

        public object KvInternalSupplier {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
    }
}