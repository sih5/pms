﻿using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit {
    public partial class PartsSalesOrderForOtherBrandsForTemplateApproveDataEditPanel : INotifyPropertyChanged {
        private DcsDomainContext domainContext = new DcsDomainContext();
        public event PropertyChangedEventHandler PropertyChanged;
        private bool customIsEnabled = false;

        private void Initialize() {
            var partsSaleOrder = this.DataContext as PartsSalesOrder;
            if(partsSaleOrder == null)
                return;
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(e => e.BranchId == partsSaleOrder.BranchId && e.Id != partsSaleOrder.SalesCategoryId && e.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                if(loadOp.HasError)
                    return;
                this.ComboBoxPartsSalesCategory.ItemsSource = loadOp.Entities;
            }, null);
            var a = new SalesUnitAffiWarehouse();
            domainContext.Load(domainContext.GetWarehousesBySalesUnitIdQuery(partsSaleOrder.SalesUnitId), loadOp4 => {
                if(loadOp4.HasError)
                    return;
                this.comboBoxWarehouse.ItemsSource = loadOp4.Entities;
            }, null);
        }

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void ComboBoxPartsSalesCategory_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            partsSalesOrder.SupplierWarehouseId = default(int);
            partsSalesOrder.SupplierWarehouseName = string.Empty;
            partsSalesOrder.SupplierWarehouseCode = string.Empty;
            if(!e.AddedItems.Cast<PartsSalesCategory>().Any())
                return;
            var currentPartsSalesCategory = e.AddedItems.Cast<PartsSalesCategory>().First();
            if(currentPartsSalesCategory == null)
                return;

            partsSalesOrder.SupplierCompanyCode = currentPartsSalesCategory.Code;
            partsSalesOrder.SupplierCompanyName = currentPartsSalesCategory.Name;

            domainContext.Load(domainContext.根据销售组织查询仓库Query(partsSalesOrder.SupplierCompanyId), loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Entities == null || !loadOp.Entities.Any()) {
                    this.CustomIsEnabled = false;
                    return;
                }
                this.CustomIsEnabled = true;
                this.ComboBoxSupplierWarehouseNames.ItemsSource = loadOp.Entities;
            }, null);
        }

        private void ComboBoxSupplierWarehouseNames_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            var partsSaleOrder = this.DataContext as PartsSalesOrder;
            if(partsSaleOrder == null)
                return;
            if(e.AddedItems.Count == 0)
                return;
            var currentSupplierWarehouse = e.AddedItems.Cast<Warehouse>().First();
            if(currentSupplierWarehouse == null)
                return;

            foreach(var detail in partsSaleOrder.PartsSalesOrderDetails) {
                detail.SupplierWarehouseId = currentSupplierWarehouse.Id;
                detail.SupplierWarehouseCode = currentSupplierWarehouse.Code;
                detail.SupplierWarehouseName = currentSupplierWarehouse.Name;
            }

            partsSaleOrder.SupplierWarehouseCode = currentSupplierWarehouse.Code;
            partsSaleOrder.SupplierWarehouseName = currentSupplierWarehouse.Name;
        }

        public bool CustomIsEnabled {
            get {
                return this.customIsEnabled;
            }
            set {
                if(this.customIsEnabled == value)
                    return;
                this.customIsEnabled = value;
                this.OnPropertyChanged("CustomIsEnabled");
            }
        }

        public PartsSalesOrderForOtherBrandsForTemplateApproveDataEditPanel() {
            InitializeComponent();
            this.Initializer.Register(this.Initialize);
        }
    }
}
