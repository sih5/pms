﻿
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit {
    public partial class PartsSalesOrderForPartsCompanyTemplateApproveDataEditPanel {

        private DcsDomainContext domainContext;
        private DcsDomainContext DomainContext {
            get {
                return this.domainContext ?? (this.domainContext = new DcsDomainContext());
            }
        }
        public PartsSalesOrderForPartsCompanyTemplateApproveDataEditPanel() {
            InitializeComponent();
            this.DataContextChanged -= PartsSalesOrderTemplateApproveDetailPanel_DataContextChanged;
            this.DataContextChanged += PartsSalesOrderTemplateApproveDetailPanel_DataContextChanged;
        }

        private void PartsSalesOrderTemplateApproveDetailPanel_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            this.DomainContext.Load(this.DomainContext.GetPartsSalesOrderTypeByPartsSalesCategoryIdQuery(partsSalesOrder.SalesCategoryId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    if(!loadOp.IsErrorHandled) {
                        loadOp.MarkErrorAsHandled();
                        return;
                    }
                if(loadOp.Entities == null)
                    return;
                this.PartsSalesOrderTypeComboBox.ItemsSource = loadOp.Entities;
            }, null);
        }
    }
}
