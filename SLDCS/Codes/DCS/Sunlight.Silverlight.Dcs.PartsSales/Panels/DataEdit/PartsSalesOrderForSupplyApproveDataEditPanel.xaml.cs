﻿using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit {
    public partial class PartsSalesOrderForSupplyApproveDataEditPanel {
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesOrderTypes = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvSalesUnitAffiWarehouses = new ObservableCollection<KeyValuePair>();

        public PartsSalesOrderForSupplyApproveDataEditPanel() {
            InitializeComponent();
            this.DataContextChanged -= PartsSalesOrderForSupplyApproveDataEditPanel_DataContextChanged;
            this.DataContextChanged += PartsSalesOrderForSupplyApproveDataEditPanel_DataContextChanged;
        }

        private void PartsSalesOrderForSupplyApproveDataEditPanel_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var domainContext = new DcsDomainContext();
            this.kvPartsSalesOrderTypes.Clear();
            if(this.DataContext as PartsSalesOrder == null)
                return;
            var entity = this.DataContext as PartsSalesOrder;
            //domainContext.Load(domainContext.GetSalesUnitsQuery().Where(r => r.Id == entity.SalesUnitId), LoadBehavior.RefreshCurrent, LoadOperation => {
            //    if(LoadOperation.HasError)
            //        return;
            //    salesUnit = LoadOperation.Entities.SingleOrDefault();
            //    if(salesUnit == null)
            //        return;
            //    domainContext.Load(domainContext.GetPartsSalesOrderTypesQuery().Where(r => r.PartsSalesCategoryId == salesUnit.PartsSalesCategoryId), LoadBehavior.RefreshCurrent, LoadOperation1 => {
            //        if(LoadOperation1.HasError)
            //            return;
            //        this.PartsSalesOrderTypeComboBox.ItemsSource = LoadOperation1.Entities.ToList();
            //    }, null);
            //}, null);
            if(entity.WarehouseId.HasValue)
                domainContext.Load(domainContext.GetWarehousesOrderByNameQuery().Where(r => r.Id == entity.WarehouseId.Value), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        loadOp.MarkErrorAsHandled();
                        return;
                    }
                    var warhouseid = entity.WarehouseId;
                    if(loadOp.Entities != null) {
                        this.WarehouseComboBox.ItemsSource = loadOp.Entities.ToList();
                        entity.WarehouseId = null;
                        entity.WarehouseId = warhouseid;
                    }
                }, null);
        }

        public ObservableCollection<KeyValuePair> KvPartsSalesOrderTypes {
            get {
                return this.kvPartsSalesOrderTypes;
            }
        }

        public ObservableCollection<KeyValuePair> KvSalesUnitAffiWarehouses {
            get {
                return this.kvSalesUnitAffiWarehouses;
            }
        }
    }
}
