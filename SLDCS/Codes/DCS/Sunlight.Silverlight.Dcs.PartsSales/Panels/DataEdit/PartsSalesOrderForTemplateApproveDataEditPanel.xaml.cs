﻿using System.Linq;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit {
    public partial class PartsSalesOrderForTemplateApproveDataEditPanel {
        private readonly string[] kvNames = new[] {
            "PartsShipping_Method"
        };

        public PartsSalesOrderForTemplateApproveDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }

        public object KvShippingMethods {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
    }
}
