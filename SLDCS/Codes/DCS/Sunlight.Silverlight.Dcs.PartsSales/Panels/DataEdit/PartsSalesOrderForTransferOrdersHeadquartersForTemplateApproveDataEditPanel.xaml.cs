﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit {
    public partial class PartsSalesOrderForTransferOrdersHeadquartersForTemplateApproveDataEditPanel {
        private ICommand addCommand;
        private RadWindow radQueryWindow;
        private DataEditViewBase dataEditView;
        private DcsDomainContext domainContext;
        private DcsDomainContext DomainContext {
            get {
                return this.domainContext ?? (this.domainContext = new DcsDomainContext());
            }
        }

        public PartsSalesOrderForTransferOrdersHeadquartersForTemplateApproveDataEditPanel() {
            InitializeComponent();
        }

        public DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsSalesOrderForSecondReport");
                    this.dataEditView.EditSubmitted += dataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        void dataEditView_EditCancelled(object sender, EventArgs e) {
            RadQueryWindow.Close();
        }

        void dataEditView_EditSubmitted(object sender, EventArgs e) {
            RadQueryWindow.Close();
        }

        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = this.DataEditView,
                    Header = DcsUIStrings.BusinessName_PartsSalesOrderReport,
                    WindowState = WindowState.Normal,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                    AllowDrop = false,
                    ResizeMode = ResizeMode.NoResize,
                    CanClose = false
                });
            }
        }

        public ICommand AddCommand {
            get {
                return this.addCommand ?? (this.addCommand = new Core.Command.DelegateCommand(this.AddDate));
            }
        }

        private void AddDate() {
            var tabItem = this.Parent as RadTabItem;
            if(tabItem == null)
                return;
            var tabControl = tabItem.Parent as RadTabControl;
            if(tabControl == null)
                return;
            var lPartsSalesOrder = tabControl.DataContext as PartsSalesOrder;
            if(lPartsSalesOrder == null)
                return;
            this.DomainContext.Load(this.DomainContext.GetPartsSalesOrderByIdAndOriginalRequirementBillIdQuery(lPartsSalesOrder.Id, (int)DcsOriginalRequirementBillType.配件销售订单), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    if(!loadOp.IsErrorHandled) {
                        loadOp.MarkErrorAsHandled();
                        return;
                    }
                if(loadOp.Entities != null && loadOp.Entities.Any()) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditPanel_Validation_PartsSales_CreateError);
                    return;
                }
                var partsSalesOrder = this.DataEditView.CreateObjectToEdit<PartsSalesOrder>();
                partsSalesOrder.Status = (int)DcsBaseDataStatus.有效;
                partsSalesOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                partsSalesOrder.BranchCode = lPartsSalesOrder.BranchCode;
                partsSalesOrder.BranchId = lPartsSalesOrder.BranchId;
                partsSalesOrder.BranchName = lPartsSalesOrder.BranchName;
                partsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.提交;
                partsSalesOrder.PartsSalesOrderTypeId = lPartsSalesOrder.PartsSalesOrderTypeId;
                partsSalesOrder.PartsSalesOrderTypeName = lPartsSalesOrder.PartsSalesOrderTypeName;
                partsSalesOrder.SalesCategoryId = lPartsSalesOrder.SalesCategoryId;
                partsSalesOrder.SalesCategoryName = lPartsSalesOrder.SalesCategoryName;
                partsSalesOrder.InvoiceReceiveCompanyCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                partsSalesOrder.InvoiceReceiveCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                partsSalesOrder.InvoiceReceiveCompanyName = BaseApp.Current.CurrentUserData.EnterpriseName;
                partsSalesOrder.InvoiceReceiveSaleCateId = lPartsSalesOrder.SalesCategoryId;
                partsSalesOrder.InvoiceReceiveSaleCateName = lPartsSalesOrder.SalesCategoryName;
                partsSalesOrder.SubmitCompanyCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                partsSalesOrder.SubmitCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                partsSalesOrder.SubmitCompanyName = BaseApp.Current.CurrentUserData.EnterpriseName;
                partsSalesOrder.ReceivingCompanyCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                partsSalesOrder.ReceivingCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                partsSalesOrder.ReceivingCompanyName = BaseApp.Current.CurrentUserData.EnterpriseName;
                partsSalesOrder.IfAgencyService = false;
                partsSalesOrder.SourceBillId = lPartsSalesOrder.Id;
                partsSalesOrder.SourceBillCode = lPartsSalesOrder.Code;
                partsSalesOrder.OriginalRequirementBillId = lPartsSalesOrder.Id;
                partsSalesOrder.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.配件销售订单;
                partsSalesOrder.WarehouseId = lPartsSalesOrder.WarehouseId;
                this.DomainContext.Load(this.DomainContext.GetWarehousesOrderByNameQuery().Where(r => r.Id == lPartsSalesOrder.WarehouseId), LoadBehavior.RefreshCurrent, loadOption2 => {
                    if(loadOption2.HasError) {
                        loadOption2.MarkErrorAsHandled();
                        return;
                    }
                    if(loadOption2.Entities != null && loadOption2.Entities.Any()) {
                        partsSalesOrder.ReceivingWarehouseId = loadOption2.Entities.First().Id;
                        partsSalesOrder.ReceivingWarehouseName = loadOption2.Entities.First().Name;
                    }
                    this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(r => r.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOption3 => {
                        if(loadOption3.HasError) {
                            loadOption3.MarkErrorAsHandled();
                            return;
                        }
                        if(loadOption3.Entities != null && loadOption3.Entities.Any()) {
                            partsSalesOrder.CustomerType = partsSalesOrder.InvoiceReceiveCompanyType = loadOption3.Entities.First().Type;
                            partsSalesOrder.Province = loadOption3.Entities.First().ProvinceName;
                            partsSalesOrder.City = loadOption3.Entities.First().CityName;
                        }
                        this.DomainContext.Load(this.DomainContext.GetRegionsQuery().Where(e => e.Type == 2 && e.Name == partsSalesOrder.Province), loadOpRegion => {
                            if(loadOpRegion.HasError)
                                return;
                            var province = loadOpRegion.Entities.SingleOrDefault();
                            if(province != null)
                                partsSalesOrder.ProvinceID = province.Id;
                            foreach(var detail in lPartsSalesOrder.PartsSalesOrderDetails) {
                                var nDetail = new PartsSalesOrderDetail {
                                    SparePartCode = detail.SparePartCode,
                                    SparePartId = detail.SparePartId,
                                    SparePartName = detail.SparePartName,
                                    MeasureUnit = detail.MeasureUnit,
                                    OrderedQuantity = detail.UnfulfilledQuantity,
                                    OriginalPrice = detail.OriginalPrice,
                                    IfCanNewPart = detail.IfCanNewPart,
                                    CustOrderPriceGradeCoefficient = 1,
                                    DiscountedPrice = 0,
                                    OrderPrice = detail.OriginalPrice * 1 - 0,
                                    PriceTypeName = detail.PriceTypeName,
                                    ABCStrategy = detail.ABCStrategy
                                };
                                nDetail.OrderSum = nDetail.OrderPrice * nDetail.OrderedQuantity;
                                partsSalesOrder.PartsSalesOrderDetails.Add(nDetail);
                            }
                            partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
                            RadQueryWindow.ShowDialog();
                        }, null);
                    }, null);
                }, null);
            }, null);
        }
    }
}
