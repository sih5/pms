﻿using System;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit {
    public partial class PartsSalesOrderOverstockPartsInformationForTemplateApproveDataEditPanel : INotifyPropertyChanged {
        private CompositeFilterItem compositeFilterItem;

        private CompositeFilterItem CompositeFilterItem {
            get {
                return this.compositeFilterItem ?? (this.compositeFilterItem = new CompositeFilterItem());
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public string supplierCompanyName;
        public int supplierCompanyId;

        public String SupplierCompanyName {
            get {
                return this.supplierCompanyName;
            }
            set {
                this.supplierCompanyName = value;
                this.OnPropertyChanged("SupplierCompanyName");
            }
        }

        public int SupplierCompanyId {
            get {
                return this.supplierCompanyId;
            }
            set {
                this.supplierCompanyId = value;
                this.OnPropertyChanged("SupplierCompanyId");
            }
        }

        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public PartsSalesOrderOverstockPartsInformationForTemplateApproveDataEditPanel() {
            InitializeComponent();
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var queryWindowMatket = DI.GetQueryWindow("OverstockPartsStockForSelect");
            queryWindowMatket.Loaded += queryWindowMatket_Loaded;
            queryWindowMatket.SelectionDecided += QueryWindowMatket_SelectionDecided;
            this.ptbCustomerInformation.PopupContent = queryWindowMatket;
        }

        private void queryWindowMatket_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null)
                return;
            this.CompositeFilterItem.Filters.Clear();
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            if(partsSalesOrder.InvoiceReceiveCompanyType == (int)DcsCompanyType.服务站)
                this.compositeFilterItem.Filters.Add(new FilterItem {
                    MemberType = typeof(int),
                    MemberName = "StorageCompanyType",
                    Operator = FilterOperator.IsEqualTo,
                    Value = (int)DcsCompanyType.服务站
                });
            if(partsSalesOrder.CustomerType == (int)DcsCompanyType.代理库 || partsSalesOrder.CustomerType == (int)DcsCompanyType.服务站兼代理库) {
                var detailCompositeFilterItem = new CompositeFilterItem();
                detailCompositeFilterItem.LogicalOperator = LogicalOperator.Or;
                detailCompositeFilterItem.Filters.Add(new FilterItem {
                    MemberType = typeof(int),
                    MemberName = "StorageCompanyType",
                    Operator = FilterOperator.IsEqualTo,
                    Value = (int)DcsCompanyType.代理库
                });
                detailCompositeFilterItem.Filters.Add(new FilterItem {
                    MemberType = typeof(int),
                    MemberName = "StorageCompanyType",
                    Operator = FilterOperator.IsEqualTo,
                    Value = (int)DcsCompanyType.服务站兼代理库
                });
                this.CompositeFilterItem.Filters.Add(detailCompositeFilterItem);
            }
            this.CompositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "StorageCompanyId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsNotEqualTo,
                Value = partsSalesOrder.SubmitCompanyId
            });
            this.CompositeFilterItem.Filters.Add(new FilterItem {
                MemberType = typeof(int),
                MemberName = "UsableQuantity",
                Operator = FilterOperator.IsGreaterThanOrEqualTo,
                Value = 0
            });
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", this.CompositeFilterItem);
        }

        private void QueryWindowMatket_SelectionDecided(object sender, EventArgs e) {
            var domainContext = new DcsDomainContext();
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var overstockPartsStock = queryWindow.SelectedEntities.Cast<OverstockPartsStock>().FirstOrDefault();
            if(overstockPartsStock == null)
                return;

            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            domainContext.Load(domainContext.GetCompaniesQuery().Where(r => r.Id == overstockPartsStock.StorageCompanyId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    if(!loadOp.IsErrorHandled) {
                        loadOp.MarkErrorAsHandled();
                        return;
                    }
                if(loadOp.Entities == null)
                    return;
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                partsSalesOrder.JSupplierCompanyId = entity.Id;
                partsSalesOrder.JSupplierCompanyName = entity.Name;
                partsSalesOrder.JSupplierCompanyCode = entity.Code;

                foreach(var detail in partsSalesOrder.PartsSalesOrderDetails.Where(r => r.SparePartCode == overstockPartsStock.SparePartCode)) {
                    detail.SupplierWarehouseId = entity.Id;
                    detail.SupplierWarehouseCode = entity.Name;
                    detail.SupplierWarehouseName = entity.Code;
                    detail.StockQuantity = overstockPartsStock.UsableQuantity;
                }
            }, null);
            SupplierCompanyName = overstockPartsStock.StorageCompanyName;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }
    }
}