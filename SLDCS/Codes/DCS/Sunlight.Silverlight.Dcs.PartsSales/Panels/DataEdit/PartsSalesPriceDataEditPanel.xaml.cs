﻿
namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit {
    public partial class PartsSalesPriceDataEditPanel {
        private readonly string[] kvNames = {
            "PartsSalesPrice_PriceType"
        };

        public PartsSalesPriceDataEditPanel() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }

        public object KvPriceTypes {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
    }
}
