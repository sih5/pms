﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit {
    public partial class PartsSalesReturnBillForAddByDeputyDataEditPanel : INotifyPropertyChanged {
        private ObservableCollection<KeyValuePair> kvSalesUnits;
        private ObservableCollection<KeyValuePair> kvReturnType;
        private ObservableCollection<KeyValuePair> kvkvReturnType;
        private DcsMultiPopupsQueryWindowBase partsOutboundBillDetailQueryWindow;
        private readonly DcsDomainContext domainContext = new DcsDomainContext();
        private int returnType;
        private int? returnCompanyId;
        private Company returnCompany;
        private readonly ObservableCollection<KeyValuePair> kvReturnWarehouse = new ObservableCollection<KeyValuePair>();
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        //设计上未写，清空数据前是否给提示
        //private int salesUnitId, partsSalesOrderId;

        public object ReturnReasons {
            get {
                return this.KeyValueManager[this.kvNames[2]];
            }
        }

        public ObservableCollection<KeyValuePair> KvReturnWarehouse {
            get {
                return this.kvReturnWarehouse;
            }
        }

        private string sReturnReason;
        public string SReturnReason {
            get {
                return this.sReturnReason;
            }
            set {
                this.sReturnReason = value;
                this.OnPropertyChanged("SReturnReason");
            }
        }

        private readonly string[] kvNames = new[] {
            "PartsSalesReturn_ReturnType", "PartsSalesReturnBill_InvoiceRequirement","ReturnReason","PartsShipping_Method"
        };

        public PartsSalesReturnBillForAddByDeputyDataEditPanel() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData(() => {
                this.KvReturnType.Clear();
                foreach(var item in this.KeyValueManager[this.kvNames[0]]) {
                    this.KvReturnType.Add(new KeyValuePair {
                        Key = item.Key,
                        Value = item.Value
                    });
                }
                KvKvReturnTypes.Clear();
                foreach(var item in this.KeyValueManager[this.kvNames[0]]) {
                    this.KvKvReturnTypes.Add(new KeyValuePair {
                        Key = item.Key,
                        Value = item.Value
                    });
                }
                this.ReturnTypeComboBox.ItemsSource = KvReturnType;
            });
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += this.PartsSalesReturnBillForAddByDeputyDataEditPanel_DataContextChanged;
        }

        private void CreateUI() {
            domainContext.Load(domainContext.根据人员查询所属销售组织Query(BaseApp.Current.CurrentUserData.UserId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvSalesUnits.Clear();
                foreach(var salesUnit in loadOp.Entities)
                    this.kvSalesUnits.Add(new KeyValuePair {
                        Key = salesUnit.Id,
                        Value = salesUnit.Name,
                        UserObject = salesUnit
                    });
            }, null);

            var customerInformationBySparepart = DI.GetQueryWindow("CustomerInformationBySparepart");
            customerInformationBySparepart.SelectionDecided += this.CustomerInformationBySparepart_SelectionDecided;
            this.ptbCustomerInformationForSparepart.PopupContent = customerInformationBySparepart;
            this.ptbPartsSalesOrder.PopupContent = this.PartsOutboundBillDetailQueryWindow;
        }

        private DcsMultiPopupsQueryWindowBase PartsOutboundBillDetailQueryWindow {
            get {
                if(this.partsOutboundBillDetailQueryWindow == null) {
                    partsOutboundBillDetailQueryWindow = DI.GetQueryWindow("PartsOutboundBillDetailForMulitQueryDetail") as DcsMultiPopupsQueryWindowBase;
                    if(partsOutboundBillDetailQueryWindow != null) {
                        partsOutboundBillDetailQueryWindow.Loaded += partsOutboundBillDetailQueryWindow_Loaded;
                        partsOutboundBillDetailQueryWindow.SelectionDecided += this.QueryWindow_SelectionDecided;
                    }
                }
                return this.partsOutboundBillDetailQueryWindow;
            }
        }

        private void partsOutboundBillDetailQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;

            if (partsSalesReturnBill.SubmitCompanyId == default(int) || partsSalesReturnBill.ReturnType == default(int)) { 
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditPanel_Validation_PartsSalesReturnBill_SubmitCompanyIsNull);
                return;
            }
            if ((this.returnCompany.Type == (int)DcsCompanyType.代理库 || this.returnCompany.Type == (int)DcsCompanyType.服务站兼代理库 )&& partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.保底退货) {
                if (partsSalesReturnBill.ReturnWarehouseId == null || partsSalesReturnBill.ReturnWarehouseId == default(int)) {
                    var parent = queryWindow.ParentOfType<RadWindow>();
                    if (parent != null)
                        parent.Close();
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditPanel_Validation_PartsSalesReturnBill_ReturnWarehouseIsNull);
                    return;
                }
            }
            var query = this.PartsOutboundBillDetailQueryWindow as PartsOutboundBillDetailForMulitQueryDetailQueryWindow;
            if(query != null) {
                query.SetQueryPanelParameters(partsSalesReturnBill.PartsSalesCategoryId);
            }
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem("SubmitCompanyId", typeof(int), FilterOperator.IsEqualTo, partsSalesReturnBill.ReturnCompanyId));
            compositeFilterItem.Filters.Add(new FilterItem("SalesUnitId", typeof(int), FilterOperator.IsEqualTo, partsSalesReturnBill.SalesUnitId));
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
            queryWindow.ExchangeData(null, "RefreshQueryResult", null);
        }

        private void CustomerInformationBySparepart_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var customerInformation = queryWindow.SelectedEntities.Cast<CustomerInformation>().FirstOrDefault();
            if(customerInformation == null)
                return;

            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;
            var detailList = partsSalesReturnBill.PartsSalesReturnBillDetails.ToList();
            foreach(var detail in detailList) {
                partsSalesReturnBill.PartsSalesReturnBillDetails.Remove(detail);
            }
            this.ClearPartsSalesReturnBill();
            partsSalesReturnBill.ReturnCompanyId = customerInformation.CustomerCompany.Id;
            partsSalesReturnBill.ReturnCompanyCode = customerInformation.CustomerCompany.Code;
            partsSalesReturnBill.ReturnCompanyName = customerInformation.CustomerCompany.Name;
            partsSalesReturnBill.ReturnCompanyType = customerInformation.CustomerCompany.Type;
            partsSalesReturnBill.InvoiceReceiveCompanyId = partsSalesReturnBill.ReturnCompanyId;
            partsSalesReturnBill.InvoiceReceiveCompanyCode = partsSalesReturnBill.ReturnCompanyCode;
            partsSalesReturnBill.InvoiceReceiveCompanyName = partsSalesReturnBill.ReturnCompanyName;
            partsSalesReturnBill.SubmitCompanyId = partsSalesReturnBill.ReturnCompanyId;
            partsSalesReturnBill.SubmitCompanyCode = partsSalesReturnBill.ReturnCompanyCode;
            partsSalesReturnBill.SubmitCompanyName = partsSalesReturnBill.ReturnCompanyName;
            //只有在退货单位选择后，如果退货单位为代理库或服务站兼代理库时退货仓库控件可见，
            this.returnCompanyId = customerInformation.CustomerCompany.Id;
            this.KvReturnWarehouse.Clear();
            if(returnCompanyId != default(int) && returnCompanyId != null) {
                domainContext.Load(domainContext.GetCompaniesQuery().Where(ex => ex.Id == returnCompanyId), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError)
                        if(!loadOp.IsErrorHandled) {
                            loadOp.MarkErrorAsHandled();
                            return;
                        }
                    var entity = loadOp.Entities.FirstOrDefault();
                    if(entity != null) {
                        this.returnCompany = entity;
                        if(entity.Type == (int)DcsCompanyType.代理库 || entity.Type == (int)DcsCompanyType.服务站兼代理库) {
                            blockReturnWarehouse.Visibility = Visibility.Visible;
                            comboxReturnWarehouse.Visibility = Visibility.Visible;
                        } else {
                            blockReturnWarehouse.Visibility = Visibility.Collapsed;
                            comboxReturnWarehouse.Visibility = Visibility.Collapsed;
                        }
                    }
                }, null);
            }
            //if(partsSalesOrderId != customerInformation.CustomerCompany.Id && partsSalesOrderId != 0 && partsSalesReturnBill.PartsSalesOrderId != 0) {
            //    DcsUtils.Confirm(PartsSalesUIStrings.DataEditPanel_Confirm_PartsSalesReturnBill_WhenPartsSalesOrderIdChangedWillClearPartsSalesReturnBill, () => {
            //        this.ClearPartsSalesReturnBill();
            //        partsSalesReturnBill.PartsSalesOrderId = customerInformation.CustomerCompany.Id;
            //        partsSalesReturnBill.ReturnCompanyCode = customerInformation.CustomerCompany.Code;
            //        partsSalesReturnBill.ReturnCompanyName = customerInformation.CustomerCompany.Name;
            //    });
            //}
            //if(partsSalesOrderId == 0 || partsSalesReturnBill.PartsSalesOrderId == 0) {
            //    partsSalesOrderId = customerInformation.CustomerCompany.Id;
            //    partsSalesReturnBill.PartsSalesOrderId = customerInformation.CustomerCompany.Id;
            //    partsSalesReturnBill.ReturnCompanyCode = customerInformation.CustomerCompany.Code;
            //    partsSalesReturnBill.ReturnCompanyName = customerInformation.CustomerCompany.Name;
            //}

            //if(partsSalesReturnBill.ReturnCompanyId != default(int) && partsSalesReturnBill.SalesUnitId != default(int)) {
            //    this.ptbPartsSalesOrder.IsEnabled = true;
            //} else {
            //    this.ptbPartsSalesOrder.IsEnabled = false;
            //}
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void PartsSalesReturnBillForAddByDeputyDataEditPanel_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            this.KvReturnWarehouse.Clear();
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;
            returnType = partsSalesReturnBill.ReturnType;
            //if(partsSalesReturnBill.Id == default(int))
            //    returnType = salesUnitId = partsSalesOrderId = default(int);
            //else {
            //    salesUnitId = partsSalesReturnBill.SalesUnitId;
            //    partsSalesOrderId = partsSalesReturnBill.PartsSalesOrderId.GetValueOrDefault();
            //}
            partsSalesReturnBill.PropertyChanged -= this.PartsSalesReturnBill_PropertyChanged;
            partsSalesReturnBill.PropertyChanged += this.PartsSalesReturnBill_PropertyChanged;
            if(partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.特殊退货) {
                this.DiscountRate.IsEnabled = true;
            } else this.DiscountRate.IsEnabled = false;
        }

        private void QueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var partsOutboundBillDetails = queryWindow.SelectedEntities.Cast<VirtualPartsOutboundBillDetail>();
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;
            if(partsSalesReturnBill.PartsSalesReturnBillDetails.Any(r => partsOutboundBillDetails.Any(item => item.SparePartId == r.SparePartId && item.PartsSalesOrderId == r.PartsSalesOrderId))) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditPanel_Validation_PartsSalesReturnBill_SameDetails);
                return;
            }
            if (returnCompany == null) { 
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditPanel_Validation_PartsSalesReturnBill_ReturnCompany);
                return;
            }
            partsSalesReturnBill.PartsSalesOrderId = partsOutboundBillDetails.First().PartsSalesOrderId;
            partsSalesReturnBill.PartsSalesOrderCode = partsOutboundBillDetails.First().PartsSalesOrderCode;
            partsSalesReturnBill.ERPSourceOrderCode = partsOutboundBillDetails.First().ERPSourceOrderCode;
            partsSalesReturnBill.WarehouseId = partsOutboundBillDetails.First().WarehouseId;
            partsSalesReturnBill.WarehouseName = partsOutboundBillDetails.First().WarehouseName;
            partsSalesReturnBill.WarehouseCode = partsOutboundBillDetails.First().WarehouseCode;

            foreach(var detailItem in partsOutboundBillDetails) {
                partsSalesReturnBill.PartsSalesReturnBillDetails.Add(new PartsSalesReturnBillDetail {
                    SparePartId = detailItem.SparePartId,
                    SparePartCode = detailItem.SparePartCode,
                    SparePartName = detailItem.SparePartName,
                    ReturnedQuantity = detailItem.ReturnedQuantity,
                    BatchNumber = detailItem.BatchNumber,
                    Remark = detailItem.Remark,
                    MeasureUnit = detailItem.MeasureUnit,
                    ReturnPrice = detailItem.SettlementPrice,
                    OriginalOrderPrice = detailItem.SettlementPrice,
                    OriginalPrice = detailItem.OriginalPrice,
                    PartsSalesOrderId = detailItem.PartsSalesOrderId,
                    PartsSalesOrderCode = detailItem.PartsSalesOrderCode,
                    TraceProperty = detailItem.TraceProperty
                });
            }
            partsSalesReturnBill.TotalAmount = partsSalesReturnBill.PartsSalesReturnBillDetails.Sum(entity => entity.ReturnedQuantity * entity.ReturnPrice);

            var partIds = partsSalesReturnBill.PartsSalesReturnBillDetails.Select(r => r.SparePartId).ToArray();
            if (partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.正常退货) {
                domainContext.Load(domainContext.GetCenterSaleReturnStrategiesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.ReturnType == (int)DcsPartsSalesReturn_ReturnType.正常退货), LoadBehavior.RefreshCurrent, loadOp => {
                    if (loadOp.HasError)
                        return;
                    var strategies = loadOp.Entities;
                    if (strategies != null) {
                        foreach (var item in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                            var partssalesorder = partsOutboundBillDetails.Where(r => r.PartsSalesOrderId == item.PartsSalesOrderId).FirstOrDefault();
                            if (partssalesorder != null) {
                                var timeSlot = (DateTime.Now.Date - partssalesorder.PartsSalesOrderSubmitTime.Value.Date).Days;
                                var returnDays = strategies.Where(r => timeSlot > r.TimeFrom.Value && timeSlot <= r.TimeTo.Value).SingleOrDefault();
                                if (returnDays != null && returnDays.DiscountRate != 0) {
                                    item.DiscountRate = returnDays.DiscountRate;
                                }
                                if (null != partsSalesReturnBill.DiscountRate) {
                                    item.ReturnPrice = Decimal.Parse((item.OriginalOrderPrice * (partsSalesReturnBill.DiscountRate)).ToString());
                                }
                                if (item.DiscountRate != null)
                                    item.ReturnPrice = Math.Round(item.ReturnPrice * Math.Round((decimal)item.DiscountRate, 2), 2);
                            }
                        }
                    }
                    partsSalesReturnBill.TotalAmount = partsSalesReturnBill.PartsSalesReturnBillDetails.Sum(entity => entity.ReturnedQuantity * entity.ReturnPrice);
                     var parent = queryWindow.ParentOfType<RadWindow>();
                     if (parent != null)
                         parent.Close();
                }, null);
            }else if (partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.保底退货){
                int? warehouseId = 0;
                if (returnCompany.Type == (int)DcsCompanyType.代理库 || returnCompany.Type == (int)DcsCompanyType.服务站兼代理库) {
                    warehouseId = partsSalesReturnBill.ReturnWarehouseId;
                }
                domainContext.Load(domainContext.GetCenterSaleReturnStrategiesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.ReturnType == (int)DcsPartsSalesReturn_ReturnType.保底退货), LoadBehavior.RefreshCurrent, loadOp => {
                    if (loadOp.HasError)
                        return;
                    var returnStrategies = loadOp.Entities;
                    domainContext.Load(domainContext.查询保底库存表信息2Query(returnCompany.Id,warehouseId,partIds), LoadBehavior.RefreshCurrent, loadOp1 => {
                        if (loadOp1.HasError)
                            return;
                        var bottomStocks = loadOp1.Entities;
                        foreach (var item in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                            if (bottomStocks != null) {
                                var bts = bottomStocks.Where(r => r.SparePartId == item.SparePartId).FirstOrDefault();
                                if(bts != null && bts.Status == (int)DcsBaseDataStatus.作废) {
                                    var abandonTime = bts.EndTime;
                                    var timeSlot = (DateTime.Now.Date - abandonTime.Value.Date).Days;
                                    var returnDays = returnStrategies.Where(r => timeSlot > r.TimeFrom.Value && timeSlot <= r.TimeTo.Value).SingleOrDefault();
                                    if (returnDays != null && returnDays.DiscountRate != 0) {
                                        item.DiscountRate = returnDays.DiscountRate;
                                    }
                                    if (null != partsSalesReturnBill.DiscountRate) {
                                        item.ReturnPrice = Decimal.Parse((item.OriginalOrderPrice * (partsSalesReturnBill.DiscountRate)).ToString());
                                    }
                                    if (item.DiscountRate != null)
                                        item.ReturnPrice = Math.Round(item.ReturnPrice * Math.Round((decimal)item.DiscountRate, 2), 2);
                                }
                            }
                        }
                        partsSalesReturnBill.TotalAmount = partsSalesReturnBill.PartsSalesReturnBillDetails.Sum(entity => entity.ReturnedQuantity * entity.ReturnPrice);
                        var parent = queryWindow.ParentOfType<RadWindow>();
                        if (parent != null)
                            parent.Close();
                    }, null);
                }, null);
            } else if(partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.特殊退货) {
                foreach(var item in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                    item.DiscountRate = 1;
                }
            }
        }

        private void PartsSalesReturnBill_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;
            switch(e.PropertyName) {
                case "SalesUnitId":
                    //if(partsSalesReturnBill.ReturnCompanyId != default(int) && partsSalesReturnBill.SalesUnitId != default(int)) {
                    //    this.ptbPartsSalesOrder.IsEnabled = true;
                    //} else
                    //    this.ptbPartsSalesOrder.IsEnabled = false;
                    if(partsSalesReturnBill.SalesUnitId == default(int))
                        return;
                    var detailList = partsSalesReturnBill.PartsSalesReturnBillDetails.ToList();
                    foreach(var detail in detailList) {
                        partsSalesReturnBill.PartsSalesReturnBillDetails.Remove(detail);
                    }
                    partsSalesReturnBill.PartsSalesOrderId = null;
                    partsSalesReturnBill.PartsSalesOrderCode = string.Empty;
                    var salesUnitInfo = this.KvSalesUnits.SingleOrDefault(entity => entity.Key == partsSalesReturnBill.SalesUnitId);
                    if(salesUnitInfo == null || !(salesUnitInfo.UserObject is SalesUnit))
                        return;
                    var sales = salesUnitInfo.UserObject as SalesUnit;
                    partsSalesReturnBill.PartsSalesCategoryId = sales.PartsSalesCategoryId;

                    partsSalesReturnBill.CustomerAccountId = default(int);
                    //获取客户账户
                    domainContext.Load(domainContext.GetCustomerAccountsQuery().Where(ex => ex.AccountGroupId == sales.AccountGroupId && ex.CustomerCompanyId == partsSalesReturnBill.ReturnCompanyId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            loadOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                            return;
                        }
                        var entity = loadOp.Entities.SingleOrDefault();
                        if(entity == null) {
                            UIHelper.ShowNotification(PartsSalesUIStrings.DataEditPanel_Notification_PartsSalesReturnBill_CustomerAccountIsNull);
                            return;
                        }
                        partsSalesReturnBill.CustomerAccountId = entity.Id;
                    }, null);
                    domainContext.Load(domainContext.GetCompaniesQuery().Where(ex => ex.Id == sales.OwnerCompanyId && ex.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp2 => {
                        if(loadOp2.HasError) {
                            loadOp2.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp2);
                            return;
                        }
                        var entity = loadOp2.Entities.SingleOrDefault();
                        if(entity == null)
                            return;
                        partsSalesReturnBill.SalesUnitOwnerCompanyId = sales.OwnerCompanyId;
                        partsSalesReturnBill.SalesUnitOwnerCompanyCode = entity.Code;
                        partsSalesReturnBill.SalesUnitOwnerCompanyName = entity.Name;
                    }, null);
                    domainContext.Load(domainContext.GetBranchesQuery().Where(ex => ex.Id == sales.BranchId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError || loadOp.Entities == null) {
                            UIHelper.ShowNotification(PartsSalesUIStrings.DataEditPanel_Validation_SalesUnit_BranchIsNull);
                            return;
                        }
                        partsSalesReturnBill.BranchId = sales.BranchId;
                        partsSalesReturnBill.BranchCode = loadOp.Entities.First().Code;
                        partsSalesReturnBill.BranchName = loadOp.Entities.First().Name;
                    }, null);
                    //退货仓库修改以后清空退货单清单与销售订单编号(第一次选择退货仓库时不清空销售订单编号)，
                    //根据选择的销售组织.配件销售类型id 获取销售组织（销售组织.隶属企业Id=提报企业Id,销售组织.配件销售类型Id=选择的配件销售类型Id）
                    //同时获取销售组织仓库关系（销售组织仓库关系.销售组织Id=获取的销售组织Id） 得到仓库Id过滤仓库数据源
                    this.KvReturnWarehouse.Clear();
                    domainContext.Load(domainContext.GetSalesUnitsQuery().Where(ex => ex.Status == (int)DcsMasterDataStatus.有效 && ex.OwnerCompanyId == returnCompanyId && ex.PartsSalesCategoryId == partsSalesReturnBill.PartsSalesCategoryId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            loadOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                            return;
                        }
                        var entity = loadOp.Entities.FirstOrDefault();
                        if(entity != null) {
                            domainContext.Load(domainContext.GetWarehousesBySalesUnitIdQuery(entity.Id), LoadBehavior.RefreshCurrent, loadOp1 => {
                                if(loadOp1.HasError) {
                                    loadOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOp1);
                                    return;
                                }
                                if(loadOp1.Entities != null && loadOp1.Entities.Any()) {
                                    foreach(var warehouses in loadOp1.Entities) {
                                        this.KvReturnWarehouse.Add(new KeyValuePair {
                                            Key = warehouses.Id,
                                            Value = warehouses.Name,
                                            UserObject = warehouses
                                        });
                                    }
                                }
                            }, null);
                        }
                    }, null);
                    break;
                case "ReturnWarehouseId":
                    var returnWarehouse = this.KvReturnWarehouse.SingleOrDefault(r => r.Key == partsSalesReturnBill.ReturnWarehouseId);
                    if(returnWarehouse == null)
                        return;
                    var warehouse = returnWarehouse.UserObject as Warehouse;
                    if(warehouse != null) {
                        partsSalesReturnBill.ReturnWarehouseCode = warehouse.Code;
                        partsSalesReturnBill.ReturnWarehouseId = warehouse.Id;
                        partsSalesReturnBill.ReturnWarehouseName = warehouse.Name;
                    }
                    break;
                case "ReturnType":
                    //if(partsSalesReturnBill.ReturnType == default(int))
                    //    return;
                    //if(returnType != default(int))
                    //    this.ClearPartsSalesReturnBill2();
                    //returnType = partsSalesReturnBill.ReturnType;
                    //this.ptbPartsSalesOrder.IsEnabled = partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturnBillReturnType.问题件退货;
                    ////if(returnType == default(int)) {
                    ////     this.ptbPartsSalesOrder.IsEnabled = partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturnBillReturnType.问题件退货;
                    ////     returnType = partsSalesReturnBill.ReturnType;
                    //// } else if(returnType != partsSalesReturnBill.ReturnType && returnType != default(int) && partsSalesReturnBill.ReturnType != default(int)) {
                    ////     DcsUtils.Confirm(PartsSalesUIStrings.DataEditPanel_Confirm_PartsSalesReturnBill_WhenReturnTypeChangedWillClearPartsSalesReturnBill, this.ClearPartsSalesReturnBill, () => {
                    ////         this.ClearPartsSalesReturnBill();
                    ////         this.ptbPartsSalesOrder.IsEnabled = partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturnBillReturnType.问题件退货;
                    ////         partsSalesReturnBill.ReturnType = returnType;
                    ////     });
                    //// }
                    if(partsSalesReturnBill.ReturnType == default(int))
                        return;

                    if(this.returnType != default(int)) {
                        DcsUtils.Confirm(PartsSalesUIStrings.DataEditPanel_Confirm_PartsSalesReturnBill_WhenReturnTypeChangedWillClearPartsSalesReturnBill, () => {
                            this.ClearPartsSalesReturnBill2();
                            if(partsSalesReturnBill.BranchId != default(int) && partsSalesReturnBill.SalesUnitId != default(int) && partsSalesReturnBill.ReturnType != default(int)) {
                                //  this.ptbPartsSalesOrder.IsEnabled = partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturnBillReturnType.问题件退货;
                            }
                            this.returnType = partsSalesReturnBill.ReturnType;
                        }, () => {
                            partsSalesReturnBill.ReturnType = this.returnType;
                            if(partsSalesReturnBill.BranchId != default(int) && partsSalesReturnBill.SalesUnitId != default(int) && partsSalesReturnBill.ReturnType != default(int)) {
                                //  this.ptbPartsSalesOrder.IsEnabled = partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturnBillReturnType.问题件退货;
                            }
                        });
                    } else {
                        if(partsSalesReturnBill.BranchId != default(int) && partsSalesReturnBill.SalesUnitId != default(int) && partsSalesReturnBill.ReturnType != default(int)) {
                            //  this.ptbPartsSalesOrder.IsEnabled = partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturnBillReturnType.问题件退货;
                        }
                        this.returnType = partsSalesReturnBill.ReturnType;
                    }
                    if(partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.特殊退货) {
                        partsSalesReturnBill.DiscountRate =decimal.Parse((0.1).ToString());
                        this.DiscountRate.IsEnabled = true;
                    } else {
                        this.DiscountRate.IsEnabled = false;
                        partsSalesReturnBill.DiscountRate = decimal.Parse((1).ToString());
                    }
                    break;
                case "ReturnCompanyType":
                    if(partsSalesReturnBill.ReturnCompanyType == (int)DcsCompanyType.服务站)
                        this.ReturnTypeComboBox.ItemsSource = KvKvReturnTypes;
                    else
                        this.ReturnTypeComboBox.ItemsSource = KvReturnType;
                    break;
            }
        }

        private void ClearPartsSalesReturnBill() {
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;
            var detailList = partsSalesReturnBill.PartsSalesReturnBillDetails.ToList();
            foreach(var item in detailList) {
                item.ValidationErrors.Clear();
                partsSalesReturnBill.PartsSalesReturnBillDetails.Remove(item);
            }
            partsSalesReturnBill.PartsSalesOrderId = null;
            partsSalesReturnBill.PartsSalesOrderCode = string.Empty;
            partsSalesReturnBill.ReturnCompanyId = default(int);
            partsSalesReturnBill.ReturnCompanyCode = string.Empty;
            partsSalesReturnBill.ReturnCompanyName = string.Empty;
            partsSalesReturnBill.CustomerAccountId = default(int);
            //partsSalesReturnBill.BranchId = default(int);
            //partsSalesReturnBill.BranchCode = string.Empty;
            //partsSalesReturnBill.BranchName = string.Empty;
            partsSalesReturnBill.SalesUnitId = default(int);
            partsSalesReturnBill.SalesUnitName = string.Empty;
            //partsSalesReturnBill.SalesUnitOwnerCompanyId = default(int);
            //partsSalesReturnBill.SalesUnitOwnerCompanyCode = string.Empty;
            //partsSalesReturnBill.SalesUnitOwnerCompanyName = string.Empty;
            partsSalesReturnBill.InvoiceRequirement = null;
            partsSalesReturnBill.BlueInvoiceNumber = string.Empty;
            partsSalesReturnBill.ReturnReason = string.Empty;
            partsSalesReturnBill.Remark = string.Empty;
            partsSalesReturnBill.TotalAmount = 0;
            partsSalesReturnBill.ContactPerson = string.Empty;
            partsSalesReturnBill.ContactPhone = string.Empty;
        }

        private void ClearPartsSalesReturnBill2() {
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;
            var detailList = partsSalesReturnBill.PartsSalesReturnBillDetails.ToList();
            foreach(var item in detailList) {
                item.ValidationErrors.Clear();
                partsSalesReturnBill.PartsSalesReturnBillDetails.Remove(item);
            }
            partsSalesReturnBill.PartsSalesOrderId = null;
            partsSalesReturnBill.PartsSalesOrderCode = string.Empty;
            //partsSalesReturnBill.CustomerAccountId = default(int);
            //partsSalesReturnBill.BranchId = default(int);
            //partsSalesReturnBill.BranchCode = string.Empty;
            //partsSalesReturnBill.BranchName = string.Empty;
            //partsSalesReturnBill.SalesUnitOwnerCompanyId = default(int);
            //partsSalesReturnBill.SalesUnitOwnerCompanyCode = string.Empty;
            //partsSalesReturnBill.SalesUnitOwnerCompanyName = string.Empty;
            partsSalesReturnBill.InvoiceRequirement = null;
            partsSalesReturnBill.BlueInvoiceNumber = string.Empty;
            partsSalesReturnBill.ReturnReason = string.Empty;
            partsSalesReturnBill.Remark = string.Empty;
            partsSalesReturnBill.TotalAmount = 0;
            partsSalesReturnBill.ContactPerson = string.Empty;
            partsSalesReturnBill.ContactPhone = string.Empty;
        }

        public object KvInvoiceRequirement {
            get {
                return this.KeyValueManager[this.kvNames[1]];
            }
        }
        public object KvShippingMethod {
            get {
                return this.KeyValueManager[this.kvNames[3]];
            }
        }
        public ObservableCollection<KeyValuePair> KvKvReturnTypes {
            get {
                return this.kvkvReturnType ?? (this.kvkvReturnType = new ObservableCollection<KeyValuePair>());
            }
        }
        public ObservableCollection<KeyValuePair> KvReturnType {
            get {
                return this.kvReturnType ?? (this.kvReturnType = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvSalesUnits {
            get {
                return this.kvSalesUnits ?? (this.kvSalesUnits = new ObservableCollection<KeyValuePair>());
            }
        }

        private void ComboBoxReturnReason_OnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill != null)
                partsSalesReturnBill.ReturnReason = this.comboBoxReturnReason.Text;
        }
    }
}
