﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit {
    public partial class PartsSalesReturnBillForReportDataEditPanel {
        private readonly ObservableCollection<KeyValuePair> kvSalesUnits = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvReturnTypes = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvReturnWarehouse = new ObservableCollection<KeyValuePair>();
        private Company company;
        private readonly DcsDomainContext domainContext = new DcsDomainContext();
        private int returnType, branchId;
        //设计上未写，清空数据前是否给提示
        //private int  salesUnitId;
        public object ReturnReasons {
            get {
                return this.KeyValueManager[this.kvNames[2]];
            }
        }

        private readonly string[] kvNames = new[] {
            "PartsSalesReturn_ReturnType", "PartsSalesReturnBill_InvoiceRequirement","ReturnReason","PartsShipping_Method"
        };

        public PartsSalesReturnBillForReportDataEditPanel() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += PartsSalesReturnBillForReportDataEditPanel_DataContextChanged;
        }

        private void PartsSalesReturnBillForReportDataEditPanel_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            //this.KvReturnWarehouse.Clear();
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;
            this.returnType = partsSalesReturnBill.ReturnType;
            branchId = partsSalesReturnBill.BranchId;
            this.KvSalesUnits.Clear();
            // this.ptbPartsSalesOrder.IsEnabled = partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturnBillReturnType.问题件退货;
            if(partsSalesReturnBill.BranchId != default(int)) {
                domainContext.Load(domainContext.GetSalesUnitsQuery().Where(ex => ex.Status == (int)DcsMasterDataStatus.有效 && ex.BranchId == partsSalesReturnBill.BranchId), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError)
                        return;
                    foreach(var salesUnit in loadOp.Entities)
                        this.KvSalesUnits.Add(new KeyValuePair {
                            Key = salesUnit.Id,
                            Value = salesUnit.Name,
                            UserObject = salesUnit
                        });
                }, null);
            }
            if(partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.特殊退货) {
                this.DiscountRate.IsEnabled = true;
            } else this.DiscountRate.IsEnabled = false;

            partsSalesReturnBill.PropertyChanged -= this.PartsSalesReturnBill_PropertyChanged;
            partsSalesReturnBill.PropertyChanged += this.PartsSalesReturnBill_PropertyChanged;
        }

        private void PartsSalesReturnBill_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;
            switch(e.PropertyName) {
                case "BranchId":
                    if(partsSalesReturnBill.BranchId == default(int))
                        return;
                    //if(partsSalesReturnBill.BranchId != default(int) && partsSalesReturnBill.SalesUnitId != default(int) && partsSalesReturnBill.ReturnType != default(int)) {
                    //    this.ptbPartsSalesOrder.IsEnabled = partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturnBillReturnType.问题件退货;
                    //}
                    System.Action assignment = () => {
                        var branchInfo = this.KvBranches.SingleOrDefault(entity => entity.Key == partsSalesReturnBill.BranchId);
                        if(branchInfo == null || !(branchInfo.UserObject is Branch))
                            return;


                        var branch = branchInfo.UserObject as Branch;
                        partsSalesReturnBill.BranchName = branch.Name;
                        partsSalesReturnBill.BranchCode = branch.Code;
                        this.KvSalesUnits.Clear();
                        domainContext.Load(domainContext.GetCompaniesQuery().Where(ex => ex.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                            if(loadOp.HasError)
                                return;
                            var companies = loadOp.Entities.SingleOrDefault();
                            if(companies != null) {
                                if(companies.Type == (int)DcsCompanyType.代理库 || companies.Type == (int)DcsCompanyType.服务站兼代理库) {
                                    domainContext.Load(domainContext.GetSalesUnitsForAgencyReportPSRBQuery(BaseApp.Current.CurrentUserData.EnterpriseId, partsSalesReturnBill.BranchId), LoadBehavior.RefreshCurrent, loadOp2 => {
                                        if(loadOp.HasError)
                                            return;
                                        foreach(var salesUnit in loadOp2.Entities)
                                            this.KvSalesUnits.Add(new KeyValuePair {
                                                Key = salesUnit.Id,
                                                Value = salesUnit.Name,
                                                UserObject = salesUnit
                                            });
                                    }, null);
                                } else if(companies.Type == (int)DcsCompanyType.服务站) {
                                    domainContext.Load(domainContext.GetSalesUnitsQuery().Where(ex => ex.Status == (int)DcsMasterDataStatus.有效 && ex.OwnerCompanyId == partsSalesReturnBill.BranchId), LoadBehavior.RefreshCurrent, loadOp2 => {
                                        if(loadOp2.HasError)
                                            return;
                                        foreach(var salesUnit in loadOp2.Entities)
                                            this.KvSalesUnits.Add(new KeyValuePair {
                                                Key = salesUnit.Id,
                                                Value = salesUnit.Name,
                                                UserObject = salesUnit
                                            });
                                        domainContext.Load(domainContext.GetAgencyDealerRelationsQuery().Where(r => r.DealerId == BaseApp.Current.CurrentUserData.EnterpriseId && r.BranchId == partsSalesReturnBill.BranchId && r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOption => {
                                            if(loadOption.HasError)
                                                return;
                                            if(loadOption.Entities != null) {
                                                var agencyIds = loadOption.Entities.Select(r => r.AgencyId).ToArray();
                                                domainContext.Load(domainContext.GetSalesUnitsQuery().Where(ex => ex.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp3 => {
                                                    if(loadOp3.HasError)
                                                        return;
                                                    foreach(var salesUnit in loadOp3.Entities.Where(r => agencyIds.Contains(r.OwnerCompanyId)))
                                                        this.KvSalesUnits.Add(new KeyValuePair {
                                                            Key = salesUnit.Id,
                                                            Value = salesUnit.Name,
                                                            UserObject = salesUnit
                                                        });
                                                }, null);
                                            }
                                        }, null);
                                    }, null);
                                }
                            }
                        }, null);
                        branchId = partsSalesReturnBill.BranchId;
                    };
                    if(branchId != default(int) && branchId != partsSalesReturnBill.BranchId)
                        DcsUtils.Confirm(PartsSalesUIStrings.DataEditPanel_Confirm_PartsSalesReturnBill_WhenBranchChangedWillClearPartsSalesReturnBill, () => {
                            this.ClearPartsSalesReturnBillByBranch();
                            assignment.Invoke();
                        }, () => partsSalesReturnBill.BranchId = branchId);
                    else
                        assignment.Invoke();
                    break;
                case "SalesUnitId":
                    if(partsSalesReturnBill.SalesUnitId == default(int))
                        return;
                    //if(partsSalesReturnBill.BranchId != default(int) && partsSalesReturnBill.SalesUnitId != default(int) && partsSalesReturnBill.ReturnType != default(int)) {
                    //    this.ptbPartsSalesOrder.IsEnabled = partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturnBillReturnType.问题件退货;
                    //}
                    var detailList = partsSalesReturnBill.PartsSalesReturnBillDetails.ToList();
                    foreach(var detail in detailList) {
                        partsSalesReturnBill.PartsSalesReturnBillDetails.Remove(detail);
                    }
                    //partsSalesReturnBill.PartsSalesOrderId = null;
                    //partsSalesReturnBill.PartsSalesOrderCode = string.Empty;
                    var salesUnitInfo = this.KvSalesUnits.SingleOrDefault(entity => entity.Key == partsSalesReturnBill.SalesUnitId);
                    if(salesUnitInfo == null || !(salesUnitInfo.UserObject is SalesUnit))
                        return;
                    var sales = salesUnitInfo.UserObject as SalesUnit;
                    partsSalesReturnBill.SalesUnitName = sales.Name;
                    partsSalesReturnBill.PartsSalesCategoryId = sales.PartsSalesCategoryId;
                    //获取隶属企业编号
                    domainContext.Load(domainContext.GetCompaniesQuery().Where(ex => ex.Id == sales.OwnerCompanyId && ex.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            loadOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                            return;
                        }
                        var entity = loadOp.Entities.SingleOrDefault();
                        if(entity == null)
                            return;
                        partsSalesReturnBill.SalesUnitOwnerCompanyId = sales.OwnerCompanyId;
                        partsSalesReturnBill.SalesUnitOwnerCompanyCode = entity.Code;
                        partsSalesReturnBill.SalesUnitOwnerCompanyName = entity.Name;
                    }, null);
                    partsSalesReturnBill.CustomerAccountId = default(int);
                    //获取客户账户
                    domainContext.Load(domainContext.GetCustomerAccountsQuery().Where(ex => ex.AccountGroupId == sales.AccountGroupId && ex.CustomerCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && ex.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            loadOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                            return;
                        }
                        var entity = loadOp.Entities.SingleOrDefault();
                        if(entity == null) {
                            UIHelper.ShowNotification(PartsSalesUIStrings.DataEditPanel_Notification_PartsSalesReturnBill_CustomerAccountIsNull);
                            return;
                        }
                        partsSalesReturnBill.CustomerAccountId = entity.Id;
                    }, null);
                    //获取退货仓库数据源
                    partsSalesReturnBill.ReturnWarehouseId = null;
                    partsSalesReturnBill.ReturnWarehouseCode = null;
                    partsSalesReturnBill.ReturnWarehouseName = null;
                    this.KvReturnWarehouse.Clear();
                    //只有在登陆企业为代理库或服务站兼代理库时退货仓库控件可见，退货仓库修改以后清空退货单清单与销售订单编号(第一次选择退货仓库时不清空销售订单编号)，
                    //根据选择的销售组织.配件销售类型id 获取销售组织（销售组织.隶属企业Id=登陆企业Id,销售组织.配件销售类型Id=选择的配件销售类型Id）
                    //同时获取销售组织仓库关系（销售组织仓库关系.销售组织Id=获取的销售组织Id） 得到仓库Id过滤仓库数据源
                    domainContext.Load(domainContext.GetSalesUnitsQuery().Where(ex => ex.Status == (int)DcsMasterDataStatus.有效 && ex.OwnerCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && ex.PartsSalesCategoryId == partsSalesReturnBill.PartsSalesCategoryId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            loadOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                            return;
                        }
                        var entity = loadOp.Entities.FirstOrDefault();
                        if(entity != null) {
                            domainContext.Load(domainContext.GetWarehousesBySalesUnitIdQuery(entity.Id), LoadBehavior.RefreshCurrent, loadOp1 => {
                                if(loadOp1.HasError) {
                                    loadOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOp1);
                                    return;
                                }
                                if(loadOp1.Entities != null && loadOp1.Entities.Any()) {
                                    foreach(var warehouses in loadOp1.Entities) {
                                        this.KvReturnWarehouse.Add(new KeyValuePair {
                                            Key = warehouses.Id,
                                            Value = warehouses.Name,
                                            UserObject = warehouses
                                        });
                                    }
                                }
                            }, null);
                        }
                    }, null);
                    break;
                case "ReturnWarehouseId":
                    var partsSalesReturnBillDetails = partsSalesReturnBill.PartsSalesReturnBillDetails.ToArray();
                    var returnWarehouse = this.KvReturnWarehouse.SingleOrDefault(r => r.Key == partsSalesReturnBill.ReturnWarehouseId);
                    if(returnWarehouse == null)
                        return;
                    var warehouse = returnWarehouse.UserObject as Warehouse;
                    if(warehouse != null) {
                        partsSalesReturnBill.ReturnWarehouseCode = warehouse.Code;
                        partsSalesReturnBill.ReturnWarehouseId = warehouse.Id;
                        partsSalesReturnBill.ReturnWarehouseName = warehouse.Name;
                    }
                    break;
                case "ReturnType":
                    if(partsSalesReturnBill.ReturnType == default(int))
                        return;

                    if(this.returnType != default(int)) {// && partsSalesReturnBill.ReturnType != this.returnType
                        //if(returnTypeIsChanged)
                        DcsUtils.Confirm(PartsSalesUIStrings.DataEditPanel_Confirm_PartsSalesReturnBill_WhenReturnTypeChangedWillClearPartsSalesReturnBill, () => {
                            this.ClearPartsSalesReturnBillByReturnType();
                            if(partsSalesReturnBill.BranchId != default(int) && partsSalesReturnBill.SalesUnitId != default(int) && partsSalesReturnBill.ReturnType != default(int)) {
                                //  this.ptbPartsSalesOrder.IsEnabled = partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturnBillReturnType.问题件退货;
                            }
                            this.returnType = partsSalesReturnBill.ReturnType;
                        }, () => {
                            partsSalesReturnBill.ReturnType = this.returnType;
                            if(partsSalesReturnBill.BranchId != default(int) && partsSalesReturnBill.SalesUnitId != default(int) && partsSalesReturnBill.ReturnType != default(int)) {
                                //  this.ptbPartsSalesOrder.IsEnabled = partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturnBillReturnType.问题件退货;
                            }
                        });
                    } else {
                        if(partsSalesReturnBill.BranchId != default(int) && partsSalesReturnBill.SalesUnitId != default(int) && partsSalesReturnBill.ReturnType != default(int)) {
                            //  this.ptbPartsSalesOrder.IsEnabled = partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturnBillReturnType.问题件退货;
                        }
                        this.returnType = partsSalesReturnBill.ReturnType;
                    }
                    if(partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.特殊退货) {
                        partsSalesReturnBill.DiscountRate = decimal.Parse((0.1).ToString());
                        this.DiscountRate.IsEnabled = true;
                    } else {
                        this.DiscountRate.IsEnabled = false;
                        partsSalesReturnBill.DiscountRate = decimal.Parse((1).ToString());
                    }
                    break;
            }
        }

        private void ClearPartsSalesReturnBillByBranch() {
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;
            foreach(var item in partsSalesReturnBill.PartsSalesReturnBillDetails.ToList()) {
                item.ValidationErrors.Clear();
                partsSalesReturnBill.PartsSalesReturnBillDetails.Remove(item);
            }
            partsSalesReturnBill.TotalAmount = 0;
            partsSalesReturnBill.SalesUnitId = 0;
            partsSalesReturnBill.SalesUnitName = string.Empty;
            partsSalesReturnBill.SalesUnitOwnerCompanyName = string.Empty;
            partsSalesReturnBill.SalesUnitOwnerCompanyCode = string.Empty;
            partsSalesReturnBill.SalesUnitOwnerCompanyId = 0;
            partsSalesReturnBill.CustomerAccountId = default(int);
            partsSalesReturnBill.InvoiceRequirement = null;
            partsSalesReturnBill.BlueInvoiceNumber = string.Empty;
            partsSalesReturnBill.ContactPerson = string.Empty;
            partsSalesReturnBill.ContactPhone = string.Empty;
            partsSalesReturnBill.PartsSalesOrderCode = string.Empty;
            partsSalesReturnBill.PartsSalesOrderId = 0;
            partsSalesReturnBill.ReturnReason = string.Empty;
            partsSalesReturnBill.Remark = string.Empty;
        }

        private void ClearPartsSalesReturnBillByReturnType() {
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;
            foreach(var item in partsSalesReturnBill.PartsSalesReturnBillDetails.ToList()) {
                item.ValidationErrors.Clear();
                partsSalesReturnBill.PartsSalesReturnBillDetails.Remove(item);
            }
            //partsSalesReturnBill.BranchId = 0;
            //partsSalesReturnBill.BranchCode = string.Empty;
            //partsSalesReturnBill.BranchName = string.Empty;

            //partsSalesReturnBill.TotalAmount = 0;
            ////partsSalesReturnBill.SalesUnitId = 0;
            ////partsSalesReturnBill.SalesUnitName = string.Empty;
            ////partsSalesReturnBill.SalesUnitOwnerCompanyName = string.Empty;
            ////partsSalesReturnBill.SalesUnitOwnerCompanyCode = string.Empty;
            ////partsSalesReturnBill.SalesUnitOwnerCompanyId = 0;
            //partsSalesReturnBill.CustomerAccountId = default(int);
            //partsSalesReturnBill.InvoiceRequirement = null;
            //partsSalesReturnBill.BlueInvoiceNumber = string.Empty;
            //partsSalesReturnBill.ContactPerson = string.Empty;
            //partsSalesReturnBill.ContactPhone = string.Empty;
            //partsSalesReturnBill.OriginalRequirementBillType = 0;
            //partsSalesReturnBill.OriginalRequirementBillId = 0;
            partsSalesReturnBill.PartsSalesOrderCode = string.Empty;
            partsSalesReturnBill.PartsSalesOrderId = 0;
            //partsSalesReturnBill.ReturnReason = string.Empty;
            //partsSalesReturnBill.Remark = string.Empty;
        }

        private void CreateUI() {
            this.KeyValueManager.LoadData(() => {
                this.KvReturnTypes.Clear();
                foreach(var item in this.KeyValueManager[this.kvNames[0]]) {
                    this.KvReturnTypes.Add(new KeyValuePair {
                        Key = item.Key,
                        Value = item.Value
                    });
                }
                domainContext.Load(domainContext.GetCompaniesQuery().Where(e => e.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError)
                        return;
                    var company = loadOp.Entities.FirstOrDefault();
                    if(company == null)
                        return;
                    this.company = company;
                    if(company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                        this.blockReturnWarehouse.Visibility = Visibility.Visible;
                        this.comboxReturnWarehouse.Visibility = Visibility.Visible;
                    }
                    //if(company.Type == (int)DcsCompanyType.服务站) {
                    //    var key = this.KvReturnTypes.SingleOrDefault(k => k.Key == (int)DcsPartsSalesReturnBillReturnType.积压件退货);
                    //    if(key == null)
                    //        return;
                    //    this.kvReturnTypes.Remove(key);
                    //}
                }, null);
            });
            domainContext.Load(domainContext.根据企业查询营销分公司Query(BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvBranches.Clear();
                foreach(var branch in loadOp.Entities)
                    this.KvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name,
                        UserObject = branch
                    });
            }, null);

            this.ptbPartsSalesOrder.PopupContent = MultiSelectQueryWindow;

        }

        private DcsMultiPopupsQueryWindowBase multiSelectQueryWindow;
        private DcsMultiPopupsQueryWindowBase MultiSelectQueryWindow {
            get {
                if(this.multiSelectQueryWindow == null) {
                    multiSelectQueryWindow = DI.GetQueryWindow("PartsOutboundBillDetailForMulitQueryDetail") as DcsMultiPopupsQueryWindowBase;
                    if(multiSelectQueryWindow != null) {
                        multiSelectQueryWindow.SelectionDecided += this.QueryWindow_SelectionDecided;
                        multiSelectQueryWindow.Loaded += queryWindow_Loaded;
                    }
                }
                return multiSelectQueryWindow;
            }
        }

        private void queryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;
            if (partsSalesReturnBill.ReturnType == default(int)) { 
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesReturnBill_ReturnType);
                return;
            }
            if ((this.company.Type == (int)DcsCompanyType.代理库 || this.company.Type == (int)DcsCompanyType.服务站兼代理库 )&& partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.保底退货) {
                if (partsSalesReturnBill.ReturnWarehouseId == null || partsSalesReturnBill.ReturnWarehouseId == default(int)) {
                    var parent = queryWindow.ParentOfType<RadWindow>();
                    if (parent != null)
                        parent.Close();
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditPanel_Validation_PartsSalesReturnBill_ReturnWarehouseIsNull);
                    return;
                }
            }
            var query = this.MultiSelectQueryWindow as PartsOutboundBillDetailForMulitQueryDetailQueryWindow;
            if(query != null) {
                query.SetQueryPanelParameters(partsSalesReturnBill.PartsSalesCategoryId);
            }
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem("SubmitCompanyId", typeof(int), FilterOperator.IsEqualTo, BaseApp.Current.CurrentUserData.EnterpriseId));
            compositeFilterItem.Filters.Add(new FilterItem("SalesUnitId", typeof(int), FilterOperator.IsEqualTo, partsSalesReturnBill.SalesUnitId));

            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }

        private void QueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var partsOutboundBillDetails = queryWindow.SelectedEntities.Cast<VirtualPartsOutboundBillDetail>();
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;
            if(partsSalesReturnBill.PartsSalesReturnBillDetails.Any(r => partsOutboundBillDetails.Any(item => item.SparePartId == r.SparePartId && item.PartsSalesOrderId == r.PartsSalesOrderId))) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditPanel_Validation_PartsSalesReturnBill_SameDetails);
                return;
            }
            partsSalesReturnBill.PartsSalesOrderId = partsOutboundBillDetails.First().PartsSalesOrderId;
            partsSalesReturnBill.PartsSalesOrderCode = partsOutboundBillDetails.First().PartsSalesOrderCode;
            partsSalesReturnBill.ERPSourceOrderCode = partsOutboundBillDetails.First().ERPSourceOrderCode;
            partsSalesReturnBill.WarehouseId = partsOutboundBillDetails.First().WarehouseId;
            partsSalesReturnBill.WarehouseName = partsOutboundBillDetails.First().WarehouseName;
            partsSalesReturnBill.WarehouseCode = partsOutboundBillDetails.First().WarehouseCode;

            foreach(var detailItem in partsOutboundBillDetails) {
                partsSalesReturnBill.PartsSalesReturnBillDetails.Add(new PartsSalesReturnBillDetail {
                    SparePartId = detailItem.SparePartId,
                    SparePartCode = detailItem.SparePartCode,
                    SparePartName = detailItem.SparePartName,
                    ReturnedQuantity = detailItem.ReturnedQuantity,
                    BatchNumber = detailItem.BatchNumber,
                    Remark = detailItem.Remark,
                    MeasureUnit = detailItem.MeasureUnit,
                    ReturnPrice = detailItem.SettlementPrice,
                    OriginalOrderPrice = detailItem.SettlementPrice,
                    OriginalPrice = detailItem.OriginalPrice,
                    PartsSalesOrderId = detailItem.PartsSalesOrderId,
                    PartsSalesOrderCode = detailItem.PartsSalesOrderCode,
                    TraceProperty=detailItem.TraceProperty
                });
            }
            partsSalesReturnBill.TotalAmount = partsSalesReturnBill.PartsSalesReturnBillDetails.Sum(entity => entity.ReturnedQuantity * entity.ReturnPrice);
            var partIds = partsSalesReturnBill.PartsSalesReturnBillDetails.Select(r => r.SparePartId).ToArray();
            if (partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.正常退货) {
                domainContext.Load(domainContext.GetCenterSaleReturnStrategiesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.ReturnType == (int)DcsPartsSalesReturn_ReturnType.正常退货), LoadBehavior.RefreshCurrent, loadOp => {
                    if (loadOp.HasError)
                        return;
                    var strategies = loadOp.Entities;
                    if (strategies != null) {
                        foreach (var item in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                            var partssalesorder = partsOutboundBillDetails.Where(r => r.PartsSalesOrderId == item.PartsSalesOrderId).FirstOrDefault();
                            if (partssalesorder != null) {
                                var timeSlot = (DateTime.Now.Date - partssalesorder.PartsSalesOrderSubmitTime.Value.Date).Days;
                                var returnDays = strategies.Where(r => timeSlot > r.TimeFrom.Value && timeSlot <= r.TimeTo.Value).SingleOrDefault();
                                if (returnDays != null && returnDays.DiscountRate != 0) {
                                    item.DiscountRate = returnDays.DiscountRate;
                                }
                                if (null != partsSalesReturnBill.DiscountRate) {
                                    item.ReturnPrice = Decimal.Parse((item.OriginalOrderPrice * (partsSalesReturnBill.DiscountRate)).ToString());
                                }
                                if (item.DiscountRate != null)
                                    item.ReturnPrice = Math.Round(item.ReturnPrice * Math.Round((decimal)item.DiscountRate, 2), 2);
                            }
                        }
                    }
                    partsSalesReturnBill.TotalAmount = partsSalesReturnBill.PartsSalesReturnBillDetails.Sum(entity => entity.ReturnedQuantity * entity.ReturnPrice);
                     var parent = queryWindow.ParentOfType<RadWindow>();
                     if (parent != null)
                         parent.Close();
                }, null);
            }else if (partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.保底退货){
                int? warehouseId = 0;
                if (company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                    warehouseId = partsSalesReturnBill.ReturnWarehouseId;
                }
                domainContext.Load(domainContext.GetCenterSaleReturnStrategiesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.ReturnType == (int)DcsPartsSalesReturn_ReturnType.保底退货), LoadBehavior.RefreshCurrent, loadOp => {
                    if (loadOp.HasError)
                        return;
                    var returnStrategies = loadOp.Entities;
                    domainContext.Load(domainContext.查询保底库存表信息2Query(company.Id,warehouseId,partIds), LoadBehavior.RefreshCurrent, loadOp1 => {
                        if (loadOp1.HasError)
                            return;
                        var bottomStocks = loadOp1.Entities;
                        foreach (var item in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                            if (bottomStocks != null) {
                                var bts = bottomStocks.Where(r => r.SparePartId == item.SparePartId).FirstOrDefault();
                                if(bts != null && bts.Status == (int)DcsBaseDataStatus.作废) {
                                    var abandonTime = bts.EndTime;
                                    var timeSlot = (DateTime.Now.Date - abandonTime.Value.Date).Days;
                                    var returnDays = returnStrategies.Where(r => timeSlot > r.TimeFrom.Value && timeSlot <= r.TimeTo.Value).SingleOrDefault();
                                    if (returnDays != null && returnDays.DiscountRate != 0) {
                                        item.DiscountRate = returnDays.DiscountRate;
                                    }
                                    if (null != partsSalesReturnBill.DiscountRate) {
                                        item.ReturnPrice = Decimal.Parse((item.OriginalOrderPrice * (partsSalesReturnBill.DiscountRate)).ToString());
                                    }
                                    if (item.DiscountRate != null)
                                        item.ReturnPrice = Math.Round(item.ReturnPrice * Math.Round((decimal)item.DiscountRate, 2), 2);
                                }
                            }
                        }
                        partsSalesReturnBill.TotalAmount = partsSalesReturnBill.PartsSalesReturnBillDetails.Sum(entity => entity.ReturnedQuantity * entity.ReturnPrice);
                        var parent = queryWindow.ParentOfType<RadWindow>();
                        if (parent != null)
                            parent.Close();
                    }, null);
                }, null);
            } else if(partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.特殊退货) {
                foreach(var item in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                    item.DiscountRate = 1;
                }
            }
        }

        public ObservableCollection<KeyValuePair> KvReturnTypes {
            get {
                return this.kvReturnTypes;
            }
        }

        public ObservableCollection<KeyValuePair> KvBranches {
            get {
                return this.kvBranches;
            }
        }

        public ObservableCollection<KeyValuePair> KvSalesUnits {
            get {
                return this.kvSalesUnits;
            }
        }

        public ObservableCollection<KeyValuePair> KvReturnWarehouse {
            get {
                return this.kvReturnWarehouse;
            }
        }

        public object KvInvoiceRequirements {
            get {
                return this.KeyValueManager[this.kvNames[1]];
            }
        }
        public object KvShippingMethod {
            get {
                return this.KeyValueManager[this.kvNames[3]];
            }
        }
    }
}
