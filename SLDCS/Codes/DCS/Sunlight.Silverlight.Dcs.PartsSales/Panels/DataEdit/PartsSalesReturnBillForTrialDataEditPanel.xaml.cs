﻿using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;


namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit
{
    public partial class PartsSalesReturnBillForTrialDataEditPanel {
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        //private ObservableCollection<KeyValuePair> kvIsPasseds;
        //public ObservableCollection<KeyValuePair> KvIsPasseds
        //{
        //    get
        //    {
        //        return this.kvIsPasseds ?? (this.kvIsPasseds = new ObservableCollection<KeyValuePair>());
        //    }
        //}
        private readonly string[] kvNames = {
            "PartsSalesReturn_ReturnType", "PartsSalesReturnBill_InvoiceRequirement","PartsShipping_Method"
        };

        public PartsSalesReturnBillForTrialDataEditPanel()
        {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += PartsSalesReturnBillForApproveDataEditPanel_DataContextChanged;
            this.Initializer.Register(this.CreateUI);
        }

        private void PartsSalesReturnBillForApproveDataEditPanel_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;
            partsSalesReturnBill.PropertyChanged -= this.PartsSalesReturnBill_PropertyChanged;
            partsSalesReturnBill.PropertyChanged += this.PartsSalesReturnBill_PropertyChanged;

            if(partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.特殊退货) {
                this.DiscountRate.IsEnabled = true;
            } else this.DiscountRate.IsEnabled = false;
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.根据销售组织查询仓库Query(partsSalesReturnBill.SalesUnitId).Where(r => r.Type != (int)DcsWarehouseType.虚拟库), LoadBehavior.RefreshCurrent, loadOp => {
                if (loadOp.HasError)
                    return;
                this.KvWarehouses.Clear();
                foreach (var warehouses in loadOp.Entities)
                    this.KvWarehouses.Add(new KeyValuePair {
                        Key = warehouses.Id,
                        Value = warehouses.Name,
                        UserObject = warehouses
                    });
            }, null);
        }

        private void PartsSalesReturnBill_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;
            switch(e.PropertyName) {
                case "WarehouseId":
                    var warehouse = this.KvWarehouses.SingleOrDefault(entity => entity.Key == partsSalesReturnBill.WarehouseId);
                    if(warehouse == null || !(warehouse.UserObject is Warehouse))
                        return;
                    var warehouseInfo = warehouse.UserObject as Warehouse;
                    partsSalesReturnBill.WarehouseId = warehouseInfo.Id;
                    partsSalesReturnBill.WarehouseName = warehouseInfo.Name;
                    partsSalesReturnBill.WarehouseCode = warehouseInfo.Code;
                    break;              
            }
        }

        public ObservableCollection<KeyValuePair> KvWarehouses {
            get {
                return this.kvWarehouses;
            }
        }
        private void CreateUI()
        {
            //this.kvIsPasseds.Add(new KeyValuePair
            //{
            //    Key = 1,
            //    Value = "审核通过"
            //});
            //this.kvIsPasseds.Add(new KeyValuePair
            //{
            //    Key = 0,
            //    Value = "驳回"
            //});
        }
    }
}
