﻿using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;


namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit
{
    public partial class PartsSalesReturnBillForUploadDataEditPanel{
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = {
            "PartsSalesReturn_ReturnType", "PartsSalesReturnBill_InvoiceRequirement","PartsShipping_Method"
        };

        public PartsSalesReturnBillForUploadDataEditPanel()
        {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += PartsSalesReturnBillForApproveDataEditPanel_DataContextChanged;
        }

        private void PartsSalesReturnBillForApproveDataEditPanel_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;          
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.根据销售组织查询仓库Query(partsSalesReturnBill.SalesUnitId).Where(r => r.Type != (int)DcsWarehouseType.虚拟库), LoadBehavior.RefreshCurrent, loadOp => {
                if (loadOp.HasError)
                    return;
                this.KvWarehouses.Clear();
                foreach (var warehouses in loadOp.Entities)
                    this.KvWarehouses.Add(new KeyValuePair {
                        Key = warehouses.Id,
                        Value = warehouses.Name,
                        UserObject = warehouses
                    });
            }, null);
        }    

        public ObservableCollection<KeyValuePair> KvWarehouses {
            get {
                return this.kvWarehouses;
            }
        }      
    }
}
