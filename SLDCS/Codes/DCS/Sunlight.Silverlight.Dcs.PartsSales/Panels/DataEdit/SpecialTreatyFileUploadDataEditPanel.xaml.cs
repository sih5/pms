﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit
{
    public partial class SpecialTreatyFileUploadDataEditPanel : INotifyPropertyChanged
    {
        public static readonly DependencyProperty FilePathesProperty = DependencyProperty.Register("FilePath", typeof(String),
            typeof(SpecialTreatyFileUploadDataEditPanel), new PropertyMetadata(OnFilePathChanged));
        private ObservableCollection<FileEntityes> filePathList = new ObservableCollection<FileEntityes>();
        public event PropertyChangedEventHandler PropertyChanged;
        private string filePath;
        private string strFileName;
        private string fileName;
        private DateTime lastTime = DateTime.Now;
        public ObservableCollection<FileEntityes> FilePathList
        {
            get
            {
                return filePathList;
            }
            set
            {
                this.filePathList = value;
                this.OnPropertyChanged("FilePathList");
            }
        }

        public TextBlock PPanelTitle
        {
            get
            {
                return this.PanelTitle;
            }
        }
        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
        /// <summary>
        /// 为true  隐藏 删除/上传按钮
        /// </summary>
        public bool isVisibility
        {
            set
            {
                if (value)
                {
                    this.dcsbutton.Visibility = Visibility.Collapsed;
                    this.RemoveItems.Visibility = Visibility.Collapsed;
                }
            }
        }

        /// <summary>
        /// 为true  隐藏 删除/上传按钮
        /// </summary>
        public bool isHiddenButtons
        {
            set
            {
                if (value)
                {
                    this.dcsbutton.Visibility = Visibility.Collapsed;
                    this.RemoveItems.Visibility = Visibility.Collapsed;
                }
            }
        }

        public string FilePath
        {
            get
            {
                filePath = "";
                foreach (var filePathLists in FilePathList)
                {
                    filePath += filePathLists.FileName + ":" + filePathLists.FilePath + ":" + filePathLists.FtpServerKey + "?";
                }
                //去掉最后一个 ?
                return filePath == "" ? filePath : filePath.Substring(0, filePath.Length - 1);
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    FilePathList.Clear();
                    return;
                }
                var listPath = value.Split('?');
                FilePathList.Clear();
                foreach (var item in listPath)
                {
                    try
                    {
                        var filePath = item.Split(':');
                        //FilePathList.Add(new FileEntityes(item.Substring(0, item.IndexOf(':')), item.Substring(item.IndexOf(':') + 1, item.Length - item.IndexOf(':') - 1)));

                        var fileEntitye = new FileEntityes();
                        fileEntitye.FileName = filePath[0];
                        fileEntitye.FilePath = filePath[1];
                        //历史数据没有存ftpserver 默认给老ftp服务器的ftpserver
                        fileEntitye.FtpServerKey = filePath.Length == 2 ? "FtpServer" : filePath[2];
                        FilePathList.Add(fileEntitye);
                    }
                    catch (Exception)
                    {
                        continue;
                    }
                }
            }
        }


        private static void OnFilePathChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            var editPanel = (PartsSaleFileUploadDataEditPanel)obj;
            editPanel.FilePathList.Clear();
            if (string.IsNullOrEmpty(editPanel.FilePath))
                return;
            //兼容有用:分隔名称和真实名称类型数据
            //如 test1.jpg:xxx\test1-absdfc-sadfsf-sadfasd-sdafsdf_asdfs_dsf_sdf.jpg?test2.jpg:xxx\test2-absdfc-sadfsf-sadfasd-sdafsdf_asdfs_dsf_sdf.jpg
            foreach (var items in editPanel.FilePath.Split(';', '?').Select(filePathName => filePathName.Split(':')))
            {
                var realPath = items.Count() > 1 ? items[1] : items[0];
                var filePath = Path.GetFileNameWithoutExtension(realPath);
                if (string.IsNullOrEmpty(filePath))
                    return;
                editPanel.FilePathList.Add(
                    new FileEntityes(filePath.Split('_')[0] + Path.GetExtension(realPath), realPath)
                    );
            }
        }
        public SpecialTreatyFileUploadDataEditPanel()
        {
            InitializeComponent();
            IsReadOnly = false;
            this.dcsbutton.Click += dcsbutton_Click;
            this.RemoveItems.Click += deleteButton_Click;
            this.RadUpload.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
            this.RadUpload.FileUploadStarting -= RadUpload_FileUploadStarting;
            this.RadUpload.FileUploadStarting += RadUpload_FileUploadStarting;
            this.RadUpload.UploadFinished -= this.RadUpload_UploadFinished;
            this.RadUpload.UploadFinished += this.RadUpload_UploadFinished;
            this.RadUpload.FilesSelected -= this.RadUpload_FilesSelected;
            this.RadUpload.FilesSelected += this.RadUpload_FilesSelected;
            this.RadUpload.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_PRODUCT_DIR);
            this.RadUpload.FileUploaded += RadUpload_FileUploaded;

        }
     
        private void RadUpload_FileUploaded(object sender, FileUploadedEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.HandlerData.Message))
            {
                UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEditPanel_Validation_UploadError);
            }
            else
            {
                this.filePathList.Add(new FileEntityes(this.fileName, e.HandlerData.CustomData["Path"].ToString(), e.HandlerData.CustomData["FtpServerKey"].ToString()));
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditPanel_Validation_UploadSuccess);
            }
        }

        private void dcsbutton_Click(object sender, RoutedEventArgs e)
        {
            this.RadUpload.ShowFileDialog();
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            var fileEntityes = this.FileContextMenu.SelectedItem as FileEntityes;
            if (fileEntityes == null)
                return;
            filePathList.Remove(fileEntityes);
        }

        private void RadUpload_UploadFinished(object sender, RoutedEventArgs e)
        {
            var upload = sender as RadUpload;
            if (upload != null && upload.CurrentSession != null && upload.CurrentSession.CurrentFile != null)
            {
                upload.CancelUpload();
            }
            //var partsClaimMaterialDetail = this.DataContext as PartsClaimMaterialDetail;
            //if(partsClaimMaterialDetail != null) {
            //    partsClaimMaterialDetail.attachment1 = this.FilePath;
            //}
            Sunlight.Silverlight.ViewModel.ShellViewModel.Current.IsBusy = false;

        }

        private void RadUpload_FilesSelected(object sender, FilesSelectedEventArgs e)
        {
            if (e.SelectedFiles.Any())
                try
                {
                    fileName = e.SelectedFiles[0].Name.Replace("+", "");
                    fileName = fileName.Replace("_", "");
                    fileName = fileName.Replace("%", "");
                    fileName = fileName.Replace("{", "");
                    fileName = fileName.Replace("}", "");
                    strFileName = fileName;
                    //因为在服务端有format此处注释以前上传文件不受影响
                    //strFileName = string.Format("{0}_{1}{2}", Path.GetFileNameWithoutExtension(fileName), Guid.NewGuid(), Path.GetExtension(fileName));
                    if (this.CheckFileName(strFileName))
                    {
                        var upload = sender as RadUpload;
                        upload.CancelUpload();
                    }
                }
                catch (Exception ex)
                {
                    UIHelper.ShowAlertMessage(ex.Message);
                    e.Handled = true;
                }
        }

        private bool CheckFileName(string filePath)
        {
            try
            {
                var strFilePath = this.FilePath;
                if (strFilePath == "")
                {
                    strFilePath = this.FilePath + ":" + filePath;
                }
                else
                {
                    strFilePath = strFilePath + "?" + this.FilePath + ":" + filePath;
                }
                if (string.IsNullOrEmpty(strFilePath))
                {
                    return false;
                }
                var listPath = strFilePath.Split('?');
                foreach (var item in listPath)
                {
                    var filename = item.Substring(0, item.IndexOf(':'));
                    var filepath = item.Substring(item.IndexOf(':') + 1, item.Length - item.IndexOf(':') - 1);
                }
                return false;
            }
            catch (Exception)
            {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditPanel_Validation_UploadSpecial);
                return true;
            }
        }

        private void RadUpload_FileUploadStarting(object sender, FileUploadStartingEventArgs e)
        {
            Sunlight.Silverlight.ViewModel.ShellViewModel.Current.IsBusy = true;
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            UIHelper.ShowNotification(PartsSalesUIStrings.DataEditPanel_Validation_Uploading);//文件正在上传
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //找到文件
            var button = sender as Button;
            if (button == null)
                return;
            var filePaths = button.Tag;
            if (filePaths == null)
                return;
            var filePath = FilePathList.SingleOrDefault(ex => ex.FilePath == (string)filePaths);
            this.FileContextMenu.SelectedItem = filePath;
            if ((DateTime.Now - lastTime).TotalMilliseconds < 300 && !this.IsReadOnly)
            {
                var sFileUrl = DcsUtils.GetDownloadFileUrl((string)filePaths, filePath.FtpServerKey).ToString();
                HtmlPage.Window.Navigate(new Uri(sFileUrl), "_blank");
                EventHandler generateNotificationOpLog = this.GenerateNotificationOpLog;
                if (generateNotificationOpLog != null)
                {
                    generateNotificationOpLog.Invoke(this, EventArgs.Empty);
                }
            }
            lastTime = DateTime.Now;
        }

        /// <summary>
        /// 生成公告操作日志，公告查询节点有此需求
        /// </summary>
        public event EventHandler GenerateNotificationOpLog;

        /// <summary>
        /// 设置控件是否可以下载图片,市场AB质量信息快报管理-服务站 节点有此特殊需求
        /// </summary>
        public bool IsReadOnly
        {
            get;
            set;
        }
    }
}
