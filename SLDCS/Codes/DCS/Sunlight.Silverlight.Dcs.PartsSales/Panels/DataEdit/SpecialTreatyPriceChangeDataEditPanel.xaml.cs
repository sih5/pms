﻿

using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using System;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit {
    public partial class SpecialTreatyPriceChangeDataEditPanel {
        public SpecialTreatyPriceChangeDataEditPanel() {
            InitializeComponent();
            this.Loaded += SpecialTreatyPriceChangeDataEditPanel_Loaded;
            var queryWindowCustomer = DI.GetQueryWindow("CustomerInformationBySparepart");
            queryWindowCustomer.SelectionDecided -= this.QueryWindowCompany_SelectionDecided;
            queryWindowCustomer.SelectionDecided += this.QueryWindowCompany_SelectionDecided;
            this.popupTextBoxDealer.PopupContent = queryWindowCustomer;
            RadComboBoxPartsSalesCategory.SelectionChanged += RadComboBoxPartsSalesCategory_SelectionChanged;
        }

        private void RadComboBoxPartsSalesCategory_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            //var specialTreatyPriceChange = this.DataContext as SpecialTreatyPriceChange;
            //if(specialTreatyPriceChange.SpecialPriceChangeLists.Count > 0)
            //    DcsUtils.Confirm("切换品牌将会清空已添加的清单信息，确定操作码？", () => {
            //        foreach(SpecialPriceChangeList item in specialTreatyPriceChange.SpecialPriceChangeLists)
            //            specialTreatyPriceChange.SpecialPriceChangeLists.Remove(item);
            //    });
        }

        private void QueryWindowCompany_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var CustomerInformation = queryWindow.SelectedEntities.Cast<CustomerInformation>().FirstOrDefault();
            if(CustomerInformation == null)
                return;
            var specialTreatyPriceChange = this.DataContext as SpecialTreatyPriceChange;
            specialTreatyPriceChange.CorporationId = CustomerInformation.CustomerCompany.Id;
            specialTreatyPriceChange.CorporationCode = CustomerInformation.CustomerCompany.Code;
            specialTreatyPriceChange.CorporationName = CustomerInformation.CustomerCompany.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        void SpecialTreatyPriceChangeDataEditPanel_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var domainContext = new DcsDomainContext();
            //if(this.RadComboBoxPartsSalesCategory.Items.Count > 0)
            //    return;
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(r => r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, LoadOperation => {
                if(LoadOperation.HasError)
                    return;
                this.RadComboBoxPartsSalesCategory.ItemsSource = LoadOperation.Entities;
                this.RadComboBoxPartsSalesCategory.SelectedIndex = 0;//默认品牌
            }, null);
        }


    }
}
