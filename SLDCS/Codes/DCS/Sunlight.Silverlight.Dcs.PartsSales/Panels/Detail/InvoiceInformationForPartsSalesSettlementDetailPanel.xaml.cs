﻿using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Detail {
    public partial class InvoiceInformationForPartsSalesSettlementDetailPanel {
        private readonly string[] kvNames = new[] {
            "InvoiceInformation_InvoicePurpose","InvoiceInformation_Type","InvoiceInformation_Status","InvoiceInformation_SourceType"
        };
        public InvoiceInformationForPartsSalesSettlementDetailPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }

        public override string Title {
            get {
                return PartsSalesUIStrings.DetailPanel_Title_InvoiceInformation;
            }
        }

    }
}
