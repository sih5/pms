﻿using System;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Detail {
    public class PartsOutboundBillDetailForDistributeDetailPanel : PartsOutboundBillDetailForDistributeDataGridView, IDetailPanel {
        public string Title {
            get {
                return PartsSalesUIStrings.DataEditPanel_Title_PartsOutboundBillDetail;
            }
        }

        public Uri Icon {
            get {
                return null;
            }
        }
    }
}
