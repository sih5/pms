﻿using System;
using System.Windows;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Detail
{
    public class PartsOutboundBillDetaillForWarehouseDetailPanel : PartsOutboundBillDetaillForWarehouseDataGridView, IDetailPanel
    {

        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }

        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.DomainDataSource.PageSize = 10;
            this.GridView.ShowGroupPanel = false;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
        }

        public Uri Icon
        {
            get
            {
                return null;
            }
        }

        public string Title
        {
            get
            {
                return PartsSalesUIStrings.DetailPanel_Title_PartsOutboundBillDetail;
            }
        }
    }
}
