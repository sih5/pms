﻿
using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Detail {
    public partial class PartsRetailOrderDetailPanel {
        private readonly string[] kvNames = {
            "PayOutBill_PaymentMethod", "PartsRetailOrder_Status"
        };

        public PartsRetailOrderDetailPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }

        public override string Title {
            get {
                return PartsSalesUIStrings.DetailPanel_Title_Common;
            }
        }
    }
}
