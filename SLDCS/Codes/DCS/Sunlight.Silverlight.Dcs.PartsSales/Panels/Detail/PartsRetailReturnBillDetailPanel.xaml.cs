﻿using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Detail {
    public partial class PartsRetailReturnBillDetailPanel {
        public PartsRetailReturnBillDetailPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }

        private readonly string[] kvNames = {
            "PartsRetailReturnBill_Status"
        };

        public override string Title {
            get {
                return PartsSalesUIStrings.DetailPanel_Title_Common;
            }
        }
    }
}
