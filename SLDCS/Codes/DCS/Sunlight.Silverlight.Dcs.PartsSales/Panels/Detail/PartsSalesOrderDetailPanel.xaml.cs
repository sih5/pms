﻿using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Detail {
    public partial class PartsSalesOrderDetailPanel {
        private readonly string[] kvNames = { 
           "PartsSalesOrder_SecondLevelOrderType", "PartsShipping_Method", "PartsSalesOrder_Status","Company_Type"
        };

        public PartsSalesOrderDetailPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }

        public override string Title {
            get {
                return PartsSalesUIStrings.DetailPanel_Title_Common;
            }
        }
    }
}
