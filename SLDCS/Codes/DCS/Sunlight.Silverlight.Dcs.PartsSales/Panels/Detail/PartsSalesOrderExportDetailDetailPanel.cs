﻿using System;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Detail {
    public class PartsSalesOrderExportDetailDetailPanel : PartsSalesOrderExportDetailDataGridView, IDetailPanel {
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        public Uri Icon {
            get {
                return null;
            }
        }

        public string Title {
            get {
                return Utils.GetEntityLocalizedName(typeof(PartsSalesOrder), "PartsSalesOrderDetails");
            }
        }


        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DomainDataSource.PageSize = 10;
            this.GridView.ShowGroupPanel = false;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
        }
    }
}
