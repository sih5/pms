﻿
namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Detail {
    public partial class PartsSalesOrderForApproveDetailPanel {
        private readonly string[] kvNames = new[] {
          "PartsSalesOrder_SecondLevelOrderType"
        };

        public PartsSalesOrderForApproveDetailPanel() {
            InitializeComponent();
            KeyValueManager.Register(this.kvNames);
        }

        public object KvSecondLevelOrderTypes {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

    }
}
