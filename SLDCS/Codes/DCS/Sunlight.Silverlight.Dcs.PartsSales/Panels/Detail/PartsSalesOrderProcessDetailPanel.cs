﻿using System;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Detail {
    public class PartsSalesOrderProcessDetailPanel : PartsSalesOrderProcessDataGridView, IDetailPanel {
        public PartsSalesOrderProcessDetailPanel() {
            this.DataContextChanged += PartsSalesOrderProcessDetailPanel_DataContextChanged;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        public Uri Icon {
            get {
                return null;
            }
        }

        public string Title {
            get {
                return PartsSalesUIStrings.DetailPanel_Title_PartsSalesOrderProcess;
            }
        }

        //写在此处的目的是保证PartsSalesOrderProcessDataGridView被重用后，不需要考虑此处的查询所造成的影响
        private void PartsSalesOrderProcessDetailPanel_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsSalesOrder = e.NewValue as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "OriginalSalesOrderId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo
                };
            this.FilterItem.Value = partsSalesOrder.Id;
            this.ExecuteQueryDelayed();
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DomainDataSource.PageSize = 10;
            this.GridView.ShowGroupPanel = false;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
        }
    }
}
