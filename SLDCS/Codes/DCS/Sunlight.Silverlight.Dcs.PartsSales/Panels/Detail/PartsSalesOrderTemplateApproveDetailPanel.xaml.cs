﻿
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Detail {
    public partial class PartsSalesOrderTemplateApproveDetailPanel {
        private ObservableCollection<PartsSalesOrderType> partsSalesOrderTypes;
        public ObservableCollection<PartsSalesOrderType> PartsSalesOrderTypes {
            get {
                return this.partsSalesOrderTypes ?? (this.partsSalesOrderTypes = new ObservableCollection<PartsSalesOrderType>());
            }
        }

        private DcsDomainContext domainContext = new DcsDomainContext();

        public PartsSalesOrderTemplateApproveDetailPanel() {
            InitializeComponent();
            this.DataContextChanged += PartsSalesOrderTemplateApproveDetailPanel_DataContextChanged;
        }

        void PartsSalesOrderTemplateApproveDetailPanel_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var tabItem = this.Parent as RadTabItem;
            if(tabItem == null)
                return;
            var tabControl = tabItem.Parent as RadTabControl;
            if(tabControl == null)
                return;
            var partsSalesOrder = tabControl.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            this.domainContext.Load(this.domainContext.GetWarehousesOrderByNameQuery().Where(r => r.Id == partsSalesOrder.WarehouseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                if(loadOp.Entities == null)
                    return;
                this.WarehouseComboBox.ItemsSource = loadOp.Entities;
            }, null);
            this.domainContext.Load(this.domainContext.GetPartsSalesOrderTypeByPartsSalesCategoryIdQuery(partsSalesOrder.SalesCategoryId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    if(!loadOp.IsErrorHandled) {
                        loadOp.MarkErrorAsHandled();
                        return;
                    }
                if(loadOp.Entities == null)
                    return;
                foreach(var entity in loadOp.Entities) {
                    this.PartsSalesOrderTypes.Add(entity);
                }
            }, null);
        }
    }
}
