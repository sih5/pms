﻿using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Detail {
    public partial class PartsSalesOrderTraceDetailPanel {
        private readonly string[] kvNames = { 
           "PartsInboundPlan_Status", "PartsShipping_Method", "PartsSalesOrder_Status","Company_Type"
        };

        public PartsSalesOrderTraceDetailPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }

        public override string Title {
            get {
                return PartsSalesUIStrings.DetailPanel_Title_Common;
            }
        }
    }
}
