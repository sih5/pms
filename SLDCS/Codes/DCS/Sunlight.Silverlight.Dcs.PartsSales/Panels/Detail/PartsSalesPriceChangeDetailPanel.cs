﻿
using System.Windows;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Detail {
    public class PartsSalesPriceChangeDetailPanel : PartsSalesPriceChangeDetailDataGridView, IDetailPanel {

        public System.Uri Icon {
            get {
                return null;
            }
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DomainDataSource.PageSize = 10;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
        }
        public string Title {
            get {
                return PartsSalesUIStrings.DetailPanel_Title_PartsSalesPriceChangeDetail;
            }
        }
    }
}
