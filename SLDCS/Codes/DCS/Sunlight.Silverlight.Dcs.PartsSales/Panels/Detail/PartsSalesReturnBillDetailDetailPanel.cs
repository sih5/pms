﻿using System;
using System.Windows;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Detail {
    /// <summary>
    /// 配件销售退货单清单面板
    /// </summary>
    public class PartsSalesReturnBillDetailDetailPanel : PartsSalesReturnBillDetailDataGridView, IDetailPanel {
        public Uri Icon {
            get {
                return null;
            }
        }

        public string Title {
            get {
                return PartsSalesUIStrings.DetailPanel_Title_PartsSalesReturnBillDetail;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DomainDataSource.PageSize = 10;
            this.GridView.ShowGroupPanel = false;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
        }
    }
}
