﻿using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Detail {
    /// <summary>
    /// 配件销售退货单详细面板
    /// </summary>
    public partial class PartsSalesReturnBillDetailPanel {
        private readonly string[] kvNames = {
            "PartsSalesReturnBill_Status", "PartsSalesReturn_ReturnType", "PartsSalesReturnBill_InvoiceRequirement","PartsShipping_Method"
        };

        public override string Title {
            get {
                return PartsSalesUIStrings.DetailPanel_Title_Common;
            }
        }

        public PartsSalesReturnBillDetailPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
