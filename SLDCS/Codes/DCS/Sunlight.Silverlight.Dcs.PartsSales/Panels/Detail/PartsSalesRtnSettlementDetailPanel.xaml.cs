﻿

using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Detail {
    public partial class PartsSalesRtnSettlementDetailPanel {
        private readonly string[] kvNames = {
            "PartsPurchaseRtnSettleBill_InvoicePath","PartsPurchaseSettleBill_SettlementPath","PartsSalesRtnSettlement_Status"
        };
        public PartsSalesRtnSettlementDetailPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }

        public object KvInvoicePaths {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public object KvSettlementPaths {
            get {
                return this.KeyValueManager[this.kvNames[1]];
            }
        }

        public object KvStatus {
            get {
                return this.KeyValueManager[this.kvNames[2]];
            }
        }
        public override string Title {
            get {
                return PartsSalesUIStrings.DetailPanel_Title_Common;
            }
        }

    }
}
