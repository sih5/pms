﻿using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Detail {
    public partial class PartsSalesSettlementDetailPanel {
        private DcsDomainContext domainContext = new DcsDomainContext();
        private readonly string[] kvNames = {
            "PartsPurchaseSettleBill_SettlementPath", "PartsSalesSettlement_Status"
        };

        public PartsSalesSettlementDetailPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += PartsSalesSettlementDetailPanel_DataContextChanged;
        }

        private void PartsSalesSettlementDetailPanel_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsSalesSettlementEx = this.DataContext as PartsSalesSettlementEx;
            if(partsSalesSettlementEx == null)
                return;
            ShellViewModel.Current.IsBusy = true;
            this.domainContext.Load(this.domainContext.查询计划价合计Query(partsSalesSettlementEx.Id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    ShellViewModel.Current.IsBusy = false;
                }
                if(loadOp.Entities != null && loadOp.Entities.Any()) {
                    var entity = loadOp.Entities.First();
                    partsSalesSettlementEx.PlannedPriceTotalAmount = entity.PlannedPriceTotalAmount;
                }
                ShellViewModel.Current.IsBusy = false;
            }, null);
        }

        public override string Title {
            get {
                return PartsSalesUIStrings.DetailPanel_Title_Common;
            }
        }
    }
}
