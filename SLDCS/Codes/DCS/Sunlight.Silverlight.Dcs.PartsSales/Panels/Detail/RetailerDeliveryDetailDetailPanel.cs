﻿using System;
using System.Net;
using System.Windows;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Detail {
    public class RetailerDeliveryDetailDetailPanel : RetailerDeliveryDetailDataGridView, IDetailPanel {
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        public Uri Icon {
            get {
                return null;
            }
        }

        public string Title {
            get {
                return PartsSalesUIStrings.DataEditPanel_Title_RetailerDeliveryDetail;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DomainDataSource.PageSize = 10;
            this.GridView.ShowGroupPanel = false;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
        }
    }
}
