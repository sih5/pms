﻿
using System.Windows;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Detail {
    public class SpecialTreatyPriceChangeDetailPanel : SpecialPriceChangeListDataGridView, IDetailPanel {

        public System.Uri Icon {
            get {
                return null;
            }
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DomainDataSource.PageSize = 10;
            this.GridView.ShowGroupPanel = false;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
        }
        public string Title {
            get {
                return PartsSalesUIStrings.DataEditPanel_Title_SpecialTreatyPriceChangeDetail;
            }
        }
    }
}
