﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class ABCTypeSafeDaysQueryPanel : DcsQueryPanelBase {

        private readonly string[] kvNames = {
            "ABCSetting_Type"
        };
        public ABCTypeSafeDaysQueryPanel() {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.Management_Title_ABCTypeSafeDayQuery,
                    EntityType = typeof(ABCTypeSafeDay),
                    QueryItems = new[] { 
                        new KeyValuesQueryItem {
                           ColumnName = "PRODUCTTYPE",
                           Title=PartsSalesUIStrings.DataGridView_Title_PartsStockCoefficient_ProductType,
                           KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        }
                    }
                }
            };
        }
    }
}
