﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class AgencyLogisticCompanyQueryPanel : DcsQueryPanelBase {

        private readonly string[] kvNames = {
            "BaseData_Status"
        };
        public AgencyLogisticCompanyQueryPanel() {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.QueryPanel_Title_AgencyLogisticCompanyQueryPanel,
                    EntityType = typeof(AgencyLogisticCompany),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                        },
                        new QueryItem {
                            ColumnName = "Name",
                        }, 
                        new KeyValuesQueryItem {
                            ColumnName = "Status",
                            DefaultValue = (int)DcsBaseDataStatus.有效,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            IsEnabled=false,
                            Title=PartsSalesUIStrings.QueryPanel_Title_AgencyLogisticCompany_Status
                        }
                    }
                }
            };
        }
    }
}
