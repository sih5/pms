﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class AgencyRetailerOrderQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "AgencyRetailerOrderStatus"
        };

        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();

        public AgencyRetailerOrderQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status != (int)DcsBaseDataStatus.作废), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.kvPartsSalesCategorys.Clear();
                foreach(var item in loadOp.Entities) {
                    this.kvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.Name
                    });
                }
                domainContext.Load(domainContext.GetWarehousesQuery().Where(entity => entity.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId).OrderBy(t => t.Name), LoadBehavior.RefreshCurrent, loadOp1 => {
                    if(loadOp1.HasError)
                        return;
                    if(loadOp1.Entities == null)
                        return;
                    foreach(var entity in loadOp1.Entities) {
                        this.kvWarehouses.Add(new KeyValuePair {
                            Key = entity.Id,
                            Value = entity.Code
                        });
                    }
                    this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        EntityType = typeof(AgencyRetailerOrder),
                        Title = PartsSalesUIStrings.QueryPanel_Title_PartsRetailOrder,
                        QueryItems = new[] {
                            new QueryItem {
                                ColumnName = "Code"
                            },
                            new KeyValuesQueryItem {
                            ColumnName = "PartsSalesCategoryId",
                            KeyValueItems = this.kvPartsSalesCategorys,
                            Title=PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsRetailOrder_PartsSalesCategoryName
                            },
                            new QueryItem {
                                ColumnName = "ERPSourceOrderCode",
                                Title = PartsSalesUIStrings.QueryPanel_Title_AgencyRetailerOrder_ERPSourceOrderCode
                            },
                            new KeyValuesQueryItem {
                                ColumnName = "WarehouseId",
                                KeyValueItems = this.kvWarehouses,
                                Title = new EntityStrings().AgencyRetailerOrder_WarehouseCode
                            },
                            new QueryItem {
                                ColumnName = "VehiclePartsHandleOrderCode"
                            },
                            new KeyValuesQueryItem {
                                ColumnName = "Status",
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                            },
                            new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                DefaultValue = new[] {
                                     new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                                }
                            }
                        }
                    }
                };
                }, null);
            }, null);
        }
    }
}