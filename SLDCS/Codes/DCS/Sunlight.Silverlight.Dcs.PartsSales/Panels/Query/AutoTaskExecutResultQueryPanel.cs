﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class AutoTaskExecutResultQueryPanel : DcsQueryPanelBase{
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();   
        private readonly string[] kvNames = {
            "ExecutionStatus"
        };
        public AutoTaskExecutResultQueryPanel() {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            //dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    foreach(var partsSalesCategorys in loadOp.Entities)
            //        this.kvPartsSalesCategorys.Add(new KeyValuePair {
            //            Key = partsSalesCategorys.Id,
            //            Value = partsSalesCategorys.Name
            //        });
            //}, null);
            this.QueryItemGroups = new[] { 
               new QueryItemGroup{
                  UniqueId = "Common",
                   Title = PartsSalesUIStrings.QueryPanel_Title_SettlementAutomaticTaskSet,
                   EntityType = typeof(SettlementAutomaticTaskSet),
                   QueryItems = new QueryItem [] {
                      //new KeyValuesQueryItem {
                      //     ColumnName = "BrandId",
                      //     KeyValueItems = this.kvPartsSalesCategorys,
                      //     Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesCategory_Name
                      //},
                      new QueryItem {
                           ColumnName = "CompanyCode",
                           Title = PartsSalesUIStrings.QueryPanel_QueeryItem_Title_Company_Code
                      }, new KeyValuesQueryItem {
                          ColumnName = "ExecutionStatus",
                          Title = PartsSalesUIStrings.QueryPanel_Title_SettlementAutomaticTaskSet_ExecutionStatus,
                          KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                      }, new QueryItem {
                           ColumnName = "CompanyName",
                           Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_Personnel_CorporationName
                      }, new DateTimeRangeQueryItem {
                          ColumnName = "ExecutionTime",
                          Title = PartsSalesUIStrings.QueryPanel_Title_SettlementAutomaticTaskSet_ExecutionTime,
                          DefaultValue = new[] {
                              new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                          }
                      }
                   }
               }
            };
        }
    }
}
