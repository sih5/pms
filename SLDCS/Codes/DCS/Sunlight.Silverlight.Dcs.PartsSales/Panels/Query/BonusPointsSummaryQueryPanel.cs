﻿using System;
using System.Collections.ObjectModel;
using System.Net;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class BonusPointsSummaryQueryPanel : DcsQueryPanelBase {
        //private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
            "BonusPointsSummaryStatus"
        };

        public BonusPointsSummaryQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            //var domainContext = new DcsDomainContext();
            //domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    foreach(var partsSalesCategorys in loadOp.Entities)
            //        this.kvPartsSalesCategorys.Add(new KeyValuePair {
            //            Key = partsSalesCategorys.Id,
            //            Value = partsSalesCategorys.Name
            //        });
                this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        EntityType = typeof(BonusPointsSummary),
                        Title = PartsSalesUIStrings.QueryPanel_Title_BonusPointsOrder,
                        QueryItems = new[] {
                            //new KeyValuesQueryItem {
                            //    ColumnName = "BrandId",
                            //    KeyValueItems =this.kvPartsSalesCategorys,
                            //     Title=PartsSalesUIStrings.DataEditPanel_Text_PartsSalesCategory_Name
                            //},
                            new QueryItem {
                               ColumnName = "BonusPointsSummaryCode",
                               Title="积分汇总单号"
                            },new KeyValuesQueryItem {
                                ColumnName = "Status",
                                 KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                                 Title=PartsSalesUIStrings.QueryPanel_Title_AgencyLogisticCompany_Status
                            },new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesSettlementRef_SourceBillCreateTime
                            },new DateTimeRangeQueryItem {
                                ColumnName = "ApproveTime",
                                Title = PartsSalesUIStrings.DataGridView_Title_PartsPackingPropertyAppQuery_ApproveTime
                            }
                        }
                    }
                };
            //}, null);
        }
    }
}
