﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class CenterABCBasisQueryPanel : DcsQueryPanelBase {

        private readonly string[] kvNames = {
            "ABCSetting_Type","ABCStrategy_Category"
        };
        public CenterABCBasisQueryPanel() {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.Management_Title_CenterABCBasisQuery,
                    EntityType = typeof(CenterABCBasi),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                            ColumnName = "CenterCode",
                            DataType=typeof(string),
                            Title=PartsSalesUIStrings.DataGridView_Title_CenterABCBasis_CenterCode
                        }, new CustomQueryItem {
                            ColumnName = "CenterName",
                            DataType=typeof(string),
                            Title=PartsSalesUIStrings.DataGridView_Title_CenterABCBasis_CenterName
                        }, new CustomQueryItem {
                           ColumnName = "SparePartCode",
                           DataType=typeof(string),
                          Title=PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_VirtualPartsSalesPrice_SparePartCode
                        }, new KeyValuesQueryItem {
                            ColumnName = "OldType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                           Title=PartsSalesUIStrings.DataGridView_Title_CenterABCBasis_OldType
                        }, new KeyValuesQueryItem {
                            ColumnName = "NewType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                           Title=PartsSalesUIStrings.DataGridView_Title_CenterABCBasis_NewType
                        }, new CustomQueryItem {
                           ColumnName = "WeekNum",
                           DataType=typeof(int),
                          Title=PartsSalesUIStrings.DataGridView_Title_CenterABCBasis_WeekNum
                        }
                    }
                }
            };
        }
    }
}
