﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class CheckPartsStockForAgenchQueryPanel : DcsQueryPanelBase {
        public CheckPartsStockForAgenchQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup{
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.QueryPanel_Title_CheckPartsStockForAgench,
                    EntityType = typeof(AgencyStockQryFrmOrder),
                    QueryItems = new[]{
                        new QueryItem{
                            ColumnName = "SparePartCode",
                            IsExact = false,
                            Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode
                        }, new QueryItem{
                            ColumnName = "SparePartName",
                            IsExact = false,
                             Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                        }
                    }
                }
            };
        }
    }
}
