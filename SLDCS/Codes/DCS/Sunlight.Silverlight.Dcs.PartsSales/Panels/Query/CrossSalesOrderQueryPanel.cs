﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System.Collections.Generic;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class CrossSalesOrderQueryPanel : DcsQueryPanelBase {

        private readonly string[] kvNames = {
            "CrossSalesOrderStatus"
        };

        public CrossSalesOrderQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {

            this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = "跨区销售备案管理查询",
                        EntityType = typeof(CrossSalesOrder),
                        QueryItems = new[] {
                            new QueryItem {
                                ColumnName = "Code",
                                Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrder_Code
                            }, new KeyValuesQueryItem {
                                ColumnName = "Status",
                                Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrder_Status,
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]],                                
                            }, new QueryItem {
                                ColumnName = "ApplyCompnayCode",
                                Title = "申请企业编号"
                            } , new QueryItem {
                                ColumnName = "ApplyCompnayName",
                                Title = "申请企业名称"
                            }, new QueryItem {
                                ColumnName = "SubCompanyCode",
                                Title = "隶属企业编号"
                            } , new QueryItem {
                                ColumnName = "SubCompanyName",
                                Title = "隶属企业名称"
                            }, new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                Title="创建时间",
                                DefaultValue = new[] {
                                DateTime.Now.Date.AddDays(-7), DateTime.Now.Date}
                            },new DateTimeRangeQueryItem {
                                 ColumnName = "SubmitTime",
                                 Title="提交时间"
                            } ,new CustomQueryItem {
                                ColumnName = "SparePartCode",
                                DataType = typeof(string),
                                Title =  PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode
                            },new CustomQueryItem {
                                ColumnName = "SparePartName",
                                DataType = typeof(string),
                                Title =  PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                            }
                        }
                    }
                };
        }
    }
}