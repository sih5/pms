﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query
{
    public class CsbomPartsQueryPanel : DcsQueryPanelBase
    {
       
        public CsbomPartsQueryPanel()
        {
            this.Initializer.Register(this.Initialize);
        }
        public void Initialize()
        {
            this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = PartsSalesUIStrings.QueryPanel_Title_PartsPackingPropertyApp,
                        EntityType = typeof(VirtualCsbomParts),
                        QueryItems = new[] {
                            new QueryItem {
                                Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode,
                                ColumnName = "Code"
                            }, new CustomQueryItem {
                                ColumnName = "Name",
                                Title = "CSBOM名称",
                                DataType = typeof(string)
                            }, new CustomQueryItem {
                                ColumnName = "SPMName",
                                Title = "SPM名称",
                                DataType = typeof(string)
                            }, new CustomQueryItem {
                                ColumnName = "EnglishName",
                                Title =  "CSBOM英文名称",
                                DataType = typeof(string)
                            }, new CustomQueryItem {
                                ColumnName = "SPMEnglishName",
                                Title =  "SPM英文名称",
                                DataType = typeof(string)
                            }, new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                Title = "传输时间",
                                DefaultValue = new[] {
                                DateTime.Now.AddDays(-7).Date, DateTime.Now.Date
                                }
                            }
                        }
                    }
                };
        }
    }
}