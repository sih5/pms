﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class CustomerDirectSpareListQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();
        public CustomerDirectSpareListQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if(loadOp.HasError)
                    return;
                this.kvPartsSalesCategoryName.Clear();
                foreach(var partsSalesCategory in loadOp.Entities)
                    this.kvPartsSalesCategoryName.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(VirtualCustomerDirectSpareList),
                    Title = PartsSalesUIStrings.QueryPanel_Title_VirtualCustomerDirectSpareListQuery,
                    QueryItems = new[] {
                        new KeyValuesQueryItem {
                            Title=PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name,
                            ColumnName = "PartsSalesCategoryId",
                            KeyValueItems=this.kvPartsSalesCategoryName
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            Title=PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Status,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsBaseDataStatus.有效
                        },new QueryItem {
                            ColumnName = "CustomerCode",
                            Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDelivery_Shop_Code
                        },new QueryItem {
                            ColumnName = "CustomerName",
                            Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDelivery_Shop_Name
                        }, new QueryItem {
                            ColumnName = "SparePartCode",
                            Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode
                        }, new QueryItem {
                            ColumnName = "SparePartName",
                            Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                        } ,new CustomQueryItem {
                            ColumnName = "IsPrimary",
                            DataType = typeof(bool),
                            Title = PartsSalesUIStrings.QueryPanel_Title_CustomerDirectSpareList_IsPrimary
                        }
                    }
                }
            };
        }
    }
}
