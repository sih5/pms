﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class CustomerInformationForPartsSalesReturnQueryPanel : DcsQueryPanelBase {
        public CustomerInformationForPartsSalesReturnQueryPanel() {
            this.Initializer.Register(this.Initialize);

        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title =PartsSalesUIStrings.QueryPanel_Title_CustomerInformationForPartsSalesReturn,
                    EntityType = typeof(CustomerInformation),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                            ColumnName = "CustomerCompany.Name",
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_CustomerInformation_Name,
                            DataType = typeof(string)
                        },new CustomQueryItem {
                            ColumnName = "CustomerCompany.Code",
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_CustomerInformation_Code,
                            DataType = typeof(string)
                        },new CustomQueryItem {
                            ColumnName = "CutOffDate",
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_CustomerInformation_CutOffDate,
                            DataType=typeof(DateTime),
                            DefaultValue = DateTime.Now
                        }
                    }
                }
            };
        }
    }
}
