﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class CustomerInformationQueryPanel : DcsQueryPanelBase {

        public CustomerInformationQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.QueryPanel_Title_CustomerCompany,
                    EntityType = typeof(CustomerInformation),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_CustomerCompany_Name,
                            ColumnName = "CustomerCompany.Name",
                            DataType = typeof(string)
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            IsExact = false,
                            DefaultValue = new[] {
                                DateTime.Now.Date.AddDays(-7), DateTime.Now.Date
                            }
                        }
                    }
                }
            };
        }
    }
}
