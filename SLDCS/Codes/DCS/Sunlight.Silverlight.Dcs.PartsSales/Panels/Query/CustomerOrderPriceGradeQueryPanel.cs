﻿using System;
using Sunlight.Silverlight.Core.Model;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System.Collections.ObjectModel;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class CustomerOrderPriceGradeQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();     
        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        public CustomerOrderPriceGradeQueryPanel() {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
             var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效&& e.BranchId==BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategorys in loadOp.Entities)
                    this.kvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = partsSalesCategorys.Id,
                        Value = partsSalesCategorys.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.QueryPanel_Title_CustomerOrderPriceGrade,
                    EntityType = typeof(CustomerOrderPriceGrade),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "CustomerCompanyCode"
                        }, new QueryItem {
                            ColumnName = "CustomerCompanyName"
                        }, new QueryItem {
                            ColumnName = "PartsSalesOrderTypeCode",
                            Title = PartsSalesUIStrings.DataPanel_Text_CustomerOrderPriceGrade_PartsSalesOrderTypeCode
                        }, new QueryItem {
                            ColumnName = "PartsSalesOrderTypeName",
                            Title = PartsSalesUIStrings.DataPanel_Text_CustomerOrderPriceGrade_PartsSalesOrderTypeName
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            DefaultValue = (int)DcsBaseDataStatus.有效,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        },new KeyValuesQueryItem {
                            Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesCategory_Name,
                            ColumnName = "PartsSalesCategoryId",
                            KeyValueItems = this.kvPartsSalesCategorys
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                            }
                        }
                    }
                }
            };
        }
    }
}