﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class DealerPartsRetailOrderForApproveQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
            "WorkflowOfSimpleApproval_Status","DealerPartsRetailOrder_RetailOrderType"
        };

        public DealerPartsRetailOrderForApproveQueryPanel() {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategory in loadOp.Entities)
                    this.kvPartsSalesCategoryName.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup{
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.QueryPanel_Title_DealerPartsRetailOrder,
                    EntityType = typeof(DealerPartsRetailOrder),
                    QueryItems = new[]{
                        new QueryItem{
                            ColumnName = "DealerName",
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_DealerName
                        }, new KeyValuesQueryItem {
                            Title= PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_SalesCategoryName,
                            ColumnName = "PartsSalesCategoryId",
                            KeyValueItems = this.kvPartsSalesCategoryName
                        }, new QueryItem{
                            ColumnName = "Code",
                            IsExact = true,
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_Code,
                        }, new KeyValuesQueryItem {
                            ColumnName = "RetailOrderType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            DefaultValue = (int)DcsDealerPartsRetailOrderRetailOrderType.普通客户
                        }, new QueryItem {
                            Title= PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_Customer,
                            ColumnName = "Customer"
                        }, new QueryItem {
                            ColumnName = "SubDealerName" 
                        },  new QueryItem{
                            ColumnName = "DealerCode",
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_DealerCode
                        },  new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsWorkflowOfSimpleApprovalStatus.新建
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                            }
                        }
                    }
                }
            };
        }

    }
}