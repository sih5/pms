﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class DealerPartsSalesPriceForSelectQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvBranch = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategory = new ObservableCollection<KeyValuePair>();
        private readonly DealerPartsSalesPriceForSelectViewModel viewModel = new DealerPartsSalesPriceForSelectViewModel();
        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        public DealerPartsSalesPriceForSelectQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.Initializer.Register(this.viewModel.Initialize);
            this.KeyValueManager.Register(kvNames);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var item in loadOp.Entities)
                    this.kvPartsSalesCategory.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.Name
                    });
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var item in loadOp.Entities)
                    this.kvBranch.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.QueryPanel_Title_DealerPartsSalesPrice,
                    EntityType = typeof(DealerPartsSalesPrice),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "SparePartCode"
                        }, new QueryItem {
                            ColumnName = "SparePartName"
                        },new ComboQueryItem {
                            ColumnName = "BranchId",
                            Title = PartsSalesUIStrings.QueryPanel_Title_BranchName,
                            SelectedValuePath="Id",
                            DisplayMemberPath="Name",
                            ItemsSource=this.viewModel.Branchs,
                            SelectedItemBinding = new Binding("SelectedBranch") {
                                    Source = this.viewModel,
                                    Mode = BindingMode.TwoWay
                                }
                        },new ComboQueryItem{
                            ColumnName="PartsSalesCategoryId",
                           Title=PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsRetailOrder_PartsSalesCategoryName,
                            SelectedValuePath="Id",
                            DisplayMemberPath="Name",
                            ItemsSource=this.viewModel.PartsSalesCategories
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                            }
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsBaseDataStatus.有效
                        }
                    }
                }

            };
        }
    }
    public class DealerPartsSalesPriceForSelectViewModel : ViewModelBase {
        private readonly ObservableCollection<PartsSalesCategory> allPartsSalesCategories = new ObservableCollection<PartsSalesCategory>();
        private readonly ObservableCollection<Branch> branchs = new ObservableCollection<Branch>();
        private PagedCollectionView partsSalesCategories;
        private Branch selectedBranch;

        public ObservableCollection<Branch> Branchs {
            get {
                return this.branchs;
            }
        }

        public Branch SelectedBranch {
            get {
                return this.selectedBranch;
            }
            set {
                if(this.selectedBranch == value)
                    return;
                this.selectedBranch = value;
                this.NotifyOfPropertyChange("Branch");
                this.PartsSalesCategories.Refresh();
            }
        }

        public PagedCollectionView PartsSalesCategories {
            get {
                if(this.partsSalesCategories == null) {
                    this.partsSalesCategories = new PagedCollectionView(this.allPartsSalesCategories);
                    this.partsSalesCategories.Filter = o => ((PartsSalesCategory)o).BranchId == (this.SelectedBranch != null ? this.SelectedBranch.Id : int.MinValue);
                }
                return this.partsSalesCategories;
            }
        }

        public void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var entity in loadOp.Entities)
                    this.allPartsSalesCategories.Add(entity);
            }, null);

            domainContext.Load(domainContext.GetDealerServiceInfoesQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.Select(e => e.BranchId).ToArray();
                if(entity != null) {
                    domainContext.Load(domainContext.GetBranchesByIdsQuery(entity).Where(e => e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp1 => {
                        if(loadOp1.HasError)
                            return;
                        foreach(var branch in loadOp1.Entities)
                            this.branchs.Add(branch);
                    }, null);
                }
            }, null);
        }

        public override void Validate() {
            //
        }
    }
}
