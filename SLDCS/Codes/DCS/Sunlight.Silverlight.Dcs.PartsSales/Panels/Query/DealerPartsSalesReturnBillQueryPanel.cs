﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class DealerPartsSalesReturnBillQueryPanel : DcsQueryPanelBase {
        public DealerPartsSalesReturnBillQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.QueryPanel_Title_PartsRetailReturnBill,
                    EntityType = typeof(DealerPartsSalesReturnBill),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Customer",
                            Title=PartsSalesUIStrings.DataEditView_DealerPartsRetailOrder_Customer
                        }, new QueryItem {
                            ColumnName = "Code",
                            Title=PartsSalesUIStrings.QueryPanel_Title_DealerPartsSalesReturnBill_Code
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesSettlementRef_SourceBillCreateTime,
                            DefaultValue = new[] {
                                DateTime.Now.Date.AddDays(-7), DateTime.Now.Date
                            }
                        }
                    }
                }
            };
        }
    }
}
