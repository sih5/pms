﻿
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class DealerPartsStockForSelectQueryPanel : DcsQueryPanelBase {
        public DealerPartsStockForSelectQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(kvNames);
        }
        private readonly string[] kvNames = { "Company_Type" };

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup{
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.QueryPanel_Title_DealerPartsStockForSelect,
                    EntityType = typeof(VirtualDealerPartsStock),
                    QueryItems = new[]{
                        new QueryItem{
                            ColumnName = "DealerName",
                            Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_Personnel_CorporationName
                        }, new KeyValuesQueryItem{
                            ColumnName = "CompanyType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title=PartsSalesUIStrings.QueryPanel_Title_VirtualDealerPartsStock_CompanyType
                        }
                        ,new CustomControlQueryItem { 
                            ColumnName = "SparePartCode", 
                            Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode, 
                            CreateControl=type =>new Controls.MultipleTextQueryControl(type,"SparePartCode") 
                        }
                        , new QueryItem{
                            ColumnName = "SparePartName",
                            Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                        }
                        //, new KeyValuesQueryItem {
                        //    ColumnName = "PriceType",
                        //    KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        //    DefaultValue = (int)DcsPartsSalesPricePriceType.基准销售价,
                        //    Title=PartsSalesUIStrings.QueryPanel_QueryItem_Title_VirtualDealerPartsStock_PriceType
                        //}
                        , new CustomQueryItem {
                            ColumnName = "IsDealerQuery",
                            DataType = typeof(bool),
                            Title = PartsSalesUIStrings.QueryPanel_Title_VirtualDealerPartsStock_IsDealerQuery,
                            DefaultValue = false
                        }, new CustomQueryItem {
                            ColumnName = "IsZero",
                            DataType = typeof(bool),
                            Title = PartsSalesUIStrings.DataQueryPanel_IsZero
                        }, new QueryItem{
                            ColumnName = "MarketingDepartmentName",
                            Title=PartsSalesUIStrings.DataGridView_Column_Text_MktName
                        }
                    }
                }
            };
        }
    }
}
