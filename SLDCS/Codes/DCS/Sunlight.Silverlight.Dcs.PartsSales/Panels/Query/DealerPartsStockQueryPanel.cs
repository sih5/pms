﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class DealerPartsStockQueryPanel : DcsQueryPanelBase {
        public DealerPartsStockQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategory = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvDealer = new ObservableCollection<KeyValuePair>();


        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategory in loadOp.Entities)
                    this.kvPartsSalesCategory.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
            }, null);

            dcsDomainContext.Load(dcsDomainContext.GetDealersQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var dealer in loadOp.Entities)
                    this.kvDealer.Add(new KeyValuePair {
                        Key = dealer.Id,
                        Value = dealer.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.QueryPanel_Title_DealerPartsStock,
                    EntityType = typeof(DealerPartsStockForBussiness),
                    QueryItems = new[] {
                        new KeyValuesQueryItem {
                            ColumnName = "DealerId",
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_DealerPartsStock_DealerName,
                            KeyValueItems = this.kvDealer
                        },
                        new KeyValuesQueryItem {
                            ColumnName = "SalesCategoryId",
                            KeyValueItems = this.kvPartsSalesCategory,
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_SalesCategoryName
                        },
                        new QueryItem {
                            ColumnName = "SparePartCode",
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_SparePartCode,
                            IsExact = false
                        },
                        new QueryItem {
                            ColumnName = "SparePartName",
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_SparePartName,
                            IsExact = false
                        }
                    }
                }
            };
        }
    }
}
