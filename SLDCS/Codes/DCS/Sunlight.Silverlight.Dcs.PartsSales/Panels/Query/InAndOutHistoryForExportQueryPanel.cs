﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class InAndOutHistoryForExportQueryPanel : DcsQueryPanelBase {
        public InAndOutHistoryForExportQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(DealerPartsStockOutInRecord),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "code",
                            Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesRtnSettlementRef_SourceCode
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                              Title = PartsSalesUIStrings.QueryPanel_Title_DealerPartsStockOutInRecord_CreateTime
                        }
                    }
                }
            };
        }
    }
}
