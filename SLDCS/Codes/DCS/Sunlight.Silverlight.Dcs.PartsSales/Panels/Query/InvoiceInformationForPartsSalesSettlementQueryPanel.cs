﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class InvoiceInformationForPartsSalesSettlementQueryPanel : DcsQueryPanelBase {

        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = new[] {
            "InvoiceInformation_Status"
        };

        public InvoiceInformationForPartsSalesSettlementQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategory in loadOp.Entities)
                    this.kvPartsSalesCategoryName.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(VirtualInvoiceInformation),
                    Title = PartsSalesUIStrings.QueryPanel_Title_InvoiceInformationForPartsSalesSettlement,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "InvoiceNumber",
                            Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "InvoiceNumber")
                        }, new QueryItem {
                            ColumnName = "SourceCode",
                            Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "SourceCode")
                        }, new QueryItem {
                            ColumnName = "InvoiceCompanyCode",
                            Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "InvoiceCompanyCode")
                        }, new QueryItem {
                            ColumnName = "InvoiceCompanyName",
                            Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "InvoiceCompanyName")
                        }, new QueryItem {
                            ColumnName = "InvoiceReceiveCompanyCode",
                            Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "InvoiceReceiveCompanyCode")
                        }, new QueryItem {
                            ColumnName = "InvoiceReceiveCompanyName",
                            Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "InvoiceReceiveCompanyName")
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "CreateTime")
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "InvoiceDate",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                            },
                            Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "InvoiceDate")
                        },new KeyValuesQueryItem {
                            Title=PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name,
                            ColumnName = "PartsSalesCategoryId",
                            KeyValueItems=this.kvPartsSalesCategoryName
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[kvNames[0]],
                            DefaultValue = (int)DcsInvoiceInformationStatus.已开票,
                            Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "Status")
                        },  new QueryItem {
                            ColumnName = "BusinessCode",
                            Title=PartsSalesUIStrings.QueryPanel_Title_VirtualInvoiceInformation_BusinessCode
                        },new QueryItem {
                            ColumnName = "CreatorName",
                            Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "CreatorName")
                        }

                    }
                }
            };
        }
    }
}
