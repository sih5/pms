﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class OptPartsSalesOrderQueryPanel : DcsQueryPanelBase {
        private ObservableCollection<KeyValuePair> kvPartsSalesOrderTypes;
        private ObservableCollection<KeyValuePair> kvPartsSalesCategorys;
        private ObservableCollection<KeyValuePair> kvSalesUints;

        private readonly string[] kvNames = {
            "PartsSalesOrder_Status", "PartsSalesOrder_SecondLevelOrderType"
        };

        public OptPartsSalesOrderQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private ObservableCollection<KeyValuePair> KvPartsSalesOrderTypes {
            get {
                return this.kvPartsSalesOrderTypes ?? (this.kvPartsSalesOrderTypes = new ObservableCollection<KeyValuePair>());
            }
        }
        private ObservableCollection<KeyValuePair> KvPartsSalesCategorys {
            get {
                return this.kvPartsSalesCategorys ?? (this.kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>());
            }
        }
        private ObservableCollection<KeyValuePair> KvSalesUints {
            get {
                return this.kvSalesUints ?? (this.kvSalesUints = new ObservableCollection<KeyValuePair>());
            }
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesOrderTypesQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var entity in loadOp.Entities) {
                    this.KvPartsSalesOrderTypes.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name
                    });
                }
            }, null);
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery(), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var categoryName in loadOp.Entities)
                    this.KvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = categoryName.Id,
                        Value = categoryName.Name,
                    });

            }, null);
            domainContext.Load(domainContext.GetSalesUnitsQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var entity in loadOp.Entities)
                    this.KvSalesUints.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name
                    });

            }, null);
            this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesOrder,
                        EntityType = typeof(PartsSalesOrder),
                        QueryItems = new QueryItem[] {
                            new KeyValuesQueryItem {
                               Title = Utils.GetEntityLocalizedName(typeof(PartsSalesOrder), "SalesUnitName"),
                               ColumnName = "SalesUnitId",
                            KeyValueItems = this.KvSalesUints
                            }, new KeyValuesQueryItem {
                                Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrder_PartsSalesOrderTypeName,
                                ColumnName = "PartsSalesOrderTypeId",
                                KeyValueItems = this.KvPartsSalesOrderTypes
                            },new KeyValuesQueryItem{
                                ColumnName="SalesCategoryId",
                                Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsRetailOrder_SalesCategoryName,
                                KeyValueItems=this.KvSalesUints
                            }, new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                DefaultValue = new[] {
                                    DateTime.Now.AddDays(-7), DateTime.Now
                                }
                            }, new KeyValuesQueryItem {
                                ColumnName = "Status",
                                Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrder_Status,
                                DefaultValue = (int)DcsPartsSalesOrderStatus.提交,
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                            }, new CustomQueryItem {
                                IsExact = false,
                                ColumnName = "IfDirectProvision",
                                DataType = typeof(bool),
                          }
                     }
                 }
            };
        }
    }
}
