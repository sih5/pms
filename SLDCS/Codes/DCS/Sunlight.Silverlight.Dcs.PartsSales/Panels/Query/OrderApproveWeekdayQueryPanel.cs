﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System;
namespace Sunlight.Silverlight.Dcs.PartsSales
    .Panels.Query {
    public class OrderApproveWeekdayQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "BaseData_Status","OrderAppDate"
        };
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();
        public OrderApproveWeekdayQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if(loadOp.HasError)
                    return;
                this.kvPartsSalesCategoryName.Clear();
                foreach(var partsSalesCategory in loadOp.Entities)
                    this.kvPartsSalesCategoryName.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(OrderApproveWeekday),
                    Title = PartsSalesUIStrings.QueryPanel_Title_SalesUnit,
                    QueryItems = new QueryItem[] {
                         new KeyValuesQueryItem {
                            ColumnName = "WeeksName",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        },new KeyValuesQueryItem {
                            Title=PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name,
                            ColumnName = "PartsSalesCategoryId",
                            KeyValueItems=this.kvPartsSalesCategoryName
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "ModifyTime"
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            DefaultValue = (int)DcsBaseDataStatus.有效,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }
                    }
                }
            };
        }
    }
}
