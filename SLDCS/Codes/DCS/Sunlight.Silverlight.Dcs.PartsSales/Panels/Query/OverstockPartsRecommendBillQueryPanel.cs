﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class OverstockPartsRecommendBillQueryPanel : DcsQueryPanelBase {
        public OverstockPartsRecommendBillQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.QueryPanel_Title_OverstockPartsRecommendBill,
                    EntityType = typeof(OverstockPartsRecommendBill),
                    QueryItems = new[] {
                        new CustomQueryItem{
                            ColumnName = "DealerCode",
                            DataType=typeof(string),
                            Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_DealerCode
                        },new CustomQueryItem{
                            ColumnName = "DealerName",
                            DataType=typeof(string),
                            Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_DealerName
                        }, new QueryItem{
                            ColumnName = "SparePartCode",
                            Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_SparePartCode,
                        }, new QueryItem{
                            ColumnName = "SparePartName",
                            Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_SparePartName,
                        },new QueryItem{
                            ColumnName = "CenterName",
                            Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_CenterName
                        },new DateTimeRangeQueryItem{
                            ColumnName = "CreateTime",
                            Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_CreateTime,
                            DefaultValue = new[]{
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                            }
                        }
                    }
                }
            };
        }
    }
}
