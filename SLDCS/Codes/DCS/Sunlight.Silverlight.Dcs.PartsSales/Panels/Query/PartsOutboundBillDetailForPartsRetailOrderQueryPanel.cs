﻿
using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class PartsOutboundBillDetailForPartsRetailOrderQueryPanel : DcsQueryPanelBase {

        public PartsOutboundBillDetailForPartsRetailOrderQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(PartsRetailOrder),
                    Title = PartsSalesUIStrings.QueryPanel_Title_PartsOutboundBillDetailForPartsRetailOrder,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_PartsRetailOrder_Code
                        }, new QueryItem {
                            ColumnName = "CustomerName"
                        }, new QueryItem {
                            ColumnName = "CustomerCellPhone"
                        }, new CustomQueryItem {
                            ColumnName = "SparePartCode",
                            Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode,
                            DataType = typeof(string)
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            IsExact = false,
                             DefaultValue = new[] {
                                DateTime.Now.Date.AddMonths(-1), DateTime.Now
                            }
                        }
                    }
                }
            };
        }
    }
}
