﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class PartsOutboundBillDetailForPartsSalesOrderQueryPanel : DcsQueryPanelBase {
        //private readonly string[] kvNames = new[] {
        //    "PartsSalesOrder_SecondLevelOrderType"
        //};

        private readonly ObservableCollection<KeyValuePair> kvPartsSalesOrderTypes = new ObservableCollection<KeyValuePair>();

        public PartsOutboundBillDetailForPartsSalesOrderQueryPanel() {
            // this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);

        }

        private DcsDomainContext dcsDomainContext;
        private DcsDomainContext DcsDomainContext {
            get {
                return this.dcsDomainContext ?? (this.dcsDomainContext = new DcsDomainContext());
            }
        }

        public override object ExchangeData(Core.View.IBaseView sender, string subject, params object[] contents) {
            this.DcsDomainContext.Load(DcsDomainContext.GetPartsSalesOrderTypesQuery().Where(t => t.Status == (int)DcsBaseDataStatus.有效 && t.PartsSalesCategoryId == (int)contents[0]), loadOp => {
                if(loadOp.HasError)
                    return;
                this.kvPartsSalesOrderTypes.Clear();
                foreach(var partsSalesOrderType in loadOp.Entities)
                    this.kvPartsSalesOrderTypes.Add(new KeyValuePair {
                        Key = partsSalesOrderType.Id,
                        Value = partsSalesOrderType.Name
                    });
            }, null);
            return null;
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.QueryPanel_Title_PartsOutboundBillDetailForPartsSalesOrder,
                    EntityType = typeof(PartsSalesOrder),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title = PartsSalesUIStrings.QueryPanel_QueeryItem_Title_PartsSalesOrder_Code,
                            IsExact = false
                        }, new KeyValuesQueryItem {
                            ColumnName = "PartsSalesOrderTypeId",
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrderForQuery_PartsSalesOrderTypeId,
                            KeyValueItems = this.kvPartsSalesOrderTypes
                        } , new QueryItem {
                            ColumnName = "ERPSourceOrderCode",
                            Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDelivery_PlatForm_Code                                                                                              
                        },  new QueryItem {
                            ColumnName = "ContactPerson",
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrder_ContactPerson,
                            IsExact = false
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.AddDays(1 - DateTime.Now.Day).Year,DateTime.Now.AddDays(1 - DateTime.Now.Day).Month,DateTime.Now.AddDays(1 - DateTime.Now.Day).Day,0,0,0), new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59)}       
                        }, new QueryItem {
                            ColumnName = "ContactPhone",
                            Title = PartsSalesUIStrings.QueryPanel_QueeryItem_Title_PartsSalesOrder_ContactPhone,
                            IsExact = false                                       
                      }
                    }
                }
            };
        }
    }
}
