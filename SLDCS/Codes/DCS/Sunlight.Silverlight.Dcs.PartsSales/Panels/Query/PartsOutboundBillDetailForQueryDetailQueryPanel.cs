﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class PartsOutboundBillDetailForQueryDetailQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesOrderTypes = new ObservableCollection<KeyValuePair>();

        public PartsOutboundBillDetailForQueryDetailQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private DcsDomainContext dcsDomainContext;
        private DcsDomainContext DcsDomainContext {
            get {
                return this.dcsDomainContext ?? (this.dcsDomainContext = new DcsDomainContext());
            }
        }

        public override object ExchangeData(Core.View.IBaseView sender, string subject, params object[] contents) {
            this.DcsDomainContext.Load(DcsDomainContext.GetPartsSalesOrderTypesQuery().Where(t => t.Status == (int)DcsBaseDataStatus.有效 && t.PartsSalesCategoryId == (int)contents[0]), loadOp => {
                if(loadOp.HasError)
                    return;
                this.kvPartsSalesOrderTypes.Clear();
                foreach(var partsSalesOrderType in loadOp.Entities)
                    this.kvPartsSalesOrderTypes.Add(new KeyValuePair {
                        Key = partsSalesOrderType.Id,
                        Value = partsSalesOrderType.Name
                    });
            }, null);
            return null;
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.QueryPanel_Title_PartsOutboundBillDetailForPartsSalesOrder,
                    EntityType = typeof(VirtualPartsOutboundBillDetail),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "PartsSalesOrderCode",
                            Title = PartsSalesUIStrings.QueryPanel_QueeryItem_Title_PartsSalesOrder_Code,
                            IsExact = false
                        }, new KeyValuesQueryItem {
                            ColumnName = "PartsSalesOrderTypeId",
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrderForQuery_PartsSalesOrderTypeId,
                            KeyValueItems = this.kvPartsSalesOrderTypes
                        } , new QueryItem {
                            ColumnName = "SparePartCode",
                            Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode                                                                                             
                        },  new QueryItem {
                            ColumnName = "SparePartName",
                            Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName,
                            IsExact = false
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesSettlementRef_SourceBillCreateTime,
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.AddDays(1 - DateTime.Now.Day).Year,DateTime.Now.AddDays(1 - DateTime.Now.Day).Month,DateTime.Now.AddDays(1 - DateTime.Now.Day).Day,0,0,0), new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59)}       
                        }, new QueryItem {
                            ColumnName = "IfDirectProvision",
                            Title = PartsSalesUIStrings.DetailPanel_Text_PartsSalesOrder_IfDirectProvision,
                            IsExact = false                                       
                      }
                    }
                }
            };
        }
    }
}
