﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query
{
    public class PartsPackingPropertyAppQueryPanel : DcsQueryPanelBase
    {
        private readonly string[] kvNames = {
           "PackingPropertyAppStatus", "PackingUnitType"
        };
       
        public PartsPackingPropertyAppQueryPanel()
        {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
        public void Initialize()
        {
            this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = PartsSalesUIStrings.QueryPanel_Title_PartsPackingPropertyApp,
                        EntityType = typeof(PartsPackingPropertyApp),
                        QueryItems = new[] {
                            new QueryItem {
                                Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesRtnSettlementRef_SourceCode,
                                ColumnName = "Code"
                            }, new KeyValuesQueryItem {
                                ColumnName = "MainPackingType",
                                KeyValueItems = this.KeyValueManager[this.kvNames[1]],    
                                Title = PartsSalesUIStrings.QueryPanel_Title_PartsPackingPropertyAppQuery_MainPackingType                             
                            }, new QueryItem {
                                ColumnName = "SpareCode",
                                 Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode,
                            }, new QueryItem {
                                ColumnName = "SpareName",
                                Title =  PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                            }, new KeyValuesQueryItem {
                                ColumnName = "Status",
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]]                           
                            }, new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                DefaultValue = new[] {
                                DateTime.Now.AddDays(1 - DateTime.Now.Day).Date, DateTime.Now.Date
                                }
                            
                            }
                        }
                    }
                };
        }
    }
}