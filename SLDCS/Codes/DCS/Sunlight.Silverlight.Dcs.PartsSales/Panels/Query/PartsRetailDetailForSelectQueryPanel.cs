﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class PartsRetailDetailForSelectQueryPanel : DcsQueryPanelBase {

        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        public PartsRetailDetailForSelectQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetWarehousesOrderByNameQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehouse in loadOp.Entities) {
                    kvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
                }
            }, null);

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(WarehousePartsStock),
                    Title = PartsSalesUIStrings.QueryPanel_Title_WarehousePartsStock,
                    QueryItems = new[] {
                         new QueryItem {
                            ColumnName = "SparePartCode",
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_WarehousePartsStock_SparePartCode
                        }, new QueryItem {
                            ColumnName = "SparePartName",
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_WarehousePartsStock_SparePartName
                        },new KeyValuesQueryItem {
                            ColumnName = "WarehouseId",
                             KeyValueItems = this.kvWarehouses,
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_WarehousePartsStock_WarehouseId
                        }
                    }
                }
            };
        }
    }
}
