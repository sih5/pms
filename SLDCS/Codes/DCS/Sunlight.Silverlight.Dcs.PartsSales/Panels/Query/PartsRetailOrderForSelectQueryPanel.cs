﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class PartsRetailOrderForSelectQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "PartsRetailOrder_Status"
        };

        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();

        public PartsRetailOrderForSelectQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetWarehousesQuery().Where(entity => entity.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId).OrderBy(t => t.Name), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Entities == null)
                    return;
                foreach(var entity in loadOp.Entities) {
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name
                    });
                }
                this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        EntityType = typeof(PartsRetailOrder),
                        Title = PartsSalesUIStrings.QueryPanel_Title_PartsRetailOrder,
                        QueryItems = new[] {
                            new QueryItem {
                                ColumnName = "CustomerName"
                            },
                            new KeyValuesQueryItem {
                                ColumnName = "WarehouseId",
                                KeyValueItems = this.kvWarehouses,
                                Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsRetailOrder_Warehouse
                            },
                            new QueryItem {
                                ColumnName = "CustomerCellPhone"
                            },
                            //new KeyValuesQueryItem {
                            //    ColumnName = "Status",
                            //    KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                            //},
                            new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                DefaultValue = new[] {
                                    new DateTime(DateTime.Now.AddMonths(-1).Year, DateTime.Now.AddMonths(-1).Month, DateTime.Now.AddMonths(-1).Day), DateTime.Now.Date
                                }
                            }
                        }
                    }
                };
            }, null);
        }
    }
}
