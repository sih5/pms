﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class PartsRetailReturnBillQueryPanel : DcsQueryPanelBase {
        public PartsRetailReturnBillQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.QueryPanel_Title_PartsRetailReturnBill,
                    EntityType = typeof(PartsRetailReturnBill),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "CustomerName"
                        }, new QueryItem {
                            ColumnName = "CustomerCellPhone"
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                DateTime.Now.Date.AddDays(-7), DateTime.Now.Date
                            }
                        }
                    }
                }
            };
        }
    }
}
