﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class PartsSalePriceIncreaseRateQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        public PartsSalePriceIncreaseRateQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(kvNames);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalePriceIncreaseRateQuery,
                    EntityType = typeof(PartsSalePriceIncreaseRate),
                    QueryItems = new QueryItem[] {
                        new QueryItem {
                            ColumnName = "GroupCode"
                        },
                        new QueryItem {
                            ColumnName = "GroupName"
                        },
                        new KeyValuesQueryItem {
                            ColumnName = "status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsBaseDataStatus.有效,
                            Title = PartsSalesUIStrings.QueryPanel_Title_AgencyLogisticCompany_Status
                        },
                        new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        }
                    }
                }
            };
        }
    }
}
