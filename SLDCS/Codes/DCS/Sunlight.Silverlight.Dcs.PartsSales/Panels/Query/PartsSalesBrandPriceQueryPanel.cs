﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class PartsSalesBrandPriceQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "BaseData_Status", "PartsSalesPrice_PriceType"
        };
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();

        public PartsSalesBrandPriceQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(kvNames);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategory in loadOp.Entities)
                    this.kvPartsSalesCategoryName.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.QueryPanel_Title_GetAllPartsSalesPriceQuery,
                    EntityType = typeof(GetAllPartsSalesPrice),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "SparePartCode",
                            Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode
                        }, new QueryItem {
                            ColumnName = "SparePartName",
                            Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                        },new ComboQueryItem {
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name,
                            ColumnName = "PartsSalesCategoryName",
                            ItemsSource = this.kvPartsSalesCategoryName,                           
                            DisplayMemberPath = "Value",
                            SelectedValuePath = "Value"
                        }, new KeyValuesQueryItem {
                            Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesPriceChange_PriceType,
                            ColumnName = "PriceType",
                            DefaultValue = (int)DcsPartsSalesPricePriceType.基准销售价,
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            DefaultValue = (int)DcsBaseDataStatus.有效,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new CustomQueryItem {
                            Title =  PartsSalesUIStrings.QueryPanel_QueryItem_Title_isSale,
                            ColumnName = "isSalable",
                            DataType = typeof(bool),
                            DefaultValue = null
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        }, new CustomQueryItem {
                            Title =  PartsSalesUIStrings.QueryPanel_QueryItem_Title_isOrderable,
                            ColumnName = "isOrderable",
                            DataType = typeof(bool),
                            DefaultValue = null
                        }
                    }
                }
            };
        }
    }
}
