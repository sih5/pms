﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class PartsSalesOrderForExportQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
            "PartsSalesOrder_Status", "PartsSalesOrder_SecondLevelOrderType"
        };

        public PartsSalesOrderForExportQueryPanel() {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
            this.Initializer.Register(this.viewModel.Initialize);
        }
        private readonly PartsSalesOrderForExportQueryPanelViewModel viewModel = new PartsSalesOrderForExportQueryPanelViewModel();
        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.查询销售组织仓库Query(BaseApp.Current.CurrentUserData.EnterpriseId, null, null, null, null), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                var wareHouses = loadOp.Entities.OrderBy(r => r.Warehouse.Name);
                foreach(var warehouse in wareHouses) {
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.WarehouseId,
                        Value = warehouse.Warehouse.Name
                    });
                }
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesOrderExport,
                    EntityType = typeof(PartsSalesOrder),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "ContractCode",
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrder_ContractCode
                        }
                        //,new ComboQueryItem {
                        //    Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_SalesCategoryName,
                        //    ColumnName = "SalesCategoryId",
                        //    SelectedValuePath = "Id",
                        //    DisplayMemberPath = "Name",
                        //    ItemsSource = this.viewModel.SalesCategories,
                        //    SelectedItemBinding = new Binding("SelectedSalesCategory") {
                        //        Source = this.viewModel,
                        //        Mode = BindingMode.TwoWay
                        //    }
                        //}
                        ,new KeyValuesQueryItem {
                            ColumnName = "Status",
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrder_Status,
                            DefaultValue = (int)DcsPartsSalesOrderStatus.新增,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }
                        ,new QueryItem {
                            ColumnName = "ReceivingCompanyCode",
                            Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesOrderQuery_ReceivingCompanyCode
                        },new QueryItem {
                            ColumnName = "ReceivingCompanyName",
                            Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesOrderQuery_ReceivingCompanyName
                        },new ComboQueryItem {
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrder_PartsSalesOrderType,
                            ColumnName = "PartsSalesOrderTypeId",
                            SelectedValuePath = "Id",
                            DisplayMemberPath = "Name",
                            ItemsSource = this.viewModel.SalesOrderTypes
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                DateTime.Now.Date.AddDays(-7), DateTime.Now.Date
                            }
                        },new QueryItem {
                            ColumnName = "Code",
                            Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesRtnSettlementRef_SourceCode
                        },new CustomQueryItem {
                            ColumnName = "SparePartCode",
                            DataType = typeof(string),
                            Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode
                        }
                    }
                }
            };
        }
    }
}

public class PartsSalesOrderForExportQueryPanelViewModel : ViewModelBase {
    private readonly ObservableCollection<PartsSalesCategory> partsSalesCategories = new ObservableCollection<PartsSalesCategory>();
    private readonly ObservableCollection<PartsSalesOrderType> allPartsSalesOrderTypes = new ObservableCollection<PartsSalesOrderType>();
    private PagedCollectionView partsSalesOrderTypes;
    private PartsSalesCategory selectedSalesCategory;

    public ObservableCollection<PartsSalesCategory> SalesCategories {
        get {
            return this.partsSalesCategories;
        }
    }

    public PagedCollectionView SalesOrderTypes {
        get {
            if(this.partsSalesOrderTypes == null) {
                this.partsSalesOrderTypes = new PagedCollectionView(this.allPartsSalesOrderTypes);
                this.partsSalesOrderTypes.Filter = o => ((PartsSalesOrderType)o).PartsSalesCategoryId == (this.SelectedSalesCategory != null ? this.SelectedSalesCategory.Id : 221);
            }
            return this.partsSalesOrderTypes;
        }
    }

    public PartsSalesCategory SelectedSalesCategory {
        get {
            return this.selectedSalesCategory;
        }
        set {
            if(this.selectedSalesCategory == value)
                return;
            this.selectedSalesCategory = value;
            this.NotifyOfPropertyChange("PartsSalesCategory");
            this.SalesOrderTypes.Refresh();
        }
    }

    public override void Validate() {
        //
    }

    public void Initialize() {
        var domainContext = new DcsDomainContext();
        domainContext.Load(domainContext.GetPartsSalesOrderTypeByQuery(), LoadBehavior.RefreshCurrent, loadOp => {
            if(loadOp.HasError)
                return;
            foreach(var entity in loadOp.Entities)
                this.allPartsSalesOrderTypes.Add(entity);
        }, null);

        //domainContext.Load(domainContext.GetPartsSalesCategoriesQuery(), LoadBehavior.RefreshCurrent, loadOp => {
        //    if(loadOp.HasError)
        //        return;
        //    foreach(var partsSaleCategory in loadOp.Entities)
        //        this.partsSalesCategories.Add(partsSaleCategory);
        //}, null);
    }
}