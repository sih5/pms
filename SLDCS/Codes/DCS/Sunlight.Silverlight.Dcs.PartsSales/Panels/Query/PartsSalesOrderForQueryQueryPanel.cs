﻿
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class PartsSalesOrderForQueryQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvSalesUnits = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesOrderTypes = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = {
            "PartsSalesOrder_Status"
        };

        public PartsSalesOrderForQueryQueryPanel() {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetSalesUnitsQuery().Where(e => e.OwnerCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var salesUnit in loadOp.Entities)
                    this.kvSalesUnits.Add(new KeyValuePair {
                        Key = salesUnit.Id,
                        Value = salesUnit.Name
                    });
            }, null);

            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesOrderTypesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesOrderType in loadOp.Entities)
                    this.kvPartsSalesOrderTypes.Add(new KeyValuePair {
                        Key = partsSalesOrderType.Id,
                        Value = partsSalesOrderType.Name
                    });
            }, null);

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesOrder,
                    EntityType = typeof(PartsSalesOrder),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "SubmitCompanyName",
                            IsExact = true
                        }, new QueryItem {
                            ColumnName = "InvoiceReceiveCompanyName",
                            IsExact = false
                        }, new KeyValuesQueryItem {
                            ColumnName = "PartsSalesOrderType",
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrder_PartsSalesOrderType,
                            KeyValueItems = this.kvPartsSalesOrderTypes
                        }, new KeyValuesQueryItem {
                            ColumnName = "SalesUnitName",
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrder_SalesUnitName,
                            KeyValueItems = this.kvSalesUnits
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                DateTime.Now.AddDays(-7), new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59)
                            }
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrder_Status,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsPartsSalesOrderStatus.审批完成
                        }, new CustomQueryItem {
                            ColumnName = "IfDirectProvision",
                            DataType = typeof(bool),
                            DefaultValue = false
                        }
                    }
                }
            };
        }
    }
}
