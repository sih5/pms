﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query
{
    public class PartsSalesOrderForSelectQueryPanel : DcsQueryPanelBase
    {
        private readonly ObservableCollection<KeyValuePair> kvSalesUnits = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesOrderTypes = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSaleCategories = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = {
            "PartsSalesOrder_Status", "PartsSalesOrder_SecondLevelOrderType"
        };

        public PartsSalesOrderForSelectQueryPanel()
        {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
            this.Initializer.Register(this.viewModel.Initialize);
        }
        private readonly PartsSalesOrderForSelectQueryPanelViewModel viewModel = new PartsSalesOrderForSelectQueryPanelViewModel();
        private void Initialize()
        {
            var dcsDomainContext = new DcsDomainContext();

            dcsDomainContext.Load(dcsDomainContext.GetSalesUnitsQuery(), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                foreach (var salesUnit in loadOp.Entities)
                    this.kvSalesUnits.Add(new KeyValuePair
                    {
                        Key = salesUnit.Id,
                        Value = salesUnit.Name
                    });
            }, null);

            dcsDomainContext.Load(dcsDomainContext.查询销售组织仓库Query(BaseApp.Current.CurrentUserData.EnterpriseId, null, null, null, null), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                var wareHouses = loadOp.Entities.OrderBy(r => r.Warehouse.Name);
                foreach (var warehouse in wareHouses)
                {
                    this.kvWarehouses.Add(new KeyValuePair
                    {
                        Key = warehouse.WarehouseId,
                        Value = warehouse.Warehouse.Name
                    });
                }
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesOrderQuery,
                    EntityType = typeof(PartsSalesOrder),
                    QueryItems = new [] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrder_Code
                        }, new QueryItem {
                            ColumnName = "SalesUnitOwnerCompanyName",
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrder_SalesUnitOwnerCompanyName
                        }, new ComboQueryItem {
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrder_PartsSalesOrderType,
                            ColumnName = "PartsSalesOrderTypeId",
                            SelectedValuePath = "Id",
                            DisplayMemberPath = "Name",
                            ItemsSource = this.viewModel.SalesOrderTypes
                            //KeyValueItems = this.kvPartsSalesOrderTypes
                        }, new ComboQueryItem {
                                Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_SalesCategoryName,
                                ColumnName = "SalesCategoryId",
                                SelectedValuePath = "Id",
                                DisplayMemberPath = "Name",
                                ItemsSource = this.viewModel.SalesCategories,
                                SelectedItemBinding = new Binding("SelectedSalesCategory") {
                                    Source = this.viewModel,
                                    Mode = BindingMode.TwoWay
                                }
                        },new KeyValuesQueryItem {
                            ColumnName = "WarehouseId",
                            KeyValueItems = this.kvWarehouses
                        }, new CustomQueryItem {
                            ColumnName = "IsDebt",
                            DataType = typeof(bool)
                        }, new CustomQueryItem {
                            ColumnName = "IfDirectProvision",
                            DataType = typeof(bool)
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                            }
                        }

                    }
                }
            };
        }
    }
}

public class PartsSalesOrderForSelectQueryPanelViewModel : ViewModelBase
{
    private readonly ObservableCollection<PartsSalesCategory> partsSalesCategories = new ObservableCollection<PartsSalesCategory>();
    private readonly ObservableCollection<PartsSalesOrderType> allPartsSalesOrderTypes = new ObservableCollection<PartsSalesOrderType>();
    //private PagedCollectionView partsSalesOrderTypes;
    //private PartsSalesCategory selectedSalesCategory;

    public ObservableCollection<PartsSalesCategory> SalesCategories
    {
        get
        {
            return this.partsSalesCategories;
        }
    }

    public ObservableCollection<PartsSalesOrderType> SalesOrderTypes {
        get {
            return this.allPartsSalesOrderTypes;
        }
    }

    //public  PagedCollectionView SalesOrderTypes
    //{
    //    get
    //    {
    //        if (this.partsSalesOrderTypes == null)
    //        {
    //            this.partsSalesOrderTypes = new PagedCollectionView(this.allPartsSalesOrderTypes);
    //            this.partsSalesOrderTypes.Filter = o => ((PartsSalesOrderType)o).PartsSalesCategoryId == (this.SelectedSalesCategory != null ? this.SelectedSalesCategory.Id : int.MinValue);
    //        }
    //        return this.partsSalesOrderTypes;
    //    }
    //}

    //public PartsSalesCategory SelectedSalesCategory
    //{
    //    get
    //    {
    //        return this.selectedSalesCategory;
    //    }
    //    set
    //    {
    //        if (this.selectedSalesCategory == value)
    //            return;
    //        this.selectedSalesCategory = value;
    //        this.NotifyOfPropertyChange("PartsSalesCategory");
    //        this.SalesOrderTypes.Refresh();
    //    }
    //}

    public override void Validate()
    {
        //
    }

    public void Initialize()
    {
        var domainContext = new DcsDomainContext();
        domainContext.Load(domainContext.GetPartsSalesOrderTypeByQuery(), LoadBehavior.RefreshCurrent, loadOp =>
        {
            if (loadOp.HasError)
                return;
            foreach (var entity in loadOp.Entities)
                this.allPartsSalesOrderTypes.Add(entity);
        }, null);

        domainContext.Load(domainContext.GetPartsSalesCategoriesQuery(), LoadBehavior.RefreshCurrent, loadOp =>
        {
            if (loadOp.HasError)
                return;
            foreach (var partsSaleCategory in loadOp.Entities)
                this.partsSalesCategories.Add(partsSaleCategory);
        }, null);
    }
}