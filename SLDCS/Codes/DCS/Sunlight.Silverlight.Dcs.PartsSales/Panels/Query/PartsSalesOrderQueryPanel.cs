﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System.Collections.Generic;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class PartsSalesOrderQueryPanel : DcsQueryPanelBase {
        private readonly PartsSalesOrderQueryPanelViewModel viewModel = new PartsSalesOrderQueryPanelViewModel();

        private readonly string[] kvNames = {
            "PartsSalesOrder_SecondLevelOrderType", "PartsSalesOrder_Status","AutoApproveStatus"
        };

        public PartsSalesOrderQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
            this.Initializer.Register(this.viewModel.Initialize);
        }

        private void Initialize() {

            this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesOrder,
                        EntityType = typeof(PartsSalesOrder),
                        QueryItems = new[] {
                            new QueryItem {
                                ColumnName = "Code",
                                Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrder_Code
                            }, new QueryItem {
                                ColumnName = "SubmitCompanyName",
                                Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrder_SubmitCompanyName
                            }
                            //, new ComboQueryItem {
                            //    Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_SalesCategoryName,
                            //    ColumnName = "SalesCategoryId",
                            //    SelectedValuePath = "Id",
                            //    DisplayMemberPath = "Name",
                            //    ItemsSource = this.viewModel.SalesCategories,
                            //    SelectedItemBinding = new Binding("SelectedSalesCategory") {
                            //        Source = this.viewModel,
                            //        Mode = BindingMode.TwoWay
                            //    }
                            //}
                            , new QueryItem {
                                ColumnName = "SubmitCompanyCode",
                                Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrder_SubmitCompanyCode
                            }, new KeyValuesQueryItem {
                                ColumnName = "Status",
                                Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrder_Status,
                                //DefaultValue = (int)DcsPartsSalesOrderStatus.提交,
                                KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                                AllowMultiSelect = true,
                                DefaultValue = new List<int>{(int)DcsPartsSalesOrderStatus.提交,(int)DcsPartsSalesOrderStatus.部分审批},
                            }, new ComboQueryItem {
                                Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrder_PartsSalesOrderType,
                                ColumnName = "PartsSalesOrderTypeId",
                                SelectedValuePath = "Id",
                                DisplayMemberPath = "Name",
                                ItemsSource = this.viewModel.SalesOrderTypes,
                            },
                            //, new CustomQueryItem {
                            //    ColumnName = "IsDebt",
                            //    DataType = typeof(bool)
                            //}
                            new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                DefaultValue = new[] {
                                DateTime.Now.Date.AddDays(-7), DateTime.Now.Date
                            }
                            },new DateTimeRangeQueryItem {
                                 ColumnName = "SubmitTime"
                            }
                            //new ComboQueryItem {
                            //    Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrder_Province,
                            //    ColumnName = "ProvinceName",
                            //    SelectedValuePath = "Id",
                            //    DisplayMemberPath = "Name",
                            //    ItemsSource = this.viewModel.Provinces,
                            //    SelectedItemBinding = new Binding("SelectedProvince") {
                            //        Source = this.viewModel,
                            //        Mode = BindingMode.TwoWay
                            //    }
                            //} , new ComboQueryItem {
                            //    Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrder_City,
                            //    ColumnName = "CityName",
                            //    SelectedValuePath = "Id",
                            //    DisplayMemberPath = "Name",
                            //    ItemsSource = this.viewModel.Citys,
                            //},
                            
                            //, new CustomQueryItem {
                            //    ColumnName = "ERPSourceOrderCode",
                            //    DataType = typeof(string),
                            //    Title = PartsSalesUIStrings.QueryPanel_Title_AgencyRetailerOrder_ERPSourceOrderCode
                            //}
                            //,new DateTimeRangeQueryItem {
                            //    ColumnName = "AbandonTime"
                            //}
                            ,new DateTimeRangeQueryItem {
                                ColumnName = "ApproveTime"
                            }
                            //,new QueryItem{
                            //    ColumnName="IfInnerDirectProvision",
                            //    Title = "是否内部直供"
                            //},new QueryItem {
                            //    ColumnName = "OverseasDemandSheetNo",
                            //    Title = "SAP订单编号"
                            //},new QueryItem {
                            //    ColumnName = "CPPartsPurchaseOrderCode",
                            //    Title =  "原始采购订单编号"
                            //}
                            ,new CustomQueryItem {
                                ColumnName = "SparePartCode",
                                DataType = typeof(string),
                                Title =  PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode
                            }, new KeyValuesQueryItem {
                                ColumnName = "AutoApproveStatus",
                                Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesOrderQuery_AutoApproveStatus,
                                KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                            }, new CustomQueryItem {
                                ColumnName = "IfDirectProvision",
                                DataType = typeof(bool)
                            }
                        }
                    }
                };
        }
    }

    public class PartsSalesOrderQueryPanelViewModel : ViewModelBase {
        private readonly ObservableCollection<PartsSalesCategory> partsSalesCategories = new ObservableCollection<PartsSalesCategory>();
        private readonly ObservableCollection<PartsSalesOrderType> allPartsSalesOrderTypes = new ObservableCollection<PartsSalesOrderType>();
        private PagedCollectionView partsSalesOrderTypes;
        private PartsSalesCategory selectedSalesCategory;

        public ObservableCollection<PartsSalesCategory> SalesCategories {
            get {
                return this.partsSalesCategories;
            }
        }

        public PagedCollectionView SalesOrderTypes {
            get {
                if(this.partsSalesOrderTypes == null) {
                    this.partsSalesOrderTypes = new PagedCollectionView(this.allPartsSalesOrderTypes);
                    this.partsSalesOrderTypes.Filter = o => ((PartsSalesOrderType)o).PartsSalesCategoryId == (this.SelectedSalesCategory != null ? this.SelectedSalesCategory.Id : 221);
                }
                return this.partsSalesOrderTypes;
            }
        }

        public PartsSalesCategory SelectedSalesCategory {
            get {
                return this.selectedSalesCategory;
            }
            set {
                if(this.selectedSalesCategory == value)
                    return;
                this.selectedSalesCategory = value;
                this.NotifyOfPropertyChange("PartsSalesCategory");
                this.SalesOrderTypes.Refresh();
            }
        }


        private readonly ObservableCollection<Region> provinces = new ObservableCollection<Region>();
        private readonly ObservableCollection<Region> allcitys = new ObservableCollection<Region>();
        private PagedCollectionView citys;
        private Region selectedProvince;

        public ObservableCollection<Region> Provinces {
            get {
                return this.provinces;
            }
        }

        public PagedCollectionView Citys {
            get {
                if(this.citys == null) {
                    this.citys = new PagedCollectionView(this.allcitys);
                    this.citys.Filter = o => ((Region)o).ParentId == (this.SelectedProvince != null ? this.SelectedProvince.Id : int.MinValue);
                }
                return this.citys;
            }
        }

        public Region SelectedProvince {
            get {
                return this.selectedProvince;
            }
            set {
                if(this.selectedProvince == value)
                    return;
                this.selectedProvince = value;
                this.NotifyOfPropertyChange("Region");
                this.Citys.Refresh();
            }
        }

        public override void Validate() {
            //
        }

        public void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesOrderTypeByQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var entity in loadOp.Entities)
                    this.allPartsSalesOrderTypes.Add(entity);
            }, null);

            //domainContext.Load(domainContext.GetPartsSalesCategoriesQuery(), LoadBehavior.RefreshCurrent, loadOp =>
            //{
            //    if (loadOp.HasError)
            //        return;
            //    foreach (var partsSaleCategory in loadOp.Entities)
            //        this.partsSalesCategories.Add(partsSaleCategory);
            //}, null);
            domainContext.Load(domainContext.GetRegionsQuery().Where(r => r.Type == 3).OrderBy(r => r.Name), LoadBehavior.RefreshCurrent, loadOpCity => {
                if(loadOpCity.HasError)
                    return;
                foreach(var entity in loadOpCity.Entities)
                    this.allcitys.Add(entity);
            }, null);

            domainContext.Load(domainContext.GetRegionsQuery().Where(r => r.Type == 2).OrderBy(r => r.Name), LoadBehavior.RefreshCurrent, loadOpProvince => {
                if(loadOpProvince.HasError)
                    return;
                foreach(var item in loadOpProvince.Entities)
                    this.provinces.Add(item);
            }, null);
        }
    }
}