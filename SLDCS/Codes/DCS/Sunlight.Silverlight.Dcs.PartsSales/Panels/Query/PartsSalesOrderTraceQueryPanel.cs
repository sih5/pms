﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class PartsSalesOrderTraceQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvAgencies = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvSalesCategories = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = { "Company_Type" };

        public PartsSalesOrderTraceQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetWarehousesQuery().Where(r => r.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId).OrderBy(e => e.Name), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehouse in loadOp.Entities) {
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
                }
            }, null);
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效).OrderBy(e => e.Name), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var salesCategory in loadOp.Entities) {
                    this.kvSalesCategories.Add(new KeyValuePair {
                        Key = salesCategory.Id,
                        Value = salesCategory.Name
                    });
                }
            }, null);

            domainContext.Load(domainContext.GetAgenciesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效).OrderBy(e => e.Name), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var agency in loadOp.Entities) {
                    this.kvAgencies.Add(new KeyValuePair {
                        Key = agency.Id,
                        Value = agency.Name
                    });
                }
            }, null);

            this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesOrder,
                        EntityType = typeof(VehiclePartsSalesOrdercs),
                        QueryItems = new[] {
                            new QueryItem {
                                ColumnName = "PartsSalesOrderCode",
                                Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrder_Code
                            }, new QueryItem {
                                ColumnName = "SubmitCompanyName",
                                Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrder_SubmitCompanyName
                            }, new KeyValuesQueryItem {
                                Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_SalesCategoryName,
                                ColumnName = "SalesCategoryId",
                                KeyValueItems = this.kvSalesCategories
                            } , new QueryItem {
                                ColumnName = "SubmitCompanyCode",
                                Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDelivery_Shop_Code
                            },new KeyValuesQueryItem {
                                ColumnName = "WarehouseId",
                                KeyValueItems = this.kvWarehouses,
                                Title=PartsSalesUIStrings.QueryPanel_Title_VehiclePartsSalesOrdercsQuery_WarehouseId
                            }, new QueryItem {
                                ColumnName = "SparePartCode"
                            }, new QueryItem {
                                ColumnName = "SparePartName" 
                            },new DateTimeRangeQueryItem {
                                 ColumnName = "SubmitTime",
                                 DefaultValue = new[] {
                                    DateTime.Now.Date, DateTime.Now.Date
                                }
                            },new KeyValuesQueryItem {
                                ColumnName = "SalesUnitOwnerCompanyId",
                                KeyValueItems = this.kvAgencies,
                                Title=PartsSalesUIStrings.QueryPanel_Title_VehiclePartsSalesOrdercsQuery_SalesUnitOwnerCompanyId,
                            } ,new QueryItem {
                                ColumnName = "ERPSourceOrderCode",
                                Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDelivery_PlatForm_Code
                            }
                           
                        }
                    }
            };
        }
    }
}
