﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class PartsSalesOrderTypeNQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
            "BaseData_Status","SalesOrderType_SettleType","SalesOrderType_BusinessType"
        };

        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();

        public PartsSalesOrderTypeNQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(kvNames);
        }
        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效 && v.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.kvPartsSalesCategoryName.Clear();
                foreach(var partsSalesCategory in loadOp.Entities) {
                    this.kvPartsSalesCategoryName.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
                }
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetBranchesQuery().Where(ex => ex.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var branch in loadOp.Entities)
                    this.kvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesOrderType,
                        EntityType = typeof(PartsSalesOrderType),
                        QueryItems = new[] {
                            new QueryItem {
                                ColumnName = "Code"
                            }, new QueryItem {
                                ColumnName = "Name"
                            },  new KeyValuesQueryItem{
                                Title=PartsSalesUIStrings.QueryPanel_Title_BranchName,
                                ColumnName="BranchId",
                                KeyValueItems=this.kvBranches
                            },  new KeyValuesQueryItem {
                                Title=PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name,
                                ColumnName = "PartsSalesCategoryId",
                                KeyValueItems=this.kvPartsSalesCategoryName
                            },new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                DefaultValue = new[] {
                                    new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                                }
                            }, new KeyValuesQueryItem {
                                ColumnName = "Status",
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                                DefaultValue = (int)DcsBaseDataStatus.有效
                            }, new KeyValuesQueryItem {
                                ColumnName = "SettleType",
                                KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                                Title=PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_SettleType
                            }, new KeyValuesQueryItem {
                                ColumnName = "BusinessType",
                                KeyValueItems = this.KeyValueManager[this.kvNames[2]],
                                Title=PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_BusinessType
                            }, new QueryItem {
                                Title=PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_BusinessCode,
                                ColumnName = "BusinessCode"
                            }
                        }
                    }
                };
        }
    }
}
