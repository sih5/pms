﻿
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class PartsSalesPriceChangeQueryPanel : DcsQueryPanelBase {
        public PartsSalesPriceChangeQueryPanel() {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private readonly string[] kvNames = {
            "PartsSalesPriceChange_Status"
        };
        //private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategories = new ObservableCollection<KeyValuePair>();

        private void Initialize() {
            //var dcsDomainContext = new DcsDomainContext();
            //dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    foreach(var branch in loadOp.Entities)
            //        this.kvPartsSalesCategories.Add(new KeyValuePair {
            //            Key = branch.Id,
            //            Value = branch.Name
            //        });
            //}, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesPriceChange,
                    EntityType = typeof(PartsSalesPriceChange),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code"
                        }
                        //, new KeyValuesQueryItem {
                        //    ColumnName = "PartsSalesCategoryId",
                        //    KeyValueItems = this.kvPartsSalesCategories,
                        //    Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesPriceChange_PartsSalesCategory
                        //}
                        ,new CustomQueryItem {
                            Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode,
                            ColumnName = "SparePartCode",
                            DataType = typeof(string)
                        }
                        ,new CustomQueryItem {
                            Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName,
                            ColumnName = "SparePartName",
                            DataType = typeof(string)
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        }
                    }
                }
            };
        }
    }
}
