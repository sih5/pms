﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesPriceForExportSelectQueryPanel : DcsQueryPanelBase {
        public PartsSalesPriceForExportSelectQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title =PartsSalesUIStrings.QueryPanel_Title_PartsSalesPrice,
                    EntityType = typeof(VirtualPartsSalesPrice),
                    QueryItems = new[] {
                        new QueryItem {
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_VirtualPartsSalesPrice_SparePartCode,
                            ColumnName = "SparePartCode"
                        },new QueryItem {
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_VirtualPartsSalesPrice_SparePartName,
                            ColumnName = "SparePartName"
                        },new QueryItem {
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_VirtualPartsSalesPrice_ReferenceCode,
                            ColumnName = "ReferenceCode"
                        },new QueryItem {
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_VirtualPartsSalesPrice_ReferenceName,
                            ColumnName = "ReferenceName"
                        }
                    }
                }
            };
        }
    }
}
