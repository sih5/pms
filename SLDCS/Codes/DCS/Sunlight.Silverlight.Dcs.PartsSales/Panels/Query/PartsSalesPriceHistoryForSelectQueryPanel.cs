﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class PartsSalesPriceHistoryForSelectQueryPanel : DcsQueryPanelBase {


        public PartsSalesPriceHistoryForSelectQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {

            //查询列表
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title =PartsSalesUIStrings.QueryPanel_Title_PartsSalesPriceHistory,
                    EntityType = typeof(PartsSalesPriceChangeDetail),
                    QueryItems = new[] {
                        new QueryItem {
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_VirtualPartsSalesPriceHistory_SparePartCode,
                            ColumnName = "SparePartCode",
                            IsExact = true
                        },new QueryItem {
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_VirtualPartsSalesPriceHistory_SparePartName,
                            ColumnName = "SparePartName",
                            IsExact = true
                        },new DateTimeRangeQueryItem  {
                            ColumnName = "PartsSalesPriceChange.CreateTime",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                            },
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_VirtualPartsSalesPriceHistory_CreateTime
                        }
                    }
                }
            };
        }
    }
}
