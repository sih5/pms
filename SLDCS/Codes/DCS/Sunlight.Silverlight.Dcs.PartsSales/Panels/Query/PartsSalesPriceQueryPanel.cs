﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class PartsSalesPriceQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "BaseData_Status", "PartsSalesPrice_PriceType"
        };

        public PartsSalesPriceQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(kvNames);
        }

        private ObservableCollection<KeyValuePair> kvPriceIncreaseRates;
        public ObservableCollection<KeyValuePair> KvPriceIncreaseRates {
            get {
                return this.kvPriceIncreaseRates ?? (this.kvPriceIncreaseRates = new ObservableCollection<KeyValuePair>());
            }
        }
        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalePriceIncreaseRatesQuery().Where(r => r.status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                foreach(var item in loadOp.Entities) {
                    this.KvPriceIncreaseRates.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.GroupCode
                    });
                }
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesPrice,
                    EntityType = typeof(PartsSalesPrice),
                    QueryItems = new[] {
                        new CustomControlQueryItem { 
                            ColumnName = "SparePartCode", 
                            Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode, 
                            CreateControl=type =>new Controls.MultipleTextQueryControl(type,"SparePartCode") 
                        }, new QueryItem {
                            ColumnName = "SparePartName"
                        }, new KeyValuesQueryItem {
                            ColumnName = "PriceType",
                            KeyValueItems = this.KvPriceIncreaseRates
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            DefaultValue = (int)DcsBaseDataStatus.有效,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                            //DefaultValue = new[] {
                            //    new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                            //}
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "ModifyTime"
                        }, new CustomQueryItem {
                            Title =  PartsSalesUIStrings.QueryPanel_QueryItem_Title_isOrderable,
                            ColumnName = "isOrderable",
                            DataType = typeof(bool),
                            DefaultValue = null
                        }, new CustomQueryItem {
                            Title =  PartsSalesUIStrings.QueryPanel_QueryItem_Title_isSale,
                            ColumnName = "isSale",
                            DataType = typeof(bool),
                            DefaultValue = null
                        }
                    }
                }
            };
        }
    }
}
