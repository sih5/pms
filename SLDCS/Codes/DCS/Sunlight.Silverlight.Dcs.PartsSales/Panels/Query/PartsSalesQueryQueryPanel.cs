﻿using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Web.Entities;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Query {
    public class PartsSalesQueryQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvWarehouseName = new ObservableCollection<KeyValuePair>();

        public PartsSalesQueryQueryPanel() {
            Initializer.Register(Initialize);

        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetBranchesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                }
                this.kvBranches.Clear();
                foreach(var branch in loadOp.Entities)
                    this.kvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Code
                    });
                dcsDomainContext.Load(dcsDomainContext.GetWarehousesOrderByNameQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效 && v.Type != (int)DcsWarehouseType.虚拟库), LoadBehavior.RefreshCurrent, loadOp2 => {
                    if(loadOp2.HasError)
                        return;
                    this.kvWarehouseName.Clear();
                    foreach(var warehouse in loadOp2.Entities) {
                        this.kvWarehouseName.Add(new KeyValuePair {
                            Key = warehouse.Id,
                            Value = warehouse.Name
                        });
                    }
                }, null);

            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetCompaniesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var company = loadOp.Entities.SingleOrDefault();
                QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesQueryOnWay,
                        EntityType = typeof(PartsSalesQuery),
                        QueryItems = new QueryItem[] {
                            new KeyValuesQueryItem {
                                Title = PartsSalesUIStrings.QueryPanel_Title_BranchName,
                                ColumnName = "BranchId",
                                KeyValueItems = kvBranches,
                                DefaultValue = company != null && company.Type == (int)DcsCompanyType.分公司 ? BaseApp.Current.CurrentUserData.EnterpriseId : 0,
                                IsEnabled = BaseApp.Current.CurrentUserData.UserCode == "ReportAdmin"
                            },
                            new KeyValuesQueryItem {
                                Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesQueryOnWay_WarehouseId,
                                ColumnName = "WarehouseId",
                                KeyValueItems = kvWarehouseName
                            },
                            new QueryItem {
                                Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesQueryOnWay_ReceivingCompanyCode,
                                ColumnName = "ReceivingCompanyCode"
                            },
                            new QueryItem {
                                Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesQueryOnWay_ReceivingCompanyName,
                                ColumnName = "ReceivingCompanyName"
                            }
                        }
                    }
                };
            }, null);
        }
    }
}
