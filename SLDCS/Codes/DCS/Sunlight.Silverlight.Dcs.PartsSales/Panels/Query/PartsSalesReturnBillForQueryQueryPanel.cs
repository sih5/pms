﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    /// <summary>
    /// 配件销售退货单查询面板
    /// </summary>
    public class PartsSalesReturnBillForQueryQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = { 
            "PartsSalesReturnBill_Status", "PartsSalesReturn_ReturnType" 
        };

        private ObservableCollection<KeyValuePair> salesUnits = new ObservableCollection<KeyValuePair>();


        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetSalesUnitsQuery().Where(e => e.OwnerCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var salesUnit in loadOp.Entities) {
                    salesUnits.Add(new KeyValuePair {
                        Key = salesUnit.Id,
                        Value = salesUnit.Name
                    });
                }
            }, null);

            this.QueryItemGroups = new[] {
                new QueryItemGroup{
                    UniqueId = "Common",
                    EntityType = typeof(PartsSalesReturnBill),
                    Title = PartsSalesUIStrings.QueryPanel_Title_PartsRetailReturnBill,
                    QueryItems = new QueryItem[]{
                        new QueryItem{
                            ColumnName = "ReturnCompanyName",
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesReturnBill_ReturnCompanyName
                        }, new KeyValuesQueryItem{
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsPartsSalesReturnBillStatus.审核通过,
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesReturnBill_Status
                        }, new KeyValuesQueryItem{
                            ColumnName = "ReturnType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        }, new DateTimeRangeQueryItem{                            
                            ColumnName = "CreateTime"
                        }, new KeyValuesQueryItem{
                            ColumnName = "SalesUnitId",
                            KeyValueItems = salesUnits,
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesReturnBill_SalesUnitName
                        }
                    }
                }
            };
        }

        public PartsSalesReturnBillForQueryQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
    }
}
