﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class PartsSalesReturnBillForReportQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "PartsSalesReturnBill_Status", "PartsSalesReturn_ReturnType"
        };

        public PartsSalesReturnBillForReportQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(kvNames);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesReturnBillForReport,
                    EntityType = typeof(PartsSalesReturnBill),
                    QueryItems = new[] {
                        new QueryItem {
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesReturnBill_Code,
                            ColumnName = "Code"
                        }, new QueryItem {
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesReturnBill_SalesUnitName,
                            ColumnName = "SalesUnitName"
                        }, new KeyValuesQueryItem {
                            ColumnName = "ReturnType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        }, new KeyValuesQueryItem {
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesReturnBill_Status,
                            ColumnName = "Status",
                            DefaultValue = (int)DcsPartsSalesReturnBillStatus.新增,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        }, new QueryItem {
                            Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_PartsSalesOrderCode,
                            ColumnName = "PartsSalesOrderCode"
                        }
                    }
                }
            };
        }
    }
}
