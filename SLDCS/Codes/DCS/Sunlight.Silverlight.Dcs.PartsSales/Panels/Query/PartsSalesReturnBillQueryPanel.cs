﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;


namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class PartsSalesReturnBillQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "PartsSalesReturnBill_Status", "PartsSalesReturn_ReturnType"
        };

        public PartsSalesReturnBillQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(kvNames);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesReturnBill,
                    EntityType = typeof(PartsSalesReturnBill),
                    QueryItems = new[] {
                         new QueryItem {
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesReturnBill_Code,
                            ColumnName = "Code"
                        },new QueryItem {
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesReturnBill_ReturnCompanyName,
                            ColumnName = "ReturnCompanyName"
                        }, new KeyValuesQueryItem {
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesReturnBill_Status,
                            ColumnName = "Status",
                            DefaultValue = (int)DcsPartsSalesReturnBillStatus.提交,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new KeyValuesQueryItem {
                            ColumnName = "ReturnType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        }, new CustomQueryItem{
                            Title =PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDelivery_PlatForm_Code,
                            ColumnName = "PartsSalesReturnBillDetails.PartsSalesOrder.ERPSourceOrderCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem{
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrder_ContactPerson,
                            ColumnName = "PartsSalesReturnBillDetails.PartsSalesOrder.ContactPerson",
                            DataType = typeof(string)
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        }, new CustomQueryItem{
                            Title = PartsSalesUIStrings.DetailPanel_Text_PartsSalesOrder_Code,
                            ColumnName = "PartsSalesOrderCode",
                            DataType = typeof(string)
                        }
                    }
                }
            };
        }
    }
}
