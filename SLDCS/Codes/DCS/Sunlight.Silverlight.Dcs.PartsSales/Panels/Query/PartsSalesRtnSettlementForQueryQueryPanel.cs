﻿
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class PartsSalesRtnSettlementForQueryQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "PartsSalesRtnSettlement_Status"
        };
        public PartsSalesRtnSettlementForQueryQueryPanel() {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private readonly ObservableCollection<KeyValuePair> kvCategoryNames = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvAccountGroups = new ObservableCollection<KeyValuePair>();

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesCategoryByBreachIdQuery(BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var categoryName in loadOp.Entities)
                    this.kvCategoryNames.Add(new KeyValuePair {
                        Key = categoryName.Id,
                        Value = categoryName.Name,
                    });
            }, null);
            domainContext.Load(domainContext.GetAccountGroupsByOwnerCompanyIdQuery(BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                foreach(var accountGroup in loadOp.Entities)
                    this.kvAccountGroups.Add(new KeyValuePair {
                        Key = accountGroup.Id,
                        Value = accountGroup.Name
                    });
            }, null);

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesRtnSettlement,
                    EntityType = typeof(PartsSalesRtnSettlement),
                    QueryItems = new[] {
                         new QueryItem {
                            ColumnName = "Code",
                            Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesRtnSettlement_Code
                        }, new QueryItem {
                            ColumnName = "CustomerCompanyCode"
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsPartsSalesRtnSettlementStatus.新建
                        },new QueryItem {
                            ColumnName = "CustomerCompanyName"
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                            }
                        },new KeyValuesQueryItem {
                                Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesSettlement_AccountGroupId,
                                ColumnName = "AccountGroupId",
                                KeyValueItems = this.kvAccountGroups
                        },
                            new KeyValuesQueryItem {
                                Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name,
                                ColumnName = "PartsSalesCategoryId",
                                KeyValueItems = this.kvCategoryNames
                        }
                    }
                }
            };
        }
    }
}
