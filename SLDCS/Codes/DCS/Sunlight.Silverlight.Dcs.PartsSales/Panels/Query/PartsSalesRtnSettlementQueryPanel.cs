﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class PartsSalesRtnSettlementQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "PartsSalesRtnSettlement_Status","SalesOrderType_SettleType","SalesOrderType_BusinessType"
        };

        public PartsSalesRtnSettlementQueryPanel() {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
        }

        //private readonly ObservableCollection<KeyValuePair> kvCategoryNames = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvAccountGroups = new ObservableCollection<KeyValuePair>();

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            //domainContext.Load(domainContext.GetPartsSalesCategoryByBreachIdQuery(BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    foreach(var categoryName in loadOp.Entities)
            //        this.kvCategoryNames.Add(new KeyValuePair {
            //            Key = categoryName.Id,
            //            Value = categoryName.Name,
            //        });
            //}, null);
            domainContext.Load(domainContext.GetAccountGroupsByOwnerCompanyIdQuery(BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                foreach(var accountGroup in loadOp.Entities)
                    this.kvAccountGroups.Add(new KeyValuePair {
                        Key = accountGroup.Id,
                        Value = accountGroup.Name
                    });
            }, null);

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesRtnSettlement,
                    EntityType = typeof(PartsSalesRtnSettlement),
                    QueryItems = new[] {
                         new QueryItem {
                            ColumnName = "Code",
                            Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesRtnSettlement_Code
                        }, new QueryItem {
                            ColumnName = "CustomerCompanyCode"
                        }
                        //,new KeyValuesQueryItem {
                        //        Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name,
                        //        ColumnName = "PartsSalesCategoryId",
                        //        KeyValueItems = this.kvCategoryNames
                        //}
                        , new DateTimeRangeQueryItem {
                                ColumnName = "InvoiceDate",
                                 Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_InvoiceDate
                         },new KeyValuesQueryItem {
                                Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesSettlement_AccountGroupId,
                                ColumnName = "AccountGroupId",
                                KeyValueItems = this.kvAccountGroups
                        }
                        , new QueryItem {
                            ColumnName = "CustomerCompanyName"
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsPartsSalesRtnSettlementStatus.新建
                        },new QueryItem {
                            ColumnName = "CreatorName"
                        },new CustomQueryItem {
                            ColumnName = "BusinessCode",
                            DataType=typeof(string),
                            Title=PartsSalesUIStrings.QueryPanel_Title_VirtualInvoiceInformation_BusinessCode
                        },new KeyValuesQueryItem {
                                ColumnName = "SettleType",
                                Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_SettleType,
                                KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        },new KeyValuesQueryItem {
                                ColumnName = "BusinessType",
                                Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_BusinessType,
                                KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                            }
                        }
                    }
                }
            };
        }
    }
}
