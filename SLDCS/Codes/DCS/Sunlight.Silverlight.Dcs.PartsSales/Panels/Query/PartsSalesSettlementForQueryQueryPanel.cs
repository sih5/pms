﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class PartsSalesSettlementForQueryQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "PartsSalesSettlement_Status"
        };

        private readonly ObservableCollection<KeyValuePair> kvAccountGroups = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> KvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();

        public PartsSalesSettlementForQueryQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetAccountGroupsByOwnerCompanyIdQuery(BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                foreach(var accountGroup in loadOp.Entities)
                    this.kvAccountGroups.Add(new KeyValuePair {
                        Key = accountGroup.Id,
                        Value = accountGroup.Name
                    });
            }, null);
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(ex => ex.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                foreach(var partsSalesCategory in loadOp.Entities)
                    this.KvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        EntityType = typeof(PartsSalesSettlement),
                        Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesSettlement,
                        QueryItems = new[] {
                            new QueryItem {
                                ColumnName = "Code",
                                Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesSettlement_Code,
                            },new QueryItem {
                                ColumnName = "CustomerCompanyCode"
                            }, new KeyValuesQueryItem {
                                ColumnName = "Status",
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                                DefaultValue = (int)DcsPartsPurchaseSettleStatus.新建
                            },new QueryItem {
                                ColumnName = "CustomerCompanyName"
                            }, new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                DefaultValue = new[] {
                                    new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                                }
                            },new KeyValuesQueryItem {
                                Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesSettlement_AccountGroupId,
                                ColumnName = "AccountGroupId",
                                KeyValueItems = this.kvAccountGroups
                            },new KeyValuesQueryItem {
                                Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesSettlement_PartsSalesCategory,
                                ColumnName = "PartsSalesCategoryId",
                                KeyValueItems = this.KvPartsSalesCategorys
                            }
                        }
                    }
                };
        }
    }
}
