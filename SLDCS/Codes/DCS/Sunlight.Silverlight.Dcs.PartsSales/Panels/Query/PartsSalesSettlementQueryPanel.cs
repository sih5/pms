﻿
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class PartsSalesSettlementQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "PartsSalesSettlement_Status","SalesOrderType_SettleType","SalesOrderType_BusinessType"
        };

        private readonly ObservableCollection<KeyValuePair> kvAccountGroups = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvCategoryNames = new ObservableCollection<KeyValuePair>();

        public PartsSalesSettlementQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            //domainContext.Load(domainContext.GetPartsSalesCategoriesWithBranchIdQuery(), loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    foreach(var categoryName in loadOp.Entities)
            //        this.kvCategoryNames.Add(new KeyValuePair {
            //            Key = categoryName.Id,
            //            Value = categoryName.Name,
            //        });
            //}, null);

            domainContext.Load(domainContext.GetAccountGroupsByOwnerCompanyIdQuery(BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                foreach(var accountGroup in loadOp.Entities)
                    this.kvAccountGroups.Add(new KeyValuePair {
                        Key = accountGroup.Id,
                        Value = accountGroup.Name
                    });
                this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        EntityType = typeof(PartsSalesSettlementEx),
                        Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesSettlement,
                        QueryItems = new[] {
                            new QueryItem {
                                ColumnName = "Code",
                                Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesSettlement_Code,
                            },  new KeyValuesQueryItem {
                                ColumnName = "Status",
                                Title = PartsSalesUIStrings.QueryPanel_Title_AgencyLogisticCompany_Status,
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                                DefaultValue = (int)DcsPartsPurchaseSettleStatus.新建
                            }
                            //, new KeyValuesQueryItem {
                            //    Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name,
                            //    ColumnName = "PartsSalesCategoryId",
                            //    KeyValueItems = this.kvCategoryNames
                            //}
                            ,new KeyValuesQueryItem {
                                Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesSettlement_AccountGroupId,
                                ColumnName = "AccountGroupId",
                                KeyValueItems = this.kvAccountGroups
                            },new QueryItem {
                                ColumnName = "CustomerCompanyCode",
                                Title = PartsSalesUIStrings.DataEditPanel_Text_CustomerOrderPriceGrade_CustomerCompanyCode
                            }, new QueryItem {
                                ColumnName = "CustomerCompanyName",
                                 Title = PartsSalesUIStrings.DataEditPanel_Text_CustomerOrderPriceGrade_CustomerCompanyName
                            }, new QueryItem {
                                ColumnName = "CreatorName",
                                 Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_CreatorName
                            },new QueryItem {
                            ColumnName = "BusinessCode" ,
                            //DataType = typeof(string),
                            Title = PartsSalesUIStrings.QueryPanel_Title_VirtualInvoiceInformation_BusinessCode
                          }, new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                 Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesSettlementRef_SourceBillCreateTime,
                                DefaultValue = new[] {
                                    new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                                }
                            }, new DateTimeRangeQueryItem {
                                ColumnName = "InvoiceDate",
                                 Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_InvoiceDate
                            },  new KeyValuesQueryItem {
                                ColumnName = "SettleType",
                                Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_SettleType,
                                KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                            },  new KeyValuesQueryItem {
                                ColumnName = "BusinessType",
                                Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_BusinessType,
                                KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                            }
                        }
                    }
                };
            }, null);
        }
    }
}
