﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class PartsSalesWeeklyBaseQueryPanel : DcsQueryPanelBase {
        public PartsSalesWeeklyBaseQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }
        private ObservableCollection<KeyValuePair> warehouse = new ObservableCollection<KeyValuePair>();
        private ObservableCollection<KeyValuePair> orderType = new ObservableCollection<KeyValuePair>();
        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetWarehousesQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var salesUnit in loadOp.Entities) {
                    warehouse.Add(new KeyValuePair {
                        Key = salesUnit.Id,
                        Value = salesUnit.Name
                    });
                }
            }, null);
            domainContext.Load(domainContext.GetPartsSalesOrderTypesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var salesUnit in loadOp.Entities) {
                    orderType.Add(new KeyValuePair {
                        Key = salesUnit.Id,
                        Value = salesUnit.Name
                    });
                }
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.Management_Title_PartsSalesWeeklyBaseQuery,
                    EntityType = typeof(PartsSalesWeeklyBase),
                    QueryItems = new QueryItem[] { 
                        new CustomQueryItem {
                           ColumnName = "SparePartCode",
                            DataType=typeof(string),
                           Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode
                        },new CustomQueryItem {
                           ColumnName = "Week",
                            DataType=typeof(int),
                           Title=PartsSalesUIStrings.DataGridView_Title_CenterABCBasis_WeekNum
                        }, new KeyValuesQueryItem{
                            ColumnName = "WarehouseId",
                            KeyValueItems = this.warehouse,
                            Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesQueryOnWay_WarehouseId
                        }, new KeyValuesQueryItem{
                            ColumnName = "PartsSalesOrderTypeId",
                            KeyValueItems =this.orderType,
                            Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_PartsSalesOrderTypeNameNew
                        }
                    }
                }
            };
        }
    }
}
