﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class PartsStockCoefficientQueryPanel : DcsQueryPanelBase {

        private readonly string[] kvNames = {
            "PartsStockCoefficientType","ABCSetting_Type","BaseData_Status"
        };
        public PartsStockCoefficientQueryPanel() {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.Management_Title_PartsStockCoefficientQuery,
                    EntityType = typeof(PartsStockCoefficient),
                    QueryItems = new[] {
                        new KeyValuesQueryItem {
                            ColumnName = "Type",
                            Title=PartsSalesUIStrings.DataGridView_Title_PartsStockCoefficient_Type,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        }, new KeyValuesQueryItem {
                           ColumnName = "PRODUCTTYPE",
                           Title=PartsSalesUIStrings.DataGridView_Title_PartsStockCoefficient_ProductType,
                           KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        }, new KeyValuesQueryItem {
                           ColumnName = "Status",
                           KeyValueItems = this.KeyValueManager[this.kvNames[2]],
                           Title=PartsSalesUIStrings.QueryPanel_Title_AgencyLogisticCompany_Status
                        }
                    }
                }
            };
        }
    }
}
