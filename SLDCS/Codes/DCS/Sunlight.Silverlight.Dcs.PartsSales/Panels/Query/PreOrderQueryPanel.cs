﻿
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class PreOrderQueryPanel : DcsQueryPanelBase {
        public PreOrderQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }
        private readonly string[] kvNames = {
            "PreOrderStatus"
        };
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title =PartsSalesUIStrings.QueryPanel_Title_CustomerInformationForPartsSalesReturn,
                    EntityType = typeof(PreOrder),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code"
                        },new QueryItem {
                            ColumnName = "PartsSalesCategoryCode"
                        },new QueryItem {
                            ColumnName = "DealerCode",
                            DefaultValue = BaseApp.Current.CurrentUserData.EnterpriseCode,
                            IsEnabled = false
                        },new QueryItem {
                            ColumnName = "PartCode"
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[kvNames[0]]   ,
                            DefaultValue = (int)DcsPreOrderStatus.新建
                        }
                    }
                }
            };
        }
    }
}
