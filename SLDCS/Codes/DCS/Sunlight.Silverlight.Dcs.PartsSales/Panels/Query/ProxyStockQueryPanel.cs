﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class ProxyStockQueryPanel : DcsQueryPanelBase {
        public ProxyStockQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.QueryPanel_Title_VirtualPartsSalesPriceWithStock,
                    EntityType = typeof(VirtualPartsSalesPriceWithStock),
                    QueryItems = new QueryItem[] {
                        new QueryItem {
                            ColumnName = "SparePartCode",
                            Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode
                        },new QueryItem {
                            ColumnName = "SparePartName",
                            Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                        },new QueryItem {
                            ColumnName = "WarehouseName",
                            Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_WarehouseName,
                            IsEnabled= false
                        }
                    }
                }
            };
        }
    }
}
