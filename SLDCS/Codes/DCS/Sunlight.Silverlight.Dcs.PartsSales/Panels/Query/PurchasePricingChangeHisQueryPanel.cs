﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query
{
    public class PurchasePricingChangeHisQueryPanel : DcsQueryPanelBase
    {
        private readonly string[] kvNames = {
            "MasterData_Status"
        };
        public PurchasePricingChangeHisQueryPanel()
        {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize()
        {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(PurchasePricingChangeHi),
                    Title = PartsSalesUIStrings.QueryPanel_Title_PurchasePricingChangeHiStock,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "SparePartCode",
                            Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode
                        },new QueryItem {
                            ColumnName = "SparePartName",
                            Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                        },new CustomQueryItem {
                            ColumnName = "ReferenceCode",
                            DataType = typeof(string),
                            Title=PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_SihCode
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            Title=PartsSalesUIStrings.QueryPanel_Title_AgencyLogisticCompany_Status,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                            }
                        }
                    }
                }
            };
        }
    }
}
