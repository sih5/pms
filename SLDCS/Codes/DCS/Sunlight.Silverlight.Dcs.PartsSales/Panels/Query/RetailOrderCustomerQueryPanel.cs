﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class RetailOrderCustomerQueryPanel: DcsQueryPanelBase {
        public RetailOrderCustomerQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup{
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.QueryPanel_Title_RetailOrderCustomerQuery,//PartsSalesUIStrings.QueryPanel_Title_DealerPartsStock,
                    EntityType = typeof(RetailOrderCustomer),
                    QueryItems = new[]{
                        new QueryItem{
                            ColumnName = "CustomerName",
                            IsExact = true
                        }, new QueryItem{
                            ColumnName = "CustomerCellPhone",
                            IsExact = false
                        }, new QueryItem{
                            ColumnName = "CustomerPhone",
                            IsExact = true
                  
                        }
                    }
                }
            };
        }
    }
}
