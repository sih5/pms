﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class SalesOrderRecommendForceQueryPanel: DcsQueryPanelBase {

        private readonly string[] kvNames = {
            "ABCSetting_Type"
        };
        public SalesOrderRecommendForceQueryPanel() {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.Management_Title_SalesOrderRecommendForce_Query,
                    EntityType = typeof(SalesOrderRecommendForce),
                     QueryItems = new QueryItem[] {
                       new CustomQueryItem {
                            ColumnName = "SparePartCode",
                            DataType=typeof(string),
                            Title=PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_VirtualPartsSalesPriceHistory_SparePartCode
                        },new CustomQueryItem {
                            ColumnName = "CenterName",
                            DataType=typeof(string),
                            Title=PartsSalesUIStrings.DetailPanel_Text_PartsSalesOrder_CenterCompanyName
                        }, new KeyValuesQueryItem {
                           ColumnName = "Type",
                           Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_Type,
                           KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        }, new DateTimeRangeQueryItem {
                          ColumnName = "CreateTime",
                          Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesWeeklyBase_CreateTime,
                          DefaultValue = new[] {
                             new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day-1), DateTime.Now.Date
                        }
                      }
                    }
                }
            };
        }
    }
}
