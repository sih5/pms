﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.PartsSales
    .Panels.Query {
    public class SalesUnitQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "MasterData_Status" ,"SalesUnit_SubmitCompanyId"
        };
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();
        public SalesUnitQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if(loadOp.HasError)
                    return;
                this.kvPartsSalesCategoryName.Clear();
                foreach(var partsSalesCategory in loadOp.Entities)
                    this.kvPartsSalesCategoryName.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(SalesUnit),
                    Title = PartsSalesUIStrings.QueryPanel_Title_SalesUnit,
                    QueryItems = new[] {
                        new KeyValuesQueryItem {
                            Title=PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name,
                            ColumnName = "PartsSalesCategoryId",
                            KeyValueItems=this.kvPartsSalesCategoryName
                        },new QueryItem {
                            ColumnName = "BusinessCode"
                        },new QueryItem {
                            ColumnName = "Name",
                            Title = PartsSalesUIStrings.DetailPanel_Text_PartsSalesReturnBill_SalesUnitName2
                        }, new QueryItem {
                            ColumnName = "BusinessName"
                        }, new CustomQueryItem {
                            ColumnName = "Company.Code",
                            DataType = typeof(string),
                            Title = PartsSalesUIStrings.QueryPanel_QueeryItem_Title_Company_Code
                        }, new CustomQueryItem {
                            ColumnName = "Company.Name",
                            DataType = typeof(string),
                            Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_Personnel_CorporationName
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            Title=PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Status,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsMasterDataStatus.有效
                        },new KeyValuesQueryItem {
                            ColumnName = "SubmitCompanyId",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        }
                    }
                }
            };
        }
    }
}
