﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class SecondClassStationPlanForSelectQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "SecondClassStationPlan_Status"
        };

        private ObservableCollection<KeyValuePair> kvPartsSalesCategory = new ObservableCollection<KeyValuePair>();

        public SecondClassStationPlanForSelectQueryPanel() {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initalize);
        }

        public void Initalize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategory in loadOp.Entities)
                    this.kvPartsSalesCategory.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
                this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_SecondClassStation,
                        EntityType = typeof(SecondClassStationPlan),
                        QueryItems = new[] {
                            new QueryItem {
                                ColumnName = "SecondClassStationName",
                                Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_SecondClassStationPlan_SecondClassStationName
                            }, new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                IsExact = false,
                                DefaultValue = new[] {
                                    DateTime.Now.Date.AddDays(-7), DateTime.Now.Date
                                }
                            }, new KeyValuesQueryItem {
                                ColumnName = "Status",
                                KeyValueItems = this.KeyValueManager[kvNames[0]]
                            }, new KeyValuesQueryItem {
                                ColumnName = "PartsSalesCategoryId",
                                Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name,
                                KeyValueItems = this.kvPartsSalesCategory
                            }
                        }
                    }
                };
            }, null);
        }
    }
}