﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class SelectDealerPartsRetailOrderQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "WorkflowOfSimpleApproval_Status","DealerPartsRetailOrder_RetailOrderType"
        };
        public SelectDealerPartsRetailOrderQueryPanel() {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), loadOp => {//&& e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategory in loadOp.Entities)
                    this.kvPartsSalesCategoryName.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.kvBranches.Clear();
                foreach(var branch in loadOp.Entities)
                    this.kvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name,
                        UserObject = branch
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup{
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.QueryPanel_Title_DealerPartsRetailOrder,
                    EntityType = typeof(DealerPartsRetailOrder),
                    QueryItems = new[]{
                        new KeyValuesQueryItem{
                            ColumnName = "BranchId",
                            KeyValueItems=this.kvBranches,
                            Title=PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsRetailOrder_BranchName
                        }, new KeyValuesQueryItem {
                            Title=PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsRetailOrder_PartsSalesCategoryName,
                            ColumnName = "PartsSalesCategoryId",
                            KeyValueItems=this.kvPartsSalesCategoryName
                        }, new QueryItem{
                            ColumnName = "Code",
                            Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsRetailOrder_Code
                          }, new KeyValuesQueryItem {
                            ColumnName = "RetailOrderType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            DefaultValue = (int)DcsDealerPartsRetailOrderRetailOrderType.普通客户
                        }, new QueryItem {
                            Title= PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_Customer,
                            ColumnName = "Customer"
                        }, new QueryItem {
                            ColumnName = "SubDealerName" 
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                            }
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsWorkflowOfSimpleApprovalStatus.已审核
                        }
                    }
                }
            };
        }
    }
}
