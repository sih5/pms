﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Web.Entities;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class ServiceAgentsHistoryQueryQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvDealerAgent = new ObservableCollection<KeyValuePair>();
        private readonly ServiceAgentsHistoryQueryQueryPanelViewModel viewModel = new ServiceAgentsHistoryQueryQueryPanelViewModel();

        public ServiceAgentsHistoryQueryQueryPanel() {
            Initializer.Register(Initialize);
            this.Initializer.Register(this.viewModel.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetBranchesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                }
                this.kvBranches.Clear();
                foreach(var branch in loadOp.Entities)
                    this.kvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Code
                    });
                dcsDomainContext.Load(dcsDomainContext.GetAgencyAffiBranchesQuery(), LoadBehavior.RefreshCurrent, loadOp3 => {
                    if(loadOp3.HasError)
                        return;
                    var serviceInfo = loadOp3.Entities.Select(t => t.AgencyId).ToArray().Distinct();
                    dcsDomainContext.Load(dcsDomainContext.GetCompaniesQuery().Where(tt => tt.Type == 3 || tt.Type == 7).OrderBy(r => r.Name), LoadBehavior.RefreshCurrent, loadOp4 => {
                        if(loadOp4.HasError)
                            return;
                        this.kvDealerAgent.Clear();
                        foreach(var dealerAgent in loadOp4.Entities) {
                            if(serviceInfo.Contains(dealerAgent.Id)) {
                                this.kvDealerAgent.Add(new KeyValuePair {
                                    Key = dealerAgent.Id,
                                    Value = dealerAgent.Name
                                });
                            }

                        }
                    }, null);
                }, null);
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetCompaniesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var company = loadOp.Entities.SingleOrDefault();
                QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.QueryPanel_Title_ServiceAgentsHistoryQuery,
                    EntityType = typeof(ServiceAgentsHistoryQuery),
                    QueryItems = new QueryItem[] {
                        new KeyValuesQueryItem {
                            Title = PartsSalesUIStrings.QueryPanel_Title_BranchName,
                            ColumnName = "BranchId",
                            KeyValueItems = kvBranches,
                            DefaultValue = company != null && company.Type == (int)DcsCompanyType.分公司 ? BaseApp.Current.CurrentUserData.EnterpriseId : 0,
                            IsEnabled = BaseApp.Current.CurrentUserData.UserCode == "ReportAdmin"
                        },
                        new ComboQueryItem {
                            Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesCategory_Name,
                            ColumnName = "SalesCategoryId",
                            SelectedValuePath = "Id",
                            DisplayMemberPath = "Name",
                            ItemsSource = this.viewModel.KvPartsSalesCategorys,
                            SelectedItemBinding = new Binding("SelectedPartsSalesCategory") {
                                Source = this.viewModel,
                                Mode = BindingMode.TwoWay
                            },

                        },
                        new KeyValuesQueryItem {
                            Title = PartsSalesUIStrings.QueryPanel_Title_VehiclePartsSalesOrdercsQuery_SalesUnitOwnerCompanyId,
                            ColumnName = "SalesUnitOwnerCompanyId",
                            KeyValueItems = kvDealerAgent
                        },
                        new ComboQueryItem {
                            Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesSettlementRef_SourceType,
                            ColumnName = "PartsSalesOrderTypeId",
                            ItemsSource = this.viewModel.PartsPurchaseOrderTypes,
                            SelectedValuePath = "Id",
                            DisplayMemberPath = "Name"
                        },
                        new QueryItem {
                            Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsStock_DealerCode,
                            ColumnName = "SubmitCompanyCode"
                        },
                        new QueryItem {
                            Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsStock_DealerName,
                            ColumnName = "SubmitCompanyName"
                        },
                        new QueryItem {
                            Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesRtnSettlementRef_SourceCode,
                            ColumnName = "Code"
                        },
                        new DateTimeRangeQueryItem {
                            Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesSettlementRef_SourceBillCreateTime,
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                DateTime.Now.AddMonths(-1), DateTime.Now
                            }
                        },
                        new DateTimeRangeQueryItem {
                            Title = PartsSalesUIStrings.QueryPanel_Title_ServiceAgentsHistoryQuery_SubmitTime,
                            ColumnName = "SubmitTime",
                            DefaultValue = new[] {
                                DateTime.Now.AddMonths(-1), DateTime.Now
                            }
                        }
                    }
                }
            };
            }, null);
        }


        public class ServiceAgentsHistoryQueryQueryPanelViewModel : ViewModelBase {

            public ObservableCollection<PartsSalesCategory> kvPartsSalesCategorys;
            private PartsSalesCategory partsSalesCategory;
            private PagedCollectionView partsPurchaseOrderTypes;

            public readonly ObservableCollection<PartsSalesOrderType> allPartsPurchaseOrderTypes = new ObservableCollection<PartsSalesOrderType>();

            public ObservableCollection<PartsSalesCategory> KvPartsSalesCategorys {
                get {
                    if(this.kvPartsSalesCategorys == null)
                        this.kvPartsSalesCategorys = new ObservableCollection<PartsSalesCategory>();
                    return kvPartsSalesCategorys;
                }
            }

            public PartsSalesCategory SelectedPartsSalesCategory {
                get {
                    return this.partsSalesCategory;
                }
                set {
                    if(this.partsSalesCategory == value)
                        return;
                    this.partsSalesCategory = value;
                    this.NotifyOfPropertyChange("PartsSalesCategory");
                    this.PartsPurchaseOrderTypes.Refresh();
                }
            }

            public PagedCollectionView PartsPurchaseOrderTypes {
                get {
                    if(this.partsPurchaseOrderTypes == null) {
                        this.partsPurchaseOrderTypes = new PagedCollectionView(this.allPartsPurchaseOrderTypes);
                        this.partsPurchaseOrderTypes.Filter = o => ((PartsSalesOrderType)o).PartsSalesCategoryId == (this.SelectedPartsSalesCategory != null ? this.SelectedPartsSalesCategory.Id : int.MinValue);
                    }
                    return this.partsPurchaseOrderTypes;
                }
            }

            public override void Validate() {
                //
            }

            public void Initialize() {
                var domainContext = new DcsDomainContext();
                domainContext.Load(domainContext.GetPartsSalesOrderTypesQuery().Where(e => e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                    if(loadOp.HasError)
                        return;
                    foreach(var orderTypes in loadOp.Entities)
                        this.allPartsPurchaseOrderTypes.Add(orderTypes);
                }, null);
                domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(e => e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError)
                        return;
                    this.KvPartsSalesCategorys.Clear();
                    foreach(var partsSalesCategories in loadOp.Entities)
                        this.KvPartsSalesCategorys.Add(partsSalesCategories);
                }, null);
            }
        }
    }
}
