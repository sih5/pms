﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class SettlementAutomaticTaskSetQueryPanel : DcsQueryPanelBase{
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();   
        private readonly string[] kvNames = {
            "SettlementAutomaticTaskStatus"
        };
        public SettlementAutomaticTaskSetQueryPanel() {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            //var dcsDomainContext = new DcsDomainContext();
            //dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    foreach(var partsSalesCategorys in loadOp.Entities)
            //        this.kvPartsSalesCategorys.Add(new KeyValuePair {
            //            Key = partsSalesCategorys.Id,
            //            Value = partsSalesCategorys.Name
            //        });
            //}, null);
            this.QueryItemGroups = new[] { 
               new QueryItemGroup{
                  UniqueId = "Common",
                   Title = PartsSalesUIStrings.QueryPanel_Title_SettlementAutomaticTaskSet,
                   EntityType = typeof(SettlementAutomaticTaskSet),
                   QueryItems = new QueryItem [] {
                      //new KeyValuesQueryItem {
                      //     ColumnName = "BrandId",
                      //     KeyValueItems = this.kvPartsSalesCategorys,
                      //     Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesCategory_Name
                      //},
                      new KeyValuesQueryItem {
                          ColumnName = "Status",
                          Title = PartsSalesUIStrings.QueryPanel_Title_AgencyLogisticCompany_Status,
                          KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                      }, new DateTimeRangeQueryItem {
                          ColumnName = "CreateTime",
                          Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesSettlementRef_SourceBillCreateTime,
                          DefaultValue = new[] {
                              new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                          }
                      }
                   }
               }
            };
        }
    }
}
