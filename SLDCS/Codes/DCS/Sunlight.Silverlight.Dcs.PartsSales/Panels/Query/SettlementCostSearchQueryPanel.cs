﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class SettlementCostSearchQueryPanel : DcsQueryPanelBase {
        public SettlementCostSearchQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.Action_Title_SettlementCostSearch,
                    EntityType = typeof(VirtualPartsSalesRtnSettlement),
                    QueryItems = new[] {
                        new QueryItem { 
                            ColumnName = "Code",
                            Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesSettlement_Code
                        }
                    }
                }
            };
        }
    }
}