﻿
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class SparePart1QueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "MasterData_Status","SparePart_MeasureUnit"
        };
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();

        public SparePart1QueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategory in loadOp.Entities)
                    this.kvPartsSalesCategoryName.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(VirtualSparePart),
                    Title = PartsSalesUIStrings.QueryPanel_Title_VirtualSparePartQuery,
                    QueryItems = new[] {
                        new KeyValuesQueryItem {
                            Title= PartsSalesUIStrings.DataEditPanel_Text_PartsSalesCategory_Name,
                            ColumnName = "PartsSalesCategoryId",
                            KeyValueItems = this.kvPartsSalesCategoryName,
                            IsEnabled = false
                        },
                        new QueryItem {
                            ColumnName = "Code",
                            Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode
                        }, new QueryItem {
                            ColumnName = "Name"
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsMasterDataStatus.有效,
                            Title = PartsSalesUIStrings.QueryPanel_Title_AgencyLogisticCompany_Status
                        }
                        //,new KeyValuesQueryItem{
                        //    ColumnName="MeasureUnit",
                        //    KeyValueItems=this.KeyValueManager[this.kvNames[1]]
                        //},new QueryItem{
                        //    ColumnName="Specification" 
                        //}
                        ,new QueryItem{
                            ColumnName="ReferenceCode",
                            Title =PartsSalesUIStrings.QueryPanel_Title_ReferenceCode
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesSettlementRef_SourceBillCreateTime
                        },new DateTimeRangeQueryItem {
                            ColumnName = "ModifyTime",
                            Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_ModifyTime
                        },new QueryItem{
                            ColumnName="OverseasPartsFigure",
                            Title =PartsSalesUIStrings.QueryPanel_Title_VirtualSparePartQuery_OverseasPartsFigure
                        }
                    }
                }
            };
        }
    }
}
