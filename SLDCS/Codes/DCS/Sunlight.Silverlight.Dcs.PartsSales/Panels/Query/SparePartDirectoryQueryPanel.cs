﻿using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class SparePartDirectoryQueryPanel : DcsQueryPanelBase {
        private readonly SparePartDirectoryViewModel viewModel = new SparePartDirectoryViewModel();

        public SparePartDirectoryQueryPanel() {
            Initializer.Register(Initialize);
        }

        private void Initialize() {
            QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = "查询电子目录图册",
                    //EntityType = typeof(PMSEPCBRANDMAPPING),
                    QueryItems = new QueryItem[] {
                        new ComboQueryItem {
                            Title = "PMS品牌",
                            ColumnName = "PMSBrandId",
                            SelectedValuePath = "Id",
                            DisplayMemberPath = "Name",
                            //ItemsSource = this.viewModel.PmsBrands,
                            SelectedItemBinding = new Binding("PMSBrand") {
                                Source = this.viewModel,
                                Mode = BindingMode.TwoWay
                            }
                        },
                        new ComboQueryItem {
                            Title = "EPC品牌",
                            ColumnName = "EPCBrandId",
                            SelectedValuePath = "EPCBrandId",
                            DisplayMemberPath = "EPCBrandName",
                            //ItemsSource = this.viewModel.EPCBrand
                        }
                    }
                }
            };
        }

        public class SparePartDirectoryViewModel : ViewModelBase {
            //private readonly ObservableCollection<PartsSalesCategory> pmsBrands = new ObservableCollection<PartsSalesCategory>();
            //private readonly ObservableCollection<PMSEPCBRANDMAPPING> epcBrands = new ObservableCollection<PMSEPCBRANDMAPPING>();
            //private PagedCollectionView epcPartsSalesCategory;
            //private PartsSalesCategory pmsPartsSalesCategory;

            //public ObservableCollection<PartsSalesCategory> PmsBrands {
            //    get {
            //        return this.pmsBrands;
            //    }
            //}

            //public PartsSalesCategory PMSBrand {
            //    get {
            //        return this.pmsPartsSalesCategory;
            //    }
            //    set {
            //        if(this.pmsPartsSalesCategory == value)
            //            return;
            //        this.pmsPartsSalesCategory = value;
            //        this.NotifyOfPropertyChange("PMSBrand");
            //        this.EPCBrand.Refresh();
            //    }
            //}

            //public PagedCollectionView EPCBrand {
            //    get {
            //        //if(this.epcPartsSalesCategory == null) {
            //        //    this.epcPartsSalesCategory = new PagedCollectionView(this.epcBrands);
            //        //    this.epcPartsSalesCategory.Filter = o => ((PMSEPCBRANDMAPPING)o).PMSBrandId == (this.PMSBrand != null ? this.PMSBrand.Id : int.MinValue);
            //        //}
            //        return this.epcPartsSalesCategory;
            //    }
            //}


            public SparePartDirectoryViewModel() {
                this.Initialize();
            }

            public void Initialize() {
                var domainContext = new DcsDomainContext();
                //domainContext.Load(domainContext.GetCompaniesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                //    if(loadOp.HasError)
                //        return;
                //    var company = loadOp.Entities.SingleOrDefault();
                //    if(company == null)
                //        return;
                //    if(company.Type == (int)DcsCompanyType.分公司) {
                //        domainContext.Load(domainContext.GetPartsSalesCategoriesByEpcSysCodeQuery().Where(r => r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp1 => {
                //            if(loadOp1.HasError)
                //                return;
                //            foreach(var entity in loadOp1.Entities) {
                //                this.pmsBrands.Add(entity);
                //            }
                //        }, null);
                //    }
                //    if(company.Type == (int)DcsCompanyType.服务站 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                //        domainContext.Load(domainContext.GetDealerServiceInfoesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.DealerId == company.Id), LoadBehavior.RefreshCurrent, loadOp2 => {
                //            if(loadOp2.HasError)
                //                return;
                //            var dealerCompany = loadOp2.Entities.Select(r => r.PartsSalesCategoryId).ToArray();
                //            domainContext.Load(domainContext.GetPartsSalesCategoriesByDealerServiceInfoOrAgencyAffiBranchEpcQuery(dealerCompany), LoadBehavior.RefreshCurrent, dealerCompanys => {
                //                if(dealerCompanys.HasError)
                //                    return;
                //                foreach(var entity in dealerCompanys.Entities) {
                //                    this.pmsBrands.Add(entity);
                //                }
                //            }, null);
                //        }, null);
                //    }
                //    if(company.Type == (int)DcsCompanyType.代理库) {



                //        domainContext.Load(domainContext.GetSalesUnitsQuery().Where(r => r.OwnerCompanyId == company.Id), LoadBehavior.RefreshCurrent, loadOp3 => {
                //            if(loadOp3.HasError)
                //                return;
                //            var agencyCompany = loadOp3.Entities.Select(r => r.PartsSalesCategoryId).ToArray();
                //            domainContext.Load(domainContext.GetPartsSalesCategoriesByDealerServiceInfoOrAgencyAffiBranchEpcQuery(agencyCompany), LoadBehavior.RefreshCurrent, agencyCompanys => {
                //                if(agencyCompanys.HasError)
                //                    return;
                //                foreach(var entity in agencyCompanys.Entities) {
                //                    this.pmsBrands.Add(entity);
                //                }
                //            }, null);
                //        }, null);
                //    }
                //}, null);
                //domainContext.Load(domainContext.GetPMSEPCBRANDMAPPINGsQuery().Where(r => r.Syscode == (int)DcsSysCode.EPC && r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp4 => {
                //    if(loadOp4.HasError)
                //        return;
                //    foreach(var epcPartsSalesCategory in loadOp4.Entities)
                //        this.epcBrands.Add(epcPartsSalesCategory);
                //}, null);
            }

            public override void Validate() {
                throw new System.NotImplementedException();
            }
        }

    }
}
