﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class SparePartForCrossSalesOrderQueryPanel : DcsQueryPanelBase {
     
        public SparePartForCrossSalesOrderQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(VirtualSparePart),
                    Title = "配件选择",
                    QueryItems = new[] {
                       new QueryItem {
                            ColumnName = "Code",
                            Title ="配件编号"
                        }, new QueryItem {
                            ColumnName = "Name",
                            Title = "配件名称"
                        }
                    }
                }
            };
        }
    }
}
