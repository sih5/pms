﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class SparePartsInfoAndPriceQueryPanel : DcsQueryPanelBase {

        private readonly string[] kvNames = {
            "MasterData_Status"
        };

        public SparePartsInfoAndPriceQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(kvNames);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title =PartsSalesUIStrings.QueryPanel_Title_SparePartsInfoAndPrice,
                    EntityType = typeof(PartsRetailGuidePrice),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "SparePartCode"
                        },new QueryItem {
                            ColumnName = "SparePartName"
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            DefaultValue = (int)DcsMasterDataStatus.有效,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }
                    }
                }
            };
        }
    }
}
