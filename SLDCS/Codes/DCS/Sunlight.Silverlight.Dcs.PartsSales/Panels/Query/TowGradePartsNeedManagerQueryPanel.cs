﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class TowGradePartsNeedManagerQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "SecondClassStationPlan_Status"
        };

        public TowGradePartsNeedManagerQueryPanel() {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.kvPartsSalesCategorys.Clear();
                foreach(var item in loadOp.Entities) {
                    this.kvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.Name
                    });
                }
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.QueryPanel_Title_SecondClassStationPlan,
                    EntityType = typeof(SecondClassStationPlan),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "SecondClassStationName",
                            Title=PartsSalesUIStrings.QueryPanel_QueryItem_Title_SecondClassStationPlan_SecondClassStationName
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue =new []{
                                 DateTime.Now.Date.AddDays(-7), DateTime.Now
                            }
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new KeyValuesQueryItem {
                            ColumnName = "PartsSalesCategoryId",
                            KeyValueItems = this.kvPartsSalesCategorys,
                            Title=PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsRetailOrder_PartsSalesCategoryName
                        }
                    }
                }

            };
        }
    }
}
