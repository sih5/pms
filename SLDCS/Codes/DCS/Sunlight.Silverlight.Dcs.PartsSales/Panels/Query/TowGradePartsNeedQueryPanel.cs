﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class TowGradePartsNeedQueryPanel : DcsQueryPanelBase {
        private ObservableCollection<KeyValuePair> kvPartsSalesCategory = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = new[] {
            "SecondClassStationPlan_Status"
        };

        public TowGradePartsNeedQueryPanel() {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            //dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
            if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategory in loadOp.Entities)
                    this.kvPartsSalesCategory.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });

                this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = PartsSalesUIStrings.QueryPanel_Title_TowGradePartsNeed,
                        EntityType = typeof(SecondClassStationPlan),
                        QueryItems = new QueryItem[] {
                            new QueryItem {
                                ColumnName = "SecondClassStationName",
                                Title=PartsSalesUIStrings.QueryPanel_QueryItem_Title_SecondClassStationPlan_SecondClassStationName 
                            }, new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                IsExact = false,
                                DefaultValue = new[] {
                                    DateTime.Now.Date.AddDays(-7), DateTime.Now.Date
                                }
                            },new  KeyValuesQueryItem{
                                     ColumnName = "Status",
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                            },new KeyValuesQueryItem{
                              ColumnName = "PartsSalesCategoryId",
                                Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name,
                                KeyValueItems = this.kvPartsSalesCategory
                            }
                        }
                    }
                };
            }, null);
        }
    }
}
