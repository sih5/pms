﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class VirtualPartsSalesSettlementQueryPanel : DcsQueryPanelBase {
        public VirtualPartsSalesSettlementQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title =PartsSalesUIStrings.Action_Title_SettlementCost,
                    EntityType = typeof(VirtualPartsSalesSettlement),
                    QueryItems = new[] {
                        new QueryItem { 
                            ColumnName = "Code",
                            Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesSettlement_Code
                        }
                    }
                }
            };
        }
    }
}
