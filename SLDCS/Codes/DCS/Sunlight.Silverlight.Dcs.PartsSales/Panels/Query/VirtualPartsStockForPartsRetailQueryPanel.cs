﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Panels.Query {
    public class VirtualPartsStockForPartsRetailQueryPanel : DcsQueryPanelBase {
        public VirtualPartsStockForPartsRetailQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsSalesUIStrings.QueryPanel_Title_VirtualPartsStock,
                    EntityType = typeof(VirtualPartsStock),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "SparePartCode"
                        }, new QueryItem {
                            ColumnName = "SparePartName"
                        }
                    }
                }
            };
        }
    }
}
