﻿using System.Linq;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs.PartsSales {
    public class UIStrings : ModelBase {
        public void Refresh(params string[] propertyNames) {
            this.NotifyOfPropertiesChange(propertyNames.Length == 0 ? this.GetType().GetProperties().Select(p => p.Name).ToArray() : propertyNames);
        }   

            public string DataEditPanel_Text_Common_Span_To {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_Common_Span_To;
            }
        }

        public string DataEditPanel_Text_Common_Unit_Currency {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_Common_Unit_Currency;
            }
        }

        public string DataEditPanel_Text_Common_Unit_Km {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_Common_Unit_Km;
            }
        }

        public string DataEditPanel_Text_Common_Unit_Month {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_Common_Unit_Month;
            }
        }

        public string DataEditPanel_Text_Common_BranchName {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_Common_BranchName;
            }
        }

        public string DetailPanel_Text_Common_BranchName {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_Common_BranchName;
            }
        }

        public string DetailPanel_Title_CustomerOrderPriceGradeHistory {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Title_CustomerOrderPriceGradeHistory;
            }
        }

        public string DataEditPanel_Text_CustomerOrderPriceGrade_Coefficient {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_CustomerOrderPriceGrade_Coefficient;
            }
        }

        public string DataEditPanel_Text_CustomerOrderPriceGrade_CustomerCompanyCode {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_CustomerOrderPriceGrade_CustomerCompanyCode;
            }
        }

        public string DataEditPanel_Text_CustomerOrderPriceGrade_CustomerCompanyName {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_CustomerOrderPriceGrade_CustomerCompanyName;
            }
        }

        public string DataEditView_Validation_CustomerOrderPriceGrade_OrderPriceAndGeadIsNull {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Validation_CustomerOrderPriceGrade_OrderPriceAndGeadIsNull;
            }
        }

        public string QueryPanel_Title_SparePart {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_SparePart;
            }
        }

        public string QueryPanel_Title_Dealer {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_Dealer;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsPurchasePricingDetail_RetailGuidePrice {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsPurchasePricingDetail_RetailGuidePrice;
            }
        }

        public string QueryPanel_Title_CustomerCompany {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_CustomerCompany;
            }
        }

        public string DetailPanel_Text_PartsSalesOrder_IfDirectProvision {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsSalesOrder_IfDirectProvision;
            }
        }

        public string DetailPanel_Text_PartsSalesOrder_SalesUnitName {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsSalesOrder_SalesUnitName;
            }
        }

        public string DetailPanel_Text_PartsSalesOrder_SubmitCompanyName {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsSalesOrder_SubmitCompanyName;
            }
        }

        public string DetailPanel_Title_PartsSalesOrderForApprove {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Title_PartsSalesOrderForApprove;
            }
        }

        public string DataEditPanel_Text_InternalAcquisitionBill_DepartmentName {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_InternalAcquisitionBill_DepartmentName;
            }
        }

        public string DataEditPanel_Text_InternalAcquisitionBill_WarehouseName {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_InternalAcquisitionBill_WarehouseName;
            }
        }

        public string DataEditPanel_Title_PartsSalesOrderProcess {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Title_PartsSalesOrderProcess;
            }
        }

        public string DetailPanel_Text_PartsSalesOrder_Warehouse {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsSalesOrder_Warehouse;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsSalesReturnBill_ReturnCompanyName {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesReturnBill_ReturnCompanyName;
            }
        }

        public string DetailPanel_Text_PartsSalesReturnBill_InvoiceRequirement {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsSalesReturnBill_InvoiceRequirement;
            }
        }

        public string DetailPanel_Text_PartsSalesReturnBill_PartsSalesOrderCode {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsSalesReturnBill_PartsSalesOrderCode;
            }
        }

        public string DetailPanel_Text_PartsSalesReturnBill_ReturnCompanyName {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsSalesReturnBill_ReturnCompanyName;
            }
        }

        public string DetailPanel_Text_PartsSalesReturnBill_SalesUnitName {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsSalesReturnBill_SalesUnitName;
            }
        }

        public string DetailPanel_Text_PartsSalesReturnBill_Status {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsSalesReturnBill_Status;
            }
        }

        public string DetailPanel_Text_PartsSalesReturnBill_TotalAmount {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsSalesReturnBill_TotalAmount;
            }
        }

        public string DataEditPanel_GroupTitle_PartsSalesOrderProcessDetailForSupplyApprove {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_GroupTitle_PartsSalesOrderProcessDetailForSupplyApprove;
            }
        }

        public string DataEditPanel_GroupTitle_CheckSupplierCompany {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_GroupTitle_CheckSupplierCompany;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrderProcessDetail_SupplierCompanyName {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrderProcessDetail_SupplierCompanyName;
            }
        }

        public string DataEditPanel_GroupTitle_PrimaryInfo {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_GroupTitle_PrimaryInfo;
            }
        }

        public string QueryPanel_Title_PartsSalesOrder {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_PartsSalesOrder;
            }
        }

        public string DetailPanel_Text_PartsSalesOrder_ContactPhone {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsSalesOrder_ContactPhone;
            }
        }

        public string DetailPanel_Text_PartsSalesOrder_IfAgencyService {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsSalesOrder_IfAgencyService;
            }
        }

        public string DetailPanel_Text_PartsSalesOrder_InvoiceReceiveCompanyName {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsSalesOrder_InvoiceReceiveCompanyName;
            }
        }

        public string DetailPanel_Text_PartsSalesOrder_PartsSalesOrderTypeName {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsSalesOrder_PartsSalesOrderTypeName;
            }
        }

        public string DetailPanel_Text_PartsSalesOrder_SalesUnitOwnerCompanyName {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsSalesOrder_SalesUnitOwnerCompanyName;
            }
        }

        public string DataEditView_GroupTitle_CustomerAccountInfo {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_GroupTitle_CustomerAccountInfo;
            }
        }

        public string DetailPanel_Text_CustomerAccount_CustomerCredenceAmount {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_CustomerAccount_CustomerCredenceAmount;
            }
        }

        public string DataEditPanel_GroupTitle_PartsSalesOrderProcess {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_GroupTitle_PartsSalesOrderProcess;
            }
        }

        public string DataEditPanel_Text_SalesUnit_AccountGroup {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_SalesUnit_AccountGroup;
            }
        }

        public string DataEditPanel_Text_SalesUnit_OwnerCompany {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_SalesUnit_OwnerCompany;
            }
        }

        public string DataEditPanel_Text_SalesUnit_PartsSalesCategory {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_SalesUnit_PartsSalesCategory;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrder_PartsSalesOrderTypeName {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_PartsSalesOrderTypeName;
            }
        }

        public string DataEditPanel_Text_PartsSalesReturnBill_ReturnCompanyName {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_ReturnCompanyName;
            }
        }

        public string DetailPanel_Text_PartsSalesReturnBill_SalesUnitName2 {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsSalesReturnBill_SalesUnitName2;
            }
        }

        public string DataEditPanel_Text_PartsSalesReturnBill_BlueInvoiceNumber {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_BlueInvoiceNumber;
            }
        }

        public string DataEditPanel_Text_PartsSalesReturnBill_InvoiceRequirement {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_InvoiceRequirement;
            }
        }

        public string DataEditPanel_Text_PartsSalesReturnBill_PartsSalesOrderCode {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_PartsSalesOrderCode;
            }
        }

        public string DataEditPanel_Text_PartsSalesReturnBill_WarehouseName {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_WarehouseName;
            }
        }

        public string DataEditView_Title_CustomerCompany_Name {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Title_CustomerCompany_Name;
            }
        }

        public string DataEditView_Title_SalesCompany_Name {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Title_SalesCompany_Name;
            }
        }

        public string DetailPanel_Title_SalesCompany_Name {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Title_SalesCompany_Name;
            }
        }

        public string QueryPanel_Title_Company {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_Company;
            }
        }

        public string DetailPanel_Title_CustomerCompany_Name {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Title_CustomerCompany_Name;
            }
        }

        public string DataEditView_Text_CustomerAccount_UseablePosition {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Text_CustomerAccount_UseablePosition;
            }
        }

        public string DataEditPanel_Title_PartsSalesReturnBill_PartsSalesOrder {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Title_PartsSalesReturnBill_PartsSalesOrder;
            }
        }

        public string QueryPanel_Title_CustomerInformation {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_CustomerInformation;
            }
        }

        public string QueryPanel_Title_CustomerInformationBySparepart {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_CustomerInformationBySparepart;
            }
        }

        public string DetailPanel_Text_PartsRetailOrder_Code {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsRetailOrder_Code;
            }
        }

        public string DetailPanel_Text_PartsRetailOrder_OriginalAmount {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsRetailOrder_OriginalAmount;
            }
        }

        public string DataEditPanel_Text_PartsRetailOrder_WarehouseName {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsRetailOrder_WarehouseName;
            }
        }

        public string QueryPanel_Title_PartsRetailOrderForPartsOutboundBillDetail {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_PartsRetailOrderForPartsOutboundBillDetail;
            }
        }

        public string QueryPanel_Title_PartsRetailReturnBill_WarehouseName {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_PartsRetailReturnBill_WarehouseName;
            }
        }

        public string QueryPanel_Title_Warehouse {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_Warehouse;
            }
        }

        public string DetailPanel_Title_PartsSalesRtnSettlement_AccountGroupName {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Title_PartsSalesRtnSettlement_AccountGroupName;
            }
        }

        public string DetailPanel_Title_PartsSalesRtnSettlement_Code {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Title_PartsSalesRtnSettlement_Code;
            }
        }

        public string DetailPanel_Title_PartsSalesRtnSettlement_SalesCompanyName {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Title_PartsSalesRtnSettlement_SalesCompanyName;
            }
        }

        public string DataEditPanel_Text_PartsSalesRtnSettlement_PartsSupplier {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesRtnSettlement_PartsSupplier;
            }
        }

        public string DataEditPanel_Title_CustomerInformationForPartsSalesReturn {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Title_CustomerInformationForPartsSalesReturn;
            }
        }

        public string Button_Text_Common_Search {
            get {
                return Resources.PartsSalesUIStrings.Button_Text_Common_Search;
            }
        }

        public string Button_Text_Common_Selected {
            get {
                return Resources.PartsSalesUIStrings.Button_Text_Common_Selected;
            }
        }

        public string DataEditPanel_Text_PartsSalesRtnSettlement_TotalSettlementAmount {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesRtnSettlement_TotalSettlementAmount;
            }
        }

        public string DataPanel_Text_CustomerOrderPriceGrade_PartsSalesOrderTypeCode {
            get {
                return Resources.PartsSalesUIStrings.DataPanel_Text_CustomerOrderPriceGrade_PartsSalesOrderTypeCode;
            }
        }

        public string DataPanel_Text_CustomerOrderPriceGrade_PartsSalesOrderTypeName {
            get {
                return Resources.PartsSalesUIStrings.DataPanel_Text_CustomerOrderPriceGrade_PartsSalesOrderTypeName;
            }
        }

        public string DataEditView_Text_Button_Search {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Text_Button_Search;
            }
        }

        public string DetailPanel_Text_PartsSalesSettlement_AccountGroupName {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsSalesSettlement_AccountGroupName;
            }
        }

        public string DetailPanel_Text_PartsSalesSettlement_Code {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsSalesSettlement_Code;
            }
        }

        public string DetailPanel_Text_PartsSalesSettlement_SalesCompanyName {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsSalesSettlement_SalesCompanyName;
            }
        }

        public string DataEditPanel_Text_PartsSalesSettlement_RebateAmount {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesSettlement_RebateAmount;
            }
        }

        public string DataEditPanel_Text_PartsSalesSettlement_TotalSettlementAmount {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesSettlement_TotalSettlementAmount;
            }
        }

        public string DataEditView_Text_Button_Save {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Text_Button_Save;
            }
        }

        public string DataEditPanel_Text_PartsSalesRtnSettlement_CutoffTime {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesRtnSettlement_CutoffTime;
            }
        }

        public string QueryPanel_Title_Agency {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_Agency;
            }
        }

        public string DataEditView_Title_PartsSalesSettlement_AccountGroupName {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Title_PartsSalesSettlement_AccountGroupName;
            }
        }

        public string DataEditView_Title_PartsSalesSettlement_CutOffTime {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Title_PartsSalesSettlement_CutOffTime;
            }
        }

        public string DataEditView_Title_PartsSalesSettlement_TotalSettlementAmount {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Title_PartsSalesSettlement_TotalSettlementAmount;
            }
        }

        public string DataEditView_GroupTitle_OrderReceivingInfo {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_GroupTitle_OrderReceivingInfo;
            }
        }

        public string Button_Text_Common_Apply {
            get {
                return Resources.PartsSalesUIStrings.Button_Text_Common_Apply;
            }
        }

        public string DataEditPanel_GroupTitle_BillInfo {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_GroupTitle_BillInfo;
            }
        }

        public string DataEditPanel_Title_PartsSalesSettlement_RebateAmount {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Title_PartsSalesSettlement_RebateAmount;
            }
        }

        public string DataEditView_Validation_PartsSalesOrderProcessDetail_CurrentFulfilledQuantityIsZeroError {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderProcessDetail_CurrentFulfilledQuantityIsZeroError;
            }
        }

        public string DataManagementView_Title_PartsSalesSettlementQuery {
            get {
                return Resources.PartsSalesUIStrings.DataManagementView_Title_PartsSalesSettlementQuery;
            }
        }

        public string DataEditView_Confirm_PartsSalesOrder_CompanyStockSatisfyOrder {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Confirm_PartsSalesOrder_CompanyStockSatisfyOrder;
            }
        }

        public string DataEditView_Title_PartsRetailReturnBill_SourceCode {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Title_PartsRetailReturnBill_SourceCode;
            }
        }

        public string DataEditView_Title_PartsRetailReturnBill_WarehouseName {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Title_PartsRetailReturnBill_WarehouseName;
            }
        }

        public string DetailPanel_Text_PartsSalesOrder_ReceivingCompanyName {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsSalesOrder_ReceivingCompanyName;
            }
        }

        public string DetailPanel_Text_PartsSalesOrder_TotalAmount {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsSalesOrder_TotalAmount;
            }
        }

        public string DataEditView_PartsSalesCategory_Code {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_PartsSalesCategory_Code;
            }
        }

        public string DataEditView_PartsSalesCategory_Name {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_PartsSalesCategory_Name;
            }
        }

        public string DataEditView_Validation_PartsSalesCategory_CodeIsNull {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Validation_PartsSalesCategory_CodeIsNull;
            }
        }

        public string DataEditView_Validation_PartsSalesCategory_NameIsNull {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Validation_PartsSalesCategory_NameIsNull;
            }
        }

        public string DataEditView_Validation_PartsSalesPriceChange_PartsSalesCategory {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Validation_PartsSalesPriceChange_PartsSalesCategory;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerPartsRetailOrder_Code {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsRetailOrder_Code;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerPartsRetailOrder_DealerCode {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsRetailOrder_DealerCode;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerPartsRetailOrder_DealerName {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsRetailOrder_DealerName;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerPartsRetailOrder_SalesCategoryName {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsRetailOrder_SalesCategoryName;
            }
        }

        public string QueryPanel_QueryItem_Title_DealerPartsRetailOrder_BranchName {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsRetailOrder_BranchName;
            }
        }

        public string QueryPanel_QueryItem_Title_DealerPartsRetailOrder_Code {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsRetailOrder_Code;
            }
        }

        public string QueryPanel_QueryItem_Title_DealerPartsRetailOrder_PartsSalesCategoryName {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsRetailOrder_PartsSalesCategoryName;
            }
        }

        public string DataManagementView_Title_DealerPartsRetailOrder {
            get {
                return Resources.PartsSalesUIStrings.DataManagementView_Title_DealerPartsRetailOrder;
            }
        }

        public string DataEditView_Text_DealerPartsRetailOrder_BranchName {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Text_DealerPartsRetailOrder_BranchName;
            }
        }

        public string DataEditView_Text_DealerPartsRetailOrder_Comment {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Text_DealerPartsRetailOrder_Comment;
            }
        }

        public string DataEditView_Text_DealerPartsRetailOrder_PartsSalesCategoryName {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Text_DealerPartsRetailOrder_PartsSalesCategoryName;
            }
        }

        public string DataEditView_Text_DealerPartsRetailOrder_SalesCategoryName {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Text_DealerPartsRetailOrder_SalesCategoryName;
            }
        }

        public string DataEditView_Text_DealerPartsRetailOrder_DealerCode {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Text_DealerPartsRetailOrder_DealerCode;
            }
        }

        public string DataEditView_Text_DealerPartsRetailOrder_DealerName {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Text_DealerPartsRetailOrder_DealerName;
            }
        }

        public string DataEditView_Error_DealerPartsStock_DoubleDealerPartsStockID {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Error_DealerPartsStock_DoubleDealerPartsStockID;
            }
        }

        public string DataEditPanel_Text_PartsSalesCategory_Name {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesCategory_Name;
            }
        }

        public string QueryPanel_Title_DealerPartsRetailOrderQueryWindow {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_DealerPartsRetailOrderQueryWindow;
            }
        }

        public string DataEditView_Text_DealerPartsRetailOrder_ParentChanelDealerCode {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Text_DealerPartsRetailOrder_ParentChanelDealerCode;
            }
        }

        public string DataEditView_Text_DealerPartsRetailOrder_ParentChanelDealerName {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Text_DealerPartsRetailOrder_ParentChanelDealerName;
            }
        }

        public string Notification_SalesUnitAffiWarehouse_WarehouseIdAndSalesUnitAffiWarehouseWarehouseIdCanNotSame {
            get {
                return Resources.PartsSalesUIStrings.Notification_SalesUnitAffiWarehouse_WarehouseIdAndSalesUnitAffiWarehouseWarehouseIdCanNotSame;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsManagementCostGrade_PartsSalesCategory {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsManagementCostGrade_PartsSalesCategory;
            }
        }

        public string DetailPanel_Text_PartsSalesOrder_BranchName {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsSalesOrder_BranchName;
            }
        }

        public string DetailPanel_Text_PartsSalesOrder_Code {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsSalesOrder_Code;
            }
        }

        public string DetailPanel_Text_PartsSalesOrder_SalesCategoryName {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsSalesOrder_SalesCategoryName;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrderProcessDetails_DirectAccess {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrderProcessDetails_DirectAccess;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrderProcessDetails_IfDirectSupply {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrderProcessDetails_IfDirectSupply;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrderProcessDetails_SendRequest {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrderProcessDetails_SendRequest;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrderProcessDetails_SupplierCompanyName {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrderProcessDetails_SupplierCompanyName;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrderProcessDetails_WarehouseName {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrderProcessDetails_WarehouseName;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrderProcessDetails_SupplierWarehouseName {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrderProcessDetails_SupplierWarehouseName;
            }
        }

        public string DetailPanel_Text_PartsSalesOrder_OrderProcess {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsSalesOrder_OrderProcess;
            }
        }

        public string DataEditView_Validation_PartsSalesOrder_InvoiceReceiveSaleCateIdIsNull {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_InvoiceReceiveSaleCateIdIsNull;
            }
        }

        public string DataEditView_Validation_PartsSalesOrder_ReceivingWarehouseNameIsNull {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_ReceivingWarehouseNameIsNull;
            }
        }

        public string DataEditView_Text_PartsSalesOrder_ReceiveSaleCateName {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Text_PartsSalesOrder_ReceiveSaleCateName;
            }
        }

        public string DataEditView_Text_PartsSalesOrder_ReceivingWarehouseName {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Text_PartsSalesOrder_ReceivingWarehouseName;
            }
        }

        public string DataManagementView_Confirm_Delete {
            get {
                return Resources.PartsSalesUIStrings.DataManagementView_Confirm_Delete;
            }
        }

        public string DataManagementView_Notification_DeleteSuccess {
            get {
                return Resources.PartsSalesUIStrings.DataManagementView_Notification_DeleteSuccess;
            }
        }

        public string DataEditView_Text_SecondClassStationPlan_FirstClassStationName {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Text_SecondClassStationPlan_FirstClassStationName;
            }
        }

        public string DataEditView_Text_SecondClassStationPlan_Remark {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Text_SecondClassStationPlan_Remark;
            }
        }

        public string DataEditView_Text_SecondClassStationPlan_SecondClassStationName {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Text_SecondClassStationPlan_SecondClassStationName;
            }
        }

        public string DetailPanel_Text_PartsSalesOrder_ReceivingWarehouseName {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsSalesOrder_ReceivingWarehouseName;
            }
        }

        public string DataEditView_DealerPartsRetailOrder_Customer {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_DealerPartsRetailOrder_Customer;
            }
        }

        public string DataEditView_Text_DealerPartsRetailOrder_SubDealerName {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Text_DealerPartsRetailOrder_SubDealerName;
            }
        }

        public string BusinessName_TowGradePartsRetailOrder {
            get {
                return Resources.PartsSalesUIStrings.BusinessName_TowGradePartsRetailOrder;
            }
        }

        public string BusinessName_TowGradePartsRetailOrderForApprove {
            get {
                return Resources.PartsSalesUIStrings.BusinessName_TowGradePartsRetailOrderForApprove;
            }
        }

        public string DataEditView_Text_PartsSalesCategory_Name {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Text_PartsSalesCategory_Name;
            }
        }

        public string DetailPanel_Text_PartsSalesOrder_SubmitCompanyName2 {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsSalesOrder_SubmitCompanyName2;
            }
        }

        public string DataEditView_Validation_PartsSalesOrderProcess_RequestedDeliveryTimeIsNull {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderProcess_RequestedDeliveryTimeIsNull;
            }
        }

        public string DataEditView_Text_Refurbish_RadButton {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Text_Refurbish_RadButton;
            }
        }

        public string DataEditView_Validation_PartsSalesOrder_WarehouseIdIdIsNull {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_WarehouseIdIdIsNull;
            }
        }

        public string DataGridView_Notification_PartsRetailOrderDetail_SelectedSparePartUsableQuantityLessThenZero {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Notification_PartsRetailOrderDetail_SelectedSparePartUsableQuantityLessThenZero;
            }
        }

        public string DataEditView_Notification_CompletedImport {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Notification_CompletedImport;
            }
        }

        public string DataEditView_Notification_CompletedImportWithError {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Notification_CompletedImportWithError;
            }
        }

        public string DataEditView_Validation_PartsSalesOrder_ApprovalCommentIsNull {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_ApprovalCommentIsNull;
            }
        }

        public string QueryPanel_Title_SubDealerQuery {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_SubDealerQuery;
            }
        }

        public string QueryPanel_Title_Dealer1 {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_Dealer1;
            }
        }

        public string DataEditView_Validation_DetailCountMoreThan200 {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Validation_DetailCountMoreThan200;
            }
        }

        public string DataEditView_Validation_PMSEPCBRANDMAPPING_EPCBrandCodeIsNull {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Validation_PMSEPCBRANDMAPPING_EPCBrandCodeIsNull;
            }
        }

        public string DataEditView_Validation_PMSEPCBRANDMAPPING_EPCBrandIdIsNull {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Validation_PMSEPCBRANDMAPPING_EPCBrandIdIsNull;
            }
        }

        public string DataEditView_Validation_PMSEPCBRANDMAPPING_EPCBrandNameIsNull {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Validation_PMSEPCBRANDMAPPING_EPCBrandNameIsNull;
            }
        }

        public string DataEditView_Validation_PMSEPCBRANDMAPPING_PMSBrandIdIsNull {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Validation_PMSEPCBRANDMAPPING_PMSBrandIdIsNull;
            }
        }

        public string DataEditView_Validation_PMSEPCBRANDMAPPING_SyscodeIsNull {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Validation_PMSEPCBRANDMAPPING_SyscodeIsNull;
            }
        }

        public string DataEditView_Confirm_PartsSalesSettlement_TotalAmountMoreThanAccountBalance {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Confirm_PartsSalesSettlement_TotalAmountMoreThanAccountBalance;
            }
        }

        public string QueryPanel_QueryItem_Tilte_PartsSalesOrder_City {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrder_City;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsSalesOrder_Province {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrder_Province;
            }
        }

        public string DetailPanel_Title_InvoiceInformation {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Title_InvoiceInformation;
            }
        }

        public string DataEditView_Validation_PartsSalesReturnBillDetail_SparePartIdIsDouble {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Validation_PartsSalesReturnBillDetail_SparePartIdIsDouble;
            }
        }

        public string DetailPanel_Title_PartsSalesSettlement_Code {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Title_PartsSalesSettlement_Code;
            }
        }

        public string DetailPanel_Title_PartsSalesSettlement_Detail {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Title_PartsSalesSettlement_Detail;
            }
        }

        public string DetailPanel_Title_PartsSalesSettlement_Type {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Title_PartsSalesSettlement_Type;
            }
        }

        public string DataEditView_Validation_PartsSalesOrder_WarehouseIdIsNull {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_WarehouseIdIsNull;
            }
        }

        public string DataGridView_ColumnItem_Title_ERPDelivery_Shop_Code {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDelivery_Shop_Code;
            }
        }

        public string DataManagementView_Title_PartsSalesPriceHistory {
            get {
                return Resources.PartsSalesUIStrings.DataManagementView_Title_PartsSalesPriceHistory;
            }
        }

        public string QueryPanel_QueryItem_Tilte_VirtualPartsSalesPriceHistory_CreateTime {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_VirtualPartsSalesPriceHistory_CreateTime;
            }
        }

        public string QueryPanel_QueryItem_Tilte_VirtualPartsSalesPriceHistory_SparePartCode {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_VirtualPartsSalesPriceHistory_SparePartCode;
            }
        }

        public string QueryPanel_QueryItem_Tilte_VirtualPartsSalesPriceHistory_SparePartName {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_VirtualPartsSalesPriceHistory_SparePartName;
            }
        }

        public string QueryPanel_QueryItem_Title_Common_PartsSalesCategory {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_QueryItem_Title_Common_PartsSalesCategory;
            }
        }

        public string QueryPanel_Title_PartsSalesPriceHistory {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_PartsSalesPriceHistory;
            }
        }

        public string DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_Code {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_Code;
            }
        }

        public string DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_CreateTime {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_CreateTime;
            }
        }

        public string DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_CreatorName {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_CreatorName;
            }
        }

        public string DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_DealerSalesPrice {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_DealerSalesPrice;
            }
        }

        public string DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_FinalApproverName {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_FinalApproverName;
            }
        }

        public string DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_FinalApproveTime {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_FinalApproveTime;
            }
        }

        public string DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_InitialApproverName {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_InitialApproverName;
            }
        }

        public string DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_InitialApproveTime {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_InitialApproveTime;
            }
        }

        public string DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_ModifierName {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_ModifierName;
            }
        }

        public string DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_ModifyTime {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_ModifyTime;
            }
        }

        public string DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_PartsSalesCategory {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_PartsSalesCategory;
            }
        }

        public string DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_PriceType {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_PriceType;
            }
        }

        public string DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_RetailGuidePrice {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_RetailGuidePrice;
            }
        }

        public string DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_SalesPrice {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_SalesPrice;
            }
        }

        public string DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_SparePartCode {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_SparePartCode;
            }
        }

        public string DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_SparePartName {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_SparePartName;
            }
        }

        public string DataGridView_ColumnItem_Title_VehiclePartsStockLevel_BrandName {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_BrandName;
            }
        }

        public string DataGridView_ColumnItem_Title_VehiclePartsStockLevel_CreateTime {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_CreateTime;
            }
        }

        public string DataGridView_ColumnItem_Title_VehiclePartsStockLevel_CreatorName {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_CreatorName;
            }
        }

        public string DataGridView_ColumnItem_Title_VehiclePartsStockLevel_ModifierName {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_ModifierName;
            }
        }

        public string DataGridView_ColumnItem_Title_VehiclePartsStockLevel_ModifyTime {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_ModifyTime;
            }
        }

        public string DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode;
            }
        }

        public string DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartName {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartName;
            }
        }

        public string DataGridView_ColumnItem_Title_VehiclePartsStockLevel_SafeStock {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_SafeStock;
            }
        }

        public string DataGridView_ColumnItem_Title_VehiclePartsStockLevel_StockMaximum {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_StockMaximum;
            }
        }

        public string DataGridView_ColumnItem_Title_VehiclePartsStockLevel_StockMinimum {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_StockMinimum;
            }
        }

        public string DataGridView_ColumnItem_Title_VehiclePartsStockLevel_WarehouseCode {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_WarehouseCode;
            }
        }

        public string DataGridView_ColumnItem_Title_VehiclePartsStockLevel_WarehouseName {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_WarehouseName;
            }
        }

        public string Action_Title_VehiclePartsStockLevel {
            get {
                return Resources.PartsSalesUIStrings.Action_Title_VehiclePartsStockLevel;
            }
        }

        public string DataManagementView_Title_VehiclePartsStockLevel {
            get {
                return Resources.PartsSalesUIStrings.DataManagementView_Title_VehiclePartsStockLevel;
            }
        }

        public string QueryPanel_Title_BonusPointsOrder {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_BonusPointsOrder;
            }
        }

        public string ActionPanel_Title_AgencyRetailerOrder {
            get {
                return Resources.PartsSalesUIStrings.ActionPanel_Title_AgencyRetailerOrder;
            }
        }

        public string DataManagementView_Title_PartsPackingProperty {
            get {
                return Resources.PartsSalesUIStrings.DataManagementView_Title_PartsPackingProperty;
            }
        }

        public string QueryPanel_Title_PartsPackingPropertyApp {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_PartsPackingPropertyApp;
            }
        }

        public string DetailPanel_Text_PartsSalesOrder_CenterCompanyName {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsSalesOrder_CenterCompanyName;
            }
        }

        public string DetailPanel_Text_PartsSalesOrder_CenterWarehouseCode {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsSalesOrder_CenterWarehouseCode;
            }
        }

        public string DataEditPanel_Action_Delete {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Action_Delete;
            }
        }

        public string DataEditPanel_Action_Export {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Action_Export;
            }
        }

        public string DataEditPanel_Action_Import {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Action_Import;
            }
        }

        public string DataEditPanel_Action_ImportNew {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Action_ImportNew;
            }
        }

        public string DataEditPanel_Action_Refres {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Action_Refres;
            }
        }

        public string DataEditPanel_Action_Upload {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Action_Upload;
            }
        }

        public string DataEditPanel_Text_InvoiceInformation_InvoiceDownloadLink {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_InvoiceInformation_InvoiceDownloadLink;
            }
        }

        public string DataEditPanel_Text_PartsPackingTask_Bag {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_Bag;
            }
        }

        public string DataEditPanel_Text_PartsPackingTask_Box {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_Box;
            }
        }

        public string DataEditPanel_Text_PartsPackingTask_Case {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_Case;
            }
        }

        public string DataEditPanel_Text_PartsPackingTask_Height {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_Height;
            }
        }

        public string DataEditPanel_Text_PartsPackingTask_IsBoxStandardPrint {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_IsBoxStandardPrint;
            }
        }

        public string DataEditPanel_Text_PartsPackingTask_IsPrintPartStandard {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_IsPrintPartStandard;
            }
        }

        public string DataEditPanel_Text_PartsPackingTask_Length {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_Length;
            }
        }

        public string DataEditPanel_Text_PartsPackingTask_Litre {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_Litre;
            }
        }

        public string DataEditPanel_Text_PartsPackingTask_MainPackingType {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_MainPackingType;
            }
        }

        public string DataEditPanel_Text_PartsPackingTask_MeasureUnit {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_MeasureUnit;
            }
        }

        public string DataEditPanel_Text_PartsPackingTask_MInSalesAmount {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_MInSalesAmount;
            }
        }

        public string DataEditPanel_Text_PartsPackingTask_OldMInPackingAmount {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_OldMInPackingAmount;
            }
        }

        public string DataEditPanel_Text_PartsPackingTask_PackingCode {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_PackingCode;
            }
        }

        public string DataEditPanel_Text_PartsPackingTask_PackingCoefficient {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_PackingCoefficient;
            }
        }

        public string DataEditPanel_Text_PartsPackingTask_PackingMaterial {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_PackingMaterial;
            }
        }

        public string DataEditPanel_Text_PartsPackingTask_PackingMaterialName {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_PackingMaterialName;
            }
        }

        public string DataEditPanel_Text_PartsPackingTask_Piece {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_Piece;
            }
        }

        public string DataEditPanel_Text_PartsPackingTask_Set {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_Set;
            }
        }

        public string DataEditPanel_Text_PartsPackingTask_SihCode {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_SihCode;
            }
        }

        public string DataEditPanel_Text_PartsPackingTask_Volume {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_Volume;
            }
        }

        public string DataEditPanel_Text_PartsPackingTask_Weight {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_Weight;
            }
        }

        public string DataEditPanel_Text_PartsPackingTask_Width {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_Width;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrderForParts_CustomerAccount {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrderForParts_CustomerAccount;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrderForParts_IsVehicleOutage {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrderForParts_IsVehicleOutage;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrderForParts_TotalAmount {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrderForParts_TotalAmount;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrder_ApproveQuantity {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_ApproveQuantity;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrder_ApproveTime {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_ApproveTime;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrder_ConfirmedReceptionTime {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_ConfirmedReceptionTime;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrder_ConfirmQuantity {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_ConfirmQuantity;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrder_CreateButton {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_CreateButton;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrder_InboundQuantity {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_InboundQuantity;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrder_InternalSupplier {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_InternalSupplier;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrder_IsCreatePurchseOrder {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_IsCreatePurchseOrder;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrder_OrderedQuantity {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_OrderedQuantity;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrder_OutboundQuantity {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_OutboundQuantity;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrder_PartsInboundCheckBillCode {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_PartsInboundCheckBillCode;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrder_PartsInboundPlanCode {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_PartsInboundPlanCode;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrder_PartsInboundPlanStatus {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_PartsInboundPlanStatus;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrder_PartsSalesOrderTypeNameNew {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_PartsSalesOrderTypeNameNew;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrder_PartsShippingOrderCode {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_PartsShippingOrderCode;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrder_PartsShippingOrderConsigneeName {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_PartsShippingOrderConsigneeName;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrder_ReceivingWarehouseCode {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_ReceivingWarehouseCode;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrder_RejecterName {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_RejecterName;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrder_RejectTime {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_RejectTime;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrder_ShippingDate {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_ShippingDate;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrder_ShippingQuantity {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_ShippingQuantity;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrder_ShippingStatus {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_ShippingStatus;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrder_Status {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_Status;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrder_SubmitTime {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_SubmitTime;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrder_WarehouseCode {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_WarehouseCode;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrder_WarehouseForCadre {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_WarehouseForCadre;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrder_WarehouseName {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_WarehouseName;
            }
        }

        public string DataEditPanel_Text_PartsSalesReturnBill_Code {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_Code;
            }
        }

        public string DataEditPanel_Text_PartsSalesReturnBill_DiscountRate {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_DiscountRate;
            }
        }

        public string DataEditPanel_Text_PartsSalesReturnBill_ReturnWarehouse {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_ReturnWarehouse;
            }
        }

        public string DataEditPanel_Text_PartsSalesReturnBill_ShippingMethod {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_ShippingMethod;
            }
        }

        public string DataEditPanel_Text_PartsSalesSettlement_PlannedPriceTotalAmount {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesSettlement_PlannedPriceTotalAmount;
            }
        }

        public string DataEditPanel_Text_SpecialTreatyPriceChange_CheckerComment {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_SpecialTreatyPriceChange_CheckerComment;
            }
        }

        public string DataEditPanel_Text_SpecialTreatyPriceChange_ExpireTime {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_SpecialTreatyPriceChange_ExpireTime;
            }
        }

        public string DataEditPanel_Text_SpecialTreatyPriceChange_InitialApproverComment {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_SpecialTreatyPriceChange_InitialApproverComment;
            }
        }

        public string DataEditPanel_Text_SpecialTreatyPriceChange_ValidationTime {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_SpecialTreatyPriceChange_ValidationTime;
            }
        }

        public string DataEditPanel_Text_UpLoad_Enclosure {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_UpLoad_Enclosure;
            }
        }

        public string DataEditPanel_Title_AgencyRetailerOrder {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Title_AgencyRetailerOrder;
            }
        }

        public string DataEditPanel_Title_BonusPointsSummaryList {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Title_BonusPointsSummaryList;
            }
        }

        public string DataEditPanel_Title_DealerRetailReturnBill {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Title_DealerRetailReturnBill;
            }
        }

        public string DataEditPanel_Title_IvecoPriceChangeAppDetail {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Title_IvecoPriceChangeAppDetail;
            }
        }

        public string DataEditPanel_Title_PartsOutboundBillDetail {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Title_PartsOutboundBillDetail;
            }
        }

        public string DataEditPanel_Title_PartsSpecialTreatyPriceHistory {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Title_PartsSpecialTreatyPriceHistory;
            }
        }

        public string DataEditPanel_Title_RetailerDeliveryDetail {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Title_RetailerDeliveryDetail;
            }
        }

        public string DataEditPanel_Title_RetailerServiceApplyDetail {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Title_RetailerServiceApplyDetail;
            }
        }

        public string DataEditPanel_Title_SpecialTreatyPriceChangeDetail {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Title_SpecialTreatyPriceChangeDetail;
            }
        }

        public string DataEditPanel_Validation_PartsPackingTask_Coefficient {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Validation_PartsPackingTask_Coefficient;
            }
        }

        public string DataEditPanel_Validation_PartsPackingTask_FirPackingCoefficient {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Validation_PartsPackingTask_FirPackingCoefficient;
            }
        }

        public string DataEditPanel_Validation_PartsPackingTask_SecPackingCoefficient {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Validation_PartsPackingTask_SecPackingCoefficient;
            }
        }

        public string DataEditPanel_Validation_PartsSalesReturnBill_ReturnCompany {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Validation_PartsSalesReturnBill_ReturnCompany;
            }
        }

        public string DataEditPanel_Validation_PartsSalesReturnBill_ReturnWarehouseIsNull {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Validation_PartsSalesReturnBill_ReturnWarehouseIsNull;
            }
        }

        public string DataEditPanel_Validation_PartsSalesReturnBill_SameDetails {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Validation_PartsSalesReturnBill_SameDetails;
            }
        }

        public string DataEditPanel_Validation_PartsSalesReturnBill_SubmitCompanyIsNull {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Validation_PartsSalesReturnBill_SubmitCompanyIsNull;
            }
        }

        public string DataEditPanel_Validation_PartsSales_CreateError {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Validation_PartsSales_CreateError;
            }
        }

        public string DataEditPanel_Validation_PartsSales_CreatePurChaseOrder {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Validation_PartsSales_CreatePurChaseOrder;
            }
        }

        public string DataEditPanel_Validation_UploadError {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Validation_UploadError;
            }
        }

        public string DataEditPanel_Validation_Uploading {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Validation_Uploading;
            }
        }

        public string DataEditPanel_Validation_UploadSpecial {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Validation_UploadSpecial;
            }
        }

        public string DataEditPanel_Validation_UploadSuccess {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Validation_UploadSuccess;
            }
        }

        public string DataEdit_Header_AgencyRetailerOrder_PartsOutboundPlanDetail {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Header_AgencyRetailerOrder_PartsOutboundPlanDetail;
            }
        }

        public string DataEdit_Header_AgencyRetailerOrder_VirtualPartsStock {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Header_AgencyRetailerOrder_VirtualPartsStock;
            }
        }

        public string DataEdit_Text_AgencyRetailerOrder_DeliveryBillNumber {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_AgencyRetailerOrder_DeliveryBillNumber;
            }
        }

        public string DataEdit_Text_AgencyRetailerOrder_LogisticCompany {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_AgencyRetailerOrder_LogisticCompany;
            }
        }

        public string DataEdit_Text_AgencyRetailerOrder_LogisticCompanySelect {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_AgencyRetailerOrder_LogisticCompanySelect;
            }
        }

        public string DataEdit_Text_CustomerSupplyInitialFee_InitialFee {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_CustomerSupplyInitialFee_InitialFee;
            }
        }

        public string DataEdit_Text_CustomerSupplyInitialFee_SupplierCode {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_CustomerSupplyInitialFee_SupplierCode;
            }
        }

        public string DataEdit_Text_DealerPartsRetailOrder_Price {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_DealerPartsRetailOrder_Price;
            }
        }

        public string DataEdit_Title_AgencyRetailerOrder {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_AgencyRetailerOrder;
            }
        }

        public string DataEdit_Title_CustomerDirect {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_CustomerDirect;
            }
        }

        public string DataEdit_Title_CustomerDirectSpareListForImport {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_CustomerDirectSpareListForImport;
            }
        }

        public string DataEdit_Title_CustomerInformationForImport {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_CustomerInformationForImport;
            }
        }

        public string DataEdit_Title_CustomerInformationForImportList {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_CustomerInformationForImportList;
            }
        }

        public string DataEdit_Title_CustomerOrderPriceGradeForImport {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_CustomerOrderPriceGradeForImport;
            }
        }

        public string DataEdit_Title_CustomerOrderPriceGradeForImportList {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_CustomerOrderPriceGradeForImportList;
            }
        }

        public string DataEdit_Title_CustomerPopoupTextBox {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_CustomerPopoupTextBox;
            }
        }

        public string DataEdit_Title_CustomerSupplyInitialFeeSet {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_CustomerSupplyInitialFeeSet;
            }
        }

        public string DataEdit_Validation_AgencyRetailerOrder_CurrentOutQuantity {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_AgencyRetailerOrder_CurrentOutQuantity;
            }
        }

        public string DataEdit_Validation_AgencyRetailerOrder_DeliveryBillNumber {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_AgencyRetailerOrder_DeliveryBillNumber;
            }
        }

        public string DataEdit_Validation_AgencyRetailerOrder_LogisticCompany {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_AgencyRetailerOrder_LogisticCompany;
            }
        }

        public string DataEdit_Validation_Customer {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_Customer;
            }
        }

        public string DataEdit_Validation_CustomerDirectSpareList {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_CustomerDirectSpareList;
            }
        }

        public string DataEdit_Validation_CustomerSupplyInitialFeeSet_Supplier {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_CustomerSupplyInitialFeeSet_Supplier;
            }
        }

        public string DataEdit_Validation_DealerPartsRetailOrder_CustomerPhone {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_CustomerPhone;
            }
        }

        public string DataEdit_Validation_DealerPartsRetailOrder_CustomerUnit {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_CustomerUnit;
            }
        }

        public string DataEdit_Validation_DealerPartsRetailOrder_RetailOrderType {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_RetailOrderType;
            }
        }

        public string DataEdit_Validation_Import_CloseFile {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_Import_CloseFile;
            }
        }

        public string DataEdit_Validation_SparePart {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_SparePart;
            }
        }

        public string QueryPanel_Title_PartsSupplierBySparepart {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_PartsSupplierBySparepart;
            }
        }

        public string DataEdit_BusinessName_DealerPartsSalesReturnBill {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_BusinessName_DealerPartsSalesReturnBill;
            }
        }

        public string DataEdit_BusinessName_IvecoPriceChangeApp {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_BusinessName_IvecoPriceChangeApp;
            }
        }

        public string DataEdit_BusinessName_OrderApproveWeekday {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_BusinessName_OrderApproveWeekday;
            }
        }

        public string DataEdit_BusinessName_PartsSalePriceIncrease {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_BusinessName_PartsSalePriceIncrease;
            }
        }

        public string DataEdit_BusinessName_PartsSalesCategoryN {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_BusinessName_PartsSalesCategoryN;
            }
        }

        public string DataEdit_Header_PartsSalesOrderDetailList {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Header_PartsSalesOrderDetailList;
            }
        }

        public string DataEdit_Header_PartsSalesOrder_Backlog {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Header_PartsSalesOrder_Backlog;
            }
        }

        public string DataEdit_Header_PartsSalesOrder_Center {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Header_PartsSalesOrder_Center;
            }
        }

        public string DataEdit_Header_PartsSalesOrder_CenterPurchase {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Header_PartsSalesOrder_CenterPurchase;
            }
        }

        public string DataEdit_Header_PartsSalesOrder_CompanyParts {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Header_PartsSalesOrder_CompanyParts;
            }
        }

        public string DataEdit_Header_PartsSalesOrder_Headquarters {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Header_PartsSalesOrder_Headquarters;
            }
        }

        public string DataEdit_Header_PartsSalesOrder_Local {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Header_PartsSalesOrder_Local;
            }
        }

        public string DataEdit_Header_PartsSalesOrder_OtherTab {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Header_PartsSalesOrder_OtherTab;
            }
        }

        public string DataEdit_Header_PartsSalesOrder_PartsBranchInfo {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Header_PartsSalesOrder_PartsBranchInfo;
            }
        }

        public string DataEdit_Header_PartsSalesOrder_ProcessDetails {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Header_PartsSalesOrder_ProcessDetails;
            }
        }

        public string DataEdit_Header_PartsSalesOrder_PurchaseOrder {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Header_PartsSalesOrder_PurchaseOrder;
            }
        }

        public string DataEdit_Text_IvecoPriceChangeAppDetail_IvecoPrice {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_IvecoPriceChangeAppDetail_IvecoPrice;
            }
        }

        public string DataEdit_Text_OrderApproveWeekday_ApproveDate {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_OrderApproveWeekday_ApproveDate;
            }
        }

        public string DataEdit_Text_OrderApproveWeekday_Province {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_OrderApproveWeekday_Province;
            }
        }

        public string DataEdit_Text_PartsPackingPropertyApp_FirPackingType {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsPackingPropertyApp_FirPackingType;
            }
        }

        public string DataEdit_Text_PartsPackingPropertyApp_MainPackingType {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsPackingPropertyApp_MainPackingType;
            }
        }

        public string DataEdit_Text_PartsPackingPropertyApp_SecPackingType {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsPackingPropertyApp_SecPackingType;
            }
        }

        public string DataEdit_Text_PartsPackingPropertyApp_ThidPackingType {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsPackingPropertyApp_ThidPackingType;
            }
        }

        public string DataEdit_Text_PartsRetailOrderDetail_DiscountPrice {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsRetailOrderDetail_DiscountPrice;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_AddList {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_AddList;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_BusinessCode {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_BusinessCode;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_BusinessType {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_BusinessType;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_ChoiceOne {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_ChoiceOne;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_CompanyPartsStockQuery {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_CompanyPartsStockQuery;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_ContractCode {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_ContractCode;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_CustomerAccount {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_CustomerAccount;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_CustomerAccount_CredenceAmount {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_CustomerAccount_CredenceAmount;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_CustomerAccount_LockBalance {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_CustomerAccount_LockBalance;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_DirectSupply {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_DirectSupply;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_MeetingTime {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_MeetingTime;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_NotApprovePrice {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_NotApprovePrice;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_NotApprovePriceQuery {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_NotApprovePriceQuery;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_OrderCode {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_OrderCode;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_OrderDetailForExport {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_OrderDetailForExport;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_OrderQuntity {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_OrderQuntity;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_OrderUseablePosition {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_OrderUseablePosition;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_PackingMethod {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_PackingMethod;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_PartsBranchInfoQuery {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_PartsBranchInfoQuery;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_PartsPurchaseOrderQuery {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_PartsPurchaseOrderQuery;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_PartsSalesPrice {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_PartsSalesPrice;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_ProcessingList {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_ProcessingList;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_ProcessingMode {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_ProcessingMode;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_ReplaceSpart {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_ReplaceSpart;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_SettleType {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_SettleType;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_Stop {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_Stop;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_Tips {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_Tips;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_TipsNew {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_TipsNew;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_TxtRebateAmount {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_TxtRebateAmount;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_Type_ExtraUrgent {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_Type_ExtraUrgent;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_Type_Normal {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_Type_Normal;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_Type_Oil {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_Type_Oil;
            }
        }

        public string DataEdit_Text_PartsSalesOrder_Type_Urgent {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_Type_Urgent;
            }
        }

        public string DataEdit_tips_PartsSalesOrder_IfDirectProvision {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_tips_PartsSalesOrder_IfDirectProvision;
            }
        }

        public string DataEdit_tips_PartsSalesOrder_WarehouseUnit {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_tips_PartsSalesOrder_WarehouseUnit;
            }
        }

        public string DataEdit_Title_AgencyWareHouse {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_AgencyWareHouse;
            }
        }

        public string DataEdit_Title_OrderApproveWeekdayForImport {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_OrderApproveWeekdayForImport;
            }
        }

        public string DataEdit_Title_OrderApproveWeekdayForImportList {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_OrderApproveWeekdayForImportList;
            }
        }

        public string DataEdit_Title_PartsPackingPropertyAppApprove {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_PartsPackingPropertyAppApprove;
            }
        }

        public string DataEdit_Title_PartsPackingPropertyDetail {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_PartsPackingPropertyDetail;
            }
        }

        public string DataEdit_Title_PartsPackingPropertyEdit {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_PartsPackingPropertyEdit;
            }
        }

        public string DataEdit_Title_PartsPurchaseOrderExport {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_PartsPurchaseOrderExport;
            }
        }

        public string DataEdit_Title_PartsPurchasePricing {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_PartsPurchasePricing;
            }
        }

        public string DataEdit_Title_PartsSalesOrderEdit {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_PartsSalesOrderEdit;
            }
        }

        public string DataEdit_Title_PartsSalesOrderForReject {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_PartsSalesOrderForReject;
            }
        }

        public string DataEdit_Title_PartsSalesOrderForStop {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_PartsSalesOrderForStop;
            }
        }

        public string DataEdit_Validation_DealerPartsRetailOrder_ApprovalComment {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_ApprovalComment;
            }
        }

        public string DataEdit_Validation_DealerPartsRetailOrder_Customer {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_Customer;
            }
        }

        public string DataEdit_Validation_DealerPartsRetailOrder_CustomerSpecial {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_CustomerSpecial;
            }
        }

        public string DataEdit_Validation_DealerPartsRetailOrder_Price {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_Price;
            }
        }

        public string DataEdit_Validation_DealerPartsRetailOrder_RetailOrderStrategy {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_RetailOrderStrategy;
            }
        }

        public string DataEdit_Validation_DealerPartsRetailOrder_TelephoneCount {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_TelephoneCount;
            }
        }

        public string DataEdit_Validation_DealerPartsRetailOrder_TelephoneFormat {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_TelephoneFormat;
            }
        }

        public string DataEdit_Validation_DealerPartsRetailOrder_TelephoneNumber {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_TelephoneNumber;
            }
        }

        public string DataEdit_Validation_DealerPartsRetailOrder_TelephoneStart {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_TelephoneStart;
            }
        }

        public string DataEdit_Validation_PartsPackingPropertyApp_ApprovalComment {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_ApprovalComment;
            }
        }

        public string DataEdit_Validation_PartsPackingPropertyApp_Fir {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_Fir;
            }
        }

        public string DataEdit_Validation_PartsPackingPropertyApp_FirMeasureUnit {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_FirMeasureUnit;
            }
        }

        public string DataEdit_Validation_PartsPackingPropertyApp_FirPackingCoefficient {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_FirPackingCoefficient;
            }
        }

        public string DataEdit_Validation_PartsPackingPropertyApp_FirPackingCoefficientMore {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_FirPackingCoefficientMore;
            }
        }

        public string DataEdit_Validation_PartsPackingPropertyApp_IsPrint {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_IsPrint;
            }
        }

        public string DataEdit_Validation_PartsPackingPropertyApp_MainPackingType {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_MainPackingType;
            }
        }

        public string DataEdit_Validation_PartsPackingPropertyApp_Sec {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_Sec;
            }
        }

        public string DataEdit_Validation_PartsPackingPropertyApp_SecMeasureUnit {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_SecMeasureUnit;
            }
        }

        public string DataEdit_Validation_PartsPackingPropertyApp_SecPackingCoefficient {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_SecPackingCoefficient;
            }
        }

        public string DataEdit_Validation_PartsPackingPropertyApp_SecPackingCoefficientMore {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_SecPackingCoefficientMore;
            }
        }

        public string DataEdit_Validation_PartsPackingPropertyApp_SparePart {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_SparePart;
            }
        }

        public string DataEdit_Validation_PartsPackingPropertyApp_Thid {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_Thid;
            }
        }

        public string DataEdit_Validation_PartsPackingPropertyApp_ThidMeasureUnit {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_ThidMeasureUnit;
            }
        }

        public string DataEdit_Validation_PartsPackingPropertyApp_ThidPackingCoefficient {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_ThidPackingCoefficient;
            }
        }

        public string DataEdit_Validation_PartsPackingPropertyApp_ThidPackingCoefficientMore {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_ThidPackingCoefficientMore;
            }
        }

        public string DataEdit_Validation_PartsSalePriceIncreaseRate_GroupCode {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalePriceIncreaseRate_GroupCode;
            }
        }

        public string DataEdit_Validation_PartsSalePriceIncreaseRate_GroupName {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalePriceIncreaseRate_GroupName;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_Amount {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_Amount;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_BusinessType {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_BusinessType;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_CenterProcessDetails {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_CenterProcessDetails;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_CenterProcessDetailsNew {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_CenterProcessDetailsNew;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_CenterSupplier {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_CenterSupplier;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_CenterWarehouse {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_CenterWarehouse;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_CurrentFulfilledAmount {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_CurrentFulfilledAmount;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_CurrentFulfilledQuantity {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_CurrentFulfilledQuantity;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_DirectSupply {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_DirectSupply;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_EditMain {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_EditMain;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_ExistsMuiltiChangedPartsSalesOrders {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_ExistsMuiltiChangedPartsSalesOrders;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_GoldenTaxClassifyCode {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_GoldenTaxClassifyCode;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_IIsUpsideDown {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_IIsUpsideDown;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_IncreaseRateSale {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_IncreaseRateSale;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_MaxInvoiceRow {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_MaxInvoiceRow;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_MInPackingAmount {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_MInPackingAmount;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_MInPackingAmountNew {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_MInPackingAmountNew;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_NoPartsBranchPackingPop {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_NoPartsBranchPackingPop;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_NoReplace {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_NoReplace;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_NoSparepart {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_NoSparepart;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_OrderedQuantity {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_OrderedQuantity;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_OrderProcessMethod {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_OrderProcessMethod;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_PreOrder {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_PreOrder;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_PriceChange {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_PriceChange;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_PurchasePrice {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_PurchasePrice;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_ReceivingWarehouse {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_ReceivingWarehouse;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_ReceivingWarehouseId {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_ReceivingWarehouseId;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_RejectReason {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_RejectReason;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_RequestedDeliveryTime {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_RequestedDeliveryTime;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_RetailGuidePrice {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_RetailGuidePrice;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_SaleingAmount {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_SaleingAmount;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_SalesUnitOwner {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_SalesUnitOwner;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_SalesUnitOwnerNew {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_SalesUnitOwnerNew;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_SameParts {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_SameParts;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_SamePartsNew {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_SamePartsNew;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_SettleType {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_SettleType;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_StopComment {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_StopComment;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_SupplierCompany {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_SupplierCompany;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_TotalAmount {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_TotalAmount;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_TotalAmountMoreUseablePosition {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_TotalAmountMoreUseablePosition;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_TransferPrice {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_TransferPrice;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_UsableQuantity {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_UsableQuantity;
            }
        }

        public string DataEdit_Validation_PartsSalesOrder_Warehouse {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_Warehouse;
            }
        }

        public string DataEditPanel_Text_PartsSalesReturnBill_DetailRemark {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_DetailRemark;
            }
        }

        public string DataEditPanel_Text_PartsSalesReturnBill_InvoiceBlue {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_InvoiceBlue;
            }
        }

        public string DataEditPanel_Text_PartsSalesReturnBill_InvoiceRequirementNew {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_InvoiceRequirementNew;
            }
        }

        public string DataEditPanel_Text_PartsSalesReturnBill_ReturnPrice {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_ReturnPrice;
            }
        }

        public string DataEditPanel_Text_PartsSalesReturnBill_ReturnReason {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_ReturnReason;
            }
        }

        public string DataEditPanel_Text_PartsSalesReturnBill_ReturnWarehouseCode {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_ReturnWarehouseCode;
            }
        }

        public string DataEditPanel_Text_PartsSalesReturnBill_StopReason {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_StopReason;
            }
        }

        public string DataEditPanel_Title_PartsSalesReturnBill_Stop {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Title_PartsSalesReturnBill_Stop;
            }
        }

        public string DataEdit_Text_PartsSalesPriceChangeApplication_AbandonComment {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_AbandonComment;
            }
        }

        public string DataEdit_Text_PartsSalesPriceChangeApplication_Centerprice {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_Centerprice;
            }
        }

        public string DataEdit_Text_PartsSalesPriceChangeApplication_Dealerprice {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_Dealerprice;
            }
        }

        public string DataEdit_Text_PartsSalesPriceChangeApplication_Orderprice {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_Orderprice;
            }
        }

        public string DataEdit_Text_PartsSalesReturnBill_Clear {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_Clear;
            }
        }

        public string DataEdit_Text_PartsSalesReturnBill_DiscountFee {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_DiscountFee;
            }
        }

        public string DataEdit_Text_PartsSalesReturnBill_DisCountSpareprt {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_DisCountSpareprt;
            }
        }

        public string DataEdit_Text_PartsSalesReturnBill_ExportDetail {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_ExportDetail;
            }
        }

        public string DataEdit_Text_PartsSalesReturnBill_ExportDetailsNew {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_ExportDetailsNew;
            }
        }

        public string DataEdit_Text_PartsSalesReturnBill_GoldenTaxClassify {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_GoldenTaxClassify;
            }
        }

        public string DataEdit_Text_PartsSalesReturnBill_InvoiceDate {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_InvoiceDate;
            }
        }

        public string DataEdit_Text_PartsSalesReturnBill_IsOil {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_IsOil;
            }
        }

        public string DataEdit_Text_PartsSalesReturnBill_NotOil {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_NotOil;
            }
        }

        public string DataEdit_Text_PartsSalesReturnBill_Return {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_Return;
            }
        }

        public string DataEdit_Text_PartsSalesReturnBill_ReturnedQuantity {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_ReturnedQuantity;
            }
        }

        public string DataEdit_Text_PartsSalesReturnBill_ReturnedType {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_ReturnedType;
            }
        }

        public string DataEdit_Text_PartsSalesReturnBill_SalesOrder {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_SalesOrder;
            }
        }

        public string DataEdit_Text_PartsSalesReturnBill_WarehouseCar {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_WarehouseCar;
            }
        }

        public string DataEdit_Text_PartsSalesReturnBill_WarehouseCarNew {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_WarehouseCarNew;
            }
        }

        public string DataEdit_Title_PartsSalesPriceChangeApplicationForAnandon {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_PartsSalesPriceChangeApplicationForAnandon;
            }
        }

        public string DataEdit_Title_PartsSalesPriceChangeApplicationForReject {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_PartsSalesPriceChangeApplicationForReject;
            }
        }

        public string DataEdit_Title_PartsSalesReturnBillForFinalTrial {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_PartsSalesReturnBillForFinalTrial;
            }
        }

        public string DataEdit_Title_PartsSalesReturnBillForFirst {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_PartsSalesReturnBillForFirst;
            }
        }

        public string DataEdit_Title_PartsSalesReturnBillForReject {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_PartsSalesReturnBillForReject;
            }
        }

        public string DataEdit_Title_PartsSalesReturnBillForReportForImport {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_PartsSalesReturnBillForReportForImport;
            }
        }

        public string DataEdit_Title_UploadFile {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_UploadFile;
            }
        }

        public string DataEdit_Validation_PartsSalesPriceChange_AnandonReason {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesPriceChange_AnandonReason;
            }
        }

        public string DataEdit_Validation_PartsSalesReturnBill_ApproveQuantity {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_ApproveQuantity;
            }
        }

        public string DataEdit_Validation_PartsSalesReturnBill_BusinessChange {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_BusinessChange;
            }
        }

        public string DataEdit_Validation_PartsSalesReturnBill_businessType {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_businessType;
            }
        }

        public string DataEdit_Validation_PartsSalesReturnBill_DiscountRate {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_DiscountRate;
            }
        }

        public string DataEdit_Validation_PartsSalesReturnBill_DiscountRateNew {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_DiscountRateNew;
            }
        }

        public string DataEdit_Validation_PartsSalesReturnBill_Error {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_Error;
            }
        }

        public string DataEdit_Validation_PartsSalesReturnBill_FictitiousWarehouse {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_FictitiousWarehouse;
            }
        }

        public string DataEdit_Validation_PartsSalesReturnBill_GoldenTaxClassifyCode {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_GoldenTaxClassifyCode;
            }
        }

        public string DataEdit_Validation_PartsSalesReturnBill_InvoiceCustomerName {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_InvoiceCustomerName;
            }
        }

        public string DataEdit_Validation_PartsSalesReturnBill_InvoiceDate {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_InvoiceDate;
            }
        }

        public string DataEdit_Validation_PartsSalesReturnBill_MaxInvoiceRow {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_MaxInvoiceRow;
            }
        }

        public string DataEdit_Validation_PartsSalesReturnBill_ReturnCompanyName {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_ReturnCompanyName;
            }
        }

        public string DataEdit_Validation_PartsSalesReturnBill_ReturnType {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_ReturnType;
            }
        }

        public string DataEdit_Validation_PartsSalesReturnBill_ReturnWarehouse {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_ReturnWarehouse;
            }
        }

        public string DataEdit_Validation_PartsSalesReturnBill_ReturnWarehouseIsnull {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_ReturnWarehouseIsnull;
            }
        }

        public string DataEdit_Validation_PartsSalesReturnBill_SalesUnitName {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_SalesUnitName;
            }
        }

        public string DataEdit_Validation_PartsSalesReturnBill_SameOrder {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_SameOrder;
            }
        }

        public string DataEdit_Validation_PartsSalesReturnBill_SameSettle {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_SameSettle;
            }
        }

        public string DataEdit_Validation_PartsSalesReturnBill_Satisfy {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_Satisfy;
            }
        }

        public string DataEdit_Validation_PartsSalesReturnBill_SettleApprove {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_SettleApprove;
            }
        }

        public string DataEdit_Validation_PartsSalesReturnBill_Settlement {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_Settlement;
            }
        }

        public string DataEdit_Validation_PartsSalesReturnBill_SettlementAllFee {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_SettlementAllFee;
            }
        }

        public string DataEdit_Validation_PartsSalesReturnBill_SettleNo {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_SettleNo;
            }
        }

        public string DataEdit_Validation_PartsSalesReturnBill_SetType {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_SetType;
            }
        }

        public string DataEdit_Validation_PartsSalesReturnBill_UnStock {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_UnStock;
            }
        }

        public string DataEdit_Validation_PartsSalesReturnBill_UnStockList {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_UnStockList;
            }
        }

        public string DataEdit_Validation_PartsSalesReturnBill_WareHouse {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_WareHouse;
            }
        }

        public string DataEdit_Validation_PartsSalesReturnBill_XNPJYF {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_XNPJYF;
            }
        }

        public string DataEdit_Validation_UploadFile {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_UploadFile;
            }
        }

        public string Action_Title_Add {
            get {
                return Resources.PartsSalesUIStrings.Action_Title_Add;
            }
        }

        public string Action_Title_AppoveRebate {
            get {
                return Resources.PartsSalesUIStrings.Action_Title_AppoveRebate;
            }
        }

        public string Action_Title_BatchAbandon {
            get {
                return Resources.PartsSalesUIStrings.Action_Title_BatchAbandon;
            }
        }

        public string Action_Title_BatchApprove {
            get {
                return Resources.PartsSalesUIStrings.Action_Title_BatchApprove;
            }
        }

        public string Action_Title_BatchFinalApproveNew {
            get {
                return Resources.PartsSalesUIStrings.Action_Title_BatchFinalApproveNew;
            }
        }

        public string Action_Title_Edit {
            get {
                return Resources.PartsSalesUIStrings.Action_Title_Edit;
            }
        }

        public string Action_Title_ExportAccurate {
            get {
                return Resources.PartsSalesUIStrings.Action_Title_ExportAccurate;
            }
        }

        public string Action_Title_ImportAbandon {
            get {
                return Resources.PartsSalesUIStrings.Action_Title_ImportAbandon;
            }
        }

        public string Action_Title_ImportEdit {
            get {
                return Resources.PartsSalesUIStrings.Action_Title_ImportEdit;
            }
        }

        public string Action_Title_PartsSalesPriceChange {
            get {
                return Resources.PartsSalesUIStrings.Action_Title_PartsSalesPriceChange;
            }
        }

        public string Action_Title_PrintInvoice {
            get {
                return Resources.PartsSalesUIStrings.Action_Title_PrintInvoice;
            }
        }

        public string Action_Title_SettlementCost {
            get {
                return Resources.PartsSalesUIStrings.Action_Title_SettlementCost;
            }
        }

        public string Action_Title_SettlementCostSearch {
            get {
                return Resources.PartsSalesUIStrings.Action_Title_SettlementCostSearch;
            }
        }

        public string DataEdit_BusinessName_SpecialTreatyPriceChange {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_BusinessName_SpecialTreatyPriceChange;
            }
        }

        public string DataEdit_Text_PartsSalesSettlement_AgreementPrice {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesSettlement_AgreementPrice;
            }
        }

        public string DataEdit_Text_PartsSalesSettlement_AutomaticSettleTime {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesSettlement_AutomaticSettleTime;
            }
        }

        public string DataEdit_Text_PartsSalesSettlement_SettleStartTime {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesSettlement_SettleStartTime;
            }
        }

        public string DataEdit_Text_PartsSalesSettlement_To {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesSettlement_To;
            }
        }

        public string DataEdit_Text_PartsSalesSettlement_TotalSettlementAmount {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesSettlement_TotalSettlementAmount;
            }
        }

        public string DataEdit_TitlePartsSpecialTreatyPriceForImport {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_TitlePartsSpecialTreatyPriceForImport;
            }
        }

        public string DataEdit_TitlePartsSpecialTreatyPriceForImportList {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_TitlePartsSpecialTreatyPriceForImportList;
            }
        }

        public string DataEdit_Title_DealerPartsStock {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_DealerPartsStock;
            }
        }

        public string DataEdit_Title_DealerPartsStockQuery {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_DealerPartsStockQuery;
            }
        }

        public string DataEdit_Title_ImportEdit {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_ImportEdit;
            }
        }

        public string DataEdit_Title_Logistics {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_Logistics;
            }
        }

        public string DataEdit_Title_LogisticsQuery {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_LogisticsQuery;
            }
        }

        public string DataEdit_Title_PersonSalesCenterLink {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_PersonSalesCenterLink;
            }
        }

        public string DataEdit_Title_PersonSalesCenterLinkEdit {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_PersonSalesCenterLinkEdit;
            }
        }

        public string DataEdit_Title_SpecialTreatyPriceChange {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_SpecialTreatyPriceChange;
            }
        }

        public string DataEdit_Title_SpecialTreatyPriceChangeImport {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_SpecialTreatyPriceChangeImport;
            }
        }

        public string DataEdit_Title_SpecialTreatyPriceChangeImportList {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_SpecialTreatyPriceChangeImportList;
            }
        }

        public string DataEdit_Title_SpecialTreatyPriceChange_FinalApprove {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_SpecialTreatyPriceChange_FinalApprove;
            }
        }

        public string DataEdit_Title_SpecialTreatyPriceChange_FirApprove {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_SpecialTreatyPriceChange_FirApprove;
            }
        }

        public string DataEdit_Title_SpecialTreatyPriceChange_SpecialPrice {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_SpecialTreatyPriceChange_SpecialPrice;
            }
        }

        public string DataEdit_Title_TowGradePartsNeed_Approve {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_TowGradePartsNeed_Approve;
            }
        }

        public string DataEdit_Title_TowGradePartsNeed_Quantity {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_TowGradePartsNeed_Quantity;
            }
        }

        public string DataEdit_Title_VehiclePartsStockLevelForImport {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_VehiclePartsStockLevelForImport;
            }
        }

        public string DataEdit_Title_VehiclePartsStockLevelForImportList {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Title_VehiclePartsStockLevelForImportList;
            }
        }

        public string DataEdit_Validation_SettlementAutomaticTaskSet_AutomaticSettleTime {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_SettlementAutomaticTaskSet_AutomaticSettleTime;
            }
        }

        public string DataEdit_Validation_SettlementAutomaticTaskSet_SettleEndTime {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_SettlementAutomaticTaskSet_SettleEndTime;
            }
        }

        public string DataEdit_Validation_SettlementAutomaticTaskSet_SettleStartTime {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_SettlementAutomaticTaskSet_SettleStartTime;
            }
        }

        public string DataEdit_Validation_SpecialTreatyPriceChange_BrandIsNull {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_SpecialTreatyPriceChange_BrandIsNull;
            }
        }

        public string DataEdit_Validation_SpecialTreatyPriceChange_Continue {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_SpecialTreatyPriceChange_Continue;
            }
        }

        public string DataEdit_Validation_SpecialTreatyPriceChange_Corporation {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_SpecialTreatyPriceChange_Corporation;
            }
        }

        public string DataEdit_Validation_SpecialTreatyPriceChange_ExpireTime {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_SpecialTreatyPriceChange_ExpireTime;
            }
        }

        public string DataEdit_Validation_SpecialTreatyPriceChange_PriceSmall {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_SpecialTreatyPriceChange_PriceSmall;
            }
        }

        public string DataEdit_Validation_SpecialTreatyPriceChange_PurchasePrice {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_SpecialTreatyPriceChange_PurchasePrice;
            }
        }

        public string DataEdit_Validation_SpecialTreatyPriceChange_SpecialTreatyPrice {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_SpecialTreatyPriceChange_SpecialTreatyPrice;
            }
        }

        public string DataEdit_Validation_SpecialTreatyPriceChange_Status {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_SpecialTreatyPriceChange_Status;
            }
        }

        public string DataEdit_Validation_SpecialTreatyPriceChange_ValidationTime {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_SpecialTreatyPriceChange_ValidationTime;
            }
        }

        public string DataEdit_Validation_TowGradePartsNeed_ProcessMode {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Validation_TowGradePartsNeed_ProcessMode;
            }
        }

        public string DataGridView_ColumnItem_Title_VehiclePartsStockLevel_StockMimimum {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_StockMimimum;
            }
        }

        public string DataGridView_Title_APartsOutboundBillDetail_WarehouseAreaCode {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_APartsOutboundBillDetail_WarehouseAreaCode;
            }
        }

        public string QueryPanel_Title_AgencyLogisticCompanyQueryPanel {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_AgencyLogisticCompanyQueryPanel;
            }
        }

        public string QueryPanel_Title_AgencyLogisticCompany_Status {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_AgencyLogisticCompany_Status;
            }
        }

        public string QueryPanel_Title_AgencyRetailerOrder_ERPSourceOrderCode {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_AgencyRetailerOrder_ERPSourceOrderCode;
            }
        }

        public string QueryPanel_Title_CustomerDirectSpareList_IsPrimary {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_CustomerDirectSpareList_IsPrimary;
            }
        }

        public string QueryPanel_Title_CustomerSupplyInitialFeeSetQuery {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_CustomerSupplyInitialFeeSetQuery;
            }
        }

        public string QueryPanel_Title_DealerPartsSalesReturnBill_Code {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_DealerPartsSalesReturnBill_Code;
            }
        }

        public string QueryPanel_Title_DealerPartsStockOutInRecord_CreateTime {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_DealerPartsStockOutInRecord_CreateTime;
            }
        }

        public string QueryPanel_Title_GetAllPartsSalesPriceQuery {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_GetAllPartsSalesPriceQuery;
            }
        }

        public string QueryPanel_Title_PartsPackingPropertyAppQuery_MainPackingType {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_PartsPackingPropertyAppQuery_MainPackingType;
            }
        }

        public string QueryPanel_Title_PartsRetailGuidePriceQuery {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_PartsRetailGuidePriceQuery;
            }
        }

        public string QueryPanel_Title_PartsSalePriceIncreaseRateQuery {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_PartsSalePriceIncreaseRateQuery;
            }
        }

        public string QueryPanel_Title_PartsSalesOrderQuery_AutoApproveStatus {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_PartsSalesOrderQuery_AutoApproveStatus;
            }
        }

        public string QueryPanel_Title_PartsSalesOrderQuery_ReceivingCompanyCode {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_PartsSalesOrderQuery_ReceivingCompanyCode;
            }
        }

        public string QueryPanel_Title_PartsSalesOrderQuery_ReceivingCompanyName {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_PartsSalesOrderQuery_ReceivingCompanyName;
            }
        }

        public string QueryPanel_Title_PartsSalesPriceChangeQuery {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_PartsSalesPriceChangeQuery;
            }
        }

        public string QueryPanel_Title_PartsSalesPriceChangeQueryNew {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_PartsSalesPriceChangeQueryNew;
            }
        }

        public string QueryPanel_Title_PartsSalesPriceChangeQuery_Code {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_PartsSalesPriceChangeQuery_Code;
            }
        }

        public string QueryPanel_Title_PartsSalesPriceQuery {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_PartsSalesPriceQuery;
            }
        }

        public string QueryPanel_Title_PartsSalesQueryOnWay {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_PartsSalesQueryOnWay;
            }
        }

        public string QueryPanel_Title_PartsSalesQueryOnWay_ReceivingCompanyCode {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_PartsSalesQueryOnWay_ReceivingCompanyCode;
            }
        }

        public string QueryPanel_Title_PartsSalesQueryOnWay_ReceivingCompanyName {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_PartsSalesQueryOnWay_ReceivingCompanyName;
            }
        }

        public string QueryPanel_Title_PartsSalesQueryOnWay_WarehouseId {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_PartsSalesQueryOnWay_WarehouseId;
            }
        }

        public string QueryPanel_Title_PurchasePricingChangeHiStock {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_PurchasePricingChangeHiStock;
            }
        }

        public string QueryPanel_Title_ReferenceCode {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_ReferenceCode;
            }
        }

        public string QueryPanel_Title_RetailOrderCustomerQuery {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_RetailOrderCustomerQuery;
            }
        }

        public string QueryPanel_Title_ServiceAgentsHistoryQuery {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_ServiceAgentsHistoryQuery;
            }
        }

        public string QueryPanel_Title_ServiceAgentsHistoryQuery_SubmitTime {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_ServiceAgentsHistoryQuery_SubmitTime;
            }
        }

        public string QueryPanel_Title_SettlementAutomaticTaskSet_ExecutionStatus {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_SettlementAutomaticTaskSet_ExecutionStatus;
            }
        }

        public string QueryPanel_Title_SettlementAutomaticTaskSet_ExecutionTime {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_SettlementAutomaticTaskSet_ExecutionTime;
            }
        }

        public string QueryPanel_Title_VehiclePartsSalesOrdercsQuery_SalesUnitOwnerCompanyId {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_VehiclePartsSalesOrdercsQuery_SalesUnitOwnerCompanyId;
            }
        }

        public string QueryPanel_Title_VehiclePartsSalesOrdercsQuery_WarehouseId {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_VehiclePartsSalesOrdercsQuery_WarehouseId;
            }
        }

        public string QueryPanel_Title_VirtualCustomerDirectSpareListQuery {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_VirtualCustomerDirectSpareListQuery;
            }
        }

        public string QueryPanel_Title_VirtualDealerPartsSalesPriceQuery {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_VirtualDealerPartsSalesPriceQuery;
            }
        }

        public string QueryPanel_Title_VirtualDealerPartsStock_CompanyType {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_VirtualDealerPartsStock_CompanyType;
            }
        }

        public string QueryPanel_Title_VirtualDealerPartsStock_IsDealerQuery {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_VirtualDealerPartsStock_IsDealerQuery;
            }
        }

        public string QueryPanel_Title_VirtualInvoiceInformation_BusinessCode {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_VirtualInvoiceInformation_BusinessCode;
            }
        }

        public string QueryPanel_Title_VirtualPartsSalesPriceWithStock {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_VirtualPartsSalesPriceWithStock;
            }
        }

        public string QueryPanel_Title_VirtualSparePartQuery {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_VirtualSparePartQuery;
            }
        }

        public string QueryPanel_Title_VirtualSparePartQuery_OverseasPartsFigure {
            get {
                return Resources.PartsSalesUIStrings.QueryPanel_Title_VirtualSparePartQuery_OverseasPartsFigure;
            }
        }

        public string DataEditPanel_Text_PartsSalesOrder_SalesUnitOwnerCompanyName {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_SalesUnitOwnerCompanyName;
            }
        }

        public string DataEdit_Text_CustomerSupplyInitialFee_SupplierName {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_CustomerSupplyInitialFee_SupplierName;
            }
        }

        public string DataGridView_Header_VirtualPartsPurchaseOrderDetail_RetailOrder {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Header_VirtualPartsPurchaseOrderDetail_RetailOrder;
            }
        }

        public string DataGridView_Header_WarehousePartsStockQueryWindow {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Header_WarehousePartsStockQueryWindow;
            }
        }

        public string DataGridView_Title_DealerPartsRetailOrder_DiscountedPrice {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_DealerPartsRetailOrder_DiscountedPrice;
            }
        }

        public string DataGridView_Title_DealerPartsRetailOrder_MinSaleQuantity {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_DealerPartsRetailOrder_MinSaleQuantity;
            }
        }

        public string DataGridView_Title_DealerPartsRetailOrder_RetailOrderType {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_DealerPartsRetailOrder_RetailOrderType;
            }
        }

        public string DataGridView_Title_DealerPartsSalesReturnBill_ApproverName {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_DealerPartsSalesReturnBill_ApproverName;
            }
        }

        public string DataGridView_Title_DealerPartsSalesReturnBill_ApproveTime {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_DealerPartsSalesReturnBill_ApproveTime;
            }
        }

        public string DataGridView_Title_DealerPartsSalesReturnBill_SourceCode {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_DealerPartsSalesReturnBill_SourceCode;
            }
        }

        public string DataGridView_Title_DealerPartsStockOutInRecord_OutInBandtype {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_DealerPartsStockOutInRecord_OutInBandtype;
            }
        }

        public string DataGridView_Title_ERPDeliveryDetailVirtual_ErrorMessage {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_ERPDeliveryDetailVirtual_ErrorMessage;
            }
        }

        public string DataGridView_Title_PartsOutboundBillDetail_OutboundAmount {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsOutboundBillDetail_OutboundAmount;
            }
        }

        public string DataGridView_Title_PartsOutboundBillDetail_UsableQuantity {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsOutboundBillDetail_UsableQuantity;
            }
        }

        public string DataGridView_Title_PartsOutboundBillDetail_WarehouseAreaCategory {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsOutboundBillDetail_WarehouseAreaCategory;
            }
        }

        public string DataGridView_Title_PartsPackingPropertyAppQuery_ApproverName {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsPackingPropertyAppQuery_ApproverName;
            }
        }

        public string DataGridView_Title_PartsPackingPropertyAppQuery_ApproveTime {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsPackingPropertyAppQuery_ApproveTime;
            }
        }

        public string DataGridView_Title_PartsPackingPropertyAppQuery_FirPackingCoefficient {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsPackingPropertyAppQuery_FirPackingCoefficient;
            }
        }

        public string DataGridView_Title_PartsPackingPropertyAppQuery_SecPackingCoefficient {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsPackingPropertyAppQuery_SecPackingCoefficient;
            }
        }

        public string DataGridView_Title_PartsPackingPropertyAppQuery_ThidPackingCoefficient {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsPackingPropertyAppQuery_ThidPackingCoefficient;
            }
        }

        public string DataGridView_Title_PartsSalesOrderProcess_CurrentFulfilledAmount {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesOrderProcess_CurrentFulfilledAmount;
            }
        }

        public string DataGridView_Title_PartsSalesOrderProcess_Time {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesOrderProcess_Time;
            }
        }

        public string DataGridView_Title_PartsSalesOrder_InvoiceReceiveCompanyName {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesOrder_InvoiceReceiveCompanyName;
            }
        }

        public string DataGridView_Title_PartsSalesPriceChangeDetail_CenterPriceRatio {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChangeDetail_CenterPriceRatio;
            }
        }

        public string DataGridView_Title_PartsSalesPriceChange_CategoryCode {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_CategoryCode;
            }
        }

        public string DataGridView_Title_PartsSalesPriceChange_CategoryName {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_CategoryName;
            }
        }

        public string DataGridView_Title_PartsSalesPriceChange_IncreaseRateRetail {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_IncreaseRateRetail;
            }
        }

        public string DataGridView_Title_PartsSalesPriceChange_IncreaseRateSale {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_IncreaseRateSale;
            }
        }

        public string DataGridView_Title_PartsSalesPriceChange_IsUplodFile {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_IsUplodFile;
            }
        }

        public string DataGridView_Title_PartsSalesPriceChange_MaxExchangeSalePrice {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_MaxExchangeSalePrice;
            }
        }

        public string DataGridView_Title_PartsSalesPriceChange_MaxPurchasePricing {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_MaxPurchasePricing;
            }
        }

        public string DataGridView_Title_PartsSalesPriceChange_MaxRetailOrderPriceFloating {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_MaxRetailOrderPriceFloating;
            }
        }

        public string DataGridView_Title_PartsSalesPriceChange_MaxSalesPriceFloating {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_MaxSalesPriceFloating;
            }
        }

        public string DataGridView_Title_PartsSalesPriceChange_MinRetailOrderPriceFloating {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_MinRetailOrderPriceFloating;
            }
        }

        public string DataGridView_Title_PartsSalesPriceChange_MinSalesPriceFloating {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_MinSalesPriceFloating;
            }
        }

        public string DataGridView_Title_PartsSalesPriceChange_PurchasePrice {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_PurchasePrice;
            }
        }

        public string DataGridView_Title_PartsSalesPriceChange_ReferencePrice {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_ReferencePrice;
            }
        }

        public string DataGridView_Title_PartsSalesPriceChange_RetailPriceFluctuationRatio {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_RetailPriceFluctuationRatio;
            }
        }

        public string DataGridView_Title_PartsSalesPriceChange_SalesPriceFluctuationRatio {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_SalesPriceFluctuationRatio;
            }
        }

        public string DataGridView_Title_PartsSalesQuery_SettlementPrice {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesQuery_SettlementPrice;
            }
        }

        public string DataGridView_Title_PartsSalesQuery_TotalPrice {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesQuery_TotalPrice;
            }
        }

        public string DataGridView_Title_PartsSalesSettlementEx_InvoiceRegistrationOperator {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesSettlementEx_InvoiceRegistrationOperator;
            }
        }

        public string DataGridView_Title_PartsSalesSettlementEx_InvoiceRegistrationTime {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesSettlementEx_InvoiceRegistrationTime;
            }
        }

        public string DataGridView_Title_PartsSalesSettlementEx_NoTaxAmount {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesSettlementEx_NoTaxAmount;
            }
        }

        public string DataGridView_Title_PartsSalesSettlementEx_OffsettedSettlementBillCode {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesSettlementEx_OffsettedSettlementBillCode;
            }
        }

        public string DataGridView_Title_PartsSalesSettlementEx_RebateAmount {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesSettlementEx_RebateAmount;
            }
        }

        public string DataGridView_Title_PartsSalesSettlementEx_SettlementPath {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesSettlementEx_SettlementPath;
            }
        }

        public string DataGridView_Title_PartsSalesSettlementEx_Tax {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesSettlementEx_Tax;
            }
        }

        public string DataGridView_Title_PartsSalesSettlementEx_TaxRate {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesSettlementEx_TaxRate;
            }
        }

        public string DataGridView_Title_ServiceAgentsHistoryQuery_AbandonerName {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_ServiceAgentsHistoryQuery_AbandonerName;
            }
        }

        public string DataGridView_Title_ServiceAgentsHistoryQuery_AbandonTime {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_ServiceAgentsHistoryQuery_AbandonTime;
            }
        }

        public string DataGridView_Title_ServiceAgentsHistoryQuery_FirstApproveTime {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_ServiceAgentsHistoryQuery_FirstApproveTime;
            }
        }

        public string DataGridView_Title_ServiceAgentsHistoryQuery_IsDebt {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_ServiceAgentsHistoryQuery_IsDebt;
            }
        }

        public string DataGridView_Title_ServiceAgentsHistoryQuery_PartsSalesOrderTypeName {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_ServiceAgentsHistoryQuery_PartsSalesOrderTypeName;
            }
        }

        public string DataGridView_Title_ServiceAgentsHistoryQuery_ReceivingAddress {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_ServiceAgentsHistoryQuery_ReceivingAddress;
            }
        }

        public string DataGridView_Title_ServiceAgentsHistoryQuery_RequestedDeliveryTime {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_ServiceAgentsHistoryQuery_RequestedDeliveryTime;
            }
        }

        public string DataGridView_Title_ServiceAgentsHistoryQuery_SubmitterName {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_ServiceAgentsHistoryQuery_SubmitterName;
            }
        }

        public string DataGridView_Title_ServiceAgentsHistoryQuery_TotalAmount {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_ServiceAgentsHistoryQuery_TotalAmount;
            }
        }

        public string DataGridView_Title_SettlementAutomaticTaskSet_SettleEndTime {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_SettlementAutomaticTaskSet_SettleEndTime;
            }
        }

        public string DataGridView_Title_SettlementAutomaticTaskSet_SettleStartTime {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_SettlementAutomaticTaskSet_SettleStartTime;
            }
        }

        public string DataGridView_Title_SpecialPriceChangeList_PurchasePrice {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_SpecialPriceChangeList_PurchasePrice;
            }
        }

        public string DataGridView_Title_SpecialPriceChangeList_Ratio {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_SpecialPriceChangeList_Ratio;
            }
        }

        public string DataGridView_Title_TowGradePartsNeed_DealerPartsStockQuantity {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_TowGradePartsNeed_DealerPartsStockQuantity;
            }
        }

        public string DataGridView_Title_VehiclePartsSalesOrdercs_PartsOutboundPlanStatus {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_VehiclePartsSalesOrdercs_PartsOutboundPlanStatus;
            }
        }

        public string DataGridView_Title_VirtualCustomerDirectSpareList_IsPrimary {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_VirtualCustomerDirectSpareList_IsPrimary;
            }
        }

        public string DataGridView_Title_VirtualDealerPartsSalesPrice_RetailGuidePrice {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_VirtualDealerPartsSalesPrice_RetailGuidePrice;
            }
        }

        public string DataGridView_Title_VirtualDealerPartsSalesPrice_SalesPrice {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_VirtualDealerPartsSalesPrice_SalesPrice;
            }
        }

        public string DataGridView_Title_VirtualDealerPartsStock_BasicSalePriceAll {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_VirtualDealerPartsStock_BasicSalePriceAll;
            }
        }

        public string DataGridView_Title_VirtualPartsOutboundBillDetail_ReturnedQuantity {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_VirtualPartsOutboundBillDetail_ReturnedQuantity;
            }
        }

        public string DataGridView_Title_VirtualPartsPurchaseOrderDetail_ConfirmAmount {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_VirtualPartsPurchaseOrderDetail_ConfirmAmount;
            }
        }

        public string DataGridView_Title_VirtualPartsPurchaseOrderDetail_OrderAmount {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_VirtualPartsPurchaseOrderDetail_OrderAmount;
            }
        }

        public string DataGridView_Title_VirtualPartsPurchaseOrderDetail_PartsPurchaseOrderCode {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_VirtualPartsPurchaseOrderDetail_PartsPurchaseOrderCode;
            }
        }

        public string DataGridView_Title_VirtualPartsPurchaseOrderDetail_PartsPurchaseOrderStatus {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_VirtualPartsPurchaseOrderDetail_PartsPurchaseOrderStatus;
            }
        }

        public string DataGridView_Title_VirtualPartsPurchaseOrderDetail_UnInboundAmount {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_VirtualPartsPurchaseOrderDetail_UnInboundAmount;
            }
        }

        public string DataGridView_Title_VirtualPartsSalesRtnSettlement_BillCode {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_VirtualPartsSalesRtnSettlement_BillCode;
            }
        }

        public string DataGridView_Title_VirtualPartsSalesRtnSettlement_MaterialCostVariance {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_VirtualPartsSalesRtnSettlement_MaterialCostVariance;
            }
        }

        public string DataGridView_Title_VirtualPartsSalesRtnSettlement_PlanAmount {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_VirtualPartsSalesRtnSettlement_PlanAmount;
            }
        }

        public string DataGridView_Title_VirtualPartsSalesRtnSettlement_PlanPrice {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_VirtualPartsSalesRtnSettlement_PlanPrice;
            }
        }

        public string DataGridView_Title_VirtualPartsSalesSettlement_BillBusinessType {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_VirtualPartsSalesSettlement_BillBusinessType;
            }
        }

        public string DataGridView_Title_VirtualPurchasePricingChangeHi_ChangeRatio {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_VirtualPurchasePricingChangeHi_ChangeRatio;
            }
        }

        public string DataGridView_Title_VirtualPurchasePricingChangeHi_MaintenanceTime {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_VirtualPurchasePricingChangeHi_MaintenanceTime;
            }
        }

        public string DataGridView_Title_VirtualPurchasePricingChangeHi_OldPurchasePrice {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_VirtualPurchasePricingChangeHi_OldPurchasePrice;
            }
        }

        public string DataGridView_Title_VirtualPurchasePricingChangeHi_PurchasePrice {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_VirtualPurchasePricingChangeHi_PurchasePrice;
            }
        }

        public string DataGridView_Title_VirtualSparePart_CADCode {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_VirtualSparePart_CADCode;
            }
        }

        public string DataGridView_Title_VirtualSparePart_Feature {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_VirtualSparePart_Feature;
            }
        }

        public string DataGridView_Title_VirtualSparePart_PartType {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_VirtualSparePart_PartType;
            }
        }

        public string DataGridView_Title_VirtualSparePart_Specification {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_VirtualSparePart_Specification;
            }
        }

        public string DataGridView_Title_WarehousePartsStock_NewPartCode {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_WarehousePartsStock_NewPartCode;
            }
        }

        public string DataGridView_Validation_PartsOutboundBillDetail_CurrentAmount {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Validation_PartsOutboundBillDetail_CurrentAmount;
            }
        }

        public string DataGridView_Validation_PartsOutboundBillDetail_OutboundAmount {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Validation_PartsOutboundBillDetail_OutboundAmount;
            }
        }

        public string DataGridView_Validation_PartsSalesOrderDetail_ApproveQuantity {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Validation_PartsSalesOrderDetail_ApproveQuantity;
            }
        }

        public string DataGridView_Validation_PartsSalesOrderDetail_SparePartCode {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Validation_PartsSalesOrderDetail_SparePartCode;
            }
        }

        public string DataGridView_Validation_PartsSalesReturnBill_ReturnWarehouseId {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Validation_PartsSalesReturnBill_ReturnWarehouseId;
            }
        }

        public string DataGridView_Validation_SalesUnitAffiPersonnel_IsAdd {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Validation_SalesUnitAffiPersonnel_IsAdd;
            }
        }

        public string DataGridView_Validation_SalesUnitAffiPersonnel_IsAddWarehouses {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Validation_SalesUnitAffiPersonnel_IsAddWarehouses;
            }
        }

        public string DataGridView_Validation_SalesUnitAffiPersonnel_IsDelete {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Validation_SalesUnitAffiPersonnel_IsDelete;
            }
        }

        public string DataGridView_Validation_SalesUnitAffiPersonnel_Only {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Validation_SalesUnitAffiPersonnel_Only;
            }
        }

        public string DataGridView_Validation_SalesUnitAffiPersonnel_PersonnelId {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Validation_SalesUnitAffiPersonnel_PersonnelId;
            }
        }

        public string DataGridView_Validation_SalesUnitAffiPersonne_DeleteWarehouses {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Validation_SalesUnitAffiPersonne_DeleteWarehouses;
            }
        }

        public string DataGridView_Validation_SalesUnitAffiPersonne_SameWarehouses {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Validation_SalesUnitAffiPersonne_SameWarehouses;
            }
        }

        public string DataGridView_Validation_SpecialPriceChangeList_BrandId {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Validation_SpecialPriceChangeList_BrandId;
            }
        }

        public string DataGridView_Validation_VirtualPartsPurchaseOrderDetail_MInSaleingAmount {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Validation_VirtualPartsPurchaseOrderDetail_MInSaleingAmount;
            }
        }

        public string DataGridView_Validation_VirtualPartsPurchaseOrderDetail_SparePartCode {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Validation_VirtualPartsPurchaseOrderDetail_SparePartCode;
            }
        }

        public string DataGridView_Validation_VirtualPartsSalesPriceWithStock_StockQuantity {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Validation_VirtualPartsSalesPriceWithStock_StockQuantity;
            }
        }

        public string Management_Title_AgencyRetailerOrderManagement {
            get {
                return Resources.PartsSalesUIStrings.Management_Title_AgencyRetailerOrderManagement;
            }
        }

        public string Management_Title_AutoTaskExecutResultManagement {
            get {
                return Resources.PartsSalesUIStrings.Management_Title_AutoTaskExecutResultManagement;
            }
        }

        public string Management_Title_BonusPointsSummaryManagement {
            get {
                return Resources.PartsSalesUIStrings.Management_Title_BonusPointsSummaryManagement;
            }
        }

        public string Management_Title_CsbomPartsManagement {
            get {
                return Resources.PartsSalesUIStrings.Management_Title_CsbomPartsManagement;
            }
        }

        public string Management_Title_DealerPartsSalesPriceQueryManagement {
            get {
                return Resources.PartsSalesUIStrings.Management_Title_DealerPartsSalesPriceQueryManagement;
            }
        }

        public string Management_Title_DealerPartsSalesReturnBillManagement {
            get {
                return Resources.PartsSalesUIStrings.Management_Title_DealerPartsSalesReturnBillManagement;
            }
        }

        public string Management_Title_IvecoPriceChangeAppManagement {
            get {
                return Resources.PartsSalesUIStrings.Management_Title_IvecoPriceChangeAppManagement;
            }
        }

        public string Management_Title_OrderApproveWeekdayManagement {
            get {
                return Resources.PartsSalesUIStrings.Management_Title_OrderApproveWeekdayManagement;
            }
        }

        public string Management_Validation_AgencyPartsRetail_BranchId {
            get {
                return Resources.PartsSalesUIStrings.Management_Validation_AgencyPartsRetail_BranchId;
            }
        }

        public string Management_Validation_AgencyPartsRetail_Stop {
            get {
                return Resources.PartsSalesUIStrings.Management_Validation_AgencyPartsRetail_Stop;
            }
        }

        public string Management_Validation_BonusPointsSummaryManagement_Anandon {
            get {
                return Resources.PartsSalesUIStrings.Management_Validation_BonusPointsSummaryManagement_Anandon;
            }
        }

        public string Management_Validation_BonusPointsSummaryManagement_Approve {
            get {
                return Resources.PartsSalesUIStrings.Management_Validation_BonusPointsSummaryManagement_Approve;
            }
        }

        public string Management_Validation_DealerPartsStockManagement_CodeCount {
            get {
                return Resources.PartsSalesUIStrings.Management_Validation_DealerPartsStockManagement_CodeCount;
            }
        }

        public string Management_Validation_InAndOutHistory_NoData {
            get {
                return Resources.PartsSalesUIStrings.Management_Validation_InAndOutHistory_NoData;
            }
        }

        public string Management_Validation_InAndOutHistory_Query {
            get {
                return Resources.PartsSalesUIStrings.Management_Validation_InAndOutHistory_Query;
            }
        }

        public string Management_Validation_PartsPackingPropertyAppManagement_partspackingEdit {
            get {
                return Resources.PartsSalesUIStrings.Management_Validation_PartsPackingPropertyAppManagement_partspackingEdit;
            }
        }

        public string QueryWindow_Title_AgentsPartsSalesOrderManagement {
            get {
                return Resources.PartsSalesUIStrings.QueryWindow_Title_AgentsPartsSalesOrderManagement;
            }
        }

        public string QueryWindow_Title_OptPartsSalesOrderDrop {
            get {
                return Resources.PartsSalesUIStrings.QueryWindow_Title_OptPartsSalesOrderDrop;
            }
        }

        public string QueryWindow_Title_PartsBranchSelectQuery {
            get {
                return Resources.PartsSalesUIStrings.QueryWindow_Title_PartsBranchSelectQuery;
            }
        }

        public string QueryWindow_Title_SparePartWithBranchDrop {
            get {
                return Resources.PartsSalesUIStrings.QueryWindow_Title_SparePartWithBranchDrop;
            }
        }

        public string Management_Header_VirtualPartsSalesSettlementQueryWindow {
            get {
                return Resources.PartsSalesUIStrings.Management_Header_VirtualPartsSalesSettlementQueryWindow;
            }
        }

        public string Management_Title_PartsSalePriceIncreaseRateManagement {
            get {
                return Resources.PartsSalesUIStrings.Management_Title_PartsSalePriceIncreaseRateManagement;
            }
        }

        public string Management_Title_PartsSalesOrderTraceQuery {
            get {
                return Resources.PartsSalesUIStrings.Management_Title_PartsSalesOrderTraceQuery;
            }
        }

        public string Management_Title_PartsSalesOrderTypeNManagement {
            get {
                return Resources.PartsSalesUIStrings.Management_Title_PartsSalesOrderTypeNManagement;
            }
        }

        public string Management_Title_SettlementAutomaticTaskSetManagement {
            get {
                return Resources.PartsSalesUIStrings.Management_Title_SettlementAutomaticTaskSetManagement;
            }
        }

        public string Management_Title_SpecialTreatyPriceChangeManagement {
            get {
                return Resources.PartsSalesUIStrings.Management_Title_SpecialTreatyPriceChangeManagement;
            }
        }

        public string Management_Validation_PartsRetailOrderManagement_MakeInvoice {
            get {
                return Resources.PartsSalesUIStrings.Management_Validation_PartsRetailOrderManagement_MakeInvoice;
            }
        }

        public string Management_Validation_PartsRetailOrderManagement_PrintTitle {
            get {
                return Resources.PartsSalesUIStrings.Management_Validation_PartsRetailOrderManagement_PrintTitle;
            }
        }

        public string Management_Validation_PartsSalesOrder_BitachApproceSuccess {
            get {
                return Resources.PartsSalesUIStrings.Management_Validation_PartsSalesOrder_BitachApproceSuccess;
            }
        }

        public string Management_Validation_PartsSalesOrder_InitialAmount {
            get {
                return Resources.PartsSalesUIStrings.Management_Validation_PartsSalesOrder_InitialAmount;
            }
        }

        public string Management_Validation_PartsSalesOrder_InitialAmountPart {
            get {
                return Resources.PartsSalesUIStrings.Management_Validation_PartsSalesOrder_InitialAmountPart;
            }
        }

        public string Management_Validation_PartsSalesOrder_NoData {
            get {
                return Resources.PartsSalesUIStrings.Management_Validation_PartsSalesOrder_NoData;
            }
        }

        public string Management_Validation_PartsSalesPriceManagement_SparePartCodeIsNull {
            get {
                return Resources.PartsSalesUIStrings.Management_Validation_PartsSalesPriceManagement_SparePartCodeIsNull;
            }
        }

        public string Management_Validation_PartsSalesReturnBillManagement_BatchApprove {
            get {
                return Resources.PartsSalesUIStrings.Management_Validation_PartsSalesReturnBillManagement_BatchApprove;
            }
        }

        public string Management_Validation_PartsSalesReturnBillManagement_BatchApproveSuccess {
            get {
                return Resources.PartsSalesUIStrings.Management_Validation_PartsSalesReturnBillManagement_BatchApproveSuccess;
            }
        }

        public string Management_Validation_PartsSalesReturnBillManagement_PartsSalesSettlementEx {
            get {
                return Resources.PartsSalesUIStrings.Management_Validation_PartsSalesReturnBillManagement_PartsSalesSettlementEx;
            }
        }

        public string Management_Validation_PartsSalesReturnBillManagement_PrintTitle {
            get {
                return Resources.PartsSalesUIStrings.Management_Validation_PartsSalesReturnBillManagement_PrintTitle;
            }
        }

        public string DataGridView_ColumnItem_Title_Approver {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_Approver;
            }
        }

        public string DataGridView_ColumnItem_Title_ApproveTime {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_ApproveTime;
            }
        }

        public string DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_CenterName {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_CenterName;
            }
        }

        public string DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_CreateTime {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_CreateTime;
            }
        }

        public string DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_DealerCode {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_DealerCode;
            }
        }

        public string DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_DealerName {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_DealerName;
            }
        }

        public string DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_DiscountRate {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_DiscountRate;
            }
        }

        public string DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_OverstockPartsAmount {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_OverstockPartsAmount;
            }
        }

        public string DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_Price {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_Price;
            }
        }

        public string DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_SparePartCode {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_SparePartCode;
            }
        }

        public string DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_SparePartName {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_SparePartName;
            }
        }

        public string DataGridView_Title_CenterABCBasis_CenterCode {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_CenterABCBasis_CenterCode;
            }
        }

        public string DataGridView_Title_CenterABCBasis_NewType {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_CenterABCBasis_NewType;
            }
        }

        public string DataGridView_Title_CenterABCBasis_OldType {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_CenterABCBasis_OldType;
            }
        }

        public string DataGridView_Title_CenterABCBasis_SaleSubAmount {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_CenterABCBasis_SaleSubAmount;
            }
        }

        public string DataGridView_Title_CenterABCBasis_SaleSubFrequency {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_CenterABCBasis_SaleSubFrequency;
            }
        }

        public string DataGridView_Title_CenterABCBasis_SaleSubNumber {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_CenterABCBasis_SaleSubNumber;
            }
        }

        public string DataGridView_Title_CenterABCBasis_SubAmountPercentage {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_CenterABCBasis_SubAmountPercentage;
            }
        }

        public string DataGridView_Title_CenterABCBasis_SubFrequencyPercentage {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_CenterABCBasis_SubFrequencyPercentage;
            }
        }

        public string Management_Title_CenterABCBasis {
            get {
                return Resources.PartsSalesUIStrings.Management_Title_CenterABCBasis;
            }
        }

        public string Management_Title_CenterABCBasisQuery {
            get {
                return Resources.PartsSalesUIStrings.Management_Title_CenterABCBasisQuery;
            }
        }

        public string DataGridView_Title_PartsStockCoefficient_Type {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsStockCoefficient_Type;
            }
        }

        public string DataGridView_Title_CenterABCBasis_WeekNum {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_CenterABCBasis_WeekNum;
            }
        }

        public string DataGridView_Title_PartsStockCoefficientPrice_MaxPrice {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsStockCoefficientPrice_MaxPrice;
            }
        }

        public string DataGridView_Title_PartsStockCoefficientPrice_MinPrice {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsStockCoefficientPrice_MinPrice;
            }
        }

        public string DataGridView_Title_PartsStockCoefficient_ProductType {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsStockCoefficient_ProductType;
            }
        }

        public string DataEditView_Validation_partsStockCoefficientDetails_MaxPriceIsNull {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Validation_partsStockCoefficientDetails_MaxPriceIsNull;
            }
        }

        public string DataEditView_Validation_partsStockCoefficientDetails_MinpriceIsNull {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Validation_partsStockCoefficientDetails_MinpriceIsNull;
            }
        }

        public string DataEditView_Validation_partsStockCoefficient_ProductTypeIsNull {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Validation_partsStockCoefficient_ProductTypeIsNull;
            }
        }

        public string DataEditView_Validation_partsStockCoefficient_TypeIsNull {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Validation_partsStockCoefficient_TypeIsNull;
            }
        }

        public string BusinessName_PartsStockCoefficient {
            get {
                return Resources.PartsSalesUIStrings.BusinessName_PartsStockCoefficient;
            }
        }

        public string DataEditView_Validation_partsStockCoefficientDetails_priceReport {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Validation_partsStockCoefficientDetails_priceReport;
            }
        }

        public string Management_Title_PartsStockCoefficient {
            get {
                return Resources.PartsSalesUIStrings.Management_Title_PartsStockCoefficient;
            }
        }

        public string Management_Title_PartsStockCoefficientQuery {
            get {
                return Resources.PartsSalesUIStrings.Management_Title_PartsStockCoefficientQuery;
            }
        }

        public string DataEditView_Validation_ABCTypeSafeDay_DaysIsNull {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_Validation_ABCTypeSafeDay_DaysIsNull;
            }
        }

        public string DataGridView_Title_ABCTypeSafeDays_Days {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_ABCTypeSafeDays_Days;
            }
        }

        public string Management_Title_ABCTypeSafeDayQuery {
            get {
                return Resources.PartsSalesUIStrings.Management_Title_ABCTypeSafeDayQuery;
            }
        }

        public string Management_Title_ABCTypeSafeDays {
            get {
                return Resources.PartsSalesUIStrings.Management_Title_ABCTypeSafeDays;
            }
        }

        public string DataGridView_Title_PartsSalesWeeklyBase_CreateTime {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesWeeklyBase_CreateTime;
            }
        }

        public string DataGridView_Title_PartsSalesWeeklyBase_Year {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesWeeklyBase_Year;
            }
        }

        public string Management_Title_PartsSalesWeeklyBase {
            get {
                return Resources.PartsSalesUIStrings.Management_Title_PartsSalesWeeklyBase;
            }
        }

        public string Management_Title_PartsSalesWeeklyBaseQuery {
            get {
                return Resources.PartsSalesUIStrings.Management_Title_PartsSalesWeeklyBaseQuery;
            }
        }

        public string DataGridView_SalesOrderDailyAvg_ArriveCycle {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_ArriveCycle;
            }
        }

        public string DataGridView_SalesOrderDailyAvg_OrderCycle {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_OrderCycle;
            }
        }

        public string DataGridView_SalesOrderDailyAvg_ReserveCoefficient {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_ReserveCoefficient;
            }
        }

        public string DataGridView_SalesOrderDailyAvg_SafeStockDays {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_SafeStockDays;
            }
        }

        public string DataGridView_SalesOrderDailyAvg_Type {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_Type;
            }
        }

        public string DataGridView_SalesOrderDailyAvg_OnLineQty {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_OnLineQty;
            }
        }

        public string DataGridView_SalesOrderDailyAvg_OweQty {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_OweQty;
            }
        }

        public string DataGridView_SalesOrderDailyAvg_PurchasePrice {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_PurchasePrice;
            }
        }

        public string DataGridView_SalesOrderDailyAvg_StandardQty {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_StandardQty;
            }
        }

        public string DataGridView_SalesOrderDailyAvg_StockUpperCoefficient {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_StockUpperCoefficient;
            }
        }

        public string DataGridView_SalesOrderDailyAvg_T1WeekAvgQty {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_T1WeekAvgQty;
            }
        }

        public string DataGridView_SalesOrderDailyAvg_T2WeekAvgQty {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_T2WeekAvgQty;
            }
        }

        public string DataGridView_SalesOrderDailyAvg_T3WeekAvgQty {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_T3WeekAvgQty;
            }
        }

        public string DataGridView_SalesOrderDailyAvg_TWeekActualAvgQty {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_TWeekActualAvgQty;
            }
        }

        public string DataGridView_SalesOrderDailyAvg_TWeekAvgQty {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_TWeekAvgQty;
            }
        }

        public string Management_Title_SalesOrderDailyAvg {
            get {
                return Resources.PartsSalesUIStrings.Management_Title_SalesOrderDailyAvg;
            }
        }

        public string DataGridView_SalesOrderDailyAvg_ExpectPlanQty {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_ExpectPlanQty;
            }
        }

        public string DataGridView_SalesOrderDailyAvg_LogisticsCycle {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_LogisticsCycle;
            }
        }

        public string DataGridView_SalesOrderDailyAvg_StockUpperQty {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_StockUpperQty;
            }
        }

        public string Management_Title_CenterAccumulateDailyExp {
            get {
                return Resources.PartsSalesUIStrings.Management_Title_CenterAccumulateDailyExp;
            }
        }

        public string Management_Title_CenterAccumulateDailyExp_Query {
            get {
                return Resources.PartsSalesUIStrings.Management_Title_CenterAccumulateDailyExp_Query;
            }
        }

        public string DataGridView_SalesOrderDailyAvg_RecommendQty {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_RecommendQty;
            }
        }

        public string Management_Title_SalesOrderRecommendWeekly {
            get {
                return Resources.PartsSalesUIStrings.Management_Title_SalesOrderRecommendWeekly;
            }
        }

        public string Management_Title_SalesOrderRecommendWeekly_Query {
            get {
                return Resources.PartsSalesUIStrings.Management_Title_SalesOrderRecommendWeekly_Query;
            }
        }

        public string DataGridView_SalesOrderDailyAvg_ActualStockUpperQty {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_ActualStockUpperQty;
            }
        }

        public string DataGridView_SalesOrderDailyAvg_ForceReserveQty {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_ForceReserveQty;
            }
        }

        public string DataGridView_SalesOrderDailyAvg_StockUpperQtys {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_StockUpperQtys;
            }
        }

        public string Management_Title_SalesOrderRecommendForce {
            get {
                return Resources.PartsSalesUIStrings.Management_Title_SalesOrderRecommendForce;
            }
        }

        public string Management_Title_SalesOrderRecommendForce_Query {
            get {
                return Resources.PartsSalesUIStrings.Management_Title_SalesOrderRecommendForce_Query;
            }
        }

        public string DataGridView_Title_CenterABCBasis_CenterName {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_CenterABCBasis_CenterName;
            }
        }

        public string DataGridView_Column_Text_MktName {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Column_Text_MktName;
            }
        }

        public string DataGridView_SalesOrderDailyAvg_CenterSubmit {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_CenterSubmit;
            }
        }

        public string DataGridView_SalesOrderDailyAvg_CumulativeFst {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_CumulativeFst;
            }
        }

        public string Management_Title_SalesOrderDailyAvgQuery {
            get {
                return Resources.PartsSalesUIStrings.Management_Title_SalesOrderDailyAvgQuery;
            }
        }

        public string Action_Title_InitApprove {
            get {
                return Resources.PartsSalesUIStrings.Action_Title_InitApprove;
            }
        }

        public string Action_Title_Submit {
            get {
                return Resources.PartsSalesUIStrings.Action_Title_Submit;
            }
        }

        public string CustomerSupplyInitialFeeSet_AppointShippingDays {
            get {
                return Resources.PartsSalesUIStrings.CustomerSupplyInitialFeeSet_AppointShippingDays;
            }
        }

        public string CustomerSupplyInitialFeeSet_Validations_AppointShippingDays {
            get {
                return Resources.PartsSalesUIStrings.CustomerSupplyInitialFeeSet_Validations_AppointShippingDays;
            }
        }

        public string DataEditPanel_Text_PartsPackingTask_MInSalesAmount2 {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_MInSalesAmount2;
            }
        }

        public string DataEditPanel_Text_PartsSalesReturnBill_PartsSalesReturnBillRemark {
            get {
                return Resources.PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_PartsSalesReturnBillRemark;
            }
        }

        public string DataEditView_OverstockPartsRecommendBill_Validation_DisCount {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_OverstockPartsRecommendBill_Validation_DisCount;
            }
        }

        public string DataEditView_OverstockPartsRecommendBill_Validation_DisCountZero {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_OverstockPartsRecommendBill_Validation_DisCountZero;
            }
        }

        public string DataEditView_PartssalesOrder_FeeOver {
            get {
                return Resources.PartsSalesUIStrings.DataEditView_PartssalesOrder_FeeOver;
            }
        }

        public string DataEdit_Text_PartsSalesSettlement_Discount {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesSettlement_Discount;
            }
        }

        public string DataEdit_Text_PartsSalesSettlement_PlannedPriceTotalAmount {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_Text_PartsSalesSettlement_PlannedPriceTotalAmount;
            }
        }

        public string DataEdit__PartsSalesOrder_RejectReason {
            get {
                return Resources.PartsSalesUIStrings.DataEdit__PartsSalesOrder_RejectReason;
            }
        }

        public string DataEdit__PartsSalesOrder_ServiceApplyCode {
            get {
                return Resources.PartsSalesUIStrings.DataEdit__PartsSalesOrder_ServiceApplyCode;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsSalesRtnSettlementRef_SourceBillCreateTime {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesRtnSettlementRef_SourceBillCreateTime;
            }
        }

        public string DataGridView_Title_PartsSalesSettlementEx_InvoiceAmountDifference {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesSettlementEx_InvoiceAmountDifference;
            }
        }

        public string DataGridView_Title_PartsSalesSettlementEx_InvoicePath {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_Title_PartsSalesSettlementEx_InvoicePath;
            }
        }

        public string DataManagementView_Notification_SubmitSuccess2 {
            get {
                return Resources.PartsSalesUIStrings.DataManagementView_Notification_SubmitSuccess2;
            }
        }

        public string DataQueryPanel_IsZero {
            get {
                return Resources.PartsSalesUIStrings.DataQueryPanel_IsZero;
            }
        }

        public string DetailPanel_Text_PartsSalesReturnBill_IsWarrantyReturn {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsSalesReturnBill_IsWarrantyReturn;
            }
        }

        public string DataGridView_ColumnItem_Title_ApproveComment {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_ApproveComment;
            }
        }

        public string DataGridView_ColumnItem_Title_InvoiceCode {
            get {
                return Resources.PartsSalesUIStrings.DataGridView_ColumnItem_Title_InvoiceCode;
            }
        }

        public string Management_Title_OverstockPartsRecommendBillMangement {
            get {
                return Resources.PartsSalesUIStrings.Management_Title_OverstockPartsRecommendBillMangement;
            }
        }

        public string Management_Title_PartsSalesPriceGroupManagement {
            get {
                return Resources.PartsSalesUIStrings.Management_Title_PartsSalesPriceGroupManagement;
            }
        }

        public string DetailPanel_Text_PartsSalesOrder_CenterCompanyCode {
            get {
                return Resources.PartsSalesUIStrings.DetailPanel_Text_PartsSalesOrder_CenterCompanyCode;
            }
        }

        public string DataEdit_PartsSalesOrder_IfDirectProvision {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_PartsSalesOrder_IfDirectProvision;
            }
        }

        public string DataEdit_PartsSalesOrder_RequestedDeliveryTime {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_PartsSalesOrder_RequestedDeliveryTime;
            }
        }

        public string DataEdit_PartsSalesOrder_SubmitCompanyName {
            get {
                return Resources.PartsSalesUIStrings.DataEdit_PartsSalesOrder_SubmitCompanyName;
            }
        }

}
}
