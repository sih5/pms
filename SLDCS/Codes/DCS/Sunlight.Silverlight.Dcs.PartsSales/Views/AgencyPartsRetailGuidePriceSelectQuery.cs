﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsBaseInfo", "PartsSalesPrice", "AgencyPartsRetailGuidePriceSelect")]
    public class AgencyPartsRetailGuidePriceSelectQuery : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        public AgencyPartsRetailGuidePriceSelectQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.DataManagementView_Title_PartsRetailGuidePriceSelect;
        }
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsRetailGuidePriceSelect"));
            }
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "AgencyPartsRetailGuidePriceSelect"
                };
            }
        }
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            throw new NotImplementedException();
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var composite = filterItem as CompositeFilterItem;
            if(composite == null)
                return;
            var branchFilter = composite.Filters.First(v => v.MemberName == "BranchId");
            if(branchFilter == null || branchFilter.Value == null) {
                UIHelper.ShowNotification(PartsSalesUIStrings.Management_Validation_AgencyPartsRetail_BranchId);
                return;
            }
            ClientVar.ConvertTime(composite);
            this.DataGridView.FilterItem = composite;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
