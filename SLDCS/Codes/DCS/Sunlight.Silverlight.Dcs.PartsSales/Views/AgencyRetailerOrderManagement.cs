﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsSales", "PartsSales", "AgencyRetailerOrder", ActionPanelKeys = new[] {
         "AgencyRetailerOrder"      
    })]
    public class AgencyRetailerOrderManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();


        public AgencyRetailerOrderManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.Management_Title_AgencyRetailerOrderManagement;

        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("AgencyRetailerOrder"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("AgencyRetailerOrderForConfirm");
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "AgencyRetailerOrder"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            dcsDomainContext.Load(dcsDomainContext.GetCompaniesQuery().Where(r => r.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity == null)
                    return;
                if(entity.Type == (int)DcsCompanyType.分公司) {
                    compositeFilterItem.Filters.Add(new FilterItem {
                        MemberName = "BranchId",
                        Operator = FilterOperator.IsEqualTo,
                        MemberType = typeof(int),
                        Value = BaseApp.Current.CurrentUserData.EnterpriseId
                    });
                }
                if(entity.Type == (int)DcsCompanyType.代理库) {
                    compositeFilterItem.Filters.Add(new FilterItem {
                        MemberName = "ShippingCompanyId",
                        Operator = FilterOperator.IsEqualTo,
                        MemberType = typeof(int),
                        Value = BaseApp.Current.CurrentUserData.EnterpriseId
                    });
                }

                this.DataGridView.FilterItem = compositeFilterItem;
                this.DataGridView.ExecuteQueryDelayed();
            }, null);



        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.CONFIRM:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<AgencyRetailerOrder>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsAgencyRetailerOrderStatus.新建 || entities[0].Status == (int)DcsAgencyRetailerOrderStatus.部分确认;
                case CommonActionKeys.TERMINATE:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities1 = this.DataGridView.SelectedEntities.Cast<AgencyRetailerOrder>().ToArray();
                    if(entities1.Length != 1)
                        return false;
                    return entities1[0].Status == (int)DcsAgencyRetailerOrderStatus.新建 || entities1[0].Status == (int)DcsAgencyRetailerOrderStatus.部分确认;
                case CommonActionKeys.MERGEEXPORT:
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;


            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.CONFIRM:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.TERMINATE:
                    DcsUtils.Confirm(PartsSalesUIStrings.Management_Validation_AgencyPartsRetail_Stop, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<AgencyRetailerOrder>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can终止代理库电商订单)
                                entity.终止代理库电商订单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_TerminateSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<AgencyRetailerOrder>().Select(r => r.Id).ToArray();
                        this.ExportAgencyRetailerOrderWithDetails(ids, null, null, null, null, null, null, null, null, null, null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        int? branchId = null;
                        int? shippingCompanyId = null;
                        var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var vehiclePartsHandleOrderCode = filterItem.Filters.Single(r => r.MemberName == "VehiclePartsHandleOrderCode").Value as string;
                        var eRPSourceOrderCode = filterItem.Filters.Single(r => r.MemberName == "ERPSourceOrderCode").Value as string;
                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                        if(filterItem.Filters.Any(r => r.MemberName == "BranchId"))
                            branchId = filterItem.Filters.FirstOrDefault(e => e.MemberName == "BranchId").Value as int?;
                        if(filterItem.Filters.Any(r => r.MemberName == "ShippingCompanyId"))
                            shippingCompanyId = filterItem.Filters.FirstOrDefault(e => e.MemberName == "ShippingCompanyId").Value as int?;
                        var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                        var createTime = filterItem.Filters.FirstOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem) && ((CompositeFilterItem)filter).Filters.Any(e => e.MemberName == "CreateTime")) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        this.ExportAgencyRetailerOrderWithDetails(null, branchId, shippingCompanyId, eRPSourceOrderCode, code, warehouseId, partsSalesCategoryId, vehiclePartsHandleOrderCode, status, createTimeBegin, createTimeEnd);
                    }
                    break;

                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<AgencyRetailerOrder>().Select(r => r.Id).ToArray();
                        this.ExportAgencyRetailerOrders(ids, null, null, null, null, null, null, null, null, null, null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        int? branchId = null;
                        int? shippingCompanyId = null;
                        var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var vehiclePartsHandleOrderCode = filterItem.Filters.Single(r => r.MemberName == "VehiclePartsHandleOrderCode").Value as string;
                        var eRPSourceOrderCode = filterItem.Filters.Single(r => r.MemberName == "ERPSourceOrderCode").Value as string;
                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                        if(filterItem.Filters.Any(r => r.MemberName == "BranchId"))
                            branchId = filterItem.Filters.FirstOrDefault(e => e.MemberName == "BranchId").Value as int?;
                        if(filterItem.Filters.Any(r => r.MemberName == "ShippingCompanyId"))
                            shippingCompanyId = filterItem.Filters.FirstOrDefault(e => e.MemberName == "ShippingCompanyId").Value as int?;
                        var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                        var createTime = filterItem.Filters.FirstOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem) && ((CompositeFilterItem)filter).Filters.Any(e => e.MemberName == "CreateTime")) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        this.ExportAgencyRetailerOrders(null, branchId, shippingCompanyId, eRPSourceOrderCode, code, warehouseId, partsSalesCategoryId, vehiclePartsHandleOrderCode, status, createTimeBegin, createTimeEnd);
                    }
                    break;
            }
        }

        private void ExportAgencyRetailerOrders(int[] ids, int? branchId, int? shippingCompanyId, string eRPSourceOrderCode, string code, int? warehouseId, int? partsSalesCategoryId, string vehiclePartsHandleOrderCode, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportAgencyRetailerOrderAsync(ids, branchId, shippingCompanyId, eRPSourceOrderCode, code, warehouseId, partsSalesCategoryId, vehiclePartsHandleOrderCode, status, createTimeBegin, createTimeEnd);
            this.excelServiceClient.ExportAgencyRetailerOrderCompleted += excelServiceClient_ExportAgencyRetailerOrderCompleted;
        }

        private void excelServiceClient_ExportAgencyRetailerOrderCompleted(object sender, ExportAgencyRetailerOrderCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }


        private void ExportAgencyRetailerOrderWithDetails(int[] ids, int? branchId, int? shippingCompanyId, string eRPSourceOrderCode, string code, int? warehouseId, int? partsSalesCategoryId, string vehiclePartsHandleOrderCode, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportAgencyRetailerOrderWithDetailsAsync(ids, branchId, shippingCompanyId, eRPSourceOrderCode, code, warehouseId, partsSalesCategoryId, vehiclePartsHandleOrderCode, status, createTimeBegin, createTimeEnd);
            this.excelServiceClient.ExportAgencyRetailerOrderWithDetailsCompleted += excelServiceClient_ExportAgencyRetailerOrderWithDetailsCompleted;
        }

        private void excelServiceClient_ExportAgencyRetailerOrderWithDetailsCompleted(object sender, ExportAgencyRetailerOrderWithDetailsCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }
    }
}