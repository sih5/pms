﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsSales", "PartsSalesSettlement", "SettlementAutomaticTaskSetQuery", ActionPanelKeys = new[]{
        CommonActionKeys.EXPORT
    })]
    public class AutoTaskExecutResultManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private DataGridViewBase DataGridView {
            get {
                return dataGridView ?? (this.dataGridView = DI.GetDataGridView("AutoTaskExecutResult"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[]{
                 "AutoTaskExecutResult"
             };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<AutoTaskExecutResult>().Select(r => r.Id).ToArray();
                        this.excelServiceClient.ExportAutoTaskExecutResultAsync(ids, null, null, null, null, null, null);
                        this.excelServiceClient.ExportAutoTaskExecutResultCompleted -= excelServiceClient_ExportAutoTaskExecutResultCompleted;
                        this.excelServiceClient.ExportAutoTaskExecutResultCompleted += excelServiceClient_ExportAutoTaskExecutResultCompleted;

                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;

                        var brandId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "BrandId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "BrandId").Value as int?;
                        var companyCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "CompanyCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "CompanyCode").Value as string;

                        var executionStatus = filterItem.Filters.SingleOrDefault(r => r.MemberName == "ExecutionStatus") == null ? null : filterItem.Filters.Single(r => r.MemberName == "ExecutionStatus").Value as int?;
                        var companyName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "CompanyName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "CompanyName").Value as string;
                        var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "ExecutionTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "ExecutionTime").Value as DateTime?;
                        }
                        this.excelServiceClient.ExportAutoTaskExecutResultAsync(new int[] { }, brandId, executionStatus, companyCode, companyName, createTimeBegin, createTimeEnd);
                        this.excelServiceClient.ExportAutoTaskExecutResultCompleted -= excelServiceClient_ExportAutoTaskExecutResultCompleted;
                        this.excelServiceClient.ExportAutoTaskExecutResultCompleted += excelServiceClient_ExportAutoTaskExecutResultCompleted;
                    }
                    break;
                default:
                    break;
            }
        }
        private void excelServiceClient_ExportAutoTaskExecutResultCompleted(object sender, ExportAutoTaskExecutResultCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey == DATA_EDIT_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        public AutoTaskExecutResultManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.Management_Title_AutoTaskExecutResultManagement;
        }
    }
}
