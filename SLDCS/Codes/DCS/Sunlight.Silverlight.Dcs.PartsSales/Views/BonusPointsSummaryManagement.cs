﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsSales", "PartsSalesSettlement", "BonusPointsSummary", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_ABANDON_APPROVE_EXPORT_MERGEEXPORT
    })]
    public class BonusPointsSummaryManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        public BonusPointsSummaryManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.Management_Title_BonusPointsSummaryManagement;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("BonusPointsSummary"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("BonusPointsSummary");
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataEditView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var bonusPointsSummary = this.DataEditView.CreateObjectToEdit<BonusPointsSummary>();
                    bonusPointsSummary.BonusPointsSummaryCode = GlobalVar.ASSIGNED_BY_SERVER;
                    bonusPointsSummary.TaxRate = 0.17;
                    bonusPointsSummary.Status = (int)DcsBonusPointsSummaryStatus.新建;
                    bonusPointsSummary.SummaryAmount = 0;
                    bonusPointsSummary.SummaryBonusPoints = 0;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsSalesUIStrings.Management_Validation_BonusPointsSummaryManagement_Anandon, () => {
                        var ids = this.DataGridView.SelectedEntities.Cast<BonusPointsSummary>().Select(r => r.Id).ToArray();
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(domainContext != null) {
                            ShellViewModel.Current.IsBusy = true;
                            domainContext.作废积分汇总单(ids, invokeOp => {
                                ShellViewModel.Current.IsBusy = false;
                                if(invokeOp.HasError)
                                    return;
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.DataGridView.ExecuteQueryDelayed();
                            }, null);
                        }
                    });
                    break;
                case CommonActionKeys.APPROVE:
                     DcsUtils.Confirm(PartsSalesUIStrings.Management_Validation_BonusPointsSummaryManagement_Approve, () => {
                        var ids = this.DataGridView.SelectedEntities.Cast<BonusPointsSummary>().Select(r => r.Id).ToArray();
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(domainContext != null) {
                            ShellViewModel.Current.IsBusy = true;
                            domainContext.审核积分汇总单(ids, invokeOp => {
                                ShellViewModel.Current.IsBusy = false;
                                if(invokeOp.HasError)
                                    return;
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_ApproveSuccess);
                                this.DataGridView.ExecuteQueryDelayed();
                            }, null);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<BonusPointsSummary>().Select(r => r.Id).ToArray();
                        this.导出积分汇总单(ids, null, null, null, null, null, null);
                     } else {
                         var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                         if(filterItem == null)
                             return;
                         var bonusPointsSummaryCode = filterItem.Filters.Single(r => r.MemberName == "BonusPointsSummaryCode").Value as string;
                         var status = filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                         var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                         DateTime? createTimeBegin = null;
                         DateTime? createTimeEnd = null;
                         if(createTime != null) {
                             createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                             createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                         }


                         var checkTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                         DateTime? checkTimeBegin = null;
                         DateTime? checkTimeEnd = null;
                         if(checkTime != null) {
                             checkTimeBegin = checkTime.Filters.First(r => r.MemberName == "ApproveTime").Value as DateTime?;
                             checkTimeEnd = checkTime.Filters.Last(r => r.MemberName == "ApproveTime").Value as DateTime?;
                         }

                         this.导出积分汇总单(new int[] { }, bonusPointsSummaryCode, status, createTimeBegin, createTimeEnd, checkTimeBegin, checkTimeEnd);
                     }
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<BonusPointsSummary>().Select(r => r.Id).ToArray();
                        this.合并导出积分汇总单(ids, null, null, null, null, null, null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var bonusPointsSummaryCode = filterItem.Filters.Single(r => r.MemberName == "BonusPointsSummaryCode").Value as string;
                        var status = filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }


                        var checkTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? checkTimeBegin = null;
                        DateTime? checkTimeEnd = null;
                        if(checkTime != null) {
                            checkTimeBegin = checkTime.Filters.First(r => r.MemberName == "ApproveTime").Value as DateTime?;
                            checkTimeEnd = checkTime.Filters.Last(r => r.MemberName == "ApproveTime").Value as DateTime?;
                        }

                        this.合并导出积分汇总单(new int[] { }, bonusPointsSummaryCode, status, createTimeBegin, createTimeEnd, checkTimeBegin, checkTimeEnd);
                    }
                    break;
            }
        }



        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.APPROVE:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitie = this.DataGridView.SelectedEntities.Cast<BonusPointsSummary>().ToArray();
                    if(!entitie.Any())
                        return false;
                    int count = entitie.Count(r => r.Status != (int)DcsBonusPointsSummaryStatus.新建);
                    if(count > 0)
                        return false;
                    return true;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private void 导出积分汇总单(int[] ids, string bonusPointsSummaryCode, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? checkTimeBegin, DateTime? checkTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.导出积分汇总单Async(ids, bonusPointsSummaryCode, status, createTimeBegin, createTimeEnd, checkTimeBegin, checkTimeEnd);
            this.excelServiceClient.导出积分汇总单Completed -= excelServiceClient_导出积分汇总单Completed;
            this.excelServiceClient.导出积分汇总单Completed += excelServiceClient_导出积分汇总单Completed;
        }

        private void excelServiceClient_导出积分汇总单Completed(object sender, 导出积分汇总单CompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        private void 合并导出积分汇总单(int[] ids, string bonusPointsSummaryCode, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? checkTimeBegin, DateTime? checkTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.合并导出积分汇总单Async(ids, bonusPointsSummaryCode, status, createTimeBegin, createTimeEnd, checkTimeBegin, checkTimeEnd);
            this.excelServiceClient.合并导出积分汇总单Completed -= excelServiceClient_合并导出积分汇总单Completed;
            this.excelServiceClient.合并导出积分汇总单Completed += excelServiceClient_合并导出积分汇总单Completed;
        }

        private void excelServiceClient_合并导出积分汇总单Completed(object sender, 合并导出积分汇总单CompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "BonusPointsSummary"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            if(filterItem is CompositeFilterItem) {
                var compositeFilter = filterItem as CompositeFilterItem;
                this.SwitchViewTo(DATA_GRID_VIEW);
                this.DataGridView.FilterItem = compositeFilter;
                this.DataGridView.ExecuteQueryDelayed();
            }
        }
    }
}
