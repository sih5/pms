﻿using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;
using System;
using System.ServiceModel.DomainServices.Client;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsSales", "IntelligentPrediction", "CenterABCBasis", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT})]
    public class CenterABCBasisManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        public CenterABCBasisManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.Management_Title_CenterABCBasis;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("CenterABCBasis"));
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "CenterABCBasis"
                };
            }
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId)
            {
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var centerCode = filterItem.Filters.Single(e => e.MemberName == "CenterCode").Value as string;
                    var centerName = filterItem.Filters.Single(e => e.MemberName == "CenterName").Value as string;
                    var newType = filterItem.Filters.Single(e => e.MemberName == "NewType").Value as int?;
                    var sparePartCode = filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                    var oldType = filterItem.Filters.Single(e => e.MemberName == "OldType").Value as int?;
                    int? weekNum = null;
                    if(null != filterItem.Filters.First(e => e.MemberName == "WeekNum").Value) {
                        weekNum = Int32.Parse(filterItem.Filters.First(e => e.MemberName == "WeekNum").Value.ToString()) as int?;
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出中心库ABC分类基础(centerCode, sparePartCode, newType, oldType, weekNum, centerName, loadOp => {
                        if(loadOp.HasError) {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;                
            }

        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}

