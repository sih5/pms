﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsSales", "PartsSales", "CrossSalesOrder", ActionPanelKeys = new[] {
        "CrossSalesOrder"
    })]
    public class CrossSalesOrderManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataFirEditView;
        private DataEditViewBase dataEditDetailView;
        private DataEditViewBase dataApproveView;
        private DataEditViewBase dataSeniorApproveView;
        private DataEditViewBase dataApproveForDirectView;
        private DataEditViewBase dataApproveForRebateView;
        private DataEditViewBase dataDetailView;
        private const string DATA_DETAIL_VIEW = "_DataDetailView_";
        private const string DATA_DIRECT_VIEW = "_DataDirectView_";
        private const string DATA_EDITDETAIL_VIEW = "_DataEditDetailView_";
        private const string DATA_APPROVE_VIEW = "DataApproveView";
        private const string DATA_SENIORAPPROVE_VIEW = "DataSeniorApproveView";
        private const string DATA_REBATE_VIEW = "_DataRebateView_";
        private const string DATA_LOGISTICS_VIEW = "_DATALOGISTICSVIEW_";
        private const string DATA_FIR_VIEW = "_DataFirEditView_";
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();


        public CrossSalesOrderManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = "跨区销售备案管理";
        }

        private DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("CrossSalesOrder");
                    this.dataGridView.RowDoubleClick += DataGridView_RowDoubleClick;
                }
                return this.dataGridView;
            }
        }
        private void DataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                this.DataEditViewDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                this.SwitchViewTo(DATA_DETAIL_VIEW);
            }
        }
        private DataEditViewBase DataEditViewDetail {
            get {
                if(this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("CrossSalesOrderDetail");
                    this.dataDetailView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataDetailView;
            }

        }
        //初审
        private DataEditViewBase DataFirEditView {
            get {
                if(this.dataFirEditView == null) {
                    this.dataFirEditView = DI.GetDataEditView("CrossSalesOrderFir");
                    ((CrossSalesOrderFirDataEditView)this.dataFirEditView).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataFirEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataFirEditView;
            }
        }
        //新增修改
        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("CrossSalesOrder");
                    var item = (CrossSalesOrderDataEditView)this.dataEditView;
                    item.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        //审核
        private DataEditViewBase DataApproveView {
            get {
                if(this.dataApproveView == null) {
                    this.dataApproveView = DI.GetDataEditView("CrossSalesOrderCheck");
                    ((CrossSalesOrderCheckDataEditView)this.dataApproveView).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataApproveView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataApproveView;
            }
        }

        //高级审批
        private DataEditViewBase DataSeniorApproveView {
            get {
                if(this.dataSeniorApproveView == null) {
                    this.dataSeniorApproveView = DI.GetDataEditView("CrossSalesOrderCheckSenior");
                    ((CrossSalesOrderCheckSeniorDataEditView)this.dataSeniorApproveView).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataSeniorApproveView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataSeniorApproveView;
            }
        }

        //双击查看详情
        private DataEditViewBase DataEditDetailView {
            get {
                if(this.dataEditDetailView == null) {
                    this.dataEditDetailView = DI.GetDataEditView("CrossSalesOrderDetail");
                    this.dataEditDetailView.EditCancelled += this.DataEditView_EditCancelled;
                    this.dataEditDetailView.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataEditDetailView;
            }

        }
        //完善资料
        private DataEditViewBase DataApproveForDirectView {
            get {
                if(this.dataApproveForDirectView == null) {
                    this.dataApproveForDirectView = DI.GetDataEditView("CrossSalesOrderFinish");
                    ((CrossSalesOrderFinishDataEditView)this.dataApproveForDirectView).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataApproveForDirectView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataApproveForDirectView;
            }
        }
        //审批
        private DataEditViewBase DataApproveForRebateView {
            get {
                if(this.dataApproveForRebateView == null) {
                    this.dataApproveForRebateView = DI.GetDataEditView("CrossSalesOrderClose");
                    ((CrossSalesOrderCloseDataEditView)this.dataApproveForRebateView).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataApproveForRebateView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataApproveForRebateView;
            }
        }


        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => DataEditView);
            this.RegisterView(DATA_FIR_VIEW, () => dataFirEditView);
            this.RegisterView(DATA_EDITDETAIL_VIEW, () => DataEditView);
            this.RegisterView(DATA_REBATE_VIEW, () => this.DataApproveForRebateView);
            this.RegisterView(DATA_APPROVE_VIEW, () => this.DataApproveView);
            this.RegisterView(DATA_SENIORAPPROVE_VIEW, () => this.DataSeniorApproveView);
            this.RegisterView(DATA_EDITDETAIL_VIEW, () => this.DataEditDetailView);
            this.RegisterView(DATA_DIRECT_VIEW, () => this.DataApproveForDirectView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataEditViewDetail);
        }


        private void ResetEditView() {
            this.dataEditView = null;
            this.dataFirEditView = null;
            this.dataEditDetailView = null;
            this.dataApproveView = null;
            this.dataSeniorApproveView = null;
            this.dataApproveForDirectView = null;
            this.dataApproveForRebateView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }
        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();

            ResetEditView();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "CrossSalesOrder"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case "Add"://新增
                    var crossSalesOrder = this.DataEditView.CreateObjectToEdit<CrossSalesOrder>();
                    crossSalesOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    crossSalesOrder.IsAutoSales = true;
                    crossSalesOrder.ApplyCompnayId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    crossSalesOrder.ApplyCompnayCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                    crossSalesOrder.ApplyCompnayName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    crossSalesOrder.Status = (int)DCSCrossSalesOrderStatus.新建;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT://修改
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON://作废
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<CrossSalesOrder>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废跨区域销售单)
                                entity.作废跨区域销售单();
                            this.ExecuteSerivcesMethod(PartsSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.SUBMIT://提交
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Submit, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<CrossSalesOrder>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can提交跨区域销售单)
                                entity.提交跨区域销售单();
                            this.ExecuteSerivcesMethod(PartsSalesUIStrings.DataManagementView_Notification_SubmitSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "InitialApprove": //初审
                    this.DataFirEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_FIR_VIEW);
                    break;
                case CommonActionKeys.AUDIT://审核
                    this.DataApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_APPROVE_VIEW);
                    break;
                case CommonActionKeys.APPROVE://审批
                    this.DataApproveForRebateView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_REBATE_VIEW);
                    break;

                case "SeniorAudit"://高级审核
                    this.DataSeniorApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_SENIORAPPROVE_VIEW);
                    break;
                case "Finish"://高级审核
                    this.DataApproveForDirectView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_DIRECT_VIEW);
                    break;

                case CommonActionKeys.TERMINATE:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Terminate, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<CrossSalesOrder>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can终止跨区域销售单)
                                entity.终止跨区域销售单();
                            this.ExecuteSerivcesMethod(PartsSalesUIStrings.DataManagementView_Notification_TerminateSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;

                case CommonActionKeys.MERGEEXPORT: {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                            var id = this.DataGridView.SelectedEntities.Cast<CrossSalesOrder>().First().Id;
                            this.ExportCrossSalesOrderWithDetails(id, null, null, null, null, null, null, null, null, null, null, null, null);
                        } else {
                            var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                            var applyCompnayCode = filterItem.Filters.Single(r => r.MemberName == "ApplyCompnayCode").Value as string;
                            var applyCompnayName = filterItem.Filters.Single(r => r.MemberName == "ApplyCompnayName").Value as string;
                            var subCompanyCode = filterItem.Filters.Single(r => r.MemberName == "SubCompanyCode").Value as string;
                            var subCompanyName = filterItem.Filters.Single(r => r.MemberName == "SubCompanyName").Value as string;
                            var sparePartCode = filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                            var sparePartName = filterItem.Filters.Single(r => r.MemberName == "SparePartName").Value as string;

                            var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;

                            DateTime? createTimeBegin = null;
                            DateTime? createTimeEnd = null;
                            DateTime? submitTimeBegin = null;
                            DateTime? submitTimeEnd = null;
                            List<int> statusList = new List<int>();
                            var dateTimeFilterItems = filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem));
                            foreach(var dateTimeFilterItem in dateTimeFilterItems) {
                                var dateTime = dateTimeFilterItem as CompositeFilterItem;
                                if(dateTime != null) {
                                    if(dateTime.Filters.Any(r => r.MemberName == "CreateTime")) {
                                        createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                        createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    }
                                    if(dateTime.Filters.Any(r => r.MemberName == "SubmitTime")) {
                                        submitTimeBegin = dateTime.Filters.First(r => r.MemberName == "SubmitTime").Value as DateTime?;
                                        submitTimeEnd = dateTime.Filters.Last(r => r.MemberName == "SubmitTime").Value as DateTime?;
                                    }
                                    if(dateTime.Filters.Any(r => r.MemberName == "Status")) {
                                        statusList = dateTime.Filters.Where(r => r.MemberName == "Status").Select(r => (int)r.Value).ToList();
                                    }
                                }
                            }

                            this.ExportCrossSalesOrderWithDetails(null, sparePartCode, sparePartName, applyCompnayCode, applyCompnayName, subCompanyCode, code, subCompanyName, status, createTimeBegin, createTimeEnd, submitTimeBegin, submitTimeEnd);

                        }
                        break;
                    }
            }
        }
        private void ExportCrossSalesOrderWithDetails(int? id, string sparePartCode, string sparePartName, string applyCompnayCode, string applyCompnayName, string subCompanyCode, string code, string subCompanyName, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? submitTimeBegin, DateTime? submitTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportCrossSalesOrderWithDetailsAsync(id, sparePartCode, sparePartName, applyCompnayCode, applyCompnayName, subCompanyCode, code, subCompanyName, status, createTimeBegin, createTimeEnd, submitTimeBegin, submitTimeEnd);
            this.excelServiceClient.ExportCrossSalesOrderWithDetailsCompleted -= excelServiceClient_ExportCrossSalesOrderWithDetailsCompleted;
            this.excelServiceClient.ExportCrossSalesOrderWithDetailsCompleted += excelServiceClient_ExportCrossSalesOrderWithDetailsCompleted;
        }

        private void excelServiceClient_ExportCrossSalesOrderWithDetailsCompleted(object sender, ExportCrossSalesOrderWithDetailsCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case "Add":
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.SUBMIT:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<CrossSalesOrder>().Any(r => r.Status == (int)DCSCrossSalesOrderStatus.新建) && this.DataGridView.SelectedEntities.Cast<CrossSalesOrder>().First().ApplyCompnayId==BaseApp.Current.CurrentUserData.EnterpriseId;
                case "InitialApprove":
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<CrossSalesOrder>().Any(r => r.Status == (int)DCSCrossSalesOrderStatus.提交);
                case CommonActionKeys.AUDIT:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<CrossSalesOrder>().Any(r => r.Status == (int)DCSCrossSalesOrderStatus.初审通过);
                case CommonActionKeys.APPROVE:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<CrossSalesOrder>().Any(r => r.Status == (int)DCSCrossSalesOrderStatus.审核通过);
                case "SeniorAudit":
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<CrossSalesOrder>().Any(r => r.Status == (int)DCSCrossSalesOrderStatus.审批通过);
                case "Finish":
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<CrossSalesOrder>().Any(r => r.Status == (int)DCSCrossSalesOrderStatus.高级审核通过);
                case CommonActionKeys.TERMINATE:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<CrossSalesOrder>().Any(r => r.Status != (int)DCSCrossSalesOrderStatus.新建 && r.Status != (int)DCSCrossSalesOrderStatus.终止 && r.Status != (int)DCSCrossSalesOrderStatus.作废 && r.Status != (int)DCSCrossSalesOrderStatus.生效);
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
    }
}