﻿
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;
using System;
using System.ServiceModel.DomainServices.Client;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views
{
    [PageMeta("PartsBaseInfo", "PartsSalesPrice", "CsbomParts", ActionPanelKeys = new[] {
        CommonActionKeys.MERGEEXPORT})]
    public class CsbomPartsManagement : DcsDataManagementViewBase
    {
        private DataGridViewBase dataGridView;
        public CsbomPartsManagement()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.Management_Title_CsbomPartsManagement;
        }

        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("CsbomParts"));
            }
        }

        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "CsbomParts"
                };
            }
        }
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId)
            {
                case CommonActionKeys.MERGEEXPORT:
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any())
                    {
                        var ids = this.DataGridView.SelectedEntities.Cast<VirtualCsbomParts>().Select(r => r.Id).ToArray();
                        this.ExportCsbomParts(ids, null, null, null, null, null, null, null);
                    }
                    else
                    {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if (filterItem == null)
                            return;
                        var code = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var name = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Name") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Name").Value as string;
                        var spmName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SPMName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SPMName").Value as string;
                        var englishName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "EnglishName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "EnglishName").Value as string;
                        var spmEnglishName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SPMEnglishName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SPMEnglishName").Value as string;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;

                        foreach (var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)))
                        {
                            var dateTime = filter as CompositeFilterItem;
                            if (dateTime != null)
                            {
                                if (dateTime.Filters.First().MemberName == "CreateTime")
                                {
                                    createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                            }
                        }

                        this.ExportCsbomParts(null, code, name, spmName, englishName, spmEnglishName, createTimeBegin, createTimeEnd);
                    }
                    break;

            }

        }
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public void ExportCsbomParts(int[] ids, string code, string name, string spmName, string englishName,string spmEnglishName,DateTime? createTimeBegin, DateTime? createTimeEnd)
        {
            this.excelServiceClient.ExportCsbomPartsAsync(ids, code, name, spmName, englishName, spmEnglishName, createTimeBegin, createTimeEnd);
            this.excelServiceClient.ExportCsbomPartsCompleted -= ExcelServiceClient_ExportCsbomPartsAsyncCompleted;
            this.excelServiceClient.ExportCsbomPartsCompleted += ExcelServiceClient_ExportCsbomPartsAsyncCompleted;
        }
        private void ExcelServiceClient_ExportCsbomPartsAsyncCompleted(object sender, ExportCsbomPartsCompletedEventArgs e)
        {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId)
            {
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem)
        {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}

