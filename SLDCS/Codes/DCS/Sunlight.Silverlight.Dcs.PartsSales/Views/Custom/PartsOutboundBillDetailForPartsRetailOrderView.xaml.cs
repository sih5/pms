﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Shell;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.Custom {
    public partial class PartsOutboundBillDetailForPartsRetailOrderView : IBaseView {
        private DataGridViewBase partsRetailOrderForOutboundBillDetailDataGridView;
        private DataGridViewBase partsOutboundBillDetailForPartsRetailOrderDataGridView;
        private DcsDomainContext domainContext;

        private DataGridViewBase PartsRetailOrderForOutboundBillDetailDataGridView {
            get {
                if (this.partsRetailOrderForOutboundBillDetailDataGridView == null) {
                    this.partsRetailOrderForOutboundBillDetailDataGridView = DI.GetDataGridView("PartsRetailOrderForOutboundBillDetail");
                    this.partsRetailOrderForOutboundBillDetailDataGridView.DomainContext = this.dcsDomainContext;
                    this.partsRetailOrderForOutboundBillDetailDataGridView.SelectionChanged += this.PartsRetailOrderForOutboundBillDetailDataGridView_SelectionChanged;
                }
                return this.partsRetailOrderForOutboundBillDetailDataGridView;
            }
        }

        private DataGridViewBase PartsOutboundBillDetailForPartsRetailOrderDataGridView {
            get {
                if (this.partsOutboundBillDetailForPartsRetailOrderDataGridView == null) {
                    this.partsOutboundBillDetailForPartsRetailOrderDataGridView = DI.GetDataGridView("PartsOutboundBillDetailForPartsRetailOrder");
                    this.partsOutboundBillDetailForPartsRetailOrderDataGridView.DomainContext = this.dcsDomainContext;
                    this.partsOutboundBillDetailForPartsRetailOrderDataGridView.SetValue(DataContextProperty, this);
                }
                return this.partsOutboundBillDetailForPartsRetailOrderDataGridView;
            }
        }

        private DcsDomainContext dcsDomainContext {
            get {
                return this.domainContext ?? (this.domainContext = new DcsDomainContext());
            }
        }

        private void PartsRetailOrderForOutboundBillDetailDataGridView_SelectionChanged(object sender, System.EventArgs e) {
            var partsRetailOrder = this.PartsRetailOrderForOutboundBillDetailDataGridView.SelectedEntities.Cast<PartsRetailOrder>().FirstOrDefault();
            this.PartsOutboundBillDetailForPartsRetailOrderDataGridView.DataContext = partsRetailOrder;
        }

        public PartsOutboundBillDetailForPartsRetailOrderView() {
            InitializeComponent();
            this.Initialize();
        }

        private void Initialize() {
            this.LayoutRoot.Children.Add(this.PartsRetailOrderForOutboundBillDetailDataGridView);

            this.PartsOutboundBillDetailForPartsRetailOrderDataGridView.SetValue(Grid.RowProperty, 1);
            this.LayoutRoot.Children.Add(this.PartsOutboundBillDetailForPartsRetailOrderDataGridView);
        }

        public void ExecuteQuery(FilterItem filterItem) {
            this.PartsRetailOrderForOutboundBillDetailDataGridView.FilterItem = filterItem;
            this.PartsRetailOrderForOutboundBillDetailDataGridView.ExecuteQuery();
        }

        public object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            return null;
        }

        public IEnumerable<Entity> SelectedEntities {
            get {
                return this.PartsOutboundBillDetailForPartsRetailOrderDataGridView.SelectedEntities;
            }
        }
    }
}
