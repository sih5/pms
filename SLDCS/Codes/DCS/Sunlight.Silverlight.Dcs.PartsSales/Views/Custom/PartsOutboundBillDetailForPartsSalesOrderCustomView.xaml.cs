﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Shell;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.Custom {
    public partial class PartsOutboundBillDetailForPartsSalesOrderCustomView : IBaseView {
        private DataGridViewBase partsSalesOrderForPartsOutboundBillDetailDataGridView;
        private DataGridViewBase partsOutboundBillDetailForPartsSalesOrderDataGridView;
        private DcsDomainContext domainContext;

        public PartsOutboundBillDetailForPartsSalesOrderCustomView() {
            InitializeComponent();
            this.Initialize();
        }

        private DcsDomainContext dcsDomainContext {
            get {
                return this.domainContext ?? (this.domainContext = new DcsDomainContext());
            }
        }

        private void Initialize() {
            this.PartsSalesOrderForPartsOutboundBillDetailDataGridView.SetValue(MinHeightProperty, 80.0);
            this.LayoutRoot.Children.Add(this.PartsSalesOrderForPartsOutboundBillDetailDataGridView);
            this.PartsOutboundBillDetailForPartsSalesOrderDataGridView.SetValue(MinHeightProperty, 80.0);
            this.PartsOutboundBillDetailForPartsSalesOrderDataGridView.SetValue(Grid.RowProperty, 1);
            this.LayoutRoot.Children.Add(this.PartsOutboundBillDetailForPartsSalesOrderDataGridView);
        }

        private DataGridViewBase PartsSalesOrderForPartsOutboundBillDetailDataGridView {
            get {
                if (this.partsSalesOrderForPartsOutboundBillDetailDataGridView == null) {
                    this.partsSalesOrderForPartsOutboundBillDetailDataGridView = DI.GetDataGridView("PartsSalesOrderForPartsOutboundBillDetail");
                    this.partsSalesOrderForPartsOutboundBillDetailDataGridView.DomainContext = dcsDomainContext;
                    this.partsSalesOrderForPartsOutboundBillDetailDataGridView.SelectionChanged += this.PartsSalesOrderForPartsOutboundBillDetailDataGridView_SelectionChanged;
                }
                return this.partsSalesOrderForPartsOutboundBillDetailDataGridView;
            }
        }

        void PartsSalesOrderForPartsOutboundBillDetailDataGridView_SelectionChanged(object sender, System.EventArgs e) {
            PartsOutboundBillDetailForPartsSalesOrderDataGridView.DataContext = this.PartsSalesOrderForPartsOutboundBillDetailDataGridView.SelectedEntities.FirstOrDefault();
        }


        private DataGridViewBase PartsOutboundBillDetailForPartsSalesOrderDataGridView {
            get {
                if (this.partsOutboundBillDetailForPartsSalesOrderDataGridView == null) {
                    this.partsOutboundBillDetailForPartsSalesOrderDataGridView = DI.GetDataGridView("PartsOutboundBillDetailForPartsSalesOrder");
                    this.partsOutboundBillDetailForPartsSalesOrderDataGridView.DomainContext = dcsDomainContext;
                    this.partsOutboundBillDetailForPartsSalesOrderDataGridView.SetValue(DataContextProperty, this);
                }
                return this.partsOutboundBillDetailForPartsSalesOrderDataGridView;
            }
        }


        public void ExecuteQuery(FilterItem filterItem) {
            this.PartsSalesOrderForPartsOutboundBillDetailDataGridView.FilterItem = filterItem;
            this.PartsSalesOrderForPartsOutboundBillDetailDataGridView.ExecuteQuery();
        }


        public object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            return null;
        }

        public IEnumerable<Entity> SelectedEntities {
            get {
                return this.PartsOutboundBillDetailForPartsSalesOrderDataGridView.SelectedEntities;
            }
        }
    }
}
