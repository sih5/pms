﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Controls;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.RibbonView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.Custom {
    public partial class PreOrderForReportQueryWindow {
        private Grid panelGrid;
        private ICommand searchCommand, saveCommand, abandonCommand;
        private KeyValueManager keyValueManager;

        private readonly string[] kvNames = { "PreOrderStatus" };

        protected KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private CompositeFilterItem compositeFilterItem;
        private CompositeFilterItem CompositeFilter {
            get {
                return this.compositeFilterItem ?? (this.compositeFilterItem = new CompositeFilterItem());
            }
        }

        public PreOrderForReportQueryWindow() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData();
            this.InitControl();
        }

        private DataGridViewBase dataGridView;
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PreOrder"));
            }
        }

        //品牌编号
        private TextBox brandCode;
        public TextBox BrandCode {
            get {
                if(brandCode == null) {
                    this.brandCode = new TextBox {
                        Tag = "BrandCode",
                        Height = 22,
                        Width = 120,
                        Margin = new Thickness(2, 10, 0, 2),
                        IsReadOnly = true
                    };
                }
                return this.brandCode;
            }
        }

        private DcsComboBox StatusCombobox {
            get;
            set;
        }

        private void InitControl() {
            this.panelGrid = new Grid();
            this.panelGrid.RowDefinitions.Add(new RowDefinition {
                Height = new GridLength(0, GridUnitType.Auto)
            });
            this.panelGrid.RowDefinitions.Add(new RowDefinition {
                Height = new GridLength(0, GridUnitType.Auto)
            });
            this.panelGrid.ColumnDefinitions.Add(new ColumnDefinition {
                Width = new GridLength(0, GridUnitType.Auto)
            });
            this.panelGrid.ColumnDefinitions.Add(new ColumnDefinition {
                Width = new GridLength(0, GridUnitType.Auto)
            });
            this.panelGrid.ColumnDefinitions.Add(new ColumnDefinition {
                Width = new GridLength(0, GridUnitType.Auto)
            });
            this.panelGrid.ColumnDefinitions.Add(new ColumnDefinition {
                Width = new GridLength(0, GridUnitType.Auto)
            });
            this.panelGrid.ColumnDefinitions.Add(new ColumnDefinition {
                Width = new GridLength(0, GridUnitType.Auto)
            });
            this.panelGrid.ColumnDefinitions.Add(new ColumnDefinition {
                Width = new GridLength(0, GridUnitType.Auto)
            });
            this.panelGrid.ColumnDefinitions.Add(new ColumnDefinition {
                Width = new GridLength(0, GridUnitType.Auto)
            });
            this.panelGrid.ColumnDefinitions.Add(new ColumnDefinition {
                Width = new GridLength(0, GridUnitType.Auto)
            });

            var codeLable = new TextBlock {
                Text = "预订单编号",
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Right,
                Margin = new Thickness(5, 10, 4, 5)
            };
            Grid.SetRow(codeLable, 0);
            Grid.SetColumn(codeLable, 0);
            this.panelGrid.Children.Add(codeLable);
            var codeEdit = new TextBox {
                Tag = "Code",
                Height = 22,
                Width = 120,
                Margin = new Thickness(2, 10, 0, 2)
            };
            Grid.SetRow(codeEdit, 0);
            Grid.SetColumn(codeEdit, 1);
            this.panelGrid.Children.Add(codeEdit);

            var brandCodeLable = new TextBlock {
                Text = "品牌编号",
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Right,
                Margin = new Thickness(5, 10, 4, 5)
            };
            Grid.SetRow(brandCodeLable, 1);
            Grid.SetColumn(brandCodeLable, 0);
            this.panelGrid.Children.Add(brandCodeLable);
            Grid.SetRow(BrandCode, 1);
            Grid.SetColumn(BrandCode, 1);
            this.panelGrid.Children.Add(BrandCode);

            var dealerCodeLable = new TextBlock {
                Text = PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsStock_DealerCode,
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Right,
                Margin = new Thickness(5, 10, 4, 5)
            };
            Grid.SetRow(dealerCodeLable, 0);
            Grid.SetColumn(dealerCodeLable, 2);
            this.panelGrid.Children.Add(dealerCodeLable);
            var dealerCode = new TextBox {
                Tag = "DealerCode",
                Height = 22,
                Width = 120,
                Margin = new Thickness(2, 10, 0, 2),
                IsReadOnly = true,
                Text = BaseApp.Current.CurrentUserData.EnterpriseCode
            };
            Grid.SetRow(dealerCode, 0);
            Grid.SetColumn(dealerCode, 3);
            this.panelGrid.Children.Add(dealerCode);

            var partCode = new TextBlock {
                Text = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode,
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Right,
                Margin = new Thickness(5, 10, 4, 5)
            };
            Grid.SetRow(partCode, 1);
            Grid.SetColumn(partCode, 2);
            this.panelGrid.Children.Add(partCode);
            var partCodeEdit = new TextBox {
                Tag = "PartCode",
                Height = 22,
                Width = 120,
                Margin = new Thickness(2, 10, 0, 2)
            };
            Grid.SetRow(partCodeEdit, 1);
            Grid.SetColumn(partCodeEdit, 3);
            this.panelGrid.Children.Add(partCodeEdit);


            var statusComboboxLable = new TextBlock {
                Text = PartsSalesUIStrings.QueryPanel_Title_AgencyLogisticCompany_Status,
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Right,
                Margin = new Thickness(5, 10, 4, 5)
            };
            Grid.SetRow(statusComboboxLable, 0);
            Grid.SetColumn(statusComboboxLable, 4);
            this.panelGrid.Children.Add(statusComboboxLable);
            this.StatusCombobox = new DcsComboBox {
                Tag = "Status",
                Height = 22,
                Width = 120,
                Margin = new Thickness(2, 10, 0, 2),
                ItemsSource = this.KeyValueManager[this.kvNames[0]],
                SelectedValue = 5,
                CanKeyboardNavigationSelectItems = true,
                ClearSelectionButtonVisibility = Visibility.Visible,
                ClearSelectionButtonContent = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_Clear,
                IsEditable = false,
                IsMouseWheelEnabled = true,
                DisplayMemberPath = "Value",
                SelectedValuePath = "Key"
            };
            Grid.SetRow(StatusCombobox, 0);
            Grid.SetColumn(StatusCombobox, 5);
            this.panelGrid.Children.Add(StatusCombobox);

            var queryButton = new RadRibbonButton {
                Text = PartsSalesUIStrings.DataEditView_Text_Button_Search,
                Size = ButtonSize.Large,
                LargeImage = new BitmapImage(new Uri("/Sunlight.Silverlight.Shell;component/Images/query.png", UriKind.Relative)),
                Command = SearchCommand
            };
            var saveButton = new RadRibbonButton {
                Text = "确认",
                Size = ButtonSize.Large,
                LargeImage = new BitmapImage(new Uri("/Sunlight.Silverlight.Dcs;component/Images/confirm.png", UriKind.Relative)),
                Command = SaveCommand
            };
            var abandonButton = new RadRibbonButton {
                Text = "作废",
                Size = ButtonSize.Large,
                LargeImage = new BitmapImage(new Uri("/Sunlight.Silverlight.Dcs;component/Images/Abandon.png", UriKind.Relative)),
                Command = AbandonCommand
            };
            var queryGroup = new RadRibbonGroup {
                FontSize = 14,
                HorizontalAlignment = HorizontalAlignment.Left,
                Header = "预订单查询"
            };
            queryGroup.Items.Add(this.panelGrid);
            queryGroup.Items.Add(new Rectangle {
                Width = 2,
                Margin = new Thickness(2),
                HorizontalAlignment = HorizontalAlignment.Center,
                Fill = new LinearGradientBrush {
                    StartPoint = new Point(0.5, 0),
                    EndPoint = new Point(0.5, 1),
                    GradientStops = new GradientStopCollection {
                        new GradientStop {
                            Offset = 0,
                            Color = Color.FromArgb(0x10, 0x0, 0x0, 0x0)
                        },
                        new GradientStop {
                            Offset = 0.25,
                            Color = Color.FromArgb(0x40, 0x0, 0x0, 0x0)
                        },
                        new GradientStop {
                            Offset = 0.75,
                            Color = Color.FromArgb(0x40, 0x0, 0x0, 0x0)
                        },
                        new GradientStop {
                            Offset = 1,
                            Color = Color.FromArgb(0x10, 0x0, 0x0, 0x0)
                        },
                    },
                }
            });
            queryGroup.Items.Add(queryButton);
            queryGroup.Items.Add(saveButton);
            queryGroup.Items.Add(abandonButton);
            this.DataGridView.SetValue(Grid.RowProperty, 1);
            this.LayoutRoot.Children.Add(queryGroup);
            this.LayoutRoot.Children.Add(this.DataGridView);
        }

        // 执行查询事件
        private ICommand SearchCommand {
            get {
                if(this.searchCommand == null)
                    searchCommand = new Core.Command.DelegateCommand(this.QueryButtonExecute);
                return searchCommand;
            }
        }

        private void QueryButtonExecute() {
            var filterItems = this.GetFilterValues(this.panelGrid);
            var partsSalesCategoryCode = Convert.ToString(filterItems["BrandCode"]);
            var partCode = Convert.ToString(filterItems["PartCode"]);
            var status = filterItems["Status"] as int?;
            var code = Convert.ToString(filterItems["Code"]);
            CompositeFilter.Filters.Clear();
            CompositeFilter.LogicalOperator = LogicalOperator.And;
            if(filterItems.ContainsKey("Status"))
                CompositeFilter.Filters.Add(new FilterItem {
                    MemberName = "Status",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                    Value = status
                });
            if(filterItems.ContainsKey("BrandCode"))
                CompositeFilter.Filters.Add(new FilterItem {
                    MemberName = "PartsSalesCategoryCode",
                    MemberType = typeof(string),
                    Operator = FilterOperator.Contains,
                    Value = string.IsNullOrEmpty(partsSalesCategoryCode) ? null : partsSalesCategoryCode
                });
            if(filterItems.ContainsKey("Code"))
                CompositeFilter.Filters.Add(new FilterItem {
                    MemberName = "Code",
                    MemberType = typeof(string),
                    Operator = FilterOperator.Contains,
                    Value = string.IsNullOrEmpty(code) ? null : code
                });
            if(filterItems.ContainsKey("PartCode"))
                CompositeFilter.Filters.Add(new FilterItem {
                    MemberName = "PartCode",
                    MemberType = typeof(string),
                    Operator = FilterOperator.Contains,
                    Value = string.IsNullOrEmpty(partCode) ? null : partCode
                });
            CompositeFilter.Filters.Add(new FilterItem {
                MemberName = "DealerCode",
                MemberType = typeof(string),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseCode
            });
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        private IDictionary<string, object> GetFilterValues(Grid filterGrid) {
            var result = new Dictionary<string, object>();
            foreach(var control in filterGrid.Children.ToArray()) {
                var itemName = ((FrameworkElement)control).Tag as string;
                if(itemName == null)
                    continue;
                object value = null;
                if(control is TextBox)
                    value = ((TextBox)control).Text;
                else if(control is DcsComboBox)
                    value = this.StatusCombobox.SelectedValue;
                result.Add(itemName, value);
            }
            return result;
        }

        //确认
        private ICommand SaveCommand {
            get {
                if(this.saveCommand == null)
                    saveCommand = new Core.Command.DelegateCommand(this.SelectedEntitiesDecided);
                return saveCommand;
            }
        }
        public event EventHandler SelectionChanged;
        public event EventHandler SelectionDecided;
        protected Entity SelectedEntityByDoubleClick;

        protected virtual IEnumerable<Entity> SelectedEntitiesProcessing(IEnumerable<Entity> entities) {
            return entities;
        }

        protected IEnumerable<Entity> GetSelectedEntitiesProcessing(string dataGridViewKey) {
            if(this.SelectedEntityByDoubleClick != null)
                return new[] {
                    this.SelectedEntityByDoubleClick
                };
            if(this.DataGridView != null)
                return this.DataGridView.SelectedEntities;
            return Enumerable.Empty<Entity>();
        }

        public IEnumerable<Entity> SelectedEntities {
            get {
                if(this.SelectedEntityByDoubleClick != null) {
                    return SelectedEntitiesProcessing(new[] {
                        this.SelectedEntityByDoubleClick
                    });
                }
                if(this.DataGridView != null)
                    return SelectedEntitiesProcessing(this.DataGridView.SelectedEntities);
                return Enumerable.Empty<Entity>();
            }
        }

        protected virtual void RaiseSelectionChanged() {
            EventHandler selectionChanged = this.SelectionChanged;
            if(selectionChanged != null) {
                selectionChanged.Invoke(this, EventArgs.Empty);
            }
        }

        protected virtual void RaiseSelectionDecided() {
            EventHandler selectionDecided = this.SelectionDecided;
            if(selectionDecided != null) {
                selectionDecided.Invoke(this, EventArgs.Empty);
            }
        }

        private void SelectedEntitiesDecided() {
            this.RaiseSelectionChanged();
            this.RaiseSelectionDecided();
        }


        // 作废
        private ICommand AbandonCommand {
            get {
                if(this.abandonCommand == null)
                    abandonCommand = new Core.Command.DelegateCommand(this.AbandonButtonExecute);
                return abandonCommand;
            }
        }
        private void AbandonButtonExecute() {
            if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                var orderIds = DataGridView.SelectedEntities.Cast<PreOrder>().Select(r => r.Id).ToArray();
                if(orderIds == null)
                    return;
                var domainContext = new DcsDomainContext();
                domainContext.作废预订单(orderIds, invokeOp => {
                    if(invokeOp.HasError) {
                        if(!invokeOp.IsErrorHandled)
                            invokeOp.MarkErrorAsHandled();
                        var error = invokeOp.ValidationErrors.FirstOrDefault();
                        if(error != null) {
                            UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage, 5);
                        } else {
                            DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                        }
                        domainContext.RejectChanges();
                        return;
                    }
                    DataGridView.ExecuteQueryDelayed();
                }, null);
            } else {
                UIHelper.ShowNotification("请选择要作废的预订单");
            }
        }

    }
}
