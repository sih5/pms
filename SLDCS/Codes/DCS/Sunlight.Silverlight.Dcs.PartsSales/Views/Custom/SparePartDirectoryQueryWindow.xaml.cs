﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Sunlight.Silverlight.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Controls;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.RibbonView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.Custom {
    public partial class SparePartDirectoryQueryWindow : IBaseView {
        private Grid PanelGrid;
        private FilterItem additionalFilterItem;
        private ICommand searchCommand;

        public SparePartDirectoryQueryWindow() {
            InitializeComponent();
            this.InitControl();
        }

        private void InitControl() {
            PanelGrid = new Grid();
            PanelGrid.RowDefinitions.Add(new RowDefinition {
                Height = new GridLength(0, GridUnitType.Auto)
            });
            PanelGrid.RowDefinitions.Add(new RowDefinition {
                Height = new GridLength(0, GridUnitType.Auto)
            });
            PanelGrid.ColumnDefinitions.Add(new ColumnDefinition {
                Width = new GridLength(0, GridUnitType.Auto)
            });
            PanelGrid.ColumnDefinitions.Add(new ColumnDefinition {
                Width = new GridLength(0, GridUnitType.Auto)
            });

            var PMSBrandIdLable = new TextBlock {
                Text = "PMS品牌",
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Right,
                Margin = new Thickness(5, 10, 4, 5)
            };
            Grid.SetRow(PMSBrandIdLable, 0);
            Grid.SetColumn(PMSBrandIdLable, 0);
            PanelGrid.Children.Add(PMSBrandIdLable);
            Grid.SetRow(PMSBrandEdit, 0);
            Grid.SetColumn(PMSBrandEdit, 1);
            PanelGrid.Children.Add(PMSBrandEdit);

            var EPCBrandIdLable = new TextBlock {
                Text = "EPC品牌",
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Right,
                Margin = new Thickness(5, 10, 4, 5)
            };
            Grid.SetRow(EPCBrandIdLable, 1);
            Grid.SetColumn(EPCBrandIdLable, 0);
            PanelGrid.Children.Add(EPCBrandIdLable);
            this.EPCBrandCombobox = new DcsComboBox {
                Tag = "EPCBrandId",
                Height = 22,
                Width = 120,
                Margin = new Thickness(2, 10, 0, 2),
                ItemsSource = this.kvEPCBrand,
                CanKeyboardNavigationSelectItems = true,
                ClearSelectionButtonVisibility = Visibility.Visible,
                ClearSelectionButtonContent = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_Clear,
                IsEditable = false,
                IsMouseWheelEnabled = true,
                DisplayMemberPath = "Value",
                SelectedValuePath = "Key"
            };
            Grid.SetRow(EPCBrandCombobox, 1);
            Grid.SetColumn(EPCBrandCombobox, 1);
            PanelGrid.Children.Add(EPCBrandCombobox);

            var queryButton = new RadRibbonButton {
                Text = PartsSalesUIStrings.DataEditView_Text_Button_Search,
                Size = ButtonSize.Large,
                LargeImage = new BitmapImage(new Uri("/Sunlight.Silverlight.Shell;component/Images/query.png", UriKind.Relative)),
                Command = SearchCommand
            };
            var queryGroup = new RadRibbonGroup {
                FontSize = 14,
                HorizontalAlignment = HorizontalAlignment.Left,
                Header = "选择电子配件目录"
            };
            queryGroup.Items.Add(PanelGrid);
            queryGroup.Items.Add(new Rectangle {
                Width = 2,
                Margin = new Thickness(2),
                HorizontalAlignment = HorizontalAlignment.Center,
                Fill = new LinearGradientBrush {
                    StartPoint = new Point(0.5, 0),
                    EndPoint = new Point(0.5, 1),
                    GradientStops = new GradientStopCollection {
                        new GradientStop {
                            Offset = 0,
                            Color = Color.FromArgb(0x10, 0x0, 0x0, 0x0)
                        },
                        new GradientStop {
                            Offset = 0.25,
                            Color = Color.FromArgb(0x40, 0x0, 0x0, 0x0)
                        },
                        new GradientStop {
                            Offset = 0.75,
                            Color = Color.FromArgb(0x40, 0x0, 0x0, 0x0)
                        },
                        new GradientStop {
                            Offset = 1,
                            Color = Color.FromArgb(0x10, 0x0, 0x0, 0x0)
                        },
                    },
                }
            });
            queryGroup.Items.Add(queryButton);
            this.LayoutRoot.Children.Add(queryGroup);
        }

        private ICommand SearchCommand {
            get {
                if(this.searchCommand == null)
                    searchCommand = new Core.Command.DelegateCommand(this.QueryButtonExecute);
                return searchCommand;
            }
        }

        string addUrlParameter;
        private int pmsBrandId;
        // 执行查询事件
        private void QueryButtonExecute() {
            addUrlParameter = "";
            var filterItems = this.GetFilterValues(PanelGrid);

            var userCode = BaseApp.Current.CurrentUserData.UserCode;
            addUrlParameter += "user=" + Uri.EscapeUriString(userCode);

            var epcBrand = filterItems["EPCBrandId"] as int?;
            if(epcBrand != null && !string.IsNullOrWhiteSpace(epcBrand.Value.ToString())) {
                addUrlParameter += "&brandSelect=" + Uri.EscapeUriString(epcBrand.Value.ToString());
            } else {
                UIHelper.ShowNotification("PMS品牌和EPC品牌查询条件不能为空");
                return;
            }
            var dealerCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
            addUrlParameter += "&DEALER_CODE=" + Uri.EscapeUriString(dealerCode);

            //var pmsBrand = filterItems["PMSBrandId"] as int?;
            //if(pmsBrand == null) {
            //    UIHelper.ShowNotification("PMS品牌和EPC品牌查询条件不能为空");
            //    return;
            //}
            var dcsDomainContext = new DcsDomainContext();
            //dcsDomainContext.Load(dcsDomainContext.GetPMSEPCBRANDMAPPINGsQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效 && v.PMSBrandId == pmsBrandId), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    var entity = loadOp.Entities.FirstOrDefault();
            //    if(entity == null) {
            //        //  UIHelper.ShowNotification("与EPC品牌映射关系不存在，请联系系统管理员。");
            //        return;
            //    }
            //    string paramBrand = entity.PMSBrandCode.ToString(CultureInfo.InvariantCulture);
            //    addUrlParameter += "&PMSBRAND=" + Uri.EscapeUriString(paramBrand);
            //    ShellViewModel.Current.IsBusy = false;
            //    var url = "http://epc.foton.com.cn/PMSServlet?" + addUrlParameter;
            //    HtmlPage.Window.Navigate(new Uri(url, UriKind.Absolute), "_blank");
            //}, null);
        }

        private IDictionary<string, object> GetFilterValues(Grid filterGrid) {
            var result = new Dictionary<string, object>();
            foreach(var control in filterGrid.Children.ToArray()) {
                var itemName = ((FrameworkElement)control).Tag as string;
                if(itemName == null)
                    continue;
                object value = null;
                if(control is DateTimeRangeBox)
                    value = new[] {
                        ((DateTimeRangeBox)control).SelectedValueFrom, ((DateTimeRangeBox)control).SelectedValueTo
                    };
                else if(control is TextBox)
                    value = ((TextBox)control).Text;
                else if(control is DcsComboBox)
                    value = this.EPCBrandCombobox.SelectedValue;
                result.Add(itemName, value);
            }
            return result;
        }

        public object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            var aa = contents[0] as CompositeFilterItem;
            var pmsBrandIds = aa.Filters.First(r => r.MemberName == "PMSBrandId").Value as int?;
            pmsBrandId = (int)pmsBrandIds;
            this.kvEPCBrand.Clear();
            var domainContext = new DcsDomainContext();
            //domainContext.Load(domainContext.GetPMSEPCBRANDMAPPINGsQuery().Where(r => r.PMSBrandId == pmsBrandId && r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    foreach(var entity in loadOp.Entities) {
            //        this.kvEPCBrand.Add(new KeyValuePair {
            //            Key = Convert.ToInt32(entity.EPCBrandId),
            //            Value = entity.EPCBrandName
            //        });
            //    }

            //}, null);
            switch(subject) {
                case "SetAdditionalFilterItem": {
                        if(contents.Length == 1) {
                            if(this.additionalFilterItem == null && contents[0] == null)
                                return false;
                            if(this.additionalFilterItem != null && this.additionalFilterItem.Equals(contents[0] as CompositeFilterItem))
                                return false;
                            this.additionalFilterItem = contents[0] as CompositeFilterItem;
                            return true;
                        }
                        throw new ArgumentException(String.Format(DcsUIStrings.QueryWindow_ExchangeData_ExceptionMessage_Contents, "SetAdditionalFilterItem", 1), "contents");
                    }
                default:
                    throw new ArgumentException(DcsUIStrings.QueryWindow_ExchangeData_Exception_Subject, "subject");
            }
        }

        private ObservableCollection<KeyValuePair> kvEPCBrand = new ObservableCollection<KeyValuePair>();
        private DcsComboBox EPCBrandCombobox {
            get;
            set;
        }

        private TextBox pMSBrand;

        public TextBox PMSBrandEdit {
            get {
                if(pMSBrand == null) {
                    this.pMSBrand = new TextBox {
                        Tag = "PMSBrand",
                        Height = 22,
                        Width = 120,
                        Margin = new Thickness(2, 10, 0, 2),
                        IsReadOnly = true
                    };
                }
                return this.pMSBrand;
            }
        }
    }
}
