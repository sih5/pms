﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("Common", "Dealer", "CustomerDirectSpareList", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_ABANDON_IMPORT_EXPORT
    })]
    public class CustomerDirectSpareListManagement : DcsDataManagementViewBase {
        private readonly DcsDomainContext domaincontext = new DcsDomainContext();
        private DataGridViewBase customerDirectSpareListDataGridView;
        private CustomerDirectSpareListDataEditView customerDirectSpareListDataEditView;
        private DataEditViewBase dataEditViewImport;
        private const string DATA_EDIT_VIEW_IMPORT = "_DataEditViewImport_";

        private DataGridViewBase CustomerDirectSpareListDataGridView {
            get {
                return this.customerDirectSpareListDataGridView ?? (this.customerDirectSpareListDataGridView = DI.GetDataGridView("CustomerDirectSpareList"));
            }
        }

        private CustomerDirectSpareListDataEditView CustomerDirectSpareListDataEditView {
            get {
                if(this.customerDirectSpareListDataEditView == null) {
                    this.customerDirectSpareListDataEditView = DI.GetDataEditView("CustomerDirectSpareList") as CustomerDirectSpareListDataEditView; ;
                    this.customerDirectSpareListDataEditView.EditCancelled += this.SalesUnitDataEditView_EditCancelled;
                    this.customerDirectSpareListDataEditView.CustomEditSubmitted += this.SalesUnitDataEditView_EditSubmitted;
                }
                return this.customerDirectSpareListDataEditView;
            }
        }

        private DataEditViewBase DataEditViewImport
        {
            get
            {
                if (this.dataEditViewImport == null)
                {
                    this.dataEditViewImport = DI.GetDataEditView("CustomerDirectSpareListForImport");
                    this.dataEditViewImport.EditCancelled += this.SalesUnitDataEditView_EditCancelled;
                }
                return this.dataEditViewImport;
            }
        }
        private void ResetEditView() {
            this.dataEditViewImport = null;
        }
        private void SalesUnitDataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.CustomerDirectSpareListDataGridView.FilterItem != null)
                this.CustomerDirectSpareListDataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void SalesUnitDataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        public CustomerDirectSpareListManagement() {
            this.Title = PartsSalesUIStrings.DataManagementView_Title_CustomerDirectSpareList;
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.CustomerDirectSpareListDataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.CustomerDirectSpareListDataEditView);
            this.RegisterView(DATA_EDIT_VIEW_IMPORT,()=>this.DataEditViewImport);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.CustomerDirectSpareListDataGridView.FilterItem = filterItem;
            this.CustomerDirectSpareListDataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var customerDirectSpareList = this.CustomerDirectSpareListDataEditView.CreateObjectToEdit<CustomerDirectSpareList>();
                    customerDirectSpareList.Status = (int)DcsBaseDataStatus.有效;
                    customerDirectSpareList.IfDirectProvision = 1;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.CustomerDirectSpareListDataGridView.SelectedEntities.Cast<VirtualCustomerDirectSpareList>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if (entity.Can作废客户直供配件清单维护2)
                                entity.作废客户直供配件清单维护2();
                            this.ExecuteSerivcesMethod(PartsSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORT);
                    break;
                case CommonActionKeys.EXPORT:
                    if(this.CustomerDirectSpareListDataGridView.SelectedEntities != null && this.CustomerDirectSpareListDataGridView.SelectedEntities.Any()) {
                        ShellViewModel.Current.IsBusy = true;
                        var ids = this.CustomerDirectSpareListDataGridView.SelectedEntities.Cast<VirtualCustomerDirectSpareList>().Select(r => r.Id).ToArray();
                        this.ExportCustomerDirectSpareList(ids, null, null, null, null, null, null,null);
                    } else {
                        var filterItem = this.CustomerDirectSpareListDataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        var customerCode = filterItem.Filters.Single(r => r.MemberName == "CustomerCode").Value as string;
                        var customerName = filterItem.Filters.Single(r => r.MemberName == "CustomerName").Value as string;
                        var sparePartCode = filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                        var sparePartName = filterItem.Filters.Single(r => r.MemberName == "SparePartName").Value as string;
                        var isPrimary = filterItem.Filters.Single(r => r.MemberName == "IsPrimary").Value as bool?;
                        ShellViewModel.Current.IsBusy = true;
                        this.ExportCustomerDirectSpareList(null, partsSalesCategoryId, customerCode, customerName, sparePartCode, sparePartName, status, isPrimary);
                    }
                    break;
            }
        }

        private void ExportCustomerDirectSpareList(int[] ids, int? salesCategoryId, string customerCode, string customerName, string sparePartCode, string sparePartName, int? status, bool? isPrimary)
        {
            this.domaincontext.ExportCustomerDirectSpareList(ids, salesCategoryId, customerCode, customerName, sparePartCode, sparePartName, status,isPrimary, loadOp => {
                if(loadOp.HasError) {
                    ShellViewModel.Current.IsBusy = false;
                    return;
                }
                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                    UIHelper.ShowNotification(loadOp.Value);
                    ShellViewModel.Current.IsBusy = false;
                }
                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                    ShellViewModel.Current.IsBusy = false;
                }
            }, null);
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.CustomerDirectSpareListDataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CustomerDirectSpareListDataGridView.ExecuteQueryDelayed();
                this.CheckActionsCanExecute();
            }, null);
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                case CommonActionKeys.IMPORT:
                    return true;
                case CommonActionKeys.ABANDON:
                    if(this.CustomerDirectSpareListDataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.CustomerDirectSpareListDataGridView.SelectedEntities.Cast<VirtualCustomerDirectSpareList>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsMasterDataStatus.有效;
                case CommonActionKeys.EXPORT:
                    return this.CustomerDirectSpareListDataGridView.Entities != null && this.CustomerDirectSpareListDataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "CustomerDirectSpareList"
                };
            }
        }
    }
}
