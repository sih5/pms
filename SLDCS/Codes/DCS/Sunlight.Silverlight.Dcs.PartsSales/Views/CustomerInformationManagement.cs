﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("Common", "Dealer", "CustomerInformation", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_PAUSE_RESUME_EXPORT_IMPORT
    })]
    public class CustomerInformationManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditViewImport;

        private const string DATA_EDIT_VIEW_IMPORT = "_DataEditViewImport_";

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("CustomerInformation"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("CustomerInformation");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataEditViewImport {
            get {
                if(this.dataEditViewImport == null) {
                    this.dataEditViewImport = DI.GetDataEditView("CustomerInformationForImport");
                    this.dataEditViewImport.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewImport;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_IMPORT, () => this.DataEditViewImport);
        }

        public CustomerInformationManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.DataManagementView_Title_CustomerInformation;
        }
        private void ResetEditView()
        {
            this.dataEditView = null;
            this.dataEditViewImport = null;
        }
        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "CustomerInformation"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var newCompositeFilterItem = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                var compositeFilterItem = filterItem as CompositeFilterItem;
                foreach(var item in compositeFilterItem.Filters)
                    newCompositeFilterItem.Filters.Add(item);
            } else
                newCompositeFilterItem.Filters.Add(filterItem);
            newCompositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "SalesCompanyId",
                Operator = FilterOperator.IsEqualTo,
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            ClientVar.ConvertTime(newCompositeFilterItem);
            this.DataGridView.FilterItem = newCompositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var customerInformation = this.DataEditView.CreateObjectToEdit<CustomerInformation>();
                    customerInformation.SalesCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    customerInformation.Status = (int)DcsMasterDataStatus.有效;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<CustomerInformation>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废客户信息)
                                entity.作废客户信息();
                            this.ExecuteSerivcesMethod(PartsSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.PAUSE:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Pause, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<CustomerInformation>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can停用客户信息)
                                entity.停用客户信息();
                            this.ExecuteSerivcesMethod(PartsSalesUIStrings.DataManagementView_Notification_PauseSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.RESUME:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Resume, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<CustomerInformation>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can恢复客户信息)
                                entity.恢复客户信息();
                            this.ExecuteSerivcesMethod(PartsSalesUIStrings.DataManagementView_Notification_ResumeSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORT);
                    break;
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<CustomerInformation>().Select(r => r.Id).ToArray();
                        this.excelServiceClient.ExportCustomerInformationAsync(ids, null, null, null);
                        this.excelServiceClient.ExportCustomerInformationCompleted -= excelServiceClient_ExportCustomerInformationCompleted;
                        this.excelServiceClient.ExportCustomerInformationCompleted += excelServiceClient_ExportCustomerInformationCompleted;

                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var customerCompanyName = filterItem.Filters.Single(r => r.MemberName == "CustomerCompany.Name").Value as string;
                        var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        this.excelServiceClient.ExportCustomerInformationAsync(new int[] { }, customerCompanyName, createTimeBegin, createTimeEnd);
                        this.excelServiceClient.ExportCustomerInformationCompleted -= excelServiceClient_ExportCustomerInformationCompleted;
                        this.excelServiceClient.ExportCustomerInformationCompleted += excelServiceClient_ExportCustomerInformationCompleted;
                    }
                    break;
            }
        }

        private void excelServiceClient_ExportCustomerInformationCompleted(object sender, ExportCustomerInformationCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                case CommonActionKeys.IMPORT:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.PAUSE:
                case CommonActionKeys.RESUME:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<CustomerInformation>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    if(uniqueId == CommonActionKeys.RESUME)
                        return entities[0].Status == (int)DcsMasterDataStatus.停用;
                    return entities[0].Status == (int)DcsMasterDataStatus.有效;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
    }
}
