﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {

    [PageMeta("Common", "Dealer", "CustomerOrderPriceGrade", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_EXPORT_IMPORT
    })]
    public class CustomerOrderPriceGradeManagement : DcsDataManagementViewBase {
        private const string DATA_ADD_VIEW = "_DataAddView_";
        private const string DATA_EDIT_VIEW_IMPORT = "_DataEditViewImport_";
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataAddView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditViewImport;

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("CustomerOrderPriceWithDetails"));
            }
        }

        private DataEditViewBase DataAddView {
            get {
                if(this.dataAddView == null) {
                    this.dataAddView = DI.GetDataEditView("CustomerOrderPriceGradeForAdd");
                    this.dataAddView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataAddView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataAddView;
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("CustomerOrderPriceGradeForEdit");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private DataEditViewBase DataEditViewImport {
            get {
                if(this.dataEditViewImport == null) {
                    this.dataEditViewImport = DI.GetDataEditView("CustomerOrderPriceGradeForImport");
                    this.dataEditViewImport.EditCancelled += this.DataEditView_EditCancelled;
                    this.dataEditViewImport.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataEditViewImport;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataAddView = null;
            this.dataEditViewImport = null;
        }
        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "CustomerOrderPriceGrade"
                };
            }
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    this.SwitchViewTo(DATA_ADD_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<CustomerOrderPriceGrade>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废客户企业订单价格等级)
                                entity.作废客户企业订单价格等级();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<CustomerOrderPriceGrade>().Select(r => r.Id).ToArray();
                        ShellViewModel.Current.IsBusy = true;
                        this.dcsDomainContext.ExportCustomerOrderPriceGrades(ids, null, null, null, null, null, null, null, null, loadOp => {
                            if(loadOp.HasError) {
                                ShellViewModel.Current.IsBusy = false;
                                return;
                            }
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                                ShellViewModel.Current.IsBusy = false;
                            }

                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                ShellViewModel.Current.IsBusy = false;
                            }
                        }, null);
                    } else {
                        //  ((DcsDataGridViewBase)this.DataGridView).ExportData();
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var centerCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "CustomerCompanyCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "CustomerCompanyCode").Value as string;
                        var centerName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "CustomerCompanyName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "CustomerCompanyName").Value as string;
                        var partsSalesOrderTypeCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSalesOrderTypeCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "PartsSalesOrderTypeCode").Value as string;
                        var partsSalesOrderTypeName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSalesOrderTypeName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "PartsSalesOrderTypeName").Value as string;
                        var status = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Status") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        var yy = createTime.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? bCreateTime = null;
                        DateTime? eCreateTime = null;
                        if(yy != null) {
                            bCreateTime = yy.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            eCreateTime = yy.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }                                       
                        ShellViewModel.Current.IsBusy = true;
                        this.dcsDomainContext.ExportCustomerOrderPriceGrades(null, centerCode, centerName, partsSalesOrderTypeCode, partsSalesOrderTypeName, status, null, bCreateTime, eCreateTime, loadOp => {
                            if(loadOp.HasError) {
                                ShellViewModel.Current.IsBusy = false;
                                return;
                            }
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                                ShellViewModel.Current.IsBusy = false;
                            }

                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                ShellViewModel.Current.IsBusy = false;
                            }
                        }, null);
                    }
                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORT);
                    break;

            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "BranchId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            if(filterItem is CompositeFilterItem) {
                var compositeFilter = filterItem as CompositeFilterItem;
                var createTimeFilters = compositeFilter.Filters.Where(filter => filter is CompositeFilterItem && (filter as CompositeFilterItem).Filters.All(item => item.MemberName == "CreateTime")).SingleOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                if(createTimeFilters != null) {
                    var dateTime = createTimeFilters.Filters.ElementAt(1).Value as DateTime?;
                    if(dateTime.HasValue)
                        createTimeFilters.Filters.ElementAt(1).Value = new DateTime(dateTime.Value.Year, dateTime.Value.Month, dateTime.Value.Day, 23, 59, 59);
                }
                compositeFilterItem.Filters.Add(compositeFilter);
            } else
                compositeFilterItem.Filters.Add(filterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<CustomerOrderPriceGrade>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsBaseDataStatus.有效;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.IMPORT:
                    return true;
                default:
                    return false;
            }
        }

        public CustomerOrderPriceGradeManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.DataManagementView_Title_CustomerOrderPriceGrade;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_ADD_VIEW, () => this.DataAddView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_IMPORT, () => this.DataEditViewImport);
        }
    }
}
