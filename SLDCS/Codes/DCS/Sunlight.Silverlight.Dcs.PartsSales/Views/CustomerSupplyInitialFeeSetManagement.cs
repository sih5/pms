﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("Common", "Dealer", "CustomerSupplyInitialFeeSet", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_ABANDON_IMPORT_EXPORT
    })]
    public class CustomerSupplyInitialFeeSetManagement : DcsDataManagementViewBase {
        private readonly DcsDomainContext domaincontext = new DcsDomainContext();
        private DataGridViewBase customerSupplyInitialFeeSetDataGridView;
        private CustomerSupplyInitialFeeSetDataEditView customerSupplyInitialFeeSetDataEditView;
        private DataEditViewBase dataEditViewImport;
        private const string DATA_EDIT_VIEW_IMPORT = "_DataEditViewImport_";
        private DataGridViewBase CustomerSupplyInitialFeeSetDataGridView {
            get {
                return this.customerSupplyInitialFeeSetDataGridView ?? (this.customerSupplyInitialFeeSetDataGridView = DI.GetDataGridView("CustomerSupplyInitialFeeSet"));
            }
        }

        private CustomerSupplyInitialFeeSetDataEditView CustomerSupplyInitialFeeSetDataEditView {
            get {
                if(this.customerSupplyInitialFeeSetDataEditView == null) {
                    this.customerSupplyInitialFeeSetDataEditView = DI.GetDataEditView("CustomerSupplyInitialFeeSet") as CustomerSupplyInitialFeeSetDataEditView; ;
                    this.customerSupplyInitialFeeSetDataEditView.EditCancelled += this.SalesUnitDataEditView_EditCancelled;
                    this.customerSupplyInitialFeeSetDataEditView.CustomEditSubmitted += this.SalesUnitDataEditView_EditSubmitted;
                }
                return this.customerSupplyInitialFeeSetDataEditView;
            }
        }

        private DataEditViewBase DataEditViewImport
        {
            get
            {
                if (this.dataEditViewImport == null)
                {
                    this.dataEditViewImport = DI.GetDataEditView("CustomerSupplyInitialFeeSetForImport");
                    this.dataEditViewImport.EditCancelled += this.SalesUnitDataEditView_EditCancelled;
                }
                return this.dataEditViewImport;
            }
        }

        private void ResetEditView() {
            this.dataEditViewImport = null;
        }
        private void SalesUnitDataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.CustomerSupplyInitialFeeSetDataGridView.FilterItem != null)
                this.CustomerSupplyInitialFeeSetDataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void SalesUnitDataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        public CustomerSupplyInitialFeeSetManagement() {
            this.Title = PartsSalesUIStrings.DataManagementView_Title_CustomerSupplyInitialFeeSet;
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.CustomerSupplyInitialFeeSetDataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.CustomerSupplyInitialFeeSetDataEditView);
            this.RegisterView(DATA_EDIT_VIEW_IMPORT, () => this.DataEditViewImport);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.CustomerSupplyInitialFeeSetDataGridView.FilterItem = filterItem;
            this.CustomerSupplyInitialFeeSetDataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var customerSupplyInitialFeeSet = this.CustomerSupplyInitialFeeSetDataEditView.CreateObjectToEdit<CustomerSupplyInitialFeeSet>();
                    customerSupplyInitialFeeSet.Status = (int)DcsBaseDataStatus.有效;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.CustomerSupplyInitialFeeSetDataGridView.SelectedEntities.Cast<CustomerSupplyInitialFeeSet>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废客户与供应商起订金额设置)
                                entity.作废客户与供应商起订金额设置();
                            this.ExecuteSerivcesMethod(PartsSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORT);
                    break;
                case CommonActionKeys.EXPORT:
                    if(this.CustomerSupplyInitialFeeSetDataGridView.SelectedEntities != null && this.CustomerSupplyInitialFeeSetDataGridView.SelectedEntities.Any()) {
                        ShellViewModel.Current.IsBusy = true;
                        var ids = this.CustomerSupplyInitialFeeSetDataGridView.SelectedEntities.Cast<CustomerSupplyInitialFeeSet>().Select(r => r.Id).ToArray();
                        this.ExportCustomerSupplyInitialFeeSet(ids, null, null, null, null, null, null);
                    } else {
                        var filterItem = this.CustomerSupplyInitialFeeSetDataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        var customerCode = filterItem.Filters.Single(r => r.MemberName == "CustomerCode").Value as string;
                        var customerName = filterItem.Filters.Single(r => r.MemberName == "CustomerName").Value as string;
                        var supplierCode = filterItem.Filters.Single(r => r.MemberName == "SupplierCode").Value as string;
                        var supplierName = filterItem.Filters.Single(r => r.MemberName == "SupplierName").Value as string;
                        ShellViewModel.Current.IsBusy = true;
                        this.ExportCustomerSupplyInitialFeeSet(null, partsSalesCategoryId, customerCode, customerName, supplierCode, supplierName, status);
                    }
                    break;
            }
        }

        private void ExportCustomerSupplyInitialFeeSet(int[] ids, int? salesCategoryId, string customerCode, string customerName, string supplierCode, string supplierName, int? status) {
            this.domaincontext.ExportCustomerSupplyInitialFeeSet(ids, salesCategoryId, customerCode, customerName, supplierCode, supplierName, status, loadOp => {
                if(loadOp.HasError) {
                    ShellViewModel.Current.IsBusy = false;
                    return;
                }
                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                    UIHelper.ShowNotification(loadOp.Value);
                    ShellViewModel.Current.IsBusy = false;
                }
                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                    ShellViewModel.Current.IsBusy = false;
                }
            }, null);
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.CustomerSupplyInitialFeeSetDataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CustomerSupplyInitialFeeSetDataGridView.ExecuteQueryDelayed();
                this.CheckActionsCanExecute();
            }, null);
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                case CommonActionKeys.IMPORT:
                    return true;
                case CommonActionKeys.ABANDON:
                    if(this.CustomerSupplyInitialFeeSetDataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.CustomerSupplyInitialFeeSetDataGridView.SelectedEntities.Cast<CustomerSupplyInitialFeeSet>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsMasterDataStatus.有效;
                case CommonActionKeys.EXPORT:
                    return this.CustomerSupplyInitialFeeSetDataGridView.Entities != null && this.CustomerSupplyInitialFeeSetDataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "CustomerSupplyInitialFeeSet"
                };
            }
        }
    }
}
