﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class ABCTypeSafeDaysDataEditView {
        private KeyValueManager keyValueManager;
        private ObservableCollection<KeyValuePair> kvProductTypes = new ObservableCollection<KeyValuePair>();
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        public ABCTypeSafeDaysDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.keyValueManager.LoadData(() => {
                this.KvProductTypes.Clear();
                foreach(var item in this.KeyValueManager[this.kvNames[0]]) {
                    this.KvProductTypes.Add(new KeyValuePair {
                        Key = item.Key,
                        Value = item.Value
                    });
                }
            });
        }
        private readonly string[] kvNames = {
            "ABCSetting_Type"
        };
        public ObservableCollection<KeyValuePair> KvProductTypes {
            get {
                return this.kvProductTypes ?? (this.kvProductTypes = new ObservableCollection<KeyValuePair>());
            }
        }
        protected override void OnEditCancelled() {
            base.OnEditCancelled();
            this.KvProductTypes.Clear();
        }

        protected override void OnEditSubmitted() {
            base.OnEditSubmitted();
            this.KvProductTypes.Clear();
        }
        protected override void OnEditSubmitting() {
            var aBCTypeSafeDay = this.DataContext as ABCTypeSafeDay;
            if(aBCTypeSafeDay == null)
                return;
            aBCTypeSafeDay.ValidationErrors.Clear();

            if(aBCTypeSafeDay.Days == null) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_ABCTypeSafeDay_DaysIsNull);
                return;
            }
            if(aBCTypeSafeDay.PRODUCTTYPE == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_partsStockCoefficient_ProductTypeIsNull);
                return;
            }

            if(aBCTypeSafeDay.HasValidationErrors )
                return;
            ((IEditableObject)aBCTypeSafeDay).EndEdit();
            base.OnEditSubmitting();
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetABCTypeSafeDaysQuery().Where(r => r.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity == null)
                    return;
                this.SetObjectToEdit(entity);
            }, null);
        }
        protected override string BusinessName {
            get {
                return PartsSalesUIStrings.Management_Title_ABCTypeSafeDays;
            }
        }

        public void OnCustomEditSubmitted() {
            this.KvProductTypes.Clear();
        }
        protected override bool OnRequestCanSubmit() {
            return true;
        }

    }
}
