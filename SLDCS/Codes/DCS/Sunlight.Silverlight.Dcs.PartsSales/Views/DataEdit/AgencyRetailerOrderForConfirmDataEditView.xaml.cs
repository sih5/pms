﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using ValidationResult = System.ComponentModel.DataAnnotations.ValidationResult;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class AgencyRetailerOrderForConfirmDataEditView : INotifyPropertyChanged {
        private KeyValueManager keyValueManager;
        private ObservableCollection<PartsOutboundBillDetail> partsOutboundBillDetails;

        private DataGridViewBase partsOutboundBillDetailForAgencyRetailOrderDataGridView;
        private DataGridViewBase virtualPartsStockForDeliverDataGridView;
        private string partsSalesCategoryName;
        protected virtual void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        public string PartsSalesCategoryName {
            get {
                return this.partsSalesCategoryName;
            }
            set {
                this.partsSalesCategoryName = value;
                this.OnPropertyChanged("PartsSalesCategoryName");
            }
        }

        private readonly string[] kvNames = {
            "Parts_OutboundType"
        };

        public AgencyRetailerOrderForConfirmDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);

        }

        public DataGridViewBase PartsOutboundPlanDetailForDeliverDataGridView {
            get {
                if(this.partsOutboundBillDetailForAgencyRetailOrderDataGridView == null) {
                    this.partsOutboundBillDetailForAgencyRetailOrderDataGridView = DI.GetDataGridView("PartsOutboundBillDetailForAgencyRetailOrder");
                    this.partsOutboundBillDetailForAgencyRetailOrderDataGridView.DomainContext = this.DomainContext;
                    //this.partsOutboundBillDetailForAgencyRetailOrderDataGridView.SelectionChanged += partsOutboundBillDetailForAgencyRetailOrderDataGridView_SelectionChanged;

                }
                return this.partsOutboundBillDetailForAgencyRetailOrderDataGridView;
            }
        }

        //private void partsOutboundBillDetailForAgencyRetailOrderDataGridView_SelectionChanged(object sender, EventArgs e) {
        //    throw new NotImplementedException();
        //}

        private DataGridViewBase VirtualPartsStockForDeliverDataGridView {
            get {
                if(this.virtualPartsStockForDeliverDataGridView == null) {
                    this.virtualPartsStockForDeliverDataGridView = DI.GetDataGridView("VirtualPartsStockForDeliver1");
                    this.virtualPartsStockForDeliverDataGridView.DomainContext = this.DomainContext;
                    this.virtualPartsStockForDeliverDataGridView.DataContext = this;
                }
                return this.virtualPartsStockForDeliverDataGridView;
            }
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private void CreateUI() {
            var agencyLogisticCompany = DI.GetQueryWindow("AgencyLogisticCompany");
            agencyLogisticCompany.SelectionDecided += agencyLogisticCompany_SelectionDecided;
            this.ptbLogisticCompanyName.PopupContent = agencyLogisticCompany;

            var tbPartsOutboundPlanDetails = new RadTabControl();
            tbPartsOutboundPlanDetails.BackgroundVisibility = Visibility.Collapsed;
            tbPartsOutboundPlanDetails.Items.Add(new RadTabItem {
                Content = this.PartsOutboundPlanDetailForDeliverDataGridView,
                Header = PartsSalesUIStrings.DataEdit_Header_AgencyRetailerOrder_PartsOutboundPlanDetail
            });
            tbPartsOutboundPlanDetails.SetValue(Grid.ColumnProperty, 1);
            this.gvPartsOutboundPlanDetail.Children.Add(tbPartsOutboundPlanDetails);
            var tbVirtualPartsStock = new DcsDetailDataEditView();
            tbVirtualPartsStock.Register(PartsSalesUIStrings.DataEdit_Header_AgencyRetailerOrder_VirtualPartsStock, null, this.VirtualPartsStockForDeliverDataGridView);
            tbVirtualPartsStock.UnregisterButton(tbVirtualPartsStock.DeleteButton);
            tbVirtualPartsStock.UnregisterButton(tbVirtualPartsStock.InsertButton);
            tbVirtualPartsStock.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.DataEditPanel_Action_Refres,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/merge.png", UriKind.Relative),
                Command = new Core.Command.DelegateCommand(this.Refresh)
            });
            tbVirtualPartsStock.SetValue(Grid.ColumnProperty, 1);
            this.gvVirtualPartsStock.Children.Add(tbVirtualPartsStock);
            this.KeyValueManager.LoadData();
        }

        private void agencyLogisticCompany_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var selectedCompany = queryWindow.SelectedEntities.Cast<AgencyLogisticCompany>().FirstOrDefault();
            if(selectedCompany == null)
                return;

            var agencyRetailOrder = this.DataContext as AgencyRetailerOrder;
            if(agencyRetailOrder == null)
                return;

            agencyRetailOrder.LogisticCompanyId = selectedCompany.Id;
            agencyRetailOrder.LogisticCompanyCode = selectedCompany.Code;
            agencyRetailOrder.LogisticCompanyName = selectedCompany.Name;

            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void Refresh() {
            var agencyRetailerOrder = this.DataContext as AgencyRetailerOrder;
            if(agencyRetailerOrder == null)
                return;
            if(this.PartsOutboundBillDetails.Any()) {
                foreach(var partsOutboundBillDetail in this.PartsOutboundBillDetails)
                    partsOutboundBillDetail.ValidationErrors.Clear();
            }


            this.DomainContext.Load(this.DomainContext.查询配件库位库存For代理库电商订单Query(agencyRetailerOrder.WarehouseId.HasValue ? agencyRetailerOrder.WarehouseId.Value : 0, BaseApp.Current.CurrentUserData.UserId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }

                var virtualPartsStocks = loadOp.Entities.ToArray();
                this.PartsOutboundBillDetails.Clear();

                if(virtualPartsStocks == null || !virtualPartsStocks.Any())
                    return;

                foreach(var agencyRetailerList in agencyRetailerOrder.AgencyRetailerLists) {
                    var price = agencyRetailerList.UnitPrice.HasValue ? agencyRetailerList.UnitPrice.Value : 0;
                    var data = virtualPartsStocks.Where(r => r.SparePartId == agencyRetailerList.SparePartId);
                    foreach(var virtualPartsStock in data) {
                        var partsOutboundBillDetail = new PartsOutboundBillDetail();
                        partsOutboundBillDetail.SparePartId = virtualPartsStock.SparePartId;
                        partsOutboundBillDetail.SparePartCode = virtualPartsStock.SparePartCode;
                        partsOutboundBillDetail.SparePartName = virtualPartsStock.SparePartName;
                        partsOutboundBillDetail.WarehouseAreaCode = virtualPartsStock.WarehouseAreaCode;
                        partsOutboundBillDetail.WarehouseAreaCategory = virtualPartsStock.WarehouseAreaCategory;
                        partsOutboundBillDetail.UsableQuantity = virtualPartsStock.UsableQuantity;
                        partsOutboundBillDetail.WarehouseAreaId = virtualPartsStock.WarehouseAreaId;
                        partsOutboundBillDetail.BatchNumber = virtualPartsStock.BatchNumber;
                        partsOutboundBillDetail.SettlementPrice = price;
                        partsOutboundBillDetail.CostPrice = price;
                        //剩余出库数量 (订货量-确认量-本次出库量)
                        var orderedQuantity = agencyRetailerList.OrderedQuantity.HasValue ? agencyRetailerList.OrderedQuantity.Value : 0;//订货量
                        var confirmedAmount = agencyRetailerList.ConfirmedAmount.HasValue ? agencyRetailerList.ConfirmedAmount.Value : 0;//已确认量
                        var currentOutQuantity = agencyRetailerList.CurrentOutQuantity;//本次出库量
                        var currentOutboundAmount = orderedQuantity - confirmedAmount - currentOutQuantity;
                        if(currentOutboundAmount == 0) {
                            partsOutboundBillDetail.OutboundAmount = 0;
                        } else {
                            partsOutboundBillDetail.OutboundAmount = currentOutboundAmount > partsOutboundBillDetail.UsableQuantity ? partsOutboundBillDetail.UsableQuantity : currentOutboundAmount;
                            agencyRetailerList.CurrentOutQuantity = agencyRetailerList.CurrentOutQuantity + partsOutboundBillDetail.OutboundAmount;
                        }
                        this.PartsOutboundBillDetails.Add(partsOutboundBillDetail);
                    }
                }

            }, null);
        }


        protected override bool OnRequestCanSubmit() {
            return true;
        }

        private void PartsOutboundPlanDetailForDeliverDataGridView_SelectionChanged(object sender, EventArgs e) {
            var dataGrid = sender as DcsDataGridViewBase;
            if(dataGrid == null || dataGrid.SelectedEntities == null)
                return;
            var partsOutboundPlanDetail = dataGrid.SelectedEntities.Cast<PartsOutboundPlanDetail>().FirstOrDefault();
            if(partsOutboundPlanDetail == null)
                return;
            var sparePartIds = new object[] {
                partsOutboundPlanDetail.SparePartId
            };
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetAgencyRetailerOrderWithDetailsQuery().Where(r => r.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity == null)
                    return;
                this.SetObjectToEdit(entity);
                this.AgencyRetailerOrder = entity;
                this.Refresh();
            }, null);
        }

        protected override string Title {
            get {
                return PartsSalesUIStrings.DataEdit_Title_AgencyRetailerOrder;
            }
        }

        protected override void OnEditSubmitting() {
            var agencyRetailerOrder = this.DataContext as AgencyRetailerOrder;
            if(agencyRetailerOrder == null)
                return;
            if(agencyRetailerOrder.LogisticCompanyId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_AgencyRetailerOrder_LogisticCompany);
                return;
            }

            if(agencyRetailerOrder.AgencyRetailerLists.Where(r => r.CurrentOutQuantity > 0).Count() <= 0) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_AgencyRetailerOrder_CurrentOutQuantity);
                return;
            }

            if(string.IsNullOrEmpty(agencyRetailerOrder.DeliveryBillNumber)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_AgencyRetailerOrder_DeliveryBillNumber);
                return;
            }
            this.DomainContext.确认代理库电商订单(agencyRetailerOrder, agencyRetailerOrder.AgencyRetailerLists.ToList(), this.PartsOutboundBillDetails, invokeOp => {
                if(invokeOp.HasError) {
                    if(!invokeOp.IsErrorHandled)
                        invokeOp.MarkErrorAsHandled();
                    var error = invokeOp.ValidationErrors.FirstOrDefault();
                    if(error != null)
                        UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage, 5);
                    else
                        DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                    return;
                }

                this.OnCustomEditSubmitted();
                base.OnEditSubmitting();
            }, null);
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        public ObservableCollection<PartsOutboundBillDetail> PartsOutboundBillDetails {
            get {
                return this.partsOutboundBillDetails ?? (this.partsOutboundBillDetails = new ObservableCollection<PartsOutboundBillDetail>());
            }
        }

        public AgencyRetailerOrder AgencyRetailerOrder {
            get;
            set;
        }

        public void OnCustomEditSubmitted() {
            var agencyRetailerOrder = this.DataContext as AgencyRetailerOrder;
            if(agencyRetailerOrder == null)
                return;
            if(this.DomainContext.AgencyRetailerOrders.Contains(agencyRetailerOrder))
                this.DomainContext.AgencyRetailerOrders.Detach(agencyRetailerOrder);
        }
        protected override void Reset() {

            var partsOutboundBill = this.DataContext as PartsOutboundBill;
            if(partsOutboundBill == null)
                return;
            if(this.DomainContext.PartsOutboundBills.Contains(partsOutboundBill))
                this.DomainContext.PartsOutboundBills.Detach(partsOutboundBill);

            this.PartsOutboundBillDetails.Clear();

        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
