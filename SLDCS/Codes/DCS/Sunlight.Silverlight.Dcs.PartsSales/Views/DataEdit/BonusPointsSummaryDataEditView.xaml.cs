﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class BonusPointsSummaryDataEditView : INotifyPropertyChanged {
        private DataGridViewBase bonusPointsSummaryListForEditDataGridView;
        private ICommand searchCommand;
        private bool searchCanUse;
        public BonusPointsSummaryDataEditView() {
            InitializeComponent();
            this.SearchCanUse = true;
            this.Initializer.Register(this.CreateUI);
            this.Loaded += BonusPointsSummaryDataEditView_Loaded;
            this.DataContextChanged += BonusPointsSummaryDataEditView_DataContextChanged;
        }

        public bool SearchCanUse {
            get {
                return this.searchCanUse;
            }
            set {
                this.searchCanUse = value;
                this.OnPropertyChanged("SearchCanUse");
            }
        }

        
        public ICommand SearchCommand {
            get {
                return this.searchCommand ?? (this.searchCommand = new DelegateCommand(this.Search));
            }
        }
        private void BonusPointsSummaryDataEditView_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var bonusPointsSummary = this.DataContext as BonusPointsSummary;
            if(bonusPointsSummary == null)
                return;
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(ex => ex.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && ex.Status == (int)DcsBaseDataStatus.有效 && ex.Name == PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_WarehouseCarNew), loadOp => {
                if(loadOp.HasError)
                    return;
                this.DcsComboBoxPartsSalesCategoryName.ItemsSource = loadOp.Entities;
                bonusPointsSummary.BrandName = loadOp.Entities.FirstOrDefault(r => r.Name == PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_WarehouseCarNew).Name;
                bonusPointsSummary.BrandId = loadOp.Entities.FirstOrDefault(r => r.Name == PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_WarehouseCarNew).Id;
            }, null);
            
        }
        private readonly DcsDetailDataEditView refDataEditView = new DcsDetailDataEditView();
        private void CreateUI() {
            refDataEditView.Register(Utils.GetEntityLocalizedName(typeof(BonusPointsSummary), "积分汇总清单"), null, this.BonusPointsSummaryListForEditDataGridView);
            refDataEditView.SetValue(Grid.RowProperty, 1);
            refDataEditView.SetValue(Grid.ColumnProperty, 0);
            refDataEditView.SetValue(Grid.ColumnSpanProperty, 3);
            this.gdBonusPointsSummaryWithRef.Children.Add(refDataEditView);
        }


        private DataGridViewBase BonusPointsSummaryListForEditDataGridView {
            get {
                if(this.bonusPointsSummaryListForEditDataGridView == null) {
                    this.bonusPointsSummaryListForEditDataGridView = DI.GetDataGridView("BonusPointsSummaryListForEdit");
                    this.bonusPointsSummaryListForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.bonusPointsSummaryListForEditDataGridView;
            }
        }


        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public event PropertyChangedEventHandler PropertyChanged;


        private void Search() {
            var bonusPointsSummary = this.DataContext as BonusPointsSummary;
            if(bonusPointsSummary == null)
                return;
            if(!bonusPointsSummary.BrandId.HasValue || string.IsNullOrEmpty(bonusPointsSummary.CutTime.ToString())) {
                UIHelper.ShowNotification("请先指定品牌和截止时间再查询");
                return;
            }

            if(bonusPointsSummary.BonusPointsSummaryLists.Count > 0) {
                foreach(var item in bonusPointsSummary.BonusPointsSummaryLists) {
                    bonusPointsSummary.BonusPointsSummaryLists.Remove(item);
                }
            }

            this.DomainContext.Load(this.DomainContext.GetBonusPointsOrdersByBrandIdAndCutTimeQuery(bonusPointsSummary.BrandId, Convert.ToDateTime(bonusPointsSummary.CutTime).AddDays(1).AddSeconds(-1)), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                foreach(var bonusPointsOrder in loadOp.Entities) {
                    var bonusPointsSummaryList = new BonusPointsSummaryList();
                    bonusPointsSummaryList.SerialNumber = bonusPointsSummary.BonusPointsSummaryLists.Any() ? bonusPointsSummary.BonusPointsSummaryLists.Max(entity => entity.SerialNumber) + 1 : 1;
                    bonusPointsSummaryList.BonusPointsOrderCode = bonusPointsOrder.BonusPointsOrderCode;
                    bonusPointsSummaryList.PlatForm_Code = bonusPointsOrder.PlatForm_Code;
                    bonusPointsSummaryList.ServiceApplyCode = bonusPointsOrder.ServiceApplyCode;
                    bonusPointsSummaryList.BonusPoints = bonusPointsOrder.BonusPoints;
                    bonusPointsSummaryList.BonusPointsAmount = bonusPointsOrder.BonusPointsAmount;
                    bonusPointsSummaryList.SouceType = bonusPointsOrder.Type;
                    bonusPointsSummaryList.BonusPointsTime = bonusPointsOrder.CreateTime;
                    bonusPointsSummaryList.BonusPointsId = bonusPointsOrder.Id;
                    bonusPointsSummary.BonusPointsSummaryLists.Add(bonusPointsSummaryList);
                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            var bonusPointsSummary = this.DataContext as BonusPointsSummary;
            if(bonusPointsSummary == null)
                return;
            if(!bonusPointsSummary.BonusPointsSummaryLists.Any()) {
                UIHelper.ShowNotification("积分订单不允许为空");
                return;
            }
            if(bonusPointsSummary.SummaryBonusPoints <= 0 || bonusPointsSummary.SummaryAmount <= 0) {
                UIHelper.ShowNotification("汇总金额和汇总积分必须大于零");
                return;
            }
            ((IEditableObject)bonusPointsSummary).EndEdit();
            try {

                if(bonusPointsSummary.Can新增积分汇总单)
                    bonusPointsSummary.新增积分汇总单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        private void bonusPointsSummary_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var bonusPointsSummary = this.DataContext as BonusPointsSummary;
            if(bonusPointsSummary == null)
                return;
            switch(e.PropertyName) {
                case "TaxRate":
                    if(bonusPointsSummary.TaxRate > 1 || bonusPointsSummary.TaxRate < 0) {
                        bonusPointsSummary.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesSettlement_TaxRateMustBetweenZeroAndOne, new[] {
                            "TaxRate"
                        }));
                        return;
                    }
                    bonusPointsSummary.Tax = Math.Round((Convert.ToDecimal(bonusPointsSummary.SummaryAmount) / (decimal)(1 + bonusPointsSummary.TaxRate)) * (decimal)bonusPointsSummary.TaxRate, 2);
                    break;
                case "SummaryAmount":
                    if(bonusPointsSummary.TaxRate <= 1 && bonusPointsSummary.TaxRate >= 0)
                        bonusPointsSummary.Tax = Math.Round((Convert.ToDecimal(bonusPointsSummary.SummaryAmount) / (decimal)(1 + bonusPointsSummary.TaxRate)) * (decimal)bonusPointsSummary.TaxRate, 2);
                    break;
            }
        }

        private void BonusPointsSummaryLists_EntityRemoved(object sender, EntityCollectionChangedEventArgs<BonusPointsSummaryList> e) {
            var bonusPointsSummary = this.DataContext as BonusPointsSummary;
            if(bonusPointsSummary == null)
                return;
            var sumnumber = bonusPointsSummary.BonusPointsSummaryLists.Where(r => r.SouceType == (int)DcsBonusPointsType.积分使用).Sum(r => r.BonusPointsAmount);
            var subnumber = bonusPointsSummary.BonusPointsSummaryLists.Where(r => r.SouceType == (int)DcsBonusPointsType.积分返还).Sum(r => r.BonusPointsAmount);
            bonusPointsSummary.SummaryAmount = sumnumber - subnumber;
            var sumpoint = bonusPointsSummary.BonusPointsSummaryLists.Where(r => r.SouceType == (int)DcsBonusPointsType.积分使用).Sum(r => r.BonusPoints);
            var subpoint = bonusPointsSummary.BonusPointsSummaryLists.Where(r => r.SouceType == (int)DcsBonusPointsType.积分返还).Sum(r => r.BonusPoints);
            bonusPointsSummary.SummaryBonusPoints = sumpoint - subpoint;
           
        }

        private void BonusPointsSummaryDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var bonusPointsSummary = this.DataContext as BonusPointsSummary;
            if(bonusPointsSummary == null)
                return;
            bonusPointsSummary.PropertyChanged -= bonusPointsSummary_PropertyChanged;
            bonusPointsSummary.PropertyChanged += bonusPointsSummary_PropertyChanged;
            bonusPointsSummary.BonusPointsSummaryLists.EntityAdded -= BonusPointsSummaryLists_EntityAdded;
            bonusPointsSummary.BonusPointsSummaryLists.EntityAdded += BonusPointsSummaryLists_EntityAdded;
            bonusPointsSummary.BonusPointsSummaryLists.EntityRemoved -= BonusPointsSummaryLists_EntityRemoved;
            bonusPointsSummary.BonusPointsSummaryLists.EntityRemoved += BonusPointsSummaryLists_EntityRemoved;
        }

        void BonusPointsSummaryLists_EntityAdded(object sender, EntityCollectionChangedEventArgs<BonusPointsSummaryList> e) {
            var bonusPointsSummary = this.DataContext as BonusPointsSummary;
            if(bonusPointsSummary == null)
                return;
            var sumnumber = bonusPointsSummary.BonusPointsSummaryLists.Where(r => r.SouceType == (int)DcsBonusPointsType.积分使用).Sum(r => r.BonusPointsAmount);
            var subnumber = bonusPointsSummary.BonusPointsSummaryLists.Where(r => r.SouceType == (int)DcsBonusPointsType.积分返还).Sum(r => r.BonusPointsAmount);
            bonusPointsSummary.SummaryAmount = sumnumber-subnumber;
            var sumpoint = bonusPointsSummary.BonusPointsSummaryLists.Where(r => r.SouceType == (int)DcsBonusPointsType.积分使用).Sum(r => r.BonusPoints);
            var subpoint = bonusPointsSummary.BonusPointsSummaryLists.Where(r => r.SouceType == (int)DcsBonusPointsType.积分返还).Sum(r => r.BonusPoints);
            bonusPointsSummary.SummaryBonusPoints = sumpoint-subpoint;
        }
    }
}
