﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class CrossSalesOrderCheckSeniorDataEditView {
        private DataGridViewBase partsPurchaseOrderDetailForEditDataGridView;
        private KeyValueManager keyValueManager;
        private ObservableCollection<KeyValuePair> crossSalesOrderTypes;
        private ObservableCollection<KeyValuePair> partsShippingMethods;

        private readonly string[] kvNames = {
            "CrossSalesOrderType","PartsShipping_Method"
        };

        private DataGridViewBase PartsPurchaseOrderDetailForEditDataGridView {
            get {
                if(this.partsPurchaseOrderDetailForEditDataGridView == null) {
                    this.partsPurchaseOrderDetailForEditDataGridView = DI.GetDataGridView("CrossSalesOrderDetailDetail");
                    this.partsPurchaseOrderDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsPurchaseOrderDetailForEditDataGridView;
            }
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        protected override string BusinessName {
            get {
                return "跨区销售备案管理高级审核";
            }
        }


        protected override bool OnRequestCanSubmit() {
            return true;
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetCrossSalesOrderWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    DataEditPanels.FilePath = entity.Path;
                    this.SetObjectToEdit(entity);

                }
            }, null);


        }

        public ObservableCollection<KeyValuePair> CrossSalesOrderTypes {
            get {
                return this.crossSalesOrderTypes ?? (crossSalesOrderTypes = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> PartsShippingMethods {
            get {
                return this.partsShippingMethods ?? (this.partsShippingMethods = new ObservableCollection<KeyValuePair>());
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register("清单", null, this.PartsPurchaseOrderDetailForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            this.Root.Children.Add(detailDataEditView);
            this.RegisterButton(new ButtonItem {
                Command = new DelegateCommand(this.RejectCurrentData),
                Title = "驳回",
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative)
            }, true);
            KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData(() => {
                foreach(var keyValuePair in this.KeyValueManager[this.kvNames[0]]) {
                    this.CrossSalesOrderTypes.Add(keyValuePair);
                }
                foreach(var keyValuePair in this.KeyValueManager[this.kvNames[1]]) {
                    this.PartsShippingMethods.Add(keyValuePair);
                }
            });
            CbShippingMethod.ItemsSource = PartsShippingMethods;
            CbType.ItemsSource = CrossSalesOrderTypes;
            //附件
            this.DataEditPanels.SetValue(Grid.RowProperty, 5);
            this.DataEditPanels.SetValue(Grid.ColumnProperty, 0);
            this.DataEditPanels.Margin = new Thickness(35, 450, 0, 0);
            this.DataEditPanels.HorizontalAlignment = HorizontalAlignment.Left;
            this.DataEditPanels.VerticalAlignment = VerticalAlignment.Top;
            this.DataEditPanels.SetValue(Grid.ColumnSpanProperty, 2);
            this.DataEditPanels.isHiddenButtons = true;
            this.Root.Children.Add(DataEditPanels);
        }
        private void RejectCurrentData() {
            var crossSalesOrder = this.DataContext as CrossSalesOrder;
            if(crossSalesOrder == null)
                return;
            crossSalesOrder.ApproveMent = this.TApproveMent.Text;

            ((IEditableObject)crossSalesOrder).EndEdit();
            try {
                if(crossSalesOrder.Can驳回跨区域销售单)
                    crossSalesOrder.驳回跨区域销售单();
                ExecuteSerivcesMethod("驳回生成");
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
        }
        private void ExecuteSerivcesMethod(string notifyMessage) {
            DomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    DomainContext.RejectChanges();
                    return;
                }
                this.NotifyEditSubmitted();
                this.OnCustomEditSubmitted();
                UIHelper.ShowNotification(notifyMessage);
            }, null);
        }
        public void OnCustomEditSubmitted() {
            this.DataContext = null;
        }
        public new event EventHandler EditSubmitted;
        private void NotifyEditSubmitted() {
            var handler = this.EditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }
        private PartsSaleFileUploadDataEditPanel productDataEditPanels;

        public PartsSaleFileUploadDataEditPanel DataEditPanels {
            get {
                return this.productDataEditPanels ?? (this.productDataEditPanels = (PartsSaleFileUploadDataEditPanel)DI.GetDataEditPanel("PartsSaleFileUpload"));
            }
        }





        protected override void OnEditSubmitting() {
            if(!this.PartsPurchaseOrderDetailForEditDataGridView.CommitEdit())
                return;

            var crossSalesOrder = this.DataContext as CrossSalesOrder;
            if(crossSalesOrder == null)
                return;

            crossSalesOrder.ValidationErrors.Clear();
            foreach(var relation in crossSalesOrder.CrossSalesOrderDetails)
                relation.ValidationErrors.Clear();
            ((IEditableObject)crossSalesOrder).EndEdit();
            try {
                if(crossSalesOrder.Can高级审核跨区域销售单)
                    crossSalesOrder.高级审核跨区域销售单();
                ExecuteSerivcesMethod("高级审核通过");
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
        }

        public CrossSalesOrderCheckSeniorDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.LoadData();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }


        protected override void Reset() {
            var crossSalesOrder = this.DataContext as CrossSalesOrder;
            if(this.DomainContext.CrossSalesOrders.Contains(crossSalesOrder))
                this.DomainContext.CrossSalesOrders.Detach(crossSalesOrder);
        }
    }
}