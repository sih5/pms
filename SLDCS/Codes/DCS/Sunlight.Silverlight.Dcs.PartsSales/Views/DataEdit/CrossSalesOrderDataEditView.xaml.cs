﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class CrossSalesOrderDataEditView {
        private DataGridViewBase partsPurchaseOrderDetailForEditDataGridView;
        private KeyValueManager keyValueManager;
        private ObservableCollection<KeyValuePair> crossSalesOrderTypes;
        private ObservableCollection<KeyValuePair> partsShippingMethods;

        private readonly string[] kvNames = {
            "CrossSalesOrderType","PartsShipping_Method"
        };

        private DataGridViewBase PartsPurchaseOrderDetailForEditDataGridView {
            get {
                if(this.partsPurchaseOrderDetailForEditDataGridView == null) {
                    this.partsPurchaseOrderDetailForEditDataGridView = DI.GetDataGridView("CrossSalesOrderDetailForEdit");
                    this.partsPurchaseOrderDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsPurchaseOrderDetailForEditDataGridView;
            }
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        protected override string BusinessName {
            get {
                return "跨区销售备案";
            }
        }


        protected override bool OnRequestCanSubmit() {
            return true;
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetCrossSalesOrderWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    DataEditPanels.FilePath = entity.Path;
                    this.SetObjectToEdit(entity);
                   
                }
            }, null);


        }

        public ObservableCollection<KeyValuePair> CrossSalesOrderTypes {
            get {
                return this.crossSalesOrderTypes ?? (crossSalesOrderTypes = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> PartsShippingMethods {
            get {
                return this.partsShippingMethods ?? (this.partsShippingMethods = new ObservableCollection<KeyValuePair>());
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register("清单", null, this.PartsPurchaseOrderDetailForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);


            var queryWindow = DI.GetQueryWindow("CrossSalesOrderCompany");
            queryWindow.SelectionDecided += this.QueryWindow_SelectionDecided;
            queryWindow.Loaded += queryWindow_Loaded;
            this.ptbSupplierCode.PopupContent = queryWindow;


            this.Root.Children.Add(detailDataEditView);

            KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData(() => {
                foreach(var keyValuePair in this.KeyValueManager[this.kvNames[0]]) {
                    this.CrossSalesOrderTypes.Add(keyValuePair);
                }
                foreach(var keyValuePair in this.KeyValueManager[this.kvNames[1]]) {
                    this.PartsShippingMethods.Add(keyValuePair);
                }
            });
            //附件
            this.DataEditPanels.SetValue(Grid.RowProperty, 5);
            this.DataEditPanels.SetValue(Grid.ColumnProperty, 0);
            this.DataEditPanels.Margin = new Thickness(30, 280, 0, 0);
            this.DataEditPanels.HorizontalAlignment = HorizontalAlignment.Left;
            this.DataEditPanels.VerticalAlignment = VerticalAlignment.Top;
            this.DataEditPanels.SetValue(Grid.ColumnSpanProperty, 2);
            this.Root.Children.Add(DataEditPanels);
        }
        private PartsSaleFileUploadDataEditPanel productDataEditPanels;

        public PartsSaleFileUploadDataEditPanel DataEditPanels {
            get {
                return this.productDataEditPanels ?? (this.productDataEditPanels = (PartsSaleFileUploadDataEditPanel)DI.GetDataEditPanel("PartsSaleFileUpload"));
            }
        }
        private void queryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            var partsPurchaseOrder = this.DataContext as PartsPurchaseOrder;
            if(queryWindow == null || partsPurchaseOrder == null)
                return;
            if(partsPurchaseOrder.PartsSalesCategoryId == default(int)) {
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
                return;
            }
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "Branch.Name", BaseApp.Current.CurrentUserData.EnterpriseName
            });
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem("PartsSalesCategoryId", typeof(int), FilterOperator.IsEqualTo, partsPurchaseOrder.PartsSalesCategoryId));
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "BranchId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }

        private void QueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var company = queryWindow.SelectedEntities.Cast<Company>().FirstOrDefault();
            if(company == null || !this.PartsPurchaseOrderDetailForEditDataGridView.CommitEdit()) {
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
                return;
            }
            var crossSalesOrder = this.DataContext as CrossSalesOrder;
            if(crossSalesOrder == null)
                return;
            try {                
                crossSalesOrder.SubCompanyId = company.Id;
                crossSalesOrder.SubCompanyCode = company.Code;
                crossSalesOrder.SubCompanyName = company.Name;

                //TODO: 删除时抛出异常 
                var deleteDetals = crossSalesOrder.CrossSalesOrderDetails.ToArray();
                ((IEditableObject)crossSalesOrder).EndEdit();
                foreach(var detail in deleteDetals) {
                    ((IEditableObject)detail).EndEdit();
                    detail.ValidationErrors.Clear();
                    crossSalesOrder.CrossSalesOrderDetails.Remove(detail);
                }
            } finally {
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }
        }


        protected override void OnEditSubmitting() {
            if(!this.PartsPurchaseOrderDetailForEditDataGridView.CommitEdit())
                return;

            var crossSalesOrder = this.DataContext as CrossSalesOrder;
            if(crossSalesOrder == null)
                return;

            crossSalesOrder.ValidationErrors.Clear();
            foreach(var relation in crossSalesOrder.CrossSalesOrderDetails)
                relation.ValidationErrors.Clear();

            if(string.IsNullOrEmpty(crossSalesOrder.ReceivingName))
                crossSalesOrder.ValidationErrors.Add(new ValidationResult("请填写收货人信息", new[] {
                    "ReceivingName"
                }));

            if(crossSalesOrder.SubCompanyId <= 0)
                crossSalesOrder.ValidationErrors.Add(new ValidationResult("请选择隶属企业", new[] {
                    "SubCompanyId"
                }));
            if(crossSalesOrder.ShippingMethod <= 0)
                crossSalesOrder.ValidationErrors.Add(new ValidationResult("请选择收货方式", new[] {
                    "ShippingMethod"
                }));

            if(string.IsNullOrEmpty(crossSalesOrder.ReceivingPhone))
                crossSalesOrder.ValidationErrors.Add(new ValidationResult("请填写收货电话", new[] {
                    "ReceivingPhone"
                }));

            if(string.IsNullOrEmpty(crossSalesOrder.ReceivingAddress))
                crossSalesOrder.ValidationErrors.Add(new ValidationResult("请填写收货地址", new[] {
                    "ReceivingAddress"
                }));
          
            if(!crossSalesOrder.CrossSalesOrderDetails.Any()) {
                UIHelper.ShowNotification("请选择配件信息");
                return;
            }
           
            if(crossSalesOrder.Type != (int)DCSCrossSalesOrderType.其他 && crossSalesOrder.CrossSalesOrderDetails.Count()>1) {
                UIHelper.ShowNotification(string.Format("只能新增一条配件"));
                return;
            }
            foreach(var relation in crossSalesOrder.CrossSalesOrderDetails) {
                if(string.IsNullOrEmpty(relation.SparePartName))
                    relation.ValidationErrors.Add(new ValidationResult("请填写配件名称", new[] {
                        "SparePartName"
                    }));
                if(relation.OrderedQuantity<=0) {
                    UIHelper.ShowNotification(string.Format("订货数量必须大于0"));
                    return;
                }
                if(string.IsNullOrEmpty(relation.Vin)) {
                    UIHelper.ShowNotification(string.Format("请填写{0},的底盘号",relation.SparePartName));
                    return;
                }
                if(string.IsNullOrEmpty(relation.Explain)) {
                    UIHelper.ShowNotification(string.Format("请填写{0},的跨区域说明" , relation.SparePartName));
                    return;
                }
                relation.IsHand = true;
                if(relation.SparePartId.HasValue) {
                    relation.IsHand = false;
                }
                
            }
            crossSalesOrder.Path = DataEditPanels.FilePath;
            ((IEditableObject)crossSalesOrder).EndEdit();
            try {
                if(this.EditState == DataEditState.New) {
                    if(crossSalesOrder.Can新增跨区域销售单)
                        crossSalesOrder.新增跨区域销售单();
                } else {
                    if(crossSalesOrder.Can修改跨区域销售单)
                        crossSalesOrder.修改跨区域销售单();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        public CrossSalesOrderDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.LoadData();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }


        protected override void Reset() {
            var crossSalesOrder = this.DataContext as CrossSalesOrder;
            if(this.DomainContext.CrossSalesOrders.Contains(crossSalesOrder))
                this.DomainContext.CrossSalesOrders.Detach(crossSalesOrder);
        }
    }
}