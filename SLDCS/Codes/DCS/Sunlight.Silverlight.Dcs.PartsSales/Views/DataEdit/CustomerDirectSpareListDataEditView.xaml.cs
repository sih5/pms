﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class CustomerDirectSpareListDataEditView {
        public event EventHandler CustomEditSubmitted;
        private ObservableCollection<KeyValuePair> kvPartsSalesCategorys;
        private DcsDomainContext domainContext = new DcsDomainContext();

        public ObservableCollection<KeyValuePair> KvPartsSalesCategorys {
            get {
                return this.kvPartsSalesCategorys ?? (this.kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>());
            }
        }

        public CustomerDirectSpareListDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            var customerInformationQueryWindow = DI.GetQueryWindow("CustomerInformationBySparepart");
            customerInformationQueryWindow.SelectionDecided += this.CustomerInformationQueryWindow_SelectionDecided;
            this.customerPopoupTextBox.PopupContent = customerInformationQueryWindow;
            var queryWindowSparePart = DI.GetQueryWindow("SparePartForPartsBranch");
            queryWindowSparePart.SelectionDecided += this.sparePartCodes_SelectionDecided;
            this.sparePartCodePopoupTextBox.PopupContent = queryWindowSparePart;
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(v => v.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && v.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvPartsSalesCategorys.Clear();
                foreach(var item in loadOp.Entities) {
                    this.KvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.Name,
                        UserObject = item
                    });
                }
                this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            }, null);
        }

        private void sparePartCodes_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var sparePart = queryWindow.SelectedEntities.Cast<SparePart>().FirstOrDefault();
            if(sparePart == null)
                return;
            var customerDirectSpareList = this.DataContext as CustomerDirectSpareList;
            if(customerDirectSpareList == null)
                return;
            customerDirectSpareList.SparePartId = sparePart.Id;
            customerDirectSpareList.SparePartCode = sparePart.Code;
            customerDirectSpareList.SparePartName = sparePart.Name;
            customerDirectSpareList.MeasureUnit = sparePart.MeasureUnit;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void CustomerInformationQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var customerInformation = queryWindow.SelectedEntities.Cast<CustomerInformation>().FirstOrDefault();
            if(customerInformation == null)
                return;
            var customerDirectSpareList = this.DataContext as CustomerDirectSpareList;
            if(customerDirectSpareList == null)
                return;
            customerDirectSpareList.CustomerId = customerInformation.CustomerCompany.Id;
            customerDirectSpareList.CustomerCode = customerInformation.CustomerCompany.Code;
            customerDirectSpareList.CustomerName = customerInformation.CustomerCompany.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        protected override void OnEditSubmitting() {
            var customerDirectSpareList = this.DataContext as CustomerDirectSpareList;
            if(customerDirectSpareList == null)
                return;
            if(customerDirectSpareList.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_CustomerDirectSpareList);
                return;
            }
            if(customerDirectSpareList.CustomerId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_Customer);
                return;
            }
            if(customerDirectSpareList.SparePartId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_SparePart);
                return;
            }
            ((IEditableObject)customerDirectSpareList).EndEdit();
            try {
                this.DomainContext.新增客户直供配件清单维护(customerDirectSpareList, invokeOp => {
                    if(invokeOp.HasError) {
                        if(!invokeOp.IsErrorHandled)
                            invokeOp.MarkErrorAsHandled();
                        var error = invokeOp.ValidationErrors.FirstOrDefault();
                        if(error != null) {
                            UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage, 5);
                            DomainContext.RejectChanges();
                            return;
                        }
                        DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                        this.DomainContext.RejectChanges();
                    }
                    this.NotifyEditSubmitted();
                }, null);
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
            }
        }

        private void NotifyEditSubmitted() {
            var handler = this.CustomEditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        protected override bool OnRequestCanSubmit() {
            this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            return true;
        }

        protected override string BusinessName {
            get {
                return PartsSalesUIStrings.BusinessName_CustomerDirectSpareList;
            }
        }
    }
}