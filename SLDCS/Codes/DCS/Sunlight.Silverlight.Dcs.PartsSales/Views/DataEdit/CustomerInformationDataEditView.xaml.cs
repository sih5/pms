﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class CustomerInformationDataEditView {

        public CustomerInformationDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            var queryWindowVin = DI.GetQueryWindow("Company");
            queryWindowVin.SelectionDecided += this.QueryWindow_SelectionDecided;
            this.ptbcustomerCompanyName.PopupContent = queryWindowVin;
            this.isConent = true;
        }

        private void QueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var selectedCompany = queryWindow.SelectedEntities.Cast<Company>().FirstOrDefault();
            if(selectedCompany == null)
                return;

            var customerInformation = this.DataContext as CustomerInformation;
            if(customerInformation == null)
                return;

            this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(entity => entity.Id == selectedCompany.Id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var company = loadOp.Entities.SingleOrDefault();
                if(company != null) {
                    customerInformation.CustomerCompany = company;
                    customerInformation.ContactPerson = company.ContactPerson;
                    customerInformation.ContactPhone = company.ContactPhone;
                }
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }, null);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetCustomerInformationWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        protected override void OnEditSubmitting() {
            var customerInformation = this.DataContext as CustomerInformation;
            if(customerInformation == null)
                return;
            customerInformation.ValidationErrors.Clear();
            if(customerInformation.CustomerCompanyId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_CustomerCompanyName_NameIsNull);
                return;
            }
            ((IEditableObject)customerInformation).EndEdit();
            try {
                if(this.EditState == DataEditState.Edit)
                    if(customerInformation.Can修改客户信息)
                        customerInformation.修改客户信息();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_CustomerInformation;
            }
        }

        public string SalesCompanyName {
            get {
                return BaseApp.Current.CurrentUserData.EnterpriseName;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
