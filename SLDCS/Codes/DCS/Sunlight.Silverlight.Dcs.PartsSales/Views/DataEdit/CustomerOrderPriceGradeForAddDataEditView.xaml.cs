﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class CustomerOrderPriceGradeForAddDataEditView : INotifyPropertyChanged {
        private DataGridViewBase customerOrderPriceGradeForEdit;
        private ObservableCollection<KeyValuePair> kvPartsSalesCategorys;
        public event PropertyChangedEventHandler PropertyChanged;
        private string customerCompanyCode, customerCompanyName, remark;
        private ObservableCollection<CustomerOrderPriceGrade> customerOrderPriceGrades;
        private int customerId, partsSalesCategoryId;

        public CustomerOrderPriceGradeForAddDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.Loaded += this.CustomerOrderPriceGradeForAddDataEditView_Loaded;
        }

        private void CustomerOrderPriceGradeForAddDataEditView_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            this.Clear();
        }
        public ObservableCollection<KeyValuePair> KvPartsSalesCategorys {
            get {
                return this.kvPartsSalesCategorys ?? (this.kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>());
            }
        }

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private DataGridViewBase CustomerOrderPriceGradeForEdit {
            get {
                return this.customerOrderPriceGradeForEdit ?? (this.customerOrderPriceGradeForEdit = DI.GetDataGridView("CustomerOrderPriceGradeForEdit"));
            }
        }

        private void CreateUI() {
            var customerInformationQueryWindow = DI.GetQueryWindow("CustomerInformationBySparepart");
            customerInformationQueryWindow.SelectionDecided += this.CustomerInformationQueryWindow_SelectionDecided;
            this.CustomerCompanyPopoupTextBox.PopupContent = customerInformationQueryWindow;
            this.Root.Children.Add(this.CreateVerticalLine(1));
            this.CustomerOrderPriceGradeForEdit.DataContext = this;
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(CustomerOrderPriceGrade), PartsSalesUIStrings.DetailPanel_Title_OrderPriceAndGrade), null, () => {
                var view = this.CustomerOrderPriceGradeForEdit;
                view.DomainContext = this.DomainContext;
                view.DataContext = this;
                return view;
            });
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(v => v.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && v.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvPartsSalesCategorys.Clear();
                foreach(var item in loadOp.Entities) {
                    this.KvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.Name,
                        UserObject = item
                    });
                }
            }, null);

        }

        private void CustomerInformationQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var customerInformation = queryWindow.SelectedEntities.Cast<CustomerInformation>().FirstOrDefault();
            if(customerInformation == null)
                return;
            this.customerId = customerInformation.CustomerCompany.Id;
            this.CustomerCompanyCode = customerInformation.CustomerCompany.Code;
            this.CustomerCompanyName = customerInformation.CustomerCompany.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void Clear() {
            this.CustomerCompanyName = string.Empty;
            this.CustomerCompanyCode = string.Empty;
            this.customerId = default(int);
            this.Remark = string.Empty;
            this.PartsSalesCategoryId = default(int);
            this.CustomerOrderPriceGrades.Clear();
        }

        protected override void OnEditSubmitting() {
            this.DomainContext.CustomerOrderPriceGrades.Clear();
            if(!this.CustomerOrderPriceGradeForEdit.CommitEdit())
                return;
            var validationErrors = new List<string>();
            if(string.IsNullOrWhiteSpace(this.CustomerCompanyCode) || string.IsNullOrWhiteSpace(this.CustomerCompanyName) || this.customerId == default(int))
                validationErrors.Add(PartsSalesUIStrings.DataEditView_Validation_CustomerOrderPriceGrade_CustomerCompanyIsNull);
            if(!this.CustomerOrderPriceGrades.Any())
                validationErrors.Add(PartsSalesUIStrings.DataEditView_Validation_CustomerOrderPriceGrade_OrderPriceAndGeadIsNull);
            var groupOrder = from priceGrades in this.CustomerOrderPriceGrades
                             group priceGrades by new {
                                 priceGrades.PartsSalesOrderTypeId,
                             }
                                 into groups
                                 select new {
                                     count = groups.Count(),
                                     key = groups.Key
                                 };
            if(groupOrder.Any(group => group.count > 1))
                validationErrors.Add(PartsSalesUIStrings.DataEditView_Validation_CustomerOrderPriceGrade_PartsSalesOrderTypeUnique);
            if(this.PartsSalesCategoryId == default(int))
                validationErrors.Add(PartsSalesUIStrings.DataEditView_Validation_CustomerOrderPriceGrade_PartsSalesCategoryIsNull);
            if(validationErrors.Any()) {
                UIHelper.ShowNotification(string.Join(Environment.NewLine, validationErrors));
                return;
            }
            foreach(var entity in this.CustomerOrderPriceGrades) {
                entity.ValidationErrors.Clear();
                entity.CustomerCompanyCode = this.CustomerCompanyCode;
                entity.CustomerCompanyName = this.CustomerCompanyName;
                entity.BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                entity.PartsSalesCategoryId = this.PartsSalesCategoryId;
                entity.CustomerCompanyId = this.customerId;
                entity.Status = (int)DcsBaseDataStatus.有效;
                entity.Remark = this.Remark;
                ((IEditableObject)entity).EndEdit();
                if(!this.DomainContext.CustomerOrderPriceGrades.Contains(entity))
                    this.DomainContext.CustomerOrderPriceGrades.Add(entity);
            }
            base.OnEditSubmitting();

        }

        protected override void Reset() {
            this.Clear();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_CustomerOrderPriceGrade;
            }
        }

        public string CustomerCompanyCode {
            get {
                return this.customerCompanyCode;
            }
            set {
                this.customerCompanyCode = value;
                this.OnPropertyChanged("CustomerCompanyCode");
            }
        }

        public string CustomerCompanyName {
            get {
                return this.customerCompanyName;
            }
            set {
                this.customerCompanyName = value;
                this.OnPropertyChanged("CustomerCompanyName");
            }
        }

        public string Remark {
            get {
                return this.remark;
            }
            set {
                this.remark = value;
                this.OnPropertyChanged("Remark");
            }
        }
        public int PartsSalesCategoryId {
            get {
                return this.partsSalesCategoryId;
            }
            set {
                this.partsSalesCategoryId = value;
                this.OnPropertyChanged("PartsSalesCategoryId");
            }
        }
        public ObservableCollection<CustomerOrderPriceGrade> CustomerOrderPriceGrades {
            get {
                return this.customerOrderPriceGrades ?? (this.customerOrderPriceGrades = new ObservableCollection<CustomerOrderPriceGrade>());
            }
        }

        private void DcsComboBox_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            //if(CustomerOrderPriceGrades.Any() && PartsSalesCategoryId != default(int)) {
            //    DcsUtils.Confirm(PartsSalesUIStrings.DataEditView_Confirm_PartsSalesCategoryId, () => {
            //        CustomerOrderPriceGrades.Clear();
            //    }, () => {

            //    });
            //}
            CustomerOrderPriceGrades.Clear();
            if(PartsSalesCategoryId != default(int)) {
                this.DomainContext.Load(this.DomainContext.GetPartsSalesOrderTypesQuery().Where(entity => entity.Status == (int)DcsBaseDataStatus.有效 && entity.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && entity.PartsSalesCategoryId == PartsSalesCategoryId), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError)
                        return;
                    foreach(var item in loadOp.Entities) {
                        var customerOrderPriceGrade = new CustomerOrderPriceGrade {
                            PartsSalesOrderTypeId = item.Id,
                            PartsSalesOrderTypeCode = item.Code,
                            PartsSalesOrderTypeName = item.Name,
                            Coefficient = 1,
                            CustomerCompanyId = this.customerId,
                            CustomerCompanyCode = this.CustomerCompanyCode,
                            CustomerCompanyName = this.customerCompanyName
                        };
                        CustomerOrderPriceGrades.Add(customerOrderPriceGrade);
                    }
                }, null);
            }
        }
    }
}