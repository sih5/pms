﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class CustomerOrderPriceGradeForEditDataEditView {

        public CustomerOrderPriceGradeForEditDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("CustomerOrderPriceGrade"));
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetCustomerCompanyAndPartsSalesPartsSalesCategoryQuery().Where(entity => entity.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();

                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        protected override void OnEditSubmitting() {
            var customerOrderPriceGrade = this.DataContext as CustomerOrderPriceGrade;
            if(customerOrderPriceGrade == null)
                return;
            customerOrderPriceGrade.ValidationErrors.Clear();
            if(customerOrderPriceGrade.Coefficient < default(double)) {
                customerOrderPriceGrade.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_CustomerOrderPriceGrade_CoefficientLessThanZero, new[] {
                    "Coefficient"
                }));
            }
            if(customerOrderPriceGrade.HasValidationErrors) {
                UIHelper.ShowNotification(string.Join(Environment.NewLine, customerOrderPriceGrade.ValidationErrors));
                return;
            }

            if(customerOrderPriceGrade.HasValidationErrors)
                return;
            ((IEditableObject)customerOrderPriceGrade).EndEdit();
            try {
                if(customerOrderPriceGrade.Can修改客户企业订单价格等级)
                    customerOrderPriceGrade.修改客户企业订单价格等级();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_CustomerOrderPriceGrade;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}