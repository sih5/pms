﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class CustomerOrderPriceGradeForImportDataEditView {
       private ICommand exportFileCommand;
       private const string EXPORT_DATA_FILE_NAME = "客户企业订单价格等级信息模板.xlsx";
        private DataGridViewBase customerInformationForImportForEdit;
        protected DataGridViewBase CustomerInformationForImportForEdit {
            get {
                if(this.customerInformationForImportForEdit == null) {
                    this.customerInformationForImportForEdit = DI.GetDataGridView("CustomerOrderPriceGradeForImport");
                    this.customerInformationForImportForEdit.DataContext = this.DataContext;
                }
                return this.customerInformationForImportForEdit;
            }
        }

     
        public CustomerOrderPriceGradeForImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(CreateUI);
        }

        protected override string Title {
            get {
                return PartsSalesUIStrings.DataEdit_Title_CustomerOrderPriceGradeForImport;
            }
        }

        private void CreateUI() {
            this.ShowSaveButton();
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = PartsSalesUIStrings.DataEdit_Title_CustomerOrderPriceGradeForImportList,
                Content = this.CustomerInformationForImportForEdit
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.ImportCustomerOrderPriceGradeAsync(fileName);
            this.ExcelServiceClient.ImportCustomerOrderPriceGradeCompleted -= ExcelServiceClient_ImportCustomerOrderPriceGradeCompleted;
            this.ExcelServiceClient.ImportCustomerOrderPriceGradeCompleted += ExcelServiceClient_ImportCustomerOrderPriceGradeCompleted;
        }

        private  void ExcelServiceClient_ImportCustomerOrderPriceGradeCompleted(object sender, ImportCustomerOrderPriceGradeCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!HasImportingError) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Notification_CompletedImport, 10);
            } else {
                if(!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if(!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }

       
        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = PartsSalesUIStrings.DataEditPanel_Text_CustomerOrderPriceGrade_CustomerCompanyCode,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsSalesUIStrings.DataEditPanel_Text_CustomerOrderPriceGrade_CustomerCompanyName,
                                                
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesCategory_Name,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsSalesUIStrings.DataGridView_ColumnItem_Title_CustomerOrderPriceGrade_PartsSalesOrderTypeName,
                                                IsRequired=true
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsSalesUIStrings.DataEditPanel_Text_Common_Coefficient,
                                                IsRequired=true
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

    }
}
