﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class CustomerSupplyInitialFeeSetDataEditView {
        public event EventHandler CustomEditSubmitted;
        private ObservableCollection<KeyValuePair> kvPartsSalesCategorys;
        private DcsDomainContext domainContext = new DcsDomainContext();

        public ObservableCollection<KeyValuePair> KvPartsSalesCategorys {
            get {
                return this.kvPartsSalesCategorys ?? (this.kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>());
            }
        }

        public CustomerSupplyInitialFeeSetDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            var customerInformationQueryWindow = DI.GetQueryWindow("CustomerInformationBySparepart");
            customerInformationQueryWindow.SelectionDecided += this.CustomerInformationQueryWindow_SelectionDecided;
            this.customerPopoupTextBox.PopupContent = customerInformationQueryWindow;
            var queryWindowSupplier = DI.GetQueryWindow("PartsSupplier");
            queryWindowSupplier.SelectionDecided +=this.queryWindowSupplier_SelectionDecided;
            this.supplierPopoupTextBox.PopupContent = queryWindowSupplier;
            this.domainContext.Load(this.domainContext.GetPartsSalesCategoriesQuery().Where(v => v.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && v.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvPartsSalesCategorys.Clear();
                foreach(var item in loadOp.Entities) {
                    this.KvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.Name,
                        UserObject = item
                    });
                }
                this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            }, null);
        }

        private void queryWindowSupplier_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var partsSupplier = queryWindow.SelectedEntities.Cast<PartsSupplier>().FirstOrDefault();
            if(partsSupplier == null)
                return;
            var customerSupplyInitialFeeSet = this.DataContext as CustomerSupplyInitialFeeSet;
            if(customerSupplyInitialFeeSet == null)
                return;
            customerSupplyInitialFeeSet.SupplierId = partsSupplier.Id;
            customerSupplyInitialFeeSet.SupplierCode = partsSupplier.Code;
            customerSupplyInitialFeeSet.SupplierName = partsSupplier.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void CustomerInformationQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var customerInformation = queryWindow.SelectedEntities.Cast<CustomerInformation>().FirstOrDefault();
            if(customerInformation == null)
                return;
            var customerSupplyInitialFeeSet = this.DataContext as CustomerSupplyInitialFeeSet;
            if(customerSupplyInitialFeeSet == null)
                return;
            customerSupplyInitialFeeSet.CustomerId = customerInformation.CustomerCompany.Id;
            customerSupplyInitialFeeSet.CustomerCode = customerInformation.CustomerCompany.Code;
            customerSupplyInitialFeeSet.CustomerName = customerInformation.CustomerCompany.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        protected override void OnEditSubmitting() {
            var customerSupplyInitialFeeSet = this.DataContext as CustomerSupplyInitialFeeSet;
            if(customerSupplyInitialFeeSet == null)
                return;
            if(customerSupplyInitialFeeSet.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_CustomerDirectSpareList);
                return;
            }
            if(customerSupplyInitialFeeSet.CustomerId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_Customer);
                return;
            }
            if(customerSupplyInitialFeeSet.SupplierId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_CustomerSupplyInitialFeeSet_Supplier);
                return;
            }
            if(customerSupplyInitialFeeSet.AppointShippingDays == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.CustomerSupplyInitialFeeSet_Validations_AppointShippingDays);
                return;
            }
            ((IEditableObject)customerSupplyInitialFeeSet).EndEdit();
            try {
                this.DomainContext.新增客户与供应商起订金额设置(customerSupplyInitialFeeSet, invokeOp => {
                    if(invokeOp.HasError) {
                        if(!invokeOp.IsErrorHandled)
                            invokeOp.MarkErrorAsHandled();
                        var error = invokeOp.ValidationErrors.FirstOrDefault();
                        if(error != null) {
                            UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage, 5);
                            this.DomainContext.RejectChanges();
                            return;
                        }
                        DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                        this.DomainContext.RejectChanges();
                    }
                    this.NotifyEditSubmitted();
                }, null);
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
            }
        }

        private void NotifyEditSubmitted() {
            var handler = this.CustomEditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        protected override bool OnRequestCanSubmit()
        {
            this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            return true;
        }

        protected override string BusinessName {
            get {
                return PartsSalesUIStrings.BusinessName_CustomerSupplyInitialFeeSet;
            }
        }

        private void radMaskedTextBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e) {
            var customerSupplyInitialFeeSet = this.DataContext as CustomerSupplyInitialFeeSet;
            if(customerSupplyInitialFeeSet == null || radMaskedTextBox1.Value == null)
                return;
            if((e.PlatformKeyCode == 8 || e.PlatformKeyCode == 46) && customerSupplyInitialFeeSet.InitialFee == default(decimal)) {
                radMaskedTextBox1.SelectionStart = 0;
                radMaskedTextBox1.Value = null;
            }
        }
        private void appointShippingDays_KeyUp(object sender, System.Windows.Input.KeyEventArgs e) {
            var customerSupplyInitialFeeSet = this.DataContext as CustomerSupplyInitialFeeSet;
            if(customerSupplyInitialFeeSet == null || appointShippingDays.Value==null)
                return;
            if((e.PlatformKeyCode == 8 || e.PlatformKeyCode == 46) && customerSupplyInitialFeeSet.InitialFee == default(int)) {
                appointShippingDays.Value=null;
            }
        }
    }
}