﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class CustomerSupplyInitialFeeSetForImportDataEditView {
        private const string EXPORT_DATA_FILE_NAME = "导出客户与供应商起订金额设置.xlsx";
        private ICommand exportFileCommand;

        public CustomerSupplyInitialFeeSetForImportDataEditView()
        {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);

        }

        protected override string Title {
            get {
                return PartsSalesUIStrings.DataEdit_Title_CustomerSupplyInitialFeeSet;
            }
        }
        private void CreateUI() {
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }
        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }
        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.ImportCustomerSupplyInitialFeeSetAsync(fileName);
            this.ExcelServiceClient.ImportCustomerSupplyInitialFeeSetCompleted -= ExcelServiceClient_ImportCustomerSupplyInitialFeeSetCompleted;
            this.ExcelServiceClient.ImportCustomerSupplyInitialFeeSetCompleted += ExcelServiceClient_ImportCustomerSupplyInitialFeeSetCompleted;
        }
        private void ExcelServiceClient_ImportCustomerSupplyInitialFeeSetCompleted(object sender, ImportCustomerSupplyInitialFeeSetCompletedEventArgs e)
        {
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!this.HasImportingError) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Notification_CompletedImport, 5);
            } else {
                if(!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if(!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }
        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }
        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columns = new List<ImportTemplateColumn> {
                                new ImportTemplateColumn {
                                   Name = PartsSalesUIStrings.DataEditView_PartsSalesCategory_Name,
                                    IsRequired = true,
                                },
                                new ImportTemplateColumn {
                                    Name = PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDelivery_Shop_Code,
                                    IsRequired = true,
                                },
                                new ImportTemplateColumn {
                                    Name = PartsSalesUIStrings.DataEdit_Text_CustomerSupplyInitialFee_SupplierCode,
                                    IsRequired = true,
                                },
                                new ImportTemplateColumn {
                                    Name = PartsSalesUIStrings.DataEdit_Text_CustomerSupplyInitialFee_InitialFee,
                                    IsRequired = true,
                                },
                                new ImportTemplateColumn {
                                    Name = PartsSalesUIStrings.CustomerSupplyInitialFeeSet_AppointShippingDays,
                                    IsRequired = true,
                                }
                            };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columns, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
    }
}