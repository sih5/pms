﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class DealerPartsRetailOrderDataEditView : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;
        private ObservableCollection<KeyValuePair> kvBranches;
        private ObservableCollection<KeyValuePair> kvPartsSalesCategoryNames;
        private ObservableCollection<KeyValuePair> kvRetailOrderTypes;
        private DataGridViewBase dealerPartsRetailOrderForEditDataGridView;
        private bool isCustomer, customIsEnabled;
        private string isVisibility;
        private int branchId, partsSalesCategoryId;
        private int? retailOrderType;
        private ICommand exportFileCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出服务站配件零售订单模板.xlsx";
        private string strFileName;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private readonly string[] kvNames = { "DealerPartsRetailOrder_RetailOrderType" };

        private KeyValueManager keyValueManager;

        public ObservableCollection<KeyValuePair> KvRetailOrderTypes {
            get {
                return this.kvRetailOrderTypes ?? (this.kvRetailOrderTypes = new ObservableCollection<KeyValuePair>());
            }
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        public DataGridViewBase DealerPartsRetailOrderForEditDataGridView {
            get {
                if(this.dealerPartsRetailOrderForEditDataGridView == null) {
                    this.dealerPartsRetailOrderForEditDataGridView = DI.GetDataGridView("DealerPartsRetailOrderForEdit");
                    this.dealerPartsRetailOrderForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.dealerPartsRetailOrderForEditDataGridView;
            }
        }

        public string IsVisibility {
            get {
                return this.isVisibility;
            }
            set {
                this.isVisibility = value;
                this.OnPropertyChanged("IsVisibility");
            }
        }

        public ObservableCollection<KeyValuePair> KvBranches {
            get {
                return this.kvBranches ?? (this.kvBranches = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvPartsSalesCategoryNames {
            get {
                return this.kvPartsSalesCategoryNames ?? (this.kvPartsSalesCategoryNames = new ObservableCollection<KeyValuePair>());
            }
        }

        public DealerPartsRetailOrderDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.keyValueManager.LoadData(() => {
                this.KvRetailOrderTypes.Clear();
                foreach(var item in this.KeyValueManager[this.kvNames[0]]) {
                    this.KvRetailOrderTypes.Add(new KeyValuePair {
                        Key = item.Key,
                        Value = item.Value
                    });
                }
            });
            this.Initializer.Register(this.CreateUI);
            isCustomer = true;
            this.CustomIsEnabled = true;
            //this.ptbSubDealerName.IsEnabled = false;
            //this.ptbCustomer.IsEnabled = true;
            this.DataContextChanged += DealerPartsRetailOrderDataEditView_DataContextChanged;
            this.Loaded += DealerPartsRetailOrderDataEditView_Loaded;
        }

        private void DealerPartsRetailOrderDataEditView_Loaded(object sender, RoutedEventArgs e)
        {
            this.DomainContext.Load(DomainContext.GetBranchesQuery().Where(er => er.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if (loadOp.HasError)
                    return;
                this.KvBranches.Clear();
                foreach (var branch in loadOp.Entities)
                    this.KvBranches.Add(new KeyValuePair
                    {
                        Key = branch.Id,
                        Value = branch.Name,
                        UserObject = branch
                    });
                this.ComboBoxBranch.SelectedIndex = 0;//默认分公司
                //默认营销分公司
                var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
                if (dealerPartsRetailOrder == null || !this.DealerPartsRetailOrderForEditDataGridView.CommitEdit())
                    return;
                dealerPartsRetailOrder.BranchId = loadOp.Entities.FirstOrDefault().Id;
                this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效 && v.BranchId == dealerPartsRetailOrder.BranchId), LoadBehavior.RefreshCurrent, loadOp2 =>
                {
                    if (loadOp2.HasError)
                        return;
                    this.KvPartsSalesCategoryNames.Clear();
                    foreach (var partsSalesCategory in loadOp2.Entities)
                    {
                        this.KvPartsSalesCategoryNames.Add(new KeyValuePair
                        {
                            Key = partsSalesCategory.Id,
                            Value = partsSalesCategory.Name,
                            UserObject = partsSalesCategory
                        });
                    }
                    this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
                    dealerPartsRetailOrder.PartsSalesCategoryId = loadOp2.Entities.FirstOrDefault().Id;
                }, null);
            }, null);
        }

        private void DealerPartsRetailOrderDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(dealerPartsRetailOrder == null || !this.DealerPartsRetailOrderForEditDataGridView.CommitEdit())
                return;
            dealerPartsRetailOrder.PropertyChanged -= dealerPartsRetailOrder_PropertyChanged;
            dealerPartsRetailOrder.PropertyChanged += dealerPartsRetailOrder_PropertyChanged;
        }

        private void dealerPartsRetailOrder_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(dealerPartsRetailOrder == null)
                return;
            switch(e.PropertyName) {
                case "PartsSalesCategoryId":
                    if(dealerPartsRetailOrder.PartsSalesCategoryId != default(int)) {
                        if(dealerPartsRetailOrder.PartsSalesCategory.IsNotWarranty) {
                            this.keyValueManager.LoadData(() => {
                                this.KvRetailOrderTypes.Clear();
                                foreach(var item in this.KeyValueManager[this.kvNames[0]].Where(r => r.Key != (int)DcsDealerPartsRetailOrderRetailOrderType.二级站)) {
                                    this.KvRetailOrderTypes.Add(new KeyValuePair {
                                        Key = item.Key,
                                        Value = item.Value
                                    });
                                }
                            });
                            this.IsVisibility = "Collapsed";
                        } else {
                            this.IsVisibility = "Visibility";
                            this.keyValueManager.LoadData(() => {
                                this.KvRetailOrderTypes.Clear();
                                foreach(var item in this.KeyValueManager[this.kvNames[0]]) {
                                    this.KvRetailOrderTypes.Add(new KeyValuePair {
                                        Key = item.Key,
                                        Value = item.Value
                                    });
                                }
                            });
                        }
                    }
                    break;
            }

        }

        private void CreateUI() {
            //InitializeComponent();
            this.IsVisibility = "Visible";
            this.LayoutRoot.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsSalesUIStrings.DataEditView_GroupTitle_PartsSalesOrderDetail, null, () => this.DealerPartsRetailOrderForEditDataGridView);

            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.LayoutRoot.Children.Add(detailDataEditView);
            var queryWindowRetailOrderCustomer = DI.GetQueryWindow("RetailOrderCustomer");
            if(queryWindowRetailOrderCustomer != null)
                queryWindowRetailOrderCustomer.SelectionDecided += this.QueryWindowRetailOrderCustomer_SelectionDecided;
            //this.ptbCustomer.PopupContent = queryWindowRetailOrderCustomer;
            //二级站选择
            //var queryWindowSubDealer = DI.GetQueryWindow("SubDealer");
            //if(queryWindowSubDealer != null)
            //    queryWindowSubDealer.SelectionDecided += this.queryWindowSubDealer_SelectionDecided;
            //this.ptbSubDealerName.PopupContent = queryWindowSubDealer;
            //this.DomainContext.Load(DomainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    this.KvBranches.Clear();
            //    foreach(var branch in loadOp.Entities)
            //        this.KvBranches.Add(new KeyValuePair {
            //            Key = branch.Id,
            //            Value = branch.Name,
            //            UserObject = branch
            //        });
            //    this.ComboBoxBranch.SelectedIndex = 0;//默认分公司
            //    //默认营销分公司
            //    var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            //    if (dealerPartsRetailOrder == null || !this.DealerPartsRetailOrderForEditDataGridView.CommitEdit())
            //        return;
            //    dealerPartsRetailOrder.BranchId = loadOp.Entities.FirstOrDefault().Id;
            //    this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效 && v.BranchId == dealerPartsRetailOrder.BranchId), LoadBehavior.RefreshCurrent, loadOp2 =>
            //    {
            //        if (loadOp2.HasError)
            //            return;
            //        this.KvPartsSalesCategoryNames.Clear();
            //        foreach (var partsSalesCategory in loadOp2.Entities)
            //        {
            //            this.KvPartsSalesCategoryNames.Add(new KeyValuePair
            //            {
            //                Key = partsSalesCategory.Id,
            //                Value = partsSalesCategory.Name,
            //                UserObject = partsSalesCategory
            //            });
            //        }
            //        this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            //        dealerPartsRetailOrder.PartsSalesCategoryId = loadOp2.Entities.FirstOrDefault().Id;
            //    }, null);
            //}, null);

            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.DataEditPanel_Action_ImportNew,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                Command = new DelegateCommand(this.ShowFileDialog)
            });
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.DataEditPanel_Action_Export,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                Command = this.ExportTemplateCommand
            });
        }

        #region 导入

        private void ShowFileDialog() {
            var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(dealerPartsRetailOrder == null)
                return;
            dealerPartsRetailOrder.ValidationErrors.Clear();
            if(string.IsNullOrWhiteSpace(dealerPartsRetailOrder.BranchName)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_BranchIdIsNull);
                return;
            }
            if(dealerPartsRetailOrder.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_CustomerOrderPriceGrade_PartsSalesCategoryIsNull);
                return;
            }
            if(dealerPartsRetailOrder.RetailOrderType == null || dealerPartsRetailOrder.RetailOrderType == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_RetailOrderType);
                return;
            }
            this.Uploader.ShowFileDialog();
        }

        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }

        private RadUpload uploader;
        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
                        if(dealerPartsRetailOrder == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.导入服务站配件零售订单Async(dealerPartsRetailOrder.PartsSalesCategoryId, dealerPartsRetailOrder.DealerId, (int)dealerPartsRetailOrder.SubDealerId, e.HandlerData.CustomData["Path"].ToString());
                        this.excelServiceClient.导入服务站配件零售订单Completed -= excelServiceClient_导入服务站配件零售订单Completed;
                        this.excelServiceClient.导入服务站配件零售订单Completed += excelServiceClient_导入服务站配件零售订单Completed;
                    };
                }
                return this.uploader;
            }
        }

        private void excelServiceClient_导入服务站配件零售订单Completed(object sender, 导入服务站配件零售订单CompletedEventArgs e) {
            var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(dealerPartsRetailOrder == null)
                return;
            foreach(var detail in dealerPartsRetailOrder.DealerRetailOrderDetails) {
                dealerPartsRetailOrder.DealerRetailOrderDetails.Remove(detail);
            }
            if(e.rightData != null && e.rightData.Length > 0) {
                int serialNum = 1;
                foreach(var data in e.rightData) {
                    dealerPartsRetailOrder.DealerRetailOrderDetails.Add(new DealerRetailOrderDetail {
                        SerialNumber = serialNum++,
                        PartsId = data.PartsId,
                        PartsCode = data.PartsCode,
                        PartsName = data.PartsName,
                        Quantity = data.Quantity,
                        Price = data.Price,
                        Remark = data.Remark,
                        SIHLabelCode=data.SIHLabelCode,
                        TraceProperty=data.TraceProperty
                    });
                }
                dealerPartsRetailOrder.TotalAmount = dealerPartsRetailOrder.DealerRetailOrderDetails.Sum(r => r.Quantity * r.Price);
            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowAlertMessage(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
        }

        /// <summary>
        /// 文件上传成功后，调用该方法进行上传后的处理。不支持异步调用
        /// <param>上传文件名 <name>fileName</name> </param>
        /// </summary>
        protected Action<string> UploadFileSuccessedProcessing;

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //取当前登录用户的HashCode来标记上传文件的唯一性
                //this.UploadFileSuccessedProcessing.Invoke(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }

        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage( PartsSalesUIStrings.DataEdit_Validation_Import_CloseFile);
                    e.Handled = true;
                }
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                              new ImportTemplateColumn {
                                                 Name=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                 Name=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                                            },
                                            new ImportTemplateColumn {
                                                 Name=PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_Quantity,
                                                IsRequired = true
                                            },new ImportTemplateColumn{
                                                Name=PartsSalesUIStrings.DataEdit_Text_DealerPartsRetailOrder_Price,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                               Name=PartsSalesUIStrings.DataEditView_Text_SecondClassStationPlan_Remark
                                            },
                                            new ImportTemplateColumn {
                                               Name="标签码"
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }

        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }

        #endregion

        private void queryWindowSubDealer_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var subDealer = queryWindow.SelectedEntities.Cast<SubDealer>().SingleOrDefault();
            if(subDealer == null)
                return;
            var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(dealerPartsRetailOrder == null)
                return;
            this.DomainContext.Load(this.DomainContext.GetSubDealersQuery().Where(r => r.Id == subDealer.Id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                if(loadOp.Entities != null && loadOp.Entities.Any()) {
                    dealerPartsRetailOrder.SubDealerId = loadOp.Entities.First().Id;
                    dealerPartsRetailOrder.SubDealerName = loadOp.Entities.First().Name;
                    dealerPartsRetailOrder.CustomerCellPhone = loadOp.Entities.First().ManagerMobile;
                    dealerPartsRetailOrder.CustomerPhone = loadOp.Entities.First().ManagerPhoneNumber;
                    dealerPartsRetailOrder.Customer = string.Empty;
                    isCustomer = false;
                    var parent = queryWindow.ParentOfType<RadWindow>();
                    if(parent != null)
                        parent.Close();
                }
            }, null);

        }

        private void QueryWindowRetailOrderCustomer_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var retailOrderCustomer = queryWindow.SelectedEntities.Cast<RetailOrderCustomer>().SingleOrDefault();
            if(retailOrderCustomer == null)
                return;
            var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(dealerPartsRetailOrder == null)
                return;
            this.DomainContext.Load(this.DomainContext.GetRetailOrderCustomersQuery().Where(r => r.Id == retailOrderCustomer.Id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                if(loadOp.Entities != null && loadOp.Entities.Any()) {
                    dealerPartsRetailOrder.Customer = loadOp.Entities.First().CustomerName;
                    dealerPartsRetailOrder.CustomerCellPhone = loadOp.Entities.First().CustomerCellPhone;
                    dealerPartsRetailOrder.CustomerPhone = loadOp.Entities.First().CustomerPhone;
                    dealerPartsRetailOrder.Address = loadOp.Entities.First().Address;
                    dealerPartsRetailOrder.CustomerUnit = loadOp.Entities.First().CustomerUnit;
                    dealerPartsRetailOrder.SubDealerName = string.Empty;
                    isCustomer = false;
                    var parent = queryWindow.ParentOfType<RadWindow>();
                    if(parent != null)
                        parent.Close();
                }
            }, null);

        }
        //导入暂不实现
        private void ImportDataInternal() {

        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsRetailOrderWithDetailsQuery().Where(entity => entity.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();

                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        protected override void OnEditSubmitting() {
            var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(dealerPartsRetailOrder == null || !this.DealerPartsRetailOrderForEditDataGridView.CommitEdit())
                return;
            if(!this.DealerPartsRetailOrderForEditDataGridView.CommitEdit())
                return;
            dealerPartsRetailOrder.ValidationErrors.Clear();
            foreach(var item in dealerPartsRetailOrder.DealerRetailOrderDetails) {
                item.ValidationErrors.Clear();
            }
            foreach(var retailOrderCustomer in DomainContext.RetailOrderCustomers) {
                retailOrderCustomer.ValidationErrors.Clear();
            }

            foreach(var relation in dealerPartsRetailOrder.DealerRetailOrderDetails.Where(e => e.Quantity <= 0)) {
                UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.DataEditView_Validation_PartsRetailGuidePrice_Quantity, relation.PartsCode));
                return;
            }

            foreach(var relation in dealerPartsRetailOrder.DealerRetailOrderDetails.Where(e => e.Price <= 0)) {
                UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.DataEditView_Validation_PartsRetailGuidePrice_Price, relation.PartsCode));
                return;
            }
            if(string.IsNullOrWhiteSpace(dealerPartsRetailOrder.BranchName)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_BranchIdIsNull);
                return;
            }
            if(dealerPartsRetailOrder.RetailOrderType == null || dealerPartsRetailOrder.RetailOrderType == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_RetailOrderType);
                return;
            }
            if(dealerPartsRetailOrder.RetailOrderType == (int)DcsDealerPartsRetailOrderRetailOrderType.二级站) {
                //if(string.IsNullOrWhiteSpace(dealerPartsRetailOrder.SubDealerName)) {
                //    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_DealerPartsRetailOrder_SubDealerNameIsNull);
                //    return;
                //}
                if(string.IsNullOrWhiteSpace(dealerPartsRetailOrder.CustomerPhone)) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_CustomerPhone);
                    return;
                }
                if(!string.IsNullOrWhiteSpace(dealerPartsRetailOrder.CustomerCellPhone))
                    if(CheckCustomerCellPhone(dealerPartsRetailOrder))
                        return;
            }
            if(dealerPartsRetailOrder.RetailOrderType == (int)DcsDealerPartsRetailOrderRetailOrderType.大客户
                || dealerPartsRetailOrder.RetailOrderType == (int)DcsDealerPartsRetailOrderRetailOrderType.社会修理厂
                || dealerPartsRetailOrder.RetailOrderType == (int)DcsDealerPartsRetailOrderRetailOrderType.社会配件经销商) {
                if(string.IsNullOrWhiteSpace(dealerPartsRetailOrder.CustomerPhone)) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_CustomerPhone);
                    return;
                }
                if(!string.IsNullOrWhiteSpace(dealerPartsRetailOrder.CustomerCellPhone))
                    if(CheckCustomerCellPhone(dealerPartsRetailOrder))
                        return;
                if(string.IsNullOrWhiteSpace(dealerPartsRetailOrder.CustomerUnit)) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_CustomerUnit);
                    return;
                }
            }


            if(dealerPartsRetailOrder.RetailOrderType == (int)DcsDealerPartsRetailOrderRetailOrderType.普通客户) {
                if(string.IsNullOrWhiteSpace(dealerPartsRetailOrder.Customer)) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_Customer);
                    return;
                }

                if(string.IsNullOrWhiteSpace(dealerPartsRetailOrder.CustomerCellPhone)) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsRetailOrder_CustomerCellPhoneIsNull);
                    return;
                }
                if(CheckCustomerCellPhone(dealerPartsRetailOrder))
                    return;
            }

            if(!string.IsNullOrEmpty(dealerPartsRetailOrder.CustomerPhone)) {
                if(!Regex.IsMatch(dealerPartsRetailOrder.CustomerPhone, @"^(\d{3,4}-?)?\d{7,8}$")) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_TelephoneFormat);
                    return;
                }
            }
            if(!string.IsNullOrEmpty(dealerPartsRetailOrder.Customer)) {
                string strName = @"^[\u4E00-\u9FA5A-Za-z0-9]{1,20}$";
                Regex regexName = new Regex(strName);
                if(regexName.IsMatch(dealerPartsRetailOrder.Customer) == false) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_CustomerSpecial);
                    return;
                }
            }
            if(!dealerPartsRetailOrder.DealerRetailOrderDetails.Any()) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_DealerPartsRetailOrder_DealerRetailOrderDetailIsNull);
                return;
            }

            ((IEditableObject)dealerPartsRetailOrder).EndEdit();
            if(dealerPartsRetailOrder.EntityState == EntityState.New) {
                if(dealerPartsRetailOrder.Can生成服务站配件零售订单)
                    dealerPartsRetailOrder.生成服务站配件零售订单();
            } else {
                if(dealerPartsRetailOrder.Can修改服务站配件零售订单)
                    dealerPartsRetailOrder.修改服务站配件零售订单();
            }

            if(this.isCustomer && !string.IsNullOrWhiteSpace(dealerPartsRetailOrder.Customer)) {
                var retailOrderCustomer = new RetailOrderCustomer();
                retailOrderCustomer.企业Id = BaseApp.Current.CurrentUserData.EnterpriseId;
                retailOrderCustomer.CustomerName = dealerPartsRetailOrder.Customer;
                retailOrderCustomer.CustomerPhone = dealerPartsRetailOrder.CustomerPhone;
                retailOrderCustomer.CustomerCellPhone = dealerPartsRetailOrder.CustomerCellPhone;
                retailOrderCustomer.Address = dealerPartsRetailOrder.Address;
                retailOrderCustomer.CustomerUnit = dealerPartsRetailOrder.CustomerUnit;
                retailOrderCustomer.Status = (int)DcsBaseDataStatus.有效;
                ((IEditableObject)retailOrderCustomer).EndEdit();
                DomainContext.RetailOrderCustomers.Add(retailOrderCustomer);
            }
            base.OnEditSubmitting();
        }

        private static bool CheckCustomerCellPhone(DealerPartsRetailOrder dealerPartsRetailOrder) {
            string str = @"^\d+$";
            string num = "^([1][3-9][0-9]|199)$";
            Regex regex = new Regex(str);
            Regex regexnum = new Regex(num);
            string cellPhoneNumber = dealerPartsRetailOrder.CustomerCellPhone;
            if(regex.IsMatch(cellPhoneNumber) == false) {
                dealerPartsRetailOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_TelephoneNumber, new[] {
                    "CustomerCellPhone"
                }));
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_TelephoneNumber);
                return true;
            }
            if(cellPhoneNumber.Length == 11) {
                string strCellPhoneNumber = cellPhoneNumber.Substring(0, 3);
                if(regexnum.IsMatch(strCellPhoneNumber) == false) {
                    dealerPartsRetailOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_TelephoneStart, new[] {
                        "CustomerCellPhone"
                    }));
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_TelephoneStart);
                    return true;
                }
            } else {
                dealerPartsRetailOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_TelephoneCount, new[] {
                    "CustomerCellPhone"
                }));
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_TelephoneCount);
                return true;
            }
            return false;
        }
        protected override bool OnRequestCanSubmit()
        {
            //this.ComboBoxBranch.SelectedIndex = 0;//默认分公司
            //this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            return true;
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_DealerPartsRetailOrder;
            }
        }

        public override void SetObjectToEditById(object id) {
            this.branchId = 0; this.partsSalesCategoryId = 0;
            this.retailOrderType = null;
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void DealerPartsRetailOrder_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(dealerPartsRetailOrder == null)
                return;
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效 && v.BranchId == dealerPartsRetailOrder.BranchId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvPartsSalesCategoryNames.Clear();
                foreach(var partsSalesCategory in loadOp.Entities) {
                    this.KvPartsSalesCategoryNames.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name,
                        UserObject = partsSalesCategory
                    });
                }
                this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            }, null);
            //分公司更改时清空清单数据
            if(this.branchId != 0 && this.branchId != dealerPartsRetailOrder.BranchId) {
                dealerPartsRetailOrder.PartsSalesCategoryId = default(int);
                dealerPartsRetailOrder.TotalAmount = default(int);
                var detailList = dealerPartsRetailOrder.DealerRetailOrderDetails.ToList();
                foreach(var item in detailList) {
                    dealerPartsRetailOrder.DealerRetailOrderDetails.Remove(item);
                }
            }
            this.branchId = dealerPartsRetailOrder.BranchId;
        }

        private void PartsSalesCategory_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(dealerPartsRetailOrder == null)
                return;
            //品牌更改时清空清单数据
            if(this.partsSalesCategoryId != 0 && this.partsSalesCategoryId != dealerPartsRetailOrder.PartsSalesCategoryId) {
                dealerPartsRetailOrder.TotalAmount = default(int);
                var detailList = dealerPartsRetailOrder.DealerRetailOrderDetails.ToList();
                foreach(var item in detailList) {
                    dealerPartsRetailOrder.DealerRetailOrderDetails.Remove(item);
                }
            }
            this.partsSalesCategoryId = dealerPartsRetailOrder.PartsSalesCategoryId;
        }

        private void RetailOrderType_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(dealerPartsRetailOrder == null)
                return;
            if(dealerPartsRetailOrder.RetailOrderType == ((int)DcsDealerPartsRetailOrderRetailOrderType.普通客户)
                || dealerPartsRetailOrder.RetailOrderType == ((int)DcsDealerPartsRetailOrderRetailOrderType.大客户)
                || dealerPartsRetailOrder.RetailOrderType == ((int)DcsDealerPartsRetailOrderRetailOrderType.社会修理厂)
                || dealerPartsRetailOrder.RetailOrderType == ((int)DcsDealerPartsRetailOrderRetailOrderType.社会配件经销商)) {
                this.CustomIsEnabled = true;
                //this.ptbCustomer.IsEnabled = true;
                //this.ptbSubDealerName.IsEnabled = false;
            } else if(dealerPartsRetailOrder.RetailOrderType == (int)DcsDealerPartsRetailOrderRetailOrderType.二级站) {
                this.CustomIsEnabled = false;
                //this.ptbCustomer.IsEnabled = false;
                //this.ptbSubDealerName.IsEnabled = true;
            }
            if(dealerPartsRetailOrder.RetailOrderType == (int)DcsDealerPartsRetailOrderRetailOrderType.二级站)
                //零售类型更改时清空主单和清单中数据
                if(this.retailOrderType.HasValue && this.retailOrderType != dealerPartsRetailOrder.RetailOrderType) {
                    dealerPartsRetailOrder.Customer = string.Empty;
                    dealerPartsRetailOrder.SubDealerName = string.Empty;
                    dealerPartsRetailOrder.CustomerCellPhone = string.Empty;
                    dealerPartsRetailOrder.CustomerPhone = string.Empty;
                    dealerPartsRetailOrder.Address = string.Empty;
                    dealerPartsRetailOrder.CustomerUnit = string.Empty;
                    dealerPartsRetailOrder.TotalAmount = default(int);
                    var detailList = dealerPartsRetailOrder.DealerRetailOrderDetails.ToList();
                    foreach(var item in detailList) {
                        dealerPartsRetailOrder.DealerRetailOrderDetails.Remove(item);
                    }
                }
            this.retailOrderType = dealerPartsRetailOrder.RetailOrderType;
        }

        public bool CustomIsEnabled {
            get {
                return this.customIsEnabled;
            }
            set {
                if(this.customIsEnabled == value)
                    return;
                this.customIsEnabled = value;
                this.OnPropertyChanged("CustomIsEnabled");
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e) {
            var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(dealerPartsRetailOrder == null || !this.DealerPartsRetailOrderForEditDataGridView.CommitEdit())
                return;
            if(string.IsNullOrEmpty(dealerPartsRetailOrder.Customer))
                return;
            dealerPartsRetailOrder.Customer = dealerPartsRetailOrder.Customer.Trim().Replace(@"\n", "").Replace(" ", "").Replace(@"\t", "").Replace(@"\r", "");
        }

        private void UIElement_OnKeyDown(object sender, KeyEventArgs e) {
            var rg = new Regex("^[\u4e00-\u9fa5\b]$"); //\b是退格键     
            if(!rg.IsMatch(e.Key.ToString())) {
                e.Handled = true;

            }
        }

    }
}
