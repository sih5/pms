﻿using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class DealerPartsRetailOrderForApproveDataEditView {
        private readonly string[] kvNames = {
            "DealerPartsRetailOrder_RetailOrderType"
        };
        private KeyValueManager keyValueManager;
        public object KvRetailOrderTypes {
            get {
                return this.keyValueManager[this.kvNames[0]];
            }
        }
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        private DataGridViewBase dealerPartsRetailOrderForEditDataGridView;

        public DataGridViewBase DealerPartsRetailOrderForEditDataGridView {
            get {
                if(this.dealerPartsRetailOrderForEditDataGridView == null) {
                    this.dealerPartsRetailOrderForEditDataGridView = DI.GetDataGridView("DealerPartsRetailOrderForAppproveEdit");
                    this.dealerPartsRetailOrderForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.dealerPartsRetailOrderForEditDataGridView;
            }
        }

        public DealerPartsRetailOrderForApproveDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData();
        }
        private void CreateUI() {
            InitializeComponent();
            this.LayoutRoot.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsSalesUIStrings.DataEditView_GroupTitle_DealerPartsRetailOrder, null, () => this.DealerPartsRetailOrderForEditDataGridView);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.LayoutRoot.Children.Add(detailDataEditView);
        }
        private void ImportDataInternal() {

        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsRetailOrderWithDetailsQuery().Where(entity => entity.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();

                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        protected override void OnEditSubmitting() {
            var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(dealerPartsRetailOrder == null)
                return;
            dealerPartsRetailOrder.ValidationErrors.Clear();
            if(string.IsNullOrWhiteSpace(dealerPartsRetailOrder.ApprovalComment)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_ApprovalCommentIsNull);
                return;
            }
            ((IEditableObject)dealerPartsRetailOrder).EndEdit();
            this.DomainContext.Load(this.DomainContext.GetSalesCenterstrategiesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesCategoryId == dealerPartsRetailOrder.PartsSalesCategoryId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null && entity.RetailOrderStrategy.HasValue && !(bool)entity.RetailOrderStrategy) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_RetailOrderStrategy);
                    return;
                }
                if(dealerPartsRetailOrder.Can服务站审批服务站配件零售订单)
                    dealerPartsRetailOrder.服务站审批服务站配件零售订单();
                base.OnEditSubmitting();
            }, null);
        }

        protected override void Reset() {
            var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(dealerPartsRetailOrder != null && this.DomainContext.DealerPartsRetailOrders.Contains(dealerPartsRetailOrder))
                this.DomainContext.DealerPartsRetailOrders.Detach(dealerPartsRetailOrder);
        }

        protected override string Title {
            get {
                return PartsSalesUIStrings.BusinessName_DealerPartsRetailOrderForApprove;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }


    }
}
