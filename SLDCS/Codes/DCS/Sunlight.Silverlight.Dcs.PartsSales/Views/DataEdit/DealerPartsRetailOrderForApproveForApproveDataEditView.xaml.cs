﻿using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class DealerPartsRetailOrderForApproveForApproveDataEditView {
        private readonly string[] kvNames = {
            "DealerPartsRetailOrder_RetailOrderType"
        };
        private KeyValueManager keyValueManager;
        public object KvRetailOrderTypes {
            get {
                return this.keyValueManager[this.kvNames[0]];
            }
        }
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        private DataGridViewBase dealerPartsRetailOrderDetailForAppproveForEditDataGridView;

        public DataGridViewBase DealerPartsRetailOrderDetailForAppproveForEditDataGridView {
            get {
                if(this.dealerPartsRetailOrderDetailForAppproveForEditDataGridView == null) {
                    this.dealerPartsRetailOrderDetailForAppproveForEditDataGridView = DI.GetDataGridView("DealerPartsRetailOrderDetailForAppproveForEdit");
                    this.dealerPartsRetailOrderDetailForAppproveForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.dealerPartsRetailOrderDetailForAppproveForEditDataGridView;
            }
        }

        public DealerPartsRetailOrderForApproveForApproveDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData();
        }

        protected override string Title {
            get {
                return DcsUIStrings.BusinessName_DealerPartsRetailOrderForApprove;
            }
        }

        private void CreateUI() {
            this.grid.Children.Add(this.CreateHorizontalLine(0, 2, 4, 0));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsSalesUIStrings.DataEditView_GroupTitle_DealerPartsRetailOrder, null, () => this.DealerPartsRetailOrderDetailForAppproveForEditDataGridView);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            Grid.SetColumn(detailDataEditView, 2);
            this.LayoutRoot.Children.Add(detailDataEditView);
        }

        private void ImportDataInternal() {

        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsRetailOrderWithDetailsQuery().Where(entity => entity.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();

                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void OnEditSubmitting() {
            var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(dealerPartsRetailOrder == null)
                return;
            if (string.IsNullOrWhiteSpace(dealerPartsRetailOrder.ApprovalComment)) {
                Sunlight.Silverlight.Core.UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_ApprovalComment);
                return;
            }
            dealerPartsRetailOrder.ValidationErrors.Clear();
            ((IEditableObject)dealerPartsRetailOrder).EndEdit();
            if(dealerPartsRetailOrder.Can审批服务站配件零售订单)
                dealerPartsRetailOrder.审批服务站配件零售订单();
            base.OnEditSubmitting();
        }
    }
}
