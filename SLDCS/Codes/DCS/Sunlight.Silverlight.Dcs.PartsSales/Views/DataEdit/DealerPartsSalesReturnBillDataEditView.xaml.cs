﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class DealerPartsSalesReturnBillDataEditView  {
        private DataGridViewBase partsRetailReturnBillDetailForEditDataGridView;

        public DealerPartsSalesReturnBillDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += this.PartsRetailReturnBillDataEditView_DataContextChanged;
        }

        private DataGridViewBase PartsRetailReturnBillDetailForEditDataGridView {
            get {
                if(this.partsRetailReturnBillDetailForEditDataGridView == null) {
                    this.partsRetailReturnBillDetailForEditDataGridView = DI.GetDataGridView("DealerRetailOrderDetailForEdit");
                    this.partsRetailReturnBillDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsRetailReturnBillDetailForEditDataGridView;
            }
        }

        private void PartsRetailReturnBillDataEditView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var partsRetailReturnBill = this.DataContext as DealerPartsSalesReturnBill;
            if(partsRetailReturnBill == null)
                return;
            partsRetailReturnBill.DealerRetailReturnBillDetails.EntityAdded -= this.DealerRetailReturnBillDetails_EntityAdded;
            partsRetailReturnBill.DealerRetailReturnBillDetails.EntityAdded += this.DealerRetailReturnBillDetails_EntityAdded;
            partsRetailReturnBill.DealerRetailReturnBillDetails.EntityRemoved -= this.DealerRetailReturnBillDetails_EntityRemoved;
            partsRetailReturnBill.DealerRetailReturnBillDetails.EntityRemoved += this.DealerRetailReturnBillDetails_EntityRemoved;
        }

        private void PartsRetailReturnBillDetail_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsRetailReturnBillDetail = sender as DealerRetailReturnBillDetail;
            if (partsRetailReturnBillDetail == null)
                return;
            switch (e.PropertyName) {
                case "Price":
                case "Quantity":
                    var partsRetailReturnBill = this.DataContext as DealerPartsSalesReturnBill;
                    if(partsRetailReturnBill == null)
                        return;
                    partsRetailReturnBill.TotalAmount = partsRetailReturnBill.DealerRetailReturnBillDetails.Sum(entity => entity.Quantity * entity.Price);
                    break;
            }
        }

        private void DealerRetailReturnBillDetails_EntityRemoved(object sender, EntityCollectionChangedEventArgs<DealerRetailReturnBillDetail> e) {
            var partsRetailReturnBill = this.DataContext as DealerPartsSalesReturnBill;
            if(partsRetailReturnBill == null)
                return;
            partsRetailReturnBill.TotalAmount = partsRetailReturnBill.DealerRetailReturnBillDetails.Sum(entity => entity.Quantity * entity.Price);
        }

        private void DealerRetailReturnBillDetails_EntityAdded(object sender, EntityCollectionChangedEventArgs<DealerRetailReturnBillDetail> e) {
            e.Entity.PropertyChanged += this.PartsRetailReturnBillDetail_PropertyChanged;
        }

        private void CreateUI() {
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsSalesUIStrings.DataGridView_Title_PartsRetailReturnBillDetail, null, this.PartsRetailReturnBillDetailForEditDataGridView);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);
            var queryWindowPartsOutboundBillDetail = DI.GetQueryWindow("DealerPartsRetailOrder") as DcsMultiSelectQueryWindowBase;
            if(queryWindowPartsOutboundBillDetail != null)
                queryWindowPartsOutboundBillDetail.SelectionDecided += this.QueryWindowPartsOutboundBillDetail_SelectionDecided;
            this.ptbSourceCode.PopupContent = queryWindowPartsOutboundBillDetail;
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPDealerPartsSalesReturnBillDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                foreach(var detail in entity.DealerRetailReturnBillDetails) {
                    detail.PropertyChanged -= this.PartsRetailReturnBillDetail_PropertyChanged;
                    detail.PropertyChanged += this.PartsRetailReturnBillDetail_PropertyChanged;
                }
                this.SetObjectToEdit(entity);
            }, null);
        }
   

        private void QueryWindowPartsOutboundBillDetail_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiSelectQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var partsOutboundBillDetails = queryWindow.SelectedEntities.Cast<DealerRetailOrderDetail>();
            if(partsOutboundBillDetails == null)
                return;
            var partsRetailReturnBill = this.DataContext as DealerPartsSalesReturnBill;
            if(partsRetailReturnBill == null)
                return;
            var details = partsRetailReturnBill.DealerRetailReturnBillDetails.ToArray();
            ((IEditableObject)partsRetailReturnBill).EndEdit();
            foreach(var detail in details) {
                ((IEditableObject)detail).EndEdit();
                detail.ValidationErrors.Clear();
                partsRetailReturnBill.DealerRetailReturnBillDetails.Remove(detail);
            }

            var outboundBillDetails = partsOutboundBillDetails as DealerRetailOrderDetail[] ?? partsOutboundBillDetails.ToArray();
            if(outboundBillDetails.Any() && outboundBillDetails.First().DealerPartsRetailOrder != null) {
                partsRetailReturnBill.SourceId = outboundBillDetails.First().DealerPartsRetailOrder.Id;
                partsRetailReturnBill.SourceCode = outboundBillDetails.First().DealerPartsRetailOrder.Code;
                partsRetailReturnBill.DealerId = outboundBillDetails.First().DealerPartsRetailOrder.DealerId;
                partsRetailReturnBill.DealerCode = outboundBillDetails.First().DealerPartsRetailOrder.DealerCode;
                partsRetailReturnBill.DealerName = outboundBillDetails.First().DealerPartsRetailOrder.DealerName;
                partsRetailReturnBill.SalesCategoryId = outboundBillDetails.First().DealerPartsRetailOrder.PartsSalesCategoryId;
                partsRetailReturnBill.SalesCategoryName = outboundBillDetails.First().DealerPartsRetailOrder.SalesCategoryName;
                partsRetailReturnBill.Customer = outboundBillDetails.First().DealerPartsRetailOrder.Customer;
                partsRetailReturnBill.BranchId = outboundBillDetails.First().DealerPartsRetailOrder.BranchId;
                partsRetailReturnBill.BranchName = outboundBillDetails.First().DealerPartsRetailOrder.BranchName;                
                foreach(var item in outboundBillDetails) {
                    var partsRetailReturnBillDetail = new DealerRetailReturnBillDetail();
                    partsRetailReturnBill.DealerRetailReturnBillDetails.Add(partsRetailReturnBillDetail);
                    partsRetailReturnBillDetail.PartsId = item.PartsId;
                    partsRetailReturnBillDetail.PartsCode = item.PartsCode;
                    partsRetailReturnBillDetail.PartsName = item.PartsName;
                    partsRetailReturnBillDetail.Price = item.Price;
                    partsRetailReturnBillDetail.TraceProperty = item.TraceProperty;
                    partsRetailReturnBillDetail.SIHLabelCode = item.SIHLabelCode;
                }
            }
            partsRetailReturnBill.TotalAmount = partsRetailReturnBill.DealerRetailReturnBillDetails.Select(r => r.Quantity * r.Price).Sum();
            //var details = partsRetailReturnBill.DealerRetailReturnBillDetails.ToArray();
            //((IEditableObject)partsRetailReturnBill).EndEdit();
            //foreach(var detail in details) {
            //    ((IEditableObject)detail).EndEdit();
            //    detail.ValidationErrors.Clear();
            //}
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

      
        protected override string BusinessName {
            get {
                return PartsSalesUIStrings.DataEdit_BusinessName_DealerPartsSalesReturnBill;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.PartsRetailReturnBillDetailForEditDataGridView.CommitEdit())
                return;

            var partsRetailReturnBill = this.DataContext as DealerPartsSalesReturnBill;
            if(partsRetailReturnBill == null)
                return;
            partsRetailReturnBill.ValidationErrors.Clear();

            if(!partsRetailReturnBill.DealerRetailReturnBillDetails.Any()) {
                UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.DataEditView_Validation_PartsRetailReturnBillDetails));
                return;
            }
            foreach(var relation in partsRetailReturnBill.DealerRetailReturnBillDetails.Where(e => e.Quantity <= 0)) {
                UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.DataEditView_Validation_PartsRetailGuidePrice_Quantity, relation.PartsCode));
                return;
            }

            foreach(var relation in partsRetailReturnBill.DealerRetailReturnBillDetails.Where(e => e.Price <= 0)) {
                UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_Price, relation.PartsCode));
                return;
            }
        
            if(partsRetailReturnBill.HasValidationErrors)
                return;
            try {
                if(this.EditState == DataEditState.New) {
                    if(partsRetailReturnBill.Can生成服务站配件零售退货单)
                        partsRetailReturnBill.生成服务站配件零售退货单();
                } else if(partsRetailReturnBill.Can修改服务站配件零售退货单)
                    partsRetailReturnBill.修改服务站配件零售退货单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            ((IEditableObject)partsRetailReturnBill).EndEdit();
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}

