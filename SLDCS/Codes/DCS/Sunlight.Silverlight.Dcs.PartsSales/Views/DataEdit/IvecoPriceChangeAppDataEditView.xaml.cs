﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class IvecoPriceChangeAppDataEditView {
        public IvecoPriceChangeAppDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += IvecoPriceChangeAppDataEditView_DataContextChanged;
        }

        void IvecoPriceChangeAppDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var ivecoPriceChangeApp = this.DataContext as IvecoPriceChangeApp;
            if(ivecoPriceChangeApp == null)
                return;
            SetPropertyChanged();
            ivecoPriceChangeApp.IvecoPriceChangeAppDetails.EntityAdded -= IvecoPriceChangeAppDetails_EntityAdded;
            ivecoPriceChangeApp.IvecoPriceChangeAppDetails.EntityAdded += IvecoPriceChangeAppDetails_EntityAdded;
        }

        private void SetPropertyChanged() {
            var IvecoPriceChangeApp = this.DataContext as IvecoPriceChangeApp;
            if(IvecoPriceChangeApp == null)
                return;
            foreach(var IvecoPriceChangeAppDetail in IvecoPriceChangeApp.IvecoPriceChangeAppDetails) {
                IvecoPriceChangeAppDetail.PropertyChanged -= IvecoPriceChangeAppDetail_PropertyChanged;
                IvecoPriceChangeAppDetail.PropertyChanged += IvecoPriceChangeAppDetail_PropertyChanged;
            }
        }

        private void IvecoPriceChangeAppDetails_EntityAdded(object sender, EntityCollectionChangedEventArgs<IvecoPriceChangeAppDetail> e) {
            SetPropertyChanged();
        }


        private void IvecoPriceChangeAppDetail_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var ivecoPriceChangeAppDetails = sender as IvecoPriceChangeAppDetail;
            if(ivecoPriceChangeAppDetails == null)
                return;
            switch(e.PropertyName) {
                case "IvecoPrice":
                    ivecoPriceChangeAppDetails.IvecoPrice = decimal.Round(ivecoPriceChangeAppDetails.IvecoPrice ?? 0, 2);
                    break;
            }
        }

        private string strFileName;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private RadUpload uploader;
        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        var IvecoPriceChangeApp = this.DataContext as IvecoPriceChangeApp;
                        if(IvecoPriceChangeApp == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.ImportIvecoPriceChangeAppDetailAsync(IvecoPriceChangeApp.PartsSalesCategoryId ?? 0,e.HandlerData.CustomData["Path"].ToString());
                        this.excelServiceClient.ImportIvecoPriceChangeAppDetailCompleted -= ExcelServiceClient_ImportIvecoPriceChangeAppDetailCompleted;
                        this.excelServiceClient.ImportIvecoPriceChangeAppDetailCompleted += ExcelServiceClient_ImportIvecoPriceChangeAppDetailCompleted;
                    };
                }
                return this.uploader;
            }
        }

        /// <summary>
        /// 文件上传成功后，调用该方法进行上传后的处理。不支持异步调用
        /// <param>上传文件名 <name>fileName</name> </param>
        /// </summary>
        protected Action<string> UploadFileSuccessedProcessing;

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //取当前登录用户的HashCode来标记上传文件的唯一性
                //this.UploadFileSuccessedProcessing.Invoke(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }

        private void ExcelServiceClient_ImportIvecoPriceChangeAppDetailCompleted(object sender, ImportIvecoPriceChangeAppDetailCompletedEventArgs e) {
            var IvecoPriceChangeApp = this.DataContext as IvecoPriceChangeApp;
            if(IvecoPriceChangeApp == null)
                return;
            foreach(var detail in IvecoPriceChangeApp.IvecoPriceChangeAppDetails) {
                IvecoPriceChangeApp.IvecoPriceChangeAppDetails.Remove(detail);
            }
            if(e.rightData != null && e.rightData.Length > 0) {
                foreach(var data in e.rightData) {
                    IvecoPriceChangeApp.IvecoPriceChangeAppDetails.Add(new IvecoPriceChangeAppDetail {
                        SparePartId = data.SparePartId,
                        SparePartCode = data.SparePartCode,
                        SparePartName = data.SparePartName,
                        SalesPrice = decimal.Round(data.SalesPrice ?? 0, 2),
                        IvecoPrice = decimal.Round(data.IvecoPrice, 2),
                    });
                }
            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowAlertMessage(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
        }

        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage( PartsSalesUIStrings.DataEdit_Validation_Import_CloseFile);
                    e.Handled = true;
                }
            }
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }
        protected override string BusinessName {
            get {
                return PartsSalesUIStrings.DataEdit_BusinessName_IvecoPriceChangeApp;
            }
        }
        private DataGridViewBase ivecoPriceChangeAppForEditDataGridView;

        private DataGridViewBase IvecoPriceChangeAppForEditDataGridView {
            get {
                if(this.ivecoPriceChangeAppForEditDataGridView == null) {
                    this.ivecoPriceChangeAppForEditDataGridView = DI.GetDataGridView("IvecoPriceChangeAppForEdit");
                    this.ivecoPriceChangeAppForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.ivecoPriceChangeAppForEditDataGridView;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.IvecoPriceChangeAppForEditDataGridView.CommitEdit())
                return;
            var IvecoPriceChangeApp = this.DataContext as IvecoPriceChangeApp;
            if(IvecoPriceChangeApp == null)
                return;
            IvecoPriceChangeApp.ValidationErrors.Clear();
            foreach(var item in IvecoPriceChangeApp.IvecoPriceChangeAppDetails)
                item.ValidationErrors.Clear();
            foreach(var relation in IvecoPriceChangeApp.IvecoPriceChangeAppDetails.Where(e => e.IvecoPrice <= 0)) {
                UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.DataEditView_Validation_IvecoPriceChangeAppDetail_SalesPrice, relation.SparePartCode));
                return;
            }
            foreach(var errorSpareParts in IvecoPriceChangeApp.IvecoPriceChangeAppDetails.GroupBy(d => new {
                d.SparePartCode,
                d.SparePartName
            }).Where(d => d.Count() > 1)) {
                UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.DataEditView_Validation_PartsSalesPriceChangeDetail_DoubleSparePart, errorSpareParts.Key.SparePartName));
                return;
            }
            if(IvecoPriceChangeApp.HasValidationErrors || IvecoPriceChangeApp.IvecoPriceChangeAppDetails.Any(e => e.HasValidationErrors))
                return;
            ((IEditableObject)IvecoPriceChangeApp).EndEdit();
            try {
                if(this.EditState == DataEditState.Edit) {
                    if(IvecoPriceChangeApp.Can修改依维柯销售价变更申请)
                        IvecoPriceChangeApp.修改依维柯销售价变更申请();
                } else {
                    if(IvecoPriceChangeApp.Can新增依维柯销售价变更申请)
                        IvecoPriceChangeApp.新增依维柯销售价变更申请();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        private ICommand exportFileCommand;
        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private const string EXPORT_DATA_FILE_NAME = "导出依维柯价格变更申请单清单模板.xlsx";

        private void InitializeCommand() {
            this.exportFileCommand = new DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                 Name=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                 Name=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName,
                                                IsRequired = true
                                            },
                                            //new ImportTemplateColumn {
                                            //    Name = "经销商价",
                                            //    IsRequired = true
                                            //},
                                            new ImportTemplateColumn {
                                                Name = PartsSalesUIStrings.DataEdit_Text_IvecoPriceChangeAppDetail_IvecoPrice,
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }

        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }
        private void CreateUI() {
            this.LayoutRoot.Children.Add(DI.GetDataEditPanel("IvecoPriceChangeApp"));

            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsSalesUIStrings.DetailPanel_Title_SparePartDetails, null, () => this.IvecoPriceChangeAppForEditDataGridView);
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.DataEditPanel_Action_ImportNew,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                Command = new DelegateCommand(() => this.Uploader.ShowFileDialog())
            });
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.DataEditPanel_Action_Export,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                Command = this.ExportTemplateCommand
            });
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.LayoutRoot.Children.Add(detailDataEditView);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetIvecoPriceChangeAppWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

    }
}
