﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class OrderApproveWeekdayDataEditView {
        private readonly string[] kvNames = {
            "OrderAppDate"
        };
        private KeyValueManager keyValueManager;
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        private ObservableCollection<KeyValuePair> kvPartsSalesCategories;
        private ObservableCollection<KeyValuePair> kvProvinceNames;

        public ObservableCollection<KeyValuePair> KvPartsSalesCategories {
            get {
                return this.kvPartsSalesCategories ?? (this.kvPartsSalesCategories = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvProvinceNames {
            get {
                return this.kvProvinceNames ?? (this.kvProvinceNames = new ObservableCollection<KeyValuePair>());
            }
        }

        public object KvWeeks {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public OrderApproveWeekdayDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            this.KvPartsSalesCategories.Clear();
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(entity => entity.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && entity.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var partsSaledCategories = loadOp.Entities;
                foreach(var partsSalesCategory in partsSaledCategories) {
                    KvPartsSalesCategories.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
                }
            }, null);
            this.DomainContext.Load(this.DomainContext.GetTiledRegionsQuery().Where(ex => ex.RegionName != null).OrderBy(ex => ex.ProvinceName), LoadBehavior.RefreshCurrent, loadOp1 => {
                if(loadOp1.HasError)
                    return;
                KvProvinceNames.Clear();
                foreach(var item in loadOp1.Entities) {
                    if(this.KvProvinceNames.All(r => r.Value != item.ProvinceName)) {
                        KvProvinceNames.Add(new KeyValuePair {
                            Key = item.Id,
                            Value = item.ProvinceName
                        });
                    }
                }
            }, null);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetOrderApproveWeekdaysQuery().Where(r => r.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.SetObjectToEdit(entity);
            }, null);
        }

        protected override string BusinessName {
            get {
                return PartsSalesUIStrings.DataEdit_BusinessName_OrderApproveWeekday;
            }
        }

        protected override void OnEditSubmitting() {
            var orderApproveWeekday = this.DataContext as OrderApproveWeekday;
            if(orderApproveWeekday == null)
                return;
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }


    }
}
