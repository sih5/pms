﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class OrderApproveWeekdayForImportDataEditView {

        private ICommand exportFileCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出储备订单自动审核日与省份关系模板.xlsx";
        private DataGridViewBase orderApproveWeekdayForImport;
        protected DataGridViewBase OrderApproveWeekdayForImport {
            get {
                if(this.orderApproveWeekdayForImport == null) {
                    this.orderApproveWeekdayForImport = DI.GetDataGridView("OrderApproveWeekdayForImport");
                    this.orderApproveWeekdayForImport.DataContext = this.DataContext;
                }
                return this.orderApproveWeekdayForImport;
            }
        }
        public OrderApproveWeekdayForImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(CreateUI);
        }

        protected override string Title {
            get {
                return PartsSalesUIStrings.DataEdit_Title_OrderApproveWeekdayForImport;
            }
        }

        private void CreateUI() {
            this.ShowSaveButton();
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = PartsSalesUIStrings.DataEdit_Title_OrderApproveWeekdayForImportList,
                Content = this.OrderApproveWeekdayForImport
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.ImportOrderapproveweekdayAsync(fileName);
            this.ExcelServiceClient.ImportOrderapproveweekdayCompleted -= ExcelServiceClient_ImportOrderapproveweekdayCompleted;
            this.ExcelServiceClient.ImportOrderapproveweekdayCompleted += ExcelServiceClient_ImportOrderapproveweekdayCompleted;
        }

        void ExcelServiceClient_ImportOrderapproveweekdayCompleted(object sender, ImportOrderapproveweekdayCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!HasImportingError) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Notification_CompletedImport, 10);
            } else {
                if(!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if(!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesCategory_Name,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsSalesUIStrings.DataEdit_Text_OrderApproveWeekday_Province,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsSalesUIStrings.DataEdit_Text_OrderApproveWeekday_ApproveDate,
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

    }
}