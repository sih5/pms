﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using System.Linq;
using System;
using System.ServiceModel.DomainServices.Client;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class OverstockPartsRecommendBillDataEditView {
        private ObservableCollection<OverstockPartsRecommendBill> virtualOverstockPartsRecommendBills;
        public ObservableCollection<OverstockPartsRecommendBill> VirtualOverstockPartsRecommendBills {
            get {
                return this.virtualOverstockPartsRecommendBills ?? (this.virtualOverstockPartsRecommendBills = new ObservableCollection<OverstockPartsRecommendBill>());
            }
        }
        public OverstockPartsRecommendBillDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.Register("", null, this.DataGridView);
            detailDataEditView.SetValue(Grid.RowProperty, 1);
            this.LayoutRoot.Children.Add(detailDataEditView);
        }
        protected override string BusinessName {
            get {
                return "积压件推荐明细提交";
            }
        }

        private DataGridViewBase dataGridView;
        private DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("OverstockPartsRecommendBillSubmit");
                    this.dataGridView.DomainContext = this.DomainContext;
                    this.dataGridView.DataContext = this;
                }
                return this.dataGridView;
            }
        }

        public override void SetObjectToEditById(object entity) {
            if(this.DomainContext == null) {
                this.Initializer.Register(() => this.LoadEntityToEdit(entity));
            } else
                this.LoadEntityToEdit(entity);
        }

        private void LoadEntityToEdit(object entitys) {
            var results = (OverstockPartsRecommendBill[])entitys;
            var ids = results.Select(o => o.Id).ToArray();
            this.virtualOverstockPartsRecommendBills.Clear();
            this.DomainContext.Load(this.DomainContext.GetOverstockPartsRecommendBillsIdsQuery(ids), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                foreach(var item in loadOp.Entities) {
                    var result = results.FirstOrDefault(o => o.Id == item.Id);
                    item.Price = result.Price;
                    item.DiscountRate = 0.7d;
                    item.OverstockPartsAmount = result.OverstockPartsAmount;
                    virtualOverstockPartsRecommendBills.Add(item);
                }
            }, null);
        }

        protected override void OnEditCancelled() {
            base.OnEditCancelled();
            this.virtualOverstockPartsRecommendBills.Clear();
            this.dataGridView = null;
        }
        protected override bool OnRequestCanSubmit() {
            return true;
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            DomainContext.生成积压件平台(VirtualOverstockPartsRecommendBills.ToArray(), submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    DomainContext.RejectChanges();
                    return;
                }
                this.NotifyEditSubmitted();
                this.OnCustomEditSubmitted();
                UIHelper.ShowNotification(notifyMessage);
            }, null);
        }
        public void OnCustomEditSubmitted() {
            this.DataContext = null;
        }

        public new event EventHandler EditSubmitted;
        private void NotifyEditSubmitted() {
            var handler = this.EditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        protected override void OnEditSubmitting() {
            if(!this.DataGridView.CommitEdit())
                return;
            foreach(var item in virtualOverstockPartsRecommendBills) {
                if((item.DiscountRate ?? 0) <= 0) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_OverstockPartsRecommendBill_Validation_DisCountZero);
                    return;
                }
                if((item.DiscountRate ?? 0) > 0.7d) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_OverstockPartsRecommendBill_Validation_DisCount);
                    return;
                }
            }
            foreach(var virtualOverstockPartsRecommendBill in this.virtualOverstockPartsRecommendBills) {
                virtualOverstockPartsRecommendBill.ValidationErrors.Clear();
            }
            if(this.DomainContext.IsBusy)
                return;
            ExecuteSerivcesMethod(PartsSalesUIStrings.DataManagementView_Notification_SubmitSuccess2);
        }
    }
}
