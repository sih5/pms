﻿using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using System;
using System.Text;
using Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit
{
    public partial class PartsPackingPropertyAppApproveDataEditView
    {
        public PartsPackingPropertyAppApproveDataEditView()
        {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
        protected override string Title
        {
            get
            {
                return PartsSalesUIStrings.DataEdit_Title_PartsPackingPropertyAppApprove;
            }
        }
        protected virtual void CreateUI()
        {
            var dealerPanel = DI.GetDataEditPanel("PartsPackingPropertyAppApprove");
            this.Root.Children.Add(dealerPanel);
            this.mainPackingType.Children.Add(this.MainPackingTypePanel);
            this.firPackingType.Children.Add(this.FirPackingTypePanel);
            this.secPackingType.Children.Add(this.SecPackingTypePanel);
            this.thidPackingType.Children.Add(this.ThidPackingType);

            this.RegisterButton(new ButtonItem {
                Command = new DelegateCommand(this.RejectCurrentData),
                Title=PartsSalesUIStrings.Action_Title_Reject, 
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative)
            });
        }

        private void RejectCurrentData() {
            var partsPackingPropertyApp = this.DataContext as PartsPackingPropertyApp;
            if (partsPackingPropertyApp == null)
                return;
            if (string.IsNullOrEmpty(partsPackingPropertyApp.ApprovalComment)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_ApprovalComment);
                return;
            }
            ((IEditableObject)partsPackingPropertyApp).EndEdit();
            try {
                if (partsPackingPropertyApp.Can驳回包装属性申请单)
                    partsPackingPropertyApp.驳回包装属性申请单();
            } catch (ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        private PartsPackingPropertyAppMainApproveDataEditPanel mainPackingTypePanel;
        private PartsPackingPropertyAppMainApproveDataEditPanel MainPackingTypePanel
        {
            get
            {
                return this.mainPackingTypePanel ?? (this.mainPackingTypePanel = (PartsPackingPropertyAppMainApproveDataEditPanel)DI.GetDataEditPanel("PartsPackingPropertyAppMainApprove"));
            }
        }

        private PartsPackingPropertyAppFirApproveDataEditPanel firPackingTypePanel;
        private PartsPackingPropertyAppFirApproveDataEditPanel FirPackingTypePanel
        {
            get
            {
                return this.firPackingTypePanel ?? (this.firPackingTypePanel = (PartsPackingPropertyAppFirApproveDataEditPanel)DI.GetDataEditPanel("PartsPackingPropertyAppFirApprove"));
            }
        }

        private PartsPackingPropertyAppSecApproveDataEditPanel secPackingTypePanel;

        private PartsPackingPropertyAppSecApproveDataEditPanel SecPackingTypePanel
        {
            get
            {
                return this.secPackingTypePanel ?? (this.secPackingTypePanel = (PartsPackingPropertyAppSecApproveDataEditPanel)DI.GetDataEditPanel("PartsPackingPropertyAppSecApprove"));
            }
        }
        private PartsPackingPropertyAppThidApproveDataEditPanel thidPackingTypePanel;
        private PartsPackingPropertyAppThidApproveDataEditPanel ThidPackingType
        {
            get
            {
                return this.thidPackingTypePanel ?? (this.thidPackingTypePanel = (PartsPackingPropertyAppThidApproveDataEditPanel)DI.GetDataEditPanel("PartsPackingPropertyAppThidApprove"));
            }
        }
        protected override bool OnRequestCanSubmit()
        {
            return true;
        }
        protected override void OnEditSubmitting()
        {
            var partsPacking = this.DataContext as PartsPackingPropertyApp;
            if (partsPacking == null)
            {
                return;
            }

            if ((!partsPacking.ApprovalId.Equals(1)) && (!partsPacking.ApprovalId.Equals(0)))
            {
                partsPacking.ApprovalId = 1;
            }
            ((IEditableObject)partsPacking).EndEdit();
            try
            {
                if (partsPacking.Can审核包装属性申请单)
                    partsPacking.审核包装属性申请单();
            }
            catch (ValidationException ex)
            {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }
        public override void SetObjectToEditById(object id)
        {
            if (this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private void LoadEntityToEdit(int id)
        {
            this.DomainContext.Load(this.DomainContext.GetPartsPackingPropertyAppsQuery().Where(r => r.Id == id), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                {
                    if (!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if (entity != null)
                {
                    entity.ApprovalId = 1;
                    //查询清单
                    this.DomainContext.Load(this.DomainContext.GetPartsPackingPropAppDetailsQuery().Where(r => r.PartsPackingPropAppId == id), LoadBehavior.RefreshCurrent, details =>
                    {
                        if (details.HasError)
                        {
                            if (!details.IsErrorHandled)
                                details.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(details);
                            return;
                        }
                        var detailsList = details.Entities.ToArray();
                        if (detailsList.Count() > 0)
                        {
                            for (int i = 0; i < detailsList.Count(); i++)
                            {
                                if ((int)DcsPackingUnitType.一级包装 == detailsList[i].PackingType)
                                {
                                    entity.FirId = detailsList[i].Id;
                                    entity.FirPackingType = detailsList[i].PackingType;
                                    entity.FirMeasureUnit = detailsList[i].MeasureUnit;
                                    entity.FirPackingCoefficient = detailsList[i].PackingCoefficient;
                                    entity.FirPackingMaterial = detailsList[i].PackingMaterial;
                                    entity.FirVolume = detailsList[i].Volume;
                                    entity.FirWeight = detailsList[i].Weight;
                                    entity.FirLength = detailsList[i].Length;
                                    entity.FirWidth = detailsList[i].Width;
                                    entity.FirHeight = detailsList[i].Height;
                                    entity.FirPackingMaterialName = detailsList[i].PackingMaterialName;
                                    entity.FirIsBoxStandardPrint = detailsList[i].IsBoxStandardPrint;
                                    entity.FirIsPrintPartStandard = detailsList[i].IsPrintPartStandard;
                                }
                                else if ((int)DcsPackingUnitType.二级包装 == detailsList[i].PackingType)
                                {
                                    entity.SecId = detailsList[i].Id;
                                    entity.SecPackingType = detailsList[i].PackingType;
                                    entity.SecMeasureUnit = detailsList[i].MeasureUnit;
                                    entity.SecPackingCoefficient = detailsList[i].PackingCoefficient;
                                    entity.SecPackingMaterial = detailsList[i].PackingMaterial;
                                    entity.SecVolume = detailsList[i].Volume;
                                    entity.SecWeight = detailsList[i].Weight;
                                    entity.SecLength = detailsList[i].Length;
                                    entity.SecWidth = detailsList[i].Width;
                                    entity.SecHeight = detailsList[i].Height;
                                    entity.SecPackingMaterialName = detailsList[i].PackingMaterialName;
                                    entity.SecIsBoxStandardPrint = detailsList[i].IsBoxStandardPrint;
                                    entity.SecIsPrintPartStandard = detailsList[i].IsPrintPartStandard;
                                }
                                else if ((int)DcsPackingUnitType.三级包装 == detailsList[i].PackingType)
                                {
                                    entity.ThidId = detailsList[i].Id;
                                    entity.ThidPackingType = detailsList[i].PackingType;
                                    entity.ThidMeasureUnit = detailsList[i].MeasureUnit;
                                    entity.ThidPackingCoefficient = detailsList[i].PackingCoefficient;
                                    entity.ThidPackingMaterial = detailsList[i].PackingMaterial;
                                    entity.ThidVolume = detailsList[i].Volume;
                                    entity.ThidWeight = detailsList[i].Weight;
                                    entity.ThidLength = detailsList[i].Length;
                                    entity.ThidWidth = detailsList[i].Width;
                                    entity.ThidHeight = detailsList[i].Height;
                                    entity.ThidPackingMaterialName = detailsList[i].PackingMaterialName;
                                    entity.ThidIsBoxStandardPrint = detailsList[i].IsBoxStandardPrint;
                                    entity.ThidIsPrintPartStandard = detailsList[i].IsPrintPartStandard;
                                }
                            }
                        }
                    }, null);

                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
    }
}
