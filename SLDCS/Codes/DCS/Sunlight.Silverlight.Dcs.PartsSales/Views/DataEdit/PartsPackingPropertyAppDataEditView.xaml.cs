﻿using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsPackingPropertyAppDataEditView {
        private readonly DcsDomainContext queryDomainContext = new DcsDomainContext();
        public PartsPackingPropertyAppDataEditView() {
            InitializeComponent();
            this.CreateUI();

        }
        protected override string Title {
            get {
                return PartsSalesUIStrings.DataEdit_Title_PartsPackingPropertyEdit;
            }
        }
        private PartsPackingPropertyAppDataEditPanel dataEditPanel;

        public PartsPackingPropertyAppDataEditPanel DataEditPanel {
            get {
                if(this.dataEditPanel == null) {
                    this.dataEditPanel = (PartsPackingPropertyAppDataEditPanel)DI.GetDataEditPanel("PartsPackingPropertyApp");
                    this.dataEditPanel.QueryDomainContext = this.queryDomainContext;
                }
                return this.dataEditPanel;
            }
        }

        protected virtual void CreateUI() {
            this.Root.Children.Add(this.DataEditPanel);
            this.mainPackingType.Children.Add(this.MainPackingTypePanel);
            this.firPackingType.Children.Add(this.FirPackingTypePanel);
            this.secPackingType.Children.Add(this.SecPackingTypePanel);
            this.thidPackingType.Children.Add(this.ThidPackingTypePanel);
        }
        private PartsPackingPropertyAppMainPackingTypeDataEditPanel mainPackingTypePanel;
        private PartsPackingPropertyAppMainPackingTypeDataEditPanel MainPackingTypePanel {
            get {
                return this.mainPackingTypePanel ?? (this.mainPackingTypePanel = (PartsPackingPropertyAppMainPackingTypeDataEditPanel)DI.GetDataEditPanel("PartsPackingPropertyAppMainPackingType"));
            }
        }
        private PartsPackingPropertyAppFirPackingTypeDataEditPanel firPackingTypePanel;
        private PartsPackingPropertyAppFirPackingTypeDataEditPanel FirPackingTypePanel {
            get {
                if(this.firPackingTypePanel == null) {
                    this.firPackingTypePanel = (PartsPackingPropertyAppFirPackingTypeDataEditPanel)DI.GetDataEditPanel("PartsPackingPropertyAppFirPackingType");
                    this.firPackingTypePanel.QueryDomainContext = this.queryDomainContext;
                }
                return this.firPackingTypePanel;
            }
        }
        private PartsPackingPropertyAppSecPackingTypeDataEditPanel secPackingTypePanel;

        private PartsPackingPropertyAppSecPackingTypeDataEditPanel SecPackingTypePanel {
            get {
                if(this.secPackingTypePanel == null) {
                    this.secPackingTypePanel = (PartsPackingPropertyAppSecPackingTypeDataEditPanel)DI.GetDataEditPanel("PartsPackingPropertyAppSecPackingType");
                    this.secPackingTypePanel.QueryDomainContext = this.queryDomainContext;
                }
                return this.secPackingTypePanel;
            }
        }
        private PartsPackingPropertyAppThidPackingTypeDataEditPanel thidPackingTypePanel;
        private PartsPackingPropertyAppThidPackingTypeDataEditPanel ThidPackingTypePanel {
            get {
                if(this.thidPackingTypePanel == null) {
                    this.thidPackingTypePanel = (PartsPackingPropertyAppThidPackingTypeDataEditPanel)DI.GetDataEditPanel("PartsPackingPropertyAppThidPackingType");
                    this.thidPackingTypePanel.QueryDomainContext = this.queryDomainContext;
                }
                return this.thidPackingTypePanel;
            }
        }
        protected override void Reset() {
            base.Reset();
            this.dataEditPanel = null;
            this.mainPackingTypePanel = null;
            this.firPackingTypePanel = null;
            this.secPackingTypePanel = null;
            this.thidPackingTypePanel = null;
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }
        protected override void OnEditSubmitting() {
            var partsPacking = this.DataContext as PartsPackingPropertyApp;
            if(partsPacking == null) {
                return;
            }
            if(null == partsPacking.PartsSalesCategoryId) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_CustomerDirectSpareList);
                return;
            }
            if(0 == partsPacking.SparePartId) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_SparePart);
                return;
            }
            if(null == partsPacking.MainPackingType) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_MainPackingType);
                return;
            }
            if(!partsPacking.FirIsBoxStandardPrint.HasValue) {
                partsPacking.FirIsBoxStandardPrint = 0;
            }
            if(!partsPacking.FirIsPrintPartStandard.HasValue) {
                partsPacking.FirIsPrintPartStandard = 0;
            }
            if(!partsPacking.SecIsBoxStandardPrint.HasValue) {
                partsPacking.SecIsBoxStandardPrint = 0;
            }
            if(!partsPacking.SecIsPrintPartStandard.HasValue) {
                partsPacking.SecIsPrintPartStandard = 0;
            }
            if(!partsPacking.ThidIsBoxStandardPrint.HasValue) {
                partsPacking.ThidIsBoxStandardPrint = 0;
            }
            if(!partsPacking.ThidIsPrintPartStandard.HasValue) {
                partsPacking.ThidIsPrintPartStandard = 0;
            }
            if(partsPacking.FirIsPrintPartStandard == 0 && partsPacking.FirIsBoxStandardPrint == 0 && partsPacking.SecIsPrintPartStandard == 0 && partsPacking.SecIsBoxStandardPrint == 0 && partsPacking.ThidIsPrintPartStandard == 0 && partsPacking.ThidIsBoxStandardPrint == 0) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_IsPrint);
                return;
            }
            if((partsPacking.SecIsPrintPartStandard == 1 || partsPacking.SecIsBoxStandardPrint == 1) && (null == partsPacking.SecMeasureUnit || "".Equals(partsPacking.SecMeasureUnit) || partsPacking.SecPackingCoefficient <= 0)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_Sec);
                return;
            }
            if((partsPacking.ThidIsPrintPartStandard == 1 || partsPacking.ThidIsBoxStandardPrint == 1) && (null == partsPacking.ThidMeasureUnit || "".Equals(partsPacking.ThidMeasureUnit) || partsPacking.ThidPackingCoefficient <= 0)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_Thid);
                return;
            }
            if(null == partsPacking.FirMeasureUnit || "".Equals(partsPacking.FirMeasureUnit) || partsPacking.FirPackingCoefficient <= 0) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_Fir);
                return;
            }
            if(null == partsPacking.FirMeasureUnit) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_FirMeasureUnit);
                return;
            }
            if(0 >= partsPacking.FirPackingCoefficient) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_FirPackingCoefficient);
                return;
            }


            if((int)DcsPackingUnitType.二级包装 == partsPacking.MainPackingType) {
                if(null == partsPacking.SecMeasureUnit || "".Equals(partsPacking.SecMeasureUnit) || partsPacking.SecPackingCoefficient <= 0) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_Sec);
                    return;
                }
            }
            if((int)DcsPackingUnitType.三级包装 == partsPacking.MainPackingType) {
                if(null == partsPacking.SecMeasureUnit || "".Equals(partsPacking.SecMeasureUnit) || partsPacking.ThidPackingCoefficient <= 0) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_Thid);
                    return;
                }
            }
            if(partsPacking.SecPackingCoefficient > 0) {
                if(null == partsPacking.SecMeasureUnit) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_SecMeasureUnit);
                    return;
                }
                if(0 >= partsPacking.SecPackingCoefficient) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_SecPackingCoefficient);
                    return;
                }
                if((0 != partsPacking.SecPackingCoefficient % partsPacking.FirPackingCoefficient)) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_SecPackingCoefficientMore);
                    return;
                }
            }
            if(partsPacking.ThidPackingCoefficient > 0) {
                if(null == partsPacking.ThidMeasureUnit) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_ThidMeasureUnit);
                    return;
                }
                if(0 >= partsPacking.ThidPackingCoefficient) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_ThidPackingCoefficient);
                    return;
                }
                if((0 != partsPacking.ThidPackingCoefficient % partsPacking.SecPackingCoefficient) || partsPacking.ThidPackingCoefficient == partsPacking.SecPackingCoefficient) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_ThidPackingCoefficientMore);
                    return;
                }
                if((0 != partsPacking.ThidPackingCoefficient % partsPacking.FirPackingCoefficient) || partsPacking.ThidPackingCoefficient == partsPacking.FirPackingCoefficient) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_FirPackingCoefficientMore);
                    return;
                }
            }
            if((int)DcsPackingUnitType.一级包装 == partsPacking.MainPackingType) {
                partsPacking.MInSalesAmount = partsPacking.FirPackingCoefficient.Value;
            } else if((int)DcsPackingUnitType.二级包装 == partsPacking.MainPackingType) {
                partsPacking.MInSalesAmount = partsPacking.SecPackingCoefficient.Value;
            } else {
                partsPacking.MInSalesAmount = partsPacking.ThidPackingCoefficient.Value;
            }
            partsPacking.ValidationErrors.Clear();
            if(0 == partsPacking.Id) {
                partsPacking.新增包装属性申请单();
            } else {
                partsPacking.修改包装属性申请单();
            }

            base.OnEditSubmitting();
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.getPartsPackingPropertyAppsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity != null) {
                    //查询清单
                    this.DomainContext.Load(this.DomainContext.GetPartsPackingPropAppDetailsQuery().Where(r => r.PartsPackingPropAppId == id), LoadBehavior.RefreshCurrent, details => {
                        if(details.HasError) {
                            if(!details.IsErrorHandled)
                                details.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(details);
                            return;
                        }
                        var detailsList = details.Entities.ToArray();
                        if(detailsList.Any()) {
                            for(int i = 0; i < detailsList.Count(); i++) {
                                if((int)DcsPackingUnitType.一级包装 == detailsList[i].PackingType) {
                                    entity.FirId = detailsList[i].Id;
                                    entity.FirPackingType = detailsList[i].PackingType;
                                    entity.FirMeasureUnit = detailsList[i].MeasureUnit;
                                    entity.FirPackingCoefficient = detailsList[i].PackingCoefficient;
                                    entity.FirPackingMaterial = detailsList[i].PackingMaterial;
                                    entity.FirVolume = detailsList[i].Volume;
                                    entity.FirWeight = detailsList[i].Weight;
                                    entity.FirLength = detailsList[i].Length;
                                    entity.FirWidth = detailsList[i].Width;
                                    entity.FirHeight = detailsList[i].Height;
                                    entity.FirPackingMaterialName = detailsList[i].PackingMaterialName;
                                    entity.FirIsBoxStandardPrint = detailsList[i].IsBoxStandardPrint == null ? 0 : detailsList[i].IsBoxStandardPrint;
                                    entity.FirIsPrintPartStandard = detailsList[i].IsPrintPartStandard == null ? 0 : detailsList[i].IsPrintPartStandard;
                                } else if((int)DcsPackingUnitType.二级包装 == detailsList[i].PackingType) {
                                    entity.SecId = detailsList[i].Id;
                                    entity.SecPackingType = detailsList[i].PackingType;
                                    entity.SecMeasureUnit = detailsList[i].MeasureUnit;
                                    entity.SecPackingCoefficient = detailsList[i].PackingCoefficient;
                                    entity.SecPackingMaterial = detailsList[i].PackingMaterial;
                                    entity.SecVolume = detailsList[i].Volume;
                                    entity.SecWeight = detailsList[i].Weight;
                                    entity.SecLength = detailsList[i].Length;
                                    entity.SecWidth = detailsList[i].Width;
                                    entity.SecHeight = detailsList[i].Height;
                                    entity.SecPackingMaterialName = detailsList[i].PackingMaterialName;
                                    entity.SecIsBoxStandardPrint = detailsList[i].IsBoxStandardPrint == null ? 0 : detailsList[i].IsBoxStandardPrint;
                                    entity.SecIsPrintPartStandard = detailsList[i].IsPrintPartStandard == null ? 0 : detailsList[i].IsPrintPartStandard;
                                } else if((int)DcsPackingUnitType.三级包装 == detailsList[i].PackingType) {
                                    entity.ThidId = detailsList[i].Id;
                                    entity.ThidPackingType = detailsList[i].PackingType;
                                    entity.ThidMeasureUnit = detailsList[i].MeasureUnit;
                                    entity.ThidPackingCoefficient = detailsList[i].PackingCoefficient;
                                    entity.ThidPackingMaterial = detailsList[i].PackingMaterial;
                                    entity.ThidVolume = detailsList[i].Volume;
                                    entity.ThidWeight = detailsList[i].Weight;
                                    entity.ThidLength = detailsList[i].Length;
                                    entity.ThidWidth = detailsList[i].Width;
                                    entity.ThidHeight = detailsList[i].Height;
                                    entity.ThidPackingMaterialName = detailsList[i].PackingMaterialName;
                                    entity.ThidIsBoxStandardPrint = detailsList[i].IsBoxStandardPrint == null ? 0 : detailsList[i].IsBoxStandardPrint;
                                    entity.ThidIsPrintPartStandard = detailsList[i].IsPrintPartStandard == null ? 0 : detailsList[i].IsPrintPartStandard;
                                }
                            }
                        }
                        this.SetObjectToEdit(entity);
                    }, null);

                }
            }, null);
        }
    }
}
