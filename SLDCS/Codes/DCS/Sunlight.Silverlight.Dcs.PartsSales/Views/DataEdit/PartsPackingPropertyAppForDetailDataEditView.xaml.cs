﻿using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.PartsSales.Panels.Detail;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsPackingPropertyAppForDetailDataEditView {
        public PartsPackingPropertyAppForDetailDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
        protected virtual void CreateUI() {
            var dd = DI.GetDetailPanel("PartsPackingPropertyApp");
            this.Root.Children.Add(dd);
            this.mainPackingType.Children.Add(this.MainPackingTypePanel);
            this.firPackingType.Children.Add(this.FirPackingTypePanel);
            this.secPackingType.Children.Add(this.SecPackingTypePanel);
            this.thidPackingType.Children.Add(this.ThidPackingTypePanel);
        }
        private PartsPackingPropertyAppMainPackingTypeDetailPanel mainPackingTypePanel;
        private PartsPackingPropertyAppMainPackingTypeDetailPanel MainPackingTypePanel {
            get {
                return this.mainPackingTypePanel ?? (this.mainPackingTypePanel = (PartsPackingPropertyAppMainPackingTypeDetailPanel)DI.GetDetailPanel("PartsPackingPropertyAppMainPackingType"));
            }
        }
        private PartsPackingPropertyAppFirPackingTypeDetailPanel firPackingTypePanel;
        private PartsPackingPropertyAppFirPackingTypeDetailPanel FirPackingTypePanel {
            get {
                return this.firPackingTypePanel ?? (this.firPackingTypePanel = (PartsPackingPropertyAppFirPackingTypeDetailPanel)DI.GetDetailPanel("PartsPackingPropertyAppFirPackingType"));
            }

        }
        private PartsPackingPropertyAppSecPackingTypeDetailPanel secPackingTypePanel;
        private PartsPackingPropertyAppSecPackingTypeDetailPanel SecPackingTypePanel {
            get {
                return this.secPackingTypePanel ?? (this.secPackingTypePanel = (PartsPackingPropertyAppSecPackingTypeDetailPanel)DI.GetDetailPanel("PartsPackingPropertyAppSecPackingType"));
            }
        }
        private PartsPackingPropertyAppThidPackingTypeDetailPanel thidPackingTypePanel;
        private PartsPackingPropertyAppThidPackingTypeDetailPanel ThidPackingTypePanel {
            get {
                return this.thidPackingTypePanel ?? (this.thidPackingTypePanel = (PartsPackingPropertyAppThidPackingTypeDetailPanel)DI.GetDetailPanel("PartsPackingPropertyAppThidPackingType"));
            }
        }
        protected override string Title {
            get {
                return PartsSalesUIStrings.DataEdit_Title_PartsPackingPropertyDetail;
            }
        }

        protected override bool ShowCancelNotification {
            get {
                return false;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsPackingPropertyAppsQuery().Where(r => r.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity != null) {
                    //查询品牌
                    this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Id == entity.PartsSalesCategoryId), LoadBehavior.RefreshCurrent, branch => {
                        if(branch.HasError) {
                            if(!branch.IsErrorHandled)
                                branch.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(branch);
                            return;
                        }
                        var branchs = branch.Entities.FirstOrDefault();
                        if(branchs != null) {
                            entity.PartsSalesCategoryName = branchs.Name;
                        }
                    }, null);

                    //查询清单
                    this.DomainContext.Load(this.DomainContext.GetPartsPackingPropAppDetailsQuery().Where(r => r.PartsPackingPropAppId == id), LoadBehavior.RefreshCurrent, details => {
                        if(details.HasError) {
                            if(!details.IsErrorHandled)
                                details.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(details);
                            return;
                        }
                        var detailsList = details.Entities.ToArray();
                        if(detailsList.Count() > 0) {
                            for(int i = 0; i < detailsList.Count(); i++) {
                                if((int)DcsPackingUnitType.一级包装 == detailsList[i].PackingType) {
                                    entity.FirId = detailsList[i].Id;
                                    entity.FirPackingType = detailsList[i].PackingType;
                                    entity.FirMeasureUnit = detailsList[i].MeasureUnit;
                                    entity.FirPackingCoefficient = detailsList[i].PackingCoefficient;
                                    entity.FirPackingMaterial = detailsList[i].PackingMaterial;
                                    entity.FirVolume = detailsList[i].Volume;
                                    entity.FirWeight = detailsList[i].Weight;
                                    entity.FirLength = detailsList[i].Length;
                                    entity.FirWidth = detailsList[i].Width;
                                    entity.FirHeight = detailsList[i].Height;
                                    entity.FirPackingMaterialName = detailsList[i].PackingMaterialName;
                                    entity.FirIsBoxStandardPrint = detailsList[i].IsBoxStandardPrint;
                                    entity.FirIsPrintPartStandard = detailsList[i].IsPrintPartStandard;
                                } else if((int)DcsPackingUnitType.二级包装 == detailsList[i].PackingType) {
                                    entity.SecId = detailsList[i].Id;
                                    entity.SecPackingType = detailsList[i].PackingType;
                                    entity.SecMeasureUnit = detailsList[i].MeasureUnit;
                                    entity.SecPackingCoefficient = detailsList[i].PackingCoefficient;
                                    entity.SecPackingMaterial = detailsList[i].PackingMaterial;
                                    entity.SecVolume = detailsList[i].Volume;
                                    entity.SecWeight = detailsList[i].Weight;
                                    entity.SecLength = detailsList[i].Length;
                                    entity.SecWidth = detailsList[i].Width;
                                    entity.SecHeight = detailsList[i].Height;
                                    entity.SecPackingMaterialName = detailsList[i].PackingMaterialName;
                                    entity.SecIsBoxStandardPrint = detailsList[i].IsBoxStandardPrint;
                                    entity.SecIsPrintPartStandard = detailsList[i].IsPrintPartStandard;
                                } else if((int)DcsPackingUnitType.三级包装 == detailsList[i].PackingType) {
                                    entity.ThidId = detailsList[i].Id;
                                    entity.ThidPackingType = detailsList[i].PackingType;
                                    entity.ThidMeasureUnit = detailsList[i].MeasureUnit;
                                    entity.ThidPackingCoefficient = detailsList[i].PackingCoefficient;
                                    entity.ThidPackingMaterial = detailsList[i].PackingMaterial;
                                    entity.ThidVolume = detailsList[i].Volume;
                                    entity.ThidWeight = detailsList[i].Weight;
                                    entity.ThidLength = detailsList[i].Length;
                                    entity.ThidWidth = detailsList[i].Width;
                                    entity.ThidHeight = detailsList[i].Height;
                                    entity.ThidPackingMaterialName = detailsList[i].PackingMaterialName;
                                    entity.ThidIsBoxStandardPrint = detailsList[i].IsBoxStandardPrint;
                                    entity.ThidIsPrintPartStandard = detailsList[i].IsPrintPartStandard;
                                }
                            }
                        }
                    }, null);

                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
    }
}
