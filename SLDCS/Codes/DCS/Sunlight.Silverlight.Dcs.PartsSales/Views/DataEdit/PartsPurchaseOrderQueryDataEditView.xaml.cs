﻿using System.Collections.ObjectModel;
using System.Windows.Browser;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsPurchaseOrderQueryDataEditView {

        public int SalesCategoryId;
        public int[] PartIds;
        public PartsPurchaseOrderQueryDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            this.HideCancelButton();
            this.HideSaveButton();
            this.RegisterButton(new ButtonItem {
                Command = new DelegateCommand(this.ExportPartsPurchaseOrderDetail),
                Icon = Utils.MakeServerUri("Client/Dcs/Images/Operations/Export.png"),
                Title = PartsSalesUIStrings.DataEdit_Title_PartsPurchaseOrderExport
            }, true);
            this.LayoutRoot.Children.Add(DataGridView);
        }

        protected override string Title {
            get {
                return " ";
            }
        }

        private ICommand exportCommand;
        public ICommand ExportCommand {
            get {
                if(exportCommand == null)
                    this.exportCommand = new DelegateCommand(this.ExportPartsPurchaseOrderDetail);
                return this.exportCommand;
            }
        }

        private void ExportPartsPurchaseOrderDetail() {
            if(PartsPurchaseOrderDetails.Count == 0)
                return;
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.ExportVirtualPartsPurchaseOrderDetail(PartIds, SalesCategoryId, loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                    UIHelper.ShowNotification(loadOp.Value);
                }
                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
            }, null);
        }

        private ObservableCollection<VirtualPartsPurchaseOrderDetail> partsPurchaseOrderDetails = new ObservableCollection<VirtualPartsPurchaseOrderDetail>();
        public ObservableCollection<VirtualPartsPurchaseOrderDetail> PartsPurchaseOrderDetails {
            get {
                return partsPurchaseOrderDetails ?? (this.partsPurchaseOrderDetails = new ObservableCollection<VirtualPartsPurchaseOrderDetail>());
            }
        }

        private DataGridViewBase dataGridView;
        private DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("PartsPurchaseOrderDetailForSalesOrderApprove");
                    this.dataGridView.DomainContext = this.DomainContext;
                    this.dataGridView.DataContext = this;
                }
                return this.dataGridView;
            }
        }
    }
}
