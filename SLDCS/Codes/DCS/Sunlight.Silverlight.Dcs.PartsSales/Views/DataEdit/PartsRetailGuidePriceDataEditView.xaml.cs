﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsRetailGuidePriceDataEditView {
        public PartsRetailGuidePriceDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("PartsRetailGuidePrice"));
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsRetailGuidePricesQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsRetailGuidePrice;
            }
        }

        protected override void OnEditSubmitting() {
            var partsRetailGuidePrice = this.DataContext as PartsRetailGuidePrice;
            if(partsRetailGuidePrice == null)
                return;
            partsRetailGuidePrice.ValidationErrors.Clear();
            if(string.IsNullOrWhiteSpace(partsRetailGuidePrice.SparePartCode))
                partsRetailGuidePrice.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsRetailGuidePrice_SparePartCodeIsNull, new[] {
                    "SparePartCode"
                }));
            if(partsRetailGuidePrice.RetailGuidePrice <= 0)
                partsRetailGuidePrice.ValidationErrors.Add(new ValidationResult(string.Format(PartsSalesUIStrings.DataEditView_Validation_PartsRetailGuidePrice_RetailGuidePriceLessZero, partsRetailGuidePrice.SparePartCode), new[] {
                    "RetailGuidePrice"
                }));
            if(partsRetailGuidePrice.HasValidationErrors)
                return;
            //if(DateTime.Compare(partsRetailGuidePrice.ValidationTime, partsRetailGuidePrice.ExpireTime) >= 0) {
            //    UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.DataEditView_Validation_PartsRetailGuidePrice_ValidationTimeUpperThanExpireTime, partsRetailGuidePrice.SparePartCode));
            //    return;
            //}
            ((IEditableObject)partsRetailGuidePrice).EndEdit();
            try {
                if(this.EditState == DataEditState.Edit) {
                    if(partsRetailGuidePrice.Can修改配件零售指导价)
                        partsRetailGuidePrice.修改配件零售指导价();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
