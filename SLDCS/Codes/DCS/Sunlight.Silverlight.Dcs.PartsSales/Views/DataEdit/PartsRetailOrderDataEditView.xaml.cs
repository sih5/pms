﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsRetailOrderDataEditView {
        private DataGridViewBase partsRetailOrderDetailForEditDataGridView;
        //设计中未写明清除数据时，是否要提示。暂时不实现
        //private int warehouseId, branchId;
        //private string branchCode, branchName;
        private ICommand calculateDiscountRate;
        private int warehouseId;
        private ICommand exportFileCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出配件零售订单模板.xlsx";
        private string strFileName;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public PartsRetailOrderDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.Loaded += PartsRetailOrderDataEditView_Loaded;
            this.DataContextChanged += this.PartsRetailOrderDataEditView_DataContextChanged;
        }

        void PartsRetailOrderDataEditView_Loaded(object sender, RoutedEventArgs e) {
            var partsRetailOrder = this.DataContext as PartsRetailOrder;
            if(partsRetailOrder == null)
                return;
            partsRetailOrder.IsQueryWindow = true;
        }

        private DataGridViewBase PartsRetailOrderDetailForEditDataGridView {
            get {
                if(this.partsRetailOrderDetailForEditDataGridView == null) {
                    this.partsRetailOrderDetailForEditDataGridView = DI.GetDataGridView("PartsRetailOrderDetailForEdit");
                    this.partsRetailOrderDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsRetailOrderDetailForEditDataGridView;
            }
        }

        private void PartsRetailOrderDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsRetailOrder = this.DataContext as PartsRetailOrder;
            if(partsRetailOrder == null)
                return;
            //this.warehouseId = partsRetailOrder.WarehouseId;
            partsRetailOrder.PropertyChanged -= this.PartsRetailOrder_PropertyChanged;
            partsRetailOrder.PropertyChanged += this.PartsRetailOrder_PropertyChanged;
            partsRetailOrder.PartsRetailOrderDetails.EntityAdded -= PartsRetailOrderDetails_EntityAdded;
            partsRetailOrder.PartsRetailOrderDetails.EntityAdded += PartsRetailOrderDetails_EntityAdded;
            partsRetailOrder.PartsRetailOrderDetails.EntityRemoved -= PartsRetailOrderDetails_EntityRemoved;
            partsRetailOrder.PartsRetailOrderDetails.EntityRemoved += PartsRetailOrderDetails_EntityRemoved;
        }

        private void PartsRetailOrderDetails_EntityRemoved(object sender, EntityCollectionChangedEventArgs<PartsRetailOrderDetail> e) {
            var partsRetailOrder = this.DataContext as PartsRetailOrder;
            if(partsRetailOrder == null)
                return;
            if(partsRetailOrder.PartsRetailOrderDetails == null)
                return;
            partsRetailOrder.DiscountAmount = partsRetailOrder.PartsRetailOrderDetails.Sum(entity => entity.DiscountAmount);
            partsRetailOrder.DiscountedAmount = partsRetailOrder.PartsRetailOrderDetails.Sum(entity => entity.DiscountedAmount);
            partsRetailOrder.OriginalAmount = partsRetailOrder.PartsRetailOrderDetails.Sum(entity => entity.SalesPrice * entity.Quantity);
        }

        private void PartsRetailOrderDetails_EntityAdded(object sender, EntityCollectionChangedEventArgs<PartsRetailOrderDetail> e) {
            var partsRetailOrder = this.DataContext as PartsRetailOrder;
            if(partsRetailOrder == null)
                return;
            if(partsRetailOrder.PartsRetailOrderDetails == null)
                return;
            partsRetailOrder.DiscountAmount = partsRetailOrder.PartsRetailOrderDetails.Sum(entity => entity.DiscountAmount);
            partsRetailOrder.DiscountedAmount = partsRetailOrder.PartsRetailOrderDetails.Sum(entity => entity.DiscountedAmount);
            partsRetailOrder.OriginalAmount = partsRetailOrder.PartsRetailOrderDetails.Sum(entity => entity.SalesPrice * entity.Quantity);
            e.Entity.PropertyChanged -= this.PartsRetailOrderDetail_PropertyChanged;
            e.Entity.PropertyChanged += this.PartsRetailOrderDetail_PropertyChanged;
        }

        private void PartsRetailOrderDetail_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsRetailOrder = this.DataContext as PartsRetailOrder;
            if(partsRetailOrder == null)
                return;
            var partsRetailOrderDetail = sender as PartsRetailOrderDetail;
            if(partsRetailOrderDetail == null)
                return;

            switch(e.PropertyName) {
                case "Quantity":
                case "SalesPrice":
                    if(partsRetailOrderDetail.Quantity == 0) {
                        partsRetailOrderDetail.DiscountedAmount = partsRetailOrderDetail.DiscountAmount = partsRetailOrderDetail.DiscountedPrice = 0;
                        return;
                    }
                    partsRetailOrderDetail.DiscountedAmount = partsRetailOrderDetail.DiscountedPrice * partsRetailOrderDetail.Quantity;
                    partsRetailOrderDetail.DiscountAmount = (partsRetailOrderDetail.SalesPrice - partsRetailOrderDetail.DiscountedPrice) * partsRetailOrderDetail.Quantity;
                    //partsRetailOrderDetail.DiscountedAmount = partsRetailOrderDetail.SalesPrice * partsRetailOrderDetail.Quantity;
                    //partsRetailOrderDetail.DiscountAmount = partsRetailOrderDetail.SalesPrice * partsRetailOrderDetail.Quantity - partsRetailOrderDetail.DiscountedAmount;
                    //partsRetailOrderDetail.DiscountedPrice = decimal.Round((partsRetailOrderDetail.DiscountedAmount / partsRetailOrderDetail.Quantity), 2);
                    partsRetailOrder.OriginalAmount = partsRetailOrder.PartsRetailOrderDetails.Sum(entity => entity.SalesPrice * entity.Quantity);
                    break;
                case "DiscountedAmount":
                    partsRetailOrder.DiscountedAmount = partsRetailOrder.PartsRetailOrderDetails.Sum(entity => entity.DiscountedAmount);
                    partsRetailOrderDetail.DiscountAmount = partsRetailOrderDetail.SalesPrice * partsRetailOrderDetail.Quantity - partsRetailOrderDetail.DiscountedAmount;
                    break;
                case "DiscountAmount":
                    partsRetailOrder.DiscountAmount = partsRetailOrder.PartsRetailOrderDetails.Sum(entity => entity.DiscountAmount);
                    break;
                case "DiscountedPrice":
                    partsRetailOrderDetail.DiscountedAmount = partsRetailOrderDetail.DiscountedPrice * partsRetailOrderDetail.Quantity;
                    partsRetailOrderDetail.DiscountAmount = (partsRetailOrderDetail.SalesPrice - partsRetailOrderDetail.DiscountedPrice) * partsRetailOrderDetail.Quantity;
                    break;
            }
        }

        private void PartsRetailOrder_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsRetailOrder = this.DataContext as PartsRetailOrder;
            if(partsRetailOrder == null)
                return;
            switch(e.PropertyName) {
                case "WarehouseId":
                    if(partsRetailOrder.WarehouseId == warehouseId)
                        return;
                    if(partsRetailOrder.PartsRetailOrderDetails.Any())
                        DcsUtils.Confirm(PartsSalesUIStrings.DataEditPanel_Confirm_partsRetailOrder_partsRetailOrderDetails, () => {
                            //清空清单
                            this.PartsRetailOrderDetailForEditDataGridView.CommitEdit();
                            foreach(var item in partsRetailOrder.PartsRetailOrderDetails)
                                partsRetailOrder.PartsRetailOrderDetails.Remove(item);
                            warehouseId = partsRetailOrder.WarehouseId;
                        }, () => {
                            //回滚
                            partsRetailOrder.WarehouseId = warehouseId;
                        });
                    else
                        warehouseId = partsRetailOrder.WarehouseId;

                    break;
                case "DiscountRate":
                    if(partsRetailOrder.DiscountRate < 0)
                        UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsRetailOrder_DiscountRateIsLessThenZero);
                    if(partsRetailOrder.DiscountRate > 1)
                        UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsRetailOrder_DiscountRateIsGreaterThanThenOne);
                    break;
            }
        }

        #region 导入

        private void ShowFileDialog() {
            var partsRetailOrder = this.DataContext as PartsRetailOrder;
            if(partsRetailOrder == null)
                return;
            partsRetailOrder.ValidationErrors.Clear();
            if(partsRetailOrder.WarehouseId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsRetailOrder_WarehouseIsNull);
                return;
            }
            this.Uploader.ShowFileDialog();
        }

        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }

        private RadUpload uploader;
        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        var partsRetailOrder = this.DataContext as PartsRetailOrder;
                        if(partsRetailOrder == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.导入配件零售订单Async(BaseApp.Current.CurrentUserData.EnterpriseId, partsRetailOrder.WarehouseId, e.HandlerData.CustomData["Path"].ToString());
                        this.excelServiceClient.导入配件零售订单Completed -= excelServiceClient_导入配件零售订单Completed;
                        this.excelServiceClient.导入配件零售订单Completed += excelServiceClient_导入配件零售订单Completed;
                    };
                }
                return this.uploader;
            }
        }

        private void excelServiceClient_导入配件零售订单Completed(object sender, 导入配件零售订单CompletedEventArgs e) {
            var PartsRetailOrder = this.DataContext as PartsRetailOrder;
            if(PartsRetailOrder == null)
                return;
            foreach(var detail in PartsRetailOrder.PartsRetailOrderDetails) {
                PartsRetailOrder.PartsRetailOrderDetails.Remove(detail);
            }
            if(e.rightData != null && e.rightData.Length > 0) {
                foreach(var data in e.rightData) {
                    PartsRetailOrder.PartsRetailOrderDetails.Add(new PartsRetailOrderDetail {
                        SparePartId = data.PartsId,
                        SparePartCode = data.PartsCode,
                        SparePartName = data.PartsName,
                        Quantity = data.Quantity,
                        SalesPrice = data.Price,
                        DiscountedPrice = Math.Round(data.DiscountPrice, 2),
                        DiscountedAmount = Math.Round(data.DiscountPrice, 2) * data.Quantity,
                        DiscountAmount = Math.Round((data.Price - data.DiscountPrice), 2) * data.Quantity,
                        Remark = data.Remark
                    });
                }
                PartsRetailOrder.OriginalAmount = PartsRetailOrder.PartsRetailOrderDetails.Sum(r => r.Quantity * r.SalesPrice);
                PartsRetailOrder.DiscountedAmount = PartsRetailOrder.PartsRetailOrderDetails.Sum(r => r.Quantity * r.DiscountedPrice);
                PartsRetailOrder.DiscountAmount = PartsRetailOrder.OriginalAmount - PartsRetailOrder.DiscountedAmount;
            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowAlertMessage(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
        }

        /// <summary>
        /// 文件上传成功后，调用该方法进行上传后的处理。不支持异步调用
        /// <param>上传文件名 <name>fileName</name> </param>
        /// </summary>
        protected Action<string> UploadFileSuccessedProcessing;

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //取当前登录用户的HashCode来标记上传文件的唯一性
                //this.UploadFileSuccessedProcessing.Invoke(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }

        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage( PartsSalesUIStrings.DataEdit_Validation_Import_CloseFile);
                    e.Handled = true;
                }
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                              new ImportTemplateColumn {
                                                 Name=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                 Name=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                                            },
                                            new ImportTemplateColumn {
                                                 Name=PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_Quantity,
                                                IsRequired = true
                                            },new ImportTemplateColumn{
                                                Name=PartsSalesUIStrings.DataEdit_Text_PartsRetailOrderDetail_DiscountPrice,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                               Name=PartsSalesUIStrings.DataEditView_Text_SecondClassStationPlan_Remark
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }

        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }

        #endregion

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsRetailOrderWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();

                if(entity != null) {
                    foreach(var partsRetailOrderDetail in entity.PartsRetailOrderDetails) {
                        partsRetailOrderDetail.PropertyChanged -= this.PartsRetailOrderDetail_PropertyChanged;
                        partsRetailOrderDetail.PropertyChanged += this.PartsRetailOrderDetail_PropertyChanged;
                    }
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("PartsRetailOrder"));
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsSalesUIStrings.DetailPanel_Title_PartsSalesRetailOrderDetail, null, () => this.PartsRetailOrderDetailForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.DataEditPanel_Action_ImportNew,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                Command = new DelegateCommand(this.ShowFileDialog)
            });
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.DataEditPanel_Action_Export,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                Command = this.ExportTemplateCommand
            });
        }

        private void CalculateDiscountRateCommand() {
            this.calculateDiscountRate = new DelegateCommand(() => {
                var partsRetailOrder = this.DataContext as PartsRetailOrder;
                if(partsRetailOrder == null)
                    return;
                var partsRetailOrderDetails = partsRetailOrder.PartsRetailOrderDetails;
                if(partsRetailOrderDetails == null || !partsRetailOrderDetails.Any())
                    return;
                DcsUtils.Confirm(PartsSalesUIStrings.DataEditView_Validation_PartsRetailOrder_RefreshPartsRetailOrderDetails, () => {
                    foreach(var partsRetailOrderDetail in partsRetailOrderDetails) {
                        partsRetailOrderDetail.DiscountedAmount = decimal.Round((decimal)(partsRetailOrder.DiscountRate * (double)partsRetailOrderDetail.SalesPrice * partsRetailOrderDetail.Quantity), 2);
                        partsRetailOrderDetail.DiscountAmount = partsRetailOrderDetail.SalesPrice * partsRetailOrderDetail.Quantity - partsRetailOrderDetail.DiscountedAmount;
                        partsRetailOrderDetail.DiscountedPrice = decimal.Round((decimal)(partsRetailOrder.DiscountRate * (double)partsRetailOrderDetail.SalesPrice), 2);
                    }
                });
            });
        }

        protected override void OnEditSubmitting() {
            var partsRetailOrder = this.DataContext as PartsRetailOrder;
            if(partsRetailOrder == null || !this.PartsRetailOrderDetailForEditDataGridView.CommitEdit())
                return;
            partsRetailOrder.ValidationErrors.Clear();
            //if(!this.isClickButton && partsRetailOrder.DiscountRate != 1) {
            //    UIHelper.ShowNotification("折扣率不等于1,请先点击应用");
            //    return;
            //}
            if(partsRetailOrder.WarehouseId == default(int))
                partsRetailOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsRetailOrder_WarehouseIsNull, new[] {
                    "WarehouseId"
                }));
            if(string.IsNullOrWhiteSpace(partsRetailOrder.CustomerName))
                partsRetailOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsRetailOrder_CustomerNameIsNull, new[] {
                    "CustomerName"
                }));
            if(string.IsNullOrWhiteSpace(partsRetailOrder.CustomerCellPhone))
                partsRetailOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsRetailOrder_CustomerCellPhoneIsNull, new[] {
                    "CustomerCellPhone"
                }));
            if(CheckCustomerCellPhone(partsRetailOrder))
                return;
            if(string.IsNullOrWhiteSpace(partsRetailOrder.CustomerAddress))
                partsRetailOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsRetailOrder_CustomerAddressIsNull, new[] {
                    "CustomerAddress"
                }));
            if(partsRetailOrder.PaymentMethod == default(int))
                partsRetailOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsRetailOrder_PaymentMethodIsNull, new[] {
                    "PaymentMethod"
                }));
            if(partsRetailOrder.PartsRetailOrderDetails == null || !partsRetailOrder.PartsRetailOrderDetails.Any()) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsRetailOrder_PartsRetailOrderDetailsIsNull);
                return;
            }
            foreach(var partsRetailOrderDetail in partsRetailOrder.PartsRetailOrderDetails) {
                partsRetailOrderDetail.ValidationErrors.Clear();
                if(partsRetailOrderDetail.Quantity <= 0)
                    partsRetailOrderDetail.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsRetailOrder_PartsRetailOrderDetails_QuantityIsEqualOrLesserThanZero, new[] {
                        "Quantity"
                    }));
                //if(partsRetailOrderDetail.DiscountedAmount < 0)
                //    partsRetailOrderDetail.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsRetailOrder_PartsRetailOrderDetails_DiscountedAmountIsLessThanZero, new[] {
                //        "DiscountedAmount"
                //    }));
                if(!partsRetailOrderDetail.HasValidationErrors)
                    ((IEditableObject)partsRetailOrderDetail).EndEdit();
            }

            //if(!partsRetailOrder.HasValidationErrors && partsRetailOrder.PartsRetailOrderDetails.All(e => !e.HasValidationErrors)) {
            //    partsRetailOrder.DiscountRate = Math.Round((double)(partsRetailOrder.DiscountedAmount / partsRetailOrder.OriginalAmount), 2);
            //    if(partsRetailOrder.DiscountRate.CompareTo(0) < 0 || partsRetailOrder.DiscountRate.CompareTo(1) > 0)
            //        partsRetailOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsRetailOrder_DiscountRateMustBetweenOneAndZero, new[] {
            //        "DiscountRate"
            //    }));
            //}
            //if(partsRetailOrder.DiscountRate !=1|| string.is) {
            //    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsRetailOrder_DiscountRateIsLessThenZero);
            //}
            if(partsRetailOrder.HasValidationErrors || partsRetailOrder.PartsRetailOrderDetails.Any(e => e.HasValidationErrors))
                return;
            ((IEditableObject)partsRetailOrder).EndEdit();
            try {
                if(this.EditState == DataEditState.New && partsRetailOrder.Can生成配件零售订单)
                    partsRetailOrder.生成配件零售订单();
            } catch(Exception ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }

            if(partsRetailOrder.IsQueryWindow) {
                var retailOrderCustomer = new RetailOrderCustomer();
                retailOrderCustomer.CustomerName = partsRetailOrder.CustomerName;
                retailOrderCustomer.CustomerPhone = partsRetailOrder.CustomerPhone;
                retailOrderCustomer.Address = partsRetailOrder.CustomerAddress;
                retailOrderCustomer.CustomerCellPhone = partsRetailOrder.CustomerCellPhone;
                retailOrderCustomer.Status = (int)DcsBaseDataStatus.有效;
                retailOrderCustomer.企业Id = BaseApp.Current.CurrentUserData.EnterpriseId;
                ((IEditableObject)retailOrderCustomer).EndEdit();
                DomainContext.RetailOrderCustomers.Add(retailOrderCustomer);
            }

            base.OnEditSubmitting();
        }

        private static bool CheckCustomerCellPhone(PartsRetailOrder partsRetailOrder) {
            string str = @"^\d+$";
            string num = "^([1][3-9][0-9]|199)$";
            Regex regex = new Regex(str);
            Regex regexnum = new Regex(num);
            string cellPhoneNumber = partsRetailOrder.CustomerCellPhone;
            if(regex.IsMatch(cellPhoneNumber) == false) {
                partsRetailOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_TelephoneNumber, new[] {
                    "CustomerCellPhone"
                }));
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_TelephoneNumber);
                return true;
            }
            if(cellPhoneNumber.Length == 11) {
                string strCellPhoneNumber = cellPhoneNumber.Substring(0, 3);
                if(regexnum.IsMatch(strCellPhoneNumber) == false) {
                    partsRetailOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_TelephoneStart, new[] {
                        "CustomerCellPhone"
                    }));
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_TelephoneStart);
                    return true;
                }
            } else {
                partsRetailOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_TelephoneCount, new[] {
                    "CustomerCellPhone"
                }));
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_DealerPartsRetailOrder_TelephoneCount);
                return true;
            }
            return false;
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsRetailOrder;
            }
        }

        public ICommand CalculateDiscountRate {
            get {
                if(this.calculateDiscountRate == null)
                    this.CalculateDiscountRateCommand();
                return this.calculateDiscountRate;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}