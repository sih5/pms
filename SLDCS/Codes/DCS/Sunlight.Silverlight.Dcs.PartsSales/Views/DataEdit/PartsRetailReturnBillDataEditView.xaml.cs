﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsRetailReturnBillDataEditView {
        private DataGridViewBase partsRetailReturnBillDetailForEditDataGridView;

        public PartsRetailReturnBillDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += this.PartsRetailReturnBillDataEditView_DataContextChanged;
        }

        private DataGridViewBase PartsRetailReturnBillDetailForEditDataGridView {
            get {
                if(this.partsRetailReturnBillDetailForEditDataGridView == null) {
                    this.partsRetailReturnBillDetailForEditDataGridView = DI.GetDataGridView("PartsRetailReturnBillDetailForEdit");
                    this.partsRetailReturnBillDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsRetailReturnBillDetailForEditDataGridView;
            }
        }

        private void PartsRetailReturnBillDataEditView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var partsRetailReturnBill = this.DataContext as PartsRetailReturnBill;
            if(partsRetailReturnBill == null)
                return;
            partsRetailReturnBill.PartsRetailReturnBillDetails.EntityAdded -= this.PartsRetailReturnBillDetails_EntityAdded;
            partsRetailReturnBill.PartsRetailReturnBillDetails.EntityAdded += this.PartsRetailReturnBillDetails_EntityAdded;
            partsRetailReturnBill.PartsRetailReturnBillDetails.EntityRemoved -= this.PartsRetailReturnBillDetails_EntityRemoved;
            partsRetailReturnBill.PartsRetailReturnBillDetails.EntityRemoved += this.PartsRetailReturnBillDetails_EntityRemoved;
        }

        private void PartsRetailReturnBillDetail_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsRetailReturnBillDetail = sender as PartsRetailReturnBillDetail;
            if (partsRetailReturnBillDetail == null)
                return;
            switch (e.PropertyName) {
                case "ReturnPrice":
                case "Quantity":
                    var partsRetailReturnBill = this.DataContext as PartsRetailReturnBill;
                    if(partsRetailReturnBill == null)
                        return;
                    partsRetailReturnBillDetail.ReturnPrice = decimal.Round(partsRetailReturnBillDetail.ReturnPrice, 2);
                    partsRetailReturnBill.TotalAmount = partsRetailReturnBill.PartsRetailReturnBillDetails.Sum(entity => entity.Quantity * entity.ReturnPrice);
                    break;
            }
        }

        private void PartsRetailReturnBillDetails_EntityRemoved(object sender, EntityCollectionChangedEventArgs<PartsRetailReturnBillDetail> e) {
            var partsRetailReturnBill = this.DataContext as PartsRetailReturnBill;
            if(partsRetailReturnBill == null)
                return;
            partsRetailReturnBill.TotalAmount = partsRetailReturnBill.PartsRetailReturnBillDetails.Sum(entity => entity.Quantity * entity.ReturnPrice);
        }

        private void PartsRetailReturnBillDetails_EntityAdded(object sender, EntityCollectionChangedEventArgs<PartsRetailReturnBillDetail> e) {
            e.Entity.PropertyChanged += this.PartsRetailReturnBillDetail_PropertyChanged;
        }

        private void CreateUI() {
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsSalesUIStrings.DataGridView_Title_PartsRetailReturnBillDetail, null, this.PartsRetailReturnBillDetailForEditDataGridView);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);
            var queryWindowPartsOutboundBillDetail = DI.GetQueryWindow("PartsOutboundBillDetailForPartsRetailOrder") as DcsMultiSelectQueryWindowBase;
            if(queryWindowPartsOutboundBillDetail != null)
                queryWindowPartsOutboundBillDetail.SelectionDecided += this.QueryWindowPartsOutboundBillDetail_SelectionDecided;
            this.ptbSourceCode.PopupContent = queryWindowPartsOutboundBillDetail;
            var queryWindowWarehouse = DI.GetQueryWindow("Warehouse");
            queryWindowWarehouse.Loaded += this.QueryWindowWarehouse_Loaded;
            queryWindowWarehouse.SelectionDecided += this.QueryWindowWarehouse_SelectionDecided;
            this.ptbWarehouseName.PopupContent = queryWindowWarehouse;
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsRetailReturnBillWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                foreach(var detail in entity.PartsRetailReturnBillDetails) {
                    detail.PropertyChanged -= this.PartsRetailReturnBillDetail_PropertyChanged;
                    detail.PropertyChanged += this.PartsRetailReturnBillDetail_PropertyChanged;
                }
                this.SetObjectToEdit(entity);
            }, null);
        }

        private void QueryWindowWarehouse_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", new FilterItem {
                MemberName = "StorageCompanyId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
        }

        private void QueryWindowPartsOutboundBillDetail_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiSelectQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var partsOutboundBillDetails = queryWindow.SelectedEntities.Cast<PartsOutboundBillDetail>();
            if(partsOutboundBillDetails == null)
                return;
            var partsRetailReturnBill = this.DataContext as PartsRetailReturnBill;
            if(partsRetailReturnBill == null)
                return;
            var outboundBillDetails = partsOutboundBillDetails as PartsOutboundBillDetail[] ?? partsOutboundBillDetails.ToArray();
            if(outboundBillDetails.Any() && outboundBillDetails.First().PartsRetailOrder != null) {
                partsRetailReturnBill.SourceId = outboundBillDetails.First().PartsRetailOrder.Id;
                partsRetailReturnBill.SourceCode = outboundBillDetails.First().PartsRetailOrder.Code;
                partsRetailReturnBill.CustomerAccountId = outboundBillDetails.First().PartsRetailOrder.CustomerAccountId;
                partsRetailReturnBill.RetailCustomerCompanyId = outboundBillDetails.First().PartsRetailOrder.RetailCustomerCompanyId;
                partsRetailReturnBill.RetailCustomerCompanyCode = outboundBillDetails.First().PartsRetailOrder.RetailCustomerCompanyCode;
                partsRetailReturnBill.RetailCustomerCompanyName = outboundBillDetails.First().PartsRetailOrder.RetailCustomerCompanyName;
                partsRetailReturnBill.SalesUnitOwnerCompanyId = outboundBillDetails.First().PartsRetailOrder.SalesUnitOwnerCompanyId;
                partsRetailReturnBill.SalesUnitOwnerCompanyCode = outboundBillDetails.First().PartsRetailOrder.SalesUnitOwnerCompanyCode;
                partsRetailReturnBill.SalesUnitOwnerCompanyCode = outboundBillDetails.First().PartsRetailOrder.SalesUnitOwnerCompanyCode;
                partsRetailReturnBill.SalesUnitOwnerCompanyName = outboundBillDetails.First().PartsRetailOrder.SalesUnitOwnerCompanyName;
                partsRetailReturnBill.BranchId = outboundBillDetails.First().PartsRetailOrder.BranchId;
                partsRetailReturnBill.BranchCode = outboundBillDetails.First().PartsRetailOrder.BranchCode;
                partsRetailReturnBill.BranchName = outboundBillDetails.First().PartsRetailOrder.BranchName;
                partsRetailReturnBill.SalesUnitId = outboundBillDetails.First().PartsRetailOrder.SalesUnitId;
                partsRetailReturnBill.SalesUnitCode = outboundBillDetails.First().PartsRetailOrder.SalesUnitCode;
                partsRetailReturnBill.SalesUnitName = outboundBillDetails.First().PartsRetailOrder.SalesUnitName;
                partsRetailReturnBill.CustomerName = outboundBillDetails.First().PartsRetailOrder.CustomerName;
                partsRetailReturnBill.CustomerPhone = outboundBillDetails.First().PartsRetailOrder.CustomerPhone;
                partsRetailReturnBill.CustomerCellPhone = outboundBillDetails.First().PartsRetailOrder.CustomerCellPhone;
                partsRetailReturnBill.CustomerAddress = outboundBillDetails.First().PartsRetailOrder.CustomerAddress;
                this.DomainContext.Load(this.DomainContext.GetPartsRetailOrderDetailsQuery().Where(entity => entity.PartsRetailOrderId == partsRetailReturnBill.SourceId), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError)
                        return;
                    var groupinfp = from tempgroup in  outboundBillDetails
                                        group tempgroup by new {
                                            tempgroup.SparePartId,
                                            tempgroup.SparePartCode,
                                            tempgroup.SparePartName    
                                        } into g
                                    select new {
                                        g.Key.SparePartId,
                                        g.Key.SparePartCode,
                                        g.Key.SparePartName
                                    };                                      
                       foreach(var item in groupinfp) {
                        var partsRetailReturnBillDetail = new PartsRetailReturnBillDetail();
                        partsRetailReturnBill.PartsRetailReturnBillDetails.Add(partsRetailReturnBillDetail);
                        partsRetailReturnBillDetail.PartsRetailReturnBillId = partsRetailReturnBill.Id;
                        partsRetailReturnBillDetail.SparePartId = item.SparePartId;
                        partsRetailReturnBillDetail.SparePartCode = item.SparePartCode;
                        partsRetailReturnBillDetail.SparePartName = item.SparePartName;
                        var partsRetailOrderDetail = loadOp.Entities.FirstOrDefault(entity => entity.SparePartId == item.SparePartId);
                        if(partsRetailOrderDetail != null)
                            partsRetailReturnBillDetail.OriginalOrderPrice = partsRetailOrderDetail.DiscountedPrice;
                        partsRetailReturnBillDetail.ReturnPrice = partsRetailReturnBillDetail.OriginalOrderPrice;
                    }
                }, null);
            }
            var details = partsRetailReturnBill.PartsRetailReturnBillDetails.ToArray();
            ((IEditableObject)partsRetailReturnBill).EndEdit();
            foreach(var detail in details) {
                ((IEditableObject)detail).EndEdit();
                detail.ValidationErrors.Clear();
                partsRetailReturnBill.PartsRetailReturnBillDetails.Remove(detail);
            }

            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void QueryWindowWarehouse_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var warehouse = queryWindow.SelectedEntities.Cast<Warehouse>().FirstOrDefault();
            if(warehouse == null /*|| warehouse.StorageCompanyId != BaseApp.Current.CurrentUserData.EnterpriseId*/)
                return;

            var partsRetailReturnBill = this.DataContext as PartsRetailReturnBill;
            if(partsRetailReturnBill == null)
                return;
            partsRetailReturnBill.WarehouseId = warehouse.Id;
            partsRetailReturnBill.WarehouseCode = warehouse.Code;
            partsRetailReturnBill.WarehouseName = warehouse.Name;

            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsRetailReturnBill;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.PartsRetailReturnBillDetailForEditDataGridView.CommitEdit())
                return;

            var partsRetailReturnBill = this.DataContext as PartsRetailReturnBill;
            if(partsRetailReturnBill == null)
                return;
            partsRetailReturnBill.ValidationErrors.Clear();
            if(string.IsNullOrWhiteSpace(partsRetailReturnBill.CustomerCellPhone)) {
                partsRetailReturnBill.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsRetailReturnBill_CustomerCellPhoneIsNull, new[] {
                    "CustomerCellPhone"
                }));
            }

            if(!partsRetailReturnBill.PartsRetailReturnBillDetails.Any()) {
                UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.DataEditView_Validation_PartsRetailReturnBillDetails));
                return;
            }
            foreach(var relation in partsRetailReturnBill.PartsRetailReturnBillDetails.Where(e => e.Quantity <= 0)) {
                UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.DataEditView_Validation_PartsRetailGuidePrice_Quantity, relation.SparePartCode));
                return;
            }

            foreach(var relation in partsRetailReturnBill.PartsRetailReturnBillDetails.Where(e => e.ReturnPrice <= 0)) {
                UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.DataEditView_Validation_PartsRetailGuidePrice_ReturnPrice, relation.SparePartCode));
                return;
            }
            //if(partsRetailReturnBillDetail.Quantity <= 0) {
            //    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsRetailReturnBillDetail_Quantity);
            //    return;
            //}
            //if(!partsRetailReturnBill.PartsRetailReturnBillDetails.Any()) {
            //    UIHelper.ShowNotification("清单信息不能为空！");
            //}
            if(partsRetailReturnBill.HasValidationErrors)
                return;
            try {
                if(this.EditState == DataEditState.New) {
                    if(partsRetailReturnBill.Can生成配件零售退货单)
                        partsRetailReturnBill.生成配件零售退货单();
                } else if(partsRetailReturnBill.Can修改配件零售退货单)
                    partsRetailReturnBill.修改配件零售退货单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            ((IEditableObject)partsRetailReturnBill).EndEdit();
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
