﻿using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalePriceIncreaseRateDataEditView {
        public PartsSalePriceIncreaseRateDataEditView() {
            InitializeComponent();
        }

        protected override string BusinessName {
            get {
                return PartsSalesUIStrings.DataEdit_BusinessName_PartsSalePriceIncrease;
            }
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalePriceIncreaseRatesQuery().Where(v => v.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void OnEditSubmitting() {
            var partsSalePriceIncreaseRate = this.DataContext as PartsSalePriceIncreaseRate;
            if(partsSalePriceIncreaseRate == null)
                return;
            if(string.IsNullOrWhiteSpace(partsSalePriceIncreaseRate.GroupCode)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalePriceIncreaseRate_GroupCode);
                return;
            }
            if(string.IsNullOrWhiteSpace(partsSalePriceIncreaseRate.GroupName)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalePriceIncreaseRate_GroupName);
                return;
            }
            ((IEditableObject)partsSalePriceIncreaseRate).EndEdit();
            base.OnEditSubmitting();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }
    }
}
