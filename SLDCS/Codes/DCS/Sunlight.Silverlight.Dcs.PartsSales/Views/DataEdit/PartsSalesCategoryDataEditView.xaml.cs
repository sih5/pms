﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesCategoryDataEditView {
        private DataGridViewBase partsSalesOrderTypeForEditDataGridView;

        public PartsSalesCategoryDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private DataGridViewBase PartsSalesOrderTypeForEditDataGridView {
            get {
                if(this.partsSalesOrderTypeForEditDataGridView == null) {
                    this.partsSalesOrderTypeForEditDataGridView = DI.GetDataGridView("PartsSalesOrderTypeForEdit");
                    this.partsSalesOrderTypeForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsSalesOrderTypeForEditDataGridView;
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("PartsSalesCategory"));
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsSalesCategory), "PartsSalesOrderTypes"), null, () => this.PartsSalesOrderTypeForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoryWithPartsSalesOrderTypesByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.SetObjectToEdit(entity);
            }, null);
        }

        protected override void OnEditSubmitting() {
            if(!this.PartsSalesOrderTypeForEditDataGridView.CommitEdit())
                return;

            var partsSalesCategory = this.DataContext as PartsSalesCategory;
            if(partsSalesCategory == null)
                return;
            partsSalesCategory.ValidationErrors.Clear();
            if(partsSalesCategory.PartsSalesOrderTypes.GroupBy(entity => new {
                entity.Name,
                entity.Code
            }).Any(array => array.Count() > 1)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesCategory_PartsSalesOrderTypesCodeAndNameUnique);
                return;
            }
            if(partsSalesCategory.Code == null)
                partsSalesCategory.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesCategory_CodeIsNull, new[] {
                    "Code"
                }));
            if(partsSalesCategory.Name == null)
                partsSalesCategory.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesCategory_NameIsNull, new[] {
                    "Name"
                }));
            foreach(var type in partsSalesCategory.PartsSalesOrderTypes) {
                type.ValidationErrors.Clear();
                ((IEditableObject)type).EndEdit();
            }
            if(partsSalesCategory.ValidationErrors.Any() || partsSalesCategory.PartsSalesOrderTypes.Any(e => e.HasValidationErrors))
                return;
            ((IEditableObject)partsSalesCategory).EndEdit();
            base.OnEditSubmitting();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsSalesCategory;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null) {
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            } else
                this.LoadEntityToEdit((int)id);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }
    }
}
