﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesCategoryNDataEditView {

        protected override string BusinessName {
            get {
                return PartsSalesUIStrings.DataEdit_BusinessName_PartsSalesCategoryN;//DcsUIStrings.BusinessName_PartsSalesCategory;
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }


        protected override void OnEditSubmitted() {
            //if(this.PartsSalesOrderTypes != null)
            //    PartsSalesOrderTypes.Clear();
            //this.DataGridViewRoot.Visibility = Visibility.Visible;
            //this.First.Visibility = Visibility.Visible;
            //this.Second.Visibility = Visibility.Collapsed;
            //this.PartsSalesCategoryId = default(int);
            //this.PartsSalesCategoryName = string.Empty;
            base.OnEditSubmitted();
        }

        protected override void OnEditCancelled() {
            base.OnEditCancelled();

        }

        protected override void OnEditSubmitting() {
            var partsSalesCategory = this.DataContext as PartsSalesCategory;
            if(string.IsNullOrWhiteSpace(partsSalesCategory.Code)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesCategory_CodeIsNull);
                return;
            }
            if(string.IsNullOrWhiteSpace(partsSalesCategory.Name)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesCategory_NameIsNull);
                return;
            }

            ((IEditableObject)partsSalesCategory).EndEdit();
            try {
                if(this.EditState == DataEditState.Edit) {
                    partsSalesCategory.Status = (int)DcsBaseDataStatus.有效;
                    ((IEditableObject)partsSalesCategory).EndEdit();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(v => v.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }


        public PartsSalesCategoryNDataEditView() {
            InitializeComponent();
        }

    }
}
