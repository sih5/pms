﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesOrderDataEditView {
        private ObservableCollection<CompanyAddress> kvCompanyAddresses;
        private KeyValueManager keyValueManager;
        private DataGridViewBase partsSalesOrderDetailForEditDataGridView;
        private ObservableCollection<KeyValuePair> kvSalesUnits;
        private ObservableCollection<KeyValuePair> kvPartsSalesOrderTypes;
        private ObservableCollection<KeyValuePair> kvShippingMethods;
        private int? CompanyType = null;
        private bool isIfDirectProvision = true;
        private readonly DcsDomainContext domainContext = new DcsDomainContext();
        private Company companys;
        private readonly ObservableCollection<KeyValuePair> kvReceiverSalesCateNames = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvReceivingWarehouseNames = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategories = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvReceivingWarehouse = new ObservableCollection<KeyValuePair>();
        private ObservableCollection<KeyValuePair> kvProvinceNames;
        private ObservableCollection<KeyValuePair> kvCityNames;
        private readonly List<TiledRegion> TiledRegions = new List<TiledRegion>();
        private int? receivingCompanyType; //收货单位企业类型
        public ObservableCollection<KeyValuePair> KvReceivingWarehouse {
            get {
                return this.kvReceivingWarehouse;
            }
        }

        private ObservableCollection<SalesUnitAffiWarehouse> kvSalesUnitAffiWarehouses;

        public ObservableCollection<SalesUnitAffiWarehouse> KvSalesUnitAffiWarehouses {
            get {
                return this.kvSalesUnitAffiWarehouses ?? (this.kvSalesUnitAffiWarehouses = new ObservableCollection<SalesUnitAffiWarehouse>());
            }
        }
        public ObservableCollection<KeyValuePair> KvProvinceNames {
            get {
                return this.kvProvinceNames ?? (this.kvProvinceNames = new ObservableCollection<KeyValuePair>());
            }
        }
        public ObservableCollection<KeyValuePair> KvCityNames {
            get {
                return this.kvCityNames ?? (this.kvCityNames = new ObservableCollection<KeyValuePair>());
            }
        }
        private decimal lnotApprovePrice;
        private decimal lorderUseablePosition;
        private decimal lRebateAmount;

        /// <summary>
        /// 当前登录人企业类型 用于过滤品牌
        /// </summary>
        private int currentCompanyType;

        public ObservableCollection<KeyValuePair> KvPartsSalesCategories {
            get {
                return this.kvPartsSalesCategories;
            }
        }

        //本地缓存，记录修改前的配件销售订单类型Id
        private int oldPartsSalesOrderTypeId;

        private string strFileName;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private RadUpload uploader;

        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        var partsSalesOrder = this.DataContext as PartsSalesOrder;
                        if(partsSalesOrder == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.ImportPartsSalesOrderDetailAsync(e.HandlerData.CustomData["Path"].ToString(), partsSalesOrder.IfDirectProvision, partsSalesOrder.SubmitCompanyId, partsSalesOrder.SalesCategoryId);
                        this.excelServiceClient.ImportPartsSalesOrderDetailCompleted -= ExcelServiceClient_ImportPartsSalesOrderDetailCompleted;
                        this.excelServiceClient.ImportPartsSalesOrderDetailCompleted += ExcelServiceClient_ImportPartsSalesOrderDetailCompleted;
                    };
                }
                return this.uploader;
            }
        }

        /// <summary>
        /// 文件上传成功后，调用该方法进行上传后的处理。不支持异步调用
        /// <param>上传文件名 <name>fileName</name> </param>
        /// </summary>
        protected Action<string> UploadFileSuccessedProcessing;

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //取当前登录用户的HashCode来标记上传文件的唯一性
                //this.UploadFileSuccessedProcessing.Invoke(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }

        private void ExcelServiceClient_ImportPartsSalesOrderDetailCompleted(object sender, ImportPartsSalesOrderDetailCompletedEventArgs e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            var detailList = partsSalesOrder.PartsSalesOrderDetails.ToList();
            foreach(var detail in detailList) {
                partsSalesOrder.PartsSalesOrderDetails.Remove(detail);
            }
            if(e.rightData != null && e.rightData.Length > 0) {
                foreach(var data in e.rightData) {
                    var partsSalesOrderDetail = new PartsSalesOrderDetail {
                        SparePartId = data.SparePartId,
                        SparePartCode = data.SparePartCode,
                        SparePartName = data.SparePartName,
                        EstimatedFulfillTime = data.EstimatedFulfillTime,
                        OrderedQuantity = data.OrderedQuantity,
                        IfCanNewPart = true,
                        Remark = data.Remark
                    };
                    partsSalesOrder.PartsSalesOrderDetails.Add(partsSalesOrderDetail);
                }
                var sparePartIds = partsSalesOrder.PartsSalesOrderDetails.Select(r => r.SparePartId).ToArray();
                this.DomainContext.Load(this.DomainContext.根据销售类型查询配件销售价2Query(partsSalesOrder.SalesCategoryId, partsSalesOrder.PartsSalesOrderTypeId, partsSalesOrder.SubmitCompanyId, partsSalesOrder.IfDirectProvision, sparePartIds, partsSalesOrder.SalesUnitOwnerCompanyId, partsSalesOrder.WarehouseId ?? 0), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        return;
                    }
                    if(loadOp.Entities != null && loadOp.Entities.Any()) {
                        var entity = loadOp.Entities.FirstOrDefault();
                        if(entity != null) {
                            foreach(var partsSalesOrderDetails in partsSalesOrder.PartsSalesOrderDetails) {
                                var partsOrderPrice = loadOp.Entities.FirstOrDefault(r => r.SparePartId == partsSalesOrderDetails.SparePartId);
                                if(partsOrderPrice != null) {
                                    if(partsOrderPrice.PartsTreatyPrice != null)
                                        partsSalesOrderDetails.OrderPrice = partsOrderPrice.PartsTreatyPrice.Value;
                                    else
                                        partsSalesOrderDetails.OrderPrice = partsOrderPrice.Price;
                                    partsSalesOrderDetails.MeasureUnit = partsOrderPrice.MeasureUnit;
                                    partsSalesOrderDetails.CustOrderPriceGradeCoefficient = partsOrderPrice.Coefficient;
                                    partsSalesOrderDetails.OriginalPrice = partsOrderPrice.RetailGuidePrice;
                                    partsSalesOrderDetails.DiscountedPrice = partsSalesOrderDetails.OriginalPrice - partsSalesOrderDetails.OrderPrice;
                                    partsSalesOrderDetails.OrderSum = partsSalesOrderDetails.OrderedQuantity * partsSalesOrderDetails.OrderPrice;
                                    partsSalesOrderDetails.PriceTypeName = partsOrderPrice.PriceTypeName;
                                    partsSalesOrderDetails.ABCStrategy = partsOrderPrice.ABCStrategy;
                                    partsSalesOrderDetails.SalesPrice = partsOrderPrice.SalesPrice;
                                    //partsSalesOrderDetails.MInSaleingAmount = partsOrderPrice.MInPackingAmount;
                                }
                            }
                        }
                        partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
                    }
                }, null);
                if(partsSalesOrder.PartsSalesOrderTypeName == "正常订单") {
                    this.DomainContext.Load(this.DomainContext.GetSparePartsByIdsQuery(sparePartIds).Where(o => o.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp1 => {
                        if(loadOp1.Entities == null || !loadOp1.Entities.Any()) {
                            UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_NoSparepart);
                            return;
                        }   
                        if(loadOp1.Entities != null && loadOp1.Entities.Any()) {
                            var spareParts = loadOp1.Entities.ToArray();
                            foreach(var detail in partsSalesOrder.PartsSalesOrderDetails) {
                                var sparePart = spareParts.FirstOrDefault(o => o.Id == detail.SparePartId);
                                if(sparePart != null) {
                                    detail.MInSaleingAmount = sparePart.MInPackingAmount.Value;
                                }
                            }
                        }
                    }, null);
                } else {
                    this.DomainContext.Load(this.DomainContext.获取当前企业的配件营销包装属性Query(partsSalesOrder.SalesCategoryId, partsSalesOrder.BranchId, sparePartIds), LoadBehavior.RefreshCurrent, loadOp2 => {
                        if(loadOp2.Entities == null || !loadOp2.Entities.Any()) {
                            UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_NoPartsBranchPackingPop);
                            return;
                        }
                        if(loadOp2.Entities != null && loadOp2.Entities.Any()) {
                            var partsBranchPackingProps = loadOp2.Entities.ToArray();

                            foreach(var detail in partsSalesOrder.PartsSalesOrderDetails) {
                                var partsBranchPackingProp = partsBranchPackingProps.FirstOrDefault(o => o.SparePartId == detail.SparePartId);
                                if(partsBranchPackingProp != null) {
                                    detail.MInSaleingAmount = partsBranchPackingProp.PackingCoefficient.Value;
                                }
                            }
                        }
                    }, null);
                }
            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowNotification(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));

        }

        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage( PartsSalesUIStrings.DataEdit_Validation_Import_CloseFile);
                    e.Handled = true;
                }
            }
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }

        private readonly string[] kvNames = {
            "PartsSalesOrder_SecondLevelOrderType", "PartsShipping_Method"
        };

        public PartsSalesOrderDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += PartsSalesOrderDataEditView_DataContextChanged;
            this.Loaded += PartsSalesOrderDataEditView_Loaded;
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetTiledRegionsQuery().Where(ex => ex.CountyName != null).OrderBy(ex => ex.ProvinceName), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var repairObject in loadOp.Entities)
                    this.TiledRegions.Add(new TiledRegion {
                        Id = repairObject.Id,
                        ProvinceName = repairObject.ProvinceName,
                        CityName = repairObject.CityName,

                    });
                foreach(var item in TiledRegions) {
                    var values = KvProvinceNames.Select(e => e.Value);
                    if(values.Contains(item.ProvinceName))
                        continue;
                    KvProvinceNames.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.ProvinceName
                    });
                }
                this.DomainContext.Load(this.DomainContext.getCenterAuthorizedProvincesForAgencyQuery(BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp1 => {
                    if(loadOp1.HasError)
                        return;
                    this.KvProvinceNames.Clear();
                    foreach(var centerAithorized in loadOp1.Entities) {
                        KvProvinceNames.Add(new KeyValuePair {
                            Key = centerAithorized.ProvinceId,
                            Value = centerAithorized.ProvinceName
                        });
                    }
                }, null);
            }, null);

            dcsDomainContext.Load(dcsDomainContext.GetKeyValueItemsQuery().Where(r => r.Name == "PartsShipping_Method" && r.Key != (int)DcsPartsShippingMethod.DHL_出口 && r.Key != (int)DcsPartsShippingMethod.海运_出口 && r.Key != (int)DcsPartsShippingMethod.空运_出口 && r.Key != (int)DcsPartsShippingMethod.铁路_出口), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvPartsSalesOrderTypes.Clear();
                foreach(var partsSalesOrderType in loadOp.Entities) {
                    this.KvShippingMethods.Add(new KeyValuePair {
                        Key = partsSalesOrderType.Key,
                        Value = partsSalesOrderType.Value,
                    });
                }
            }, null);
        }
        private void PartsSalesOrderDataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(r => r.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                if(loadOp.Entities != null && loadOp.Entities.Any()) {
                    currentCompanyType = loadOp.Entities.First().Type;
                }
            }, null);
            if(this.KvCompanyAddresses.Count == 1) {
                var partsSalesOrder = this.DataContext as PartsSalesOrder;
                if(partsSalesOrder != null)
                    if(string.IsNullOrEmpty(partsSalesOrder.ReceivingAddress)) {
                        partsSalesOrder.ReceivingAddress = this.KvCompanyAddresses.First().DetailAddress;
                        partsSalesOrder.ContactPerson = this.KvCompanyAddresses.First().ContactPerson;
                        partsSalesOrder.ContactPhone = this.KvCompanyAddresses.First().ContactPhone;
                        partsSalesOrder.CompanyAddressId = this.KvCompanyAddresses.First().Id;
                    }
            }
            this.blockReturnWarehouse.Visibility = Visibility.Collapsed;
            this.comboxReceivingWarehouse.Visibility = Visibility.Collapsed;
            if(this.receivingCompanyType != (int)DcsCompanyType.代理库 && this.receivingCompanyType != (int)DcsCompanyType.服务站兼代理库)
                return;
            {
                var partsSalesOrder = this.DataContext as PartsSalesOrder;
                if(partsSalesOrder != null && partsSalesOrder.ReceivingCompanyId != default(int)) {
                    this.blockReturnWarehouse.Visibility = Visibility.Visible;
                    this.comboxReceivingWarehouse.Visibility = Visibility.Visible;
                }
            }
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        public ObservableCollection<KeyValuePair> KvReceiverSalesCateNames {
            get {
                return kvReceiverSalesCateNames;
            }
        }

        public ObservableCollection<KeyValuePair> KvReceivingWarehouseNames {
            get {
                return kvReceivingWarehouseNames;
            }
        }

        private DataGridViewBase PartsSalesOrderDetailForEditDataGridView {
            get {
                if(this.partsSalesOrderDetailForEditDataGridView == null) {
                    this.partsSalesOrderDetailForEditDataGridView = DI.GetDataGridView("PartsSalesOrderDetailForEdit");
                    this.partsSalesOrderDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsSalesOrderDetailForEditDataGridView;
            }
        }

        #region 注释
        /*
        private RadWindow radProxyStockWindow;
        private RadWindow RadProxyStockWindow {
            get {
                return this.radProxyStockWindow ?? (this.radProxyStockWindow = new RadWindow {
                    Content = this.ProxyStockQueryWindow,
                    Header = "代理库库存查询",
                    WindowState = WindowState.Normal,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                    AllowDrop = true,
                    ResizeMode = ResizeMode.CanResize,
                    CanClose = true,
                    Width = 800
                });
            }
        }

        private ProxyStockQueryWindow proxyStockQueryWindow;
        private ProxyStockQueryWindow ProxyStockQueryWindow {
            get {
                if(proxyStockQueryWindow == null) {
                    proxyStockQueryWindow = new ProxyStockQueryWindow();
                    proxyStockQueryWindow.Loaded += this.proxyStockQueryWindow_Loaded;
                    proxyStockQueryWindow.SelectionDecided += proxyStockQueryWindow_SelectionDecided;
                }
                return this.proxyStockQueryWindow;
            }
        }
         * 
        private void proxyStockQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetWarehousesQuery().Where(s => s.Id == partsSalesOrder.WarehouseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity == null)
                    return;
                partsSalesOrder.WarehouseName = entity.Name;
                var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
                if(queryWindow == null || partsSalesOrder == null)
                    return;
                var compositeFilterItem = new CompositeFilterItem();
                queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                     "Common", "WarehouseName", partsSalesOrder.WarehouseName
                });
                if(partsSalesOrder.SalesUnit != null) {
                    compositeFilterItem.Filters.Add(new FilterItem("PartsSalesCategoryId", typeof(int), FilterOperator.IsEqualTo, partsSalesOrder.SalesUnit.PartsSalesCategoryId));
                }
                compositeFilterItem.Filters.Add(new FilterItem("PartsSalesOrderTypeId", typeof(int), FilterOperator.IsEqualTo, partsSalesOrder.PartsSalesOrderTypeId));
                compositeFilterItem.Filters.Add(new FilterItem("CustomerId", typeof(int), FilterOperator.IsEqualTo, partsSalesOrder.SubmitCompanyId));
                compositeFilterItem.Filters.Add(new FilterItem("WarehouseId", typeof(int), FilterOperator.IsEqualTo, partsSalesOrder.WarehouseId));
                queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
            }, null);
        }

        private void proxyStockQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var virtualPartsSalesPrices = queryWindow.SelectedEntities.Cast<VirtualPartsSalesPriceWithStock>();
            if(virtualPartsSalesPrices == null)
                return;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            foreach(var virtualPartsSalesPrice in virtualPartsSalesPrices) {
                if(partsSalesOrder.PartsSalesOrderDetails.Any(r => r.SparePartId == virtualPartsSalesPrice.SparePartId)) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataGridView_Validation_PartsSalesOrderDetail_SparePartCode);
                    return;
                }
                var partsSalesOrderDetail = new PartsSalesOrderDetail();
                partsSalesOrderDetail.ApproveQuantity = default(int);
                partsSalesOrderDetail.SparePartId = virtualPartsSalesPrice.SparePartId;
                partsSalesOrderDetail.SparePartCode = virtualPartsSalesPrice.SparePartCode;
                partsSalesOrderDetail.SparePartName = virtualPartsSalesPrice.SparePartName;
                partsSalesOrderDetail.OrderedQuantity = 1;
                partsSalesOrderDetail.OriginalPrice = virtualPartsSalesPrice.SpecialTreatyPrice.HasValue ? (virtualPartsSalesPrice.SpecialTreatyPrice == default(decimal) ? virtualPartsSalesPrice.SalesPrice : virtualPartsSalesPrice.SpecialTreatyPrice.Value) : virtualPartsSalesPrice.SalesPrice;
                partsSalesOrderDetail.CustOrderPriceGradeCoefficient = virtualPartsSalesPrice.PriceGradeCoefficient;
                partsSalesOrderDetail.OrderPrice = virtualPartsSalesPrice.SalesPrice * (decimal)virtualPartsSalesPrice.PriceGradeCoefficient;
                partsSalesOrderDetail.OrderSum = partsSalesOrderDetail.OrderPrice * partsSalesOrderDetail.OrderedQuantity;
                partsSalesOrderDetail.IfCanNewPart = true;
                partsSalesOrderDetail.MeasureUnit = virtualPartsSalesPrice.MeasureUnit;
                partsSalesOrder.PartsSalesOrderDetails.Add(partsSalesOrderDetail);
            }
            partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }
         
        //代理库库存选择
        private void ShowProxyStock() {

            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            if(partsSalesOrder.BranchId == default(int)) {
                UIHelper.ShowNotification("提报单位名称不能为空");
                return;
            }
            if(partsSalesOrder.SalesCategoryId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_SalesCategoryIdIsNull);
                return;
            }
            if(!partsSalesOrder.WarehouseId.HasValue || partsSalesOrder.WarehouseId.Value == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_SalesUnit_WarehouseIsNull2);
                return;
            }
            if(partsSalesOrder.PartsSalesOrderTypeId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_PartsSalesOrderTypeIdIsNull2);
                return;
            }
            this.RadProxyStockWindow.ShowDialog();
        }*/
        #endregion

        private void CreateUI() {
            this.KeyValueManager.LoadData();
            this.SubRoot.Children.Add(this.CreateVerticalLine(2));
            var detailEditView = new DcsDetailDataEditView();
            detailEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsSalesOrder), "PartsSalesOrderDetails"), null, () => this.PartsSalesOrderDetailForEditDataGridView);
            detailEditView.SetValue(Grid.ColumnProperty, 3);
            detailEditView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.DataEditPanel_Action_ImportNew,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                Command = new DelegateCommand(() => this.Uploader.ShowFileDialog())
            });
            detailEditView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.DataEditPanel_Action_Export,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                Command = this.ExportTemplateCommand
            });
            //detailEditView.RegisterButton(new ButtonItem {
            //    Title = "代理库库存选择",
            //    Command = new DelegateCommand(this.ShowProxyStock)
            //}, true);
            this.SubRoot.Children.Add(detailEditView);
            //提报单位名称
            var customerInformationBySparepart = DI.GetQueryWindow("CustomerInformationBySparepart");
            customerInformationBySparepart.SelectionDecided += this.QueryWindowForCustomerInformationBySparepart_SelectionDecided;
            customerInformationBySparepart.Loaded += customerInformationBySparepart_Loaded;
            this.popupTextBoxSubmitCompanyName.PopupContent = customerInformationBySparepart;
            //收货单位名称
            var receivingCompanyBySparepart = DI.GetQueryWindow("CustomerInformationBySparepart");
            receivingCompanyBySparepart.SelectionDecided += receivingCompanyBySparepart_SelectionDecided;
            receivingCompanyBySparepart.Loaded += customerInformationBySparepart_Loaded;
            this.popupTextBoxReceivingCompanyName.PopupContent = receivingCompanyBySparepart;

            var queryWindow = DI.GetQueryWindow("CustomerInformationBySparepart");
            queryWindow.SelectionDecided += queryWindow_SelectionDecided;
            this.provinceNameRadCombox.SelectionChanged += provinceNameRadCombox_SelectionChanged;
            //this.comboBoxSalesUnitName.SelectionChanged += this.ComboBoxSalesUnitName_SelectionChanged;
            this.DomainContext.Load(this.DomainContext.GetSalesUnitsQuery().Where(r => r.OwnerCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                this.KvSalesUnits.Clear();
                foreach(var entity in loadOp.Entities) {
                    this.KvSalesUnits.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name,
                        UserObject = entity
                    });
                }
            }, null);
        }

        //收货单位选择
        private void receivingCompanyBySparepart_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;
            if(queryWindow.SelectedEntities == null || !queryWindow.SelectedEntities.Any())
                return;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            var customerInformation = queryWindow.SelectedEntities.Cast<CustomerInformation>().FirstOrDefault();
            if(customerInformation == null)
                return;
            //收货
            partsSalesOrder.ReceivingCompanyId = customerInformation.CustomerCompany.Id;
            partsSalesOrder.ReceivingCompanyCode = customerInformation.CustomerCompany.Code;
            partsSalesOrder.ReceivingCompanyName = customerInformation.CustomerCompany.Name;

            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        void provinceNameRadCombox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            this.KvCityNames.Clear();
            foreach(var item in TiledRegions.Where(ex => ex.ProvinceName == provinceNameRadCombox.Text).OrderBy(ex => ex.CityName)) {
                var values = KvCityNames.Select(ex => ex.Value);
                if(values.Contains(item.CityName))
                    continue;
                KvCityNames.Add(new KeyValuePair {
                    Key = item.Id,
                    Value = item.CityName
                });
            }
        }
        private ICommand exportFileCommand;

        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private const string EXPORT_DATA_FILE_NAME = "导出配件销售订单清单模板.xlsx";

        private void InitializeCommand() {
            this.exportFileCommand = new DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                    new ImportTemplateColumn {
                        Name=PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                         Name=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                    },
                    new ImportTemplateColumn {
                        Name=PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_OrderQuntity,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = Name=PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_MeetingTime,
                    },
                    new ImportTemplateColumn {
                         Name=PartsSalesUIStrings.DataEditView_Text_SecondClassStationPlan_Remark,
                    },
                    new ImportTemplateColumn {
                        Name = Name=PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_OrderCode,
                    }
                };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }

        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }

        /// <summary>
        /// 根据条件查询本企业和下属企业地址
        /// </summary>
        /// <param name="isBranch">是为包含下属企业</param>
        /// <param name="companyId">companyId</param>
        private void InitCompanyAddress(bool isBranch, int companyId) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            if(isBranch) {
                //查询企业收获地址
                this.DomainContext.Load(this.DomainContext.GetCompanyAddressWithAgencyDealerRelationsQuery(partsSalesOrder.SalesCategoryId, companyId), LoadBehavior.RefreshCurrent, loadOption => {
                    if(loadOption.HasError) {
                        loadOption.MarkErrorAsHandled();
                        return;
                    }
                    this.KvCompanyAddresses.Clear();
                    foreach(var entity in loadOption.Entities) {
                        this.KvCompanyAddresses.Add(entity);
                    }
                    //默认选中提报单位所对应的收货地址
                    if(loadOption.Entities != null && loadOption.Entities.Count() >= 1) {
                        if(string.IsNullOrEmpty(partsSalesOrder.ReceivingAddress)) {
                            var tihsAddress = loadOption.Entities.Where(r => r.CompanyId == companyId).FirstOrDefault();
                            if (tihsAddress != null) {
                                partsSalesOrder.ReceivingAddress = tihsAddress.DetailAddress;
                                partsSalesOrder.ContactPerson = tihsAddress.ContactPerson;
                                partsSalesOrder.ContactPhone = tihsAddress.ContactPhone;
                                partsSalesOrder.CompanyAddressId = tihsAddress.Id;
                            }
                        }
                    }
                }, null);
            } else {
                //查询企业收货地址
                this.DomainContext.Load(this.DomainContext.GetCompanyAddressesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.CompanyId == companyId), LoadBehavior.RefreshCurrent, loadOption => {
                    if(loadOption.HasError) {
                        loadOption.MarkErrorAsHandled();
                        return;
                    }
                    this.KvCompanyAddresses.Clear();
                    foreach(var entity in loadOption.Entities) {
                        this.KvCompanyAddresses.Add(entity);
                    }
                    if(loadOption.Entities != null && loadOption.Entities.Count() >= 1) {
                        if(string.IsNullOrEmpty(partsSalesOrder.ReceivingAddress)) {
                            partsSalesOrder.ReceivingAddress = loadOption.Entities.First().DetailAddress;
                            partsSalesOrder.ContactPerson = loadOption.Entities.First().ContactPerson;
                            partsSalesOrder.ContactPhone = loadOption.Entities.First().ContactPhone;
                            partsSalesOrder.CompanyAddressId = loadOption.Entities.First().Id;
                        }
                    }
                }, null);
            }
            if(partsSalesOrder.PartsSalesOrderTypeName != "正常订单") {
                if(this.cbAddress != null)
                    this.cbAddress.IsEditable = true;
            } else {
                if(this.cbAddress != null)
                    this.cbAddress.IsEditable = false;
            }
        }

        #region 界面事件
        private void PartsSalesOrderDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            this.KvPartsSalesOrderTypes.Clear();
            this.KvCompanyAddresses.Clear();
            checkBoxIfDirectProvision.IsEnabled = false;
            partsSalesOrder.PropertyChanged -= this.PartsSalesOrder_PropertyChanged;
            partsSalesOrder.PropertyChanged += this.PartsSalesOrder_PropertyChanged;
            partsSalesOrder.PartsSalesOrderDetails.EntityRemoved -= this.PartsSalesOrderDetails_EntityRemoved;
            partsSalesOrder.PartsSalesOrderDetails.EntityRemoved += this.PartsSalesOrderDetails_EntityRemoved;
        }

        private void PartsSalesOrderDetails_EntityRemoved(object sender, EntityCollectionChangedEventArgs<PartsSalesOrderDetail> e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
        }

        private void PartsSalesOrder_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            switch(e.PropertyName) {
                case "SubmitCompanyId":
                    //根据提报单位是否为代理库或服务站兼代理库要求退货仓库为必填
                    this.domainContext.Load(this.domainContext.GetCompaniesQuery().Where(ex => ex.Id == partsSalesOrder.SubmitCompanyId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError)
                            return;
                        var company = loadOp.Entities.FirstOrDefault();
                        if(company == null)
                            return;
                        this.companys = company;
                        //查询企业收获地址
                        this.loadCompanyAddress();
                    }, null);
                    break;
                case "PartsSalesOrderTypeId":
                    if(this.oldPartsSalesOrderTypeId != partsSalesOrder.PartsSalesOrderTypeId) {
                        if(partsSalesOrder.PartsSalesOrderTypeId == 0)
                            return;
                        this.DomainContext.Load(this.DomainContext.GetCustomerCompanyAndPartsSalesOrderTypeQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesOrderTypeId == partsSalesOrder.PartsSalesOrderTypeId && r.CustomerCompanyId == partsSalesOrder.SubmitCompanyId && r.PartsSalesCategoryId == partsSalesOrder.PartsSalesCategory.Id), LoadBehavior.RefreshCurrent, loadOp => {
                            if(loadOp.Entities == null || !loadOp.Entities.Any()) {
                                partsSalesOrder.PartsSalesOrderTypeId = 0;
                                UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEditView_Confirm_PartsSalesOrder_PartsSalesOrderTypeNameisNull);
                                return;
                            }
                            if(partsSalesOrder.PartsSalesOrderTypeName == "正常订单" || partsSalesOrder.PartsSalesOrderTypeName == "油品订单")
                                partsSalesOrder.ShippingMethod = (int)DcsPartsShippingMethod.公路物流;
                            if(partsSalesOrder.PartsSalesOrderTypeName == "油品订单")
                                partsSalesOrder.IfDirectProvision = true;
                            else
                                partsSalesOrder.IfDirectProvision = false;

                            if(!partsSalesOrder.PartsSalesOrderDetails.Any()) {
                                this.oldPartsSalesOrderTypeId = partsSalesOrder.PartsSalesOrderTypeId;
                                this.loadCompanyAddress();
                                return;
                            }

                            if(partsSalesOrder.PartsSalesOrderTypeName == "油品订单") {
                                foreach(var detail in partsSalesOrder.PartsSalesOrderDetails.ToArray()) {
                                    partsSalesOrder.PartsSalesOrderDetails.Remove(detail);
                                }
                            }
                            if(partsSalesOrder.PartsSalesOrderTypeName != "油品订单") {
                                DcsUtils.Confirm(PartsSalesUIStrings.DataEditView_Confirm_RebuildPartsSalesPrice, () => {
                                    //订单类型变更，重新计算订货价格
                                    this.DomainContext.Load(this.DomainContext.根据销售类型查询配件销售价3Query(partsSalesOrder.SalesCategoryId, partsSalesOrder.PartsSalesOrderTypeId, partsSalesOrder.SubmitCompanyId, partsSalesOrder.IfDirectProvision, partsSalesOrder.SalesUnitOwnerCompanyId, partsSalesOrder.WarehouseId ?? 0, null, null, null, null), LoadBehavior.RefreshCurrent, loadOp1 => {
                                        if(loadOp1.HasError)
                                            return;
                                        foreach(var detail in partsSalesOrder.PartsSalesOrderDetails) {
                                            var virtualPartsSalesPrice = loadOp1.Entities.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                                            if(virtualPartsSalesPrice != null) {
                                                if(virtualPartsSalesPrice.PartsTreatyPrice != null)
                                                    detail.OrderPrice = virtualPartsSalesPrice.PartsTreatyPrice.Value;
                                                else
                                                    detail.OrderPrice = virtualPartsSalesPrice.Price;
                                                detail.MeasureUnit = virtualPartsSalesPrice.MeasureUnit;
                                                detail.CustOrderPriceGradeCoefficient = virtualPartsSalesPrice.Coefficient;
                                                detail.OriginalPrice = virtualPartsSalesPrice.RetailGuidePrice;
                                                detail.DiscountedPrice = detail.OriginalPrice - detail.OrderPrice;
                                                detail.OrderSum = detail.OrderedQuantity * detail.OrderPrice;
                                                detail.PriceTypeName = virtualPartsSalesPrice.PriceTypeName;
                                                detail.ABCStrategy = virtualPartsSalesPrice.ABCStrategy;
                                            }
                                        }
                                        partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
                                        this.loadCompanyAddress();
                                    }, null);
                                    this.oldPartsSalesOrderTypeId = partsSalesOrder.PartsSalesOrderTypeId;
                                    if(partsSalesOrder.PartsSalesOrderTypeName == "正常订单") {
                                        var sparePartIds = partsSalesOrder.PartsSalesOrderDetails.Select(o => o.SparePartId).Distinct().ToArray();
                                        this.DomainContext.Load(this.DomainContext.GetSparePartsByIdsQuery(sparePartIds).Where(o => o.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp1 => {
                                            if(loadOp1.Entities == null || !loadOp1.Entities.Any()) {
                                                UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_NoSparepart);
                                                return;
                                            }
                                            if(loadOp1.Entities != null && loadOp1.Entities.Any()) {
                                                var spareParts = loadOp1.Entities.ToArray();
                                                foreach(var detail in partsSalesOrder.PartsSalesOrderDetails) {
                                                    var sparePart = spareParts.FirstOrDefault(o => o.Id == detail.SparePartId);
                                                    if(sparePart != null) {
                                                        detail.MInSaleingAmount = sparePart.MInPackingAmount.Value;
                                                    }
                                                }
                                            }
                                        }, null);
                                    } else {
                                        var sparePartIds = partsSalesOrder.PartsSalesOrderDetails.Select(o => o.SparePartId).Distinct().ToArray();
                                        this.DomainContext.Load(this.DomainContext.获取当前企业的配件营销包装属性Query(partsSalesOrder.SalesCategoryId, partsSalesOrder.BranchId, sparePartIds), LoadBehavior.RefreshCurrent, loadOp2 => {
                                            if(loadOp2.Entities == null || !loadOp2.Entities.Any()) {
                                                UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_NoPartsBranchPackingPop);
                                                return;
                                            }
                                            if(loadOp2.Entities != null && loadOp2.Entities.Any()) {
                                                var partsBranchPackingProps = loadOp2.Entities.ToArray();

                                                foreach(var detail in partsSalesOrder.PartsSalesOrderDetails) {
                                                    var partsBranchPackingProp = partsBranchPackingProps.FirstOrDefault(o => o.SparePartId == detail.SparePartId);
                                                    if(partsBranchPackingProp != null) {
                                                        detail.MInSaleingAmount = partsBranchPackingProp.PackingCoefficient.Value;
                                                    }
                                                }
                                            }
                                        }, null);
                                    }
                                }, () => {
                                    partsSalesOrder.PartsSalesOrderTypeId = this.oldPartsSalesOrderTypeId;
                                });
                            }
                        }, null);
                    }
                    break;
                case "IfDirectProvision":
                    //是否直供改变。 提示是否清空清单
                    if(partsSalesOrder.PartsSalesOrderDetails.Any() && isIfDirectProvision)
                        DcsUtils.Confirm(PartsSalesUIStrings.DataEdit_tips_PartsSalesOrder_IfDirectProvision, () => {
                            var oldList = partsSalesOrder.PartsSalesOrderDetails.ToList();
                            //清空清单
                            foreach(var partsSalesOrderDetails in oldList) {
                                partsSalesOrderDetails.ValidationErrors.Clear();
                                partsSalesOrder.PartsSalesOrderDetails.Remove(partsSalesOrderDetails);
                            }
                        }, () => {
                            //不改变
                            isIfDirectProvision = false;
                            partsSalesOrder.IfDirectProvision = !partsSalesOrder.IfDirectProvision;
                        });
                    if(partsSalesOrder.ValidationErrors.Any(entity => entity.MemberNames.Contains("RequestedDeliveryTime")))
                        partsSalesOrder.ValidationErrors.Remove(partsSalesOrder.ValidationErrors.FirstOrDefault(entity => entity.MemberNames.Contains("RequestedDeliveryTime")));
                    else if(partsSalesOrder.IfDirectProvision && !partsSalesOrder.RequestedDeliveryTime.HasValue)
                        partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_IfDirectProvisionThenRequestedDeliveryTimeIsNotNull, new[] {
                            "RequestedDeliveryTime"
                        }));
                    isIfDirectProvision = true;
                    break;
                case "SalesCategoryId":
                    partsSalesOrder.SalesUnitId = 0;
                    partsSalesOrder.SalesUnitCode = string.Empty;
                    partsSalesOrder.SalesUnitName = string.Empty;
                    txtUseablePosition.Text = "￥" + "0.00";
                    orderUseablePosition.Text = "￥" + "0.00";
                    txtRebateAmount.Text = "￥" + "0.00";
                    //获取收货仓库数据源
                    partsSalesOrder.ReceivingWarehouseId = null;
                    partsSalesOrder.ReceivingWarehouseName = null;
                    //只有在登陆企业为代理库或服务站兼代理库时退货仓库控件可见，收货仓库修改以后清空退货单清单与销售订单编号(第一次选择退货仓库时不清空销售订单编号)，
                    //根据选择的销售组织.配件销售类型id 获取销售组织（销售组织.隶属企业Id=登陆企业Id,销售组织.配件销售类型Id=选择的配件销售类型Id）
                    //同时获取销售组织仓库关系（销售组织仓库关系.销售组织Id=获取的销售组织Id） 得到仓库Id过滤仓库数据源
                    this.domainContext.Load(this.domainContext.GetSalesUnitsQuery().Where(ex => ex.Status == (int)DcsMasterDataStatus.有效 && ex.OwnerCompanyId == partsSalesOrder.ReceivingCompanyId && ex.PartsSalesCategoryId == partsSalesOrder.SalesCategoryId), LoadBehavior.RefreshCurrent, loadOp2 => {
                        if(loadOp2.HasError) {
                            loadOp2.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp2);
                            return;
                        }
                        var entity = loadOp2.Entities.FirstOrDefault();
                        if(entity != null) {
                            this.domainContext.Load(this.domainContext.GetWarehousesBySalesUnitIdQuery(entity.Id), LoadBehavior.RefreshCurrent, loadOp1 => {
                                if(loadOp1.HasError) {
                                    loadOp1.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOp1);
                                    return;
                                }
                                this.KvReceivingWarehouse.Clear();
                                if(loadOp1.Entities != null && loadOp1.Entities.Any()) {
                                    foreach(var warehouses in loadOp1.Entities) {
                                        this.KvReceivingWarehouse.Add(new KeyValuePair {
                                            Key = warehouses.Id,
                                            Value = warehouses.Name,
                                            UserObject = warehouses
                                        });
                                    }
                                }
                            }, null);
                        }
                    }, null);
                    var details = partsSalesOrder.PartsSalesOrderDetails.ToList();
                    foreach(var detail in details)
                        partsSalesOrder.PartsSalesOrderDetails.Remove(detail);
                    this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Id == partsSalesOrder.SalesCategoryId), LoadBehavior.RefreshCurrent, loadOptionM => {
                        if(loadOptionM.HasError) {
                            loadOptionM.MarkErrorAsHandled();
                            return;
                        }
                        if(loadOptionM.Entities != null && loadOptionM.Entities.Any()) {
                            partsSalesOrder.BranchId = loadOptionM.Entities.First().BranchId;
                            partsSalesOrder.BranchCode = loadOptionM.Entities.First().BranchCode;
                            partsSalesOrder.BranchName = loadOptionM.Entities.First().BranchName;
                        }
                        if(currentCompanyType == (int)DcsCompanyType.代理库 || currentCompanyType == (int)DcsCompanyType.服务站兼代理库) {
                            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Id == partsSalesOrder.SalesCategoryId), LoadBehavior.RefreshCurrent, loadOption => {
                                if(loadOption.HasError) {
                                    loadOption.MarkErrorAsHandled();
                                    return;
                                }
                                if(loadOption.Entities != null && loadOption.Entities.Any()) {
                                    this.LoadWarehouse(partsSalesOrder.SubmitCompanyId, loadOption.Entities.First().BranchId, partsSalesOrder.SalesCategoryId);
                                }
                            }, null);
                        } else {
                            this.LoadWarehouse(partsSalesOrder.SubmitCompanyId, partsSalesOrder.BranchId, partsSalesOrder.SalesCategoryId);
                        }
                      //  this.LoadWarehouse(partsSalesOrder.SubmitCompanyId, partsSalesOrder.BranchId, partsSalesOrder.SalesCategoryId);
                        this.notApprovePrice.Text = "￥0.00";
                        this.lnotApprovePrice = 0;
                        partsSalesOrder.CustomerAccount = null;
                        partsSalesOrder.PartsSalesOrderTypeId = 0;
                        partsSalesOrder.PartsSalesOrderTypeName = string.Empty;
                        this.DomainContext.Load(this.DomainContext.GetPartsSalesOrderTypeWithCustomerOrderPriceGradesQuery(partsSalesOrder.SalesCategoryId, partsSalesOrder.SubmitCompanyId), LoadBehavior.RefreshCurrent, loadOp => {
                            if(loadOp.HasError)
                                return;
                            this.KvPartsSalesOrderTypes.Clear();
                            foreach(var partsSalesOrderType in loadOp.Entities) {
                                this.KvPartsSalesOrderTypes.Add(new KeyValuePair {
                                    Key = partsSalesOrderType.Id,
                                    Value = partsSalesOrderType.Name,
                                });
                            }
                        }, null);
                        if(partsSalesOrder.InvoiceReceiveCompanyType == (int)DcsCompanyType.代理库 || partsSalesOrder.InvoiceReceiveCompanyType == (int)DcsCompanyType.服务站兼代理库) {

                            //this.DomainContext.Load(this.DomainContext.查询销售组织仓库Query(partsSalesOrder.SubmitCompanyId, partsSalesOrder.BranchId, null, partsSalesOrder.SalesCategoryId, currentCompanyType).Where(r => r.Warehouse.Type != (int)DcsWarehouseType.虚拟库), LoadBehavior.RefreshCurrent, loadop => {
                            //    if(loadop.HasError) {
                            //        loadop.MarkErrorAsHandled();
                            //        return;
                            //    }
                            //    this.KvSalesUnitAffiWarehouses.Clear();
                            //    if(loadop.Entities != null)
                            //        foreach(var salesUnitAffiWarehouse in loadop.Entities)
                            //            if(salesUnitAffiWarehouse.Warehouse.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && salesUnitAffiWarehouse.Warehouse.IsQualityWarehouse != true)
                            //                this.KvSalesUnitAffiWarehouses.Add(salesUnitAffiWarehouse);

                            //    this.DomainContext.Load(this.DomainContext.GetWarehousesForSalesOrderReportQuery(partsSalesOrder.SalesCategoryId, partsSalesOrder.SubmitCompanyId), LoadBehavior.RefreshCurrent, loadOp3 => {
                            //        if(loadOp3.HasError) {
                            //            if(!loadOp3.IsErrorHandled)
                            //                loadOp3.MarkErrorAsHandled();
                            //            DcsUtils.ShowDomainServiceOperationWindow(loadOp3);
                            //            return;
                            //        }
                            //        this.DomainContext.Load(this.DomainContext.GetAgenciesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.Id == partsSalesOrder.SubmitCompanyId), LoadBehavior.RefreshCurrent, loadOption1 => {
                            //            if(loadOption1.HasError)
                            //                return;
                            //            if(loadOption1.Entities.Count() != 0) {
                            //                partsSalesOrder.WarehouseId = loadOption1.Entities.FirstOrDefault().WarehouseId;
                            //                partsSalesOrder.WarehouseName = loadOption1.Entities.FirstOrDefault().WarehouseName;
                            //            }
                            //            if(loadOp3.Entities != null && loadOp3.Entities.Count() >= 1) {
                            //                if(!partsSalesOrder.WarehouseId.HasValue || (partsSalesOrder.WarehouseId.HasValue && !loadOp3.Entities.Any(t => t.Id == partsSalesOrder.WarehouseId))) {
                            //                    partsSalesOrder.WarehouseId = loadOp3.Entities.First().Id;
                            //                    partsSalesOrder.WarehouseName = loadOp3.Entities.First().Name;
                            //                }
                            //            }
                            //            if(!loadOp3.Entities.Any()) {
                            //                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_tips_PartsSalesOrder_WarehouseUnit);
                            //                partsSalesOrder.WarehouseId = default(int);
                            //                partsSalesOrder.WarehouseName = string.Empty;
                            //            }
                            //        }, null);

                            //    }, null);
                            //}, null);
                        }
                    }, null);

                    break;
                case "WarehouseId":
                    checkBoxIfDirectProvision.IsEnabled = true;
                    var salesUnitKv = this.KvSalesUnitAffiWarehouses.FirstOrDefault(kv => kv.WarehouseId == partsSalesOrder.WarehouseId);
                    if(salesUnitKv == null)
                        return;
                    var salesUnitInsert = salesUnitKv;
                    if(salesUnitInsert.SalesUnit == null)
                        return;
                    partsSalesOrder.SalesUnitId = salesUnitInsert.SalesUnitId;
                    partsSalesOrder.SalesUnitCode = salesUnitInsert.SalesUnit.Code;
                    partsSalesOrder.SalesUnitName = salesUnitInsert.SalesUnit.Name;
                    partsSalesOrder.SalesUnitOwnerCompanyId = salesUnitInsert.SalesUnit.OwnerCompanyId;
                    var parameteras = new Dictionary<string, object>();
                    parameteras.Add("submitCompanyId", partsSalesOrder.SubmitCompanyId);
                    parameteras.Add("salesUnitId", partsSalesOrder.SalesUnitId);
                    parameteras.Add("partsSalesCategoryId", partsSalesOrder.SalesCategoryId);
                    this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(r => r.Id == salesUnitInsert.SalesUnit.OwnerCompanyId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            loadOp.MarkErrorAsHandled();
                            return;
                        }
                        if(loadOp.Entities != null && loadOp.Entities.Any()) {
                            partsSalesOrder.SalesUnitOwnerCompanyId = loadOp.Entities.First().Id;
                            partsSalesOrder.SalesUnitOwnerCompanyCode = loadOp.Entities.First().Code;
                            partsSalesOrder.SalesUnitOwnerCompanyName = loadOp.Entities.First().Name;
                            partsSalesOrder.SalesUnitOwnerCompanyType = loadOp.Entities.First().Type;
                        }
                    }, null);
                    this.DomainContext.InvokeOperation("查询未审核订单金额", typeof(decimal), parameteras, true, invokeOp => {
                        if(invokeOp.HasError)
                            return;
                        this.notApprovePrice.Text = invokeOp.Value.ToString();
                        this.lnotApprovePrice = decimal.Parse(invokeOp.Value.ToString());
                        this.DomainContext.Load(this.DomainContext.查询客户可用资金Query(null, salesUnitInsert.SalesUnit.AccountGroupId, partsSalesOrder.InvoiceReceiveCompanyId), LoadBehavior.RefreshCurrent, loadOption1 => {
                            if(loadOption1.HasError) {
                                loadOption1.MarkErrorAsHandled();
                                DcsUtils.ShowDomainServiceOperationWindow(loadOption1);
                                return;
                            }
                            partsSalesOrder.CustomerAccount = loadOption1.Entities.FirstOrDefault();
                            if(partsSalesOrder.CustomerAccount == null)
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Notification_PartsSalesOrder_CustomerAccountIsNull);
                            else {
                                this.DomainContext.Load(this.DomainContext.GetPartsRebateAccountsQuery().Where(r => r.AccountGroupId == partsSalesOrder.SalesUnit.AccountGroupId && r.CustomerCompanyId == partsSalesOrder.SubmitCompanyId && r.BranchId == salesUnitInsert.SalesUnit.OwnerCompanyId), LoadBehavior.RefreshCurrent, loadRebateAccount => {
                                    if(loadRebateAccount.HasError) {
                                        loadRebateAccount.MarkErrorAsHandled();
                                        DcsUtils.ShowDomainServiceOperationWindow(loadRebateAccount);
                                        return;
                                    }
                                    if(loadRebateAccount.Entities.Any()) {
                                        this.txtRebateAmount.Text = loadRebateAccount.Entities.First().AccountBalanceAmount.ToString("c2");
                                        this.lRebateAmount = loadRebateAccount.Entities.First().AccountBalanceAmount;
                                        this.txtUseablePosition.Text = (partsSalesOrder.CustomerAccount.UseablePosition).ToString("c2");
                                        //partsSalesOrder.CustomerAccount.UseablePosition = partsSalesOrder.CustomerAccount.UseablePosition + lRebateAmount;
                                    } else {
                                        this.txtRebateAmount.Text = "￥" + "0.00";
                                        this.txtUseablePosition.Text = partsSalesOrder.CustomerAccount.UseablePosition.ToString("c2");
                                    }
                                    this.txtCredenceAmount.Text = (partsSalesOrder.CustomerAccount.CredenceAmount + (partsSalesOrder.CustomerAccount.SIHCredit ?? 0)).ToString("c2");
                                    this.orderUseablePosition.Text = (partsSalesOrder.CustomerAccount.AccountBalance + lRebateAmount + partsSalesOrder.CustomerAccount.CustomerCredenceAmount + (partsSalesOrder.CustomerAccount.TempCreditTotalFee ?? 0) + (partsSalesOrder.CustomerAccount.SIHCredit ?? 0)- partsSalesOrder.CustomerAccount.LockBalance - lnotApprovePrice).ToString("C");
                                    this.lorderUseablePosition = partsSalesOrder.CustomerAccount.AccountBalance + lRebateAmount + partsSalesOrder.CustomerAccount.CustomerCredenceAmount + (partsSalesOrder.CustomerAccount.TempCreditTotalFee ?? 0) + (partsSalesOrder.CustomerAccount.SIHCredit ?? 0) - partsSalesOrder.CustomerAccount.LockBalance - lnotApprovePrice;
                                }, null);
                            }
                        }, null);
                    }, null);
                    break;
                case "ReceivingCompanyId":
                    this.domainContext.Load(this.domainContext.GetCompaniesQuery().Where(ex => ex.Id == partsSalesOrder.ReceivingCompanyId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError)
                            return;
                        var company = loadOp.Entities.FirstOrDefault();
                        if(company == null)
                            return;
                        this.receivingCompanyType = company.Type;
                        if(this.receivingCompanyType == (int)DcsCompanyType.代理库 || this.receivingCompanyType == (int)DcsCompanyType.服务站兼代理库) {
                            this.blockReturnWarehouse.Visibility = Visibility.Visible;
                            this.comboxReceivingWarehouse.Visibility = Visibility.Visible;
                            //只有在登陆企业为代理库或服务站兼代理库时退货仓库控件可见，收货仓库修改以后清空退货单清单与销售订单编号(第一次选择退货仓库时不清空销售订单编号)，
                            //根据选择的销售组织.配件销售类型id 获取销售组织（销售组织.隶属企业Id=登陆企业Id,销售组织.配件销售类型Id=选择的配件销售类型Id）
                            //同时获取销售组织仓库关系（销售组织仓库关系.销售组织Id=获取的销售组织Id） 得到仓库Id过滤仓库数据源
                            this.domainContext.Load(this.domainContext.GetSalesUnitsQuery().Where(ex => ex.Status == (int)DcsMasterDataStatus.有效 && ex.OwnerCompanyId == partsSalesOrder.ReceivingCompanyId && ex.PartsSalesCategoryId == partsSalesOrder.SalesCategoryId), LoadBehavior.RefreshCurrent, loadOp2 => {
                                if(loadOp2.HasError) {
                                    loadOp2.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOp2);
                                    return;
                                }
                                var entity = loadOp2.Entities.FirstOrDefault();
                                if(entity != null) {
                                    this.domainContext.Load(this.domainContext.GetWarehousesBySalesUnitIdQuery(entity.Id), LoadBehavior.RefreshCurrent, loadOp1 => {
                                        if(loadOp1.HasError) {
                                            loadOp1.MarkErrorAsHandled();
                                            DcsUtils.ShowDomainServiceOperationWindow(loadOp1);
                                            return;
                                        }
                                        this.KvReceivingWarehouse.Clear();
                                        if(loadOp1.Entities != null && loadOp1.Entities.Any()) {
                                            foreach(var warehouses in loadOp1.Entities) {
                                                this.KvReceivingWarehouse.Add(new KeyValuePair {
                                                    Key = warehouses.Id,
                                                    Value = warehouses.Name,
                                                    UserObject = warehouses
                                                });
                                            }
                                        }
                                    }, null);
                                }
                            }, null);
                        } else {
                            this.blockReturnWarehouse.Visibility = Visibility.Collapsed;
                            this.comboxReceivingWarehouse.Visibility = Visibility.Collapsed;
                        }
                        //查询企业收获地址
                        this.loadCompanyAddress();
                    }, null);
                    break;

            }
        }


        //加载企业地址
        private void loadCompanyAddress() {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            if(this.companys == null)
                return;
            if(this.companys.Type == (int)DcsCompanyType.代理库 && (partsSalesOrder.PartsSalesOrderTypeName == "紧急订单" || partsSalesOrder.PartsSalesOrderTypeName == "特急订单" || partsSalesOrder.PartsSalesOrderTypeName == "事故订单")) {
                this.InitCompanyAddress(true, partsSalesOrder.ReceivingCompanyId); //查询登陆企业和下属企业的收货地址。 
            } else {
                this.InitCompanyAddress(false, partsSalesOrder.ReceivingCompanyId);//只查询登陆企业的收货地址。
            }
        }

        private void LoadWarehouse(int submitCompanyId, int branchId, int salesCategoryId) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            this.DomainContext.Load(this.DomainContext.查询销售组织仓库Query(submitCompanyId, branchId, null, salesCategoryId, currentCompanyType).Where(r => r.Warehouse.Type != (int)DcsWarehouseType.虚拟库), LoadBehavior.RefreshCurrent, loadop => {
                if(loadop.HasError) {
                    loadop.MarkErrorAsHandled();
                    return;
                }
                this.KvSalesUnitAffiWarehouses.Clear();
                if(loadop.Entities != null)
                    foreach(var salesUnitAffiWarehouse in loadop.Entities)
                        if(salesUnitAffiWarehouse.Warehouse.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && salesUnitAffiWarehouse.Warehouse.IsQualityWarehouse != true)
                            this.KvSalesUnitAffiWarehouses.Add(salesUnitAffiWarehouse);
                if(partsSalesOrder.InvoiceReceiveCompanyType == (int)DcsCompanyType.代理库 || partsSalesOrder.InvoiceReceiveCompanyType == (int)DcsCompanyType.服务站兼代理库) {
                    this.DomainContext.Load(this.DomainContext.GetAgenciesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.Id == partsSalesOrder.SubmitCompanyId), LoadBehavior.RefreshCurrent, loadOption1 => {
                        if(loadOption1.HasError)
                            return;
                        if(loadOption1.Entities.Count() != 0) {
                            partsSalesOrder.WarehouseId = loadOption1.Entities.FirstOrDefault().WarehouseId;
                            partsSalesOrder.WarehouseName = loadOption1.Entities.FirstOrDefault().WarehouseName;
                        }
                        if(KvSalesUnitAffiWarehouses.Count() >= 1) {
                            if(!partsSalesOrder.WarehouseId.HasValue || (partsSalesOrder.WarehouseId.HasValue && !KvSalesUnitAffiWarehouses.Any(t => t.WarehouseId == partsSalesOrder.WarehouseId))) {
                                partsSalesOrder.WarehouseId = KvSalesUnitAffiWarehouses.First().WarehouseId;
                                partsSalesOrder.WarehouseName = KvSalesUnitAffiWarehouses.First().Warehouse.Name;
                            }
                        }
                        if(!KvSalesUnitAffiWarehouses.Any()) {
                          //  UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_tips_PartsSalesOrder_WarehouseUnit);
                            partsSalesOrder.WarehouseId = default(int);
                            partsSalesOrder.WarehouseName = string.Empty;
                        }
                    }, null);
                }
            }, null);
        }

        private void queryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;

            if(queryWindow.SelectedEntities == null || !queryWindow.SelectedEntities.Any() || queryWindow.SelectedEntities.Cast<PartsClaimPrice>() == null)
                return;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;

            var customerInformation = queryWindow.SelectedEntities.Cast<CustomerInformation>().FirstOrDefault();
            if(customerInformation == null)
                return;
            partsSalesOrder.ReceivingCompanyId = customerInformation.CustomerCompany.Id;
            partsSalesOrder.ReceivingCompanyCode = customerInformation.CustomerCompany.Code;
            partsSalesOrder.ReceivingCompanyName = customerInformation.CustomerCompany.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void QueryWindowForCustomerInformationBySparepart_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;

            if(queryWindow.SelectedEntities == null || !queryWindow.SelectedEntities.Any() || queryWindow.SelectedEntities.Cast<CustomerInformation>() == null)
                return;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            var customerInformation = queryWindow.SelectedEntities.Cast<CustomerInformation>().FirstOrDefault();
            if(customerInformation == null)
                return;
            if(partsSalesOrder.PartsSalesOrderDetails != null && partsSalesOrder.PartsSalesOrderDetails.Any()) {
                var detailList = partsSalesOrder.PartsSalesOrderDetails.ToList();
                foreach(var detail in detailList) {
                    partsSalesOrder.PartsSalesOrderDetails.Remove(detail);
                }
            }

            //var salesUnitKv = this.KvSalesUnits.SingleOrDefault(r => r.Key == partsSalesOrder.SalesUnitId && r.UserObject is SalesUnit);
            //if(salesUnitKv == null)
            //    return;
            //var selectedSalesUnit = salesUnitKv.UserObject as SalesUnit;
            //if(selectedSalesUnit == null)
            //    return;
            this.oldPartsSalesOrderTypeId = 0;
            partsSalesOrder.CustomerType = customerInformation.CustomerCompany.Type;
            //收票
            partsSalesOrder.InvoiceReceiveCompanyId = customerInformation.CustomerCompany.Id;
            partsSalesOrder.InvoiceReceiveCompanyCode = customerInformation.CustomerCompany.Code;
            partsSalesOrder.InvoiceReceiveCompanyName = customerInformation.CustomerCompany.Name;
            partsSalesOrder.InvoiceReceiveCompanyType = customerInformation.CustomerCompany.Type;
            //收货
            partsSalesOrder.ReceivingCompanyId = customerInformation.CustomerCompany.Id;
            partsSalesOrder.ReceivingCompanyCode = customerInformation.CustomerCompany.Code;
            partsSalesOrder.ReceivingCompanyName = customerInformation.CustomerCompany.Name;
            //提报
            partsSalesOrder.SubmitCompanyId = customerInformation.CustomerCompany.Id;
            partsSalesOrder.SubmitCompanyName = customerInformation.CustomerCompany.Name;
            partsSalesOrder.SubmitCompanyCode = customerInformation.CustomerCompany.Code;

            partsSalesOrder.SalesCategoryId = default(int);
            partsSalesOrder.SalesCategoryName = string.Empty;
            partsSalesOrder.WarehouseId = null;

            if(currentCompanyType == (int)DcsCompanyType.分公司) {
                if(partsSalesOrder.InvoiceReceiveCompanyType == (int)DcsCompanyType.代理库 || partsSalesOrder.InvoiceReceiveCompanyType == (int)DcsCompanyType.服务站兼代理库) {
                    this.kvPartsSalesCategories.Clear();
                    this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOption => {
                        if(loadOption.HasError)
                            return;
                        this.KvPartsSalesCategories.Clear();
                        foreach(var partsSalesCategory in loadOption.Entities) {
                            this.KvPartsSalesCategories.Add(new KeyValuePair {
                                Key = partsSalesCategory.Id,
                                Value = partsSalesCategory.Name
                            });
                        }
                        this.txtSalesCategory.SelectedIndex = 0;//默认品牌
                     
                    }, null);
                    
                }
                if(partsSalesOrder.InvoiceReceiveCompanyType == (int)DcsCompanyType.服务站 || partsSalesOrder.InvoiceReceiveCompanyType == (int)DcsCompanyType.集团企业) {
                    this.kvPartsSalesCategories.Clear();
                    this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesByDealerServiceInfoQuery(BaseApp.Current.CurrentUserData.EnterpriseId, partsSalesOrder.SubmitCompanyId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError)
                            return;
                        this.KvPartsSalesCategories.Clear();
                        foreach(var partsSalesCategory in loadOp.Entities) {
                            this.KvPartsSalesCategories.Add(new KeyValuePair {
                                Key = partsSalesCategory.Id,
                                Value = partsSalesCategory.Name
                            });
                        }
                        this.txtSalesCategory.SelectedIndex = 0;//默认品牌
                    }, null);
                }
            }
            if(partsSalesOrder.InvoiceReceiveCompanyType == (int)DcsCompanyType.分公司) {
                this.kvPartsSalesCategories.Clear();
                this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOption => {
                    if(loadOption.HasError)
                        return;
                    this.KvPartsSalesCategories.Clear();
                    foreach(var partsSalesCategory in loadOption.Entities) {
                        this.KvPartsSalesCategories.Add(new KeyValuePair {
                            Key = partsSalesCategory.Id,
                            Value = partsSalesCategory.Name
                        });
                    }
                    this.txtSalesCategory.SelectedIndex = 0;//默认品牌
                }, null);
            }

            if(currentCompanyType == (int)DcsCompanyType.代理库 || currentCompanyType == (int)DcsCompanyType.服务站兼代理库) {
                this.kvPartsSalesCategories.Clear();
                this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesByAgencyDealerRelationQuery(BaseApp.Current.CurrentUserData.EnterpriseId, partsSalesOrder.SubmitCompanyId), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        loadOp.MarkErrorAsHandled();
                        return;
                    }
                    this.KvPartsSalesCategories.Clear();
                    foreach(var partsSalesCategory in loadOp.Entities) {
                        this.KvPartsSalesCategories.Add(new KeyValuePair {
                            Key = partsSalesCategory.Id,
                            Value = partsSalesCategory.Name
                        });
                    }
                    this.txtSalesCategory.SelectedIndex = 0;//默认品牌
                }, null);
            }
            //if(partsSalesOrder.SalesUnitId != default(int) && partsSalesOrder.SubmitCompanyId != default(int))
            //    this.DomainContext.Load(this.DomainContext.查询客户可用资金Query(null, partsSalesOrder.SalesUnit.AccountGroupId, partsSalesOrder.InvoiceReceiveCompanyId), LoadBehavior.RefreshCurrent, loadOption1 => {
            //        var parent = queryWindow.ParentOfType<RadWindow>();
            //        if(loadOption1.HasError) {
            //            loadOption1.MarkErrorAsHandled();
            //            DcsUtils.ShowDomainServiceOperationWindow(loadOption1);
            //            if(parent != null)
            //                parent.Close();
            //            return;
            //        }
            //        partsSalesOrder.CustomerAccount = loadOption1.Entities.FirstOrDefault();
            //        if(partsSalesOrder.CustomerAccount == null)
            //            UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Notification_PartsSalesOrder_CustomerAccountIsNull);
            //        if(parent != null)
            //            parent.Close();
            //    }, null);
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        protected void customerInformationBySparepart_Loaded(object sender, RoutedEventArgs e) {
            var queryWindowCustomerInformationBySparepart = sender as QueryWindowBase;
            if(queryWindowCustomerInformationBySparepart == null)
                return;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem("SalesCompanyId", typeof(int), FilterOperator.IsEqualTo, BaseApp.Current.CurrentUserData.EnterpriseId));
            queryWindowCustomerInformationBySparepart.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }
        #endregion 界面事件

        protected override void OnEditCancelled() {
            base.OnEditCancelled();
            this.KvPartsSalesOrderTypes.Clear();
            this.KvCompanyAddresses.Clear();
            this.KvReceivingWarehouse.Clear();
            this.KvPartsSalesCategories.Clear();
            this.notApprovePrice.Text = "￥0.00";
            this.orderUseablePosition.Text = "￥0.00";
            this.lnotApprovePrice = 0;
            this.lorderUseablePosition = 0;
            this.lRebateAmount = 0;
            this.oldPartsSalesOrderTypeId = 0;
            this.blockReturnWarehouse.Visibility = Visibility.Collapsed;
            this.comboxReceivingWarehouse.Visibility = Visibility.Collapsed;
        }

        protected override void OnEditSubmitting() {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null || !this.PartsSalesOrderDetailForEditDataGridView.CommitEdit())
                return;
            partsSalesOrder.ValidationErrors.Clear();

            foreach(var item in partsSalesOrder.PartsSalesOrderDetails) {
                item.ValidationErrors.Clear();
            }

            if(partsSalesOrder.WarehouseId == default(int) || partsSalesOrder.WarehouseId == null) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_WarehouseIdIdIsNull);
                return;
            }

            if(partsSalesOrder.PartsSalesOrderTypeId == default(int))
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_PartsSalesOrderTypeIsNull, new[] {
                    "PartsSalesOrderTypeName"
                }));
            if(CompanyType == (int)DcsCompanyType.分公司) {
                if(!partsSalesOrder.ReceivingWarehouseId.HasValue)
                    partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_ReceivingWarehouseNameIsNull, new[] {
                        "ReceivingWarehouseId"
                    }));
                if(!partsSalesOrder.InvoiceReceiveSaleCateId.HasValue)
                    partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_InvoiceReceiveSaleCateIdIsNull, new[] {
                        "InvoiceReceiveSaleCateId"
                    }));
            }
            if(partsSalesOrder.ShippingMethod == default(int))
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_ShippingMethodIsNull, new[] {
                    "ShippingMethod"
                }));
            if(partsSalesOrder.SalesUnitId == default(int))
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_SalesUnitIdIsNull, new[] {
                    "SalesUnitId"
                }));
            if(partsSalesOrder.SubmitCompanyId == default(int))
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_SubmitCompanyIdIsNull, new[] {
                    "SubmitCompanyName"
                }));
            if(string.IsNullOrEmpty(partsSalesOrder.ReceivingAddress))
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_ReceivingAddressIsNull, new[] {
                    "ReceivingAddress"
                }));
            if(partsSalesOrder.PartsSalesOrderTypeId == default(int))
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_PartsSalesOrderTypeIdIsNull, new[] {
                    "PartsSalesOrderTypeId"
                }));
            if(partsSalesOrder.ReceivingCompanyId == default(int))
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_ReceivingCompanyIdIsNull, new[] {
                    "ReceivingCompanyName"
                }));
            if(partsSalesOrder.SalesUnitOwnerCompanyId == partsSalesOrder.SubmitCompanyId) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_SalesUnitOwner);
                return;
            }

            if(companys.Type == (int)DcsCompanyType.代理库 || companys.Type == (int)DcsCompanyType.服务站兼代理库) {
                if(partsSalesOrder.ReceivingWarehouseId == default(int) || partsSalesOrder.ReceivingWarehouseId == null) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_ReceivingWarehouseNameIsNull);
                    return;
                }
            }

            if(partsSalesOrder.CustomerAccount == null) {
                var validationMessage = PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_AccountBalanceIsNull;
                validationMessage = string.Concat(validationMessage, Environment.NewLine, PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_PendingAmountIsNull);
                validationMessage = string.Concat(validationMessage, Environment.NewLine, PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_ShippedProductValueIsNull);
                validationMessage = string.Concat(validationMessage, Environment.NewLine, PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_CustomerCredenceAmountIsNull);
                validationMessage = string.Concat(validationMessage, Environment.NewLine, PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_UseablePositionIsNull);
                UIHelper.ShowAlertMessage(validationMessage);
                return;
            }
            if(!partsSalesOrder.RequestedDeliveryTime.HasValue) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderProcess_RequestedDeliveryTimeIsNull);
                return;
            }

            if(partsSalesOrder.RequestedDeliveryTime.Value < new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_RequestedDeliveryTime);
                return;
            }

            if(!partsSalesOrder.PartsSalesOrderDetails.Any()) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_PartsSalesOrderDetailsIsEmpty);
                return;
            }
            if(partsSalesOrder.TotalAmount > 1000000) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_PartssalesOrder_FeeOver);
                return;
            }
            foreach(var detail in partsSalesOrder.PartsSalesOrderDetails) {
                detail.ValidationErrors.Clear();
                if(detail.OrderPrice == 0)
                    detail.ValidationErrors.Add(new ValidationResult(string.Format(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_InCurrentPartsSalesOrderTypeNameOrderSumIsZeroError, partsSalesOrder.PartsSalesOrderTypeName, detail.SparePartCode), new[] {
                        "OrderPrice"
                    }));
                if(detail.OrderedQuantity <= default(int))
                    detail.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderDetail_OrderedQuantityIsLessThanZeroError, new[] {
                        "OrderedQuantity"
                    }));
                if(!detail.HasValidationErrors)
                    ((IEditableObject)detail).EndEdit();
            }

            if(partsSalesOrder.HasValidationErrors || partsSalesOrder.PartsSalesOrderDetails.Any(e => e.HasValidationErrors))
                return;
            if(partsSalesOrder.ReceivingWarehouseId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_ReceivingWarehouse);
                return;
            }
            partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
            ((IEditableObject)partsSalesOrder).EndEdit();
            //销售订单生成时 检查 销售组织隶属企业id和编号名称必须属于一个企业
            this.DomainContext.Load(this.DomainContext.GetCompaniesQuery(), LoadBehavior.RefreshCurrent, loadOpOwnerCompany => {
                if(loadOpOwnerCompany.HasError)
                    if(loadOpOwnerCompany.IsErrorHandled) {
                        loadOpOwnerCompany.MarkErrorAsHandled();
                        return;
                    }
                var companies = loadOpOwnerCompany.Entities;
                if(companies != null && companies.Any()) {
                    if(!companies.Any(r => r.Id == partsSalesOrder.SalesUnitOwnerCompanyId && r.Code == partsSalesOrder.SalesUnitOwnerCompanyCode && r.Name == partsSalesOrder.SalesUnitOwnerCompanyName)) {
                        UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_SalesUnitOwnerNew);
                        return;
                    }
                } else
                    return;
                var sparePartIds = partsSalesOrder.PartsSalesOrderDetails.Select(r => r.SparePartId).ToArray();
                //校验销售订单清单数是否大于分公司策略的最大开票行数
                this.DomainContext.Load(this.DomainContext.GetBranchstrategiesQuery().Where(r => r.BranchId == partsSalesOrder.BranchId), LoadBehavior.RefreshCurrent, loadOpS => {
                    if(loadOpS.HasError) {
                        if(loadOpS.IsErrorHandled)
                            loadOpS.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOpS);
                        return;
                    }
                    var branchstrategy = loadOpS.Entities.SingleOrDefault();
                    if(branchstrategy != null && partsSalesOrder.PartsSalesOrderDetails.Count > branchstrategy.MaxInvoiceRow) {
                        UIHelper.ShowAlertMessage(string.Format(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_MaxInvoiceRow, branchstrategy.MaxInvoiceRow));
                        return;
                    }
                    this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(e => e.Id == partsSalesOrder.SubmitCompanyId), loadOpCompany => {
                        if(loadOpCompany.HasError)
                            return;
                        var company = loadOpCompany.Entities.SingleOrDefault();
                        if(company == null)
                            return;

                        if(string.IsNullOrWhiteSpace(this.provinceNameRadCombox.Text)) {
                            partsSalesOrder.Province = company.ProvinceName;
                            partsSalesOrder.City = company.CityName;
                        } else {
                            partsSalesOrder.Province = this.provinceNameRadCombox.Text;
                            partsSalesOrder.City = this.cityNameRadCombox.Text;
                        }

                        this.DomainContext.Load(this.DomainContext.GetRegionsQuery().Where(e => e.Type == 2 && e.Name == partsSalesOrder.Province), loadOpRegion => {
                            if(loadOpRegion.HasError)
                                return;
                            var province = loadOpRegion.Entities.SingleOrDefault();
                            if(province != null)
                                partsSalesOrder.ProvinceID = province.Id;
                            this.DomainContext.Load(this.DomainContext.查询企业库存Query(partsSalesOrder.SubmitCompanyId, sparePartIds), LoadBehavior.RefreshCurrent, loadOp => {
                                if(loadOp.HasError)
                                    return;
                                var companyPartsStocks = loadOp.Entities;
                                bool flag = false;
                                foreach(var partsSalesOrderDetail in partsSalesOrder.PartsSalesOrderDetails) {
                                    var entity = companyPartsStocks.Where(r => r.SparePartId == partsSalesOrderDetail.SparePartId);
                                    if(entity == null || !entity.Any()) {
                                        flag = true;
                                        break;
                                    }
                                    if(partsSalesOrderDetail.OrderedQuantity >= entity.FirstOrDefault().UsableQuantity) {
                                        flag = true;
                                        break;
                                    }
                                }
                                if(!flag)
                                    DcsUtils.Confirm(PartsSalesUIStrings.DataEditView_Confirm_PartsSalesOrder_CompanyStockSatisfyOrder, () => this.CheckNumberMultiple(partsSalesOrder));
                                else
                                    this.CheckNumberMultiple(partsSalesOrder);
                            }, null);
                        }, null);
                    }, null);
                }, null);
            }, null);
            //5.如果订单总金额>可用额度时，系统提示“当前订单总金额大于账户可用额度， 是否继续保存”。用户选“是”，调用新增服务，否则，反回新增界面。
        }
        private void CheckNumberMultiple(PartsSalesOrder partsSalesOrder) {
            var sparepartIds = partsSalesOrder.PartsSalesOrderDetails.Select(r => r.SparePartId).ToArray();
            this.DomainContext.Load(DomainContext.GetSparePartsByIdsQuery(sparepartIds), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled) {
                        loadOp.MarkErrorAsHandled();
                        return;
                    }
                }
                this.DomainContext.Load(this.DomainContext.GetAgencyAffiBranchesQuery().Where(r => r.BranchId == partsSalesOrder.BranchId), LoadBehavior.RefreshCurrent, aloadOp => {
                    if(aloadOp.HasError) {
                        if(!aloadOp.IsErrorHandled)
                            aloadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(aloadOp);
                        return;
                    }
                    var entity = aloadOp.Entities.Select(e => e.BranchId).ToArray();
                    if(loadOp.Entities != null && loadOp.Entities.Any()) {
                        var spareParts = loadOp.Entities.ToArray();
                        if(partsSalesOrder.PartsSalesOrderTypeName == "油品订单") {
                            var sparePart = spareParts.FirstOrDefault(r => r.GoldenTaxClassifyCode != "1");
                            if(sparePart != null) {
                                UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_GoldenTaxClassifyCode, sparePart.Code));
                                return;
                            }
                        }
                        var isNotCheckMInPackingAmount = (partsSalesOrder.SalesUnitOwnerCompanyType == (int)DcsCompanyType.代理库 || partsSalesOrder.SalesUnitOwnerCompanyType == (int)DcsCompanyType.服务站兼代理库)
                            && entity.Any();
                        if(spareParts.Any(r => r.MInPackingAmount == null || r.MInPackingAmount == 0) && !isNotCheckMInPackingAmount) {
                            UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_MInPackingAmount);
                            return;
                        }
                        if(!isNotCheckMInPackingAmount && partsSalesOrder.PartsSalesOrderTypeName != "紧急订单" && partsSalesOrder.PartsSalesOrderTypeName != "特急订单" && partsSalesOrder.PartsSalesOrderTypeName != "事故订单" && partsSalesOrder.PartsSalesOrderTypeName != "三包垫件" && partsSalesOrder.PartsSalesOrderTypeName != "国内赠送" && partsSalesOrder.PartsSalesOrderTypeName != "集团订单" && partsSalesOrder.PartsSalesOrderTypeName != "6K1" && partsSalesOrder.PartsSalesOrderTypeName != "6K2")
                            foreach(var partsSalesOrderDetail in partsSalesOrder.PartsSalesOrderDetails) {
                                var partsValidation = spareParts.FirstOrDefault(r => r.Id == partsSalesOrderDetail.SparePartId && (partsSalesOrderDetail.OrderedQuantity % r.MInPackingAmount.Value) != 0);
                                if(spareParts.Any(r => r.Id == partsSalesOrderDetail.SparePartId && (partsSalesOrderDetail.OrderedQuantity % r.MInPackingAmount.Value) != 0) && partsValidation != null) {
                                    UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_OrderedQuantity, partsValidation.Code, partsValidation.MInPackingAmount));
                                    partsSalesOrderDetail.MInSaleingAmount = partsValidation.MInPackingAmount;
                                    partsSalesOrderDetail.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_SaleingAmount, new[] { "OrderedQuantity" }));
                                    return;
                                }
                            }
                        if(partsSalesOrder.SalesUnitOwnerCompanyType == (int)DcsCompanyType.分公司) {
                            this.DomainContext.Load(this.DomainContext.GetBranchstrategiesQuery().Where(r => r.BranchId == partsSalesOrder.SalesUnitOwnerCompanyId), LoadBehavior.RefreshCurrent, loadOpS => {
                                if(loadOpS.HasError) {
                                    if(loadOpS.IsErrorHandled)
                                        loadOpS.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOpS);
                                    return;
                                }
                                var branchstrategy = loadOpS.Entities.SingleOrDefault();
                                if(branchstrategy != null && branchstrategy.PurDistributionStrategy.HasValue && branchstrategy.PurDistributionStrategy == (int)DcsPurDistStrategy.纳入统购分销) {
                                    this.SubmitCurrentData(partsSalesOrder);
                                } else {
                                    this.CheckAllowCustomerCredit(partsSalesOrder);
                                }
                            }, null);
                        } else {
                            this.CheckAllowCustomerCredit(partsSalesOrder);
                        }
                    }
                }, null);
            }, null);
        }
        private void CheckAllowCustomerCredit(PartsSalesOrder partsSalesOrder) {
            this.DomainContext.Load(this.DomainContext.查询客户可用资金含未审批订单2Query(partsSalesOrder.CustomerAccountId, partsSalesOrder.CustomerAccount.AccountGroupId, partsSalesOrder.InvoiceReceiveCompanyId, null, partsSalesOrder.SalesCategoryId, partsSalesOrder.SalesUnitId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled) {
                        loadOp.MarkErrorAsHandled();
                        return;
                    }
                }
                if(loadOp.Entities != null && loadOp.Entities.Any()) {
                    var customerAccount = loadOp.Entities.First();
                    if(customerAccount.UseablePosition < partsSalesOrder.TotalAmount) {
                        UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_TotalAmount);
                    } else {
                        this.SubmitCurrentData(partsSalesOrder);
                    }
                } else {
                    this.SubmitCurrentData(partsSalesOrder);
                }
            }, null);

        }

        private void CheckStock(Dictionary<string, object> parameteras) {
            //TODO:临时取消
            //this.DomainContext.InvokeOperation("校验销售订单库存满足", typeof(bool), parameteras, true, invokeOp => {
            //    if(invokeOp.HasError)
            //        return;
            //    if(!bool.Parse(invokeOp.Value.ToString()))
            //        DcsUtils.Confirm("配件库存不足,是否继续保存", () => base.OnEditSubmitting());
            //    else
            //        base.OnEditSubmitting();
            //}, null);
            base.OnEditSubmitting();
        }

        private void SubmitCurrentData(PartsSalesOrder partsSalesOrder) {
            try {
                this.DomainContext.新增配件销售订单代做(partsSalesOrder, partsSalesOrder.PartsSalesOrderDetails.ToList(), invokeOp => {
                    if(invokeOp.HasError) {
                        if(!invokeOp.IsErrorHandled)
                            invokeOp.MarkErrorAsHandled();
                        var error = invokeOp.ValidationErrors.FirstOrDefault();
                        if(error != null) {
                            UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage, 5);
                            //DomainContext.RejectChanges();
                            return;
                        }
                        DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                        this.DomainContext.RejectChanges();
                        return;
                    }
                    this.NotifyEditSubmitted();
                    this.OnCustomEditSubmitted();
                }, null);
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
        }

        protected override bool OnRequestCanSubmit() {
            this.txtSalesCategory.SelectedIndex = 0;//默认品牌
            return true;
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsSalesOrder;
            }
        }

        public ObservableCollection<CompanyAddress> KvCompanyAddresses {
            get {
                return this.kvCompanyAddresses ?? (this.kvCompanyAddresses = new ObservableCollection<CompanyAddress>());
            }
        }

        public ObservableCollection<KeyValuePair> KvShippingMethods {
            get {
                return this.kvShippingMethods ?? (this.kvShippingMethods = new ObservableCollection<KeyValuePair>());

            }
        }

        public ObservableCollection<KeyValuePair> KvSalesUnits {
            get {
                return this.kvSalesUnits ?? (this.kvSalesUnits = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvPartsSalesOrderTypes {
            get {
                return this.kvPartsSalesOrderTypes ?? (this.kvPartsSalesOrderTypes = new ObservableCollection<KeyValuePair>());
            }
        }

        public object KvSecondLevelOrderTypes {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        private void DcsComboBox_SelectionChanged_1(object sender, SelectionChangedEventArgs e) {
            var comboBox = sender as RadComboBox;
            if(comboBox == null)
                return;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            var companyAddress = comboBox.SelectedItem as CompanyAddress;
            if(companyAddress == null) {
                partsSalesOrder.ContactPerson = string.Empty;
                partsSalesOrder.ContactPhone = string.Empty;
                return;
            }
            partsSalesOrder.CompanyAddressId = companyAddress.Id;
            partsSalesOrder.ContactPerson = companyAddress.ContactPerson;
            partsSalesOrder.ContactPhone = companyAddress.ContactPhone;
            if(partsSalesOrder.PartsSalesOrderTypeName == "正常订单") {
                this.TextBoxContactPerson.IsReadOnly = true;
                this.TextBoxContactPhone.IsReadOnly = true;
            } else {
                this.TextBoxContactPerson.IsReadOnly = false;
                this.TextBoxContactPhone.IsReadOnly = false;
            }
        }

        //提报单位为代理库或服务站兼代理库时必须填写收货仓库，并且限制选择收货仓库后按Backspace键不清空收货仓库
        private int comboxIndex;

        private void ComboBoxReceivingWarehouse_OnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            var comboBox = sender as RadComboBox;
            if(comboBox == null)
                return;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            var keyValuePair = comboBox.SelectedItem as KeyValuePair;
            if(keyValuePair == null)
                return;
            var warehouse = keyValuePair.UserObject as Warehouse;
            if(warehouse != null) {
                partsSalesOrder.ReceivingWarehouseId = warehouse.Id;
                partsSalesOrder.ReceivingWarehouseName = warehouse.Name;
            }
            comboxIndex = comboBox.SelectedIndex;
        }

        private void ComboxReceivingWarehouse_OnKeyUp(object sender, KeyEventArgs e) {
            if(e.PlatformKeyCode == 8) {
                comboxReceivingWarehouse.SelectedIndex = comboxIndex;
            }
        }

        public new event EventHandler EditSubmitted;

        private void NotifyEditSubmitted() {
            var handler = this.EditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        public void OnCustomEditSubmitted() {
            this.KvPartsSalesOrderTypes.Clear();
            this.KvCompanyAddresses.Clear();
            this.KvReceivingWarehouse.Clear();
            this.KvPartsSalesCategories.Clear();
            this.notApprovePrice.Text = "￥0.00";
            this.orderUseablePosition.Text = "￥0.00";
            this.lnotApprovePrice = 0;
            this.lRebateAmount = 0;
            this.lorderUseablePosition = 0;
            this.oldPartsSalesOrderTypeId = 0;
            this.receivingCompanyType = null;
            this.blockReturnWarehouse.Visibility = Visibility.Collapsed;
            this.comboxReceivingWarehouse.Visibility = Visibility.Collapsed;
        }

        private void DcsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            if(partsSalesOrder.PartsSalesOrderTypeName == "油品订单" && this.receivingCompanyType == (int)DcsCompanyType.代理库) {
                this.checkBoxIfDirectProvision.IsEnabled = false;
            } else {
                this.checkBoxIfDirectProvision.IsEnabled = true;
            }
            var dcsDomainContext = new DcsDomainContext();
            if(this.PartsSalesOrderType.Text == "紧急订单"||this.PartsSalesOrderType.Text == "事故订单") {
                this.ShippingMethod.IsEnabled = true;
                dcsDomainContext.Load(dcsDomainContext.GetKeyValueItemsQuery().Where(r => r.Name == "PartsShipping_Method" && (r.Key == (int)DcsPartsShippingMethod.顺丰公路 || r.Key == (int)DcsPartsShippingMethod.顺丰航空 || r.Key == (int)DcsPartsShippingMethod.德邦物流 || r.Key == (int)DcsPartsShippingMethod.航空)), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError)
                        return;
                    this.KvShippingMethods.Clear();
                    foreach(var partsSalesOrderType in loadOp.Entities) {
                        this.KvShippingMethods.Add(new KeyValuePair {
                            Key = partsSalesOrderType.Key,
                            Value = partsSalesOrderType.Value,
                        });
                    }
                }, null);
            } else if(this.PartsSalesOrderType.Text == "特急订单") {
                this.ShippingMethod.IsEnabled = true;
                dcsDomainContext.Load(dcsDomainContext.GetKeyValueItemsQuery().Where(r => r.Name == "PartsShipping_Method" && ( r.Key == (int)DcsPartsShippingMethod.顺丰航空 || r.Key == (int)DcsPartsShippingMethod.航空)), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError)
                        return;
                    this.KvShippingMethods.Clear();
                    foreach(var partsSalesOrderType in loadOp.Entities) {
                        this.KvShippingMethods.Add(new KeyValuePair {
                            Key = partsSalesOrderType.Key,
                            Value = partsSalesOrderType.Value,
                        });
                    }
                }, null);
            } else {
                dcsDomainContext.Load(dcsDomainContext.GetKeyValueItemsQuery().Where(r => r.Name == "PartsShipping_Method" && r.Key != (int)DcsPartsShippingMethod.DHL_出口 && r.Key != (int)DcsPartsShippingMethod.海运_出口 && r.Key != (int)DcsPartsShippingMethod.空运_出口 && r.Key != (int)DcsPartsShippingMethod.铁路_出口), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError)
                        return;
                    this.KvShippingMethods.Clear();
                    foreach(var partsSalesOrderType in loadOp.Entities) {
                        this.KvShippingMethods.Add(new KeyValuePair {
                            Key = partsSalesOrderType.Key,
                            Value = partsSalesOrderType.Value,
                        });
                    }
                    if(this.PartsSalesOrderType.Text == "正常订单" || this.PartsSalesOrderType.Text == "油品订单") {
                        partsSalesOrder.ShippingMethod = (int)DcsPartsShippingMethod.公路物流;
                        this.ShippingMethod.IsEnabled = false;
                    } else {
                        this.ShippingMethod.IsEnabled = true;
                    }
                }, null);
            }

            
        }
    }
}