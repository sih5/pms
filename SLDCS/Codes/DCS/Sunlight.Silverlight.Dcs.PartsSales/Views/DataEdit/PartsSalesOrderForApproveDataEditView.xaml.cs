﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesOrderForApproveDataEditView {
        private DataGridViewBase partsSalesOrderProcessDetailDataGridView;
        private KeyValueManager keyValueManager;

        private ObservableCollection<KeyValuePair> kvWarehouses;

        private readonly string[] kvNames = new[] {
            "PartsShipping_Method"
        };

        public PartsSalesOrderForApproveDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData();
            this.Initializer.Register(this.CreateUI);
        }

        private DataGridViewBase PartsSalesOrderProcessDetailDataGridView {
            get {
                if(partsSalesOrderProcessDetailDataGridView == null) {
                    this.partsSalesOrderProcessDetailDataGridView = DI.GetDataGridView("PartsSalesOrderProcessDetailForApprove");
                    this.partsSalesOrderProcessDetailDataGridView.DomainContext = this.DomainContext;
                }
                return partsSalesOrderProcessDetailDataGridView;
            }
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("PartsSalesOrderForApprove"));
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Content = this.PartsSalesOrderProcessDetailDataGridView,
                Header = Utils.GetEntityLocalizedName(typeof(PartsSalesOrderProcess), "PartsSalesOrderProcessDetails")
            });
            tabControl.SetValue(Grid.RowProperty, 2);
            this.Root.Children.Add(tabControl);
            this.comboBoxWarehouse.SelectionChanged += this.ComboBoxWarehouse_SelectionChanged;
            this.comboBoxWarehouse.ItemsSource = this.KvWarehouses;
            this.comboBoxShippingMethod.ItemsSource = this.KvShippingMethods;
            //this.DataContextChanged += PartsSalesOrderProcessDetailDataGridView_DataContextChanged;
        }


        private void PartsSalesOrderProcessDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            if(this.DataContext as PartsSalesOrderProcess == null) return;
            (this.DataContext as PartsSalesOrderProcess).PartsSalesOrder.PropertyChanged -= this.PartsSalesOrder_PropertyChanged;
            (this.DataContext as PartsSalesOrderProcess).PartsSalesOrder.PropertyChanged += this.PartsSalesOrder_PropertyChanged;

        }

        private void PartsSalesOrder_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsSalesOrderProcess = this.DataContext as PartsSalesOrderProcess;
            if(partsSalesOrderProcess == null)
                return;
            var partsSalesOrder = partsSalesOrderProcess.PartsSalesOrder;
            switch(e.PropertyName) {
                case "PartsSalesOrderTypeId":
                    var sparePartIds = partsSalesOrder.PartsSalesOrderDetails.Select(r => r.SparePartId).ToArray();
                    this.DomainContext.Load(this.DomainContext.根据配件清单获取销售价格Query(partsSalesOrder.PartsSalesOrderTypeId, partsSalesOrder.SubmitCompanyId, sparePartIds), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError)
                            return;
                        foreach(var detail in partsSalesOrder.PartsSalesOrderDetails) {
                            var salesPrice = loadOp.Entities.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                            if(salesPrice != null) {
                                if(salesPrice.PartsTreatyPrice != null)
                                    detail.OrderPrice = salesPrice.PartsTreatyPrice.Value;
                                else
                                    detail.OrderPrice = salesPrice.Price;
                                detail.MeasureUnit = salesPrice.MeasureUnit;
                                detail.CustOrderPriceGradeCoefficient = salesPrice.Coefficient;
                                detail.OriginalPrice = salesPrice.RetailGuidePrice;
                                detail.DiscountedPrice = detail.OriginalPrice - detail.OrderPrice;
                                detail.PriceTypeName = salesPrice.PriceTypeName;
                                detail.ABCStrategy = salesPrice.ABCStrategy;
                            } else
                                detail.OrderPrice = 0;
                            detail.OrderSum = detail.OrderedQuantity * detail.OrderPrice;
                            var partSalesOrderProcessDetails = partsSalesOrderProcess.PartsSalesOrderProcessDetails.SingleOrDefault(r => r.SparePartId == detail.SparePartId);
                            if(partSalesOrderProcessDetails != null)
                                partSalesOrderProcessDetails.OrderPrice = detail.OrderPrice;
                        }
                        partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
                    }, null);
                    break;

            }
        }

        private void ComboBoxWarehouse_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if((sender as RadComboBox) == null || e.AddedItems.Count != 1)
                return;
            var partsSalesOrderProcess = this.DataContext as PartsSalesOrderProcess;
            if(partsSalesOrderProcess == null)
                return;
            var keyValuePair = e.AddedItems.Cast<KeyValuePair>().FirstOrDefault();
            if(keyValuePair == null || !(keyValuePair.UserObject is Warehouse))
                return;
            if(partsSalesOrderProcess.PartsSalesOrder.SalesUnit == null)
                return;
            // 页面编辑无法自动清除错误信息，此处手工清除
            if(partsSalesOrderProcess.ValidationErrors.Any(v => v.MemberNames.Contains("WarehouseId")) && partsSalesOrderProcess.WarehouseId != default(int))
                partsSalesOrderProcess.ValidationErrors.Remove(partsSalesOrderProcess.ValidationErrors.FirstOrDefault(v => v.MemberNames.Contains("WarehouseId")));
            var warehouse = keyValuePair.UserObject as Warehouse;
            // 操作步骤1
            var sparePartIds = partsSalesOrderProcess.PartsSalesOrderProcessDetails.Select(r => r.SparePartId).ToArray();
            this.DomainContext.Load(this.DomainContext.查询企业库存Query(partsSalesOrderProcess.PartsSalesOrder.SalesUnit.OwnerCompanyId, sparePartIds), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var processDetail in partsSalesOrderProcess.PartsSalesOrderProcessDetails) {
                    var companyPartsStock = loadOp.Entities.FirstOrDefault(r => r.SparePartId == processDetail.SparePartId);
                    if(companyPartsStock != null) {
                        if(companyPartsStock.UsableQuantity >= processDetail.UnfulfilledQuantity) {
                            processDetail.CurrentFulfilledQuantity = processDetail.UnfulfilledQuantity;
                            processDetail.OrderProcessStatus = (int)DcsPartsSalesOrderProcessDetailOrderProcessStatus.满足;
                        } else if(companyPartsStock.UsableQuantity > 0) {
                            processDetail.CurrentFulfilledQuantity = companyPartsStock.UsableQuantity;
                            processDetail.OrderProcessStatus = (int)DcsPartsSalesOrderProcessDetailOrderProcessStatus.未满足;
                        }
                    }
                    processDetail.WarehouseId = warehouse.Id;
                    processDetail.WarehouseCode = warehouse.Code;
                    processDetail.WarehouseName = warehouse.Name;
                    processDetail.OrderProcessMethod = (int)DcsPartsSalesOrderProcessDetailProcessMethod.积压件平台;
                }
            }, null);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesOrderWithPartsSalesOrderDetailsQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                //this.DetailPanel.DataContext = null;
                //this.DetailPanel.DataContext = entity;
                var salesOrderProcess = new PartsSalesOrderProcess();
                salesOrderProcess.Code = GlobalVar.ASSIGNED_BY_SERVER;
                salesOrderProcess.OriginalSalesOrderId = entity.Id;
                salesOrderProcess.BillStatusBeforeProcess = entity.Status;
                salesOrderProcess.ShippingMethod = entity.ShippingMethod;
                salesOrderProcess.RequestedShippingTime = entity.RequestedShippingTime;
                salesOrderProcess.RequestedDeliveryTime = entity.RequestedDeliveryTime;
                salesOrderProcess.PartsSalesOrder = entity;

                foreach(var detail in entity.PartsSalesOrderDetails) {
                    salesOrderProcess.PartsSalesOrderProcessDetails.Add(new PartsSalesOrderProcessDetail {
                        SparePartId = detail.SparePartId,
                        OrderProcessMethod = (int)DcsPartsSalesOrderProcessDetailProcessMethod.积压件平台,
                        SparePartCode = detail.SparePartCode,
                        SparePartName = detail.SparePartName,
                        MeasureUnit = detail.MeasureUnit,
                        OrderedQuantity = detail.OrderedQuantity,
                        UnfulfilledQuantity = detail.OrderedQuantity - detail.ApproveQuantity.GetValueOrDefault(),
                        OrderPrice = detail.OrderPrice,
                        OrderProcessStatus = (int)DcsPartsSalesOrderProcessDetailOrderProcessStatus.未满足
                    });
                }
                this.DomainContext.Load(this.DomainContext.GetWarehousesBySalesUnitIdQuery(salesOrderProcess.PartsSalesOrder.SalesUnitId), LoadBehavior.RefreshCurrent, loadOption => {
                    if(loadOption.HasError)
                        return;
                    this.KvWarehouses.Clear();
                    foreach(var warehouse in loadOption.Entities) {
                        this.KvWarehouses.Add(new KeyValuePair {
                            Key = warehouse.Id,
                            Value = warehouse.Name,
                            UserObject = warehouse
                        });
                    }
                }, null);
                this.SetObjectToEdit(salesOrderProcess);
                this.DomainContext.PartsSalesOrderProcesses.Add(salesOrderProcess);
            }, null);
        }

        protected override void OnEditSubmitting() {
            var partsSalesOrderProcess = this.DataContext as PartsSalesOrderProcess;
            if(partsSalesOrderProcess == null)
                return;

            partsSalesOrderProcess.CurrentFulfilledAmount = default(int);
            partsSalesOrderProcess.CurrentFulfilledAmount = partsSalesOrderProcess.PartsSalesOrderProcessDetails.Sum(v => v.CurrentFulfilledQuantity * v.OrderPrice);

            if(!this.PartsSalesOrderProcessDetailDataGridView.CommitEdit())
                return;
            partsSalesOrderProcess.ValidationErrors.Clear();

            if(partsSalesOrderProcess.WarehouseId == default(int))
                partsSalesOrderProcess.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderProcess_WarehouseIdIsNull, new[] {
                    "WarehouseId"
                }));

            //配件编号唯一性校验
            var sparePartIds = partsSalesOrderProcess.PartsSalesOrderProcessDetails.Select(v => v.SparePartId);
            if(partsSalesOrderProcess.PartsSalesOrderProcessDetails.Count(r => sparePartIds.Contains(r.SparePartId)) > 1) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderProcessDetail_SparePartIsRepeat);
                return;
            }

            if(partsSalesOrderProcess.PartsSalesOrderProcessDetails.Sum(e => e.CurrentFulfilledQuantity) <= 0) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderProcessDetail_CurrentFulfilledQuantityIsZeroError);
                return;
            }

            foreach(var detail in partsSalesOrderProcess.PartsSalesOrderProcessDetails) {
                detail.ValidationErrors.Clear();
                if(detail.OrderProcessMethod == default(int))
                    detail.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderProcessDetail_OrderProcessMethodIsNull, new[] {
                        "OrderProcessMethod"
                    }));
                if(detail.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.积压件平台) {
                    if(detail.TransferPrice == 0)
                        detail.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderProcessDetail_TransferPriceAndSupplierCompanyIsRequired, new[] {
                        "TransferPrice"
                    }));
                    if(detail.SupplierCompanyId == default(int))
                        detail.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderProcessDetail_TransferPriceAndSupplierCompanyIsRequired, new[] {
                         "SupplierCompanyName"
                    }));
                }
                if(detail.OrderedQuantity <= 0)
                    detail.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderProcessDetail_OrderedQuantityIsZeroError, new[] {
                        "OrderedQuantity"
                    }));
                if(detail.WarehouseId == default(int))
                    detail.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderProcessDetail_WarehouseIdIsNull, new[] {
                        "WarehouseId"
                    }));
                if(detail.UnfulfilledQuantity < detail.CurrentFulfilledQuantity)
                    detail.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderProcessDetail_CurrentFulfilledQuantityGreateThanUnfulfilledQuantityError, new[] {
                        "CurrentFulfilledQuantity"
                    }));
                //对本次满足数量为零的清单实体进行过滤
                if(detail.CurrentFulfilledQuantity == 0) {
                    partsSalesOrderProcess.PartsSalesOrderProcessDetails.Remove(detail);
                }

                if(!detail.HasValidationErrors)
                    ((IEditableObject)detail).EndEdit();
            }

            if(partsSalesOrderProcess.HasValidationErrors || partsSalesOrderProcess.PartsSalesOrderProcessDetails.Any(e => e.HasValidationErrors))
                return;
            ((IEditableObject)partsSalesOrderProcess).EndEdit();
            try {
                if(partsSalesOrderProcess.Can审批配件销售订单正常订单)
                    partsSalesOrderProcess.审批配件销售订单正常订单();
            } catch(Exception ex) {
                UIHelper.ShowAlertMessage(ex.Message);
            }

            base.OnEditSubmitting();
        }

        protected override string Title {
            get {
                return PartsSalesUIStrings.DataEditView_Title_Approve_PartsSalesOrder;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public ObservableCollection<KeyValuePair> KvWarehouses {
            get {
                return this.kvWarehouses ?? (this.kvWarehouses = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvShippingMethods {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
    }
}
