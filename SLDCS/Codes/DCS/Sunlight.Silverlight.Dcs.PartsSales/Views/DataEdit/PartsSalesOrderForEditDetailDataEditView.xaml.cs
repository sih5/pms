﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesOrderForEditDetailDataEditView {
        private decimal lnotApprovePrice;
        private ObservableCollection<PartsSalesOrderDetail> partsSalesOrderDetails;
        private DataGridViewBase partsSalesOrderDetailForEditDataGridView;
        private KeyValueManager keyValueManager;
        private decimal lRebateAmount;

        private readonly ObservableCollection<KeyValuePair> kvReceivingWarehouse = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesOrderTypes = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategories = new ObservableCollection<KeyValuePair>();
        private ObservableCollection<SalesUnitAffiWarehouse> kvSalesUnitAffiWarehouses;

        public ObservableCollection<KeyValuePair> KvPartsSalesCategories {
            get {
                return kvPartsSalesCategories;
            }
        }

        public ObservableCollection<PartsSalesOrderDetail> PartsSalesOrderDetails {
            get {
                return this.partsSalesOrderDetails ?? (this.partsSalesOrderDetails = new ObservableCollection<PartsSalesOrderDetail>());
            }
        }

        private ObservableCollection<CompanyAddress> kvCompanyAddresses;

        public ObservableCollection<SalesUnitAffiWarehouse> KvSalesUnitAffiWarehouses {
            get {
                return this.kvSalesUnitAffiWarehouses ?? (this.kvSalesUnitAffiWarehouses = new ObservableCollection<SalesUnitAffiWarehouse>());
            }
        }


        private readonly string[] kvNames = {
            "PartsSalesOrder_SecondLevelOrderType", "PartsShipping_Method"
        };

        public PartsSalesOrderForEditDetailDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private void CreateUI() {
            this.KeyValueManager.LoadData();
            //查询营销分公司
            this.DomainContext.Load(this.DomainContext.根据企业查询营销分公司Query(BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvBranches.Clear();
                foreach(var branch in loadOp.Entities) {
                    this.KvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name,
                        UserObject = branch
                    });
                }
            }, null);
            //查询企业收获地址
            this.DomainContext.Load(this.DomainContext.GetCompanyAddressesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOption => {
                if(loadOption.HasError) {
                    loadOption.MarkErrorAsHandled();
                    return;
                }

                this.KvCompanyAddresses.Clear();
                foreach(var entity in loadOption.Entities) {
                    this.KvCompanyAddresses.Add(entity);
                }
            }, null);
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsSalesOrder), "PartsSalesOrderDetails"), null, () => this.PartsSalesOrderDetailForEditDataGridView);
            this.Detail.Children.Add(detailDataEditView);

        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesOrderWithPartsSalesOrderDetailsAndCustomerAccountForEditDetailQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity == null)
                    return;
                this.PartsSalesOrderDetails.Clear();
                var details = entity.PartsSalesOrderDetails.Where(r => (r.ApproveQuantity.HasValue ? r.ApproveQuantity.Value : 0) != r.OrderedQuantity).ToArray();
                foreach(var item in details)
                    PartsSalesOrderDetails.Add(item);
                this.DomainContext.Load(this.DomainContext.查询销售组织仓库Query(BaseApp.Current.CurrentUserData.EnterpriseId, entity.BranchId, null, entity.SalesCategoryId, null), LoadBehavior.RefreshCurrent, loadop => {
                    if(loadop.HasError) {
                        loadop.MarkErrorAsHandled();
                        return;
                    }
                    this.KvSalesUnitAffiWarehouses.Clear();
                    if(loadop.Entities != null)
                        foreach(var salesUnitAffiWarehouse in loadop.Entities) {
                            this.KvSalesUnitAffiWarehouses.Add(salesUnitAffiWarehouse);
                        }
                }, null);
                var parameteras = new Dictionary<string, object>();
                parameteras.Add("submitCompanyId", entity.SubmitCompanyId);
                parameteras.Add("salesUnitId", entity.SalesUnitId);
                parameteras.Add("partsSalesCategoryId", entity.SalesCategoryId);
                this.DomainContext.InvokeOperation("查询未审核订单金额", typeof(decimal), parameteras, true, invokeOp => {
                    if(invokeOp.HasError)
                        return;
                    this.notApprovePrice.Text = "￥" + invokeOp.Value.ToString();
                    this.lnotApprovePrice = decimal.Parse(invokeOp.Value.ToString());
                    this.DomainContext.Load(this.DomainContext.查询客户可用资金Query(null, entity.SalesUnit.AccountGroupId, entity.InvoiceReceiveCompanyId), LoadBehavior.RefreshCurrent, loadOp2 => {
                        if(loadOp2.HasError) {
                            loadOp.MarkErrorAsHandled();
                            return;
                        }
                        var customerAccount = loadOp2.Entities.FirstOrDefault();
                        if(customerAccount == null) {
                            UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Notification_PartsSalesOrder_CustomerAccountIsNull);
                            entity.CustomerAccount = null;
                            return;
                        }
                        entity.CustomerAccountId = customerAccount.Id;
                        entity.CustomerAccount = customerAccount;
                        this.DomainContext.Load(this.DomainContext.GetPartsRebateAccountsQuery().Where(r => r.AccountGroupId == entity.SalesUnit.AccountGroupId && r.CustomerCompanyId == entity.SubmitCompanyId && r.BranchId == entity.SalesUnit.OwnerCompanyId), LoadBehavior.RefreshCurrent, loadRebateAccount => {
                            if(loadRebateAccount.HasError) {
                                loadRebateAccount.MarkErrorAsHandled();
                                DcsUtils.ShowDomainServiceOperationWindow(loadRebateAccount);
                                return;
                            }
                            if(loadRebateAccount.Entities.Any()) {
                                this.txtRebateAmount.Text = loadRebateAccount.Entities.First().AccountBalanceAmount.ToString("c2");
                                this.lRebateAmount = loadRebateAccount.Entities.First().AccountBalanceAmount;
                                this.txtUseablePosition.Text = (entity.CustomerAccount.UseablePosition).ToString("c2");
                            } else {
                                this.txtRebateAmount.Text = "￥" + "0.00";
                                this.txtUseablePosition.Text = entity.CustomerAccount.UseablePosition.ToString("c2");
                            }
                            this.orderUseablePosition.Text = (entity.CustomerAccount.AccountBalance + entity.CustomerAccount.CustomerCredenceAmount + lRebateAmount - entity.CustomerAccount.LockBalance - lnotApprovePrice).ToString("c2");
                        }, null);
                        this.SetObjectToEdit(entity);
                    }, null);
                }, null);
            }, null);
        }

        private DataGridViewBase PartsSalesOrderDetailForEditDataGridView {
            get {
                if(this.partsSalesOrderDetailForEditDataGridView == null) {
                    this.partsSalesOrderDetailForEditDataGridView = DI.GetDataGridView("PartsSalesOrderDetailForEditDetail");
                    this.partsSalesOrderDetailForEditDataGridView.DomainContext = this.DomainContext;
                    this.partsSalesOrderDetailForEditDataGridView.DataContext = this;
                }
                return this.partsSalesOrderDetailForEditDataGridView;
            }
        }

        protected override void Reset() {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            if(this.DomainContext.PartsSalesOrders.Contains(partsSalesOrder))
                this.DomainContext.PartsSalesOrders.Detach(partsSalesOrder);
            this.KvPartsSalesOrderTypes.Clear();
            this.KvPartsSalesCategories.Clear();
            this.KvSalesUnitAffiWarehouses.Clear();
            this.notApprovePrice.Text = "￥0.00";
            this.orderUseablePosition.Text = "￥0.00";
            this.txtRebateAmount.Text = "￥0.00";
            this.txtUseablePosition.Text = "￥0.00";
            this.lnotApprovePrice = 0;
            this.PartsSalesOrderDetails.Clear();
        }

        protected override void OnEditSubmitting() {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null || !this.PartsSalesOrderDetailForEditDataGridView.CommitEdit())
                return;
            if(partsSalesOrder.WarehouseId == default(int) || partsSalesOrder.WarehouseId == null) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_WarehouseIdIdIsNull);
                return;
            }
            if(!partsSalesOrder.PartsSalesOrderDetails.Any()) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_PartsSalesOrderDetailsIsEmpty);
                return;
            }
            //部分审批的销售订单，将未满足的配件清单删除后 销售订单状态修改成：审批完成
            if(partsSalesOrder.Status == (int)DcsPartsSalesOrderStatus.部分审批 && partsSalesOrder.PartsSalesOrderDetails.All(r => r.OrderedQuantity == (r.ApproveQuantity.HasValue ? r.ApproveQuantity.Value : 0))) {
                partsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.审批完成;
            }
            partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
            ((IEditableObject)partsSalesOrder).EndEdit();
            base.OnEditSubmitting();
        }

        protected override string Title {
            get {
                return PartsSalesUIStrings.DataEdit_Title_PartsSalesOrderEdit;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public ObservableCollection<CompanyAddress> KvCompanyAddresses {
            get {
                return this.kvCompanyAddresses ?? (this.kvCompanyAddresses = new ObservableCollection<CompanyAddress>());
            }
        }

        public ObservableCollection<KeyValuePair> KvBranches {
            get {
                return this.kvBranches;
            }
        }

        public ObservableCollection<KeyValuePair> KvReceivingWarehouse {
            get {
                return this.kvReceivingWarehouse;
            }
        }

        public ObservableCollection<KeyValuePair> KvPartsSalesOrderTypes {
            get {
                return this.kvPartsSalesOrderTypes;
            }
        }

        public object KvSecondLevelOrderTypes {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public object KvShippingMethods {
            get {
                return this.KeyValueManager[this.kvNames[1]];
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }
    }

}