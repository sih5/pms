﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Controls;
using Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using FilterOperator = Sunlight.Silverlight.Core.Model.FilterOperator;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesOrderForExportApproveDataEditView : INotifyPropertyChanged {
        private DataGridViewBase partsSalesOrderDetailTemplateApproveDataGridView;
        private DataGridViewBase partsSalesOrderProcessDetailForApproveDataGridView;
        private FrameworkElement partsSalesOrderTemplateApproveDetailPanel;
        private DataGridViewBase partsExchangeApproveDataGridView;
        private ObservableCollection<PartsSalesOrderProcessDetail> partsSalesOrderProcessDetails;
        private ObservableCollection<WarehousePartsStock> warehousePartsStocks;
        private readonly RadTabControl tabControlChild = new RadTabControl();
        private DcsDetailDataEditView partsSalesOrderInsideDataEditView, partsSalesOrderDataEditView, partsExchangeDataEditView;
        private bool buttonCanUse;

        public bool ButtonCanUse {
            get {
                return this.buttonCanUse;
            }
            set {
                this.buttonCanUse = value;
                this.OnPropertyChanged("ButtonCanUse");
            }
        }

        private FrameworkElement partsSalesOrderForDistributionForTemplateApproveDataEditPanel;

        private FrameworkElement PartsSalesOrderForDistributionForTemplateApproveDataEditPanel {
            get {
                return this.partsSalesOrderForDistributionForTemplateApproveDataEditPanel ?? (this.partsSalesOrderForDistributionForTemplateApproveDataEditPanel = DI.GetDataEditPanel("PartsSalesOrderForDistributionForTemplateApprove"));
            }
        }

        private ObservableCollection<PartsSalesOrderDetail> lpartsSalesOrderDetails = new ObservableItemCollection<PartsSalesOrderDetail>();

        private void PartsSalesOrderForExportApproveDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            partsSalesOrder.PropertyChanged -= partsSalesOrder_PropertyChanged;
            partsSalesOrder.PropertyChanged += partsSalesOrder_PropertyChanged;
        }

        private void partsSalesOrder_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            switch(e.PropertyName) {
                case "OutWarehouseId":
                case "SupplierWarehouseId":
                case "ECommercePlatformOutWarehouseId":
                case "CenterWarehouseName":
                    this.UpdatePartsSalesOrderDetails(e.PropertyName, partsSalesOrder);
                    break;
                case "DistributionOWarehouseId":
                    this.ButtonCanUse = false;
                    partsSalesOrder.IsCreatePurchseOrder = false;
                    partsSalesOrder.DistributionIWarehouseId = 0;
                    partsSalesOrder.DistributionIWarehouseCode = string.Empty;
                    partsSalesOrder.DistributionIWarehouseName = string.Empty;
                    this.UpdatePartsSalesOrderDetails(e.PropertyName, partsSalesOrder);
                    break;
            }
        }

        public ObservableCollection<PartsSalesOrderProcessDetail> PartsSalesOrderProcessDetails {
            get {
                if(this.partsSalesOrderProcessDetails == null) {
                    this.partsSalesOrderProcessDetails = new ObservableCollection<PartsSalesOrderProcessDetail>();
                    this.partsSalesOrderProcessDetails.CollectionChanged -= PartsSalesOrderProcessDetails_CollectionChanged;
                    this.partsSalesOrderProcessDetails.CollectionChanged += PartsSalesOrderProcessDetails_CollectionChanged;
                }
                return this.partsSalesOrderProcessDetails;
            }
        }

        public ObservableCollection<WarehousePartsStock> WarehousePartsStocks {
            get {
                return this.warehousePartsStocks ?? (this.warehousePartsStocks = new ObservableCollection<WarehousePartsStock>());
            }
        }

        private void UpdatePartsSalesOrderDetails(string bindingName, PartsSalesOrder partsSalesOrder) {
            var partIds = partsSalesOrder.PartsSalesOrderDetails.Select(v => v.SparePartId).ToArray();
            if(bindingName.Equals("OutWarehouseId") || bindingName.Equals("SupplierWarehouseId")) {
                int currentWarehouseId, ownerCompanyId;
                string currentWarehouseCode, currentWarehouseName;
                if(bindingName.Equals("OutWarehouseId")) {
                    currentWarehouseId = partsSalesOrder.OutWarehouseId ?? 0;
                    currentWarehouseCode = partsSalesOrder.WarehouseCode;
                    currentWarehouseName = partsSalesOrder.WarehouseName;
                    ownerCompanyId = partsSalesOrder.SalesUnitOwnerCompanyId;
                } else {
                    currentWarehouseId = partsSalesOrder.SupplierWarehouseId;
                    currentWarehouseCode = partsSalesOrder.SupplierWarehouseCode;
                    currentWarehouseName = partsSalesOrder.SupplierWarehouseName;
                    ownerCompanyId = partsSalesOrder.SalesUnitOwnerCompanyId;
                }
                this.DomainContext.Load(DomainContext.查询仓库库存销售审核Query(ownerCompanyId, currentWarehouseId, partIds), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError)
                        return;
                    if(bindingName.Equals("OutWarehouseId")) {
                        currentWarehouseCode = partsSalesOrder.WarehouseCode;
                        currentWarehouseName = partsSalesOrder.WarehouseName;
                    }
                    //1.查询仓库库存应返回单条数据（设计确认）2.根据条件查询不到数据，货源仓库及本次出库数量是否清空
                    if(loadOp.Entities == null || !loadOp.Entities.Any()) {
                        if(currentWarehouseId != default(int))
                            foreach(var partsSalesOrderDetail in this.TempPartsSalesOrderDetails) {
                                partsSalesOrderDetail.StockQuantity = default(int);
                                partsSalesOrderDetail.CurrentFulfilledQuantity = default(int);
                                partsSalesOrderDetail.SupplierWarehouseId = currentWarehouseId;
                                partsSalesOrderDetail.SupplierWarehouseCode = currentWarehouseCode;
                                partsSalesOrderDetail.SupplierWarehouseName = currentWarehouseName;
                            } else
                            foreach(var partsSalesOrderDetail in this.TempPartsSalesOrderDetails) {
                                partsSalesOrderDetail.StockQuantity = default(int);
                                partsSalesOrderDetail.CurrentFulfilledQuantity = default(int);
                                partsSalesOrderDetail.SupplierWarehouseId = default(int);
                                partsSalesOrderDetail.SupplierWarehouseCode = string.Empty;
                                partsSalesOrderDetail.SupplierWarehouseName = string.Empty;
                            }
                        return;
                    }
                    foreach(var entityOrder in loadOp.Entities) {
                        foreach(var entity in this.TempPartsSalesOrderDetails)
                            if(entity.SparePartId == entityOrder.SparePartId) {
                                entity.StockQuantity = entityOrder.UsableQuantity;
                                var salesOrderProcessDetails = this.PartsSalesOrderProcessDetails.Where(r => (r.WarehouseId == currentWarehouseId) && r.SparePartId == entity.SparePartId && r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.本部处理);
                                if(salesOrderProcessDetails.Any())
                                    entity.StockQuantity = entity.StockQuantity - salesOrderProcessDetails.Sum(r => r.CurrentFulfilledQuantity) <= 0 ? 0 : entity.StockQuantity - salesOrderProcessDetails.Sum(r => r.CurrentFulfilledQuantity);
                                Debug.Assert(partsSalesOrder.OutWarehouseId != null, "partsSalesOrder.OutWarehouseId != null");
                                entity.SupplierWarehouseId = currentWarehouseId;
                                entity.SupplierWarehouseCode = currentWarehouseCode;
                                entity.SupplierWarehouseName = currentWarehouseName;
                                if(entity.StockQuantity >= entity.UnfulfilledQuantity)
                                    entity.CurrentFulfilledQuantity = entity.UnfulfilledQuantity;
                                if(entity.StockQuantity > 0 && entity.StockQuantity < entity.UnfulfilledQuantity)
                                    entity.CurrentFulfilledQuantity = entity.StockQuantity;
                            }
                    }
                    var ids = loadOp.Entities.Select(v => v.SparePartId).ToArray();
                    foreach(var entity in this.TempPartsSalesOrderDetails.Where(r => !ids.Contains(r.SparePartId))) {
                        entity.StockQuantity = default(int);
                        entity.CurrentFulfilledQuantity = default(int);
                        entity.SupplierWarehouseId = currentWarehouseId;
                        entity.SupplierWarehouseCode = currentWarehouseCode;
                        entity.SupplierWarehouseName = currentWarehouseName;
                    }
                }, null);
            }
        }

        /// <summary>
        /// 配件销售订单清单 非转内部直供
        /// </summary>
        private DataGridViewBase PartsSalesOrderDetailTemplateApproveDataGridView {
            get {
                if(this.partsSalesOrderDetailTemplateApproveDataGridView == null) {
                    this.partsSalesOrderDetailTemplateApproveDataGridView = DI.GetDataGridView("PartsSalesOrderDetailTemplateApprove");
                    this.PartsSalesOrderDetailTemplateApproveDataGridView.DataContext = this;
                    this.partsSalesOrderDetailTemplateApproveDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsSalesOrderDetailTemplateApproveDataGridView;
            }
        }

        /// <summary>
        /// 配件销售订单清单 转内部直供
        /// </summary>
        private DataGridViewBase partsSalesOrderDetailInsideTemplateApproveDataGridView;
        private DataGridViewBase PartsSalesOrderDetailInsideTemplateApproveDataGridView {
            get {
                if(this.partsSalesOrderDetailInsideTemplateApproveDataGridView == null) {
                    this.partsSalesOrderDetailInsideTemplateApproveDataGridView = DI.GetDataGridView("PartsSalesOrderDetailInsideTemplateApprove");
                    this.partsSalesOrderDetailInsideTemplateApproveDataGridView.DataContext = this;
                    this.partsSalesOrderDetailInsideTemplateApproveDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsSalesOrderDetailInsideTemplateApproveDataGridView;
            }
        }

        /// <summary>
        /// 处理清单
        /// </summary>
        private DataGridViewBase PartsSalesOrderProcessDetailForApproveDataGridView {
            get {
                if(this.partsSalesOrderProcessDetailForApproveDataGridView == null) {
                    this.partsSalesOrderProcessDetailForApproveDataGridView = DI.GetDataGridView("PartsSalesOrderProcessDetailForPartsSalesOrder");
                    this.partsSalesOrderProcessDetailForApproveDataGridView.DomainContext = this.DomainContext;
                    this.partsSalesOrderProcessDetailForApproveDataGridView.DataContext = this;
                }
                return this.partsSalesOrderProcessDetailForApproveDataGridView;
            }
        }

        /// <summary>
        /// 替互换件审批清单
        /// </summary>
        private DataGridViewBase PartsExchangeApproveDataGridView {
            get {
                if(this.partsExchangeApproveDataGridView == null) {
                    this.partsExchangeApproveDataGridView = DI.GetDataGridView("PartsExchangeApprove");
                    this.partsExchangeApproveDataGridView.DataContext = this;
                    this.partsExchangeApproveDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsExchangeApproveDataGridView;
            }
        }

        /// <summary>
        /// 订单主信息
        /// </summary>
        private FrameworkElement PartsSalesOrderTemplateApproveDetailPanel {
            get {
                return this.partsSalesOrderTemplateApproveDetailPanel ?? (this.partsSalesOrderTemplateApproveDetailPanel = DI.GetDetailPanel("PartsSalesOrderTemplateApprove"));
            }
        }

        private RadTabControl radTabControl;
        private RadTabControl RadTabControl {
            get {
                return this.radTabControl ?? (this.radTabControl = new RadTabControl());
            }
        }

        private void CreateUI() {
            this.RegisterButton(new ButtonItem {
                Command = new DelegateCommand(this.CheckPartsStockForAgenchyWindow.ShowDialog),
                Icon = Utils.MakeServerUri("Client/Dcs/Images/Operations/CheckStock.png"),
                Title = PartsSalesUIStrings.DataEdit_Title_AgencyWareHouse
            }, true);
            this.ButtonCanUse = false;
            var partsSalesOrderForSecondTemplateApproveDataEditPanel = DI.GetDataEditPanel("PartsSalesOrderForSecondTemplateApprove");
            this.RadTabControl.BorderThickness = new Thickness(0);
            this.RadTabControl.BackgroundVisibility = Visibility.Collapsed;
            this.RadTabControl.SetValue(StyleManager.ThemeProperty, new Office_BlackTheme());
            var tabItemsGrid = new Grid();

            tabItemsGrid.RowDefinitions.Add(new RowDefinition {
                Height = GridLength.Auto
            });
            tabControlChild.SetValue(StyleManager.ThemeProperty, new Office_BlackTheme());
            tabControlChild.Items.Add(new RadTabItem {
                Header = PartsSalesUIStrings.DataEdit_Header_PartsSalesOrder_Local,
                Content = DI.GetDataEditPanel("PartsSalesOrderForTemplateApprove")
            });
            tabControlChild.SelectionChanged += TabControlChild_SelectionChanged;
            tabItemsGrid.Children.Add(tabControlChild);
            var tabItemsEditGrid = new Grid();
            tabItemsEditGrid.RowDefinitions.Add(new RowDefinition {
                Height = GridLength.Auto
            });
            tabItemsEditGrid.RowDefinitions.Add(new RowDefinition {
                Height = GridLength.Auto
            });
            tabItemsEditGrid.RowDefinitions.Add(new RowDefinition {
                Height = GridLength.Auto
            });
            tabItemsEditGrid.RowDefinitions.Add(new RowDefinition {
                Height = GridLength.Auto
            });
            tabItemsEditGrid.RowDefinitions.Add(new RowDefinition {
                Height = GridLength.Auto,
                MaxHeight = 200
            });
            tabItemsEditGrid.ColumnDefinitions.Add(new ColumnDefinition {
                Width = GridLength.Auto
            });
            tabItemsEditGrid.ColumnDefinitions.Add(new ColumnDefinition {
                Width = new GridLength(10)
            });
            tabItemsEditGrid.ColumnDefinitions.Add(new ColumnDefinition {
                Width = GridLength.Auto
            });
            var titlOrderHanderWay = new TextBlock {
                Text = PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_ProcessingMode,
                FontSize = 14,
                Margin = new Thickness(3, 10, 3, 6),
                VerticalAlignment = VerticalAlignment.Bottom
            };
            partsSalesOrderForSecondTemplateApproveDataEditPanel.SetValue(Grid.ColumnSpanProperty, 3);
            tabItemsEditGrid.Children.Add(partsSalesOrderForSecondTemplateApproveDataEditPanel);
            titlOrderHanderWay.SetValue(Grid.RowProperty, 1);
            tabItemsEditGrid.Children.Add(titlOrderHanderWay);
            tabItemsGrid.SetValue(Grid.RowProperty, 2);
            tabItemsGrid.SetValue(Grid.ColumnSpanProperty, 3);
            tabItemsEditGrid.Children.Add(tabItemsGrid);
            //创建非内部转直供的清单面板
            partsSalesOrderDataEditView = new DcsDetailDataEditView();
            CreatePartsSalesDetailGird(tabItemsEditGrid, partsSalesOrderDataEditView, this.PartsSalesOrderDetailTemplateApproveDataGridView, false);
            //创建内部转直供的清单面板
            partsSalesOrderInsideDataEditView = new DcsDetailDataEditView();
            CreatePartsSalesDetailGird(tabItemsEditGrid, partsSalesOrderInsideDataEditView, this.PartsSalesOrderDetailInsideTemplateApproveDataGridView, true);
            partsExchangeDataEditView = new DcsDetailDataEditView();
            partsExchangeDataEditView.UnregisterButton(partsExchangeDataEditView.InsertButton);
            partsExchangeDataEditView.UnregisterButton(partsExchangeDataEditView.DeleteButton);
            partsExchangeDataEditView.Register(PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_ReplaceSpart, null, () => this.PartsExchangeApproveDataGridView);
            this.PartsExchangeApproveDataGridView.SetValue(MinHeightProperty, Convert.ToDouble(200));
            partsExchangeDataEditView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.DataEditView_Text_Button_Search,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/query.png", UriKind.Relative),
                Command = new DelegateCommand(this.Inquiry)
            });
            partsExchangeDataEditView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_AddList,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/import.png", UriKind.Relative),
                Command = new DelegateCommand(this.GetInquiry)
            }, true);
            partsExchangeDataEditView.SetValue(Grid.RowProperty, 3);
            partsExchangeDataEditView.SetValue(VerticalAlignmentProperty, VerticalAlignment.Top);
            partsExchangeDataEditView.SetValue(Grid.ColumnProperty, 2);
            tabItemsEditGrid.Children.Add(partsExchangeDataEditView);
            var partsSalesOrderProcessDetailForApproveDataEditView = new DcsDetailDataEditView();
            partsSalesOrderProcessDetailForApproveDataEditView.UnregisterButton(partsSalesOrderProcessDetailForApproveDataEditView.InsertButton);
            partsSalesOrderProcessDetailForApproveDataEditView.Register(PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_ProcessingList, null, () => this.PartsSalesOrderProcessDetailForApproveDataGridView);
            PartsSalesOrderProcessDetailForApproveDataGridView.SetValue(MinHeightProperty, Convert.ToDouble(200));
            partsSalesOrderProcessDetailForApproveDataEditView.SetValue(Grid.RowProperty, 4);
            partsSalesOrderProcessDetailForApproveDataEditView.SetValue(Grid.ColumnSpanProperty, 3);
            tabItemsEditGrid.Children.Add(partsSalesOrderProcessDetailForApproveDataEditView);

            this.RadTabControl.Items.Add(new RadTabItem {
                Header = PartsSalesUIStrings.DataEditPanel_GroupTitle_ApprovalWorkbench,
                Content = tabItemsEditGrid
            });
            this.RadTabControl.Items.Add(new RadTabItem {
                Header = PartsSalesUIStrings.DataEditPanel_GroupTitle_PartsSalesOrder,
                Content = this.PartsSalesOrderTemplateApproveDetailPanel
            });
            this.LayoutRoot.Children.Add(this.RadTabControl);
        }

        private void CreatePartsSalesDetailGird(Grid grid, DcsDetailDataEditView detailView, DataGridViewBase dataGridView, bool defaultVisibility) {
            detailView.UnregisterButton(detailView.InsertButton);
            detailView.UnregisterButton(detailView.DeleteButton);
            detailView.Register(PartsSalesUIStrings.DataEditPanel_Title_PartsSalesOrderDetail, null, () => dataGridView);
            detailView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_AddList,
                Command = new DelegateCommand(this.ProcessInternal)
            }, true);
            detailView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_PartsBranchInfoQuery,
                Command = new DelegateCommand(this.DetailPartsBranchInfoQuery)
            }, true);
            detailView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_CompanyPartsStockQuery,
                Command = new DelegateCommand(this.SubmitCompanyPartsStockQuery)
            }, true);
            detailView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_PartsPurchaseOrderQuery,
                Command = new DelegateCommand(this.PartsPurchaseOrderQuery)
            }, true);

            dataGridView.SetValue(MinHeightProperty, Convert.ToDouble(200));
            dataGridView.SetValue(MaxHeightProperty, Convert.ToDouble(300));
            detailView.SetValue(Grid.RowProperty, 3);
            detailView.SetValue(Grid.VerticalAlignmentProperty, VerticalAlignment.Top);
            if(defaultVisibility) {
                detailView.SetValue(VisibilityProperty, Visibility.Collapsed);
            }
            grid.Children.Add(detailView);
        }

        private int lastIndex = 0;
        public void TabControlChild_SelectionChanged(object sender, RadSelectionChangedEventArgs e) {
            this.PartsSalesOrderDetailTemplateApproveDataGridView.CommitEdit();
            var partsSaleOrder = this.DataContext as PartsSalesOrder;
            if(partsSaleOrder == null)
                return;
            var selectTabItem = this.tabControlChild.SelectedItem as RadTabItem;
            if(selectTabItem == null)
                return;
            partsSaleOrder.ProcessMethod = this.SetOrderProcessMethod();
            switch(selectTabItem.Header.ToString()) {
                case "本部处理":
                    partsSalesOrderDataEditView.SetValue(VisibilityProperty, Visibility.Visible);
                    partsSalesOrderInsideDataEditView.SetValue(VisibilityProperty, Visibility.Collapsed);
                    if(selectTabItem.Header.ToString().Equals(PartsSalesUIStrings.DataEdit_Header_PartsSalesOrder_Local)) {
                        if(!partsSaleOrder.OutWarehouseId.HasValue)
                            return;
                        this.UpdatePartsSalesOrderDetails("OutWarehouseId", partsSaleOrder);
                    }
                    break;
            }
        }

        private void PartsSalesOrderProcessDetails_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;

            if(e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove) {
                var currentRemovePartsSalesOrderProcessDetails = e.OldItems.Cast<PartsSalesOrderProcessDetail>();
                foreach(var currentRemoveEntity in currentRemovePartsSalesOrderProcessDetails) {
                    if(!this.TempPartsSalesOrderDetails.Any(r => r.SparePartId == currentRemoveEntity.SparePartId)) {
                        this.TempPartsSalesOrderDetails.Add(lpartsSalesOrderDetails.First(r => r.SparePartId == currentRemoveEntity.SparePartId));
                    }
                    var partsSalesOrderDetails = this.TempPartsSalesOrderDetails.Where(v => v.SparePartId == currentRemoveEntity.SparePartId);
                    if(partsSalesOrderDetails.Any())
                        foreach(var partsSalesOrderDetail in partsSalesOrderDetails) {
                            partsSalesOrderDetail.UnfulfilledQuantity += currentRemoveEntity.CurrentFulfilledQuantity;
                        }
                }
                var selectTabItem = this.tabControlChild.SelectedItem as RadTabItem;
                if(selectTabItem == null)
                    return;
                switch(selectTabItem.Header.ToString()) {
                    case "本部处理":
                        ObservableCollection<PartsSalesOrderDetail> lPartsSalesOrderDetails;
                        ObservableCollection<PartsSalesOrderDetail> nPartsSalesOrderDetails;
                        if(selectTabItem.Header.ToString().Equals(PartsSalesUIStrings.DataEdit_Header_PartsSalesOrder_Local)) {
                            if(!partsSalesOrder.OutWarehouseId.HasValue)
                                break;
                            if(this.IPartsSalesOrderDetails.TryGetValue(lastIndex, out lPartsSalesOrderDetails)) {
                                SetTempPartsSalesOrderDetailStockQuantityWhenRemovePartsSalesOrderProcessDetail(PartsSalesUIStrings.DataEdit_Header_PartsSalesOrder_Local, partsSalesOrder, lPartsSalesOrderDetails, false);
                            }

                            lastIndex = this.tabControlChild.SelectedIndex;
                            if(this.IPartsSalesOrderDetails.TryGetValue(lastIndex, out nPartsSalesOrderDetails)) {
                                SetTempPartsSalesOrderDetailStockQuantityWhenRemovePartsSalesOrderProcessDetail(PartsSalesUIStrings.DataEdit_Header_PartsSalesOrder_Local, partsSalesOrder, nPartsSalesOrderDetails, true);
                            }
                        }
                        break;
                }
                if(currentRemovePartsSalesOrderProcessDetails.Any(r => Enum.GetName(typeof(DcsPartsSalesOrderProcessDetailProcessMethod), r.OrderProcessMethod) != selectTabItem.Header.ToString())) {
                    var orderProcessMethod = currentRemovePartsSalesOrderProcessDetails.First(r => Enum.GetName(typeof(DcsPartsSalesOrderProcessDetailProcessMethod), r.OrderProcessMethod) != selectTabItem.Header.ToString()).OrderProcessMethod;
                    if(orderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.本部处理)
                        this.tabControlChild.SelectedIndex = 0;
                }
            }
        }

        private void Inquiry() {
            if(this.PartsSalesOrderDetailTemplateApproveDataGridView.SelectedEntities == null || !this.PartsSalesOrderDetailTemplateApproveDataGridView.SelectedEntities.Any()) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_ChoiceOne);
                return;
            }
            PartsSalesOrderDetail partsSaleOrderDetail = this.PartsSalesOrderDetailTemplateApproveDataGridView.SelectedEntities.Cast<PartsSalesOrderDetail>().SingleOrDefault();

            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;

            if(partsSaleOrderDetail == null || !partsSaleOrderDetail.IfCanNewPart)
                return;
            switch(this.GetTabItemName()) {
                 case "本部处理":
                    this.GetTiHuHuanKuCun(partsSaleOrderDetail);
                    break;
            }
        }

        //代理库库存校验
        private RadWindow checkPartsStockForAgenchWindow;
        private RadWindow CheckPartsStockForAgenchyWindow {
            get {
                return this.checkPartsStockForAgenchWindow ?? (this.checkPartsStockForAgenchWindow = new RadWindow {
                    Content = this.CheckPartsStockForAgenchQueryWindow,
                    Header = PartsSalesUIStrings.QueryPanel_Title_CheckPartsStockForAgench,
                    WindowState = WindowState.Normal,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                    AllowDrop = false,
                    ResizeMode = ResizeMode.NoResize,
                    CanClose = true
                });
            }
        }

        private QueryWindowBase checkPartsStockForAgenchQueryWindow;
        private QueryWindowBase CheckPartsStockForAgenchQueryWindow {
            get {
                if(checkPartsStockForAgenchQueryWindow == null) {
                    this.checkPartsStockForAgenchQueryWindow = DI.GetQueryWindow("CheckPartsStockForAgench");
                    this.checkPartsStockForAgenchQueryWindow.Loaded += repairClaimBillHistoryQueryWindow_Loaded;
                }
                return this.checkPartsStockForAgenchQueryWindow;
            }
        }

        private void repairClaimBillHistoryQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null || queryWindow == null)
                return;
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] { "Common", "SparePartCode", "" });
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] { "Common", "SparePartName", "" });
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", new FilterItem("PartsSalesOrderId", typeof(int), FilterOperator.IsEqualTo, partsSalesOrder.Id));
        }

        //查询替互换件库存
        private void GetTiHuHuanKuCun(PartsSalesOrderDetail partsSaleOrderDetail) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            this.DomainContext.Load(this.DomainContext.查询替互换件库存销售审核Query(partsSalesOrder.SalesCategoryId, partsSaleOrderDetail.SupplierWarehouseId, partsSaleOrderDetail.SparePartId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Entities == null)
                    return;
                this.WarehousePartsStocks.Clear();
                foreach(var warehousePartsStock in loadOp.Entities) {
                    var warehousePartsStockItem = new WarehousePartsStock();
                    warehousePartsStockItem.ReplacementType = warehousePartsStock.ReplacementType;
                    warehousePartsStockItem.Quantity = warehousePartsStock.UsableQuantity;
                    warehousePartsStockItem.UsableQuantity = partsSaleOrderDetail.UnfulfilledQuantity - warehousePartsStock.Quantity > 0 ? warehousePartsStock.Quantity : partsSaleOrderDetail.UnfulfilledQuantity;
                    warehousePartsStockItem.NewPartId = warehousePartsStock.NewPartId;
                    warehousePartsStockItem.NewPartCode = warehousePartsStock.NewPartCode;
                    warehousePartsStockItem.NewPartName = warehousePartsStock.NewPartName;
                    warehousePartsStockItem.SparePartId = partsSaleOrderDetail.SparePartId;
                    warehousePartsStockItem.SparePartCode = partsSaleOrderDetail.SparePartCode;
                    warehousePartsStockItem.SparePartName = partsSaleOrderDetail.SparePartName;
                    warehousePartsStockItem.PartsSalesPrice = warehousePartsStock.PartsSalesPrice;
                    var salesOrderProcessDetails = this.PartsSalesOrderProcessDetails.Where(r => (r.WarehouseId == partsSaleOrderDetail.SupplierWarehouseId) && r.NewPartId == warehousePartsStock.NewPartId);
                    if(salesOrderProcessDetails != null)
                        //根据处理清单的本次满足数量更新可用库存
                        warehousePartsStockItem.Quantity = warehousePartsStockItem.Quantity - salesOrderProcessDetails.Sum(r => r.CurrentFulfilledQuantity) <= 0 ? 0 : warehousePartsStockItem.Quantity - salesOrderProcessDetails.Sum(r => r.CurrentFulfilledQuantity);
                    this.WarehousePartsStocks.Add(warehousePartsStockItem);
                }
                this.DomainContext.Load(this.DomainContext.根据配件清单获取销售价格出口使用Query(partsSalesOrder.PartsSalesOrderTypeId, partsSalesOrder.ReceivingCompanyId, this.WarehousePartsStocks.Select(r => r.NewPartId).ToArray(), partsSalesOrder.PriceType ?? 0), LoadBehavior.RefreshCurrent, loadOption => {
                    if(loadOption.HasError)
                        return;
                    foreach(var detail in this.WarehousePartsStocks) {
                        var virtualPartsSalesPrice = loadOption.Entities.FirstOrDefault(r => r.SparePartId == detail.NewPartId);
                        if(virtualPartsSalesPrice == null) {
                            UIHelper.ShowAlertMessage(string.Format(PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_PartsSalesPrice, detail.SparePartCode));
                            return;
                        }
                        detail.PartsSalesPrice = virtualPartsSalesPrice.Price;
                    }
                }, null);
            }, null);
        }

        //查询经销商替互件库存
        private void GetJxsTiHuHuanKuCun(PartsSalesOrder partsSalesOrder, PartsSalesOrderDetail partsSalesOrderDetail) {
            this.DomainContext.Load(this.DomainContext.查询经销商替互换件库存Query(partsSalesOrder.SalesCategoryId, partsSalesOrder.SupplierCompanyId, partsSalesOrderDetail.SparePartId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    if(!loadOp.IsErrorHandled) {
                        loadOp.MarkErrorAsHandled();
                        return;
                    }
                if(loadOp.Entities == null)
                    return;
                this.WarehousePartsStocks.Clear();
                foreach(var warehousePartsStock in loadOp.Entities) {
                    var warehousePartsStockItem = new WarehousePartsStock();
                    warehousePartsStockItem.ReplacementType = warehousePartsStock.ReplacementType;
                    warehousePartsStockItem.Quantity = warehousePartsStock.UsableQuantity;
                    warehousePartsStockItem.UsableQuantity = partsSalesOrderDetail.UnfulfilledQuantity - warehousePartsStock.Quantity > 0 ? warehousePartsStock.Quantity : partsSalesOrderDetail.UnfulfilledQuantity;
                    warehousePartsStockItem.NewPartId = warehousePartsStock.NewPartId;
                    warehousePartsStockItem.NewPartCode = warehousePartsStock.NewPartCode;
                    warehousePartsStockItem.NewPartName = warehousePartsStock.NewPartName;
                    warehousePartsStockItem.SparePartId = partsSalesOrderDetail.SparePartId;
                    warehousePartsStockItem.SparePartCode = partsSalesOrderDetail.SparePartCode;
                    warehousePartsStockItem.SparePartName = partsSalesOrderDetail.SparePartName;
                    warehousePartsStockItem.PartsSalesPrice = warehousePartsStock.PartsSalesPrice;
                    var salesOrderProcessDetails = this.PartsSalesOrderProcessDetails.Where(r => (r.WarehouseId == partsSalesOrderDetail.SupplierWarehouseId) && r.NewPartId == warehousePartsStock.NewPartId);
                    if(salesOrderProcessDetails != null)
                        //根据处理清单的本次满足数量更新可用库存
                        warehousePartsStockItem.Quantity = warehousePartsStockItem.Quantity - salesOrderProcessDetails.Sum(r => r.CurrentFulfilledQuantity) <= 0 ? 0 : warehousePartsStockItem.Quantity - salesOrderProcessDetails.Sum(r => r.CurrentFulfilledQuantity);
                    this.WarehousePartsStocks.Add(warehousePartsStockItem);
                }
                this.DomainContext.Load(this.DomainContext.根据配件清单获取销售价格出口使用Query(partsSalesOrder.PartsSalesOrderTypeId, partsSalesOrder.ReceivingCompanyId, this.WarehousePartsStocks.Select(r => r.NewPartId).ToArray(), partsSalesOrder.PriceType ?? 0), LoadBehavior.RefreshCurrent, loadOption => {
                    if(loadOption.HasError)
                        return;
                    foreach(var detail in this.WarehousePartsStocks) {
                        var virtualPartsSalesPrice = loadOption.Entities.FirstOrDefault(r => r.SparePartId == detail.NewPartId);
                        if(virtualPartsSalesPrice == null) {
                            UIHelper.ShowAlertMessage(string.Format(PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_PartsSalesPrice, detail.SparePartCode));
                            return;
                        }
                        detail.PartsSalesPrice = virtualPartsSalesPrice.Price;
                    }
                }, null);
            }, null);

        }

        //添加处理清单
        private void ProcessInternal() {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            var tabName = this.GetTabItemName();
            this.PartsSalesOrderDetailTemplateApproveDataGridView.CommitEdit();
            this.PartsSalesOrderDetailInsideTemplateApproveDataGridView.CommitEdit();
            var hasError = false;
            var hasError2 = false;
            foreach(var selectedEntity in this.TempPartsSalesOrderDetails) {
                if(!tabName.Equals(PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_DirectSupply) /*&& !tabName.Equals("转内部直供")*/ && (selectedEntity.CurrentFulfilledQuantity > selectedEntity.UnfulfilledQuantity || selectedEntity.CurrentFulfilledQuantity > selectedEntity.StockQuantity)) {
                    hasError = true;
                    continue;
                }
                if(tabName.Equals(PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_DirectSupply) && selectedEntity.CurrentFulfilledQuantity > selectedEntity.UnfulfilledQuantity) {
                    hasError2 = true;
                    continue;
                }

                //根据配件ID，仓库ID，订单处理方式,替互换类型ID为空进行合并
                selectedEntity.UnfulfilledQuantity = selectedEntity.UnfulfilledQuantity - selectedEntity.CurrentFulfilledQuantity;
                var salesOrderProcessDetails = this.PartsSalesOrderProcessDetails.Where(r => (r.WarehouseId == selectedEntity.SupplierWarehouseId || (r.PurchaseWarehouseId == selectedEntity.SupplierWarehouseId && r.WarehouseId == partsSalesOrder.DistributionIWarehouseId)) && r.SparePartId == selectedEntity.SparePartId && r.NewPartId == null && this.PartsSalesOrderProcessDetails.Any(p => p.OrderProcessMethod == this.SetOrderProcessMethod()));
                if(salesOrderProcessDetails != null && salesOrderProcessDetails.Any()) {
                    foreach(var partsSalesOrderProcessDetail in salesOrderProcessDetails.ToList()) {
                        partsSalesOrderProcessDetail.OrderedQuantity = selectedEntity.OrderedQuantity;
                        partsSalesOrderProcessDetail.UnfulfilledQuantity = selectedEntity.UnfulfilledQuantity;
                        partsSalesOrderProcessDetail.CurrentFulfilledQuantity += selectedEntity.CurrentFulfilledQuantity;
                        SetTempPartsSalesOrderDetailStockQuantity(selectedEntity, partsSalesOrderProcessDetail);
                        SetPartsSalesOrderProcessDetailOrderProcessStatus(selectedEntity, partsSalesOrderProcessDetail);
                    }
                } else {
                    if(selectedEntity.CurrentFulfilledQuantity > 0) {
                        var partsSalesOrderProcessDetail = new PartsSalesOrderProcessDetail {
                            SparePartId = selectedEntity.SparePartId,
                            SparePartCode = selectedEntity.SparePartCode,
                            SparePartName = selectedEntity.SparePartName,
                            Factury = selectedEntity.SparePart.Factury,
                            OrderedQuantity = selectedEntity.OrderedQuantity,
                            UnfulfilledQuantity = selectedEntity.UnfulfilledQuantity,
                            CurrentFulfilledQuantity = selectedEntity.CurrentFulfilledQuantity,
                            OrderPrice = selectedEntity.OrderPrice,
                            WarehouseId = selectedEntity.SupplierWarehouseId,
                            WarehouseCode = selectedEntity.SupplierWarehouseCode,
                            WarehouseName = selectedEntity.SupplierWarehouseName,
                            OrderProcessMethod = this.SetOrderProcessMethod()
                        };
                        SetTempPartsSalesOrderDetailStockQuantity(selectedEntity, partsSalesOrderProcessDetail);
                        SetPartsSalesOrderProcessDetailOrderProcessStatus(selectedEntity, partsSalesOrderProcessDetail);
                        this.PartsSalesOrderProcessDetails.Add(partsSalesOrderProcessDetail);
                    }
                }
                selectedEntity.CurrentFulfilledQuantity = 0;
            }
            if(hasError) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_Amount);
                hasError = false;
            }
            if(hasError2) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderProcessDetail_CurrentFulfilledQuantityGreateThanUnfulfilledQuantityError);
                hasError2 = false;
            }
            if(partsSalesOrder.PartsSalesOrderDetails.Any(r => r.UnfulfilledQuantity == 0)) {
                foreach(var detail in this.TempPartsSalesOrderDetails.Where(r => r.UnfulfilledQuantity == 0).ToArray()) {
                    this.TempPartsSalesOrderDetails.Remove(detail);
                    lpartsSalesOrderDetails.Add(detail);
                }
            }
            if(partsSalesOrder.PartsSalesOrderDetails.Any(r => r.UnfulfilledQuantity != 0)) {

            }
        }

        //根据处理单清单的本次满足数量更新销售订单清单的可用库存
        private void SetTempPartsSalesOrderDetailStockQuantity(PartsSalesOrderDetail selectedEntity, PartsSalesOrderProcessDetail partsSalesOrderProcessDetail) {
            if(selectedEntity.UnfulfilledQuantity != 0)
                selectedEntity.StockQuantity = selectedEntity.StockQuantity - partsSalesOrderProcessDetail.CurrentFulfilledQuantity <= 0 ? 0 : selectedEntity.StockQuantity - partsSalesOrderProcessDetail.CurrentFulfilledQuantity;
        }

        //删除处理清单时更新可用库存
        private void SetTempPartsSalesOrderDetailStockQuantityWhenRemovePartsSalesOrderProcessDetail(string tabName, PartsSalesOrder partsSalesOrder, ObservableCollection<PartsSalesOrderDetail> outpartsSalesOrderDetails, bool isFirst) {
            if(tabName.Equals(PartsSalesUIStrings.DataEdit_Header_PartsSalesOrder_Local)) {
                var sparePartIds = this.TempPartsSalesOrderDetails.Select(r => r.SparePartId).ToArray();
                var nSparePartIds = outpartsSalesOrderDetails.Select(r => r.SparePartId).ToArray();
                this.DomainContext.Load(DomainContext.查询仓库库存销售审核Query(partsSalesOrder.SalesUnitOwnerCompanyId, partsSalesOrder.OutWarehouseId, isFirst ? nSparePartIds : sparePartIds), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    if(loadOp.Entities != null && loadOp.Entities.Any()) {
                        if(isFirst) {
                            foreach(var partsSalesOrderDetail in outpartsSalesOrderDetails) {
                                var detail = TempPartsSalesOrderDetails.SingleOrDefault(l => l.SparePartId == partsSalesOrderDetail.SparePartId);
                                if(detail != null) {
                                    var currentExitPartsSalesOrderProcessDetails = this.PartsSalesOrderProcessDetails.Where(r => r.WarehouseId == partsSalesOrder.OutWarehouseId && r.SparePartId == detail.SparePartId && r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.本部处理).ToArray();
                                    detail.StockQuantity = loadOp.Entities.FirstOrDefault(r => r.SparePartId == detail.SparePartId) != null ? loadOp.Entities.First(r => r.SparePartId == detail.SparePartId).UsableQuantity : 0;
                                    if(currentExitPartsSalesOrderProcessDetails != null)
                                        detail.StockQuantity = detail.StockQuantity - currentExitPartsSalesOrderProcessDetails.Sum(r => r.CurrentFulfilledQuantity);
                                    detail.SupplierWarehouseId = partsSalesOrder.OutWarehouseId.Value;
                                    detail.SupplierWarehouseCode = partsSalesOrder.WarehouseCode;
                                    detail.SupplierWarehouseName = partsSalesOrder.WarehouseName;
                                    detail.CurrentFulfilledQuantity = detail.UnfulfilledQuantity > detail.StockQuantity ? detail.StockQuantity : detail.UnfulfilledQuantity;
                                }
                            }
                        } else {
                            foreach(var partsSalesOrderDetail in this.TempPartsSalesOrderDetails) {
                                var detail = outpartsSalesOrderDetails.SingleOrDefault(l => l.SparePartId == partsSalesOrderDetail.SparePartId);
                                if(detail != null) {
                                    var currentExitPartsSalesOrderProcessDetails = this.PartsSalesOrderProcessDetails.Where(r => r.WarehouseId == partsSalesOrder.OutWarehouseId && r.SparePartId == detail.SparePartId && r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.本部处理).ToArray();
                                    detail.StockQuantity = loadOp.Entities.FirstOrDefault(r => r.SparePartId == detail.SparePartId) != null ? loadOp.Entities.First(r => r.SparePartId == detail.SparePartId).UsableQuantity : 0;
                                    if(currentExitPartsSalesOrderProcessDetails != null)
                                        detail.StockQuantity = detail.StockQuantity - currentExitPartsSalesOrderProcessDetails.Sum(r => r.CurrentFulfilledQuantity);
                                    detail.SupplierWarehouseId = partsSalesOrder.OutWarehouseId.Value;
                                    detail.SupplierWarehouseCode = partsSalesOrder.WarehouseCode;
                                    detail.SupplierWarehouseName = partsSalesOrder.WarehouseName;

                                }
                            }
                        }
                    }

                }, null);
            }
        }

        private static void SetPartsSalesOrderProcessDetailOrderProcessStatus(PartsSalesOrderDetail selectedEntity, PartsSalesOrderProcessDetail partsSalesOrderProcessDetail) {
            //未满足数量=本次满足数量  赋值：订单处理状态=满足， 未满足数量>本次满足数量  赋值：订单处理状态=未满足
            if(selectedEntity.UnfulfilledQuantity == 0)
                partsSalesOrderProcessDetail.OrderProcessStatus = (int)DcsPartsSalesOrderProcessDetailOrderProcessStatus.满足;
            else
                partsSalesOrderProcessDetail.OrderProcessStatus = (int)DcsPartsSalesOrderProcessDetailOrderProcessStatus.未满足;
        }

        //订单处理方法赋值
        private int SetOrderProcessMethod() {
            switch((this.tabControlChild.SelectedItem as RadTabItem).Header.ToString()) {
                case "本部处理":
                    return (int)DcsPartsSalesOrderProcessDetailProcessMethod.本部处理;
                default:
                    return -1;
            }
        }

        //添加替互换
        private void GetInquiry() {
            var tabName = this.GetTabItemName();
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            if(this.PartsExchangeApproveDataGridView.SelectedEntities == null || !this.PartsExchangeApproveDataGridView.SelectedEntities.Any())
                return;
            var partsSaleOrderDetail = this.PartsSalesOrderDetailTemplateApproveDataGridView.SelectedEntities.Cast<PartsSalesOrderDetail>().FirstOrDefault();
            if(partsSaleOrderDetail == null || !partsSaleOrderDetail.IfCanNewPart)
                return;

            switch(tabName) {
                case "本部处理":
                    if(this.PartsExchangeApproveDataGridView.SelectedEntities == null || !this.PartsExchangeApproveDataGridView.SelectedEntities.Any()) {
                        UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_ChoiceOne);
                        return;
                    }
                    foreach(var selectedEntity in this.PartsExchangeApproveDataGridView.SelectedEntities.Cast<WarehousePartsStock>()) {
                        if(partsSalesOrder.PartsSalesOrderDetails == null || !partsSalesOrder.PartsSalesOrderDetails.Any())
                            return;
                        var detail = partsSalesOrder.PartsSalesOrderDetails.First(r => r.SparePartId == selectedEntity.SparePartId);
                        if(selectedEntity.UsableQuantity > detail.UnfulfilledQuantity) {
                            UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderProcessDetail_CurrentFulfilledQuantityGreateThanUnfulfilledQuantityError);
                            return;
                        }
                        if(selectedEntity.UsableQuantity > selectedEntity.Quantity) {
                            UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_UsableQuantity);
                            return;
                        }
                        var partsSalesOrderProcessDetails = this.PartsSalesOrderProcessDetails.Where(r => (r.WarehouseId == selectedEntity.WarehouseId || (r.PurchaseWarehouseId == selectedEntity.WarehouseId && r.WarehouseId == partsSalesOrder.DistributionIWarehouseId)) && r.SparePartId == selectedEntity.SparePartId && r.NewPartId == selectedEntity.NewPartId && this.PartsSalesOrderProcessDetails.Any(p => p.OrderProcessMethod == this.SetOrderProcessMethod()));
                        if(partsSalesOrderProcessDetails.Any()) {
                            foreach(var partsSalesOrderProcessDetail in partsSalesOrderProcessDetails.ToList()) {
                                partsSalesOrderProcessDetail.CurrentFulfilledQuantity += selectedEntity.UsableQuantity;
                            }
                        } else {
                            //if(PartsSalesOrderProcessDetails.Any(r => r.SparePartId == selectedEntity.SparePartId))
                            //    continue;
                            if(selectedEntity.UsableQuantity > 0) {
                                var partsSalesOrderProcessDetail = new PartsSalesOrderProcessDetail {
                                    SparePartId = detail.SparePartId,
                                    SparePartCode = detail.SparePartCode,
                                    SparePartName = detail.SparePartName,
                                    NewPartId = selectedEntity.NewPartId,
                                    NewPartCode = selectedEntity.NewPartCode,
                                    NewPartName = selectedEntity.NewPartName,
                                    OrderedQuantity = detail.OrderedQuantity,
                                    //CurrentFulfilledQuantity = detail.CurrentFulfilledQuantity,
                                    CurrentFulfilledQuantity = selectedEntity.UsableQuantity,
                                    WarehouseId = detail.SupplierWarehouseId,
                                    WarehouseCode = detail.SupplierWarehouseCode,
                                    WarehouseName = detail.SupplierWarehouseName,
                                    OrderPrice = selectedEntity.PartsSalesPrice,
                                    IfDirectSupply = false,
                                    OrderProcessMethod = this.SetOrderProcessMethod()
                                };
                                if(tabName.Equals(PartsSalesUIStrings.DataEdit_Header_PartsSalesOrder_OtherTab) || tabName.Equals(PartsSalesUIStrings.DataEdit_Header_PartsSalesOrder_Backlog)) {
                                    partsSalesOrderProcessDetail.SupplierCompanyId = partsSalesOrder.SupplierCompanyId;
                                    partsSalesOrderProcessDetail.SupplierCompanyName = partsSalesOrder.SupplierCompanyName;
                                    partsSalesOrderProcessDetail.SupplierCompanyCode = partsSalesOrder.SupplierCompanyCode;
                                }
                                if(tabName.Equals(PartsSalesUIStrings.DataEdit_Header_PartsSalesOrder_Center)) {
                                    partsSalesOrderProcessDetail.SupplierCompanyId = partsSalesOrder.CenterCompanyId;
                                    partsSalesOrderProcessDetail.SupplierCompanyName = partsSalesOrder.CenterCompanyName;
                                    partsSalesOrderProcessDetail.SupplierCompanyCode = partsSalesOrder.CenterCompanyCode;
                                }
                                //更新互换件的可用库存为最初的可用库存减去对应的处理清单的本次满足数量
                                selectedEntity.Quantity = selectedEntity.Quantity - partsSalesOrderProcessDetail.CurrentFulfilledQuantity <= 0 ? 0 : selectedEntity.Quantity - partsSalesOrderProcessDetail.CurrentFulfilledQuantity;
                                this.PartsSalesOrderProcessDetails.Add(partsSalesOrderProcessDetail);
                            }
                        }
                        detail.UnfulfilledQuantity = detail.UnfulfilledQuantity - selectedEntity.UsableQuantity;
                        selectedEntity.UsableQuantity = 0;
                    }
                    if(partsSalesOrder.PartsSalesOrderDetails.Any(r => r.UnfulfilledQuantity == 0)) {
                        foreach(var detail in this.TempPartsSalesOrderDetails.Where(r => r.UnfulfilledQuantity == 0).ToArray()) {
                            TempPartsSalesOrderDetails.Remove(detail);
                            this.lpartsSalesOrderDetails.Add(detail);
                        }
                    }
                    break;
            }
        }

        private ObservableCollection<PartsSalesOrderDetail> tempPartsSalesOrderDetail;

        public ObservableCollection<PartsSalesOrderDetail> TempPartsSalesOrderDetails {
            get {
                return this.tempPartsSalesOrderDetail ?? (this.tempPartsSalesOrderDetail = new ObservableCollection<PartsSalesOrderDetail>());
            }
        }

        private string GetTabItemName() {
            var tabItem = this.tabControlChild.SelectedItem as RadTabItem;
            if(tabItem == null)
                return string.Empty;
            return tabItem.Header.ToString();
        }

        protected override string Title {
            get {
                return PartsSalesUIStrings.DataEditView_Title_Approve_PartsSalesOrderExport;
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {
            if(!this.PartsSalesOrderDetailTemplateApproveDataGridView.CommitEdit() || !this.PartsSalesOrderProcessDetailForApproveDataGridView.CommitEdit() || !this.PartsExchangeApproveDataGridView.CommitEdit())
                return;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            if(!this.PartsSalesOrderProcessDetails.Any()) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Header_PartsSalesOrder_ProcessDetails);
                return;
            }
            if(partsSalesOrder.WarehouseId == default(int) || partsSalesOrder.WarehouseId == null) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_WarehouseIdIdIsNull);
                return;
            }
            if(this.DomainContext.IsBusy)
                return;
            ((IEditableObject)partsSalesOrder).EndEdit();
            //校验订单处理清单本次满足数量必须为配件信息.最小包装数量整数倍
            var checkpackageids = PartsSalesOrderProcessDetails.Select(v => v.SparePartId).ToArray();
            this.DomainContext.Load(this.DomainContext.GetSparePartByIdsQuery(checkpackageids), LoadBehavior.RefreshCurrent, loadSparePart => {
                if(loadSparePart.HasError) {
                    if(!loadSparePart.IsErrorHandled) {
                        loadSparePart.MarkErrorAsHandled();
                        return;
                    }
                }
                if(this.DomainContext.PartsSalesOrderProcesses.Any(r => r.EntityState == EntityState.New)) {
                    var partsSalesOrderProcesses = this.DomainContext.PartsSalesOrderProcesses.Where(r => r.EntityState == EntityState.New).ToArray();
                    foreach(var salesOrderProces in partsSalesOrderProcesses) {
                        this.DomainContext.PartsSalesOrderProcesses.Remove(salesOrderProces);
                    }
                }
                var approvalComment = partsSalesOrder.ApprovalComment;
                var partsSalesOrderProcess = ProcessSalesOrderInternal(partsSalesOrder);
                ((IEditableObject)partsSalesOrderProcess).EndEdit();
                partsSalesOrderProcess.ApprovalComment = approvalComment;
                this.DomainContext.PartsSalesOrderProcesses.Add(partsSalesOrderProcess);
                //校验销售订单处理单的本次满足金额是否大于分公司策略的最大开票金额
                this.DomainContext.Load(this.DomainContext.GetBranchstrategiesQuery().Where(r => r.BranchId == partsSalesOrder.BranchId), LoadBehavior.RefreshCurrent, loadBranchStrategy => {
                    if(loadBranchStrategy.HasError) {
                        if(!loadBranchStrategy.IsErrorHandled) {
                            loadBranchStrategy.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadBranchStrategy);
                            return;
                        }
                    }
                    var branchstrategy = loadBranchStrategy.Entities.SingleOrDefault();
                    if(branchstrategy != null && partsSalesOrderProcess.CurrentFulfilledAmount > branchstrategy.MaxInvoiceAmount) {
                        UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_CurrentFulfilledAmount);
                        return;
                    }
                    //销售订单处理单清单保存的时候增加校验 出库仓库id 与 出库仓库编号、名称必须匹配，为同一个仓库的编号名称，否则不允许保存
                    this.DomainContext.Load(this.DomainContext.GetWarehousesQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError)
                            if(loadOp.IsErrorHandled) {
                                loadOp.MarkErrorAsHandled();
                                return;
                            }
                        var warehouses = loadOp.Entities;
                        if(warehouses != null && warehouses.Any()) {
                            //直供是不需要仓库信息的，因为是直接从供应商那边发货到客户那边，不需要走总部仓库的
                            //所有不需要校验这类
                            foreach(var item in PartsSalesOrderProcessDetails.Where(r => r.OrderProcessMethod != (int)DcsPartsSalesOrderProcessDetailProcessMethod.供应商直发 && r.OrderProcessMethod != (int)DcsPartsSalesOrderProcessDetailProcessMethod.积压件平台)) {
                                if(!warehouses.Any(r => r.Id == item.WarehouseId && r.Code == item.WarehouseCode && r.Name == item.WarehouseName)) {
                                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_Warehouse);
                                    return;
                                }
                            }
                        } else
                            return;
                        try {
                            if(partsSalesOrderProcess.Can审批配件销售订单正常订单2)
                                partsSalesOrderProcess.审批配件销售订单正常订单2(partsSalesOrder.PartsSalesOrderTypeId, partsSalesOrder.Remark, partsSalesOrder.SalesCategoryId, false);
                        } catch(ValidationException ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                            return;
                        }
                        base.OnEditSubmitting();
                    }, null);

                }, null);
            }, null);
        }

        protected override void OnEditCancelled() {
            this.Reset();
            base.OnEditCancelled();
        }

        protected override void OnEditSubmitted() {
            this.Reset();
            base.OnEditSubmitted();
        }

        protected override void Reset() {
            this.ButtonCanUse = false;
            this.DataContext = null;
            ((PartsSalesOrderForDistributionForTemplateApproveDataEditPanel)this.PartsSalesOrderForDistributionForTemplateApproveDataEditPanel).comboBoxWarehouseForCadre.ItemsSource = null;
            this.WarehousePartsStocks.Clear();
            foreach(var item in this.tabControlChild.Items) {
                var nItem = item as RadTabItem;
                if(nItem != null)
                    nItem.Visibility = Visibility.Visible;
            }
            this.tabControlChild.SelectedItem = this.tabControlChild.Items.First(r => ((RadTabItem)r).Header.Equals(PartsSalesUIStrings.DataEdit_Header_PartsSalesOrder_Local));
            this.partsExchangeDataEditView.SetValue(VisibilityProperty, Visibility.Visible);
            lastIndex = 0;
        }

        private PartsSalesOrderProcess ProcessSalesOrderInternal(PartsSalesOrder partsSalesOrder) {
            var partsSalesOrderProcess = new PartsSalesOrderProcess();
            partsSalesOrderProcess.Code = GlobalVar.ASSIGNED_BY_SERVER;
            //partsSalesOrderProcess.PartsSalesOrder = partsSalesOrder;
            partsSalesOrderProcess.OriginalSalesOrderId = partsSalesOrder.Id;
            partsSalesOrderProcess.CurrentFulfilledAmount = this.PartsSalesOrderProcessDetails.Sum(r => r.OrderPrice * r.CurrentFulfilledQuantity);
            partsSalesOrderProcess.ShippingMethod = partsSalesOrder.ShippingMethod;
            partsSalesOrderProcess.RequestedShippingTime = partsSalesOrder.RequestedShippingTime;
            partsSalesOrderProcess.RequestedDeliveryTime = partsSalesOrder.RequestedDeliveryTime;
            partsSalesOrderProcess.BillStatusBeforeProcess = partsSalesOrder.Status;
            foreach(var detail in this.PartsSalesOrderProcessDetails) {
                partsSalesOrderProcess.PartsSalesOrderProcessDetails.Add(detail);
            }
            return partsSalesOrderProcess;
        }

        private IDictionary<int, ObservableCollection<PartsSalesOrderDetail>> IPartsSalesOrderDetails;

        private void LoadEntityToEdit(int id) {
            var partsSalesOrders = this.DomainContext.PartsSalesOrders.ToList();
            foreach(var partsSalesOrder in partsSalesOrders)
                this.DomainContext.PartsSalesOrders.Detach(partsSalesOrder);
            this.PartsSalesOrderProcessDetails.Clear();
            //隐藏其它品牌处理方式
            foreach(var item in this.tabControlChild.Items) {
                var nItem = item as RadTabItem;
                if(nItem != null)
                    if(nItem.Header.ToString().Equals(PartsSalesUIStrings.DataEdit_Header_PartsSalesOrder_OtherTab))
                        nItem.Visibility = Visibility.Collapsed;
            }
            partsSalesOrderDataEditView.SetValue(VisibilityProperty, Visibility.Visible);
            partsSalesOrderInsideDataEditView.SetValue(VisibilityProperty, Visibility.Collapsed);

            this.DomainContext.Load(this.DomainContext.GetPartsSalesOrderWithPartsSalesOrderDetailsAndCustomerAccountQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                //获取出库仓库初始化条件
                this.FrameworkElementForBenBuTab(entity);
                this.TempPartsSalesOrderDetails.Clear();
                foreach(var detail in entity.PartsSalesOrderDetails.OrderByDescending(r => r.UnfulfilledQuantity)) {
                    this.TempPartsSalesOrderDetails.Add(detail);
                }
                if(entity.SalesCategoryName != "欧曼电商") {
                    entity.OutWarehouseId = entity.WarehouseId;
                    if(entity.OutWarehouseId.HasValue) {
                        if(entity.OutWarehouseId.Value != default(int)) {
                            this.DomainContext.Load(this.DomainContext.GetWarehousesOrderByNameQuery().Where(r => r.Id == entity.OutWarehouseId), LoadBehavior.RefreshCurrent, loadOption => {
                                if(loadOption.HasError) {
                                    loadOption.MarkErrorAsHandled();
                                    return;
                                }
                                if(loadOption.Entities != null && loadOption.Entities.Any()) {
                                    entity.WarehouseCode = loadOption.Entities.First().Code;
                                    entity.WarehouseName = loadOption.Entities.First().Name;
                                }
                                this.UpdatePartsSalesOrderDetails("OutWarehouseId", entity);
                            }, null);
                        }
                    } else {
                        this.UpdatePartsSalesOrderDetails("SupplierWarehouseId", entity);
                    }
                }
                //设置内部转直供默认值
                entity.InsideSupplyingId = (int)DcsInternalSupplier.FT001078;
                this.SetObjectToEdit(entity);
                this.IPartsSalesOrderDetails = new Dictionary<int, ObservableCollection<PartsSalesOrderDetail>>();

                int ID = int.MaxValue;
                var PartsSalesOrderDetails0 = new ObservableCollection<PartsSalesOrderDetail>();
                var PartsSalesOrderDetails1 = new ObservableCollection<PartsSalesOrderDetail>();
                var PartsSalesOrderDetails2 = new ObservableCollection<PartsSalesOrderDetail>();
                var PartsSalesOrderDetails3 = new ObservableCollection<PartsSalesOrderDetail>();
                foreach(var detail in entity.PartsSalesOrderDetails) {
                    PartsSalesOrderDetails0.Add(new PartsSalesOrderDetail {
                        Id = ID--,
                        SparePartId = detail.SparePartId,
                        SparePartCode = detail.SparePartCode,
                        SparePartName = detail.SparePartName,
                        UnfulfilledQuantity = detail.UnfulfilledQuantity,
                        OrderedQuantity = detail.OrderedQuantity,
                        CurrentFulfilledQuantity = 0,
                        StockQuantity = 0,
                        SupplierWarehouseId = 0,
                        SupplierWarehouseCode = string.Empty,
                        SupplierWarehouseName = string.Empty
                    });
                    PartsSalesOrderDetails1.Add(new PartsSalesOrderDetail {
                        Id = ID--,
                        SparePartId = detail.SparePartId,
                        SparePartCode = detail.SparePartCode,
                        SparePartName = detail.SparePartName,
                        UnfulfilledQuantity = detail.UnfulfilledQuantity,
                        OrderedQuantity = detail.OrderedQuantity,
                        CurrentFulfilledQuantity = 0,
                        StockQuantity = 0,
                        SupplierWarehouseId = 0,
                        SupplierWarehouseCode = string.Empty,
                        SupplierWarehouseName = string.Empty
                    });
                    PartsSalesOrderDetails2.Add(new PartsSalesOrderDetail {
                        Id = ID--,
                        SparePartId = detail.SparePartId,
                        SparePartCode = detail.SparePartCode,
                        SparePartName = detail.SparePartName,
                        UnfulfilledQuantity = detail.UnfulfilledQuantity,
                        OrderedQuantity = detail.OrderedQuantity,
                        CurrentFulfilledQuantity = 0,
                        StockQuantity = 0,
                        SupplierWarehouseId = 0,
                        SupplierWarehouseCode = string.Empty,
                        SupplierWarehouseName = string.Empty
                    });
                    PartsSalesOrderDetails3.Add(new PartsSalesOrderDetail {
                        Id = ID--,
                        SparePartId = detail.SparePartId,
                        SparePartCode = detail.SparePartCode,
                        SparePartName = detail.SparePartName,
                        UnfulfilledQuantity = detail.UnfulfilledQuantity,
                        OrderedQuantity = detail.OrderedQuantity,
                        CurrentFulfilledQuantity = 0,
                        StockQuantity = 0,
                        SupplierWarehouseId = 0,
                        SupplierWarehouseCode = string.Empty,
                        SupplierWarehouseName = string.Empty
                    });
                }
                this.IPartsSalesOrderDetails.Add(0, PartsSalesOrderDetails0);
                this.IPartsSalesOrderDetails.Add(1, PartsSalesOrderDetails1);
                this.IPartsSalesOrderDetails.Add(2, PartsSalesOrderDetails2);
                this.IPartsSalesOrderDetails.Add(3, PartsSalesOrderDetails3);
            }, null);
        }

        private void FrameworkElementForBenBuTab(PartsSalesOrder partsSalesOrder) {
            foreach(var templateApproveDataEditPanel in this.tabControlChild.Items.Where(v => v is RadTabItem).Cast<RadTabItem>()) {
                if(templateApproveDataEditPanel.Header.Equals(PartsSalesUIStrings.DataEdit_Header_PartsSalesOrder_Local)) {
                    var templateApproveDataEditPanel1 = templateApproveDataEditPanel.Content as PartsSalesOrderForTemplateApproveDataEditPanel;
                    if(templateApproveDataEditPanel1 == null)
                        return;
                    var comboBoxWarehouse = templateApproveDataEditPanel1.LayoutRoot.Children.Where(control => control is Sunlight.Silverlight.Dcs.Controls.DcsComboBox).Cast<DcsComboBox>().FirstOrDefault(v => v.Name == "ComboBoxWarehouse");
                    if(comboBoxWarehouse == null)
                        return;
                    comboBoxWarehouse.SelectionChanged += (sender, e) => {
                        if(!e.AddedItems.Cast<Warehouse>().Any())
                            return;
                        var currentSelectItem = e.AddedItems.Cast<Warehouse>().First();
                        if(currentSelectItem == null)
                            return;
                        partsSalesOrder.WarehouseCode = currentSelectItem.Code;
                        partsSalesOrder.WarehouseName = currentSelectItem.Name;
                    };
                    this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(r => r.Id == BaseApp.Current.CurrentUserData.EnterpriseId && r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOption => {
                        if(loadOption.HasError) {
                            loadOption.MarkErrorAsHandled();
                            return;
                        }
                        if(loadOption.Entities.First().Type == (int)DcsCompanyType.代理库 || loadOption.Entities.First().Type == (int)DcsCompanyType.服务站兼代理库)
                            foreach(var item in this.tabControlChild.Items) {
                                var nItem = item as RadTabItem;
                                if(nItem != null)
                                    if(nItem.Header.ToString().Equals(PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_DirectSupply) || nItem.Header.ToString().Equals(PartsSalesUIStrings.DataEdit_Header_PartsSalesOrder_Center))
                                        nItem.Visibility = Visibility.Collapsed;
                            }
                        if(loadOption.Entities.First().Type == (int)DcsCompanyType.分公司)
                            foreach(var item in this.tabControlChild.Items) {
                                var nItem = item as RadTabItem;
                                if(nItem != null)
                                    if(nItem.Header.ToString().Equals(PartsSalesUIStrings.DataEdit_Header_PartsSalesOrder_Headquarters))
                                        nItem.Visibility = Visibility.Collapsed;
                            }
                        if(loadOption.Entities.First().Type == (int)DcsCompanyType.代理库 || loadOption.Entities.First().Type == (int)DcsCompanyType.服务站兼代理库) {
                            this.DomainContext.Load(this.DomainContext.GetWarehousesOrderByNameQuery().Where(r => r.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && r.Status == (int)DcsBaseDataStatus.有效 && r.IsQualityWarehouse == false), LoadBehavior.RefreshCurrent, loadOp => {
                                if(loadOp.HasError)
                                    return;
                                comboBoxWarehouse.ItemsSource = loadOp.Entities;
                            }, null);
                        } else {
                            this.DomainContext.Load(this.DomainContext.根据销售组织查询仓库Query(partsSalesOrder.SalesUnitId).Where(r => r.IsQualityWarehouse == false), LoadBehavior.RefreshCurrent, loadOp => {
                                if(loadOp.HasError)
                                    return;
                                comboBoxWarehouse.ItemsSource = loadOp.Entities;
                            }, null);
                        }
                    }, null);
                }
            }
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public PartsSalesOrderForExportApproveDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += PartsSalesOrderForExportApproveDataEditView_DataContextChanged;
        }

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #region 配件分品牌查看 需求单位库存查询 采购订单查看
        private void DetailPartsBranchInfoQuery() {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null || (!TempPartsSalesOrderDetails.Any(r => r.CurrentFulfilledQuantity < r.UnfulfilledQuantity)))
                return;
            this.SparePartWithPartBranchInfoes.Clear();
            var partIds = this.TempPartsSalesOrderDetails.Where(r => r.CurrentFulfilledQuantity < r.UnfulfilledQuantity).Select(r => r.SparePartId).ToArray();
            this.DomainContext.Load(this.DomainContext.GetSparePartWithPartsBranchQuery(partIds, partsSalesOrder.SalesCategoryId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                if(loadOp.Entities.Any()) {
                    foreach(var item in loadOp.Entities) {
                        SparePartWithPartBranchInfoes.Add(item);
                    }
                }
                this.DetailPartsBranchInfoQueryWindow.ShowDialog();
            }, null);
        }

        private RadWindow detailPartsBranchInfoQueryWindow;
        private RadWindow DetailPartsBranchInfoQueryWindow {
            get {
                return this.detailPartsBranchInfoQueryWindow ?? (this.detailPartsBranchInfoQueryWindow = new RadWindow {
                    Content = this.DetailPartsBranchInfoDataGridView,
                    WindowState = WindowState.Normal,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                    AllowDrop = false,
                    ResizeMode = ResizeMode.NoResize,
                    CanClose = true,
                    Header = PartsSalesUIStrings.DataEdit_Header_PartsSalesOrder_PartsBranchInfo
                });
            }
        }

        private DataGridViewBase detailPartsBranchInfoDataGridView;
        private DataGridViewBase DetailPartsBranchInfoDataGridView {
            get {
                if(this.detailPartsBranchInfoDataGridView == null) {
                    this.detailPartsBranchInfoDataGridView = DI.GetDataGridView("VirtualSparePartDetailForSalesOrderApprove");
                    this.detailPartsBranchInfoDataGridView.DomainContext = this.DomainContext;
                    this.detailPartsBranchInfoDataGridView.DataContext = this;
                }
                return this.detailPartsBranchInfoDataGridView;
            }
        }

        private ObservableCollection<VirtualSparePart> sparePartWithPartBranchInfoes = new ObservableCollection<VirtualSparePart>();
        public ObservableCollection<VirtualSparePart> SparePartWithPartBranchInfoes {
            get {
                return sparePartWithPartBranchInfoes ?? (this.sparePartWithPartBranchInfoes = new ObservableCollection<VirtualSparePart>());
            }
        }

        private void SubmitCompanyPartsStockQuery() {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null || (!TempPartsSalesOrderDetails.Any()))
                return;
            this.CompanyPartsStocks.Clear();
            var partIds = this.TempPartsSalesOrderDetails.Select(r => r.SparePartId).ToArray();
            this.DomainContext.Load(this.DomainContext.查询服务站代理库库存Query(partsSalesOrder.SubmitCompanyId, partIds, partsSalesOrder.BranchId, partsSalesOrder.SalesCategoryId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                if(loadOp.Entities.Any()) {
                    foreach(var item in loadOp.Entities) {
                        CompanyPartsStocks.Add(item);
                    }
                }
                var idsCount = loadOp.Entities.Distinct().Count();
                if(idsCount != TempPartsSalesOrderDetails.Count()) {
                    var loadOpPartIds = loadOp.Entities.Distinct().Select(r => r.SparePartId);
                    foreach(var detail in TempPartsSalesOrderDetails.Where(r => !loadOpPartIds.Contains(r.SparePartId))) {
                        var companyPartsStock = new CompanyPartsStock {
                            SparePartCode = detail.SparePartCode,
                            SparePartName = detail.SparePartName,
                            CompanyCode = partsSalesOrder.SubmitCompanyCode,
                            CompanyName = partsSalesOrder.SubmitCompanyName,
                            Quantity = 0
                        };
                        CompanyPartsStocks.Add(companyPartsStock);
                    }
                }
                this.SubmitCompanyPartsStockQueryQueryWindow.ShowDialog();
            }, null);
        }

        private RadWindow submitCompanyPartsStockQueryQueryWindow;
        private RadWindow SubmitCompanyPartsStockQueryQueryWindow {
            get {
                return this.submitCompanyPartsStockQueryQueryWindow ?? (this.submitCompanyPartsStockQueryQueryWindow = new RadWindow {
                    Content = this.SubmitCompanyPartsStockDataGridView,
                    WindowState = WindowState.Normal,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                    AllowDrop = false,
                    ResizeMode = ResizeMode.NoResize,
                    CanClose = true,
                    Header = PartsSalesUIStrings.DataEdit_Header_PartsSalesOrder_CompanyParts
                });
            }
        }

        private DataGridViewBase submitCompanyPartsStockDataGridView;
        private DataGridViewBase SubmitCompanyPartsStockDataGridView {
            get {
                if(this.submitCompanyPartsStockDataGridView == null) {
                    this.submitCompanyPartsStockDataGridView = DI.GetDataGridView("SubmitCompanyPartsStockForSalesOrderApprove");
                    this.submitCompanyPartsStockDataGridView.DomainContext = this.DomainContext;
                    this.submitCompanyPartsStockDataGridView.DataContext = this;
                }
                return this.submitCompanyPartsStockDataGridView;
            }
        }

        private ObservableCollection<CompanyPartsStock> companyPartsStocks = new ObservableCollection<CompanyPartsStock>();
        public ObservableCollection<CompanyPartsStock> CompanyPartsStocks {
            get {
                return companyPartsStocks ?? (this.companyPartsStocks = new ObservableCollection<CompanyPartsStock>());
            }
        }


        private PartsPurchaseOrderQueryDataEditView queryDataEditView = new PartsPurchaseOrderQueryDataEditView();
        public PartsPurchaseOrderQueryDataEditView QueryDataEditView {
            get {
                return queryDataEditView ?? (this.queryDataEditView = new PartsPurchaseOrderQueryDataEditView());
            }
        }

        private void PartsPurchaseOrderQuery() {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null || (!TempPartsSalesOrderDetails.Any(r => r.CurrentFulfilledQuantity < r.UnfulfilledQuantity)))
                return;
            this.SparePartWithPartBranchInfoes.Clear();
            var partIds = this.TempPartsSalesOrderDetails.Where(r => r.CurrentFulfilledQuantity < r.UnfulfilledQuantity).Select(r => r.SparePartId).ToArray();
            this.DomainContext.Load(this.DomainContext.查询采购订单未入库量Query(partsSalesOrder.SalesCategoryId, partIds), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                QueryDataEditView.SalesCategoryId = partsSalesOrder.SalesCategoryId;
                QueryDataEditView.PartIds = partIds;
                QueryDataEditView.PartsPurchaseOrderDetails.Clear();
                if(loadOp.Entities.Any()) {
                    foreach(var item in loadOp.Entities) {
                        QueryDataEditView.PartsPurchaseOrderDetails.Add(item);
                    }
                }
                this.PartsPurchaseOrderQueryQueryWindow.ShowDialog();
            }, null);
        }

        private RadWindow partsPurchaseOrderQueryQueryWindow;
        private RadWindow PartsPurchaseOrderQueryQueryWindow {
            get {
                return this.partsPurchaseOrderQueryQueryWindow ?? (this.partsPurchaseOrderQueryQueryWindow = new RadWindow {
                    Content = this.QueryDataEditView,
                    WindowState = WindowState.Normal,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                    AllowDrop = false,
                    ResizeMode = ResizeMode.NoResize,
                    CanClose = true,
                    Header = PartsSalesUIStrings.DataEdit_Header_PartsSalesOrder_PurchaseOrder
                });
            }
        }

        # endregion
    }
}
