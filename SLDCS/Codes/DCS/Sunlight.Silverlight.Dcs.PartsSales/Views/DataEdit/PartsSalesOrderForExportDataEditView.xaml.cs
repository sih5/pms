﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesOrderForExportDataEditView {
        private ObservableCollection<CompanyAddress> kvCompanyAddresses;
        private KeyValueManager keyValueManager;
        private DataGridViewBase partsSalesOrderDetailForExportInfoDataGridView;
        private ObservableCollection<KeyValuePair> kvSalesUnits;
        private ObservableCollection<KeyValuePair> kvPartsSalesOrderTypes;
        //private bool isIfDirectProvision = true;
        //private Company companys;
        private readonly ObservableCollection<KeyValuePair> kvReceiverSalesCateNames = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvReceivingWarehouseNames = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategories = new ObservableCollection<KeyValuePair>();

        private ObservableCollection<SalesUnitAffiWarehouse> kvSalesUnitAffiWarehouses;

        public ObservableCollection<SalesUnitAffiWarehouse> KvSalesUnitAffiWarehouses {
            get {
                return this.kvSalesUnitAffiWarehouses ?? (this.kvSalesUnitAffiWarehouses = new ObservableCollection<SalesUnitAffiWarehouse>());
            }
        }
        /// <summary>
        /// 当前登录人企业类型 用于过滤品牌
        /// </summary>
        private int currentCompanyType;


        public ObservableCollection<KeyValuePair> KvPartsSalesCategories {
            get {
                return this.kvPartsSalesCategories;
            }
        }

        //本地缓存，记录修改前的配件销售订单类型Id
        private int oldPartsSalesOrderTypeId;

        private string strFileName;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private RadUpload uploader;

        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        var partsSalesOrder = this.DataContext as PartsSalesOrder;
                        if(partsSalesOrder == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.ImportPartsSalesOrderDetailAsync(e.HandlerData.CustomData["Path"].ToString(), partsSalesOrder.IfDirectProvision, partsSalesOrder.SubmitCompanyId, partsSalesOrder.SalesCategoryId);
                        this.excelServiceClient.ImportPartsSalesOrderDetailCompleted -= ExcelServiceClient_ImportPartsSalesOrderDetailCompleted;
                        this.excelServiceClient.ImportPartsSalesOrderDetailCompleted += ExcelServiceClient_ImportPartsSalesOrderDetailCompleted;
                    };
                }
                return this.uploader;
            }
        }

        /// <summary>
        /// 文件上传成功后，调用该方法进行上传后的处理。不支持异步调用
        /// <param>上传文件名 <name>fileName</name> </param>
        /// </summary>
        protected Action<string> UploadFileSuccessedProcessing;

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //取当前登录用户的HashCode来标记上传文件的唯一性
                //this.UploadFileSuccessedProcessing.Invoke(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }

        private void ExcelServiceClient_ImportPartsSalesOrderDetailCompleted(object sender, ImportPartsSalesOrderDetailCompletedEventArgs e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            var detailList = partsSalesOrder.PartsSalesOrderDetails.ToList();
            foreach(var detail in detailList) {
                partsSalesOrder.PartsSalesOrderDetails.Remove(detail);
            }
            if(e.rightData != null && e.rightData.Length > 0) {
                foreach(var data in e.rightData) {
                    var partsSalesOrderDetail = new PartsSalesOrderDetail {
                        SparePartId = data.SparePartId,
                        SparePartCode = data.SparePartCode,
                        SparePartName = data.SparePartName,
                        EstimatedFulfillTime = data.EstimatedFulfillTime,
                        OrderedQuantity = data.OrderedQuantity,
                        IfCanNewPart = true,
                        Remark = data.Remark,
                        OrderNo=data.OrderNo
                    };
                    partsSalesOrder.PartsSalesOrderDetails.Add(partsSalesOrderDetail);
                }
                var sparePartIds = partsSalesOrder.PartsSalesOrderDetails.Select(r => r.SparePartId).ToArray();
                this.DomainContext.Load(this.DomainContext.根据销售类型查询配件销售价出口使用Query(partsSalesOrder.SalesCategoryId, partsSalesOrder.PartsSalesOrderTypeId, partsSalesOrder.ReceivingCompanyId, partsSalesOrder.IfDirectProvision, partsSalesOrder.PriceType ?? 0, sparePartIds), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        return;
                    }
                    if(loadOp.Entities != null && loadOp.Entities.Any()) {
                        var entity = loadOp.Entities.FirstOrDefault();
                        if(entity != null) {
                            foreach(var partsSalesOrderDetails in partsSalesOrder.PartsSalesOrderDetails) {
                                var partsOrderPrice = loadOp.Entities.FirstOrDefault(r => r.SparePartId == partsSalesOrderDetails.SparePartId);
                                if(partsOrderPrice != null) {
                                    partsSalesOrderDetails.OrderPrice = partsOrderPrice.Price;
                                    partsSalesOrderDetails.MeasureUnit = partsOrderPrice.MeasureUnit;
                                    partsSalesOrderDetails.CustOrderPriceGradeCoefficient = partsOrderPrice.Coefficient;
                                    partsSalesOrderDetails.OriginalPrice = partsOrderPrice.RetailGuidePrice;
                                    partsSalesOrderDetails.DiscountedPrice = partsSalesOrderDetails.OriginalPrice - partsSalesOrderDetails.OrderPrice;
                                    partsSalesOrderDetails.OrderSum = partsSalesOrderDetails.OrderedQuantity * partsSalesOrderDetails.OrderPrice;
                                    partsSalesOrderDetails.PriceTypeName = partsOrderPrice.PriceTypeName;
                                    partsSalesOrderDetails.ABCStrategy = partsOrderPrice.ABCStrategy;
                                    partsSalesOrderDetails.MInSaleingAmount = partsOrderPrice.MInPackingAmount;
                                    partsSalesOrderDetails.SalesPrice = partsOrderPrice.SalesPrice;
                                }
                            }
                        }
                        partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
                    }
                }, null);

            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowNotification(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));

        }

        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage( PartsSalesUIStrings.DataEdit_Validation_Import_CloseFile);
                    e.Handled = true;
                }
            }
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }

        private readonly string[] kvNames = {
            "PartsSalesOrder_SecondLevelOrderType", "PartsShipping_Method" ,"ExportCustPriceType","PartsSalesOrder_PackingMethod"
        };

        public PartsSalesOrderForExportDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
            this.Loaded += PartsSalesOrderForExportDataEditView_Loaded;
            this.DataContextChanged += PartsSalesOrderDataEditView_DataContextChanged;
        }

        private void PartsSalesOrderForExportDataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(r => r.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                if(loadOp.Entities != null && loadOp.Entities.Any()) {
                    currentCompanyType = loadOp.Entities.First().Type;
                }
            }, null);
            this.DomainContext.Load(this.DomainContext.GetPartsSalesOrderTypeWithSettleTypesQuery((int)DcsSalesOrderType_SettleType.出口结算), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvPartsSalesOrderTypes.Clear();
                foreach(var partsSalesOrderType in loadOp.Entities) {
                    this.KvPartsSalesOrderTypes.Add(new KeyValuePair {
                        Key = partsSalesOrderType.Id,
                        Value = partsSalesOrderType.Name,
                    });
                }
            }, null);
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        public ObservableCollection<KeyValuePair> KvReceiverSalesCateNames {
            get {
                return kvReceiverSalesCateNames;
            }
        }

        public ObservableCollection<KeyValuePair> KvReceivingWarehouseNames {
            get {
                return kvReceivingWarehouseNames;
            }
        }

        private DataGridViewBase PartsSalesOrderDetailForExportInfoDataGridView {
            get {
                if(this.partsSalesOrderDetailForExportInfoDataGridView == null) {
                    this.partsSalesOrderDetailForExportInfoDataGridView = DI.GetDataGridView("PartsSalesOrderDetailForExportInfo");
                    this.partsSalesOrderDetailForExportInfoDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsSalesOrderDetailForExportInfoDataGridView;
            }
        }

        private void CreateUI() {
            this.KeyValueManager.LoadData();
            var detailEditView = new DcsDetailDataEditView();
            detailEditView.Register(PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_OrderDetailForExport, null, () => this.PartsSalesOrderDetailForExportInfoDataGridView);
            detailEditView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.DataEditPanel_Action_ImportNew,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                Command = new DelegateCommand(() => this.Uploader.ShowFileDialog())
            });
            detailEditView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.DataEditPanel_Action_Export,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                Command = this.ExportTemplateCommand
            });
            this.SubRoot.Children.Add(detailEditView);
            //提报单位名称
            var exportCustomerInfoQueryWindow = DI.GetQueryWindow("ExportCustomerInfo");
            exportCustomerInfoQueryWindow.SelectionDecided += this.exportCustomerInfoQueryWindow_SelectionDecided;
            this.popupTextBoxSubmitCompanyName.PopupContent = exportCustomerInfoQueryWindow;
        }

        private void exportCustomerInfoQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;

            if(queryWindow.SelectedEntities == null || !queryWindow.SelectedEntities.Any())
                return;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            var exportCustomerInfo = queryWindow.SelectedEntities.Cast<ExportCustomerInfo>().FirstOrDefault();
            if(exportCustomerInfo == null)
                return;
            if(partsSalesOrder.PartsSalesOrderDetails != null && partsSalesOrder.PartsSalesOrderDetails.Any()) {
                var detailList = partsSalesOrder.PartsSalesOrderDetails.ToList();
                foreach(var detail in detailList) {
                    partsSalesOrder.PartsSalesOrderDetails.Remove(detail);
                }
            }
            partsSalesOrder.ReceivingCompanyId = exportCustomerInfo.Id;
            partsSalesOrder.ReceivingCompanyCode = exportCustomerInfo.CustomerCode;
            partsSalesOrder.ReceivingCompanyName = exportCustomerInfo.CustomerName;
            partsSalesOrder.PriceType = exportCustomerInfo.PriceType;

            partsSalesOrder.SalesCategoryId = default(int);
            partsSalesOrder.SalesCategoryName = string.Empty;

            this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(r => r.Id == exportCustomerInfo.InvoiceCompanyId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                if(loadOp.Entities != null && loadOp.Entities.Any()) {
                    partsSalesOrder.CustomerType = loadOp.Entities.First().Type;
                    partsSalesOrder.CustomerAccountId = exportCustomerInfo.InvoiceCompanyId ?? 0;

                    //收票
                    partsSalesOrder.InvoiceReceiveCompanyId = loadOp.Entities.First().Id;
                    partsSalesOrder.InvoiceReceiveCompanyCode = loadOp.Entities.First().Code;
                    partsSalesOrder.InvoiceReceiveCompanyName = loadOp.Entities.First().Name;
                    partsSalesOrder.InvoiceReceiveCompanyType = loadOp.Entities.First().Type;

                    //提报
                    partsSalesOrder.SubmitCompanyName = loadOp.Entities.First().Name;
                    partsSalesOrder.SubmitCompanyCode = loadOp.Entities.First().Code;
                    partsSalesOrder.SubmitCompanyId = loadOp.Entities.First().Id;
                }
                if(currentCompanyType == (int)DcsCompanyType.分公司) {
                    if(partsSalesOrder.InvoiceReceiveCompanyType == (int)DcsCompanyType.代理库 || partsSalesOrder.InvoiceReceiveCompanyType == (int)DcsCompanyType.服务站兼代理库) {
                        this.kvPartsSalesCategories.Clear();
                        this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOption => {
                            if(loadOption.HasError)
                                return;
                            this.KvPartsSalesCategories.Clear();
                            foreach(var partsSalesCategory in loadOption.Entities) {
                                this.KvPartsSalesCategories.Add(new KeyValuePair {
                                    Key = partsSalesCategory.Id,
                                    Value = partsSalesCategory.Name
                                });
                            }
                            this.txtSalesCategory.SelectedIndex = 0;//默认品牌
                        }, null);
                    }
                    if(partsSalesOrder.InvoiceReceiveCompanyType == (int)DcsCompanyType.服务站 || partsSalesOrder.InvoiceReceiveCompanyType == (int)DcsCompanyType.集团企业) {
                        this.kvPartsSalesCategories.Clear();
                        this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesByDealerServiceInfoQuery(BaseApp.Current.CurrentUserData.EnterpriseId, partsSalesOrder.SubmitCompanyId), LoadBehavior.RefreshCurrent, loadOp1 => {
                            if(loadOp1.HasError)
                                return;
                            this.KvPartsSalesCategories.Clear();
                            foreach(var partsSalesCategory in loadOp1.Entities) {
                                this.KvPartsSalesCategories.Add(new KeyValuePair {
                                    Key = partsSalesCategory.Id,
                                    Value = partsSalesCategory.Name
                                });
                            }
                            this.txtSalesCategory.SelectedIndex = 0;//默认品牌
                        }, null);
                    }
                }
                if(partsSalesOrder.InvoiceReceiveCompanyType == (int)DcsCompanyType.分公司) {
                    this.kvPartsSalesCategories.Clear();
                    this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOption => {
                        if(loadOption.HasError)
                            return;
                        this.KvPartsSalesCategories.Clear();
                        foreach(var partsSalesCategory in loadOption.Entities) {
                            this.KvPartsSalesCategories.Add(new KeyValuePair {
                                Key = partsSalesCategory.Id,
                                Value = partsSalesCategory.Name
                            });
                        }
                        this.txtSalesCategory.SelectedIndex = 0;//默认品牌
                    }, null);
                }

                if(currentCompanyType == (int)DcsCompanyType.代理库 || currentCompanyType == (int)DcsCompanyType.服务站兼代理库) {
                    this.kvPartsSalesCategories.Clear();
                    this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesByAgencyDealerRelationQuery(BaseApp.Current.CurrentUserData.EnterpriseId, partsSalesOrder.SubmitCompanyId), LoadBehavior.RefreshCurrent, loadOp2 => {
                        if(loadOp2.HasError) {
                            loadOp2.MarkErrorAsHandled();
                            return;
                        }
                        this.KvPartsSalesCategories.Clear();
                        foreach(var partsSalesCategory in loadOp2.Entities) {
                            this.KvPartsSalesCategories.Add(new KeyValuePair {
                                Key = partsSalesCategory.Id,
                                Value = partsSalesCategory.Name
                            });
                        }
                        this.txtSalesCategory.SelectedIndex = 0;//默认品牌
                    }, null);
                }
                //获取企业类型
                this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(r => r.Id == partsSalesOrder.SubmitCompanyId), loadCom => {
                    if(loadCom.HasError)
                        return;
                    var company = loadCom.Entities.SingleOrDefault();
                    if(company == null)
                        return;
                    partsSalesOrder.InvoiceReceiveCompanyType = company.Type;
                    partsSalesOrder.CustomerType = company.Type;
                    partsSalesOrder.Province = company.ProvinceName;
                    partsSalesOrder.City = company.CityName;
                    this.DomainContext.Load(this.DomainContext.GetRegionsQuery().Where(r => r.Type == 2 && r.Name == partsSalesOrder.Province), loadOpRegion => {
                        if(loadOpRegion.HasError)
                            return;
                        var province = loadOpRegion.Entities.SingleOrDefault();
                        if(province != null)
                            partsSalesOrder.ProvinceID = province.Id;
                    }, null);
                }, null);
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }, null);
        }

        private ICommand exportFileCommand;

        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private const string EXPORT_DATA_FILE_NAME = "导出配件销售订单清单模板.xlsx";

        private void InitializeCommand() {
            this.exportFileCommand = new DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                    new ImportTemplateColumn {
                        Name=PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                         Name=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                    },
                    new ImportTemplateColumn {
                        Name=PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_OrderQuntity,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = Name=PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_MeetingTime,
                    },
                    new ImportTemplateColumn {
                         Name=PartsSalesUIStrings.DataEditView_Text_SecondClassStationPlan_Remark
                    },
                    new ImportTemplateColumn {
                        Name = Name=PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_OrderCode
                    }
                };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }

        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }

        /// <summary>
        /// 根据条件查询本企业和下属企业地址
        /// </summary>
        /// <param name="companyId">companyId</param>
        private void InitCompanyAddress(int companyId) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            //查询企业收货地址
            this.DomainContext.Load(this.DomainContext.GetCompanyAddressesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.CompanyId == companyId), LoadBehavior.RefreshCurrent, loadOption => {
                if(loadOption.HasError) {
                    loadOption.MarkErrorAsHandled();
                    return;
                }
                this.KvCompanyAddresses.Clear();
                foreach(var entity in loadOption.Entities) {
                    this.KvCompanyAddresses.Add(entity);
                }
                if(loadOption.Entities != null && loadOption.Entities.Count() == 1) {
                    if(string.IsNullOrEmpty(partsSalesOrder.ReceivingAddress)) {
                        partsSalesOrder.ReceivingAddress = loadOption.Entities.First().DetailAddress;
                        partsSalesOrder.ContactPerson = loadOption.Entities.First().ContactPerson;
                        partsSalesOrder.ContactPhone = loadOption.Entities.First().ContactPhone;
                        partsSalesOrder.CompanyAddressId = loadOption.Entities.First().Id;
                    }
                }
            }, null);
        }

        #region 界面事件
        private void PartsSalesOrderDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            partsSalesOrder.PropertyChanged -= this.PartsSalesOrder_PropertyChanged;
            partsSalesOrder.PropertyChanged += this.PartsSalesOrder_PropertyChanged;
            partsSalesOrder.PartsSalesOrderDetails.EntityRemoved -= this.PartsSalesOrderDetails_EntityRemoved;
            partsSalesOrder.PartsSalesOrderDetails.EntityRemoved += this.PartsSalesOrderDetails_EntityRemoved;
        }

        private void PartsSalesOrderDetails_EntityRemoved(object sender, EntityCollectionChangedEventArgs<PartsSalesOrderDetail> e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
        }

        private void PartsSalesOrder_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            switch(e.PropertyName) {
                case "PartsSalesOrderTypeId":
                    if(this.oldPartsSalesOrderTypeId != partsSalesOrder.PartsSalesOrderTypeId) {
                        if(partsSalesOrder.PartsSalesOrderTypeId == 0 || partsSalesOrder.PartsSalesCategory == null)
                            return;
                        if(!partsSalesOrder.PartsSalesOrderDetails.Any()) {
                            this.oldPartsSalesOrderTypeId = partsSalesOrder.PartsSalesOrderTypeId;
                            return;
                        }
                        DcsUtils.Confirm(PartsSalesUIStrings.DataEditView_Confirm_RebuildPartsSalesPrice, () => {
                            partsSalesOrder.PartsSalesOrderDetails.Select(r => r.SparePartId).ToArray();
                            //订单类型变更，重新计算订货价格
                            this.DomainContext.Load(this.DomainContext.根据销售类型查询配件销售价出口使用Query(partsSalesOrder.SalesCategoryId, partsSalesOrder.PartsSalesOrderTypeId, partsSalesOrder.ReceivingCompanyId, partsSalesOrder.IfDirectProvision, partsSalesOrder.PriceType ?? 0, null), LoadBehavior.RefreshCurrent, loadOp1 => {
                                if(loadOp1.HasError)
                                    return;
                                foreach(var detail in partsSalesOrder.PartsSalesOrderDetails) {
                                    var virtualPartsSalesPrice = loadOp1.Entities.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                                    if(virtualPartsSalesPrice != null) {
                                        detail.OrderPrice = virtualPartsSalesPrice.Price;
                                        detail.MeasureUnit = virtualPartsSalesPrice.MeasureUnit;
                                        detail.CustOrderPriceGradeCoefficient = virtualPartsSalesPrice.Coefficient;
                                        detail.OriginalPrice = virtualPartsSalesPrice.RetailGuidePrice;
                                        detail.DiscountedPrice = detail.OriginalPrice - detail.OrderPrice;
                                        detail.OrderSum = detail.OrderedQuantity * detail.OrderPrice;
                                        detail.PriceTypeName = virtualPartsSalesPrice.PriceTypeName;
                                        detail.ABCStrategy = virtualPartsSalesPrice.ABCStrategy;
                                        detail.SalesPrice = virtualPartsSalesPrice.SalesPrice;
                                    }
                                }
                                partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
                            }, null);
                            this.oldPartsSalesOrderTypeId = partsSalesOrder.PartsSalesOrderTypeId;
                        }, () => {
                            partsSalesOrder.PartsSalesOrderTypeId = this.oldPartsSalesOrderTypeId;
                        });
                    }
                    break;
                case "SalesCategoryId":
                    partsSalesOrder.SalesUnitId = 0;
                    partsSalesOrder.SalesUnitCode = string.Empty;
                    partsSalesOrder.SalesUnitName = string.Empty;
                    //获取收货仓库数据源
                    partsSalesOrder.ReceivingWarehouseId = null;
                    partsSalesOrder.ReceivingWarehouseName = null;

                    var details = partsSalesOrder.PartsSalesOrderDetails.ToList();
                    foreach(var detail in details)
                        partsSalesOrder.PartsSalesOrderDetails.Remove(detail);
                    this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Id == partsSalesOrder.SalesCategoryId), LoadBehavior.RefreshCurrent, loadOptionM => {
                        if(loadOptionM.HasError) {
                            loadOptionM.MarkErrorAsHandled();
                            return;
                        }
                        if(loadOptionM.Entities != null && loadOptionM.Entities.Any()) {
                            partsSalesOrder.BranchId = loadOptionM.Entities.First().BranchId;
                            partsSalesOrder.BranchCode = loadOptionM.Entities.First().BranchCode;
                            partsSalesOrder.BranchName = loadOptionM.Entities.First().BranchName;
                        }
                        this.LoadWarehouse(partsSalesOrder.SubmitCompanyId, partsSalesOrder.BranchId, partsSalesOrder.SalesCategoryId, partsSalesOrder.CustomerType);
                        //this.lnotApprovePrice = 0;
                        partsSalesOrder.CustomerAccount = null;
                        if(partsSalesOrder.InvoiceReceiveCompanyType == (int)DcsCompanyType.代理库 || partsSalesOrder.InvoiceReceiveCompanyType == (int)DcsCompanyType.服务站兼代理库) {
                            this.DomainContext.Load(this.DomainContext.GetWarehousesForSalesOrderReportQuery(partsSalesOrder.SalesCategoryId, partsSalesOrder.SubmitCompanyId), LoadBehavior.RefreshCurrent, loadOp3 => {
                                if(loadOp3.HasError) {
                                    if(!loadOp3.IsErrorHandled)
                                        loadOp3.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOp3);
                                    return;
                                }
                                if(loadOp3.Entities != null && loadOp3.Entities.Count() == 1) {
                                    partsSalesOrder.ReceivingWarehouseId = loadOp3.Entities.First().Id;
                                    partsSalesOrder.ReceivingWarehouseName = loadOp3.Entities.First().Name;
                                }
                                if(loadOp3.Entities != null && !loadOp3.Entities.Any()) {
                                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_ReceivingWarehouse);
                                    partsSalesOrder.ReceivingWarehouseId = default(int);
                                    partsSalesOrder.ReceivingWarehouseName = string.Empty;
                                }
                            }, null);
                        }
                    }, null);
                    break;
                case "WarehouseId":
                    var salesUnitKv = this.KvSalesUnitAffiWarehouses.FirstOrDefault(kv => kv.WarehouseId == partsSalesOrder.WarehouseId);
                    if(salesUnitKv == null)
                        return;
                    var salesUnitInsert = salesUnitKv;
                    if(salesUnitInsert.SalesUnit == null)
                        return;
                    partsSalesOrder.SalesUnitId = salesUnitInsert.SalesUnitId;
                    partsSalesOrder.SalesUnitCode = salesUnitInsert.SalesUnit.Code;
                    partsSalesOrder.SalesUnitName = salesUnitInsert.SalesUnit.Name;
                    partsSalesOrder.SalesUnitOwnerCompanyId = salesUnitInsert.SalesUnit.OwnerCompanyId;
                    var parameteras = new Dictionary<string, object>();
                    parameteras.Add("submitCompanyId", partsSalesOrder.SubmitCompanyId);
                    parameteras.Add("salesUnitId", partsSalesOrder.SalesUnitId);
                    parameteras.Add("partsSalesCategoryId", partsSalesOrder.SalesCategoryId);
                    this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(r => r.Id == salesUnitInsert.SalesUnit.OwnerCompanyId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            loadOp.MarkErrorAsHandled();
                            return;
                        }
                        if(loadOp.Entities != null && loadOp.Entities.Any()) {
                            partsSalesOrder.SalesUnitOwnerCompanyId = loadOp.Entities.First().Id;
                            partsSalesOrder.SalesUnitOwnerCompanyCode = loadOp.Entities.First().Code;
                            partsSalesOrder.SalesUnitOwnerCompanyName = loadOp.Entities.First().Name;
                            partsSalesOrder.SalesUnitOwnerCompanyType = loadOp.Entities.First().Type;
                        }
                    }, null);
                    this.DomainContext.InvokeOperation("查询未审核订单金额", typeof(decimal), parameteras, true, invokeOp => {
                        if(invokeOp.HasError)
                            return;
                        //this.notApprovePrice.Text = invokeOp.Value.ToString();
                        //this.lnotApprovePrice = decimal.Parse(invokeOp.Value.ToString());
                        this.DomainContext.Load(this.DomainContext.查询客户可用资金Query(null, salesUnitInsert.SalesUnit.AccountGroupId, partsSalesOrder.InvoiceReceiveCompanyId), LoadBehavior.RefreshCurrent, loadOption1 => {
                            if(loadOption1.HasError) {
                                loadOption1.MarkErrorAsHandled();
                                DcsUtils.ShowDomainServiceOperationWindow(loadOption1);
                                return;
                            }
                            partsSalesOrder.CustomerAccount = loadOption1.Entities.FirstOrDefault();
                            if(partsSalesOrder.CustomerAccount == null)
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Notification_PartsSalesOrder_CustomerAccountIsNull);
                            else {
                                this.DomainContext.Load(this.DomainContext.GetPartsRebateAccountsQuery().Where(r => r.AccountGroupId == partsSalesOrder.SalesUnit.AccountGroupId && r.CustomerCompanyId == partsSalesOrder.SubmitCompanyId && r.BranchId == salesUnitInsert.SalesUnit.OwnerCompanyId), LoadBehavior.RefreshCurrent, loadRebateAccount => {
                                    if(loadRebateAccount.HasError) {
                                        loadRebateAccount.MarkErrorAsHandled();
                                        DcsUtils.ShowDomainServiceOperationWindow(loadRebateAccount);
                                        return;
                                    }
                                    if(loadRebateAccount.Entities.Any()) {
                                        //this.txtRebateAmount.Text = loadRebateAccount.Entities.First().AccountBalanceAmount.ToString("c2");
                                        // this.lRebateAmount = loadRebateAccount.Entities.First().AccountBalanceAmount;
                                        // this.txtUseablePosition.Text = (partsSalesOrder.CustomerAccount.UseablePosition).ToString("c2");
                                        //partsSalesOrder.CustomerAccount.UseablePosition = partsSalesOrder.CustomerAccount.UseablePosition + lRebateAmount;
                                    } else {
                                        //this.txtRebateAmount.Text = "￥" + "0.00";
                                        //this.txtUseablePosition.Text = partsSalesOrder.CustomerAccount.UseablePosition.ToString("c2");
                                    }
                                    //this.orderUseablePosition.Text = (partsSalesOrder.CustomerAccount.AccountBalance + lRebateAmount + partsSalesOrder.CustomerAccount.CustomerCredenceAmount + (partsSalesOrder.CustomerAccount.TempCreditTotalFee ?? 0) - partsSalesOrder.CustomerAccount.LockBalance - lnotApprovePrice).ToString("C");
                                    //this.lorderUseablePosition = partsSalesOrder.CustomerAccount.AccountBalance + lRebateAmount + partsSalesOrder.CustomerAccount.CustomerCredenceAmount + (partsSalesOrder.CustomerAccount.TempCreditTotalFee ?? 0) - partsSalesOrder.CustomerAccount.LockBalance - lnotApprovePrice;
                                }, null);
                            }
                        }, null);
                    }, null);
                    break;
                case "ReceivingCompanyId":
                    //查询企业收获地址
                    this.InitCompanyAddress(partsSalesOrder.ReceivingCompanyId);//只查询登陆企业的收货地址。
                    break;
            }
        }

        private void LoadWarehouse(int submitCompanyId, int branchId, int salesCategoryId, int companyType) {
            this.DomainContext.Load(this.DomainContext.查询销售组织仓库Query(submitCompanyId, branchId, null, salesCategoryId, companyType).Where(r => r.Warehouse.Type != (int)DcsWarehouseType.虚拟库), LoadBehavior.RefreshCurrent, loadop => {
                if(loadop.HasError) {
                    loadop.MarkErrorAsHandled();
                    return;
                }
                this.KvSalesUnitAffiWarehouses.Clear();
                if(loadop.Entities != null)
                    foreach(var salesUnitAffiWarehouse in loadop.Entities)
                        if(salesUnitAffiWarehouse.Warehouse.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && salesUnitAffiWarehouse.Warehouse.IsQualityWarehouse != true)
                            this.KvSalesUnitAffiWarehouses.Add(salesUnitAffiWarehouse);
            }, null);
        }

        #endregion 界面事件

        protected override void OnEditCancelled() {
            base.OnEditCancelled();
            this.KvPartsSalesOrderTypes.Clear();
            this.KvCompanyAddresses.Clear();
            this.KvPartsSalesCategories.Clear();
            this.oldPartsSalesOrderTypeId = 0;
        }

        protected override void OnEditSubmitting() {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null || !this.PartsSalesOrderDetailForExportInfoDataGridView.CommitEdit())
                return;
            partsSalesOrder.ValidationErrors.Clear();
            foreach(var item in partsSalesOrder.PartsSalesOrderDetails) {
                item.ValidationErrors.Clear();
            }
            if(string.IsNullOrEmpty(partsSalesOrder.ContractCode)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_ContractCodeIsNull);
                return;
            }
            if(partsSalesOrder.WarehouseId == default(int) || partsSalesOrder.WarehouseId == null) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_WarehouseIsNull);
                return;
            }
            if(partsSalesOrder.PartsSalesOrderTypeId == default(int))
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_PartsSalesOrderTypeIsNull, new[] {
                    "PartsSalesOrderTypeName"
                }));
            if(partsSalesOrder.ShippingMethod == default(int))
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_ShippingMethodIsNull, new[] {
                    "ShippingMethod"
                }));
            if(partsSalesOrder.SalesUnitId == default(int))
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_SalesUnitIdIsNull, new[] {
                    "SalesUnitId"
                }));
            if(partsSalesOrder.SubmitCompanyId == default(int))
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_SubmitCompanyIdIsNull, new[] {
                    "SubmitCompanyName"
                }));
            if(string.IsNullOrEmpty(partsSalesOrder.ReceivingAddress))
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_ReceivingAddressIsNull, new[] {
                    "ReceivingAddress"
                }));
            if(partsSalesOrder.PartsSalesOrderTypeId == default(int))
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_PartsSalesOrderTypeIdIsNull, new[] {
                    "PartsSalesOrderTypeId"
                }));
            if(partsSalesOrder.ReceivingCompanyId == default(int))
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_ReceivingCompanyIdIsNull, new[] {
                    "ReceivingCompanyName"
                }));
            if(string.IsNullOrEmpty(partsSalesOrder.PackingMethod)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_PackingMethodIsNull);
                return;
            }
            if(partsSalesOrder.SalesUnitOwnerCompanyId == partsSalesOrder.SubmitCompanyId) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_SalesUnitOwner);
                return;
            }
            //if(partsSalesOrder.CustomerAccount == null) {
            //    var validationMessage = PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_AccountBalanceIsNull;
            //    validationMessage = string.Concat(validationMessage, Environment.NewLine, PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_PendingAmountIsNull);
            //    validationMessage = string.Concat(validationMessage, Environment.NewLine, PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_ShippedProductValueIsNull);
            //    validationMessage = string.Concat(validationMessage, Environment.NewLine, PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_CustomerCredenceAmountIsNull);
            //    validationMessage = string.Concat(validationMessage, Environment.NewLine, PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_UseablePositionIsNull);
            //    UIHelper.ShowAlertMessage(validationMessage);
            //    return;
            //}

            if(!partsSalesOrder.PartsSalesOrderDetails.Any()) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_PartsSalesOrderDetailsIsEmpty);
                return;
            }

            foreach(var detail in partsSalesOrder.PartsSalesOrderDetails) {
                detail.ValidationErrors.Clear();
                if(detail.OrderPrice == 0)
                    detail.ValidationErrors.Add(new ValidationResult(string.Format(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_InCurrentPartsSalesOrderTypeNameOrderSumIsZeroError, partsSalesOrder.PartsSalesOrderTypeName, detail.SparePartCode), new[] {
                        "OrderPrice"
                    }));
                if(detail.OrderedQuantity <= default(int))
                    detail.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderDetail_OrderedQuantityIsLessThanZeroError, new[] {
                        "OrderedQuantity"
                    }));
                if(!detail.HasValidationErrors)
                    ((IEditableObject)detail).EndEdit();
            }

            if(partsSalesOrder.HasValidationErrors || partsSalesOrder.PartsSalesOrderDetails.Any(e => e.HasValidationErrors))
                return;
            //if(partsSalesOrder.ReceivingWarehouseId == default(int)) {
            //    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_ReceivingWarehouse);
            //    return;
            //}
            partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
            ((IEditableObject)partsSalesOrder).EndEdit();
            //销售订单生成时 检查 销售组织隶属企业id和编号名称必须属于一个企业
            this.DomainContext.Load(this.DomainContext.GetCompaniesQuery(), LoadBehavior.RefreshCurrent, loadOpOwnerCompany => {
                if(loadOpOwnerCompany.HasError)
                    if(loadOpOwnerCompany.IsErrorHandled) {
                        loadOpOwnerCompany.MarkErrorAsHandled();
                        return;
                    }
                var companies = loadOpOwnerCompany.Entities;
                if(companies != null && companies.Any()) {
                    if(!companies.Any(r => r.Id == partsSalesOrder.SalesUnitOwnerCompanyId && r.Code == partsSalesOrder.SalesUnitOwnerCompanyCode && r.Name == partsSalesOrder.SalesUnitOwnerCompanyName)) {
                        UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_SalesUnitOwnerNew);
                        return;
                    }
                } else
                    return;
                var sparePartIds = partsSalesOrder.PartsSalesOrderDetails.Select(r => r.SparePartId).ToArray();
                //校验销售订单清单数是否大于分公司策略的最大开票行数
                this.DomainContext.Load(this.DomainContext.GetBranchstrategiesQuery().Where(r => r.BranchId == partsSalesOrder.BranchId), LoadBehavior.RefreshCurrent, loadOpS => {
                    if(loadOpS.HasError) {
                        if(loadOpS.IsErrorHandled)
                            loadOpS.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOpS);
                        return;
                    }
                    var branchstrategy = loadOpS.Entities.SingleOrDefault();
                    if(branchstrategy != null && partsSalesOrder.PartsSalesOrderDetails.Count > branchstrategy.MaxInvoiceRow) {
                        UIHelper.ShowAlertMessage(string.Format(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_MaxInvoiceRow, branchstrategy.MaxInvoiceRow));
                        return;
                    }
                    this.DomainContext.Load(this.DomainContext.GetRegionsQuery().Where(e => e.Type == 2 && e.Name == partsSalesOrder.Province), loadOpRegion => {
                        if(loadOpRegion.HasError)
                            return;
                        var province = loadOpRegion.Entities.SingleOrDefault();
                        if(province != null)
                            partsSalesOrder.ProvinceID = province.Id;
                        this.DomainContext.Load(this.DomainContext.查询企业库存Query(partsSalesOrder.SubmitCompanyId, sparePartIds), LoadBehavior.RefreshCurrent, loadOp => {
                            if(loadOp.HasError)
                                return;
                            var companyPartsStocks = loadOp.Entities;
                            bool flag = false;
                            foreach(var partsSalesOrderDetail in partsSalesOrder.PartsSalesOrderDetails) {
                                var entity = companyPartsStocks.Where(r => r.SparePartId == partsSalesOrderDetail.SparePartId);
                                if(entity == null || !entity.Any()) {
                                    flag = true;
                                    break;
                                }
                                if(partsSalesOrderDetail.OrderedQuantity >= entity.FirstOrDefault().UsableQuantity) {
                                    flag = true;
                                    break;
                                }
                            }
                            if(!flag)
                                DcsUtils.Confirm(PartsSalesUIStrings.DataEditView_Confirm_PartsSalesOrder_CompanyStockSatisfyOrder, () => this.CheckNumberMultiple(partsSalesOrder));
                            else
                                this.CheckNumberMultiple(partsSalesOrder);
                        }, null);
                    }, null);
                    //}, null);
                }, null);
            }, null);
            //5.如果订单总金额>可用额度时，系统提示“当前订单总金额大于账户可用额度， 是否继续保存”。用户选“是”，调用新增服务，否则，反回新增界面。
        }
        private void CheckNumberMultiple(PartsSalesOrder partsSalesOrder) {
            var sparepartIds = partsSalesOrder.PartsSalesOrderDetails.Select(r => r.SparePartId).ToArray();
            this.DomainContext.Load(DomainContext.GetSparePartsByIdsQuery(sparepartIds), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled) {
                        loadOp.MarkErrorAsHandled();
                        return;
                    }
                }
                this.DomainContext.Load(this.DomainContext.GetAgencyAffiBranchesQuery().Where(r => r.BranchId == partsSalesOrder.BranchId), LoadBehavior.RefreshCurrent, aloadOp => {
                    if(aloadOp.HasError) {
                        if(!aloadOp.IsErrorHandled)
                            aloadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(aloadOp);
                        return;
                    }
                    var entity = aloadOp.Entities.Select(e => e.BranchId).ToArray();
                    if(loadOp.Entities != null && loadOp.Entities.Any()) {
                        var spareParts = loadOp.Entities.ToArray();
                        var isNotCheckMInPackingAmount = (partsSalesOrder.SalesUnitOwnerCompanyType == (int)DcsCompanyType.代理库 || partsSalesOrder.SalesUnitOwnerCompanyType == (int)DcsCompanyType.服务站兼代理库)
                            && entity.Any();
                        if(spareParts.Any(r => r.MInPackingAmount == null || r.MInPackingAmount == 0) && !isNotCheckMInPackingAmount) {
                            UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_MInPackingAmount);
                            return;
                        }
                        if(partsSalesOrder.SalesUnitOwnerCompanyType == (int)DcsCompanyType.分公司) {
                            this.DomainContext.Load(this.DomainContext.GetBranchstrategiesQuery().Where(r => r.BranchId == partsSalesOrder.SalesUnitOwnerCompanyId), LoadBehavior.RefreshCurrent, loadOpS => {
                                if(loadOpS.HasError) {
                                    if(loadOpS.IsErrorHandled)
                                        loadOpS.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOpS);
                                    return;
                                }
                                var branchstrategy = loadOpS.Entities.SingleOrDefault();
                                if(branchstrategy != null && branchstrategy.PurDistributionStrategy.HasValue && branchstrategy.PurDistributionStrategy == (int)DcsPurDistStrategy.纳入统购分销) {
                                    base.OnEditSubmitting();
                                } else {
                                    this.CheckAllowCustomerCredit(partsSalesOrder);
                                }
                            }, null);
                        } else {
                            this.CheckAllowCustomerCredit(partsSalesOrder);
                        }
                    }
                }, null);
            }, null);
        }
        private void CheckAllowCustomerCredit(PartsSalesOrder partsSalesOrder) {
            this.DomainContext.Load(this.DomainContext.查询客户可用资金含未审批订单Query(partsSalesOrder.CustomerAccountId, partsSalesOrder.CustomerAccount.AccountGroupId, partsSalesOrder.InvoiceReceiveCompanyId, null, partsSalesOrder.SalesCategoryId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled) {
                        loadOp.MarkErrorAsHandled();
                        return;
                    }
                }
                if(loadOp.Entities != null && loadOp.Entities.Any()) {
                    var customerAccount = loadOp.Entities.First();
                    if(customerAccount.UseablePosition < partsSalesOrder.TotalAmount) {
                        UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_TotalAmount);
                        return;
                    }
                    base.OnEditSubmitting();
                } else {
                    base.OnEditSubmitting();
                }
            }, null);

        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(r => r.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadCom => {
                if(loadCom.HasError) {
                    loadCom.MarkErrorAsHandled();
                    return;
                }
                if(loadCom.Entities != null && loadCom.Entities.Any()) {
                    this.currentCompanyType = loadCom.Entities.First().Type;
                }
                this.DomainContext.Load(this.DomainContext.GetPartsSalesOrderWithPartsSalesOrderDetailsAndCustomerAccountQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    var entity = loadOp.Entities.FirstOrDefault();
                    if(entity == null)
                        return;
                    entity.Status = (int)DcsPartsSalesOrderStatus.新增;
                    this.DomainContext.Load(this.DomainContext.查询销售组织仓库Query(entity.SubmitCompanyId, entity.BranchId, null, entity.SalesCategoryId, null), LoadBehavior.RefreshCurrent, loadop => {
                        if(loadop.HasError) {
                            loadop.MarkErrorAsHandled();
                            return;
                        }
                        var warhouseId = entity.WarehouseId.Value;
                        this.KvSalesUnitAffiWarehouses.Clear();
                        if(loadop.Entities != null)
                            foreach(var salesUnitAffiWarehouse in loadop.Entities) {
                                this.KvSalesUnitAffiWarehouses.Add(salesUnitAffiWarehouse);
                            }
                        //isCanCheck = false;
                        entity.WarehouseId = warhouseId;
                        //isCanCheck = true;
                    }, null);
                    this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(r => r.Id == entity.SalesUnitOwnerCompanyId), LoadBehavior.RefreshCurrent, loadOpCom => {
                        if(loadOpCom.HasError) {
                            loadOpCom.MarkErrorAsHandled();
                            return;
                        }
                        if(loadOpCom.Entities != null && loadOpCom.Entities.Any()) {
                            entity.SalesUnitOwnerCompanyType = loadOpCom.Entities.First().Type;
                        }
                    }, null);

                    #region 加载品牌
                    if(currentCompanyType == (int)DcsCompanyType.分公司) {
                        if(entity.InvoiceReceiveCompanyType == (int)DcsCompanyType.代理库 || entity.InvoiceReceiveCompanyType == (int)DcsCompanyType.服务站兼代理库) {
                            this.kvPartsSalesCategories.Clear();
                            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOption => {
                                if(loadOption.HasError)
                                    return;
                                this.KvPartsSalesCategories.Clear();
                                foreach(var partsSalesCategory in loadOption.Entities) {
                                    this.KvPartsSalesCategories.Add(new KeyValuePair {
                                        Key = partsSalesCategory.Id,
                                        Value = partsSalesCategory.Name
                                    });
                                }
                            }, null);
                        }
                        if(entity.InvoiceReceiveCompanyType == (int)DcsCompanyType.服务站 || entity.InvoiceReceiveCompanyType == (int)DcsCompanyType.集团企业) {
                            this.kvPartsSalesCategories.Clear();
                            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesByDealerServiceInfoQuery(BaseApp.Current.CurrentUserData.EnterpriseId, entity.SubmitCompanyId), LoadBehavior.RefreshCurrent, loadOp1 => {
                                if(loadOp1.HasError)
                                    return;
                                this.KvPartsSalesCategories.Clear();
                                foreach(var partsSalesCategory in loadOp1.Entities) {
                                    this.KvPartsSalesCategories.Add(new KeyValuePair {
                                        Key = partsSalesCategory.Id,
                                        Value = partsSalesCategory.Name
                                    });
                                }
                            }, null);
                        }
                    }
                    if(entity.InvoiceReceiveCompanyType == (int)DcsCompanyType.分公司) {
                        this.kvPartsSalesCategories.Clear();
                        this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOption => {
                            if(loadOption.HasError)
                                return;
                            this.KvPartsSalesCategories.Clear();
                            foreach(var partsSalesCategory in loadOption.Entities) {
                                this.KvPartsSalesCategories.Add(new KeyValuePair {
                                    Key = partsSalesCategory.Id,
                                    Value = partsSalesCategory.Name
                                });
                            }
                        }, null);
                    }

                    if(currentCompanyType == (int)DcsCompanyType.代理库 || currentCompanyType == (int)DcsCompanyType.服务站兼代理库) {
                        this.kvPartsSalesCategories.Clear();
                        this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesByAgencyDealerRelationQuery(BaseApp.Current.CurrentUserData.EnterpriseId, entity.SubmitCompanyId), LoadBehavior.RefreshCurrent, loadOp2 => {
                            if(loadOp2.HasError) {
                                loadOp2.MarkErrorAsHandled();
                                return;
                            }
                            this.KvPartsSalesCategories.Clear();
                            foreach(var partsSalesCategory in loadOp2.Entities) {
                                this.KvPartsSalesCategories.Add(new KeyValuePair {
                                    Key = partsSalesCategory.Id,
                                    Value = partsSalesCategory.Name
                                });
                            }
                        }, null);
                    }
                    #endregion
                    this.DomainContext.Load(this.DomainContext.GetPartsSalesOrderTypeWithSettleTypesQuery((int)DcsSalesOrderType_SettleType.出口结算), LoadBehavior.RefreshCurrent, loadOrder => {
                        if(loadOrder.HasError)
                            return;
                        this.KvPartsSalesOrderTypes.Clear();
                        foreach(var partsSalesOrderType in loadOrder.Entities) {
                            this.KvPartsSalesOrderTypes.Add(new KeyValuePair {
                                Key = partsSalesOrderType.Id,
                                Value = partsSalesOrderType.Name,
                            });
                        }
                        this.SetObjectToEdit(entity);
                        this.InitCompanyAddress(entity.ReceivingCompanyId);//只查询登陆企业的收货地址。
                    }, null);
                }, null);
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override bool OnRequestCanSubmit()
        {
            this.txtSalesCategory.SelectedIndex = 0;//默认品牌
            return true;
        }

        protected override string BusinessName {
            get {
                return PartsSalesUIStrings.BusinessName_PartsSalesOrderExport;
            }
        }

        public ObservableCollection<CompanyAddress> KvCompanyAddresses {
            get {
                return this.kvCompanyAddresses ?? (this.kvCompanyAddresses = new ObservableCollection<CompanyAddress>());
            }
        }

        public object KvShippingMethods {
            get {
                return this.KeyValueManager[this.kvNames[1]];
            }
        }

        public ObservableCollection<KeyValuePair> KvSalesUnits {
            get {
                return this.kvSalesUnits ?? (this.kvSalesUnits = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvPartsSalesOrderTypes {
            get {
                return this.kvPartsSalesOrderTypes ?? (this.kvPartsSalesOrderTypes = new ObservableCollection<KeyValuePair>());
            }
        }

        public object KvSecondLevelOrderTypes {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public object KvPriceTypes {
            get {
                return this.KeyValueManager[this.kvNames[2]];
            }
        }

        public object KvPackingMethods {
            get {
                return this.KeyValueManager[this.kvNames[3]];
            }
        }

        private void DcsComboBox_SelectionChanged_1(object sender, SelectionChangedEventArgs e) {
            var comboBox = sender as RadComboBox;
            if(comboBox == null)
                return;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            var companyAddress = comboBox.SelectedItem as CompanyAddress;
            if(companyAddress == null) {
                partsSalesOrder.ContactPerson = string.Empty;
                partsSalesOrder.ContactPhone = string.Empty;
                return;
            }
            partsSalesOrder.CompanyAddressId = companyAddress.Id;
            partsSalesOrder.ContactPerson = companyAddress.ContactPerson;
            partsSalesOrder.ContactPhone = companyAddress.ContactPhone;
        }

        public void OnCustomEditSubmitted() {
            this.KvPartsSalesOrderTypes.Clear();
            this.KvCompanyAddresses.Clear();
            this.KvPartsSalesCategories.Clear();
            this.oldPartsSalesOrderTypeId = 0;
        }
    }
}