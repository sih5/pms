﻿using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesOrderForRebateApproveDataEditView {
        private DataGridViewBase partsSalesOrderProcessDetailDataGridView;
        private KeyValueManager keyValueManager;

        private readonly string[] kvNames = new[] {
            "PartsShipping_Method"
        };

        public PartsSalesOrderForRebateApproveDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData();
            this.Initializer.Register(this.CreateUI);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesOrderWithPartsSalesOrderDetailsAndCustomerAccountQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                foreach(var detail in entity.PartsSalesOrderDetails) {
                    detail.DiscountedPrice = detail.OriginalPrice * (decimal)detail.CustOrderPriceGradeCoefficient - detail.OrderPrice;
                    detail.OrderSum = detail.OrderPrice * detail.OrderedQuantity;
                }
                entity.TotalAmount = entity.PartsSalesOrderDetails.Sum(r => r.OrderSum);
                if(entity.PartsSalesOrderDetails.Any(r => r.OriginalPrice == 0))
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Notification_PartsSalesOrderDetail_OriginalPriceIsZero);
                if(entity.PartsSalesOrderDetails.Any(r => r.CustOrderPriceGradeCoefficient.CompareTo(0) == 0))
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Notification_PartsSalesOrderDetail_CustOrderPriceGradeCoefficientIsZero);
                if(entity.PartsSalesOrderDetails.Any(r => r.OrderedQuantity == 0))
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Notification_PartsSalesOrderDetail_OrderedQuantityIsZero);
                var amount = entity.PartsSalesOrderDetails.Sum(r => (double)r.OriginalPrice * r.CustOrderPriceGradeCoefficient * r.OrderedQuantity);
                //检查是否是除零
                if(amount.CompareTo(0) > 0)
                    entity.SalesActivityDiscountRate = (double)entity.TotalAmount / amount;
                entity.SalesActivityDiscountAmount = entity.PartsSalesOrderDetails.Sum(r => r.DiscountedPrice * r.OrderedQuantity);
                this.SetObjectToEdit(entity);
            }, null);
        }

        private DataGridViewBase PartsSalesOrderProcessDetailDataGridView {
            get {
                if(this.partsSalesOrderProcessDetailDataGridView == null) {
                    this.partsSalesOrderProcessDetailDataGridView = DI.GetDataGridView("PartsSalesOrderDetailForRebateApprove");
                    this.partsSalesOrderProcessDetailDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsSalesOrderProcessDetailDataGridView;
            }
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDetailPanel("PartsSalesOrderForApprove"));
            this.SubRoot.Children.Add(this.CreateVerticalLine(2));
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Content = this.PartsSalesOrderProcessDetailDataGridView,
                Header = Utils.GetEntityLocalizedName(typeof(PartsSalesOrderProcess), "PartsSalesOrderProcessDetails")
            });
            tabControl.SetValue(Grid.ColumnProperty, 3);
            this.SubRoot.Children.Add(tabControl);
        }

        protected override string Title {
            get {
                return PartsSalesUIStrings.DataEditView_Title_RebateApprove_PartsSalesOrder;
            }
        }

        protected override void OnEditSubmitting() {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null || !this.PartsSalesOrderProcessDetailDataGridView.CommitEdit())
                return;
            partsSalesOrder.ValidationErrors.Clear();
            if(partsSalesOrder.PartsSalesOrderDetails.Any(e => e.CustOrderPriceGradeCoefficient.CompareTo(0) == 0)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Notification_PartsSalesOrder_CustOrderPriceGradeCoefficientIsNull);
                return;
            }
            foreach(var detail in partsSalesOrder.PartsSalesOrderDetails) {
                detail.ValidationErrors.Clear();
                if(!detail.HasValidationErrors)
                    ((IEditableObject)detail).EndEdit();
            }

            if(partsSalesOrder.HasValidationErrors || partsSalesOrder.PartsSalesOrderDetails.Any(e => e.HasValidationErrors))
                return;
            ((IEditableObject)partsSalesOrder).EndEdit();
            if(partsSalesOrder.TotalAmount > partsSalesOrder.CustomerAccount.UseablePosition)
                DcsUtils.Confirm(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_TheMoneyIsNotEnoughToPay, () => base.OnEditSubmitting());
            else
                base.OnEditSubmitting();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);

            var entity = new PartsSalesOrder();
            entity.SalesActivityDiscountRate = (double)entity.TotalAmount / entity.PartsSalesOrderDetails.Sum(r => (double)r.OriginalPrice * r.CustOrderPriceGradeCoefficient * r.OrderedQuantity);
            entity.SalesActivityDiscountAmount = entity.PartsSalesOrderDetails.Sum(r => r.DiscountedPrice * r.OrderedQuantity);
        }
    }
}